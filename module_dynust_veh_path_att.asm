; mark_description "Intel(R) Visual Fortran Compiler XE for applications running on IA-32, Version 13.1.1.171 Build 20130313";
; mark_description "-S";
	.686P
 	.387
	OPTION DOTNAME
	ASSUME	CS:FLAT,DS:FLAT,SS:FLAT
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
TXTST0:
; -- Begin  _DYNUST_VEH_PATH_ATT_MODULE$
; mark_begin;
IF @Version GE 800
  .MMX
ELSEIF @Version GE 612
  .MMX
  MMWORD TEXTEQU <QWORD>
ENDIF
IF @Version GE 800
  .XMM
ELSEIF @Version GE 614
  .XMM
  XMMWORD TEXTEQU <OWORD>
ENDIF
       ALIGN     16
	PUBLIC _DYNUST_VEH_PATH_ATT_MODULE$
_DYNUST_VEH_PATH_ATT_MODULE$	PROC NEAR 
.B1.1:                          ; Preds .B1.0
        ret                                                     ;1.8
        ALIGN     16
                                ; LOE
; mark_end;
_DYNUST_VEH_PATH_ATT_MODULE$ ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_VEH_PATH_ATT_MODULE$
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_2DSETUP
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_2DSETUP
_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_2DSETUP	PROC NEAR 
; parameter 1: 8 + ebp
.B2.1:                          ; Preds .B2.0
        push      ebp                                           ;60.12
        mov       ebp, esp                                      ;60.12
        and       esp, -16                                      ;60.12
        push      esi                                           ;60.12
        push      edi                                           ;60.12
        push      ebx                                           ;60.12
        sub       esp, 196                                      ;60.12
        mov       esi, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY+12] ;65.3
        test      esi, 1                                        ;65.7
        je        .B2.17        ; Prob 60%                      ;65.7
                                ; LOE esi
.B2.2:                          ; Preds .B2.1
        mov       edx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAYSIZE] ;66.10
        test      edx, edx                                      ;66.10
        jle       .B2.14        ; Prob 2%                       ;66.10
                                ; LOE edx esi
.B2.3:                          ; Preds .B2.2
        imul      edi, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY+32], -76 ;
        mov       eax, 1                                        ;
        add       edi, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY] ;
        mov       ebx, 76                                       ;
        mov       DWORD PTR [4+esp], edx                        ;
        mov       DWORD PTR [esp], esi                          ;
        mov       esi, eax                                      ;
                                ; LOE ebx esi edi
.B2.4:                          ; Preds .B2.12 .B2.3
        movsx     edx, WORD PTR [74+ebx+edi]                    ;66.10
        test      edx, edx                                      ;66.10
        jle       .B2.8         ; Prob 79%                      ;66.10
                                ; LOE ebx esi edi
.B2.5:                          ; Preds .B2.4
        mov       edx, DWORD PTR [48+ebx+edi]                   ;66.10
        mov       ecx, edx                                      ;66.10
        shr       ecx, 1                                        ;66.10
        and       edx, 1                                        ;66.10
        and       ecx, 1                                        ;66.10
        add       edx, edx                                      ;66.10
        shl       ecx, 2                                        ;66.10
        or        ecx, 1                                        ;66.10
        or        ecx, edx                                      ;66.10
        or        ecx, 262144                                   ;66.10
        push      ecx                                           ;66.10
        push      DWORD PTR [36+ebx+edi]                        ;66.10
        call      _for_dealloc_allocatable                      ;66.10
                                ; LOE eax ebx esi edi
.B2.51:                         ; Preds .B2.5
        add       esp, 8                                        ;66.10
                                ; LOE eax ebx esi edi
.B2.6:                          ; Preds .B2.51
        and       DWORD PTR [48+ebx+edi], -2                    ;66.10
        mov       DWORD PTR [36+ebx+edi], 0                     ;66.10
        test      eax, eax                                      ;66.10
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VECTORERROR], eax ;66.10
        jne       .B2.44        ; Prob 5%                       ;66.10
                                ; LOE ebx esi edi
.B2.7:                          ; Preds .B2.6 .B2.60
        xor       edx, edx                                      ;66.10
        mov       WORD PTR [74+ebx+edi], dx                     ;66.10
                                ; LOE ebx esi edi
.B2.8:                          ; Preds .B2.4 .B2.7
        movsx     edx, WORD PTR [72+ebx+edi]                    ;66.10
        test      edx, edx                                      ;66.10
        jle       .B2.12        ; Prob 79%                      ;66.10
                                ; LOE ebx esi edi
.B2.9:                          ; Preds .B2.8
        mov       edx, DWORD PTR [12+ebx+edi]                   ;66.10
        mov       ecx, edx                                      ;66.10
        shr       ecx, 1                                        ;66.10
        and       edx, 1                                        ;66.10
        and       ecx, 1                                        ;66.10
        add       edx, edx                                      ;66.10
        shl       ecx, 2                                        ;66.10
        or        ecx, 1                                        ;66.10
        or        ecx, edx                                      ;66.10
        or        ecx, 262144                                   ;66.10
        push      ecx                                           ;66.10
        push      DWORD PTR [ebx+edi]                           ;66.10
        call      _for_dealloc_allocatable                      ;66.10
                                ; LOE eax ebx esi edi
.B2.52:                         ; Preds .B2.9
        add       esp, 8                                        ;66.10
                                ; LOE eax ebx esi edi
.B2.10:                         ; Preds .B2.52
        and       DWORD PTR [12+ebx+edi], -2                    ;66.10
        mov       DWORD PTR [ebx+edi], 0                        ;66.10
        test      eax, eax                                      ;66.10
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VECTORERROR], eax ;66.10
        jne       .B2.41        ; Prob 5%                       ;66.10
                                ; LOE ebx esi edi
.B2.11:                         ; Preds .B2.10 .B2.59
        xor       edx, edx                                      ;66.10
        mov       WORD PTR [72+ebx+edi], dx                     ;66.10
                                ; LOE ebx esi edi
.B2.12:                         ; Preds .B2.8 .B2.11
        inc       esi                                           ;66.10
        add       ebx, 76                                       ;66.10
        cmp       esi, DWORD PTR [4+esp]                        ;66.10
        jle       .B2.4         ; Prob 82%                      ;66.10
                                ; LOE ebx esi edi
.B2.13:                         ; Preds .B2.12
        mov       esi, DWORD PTR [esp]                          ;
                                ; LOE esi
.B2.14:                         ; Preds .B2.13 .B2.2
        mov       edx, esi                                      ;66.10
        mov       eax, esi                                      ;66.10
        shr       edx, 1                                        ;66.10
        and       eax, 1                                        ;66.10
        and       edx, 1                                        ;66.10
        add       eax, eax                                      ;66.10
        shl       edx, 2                                        ;66.10
        or        edx, 1                                        ;66.10
        or        edx, eax                                      ;66.10
        or        edx, 262144                                   ;66.10
        push      edx                                           ;66.10
        push      DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY] ;66.10
        call      _for_dealloc_allocatable                      ;66.10
                                ; LOE eax esi
.B2.53:                         ; Preds .B2.14
        add       esp, 8                                        ;66.10
                                ; LOE eax esi
.B2.15:                         ; Preds .B2.53
        and       esi, -2                                       ;66.10
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY], 0 ;66.10
        test      eax, eax                                      ;66.10
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY+12], esi ;66.10
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VECTORERROR], eax ;66.10
        jne       .B2.39        ; Prob 5%                       ;66.10
                                ; LOE esi
.B2.16:                         ; Preds .B2.58 .B2.15
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAYSIZE], 0 ;66.10
                                ; LOE esi
.B2.17:                         ; Preds .B2.16 .B2.1
        mov       ebx, DWORD PTR [8+ebp]                        ;60.12
        test      esi, 1                                        ;71.13
        jne       .B2.25        ; Prob 40%                      ;71.13
                                ; LOE ebx
.B2.18:                         ; Preds .B2.17
        xor       esi, esi                                      ;72.5
        lea       edx, DWORD PTR [164+esp]                      ;72.5
        mov       eax, DWORD PTR [ebx]                          ;72.5
        test      eax, eax                                      ;72.5
        push      76                                            ;72.5
        cmovl     eax, esi                                      ;72.5
        push      eax                                           ;72.5
        push      2                                             ;72.5
        push      edx                                           ;72.5
        call      _for_check_mult_overflow                      ;72.5
                                ; LOE eax ebx esi
.B2.19:                         ; Preds .B2.18
        mov       edx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY+12] ;72.5
        and       eax, 1                                        ;72.5
        and       edx, 1                                        ;72.5
        add       edx, edx                                      ;72.5
        shl       eax, 4                                        ;72.5
        or        edx, 1                                        ;72.5
        or        edx, eax                                      ;72.5
        or        edx, 262144                                   ;72.5
        push      edx                                           ;72.5
        push      OFFSET FLAT: _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY ;72.5
        push      DWORD PTR [188+esp]                           ;72.5
        call      _for_alloc_allocatable                        ;72.5
                                ; LOE eax ebx esi
.B2.55:                         ; Preds .B2.19
        add       esp, 28                                       ;72.5
        mov       edi, eax                                      ;72.5
                                ; LOE ebx esi edi
.B2.20:                         ; Preds .B2.55
        test      edi, edi                                      ;72.5
        je        .B2.23        ; Prob 50%                      ;72.5
                                ; LOE ebx esi edi
.B2.21:                         ; Preds .B2.20
        mov       DWORD PTR [128+esp], 0                        ;74.5
        lea       edx, DWORD PTR [128+esp]                      ;74.5
        mov       DWORD PTR [168+esp], 18                       ;74.5
        lea       eax, DWORD PTR [168+esp]                      ;74.5
        mov       DWORD PTR [172+esp], OFFSET FLAT: __STRLITPACK_32 ;74.5
        push      32                                            ;74.5
        push      eax                                           ;74.5
        push      OFFSET FLAT: __STRLITPACK_34.0.2              ;74.5
        push      -2088435968                                   ;74.5
        push      911                                           ;74.5
        push      edx                                           ;74.5
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VECTORERROR], edi ;72.5
        call      _for_write_seq_lis                            ;74.5
                                ; LOE ebx esi
.B2.22:                         ; Preds .B2.21
        push      32                                            ;75.4
        push      esi                                           ;75.4
        push      esi                                           ;75.4
        push      -2088435968                                   ;75.4
        push      esi                                           ;75.4
        push      OFFSET FLAT: __STRLITPACK_35                  ;75.4
        call      _for_stop_core                                ;75.4
                                ; LOE ebx
.B2.56:                         ; Preds .B2.22
        add       esp, 48                                       ;75.4
        jmp       .B2.25        ; Prob 100%                     ;75.4
                                ; LOE ebx
.B2.23:                         ; Preds .B2.20
        mov       ecx, DWORD PTR [ebx]                          ;72.5
        mov       edx, 1                                        ;72.5
        test      ecx, ecx                                      ;72.5
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY+16], edx ;72.5
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY+8], esi ;72.5
        cmovge    esi, ecx                                      ;72.5
        mov       eax, 76                                       ;72.5
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY+32], edx ;72.5
        lea       edx, DWORD PTR [124+esp]                      ;72.5
        push      eax                                           ;72.5
        push      esi                                           ;72.5
        push      2                                             ;72.5
        push      edx                                           ;72.5
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY+12], 133 ;72.5
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY+4], eax ;72.5
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY+24], esi ;72.5
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY+28], eax ;72.5
        call      _for_check_mult_overflow                      ;72.5
                                ; LOE ebx edi
.B2.57:                         ; Preds .B2.23
        add       esp, 16                                       ;72.5
                                ; LOE ebx edi
.B2.24:                         ; Preds .B2.57
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VECTORERROR], edi ;72.5
                                ; LOE ebx
.B2.25:                         ; Preds .B2.56 .B2.17 .B2.24
        mov       eax, DWORD PTR [ebx]                          ;80.4
        test      eax, eax                                      ;80.4
        jle       .B2.38        ; Prob 50%                      ;80.4
                                ; LOE eax
.B2.26:                         ; Preds .B2.25
        mov       ecx, eax                                      ;80.4
        shr       ecx, 31                                       ;80.4
        add       ecx, eax                                      ;80.4
        sar       ecx, 1                                        ;80.4
        mov       ebx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY] ;80.4
        test      ecx, ecx                                      ;80.4
        mov       edx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY+32] ;80.4
        jbe       .B2.48        ; Prob 10%                      ;80.4
                                ; LOE eax edx ecx ebx
.B2.27:                         ; Preds .B2.26
        imul      esi, edx, -76                                 ;
        xor       edi, edi                                      ;
        add       esi, ebx                                      ;
        mov       DWORD PTR [180+esp], eax                      ;
        mov       DWORD PTR [176+esp], ebx                      ;
        xor       ebx, ebx                                      ;
        mov       eax, ebx                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B2.28:                         ; Preds .B2.28 .B2.27
        inc       edi                                           ;80.4
        mov       WORD PTR [148+eax+esi], bx                    ;80.4
        mov       WORD PTR [224+eax+esi], bx                    ;80.4
        add       eax, 152                                      ;80.4
        cmp       edi, ecx                                      ;80.4
        jb        .B2.28        ; Prob 63%                      ;80.4
                                ; LOE eax edx ecx ebx esi edi
.B2.29:                         ; Preds .B2.28
        mov       ebx, DWORD PTR [176+esp]                      ;
        lea       esi, DWORD PTR [1+edi+edi]                    ;80.4
        mov       eax, DWORD PTR [180+esp]                      ;
                                ; LOE eax edx ecx ebx esi
.B2.30:                         ; Preds .B2.29 .B2.48
        lea       edi, DWORD PTR [-1+esi]                       ;80.4
        cmp       eax, edi                                      ;80.4
        jbe       .B2.32        ; Prob 10%                      ;80.4
                                ; LOE eax edx ecx ebx esi
.B2.31:                         ; Preds .B2.30
        mov       edi, edx                                      ;80.4
        neg       edi                                           ;80.4
        add       edi, esi                                      ;80.4
        xor       esi, esi                                      ;80.4
        imul      edi, edi, 76                                  ;80.4
        mov       WORD PTR [72+ebx+edi], si                     ;80.4
                                ; LOE eax edx ecx ebx
.B2.32:                         ; Preds .B2.30 .B2.31
        test      ecx, ecx                                      ;81.4
        jbe       .B2.47        ; Prob 10%                      ;81.4
                                ; LOE eax edx ecx ebx
.B2.33:                         ; Preds .B2.32
        imul      esi, edx, -76                                 ;
        xor       edi, edi                                      ;
        add       esi, ebx                                      ;
        mov       DWORD PTR [180+esp], eax                      ;
        mov       DWORD PTR [176+esp], ebx                      ;
        xor       ebx, ebx                                      ;
        mov       eax, ebx                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B2.34:                         ; Preds .B2.34 .B2.33
        inc       edi                                           ;81.4
        mov       WORD PTR [150+eax+esi], bx                    ;81.4
        mov       WORD PTR [226+eax+esi], bx                    ;81.4
        add       eax, 152                                      ;81.4
        cmp       edi, ecx                                      ;81.4
        jb        .B2.34        ; Prob 63%                      ;81.4
                                ; LOE eax edx ecx ebx esi edi
.B2.35:                         ; Preds .B2.34
        mov       ebx, DWORD PTR [176+esp]                      ;
        lea       esi, DWORD PTR [1+edi+edi]                    ;81.4
        mov       eax, DWORD PTR [180+esp]                      ;
                                ; LOE eax edx ebx esi
.B2.36:                         ; Preds .B2.35 .B2.47
        lea       ecx, DWORD PTR [-1+esi]                       ;81.4
        cmp       eax, ecx                                      ;81.4
        jbe       .B2.38        ; Prob 10%                      ;81.4
                                ; LOE eax edx ebx esi
.B2.37:                         ; Preds .B2.36
        neg       edx                                           ;81.4
        add       edx, esi                                      ;81.4
        imul      ecx, edx, 76                                  ;81.4
        xor       edx, edx                                      ;81.4
        mov       WORD PTR [74+ebx+ecx], dx                     ;81.4
                                ; LOE eax
.B2.38:                         ; Preds .B2.36 .B2.25 .B2.37
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAYSIZE], eax ;82.4
        add       esp, 196                                      ;83.1
        pop       ebx                                           ;83.1
        pop       edi                                           ;83.1
        pop       esi                                           ;83.1
        mov       esp, ebp                                      ;83.1
        pop       ebp                                           ;83.1
        ret                                                     ;83.1
                                ; LOE
.B2.39:                         ; Preds .B2.15                  ; Infreq
        mov       DWORD PTR [esp], 0                            ;66.10
        lea       edx, DWORD PTR [esp]                          ;66.10
        mov       DWORD PTR [32+esp], 14                        ;66.10
        lea       eax, DWORD PTR [32+esp]                       ;66.10
        mov       DWORD PTR [36+esp], OFFSET FLAT: __STRLITPACK_0 ;66.10
        push      32                                            ;66.10
        push      eax                                           ;66.10
        push      OFFSET FLAT: __STRLITPACK_69.0.13             ;66.10
        push      -2088435968                                   ;66.10
        push      911                                           ;66.10
        push      edx                                           ;66.10
        call      _for_write_seq_lis                            ;66.10
                                ; LOE esi
.B2.40:                         ; Preds .B2.39                  ; Infreq
        push      32                                            ;66.10
        xor       eax, eax                                      ;66.10
        push      eax                                           ;66.10
        push      eax                                           ;66.10
        push      -2088435968                                   ;66.10
        push      eax                                           ;66.10
        push      OFFSET FLAT: __STRLITPACK_70                  ;66.10
        call      _for_stop_core                                ;66.10
                                ; LOE esi
.B2.58:                         ; Preds .B2.40                  ; Infreq
        add       esp, 48                                       ;66.10
        jmp       .B2.16        ; Prob 100%                     ;66.10
                                ; LOE esi
.B2.41:                         ; Preds .B2.10                  ; Infreq
        mov       DWORD PTR [80+esp], 0                         ;66.10
        lea       ecx, DWORD PTR [80+esp]                       ;66.10
        mov       DWORD PTR [112+esp], 37                       ;66.10
        lea       edx, DWORD PTR [112+esp]                      ;66.10
        mov       DWORD PTR [116+esp], OFFSET FLAT: __STRLITPACK_4 ;66.10
        push      32                                            ;66.10
        push      edx                                           ;66.10
        push      OFFSET FLAT: __STRLITPACK_63.0.10             ;66.10
        push      -2088435968                                   ;66.10
        push      911                                           ;66.10
        push      ecx                                           ;66.10
        call      _for_write_seq_lis                            ;66.10
                                ; LOE ebx esi edi
.B2.42:                         ; Preds .B2.41                  ; Infreq
        mov       DWORD PTR [104+esp], 0                        ;66.10
        lea       edx, DWORD PTR [184+esp]                      ;66.10
        mov       DWORD PTR [184+esp], esi                      ;66.10
        push      32                                            ;66.10
        push      edx                                           ;66.10
        push      OFFSET FLAT: __STRLITPACK_64.0.10             ;66.10
        push      -2088435968                                   ;66.10
        push      911                                           ;66.10
        lea       ecx, DWORD PTR [124+esp]                      ;66.10
        push      ecx                                           ;66.10
        call      _for_write_seq_lis                            ;66.10
                                ; LOE ebx esi edi
.B2.43:                         ; Preds .B2.42                  ; Infreq
        push      32                                            ;66.10
        xor       edx, edx                                      ;66.10
        push      edx                                           ;66.10
        push      edx                                           ;66.10
        push      -2088435968                                   ;66.10
        push      edx                                           ;66.10
        push      OFFSET FLAT: __STRLITPACK_65                  ;66.10
        call      _for_stop_core                                ;66.10
                                ; LOE ebx esi edi
.B2.59:                         ; Preds .B2.43                  ; Infreq
        add       esp, 72                                       ;66.10
        jmp       .B2.11        ; Prob 100%                     ;66.10
                                ; LOE ebx esi edi
.B2.44:                         ; Preds .B2.6                   ; Infreq
        mov       DWORD PTR [48+esp], 0                         ;66.10
        lea       ecx, DWORD PTR [48+esp]                       ;66.10
        mov       DWORD PTR [40+esp], 37                        ;66.10
        lea       edx, DWORD PTR [40+esp]                       ;66.10
        mov       DWORD PTR [44+esp], OFFSET FLAT: __STRLITPACK_2 ;66.10
        push      32                                            ;66.10
        push      edx                                           ;66.10
        push      OFFSET FLAT: __STRLITPACK_66.0.11             ;66.10
        push      -2088435968                                   ;66.10
        push      911                                           ;66.10
        push      ecx                                           ;66.10
        call      _for_write_seq_lis                            ;66.10
                                ; LOE ebx esi edi
.B2.45:                         ; Preds .B2.44                  ; Infreq
        mov       DWORD PTR [72+esp], 0                         ;66.10
        lea       edx, DWORD PTR [144+esp]                      ;66.10
        mov       DWORD PTR [144+esp], esi                      ;66.10
        push      32                                            ;66.10
        push      edx                                           ;66.10
        push      OFFSET FLAT: __STRLITPACK_67.0.11             ;66.10
        push      -2088435968                                   ;66.10
        push      911                                           ;66.10
        lea       ecx, DWORD PTR [92+esp]                       ;66.10
        push      ecx                                           ;66.10
        call      _for_write_seq_lis                            ;66.10
                                ; LOE ebx esi edi
.B2.46:                         ; Preds .B2.45                  ; Infreq
        push      32                                            ;66.10
        xor       edx, edx                                      ;66.10
        push      edx                                           ;66.10
        push      edx                                           ;66.10
        push      -2088435968                                   ;66.10
        push      edx                                           ;66.10
        push      OFFSET FLAT: __STRLITPACK_68                  ;66.10
        call      _for_stop_core                                ;66.10
                                ; LOE ebx esi edi
.B2.60:                         ; Preds .B2.46                  ; Infreq
        add       esp, 72                                       ;66.10
        jmp       .B2.7         ; Prob 100%                     ;66.10
                                ; LOE ebx esi edi
.B2.47:                         ; Preds .B2.32                  ; Infreq
        mov       esi, 1                                        ;
        jmp       .B2.36        ; Prob 100%                     ;
                                ; LOE eax edx ebx esi
.B2.48:                         ; Preds .B2.26                  ; Infreq
        mov       esi, 1                                        ;
        jmp       .B2.30        ; Prob 100%                     ;
        ALIGN     16
                                ; LOE eax edx ecx ebx esi
; mark_end;
_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_2DSETUP ENDP
_TEXT	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
__STRLITPACK_34.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_2DSETUP
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_P_SETUP
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_P_SETUP
_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_P_SETUP	PROC NEAR 
; parameter 1: 8 + ebp
; parameter 2: 12 + ebp
.B3.1:                          ; Preds .B3.0
        push      ebp                                           ;88.12
        mov       ebp, esp                                      ;88.12
        and       esp, -16                                      ;88.12
        push      esi                                           ;88.12
        push      edi                                           ;88.12
        push      ebx                                           ;88.12
        sub       esp, 148                                      ;88.12
        mov       eax, DWORD PTR [8+ebp]                        ;88.12
        mov       edx, DWORD PTR [.T254_.0.3]                   ;92.37
        mov       DWORD PTR [64+esp], edx                       ;92.37
        mov       edx, DWORD PTR [.T254_.0.3+8]                 ;92.37
        mov       eax, DWORD PTR [eax]                          ;98.5
        mov       DWORD PTR [72+esp], edx                       ;92.37
        mov       edx, DWORD PTR [.T254_.0.3+28]                ;92.37
        sub       eax, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY+32] ;98.28
        mov       DWORD PTR [92+esp], edx                       ;92.37
        imul      edx, eax, 76                                  ;98.28
        mov       ecx, DWORD PTR [.T254_.0.3+4]                 ;92.37
        mov       DWORD PTR [68+esp], ecx                       ;92.37
        mov       eax, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY] ;98.1
        mov       ecx, DWORD PTR [.T254_.0.3+12]                ;92.37
        mov       DWORD PTR [76+esp], ecx                       ;92.37
        mov       ecx, DWORD PTR [.T254_.0.3+32]                ;92.37
        mov       DWORD PTR [96+esp], ecx                       ;92.37
        mov       ebx, DWORD PTR [.T254_.0.3+16]                ;92.37
        mov       esi, DWORD PTR [.T254_.0.3+20]                ;92.37
        mov       edi, DWORD PTR [.T254_.0.3+24]                ;92.37
        movsx     ecx, WORD PTR [72+eax+edx]                    ;98.5
        test      ecx, ecx                                      ;98.28
        mov       DWORD PTR [80+esp], ebx                       ;92.37
        mov       DWORD PTR [84+esp], esi                       ;92.37
        mov       DWORD PTR [88+esp], edi                       ;92.37
        mov       DWORD PTR [128+esp], ecx                      ;98.5
        jle       .B3.77        ; Prob 16%                      ;98.28
                                ; LOE
.B3.2:                          ; Preds .B3.1
        mov       eax, 0                                        ;100.6
        lea       ecx, DWORD PTR [120+esp]                      ;100.6
        mov       edx, DWORD PTR [128+esp]                      ;100.6
        push      4                                             ;100.6
        cmovl     edx, eax                                      ;100.6
        push      edx                                           ;100.6
        push      2                                             ;100.6
        push      ecx                                           ;100.6
        call      _for_check_mult_overflow                      ;100.6
                                ; LOE eax
.B3.3:                          ; Preds .B3.2
        mov       edx, DWORD PTR [92+esp]                       ;100.6
        and       eax, 1                                        ;100.6
        and       edx, 1                                        ;100.6
        lea       ecx, DWORD PTR [80+esp]                       ;100.6
        add       edx, edx                                      ;100.6
        shl       eax, 4                                        ;100.6
        or        edx, 1                                        ;100.6
        or        edx, eax                                      ;100.6
        or        edx, 262144                                   ;100.6
        push      edx                                           ;100.6
        push      ecx                                           ;100.6
        push      DWORD PTR [144+esp]                           ;100.6
        call      _for_alloc_allocatable                        ;100.6
                                ; LOE eax
.B3.98:                         ; Preds .B3.3
        add       esp, 28                                       ;100.6
        mov       ebx, eax                                      ;100.6
                                ; LOE ebx
.B3.4:                          ; Preds .B3.98
        test      ebx, ebx                                      ;100.6
        je        .B3.7         ; Prob 50%                      ;100.6
                                ; LOE ebx
.B3.5:                          ; Preds .B3.4
        mov       DWORD PTR [32+esp], 0                         ;102.5
        lea       edx, DWORD PTR [32+esp]                       ;102.5
        mov       DWORD PTR [104+esp], 25                       ;102.5
        lea       eax, DWORD PTR [104+esp]                      ;102.5
        mov       DWORD PTR [108+esp], OFFSET FLAT: __STRLITPACK_30 ;102.5
        push      32                                            ;102.5
        push      eax                                           ;102.5
        push      OFFSET FLAT: __STRLITPACK_36.0.3              ;102.5
        push      -2088435968                                   ;102.5
        push      911                                           ;102.5
        push      edx                                           ;102.5
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VECTORERROR], ebx ;100.6
        call      _for_write_seq_lis                            ;102.5
                                ; LOE
.B3.6:                          ; Preds .B3.5
        push      32                                            ;103.5
        xor       eax, eax                                      ;103.5
        push      eax                                           ;103.5
        push      eax                                           ;103.5
        push      -2088435968                                   ;103.5
        push      eax                                           ;103.5
        push      OFFSET FLAT: __STRLITPACK_37                  ;103.5
        call      _for_stop_core                                ;103.5
                                ; LOE
.B3.99:                         ; Preds .B3.6
        add       esp, 48                                       ;103.5
        jmp       .B3.9         ; Prob 100%                     ;103.5
                                ; LOE
.B3.7:                          ; Preds .B3.4
        mov       esi, DWORD PTR [8+ebp]                        ;100.6
        mov       ecx, 1                                        ;100.6
        mov       edi, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY+32] ;100.6
        xor       eax, eax                                      ;100.6
        neg       edi                                           ;100.6
        mov       edx, 4                                        ;100.6
        add       edi, DWORD PTR [esi]                          ;100.6
        imul      esi, edi, 76                                  ;100.6
        mov       DWORD PTR [80+esp], ecx                       ;100.6
        mov       DWORD PTR [96+esp], ecx                       ;100.6
        mov       ecx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY] ;100.6
        mov       DWORD PTR [76+esp], 133                       ;100.6
        mov       DWORD PTR [68+esp], edx                       ;100.6
        movsx     ecx, WORD PTR [72+ecx+esi]                    ;100.6
        test      ecx, ecx                                      ;100.6
        mov       DWORD PTR [72+esp], eax                       ;100.6
        cmovl     ecx, eax                                      ;100.6
        lea       eax, DWORD PTR [28+esp]                       ;100.6
        mov       DWORD PTR [88+esp], ecx                       ;100.6
        mov       DWORD PTR [92+esp], edx                       ;100.6
        push      edx                                           ;100.6
        push      ecx                                           ;100.6
        push      2                                             ;100.6
        push      eax                                           ;100.6
        call      _for_check_mult_overflow                      ;100.6
                                ; LOE ebx
.B3.100:                        ; Preds .B3.7
        add       esp, 16                                       ;100.6
                                ; LOE ebx
.B3.8:                          ; Preds .B3.100
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VECTORERROR], ebx ;100.6
                                ; LOE
.B3.9:                          ; Preds .B3.99 .B3.8
        mov       ebx, DWORD PTR [8+ebp]                        ;107.4
        mov       edx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY] ;107.4
        mov       DWORD PTR [112+esp], edx                      ;107.4
        mov       eax, DWORD PTR [ebx]                          ;107.4
        sub       eax, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY+32] ;107.4
        imul      ebx, eax, 76                                  ;107.4
        movsx     eax, WORD PTR [72+edx+ebx]                    ;107.4
        test      eax, eax                                      ;107.4
        jle       .B3.37        ; Prob 0%                       ;107.4
                                ; LOE eax edx ebx dl dh
.B3.10:                         ; Preds .B3.9
        mov       ecx, DWORD PTR [64+esp]                       ;107.4
        mov       esi, DWORD PTR [96+esp]                       ;107.4
        mov       edi, DWORD PTR [28+edx+ebx]                   ;107.4
        cmp       edi, 4                                        ;107.4
        mov       DWORD PTR [100+esp], edi                      ;107.4
        jne       .B3.31        ; Prob 50%                      ;107.4
                                ; LOE eax ecx ebx esi
.B3.11:                         ; Preds .B3.10
        cmp       eax, 4                                        ;107.4
        jl        .B3.67        ; Prob 10%                      ;107.4
                                ; LOE eax ecx ebx esi
.B3.12:                         ; Preds .B3.11
        lea       edx, DWORD PTR [esi*4]                        ;107.4
        neg       edx                                           ;107.4
        add       edx, ecx                                      ;107.4
        mov       DWORD PTR [12+esp], edx                       ;107.4
        lea       edi, DWORD PTR [4+edx]                        ;107.4
        and       edi, 15                                       ;107.4
        mov       DWORD PTR [16+esp], edi                       ;107.4
        je        .B3.15        ; Prob 50%                      ;107.4
                                ; LOE eax ecx ebx esi edi
.B3.13:                         ; Preds .B3.12
        test      BYTE PTR [16+esp], 3                          ;107.4
        jne       .B3.67        ; Prob 10%                      ;107.4
                                ; LOE eax ecx ebx esi edi
.B3.14:                         ; Preds .B3.13
        mov       edx, edi                                      ;107.4
        neg       edx                                           ;107.4
        add       edx, 16                                       ;107.4
        shr       edx, 2                                        ;107.4
        mov       DWORD PTR [16+esp], edx                       ;107.4
                                ; LOE eax ecx ebx esi
.B3.15:                         ; Preds .B3.14 .B3.12
        mov       edx, DWORD PTR [16+esp]                       ;107.4
        lea       edi, DWORD PTR [4+edx]                        ;107.4
        cmp       eax, edi                                      ;107.4
        jl        .B3.67        ; Prob 10%                      ;107.4
                                ; LOE eax edx ecx ebx esi dl dh
.B3.16:                         ; Preds .B3.15
        mov       DWORD PTR [20+esp], ecx                       ;
        mov       ecx, eax                                      ;107.4
        sub       ecx, edx                                      ;107.4
        mov       edi, DWORD PTR [112+esp]                      ;107.4
        and       ecx, 3                                        ;107.4
        neg       ecx                                           ;107.4
        add       ecx, eax                                      ;107.4
        mov       DWORD PTR [24+esp], ecx                       ;107.4
        mov       ecx, DWORD PTR [32+edi+ebx]                   ;107.4
        shl       ecx, 2                                        ;
        neg       ecx                                           ;
        add       ecx, DWORD PTR [edi+ebx]                      ;
        mov       DWORD PTR [100+esp], ecx                      ;
        test      edx, edx                                      ;107.4
        mov       ecx, DWORD PTR [20+esp]                       ;107.4
        jbe       .B3.20        ; Prob 10%                      ;107.4
                                ; LOE eax ecx ebx esi cl ch
.B3.17:                         ; Preds .B3.16
        mov       DWORD PTR [esp], esi                          ;
        xor       edx, edx                                      ;
        mov       DWORD PTR [8+esp], ebx                        ;
        mov       DWORD PTR [4+esp], eax                        ;
        mov       ebx, DWORD PTR [100+esp]                      ;
        mov       esi, DWORD PTR [16+esp]                       ;
        mov       edi, DWORD PTR [12+esp]                       ;
                                ; LOE edx ecx ebx esi edi
.B3.18:                         ; Preds .B3.18 .B3.17
        mov       eax, DWORD PTR [4+ebx+edx*4]                  ;107.4
        mov       DWORD PTR [4+edi+edx*4], eax                  ;107.4
        inc       edx                                           ;107.4
        cmp       edx, esi                                      ;107.4
        jb        .B3.18        ; Prob 82%                      ;107.4
                                ; LOE edx ecx ebx esi edi
.B3.19:                         ; Preds .B3.18
        mov       DWORD PTR [16+esp], esi                       ;
        mov       esi, DWORD PTR [esp]                          ;
        mov       eax, DWORD PTR [4+esp]                        ;
        mov       ebx, DWORD PTR [8+esp]                        ;
                                ; LOE eax ecx ebx esi
.B3.20:                         ; Preds .B3.16 .B3.19
        mov       edx, DWORD PTR [100+esp]                      ;107.4
        mov       edi, DWORD PTR [16+esp]                       ;107.4
        lea       edx, DWORD PTR [4+edx+edi*4]                  ;107.4
        test      dl, 15                                        ;107.4
        je        .B3.24        ; Prob 60%                      ;107.4
                                ; LOE eax ecx ebx esi edi
.B3.21:                         ; Preds .B3.20
        mov       DWORD PTR [4+esp], eax                        ;
        mov       DWORD PTR [8+esp], ebx                        ;
        mov       eax, DWORD PTR [24+esp]                       ;
        mov       edx, DWORD PTR [100+esp]                      ;
        mov       ebx, edi                                      ;
        mov       edi, DWORD PTR [12+esp]                       ;
                                ; LOE eax edx ecx ebx esi edi
.B3.22:                         ; Preds .B3.22 .B3.21
        movdqu    xmm0, XMMWORD PTR [4+edx+ebx*4]               ;107.4
        movdqa    XMMWORD PTR [4+edi+ebx*4], xmm0               ;107.4
        add       ebx, 4                                        ;107.4
        cmp       ebx, eax                                      ;107.4
        jb        .B3.22        ; Prob 82%                      ;107.4
        jmp       .B3.26        ; Prob 100%                     ;107.4
                                ; LOE eax edx ecx ebx esi edi
.B3.24:                         ; Preds .B3.20
        mov       DWORD PTR [4+esp], eax                        ;
        mov       DWORD PTR [8+esp], ebx                        ;
        mov       eax, DWORD PTR [24+esp]                       ;
        mov       edx, DWORD PTR [100+esp]                      ;
        mov       ebx, edi                                      ;
        mov       edi, DWORD PTR [12+esp]                       ;
                                ; LOE eax edx ecx ebx esi edi
.B3.25:                         ; Preds .B3.25 .B3.24
        movdqa    xmm0, XMMWORD PTR [4+edx+ebx*4]               ;107.4
        movdqa    XMMWORD PTR [4+edi+ebx*4], xmm0               ;107.4
        add       ebx, 4                                        ;107.4
        cmp       ebx, eax                                      ;107.4
        jb        .B3.25        ; Prob 82%                      ;107.4
                                ; LOE eax edx ecx ebx esi edi
.B3.26:                         ; Preds .B3.22 .B3.25
        mov       DWORD PTR [24+esp], eax                       ;
        mov       eax, DWORD PTR [4+esp]                        ;
        mov       ebx, DWORD PTR [8+esp]                        ;
                                ; LOE eax ecx ebx esi
.B3.27:                         ; Preds .B3.26 .B3.67
        cmp       eax, DWORD PTR [24+esp]                       ;107.4
        jbe       .B3.37        ; Prob 10%                      ;107.4
                                ; LOE eax ecx ebx esi
.B3.28:                         ; Preds .B3.27
        mov       edi, DWORD PTR [112+esp]                      ;107.4
        shl       esi, 2                                        ;
        sub       ecx, esi                                      ;
        mov       edx, DWORD PTR [32+edi+ebx]                   ;107.4
        shl       edx, 2                                        ;
        neg       edx                                           ;
        add       edx, DWORD PTR [edi+ebx]                      ;
        mov       edi, DWORD PTR [24+esp]                       ;
                                ; LOE eax edx ecx ebx edi
.B3.29:                         ; Preds .B3.29 .B3.28
        mov       esi, DWORD PTR [4+edx+edi*4]                  ;107.4
        mov       DWORD PTR [4+ecx+edi*4], esi                  ;107.4
        inc       edi                                           ;107.4
        cmp       edi, eax                                      ;107.4
        jb        .B3.29        ; Prob 82%                      ;107.4
        jmp       .B3.37        ; Prob 100%                     ;107.4
                                ; LOE eax edx ecx ebx edi
.B3.31:                         ; Preds .B3.10
        mov       edx, eax                                      ;107.4
        shr       edx, 31                                       ;107.4
        add       edx, eax                                      ;107.4
        sar       edx, 1                                        ;107.4
        mov       DWORD PTR [24+esp], edx                       ;107.4
        test      edx, edx                                      ;107.4
        jbe       .B3.76        ; Prob 10%                      ;107.4
                                ; LOE eax ecx ebx esi
.B3.32:                         ; Preds .B3.31
        mov       DWORD PTR [4+esp], eax                        ;
        mov       eax, DWORD PTR [112+esp]                      ;107.4
        mov       DWORD PTR [16+esp], 0                         ;
        mov       DWORD PTR [20+esp], ecx                       ;
        mov       edx, DWORD PTR [32+eax+ebx]                   ;107.4
        imul      edx, DWORD PTR [100+esp]                      ;
        mov       edi, DWORD PTR [eax+ebx]                      ;107.4
        lea       eax, DWORD PTR [esi*4]                        ;
        neg       eax                                           ;
        sub       edi, edx                                      ;
        add       eax, ecx                                      ;
        mov       DWORD PTR [12+esp], eax                       ;
        mov       DWORD PTR [esp], esi                          ;
        mov       DWORD PTR [8+esp], ebx                        ;
        mov       edx, DWORD PTR [16+esp]                       ;
        mov       esi, DWORD PTR [12+esp]                       ;
        mov       ecx, edi                                      ;
        mov       eax, DWORD PTR [24+esp]                       ;
        mov       ebx, DWORD PTR [100+esp]                      ;
                                ; LOE eax edx ecx ebx esi
.B3.33:                         ; Preds .B3.33 .B3.32
        lea       edi, DWORD PTR [1+edx+edx]                    ;107.4
        imul      edi, ebx                                      ;107.4
        mov       edi, DWORD PTR [ecx+edi]                      ;107.4
        mov       DWORD PTR [4+esi+edx*8], edi                  ;107.4
        lea       edi, DWORD PTR [2+edx+edx]                    ;107.4
        imul      edi, ebx                                      ;107.4
        mov       edi, DWORD PTR [ecx+edi]                      ;107.4
        mov       DWORD PTR [8+esi+edx*8], edi                  ;107.4
        inc       edx                                           ;107.4
        cmp       edx, eax                                      ;107.4
        jb        .B3.33        ; Prob 63%                      ;107.4
                                ; LOE eax edx ecx ebx esi
.B3.34:                         ; Preds .B3.33
        mov       ecx, DWORD PTR [20+esp]                       ;
        lea       edx, DWORD PTR [1+edx+edx]                    ;107.4
        mov       esi, DWORD PTR [esp]                          ;
        mov       eax, DWORD PTR [4+esp]                        ;
        mov       ebx, DWORD PTR [8+esp]                        ;
        mov       DWORD PTR [12+esp], edx                       ;107.4
                                ; LOE eax ecx ebx esi
.B3.35:                         ; Preds .B3.34 .B3.76
        mov       edx, DWORD PTR [12+esp]                       ;107.4
        lea       edi, DWORD PTR [-1+edx]                       ;107.4
        cmp       eax, edi                                      ;107.4
        jbe       .B3.37        ; Prob 10%                      ;107.4
                                ; LOE edx ecx ebx esi dl dh
.B3.36:                         ; Preds .B3.35
        mov       edi, DWORD PTR [112+esp]                      ;107.4
        neg       esi                                           ;107.4
        add       esi, edx                                      ;107.4
        mov       eax, DWORD PTR [32+edi+ebx]                   ;107.4
        neg       eax                                           ;107.4
        add       eax, edx                                      ;107.4
        imul      eax, DWORD PTR [100+esp]                      ;107.4
        mov       edi, DWORD PTR [edi+ebx]                      ;107.4
        mov       eax, DWORD PTR [edi+eax]                      ;107.4
        mov       DWORD PTR [ecx+esi*4], eax                    ;107.4
                                ; LOE ebx
.B3.37:                         ; Preds .B3.29 .B3.27 .B3.35 .B3.9 .B3.36
                                ;      
        mov       eax, DWORD PTR [112+esp]                      ;110.6
        mov       esi, DWORD PTR [12+eax+ebx]                   ;110.6
        test      esi, 1                                        ;110.10
        je        .B3.40        ; Prob 60%                      ;110.10
                                ; LOE ebx esi
.B3.38:                         ; Preds .B3.37
        mov       edx, esi                                      ;111.4
        mov       eax, esi                                      ;111.4
        shr       edx, 1                                        ;111.4
        and       eax, 1                                        ;111.4
        and       edx, 1                                        ;111.4
        add       eax, eax                                      ;111.4
        shl       edx, 2                                        ;111.4
        or        edx, 1                                        ;111.4
        or        edx, eax                                      ;111.4
        or        edx, 262144                                   ;111.4
        push      edx                                           ;111.4
        mov       ecx, DWORD PTR [116+esp]                      ;111.4
        push      DWORD PTR [ecx+ebx]                           ;111.4
        call      _for_dealloc_allocatable                      ;111.4
                                ; LOE eax ebx esi
.B3.101:                        ; Preds .B3.38
        add       esp, 8                                        ;111.4
                                ; LOE eax ebx esi
.B3.39:                         ; Preds .B3.101
        mov       edx, DWORD PTR [112+esp]                      ;111.4
        and       esi, -2                                       ;111.4
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VECTORERROR], eax ;111.4
        test      eax, eax                                      ;112.19
        mov       DWORD PTR [edx+ebx], 0                        ;111.4
        mov       DWORD PTR [12+edx+ebx], esi                   ;111.4
        jne       .B3.70        ; Prob 5%                       ;112.19
                                ; LOE
.B3.40:                         ; Preds .B3.107 .B3.37 .B3.39
        mov       eax, DWORD PTR [12+ebp]                       ;118.4
        xor       edx, edx                                      ;118.4
        lea       ebx, DWORD PTR [100+esp]                      ;118.4
        push      4                                             ;118.4
        mov       ecx, DWORD PTR [eax]                          ;118.4
        test      ecx, ecx                                      ;118.4
        cmovl     ecx, edx                                      ;118.4
        push      ecx                                           ;118.4
        push      2                                             ;118.4
        push      ebx                                           ;118.4
        call      _for_check_mult_overflow                      ;118.4
                                ; LOE eax
.B3.41:                         ; Preds .B3.40
        mov       edx, DWORD PTR [8+ebp]                        ;118.4
        and       eax, 1                                        ;118.4
        mov       ecx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY+32] ;118.4
        neg       ecx                                           ;118.4
        add       ecx, DWORD PTR [edx]                          ;118.4
        imul      ebx, ecx, 76                                  ;118.4
        shl       eax, 4                                        ;118.4
        or        eax, 262145                                   ;118.4
        add       ebx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY] ;118.4
        push      eax                                           ;118.4
        push      ebx                                           ;118.4
        push      DWORD PTR [124+esp]                           ;118.4
        call      _for_allocate                                 ;118.4
                                ; LOE eax
.B3.103:                        ; Preds .B3.41
        add       esp, 28                                       ;118.4
        mov       ebx, eax                                      ;118.4
                                ; LOE ebx
.B3.42:                         ; Preds .B3.103
        test      ebx, ebx                                      ;118.4
        je        .B3.46        ; Prob 50%                      ;118.4
                                ; LOE ebx
.B3.43:                         ; Preds .B3.42
        mov       DWORD PTR [32+esp], 0                         ;120.7
        lea       edx, DWORD PTR [32+esp]                       ;120.7
        mov       DWORD PTR [112+esp], 36                       ;120.7
        lea       eax, DWORD PTR [112+esp]                      ;120.7
        mov       DWORD PTR [116+esp], OFFSET FLAT: __STRLITPACK_26 ;120.7
        push      32                                            ;120.7
        push      eax                                           ;120.7
        push      OFFSET FLAT: __STRLITPACK_40.0.3              ;120.7
        push      -2088435968                                   ;120.7
        push      911                                           ;120.7
        push      edx                                           ;120.7
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VECTORERROR], ebx ;118.4
        call      _for_write_seq_lis                            ;120.7
                                ; LOE
.B3.44:                         ; Preds .B3.43
        push      32                                            ;121.4
        xor       eax, eax                                      ;121.4
        push      eax                                           ;121.4
        push      eax                                           ;121.4
        push      -2088435968                                   ;121.4
        push      eax                                           ;121.4
        push      OFFSET FLAT: __STRLITPACK_41                  ;121.4
        call      _for_stop_core                                ;121.4
                                ; LOE
.B3.104:                        ; Preds .B3.44
        add       esp, 48                                       ;121.4
                                ; LOE
.B3.45:                         ; Preds .B3.104
        mov       ecx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY] ;130.29
        mov       edi, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY+32] ;130.29
        jmp       .B3.48        ; Prob 100%                     ;130.29
                                ; LOE ecx edi
.B3.46:                         ; Preds .B3.42
        mov       esi, DWORD PTR [8+ebp]                        ;118.13
        xor       edi, edi                                      ;118.4
        mov       ecx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY] ;118.4
        mov       eax, DWORD PTR [esi]                          ;118.13
        mov       esi, 4                                        ;118.4
        sub       eax, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY+32] ;118.4
        imul      edx, eax, 76                                  ;118.4
        mov       eax, 1                                        ;118.4
        mov       DWORD PTR [16+ecx+edx], eax                   ;118.4
        mov       DWORD PTR [32+ecx+edx], eax                   ;118.4
        mov       eax, DWORD PTR [12+ebp]                       ;118.4
        mov       DWORD PTR [12+ecx+edx], 5                     ;118.4
        mov       DWORD PTR [4+ecx+edx], esi                    ;118.4
        mov       eax, DWORD PTR [eax]                          ;118.4
        test      eax, eax                                      ;118.4
        mov       DWORD PTR [8+ecx+edx], edi                    ;118.4
        cmovl     eax, edi                                      ;118.4
        mov       DWORD PTR [24+ecx+edx], eax                   ;118.4
        mov       DWORD PTR [28+ecx+edx], esi                   ;118.4
        lea       edx, DWORD PTR [12+esp]                       ;118.4
        push      esi                                           ;118.4
        push      eax                                           ;118.4
        push      2                                             ;118.4
        push      edx                                           ;118.4
        call      _for_check_mult_overflow                      ;118.4
                                ; LOE ebx
.B3.105:                        ; Preds .B3.46
        add       esp, 16                                       ;118.4
                                ; LOE ebx
.B3.47:                         ; Preds .B3.105
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VECTORERROR], ebx ;118.4
        mov       ecx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY] ;130.29
        mov       edi, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY+32] ;130.29
                                ; LOE ecx edi
.B3.48:                         ; Preds .B3.45 .B3.47
        mov       ebx, DWORD PTR [128+esp]                      ;125.4
        mov       esi, ebx                                      ;125.4
        imul      edi, edi, 76                                  ;
        shr       esi, 31                                       ;125.4
        add       esi, ebx                                      ;125.4
        sar       esi, 1                                        ;125.4
        mov       edx, DWORD PTR [64+esp]                       ;125.4
        test      esi, esi                                      ;125.4
        mov       eax, DWORD PTR [96+esp]                       ;125.4
        mov       DWORD PTR [124+esp], edi                      ;
        jbe       .B3.75        ; Prob 0%                       ;125.4
                                ; LOE eax edx ecx esi
.B3.49:                         ; Preds .B3.48
        mov       edi, ecx                                      ;
        xor       ebx, ebx                                      ;
        sub       edi, DWORD PTR [124+esp]                      ;
        mov       DWORD PTR [16+esp], edi                       ;
        lea       edi, DWORD PTR [eax*4]                        ;
        neg       edi                                           ;
        add       edi, edx                                      ;
        mov       DWORD PTR [136+esp], edi                      ;
        mov       DWORD PTR [132+esp], esi                      ;
        mov       DWORD PTR [20+esp], eax                       ;
        mov       DWORD PTR [24+esp], edx                       ;
        mov       DWORD PTR [8+esp], ecx                        ;
        mov       edi, DWORD PTR [16+esp]                       ;
                                ; LOE ebx edi
.B3.50:                         ; Preds .B3.50 .B3.49
        mov       esi, DWORD PTR [8+ebp]                        ;125.4
        lea       ecx, DWORD PTR [1+ebx+ebx]                    ;125.4
        imul      eax, DWORD PTR [esi], 76                      ;125.4
        sub       ecx, DWORD PTR [32+eax+edi]                   ;125.4
        imul      ecx, DWORD PTR [28+eax+edi]                   ;125.4
        mov       edx, DWORD PTR [eax+edi]                      ;125.4
        mov       eax, DWORD PTR [136+esp]                      ;125.4
        mov       eax, DWORD PTR [4+eax+ebx*8]                  ;125.4
        mov       DWORD PTR [edx+ecx], eax                      ;125.4
        lea       eax, DWORD PTR [2+ebx+ebx]                    ;125.4
        imul      edx, DWORD PTR [esi], 76                      ;125.4
        sub       eax, DWORD PTR [32+edx+edi]                   ;125.4
        imul      eax, DWORD PTR [28+edx+edi]                   ;125.4
        mov       ecx, DWORD PTR [136+esp]                      ;125.4
        mov       edx, DWORD PTR [edx+edi]                      ;125.4
        mov       esi, DWORD PTR [8+ecx+ebx*8]                  ;125.4
        inc       ebx                                           ;125.4
        mov       DWORD PTR [edx+eax], esi                      ;125.4
        cmp       ebx, DWORD PTR [132+esp]                      ;125.4
        jb        .B3.50        ; Prob 64%                      ;125.4
                                ; LOE ebx edi
.B3.51:                         ; Preds .B3.50
        mov       eax, DWORD PTR [20+esp]                       ;
        lea       esi, DWORD PTR [1+ebx+ebx]                    ;125.4
        mov       edx, DWORD PTR [24+esp]                       ;
        mov       ecx, DWORD PTR [8+esp]                        ;
                                ; LOE eax edx ecx esi
.B3.52:                         ; Preds .B3.51 .B3.75
        lea       ebx, DWORD PTR [-1+esi]                       ;125.4
        cmp       ebx, DWORD PTR [128+esp]                      ;125.4
        jae       .B3.54        ; Prob 0%                       ;125.4
                                ; LOE eax edx ecx esi
.B3.53:                         ; Preds .B3.52
        mov       edi, DWORD PTR [8+ebp]                        ;125.4
        mov       ebx, ecx                                      ;125.4
        sub       ebx, DWORD PTR [124+esp]                      ;125.4
        neg       eax                                           ;125.4
        add       eax, esi                                      ;125.4
        imul      edi, DWORD PTR [edi], 76                      ;125.4
        sub       esi, DWORD PTR [32+edi+ebx]                   ;125.4
        imul      esi, DWORD PTR [28+edi+ebx]                   ;125.4
        mov       ebx, DWORD PTR [edi+ebx]                      ;125.4
        mov       eax, DWORD PTR [edx+eax*4]                    ;125.4
        mov       DWORD PTR [ebx+esi], eax                      ;125.4
                                ; LOE ecx
.B3.54:                         ; Preds .B3.53 .B3.52
        mov       eax, DWORD PTR [12+ebp]                       ;128.4
        mov       esi, DWORD PTR [eax]                          ;128.4
        mov       eax, esi                                      ;128.4
        sub       eax, DWORD PTR [128+esp]                      ;128.4
        test      eax, eax                                      ;128.4
        jle       .B3.62        ; Prob 50%                      ;128.4
                                ; LOE eax ecx esi
.B3.55:                         ; Preds .B3.54
        mov       ebx, eax                                      ;128.4
        shr       ebx, 31                                       ;128.4
        add       ebx, eax                                      ;128.4
        sar       ebx, 1                                        ;128.4
        test      ebx, ebx                                      ;128.4
        jbe       .B3.72        ; Prob 10%                      ;128.4
                                ; LOE ecx ebx esi
.B3.56:                         ; Preds .B3.55
        mov       eax, ecx                                      ;
        xor       edx, edx                                      ;
        mov       DWORD PTR [16+esp], esi                       ;
        sub       eax, DWORD PTR [124+esp]                      ;
        mov       DWORD PTR [20+esp], ebx                       ;
        mov       DWORD PTR [8+esp], ecx                        ;
        mov       esi, DWORD PTR [8+ebp]                        ;
                                ; LOE eax edx esi
.B3.57:                         ; Preds .B3.57 .B3.56
        mov       edi, DWORD PTR [128+esp]                      ;128.4
        mov       DWORD PTR [24+esp], edx                       ;
        lea       ecx, DWORD PTR [edi+edx*2]                    ;128.4
        imul      edx, DWORD PTR [esi], 76                      ;128.4
        mov       edi, DWORD PTR [edx+eax]                      ;128.4
        lea       ebx, DWORD PTR [1+ecx]                        ;128.4
        sub       ebx, DWORD PTR [32+edx+eax]                   ;128.4
        add       ecx, 2                                        ;128.4
        imul      ebx, DWORD PTR [28+edx+eax]                   ;128.4
        xor       edx, edx                                      ;128.4
        mov       DWORD PTR [edi+ebx], edx                      ;128.4
        imul      ebx, DWORD PTR [esi], 76                      ;128.4
        sub       ecx, DWORD PTR [32+ebx+eax]                   ;128.4
        imul      ecx, DWORD PTR [28+ebx+eax]                   ;128.4
        mov       ebx, DWORD PTR [ebx+eax]                      ;128.4
        mov       DWORD PTR [ebx+ecx], edx                      ;128.4
        mov       edx, DWORD PTR [24+esp]                       ;128.4
        inc       edx                                           ;128.4
        cmp       edx, DWORD PTR [20+esp]                       ;128.4
        jb        .B3.57        ; Prob 64%                      ;128.4
                                ; LOE eax edx esi
.B3.58:                         ; Preds .B3.57
        mov       esi, DWORD PTR [16+esp]                       ;
        lea       eax, DWORD PTR [1+edx+edx]                    ;128.4
        mov       ecx, DWORD PTR [8+esp]                        ;
                                ; LOE eax ecx esi
.B3.59:                         ; Preds .B3.58 .B3.72
        sub       esi, DWORD PTR [128+esp]                      ;128.4
        lea       edx, DWORD PTR [-1+eax]                       ;128.4
        cmp       esi, edx                                      ;128.4
        jbe       .B3.61        ; Prob 10%                      ;128.4
                                ; LOE eax ecx
.B3.60:                         ; Preds .B3.59
        mov       edx, DWORD PTR [8+ebp]                        ;128.4
        mov       esi, ecx                                      ;128.4
        sub       esi, DWORD PTR [124+esp]                      ;128.4
        add       eax, DWORD PTR [128+esp]                      ;128.4
        imul      ebx, DWORD PTR [edx], 76                      ;128.4
        sub       eax, DWORD PTR [32+ebx+esi]                   ;128.4
        imul      eax, DWORD PTR [28+ebx+esi]                   ;128.4
        mov       edi, DWORD PTR [ebx+esi]                      ;128.4
        mov       DWORD PTR [edi+eax], 0                        ;128.4
                                ; LOE ecx
.B3.61:                         ; Preds .B3.59 .B3.60
        mov       eax, DWORD PTR [12+ebp]                       ;130.29
        mov       esi, DWORD PTR [eax]                          ;130.29
                                ; LOE ecx esi
.B3.62:                         ; Preds .B3.61 .B3.54
        mov       eax, DWORD PTR [8+ebp]                        ;130.4
        mov       ebx, DWORD PTR [76+esp]                       ;131.4
        imul      edx, DWORD PTR [eax], 76                      ;130.4
        add       ecx, edx                                      ;130.4
        sub       ecx, DWORD PTR [124+esp]                      ;130.4
        test      bl, 1                                         ;131.7
        mov       WORD PTR [72+ecx], si                         ;130.4
        je        .B3.65        ; Prob 60%                      ;131.7
                                ; LOE ebx
.B3.63:                         ; Preds .B3.62
        mov       edx, ebx                                      ;131.25
        mov       eax, ebx                                      ;131.25
        shr       edx, 1                                        ;131.25
        and       eax, 1                                        ;131.25
        and       edx, 1                                        ;131.25
        add       eax, eax                                      ;131.25
        shl       edx, 2                                        ;131.25
        or        edx, eax                                      ;131.25
        or        edx, 262144                                   ;131.25
        push      edx                                           ;131.25
        push      DWORD PTR [68+esp]                            ;131.25
        call      _for_dealloc_allocatable                      ;131.25
                                ; LOE ebx
.B3.106:                        ; Preds .B3.63
        add       esp, 8                                        ;131.25
                                ; LOE ebx
.B3.64:                         ; Preds .B3.106
        and       ebx, -2                                       ;131.25
        mov       DWORD PTR [64+esp], 0                         ;131.25
        mov       DWORD PTR [76+esp], ebx                       ;131.25
                                ; LOE ebx
.B3.65:                         ; Preds .B3.64 .B3.62 .B3.92
        test      bl, 1                                         ;146.1
        jne       .B3.73        ; Prob 3%                       ;146.1
                                ; LOE ebx
.B3.66:                         ; Preds .B3.65
        add       esp, 148                                      ;146.1
        pop       ebx                                           ;146.1
        pop       edi                                           ;146.1
        pop       esi                                           ;146.1
        mov       esp, ebp                                      ;146.1
        pop       ebp                                           ;146.1
        ret                                                     ;146.1
                                ; LOE
.B3.67:                         ; Preds .B3.11 .B3.15 .B3.13    ; Infreq
        xor       edx, edx                                      ;107.4
        mov       DWORD PTR [24+esp], edx                       ;107.4
        jmp       .B3.27        ; Prob 100%                     ;107.4
                                ; LOE eax ecx ebx esi
.B3.70:                         ; Preds .B3.39                  ; Infreq
        mov       DWORD PTR [32+esp], 0                         ;113.6
        lea       edx, DWORD PTR [32+esp]                       ;113.6
        mov       DWORD PTR [esp], 38                           ;113.6
        lea       eax, DWORD PTR [esp]                          ;113.6
        mov       DWORD PTR [4+esp], OFFSET FLAT: __STRLITPACK_28 ;113.6
        push      32                                            ;113.6
        push      eax                                           ;113.6
        push      OFFSET FLAT: __STRLITPACK_38.0.3              ;113.6
        push      -2088435968                                   ;113.6
        push      911                                           ;113.6
        push      edx                                           ;113.6
        call      _for_write_seq_lis                            ;113.6
                                ; LOE
.B3.71:                         ; Preds .B3.70                  ; Infreq
        push      32                                            ;114.6
        xor       eax, eax                                      ;114.6
        push      eax                                           ;114.6
        push      eax                                           ;114.6
        push      -2088435968                                   ;114.6
        push      eax                                           ;114.6
        push      OFFSET FLAT: __STRLITPACK_39                  ;114.6
        call      _for_stop_core                                ;114.6
                                ; LOE
.B3.107:                        ; Preds .B3.71                  ; Infreq
        add       esp, 48                                       ;114.6
        jmp       .B3.40        ; Prob 100%                     ;114.6
                                ; LOE
.B3.72:                         ; Preds .B3.55                  ; Infreq
        mov       eax, 1                                        ;
        jmp       .B3.59        ; Prob 100%                     ;
                                ; LOE eax ecx esi
.B3.73:                         ; Preds .B3.65                  ; Infreq
        mov       edx, ebx                                      ;146.1
        mov       eax, ebx                                      ;146.1
        shr       edx, 1                                        ;146.1
        and       eax, 1                                        ;146.1
        and       edx, 1                                        ;146.1
        add       eax, eax                                      ;146.1
        shl       edx, 2                                        ;146.1
        or        edx, eax                                      ;146.1
        or        edx, 262144                                   ;146.1
        push      edx                                           ;146.1
        push      DWORD PTR [68+esp]                            ;146.1
        call      _for_dealloc_allocatable                      ;146.1
                                ; LOE ebx
.B3.108:                        ; Preds .B3.73                  ; Infreq
        add       esp, 8                                        ;146.1
                                ; LOE ebx
.B3.74:                         ; Preds .B3.108                 ; Infreq
        and       ebx, -2                                       ;146.1
        mov       DWORD PTR [64+esp], 0                         ;146.1
        mov       DWORD PTR [76+esp], ebx                       ;146.1
        add       esp, 148                                      ;146.1
        pop       ebx                                           ;146.1
        pop       edi                                           ;146.1
        pop       esi                                           ;146.1
        mov       esp, ebp                                      ;146.1
        pop       ebp                                           ;146.1
        ret                                                     ;146.1
                                ; LOE
.B3.75:                         ; Preds .B3.48                  ; Infreq
        mov       esi, 1                                        ;
        jmp       .B3.52        ; Prob 100%                     ;
                                ; LOE eax edx ecx esi
.B3.76:                         ; Preds .B3.31                  ; Infreq
        mov       DWORD PTR [12+esp], 1                         ;
        jmp       .B3.35        ; Prob 100%                     ;
                                ; LOE eax ecx ebx esi
.B3.77:                         ; Preds .B3.1                   ; Infreq
        mov       eax, DWORD PTR [12+ebp]                       ;136.4
        xor       edx, edx                                      ;136.4
        lea       ebx, DWORD PTR [24+esp]                       ;136.4
        push      4                                             ;136.4
        mov       ecx, DWORD PTR [eax]                          ;136.4
        test      ecx, ecx                                      ;136.4
        cmovl     ecx, edx                                      ;136.4
        push      ecx                                           ;136.4
        push      2                                             ;136.4
        push      ebx                                           ;136.4
        call      _for_check_mult_overflow                      ;136.4
                                ; LOE eax
.B3.78:                         ; Preds .B3.77                  ; Infreq
        mov       edx, DWORD PTR [8+ebp]                        ;136.4
        and       eax, 1                                        ;136.4
        mov       ecx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY+32] ;136.4
        neg       ecx                                           ;136.4
        add       ecx, DWORD PTR [edx]                          ;136.4
        imul      ebx, ecx, 76                                  ;136.4
        shl       eax, 4                                        ;136.4
        or        eax, 262145                                   ;136.4
        add       ebx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY] ;136.4
        push      eax                                           ;136.4
        push      ebx                                           ;136.4
        push      DWORD PTR [48+esp]                            ;136.4
        call      _for_allocate                                 ;136.4
                                ; LOE eax
.B3.110:                        ; Preds .B3.78                  ; Infreq
        add       esp, 28                                       ;136.4
        mov       ebx, eax                                      ;136.4
                                ; LOE ebx
.B3.79:                         ; Preds .B3.110                 ; Infreq
        test      ebx, ebx                                      ;136.4
        je        .B3.82        ; Prob 50%                      ;136.4
                                ; LOE ebx
.B3.80:                         ; Preds .B3.79                  ; Infreq
        mov       DWORD PTR [32+esp], 0                         ;138.7
        lea       edx, DWORD PTR [32+esp]                       ;138.7
        mov       DWORD PTR [16+esp], 36                        ;138.7
        lea       eax, DWORD PTR [16+esp]                       ;138.7
        mov       DWORD PTR [20+esp], OFFSET FLAT: __STRLITPACK_24 ;138.7
        push      32                                            ;138.7
        push      eax                                           ;138.7
        push      OFFSET FLAT: __STRLITPACK_42.0.3              ;138.7
        push      -2088435968                                   ;138.7
        push      911                                           ;138.7
        push      edx                                           ;138.7
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VECTORERROR], ebx ;136.4
        call      _for_write_seq_lis                            ;138.7
                                ; LOE
.B3.81:                         ; Preds .B3.80                  ; Infreq
        push      32                                            ;139.4
        xor       eax, eax                                      ;139.4
        push      eax                                           ;139.4
        push      eax                                           ;139.4
        push      -2088435968                                   ;139.4
        push      eax                                           ;139.4
        push      OFFSET FLAT: __STRLITPACK_43                  ;139.4
        call      _for_stop_core                                ;139.4
                                ; LOE
.B3.111:                        ; Preds .B3.81                  ; Infreq
        add       esp, 48                                       ;139.4
        jmp       .B3.84        ; Prob 100%                     ;139.4
                                ; LOE
.B3.82:                         ; Preds .B3.79                  ; Infreq
        mov       esi, DWORD PTR [8+ebp]                        ;136.13
        xor       edi, edi                                      ;136.4
        mov       ecx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY] ;136.4
        mov       eax, DWORD PTR [esi]                          ;136.13
        mov       esi, 4                                        ;136.4
        sub       eax, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY+32] ;136.4
        imul      edx, eax, 76                                  ;136.4
        mov       eax, 1                                        ;136.4
        mov       DWORD PTR [16+ecx+edx], eax                   ;136.4
        mov       DWORD PTR [32+ecx+edx], eax                   ;136.4
        mov       eax, DWORD PTR [12+ebp]                       ;136.4
        mov       DWORD PTR [12+ecx+edx], 5                     ;136.4
        mov       DWORD PTR [4+ecx+edx], esi                    ;136.4
        mov       eax, DWORD PTR [eax]                          ;136.4
        test      eax, eax                                      ;136.4
        mov       DWORD PTR [8+ecx+edx], edi                    ;136.4
        cmovl     eax, edi                                      ;136.4
        mov       DWORD PTR [24+ecx+edx], eax                   ;136.4
        mov       DWORD PTR [28+ecx+edx], esi                   ;136.4
        lea       edx, DWORD PTR [8+esp]                        ;136.4
        push      esi                                           ;136.4
        push      eax                                           ;136.4
        push      2                                             ;136.4
        push      edx                                           ;136.4
        call      _for_check_mult_overflow                      ;136.4
                                ; LOE ebx
.B3.112:                        ; Preds .B3.82                  ; Infreq
        add       esp, 16                                       ;136.4
                                ; LOE ebx
.B3.83:                         ; Preds .B3.112                 ; Infreq
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VECTORERROR], ebx ;136.4
                                ; LOE
.B3.84:                         ; Preds .B3.111 .B3.83          ; Infreq
        mov       eax, DWORD PTR [12+ebp]                       ;143.7
        mov       ebx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY] ;144.29
        mov       ecx, DWORD PTR [eax]                          ;143.7
        test      ecx, ecx                                      ;143.7
        mov       eax, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY+32] ;144.29
        jle       .B3.94        ; Prob 10%                      ;143.7
                                ; LOE eax ecx ebx
.B3.85:                         ; Preds .B3.84                  ; Infreq
        mov       edi, ecx                                      ;143.7
        shr       edi, 31                                       ;143.7
        add       edi, ecx                                      ;143.7
        sar       edi, 1                                        ;143.7
        imul      esi, eax, 76                                  ;
        test      edi, edi                                      ;143.7
        jbe       .B3.93        ; Prob 0%                       ;143.7
                                ; LOE ecx ebx esi edi
.B3.86:                         ; Preds .B3.85                  ; Infreq
        xor       edx, edx                                      ;
        mov       eax, ebx                                      ;
        mov       DWORD PTR [4+esp], esi                        ;
        sub       eax, esi                                      ;
        mov       DWORD PTR [esp], ecx                          ;
        mov       DWORD PTR [28+esp], edi                       ;
        xor       edi, edi                                      ;
        mov       DWORD PTR [12+esp], ebx                       ;
        mov       esi, DWORD PTR [8+ebp]                        ;
                                ; LOE eax edx esi edi
.B3.87:                         ; Preds .B3.87 .B3.86           ; Infreq
        imul      ecx, DWORD PTR [esi], 76                      ;143.7
        lea       ebx, DWORD PTR [1+edx+edx]                    ;143.7
        sub       ebx, DWORD PTR [32+ecx+eax]                   ;143.7
        imul      ebx, DWORD PTR [28+ecx+eax]                   ;143.7
        mov       ecx, DWORD PTR [ecx+eax]                      ;143.7
        mov       DWORD PTR [ecx+ebx], edi                      ;143.7
        lea       ecx, DWORD PTR [2+edx+edx]                    ;143.7
        imul      ebx, DWORD PTR [esi], 76                      ;143.7
        inc       edx                                           ;143.7
        sub       ecx, DWORD PTR [32+ebx+eax]                   ;143.7
        imul      ecx, DWORD PTR [28+ebx+eax]                   ;143.7
        mov       ebx, DWORD PTR [ebx+eax]                      ;143.7
        cmp       edx, DWORD PTR [28+esp]                       ;143.7
        mov       DWORD PTR [ebx+ecx], edi                      ;143.7
        jb        .B3.87        ; Prob 64%                      ;143.7
                                ; LOE eax edx esi edi
.B3.88:                         ; Preds .B3.87                  ; Infreq
        mov       ecx, DWORD PTR [esp]                          ;
        lea       eax, DWORD PTR [1+edx+edx]                    ;143.7
        mov       esi, DWORD PTR [4+esp]                        ;
        mov       ebx, DWORD PTR [12+esp]                       ;
                                ; LOE eax ecx ebx esi
.B3.89:                         ; Preds .B3.88 .B3.93           ; Infreq
        lea       edx, DWORD PTR [-1+eax]                       ;143.7
        cmp       ecx, edx                                      ;143.7
        jbe       .B3.91        ; Prob 0%                       ;143.7
                                ; LOE eax ebx esi
.B3.90:                         ; Preds .B3.89                  ; Infreq
        mov       edx, DWORD PTR [8+ebp]                        ;143.7
        mov       edi, ebx                                      ;143.7
        sub       edi, esi                                      ;143.7
        imul      ecx, DWORD PTR [edx], 76                      ;143.7
        sub       eax, DWORD PTR [32+ecx+edi]                   ;143.7
        imul      eax, DWORD PTR [28+ecx+edi]                   ;143.7
        mov       edx, DWORD PTR [ecx+edi]                      ;143.7
        mov       DWORD PTR [edx+eax], 0                        ;143.7
                                ; LOE ebx esi
.B3.91:                         ; Preds .B3.89 .B3.90           ; Infreq
        mov       eax, DWORD PTR [12+ebp]                       ;144.29
        mov       ecx, DWORD PTR [eax]                          ;144.29
                                ; LOE ecx ebx esi
.B3.92:                         ; Preds .B3.94 .B3.91           ; Infreq
        mov       eax, DWORD PTR [8+ebp]                        ;144.4
        imul      edx, DWORD PTR [eax], 76                      ;144.4
        add       ebx, edx                                      ;144.4
        sub       ebx, esi                                      ;144.4
        mov       WORD PTR [72+ebx], cx                         ;144.4
        mov       ebx, DWORD PTR [76+esp]                       ;146.1
        jmp       .B3.65        ; Prob 100%                     ;146.1
                                ; LOE ebx
.B3.93:                         ; Preds .B3.85                  ; Infreq
        mov       eax, 1                                        ;
        jmp       .B3.89        ; Prob 100%                     ;
                                ; LOE eax ecx ebx esi
.B3.94:                         ; Preds .B3.84                  ; Infreq
        imul      esi, eax, 76                                  ;
        jmp       .B3.92        ; Prob 100%                     ;
        ALIGN     16
                                ; LOE ecx ebx esi
; mark_end;
_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_P_SETUP ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_P_SETUP
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_A_SETUP
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_A_SETUP
_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_A_SETUP	PROC NEAR 
; parameter 1: 8 + ebp
; parameter 2: 12 + ebp
.B4.1:                          ; Preds .B4.0
        push      ebp                                           ;149.12
        mov       ebp, esp                                      ;149.12
        and       esp, -16                                      ;149.12
        push      esi                                           ;149.12
        push      edi                                           ;149.12
        push      ebx                                           ;149.12
        sub       esp, 148                                      ;149.12
        mov       eax, DWORD PTR [8+ebp]                        ;149.12
        mov       edx, DWORD PTR [.T328_.0.4]                   ;152.34
        mov       DWORD PTR [64+esp], edx                       ;152.34
        mov       edx, DWORD PTR [.T328_.0.4+8]                 ;152.34
        mov       eax, DWORD PTR [eax]                          ;158.5
        mov       DWORD PTR [72+esp], edx                       ;152.34
        mov       edx, DWORD PTR [.T328_.0.4+28]                ;152.34
        sub       eax, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY+32] ;158.29
        mov       DWORD PTR [92+esp], edx                       ;152.34
        imul      edx, eax, 76                                  ;158.29
        mov       ecx, DWORD PTR [.T328_.0.4+4]                 ;152.34
        mov       DWORD PTR [68+esp], ecx                       ;152.34
        mov       eax, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY] ;158.1
        mov       ecx, DWORD PTR [.T328_.0.4+12]                ;152.34
        mov       DWORD PTR [76+esp], ecx                       ;152.34
        mov       ecx, DWORD PTR [.T328_.0.4+32]                ;152.34
        mov       DWORD PTR [96+esp], ecx                       ;152.34
        mov       ebx, DWORD PTR [.T328_.0.4+16]                ;152.34
        mov       esi, DWORD PTR [.T328_.0.4+20]                ;152.34
        mov       edi, DWORD PTR [.T328_.0.4+24]                ;152.34
        movsx     ecx, WORD PTR [74+eax+edx]                    ;158.5
        test      ecx, ecx                                      ;158.29
        mov       DWORD PTR [80+esp], ebx                       ;152.34
        mov       DWORD PTR [84+esp], esi                       ;152.34
        mov       DWORD PTR [88+esp], edi                       ;152.34
        mov       DWORD PTR [124+esp], ecx                      ;158.5
        jle       .B4.120       ; Prob 16%                      ;158.29
                                ; LOE
.B4.2:                          ; Preds .B4.1
        mov       eax, DWORD PTR [12+ebp]                       ;160.6
        xor       ecx, ecx                                      ;160.6
        mov       edx, DWORD PTR [124+esp]                      ;160.6
        lea       esi, DWORD PTR [120+esp]                      ;160.6
        push      12                                            ;160.6
        mov       ebx, DWORD PTR [eax]                          ;160.6
        cmp       ebx, edx                                      ;160.6
        cmovl     ebx, edx                                      ;160.6
        test      ebx, ebx                                      ;160.6
        cmovl     ebx, ecx                                      ;160.6
        push      ebx                                           ;160.6
        push      2                                             ;160.6
        push      esi                                           ;160.6
        call      _for_check_mult_overflow                      ;160.6
                                ; LOE eax
.B4.3:                          ; Preds .B4.2
        mov       edx, DWORD PTR [92+esp]                       ;160.6
        and       eax, 1                                        ;160.6
        and       edx, 1                                        ;160.6
        lea       ecx, DWORD PTR [80+esp]                       ;160.6
        add       edx, edx                                      ;160.6
        shl       eax, 4                                        ;160.6
        or        edx, 1                                        ;160.6
        or        edx, eax                                      ;160.6
        or        edx, 262144                                   ;160.6
        push      edx                                           ;160.6
        push      ecx                                           ;160.6
        push      DWORD PTR [144+esp]                           ;160.6
        call      _for_alloc_allocatable                        ;160.6
                                ; LOE eax
.B4.156:                        ; Preds .B4.3
        add       esp, 28                                       ;160.6
        mov       ebx, eax                                      ;160.6
                                ; LOE ebx
.B4.4:                          ; Preds .B4.156
        test      ebx, ebx                                      ;160.6
        je        .B4.7         ; Prob 50%                      ;160.6
                                ; LOE ebx
.B4.5:                          ; Preds .B4.4
        mov       DWORD PTR [32+esp], 0                         ;162.5
        lea       edx, DWORD PTR [32+esp]                       ;162.5
        mov       DWORD PTR [104+esp], 25                       ;162.5
        lea       eax, DWORD PTR [104+esp]                      ;162.5
        mov       DWORD PTR [108+esp], OFFSET FLAT: __STRLITPACK_22 ;162.5
        push      32                                            ;162.5
        push      eax                                           ;162.5
        push      OFFSET FLAT: __STRLITPACK_44.0.4              ;162.5
        push      -2088435968                                   ;162.5
        push      911                                           ;162.5
        push      edx                                           ;162.5
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VECTORERROR], ebx ;160.6
        call      _for_write_seq_lis                            ;162.5
                                ; LOE
.B4.6:                          ; Preds .B4.5
        push      32                                            ;163.5
        xor       eax, eax                                      ;163.5
        push      eax                                           ;163.5
        push      eax                                           ;163.5
        push      -2088435968                                   ;163.5
        push      eax                                           ;163.5
        push      OFFSET FLAT: __STRLITPACK_45                  ;163.5
        call      _for_stop_core                                ;163.5
                                ; LOE
.B4.157:                        ; Preds .B4.6
        add       esp, 48                                       ;163.5
        jmp       .B4.9         ; Prob 100%                     ;163.5
                                ; LOE
.B4.7:                          ; Preds .B4.4
        mov       esi, DWORD PTR [12+ebp]                       ;160.6
        mov       ecx, 1                                        ;160.6
        mov       DWORD PTR [80+esp], ecx                       ;160.6
        xor       eax, eax                                      ;160.6
        mov       DWORD PTR [96+esp], ecx                       ;160.6
        mov       edx, 12                                       ;160.6
        mov       ecx, DWORD PTR [esi]                          ;160.6
        mov       edi, DWORD PTR [124+esp]                      ;160.6
        cmp       ecx, edi                                      ;160.6
        mov       DWORD PTR [76+esp], 133                       ;160.6
        cmovl     ecx, edi                                      ;160.6
        test      ecx, ecx                                      ;160.6
        mov       DWORD PTR [68+esp], edx                       ;160.6
        cmovl     ecx, eax                                      ;160.6
        mov       DWORD PTR [72+esp], eax                       ;160.6
        lea       eax, DWORD PTR [28+esp]                       ;160.6
        mov       DWORD PTR [88+esp], ecx                       ;160.6
        mov       DWORD PTR [92+esp], edx                       ;160.6
        push      edx                                           ;160.6
        push      ecx                                           ;160.6
        push      2                                             ;160.6
        push      eax                                           ;160.6
        call      _for_check_mult_overflow                      ;160.6
                                ; LOE ebx
.B4.158:                        ; Preds .B4.7
        add       esp, 16                                       ;160.6
                                ; LOE ebx
.B4.8:                          ; Preds .B4.158
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VECTORERROR], ebx ;160.6
                                ; LOE
.B4.9:                          ; Preds .B4.157 .B4.8
        mov       eax, DWORD PTR [8+ebp]                        ;168.5
        mov       ebx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY] ;168.5
        mov       edx, DWORD PTR [eax]                          ;168.5
        sub       edx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY+32] ;168.5
        imul      esi, edx, 76                                  ;168.5
        movsx     eax, WORD PTR [74+ebx+esi]                    ;168.5
        test      eax, eax                                      ;168.5
        jle       .B4.49        ; Prob 0%                       ;168.5
                                ; LOE eax ebx esi
.B4.10:                         ; Preds .B4.9
        mov       edx, DWORD PTR [64+esp]                       ;168.5
        mov       ecx, DWORD PTR [96+esp]                       ;168.5
        mov       edi, DWORD PTR [64+ebx+esi]                   ;168.5
        cmp       edi, 4                                        ;168.5
        mov       DWORD PTR [100+esp], edx                      ;168.5
        mov       DWORD PTR [24+esp], ecx                       ;168.5
        mov       DWORD PTR [112+esp], edi                      ;168.5
        jne       .B4.17        ; Prob 50%                      ;168.5
                                ; LOE eax ebx esi
.B4.11:                         ; Preds .B4.10
        mov       edi, eax                                      ;168.5
        shr       edi, 31                                       ;168.5
        add       edi, eax                                      ;168.5
        sar       edi, 1                                        ;168.5
        test      edi, edi                                      ;168.5
        jbe       .B4.104       ; Prob 10%                      ;168.5
                                ; LOE eax ebx esi edi
.B4.12:                         ; Preds .B4.11
        mov       ecx, DWORD PTR [68+ebx+esi]                   ;168.5
        xor       edx, edx                                      ;
        shl       ecx, 2                                        ;
        neg       ecx                                           ;
        add       ecx, DWORD PTR [36+ebx+esi]                   ;
        mov       DWORD PTR [20+esp], ecx                       ;
        mov       ecx, DWORD PTR [24+esp]                       ;
        mov       DWORD PTR [16+esp], edx                       ;
        mov       DWORD PTR [esp], eax                          ;
        mov       DWORD PTR [4+esp], ebx                        ;
        lea       edx, DWORD PTR [ecx*8]                        ;
        mov       DWORD PTR [8+esp], esi                        ;
        lea       edx, DWORD PTR [edx+ecx*4]                    ;
        neg       edx                                           ;
        add       edx, DWORD PTR [100+esp]                      ;
        mov       DWORD PTR [12+esp], edx                       ;
        mov       edx, DWORD PTR [20+esp]                       ;
        mov       ecx, DWORD PTR [16+esp]                       ;
        mov       eax, DWORD PTR [12+esp]                       ;
                                ; LOE eax edx ecx edi
.B4.13:                         ; Preds .B4.13 .B4.12
        mov       esi, DWORD PTR [4+edx+ecx*8]                  ;168.5
        lea       ebx, DWORD PTR [ecx+ecx*2]                    ;168.5
        mov       DWORD PTR [12+eax+ebx*8], esi                 ;168.5
        mov       esi, DWORD PTR [8+edx+ecx*8]                  ;168.5
        inc       ecx                                           ;168.5
        mov       DWORD PTR [24+eax+ebx*8], esi                 ;168.5
        cmp       ecx, edi                                      ;168.5
        jb        .B4.13        ; Prob 63%                      ;168.5
                                ; LOE eax edx ecx edi
.B4.14:                         ; Preds .B4.13
        mov       eax, DWORD PTR [esp]                          ;
        lea       edx, DWORD PTR [1+ecx+ecx]                    ;168.5
        mov       ebx, DWORD PTR [4+esp]                        ;
        mov       esi, DWORD PTR [8+esp]                        ;
                                ; LOE eax edx ebx esi
.B4.15:                         ; Preds .B4.14 .B4.104
        lea       ecx, DWORD PTR [-1+edx]                       ;168.5
        cmp       eax, ecx                                      ;168.5
        jbe       .B4.23        ; Prob 10%                      ;168.5
                                ; LOE eax edx ebx esi
.B4.16:                         ; Preds .B4.15
        mov       DWORD PTR [esp], eax                          ;
        mov       eax, DWORD PTR [68+ebx+esi]                   ;168.5
        neg       eax                                           ;168.5
        mov       edi, DWORD PTR [24+esp]                       ;168.5
        add       eax, edx                                      ;168.5
        neg       edi                                           ;168.5
        mov       ecx, DWORD PTR [36+ebx+esi]                   ;168.5
        add       edi, edx                                      ;168.5
        mov       eax, DWORD PTR [ecx+eax*4]                    ;168.5
        mov       ecx, DWORD PTR [100+esp]                      ;168.5
        lea       edx, DWORD PTR [edi*8]                        ;168.5
        lea       edx, DWORD PTR [edx+edi*4]                    ;168.5
        mov       DWORD PTR [ecx+edx], eax                      ;168.5
        mov       eax, DWORD PTR [esp]                          ;168.5
        jmp       .B4.23        ; Prob 100%                     ;168.5
                                ; LOE eax ebx esi
.B4.17:                         ; Preds .B4.10
        mov       edx, eax                                      ;168.5
        shr       edx, 31                                       ;168.5
        add       edx, eax                                      ;168.5
        sar       edx, 1                                        ;168.5
        mov       DWORD PTR [16+esp], edx                       ;168.5
        test      edx, edx                                      ;168.5
        jbe       .B4.119       ; Prob 10%                      ;168.5
                                ; LOE eax ebx esi
.B4.18:                         ; Preds .B4.17
        mov       edx, DWORD PTR [68+ebx+esi]                   ;168.5
        xor       ecx, ecx                                      ;
        imul      edx, DWORD PTR [112+esp]                      ;
        mov       edi, DWORD PTR [36+ebx+esi]                   ;168.5
        sub       edi, edx                                      ;
        mov       DWORD PTR [12+esp], edi                       ;
        mov       edi, DWORD PTR [24+esp]                       ;
        mov       DWORD PTR [esp], eax                          ;
        mov       DWORD PTR [4+esp], ebx                        ;
        mov       DWORD PTR [8+esp], esi                        ;
        lea       edx, DWORD PTR [edi*8]                        ;
        mov       eax, DWORD PTR [12+esp]                       ;
        lea       edx, DWORD PTR [edx+edi*4]                    ;
        neg       edx                                           ;
        add       edx, DWORD PTR [100+esp]                      ;
                                ; LOE eax edx ecx
.B4.19:                         ; Preds .B4.19 .B4.18
        mov       esi, DWORD PTR [112+esp]                      ;168.5
        lea       edi, DWORD PTR [1+ecx+ecx]                    ;168.5
        imul      edi, esi                                      ;168.5
        lea       ebx, DWORD PTR [ecx+ecx*2]                    ;168.5
        mov       edi, DWORD PTR [eax+edi]                      ;168.5
        mov       DWORD PTR [12+edx+ebx*8], edi                 ;168.5
        lea       edi, DWORD PTR [2+ecx+ecx]                    ;168.5
        imul      edi, esi                                      ;168.5
        inc       ecx                                           ;168.5
        mov       esi, DWORD PTR [eax+edi]                      ;168.5
        mov       DWORD PTR [24+edx+ebx*8], esi                 ;168.5
        cmp       ecx, DWORD PTR [16+esp]                       ;168.5
        jb        .B4.19        ; Prob 63%                      ;168.5
                                ; LOE eax edx ecx
.B4.20:                         ; Preds .B4.19
        mov       eax, DWORD PTR [esp]                          ;
        lea       edx, DWORD PTR [1+ecx+ecx]                    ;168.5
        mov       ebx, DWORD PTR [4+esp]                        ;
        mov       esi, DWORD PTR [8+esp]                        ;
                                ; LOE eax edx ebx esi
.B4.21:                         ; Preds .B4.20 .B4.119
        lea       ecx, DWORD PTR [-1+edx]                       ;168.5
        cmp       eax, ecx                                      ;168.5
        jbe       .B4.23        ; Prob 10%                      ;168.5
                                ; LOE eax edx ebx esi
.B4.22:                         ; Preds .B4.21
        mov       DWORD PTR [esp], eax                          ;
        mov       eax, DWORD PTR [68+ebx+esi]                   ;168.5
        neg       eax                                           ;168.5
        add       eax, edx                                      ;168.5
        imul      eax, DWORD PTR [112+esp]                      ;168.5
        mov       edi, DWORD PTR [24+esp]                       ;168.5
        neg       edi                                           ;168.5
        mov       ecx, DWORD PTR [36+ebx+esi]                   ;168.5
        add       edi, edx                                      ;168.5
        mov       eax, DWORD PTR [ecx+eax]                      ;168.5
        mov       ecx, DWORD PTR [100+esp]                      ;168.5
        lea       edx, DWORD PTR [edi*8]                        ;168.5
        lea       edx, DWORD PTR [edx+edi*4]                    ;168.5
        mov       DWORD PTR [ecx+edx], eax                      ;168.5
        mov       eax, DWORD PTR [esp]                          ;168.5
                                ; LOE eax ebx esi
.B4.23:                         ; Preds .B4.16 .B4.15 .B4.22 .B4.21
        cmp       DWORD PTR [112+esp], 4                        ;169.5
        jne       .B4.30        ; Prob 50%                      ;169.5
                                ; LOE eax ebx esi
.B4.24:                         ; Preds .B4.23
        mov       edi, eax                                      ;169.5
        shr       edi, 31                                       ;169.5
        add       edi, eax                                      ;169.5
        sar       edi, 1                                        ;169.5
        test      edi, edi                                      ;169.5
        jbe       .B4.105       ; Prob 10%                      ;169.5
                                ; LOE eax ebx esi edi
.B4.25:                         ; Preds .B4.24
        mov       ecx, DWORD PTR [68+ebx+esi]                   ;169.5
        xor       edx, edx                                      ;
        shl       ecx, 2                                        ;
        neg       ecx                                           ;
        add       ecx, DWORD PTR [36+ebx+esi]                   ;
        mov       DWORD PTR [20+esp], ecx                       ;
        mov       ecx, DWORD PTR [24+esp]                       ;
        mov       DWORD PTR [16+esp], edx                       ;
        mov       DWORD PTR [esp], eax                          ;
        mov       DWORD PTR [4+esp], ebx                        ;
        lea       edx, DWORD PTR [ecx*8]                        ;
        mov       DWORD PTR [8+esp], esi                        ;
        lea       edx, DWORD PTR [edx+ecx*4]                    ;
        neg       edx                                           ;
        add       edx, DWORD PTR [100+esp]                      ;
        mov       DWORD PTR [12+esp], edx                       ;
        mov       edx, DWORD PTR [20+esp]                       ;
        mov       ecx, DWORD PTR [16+esp]                       ;
        mov       eax, DWORD PTR [12+esp]                       ;
                                ; LOE eax edx ecx edi
.B4.26:                         ; Preds .B4.26 .B4.25
        mov       esi, DWORD PTR [8+edx+ecx*8]                  ;169.5
        lea       ebx, DWORD PTR [ecx+ecx*2]                    ;169.5
        mov       DWORD PTR [16+eax+ebx*8], esi                 ;169.5
        mov       esi, DWORD PTR [12+edx+ecx*8]                 ;169.5
        inc       ecx                                           ;169.5
        mov       DWORD PTR [28+eax+ebx*8], esi                 ;169.5
        cmp       ecx, edi                                      ;169.5
        jb        .B4.26        ; Prob 63%                      ;169.5
                                ; LOE eax edx ecx edi
.B4.27:                         ; Preds .B4.26
        mov       eax, DWORD PTR [esp]                          ;
        lea       edx, DWORD PTR [1+ecx+ecx]                    ;169.5
        mov       ebx, DWORD PTR [4+esp]                        ;
        mov       esi, DWORD PTR [8+esp]                        ;
                                ; LOE eax edx ebx esi
.B4.28:                         ; Preds .B4.27 .B4.105
        lea       ecx, DWORD PTR [-1+edx]                       ;169.5
        cmp       eax, ecx                                      ;169.5
        jbe       .B4.36        ; Prob 10%                      ;169.5
                                ; LOE eax edx ebx esi
.B4.29:                         ; Preds .B4.28
        mov       DWORD PTR [esp], eax                          ;
        mov       eax, DWORD PTR [68+ebx+esi]                   ;169.5
        neg       eax                                           ;169.5
        mov       edi, DWORD PTR [24+esp]                       ;169.5
        add       eax, edx                                      ;169.5
        neg       edi                                           ;169.5
        mov       ecx, DWORD PTR [36+ebx+esi]                   ;169.5
        add       edi, edx                                      ;169.5
        mov       eax, DWORD PTR [4+ecx+eax*4]                  ;169.5
        mov       ecx, DWORD PTR [100+esp]                      ;169.5
        lea       edx, DWORD PTR [edi*8]                        ;169.5
        lea       edx, DWORD PTR [edx+edi*4]                    ;169.5
        mov       DWORD PTR [4+ecx+edx], eax                    ;169.5
        mov       eax, DWORD PTR [esp]                          ;169.5
        jmp       .B4.36        ; Prob 100%                     ;169.5
                                ; LOE eax ebx esi
.B4.30:                         ; Preds .B4.23
        mov       edx, eax                                      ;169.5
        shr       edx, 31                                       ;169.5
        add       edx, eax                                      ;169.5
        sar       edx, 1                                        ;169.5
        mov       DWORD PTR [16+esp], edx                       ;169.5
        test      edx, edx                                      ;169.5
        jbe       .B4.118       ; Prob 10%                      ;169.5
                                ; LOE eax ebx esi
.B4.31:                         ; Preds .B4.30
        mov       edx, DWORD PTR [68+ebx+esi]                   ;169.5
        xor       ecx, ecx                                      ;
        imul      edx, DWORD PTR [112+esp]                      ;
        mov       edi, DWORD PTR [36+ebx+esi]                   ;169.5
        sub       edi, edx                                      ;
        mov       DWORD PTR [12+esp], edi                       ;
        mov       edi, DWORD PTR [24+esp]                       ;
        mov       DWORD PTR [esp], eax                          ;
        mov       DWORD PTR [4+esp], ebx                        ;
        mov       DWORD PTR [8+esp], esi                        ;
        lea       edx, DWORD PTR [edi*8]                        ;
        mov       eax, DWORD PTR [12+esp]                       ;
        lea       edx, DWORD PTR [edx+edi*4]                    ;
        neg       edx                                           ;
        add       edx, DWORD PTR [100+esp]                      ;
                                ; LOE eax edx ecx
.B4.32:                         ; Preds .B4.32 .B4.31
        mov       esi, DWORD PTR [112+esp]                      ;169.5
        lea       edi, DWORD PTR [1+ecx+ecx]                    ;169.5
        imul      edi, esi                                      ;169.5
        lea       ebx, DWORD PTR [ecx+ecx*2]                    ;169.5
        mov       edi, DWORD PTR [4+eax+edi]                    ;169.5
        mov       DWORD PTR [16+edx+ebx*8], edi                 ;169.5
        lea       edi, DWORD PTR [2+ecx+ecx]                    ;169.5
        imul      edi, esi                                      ;169.5
        inc       ecx                                           ;169.5
        mov       esi, DWORD PTR [4+eax+edi]                    ;169.5
        mov       DWORD PTR [28+edx+ebx*8], esi                 ;169.5
        cmp       ecx, DWORD PTR [16+esp]                       ;169.5
        jb        .B4.32        ; Prob 63%                      ;169.5
                                ; LOE eax edx ecx
.B4.33:                         ; Preds .B4.32
        mov       eax, DWORD PTR [esp]                          ;
        lea       edx, DWORD PTR [1+ecx+ecx]                    ;169.5
        mov       ebx, DWORD PTR [4+esp]                        ;
        mov       esi, DWORD PTR [8+esp]                        ;
                                ; LOE eax edx ebx esi
.B4.34:                         ; Preds .B4.33 .B4.118
        lea       ecx, DWORD PTR [-1+edx]                       ;169.5
        cmp       eax, ecx                                      ;169.5
        jbe       .B4.36        ; Prob 10%                      ;169.5
                                ; LOE eax edx ebx esi
.B4.35:                         ; Preds .B4.34
        mov       DWORD PTR [esp], eax                          ;
        mov       eax, DWORD PTR [68+ebx+esi]                   ;169.5
        neg       eax                                           ;169.5
        add       eax, edx                                      ;169.5
        imul      eax, DWORD PTR [112+esp]                      ;169.5
        mov       edi, DWORD PTR [24+esp]                       ;169.5
        neg       edi                                           ;169.5
        mov       ecx, DWORD PTR [36+ebx+esi]                   ;169.5
        add       edi, edx                                      ;169.5
        mov       eax, DWORD PTR [4+ecx+eax]                    ;169.5
        mov       ecx, DWORD PTR [100+esp]                      ;169.5
        lea       edx, DWORD PTR [edi*8]                        ;169.5
        lea       edx, DWORD PTR [edx+edi*4]                    ;169.5
        mov       DWORD PTR [4+ecx+edx], eax                    ;169.5
        mov       eax, DWORD PTR [esp]                          ;169.5
                                ; LOE eax ebx esi
.B4.36:                         ; Preds .B4.29 .B4.28 .B4.35 .B4.34
        cmp       DWORD PTR [112+esp], 4                        ;170.5
        jne       .B4.43        ; Prob 50%                      ;170.5
                                ; LOE eax ebx esi
.B4.37:                         ; Preds .B4.36
        mov       edx, eax                                      ;170.5
        shr       edx, 31                                       ;170.5
        add       edx, eax                                      ;170.5
        sar       edx, 1                                        ;170.5
        mov       DWORD PTR [16+esp], edx                       ;170.5
        test      edx, edx                                      ;170.5
        jbe       .B4.106       ; Prob 10%                      ;170.5
                                ; LOE eax ebx esi
.B4.38:                         ; Preds .B4.37
        mov       edx, DWORD PTR [68+ebx+esi]                   ;170.5
        xor       ecx, ecx                                      ;
        shl       edx, 2                                        ;
        neg       edx                                           ;
        add       edx, DWORD PTR [36+ebx+esi]                   ;
        mov       DWORD PTR [12+esp], edx                       ;
        mov       edx, DWORD PTR [24+esp]                       ;
        mov       DWORD PTR [esp], eax                          ;
        mov       DWORD PTR [8+esp], esi                        ;
        mov       DWORD PTR [4+esp], ebx                        ;
        lea       edi, DWORD PTR [edx*8]                        ;
        mov       esi, DWORD PTR [12+esp]                       ;
        lea       edx, DWORD PTR [edi+edx*4]                    ;
        neg       edx                                           ;
        add       edx, DWORD PTR [100+esp]                      ;
        mov       eax, DWORD PTR [16+esp]                       ;
                                ; LOE eax edx ecx esi
.B4.39:                         ; Preds .B4.39 .B4.38
        mov       edi, DWORD PTR [12+esi+ecx*8]                 ;170.5
        lea       ebx, DWORD PTR [ecx+ecx*2]                    ;170.5
        mov       DWORD PTR [20+edx+ebx*8], edi                 ;170.5
        mov       edi, DWORD PTR [16+esi+ecx*8]                 ;170.5
        inc       ecx                                           ;170.5
        mov       DWORD PTR [32+edx+ebx*8], edi                 ;170.5
        cmp       ecx, eax                                      ;170.5
        jb        .B4.39        ; Prob 63%                      ;170.5
                                ; LOE eax edx ecx esi
.B4.40:                         ; Preds .B4.39
        mov       eax, DWORD PTR [esp]                          ;
        lea       edx, DWORD PTR [1+ecx+ecx]                    ;170.5
        mov       ebx, DWORD PTR [4+esp]                        ;
        mov       esi, DWORD PTR [8+esp]                        ;
                                ; LOE eax edx ebx esi
.B4.41:                         ; Preds .B4.40 .B4.106
        lea       ecx, DWORD PTR [-1+edx]                       ;170.5
        cmp       eax, ecx                                      ;170.5
        jbe       .B4.49        ; Prob 10%                      ;170.5
                                ; LOE edx ebx esi
.B4.42:                         ; Preds .B4.41
        mov       eax, DWORD PTR [68+ebx+esi]                   ;170.5
        neg       eax                                           ;170.5
        mov       edi, DWORD PTR [24+esp]                       ;170.5
        add       eax, edx                                      ;170.5
        neg       edi                                           ;170.5
        mov       ecx, DWORD PTR [36+ebx+esi]                   ;170.5
        add       edi, edx                                      ;170.5
        mov       eax, DWORD PTR [8+ecx+eax*4]                  ;170.5
        mov       ecx, DWORD PTR [100+esp]                      ;170.5
        lea       edx, DWORD PTR [edi*8]                        ;170.5
        lea       edx, DWORD PTR [edx+edi*4]                    ;170.5
        mov       DWORD PTR [8+ecx+edx], eax                    ;170.5
        jmp       .B4.49        ; Prob 100%                     ;170.5
                                ; LOE ebx esi
.B4.43:                         ; Preds .B4.36
        mov       edx, eax                                      ;170.5
        shr       edx, 31                                       ;170.5
        add       edx, eax                                      ;170.5
        sar       edx, 1                                        ;170.5
        mov       DWORD PTR [16+esp], edx                       ;170.5
        test      edx, edx                                      ;170.5
        jbe       .B4.117       ; Prob 10%                      ;170.5
                                ; LOE eax ebx esi
.B4.44:                         ; Preds .B4.43
        mov       edx, DWORD PTR [68+ebx+esi]                   ;170.5
        xor       ecx, ecx                                      ;
        imul      edx, DWORD PTR [112+esp]                      ;
        mov       edi, DWORD PTR [36+ebx+esi]                   ;170.5
        sub       edi, edx                                      ;
        mov       DWORD PTR [12+esp], edi                       ;
        mov       edi, DWORD PTR [24+esp]                       ;
        mov       DWORD PTR [esp], eax                          ;
        mov       DWORD PTR [4+esp], ebx                        ;
        mov       DWORD PTR [8+esp], esi                        ;
        lea       edx, DWORD PTR [edi*8]                        ;
        mov       eax, DWORD PTR [12+esp]                       ;
        lea       edx, DWORD PTR [edx+edi*4]                    ;
        neg       edx                                           ;
        add       edx, DWORD PTR [100+esp]                      ;
                                ; LOE eax edx ecx
.B4.45:                         ; Preds .B4.45 .B4.44
        mov       esi, DWORD PTR [112+esp]                      ;170.5
        lea       edi, DWORD PTR [1+ecx+ecx]                    ;170.5
        imul      edi, esi                                      ;170.5
        lea       ebx, DWORD PTR [ecx+ecx*2]                    ;170.5
        mov       edi, DWORD PTR [8+eax+edi]                    ;170.5
        mov       DWORD PTR [20+edx+ebx*8], edi                 ;170.5
        lea       edi, DWORD PTR [2+ecx+ecx]                    ;170.5
        imul      edi, esi                                      ;170.5
        inc       ecx                                           ;170.5
        mov       esi, DWORD PTR [8+eax+edi]                    ;170.5
        mov       DWORD PTR [32+edx+ebx*8], esi                 ;170.5
        cmp       ecx, DWORD PTR [16+esp]                       ;170.5
        jb        .B4.45        ; Prob 63%                      ;170.5
                                ; LOE eax edx ecx
.B4.46:                         ; Preds .B4.45
        mov       eax, DWORD PTR [esp]                          ;
        lea       edx, DWORD PTR [1+ecx+ecx]                    ;170.5
        mov       ebx, DWORD PTR [4+esp]                        ;
        mov       esi, DWORD PTR [8+esp]                        ;
                                ; LOE eax edx ebx esi
.B4.47:                         ; Preds .B4.46 .B4.117
        lea       ecx, DWORD PTR [-1+edx]                       ;170.5
        cmp       eax, ecx                                      ;170.5
        jbe       .B4.49        ; Prob 10%                      ;170.5
                                ; LOE edx ebx esi
.B4.48:                         ; Preds .B4.47
        mov       eax, DWORD PTR [68+ebx+esi]                   ;170.5
        neg       eax                                           ;170.5
        add       eax, edx                                      ;170.5
        imul      eax, DWORD PTR [112+esp]                      ;170.5
        mov       edi, DWORD PTR [24+esp]                       ;170.5
        neg       edi                                           ;170.5
        mov       ecx, DWORD PTR [36+ebx+esi]                   ;170.5
        add       edi, edx                                      ;170.5
        mov       eax, DWORD PTR [8+ecx+eax]                    ;170.5
        mov       ecx, DWORD PTR [100+esp]                      ;170.5
        lea       edx, DWORD PTR [edi*8]                        ;170.5
        lea       edx, DWORD PTR [edx+edi*4]                    ;170.5
        mov       DWORD PTR [8+ecx+edx], eax                    ;170.5
                                ; LOE ebx esi
.B4.49:                         ; Preds .B4.9 .B4.41 .B4.47 .B4.42 .B4.48
                                ;      
        mov       edi, DWORD PTR [48+ebx+esi]                   ;173.6
        test      edi, 1                                        ;173.10
        je        .B4.52        ; Prob 60%                      ;173.10
                                ; LOE ebx esi edi
.B4.50:                         ; Preds .B4.49
        mov       edx, edi                                      ;174.4
        mov       eax, edi                                      ;174.4
        shr       edx, 1                                        ;174.4
        and       eax, 1                                        ;174.4
        and       edx, 1                                        ;174.4
        add       eax, eax                                      ;174.4
        shl       edx, 2                                        ;174.4
        or        edx, 1                                        ;174.4
        or        edx, eax                                      ;174.4
        or        edx, 262144                                   ;174.4
        push      edx                                           ;174.4
        push      DWORD PTR [36+ebx+esi]                        ;174.4
        call      _for_dealloc_allocatable                      ;174.4
                                ; LOE eax ebx esi edi
.B4.159:                        ; Preds .B4.50
        add       esp, 8                                        ;174.4
                                ; LOE eax ebx esi edi
.B4.51:                         ; Preds .B4.159
        and       edi, -2                                       ;174.4
        mov       DWORD PTR [36+ebx+esi], 0                     ;174.4
        test      eax, eax                                      ;175.19
        mov       DWORD PTR [48+ebx+esi], edi                   ;174.4
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VECTORERROR], eax ;174.4
        jne       .B4.107       ; Prob 5%                       ;175.19
                                ; LOE
.B4.52:                         ; Preds .B4.49 .B4.51 .B4.165
        mov       eax, DWORD PTR [12+ebp]                       ;182.4
        xor       ecx, ecx                                      ;182.4
        mov       edx, DWORD PTR [124+esp]                      ;182.4
        lea       esi, DWORD PTR [100+esp]                      ;182.4
        push      12                                            ;182.4
        mov       ebx, DWORD PTR [eax]                          ;182.4
        cmp       ebx, edx                                      ;182.4
        cmovl     ebx, edx                                      ;182.4
        test      ebx, ebx                                      ;182.4
        cmovl     ebx, ecx                                      ;182.4
        push      ebx                                           ;182.4
        push      2                                             ;182.4
        push      esi                                           ;182.4
        call      _for_check_mult_overflow                      ;182.4
                                ; LOE eax
.B4.53:                         ; Preds .B4.52
        mov       edx, DWORD PTR [8+ebp]                        ;182.4
        and       eax, 1                                        ;182.4
        mov       ecx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY+32] ;182.4
        neg       ecx                                           ;182.4
        add       ecx, DWORD PTR [edx]                          ;182.4
        imul      ebx, ecx, 76                                  ;182.4
        mov       esi, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY] ;182.4
        shl       eax, 4                                        ;182.4
        or        eax, 262145                                   ;182.4
        push      eax                                           ;182.4
        lea       edi, DWORD PTR [36+ebx+esi]                   ;182.4
        push      edi                                           ;182.4
        push      DWORD PTR [124+esp]                           ;182.4
        call      _for_allocate                                 ;182.4
                                ; LOE eax
.B4.161:                        ; Preds .B4.53
        add       esp, 28                                       ;182.4
        mov       ebx, eax                                      ;182.4
                                ; LOE ebx
.B4.54:                         ; Preds .B4.161
        test      ebx, ebx                                      ;182.4
        je        .B4.57        ; Prob 50%                      ;182.4
                                ; LOE ebx
.B4.55:                         ; Preds .B4.54
        mov       DWORD PTR [32+esp], 0                         ;184.7
        lea       edx, DWORD PTR [32+esp]                       ;184.7
        mov       DWORD PTR [112+esp], 36                       ;184.7
        lea       eax, DWORD PTR [112+esp]                      ;184.7
        mov       DWORD PTR [116+esp], OFFSET FLAT: __STRLITPACK_18 ;184.7
        push      32                                            ;184.7
        push      eax                                           ;184.7
        push      OFFSET FLAT: __STRLITPACK_48.0.4              ;184.7
        push      -2088435968                                   ;184.7
        push      911                                           ;184.7
        push      edx                                           ;184.7
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VECTORERROR], ebx ;182.4
        call      _for_write_seq_lis                            ;184.7
                                ; LOE
.B4.56:                         ; Preds .B4.55
        push      32                                            ;185.4
        xor       eax, eax                                      ;185.4
        push      eax                                           ;185.4
        push      eax                                           ;185.4
        push      -2088435968                                   ;185.4
        push      eax                                           ;185.4
        push      OFFSET FLAT: __STRLITPACK_49                  ;185.4
        call      _for_stop_core                                ;185.4
                                ; LOE
.B4.162:                        ; Preds .B4.56
        add       esp, 48                                       ;185.4
        jmp       .B4.59        ; Prob 100%                     ;185.4
                                ; LOE
.B4.57:                         ; Preds .B4.54
        mov       edi, DWORD PTR [8+ebp]                        ;182.13
        mov       eax, 1                                        ;182.4
        mov       DWORD PTR [8+esp], ebx                        ;
        xor       edx, edx                                      ;182.4
        mov       ebx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY] ;182.4
        mov       esi, DWORD PTR [edi]                          ;182.13
        sub       esi, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY+32] ;182.4
        imul      ecx, esi, 76                                  ;182.4
        mov       esi, 12                                       ;182.4
        mov       edi, DWORD PTR [12+ebp]                       ;182.4
        mov       DWORD PTR [52+ebx+ecx], eax                   ;182.4
        mov       DWORD PTR [68+ebx+ecx], eax                   ;182.4
        mov       eax, DWORD PTR [edi]                          ;182.4
        mov       edi, DWORD PTR [124+esp]                      ;182.4
        cmp       eax, edi                                      ;182.4
        mov       DWORD PTR [44+ebx+ecx], edx                   ;182.4
        cmovl     eax, edi                                      ;182.4
        test      eax, eax                                      ;182.4
        mov       DWORD PTR [48+ebx+ecx], 5                     ;182.4
        cmovl     eax, edx                                      ;182.4
        lea       edx, DWORD PTR [12+esp]                       ;182.4
        push      esi                                           ;182.4
        push      eax                                           ;182.4
        push      2                                             ;182.4
        push      edx                                           ;182.4
        mov       DWORD PTR [40+ebx+ecx], esi                   ;182.4
        mov       DWORD PTR [60+ebx+ecx], eax                   ;182.4
        mov       DWORD PTR [64+ebx+ecx], esi                   ;182.4
        mov       ebx, DWORD PTR [24+esp]                       ;182.4
        call      _for_check_mult_overflow                      ;182.4
                                ; LOE ebx bl bh
.B4.163:                        ; Preds .B4.57
        add       esp, 16                                       ;182.4
                                ; LOE ebx bl bh
.B4.58:                         ; Preds .B4.163
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VECTORERROR], ebx ;182.4
                                ; LOE
.B4.59:                         ; Preds .B4.162 .B4.58
        mov       ebx, DWORD PTR [124+esp]                      ;189.6
        mov       esi, ebx                                      ;189.6
        shr       esi, 31                                       ;189.6
        add       esi, ebx                                      ;189.6
        mov       eax, DWORD PTR [64+esp]                       ;189.6
        sar       esi, 1                                        ;189.6
        mov       DWORD PTR [24+esp], eax                       ;189.6
        test      esi, esi                                      ;189.6
        mov       eax, DWORD PTR [96+esp]                       ;189.6
        mov       ecx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY] ;189.6
        mov       edx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY+32] ;189.6
        mov       DWORD PTR [128+esp], esi                      ;189.6
        jbe       .B4.116       ; Prob 11%                      ;189.6
                                ; LOE eax edx ecx
.B4.60:                         ; Preds .B4.59
        imul      esi, edx, -76                                 ;
        lea       edi, DWORD PTR [eax*8]                        ;
        add       esi, ecx                                      ;
        lea       edi, DWORD PTR [edi+eax*4]                    ;
        neg       edi                                           ;
        xor       ebx, ebx                                      ;
        add       edi, DWORD PTR [24+esp]                       ;
        mov       DWORD PTR [132+esp], edi                      ;
        mov       DWORD PTR [136+esp], esi                      ;
        mov       DWORD PTR [16+esp], edx                       ;
        mov       DWORD PTR [8+esp], ecx                        ;
        mov       DWORD PTR [20+esp], eax                       ;
                                ; LOE ebx
.B4.61:                         ; Preds .B4.61 .B4.60
        mov       ecx, DWORD PTR [8+ebp]                        ;189.6
        lea       esi, DWORD PTR [1+ebx+ebx]                    ;189.6
        mov       edx, DWORD PTR [136+esp]                      ;189.6
        lea       edi, DWORD PTR [ebx+ebx*2]                    ;189.6
        imul      eax, DWORD PTR [ecx], 76                      ;189.6
        sub       esi, DWORD PTR [68+eax+edx]                   ;189.6
        imul      esi, DWORD PTR [64+eax+edx]                   ;189.6
        mov       edx, DWORD PTR [36+eax+edx]                   ;189.6
        mov       eax, DWORD PTR [132+esp]                      ;189.6
        mov       eax, DWORD PTR [12+eax+edi*8]                 ;189.6
        mov       DWORD PTR [edx+esi], eax                      ;189.6
        lea       eax, DWORD PTR [2+ebx+ebx]                    ;189.6
        imul      ecx, DWORD PTR [ecx], 76                      ;189.6
        inc       ebx                                           ;189.6
        mov       esi, DWORD PTR [136+esp]                      ;189.6
        mov       edx, DWORD PTR [132+esp]                      ;189.6
        sub       eax, DWORD PTR [68+ecx+esi]                   ;189.6
        imul      eax, DWORD PTR [64+ecx+esi]                   ;189.6
        mov       ecx, DWORD PTR [36+ecx+esi]                   ;189.6
        mov       edi, DWORD PTR [24+edx+edi*8]                 ;189.6
        cmp       ebx, DWORD PTR [128+esp]                      ;189.6
        mov       DWORD PTR [ecx+eax], edi                      ;189.6
        jb        .B4.61        ; Prob 64%                      ;189.6
                                ; LOE ebx
.B4.62:                         ; Preds .B4.61
        mov       edx, DWORD PTR [16+esp]                       ;
        lea       ebx, DWORD PTR [1+ebx+ebx]                    ;189.6
        mov       ecx, DWORD PTR [8+esp]                        ;
        mov       eax, DWORD PTR [20+esp]                       ;
                                ; LOE eax edx ecx ebx
.B4.63:                         ; Preds .B4.62 .B4.116
        lea       esi, DWORD PTR [-1+ebx]                       ;189.6
        cmp       esi, DWORD PTR [124+esp]                      ;189.6
        jae       .B4.65        ; Prob 11%                      ;189.6
                                ; LOE eax edx ecx ebx
.B4.64:                         ; Preds .B4.63
        mov       esi, DWORD PTR [8+ebp]                        ;189.6
        mov       DWORD PTR [16+esp], edx                       ;
        neg       edx                                           ;189.6
        add       edx, DWORD PTR [esi]                          ;189.6
        mov       esi, eax                                      ;189.6
        imul      edi, edx, 76                                  ;189.6
        neg       esi                                           ;189.6
        add       esi, ebx                                      ;189.6
        sub       ebx, DWORD PTR [68+ecx+edi]                   ;189.6
        imul      ebx, DWORD PTR [64+ecx+edi]                   ;189.6
        lea       edx, DWORD PTR [esi*8]                        ;189.6
        lea       esi, DWORD PTR [edx+esi*4]                    ;189.6
        mov       edx, DWORD PTR [36+ecx+edi]                   ;189.6
        mov       edi, DWORD PTR [24+esp]                       ;189.6
        mov       esi, DWORD PTR [edi+esi]                      ;189.6
        mov       DWORD PTR [edx+ebx], esi                      ;189.6
        mov       edx, DWORD PTR [16+esp]                       ;189.6
                                ; LOE eax edx ecx
.B4.65:                         ; Preds .B4.63 .B4.64
        cmp       DWORD PTR [128+esp], 0                        ;190.6
        jbe       .B4.115       ; Prob 10%                      ;190.6
                                ; LOE eax edx ecx
.B4.66:                         ; Preds .B4.65
        imul      esi, edx, -76                                 ;
        lea       edi, DWORD PTR [eax*8]                        ;
        add       esi, ecx                                      ;
        lea       edi, DWORD PTR [edi+eax*4]                    ;
        neg       edi                                           ;
        xor       ebx, ebx                                      ;
        add       edi, DWORD PTR [24+esp]                       ;
        mov       DWORD PTR [132+esp], edi                      ;
        mov       DWORD PTR [136+esp], esi                      ;
        mov       DWORD PTR [16+esp], edx                       ;
        mov       DWORD PTR [8+esp], ecx                        ;
        mov       DWORD PTR [20+esp], eax                       ;
                                ; LOE ebx
.B4.67:                         ; Preds .B4.67 .B4.66
        mov       ecx, DWORD PTR [8+ebp]                        ;190.6
        lea       esi, DWORD PTR [1+ebx+ebx]                    ;190.6
        mov       edx, DWORD PTR [136+esp]                      ;190.6
        lea       edi, DWORD PTR [ebx+ebx*2]                    ;190.6
        imul      eax, DWORD PTR [ecx], 76                      ;190.6
        sub       esi, DWORD PTR [68+eax+edx]                   ;190.6
        imul      esi, DWORD PTR [64+eax+edx]                   ;190.6
        mov       edx, DWORD PTR [36+eax+edx]                   ;190.6
        mov       eax, DWORD PTR [132+esp]                      ;190.6
        mov       eax, DWORD PTR [16+eax+edi*8]                 ;190.6
        mov       DWORD PTR [4+edx+esi], eax                    ;190.6
        lea       eax, DWORD PTR [2+ebx+ebx]                    ;190.6
        imul      ecx, DWORD PTR [ecx], 76                      ;190.6
        inc       ebx                                           ;190.6
        mov       esi, DWORD PTR [136+esp]                      ;190.6
        mov       edx, DWORD PTR [132+esp]                      ;190.6
        sub       eax, DWORD PTR [68+ecx+esi]                   ;190.6
        imul      eax, DWORD PTR [64+ecx+esi]                   ;190.6
        mov       ecx, DWORD PTR [36+ecx+esi]                   ;190.6
        mov       edi, DWORD PTR [28+edx+edi*8]                 ;190.6
        cmp       ebx, DWORD PTR [128+esp]                      ;190.6
        mov       DWORD PTR [4+ecx+eax], edi                    ;190.6
        jb        .B4.67        ; Prob 64%                      ;190.6
                                ; LOE ebx
.B4.68:                         ; Preds .B4.67
        mov       edx, DWORD PTR [16+esp]                       ;
        lea       ebx, DWORD PTR [1+ebx+ebx]                    ;190.6
        mov       ecx, DWORD PTR [8+esp]                        ;
        mov       eax, DWORD PTR [20+esp]                       ;
                                ; LOE eax edx ecx ebx
.B4.69:                         ; Preds .B4.68 .B4.115
        lea       esi, DWORD PTR [-1+ebx]                       ;190.6
        cmp       esi, DWORD PTR [124+esp]                      ;190.6
        jae       .B4.71        ; Prob 10%                      ;190.6
                                ; LOE eax edx ecx ebx
.B4.70:                         ; Preds .B4.69
        mov       esi, DWORD PTR [8+ebp]                        ;190.6
        mov       DWORD PTR [16+esp], edx                       ;
        neg       edx                                           ;190.6
        add       edx, DWORD PTR [esi]                          ;190.6
        mov       esi, eax                                      ;190.6
        imul      edi, edx, 76                                  ;190.6
        neg       esi                                           ;190.6
        add       esi, ebx                                      ;190.6
        sub       ebx, DWORD PTR [68+ecx+edi]                   ;190.6
        imul      ebx, DWORD PTR [64+ecx+edi]                   ;190.6
        lea       edx, DWORD PTR [esi*8]                        ;190.6
        lea       esi, DWORD PTR [edx+esi*4]                    ;190.6
        mov       edx, DWORD PTR [36+ecx+edi]                   ;190.6
        mov       edi, DWORD PTR [24+esp]                       ;190.6
        mov       esi, DWORD PTR [4+edi+esi]                    ;190.6
        mov       DWORD PTR [4+edx+ebx], esi                    ;190.6
        mov       edx, DWORD PTR [16+esp]                       ;190.6
                                ; LOE eax edx ecx
.B4.71:                         ; Preds .B4.69 .B4.70
        cmp       DWORD PTR [128+esp], 0                        ;191.6
        jbe       .B4.114       ; Prob 10%                      ;191.6
                                ; LOE eax edx ecx
.B4.72:                         ; Preds .B4.71
        imul      esi, edx, -76                                 ;
        lea       edi, DWORD PTR [eax*8]                        ;
        add       esi, ecx                                      ;
        lea       edi, DWORD PTR [edi+eax*4]                    ;
        neg       edi                                           ;
        xor       ebx, ebx                                      ;
        add       edi, DWORD PTR [24+esp]                       ;
        mov       DWORD PTR [132+esp], edi                      ;
        mov       DWORD PTR [136+esp], esi                      ;
        mov       DWORD PTR [16+esp], edx                       ;
        mov       DWORD PTR [8+esp], ecx                        ;
        mov       DWORD PTR [20+esp], eax                       ;
                                ; LOE ebx
.B4.73:                         ; Preds .B4.73 .B4.72
        mov       ecx, DWORD PTR [8+ebp]                        ;191.6
        lea       esi, DWORD PTR [1+ebx+ebx]                    ;191.6
        mov       edx, DWORD PTR [136+esp]                      ;191.6
        lea       edi, DWORD PTR [ebx+ebx*2]                    ;191.6
        imul      eax, DWORD PTR [ecx], 76                      ;191.6
        sub       esi, DWORD PTR [68+eax+edx]                   ;191.6
        imul      esi, DWORD PTR [64+eax+edx]                   ;191.6
        mov       edx, DWORD PTR [36+eax+edx]                   ;191.6
        mov       eax, DWORD PTR [132+esp]                      ;191.6
        mov       eax, DWORD PTR [20+eax+edi*8]                 ;191.6
        mov       DWORD PTR [8+edx+esi], eax                    ;191.6
        lea       eax, DWORD PTR [2+ebx+ebx]                    ;191.6
        imul      ecx, DWORD PTR [ecx], 76                      ;191.6
        inc       ebx                                           ;191.6
        mov       esi, DWORD PTR [136+esp]                      ;191.6
        mov       edx, DWORD PTR [132+esp]                      ;191.6
        sub       eax, DWORD PTR [68+ecx+esi]                   ;191.6
        imul      eax, DWORD PTR [64+ecx+esi]                   ;191.6
        mov       ecx, DWORD PTR [36+ecx+esi]                   ;191.6
        mov       edi, DWORD PTR [32+edx+edi*8]                 ;191.6
        cmp       ebx, DWORD PTR [128+esp]                      ;191.6
        mov       DWORD PTR [8+ecx+eax], edi                    ;191.6
        jb        .B4.73        ; Prob 64%                      ;191.6
                                ; LOE ebx
.B4.74:                         ; Preds .B4.73
        mov       edx, DWORD PTR [16+esp]                       ;
        lea       ebx, DWORD PTR [1+ebx+ebx]                    ;191.6
        mov       ecx, DWORD PTR [8+esp]                        ;
        mov       eax, DWORD PTR [20+esp]                       ;
                                ; LOE eax edx ecx ebx
.B4.75:                         ; Preds .B4.74 .B4.114
        lea       esi, DWORD PTR [-1+ebx]                       ;191.6
        cmp       esi, DWORD PTR [124+esp]                      ;191.6
        jae       .B4.77        ; Prob 10%                      ;191.6
                                ; LOE eax edx ecx ebx
.B4.76:                         ; Preds .B4.75
        mov       esi, DWORD PTR [8+ebp]                        ;191.6
        mov       edi, edx                                      ;191.6
        neg       edi                                           ;191.6
        neg       eax                                           ;191.6
        add       eax, ebx                                      ;191.6
        add       edi, DWORD PTR [esi]                          ;191.6
        imul      edi, edi, 76                                  ;191.6
        sub       ebx, DWORD PTR [68+ecx+edi]                   ;191.6
        lea       esi, DWORD PTR [eax*8]                        ;191.6
        imul      ebx, DWORD PTR [64+ecx+edi]                   ;191.6
        lea       esi, DWORD PTR [esi+eax*4]                    ;191.6
        mov       eax, DWORD PTR [36+ecx+edi]                   ;191.6
        mov       edi, DWORD PTR [24+esp]                       ;191.6
        mov       esi, DWORD PTR [8+edi+esi]                    ;191.6
        mov       DWORD PTR [8+eax+ebx], esi                    ;191.6
                                ; LOE edx ecx
.B4.77:                         ; Preds .B4.75 .B4.76
        mov       eax, DWORD PTR [12+ebp]                       ;194.7
        mov       eax, DWORD PTR [eax]                          ;194.7
        mov       ebx, eax                                      ;194.7
        sub       ebx, DWORD PTR [124+esp]                      ;194.7
        test      ebx, ebx                                      ;194.7
        jle       .B4.85        ; Prob 50%                      ;194.7
                                ; LOE eax edx ecx ebx
.B4.78:                         ; Preds .B4.77
        mov       edi, ebx                                      ;194.7
        shr       edi, 31                                       ;194.7
        add       edi, ebx                                      ;194.7
        sar       edi, 1                                        ;194.7
        test      edi, edi                                      ;194.7
        jbe       .B4.109       ; Prob 10%                      ;194.7
                                ; LOE eax edx ecx edi
.B4.79:                         ; Preds .B4.78
        imul      esi, edx, -76                                 ;
        xor       ebx, ebx                                      ;
        mov       DWORD PTR [8+esp], ecx                        ;
        add       esi, ecx                                      ;
        mov       DWORD PTR [20+esp], eax                       ;
        mov       DWORD PTR [24+esp], edi                       ;
        mov       DWORD PTR [16+esp], edx                       ;
        mov       ecx, DWORD PTR [8+ebp]                        ;
                                ; LOE ecx ebx esi
.B4.80:                         ; Preds .B4.80 .B4.79
        mov       edi, DWORD PTR [124+esp]                      ;194.7
        mov       DWORD PTR [128+esp], ebx                      ;
        lea       eax, DWORD PTR [edi+ebx*2]                    ;194.7
        imul      ebx, DWORD PTR [ecx], 76                      ;194.7
        mov       edi, DWORD PTR [36+ebx+esi]                   ;194.7
        lea       edx, DWORD PTR [1+eax]                        ;194.7
        sub       edx, DWORD PTR [68+ebx+esi]                   ;194.7
        add       eax, 2                                        ;194.7
        imul      edx, DWORD PTR [64+ebx+esi]                   ;194.7
        xor       ebx, ebx                                      ;194.7
        mov       DWORD PTR [edi+edx], ebx                      ;194.7
        imul      edx, DWORD PTR [ecx], 76                      ;194.7
        sub       eax, DWORD PTR [68+edx+esi]                   ;194.7
        imul      eax, DWORD PTR [64+edx+esi]                   ;194.7
        mov       edx, DWORD PTR [36+edx+esi]                   ;194.7
        mov       DWORD PTR [edx+eax], ebx                      ;194.7
        mov       ebx, DWORD PTR [128+esp]                      ;194.7
        inc       ebx                                           ;194.7
        cmp       ebx, DWORD PTR [24+esp]                       ;194.7
        jb        .B4.80        ; Prob 64%                      ;194.7
                                ; LOE ecx ebx esi
.B4.81:                         ; Preds .B4.80
        mov       eax, DWORD PTR [20+esp]                       ;
        lea       ebx, DWORD PTR [1+ebx+ebx]                    ;194.7
        mov       edx, DWORD PTR [16+esp]                       ;
        mov       ecx, DWORD PTR [8+esp]                        ;
                                ; LOE eax edx ecx ebx
.B4.82:                         ; Preds .B4.81 .B4.109
        sub       eax, DWORD PTR [124+esp]                      ;194.7
        lea       esi, DWORD PTR [-1+ebx]                       ;194.7
        cmp       eax, esi                                      ;194.7
        jbe       .B4.84        ; Prob 10%                      ;194.7
                                ; LOE edx ecx ebx
.B4.83:                         ; Preds .B4.82
        mov       eax, DWORD PTR [8+ebp]                        ;194.7
        mov       esi, edx                                      ;194.7
        neg       esi                                           ;194.7
        add       ebx, DWORD PTR [124+esp]                      ;194.7
        add       esi, DWORD PTR [eax]                          ;194.7
        imul      edi, esi, 76                                  ;194.7
        sub       ebx, DWORD PTR [68+ecx+edi]                   ;194.7
        imul      ebx, DWORD PTR [64+ecx+edi]                   ;194.7
        mov       eax, DWORD PTR [36+ecx+edi]                   ;194.7
        mov       DWORD PTR [eax+ebx], 0                        ;194.7
                                ; LOE edx ecx
.B4.84:                         ; Preds .B4.82 .B4.83
        mov       eax, DWORD PTR [12+ebp]                       ;195.7
        mov       eax, DWORD PTR [eax]                          ;195.7
                                ; LOE eax edx ecx
.B4.85:                         ; Preds .B4.84 .B4.77
        sub       eax, DWORD PTR [124+esp]                      ;195.7
        test      eax, eax                                      ;195.7
        jle       .B4.99        ; Prob 50%                      ;195.7
                                ; LOE eax edx ecx
.B4.86:                         ; Preds .B4.85
        mov       edi, eax                                      ;195.7
        shr       edi, 31                                       ;195.7
        add       edi, eax                                      ;195.7
        sar       edi, 1                                        ;195.7
        test      edi, edi                                      ;195.7
        jbe       .B4.111       ; Prob 10%                      ;195.7
                                ; LOE eax edx ecx edi
.B4.87:                         ; Preds .B4.86
        imul      esi, edx, -76                                 ;
        xor       ebx, ebx                                      ;
        mov       DWORD PTR [8+esp], ecx                        ;
        add       esi, ecx                                      ;
        mov       DWORD PTR [24+esp], edi                       ;
        mov       DWORD PTR [20+esp], eax                       ;
        mov       DWORD PTR [16+esp], edx                       ;
        mov       ecx, DWORD PTR [8+ebp]                        ;
                                ; LOE ecx ebx esi
.B4.88:                         ; Preds .B4.88 .B4.87
        mov       edi, DWORD PTR [124+esp]                      ;195.7
        mov       DWORD PTR [128+esp], ebx                      ;
        lea       eax, DWORD PTR [edi+ebx*2]                    ;195.7
        imul      ebx, DWORD PTR [ecx], 76                      ;195.7
        mov       edi, DWORD PTR [36+ebx+esi]                   ;195.7
        lea       edx, DWORD PTR [1+eax]                        ;195.7
        sub       edx, DWORD PTR [68+ebx+esi]                   ;195.7
        add       eax, 2                                        ;195.7
        imul      edx, DWORD PTR [64+ebx+esi]                   ;195.7
        xor       ebx, ebx                                      ;195.7
        mov       DWORD PTR [4+edi+edx], ebx                    ;195.7
        imul      edx, DWORD PTR [ecx], 76                      ;195.7
        sub       eax, DWORD PTR [68+edx+esi]                   ;195.7
        imul      eax, DWORD PTR [64+edx+esi]                   ;195.7
        mov       edx, DWORD PTR [36+edx+esi]                   ;195.7
        mov       DWORD PTR [4+edx+eax], ebx                    ;195.7
        mov       ebx, DWORD PTR [128+esp]                      ;195.7
        inc       ebx                                           ;195.7
        cmp       ebx, DWORD PTR [24+esp]                       ;195.7
        jb        .B4.88        ; Prob 64%                      ;195.7
                                ; LOE ecx ebx esi
.B4.89:                         ; Preds .B4.88
        mov       eax, DWORD PTR [20+esp]                       ;
        lea       ebx, DWORD PTR [1+ebx+ebx]                    ;195.7
        mov       edx, DWORD PTR [16+esp]                       ;
        mov       ecx, DWORD PTR [8+esp]                        ;
                                ; LOE eax edx ecx ebx
.B4.90:                         ; Preds .B4.89 .B4.111
        lea       esi, DWORD PTR [-1+ebx]                       ;195.7
        cmp       eax, esi                                      ;195.7
        jbe       .B4.92        ; Prob 10%                      ;195.7
                                ; LOE edx ecx ebx
.B4.91:                         ; Preds .B4.90
        mov       eax, DWORD PTR [8+ebp]                        ;195.7
        mov       esi, edx                                      ;195.7
        neg       esi                                           ;195.7
        add       ebx, DWORD PTR [124+esp]                      ;195.7
        add       esi, DWORD PTR [eax]                          ;195.7
        imul      edi, esi, 76                                  ;195.7
        sub       ebx, DWORD PTR [68+ecx+edi]                   ;195.7
        imul      ebx, DWORD PTR [64+ecx+edi]                   ;195.7
        mov       eax, DWORD PTR [36+ecx+edi]                   ;195.7
        mov       DWORD PTR [4+eax+ebx], 0                      ;195.7
                                ; LOE edx ecx
.B4.92:                         ; Preds .B4.90 .B4.91
        mov       eax, DWORD PTR [12+ebp]                       ;196.7
        mov       eax, DWORD PTR [eax]                          ;196.7
        sub       eax, DWORD PTR [124+esp]                      ;196.7
        test      eax, eax                                      ;196.7
        jle       .B4.99        ; Prob 50%                      ;196.7
                                ; LOE eax edx ecx
.B4.93:                         ; Preds .B4.92
        mov       edi, eax                                      ;196.7
        shr       edi, 31                                       ;196.7
        add       edi, eax                                      ;196.7
        sar       edi, 1                                        ;196.7
        test      edi, edi                                      ;196.7
        jbe       .B4.110       ; Prob 10%                      ;196.7
                                ; LOE eax edx ecx edi
.B4.94:                         ; Preds .B4.93
        imul      esi, edx, -76                                 ;
        xor       ebx, ebx                                      ;
        mov       DWORD PTR [8+esp], ecx                        ;
        add       esi, ecx                                      ;
        mov       DWORD PTR [24+esp], edi                       ;
        mov       DWORD PTR [20+esp], eax                       ;
        mov       DWORD PTR [16+esp], edx                       ;
        mov       ecx, DWORD PTR [8+ebp]                        ;
                                ; LOE ecx ebx esi
.B4.95:                         ; Preds .B4.95 .B4.94
        mov       edi, DWORD PTR [124+esp]                      ;196.7
        mov       DWORD PTR [128+esp], ebx                      ;
        lea       eax, DWORD PTR [edi+ebx*2]                    ;196.7
        imul      ebx, DWORD PTR [ecx], 76                      ;196.7
        mov       edi, DWORD PTR [36+ebx+esi]                   ;196.7
        lea       edx, DWORD PTR [1+eax]                        ;196.7
        sub       edx, DWORD PTR [68+ebx+esi]                   ;196.7
        add       eax, 2                                        ;196.7
        imul      edx, DWORD PTR [64+ebx+esi]                   ;196.7
        xor       ebx, ebx                                      ;196.7
        mov       DWORD PTR [8+edi+edx], ebx                    ;196.7
        imul      edx, DWORD PTR [ecx], 76                      ;196.7
        sub       eax, DWORD PTR [68+edx+esi]                   ;196.7
        imul      eax, DWORD PTR [64+edx+esi]                   ;196.7
        mov       edx, DWORD PTR [36+edx+esi]                   ;196.7
        mov       DWORD PTR [8+edx+eax], ebx                    ;196.7
        mov       ebx, DWORD PTR [128+esp]                      ;196.7
        inc       ebx                                           ;196.7
        cmp       ebx, DWORD PTR [24+esp]                       ;196.7
        jb        .B4.95        ; Prob 64%                      ;196.7
                                ; LOE ecx ebx esi
.B4.96:                         ; Preds .B4.95
        mov       eax, DWORD PTR [20+esp]                       ;
        lea       ebx, DWORD PTR [1+ebx+ebx]                    ;196.7
        mov       edx, DWORD PTR [16+esp]                       ;
        mov       ecx, DWORD PTR [8+esp]                        ;
                                ; LOE eax edx ecx ebx
.B4.97:                         ; Preds .B4.96 .B4.110
        lea       esi, DWORD PTR [-1+ebx]                       ;196.7
        cmp       eax, esi                                      ;196.7
        jbe       .B4.99        ; Prob 10%                      ;196.7
                                ; LOE edx ecx ebx
.B4.98:                         ; Preds .B4.97
        mov       eax, DWORD PTR [8+ebp]                        ;196.7
        neg       edx                                           ;196.7
        add       ebx, DWORD PTR [124+esp]                      ;196.7
        add       edx, DWORD PTR [eax]                          ;196.7
        imul      edx, edx, 76                                  ;196.7
        sub       ebx, DWORD PTR [68+ecx+edx]                   ;196.7
        imul      ebx, DWORD PTR [64+ecx+edx]                   ;196.7
        mov       ecx, DWORD PTR [36+ecx+edx]                   ;196.7
        mov       DWORD PTR [8+ecx+ebx], 0                      ;196.7
                                ; LOE
.B4.99:                         ; Preds .B4.85 .B4.92 .B4.97 .B4.98
        mov       ebx, DWORD PTR [76+esp]                       ;198.4
        test      bl, 1                                         ;198.7
        je        .B4.102       ; Prob 60%                      ;198.7
                                ; LOE ebx
.B4.100:                        ; Preds .B4.99
        mov       edx, ebx                                      ;198.25
        mov       eax, ebx                                      ;198.25
        shr       edx, 1                                        ;198.25
        and       eax, 1                                        ;198.25
        and       edx, 1                                        ;198.25
        add       eax, eax                                      ;198.25
        shl       edx, 2                                        ;198.25
        or        edx, eax                                      ;198.25
        or        edx, 262144                                   ;198.25
        push      edx                                           ;198.25
        push      DWORD PTR [68+esp]                            ;198.25
        call      _for_dealloc_allocatable                      ;198.25
                                ; LOE ebx
.B4.164:                        ; Preds .B4.100
        add       esp, 8                                        ;198.25
                                ; LOE ebx
.B4.101:                        ; Preds .B4.164
        and       ebx, -2                                       ;198.25
        mov       DWORD PTR [64+esp], 0                         ;198.25
        mov       DWORD PTR [76+esp], ebx                       ;198.25
                                ; LOE ebx
.B4.102:                        ; Preds .B4.101 .B4.99 .B4.148
        test      bl, 1                                         ;212.1
        jne       .B4.112       ; Prob 3%                       ;212.1
                                ; LOE ebx
.B4.103:                        ; Preds .B4.102
        add       esp, 148                                      ;212.1
        pop       ebx                                           ;212.1
        pop       edi                                           ;212.1
        pop       esi                                           ;212.1
        mov       esp, ebp                                      ;212.1
        pop       ebp                                           ;212.1
        ret                                                     ;212.1
                                ; LOE
.B4.104:                        ; Preds .B4.11                  ; Infreq
        mov       edx, 1                                        ;
        jmp       .B4.15        ; Prob 100%                     ;
                                ; LOE eax edx ebx esi
.B4.105:                        ; Preds .B4.24                  ; Infreq
        mov       edx, 1                                        ;
        jmp       .B4.28        ; Prob 100%                     ;
                                ; LOE eax edx ebx esi
.B4.106:                        ; Preds .B4.37                  ; Infreq
        mov       edx, 1                                        ;
        jmp       .B4.41        ; Prob 100%                     ;
                                ; LOE eax edx ebx esi
.B4.107:                        ; Preds .B4.51                  ; Infreq
        mov       DWORD PTR [32+esp], 0                         ;176.6
        lea       edx, DWORD PTR [32+esp]                       ;176.6
        mov       DWORD PTR [esp], 38                           ;176.6
        lea       eax, DWORD PTR [esp]                          ;176.6
        mov       DWORD PTR [4+esp], OFFSET FLAT: __STRLITPACK_20 ;176.6
        push      32                                            ;176.6
        push      eax                                           ;176.6
        push      OFFSET FLAT: __STRLITPACK_46.0.4              ;176.6
        push      -2088435968                                   ;176.6
        push      911                                           ;176.6
        push      edx                                           ;176.6
        call      _for_write_seq_lis                            ;176.6
                                ; LOE
.B4.108:                        ; Preds .B4.107                 ; Infreq
        push      32                                            ;177.6
        xor       eax, eax                                      ;177.6
        push      eax                                           ;177.6
        push      eax                                           ;177.6
        push      -2088435968                                   ;177.6
        push      eax                                           ;177.6
        push      OFFSET FLAT: __STRLITPACK_47                  ;177.6
        call      _for_stop_core                                ;177.6
                                ; LOE
.B4.165:                        ; Preds .B4.108                 ; Infreq
        add       esp, 48                                       ;177.6
        jmp       .B4.52        ; Prob 100%                     ;177.6
                                ; LOE
.B4.109:                        ; Preds .B4.78                  ; Infreq
        mov       ebx, 1                                        ;
        jmp       .B4.82        ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx
.B4.110:                        ; Preds .B4.93                  ; Infreq
        mov       ebx, 1                                        ;
        jmp       .B4.97        ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx
.B4.111:                        ; Preds .B4.86                  ; Infreq
        mov       ebx, 1                                        ;
        jmp       .B4.90        ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx
.B4.112:                        ; Preds .B4.102                 ; Infreq
        mov       edx, ebx                                      ;212.1
        mov       eax, ebx                                      ;212.1
        shr       edx, 1                                        ;212.1
        and       eax, 1                                        ;212.1
        and       edx, 1                                        ;212.1
        add       eax, eax                                      ;212.1
        shl       edx, 2                                        ;212.1
        or        edx, eax                                      ;212.1
        or        edx, 262144                                   ;212.1
        push      edx                                           ;212.1
        push      DWORD PTR [68+esp]                            ;212.1
        call      _for_dealloc_allocatable                      ;212.1
                                ; LOE ebx
.B4.166:                        ; Preds .B4.112                 ; Infreq
        add       esp, 8                                        ;212.1
                                ; LOE ebx
.B4.113:                        ; Preds .B4.166                 ; Infreq
        and       ebx, -2                                       ;212.1
        mov       DWORD PTR [64+esp], 0                         ;212.1
        mov       DWORD PTR [76+esp], ebx                       ;212.1
        add       esp, 148                                      ;212.1
        pop       ebx                                           ;212.1
        pop       edi                                           ;212.1
        pop       esi                                           ;212.1
        mov       esp, ebp                                      ;212.1
        pop       ebp                                           ;212.1
        ret                                                     ;212.1
                                ; LOE
.B4.114:                        ; Preds .B4.71                  ; Infreq
        mov       ebx, 1                                        ;
        jmp       .B4.75        ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx
.B4.115:                        ; Preds .B4.65                  ; Infreq
        mov       ebx, 1                                        ;
        jmp       .B4.69        ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx
.B4.116:                        ; Preds .B4.59                  ; Infreq
        mov       ebx, 1                                        ;
        jmp       .B4.63        ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx
.B4.117:                        ; Preds .B4.43                  ; Infreq
        mov       edx, 1                                        ;
        jmp       .B4.47        ; Prob 100%                     ;
                                ; LOE eax edx ebx esi
.B4.118:                        ; Preds .B4.30                  ; Infreq
        mov       edx, 1                                        ;
        jmp       .B4.34        ; Prob 100%                     ;
                                ; LOE eax edx ebx esi
.B4.119:                        ; Preds .B4.17                  ; Infreq
        mov       edx, 1                                        ;
        jmp       .B4.21        ; Prob 100%                     ;
                                ; LOE eax edx ebx esi
.B4.120:                        ; Preds .B4.1                   ; Infreq
        mov       eax, DWORD PTR [12+ebp]                       ;202.4
        xor       edx, edx                                      ;202.4
        lea       ebx, DWORD PTR [24+esp]                       ;202.4
        push      12                                            ;202.4
        mov       ecx, DWORD PTR [eax]                          ;202.4
        test      ecx, ecx                                      ;202.4
        cmovl     ecx, edx                                      ;202.4
        push      ecx                                           ;202.4
        push      2                                             ;202.4
        push      ebx                                           ;202.4
        call      _for_check_mult_overflow                      ;202.4
                                ; LOE eax
.B4.121:                        ; Preds .B4.120                 ; Infreq
        mov       edx, DWORD PTR [8+ebp]                        ;202.4
        and       eax, 1                                        ;202.4
        mov       ecx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY+32] ;202.4
        neg       ecx                                           ;202.4
        add       ecx, DWORD PTR [edx]                          ;202.4
        imul      ebx, ecx, 76                                  ;202.4
        mov       esi, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY] ;202.4
        shl       eax, 4                                        ;202.4
        or        eax, 262145                                   ;202.4
        push      eax                                           ;202.4
        lea       edi, DWORD PTR [36+ebx+esi]                   ;202.4
        push      edi                                           ;202.4
        push      DWORD PTR [48+esp]                            ;202.4
        call      _for_allocate                                 ;202.4
                                ; LOE eax
.B4.168:                        ; Preds .B4.121                 ; Infreq
        add       esp, 28                                       ;202.4
        mov       ebx, eax                                      ;202.4
                                ; LOE ebx
.B4.122:                        ; Preds .B4.168                 ; Infreq
        test      ebx, ebx                                      ;202.4
        je        .B4.125       ; Prob 50%                      ;202.4
                                ; LOE ebx
.B4.123:                        ; Preds .B4.122                 ; Infreq
        mov       DWORD PTR [32+esp], 0                         ;204.7
        lea       edx, DWORD PTR [32+esp]                       ;204.7
        mov       DWORD PTR [16+esp], 36                        ;204.7
        lea       eax, DWORD PTR [16+esp]                       ;204.7
        mov       DWORD PTR [20+esp], OFFSET FLAT: __STRLITPACK_16 ;204.7
        push      32                                            ;204.7
        push      eax                                           ;204.7
        push      OFFSET FLAT: __STRLITPACK_50.0.4              ;204.7
        push      -2088435968                                   ;204.7
        push      911                                           ;204.7
        push      edx                                           ;204.7
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VECTORERROR], ebx ;202.4
        call      _for_write_seq_lis                            ;204.7
                                ; LOE
.B4.124:                        ; Preds .B4.123                 ; Infreq
        push      32                                            ;205.4
        xor       eax, eax                                      ;205.4
        push      eax                                           ;205.4
        push      eax                                           ;205.4
        push      -2088435968                                   ;205.4
        push      eax                                           ;205.4
        push      OFFSET FLAT: __STRLITPACK_51                  ;205.4
        call      _for_stop_core                                ;205.4
                                ; LOE
.B4.169:                        ; Preds .B4.124                 ; Infreq
        add       esp, 48                                       ;205.4
        jmp       .B4.127       ; Prob 100%                     ;205.4
                                ; LOE
.B4.125:                        ; Preds .B4.122                 ; Infreq
        mov       esi, DWORD PTR [8+ebp]                        ;202.13
        xor       edi, edi                                      ;202.4
        mov       ecx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY] ;202.4
        mov       eax, DWORD PTR [esi]                          ;202.13
        mov       esi, 12                                       ;202.4
        sub       eax, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY+32] ;202.4
        imul      edx, eax, 76                                  ;202.4
        mov       eax, 1                                        ;202.4
        mov       DWORD PTR [52+ecx+edx], eax                   ;202.4
        mov       DWORD PTR [68+ecx+edx], eax                   ;202.4
        mov       eax, DWORD PTR [12+ebp]                       ;202.4
        mov       DWORD PTR [48+ecx+edx], 5                     ;202.4
        mov       DWORD PTR [40+ecx+edx], esi                   ;202.4
        mov       eax, DWORD PTR [eax]                          ;202.4
        test      eax, eax                                      ;202.4
        mov       DWORD PTR [44+ecx+edx], edi                   ;202.4
        cmovl     eax, edi                                      ;202.4
        mov       DWORD PTR [60+ecx+edx], eax                   ;202.4
        mov       DWORD PTR [64+ecx+edx], esi                   ;202.4
        lea       edx, DWORD PTR [8+esp]                        ;202.4
        push      esi                                           ;202.4
        push      eax                                           ;202.4
        push      2                                             ;202.4
        push      edx                                           ;202.4
        call      _for_check_mult_overflow                      ;202.4
                                ; LOE ebx
.B4.170:                        ; Preds .B4.125                 ; Infreq
        add       esp, 16                                       ;202.4
                                ; LOE ebx
.B4.126:                        ; Preds .B4.170                 ; Infreq
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VECTORERROR], ebx ;202.4
                                ; LOE
.B4.127:                        ; Preds .B4.169 .B4.126         ; Infreq
        mov       eax, DWORD PTR [12+ebp]                       ;208.4
        mov       esi, DWORD PTR [eax]                          ;208.4
        test      esi, esi                                      ;208.4
        jle       .B4.148       ; Prob 50%                      ;208.4
                                ; LOE esi
.B4.128:                        ; Preds .B4.127                 ; Infreq
        mov       edi, esi                                      ;208.4
        shr       edi, 31                                       ;208.4
        add       edi, esi                                      ;208.4
        sar       edi, 1                                        ;208.4
        mov       edx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY] ;208.4
        test      edi, edi                                      ;208.4
        mov       eax, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY+32] ;208.4
        jbe       .B4.152       ; Prob 10%                      ;208.4
                                ; LOE eax edx esi edi
.B4.129:                        ; Preds .B4.128                 ; Infreq
        imul      ecx, eax, -76                                 ;
        xor       ebx, ebx                                      ;
        mov       DWORD PTR [12+esp], esi                       ;
        add       ecx, edx                                      ;
        mov       DWORD PTR [28+esp], edi                       ;
        xor       edi, edi                                      ;
        mov       DWORD PTR [esp], eax                          ;
        mov       DWORD PTR [4+esp], edx                        ;
        mov       esi, DWORD PTR [8+ebp]                        ;
                                ; LOE ecx ebx esi edi
.B4.130:                        ; Preds .B4.130 .B4.129         ; Infreq
        imul      eax, DWORD PTR [esi], 76                      ;208.4
        lea       edx, DWORD PTR [1+ebx+ebx]                    ;208.4
        sub       edx, DWORD PTR [68+eax+ecx]                   ;208.4
        imul      edx, DWORD PTR [64+eax+ecx]                   ;208.4
        mov       eax, DWORD PTR [36+eax+ecx]                   ;208.4
        mov       DWORD PTR [eax+edx], edi                      ;208.4
        lea       eax, DWORD PTR [2+ebx+ebx]                    ;208.4
        imul      edx, DWORD PTR [esi], 76                      ;208.4
        inc       ebx                                           ;208.4
        sub       eax, DWORD PTR [68+edx+ecx]                   ;208.4
        imul      eax, DWORD PTR [64+edx+ecx]                   ;208.4
        mov       edx, DWORD PTR [36+edx+ecx]                   ;208.4
        cmp       ebx, DWORD PTR [28+esp]                       ;208.4
        mov       DWORD PTR [edx+eax], edi                      ;208.4
        jb        .B4.130       ; Prob 64%                      ;208.4
                                ; LOE ecx ebx esi edi
.B4.131:                        ; Preds .B4.130                 ; Infreq
        mov       eax, DWORD PTR [esp]                          ;
        lea       ecx, DWORD PTR [1+ebx+ebx]                    ;208.4
        mov       edx, DWORD PTR [4+esp]                        ;
        mov       esi, DWORD PTR [12+esp]                       ;
                                ; LOE eax edx ecx esi
.B4.132:                        ; Preds .B4.131 .B4.152         ; Infreq
        lea       ebx, DWORD PTR [-1+ecx]                       ;208.4
        cmp       esi, ebx                                      ;208.4
        jbe       .B4.134       ; Prob 10%                      ;208.4
                                ; LOE eax edx ecx
.B4.133:                        ; Preds .B4.132                 ; Infreq
        mov       ebx, DWORD PTR [8+ebp]                        ;208.4
        mov       esi, eax                                      ;208.4
        neg       esi                                           ;208.4
        add       esi, DWORD PTR [ebx]                          ;208.4
        imul      edi, esi, 76                                  ;208.4
        sub       ecx, DWORD PTR [68+edx+edi]                   ;208.4
        imul      ecx, DWORD PTR [64+edx+edi]                   ;208.4
        mov       ebx, DWORD PTR [36+edx+edi]                   ;208.4
        mov       DWORD PTR [ebx+ecx], 0                        ;208.4
                                ; LOE eax edx
.B4.134:                        ; Preds .B4.132 .B4.133         ; Infreq
        mov       ecx, DWORD PTR [12+ebp]                       ;209.4
        mov       esi, DWORD PTR [ecx]                          ;209.4
        test      esi, esi                                      ;209.4
        jle       .B4.148       ; Prob 50%                      ;209.4
                                ; LOE eax edx esi
.B4.135:                        ; Preds .B4.134                 ; Infreq
        mov       edi, esi                                      ;209.4
        shr       edi, 31                                       ;209.4
        add       edi, esi                                      ;209.4
        sar       edi, 1                                        ;209.4
        test      edi, edi                                      ;209.4
        jbe       .B4.151       ; Prob 10%                      ;209.4
                                ; LOE eax edx esi edi
.B4.136:                        ; Preds .B4.135                 ; Infreq
        imul      ecx, eax, -76                                 ;
        xor       ebx, ebx                                      ;
        mov       DWORD PTR [12+esp], esi                       ;
        add       ecx, edx                                      ;
        mov       DWORD PTR [28+esp], edi                       ;
        xor       edi, edi                                      ;
        mov       DWORD PTR [esp], eax                          ;
        mov       DWORD PTR [4+esp], edx                        ;
        mov       esi, DWORD PTR [8+ebp]                        ;
                                ; LOE ecx ebx esi edi
.B4.137:                        ; Preds .B4.137 .B4.136         ; Infreq
        imul      eax, DWORD PTR [esi], 76                      ;209.4
        lea       edx, DWORD PTR [1+ebx+ebx]                    ;209.4
        sub       edx, DWORD PTR [68+eax+ecx]                   ;209.4
        imul      edx, DWORD PTR [64+eax+ecx]                   ;209.4
        mov       eax, DWORD PTR [36+eax+ecx]                   ;209.4
        mov       DWORD PTR [4+eax+edx], edi                    ;209.4
        lea       eax, DWORD PTR [2+ebx+ebx]                    ;209.4
        imul      edx, DWORD PTR [esi], 76                      ;209.4
        inc       ebx                                           ;209.4
        sub       eax, DWORD PTR [68+edx+ecx]                   ;209.4
        imul      eax, DWORD PTR [64+edx+ecx]                   ;209.4
        mov       edx, DWORD PTR [36+edx+ecx]                   ;209.4
        cmp       ebx, DWORD PTR [28+esp]                       ;209.4
        mov       DWORD PTR [4+edx+eax], edi                    ;209.4
        jb        .B4.137       ; Prob 64%                      ;209.4
                                ; LOE ecx ebx esi edi
.B4.138:                        ; Preds .B4.137                 ; Infreq
        mov       esi, DWORD PTR [12+esp]                       ;
        lea       ecx, DWORD PTR [1+ebx+ebx]                    ;209.4
        mov       eax, DWORD PTR [esp]                          ;
        mov       edx, DWORD PTR [4+esp]                        ;
                                ; LOE eax edx ecx esi
.B4.139:                        ; Preds .B4.138 .B4.151         ; Infreq
        lea       ebx, DWORD PTR [-1+ecx]                       ;209.4
        cmp       esi, ebx                                      ;209.4
        jbe       .B4.141       ; Prob 10%                      ;209.4
                                ; LOE eax edx ecx
.B4.140:                        ; Preds .B4.139                 ; Infreq
        mov       ebx, DWORD PTR [8+ebp]                        ;209.4
        mov       esi, eax                                      ;209.4
        neg       esi                                           ;209.4
        add       esi, DWORD PTR [ebx]                          ;209.4
        imul      edi, esi, 76                                  ;209.4
        sub       ecx, DWORD PTR [68+edx+edi]                   ;209.4
        imul      ecx, DWORD PTR [64+edx+edi]                   ;209.4
        mov       ebx, DWORD PTR [36+edx+edi]                   ;209.4
        mov       DWORD PTR [4+ebx+ecx], 0                      ;209.4
                                ; LOE eax edx
.B4.141:                        ; Preds .B4.139 .B4.140         ; Infreq
        mov       ecx, DWORD PTR [12+ebp]                       ;210.4
        mov       esi, DWORD PTR [ecx]                          ;210.4
        test      esi, esi                                      ;210.4
        jle       .B4.148       ; Prob 10%                      ;210.4
                                ; LOE eax edx esi
.B4.142:                        ; Preds .B4.141                 ; Infreq
        mov       edi, esi                                      ;210.4
        shr       edi, 31                                       ;210.4
        add       edi, esi                                      ;210.4
        sar       edi, 1                                        ;210.4
        test      edi, edi                                      ;210.4
        jbe       .B4.150       ; Prob 0%                       ;210.4
                                ; LOE eax edx esi edi
.B4.143:                        ; Preds .B4.142                 ; Infreq
        imul      ecx, eax, -76                                 ;
        xor       ebx, ebx                                      ;
        mov       DWORD PTR [28+esp], edi                       ;
        add       ecx, edx                                      ;
        mov       DWORD PTR [12+esp], esi                       ;
        xor       esi, esi                                      ;
        mov       DWORD PTR [esp], eax                          ;
        mov       DWORD PTR [4+esp], edx                        ;
        mov       edi, DWORD PTR [8+ebp]                        ;
                                ; LOE ecx ebx esi edi
.B4.144:                        ; Preds .B4.144 .B4.143         ; Infreq
        imul      eax, DWORD PTR [edi], 76                      ;210.4
        lea       edx, DWORD PTR [1+ebx+ebx]                    ;210.4
        sub       edx, DWORD PTR [68+eax+ecx]                   ;210.4
        imul      edx, DWORD PTR [64+eax+ecx]                   ;210.4
        mov       eax, DWORD PTR [36+eax+ecx]                   ;210.4
        mov       DWORD PTR [8+eax+edx], esi                    ;210.4
        lea       eax, DWORD PTR [2+ebx+ebx]                    ;210.4
        imul      edx, DWORD PTR [edi], 76                      ;210.4
        inc       ebx                                           ;210.4
        sub       eax, DWORD PTR [68+edx+ecx]                   ;210.4
        imul      eax, DWORD PTR [64+edx+ecx]                   ;210.4
        mov       edx, DWORD PTR [36+edx+ecx]                   ;210.4
        cmp       ebx, DWORD PTR [28+esp]                       ;210.4
        mov       DWORD PTR [8+edx+eax], esi                    ;210.4
        jb        .B4.144       ; Prob 64%                      ;210.4
                                ; LOE ecx ebx esi edi
.B4.145:                        ; Preds .B4.144                 ; Infreq
        mov       esi, DWORD PTR [12+esp]                       ;
        lea       ecx, DWORD PTR [1+ebx+ebx]                    ;210.4
        mov       eax, DWORD PTR [esp]                          ;
        mov       edx, DWORD PTR [4+esp]                        ;
                                ; LOE eax edx ecx esi
.B4.146:                        ; Preds .B4.145 .B4.150         ; Infreq
        lea       ebx, DWORD PTR [-1+ecx]                       ;210.4
        cmp       esi, ebx                                      ;210.4
        jbe       .B4.148       ; Prob 0%                       ;210.4
                                ; LOE eax edx ecx
.B4.147:                        ; Preds .B4.146                 ; Infreq
        mov       ebx, DWORD PTR [8+ebp]                        ;210.4
        neg       eax                                           ;210.4
        add       eax, DWORD PTR [ebx]                          ;210.4
        imul      eax, eax, 76                                  ;210.4
        sub       ecx, DWORD PTR [68+edx+eax]                   ;210.4
        imul      ecx, DWORD PTR [64+edx+eax]                   ;210.4
        mov       edx, DWORD PTR [36+edx+eax]                   ;210.4
        mov       DWORD PTR [8+edx+ecx], 0                      ;210.4
                                ; LOE
.B4.148:                        ; Preds .B4.141 .B4.134 .B4.127 .B4.146 .B4.147
                                ;                               ; Infreq
        mov       ebx, DWORD PTR [76+esp]                       ;212.1
        jmp       .B4.102       ; Prob 100%                     ;212.1
                                ; LOE ebx
.B4.150:                        ; Preds .B4.142                 ; Infreq
        mov       ecx, 1                                        ;
        jmp       .B4.146       ; Prob 100%                     ;
                                ; LOE eax edx ecx esi
.B4.151:                        ; Preds .B4.135                 ; Infreq
        mov       ecx, 1                                        ;
        jmp       .B4.139       ; Prob 100%                     ;
                                ; LOE eax edx ecx esi
.B4.152:                        ; Preds .B4.128                 ; Infreq
        mov       ecx, 1                                        ;
        jmp       .B4.132       ; Prob 100%                     ;
        ALIGN     16
                                ; LOE eax edx ecx esi
; mark_end;
_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_A_SETUP ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_A_SETUP
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_INSERT
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_INSERT
_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_INSERT	PROC NEAR 
; parameter 1: 8 + ebp
; parameter 2: 12 + ebp
; parameter 3: 16 + ebp
; parameter 4: 20 + ebp
.B5.1:                          ; Preds .B5.0
        push      ebp                                           ;217.12
        mov       ebp, esp                                      ;217.12
        and       esp, -16                                      ;217.12
        push      esi                                           ;217.12
        push      edi                                           ;217.12
        push      ebx                                           ;217.12
        sub       esp, 276                                      ;217.12
        mov       edi, DWORD PTR [8+ebp]                        ;217.12
        imul      edx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY+32], 76 ;223.3
        imul      esi, DWORD PTR [edi], 76                      ;223.3
        mov       ebx, edx                                      ;223.3
        mov       ecx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY] ;223.3
        add       esi, ecx                                      ;223.3
        neg       ebx                                           ;223.3
        mov       eax, DWORD PTR [16+ebp]                       ;217.12
        mov       eax, DWORD PTR [eax]                          ;224.3
        cmp       eax, 1                                        ;224.13
        movsx     ebx, WORD PTR [72+ebx+esi]                    ;223.14
        je        .B5.9         ; Prob 16%                      ;224.13
                                ; LOE eax edx ecx ebx edi
.B5.2:                          ; Preds .B5.1 .B5.174
        cmp       eax, 2                                        ;229.3
        je        .B5.7         ; Prob 25%                      ;229.3
                                ; LOE eax edi
.B5.3:                          ; Preds .B5.2
        cmp       eax, 3                                        ;229.3
        jne       .B5.5         ; Prob 67%                      ;229.3
                                ; LOE eax edi
.B5.4:                          ; Preds .B5.3
        mov       eax, DWORD PTR [20+ebp]                       ;234.44
        movss     xmm3, DWORD PTR [_2il0floatpacket.21]         ;234.44
        movss     xmm0, DWORD PTR [_2il0floatpacket.22]         ;234.44
        mulss     xmm3, DWORD PTR [eax]                         ;234.44
        andps     xmm0, xmm3                                    ;234.44
        pxor      xmm3, xmm0                                    ;234.44
        movss     xmm1, DWORD PTR [_2il0floatpacket.23]         ;234.44
        movaps    xmm2, xmm3                                    ;234.44
        movss     xmm4, DWORD PTR [_2il0floatpacket.24]         ;234.44
        cmpltss   xmm2, xmm1                                    ;234.44
        andps     xmm1, xmm2                                    ;234.44
        movaps    xmm2, xmm3                                    ;234.44
        movaps    xmm6, xmm4                                    ;234.44
        addss     xmm2, xmm1                                    ;234.44
        addss     xmm6, xmm4                                    ;234.44
        subss     xmm2, xmm1                                    ;234.44
        movaps    xmm7, xmm2                                    ;234.44
        mov       edx, DWORD PTR [edi]                          ;234.5
        subss     xmm7, xmm3                                    ;234.44
        movaps    xmm5, xmm7                                    ;234.44
        cmpless   xmm7, DWORD PTR [_2il0floatpacket.25]         ;234.44
        cmpnless  xmm5, xmm4                                    ;234.44
        andps     xmm5, xmm6                                    ;234.44
        andps     xmm7, xmm6                                    ;234.44
        sub       edx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY+32] ;234.5
        subss     xmm2, xmm5                                    ;234.44
        imul      esi, edx, 76                                  ;234.5
        addss     xmm2, xmm7                                    ;234.44
        mov       ecx, DWORD PTR [12+ebp]                       ;234.5
        orps      xmm2, xmm0                                    ;234.44
        mov       ebx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY] ;234.44
        cvtss2si  edi, xmm2                                     ;234.5
        movsx     eax, WORD PTR [ecx]                           ;234.5
        sub       eax, DWORD PTR [68+ebx+esi]                   ;234.5
        imul      eax, DWORD PTR [64+ebx+esi]                   ;234.5
        mov       ebx, DWORD PTR [36+ebx+esi]                   ;234.5
        mov       DWORD PTR [4+ebx+eax], edi                    ;234.5
        add       esp, 276                                      ;234.5
        pop       ebx                                           ;234.5
        pop       edi                                           ;234.5
        pop       esi                                           ;234.5
        mov       esp, ebp                                      ;234.5
        pop       ebp                                           ;234.5
        ret                                                     ;234.5
                                ; LOE
.B5.5:                          ; Preds .B5.3
        cmp       eax, 4                                        ;229.3
        jne       .B5.8         ; Prob 50%                      ;229.3
                                ; LOE edi
.B5.6:                          ; Preds .B5.5
        mov       eax, DWORD PTR [20+ebp]                       ;236.47
        movss     xmm3, DWORD PTR [_2il0floatpacket.21]         ;236.47
        movss     xmm0, DWORD PTR [_2il0floatpacket.22]         ;236.47
        mulss     xmm3, DWORD PTR [eax]                         ;236.47
        andps     xmm0, xmm3                                    ;236.47
        pxor      xmm3, xmm0                                    ;236.47
        movss     xmm1, DWORD PTR [_2il0floatpacket.23]         ;236.47
        movaps    xmm2, xmm3                                    ;236.47
        movss     xmm4, DWORD PTR [_2il0floatpacket.24]         ;236.47
        cmpltss   xmm2, xmm1                                    ;236.47
        andps     xmm1, xmm2                                    ;236.47
        movaps    xmm2, xmm3                                    ;236.47
        movaps    xmm6, xmm4                                    ;236.47
        addss     xmm2, xmm1                                    ;236.47
        addss     xmm6, xmm4                                    ;236.47
        subss     xmm2, xmm1                                    ;236.47
        movaps    xmm7, xmm2                                    ;236.47
        mov       edx, DWORD PTR [edi]                          ;236.5
        subss     xmm7, xmm3                                    ;236.47
        movaps    xmm5, xmm7                                    ;236.47
        cmpless   xmm7, DWORD PTR [_2il0floatpacket.25]         ;236.47
        cmpnless  xmm5, xmm4                                    ;236.47
        andps     xmm5, xmm6                                    ;236.47
        andps     xmm7, xmm6                                    ;236.47
        sub       edx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY+32] ;236.5
        subss     xmm2, xmm5                                    ;236.47
        imul      esi, edx, 76                                  ;236.5
        addss     xmm2, xmm7                                    ;236.47
        mov       ecx, DWORD PTR [12+ebp]                       ;236.5
        orps      xmm2, xmm0                                    ;236.47
        mov       ebx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY] ;236.47
        cvtss2si  edi, xmm2                                     ;236.5
        movsx     eax, WORD PTR [ecx]                           ;236.5
        sub       eax, DWORD PTR [68+ebx+esi]                   ;236.5
        imul      eax, DWORD PTR [64+ebx+esi]                   ;236.5
        mov       ebx, DWORD PTR [36+ebx+esi]                   ;236.5
        mov       DWORD PTR [8+ebx+eax], edi                    ;236.5
        add       esp, 276                                      ;236.5
        pop       ebx                                           ;236.5
        pop       edi                                           ;236.5
        pop       esi                                           ;236.5
        mov       esp, ebp                                      ;236.5
        pop       ebp                                           ;236.5
        ret                                                     ;236.5
                                ; LOE
.B5.7:                          ; Preds .B5.2
        mov       eax, DWORD PTR [20+ebp]                       ;232.44
        movss     xmm3, DWORD PTR [_2il0floatpacket.21]         ;232.44
        movss     xmm0, DWORD PTR [_2il0floatpacket.22]         ;232.44
        mulss     xmm3, DWORD PTR [eax]                         ;232.44
        andps     xmm0, xmm3                                    ;232.44
        pxor      xmm3, xmm0                                    ;232.44
        movss     xmm1, DWORD PTR [_2il0floatpacket.23]         ;232.44
        movaps    xmm2, xmm3                                    ;232.44
        movss     xmm4, DWORD PTR [_2il0floatpacket.24]         ;232.44
        cmpltss   xmm2, xmm1                                    ;232.44
        andps     xmm1, xmm2                                    ;232.44
        movaps    xmm2, xmm3                                    ;232.44
        movaps    xmm6, xmm4                                    ;232.44
        addss     xmm2, xmm1                                    ;232.44
        addss     xmm6, xmm4                                    ;232.44
        subss     xmm2, xmm1                                    ;232.44
        movaps    xmm7, xmm2                                    ;232.44
        mov       edx, DWORD PTR [edi]                          ;232.5
        subss     xmm7, xmm3                                    ;232.44
        movaps    xmm5, xmm7                                    ;232.44
        cmpless   xmm7, DWORD PTR [_2il0floatpacket.25]         ;232.44
        cmpnless  xmm5, xmm4                                    ;232.44
        andps     xmm5, xmm6                                    ;232.44
        andps     xmm7, xmm6                                    ;232.44
        sub       edx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY+32] ;232.5
        subss     xmm2, xmm5                                    ;232.44
        imul      esi, edx, 76                                  ;232.5
        addss     xmm2, xmm7                                    ;232.44
        mov       ecx, DWORD PTR [12+ebp]                       ;232.5
        orps      xmm2, xmm0                                    ;232.44
        mov       ebx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY] ;232.44
        cvtss2si  edi, xmm2                                     ;232.5
        movsx     eax, WORD PTR [ecx]                           ;232.5
        sub       eax, DWORD PTR [68+ebx+esi]                   ;232.5
        imul      eax, DWORD PTR [64+ebx+esi]                   ;232.5
        mov       ebx, DWORD PTR [36+ebx+esi]                   ;232.5
        mov       DWORD PTR [ebx+eax], edi                      ;232.5
                                ; LOE
.B5.8:                          ; Preds .B5.5 .B5.7
        add       esp, 276                                      ;238.1
        pop       ebx                                           ;238.1
        pop       edi                                           ;238.1
        pop       esi                                           ;238.1
        mov       esp, ebp                                      ;238.1
        pop       ebp                                           ;238.1
        ret                                                     ;238.1
                                ; LOE
.B5.9:                          ; Preds .B5.1                   ; Infreq
        mov       eax, DWORD PTR [12+ebp]                       ;224.22
        movsx     eax, WORD PTR [eax]                           ;224.22
        cmp       eax, ebx                                      ;224.30
        jle       .B5.175       ; Prob 50%                      ;224.30
                                ; LOE eax edx ecx ebx edi
.B5.10:                         ; Preds .B5.9                   ; Infreq
        mov       edx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_V_INCREASESIZE] ;225.13
        add       edx, eax                                      ;225.3
        mov       eax, DWORD PTR [.T254_.0.3]                   ;226.11
        test      ebx, ebx                                      ;226.11
        mov       DWORD PTR [252+esp], edx                      ;225.3
        mov       DWORD PTR [112+esp], eax                      ;226.11
        mov       ecx, DWORD PTR [.T254_.0.3+4]                 ;226.11
        mov       esi, DWORD PTR [.T254_.0.3+8]                 ;226.11
        mov       eax, DWORD PTR [.T254_.0.3+12]                ;226.11
        mov       edx, DWORD PTR [.T254_.0.3+16]                ;226.11
        mov       DWORD PTR [116+esp], ecx                      ;226.11
        mov       DWORD PTR [120+esp], esi                      ;226.11
        mov       DWORD PTR [124+esp], eax                      ;226.11
        mov       DWORD PTR [128+esp], edx                      ;226.11
        mov       ecx, DWORD PTR [.T254_.0.3+20]                ;226.11
        mov       esi, DWORD PTR [.T254_.0.3+24]                ;226.11
        mov       eax, DWORD PTR [.T254_.0.3+28]                ;226.11
        mov       edx, DWORD PTR [.T254_.0.3+32]                ;226.11
        mov       DWORD PTR [132+esp], ecx                      ;226.11
        mov       DWORD PTR [136+esp], esi                      ;226.11
        mov       DWORD PTR [140+esp], eax                      ;226.11
        mov       DWORD PTR [144+esp], edx                      ;226.11
        jle       .B5.234       ; Prob 16%                      ;226.11
                                ; LOE ebx edi
.B5.11:                         ; Preds .B5.10                  ; Infreq
        mov       eax, 0                                        ;226.11
        mov       edx, ebx                                      ;226.11
        cmovl     edx, eax                                      ;226.11
        lea       ecx, DWORD PTR [232+esp]                      ;226.11
        push      4                                             ;226.11
        push      edx                                           ;226.11
        push      2                                             ;226.11
        push      ecx                                           ;226.11
        call      _for_check_mult_overflow                      ;226.11
                                ; LOE eax ebx edi
.B5.12:                         ; Preds .B5.11                  ; Infreq
        mov       edx, DWORD PTR [140+esp]                      ;226.11
        and       eax, 1                                        ;226.11
        and       edx, 1                                        ;226.11
        lea       ecx, DWORD PTR [128+esp]                      ;226.11
        add       edx, edx                                      ;226.11
        shl       eax, 4                                        ;226.11
        or        edx, 1                                        ;226.11
        or        edx, eax                                      ;226.11
        or        edx, 262144                                   ;226.11
        push      edx                                           ;226.11
        push      ecx                                           ;226.11
        push      DWORD PTR [256+esp]                           ;226.11
        call      _for_alloc_allocatable                        ;226.11
                                ; LOE eax ebx edi
.B5.254:                        ; Preds .B5.12                  ; Infreq
        add       esp, 28                                       ;226.11
        mov       esi, eax                                      ;226.11
                                ; LOE ebx esi edi
.B5.13:                         ; Preds .B5.254                 ; Infreq
        test      esi, esi                                      ;226.11
        je        .B5.16        ; Prob 50%                      ;226.11
                                ; LOE ebx esi edi
.B5.14:                         ; Preds .B5.13                  ; Infreq
        mov       DWORD PTR [48+esp], 0                         ;226.11
        lea       edx, DWORD PTR [48+esp]                       ;226.11
        mov       DWORD PTR [200+esp], 25                       ;226.11
        lea       eax, DWORD PTR [200+esp]                      ;226.11
        mov       DWORD PTR [204+esp], OFFSET FLAT: __STRLITPACK_30 ;226.11
        push      32                                            ;226.11
        push      eax                                           ;226.11
        push      OFFSET FLAT: __STRLITPACK_36.0.3              ;226.11
        push      -2088435968                                   ;226.11
        push      911                                           ;226.11
        push      edx                                           ;226.11
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VECTORERROR], esi ;226.11
        call      _for_write_seq_lis                            ;226.11
                                ; LOE ebx edi
.B5.15:                         ; Preds .B5.14                  ; Infreq
        push      32                                            ;226.11
        xor       eax, eax                                      ;226.11
        push      eax                                           ;226.11
        push      eax                                           ;226.11
        push      -2088435968                                   ;226.11
        push      eax                                           ;226.11
        push      OFFSET FLAT: __STRLITPACK_37                  ;226.11
        call      _for_stop_core                                ;226.11
                                ; LOE ebx edi
.B5.255:                        ; Preds .B5.15                  ; Infreq
        add       esp, 48                                       ;226.11
        jmp       .B5.18        ; Prob 100%                     ;226.11
                                ; LOE ebx edi
.B5.16:                         ; Preds .B5.13                  ; Infreq
        mov       DWORD PTR [esp], esi                          ;
        mov       ecx, 1                                        ;226.11
        mov       esi, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY+32] ;226.11
        xor       eax, eax                                      ;226.11
        neg       esi                                           ;226.11
        mov       edx, 4                                        ;226.11
        add       esi, DWORD PTR [edi]                          ;226.11
        imul      esi, esi, 76                                  ;226.11
        mov       DWORD PTR [128+esp], ecx                      ;226.11
        mov       DWORD PTR [144+esp], ecx                      ;226.11
        mov       ecx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY] ;226.11
        mov       DWORD PTR [124+esp], 133                      ;226.11
        mov       DWORD PTR [116+esp], edx                      ;226.11
        movsx     ecx, WORD PTR [72+ecx+esi]                    ;226.11
        test      ecx, ecx                                      ;226.11
        mov       DWORD PTR [120+esp], eax                      ;226.11
        cmovl     ecx, eax                                      ;226.11
        lea       eax, DWORD PTR [188+esp]                      ;226.11
        mov       DWORD PTR [136+esp], ecx                      ;226.11
        mov       DWORD PTR [140+esp], edx                      ;226.11
        push      edx                                           ;226.11
        push      ecx                                           ;226.11
        push      2                                             ;226.11
        push      eax                                           ;226.11
        mov       esi, DWORD PTR [16+esp]                       ;226.11
        call      _for_check_mult_overflow                      ;226.11
                                ; LOE ebx esi edi
.B5.256:                        ; Preds .B5.16                  ; Infreq
        add       esp, 16                                       ;226.11
                                ; LOE ebx esi edi
.B5.17:                         ; Preds .B5.256                 ; Infreq
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VECTORERROR], esi ;226.11
                                ; LOE ebx edi
.B5.18:                         ; Preds .B5.255 .B5.17          ; Infreq
        mov       eax, DWORD PTR [edi]                          ;226.11
        sub       eax, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY+32] ;226.11
        imul      ecx, eax, 76                                  ;226.11
        mov       edx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY] ;226.11
        mov       DWORD PTR [32+esp], edx                       ;226.11
        mov       DWORD PTR [36+esp], ecx                       ;226.11
        movsx     edx, WORD PTR [72+edx+ecx]                    ;226.11
        test      edx, edx                                      ;226.11
        jle       .B5.46        ; Prob 0%                       ;226.11
                                ; LOE edx ecx ebx edi cl ch
.B5.19:                         ; Preds .B5.18                  ; Infreq
        mov       eax, DWORD PTR [112+esp]                      ;226.11
        mov       DWORD PTR [20+esp], eax                       ;226.11
        mov       esi, DWORD PTR [32+esp]                       ;226.11
        mov       eax, ecx                                      ;226.11
        mov       ecx, DWORD PTR [144+esp]                      ;226.11
        mov       DWORD PTR [24+esp], ecx                       ;226.11
        mov       ecx, DWORD PTR [28+esi+eax]                   ;226.11
        cmp       ecx, 4                                        ;226.11
        mov       DWORD PTR [28+esp], ecx                       ;226.11
        jne       .B5.40        ; Prob 50%                      ;226.11
                                ; LOE edx ebx esi edi
.B5.20:                         ; Preds .B5.19                  ; Infreq
        cmp       edx, 4                                        ;226.11
        jl        .B5.176       ; Prob 10%                      ;226.11
                                ; LOE edx ebx edi
.B5.21:                         ; Preds .B5.20                  ; Infreq
        mov       eax, DWORD PTR [24+esp]                       ;226.11
        lea       ecx, DWORD PTR [eax*4]                        ;226.11
        neg       ecx                                           ;226.11
        add       ecx, DWORD PTR [20+esp]                       ;226.11
        mov       DWORD PTR [esp], ecx                          ;226.11
        lea       esi, DWORD PTR [4+ecx]                        ;226.11
        and       esi, 15                                       ;226.11
        mov       DWORD PTR [4+esp], esi                        ;226.11
        je        .B5.24        ; Prob 50%                      ;226.11
                                ; LOE edx ebx esi edi
.B5.22:                         ; Preds .B5.21                  ; Infreq
        test      BYTE PTR [4+esp], 3                           ;226.11
        jne       .B5.176       ; Prob 10%                      ;226.11
                                ; LOE edx ebx esi edi
.B5.23:                         ; Preds .B5.22                  ; Infreq
        mov       eax, esi                                      ;226.11
        neg       eax                                           ;226.11
        add       eax, 16                                       ;226.11
        shr       eax, 2                                        ;226.11
        mov       DWORD PTR [4+esp], eax                        ;226.11
                                ; LOE edx ebx edi
.B5.24:                         ; Preds .B5.23 .B5.21           ; Infreq
        mov       eax, DWORD PTR [4+esp]                        ;226.11
        lea       ecx, DWORD PTR [4+eax]                        ;226.11
        cmp       edx, ecx                                      ;226.11
        jl        .B5.176       ; Prob 10%                      ;226.11
                                ; LOE eax edx ebx edi al ah
.B5.25:                         ; Preds .B5.24                  ; Infreq
        mov       esi, edx                                      ;226.11
        sub       esi, eax                                      ;226.11
        and       esi, 3                                        ;226.11
        neg       esi                                           ;226.11
        add       esi, edx                                      ;226.11
        mov       DWORD PTR [12+esp], edx                       ;
        mov       edx, DWORD PTR [32+esp]                       ;226.11
        mov       ecx, DWORD PTR [36+esp]                       ;226.11
        mov       DWORD PTR [8+esp], esi                        ;226.11
        mov       esi, DWORD PTR [32+edx+ecx]                   ;226.11
        shl       esi, 2                                        ;
        neg       esi                                           ;
        add       esi, DWORD PTR [edx+ecx]                      ;
        mov       DWORD PTR [16+esp], esi                       ;
        test      eax, eax                                      ;226.11
        mov       edx, DWORD PTR [12+esp]                       ;226.11
        jbe       .B5.29        ; Prob 10%                      ;226.11
                                ; LOE edx ebx esi edi dl dh
.B5.26:                         ; Preds .B5.25                  ; Infreq
        mov       DWORD PTR [40+esp], ebx                       ;
        xor       eax, eax                                      ;
        mov       ebx, esi                                      ;
        mov       esi, DWORD PTR [4+esp]                        ;
        mov       edi, DWORD PTR [esp]                          ;
                                ; LOE eax edx ebx esi edi
.B5.27:                         ; Preds .B5.27 .B5.26           ; Infreq
        mov       ecx, DWORD PTR [4+ebx+eax*4]                  ;226.11
        mov       DWORD PTR [4+edi+eax*4], ecx                  ;226.11
        inc       eax                                           ;226.11
        cmp       eax, esi                                      ;226.11
        jb        .B5.27        ; Prob 82%                      ;226.11
                                ; LOE eax edx ebx esi edi
.B5.28:                         ; Preds .B5.27                  ; Infreq
        mov       DWORD PTR [4+esp], esi                        ;
        mov       ebx, DWORD PTR [40+esp]                       ;
        mov       edi, DWORD PTR [8+ebp]                        ;
                                ; LOE edx ebx edi
.B5.29:                         ; Preds .B5.25 .B5.28           ; Infreq
        mov       eax, DWORD PTR [16+esp]                       ;226.11
        mov       ecx, DWORD PTR [4+esp]                        ;226.11
        lea       esi, DWORD PTR [4+eax+ecx*4]                  ;226.11
        test      esi, 15                                       ;226.11
        je        .B5.33        ; Prob 60%                      ;226.11
                                ; LOE edx ebx edi
.B5.30:                         ; Preds .B5.29                  ; Infreq
        mov       eax, DWORD PTR [8+esp]                        ;
        mov       ecx, DWORD PTR [16+esp]                       ;
        mov       esi, DWORD PTR [4+esp]                        ;
        mov       edi, DWORD PTR [esp]                          ;
                                ; LOE eax edx ecx ebx esi edi
.B5.31:                         ; Preds .B5.31 .B5.30           ; Infreq
        movdqu    xmm0, XMMWORD PTR [4+ecx+esi*4]               ;226.11
        movdqa    XMMWORD PTR [4+edi+esi*4], xmm0               ;226.11
        add       esi, 4                                        ;226.11
        cmp       esi, eax                                      ;226.11
        jb        .B5.31        ; Prob 82%                      ;226.11
        jmp       .B5.35        ; Prob 100%                     ;226.11
                                ; LOE eax edx ecx ebx esi edi
.B5.33:                         ; Preds .B5.29                  ; Infreq
        mov       eax, DWORD PTR [8+esp]                        ;
        mov       ecx, DWORD PTR [16+esp]                       ;
        mov       esi, DWORD PTR [4+esp]                        ;
        mov       edi, DWORD PTR [esp]                          ;
                                ; LOE eax edx ecx ebx esi edi
.B5.34:                         ; Preds .B5.34 .B5.33           ; Infreq
        movdqa    xmm0, XMMWORD PTR [4+ecx+esi*4]               ;226.11
        movdqa    XMMWORD PTR [4+edi+esi*4], xmm0               ;226.11
        add       esi, 4                                        ;226.11
        cmp       esi, eax                                      ;226.11
        jb        .B5.34        ; Prob 82%                      ;226.11
                                ; LOE eax edx ecx ebx esi edi
.B5.35:                         ; Preds .B5.31 .B5.34           ; Infreq
        mov       DWORD PTR [8+esp], eax                        ;
        mov       edi, DWORD PTR [8+ebp]                        ;
                                ; LOE edx ebx edi
.B5.36:                         ; Preds .B5.35 .B5.176          ; Infreq
        cmp       edx, DWORD PTR [8+esp]                        ;226.11
        jbe       .B5.46        ; Prob 10%                      ;226.11
                                ; LOE edx ebx edi
.B5.37:                         ; Preds .B5.36                  ; Infreq
        mov       eax, DWORD PTR [32+esp]                       ;226.11
        mov       ecx, DWORD PTR [36+esp]                       ;226.11
        mov       edi, DWORD PTR [8+esp]                        ;
        mov       esi, DWORD PTR [32+eax+ecx]                   ;226.11
        shl       esi, 2                                        ;
        neg       esi                                           ;
        add       esi, DWORD PTR [eax+ecx]                      ;
        mov       eax, DWORD PTR [24+esp]                       ;
        shl       eax, 2                                        ;
        sub       DWORD PTR [20+esp], eax                       ;
        mov       ecx, DWORD PTR [20+esp]                       ;
                                ; LOE edx ecx ebx esi edi
.B5.38:                         ; Preds .B5.38 .B5.37           ; Infreq
        mov       eax, DWORD PTR [4+esi+edi*4]                  ;226.11
        mov       DWORD PTR [4+ecx+edi*4], eax                  ;226.11
        inc       edi                                           ;226.11
        cmp       edi, edx                                      ;226.11
        jb        .B5.38        ; Prob 82%                      ;226.11
                                ; LOE edx ecx ebx esi edi
.B5.39:                         ; Preds .B5.38                  ; Infreq
        mov       edi, DWORD PTR [8+ebp]                        ;
        jmp       .B5.46        ; Prob 100%                     ;
                                ; LOE ebx edi
.B5.40:                         ; Preds .B5.19                  ; Infreq
        mov       eax, edx                                      ;226.11
        shr       eax, 31                                       ;226.11
        add       eax, edx                                      ;226.11
        sar       eax, 1                                        ;226.11
        mov       DWORD PTR [16+esp], eax                       ;226.11
        test      eax, eax                                      ;226.11
        jbe       .B5.233       ; Prob 10%                      ;226.11
                                ; LOE edx ebx esi edi
.B5.41:                         ; Preds .B5.40                  ; Infreq
        mov       DWORD PTR [12+esp], edx                       ;
        mov       eax, esi                                      ;226.11
        mov       edx, DWORD PTR [36+esp]                       ;226.11
        mov       DWORD PTR [8+esp], 0                          ;
        mov       DWORD PTR [40+esp], ebx                       ;
        mov       ecx, DWORD PTR [32+eax+edx]                   ;226.11
        mov       esi, DWORD PTR [eax+edx]                      ;226.11
        mov       eax, DWORD PTR [24+esp]                       ;
        imul      ecx, DWORD PTR [28+esp]                       ;
        sub       esi, ecx                                      ;
        lea       edx, DWORD PTR [eax*4]                        ;
        neg       edx                                           ;
        add       edx, DWORD PTR [20+esp]                       ;
        mov       DWORD PTR [esp], esi                          ;
        mov       DWORD PTR [4+esp], edx                        ;
        mov       eax, DWORD PTR [8+esp]                        ;
        mov       esi, DWORD PTR [4+esp]                        ;
        mov       ecx, DWORD PTR [esp]                          ;
        mov       edx, DWORD PTR [16+esp]                       ;
        mov       ebx, DWORD PTR [28+esp]                       ;
                                ; LOE eax edx ecx ebx esi
.B5.42:                         ; Preds .B5.42 .B5.41           ; Infreq
        lea       edi, DWORD PTR [1+eax+eax]                    ;226.11
        imul      edi, ebx                                      ;226.11
        mov       edi, DWORD PTR [ecx+edi]                      ;226.11
        mov       DWORD PTR [4+esi+eax*8], edi                  ;226.11
        lea       edi, DWORD PTR [2+eax+eax]                    ;226.11
        imul      edi, ebx                                      ;226.11
        mov       edi, DWORD PTR [ecx+edi]                      ;226.11
        mov       DWORD PTR [8+esi+eax*8], edi                  ;226.11
        inc       eax                                           ;226.11
        cmp       eax, edx                                      ;226.11
        jb        .B5.42        ; Prob 64%                      ;226.11
                                ; LOE eax edx ecx ebx esi
.B5.43:                         ; Preds .B5.42                  ; Infreq
        mov       edx, DWORD PTR [12+esp]                       ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;226.11
        mov       ebx, DWORD PTR [40+esp]                       ;
        mov       edi, DWORD PTR [8+ebp]                        ;
        mov       DWORD PTR [esp], eax                          ;226.11
                                ; LOE edx ebx edi
.B5.44:                         ; Preds .B5.43 .B5.233          ; Infreq
        mov       eax, DWORD PTR [esp]                          ;226.11
        lea       ecx, DWORD PTR [-1+eax]                       ;226.11
        cmp       edx, ecx                                      ;226.11
        jbe       .B5.46        ; Prob 10%                      ;226.11
                                ; LOE eax ebx edi al ah
.B5.45:                         ; Preds .B5.44                  ; Infreq
        mov       DWORD PTR [40+esp], ebx                       ;
        mov       ebx, DWORD PTR [32+esp]                       ;226.11
        mov       esi, DWORD PTR [36+esp]                       ;226.11
        mov       ecx, eax                                      ;226.11
        mov       eax, DWORD PTR [24+esp]                       ;226.11
        mov       edx, DWORD PTR [32+ebx+esi]                   ;226.11
        neg       eax                                           ;226.11
        neg       edx                                           ;226.11
        add       eax, ecx                                      ;226.11
        add       edx, ecx                                      ;226.11
        imul      edx, DWORD PTR [28+esp]                       ;226.11
        mov       ebx, DWORD PTR [ebx+esi]                      ;226.11
        mov       ecx, DWORD PTR [20+esp]                       ;226.11
        mov       edx, DWORD PTR [ebx+edx]                      ;226.11
        mov       DWORD PTR [ecx+eax*4], edx                    ;226.11
        mov       ebx, DWORD PTR [40+esp]                       ;226.11
                                ; LOE ebx edi
.B5.46:                         ; Preds .B5.18 .B5.36 .B5.39 .B5.44 .B5.45
                                ;                               ; Infreq
        mov       eax, DWORD PTR [32+esp]                       ;226.11
        mov       edx, DWORD PTR [36+esp]                       ;226.11
        mov       ecx, DWORD PTR [12+eax+edx]                   ;226.11
        test      cl, 1                                         ;226.11
        mov       DWORD PTR [esp], ecx                          ;226.11
        je        .B5.49        ; Prob 60%                      ;226.11
                                ; LOE ecx ebx edi cl ch
.B5.47:                         ; Preds .B5.46                  ; Infreq
        mov       eax, ecx                                      ;226.11
        mov       edx, eax                                      ;226.11
        shr       edx, 1                                        ;226.11
        and       eax, 1                                        ;226.11
        and       edx, 1                                        ;226.11
        add       eax, eax                                      ;226.11
        shl       edx, 2                                        ;226.11
        or        edx, 1                                        ;226.11
        or        edx, eax                                      ;226.11
        or        edx, 262144                                   ;226.11
        push      edx                                           ;226.11
        mov       ecx, DWORD PTR [36+esp]                       ;226.11
        mov       esi, DWORD PTR [40+esp]                       ;226.11
        push      DWORD PTR [ecx+esi]                           ;226.11
        call      _for_dealloc_allocatable                      ;226.11
                                ; LOE eax ebx esi edi
.B5.257:                        ; Preds .B5.47                  ; Infreq
        add       esp, 8                                        ;226.11
                                ; LOE eax ebx esi edi
.B5.48:                         ; Preds .B5.257                 ; Infreq
        mov       ecx, DWORD PTR [32+esp]                       ;226.11
        mov       edx, DWORD PTR [esp]                          ;226.11
        and       edx, -2                                       ;226.11
        mov       DWORD PTR [ecx+esi], 0                        ;226.11
        test      eax, eax                                      ;226.11
        mov       DWORD PTR [12+ecx+esi], edx                   ;226.11
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VECTORERROR], eax ;226.11
        jne       .B5.179       ; Prob 5%                       ;226.11
                                ; LOE ebx edi
.B5.49:                         ; Preds .B5.273 .B5.48 .B5.46   ; Infreq
        mov       edx, DWORD PTR [252+esp]                      ;226.11
        xor       eax, eax                                      ;226.11
        test      edx, edx                                      ;226.11
        lea       ecx, DWORD PTR [236+esp]                      ;226.11
        cmovg     eax, edx                                      ;226.11
        mov       DWORD PTR [32+esp], eax                       ;226.11
        push      4                                             ;226.11
        push      eax                                           ;226.11
        push      2                                             ;226.11
        push      ecx                                           ;226.11
        call      _for_check_mult_overflow                      ;226.11
                                ; LOE eax ebx edi
.B5.50:                         ; Preds .B5.49                  ; Infreq
        mov       edx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY+32] ;226.11
        and       eax, 1                                        ;226.11
        neg       edx                                           ;226.11
        add       edx, DWORD PTR [edi]                          ;226.11
        imul      ecx, edx, 76                                  ;226.11
        shl       eax, 4                                        ;226.11
        or        eax, 262145                                   ;226.11
        add       ecx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY] ;226.11
        push      eax                                           ;226.11
        push      ecx                                           ;226.11
        push      DWORD PTR [260+esp]                           ;226.11
        call      _for_allocate                                 ;226.11
                                ; LOE eax ebx edi
.B5.259:                        ; Preds .B5.50                  ; Infreq
        add       esp, 28                                       ;226.11
        mov       esi, eax                                      ;226.11
                                ; LOE ebx esi edi
.B5.51:                         ; Preds .B5.259                 ; Infreq
        test      esi, esi                                      ;226.11
        je        .B5.55        ; Prob 50%                      ;226.11
                                ; LOE ebx esi edi
.B5.52:                         ; Preds .B5.51                  ; Infreq
        mov       DWORD PTR [48+esp], 0                         ;226.11
        lea       edx, DWORD PTR [48+esp]                       ;226.11
        mov       DWORD PTR [208+esp], 36                       ;226.11
        lea       eax, DWORD PTR [208+esp]                      ;226.11
        mov       DWORD PTR [212+esp], OFFSET FLAT: __STRLITPACK_26 ;226.11
        push      32                                            ;226.11
        push      eax                                           ;226.11
        push      OFFSET FLAT: __STRLITPACK_40.0.3              ;226.11
        push      -2088435968                                   ;226.11
        push      911                                           ;226.11
        push      edx                                           ;226.11
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VECTORERROR], esi ;226.11
        call      _for_write_seq_lis                            ;226.11
                                ; LOE ebx edi
.B5.53:                         ; Preds .B5.52                  ; Infreq
        push      32                                            ;226.11
        xor       eax, eax                                      ;226.11
        push      eax                                           ;226.11
        push      eax                                           ;226.11
        push      -2088435968                                   ;226.11
        push      eax                                           ;226.11
        push      OFFSET FLAT: __STRLITPACK_41                  ;226.11
        call      _for_stop_core                                ;226.11
                                ; LOE ebx edi
.B5.260:                        ; Preds .B5.53                  ; Infreq
        add       esp, 48                                       ;226.11
                                ; LOE ebx edi
.B5.54:                         ; Preds .B5.260                 ; Infreq
        mov       edx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY+32] ;226.11
        mov       eax, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY] ;226.11
        mov       DWORD PTR [36+esp], edx                       ;226.11
        jmp       .B5.57        ; Prob 100%                     ;226.11
                                ; LOE eax ebx edi
.B5.55:                         ; Preds .B5.51                  ; Infreq
        mov       ecx, DWORD PTR [edi]                          ;226.11
        mov       eax, 1                                        ;226.11
        sub       ecx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY+32] ;226.11
        mov       DWORD PTR [8+esp], esi                        ;
        imul      esi, ecx, 76                                  ;226.11
        mov       ecx, 4                                        ;226.11
        mov       edx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY] ;226.11
        mov       DWORD PTR [16+edx+esi], eax                   ;226.11
        mov       DWORD PTR [32+edx+esi], eax                   ;226.11
        mov       eax, DWORD PTR [32+esp]                       ;226.11
        mov       DWORD PTR [12+edx+esi], 5                     ;226.11
        mov       DWORD PTR [4+edx+esi], ecx                    ;226.11
        mov       DWORD PTR [8+edx+esi], 0                      ;226.11
        mov       DWORD PTR [24+edx+esi], eax                   ;226.11
        mov       DWORD PTR [28+edx+esi], ecx                   ;226.11
        lea       edx, DWORD PTR [192+esp]                      ;226.11
        push      ecx                                           ;226.11
        push      eax                                           ;226.11
        push      2                                             ;226.11
        push      edx                                           ;226.11
        mov       esi, DWORD PTR [24+esp]                       ;226.11
        call      _for_check_mult_overflow                      ;226.11
                                ; LOE ebx esi edi
.B5.261:                        ; Preds .B5.55                  ; Infreq
        add       esp, 16                                       ;226.11
                                ; LOE ebx esi edi
.B5.56:                         ; Preds .B5.261                 ; Infreq
        mov       edx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY+32] ;226.11
        mov       eax, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY] ;226.11
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VECTORERROR], esi ;226.11
        mov       DWORD PTR [36+esp], edx                       ;226.11
                                ; LOE eax ebx edi
.B5.57:                         ; Preds .B5.54 .B5.56           ; Infreq
        imul      esi, DWORD PTR [36+esp], -76                  ;
        mov       ecx, ebx                                      ;226.11
        shr       ecx, 31                                       ;226.11
        add       esi, eax                                      ;
        add       ecx, ebx                                      ;226.11
        mov       edx, DWORD PTR [112+esp]                      ;226.11
        sar       ecx, 1                                        ;226.11
        mov       DWORD PTR [16+esp], edx                       ;226.11
        test      ecx, ecx                                      ;226.11
        mov       edx, DWORD PTR [144+esp]                      ;226.11
        mov       DWORD PTR [24+esp], esi                       ;
        jbe       .B5.232       ; Prob 0%                       ;226.11
                                ; LOE eax edx ecx ebx edi
.B5.58:                         ; Preds .B5.57                  ; Infreq
        xor       esi, esi                                      ;
        mov       DWORD PTR [20+esp], esi                       ;
        lea       esi, DWORD PTR [edx*4]                        ;
        neg       esi                                           ;
        add       esi, DWORD PTR [16+esp]                       ;
        mov       DWORD PTR [44+esp], esi                       ;
        mov       DWORD PTR [8+esp], eax                        ;
        mov       DWORD PTR [28+esp], ecx                       ;
        mov       DWORD PTR [12+esp], edx                       ;
        mov       DWORD PTR [40+esp], ebx                       ;
        mov       esi, DWORD PTR [20+esp]                       ;
        ALIGN     16
                                ; LOE esi
.B5.59:                         ; Preds .B5.59 .B5.58           ; Infreq
        mov       ecx, DWORD PTR [8+ebp]                        ;226.11
        lea       ebx, DWORD PTR [1+esi+esi]                    ;226.11
        mov       edi, DWORD PTR [24+esp]                       ;226.11
        imul      eax, DWORD PTR [ecx], 76                      ;226.11
        sub       ebx, DWORD PTR [32+eax+edi]                   ;226.11
        imul      ebx, DWORD PTR [28+eax+edi]                   ;226.11
        mov       edx, DWORD PTR [eax+edi]                      ;226.11
        mov       eax, DWORD PTR [44+esp]                       ;226.11
        mov       eax, DWORD PTR [4+eax+esi*8]                  ;226.11
        mov       DWORD PTR [edx+ebx], eax                      ;226.11
        lea       eax, DWORD PTR [2+esi+esi]                    ;226.11
        imul      edx, DWORD PTR [ecx], 76                      ;226.11
        sub       eax, DWORD PTR [32+edx+edi]                   ;226.11
        imul      eax, DWORD PTR [28+edx+edi]                   ;226.11
        mov       ecx, DWORD PTR [44+esp]                       ;226.11
        mov       edi, DWORD PTR [edx+edi]                      ;226.11
        mov       ebx, DWORD PTR [8+ecx+esi*8]                  ;226.11
        inc       esi                                           ;226.11
        mov       DWORD PTR [edi+eax], ebx                      ;226.11
        cmp       esi, DWORD PTR [28+esp]                       ;226.11
        jb        .B5.59        ; Prob 64%                      ;226.11
                                ; LOE esi
.B5.60:                         ; Preds .B5.59                  ; Infreq
        mov       DWORD PTR [20+esp], esi                       ;
        mov       ecx, esi                                      ;226.11
        mov       eax, DWORD PTR [8+esp]                        ;
        mov       edx, DWORD PTR [12+esp]                       ;
        mov       ebx, DWORD PTR [40+esp]                       ;
        lea       ecx, DWORD PTR [1+ecx+ecx]                    ;226.11
        mov       edi, DWORD PTR [8+ebp]                        ;
                                ; LOE eax edx ecx ebx edi
.B5.61:                         ; Preds .B5.60 .B5.232          ; Infreq
        lea       esi, DWORD PTR [-1+ecx]                       ;226.11
        cmp       ebx, esi                                      ;226.11
        jbe       .B5.63        ; Prob 0%                       ;226.11
                                ; LOE eax edx ecx ebx edi
.B5.62:                         ; Preds .B5.61                  ; Infreq
        imul      esi, DWORD PTR [edi], 76                      ;226.11
        neg       edx                                           ;226.11
        mov       DWORD PTR [8+esp], eax                        ;
        add       edx, ecx                                      ;226.11
        mov       eax, DWORD PTR [24+esp]                       ;226.11
        sub       ecx, DWORD PTR [32+esi+eax]                   ;226.11
        imul      ecx, DWORD PTR [28+esi+eax]                   ;226.11
        mov       eax, DWORD PTR [esi+eax]                      ;226.11
        mov       esi, DWORD PTR [16+esp]                       ;226.11
        mov       edx, DWORD PTR [esi+edx*4]                    ;226.11
        mov       DWORD PTR [eax+ecx], edx                      ;226.11
        mov       eax, DWORD PTR [8+esp]                        ;226.11
                                ; LOE eax ebx edi
.B5.63:                         ; Preds .B5.62 .B5.61           ; Infreq
        mov       edx, DWORD PTR [252+esp]                      ;226.11
        sub       edx, ebx                                      ;226.11
        test      edx, edx                                      ;226.11
        jle       .B5.70        ; Prob 50%                      ;226.11
                                ; LOE eax edx ebx edi
.B5.64:                         ; Preds .B5.63                  ; Infreq
        mov       ecx, edx                                      ;226.11
        shr       ecx, 31                                       ;226.11
        add       ecx, edx                                      ;226.11
        sar       ecx, 1                                        ;226.11
        test      ecx, ecx                                      ;226.11
        jbe       .B5.181       ; Prob 11%                      ;226.11
                                ; LOE eax edx ecx ebx edi
.B5.65:                         ; Preds .B5.64                  ; Infreq
        mov       DWORD PTR [8+esp], eax                        ;
        xor       esi, esi                                      ;
        mov       DWORD PTR [16+esp], ecx                       ;
        mov       DWORD PTR [12+esp], edx                       ;
        mov       DWORD PTR [40+esp], ebx                       ;
        ALIGN     16
                                ; LOE esi edi
.B5.66:                         ; Preds .B5.66 .B5.65           ; Infreq
        mov       edx, DWORD PTR [40+esp]                       ;226.11
        xor       eax, eax                                      ;226.11
        mov       DWORD PTR [20+esp], esi                       ;
        mov       ebx, DWORD PTR [24+esp]                       ;226.11
        lea       edx, DWORD PTR [edx+esi*2]                    ;226.11
        imul      esi, DWORD PTR [edi], 76                      ;226.11
        lea       ecx, DWORD PTR [1+edx]                        ;226.11
        add       edx, 2                                        ;226.11
        sub       ecx, DWORD PTR [32+esi+ebx]                   ;226.11
        imul      ecx, DWORD PTR [28+esi+ebx]                   ;226.11
        mov       esi, DWORD PTR [esi+ebx]                      ;226.11
        mov       DWORD PTR [esi+ecx], eax                      ;226.11
        imul      ecx, DWORD PTR [edi], 76                      ;226.11
        sub       edx, DWORD PTR [32+ecx+ebx]                   ;226.11
        imul      edx, DWORD PTR [28+ecx+ebx]                   ;226.11
        mov       ebx, DWORD PTR [ecx+ebx]                      ;226.11
        mov       esi, DWORD PTR [20+esp]                       ;226.11
        inc       esi                                           ;226.11
        mov       DWORD PTR [ebx+edx], eax                      ;226.11
        cmp       esi, DWORD PTR [16+esp]                       ;226.11
        jb        .B5.66        ; Prob 64%                      ;226.11
                                ; LOE esi edi
.B5.67:                         ; Preds .B5.66                  ; Infreq
        mov       eax, DWORD PTR [8+esp]                        ;
        lea       ecx, DWORD PTR [1+esi+esi]                    ;226.11
        mov       edx, DWORD PTR [12+esp]                       ;
        mov       ebx, DWORD PTR [40+esp]                       ;
                                ; LOE eax edx ecx ebx edi
.B5.68:                         ; Preds .B5.67 .B5.181          ; Infreq
        lea       esi, DWORD PTR [-1+ecx]                       ;226.11
        cmp       edx, esi                                      ;226.11
        jbe       .B5.70        ; Prob 11%                      ;226.11
                                ; LOE eax ecx ebx edi
.B5.69:                         ; Preds .B5.68                  ; Infreq
        imul      edx, DWORD PTR [edi], 76                      ;226.11
        add       ecx, ebx                                      ;226.11
        mov       ebx, DWORD PTR [24+esp]                       ;226.11
        sub       ecx, DWORD PTR [32+edx+ebx]                   ;226.11
        imul      ecx, DWORD PTR [28+edx+ebx]                   ;226.11
        mov       esi, DWORD PTR [edx+ebx]                      ;226.11
        mov       DWORD PTR [esi+ecx], 0                        ;226.11
                                ; LOE eax edi
.B5.70:                         ; Preds .B5.63 .B5.68 .B5.69    ; Infreq
        mov       ebx, DWORD PTR [edi]                          ;226.11
        imul      ecx, ebx, 76                                  ;226.11
        mov       esi, DWORD PTR [24+esp]                       ;226.11
        mov       edx, DWORD PTR [252+esp]                      ;226.11
        mov       WORD PTR [72+ecx+esi], dx                     ;226.11
        mov       esi, DWORD PTR [124+esp]                      ;226.11
        test      esi, 1                                        ;226.11
        je        .B5.73        ; Prob 60%                      ;226.11
                                ; LOE eax ebx esi edi
.B5.71:                         ; Preds .B5.70                  ; Infreq
        mov       ecx, esi                                      ;226.11
        mov       edx, esi                                      ;226.11
        shr       ecx, 1                                        ;226.11
        and       edx, 1                                        ;226.11
        and       ecx, 1                                        ;226.11
        add       edx, edx                                      ;226.11
        shl       ecx, 2                                        ;226.11
        or        ecx, edx                                      ;226.11
        or        ecx, 262144                                   ;226.11
        push      ecx                                           ;226.11
        push      DWORD PTR [116+esp]                           ;226.11
        mov       DWORD PTR [16+esp], eax                       ;226.11
        call      _for_dealloc_allocatable                      ;226.11
                                ; LOE ebx esi edi
.B5.262:                        ; Preds .B5.71                  ; Infreq
        mov       eax, DWORD PTR [16+esp]                       ;
        add       esp, 8                                        ;226.11
                                ; LOE eax ebx esi edi al ah
.B5.72:                         ; Preds .B5.262                 ; Infreq
        and       esi, -2                                       ;226.11
        mov       DWORD PTR [112+esp], 0                        ;226.11
        mov       DWORD PTR [124+esp], esi                      ;226.11
                                ; LOE eax ebx esi edi
.B5.73:                         ; Preds .B5.72 .B5.70 .B5.248   ; Infreq
        test      esi, 1                                        ;226.11
        jne       .B5.230       ; Prob 4%                       ;226.11
                                ; LOE eax ebx esi edi
.B5.74:                         ; Preds .B5.73 .B5.231          ; Infreq
        sub       ebx, DWORD PTR [36+esp]                       ;227.11
        imul      ebx, ebx, 76                                  ;227.11
        mov       edx, DWORD PTR [.T328_.0.4]                   ;227.11
        mov       ecx, DWORD PTR [.T328_.0.4+4]                 ;227.11
        mov       esi, DWORD PTR [.T328_.0.4+8]                 ;227.11
        mov       DWORD PTR [152+esp], edx                      ;227.11
        mov       DWORD PTR [156+esp], ecx                      ;227.11
        mov       DWORD PTR [160+esp], esi                      ;227.11
        mov       edx, DWORD PTR [.T328_.0.4+12]                ;227.11
        mov       ecx, DWORD PTR [.T328_.0.4+16]                ;227.11
        mov       esi, DWORD PTR [.T328_.0.4+20]                ;227.11
        mov       DWORD PTR [164+esp], edx                      ;227.11
        mov       DWORD PTR [168+esp], ecx                      ;227.11
        mov       DWORD PTR [172+esp], esi                      ;227.11
        mov       edx, DWORD PTR [.T328_.0.4+24]                ;227.11
        mov       ecx, DWORD PTR [.T328_.0.4+28]                ;227.11
        mov       esi, DWORD PTR [.T328_.0.4+32]                ;227.11
        movsx     eax, WORD PTR [74+eax+ebx]                    ;227.11
        test      eax, eax                                      ;227.11
        mov       DWORD PTR [176+esp], edx                      ;227.11
        mov       DWORD PTR [180+esp], ecx                      ;227.11
        mov       DWORD PTR [184+esp], esi                      ;227.11
        mov       DWORD PTR [256+esp], eax                      ;227.11
        jle       .B5.199       ; Prob 16%                      ;227.11
                                ; LOE eax edi al ah
.B5.75:                         ; Preds .B5.74                  ; Infreq
        mov       edx, eax                                      ;227.11
        xor       ecx, ecx                                      ;227.11
        mov       eax, DWORD PTR [252+esp]                      ;227.11
        cmp       edx, eax                                      ;227.11
        lea       ebx, DWORD PTR [240+esp]                      ;227.11
        cmovge    eax, edx                                      ;227.11
        test      eax, eax                                      ;227.11
        cmovl     eax, ecx                                      ;227.11
        mov       DWORD PTR [244+esp], eax                      ;227.11
        push      12                                            ;227.11
        push      eax                                           ;227.11
        push      2                                             ;227.11
        push      ebx                                           ;227.11
        call      _for_check_mult_overflow                      ;227.11
                                ; LOE eax edi
.B5.76:                         ; Preds .B5.75                  ; Infreq
        mov       edx, DWORD PTR [180+esp]                      ;227.11
        and       eax, 1                                        ;227.11
        and       edx, 1                                        ;227.11
        lea       ecx, DWORD PTR [168+esp]                      ;227.11
        add       edx, edx                                      ;227.11
        shl       eax, 4                                        ;227.11
        or        edx, 1                                        ;227.11
        or        edx, eax                                      ;227.11
        or        edx, 262144                                   ;227.11
        push      edx                                           ;227.11
        push      ecx                                           ;227.11
        push      DWORD PTR [264+esp]                           ;227.11
        call      _for_alloc_allocatable                        ;227.11
                                ; LOE eax edi
.B5.264:                        ; Preds .B5.76                  ; Infreq
        add       esp, 28                                       ;227.11
        mov       ebx, eax                                      ;227.11
                                ; LOE ebx edi
.B5.77:                         ; Preds .B5.264                 ; Infreq
        test      ebx, ebx                                      ;227.11
        je        .B5.80        ; Prob 50%                      ;227.11
                                ; LOE ebx edi
.B5.78:                         ; Preds .B5.77                  ; Infreq
        mov       DWORD PTR [80+esp], 0                         ;227.11
        lea       edx, DWORD PTR [80+esp]                       ;227.11
        mov       DWORD PTR [216+esp], 25                       ;227.11
        lea       eax, DWORD PTR [216+esp]                      ;227.11
        mov       DWORD PTR [220+esp], OFFSET FLAT: __STRLITPACK_22 ;227.11
        push      32                                            ;227.11
        push      eax                                           ;227.11
        push      OFFSET FLAT: __STRLITPACK_44.0.4              ;227.11
        push      -2088435968                                   ;227.11
        push      911                                           ;227.11
        push      edx                                           ;227.11
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VECTORERROR], ebx ;227.11
        call      _for_write_seq_lis                            ;227.11
                                ; LOE edi
.B5.79:                         ; Preds .B5.78                  ; Infreq
        push      32                                            ;227.11
        xor       eax, eax                                      ;227.11
        push      eax                                           ;227.11
        push      eax                                           ;227.11
        push      -2088435968                                   ;227.11
        push      eax                                           ;227.11
        push      OFFSET FLAT: __STRLITPACK_45                  ;227.11
        call      _for_stop_core                                ;227.11
                                ; LOE edi
.B5.265:                        ; Preds .B5.79                  ; Infreq
        add       esp, 48                                       ;227.11
        jmp       .B5.82        ; Prob 100%                     ;227.11
                                ; LOE edi
.B5.80:                         ; Preds .B5.77                  ; Infreq
        mov       ecx, DWORD PTR [244+esp]                      ;227.11
        mov       edx, 12                                       ;227.11
        mov       eax, 1                                        ;227.11
        lea       esi, DWORD PTR [196+esp]                      ;227.11
        mov       DWORD PTR [164+esp], 133                      ;227.11
        mov       DWORD PTR [156+esp], edx                      ;227.11
        mov       DWORD PTR [168+esp], eax                      ;227.11
        mov       DWORD PTR [160+esp], 0                        ;227.11
        mov       DWORD PTR [184+esp], eax                      ;227.11
        mov       DWORD PTR [176+esp], ecx                      ;227.11
        mov       DWORD PTR [180+esp], edx                      ;227.11
        push      edx                                           ;227.11
        push      ecx                                           ;227.11
        push      2                                             ;227.11
        push      esi                                           ;227.11
        call      _for_check_mult_overflow                      ;227.11
                                ; LOE ebx edi
.B5.266:                        ; Preds .B5.80                  ; Infreq
        add       esp, 16                                       ;227.11
                                ; LOE ebx edi
.B5.81:                         ; Preds .B5.266                 ; Infreq
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VECTORERROR], ebx ;227.11
                                ; LOE edi
.B5.82:                         ; Preds .B5.265 .B5.81          ; Infreq
        mov       eax, DWORD PTR [edi]                          ;227.11
        sub       eax, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY+32] ;227.11
        imul      esi, eax, 76                                  ;227.11
        mov       ebx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY] ;227.11
        movsx     eax, WORD PTR [74+ebx+esi]                    ;227.11
        test      eax, eax                                      ;227.11
        jle       .B5.122       ; Prob 0%                       ;227.11
                                ; LOE eax ebx esi edi
.B5.83:                         ; Preds .B5.82                  ; Infreq
        mov       edx, DWORD PTR [152+esp]                      ;227.11
        mov       DWORD PTR [228+esp], edx                      ;227.11
        mov       ecx, DWORD PTR [184+esp]                      ;227.11
        mov       edx, DWORD PTR [64+ebx+esi]                   ;227.11
        cmp       edx, 4                                        ;227.11
        mov       DWORD PTR [224+esp], ecx                      ;227.11
        mov       DWORD PTR [248+esp], edx                      ;227.11
        jne       .B5.90        ; Prob 50%                      ;227.11
                                ; LOE eax ebx esi edi
.B5.84:                         ; Preds .B5.83                  ; Infreq
        mov       edx, eax                                      ;227.11
        shr       edx, 31                                       ;227.11
        add       edx, eax                                      ;227.11
        sar       edx, 1                                        ;227.11
        test      edx, edx                                      ;227.11
        jbe       .B5.182       ; Prob 10%                      ;227.11
                                ; LOE eax edx ebx esi edi
.B5.85:                         ; Preds .B5.84                  ; Infreq
        mov       ecx, DWORD PTR [68+ebx+esi]                   ;227.11
        shl       ecx, 2                                        ;
        neg       ecx                                           ;
        add       ecx, DWORD PTR [36+ebx+esi]                   ;
        mov       DWORD PTR [36+esp], ecx                       ;
        mov       ecx, DWORD PTR [224+esp]                      ;
        mov       DWORD PTR [44+esp], edx                       ;
        xor       edx, edx                                      ;
        mov       DWORD PTR [148+esp], edx                      ;
        mov       DWORD PTR [8+esp], eax                        ;
        lea       edx, DWORD PTR [ecx*8]                        ;
        mov       DWORD PTR [12+esp], ebx                       ;
        lea       edx, DWORD PTR [edx+ecx*4]                    ;
        neg       edx                                           ;
        add       edx, DWORD PTR [228+esp]                      ;
        mov       DWORD PTR [32+esp], edx                       ;
        mov       ecx, DWORD PTR [148+esp]                      ;
        mov       edx, DWORD PTR [44+esp]                       ;
        mov       ebx, DWORD PTR [32+esp]                       ;
        mov       eax, DWORD PTR [36+esp]                       ;
        mov       DWORD PTR [20+esp], esi                       ;
                                ; LOE eax edx ecx ebx
.B5.86:                         ; Preds .B5.86 .B5.85           ; Infreq
        mov       edi, DWORD PTR [4+eax+ecx*8]                  ;227.11
        lea       esi, DWORD PTR [ecx+ecx*2]                    ;227.11
        mov       DWORD PTR [12+ebx+esi*8], edi                 ;227.11
        mov       edi, DWORD PTR [8+eax+ecx*8]                  ;227.11
        inc       ecx                                           ;227.11
        mov       DWORD PTR [24+ebx+esi*8], edi                 ;227.11
        cmp       ecx, edx                                      ;227.11
        jb        .B5.86        ; Prob 64%                      ;227.11
                                ; LOE eax edx ecx ebx
.B5.87:                         ; Preds .B5.86                  ; Infreq
        mov       eax, DWORD PTR [8+esp]                        ;
        lea       edx, DWORD PTR [1+ecx+ecx]                    ;227.11
        mov       ebx, DWORD PTR [12+esp]                       ;
        mov       esi, DWORD PTR [20+esp]                       ;
        mov       edi, DWORD PTR [8+ebp]                        ;
                                ; LOE eax edx ebx esi edi
.B5.88:                         ; Preds .B5.87 .B5.182          ; Infreq
        lea       ecx, DWORD PTR [-1+edx]                       ;227.11
        cmp       eax, ecx                                      ;227.11
        jbe       .B5.96        ; Prob 10%                      ;227.11
                                ; LOE eax edx ebx esi edi
.B5.89:                         ; Preds .B5.88                  ; Infreq
        mov       ecx, DWORD PTR [224+esp]                      ;227.11
        neg       ecx                                           ;227.11
        add       ecx, edx                                      ;227.11
        mov       DWORD PTR [8+esp], eax                        ;
        mov       eax, DWORD PTR [68+ebx+esi]                   ;227.11
        neg       eax                                           ;227.11
        add       eax, edx                                      ;227.11
        lea       edx, DWORD PTR [ecx*8]                        ;227.11
        mov       DWORD PTR [12+esp], ebx                       ;
        lea       edx, DWORD PTR [edx+ecx*4]                    ;227.11
        mov       ebx, DWORD PTR [36+ebx+esi]                   ;227.11
        mov       ecx, DWORD PTR [228+esp]                      ;227.11
        mov       eax, DWORD PTR [ebx+eax*4]                    ;227.11
        mov       DWORD PTR [ecx+edx], eax                      ;227.11
        mov       eax, DWORD PTR [8+esp]                        ;227.11
        mov       ebx, DWORD PTR [12+esp]                       ;227.11
        jmp       .B5.96        ; Prob 100%                     ;227.11
                                ; LOE eax ebx esi edi
.B5.90:                         ; Preds .B5.83                  ; Infreq
        mov       ecx, eax                                      ;227.11
        shr       ecx, 31                                       ;227.11
        add       ecx, eax                                      ;227.11
        sar       ecx, 1                                        ;227.11
        test      ecx, ecx                                      ;227.11
        jbe       .B5.198       ; Prob 10%                      ;227.11
                                ; LOE eax ecx ebx esi edi
.B5.91:                         ; Preds .B5.90                  ; Infreq
        mov       DWORD PTR [148+esp], ecx                      ;
        xor       edx, edx                                      ;
        mov       ecx, DWORD PTR [68+ebx+esi]                   ;227.11
        imul      ecx, DWORD PTR [248+esp]                      ;
        mov       DWORD PTR [44+esp], edx                       ;
        mov       edx, DWORD PTR [36+ebx+esi]                   ;227.11
        sub       edx, ecx                                      ;
        mov       DWORD PTR [32+esp], edx                       ;
        mov       edx, DWORD PTR [224+esp]                      ;
        mov       DWORD PTR [8+esp], eax                        ;
        mov       DWORD PTR [12+esp], ebx                       ;
        mov       DWORD PTR [20+esp], esi                       ;
        lea       ecx, DWORD PTR [edx*8]                        ;
        lea       ecx, DWORD PTR [ecx+edx*4]                    ;
        mov       edx, DWORD PTR [44+esp]                       ;
        neg       ecx                                           ;
        add       ecx, DWORD PTR [228+esp]                      ;
        mov       DWORD PTR [36+esp], ecx                       ;
        mov       eax, DWORD PTR [36+esp]                       ;
        mov       ecx, DWORD PTR [32+esp]                       ;
                                ; LOE eax edx ecx
.B5.92:                         ; Preds .B5.92 .B5.91           ; Infreq
        mov       ebx, DWORD PTR [248+esp]                      ;227.11
        lea       edi, DWORD PTR [1+edx+edx]                    ;227.11
        imul      edi, ebx                                      ;227.11
        lea       esi, DWORD PTR [edx+edx*2]                    ;227.11
        mov       edi, DWORD PTR [ecx+edi]                      ;227.11
        mov       DWORD PTR [12+eax+esi*8], edi                 ;227.11
        lea       edi, DWORD PTR [2+edx+edx]                    ;227.11
        imul      edi, ebx                                      ;227.11
        inc       edx                                           ;227.11
        mov       ebx, DWORD PTR [ecx+edi]                      ;227.11
        mov       DWORD PTR [24+eax+esi*8], ebx                 ;227.11
        cmp       edx, DWORD PTR [148+esp]                      ;227.11
        jb        .B5.92        ; Prob 64%                      ;227.11
                                ; LOE eax edx ecx
.B5.93:                         ; Preds .B5.92                  ; Infreq
        mov       eax, DWORD PTR [8+esp]                        ;
        lea       edx, DWORD PTR [1+edx+edx]                    ;227.11
        mov       ebx, DWORD PTR [12+esp]                       ;
        mov       esi, DWORD PTR [20+esp]                       ;
        mov       edi, DWORD PTR [8+ebp]                        ;
                                ; LOE eax edx ebx esi edi
.B5.94:                         ; Preds .B5.93 .B5.198          ; Infreq
        lea       ecx, DWORD PTR [-1+edx]                       ;227.11
        cmp       eax, ecx                                      ;227.11
        jbe       .B5.96        ; Prob 10%                      ;227.11
                                ; LOE eax edx ebx esi edi
.B5.95:                         ; Preds .B5.94                  ; Infreq
        mov       ecx, DWORD PTR [224+esp]                      ;227.11
        neg       ecx                                           ;227.11
        mov       DWORD PTR [8+esp], eax                        ;
        add       ecx, edx                                      ;227.11
        mov       eax, DWORD PTR [68+ebx+esi]                   ;227.11
        neg       eax                                           ;227.11
        add       eax, edx                                      ;227.11
        imul      eax, DWORD PTR [248+esp]                      ;227.11
        lea       edx, DWORD PTR [ecx*8]                        ;227.11
        mov       DWORD PTR [12+esp], ebx                       ;
        lea       edx, DWORD PTR [edx+ecx*4]                    ;227.11
        mov       ebx, DWORD PTR [36+ebx+esi]                   ;227.11
        mov       ecx, DWORD PTR [228+esp]                      ;227.11
        mov       eax, DWORD PTR [ebx+eax]                      ;227.11
        mov       DWORD PTR [ecx+edx], eax                      ;227.11
        mov       eax, DWORD PTR [8+esp]                        ;227.11
        mov       ebx, DWORD PTR [12+esp]                       ;227.11
                                ; LOE eax ebx esi edi
.B5.96:                         ; Preds .B5.89 .B5.88 .B5.95 .B5.94 ; Infreq
        cmp       DWORD PTR [248+esp], 4                        ;227.11
        jne       .B5.103       ; Prob 50%                      ;227.11
                                ; LOE eax ebx esi edi
.B5.97:                         ; Preds .B5.96                  ; Infreq
        mov       edx, eax                                      ;227.11
        shr       edx, 31                                       ;227.11
        add       edx, eax                                      ;227.11
        sar       edx, 1                                        ;227.11
        test      edx, edx                                      ;227.11
        jbe       .B5.183       ; Prob 10%                      ;227.11
                                ; LOE eax edx ebx esi edi
.B5.98:                         ; Preds .B5.97                  ; Infreq
        mov       ecx, DWORD PTR [68+ebx+esi]                   ;227.11
        shl       ecx, 2                                        ;
        neg       ecx                                           ;
        add       ecx, DWORD PTR [36+ebx+esi]                   ;
        mov       DWORD PTR [36+esp], ecx                       ;
        mov       ecx, DWORD PTR [224+esp]                      ;
        mov       DWORD PTR [44+esp], edx                       ;
        xor       edx, edx                                      ;
        mov       DWORD PTR [148+esp], edx                      ;
        mov       DWORD PTR [8+esp], eax                        ;
        lea       edx, DWORD PTR [ecx*8]                        ;
        mov       DWORD PTR [12+esp], ebx                       ;
        lea       edx, DWORD PTR [edx+ecx*4]                    ;
        neg       edx                                           ;
        add       edx, DWORD PTR [228+esp]                      ;
        mov       DWORD PTR [32+esp], edx                       ;
        mov       ecx, DWORD PTR [148+esp]                      ;
        mov       edx, DWORD PTR [44+esp]                       ;
        mov       ebx, DWORD PTR [32+esp]                       ;
        mov       eax, DWORD PTR [36+esp]                       ;
        mov       DWORD PTR [20+esp], esi                       ;
                                ; LOE eax edx ecx ebx
.B5.99:                         ; Preds .B5.99 .B5.98           ; Infreq
        mov       edi, DWORD PTR [8+eax+ecx*8]                  ;227.11
        lea       esi, DWORD PTR [ecx+ecx*2]                    ;227.11
        mov       DWORD PTR [16+ebx+esi*8], edi                 ;227.11
        mov       edi, DWORD PTR [12+eax+ecx*8]                 ;227.11
        inc       ecx                                           ;227.11
        mov       DWORD PTR [28+ebx+esi*8], edi                 ;227.11
        cmp       ecx, edx                                      ;227.11
        jb        .B5.99        ; Prob 64%                      ;227.11
                                ; LOE eax edx ecx ebx
.B5.100:                        ; Preds .B5.99                  ; Infreq
        mov       eax, DWORD PTR [8+esp]                        ;
        lea       edx, DWORD PTR [1+ecx+ecx]                    ;227.11
        mov       ebx, DWORD PTR [12+esp]                       ;
        mov       esi, DWORD PTR [20+esp]                       ;
        mov       edi, DWORD PTR [8+ebp]                        ;
                                ; LOE eax edx ebx esi edi
.B5.101:                        ; Preds .B5.100 .B5.183         ; Infreq
        lea       ecx, DWORD PTR [-1+edx]                       ;227.11
        cmp       eax, ecx                                      ;227.11
        jbe       .B5.109       ; Prob 10%                      ;227.11
                                ; LOE eax edx ebx esi edi
.B5.102:                        ; Preds .B5.101                 ; Infreq
        mov       ecx, DWORD PTR [224+esp]                      ;227.11
        neg       ecx                                           ;227.11
        add       ecx, edx                                      ;227.11
        mov       DWORD PTR [8+esp], eax                        ;
        mov       eax, DWORD PTR [68+ebx+esi]                   ;227.11
        neg       eax                                           ;227.11
        add       eax, edx                                      ;227.11
        lea       edx, DWORD PTR [ecx*8]                        ;227.11
        mov       DWORD PTR [12+esp], ebx                       ;
        lea       edx, DWORD PTR [edx+ecx*4]                    ;227.11
        mov       ebx, DWORD PTR [36+ebx+esi]                   ;227.11
        mov       ecx, DWORD PTR [228+esp]                      ;227.11
        mov       eax, DWORD PTR [4+ebx+eax*4]                  ;227.11
        mov       DWORD PTR [4+ecx+edx], eax                    ;227.11
        mov       eax, DWORD PTR [8+esp]                        ;227.11
        mov       ebx, DWORD PTR [12+esp]                       ;227.11
        jmp       .B5.109       ; Prob 100%                     ;227.11
                                ; LOE eax ebx esi edi
.B5.103:                        ; Preds .B5.96                  ; Infreq
        mov       ecx, eax                                      ;227.11
        shr       ecx, 31                                       ;227.11
        add       ecx, eax                                      ;227.11
        sar       ecx, 1                                        ;227.11
        test      ecx, ecx                                      ;227.11
        jbe       .B5.197       ; Prob 10%                      ;227.11
                                ; LOE eax ecx ebx esi edi
.B5.104:                        ; Preds .B5.103                 ; Infreq
        mov       DWORD PTR [148+esp], ecx                      ;
        xor       edx, edx                                      ;
        mov       ecx, DWORD PTR [68+ebx+esi]                   ;227.11
        imul      ecx, DWORD PTR [248+esp]                      ;
        mov       DWORD PTR [44+esp], edx                       ;
        mov       edx, DWORD PTR [36+ebx+esi]                   ;227.11
        sub       edx, ecx                                      ;
        mov       DWORD PTR [32+esp], edx                       ;
        mov       edx, DWORD PTR [224+esp]                      ;
        mov       DWORD PTR [8+esp], eax                        ;
        mov       DWORD PTR [12+esp], ebx                       ;
        mov       DWORD PTR [20+esp], esi                       ;
        lea       ecx, DWORD PTR [edx*8]                        ;
        lea       ecx, DWORD PTR [ecx+edx*4]                    ;
        mov       edx, DWORD PTR [44+esp]                       ;
        neg       ecx                                           ;
        add       ecx, DWORD PTR [228+esp]                      ;
        mov       DWORD PTR [36+esp], ecx                       ;
        mov       eax, DWORD PTR [36+esp]                       ;
        mov       ecx, DWORD PTR [32+esp]                       ;
                                ; LOE eax edx ecx
.B5.105:                        ; Preds .B5.105 .B5.104         ; Infreq
        mov       ebx, DWORD PTR [248+esp]                      ;227.11
        lea       edi, DWORD PTR [1+edx+edx]                    ;227.11
        imul      edi, ebx                                      ;227.11
        lea       esi, DWORD PTR [edx+edx*2]                    ;227.11
        mov       edi, DWORD PTR [4+ecx+edi]                    ;227.11
        mov       DWORD PTR [16+eax+esi*8], edi                 ;227.11
        lea       edi, DWORD PTR [2+edx+edx]                    ;227.11
        imul      edi, ebx                                      ;227.11
        inc       edx                                           ;227.11
        mov       ebx, DWORD PTR [4+ecx+edi]                    ;227.11
        mov       DWORD PTR [28+eax+esi*8], ebx                 ;227.11
        cmp       edx, DWORD PTR [148+esp]                      ;227.11
        jb        .B5.105       ; Prob 64%                      ;227.11
                                ; LOE eax edx ecx
.B5.106:                        ; Preds .B5.105                 ; Infreq
        mov       eax, DWORD PTR [8+esp]                        ;
        lea       edx, DWORD PTR [1+edx+edx]                    ;227.11
        mov       ebx, DWORD PTR [12+esp]                       ;
        mov       esi, DWORD PTR [20+esp]                       ;
        mov       edi, DWORD PTR [8+ebp]                        ;
                                ; LOE eax edx ebx esi edi
.B5.107:                        ; Preds .B5.106 .B5.197         ; Infreq
        lea       ecx, DWORD PTR [-1+edx]                       ;227.11
        cmp       eax, ecx                                      ;227.11
        jbe       .B5.109       ; Prob 10%                      ;227.11
                                ; LOE eax edx ebx esi edi
.B5.108:                        ; Preds .B5.107                 ; Infreq
        mov       ecx, DWORD PTR [224+esp]                      ;227.11
        neg       ecx                                           ;227.11
        mov       DWORD PTR [8+esp], eax                        ;
        add       ecx, edx                                      ;227.11
        mov       eax, DWORD PTR [68+ebx+esi]                   ;227.11
        neg       eax                                           ;227.11
        add       eax, edx                                      ;227.11
        imul      eax, DWORD PTR [248+esp]                      ;227.11
        lea       edx, DWORD PTR [ecx*8]                        ;227.11
        mov       DWORD PTR [12+esp], ebx                       ;
        lea       edx, DWORD PTR [edx+ecx*4]                    ;227.11
        mov       ebx, DWORD PTR [36+ebx+esi]                   ;227.11
        mov       ecx, DWORD PTR [228+esp]                      ;227.11
        mov       eax, DWORD PTR [4+ebx+eax]                    ;227.11
        mov       DWORD PTR [4+ecx+edx], eax                    ;227.11
        mov       eax, DWORD PTR [8+esp]                        ;227.11
        mov       ebx, DWORD PTR [12+esp]                       ;227.11
                                ; LOE eax ebx esi edi
.B5.109:                        ; Preds .B5.102 .B5.101 .B5.108 .B5.107 ; Infreq
        cmp       DWORD PTR [248+esp], 4                        ;227.11
        jne       .B5.116       ; Prob 50%                      ;227.11
                                ; LOE eax ebx esi edi
.B5.110:                        ; Preds .B5.109                 ; Infreq
        mov       edx, eax                                      ;227.11
        shr       edx, 31                                       ;227.11
        add       edx, eax                                      ;227.11
        sar       edx, 1                                        ;227.11
        test      edx, edx                                      ;227.11
        jbe       .B5.184       ; Prob 10%                      ;227.11
                                ; LOE eax edx ebx esi edi
.B5.111:                        ; Preds .B5.110                 ; Infreq
        mov       ecx, DWORD PTR [68+ebx+esi]                   ;227.11
        shl       ecx, 2                                        ;
        neg       ecx                                           ;
        add       ecx, DWORD PTR [36+ebx+esi]                   ;
        mov       DWORD PTR [36+esp], ecx                       ;
        mov       ecx, DWORD PTR [224+esp]                      ;
        mov       DWORD PTR [44+esp], edx                       ;
        xor       edx, edx                                      ;
        mov       DWORD PTR [148+esp], edx                      ;
        mov       DWORD PTR [8+esp], eax                        ;
        lea       edx, DWORD PTR [ecx*8]                        ;
        mov       DWORD PTR [12+esp], ebx                       ;
        lea       edx, DWORD PTR [edx+ecx*4]                    ;
        neg       edx                                           ;
        add       edx, DWORD PTR [228+esp]                      ;
        mov       DWORD PTR [32+esp], edx                       ;
        mov       ecx, DWORD PTR [148+esp]                      ;
        mov       edx, DWORD PTR [44+esp]                       ;
        mov       ebx, DWORD PTR [32+esp]                       ;
        mov       eax, DWORD PTR [36+esp]                       ;
        mov       DWORD PTR [20+esp], esi                       ;
                                ; LOE eax edx ecx ebx
.B5.112:                        ; Preds .B5.112 .B5.111         ; Infreq
        mov       edi, DWORD PTR [12+eax+ecx*8]                 ;227.11
        lea       esi, DWORD PTR [ecx+ecx*2]                    ;227.11
        mov       DWORD PTR [20+ebx+esi*8], edi                 ;227.11
        mov       edi, DWORD PTR [16+eax+ecx*8]                 ;227.11
        inc       ecx                                           ;227.11
        mov       DWORD PTR [32+ebx+esi*8], edi                 ;227.11
        cmp       ecx, edx                                      ;227.11
        jb        .B5.112       ; Prob 64%                      ;227.11
                                ; LOE eax edx ecx ebx
.B5.113:                        ; Preds .B5.112                 ; Infreq
        mov       eax, DWORD PTR [8+esp]                        ;
        lea       edx, DWORD PTR [1+ecx+ecx]                    ;227.11
        mov       ebx, DWORD PTR [12+esp]                       ;
        mov       esi, DWORD PTR [20+esp]                       ;
        mov       edi, DWORD PTR [8+ebp]                        ;
                                ; LOE eax edx ebx esi edi
.B5.114:                        ; Preds .B5.113 .B5.184         ; Infreq
        lea       ecx, DWORD PTR [-1+edx]                       ;227.11
        cmp       eax, ecx                                      ;227.11
        jbe       .B5.122       ; Prob 10%                      ;227.11
                                ; LOE edx ebx esi edi
.B5.115:                        ; Preds .B5.114                 ; Infreq
        mov       ecx, DWORD PTR [224+esp]                      ;227.11
        neg       ecx                                           ;227.11
        add       ecx, edx                                      ;227.11
        mov       eax, DWORD PTR [68+ebx+esi]                   ;227.11
        neg       eax                                           ;227.11
        add       eax, edx                                      ;227.11
        mov       DWORD PTR [12+esp], ebx                       ;
        lea       edx, DWORD PTR [ecx*8]                        ;227.11
        mov       ebx, DWORD PTR [36+ebx+esi]                   ;227.11
        lea       edx, DWORD PTR [edx+ecx*4]                    ;227.11
        mov       ecx, DWORD PTR [228+esp]                      ;227.11
        mov       eax, DWORD PTR [8+ebx+eax*4]                  ;227.11
        mov       DWORD PTR [8+ecx+edx], eax                    ;227.11
        mov       ebx, DWORD PTR [12+esp]                       ;227.11
        jmp       .B5.122       ; Prob 100%                     ;227.11
                                ; LOE ebx esi edi
.B5.116:                        ; Preds .B5.109                 ; Infreq
        mov       ecx, eax                                      ;227.11
        shr       ecx, 31                                       ;227.11
        add       ecx, eax                                      ;227.11
        sar       ecx, 1                                        ;227.11
        test      ecx, ecx                                      ;227.11
        jbe       .B5.196       ; Prob 10%                      ;227.11
                                ; LOE eax ecx ebx esi edi
.B5.117:                        ; Preds .B5.116                 ; Infreq
        mov       DWORD PTR [148+esp], ecx                      ;
        xor       edx, edx                                      ;
        mov       ecx, DWORD PTR [68+ebx+esi]                   ;227.11
        imul      ecx, DWORD PTR [248+esp]                      ;
        mov       DWORD PTR [44+esp], edx                       ;
        mov       edx, DWORD PTR [36+ebx+esi]                   ;227.11
        sub       edx, ecx                                      ;
        mov       DWORD PTR [32+esp], edx                       ;
        mov       edx, DWORD PTR [224+esp]                      ;
        mov       DWORD PTR [8+esp], eax                        ;
        mov       DWORD PTR [12+esp], ebx                       ;
        mov       DWORD PTR [20+esp], esi                       ;
        lea       ecx, DWORD PTR [edx*8]                        ;
        lea       ecx, DWORD PTR [ecx+edx*4]                    ;
        mov       edx, DWORD PTR [44+esp]                       ;
        neg       ecx                                           ;
        add       ecx, DWORD PTR [228+esp]                      ;
        mov       DWORD PTR [36+esp], ecx                       ;
        mov       eax, DWORD PTR [36+esp]                       ;
        mov       ecx, DWORD PTR [32+esp]                       ;
                                ; LOE eax edx ecx
.B5.118:                        ; Preds .B5.118 .B5.117         ; Infreq
        mov       ebx, DWORD PTR [248+esp]                      ;227.11
        lea       edi, DWORD PTR [1+edx+edx]                    ;227.11
        imul      edi, ebx                                      ;227.11
        lea       esi, DWORD PTR [edx+edx*2]                    ;227.11
        mov       edi, DWORD PTR [8+ecx+edi]                    ;227.11
        mov       DWORD PTR [20+eax+esi*8], edi                 ;227.11
        lea       edi, DWORD PTR [2+edx+edx]                    ;227.11
        imul      edi, ebx                                      ;227.11
        inc       edx                                           ;227.11
        mov       ebx, DWORD PTR [8+ecx+edi]                    ;227.11
        mov       DWORD PTR [32+eax+esi*8], ebx                 ;227.11
        cmp       edx, DWORD PTR [148+esp]                      ;227.11
        jb        .B5.118       ; Prob 64%                      ;227.11
                                ; LOE eax edx ecx
.B5.119:                        ; Preds .B5.118                 ; Infreq
        mov       eax, DWORD PTR [8+esp]                        ;
        lea       edx, DWORD PTR [1+edx+edx]                    ;227.11
        mov       ebx, DWORD PTR [12+esp]                       ;
        mov       esi, DWORD PTR [20+esp]                       ;
        mov       edi, DWORD PTR [8+ebp]                        ;
                                ; LOE eax edx ebx esi edi
.B5.120:                        ; Preds .B5.119 .B5.196         ; Infreq
        lea       ecx, DWORD PTR [-1+edx]                       ;227.11
        cmp       eax, ecx                                      ;227.11
        jbe       .B5.122       ; Prob 10%                      ;227.11
                                ; LOE edx ebx esi edi
.B5.121:                        ; Preds .B5.120                 ; Infreq
        mov       ecx, DWORD PTR [224+esp]                      ;227.11
        neg       ecx                                           ;227.11
        mov       eax, DWORD PTR [68+ebx+esi]                   ;227.11
        add       ecx, edx                                      ;227.11
        neg       eax                                           ;227.11
        add       eax, edx                                      ;227.11
        imul      eax, DWORD PTR [248+esp]                      ;227.11
        mov       DWORD PTR [12+esp], ebx                       ;
        lea       edx, DWORD PTR [ecx*8]                        ;227.11
        mov       ebx, DWORD PTR [36+ebx+esi]                   ;227.11
        lea       edx, DWORD PTR [edx+ecx*4]                    ;227.11
        mov       ecx, DWORD PTR [228+esp]                      ;227.11
        mov       eax, DWORD PTR [8+ebx+eax]                    ;227.11
        mov       DWORD PTR [8+ecx+edx], eax                    ;227.11
        mov       ebx, DWORD PTR [12+esp]                       ;227.11
                                ; LOE ebx esi edi
.B5.122:                        ; Preds .B5.82 .B5.114 .B5.120 .B5.115 .B5.121
                                ;                               ; Infreq
        mov       edx, DWORD PTR [48+ebx+esi]                   ;227.11
        test      dl, 1                                         ;227.11
        je        .B5.125       ; Prob 60%                      ;227.11
                                ; LOE edx ebx esi edi
.B5.123:                        ; Preds .B5.122                 ; Infreq
        mov       ecx, edx                                      ;227.11
        mov       eax, edx                                      ;227.11
        shr       ecx, 1                                        ;227.11
        and       eax, 1                                        ;227.11
        and       ecx, 1                                        ;227.11
        add       eax, eax                                      ;227.11
        shl       ecx, 2                                        ;227.11
        or        ecx, 1                                        ;227.11
        or        ecx, eax                                      ;227.11
        or        ecx, 262144                                   ;227.11
        push      ecx                                           ;227.11
        push      DWORD PTR [36+ebx+esi]                        ;227.11
        mov       DWORD PTR [16+esp], edx                       ;227.11
        call      _for_dealloc_allocatable                      ;227.11
                                ; LOE eax ebx esi edi
.B5.267:                        ; Preds .B5.123                 ; Infreq
        mov       edx, DWORD PTR [16+esp]                       ;
        add       esp, 8                                        ;227.11
                                ; LOE eax edx ebx esi edi dl dh
.B5.124:                        ; Preds .B5.267                 ; Infreq
        and       edx, -2                                       ;227.11
        mov       DWORD PTR [36+ebx+esi], 0                     ;227.11
        test      eax, eax                                      ;227.11
        mov       DWORD PTR [48+ebx+esi], edx                   ;227.11
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VECTORERROR], eax ;227.11
        jne       .B5.185       ; Prob 5%                       ;227.11
                                ; LOE edi
.B5.125:                        ; Preds .B5.122 .B5.124 .B5.274 ; Infreq
        lea       eax, DWORD PTR [248+esp]                      ;227.11
        push      12                                            ;227.11
        push      DWORD PTR [248+esp]                           ;227.11
        push      2                                             ;227.11
        push      eax                                           ;227.11
        call      _for_check_mult_overflow                      ;227.11
                                ; LOE eax edi
.B5.126:                        ; Preds .B5.125                 ; Infreq
        mov       edx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY+32] ;227.11
        and       eax, 1                                        ;227.11
        neg       edx                                           ;227.11
        add       edx, DWORD PTR [edi]                          ;227.11
        imul      ecx, edx, 76                                  ;227.11
        mov       ebx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY] ;227.11
        shl       eax, 4                                        ;227.11
        or        eax, 262145                                   ;227.11
        push      eax                                           ;227.11
        lea       esi, DWORD PTR [36+ecx+ebx]                   ;227.11
        push      esi                                           ;227.11
        push      DWORD PTR [272+esp]                           ;227.11
        call      _for_allocate                                 ;227.11
                                ; LOE eax edi
.B5.269:                        ; Preds .B5.126                 ; Infreq
        add       esp, 28                                       ;227.11
        mov       ebx, eax                                      ;227.11
                                ; LOE ebx edi
.B5.127:                        ; Preds .B5.269                 ; Infreq
        test      ebx, ebx                                      ;227.11
        je        .B5.130       ; Prob 50%                      ;227.11
                                ; LOE ebx edi
.B5.128:                        ; Preds .B5.127                 ; Infreq
        mov       DWORD PTR [80+esp], 0                         ;227.11
        lea       edx, DWORD PTR [80+esp]                       ;227.11
        mov       DWORD PTR [224+esp], 36                       ;227.11
        lea       eax, DWORD PTR [224+esp]                      ;227.11
        mov       DWORD PTR [228+esp], OFFSET FLAT: __STRLITPACK_18 ;227.11
        push      32                                            ;227.11
        push      eax                                           ;227.11
        push      OFFSET FLAT: __STRLITPACK_48.0.4              ;227.11
        push      -2088435968                                   ;227.11
        push      911                                           ;227.11
        push      edx                                           ;227.11
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VECTORERROR], ebx ;227.11
        call      _for_write_seq_lis                            ;227.11
                                ; LOE edi
.B5.129:                        ; Preds .B5.128                 ; Infreq
        push      32                                            ;227.11
        xor       eax, eax                                      ;227.11
        push      eax                                           ;227.11
        push      eax                                           ;227.11
        push      -2088435968                                   ;227.11
        push      eax                                           ;227.11
        push      OFFSET FLAT: __STRLITPACK_49                  ;227.11
        call      _for_stop_core                                ;227.11
                                ; LOE edi
.B5.270:                        ; Preds .B5.129                 ; Infreq
        add       esp, 48                                       ;227.11
        jmp       .B5.132       ; Prob 100%                     ;227.11
                                ; LOE edi
.B5.130:                        ; Preds .B5.127                 ; Infreq
        mov       ecx, DWORD PTR [edi]                          ;227.11
        mov       eax, 1                                        ;227.11
        sub       ecx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY+32] ;227.11
        imul      esi, ecx, 76                                  ;227.11
        mov       ecx, 12                                       ;227.11
        mov       edx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY] ;227.11
        mov       DWORD PTR [52+edx+esi], eax                   ;227.11
        mov       DWORD PTR [68+edx+esi], eax                   ;227.11
        mov       eax, DWORD PTR [244+esp]                      ;227.11
        mov       DWORD PTR [48+edx+esi], 5                     ;227.11
        mov       DWORD PTR [40+edx+esi], ecx                   ;227.11
        mov       DWORD PTR [44+edx+esi], 0                     ;227.11
        mov       DWORD PTR [60+edx+esi], eax                   ;227.11
        mov       DWORD PTR [64+edx+esi], ecx                   ;227.11
        lea       edx, DWORD PTR [148+esp]                      ;227.11
        push      ecx                                           ;227.11
        push      eax                                           ;227.11
        push      2                                             ;227.11
        push      edx                                           ;227.11
        call      _for_check_mult_overflow                      ;227.11
                                ; LOE ebx edi
.B5.271:                        ; Preds .B5.130                 ; Infreq
        add       esp, 16                                       ;227.11
                                ; LOE ebx edi
.B5.131:                        ; Preds .B5.271                 ; Infreq
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VECTORERROR], ebx ;227.11
                                ; LOE edi
.B5.132:                        ; Preds .B5.270 .B5.131         ; Infreq
        mov       ecx, DWORD PTR [152+esp]                      ;227.11
        mov       esi, DWORD PTR [256+esp]                      ;227.11
        mov       DWORD PTR [44+esp], ecx                       ;227.11
        mov       ecx, esi                                      ;227.11
        shr       ecx, 31                                       ;227.11
        add       ecx, esi                                      ;227.11
        sar       ecx, 1                                        ;227.11
        mov       ebx, DWORD PTR [184+esp]                      ;227.11
        test      ecx, ecx                                      ;227.11
        mov       edx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY] ;227.11
        mov       eax, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY+32] ;227.11
        mov       DWORD PTR [36+esp], ebx                       ;227.11
        mov       DWORD PTR [244+esp], ecx                      ;227.11
        jbe       .B5.195       ; Prob 11%                      ;227.11
                                ; LOE eax edx edi
.B5.133:                        ; Preds .B5.132                 ; Infreq
        imul      ebx, eax, -76                                 ;
        xor       ecx, ecx                                      ;
        add       ebx, edx                                      ;
        mov       DWORD PTR [264+esp], ebx                      ;
        mov       ebx, DWORD PTR [36+esp]                       ;
        mov       DWORD PTR [20+esp], eax                       ;
        mov       DWORD PTR [32+esp], edx                       ;
        lea       esi, DWORD PTR [ebx*8]                        ;
        lea       ebx, DWORD PTR [esi+ebx*4]                    ;
        neg       ebx                                           ;
        add       ebx, DWORD PTR [44+esp]                       ;
        mov       DWORD PTR [260+esp], ebx                      ;
        ALIGN     16
                                ; LOE ecx
.B5.134:                        ; Preds .B5.134 .B5.133         ; Infreq
        mov       ebx, DWORD PTR [8+ebp]                        ;227.11
        lea       esi, DWORD PTR [1+ecx+ecx]                    ;227.11
        mov       edx, DWORD PTR [264+esp]                      ;227.11
        lea       edi, DWORD PTR [ecx+ecx*2]                    ;227.11
        imul      eax, DWORD PTR [ebx], 76                      ;227.11
        sub       esi, DWORD PTR [68+eax+edx]                   ;227.11
        imul      esi, DWORD PTR [64+eax+edx]                   ;227.11
        mov       edx, DWORD PTR [36+eax+edx]                   ;227.11
        mov       eax, DWORD PTR [260+esp]                      ;227.11
        mov       eax, DWORD PTR [12+eax+edi*8]                 ;227.11
        mov       DWORD PTR [edx+esi], eax                      ;227.11
        lea       eax, DWORD PTR [2+ecx+ecx]                    ;227.11
        imul      ebx, DWORD PTR [ebx], 76                      ;227.11
        inc       ecx                                           ;227.11
        mov       esi, DWORD PTR [264+esp]                      ;227.11
        mov       edx, DWORD PTR [260+esp]                      ;227.11
        sub       eax, DWORD PTR [68+ebx+esi]                   ;227.11
        imul      eax, DWORD PTR [64+ebx+esi]                   ;227.11
        mov       ebx, DWORD PTR [36+ebx+esi]                   ;227.11
        mov       edi, DWORD PTR [24+edx+edi*8]                 ;227.11
        cmp       ecx, DWORD PTR [244+esp]                      ;227.11
        mov       DWORD PTR [ebx+eax], edi                      ;227.11
        jb        .B5.134       ; Prob 64%                      ;227.11
                                ; LOE ecx
.B5.135:                        ; Preds .B5.134                 ; Infreq
        mov       eax, DWORD PTR [20+esp]                       ;
        lea       ecx, DWORD PTR [1+ecx+ecx]                    ;227.11
        mov       edx, DWORD PTR [32+esp]                       ;
        mov       edi, DWORD PTR [8+ebp]                        ;
                                ; LOE eax edx ecx edi
.B5.136:                        ; Preds .B5.135 .B5.195         ; Infreq
        lea       ebx, DWORD PTR [-1+ecx]                       ;227.11
        cmp       ebx, DWORD PTR [256+esp]                      ;227.11
        jae       .B5.138       ; Prob 11%                      ;227.11
                                ; LOE eax edx ecx edi
.B5.137:                        ; Preds .B5.136                 ; Infreq
        mov       DWORD PTR [20+esp], eax                       ;
        neg       eax                                           ;227.11
        add       eax, DWORD PTR [edi]                          ;227.11
        imul      esi, eax, 76                                  ;227.11
        mov       eax, DWORD PTR [36+esp]                       ;227.11
        neg       eax                                           ;227.11
        add       eax, ecx                                      ;227.11
        sub       ecx, DWORD PTR [68+edx+esi]                   ;227.11
        imul      ecx, DWORD PTR [64+edx+esi]                   ;227.11
        lea       ebx, DWORD PTR [eax*8]                        ;227.11
        lea       ebx, DWORD PTR [ebx+eax*4]                    ;227.11
        mov       eax, DWORD PTR [36+edx+esi]                   ;227.11
        mov       esi, DWORD PTR [44+esp]                       ;227.11
        mov       ebx, DWORD PTR [esi+ebx]                      ;227.11
        mov       DWORD PTR [eax+ecx], ebx                      ;227.11
        mov       eax, DWORD PTR [20+esp]                       ;227.11
                                ; LOE eax edx edi
.B5.138:                        ; Preds .B5.136 .B5.137         ; Infreq
        cmp       DWORD PTR [244+esp], 0                        ;227.11
        jbe       .B5.194       ; Prob 11%                      ;227.11
                                ; LOE eax edx edi
.B5.139:                        ; Preds .B5.138                 ; Infreq
        imul      ebx, eax, -76                                 ;
        xor       ecx, ecx                                      ;
        add       ebx, edx                                      ;
        mov       DWORD PTR [264+esp], ebx                      ;
        mov       ebx, DWORD PTR [36+esp]                       ;
        mov       DWORD PTR [20+esp], eax                       ;
        mov       DWORD PTR [32+esp], edx                       ;
        lea       esi, DWORD PTR [ebx*8]                        ;
        lea       ebx, DWORD PTR [esi+ebx*4]                    ;
        neg       ebx                                           ;
        add       ebx, DWORD PTR [44+esp]                       ;
        mov       DWORD PTR [260+esp], ebx                      ;
        ALIGN     16
                                ; LOE ecx
.B5.140:                        ; Preds .B5.140 .B5.139         ; Infreq
        mov       ebx, DWORD PTR [8+ebp]                        ;227.11
        lea       esi, DWORD PTR [1+ecx+ecx]                    ;227.11
        mov       edx, DWORD PTR [264+esp]                      ;227.11
        lea       edi, DWORD PTR [ecx+ecx*2]                    ;227.11
        imul      eax, DWORD PTR [ebx], 76                      ;227.11
        sub       esi, DWORD PTR [68+eax+edx]                   ;227.11
        imul      esi, DWORD PTR [64+eax+edx]                   ;227.11
        mov       edx, DWORD PTR [36+eax+edx]                   ;227.11
        mov       eax, DWORD PTR [260+esp]                      ;227.11
        mov       eax, DWORD PTR [16+eax+edi*8]                 ;227.11
        mov       DWORD PTR [4+edx+esi], eax                    ;227.11
        lea       eax, DWORD PTR [2+ecx+ecx]                    ;227.11
        imul      ebx, DWORD PTR [ebx], 76                      ;227.11
        inc       ecx                                           ;227.11
        mov       esi, DWORD PTR [264+esp]                      ;227.11
        mov       edx, DWORD PTR [260+esp]                      ;227.11
        sub       eax, DWORD PTR [68+ebx+esi]                   ;227.11
        imul      eax, DWORD PTR [64+ebx+esi]                   ;227.11
        mov       ebx, DWORD PTR [36+ebx+esi]                   ;227.11
        mov       edi, DWORD PTR [28+edx+edi*8]                 ;227.11
        cmp       ecx, DWORD PTR [244+esp]                      ;227.11
        mov       DWORD PTR [4+ebx+eax], edi                    ;227.11
        jb        .B5.140       ; Prob 64%                      ;227.11
                                ; LOE ecx
.B5.141:                        ; Preds .B5.140                 ; Infreq
        mov       eax, DWORD PTR [20+esp]                       ;
        lea       ecx, DWORD PTR [1+ecx+ecx]                    ;227.11
        mov       edx, DWORD PTR [32+esp]                       ;
        mov       edi, DWORD PTR [8+ebp]                        ;
                                ; LOE eax edx ecx edi
.B5.142:                        ; Preds .B5.141 .B5.194         ; Infreq
        lea       ebx, DWORD PTR [-1+ecx]                       ;227.11
        cmp       ebx, DWORD PTR [256+esp]                      ;227.11
        jae       .B5.144       ; Prob 11%                      ;227.11
                                ; LOE eax edx ecx edi
.B5.143:                        ; Preds .B5.142                 ; Infreq
        mov       DWORD PTR [20+esp], eax                       ;
        neg       eax                                           ;227.11
        add       eax, DWORD PTR [edi]                          ;227.11
        imul      esi, eax, 76                                  ;227.11
        mov       eax, DWORD PTR [36+esp]                       ;227.11
        neg       eax                                           ;227.11
        add       eax, ecx                                      ;227.11
        sub       ecx, DWORD PTR [68+edx+esi]                   ;227.11
        imul      ecx, DWORD PTR [64+edx+esi]                   ;227.11
        lea       ebx, DWORD PTR [eax*8]                        ;227.11
        lea       ebx, DWORD PTR [ebx+eax*4]                    ;227.11
        mov       eax, DWORD PTR [36+edx+esi]                   ;227.11
        mov       esi, DWORD PTR [44+esp]                       ;227.11
        mov       ebx, DWORD PTR [4+esi+ebx]                    ;227.11
        mov       DWORD PTR [4+eax+ecx], ebx                    ;227.11
        mov       eax, DWORD PTR [20+esp]                       ;227.11
                                ; LOE eax edx edi
.B5.144:                        ; Preds .B5.142 .B5.143         ; Infreq
        cmp       DWORD PTR [244+esp], 0                        ;227.11
        jbe       .B5.193       ; Prob 11%                      ;227.11
                                ; LOE eax edx edi
.B5.145:                        ; Preds .B5.144                 ; Infreq
        imul      ebx, eax, -76                                 ;
        xor       ecx, ecx                                      ;
        add       ebx, edx                                      ;
        mov       DWORD PTR [264+esp], ebx                      ;
        mov       ebx, DWORD PTR [36+esp]                       ;
        mov       DWORD PTR [20+esp], eax                       ;
        mov       DWORD PTR [32+esp], edx                       ;
        lea       esi, DWORD PTR [ebx*8]                        ;
        lea       ebx, DWORD PTR [esi+ebx*4]                    ;
        neg       ebx                                           ;
        add       ebx, DWORD PTR [44+esp]                       ;
        mov       DWORD PTR [260+esp], ebx                      ;
        ALIGN     16
                                ; LOE ecx
.B5.146:                        ; Preds .B5.146 .B5.145         ; Infreq
        mov       ebx, DWORD PTR [8+ebp]                        ;227.11
        lea       esi, DWORD PTR [1+ecx+ecx]                    ;227.11
        mov       edx, DWORD PTR [264+esp]                      ;227.11
        lea       edi, DWORD PTR [ecx+ecx*2]                    ;227.11
        imul      eax, DWORD PTR [ebx], 76                      ;227.11
        sub       esi, DWORD PTR [68+eax+edx]                   ;227.11
        imul      esi, DWORD PTR [64+eax+edx]                   ;227.11
        mov       edx, DWORD PTR [36+eax+edx]                   ;227.11
        mov       eax, DWORD PTR [260+esp]                      ;227.11
        mov       eax, DWORD PTR [20+eax+edi*8]                 ;227.11
        mov       DWORD PTR [8+edx+esi], eax                    ;227.11
        lea       eax, DWORD PTR [2+ecx+ecx]                    ;227.11
        imul      ebx, DWORD PTR [ebx], 76                      ;227.11
        inc       ecx                                           ;227.11
        mov       esi, DWORD PTR [264+esp]                      ;227.11
        mov       edx, DWORD PTR [260+esp]                      ;227.11
        sub       eax, DWORD PTR [68+ebx+esi]                   ;227.11
        imul      eax, DWORD PTR [64+ebx+esi]                   ;227.11
        mov       ebx, DWORD PTR [36+ebx+esi]                   ;227.11
        mov       edi, DWORD PTR [32+edx+edi*8]                 ;227.11
        cmp       ecx, DWORD PTR [244+esp]                      ;227.11
        mov       DWORD PTR [8+ebx+eax], edi                    ;227.11
        jb        .B5.146       ; Prob 64%                      ;227.11
                                ; LOE ecx
.B5.147:                        ; Preds .B5.146                 ; Infreq
        mov       eax, DWORD PTR [20+esp]                       ;
        lea       ecx, DWORD PTR [1+ecx+ecx]                    ;227.11
        mov       edx, DWORD PTR [32+esp]                       ;
        mov       edi, DWORD PTR [8+ebp]                        ;
                                ; LOE eax edx ecx edi
.B5.148:                        ; Preds .B5.147 .B5.193         ; Infreq
        lea       ebx, DWORD PTR [-1+ecx]                       ;227.11
        cmp       ebx, DWORD PTR [256+esp]                      ;227.11
        jae       .B5.150       ; Prob 11%                      ;227.11
                                ; LOE eax edx ecx edi
.B5.149:                        ; Preds .B5.148                 ; Infreq
        mov       DWORD PTR [20+esp], eax                       ;
        neg       eax                                           ;227.11
        add       eax, DWORD PTR [edi]                          ;227.11
        imul      esi, eax, 76                                  ;227.11
        mov       eax, DWORD PTR [36+esp]                       ;227.11
        neg       eax                                           ;227.11
        add       eax, ecx                                      ;227.11
        sub       ecx, DWORD PTR [68+edx+esi]                   ;227.11
        imul      ecx, DWORD PTR [64+edx+esi]                   ;227.11
        lea       ebx, DWORD PTR [eax*8]                        ;227.11
        lea       ebx, DWORD PTR [ebx+eax*4]                    ;227.11
        mov       eax, DWORD PTR [36+edx+esi]                   ;227.11
        mov       esi, DWORD PTR [44+esp]                       ;227.11
        mov       ebx, DWORD PTR [8+esi+ebx]                    ;227.11
        mov       DWORD PTR [8+eax+ecx], ebx                    ;227.11
        mov       eax, DWORD PTR [20+esp]                       ;227.11
                                ; LOE eax edx edi
.B5.150:                        ; Preds .B5.148 .B5.149         ; Infreq
        mov       ecx, DWORD PTR [252+esp]                      ;227.11
        sub       ecx, DWORD PTR [256+esp]                      ;227.11
        mov       DWORD PTR [252+esp], ecx                      ;227.11
        test      ecx, ecx                                      ;227.11
        jle       .B5.169       ; Prob 50%                      ;227.11
                                ; LOE eax edx ecx edi cl ch
.B5.151:                        ; Preds .B5.150                 ; Infreq
        mov       ebx, ecx                                      ;227.11
        shr       ebx, 31                                       ;227.11
        add       ebx, ecx                                      ;227.11
        sar       ebx, 1                                        ;227.11
        test      ebx, ebx                                      ;227.11
        jbe       .B5.189       ; Prob 11%                      ;227.11
                                ; LOE eax edx ebx edi
.B5.152:                        ; Preds .B5.151                 ; Infreq
        imul      ecx, eax, -76                                 ;
        xor       esi, esi                                      ;
        mov       DWORD PTR [36+esp], ebx                       ;
        add       ecx, edx                                      ;
        mov       DWORD PTR [20+esp], eax                       ;
        mov       DWORD PTR [32+esp], edx                       ;
        ALIGN     16
                                ; LOE ecx esi edi
.B5.153:                        ; Preds .B5.153 .B5.152         ; Infreq
        mov       ebx, DWORD PTR [256+esp]                      ;227.11
        xor       eax, eax                                      ;227.11
        mov       DWORD PTR [44+esp], esi                       ;
        lea       ebx, DWORD PTR [ebx+esi*2]                    ;227.11
        imul      esi, DWORD PTR [edi], 76                      ;227.11
        lea       edx, DWORD PTR [1+ebx]                        ;227.11
        add       ebx, 2                                        ;227.11
        sub       edx, DWORD PTR [68+esi+ecx]                   ;227.11
        imul      edx, DWORD PTR [64+esi+ecx]                   ;227.11
        mov       esi, DWORD PTR [36+esi+ecx]                   ;227.11
        mov       DWORD PTR [esi+edx], eax                      ;227.11
        imul      edx, DWORD PTR [edi], 76                      ;227.11
        sub       ebx, DWORD PTR [68+edx+ecx]                   ;227.11
        imul      ebx, DWORD PTR [64+edx+ecx]                   ;227.11
        mov       edx, DWORD PTR [36+edx+ecx]                   ;227.11
        mov       esi, DWORD PTR [44+esp]                       ;227.11
        inc       esi                                           ;227.11
        mov       DWORD PTR [edx+ebx], eax                      ;227.11
        cmp       esi, DWORD PTR [36+esp]                       ;227.11
        jb        .B5.153       ; Prob 64%                      ;227.11
                                ; LOE ecx esi edi
.B5.154:                        ; Preds .B5.153                 ; Infreq
        mov       eax, DWORD PTR [20+esp]                       ;
        lea       ecx, DWORD PTR [1+esi+esi]                    ;227.11
        mov       edx, DWORD PTR [32+esp]                       ;
                                ; LOE eax edx ecx edi
.B5.155:                        ; Preds .B5.154 .B5.189         ; Infreq
        lea       ebx, DWORD PTR [-1+ecx]                       ;227.11
        cmp       ebx, DWORD PTR [252+esp]                      ;227.11
        jae       .B5.157       ; Prob 11%                      ;227.11
                                ; LOE eax edx ecx edi
.B5.156:                        ; Preds .B5.155                 ; Infreq
        mov       ebx, eax                                      ;227.11
        neg       ebx                                           ;227.11
        add       ebx, DWORD PTR [edi]                          ;227.11
        imul      esi, ebx, 76                                  ;227.11
        add       ecx, DWORD PTR [256+esp]                      ;227.11
        sub       ecx, DWORD PTR [68+edx+esi]                   ;227.11
        imul      ecx, DWORD PTR [64+edx+esi]                   ;227.11
        mov       ebx, DWORD PTR [36+edx+esi]                   ;227.11
        mov       DWORD PTR [ebx+ecx], 0                        ;227.11
                                ; LOE eax edx edi
.B5.157:                        ; Preds .B5.156 .B5.155         ; Infreq
        mov       ecx, DWORD PTR [252+esp]                      ;227.11
        mov       ebx, ecx                                      ;227.11
        shr       ebx, 31                                       ;227.11
        add       ebx, ecx                                      ;227.11
        sar       ebx, 1                                        ;227.11
        test      ebx, ebx                                      ;227.11
        jbe       .B5.188       ; Prob 11%                      ;227.11
                                ; LOE eax edx ebx edi
.B5.158:                        ; Preds .B5.157                 ; Infreq
        imul      ecx, eax, -76                                 ;
        xor       esi, esi                                      ;
        mov       DWORD PTR [36+esp], ebx                       ;
        add       ecx, edx                                      ;
        mov       DWORD PTR [20+esp], eax                       ;
        mov       DWORD PTR [32+esp], edx                       ;
        ALIGN     16
                                ; LOE ecx esi edi
.B5.159:                        ; Preds .B5.159 .B5.158         ; Infreq
        mov       ebx, DWORD PTR [256+esp]                      ;227.11
        xor       eax, eax                                      ;227.11
        mov       DWORD PTR [44+esp], esi                       ;
        lea       ebx, DWORD PTR [ebx+esi*2]                    ;227.11
        imul      esi, DWORD PTR [edi], 76                      ;227.11
        lea       edx, DWORD PTR [1+ebx]                        ;227.11
        add       ebx, 2                                        ;227.11
        sub       edx, DWORD PTR [68+esi+ecx]                   ;227.11
        imul      edx, DWORD PTR [64+esi+ecx]                   ;227.11
        mov       esi, DWORD PTR [36+esi+ecx]                   ;227.11
        mov       DWORD PTR [4+esi+edx], eax                    ;227.11
        imul      edx, DWORD PTR [edi], 76                      ;227.11
        sub       ebx, DWORD PTR [68+edx+ecx]                   ;227.11
        imul      ebx, DWORD PTR [64+edx+ecx]                   ;227.11
        mov       edx, DWORD PTR [36+edx+ecx]                   ;227.11
        mov       esi, DWORD PTR [44+esp]                       ;227.11
        inc       esi                                           ;227.11
        mov       DWORD PTR [4+edx+ebx], eax                    ;227.11
        cmp       esi, DWORD PTR [36+esp]                       ;227.11
        jb        .B5.159       ; Prob 64%                      ;227.11
                                ; LOE ecx esi edi
.B5.160:                        ; Preds .B5.159                 ; Infreq
        mov       eax, DWORD PTR [20+esp]                       ;
        lea       ecx, DWORD PTR [1+esi+esi]                    ;227.11
        mov       edx, DWORD PTR [32+esp]                       ;
                                ; LOE eax edx ecx edi
.B5.161:                        ; Preds .B5.160 .B5.188         ; Infreq
        lea       ebx, DWORD PTR [-1+ecx]                       ;227.11
        cmp       ebx, DWORD PTR [252+esp]                      ;227.11
        jae       .B5.163       ; Prob 11%                      ;227.11
                                ; LOE eax edx ecx edi
.B5.162:                        ; Preds .B5.161                 ; Infreq
        mov       ebx, eax                                      ;227.11
        neg       ebx                                           ;227.11
        add       ebx, DWORD PTR [edi]                          ;227.11
        imul      esi, ebx, 76                                  ;227.11
        add       ecx, DWORD PTR [256+esp]                      ;227.11
        sub       ecx, DWORD PTR [68+edx+esi]                   ;227.11
        imul      ecx, DWORD PTR [64+edx+esi]                   ;227.11
        mov       ebx, DWORD PTR [36+edx+esi]                   ;227.11
        mov       DWORD PTR [4+ebx+ecx], 0                      ;227.11
                                ; LOE eax edx edi
.B5.163:                        ; Preds .B5.162 .B5.161         ; Infreq
        mov       ecx, DWORD PTR [252+esp]                      ;227.11
        mov       ebx, ecx                                      ;227.11
        shr       ebx, 31                                       ;227.11
        add       ebx, ecx                                      ;227.11
        sar       ebx, 1                                        ;227.11
        test      ebx, ebx                                      ;227.11
        jbe       .B5.187       ; Prob 11%                      ;227.11
                                ; LOE eax edx ebx edi
.B5.164:                        ; Preds .B5.163                 ; Infreq
        imul      ecx, eax, -76                                 ;
        xor       esi, esi                                      ;
        mov       DWORD PTR [36+esp], ebx                       ;
        add       ecx, edx                                      ;
        mov       DWORD PTR [20+esp], eax                       ;
        mov       DWORD PTR [32+esp], edx                       ;
        ALIGN     16
                                ; LOE ecx esi edi
.B5.165:                        ; Preds .B5.165 .B5.164         ; Infreq
        mov       ebx, DWORD PTR [256+esp]                      ;227.11
        xor       eax, eax                                      ;227.11
        mov       DWORD PTR [44+esp], esi                       ;
        lea       ebx, DWORD PTR [ebx+esi*2]                    ;227.11
        imul      esi, DWORD PTR [edi], 76                      ;227.11
        lea       edx, DWORD PTR [1+ebx]                        ;227.11
        add       ebx, 2                                        ;227.11
        sub       edx, DWORD PTR [68+esi+ecx]                   ;227.11
        imul      edx, DWORD PTR [64+esi+ecx]                   ;227.11
        mov       esi, DWORD PTR [36+esi+ecx]                   ;227.11
        mov       DWORD PTR [8+esi+edx], eax                    ;227.11
        imul      edx, DWORD PTR [edi], 76                      ;227.11
        sub       ebx, DWORD PTR [68+edx+ecx]                   ;227.11
        imul      ebx, DWORD PTR [64+edx+ecx]                   ;227.11
        mov       edx, DWORD PTR [36+edx+ecx]                   ;227.11
        mov       esi, DWORD PTR [44+esp]                       ;227.11
        inc       esi                                           ;227.11
        mov       DWORD PTR [8+edx+ebx], eax                    ;227.11
        cmp       esi, DWORD PTR [36+esp]                       ;227.11
        jb        .B5.165       ; Prob 64%                      ;227.11
                                ; LOE ecx esi edi
.B5.166:                        ; Preds .B5.165                 ; Infreq
        mov       eax, DWORD PTR [20+esp]                       ;
        lea       ecx, DWORD PTR [1+esi+esi]                    ;227.11
        mov       edx, DWORD PTR [32+esp]                       ;
                                ; LOE eax edx ecx edi
.B5.167:                        ; Preds .B5.166 .B5.187         ; Infreq
        lea       ebx, DWORD PTR [-1+ecx]                       ;227.11
        cmp       ebx, DWORD PTR [252+esp]                      ;227.11
        jae       .B5.169       ; Prob 11%                      ;227.11
                                ; LOE eax edx ecx edi
.B5.168:                        ; Preds .B5.167                 ; Infreq
        neg       eax                                           ;227.11
        add       eax, DWORD PTR [edi]                          ;227.11
        imul      eax, eax, 76                                  ;227.11
        add       ecx, DWORD PTR [256+esp]                      ;227.11
        sub       ecx, DWORD PTR [68+edx+eax]                   ;227.11
        imul      ecx, DWORD PTR [64+edx+eax]                   ;227.11
        mov       edx, DWORD PTR [36+edx+eax]                   ;227.11
        mov       DWORD PTR [8+edx+ecx], 0                      ;227.11
                                ; LOE edi
.B5.169:                        ; Preds .B5.150 .B5.167 .B5.168 ; Infreq
        mov       ebx, DWORD PTR [164+esp]                      ;227.11
        test      bl, 1                                         ;227.11
        je        .B5.172       ; Prob 60%                      ;227.11
                                ; LOE ebx edi
.B5.170:                        ; Preds .B5.169                 ; Infreq
        mov       edx, ebx                                      ;227.11
        mov       eax, ebx                                      ;227.11
        shr       edx, 1                                        ;227.11
        and       eax, 1                                        ;227.11
        and       edx, 1                                        ;227.11
        add       eax, eax                                      ;227.11
        shl       edx, 2                                        ;227.11
        or        edx, eax                                      ;227.11
        or        edx, 262144                                   ;227.11
        push      edx                                           ;227.11
        push      DWORD PTR [156+esp]                           ;227.11
        call      _for_dealloc_allocatable                      ;227.11
                                ; LOE ebx edi
.B5.272:                        ; Preds .B5.170                 ; Infreq
        add       esp, 8                                        ;227.11
                                ; LOE ebx edi
.B5.171:                        ; Preds .B5.272                 ; Infreq
        and       ebx, -2                                       ;227.11
        mov       DWORD PTR [152+esp], 0                        ;227.11
        mov       DWORD PTR [164+esp], ebx                      ;227.11
                                ; LOE ebx edi
.B5.172:                        ; Preds .B5.171 .B5.169 .B5.225 ; Infreq
        test      bl, 1                                         ;227.11
        jne       .B5.191       ; Prob 4%                       ;227.11
                                ; LOE ebx edi
.B5.173:                        ; Preds .B5.275 .B5.172         ; Infreq
        mov       eax, DWORD PTR [16+ebp]                       ;229.3
        mov       eax, DWORD PTR [eax]                          ;229.3
                                ; LOE eax edi
.B5.174:                        ; Preds .B5.173                 ; Infreq
        cmp       eax, 1                                        ;229.3
        je        .B5.190       ; Prob 20%                      ;229.3
        jmp       .B5.2         ; Prob 100%                     ;229.3
                                ; LOE eax edi
.B5.175:                        ; Preds .B5.9 .B5.190           ; Infreq
        mov       ebx, DWORD PTR [20+ebp]                       ;230.44
        movss     xmm0, DWORD PTR [_2il0floatpacket.22]         ;230.44
        movss     xmm1, DWORD PTR [_2il0floatpacket.23]         ;230.44
        movss     xmm3, DWORD PTR [ebx]                         ;230.44
        andps     xmm0, xmm3                                    ;230.44
        pxor      xmm3, xmm0                                    ;230.44
        movaps    xmm2, xmm3                                    ;230.44
        movss     xmm4, DWORD PTR [_2il0floatpacket.24]         ;230.44
        cmpltss   xmm2, xmm1                                    ;230.44
        imul      esi, DWORD PTR [edi], 76                      ;230.5
        andps     xmm1, xmm2                                    ;230.44
        movaps    xmm2, xmm3                                    ;230.44
        movaps    xmm6, xmm4                                    ;230.44
        add       ecx, esi                                      ;230.5
        sub       ecx, edx                                      ;230.5
        addss     xmm2, xmm1                                    ;230.44
        addss     xmm6, xmm4                                    ;230.44
        subss     xmm2, xmm1                                    ;230.44
        movaps    xmm7, xmm2                                    ;230.44
        sub       eax, DWORD PTR [32+ecx]                       ;230.5
        subss     xmm7, xmm3                                    ;230.44
        imul      eax, DWORD PTR [28+ecx]                       ;230.5
        movaps    xmm5, xmm7                                    ;230.44
        cmpless   xmm7, DWORD PTR [_2il0floatpacket.25]         ;230.44
        cmpnless  xmm5, xmm4                                    ;230.44
        andps     xmm5, xmm6                                    ;230.44
        andps     xmm7, xmm6                                    ;230.44
        mov       ecx, DWORD PTR [ecx]                          ;230.5
        subss     xmm2, xmm5                                    ;230.44
        addss     xmm2, xmm7                                    ;230.44
        orps      xmm2, xmm0                                    ;230.44
        cvtss2si  edx, xmm2                                     ;230.5
        mov       DWORD PTR [ecx+eax], edx                      ;230.5
        add       esp, 276                                      ;230.5
        pop       ebx                                           ;230.5
        pop       edi                                           ;230.5
        pop       esi                                           ;230.5
        mov       esp, ebp                                      ;230.5
        pop       ebp                                           ;230.5
        ret                                                     ;230.5
                                ; LOE
.B5.176:                        ; Preds .B5.20 .B5.24 .B5.22    ; Infreq
        xor       eax, eax                                      ;226.11
        mov       DWORD PTR [8+esp], eax                        ;226.11
        jmp       .B5.36        ; Prob 100%                     ;226.11
                                ; LOE edx ebx edi
.B5.179:                        ; Preds .B5.48                  ; Infreq
        mov       DWORD PTR [48+esp], 0                         ;226.11
        lea       edx, DWORD PTR [48+esp]                       ;226.11
        mov       DWORD PTR [esp], 38                           ;226.11
        lea       eax, DWORD PTR [esp]                          ;226.11
        mov       DWORD PTR [4+esp], OFFSET FLAT: __STRLITPACK_28 ;226.11
        push      32                                            ;226.11
        push      eax                                           ;226.11
        push      OFFSET FLAT: __STRLITPACK_38.0.3              ;226.11
        push      -2088435968                                   ;226.11
        push      911                                           ;226.11
        push      edx                                           ;226.11
        call      _for_write_seq_lis                            ;226.11
                                ; LOE ebx edi
.B5.180:                        ; Preds .B5.179                 ; Infreq
        push      32                                            ;226.11
        xor       eax, eax                                      ;226.11
        push      eax                                           ;226.11
        push      eax                                           ;226.11
        push      -2088435968                                   ;226.11
        push      eax                                           ;226.11
        push      OFFSET FLAT: __STRLITPACK_39                  ;226.11
        call      _for_stop_core                                ;226.11
                                ; LOE ebx edi
.B5.273:                        ; Preds .B5.180                 ; Infreq
        add       esp, 48                                       ;226.11
        jmp       .B5.49        ; Prob 100%                     ;226.11
                                ; LOE ebx edi
.B5.181:                        ; Preds .B5.64                  ; Infreq
        mov       ecx, 1                                        ;
        jmp       .B5.68        ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx edi
.B5.182:                        ; Preds .B5.84                  ; Infreq
        mov       edx, 1                                        ;
        jmp       .B5.88        ; Prob 100%                     ;
                                ; LOE eax edx ebx esi edi
.B5.183:                        ; Preds .B5.97                  ; Infreq
        mov       edx, 1                                        ;
        jmp       .B5.101       ; Prob 100%                     ;
                                ; LOE eax edx ebx esi edi
.B5.184:                        ; Preds .B5.110                 ; Infreq
        mov       edx, 1                                        ;
        jmp       .B5.114       ; Prob 100%                     ;
                                ; LOE eax edx ebx esi edi
.B5.185:                        ; Preds .B5.124                 ; Infreq
        mov       DWORD PTR [80+esp], 0                         ;227.11
        lea       edx, DWORD PTR [80+esp]                       ;227.11
        mov       DWORD PTR [8+esp], 38                         ;227.11
        lea       eax, DWORD PTR [8+esp]                        ;227.11
        mov       DWORD PTR [12+esp], OFFSET FLAT: __STRLITPACK_20 ;227.11
        push      32                                            ;227.11
        push      eax                                           ;227.11
        push      OFFSET FLAT: __STRLITPACK_46.0.4              ;227.11
        push      -2088435968                                   ;227.11
        push      911                                           ;227.11
        push      edx                                           ;227.11
        call      _for_write_seq_lis                            ;227.11
                                ; LOE edi
.B5.186:                        ; Preds .B5.185                 ; Infreq
        push      32                                            ;227.11
        xor       eax, eax                                      ;227.11
        push      eax                                           ;227.11
        push      eax                                           ;227.11
        push      -2088435968                                   ;227.11
        push      eax                                           ;227.11
        push      OFFSET FLAT: __STRLITPACK_47                  ;227.11
        call      _for_stop_core                                ;227.11
                                ; LOE edi
.B5.274:                        ; Preds .B5.186                 ; Infreq
        add       esp, 48                                       ;227.11
        jmp       .B5.125       ; Prob 100%                     ;227.11
                                ; LOE edi
.B5.187:                        ; Preds .B5.163                 ; Infreq
        mov       ecx, 1                                        ;
        jmp       .B5.167       ; Prob 100%                     ;
                                ; LOE eax edx ecx edi
.B5.188:                        ; Preds .B5.157                 ; Infreq
        mov       ecx, 1                                        ;
        jmp       .B5.161       ; Prob 100%                     ;
                                ; LOE eax edx ecx edi
.B5.189:                        ; Preds .B5.151                 ; Infreq
        mov       ecx, 1                                        ;
        jmp       .B5.155       ; Prob 100%                     ;
                                ; LOE eax edx ecx edi
.B5.190:                        ; Preds .B5.174                 ; Infreq
        mov       eax, DWORD PTR [12+ebp]                       ;230.5
        mov       ecx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY] ;230.44
        imul      edx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY+32], 76 ;
        movsx     eax, WORD PTR [eax]                           ;230.5
        jmp       .B5.175       ; Prob 100%                     ;230.5
                                ; LOE eax edx ecx edi
.B5.191:                        ; Preds .B5.172                 ; Infreq
        mov       eax, ebx                                      ;227.11
        and       ebx, 1                                        ;227.11
        shr       eax, 1                                        ;227.11
        add       ebx, ebx                                      ;227.11
        and       eax, 1                                        ;227.11
        shl       eax, 2                                        ;227.11
        or        eax, ebx                                      ;227.11
        or        eax, 262144                                   ;227.11
        push      eax                                           ;227.11
        push      DWORD PTR [156+esp]                           ;227.11
        call      _for_dealloc_allocatable                      ;227.11
                                ; LOE edi
.B5.275:                        ; Preds .B5.191                 ; Infreq
        add       esp, 8                                        ;227.11
        jmp       .B5.173       ; Prob 100%                     ;227.11
                                ; LOE edi
.B5.193:                        ; Preds .B5.144                 ; Infreq
        mov       ecx, 1                                        ;
        jmp       .B5.148       ; Prob 100%                     ;
                                ; LOE eax edx ecx edi
.B5.194:                        ; Preds .B5.138                 ; Infreq
        mov       ecx, 1                                        ;
        jmp       .B5.142       ; Prob 100%                     ;
                                ; LOE eax edx ecx edi
.B5.195:                        ; Preds .B5.132                 ; Infreq
        mov       ecx, 1                                        ;
        jmp       .B5.136       ; Prob 100%                     ;
                                ; LOE eax edx ecx edi
.B5.196:                        ; Preds .B5.116                 ; Infreq
        mov       edx, 1                                        ;
        jmp       .B5.120       ; Prob 100%                     ;
                                ; LOE eax edx ebx esi edi
.B5.197:                        ; Preds .B5.103                 ; Infreq
        mov       edx, 1                                        ;
        jmp       .B5.107       ; Prob 100%                     ;
                                ; LOE eax edx ebx esi edi
.B5.198:                        ; Preds .B5.90                  ; Infreq
        mov       edx, 1                                        ;
        jmp       .B5.94        ; Prob 100%                     ;
                                ; LOE eax edx ebx esi edi
.B5.199:                        ; Preds .B5.74                  ; Infreq
        lea       eax, DWORD PTR [44+esp]                       ;227.11
        push      12                                            ;227.11
        push      DWORD PTR [36+esp]                            ;227.11
        push      2                                             ;227.11
        push      eax                                           ;227.11
        call      _for_check_mult_overflow                      ;227.11
                                ; LOE eax edi
.B5.200:                        ; Preds .B5.199                 ; Infreq
        mov       edx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY+32] ;227.11
        and       eax, 1                                        ;227.11
        neg       edx                                           ;227.11
        add       edx, DWORD PTR [edi]                          ;227.11
        imul      ecx, edx, 76                                  ;227.11
        mov       ebx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY] ;227.11
        shl       eax, 4                                        ;227.11
        or        eax, 262145                                   ;227.11
        push      eax                                           ;227.11
        lea       esi, DWORD PTR [36+ecx+ebx]                   ;227.11
        push      esi                                           ;227.11
        push      DWORD PTR [68+esp]                            ;227.11
        call      _for_allocate                                 ;227.11
                                ; LOE eax edi
.B5.277:                        ; Preds .B5.200                 ; Infreq
        add       esp, 28                                       ;227.11
        mov       ebx, eax                                      ;227.11
                                ; LOE ebx edi
.B5.201:                        ; Preds .B5.277                 ; Infreq
        test      ebx, ebx                                      ;227.11
        je        .B5.204       ; Prob 50%                      ;227.11
                                ; LOE ebx edi
.B5.202:                        ; Preds .B5.201                 ; Infreq
        mov       DWORD PTR [80+esp], 0                         ;227.11
        lea       edx, DWORD PTR [80+esp]                       ;227.11
        mov       DWORD PTR [32+esp], 36                        ;227.11
        lea       eax, DWORD PTR [32+esp]                       ;227.11
        mov       DWORD PTR [36+esp], OFFSET FLAT: __STRLITPACK_16 ;227.11
        push      32                                            ;227.11
        push      eax                                           ;227.11
        push      OFFSET FLAT: __STRLITPACK_50.0.4              ;227.11
        push      -2088435968                                   ;227.11
        push      911                                           ;227.11
        push      edx                                           ;227.11
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VECTORERROR], ebx ;227.11
        call      _for_write_seq_lis                            ;227.11
                                ; LOE edi
.B5.203:                        ; Preds .B5.202                 ; Infreq
        push      32                                            ;227.11
        xor       eax, eax                                      ;227.11
        push      eax                                           ;227.11
        push      eax                                           ;227.11
        push      -2088435968                                   ;227.11
        push      eax                                           ;227.11
        push      OFFSET FLAT: __STRLITPACK_51                  ;227.11
        call      _for_stop_core                                ;227.11
                                ; LOE edi
.B5.278:                        ; Preds .B5.203                 ; Infreq
        add       esp, 48                                       ;227.11
        jmp       .B5.206       ; Prob 100%                     ;227.11
                                ; LOE edi
.B5.204:                        ; Preds .B5.201                 ; Infreq
        mov       ecx, DWORD PTR [edi]                          ;227.11
        mov       eax, 1                                        ;227.11
        sub       ecx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY+32] ;227.11
        imul      esi, ecx, 76                                  ;227.11
        mov       ecx, 12                                       ;227.11
        mov       edx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY] ;227.11
        mov       DWORD PTR [52+edx+esi], eax                   ;227.11
        mov       DWORD PTR [68+edx+esi], eax                   ;227.11
        mov       eax, DWORD PTR [32+esp]                       ;227.11
        mov       DWORD PTR [48+edx+esi], 5                     ;227.11
        mov       DWORD PTR [40+edx+esi], ecx                   ;227.11
        mov       DWORD PTR [44+edx+esi], 0                     ;227.11
        mov       DWORD PTR [60+edx+esi], eax                   ;227.11
        mov       DWORD PTR [64+edx+esi], ecx                   ;227.11
        lea       edx, DWORD PTR [20+esp]                       ;227.11
        push      ecx                                           ;227.11
        push      eax                                           ;227.11
        push      2                                             ;227.11
        push      edx                                           ;227.11
        call      _for_check_mult_overflow                      ;227.11
                                ; LOE ebx edi
.B5.279:                        ; Preds .B5.204                 ; Infreq
        add       esp, 16                                       ;227.11
                                ; LOE ebx edi
.B5.205:                        ; Preds .B5.279                 ; Infreq
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VECTORERROR], ebx ;227.11
                                ; LOE edi
.B5.206:                        ; Preds .B5.278 .B5.205         ; Infreq
        cmp       DWORD PTR [252+esp], 0                        ;227.11
        jle       .B5.225       ; Prob 50%                      ;227.11
                                ; LOE edi
.B5.207:                        ; Preds .B5.206                 ; Infreq
        mov       ecx, DWORD PTR [252+esp]                      ;227.11
        mov       ebx, ecx                                      ;227.11
        shr       ebx, 31                                       ;227.11
        add       ebx, ecx                                      ;227.11
        sar       ebx, 1                                        ;227.11
        mov       eax, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY] ;227.11
        test      ebx, ebx                                      ;227.11
        mov       edx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY+32] ;227.11
        mov       DWORD PTR [148+esp], ebx                      ;227.11
        jbe       .B5.229       ; Prob 11%                      ;227.11
                                ; LOE eax edx edi
.B5.208:                        ; Preds .B5.207                 ; Infreq
        imul      ecx, edx, -76                                 ;
        xor       ebx, ebx                                      ;
        mov       DWORD PTR [8+esp], edx                        ;
        add       ecx, eax                                      ;
        mov       DWORD PTR [12+esp], eax                       ;
        xor       esi, esi                                      ;
                                ; LOE ecx ebx esi edi
.B5.209:                        ; Preds .B5.209 .B5.208         ; Infreq
        imul      eax, DWORD PTR [edi], 76                      ;227.11
        lea       edx, DWORD PTR [1+ebx+ebx]                    ;227.11
        sub       edx, DWORD PTR [68+eax+ecx]                   ;227.11
        imul      edx, DWORD PTR [64+eax+ecx]                   ;227.11
        mov       eax, DWORD PTR [36+eax+ecx]                   ;227.11
        mov       DWORD PTR [eax+edx], esi                      ;227.11
        lea       eax, DWORD PTR [2+ebx+ebx]                    ;227.11
        imul      edx, DWORD PTR [edi], 76                      ;227.11
        inc       ebx                                           ;227.11
        sub       eax, DWORD PTR [68+edx+ecx]                   ;227.11
        imul      eax, DWORD PTR [64+edx+ecx]                   ;227.11
        mov       edx, DWORD PTR [36+edx+ecx]                   ;227.11
        cmp       ebx, DWORD PTR [148+esp]                      ;227.11
        mov       DWORD PTR [edx+eax], esi                      ;227.11
        jb        .B5.209       ; Prob 64%                      ;227.11
                                ; LOE ecx ebx esi edi
.B5.210:                        ; Preds .B5.209                 ; Infreq
        mov       edx, DWORD PTR [8+esp]                        ;
        lea       ecx, DWORD PTR [1+ebx+ebx]                    ;227.11
        mov       eax, DWORD PTR [12+esp]                       ;
                                ; LOE eax edx ecx edi
.B5.211:                        ; Preds .B5.210 .B5.229         ; Infreq
        lea       ebx, DWORD PTR [-1+ecx]                       ;227.11
        cmp       ebx, DWORD PTR [252+esp]                      ;227.11
        jae       .B5.213       ; Prob 11%                      ;227.11
                                ; LOE eax edx ecx edi
.B5.212:                        ; Preds .B5.211                 ; Infreq
        mov       ebx, edx                                      ;227.11
        neg       ebx                                           ;227.11
        add       ebx, DWORD PTR [edi]                          ;227.11
        imul      esi, ebx, 76                                  ;227.11
        sub       ecx, DWORD PTR [68+eax+esi]                   ;227.11
        imul      ecx, DWORD PTR [64+eax+esi]                   ;227.11
        mov       ebx, DWORD PTR [36+eax+esi]                   ;227.11
        mov       DWORD PTR [ebx+ecx], 0                        ;227.11
                                ; LOE eax edx edi
.B5.213:                        ; Preds .B5.211 .B5.212         ; Infreq
        cmp       DWORD PTR [148+esp], 0                        ;227.11
        jbe       .B5.228       ; Prob 11%                      ;227.11
                                ; LOE eax edx edi
.B5.214:                        ; Preds .B5.213                 ; Infreq
        imul      ecx, edx, -76                                 ;
        xor       ebx, ebx                                      ;
        mov       DWORD PTR [8+esp], edx                        ;
        add       ecx, eax                                      ;
        mov       DWORD PTR [12+esp], eax                       ;
        xor       esi, esi                                      ;
                                ; LOE ecx ebx esi edi
.B5.215:                        ; Preds .B5.215 .B5.214         ; Infreq
        imul      eax, DWORD PTR [edi], 76                      ;227.11
        lea       edx, DWORD PTR [1+ebx+ebx]                    ;227.11
        sub       edx, DWORD PTR [68+eax+ecx]                   ;227.11
        imul      edx, DWORD PTR [64+eax+ecx]                   ;227.11
        mov       eax, DWORD PTR [36+eax+ecx]                   ;227.11
        mov       DWORD PTR [4+eax+edx], esi                    ;227.11
        lea       eax, DWORD PTR [2+ebx+ebx]                    ;227.11
        imul      edx, DWORD PTR [edi], 76                      ;227.11
        inc       ebx                                           ;227.11
        sub       eax, DWORD PTR [68+edx+ecx]                   ;227.11
        imul      eax, DWORD PTR [64+edx+ecx]                   ;227.11
        mov       edx, DWORD PTR [36+edx+ecx]                   ;227.11
        cmp       ebx, DWORD PTR [148+esp]                      ;227.11
        mov       DWORD PTR [4+edx+eax], esi                    ;227.11
        jb        .B5.215       ; Prob 64%                      ;227.11
                                ; LOE ecx ebx esi edi
.B5.216:                        ; Preds .B5.215                 ; Infreq
        mov       edx, DWORD PTR [8+esp]                        ;
        lea       ecx, DWORD PTR [1+ebx+ebx]                    ;227.11
        mov       eax, DWORD PTR [12+esp]                       ;
                                ; LOE eax edx ecx edi
.B5.217:                        ; Preds .B5.216 .B5.228         ; Infreq
        lea       ebx, DWORD PTR [-1+ecx]                       ;227.11
        cmp       ebx, DWORD PTR [252+esp]                      ;227.11
        jae       .B5.219       ; Prob 11%                      ;227.11
                                ; LOE eax edx ecx edi
.B5.218:                        ; Preds .B5.217                 ; Infreq
        mov       ebx, edx                                      ;227.11
        neg       ebx                                           ;227.11
        add       ebx, DWORD PTR [edi]                          ;227.11
        imul      esi, ebx, 76                                  ;227.11
        sub       ecx, DWORD PTR [68+eax+esi]                   ;227.11
        imul      ecx, DWORD PTR [64+eax+esi]                   ;227.11
        mov       ebx, DWORD PTR [36+eax+esi]                   ;227.11
        mov       DWORD PTR [4+ebx+ecx], 0                      ;227.11
                                ; LOE eax edx edi
.B5.219:                        ; Preds .B5.217 .B5.218         ; Infreq
        cmp       DWORD PTR [148+esp], 0                        ;227.11
        jbe       .B5.227       ; Prob 0%                       ;227.11
                                ; LOE eax edx edi
.B5.220:                        ; Preds .B5.219                 ; Infreq
        imul      ecx, edx, -76                                 ;
        xor       ebx, ebx                                      ;
        mov       DWORD PTR [8+esp], edx                        ;
        add       ecx, eax                                      ;
        mov       DWORD PTR [12+esp], eax                       ;
        xor       esi, esi                                      ;
                                ; LOE ecx ebx esi edi
.B5.221:                        ; Preds .B5.221 .B5.220         ; Infreq
        imul      eax, DWORD PTR [edi], 76                      ;227.11
        lea       edx, DWORD PTR [1+ebx+ebx]                    ;227.11
        sub       edx, DWORD PTR [68+eax+ecx]                   ;227.11
        imul      edx, DWORD PTR [64+eax+ecx]                   ;227.11
        mov       eax, DWORD PTR [36+eax+ecx]                   ;227.11
        mov       DWORD PTR [8+eax+edx], esi                    ;227.11
        lea       eax, DWORD PTR [2+ebx+ebx]                    ;227.11
        imul      edx, DWORD PTR [edi], 76                      ;227.11
        inc       ebx                                           ;227.11
        sub       eax, DWORD PTR [68+edx+ecx]                   ;227.11
        imul      eax, DWORD PTR [64+edx+ecx]                   ;227.11
        mov       edx, DWORD PTR [36+edx+ecx]                   ;227.11
        cmp       ebx, DWORD PTR [148+esp]                      ;227.11
        mov       DWORD PTR [8+edx+eax], esi                    ;227.11
        jb        .B5.221       ; Prob 64%                      ;227.11
                                ; LOE ecx ebx esi edi
.B5.222:                        ; Preds .B5.221                 ; Infreq
        mov       edx, DWORD PTR [8+esp]                        ;
        lea       ecx, DWORD PTR [1+ebx+ebx]                    ;227.11
        mov       eax, DWORD PTR [12+esp]                       ;
                                ; LOE eax edx ecx edi
.B5.223:                        ; Preds .B5.222 .B5.227         ; Infreq
        lea       ebx, DWORD PTR [-1+ecx]                       ;227.11
        cmp       ebx, DWORD PTR [252+esp]                      ;227.11
        jae       .B5.225       ; Prob 0%                       ;227.11
                                ; LOE eax edx ecx edi
.B5.224:                        ; Preds .B5.223                 ; Infreq
        neg       edx                                           ;227.11
        add       edx, DWORD PTR [edi]                          ;227.11
        imul      edx, edx, 76                                  ;227.11
        sub       ecx, DWORD PTR [68+eax+edx]                   ;227.11
        imul      ecx, DWORD PTR [64+eax+edx]                   ;227.11
        mov       eax, DWORD PTR [36+eax+edx]                   ;227.11
        mov       DWORD PTR [8+eax+ecx], 0                      ;227.11
                                ; LOE edi
.B5.225:                        ; Preds .B5.206 .B5.223 .B5.224 ; Infreq
        mov       ebx, DWORD PTR [164+esp]                      ;227.11
        jmp       .B5.172       ; Prob 100%                     ;227.11
                                ; LOE ebx edi
.B5.227:                        ; Preds .B5.219                 ; Infreq
        mov       ecx, 1                                        ;
        jmp       .B5.223       ; Prob 100%                     ;
                                ; LOE eax edx ecx edi
.B5.228:                        ; Preds .B5.213                 ; Infreq
        mov       ecx, 1                                        ;
        jmp       .B5.217       ; Prob 100%                     ;
                                ; LOE eax edx ecx edi
.B5.229:                        ; Preds .B5.207                 ; Infreq
        mov       ecx, 1                                        ;
        jmp       .B5.211       ; Prob 100%                     ;
                                ; LOE eax edx ecx edi
.B5.230:                        ; Preds .B5.73                  ; Infreq
        mov       ecx, esi                                      ;226.11
        mov       edx, esi                                      ;226.11
        shr       ecx, 1                                        ;226.11
        and       edx, 1                                        ;226.11
        and       ecx, 1                                        ;226.11
        add       edx, edx                                      ;226.11
        shl       ecx, 2                                        ;226.11
        or        ecx, edx                                      ;226.11
        or        ecx, 262144                                   ;226.11
        push      ecx                                           ;226.11
        push      DWORD PTR [116+esp]                           ;226.11
        mov       DWORD PTR [16+esp], eax                       ;226.11
        call      _for_dealloc_allocatable                      ;226.11
                                ; LOE ebx esi edi
.B5.280:                        ; Preds .B5.230                 ; Infreq
        mov       eax, DWORD PTR [16+esp]                       ;
        add       esp, 8                                        ;226.11
                                ; LOE eax ebx esi edi al ah
.B5.231:                        ; Preds .B5.280                 ; Infreq
        and       esi, -2                                       ;226.11
        mov       DWORD PTR [112+esp], 0                        ;226.11
        mov       DWORD PTR [124+esp], esi                      ;226.11
        jmp       .B5.74        ; Prob 100%                     ;226.11
                                ; LOE eax ebx edi
.B5.232:                        ; Preds .B5.57                  ; Infreq
        mov       ecx, 1                                        ;
        jmp       .B5.61        ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx edi
.B5.233:                        ; Preds .B5.40                  ; Infreq
        mov       DWORD PTR [esp], 1                            ;
        jmp       .B5.44        ; Prob 100%                     ;
                                ; LOE edx ebx edi
.B5.234:                        ; Preds .B5.10                  ; Infreq
        mov       eax, DWORD PTR [252+esp]                      ;226.11
        xor       edx, edx                                      ;226.11
        test      eax, eax                                      ;226.11
        lea       ecx, DWORD PTR [40+esp]                       ;226.11
        cmovle    eax, edx                                      ;226.11
        mov       DWORD PTR [32+esp], eax                       ;226.11
        push      4                                             ;226.11
        push      eax                                           ;226.11
        push      2                                             ;226.11
        push      ecx                                           ;226.11
        call      _for_check_mult_overflow                      ;226.11
                                ; LOE eax edi
.B5.235:                        ; Preds .B5.234                 ; Infreq
        mov       edx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY+32] ;226.11
        and       eax, 1                                        ;226.11
        neg       edx                                           ;226.11
        add       edx, DWORD PTR [edi]                          ;226.11
        imul      ecx, edx, 76                                  ;226.11
        shl       eax, 4                                        ;226.11
        or        eax, 262145                                   ;226.11
        add       ecx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY] ;226.11
        push      eax                                           ;226.11
        push      ecx                                           ;226.11
        push      DWORD PTR [64+esp]                            ;226.11
        call      _for_allocate                                 ;226.11
                                ; LOE eax edi
.B5.282:                        ; Preds .B5.235                 ; Infreq
        add       esp, 28                                       ;226.11
        mov       ebx, eax                                      ;226.11
                                ; LOE ebx edi
.B5.236:                        ; Preds .B5.282                 ; Infreq
        test      ebx, ebx                                      ;226.11
        je        .B5.239       ; Prob 50%                      ;226.11
                                ; LOE ebx edi
.B5.237:                        ; Preds .B5.236                 ; Infreq
        mov       DWORD PTR [48+esp], 0                         ;226.11
        lea       edx, DWORD PTR [48+esp]                       ;226.11
        mov       DWORD PTR [24+esp], 36                        ;226.11
        lea       eax, DWORD PTR [24+esp]                       ;226.11
        mov       DWORD PTR [28+esp], OFFSET FLAT: __STRLITPACK_24 ;226.11
        push      32                                            ;226.11
        push      eax                                           ;226.11
        push      OFFSET FLAT: __STRLITPACK_42.0.3              ;226.11
        push      -2088435968                                   ;226.11
        push      911                                           ;226.11
        push      edx                                           ;226.11
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VECTORERROR], ebx ;226.11
        call      _for_write_seq_lis                            ;226.11
                                ; LOE edi
.B5.238:                        ; Preds .B5.237                 ; Infreq
        push      32                                            ;226.11
        xor       eax, eax                                      ;226.11
        push      eax                                           ;226.11
        push      eax                                           ;226.11
        push      -2088435968                                   ;226.11
        push      eax                                           ;226.11
        push      OFFSET FLAT: __STRLITPACK_43                  ;226.11
        call      _for_stop_core                                ;226.11
                                ; LOE edi
.B5.283:                        ; Preds .B5.238                 ; Infreq
        add       esp, 48                                       ;226.11
        jmp       .B5.241       ; Prob 100%                     ;226.11
                                ; LOE edi
.B5.239:                        ; Preds .B5.236                 ; Infreq
        mov       ecx, DWORD PTR [edi]                          ;226.11
        mov       eax, 1                                        ;226.11
        sub       ecx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY+32] ;226.11
        imul      esi, ecx, 76                                  ;226.11
        mov       ecx, 4                                        ;226.11
        mov       edx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY] ;226.11
        mov       DWORD PTR [16+edx+esi], eax                   ;226.11
        mov       DWORD PTR [32+edx+esi], eax                   ;226.11
        mov       eax, DWORD PTR [32+esp]                       ;226.11
        mov       DWORD PTR [12+edx+esi], 5                     ;226.11
        mov       DWORD PTR [4+edx+esi], ecx                    ;226.11
        mov       DWORD PTR [8+edx+esi], 0                      ;226.11
        mov       DWORD PTR [24+edx+esi], eax                   ;226.11
        mov       DWORD PTR [28+edx+esi], ecx                   ;226.11
        lea       edx, DWORD PTR [16+esp]                       ;226.11
        push      ecx                                           ;226.11
        push      eax                                           ;226.11
        push      2                                             ;226.11
        push      edx                                           ;226.11
        call      _for_check_mult_overflow                      ;226.11
                                ; LOE ebx edi
.B5.284:                        ; Preds .B5.239                 ; Infreq
        add       esp, 16                                       ;226.11
                                ; LOE ebx edi
.B5.240:                        ; Preds .B5.284                 ; Infreq
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VECTORERROR], ebx ;226.11
                                ; LOE edi
.B5.241:                        ; Preds .B5.283 .B5.240         ; Infreq
        mov       edx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY+32] ;226.11
        mov       eax, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY] ;226.11
        mov       DWORD PTR [36+esp], edx                       ;226.11
        cmp       DWORD PTR [252+esp], 0                        ;226.11
        jle       .B5.250       ; Prob 10%                      ;226.11
                                ; LOE eax edi
.B5.242:                        ; Preds .B5.241                 ; Infreq
        mov       edx, DWORD PTR [252+esp]                      ;226.11
        mov       ebx, edx                                      ;226.11
        imul      ecx, DWORD PTR [36+esp], 76                   ;
        shr       ebx, 31                                       ;226.11
        add       ebx, edx                                      ;226.11
        sar       ebx, 1                                        ;226.11
        mov       DWORD PTR [esp], ecx                          ;
        test      ebx, ebx                                      ;226.11
        jbe       .B5.249       ; Prob 0%                       ;226.11
                                ; LOE eax ebx edi
.B5.243:                        ; Preds .B5.242                 ; Infreq
        xor       ecx, ecx                                      ;
        mov       edx, eax                                      ;
        sub       edx, DWORD PTR [esp]                          ;
        xor       esi, esi                                      ;
        mov       DWORD PTR [4+esp], ebx                        ;
        mov       DWORD PTR [8+esp], eax                        ;
                                ; LOE edx ecx esi edi
.B5.244:                        ; Preds .B5.244 .B5.243         ; Infreq
        imul      eax, DWORD PTR [edi], 76                      ;226.11
        lea       ebx, DWORD PTR [1+ecx+ecx]                    ;226.11
        sub       ebx, DWORD PTR [32+eax+edx]                   ;226.11
        imul      ebx, DWORD PTR [28+eax+edx]                   ;226.11
        mov       eax, DWORD PTR [eax+edx]                      ;226.11
        mov       DWORD PTR [eax+ebx], esi                      ;226.11
        lea       eax, DWORD PTR [2+ecx+ecx]                    ;226.11
        imul      ebx, DWORD PTR [edi], 76                      ;226.11
        inc       ecx                                           ;226.11
        sub       eax, DWORD PTR [32+ebx+edx]                   ;226.11
        imul      eax, DWORD PTR [28+ebx+edx]                   ;226.11
        mov       ebx, DWORD PTR [ebx+edx]                      ;226.11
        cmp       ecx, DWORD PTR [4+esp]                        ;226.11
        mov       DWORD PTR [ebx+eax], esi                      ;226.11
        jb        .B5.244       ; Prob 64%                      ;226.11
                                ; LOE edx ecx esi edi
.B5.245:                        ; Preds .B5.244                 ; Infreq
        mov       eax, DWORD PTR [8+esp]                        ;
        lea       edx, DWORD PTR [1+ecx+ecx]                    ;226.11
                                ; LOE eax edx edi
.B5.246:                        ; Preds .B5.245 .B5.249         ; Infreq
        lea       ecx, DWORD PTR [-1+edx]                       ;226.11
        cmp       ecx, DWORD PTR [252+esp]                      ;226.11
        jae       .B5.248       ; Prob 0%                       ;226.11
                                ; LOE eax edx edi
.B5.247:                        ; Preds .B5.246                 ; Infreq
        imul      ecx, DWORD PTR [edi], 76                      ;226.11
        mov       ebx, eax                                      ;226.11
        sub       ebx, DWORD PTR [esp]                          ;226.11
        sub       edx, DWORD PTR [32+ecx+ebx]                   ;226.11
        imul      edx, DWORD PTR [28+ecx+ebx]                   ;226.11
        mov       esi, DWORD PTR [ecx+ebx]                      ;226.11
        mov       DWORD PTR [esi+edx], 0                        ;226.11
                                ; LOE eax edi
.B5.248:                        ; Preds .B5.250 .B5.247 .B5.246 ; Infreq
        mov       ebx, DWORD PTR [edi]                          ;226.11
        imul      ecx, ebx, 76                                  ;226.11
        add       ecx, eax                                      ;226.11
        sub       ecx, DWORD PTR [esp]                          ;226.11
        mov       edx, DWORD PTR [252+esp]                      ;226.11
        mov       esi, DWORD PTR [124+esp]                      ;226.11
        mov       WORD PTR [72+ecx], dx                         ;226.11
        jmp       .B5.73        ; Prob 100%                     ;226.11
                                ; LOE eax ebx esi edi
.B5.249:                        ; Preds .B5.242                 ; Infreq
        mov       edx, 1                                        ;
        jmp       .B5.246       ; Prob 100%                     ;
                                ; LOE eax edx edi
.B5.250:                        ; Preds .B5.241                 ; Infreq
        imul      edx, DWORD PTR [36+esp], 76                   ;
        mov       DWORD PTR [esp], edx                          ;
        jmp       .B5.248       ; Prob 100%                     ;
        ALIGN     16
                                ; LOE eax edi
; mark_end;
_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_INSERT ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_INSERT
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_VALUE
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_VALUE
_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_VALUE	PROC NEAR 
; parameter 1: 8 + ebp
; parameter 2: 12 + ebp
; parameter 3: 16 + ebp
.B6.1:                          ; Preds .B6.0
        push      ebp                                           ;242.15
        mov       ebp, esp                                      ;242.15
        and       esp, -16                                      ;242.15
        push      esi                                           ;242.15
        push      edi                                           ;242.15
        push      ebx                                           ;242.15
        sub       esp, 100                                      ;242.15
        mov       eax, DWORD PTR [12+ebp]                       ;242.15
        mov       edx, DWORD PTR [8+ebp]                        ;242.15
        imul      ebx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY+32], -76 ;245.15
        mov       esi, DWORD PTR [eax]                          ;245.3
        mov       eax, DWORD PTR [edx]                          ;245.17
        imul      edi, eax, 76                                  ;245.17
        add       ebx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY] ;245.15
        movsx     ecx, WORD PTR [72+edi+ebx]                    ;245.17
        cmp       esi, ecx                                      ;245.15
        mov       DWORD PTR [4+esp], ecx                        ;245.17
        jle       .B6.9         ; Prob 50%                      ;245.15
                                ; LOE eax ebx esi edi
.B6.2:                          ; Preds .B6.1
        mov       DWORD PTR [16+esp], 0                         ;246.6
        lea       ecx, DWORD PTR [16+esp]                       ;246.6
        mov       DWORD PTR [48+esp], 28                        ;246.6
        lea       edx, DWORD PTR [48+esp]                       ;246.6
        mov       DWORD PTR [52+esp], OFFSET FLAT: __STRLITPACK_14 ;246.6
        push      32                                            ;246.6
        push      edx                                           ;246.6
        push      OFFSET FLAT: __STRLITPACK_52.0.6              ;246.6
        push      -2088435968                                   ;246.6
        push      911                                           ;246.6
        push      ecx                                           ;246.6
        mov       DWORD PTR [24+esp], eax                       ;246.6
        call      _for_write_seq_lis                            ;246.6
                                ; LOE ebx esi edi
.B6.3:                          ; Preds .B6.2
        mov       eax, DWORD PTR [24+esp]                       ;
        mov       DWORD PTR [96+esp], eax                       ;246.6
        lea       eax, DWORD PTR [96+esp]                       ;246.6
        push      eax                                           ;246.6
        push      OFFSET FLAT: __STRLITPACK_53.0.6              ;246.6
        lea       edx, DWORD PTR [48+esp]                       ;246.6
        push      edx                                           ;246.6
        call      _for_write_seq_lis_xmit                       ;246.6
                                ; LOE ebx esi edi
.B6.4:                          ; Preds .B6.3
        mov       DWORD PTR [52+esp], 0                         ;247.6
        lea       eax, DWORD PTR [92+esp]                       ;247.6
        mov       DWORD PTR [92+esp], 9                         ;247.6
        mov       DWORD PTR [96+esp], OFFSET FLAT: __STRLITPACK_12 ;247.6
        push      32                                            ;247.6
        push      eax                                           ;247.6
        push      OFFSET FLAT: __STRLITPACK_54.0.6              ;247.6
        push      -2088435968                                   ;247.6
        push      911                                           ;247.6
        lea       edx, DWORD PTR [72+esp]                       ;247.6
        push      edx                                           ;247.6
        call      _for_write_seq_lis                            ;247.6
                                ; LOE ebx esi edi
.B6.5:                          ; Preds .B6.4
        mov       DWORD PTR [140+esp], esi                      ;247.6
        lea       eax, DWORD PTR [140+esp]                      ;247.6
        push      eax                                           ;247.6
        push      OFFSET FLAT: __STRLITPACK_55.0.6              ;247.6
        lea       edx, DWORD PTR [84+esp]                       ;247.6
        push      edx                                           ;247.6
        call      _for_write_seq_lis_xmit                       ;247.6
                                ; LOE ebx esi edi
.B6.6:                          ; Preds .B6.5
        mov       DWORD PTR [88+esp], 0                         ;248.3
        lea       eax, DWORD PTR [136+esp]                      ;248.3
        mov       DWORD PTR [136+esp], 24                       ;248.3
        mov       DWORD PTR [140+esp], OFFSET FLAT: __STRLITPACK_10 ;248.3
        push      32                                            ;248.3
        push      eax                                           ;248.3
        push      OFFSET FLAT: __STRLITPACK_56.0.6              ;248.3
        push      -2088435968                                   ;248.3
        push      911                                           ;248.3
        lea       edx, DWORD PTR [108+esp]                      ;248.3
        push      edx                                           ;248.3
        call      _for_write_seq_lis                            ;248.3
                                ; LOE ebx esi edi
.B6.7:                          ; Preds .B6.6
        mov       eax, DWORD PTR [100+esp]                      ;248.3
        lea       edx, DWORD PTR [184+esp]                      ;248.3
        mov       WORD PTR [184+esp], ax                        ;248.3
        push      edx                                           ;248.3
        push      OFFSET FLAT: __STRLITPACK_57.0.6              ;248.3
        lea       ecx, DWORD PTR [120+esp]                      ;248.3
        push      ecx                                           ;248.3
        call      _for_write_seq_lis_xmit                       ;248.3
                                ; LOE ebx esi edi
.B6.25:                         ; Preds .B6.7
        add       esp, 108                                      ;248.3
                                ; LOE ebx esi edi
.B6.8:                          ; Preds .B6.25
        push      32                                            ;249.6
        xor       eax, eax                                      ;249.6
        push      eax                                           ;249.6
        push      eax                                           ;249.6
        push      -2088435968                                   ;249.6
        push      eax                                           ;249.6
        push      OFFSET FLAT: __STRLITPACK_58                  ;249.6
        call      _for_stop_core                                ;249.6
                                ; LOE ebx esi edi
.B6.26:                         ; Preds .B6.8
        add       esp, 24                                       ;249.6
                                ; LOE ebx esi edi
.B6.9:                          ; Preds .B6.26 .B6.1
        mov       eax, DWORD PTR [16+ebp]                       ;242.15
        mov       eax, DWORD PTR [eax]                          ;252.3
        cmp       eax, 1                                        ;252.3
        je        .B6.20        ; Prob 20%                      ;252.3
                                ; LOE eax ebx esi edi
.B6.10:                         ; Preds .B6.9
        cmp       eax, 2                                        ;252.3
        je        .B6.18        ; Prob 25%                      ;252.3
                                ; LOE eax ebx esi edi
.B6.11:                         ; Preds .B6.10
        cmp       eax, 3                                        ;252.3
        jne       .B6.13        ; Prob 67%                      ;252.3
                                ; LOE eax ebx esi edi
.B6.12:                         ; Preds .B6.11
        mov       eax, DWORD PTR [68+edi+ebx]                   ;257.21
        neg       eax                                           ;257.6
        add       eax, esi                                      ;257.6
        imul      eax, DWORD PTR [64+edi+ebx]                   ;257.6
        mov       ebx, DWORD PTR [36+edi+ebx]                   ;257.21
        cvtsi2ss  xmm1, DWORD PTR [4+ebx+eax]                   ;257.6
        jmp       .B6.19        ; Prob 100%                     ;257.6
                                ; LOE xmm1
.B6.13:                         ; Preds .B6.11
        cmp       eax, 4                                        ;252.3
        jne       .B6.15        ; Prob 50%                      ;252.3
                                ; LOE ebx esi edi
.B6.14:                         ; Preds .B6.13
        mov       eax, DWORD PTR [68+edi+ebx]                   ;259.21
        neg       eax                                           ;259.6
        add       eax, esi                                      ;259.6
        imul      eax, DWORD PTR [64+edi+ebx]                   ;259.6
        mov       ebx, DWORD PTR [36+edi+ebx]                   ;259.21
        cvtsi2ss  xmm1, DWORD PTR [8+ebx+eax]                   ;259.6
        jmp       .B6.19        ; Prob 100%                     ;259.6
                                ; LOE xmm1
.B6.15:                         ; Preds .B6.13
        mov       DWORD PTR [16+esp], 0                         ;261.5
        lea       edx, DWORD PTR [16+esp]                       ;261.5
        mov       DWORD PTR [esp], 22                           ;261.5
        lea       eax, DWORD PTR [esp]                          ;261.5
        mov       DWORD PTR [4+esp], OFFSET FLAT: __STRLITPACK_8 ;261.5
        push      32                                            ;261.5
        push      eax                                           ;261.5
        push      OFFSET FLAT: __STRLITPACK_59.0.6              ;261.5
        push      -2088435968                                   ;261.5
        push      911                                           ;261.5
        push      edx                                           ;261.5
        call      _for_write_seq_lis                            ;261.5
                                ; LOE
.B6.16:                         ; Preds .B6.15
        push      32                                            ;262.5
        xor       eax, eax                                      ;262.5
        push      eax                                           ;262.5
        push      eax                                           ;262.5
        push      -2088435968                                   ;262.5
        push      eax                                           ;262.5
        push      OFFSET FLAT: __STRLITPACK_60                  ;262.5
        call      _for_stop_core                                ;262.5
                                ; LOE
.B6.27:                         ; Preds .B6.16
        add       esp, 48                                       ;262.5
                                ; LOE
.B6.17:                         ; Preds .B6.27
        pxor      xmm1, xmm1                                    ;
        jmp       .B6.19        ; Prob 100%                     ;
                                ; LOE xmm1
.B6.18:                         ; Preds .B6.10
        mov       eax, DWORD PTR [68+edi+ebx]                   ;255.21
        neg       eax                                           ;255.6
        add       eax, esi                                      ;255.6
        imul      eax, DWORD PTR [64+edi+ebx]                   ;255.6
        mov       ebx, DWORD PTR [36+edi+ebx]                   ;255.21
        cvtsi2ss  xmm1, DWORD PTR [ebx+eax]                     ;255.6
                                ; LOE xmm1
.B6.19:                         ; Preds .B6.18 .B6.12 .B6.14 .B6.17 .B6.28
                                ;       .B6.20
        movss     DWORD PTR [esp], xmm1                         ;268.1
        fld       DWORD PTR [esp]                               ;268.1
        add       esp, 100                                      ;268.1
        pop       ebx                                           ;268.1
        pop       edi                                           ;268.1
        pop       esi                                           ;268.1
        mov       esp, ebp                                      ;268.1
        pop       ebp                                           ;268.1
        ret                                                     ;268.1
                                ; LOE
.B6.20:                         ; Preds .B6.9                   ; Infreq
        mov       eax, DWORD PTR [32+edi+ebx]                   ;253.27
        neg       eax                                           ;253.6
        add       eax, esi                                      ;253.6
        imul      eax, DWORD PTR [28+edi+ebx]                   ;253.6
        movss     xmm0, DWORD PTR [_2il0floatpacket.29]         ;264.16
        mov       ebx, DWORD PTR [ebx+edi]                      ;253.27
        cvtsi2ss  xmm1, DWORD PTR [ebx+eax]                     ;253.6
        comiss    xmm0, xmm1                                    ;264.34
        jbe       .B6.19        ; Prob 95%                      ;264.34
                                ; LOE xmm1
.B6.21:                         ; Preds .B6.20                  ; Infreq
        mov       DWORD PTR [16+esp], 0                         ;265.5
        lea       edx, DWORD PTR [16+esp]                       ;265.5
        mov       DWORD PTR [esp], 25                           ;265.5
        lea       eax, DWORD PTR [esp]                          ;265.5
        mov       DWORD PTR [4+esp], OFFSET FLAT: __STRLITPACK_6 ;265.5
        push      32                                            ;265.5
        push      eax                                           ;265.5
        push      OFFSET FLAT: __STRLITPACK_61.0.6              ;265.5
        push      -2088435968                                   ;265.5
        push      911                                           ;265.5
        push      edx                                           ;265.5
        movss     DWORD PTR [32+esp], xmm1                      ;265.5
        call      _for_write_seq_lis                            ;265.5
                                ; LOE
.B6.22:                         ; Preds .B6.21                  ; Infreq
        xor       eax, eax                                      ;266.2
        push      32                                            ;266.2
        push      eax                                           ;266.2
        push      eax                                           ;266.2
        push      -2088435968                                   ;266.2
        push      eax                                           ;266.2
        push      OFFSET FLAT: __STRLITPACK_62                  ;266.2
        call      _for_stop_core                                ;266.2
                                ; LOE
.B6.28:                         ; Preds .B6.22                  ; Infreq
        movss     xmm1, DWORD PTR [56+esp]                      ;
        add       esp, 48                                       ;266.2
        jmp       .B6.19        ; Prob 100%                     ;266.2
        ALIGN     16
                                ; LOE xmm1
; mark_end;
_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_VALUE ENDP
_TEXT	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_52.0.6	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_53.0.6	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_54.0.6	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_55.0.6	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_56.0.6	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_57.0.6	DB	7
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_59.0.6	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_61.0.6	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_VALUE
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ACTIVE_SIZE
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ACTIVE_SIZE
_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ACTIVE_SIZE	PROC NEAR 
; parameter 1: 4 + esp
; parameter 2: 8 + esp
.B7.1:                          ; Preds .B7.0
        mov       eax, DWORD PTR [4+esp]                        ;271.12
        mov       edx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY+32] ;272.30
        neg       edx                                           ;272.4
        add       edx, DWORD PTR [eax]                          ;272.4
        imul      edx, edx, 76                                  ;272.4
        mov       ecx, DWORD PTR [8+esp]                        ;271.12
        mov       eax, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY] ;272.30
        mov       ecx, DWORD PTR [ecx]                          ;272.30
        mov       WORD PTR [74+eax+edx], cx                     ;272.4
        ret                                                     ;273.1
        ALIGN     16
                                ; LOE
; mark_end;
_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ACTIVE_SIZE ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ACTIVE_SIZE
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_P_SIZE
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_P_SIZE
_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_P_SIZE	PROC NEAR 
; parameter 1: 4 + esp
.B8.1:                          ; Preds .B8.0
        mov       edx, DWORD PTR [4+esp]                        ;275.18
        mov       ecx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY+32] ;276.4
        neg       ecx                                           ;277.1
        add       ecx, DWORD PTR [edx]                          ;277.1
        imul      edx, ecx, 76                                  ;277.1
        mov       eax, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY] ;276.4
        movsx     eax, WORD PTR [74+eax+edx]                    ;276.20
        ret                                                     ;277.1
        ALIGN     16
                                ; LOE
; mark_end;
_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_P_SIZE ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_P_SIZE
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_A_SIZE
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_A_SIZE
_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_A_SIZE	PROC NEAR 
; parameter 1: 4 + esp
.B9.1:                          ; Preds .B9.0
        mov       edx, DWORD PTR [4+esp]                        ;279.18
        mov       ecx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY+32] ;280.4
        neg       ecx                                           ;281.1
        add       ecx, DWORD PTR [edx]                          ;281.1
        imul      edx, ecx, 76                                  ;281.1
        mov       eax, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY] ;280.4
        movsx     eax, WORD PTR [74+eax+edx]                    ;280.20
        ret                                                     ;281.1
        ALIGN     16
                                ; LOE
; mark_end;
_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_A_SIZE ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_A_SIZE
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_P_REMOVE
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_P_REMOVE
_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_P_REMOVE	PROC NEAR 
; parameter 1: 8 + ebp
.B10.1:                         ; Preds .B10.0
        push      ebp                                           ;284.12
        mov       ebp, esp                                      ;284.12
        and       esp, -16                                      ;284.12
        push      esi                                           ;284.12
        push      edi                                           ;284.12
        push      ebx                                           ;284.12
        sub       esp, 52                                       ;284.12
        mov       eax, DWORD PTR [8+ebp]                        ;284.12
        imul      edi, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY+32], -76 ;287.30
        mov       esi, DWORD PTR [eax]                          ;287.7
        imul      ebx, esi, 76                                  ;287.7
        add       edi, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY] ;287.30
        movsx     edx, WORD PTR [72+ebx+edi]                    ;287.7
        test      edx, edx                                      ;287.30
        jle       .B10.5        ; Prob 79%                      ;287.30
                                ; LOE ebx esi edi
.B10.2:                         ; Preds .B10.1
        mov       eax, DWORD PTR [12+ebx+edi]                   ;288.16
        mov       edx, eax                                      ;288.5
        shr       edx, 1                                        ;288.5
        and       edx, 1                                        ;288.5
        mov       DWORD PTR [esp], eax                          ;288.16
        and       eax, 1                                        ;288.5
        shl       edx, 2                                        ;288.5
        add       eax, eax                                      ;288.5
        or        edx, 1                                        ;288.5
        or        edx, eax                                      ;288.5
        or        edx, 262144                                   ;288.5
        push      edx                                           ;288.5
        push      DWORD PTR [edi+ebx]                           ;288.5
        call      _for_dealloc_allocatable                      ;288.5
                                ; LOE eax ebx esi edi
.B10.11:                        ; Preds .B10.2
        add       esp, 8                                        ;288.5
                                ; LOE eax ebx esi edi
.B10.3:                         ; Preds .B10.11
        mov       edx, DWORD PTR [esp]                          ;288.5
        and       edx, -2                                       ;288.5
        mov       DWORD PTR [edi+ebx], 0                        ;288.5
        test      eax, eax                                      ;289.17
        mov       DWORD PTR [12+ebx+edi], edx                   ;288.5
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VECTORERROR], eax ;288.5
        jne       .B10.6        ; Prob 5%                       ;289.17
                                ; LOE ebx esi edi
.B10.13:                        ; Preds .B10.3
        xor       esi, esi                                      ;294.5
                                ; LOE ebx esi edi
.B10.4:                         ; Preds .B10.12 .B10.13
        mov       WORD PTR [72+ebx+edi], si                     ;294.5
                                ; LOE
.B10.5:                         ; Preds .B10.1 .B10.4
        add       esp, 52                                       ;296.1
        pop       ebx                                           ;296.1
        pop       edi                                           ;296.1
        pop       esi                                           ;296.1
        mov       esp, ebp                                      ;296.1
        pop       ebp                                           ;296.1
        ret                                                     ;296.1
                                ; LOE
.B10.6:                         ; Preds .B10.3                  ; Infreq
        mov       DWORD PTR [esp], 0                            ;290.4
        lea       edx, DWORD PTR [esp]                          ;290.4
        mov       DWORD PTR [32+esp], 37                        ;290.4
        lea       eax, DWORD PTR [32+esp]                       ;290.4
        mov       DWORD PTR [36+esp], OFFSET FLAT: __STRLITPACK_4 ;290.4
        push      32                                            ;290.4
        push      eax                                           ;290.4
        push      OFFSET FLAT: __STRLITPACK_63.0.10             ;290.4
        push      -2088435968                                   ;290.4
        push      911                                           ;290.4
        push      edx                                           ;290.4
        call      _for_write_seq_lis                            ;290.4
                                ; LOE ebx esi edi
.B10.7:                         ; Preds .B10.6                  ; Infreq
        mov       DWORD PTR [24+esp], 0                         ;291.4
        lea       eax, DWORD PTR [64+esp]                       ;291.4
        mov       DWORD PTR [64+esp], esi                       ;291.4
        push      32                                            ;291.4
        push      eax                                           ;291.4
        push      OFFSET FLAT: __STRLITPACK_64.0.10             ;291.4
        push      -2088435968                                   ;291.4
        push      911                                           ;291.4
        lea       edx, DWORD PTR [44+esp]                       ;291.4
        push      edx                                           ;291.4
        call      _for_write_seq_lis                            ;291.4
                                ; LOE ebx edi
.B10.8:                         ; Preds .B10.7                  ; Infreq
        push      32                                            ;292.4
        xor       esi, esi                                      ;292.4
        push      esi                                           ;292.4
        push      esi                                           ;292.4
        push      -2088435968                                   ;292.4
        push      esi                                           ;292.4
        push      OFFSET FLAT: __STRLITPACK_65                  ;292.4
        call      _for_stop_core                                ;292.4
                                ; LOE ebx esi edi
.B10.12:                        ; Preds .B10.8                  ; Infreq
        add       esp, 72                                       ;292.4
        jmp       .B10.4        ; Prob 100%                     ;292.4
        ALIGN     16
                                ; LOE ebx esi edi
; mark_end;
_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_P_REMOVE ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_P_REMOVE
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_A_REMOVE
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_A_REMOVE
_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_A_REMOVE	PROC NEAR 
; parameter 1: 8 + ebp
.B11.1:                         ; Preds .B11.0
        push      ebp                                           ;299.12
        mov       ebp, esp                                      ;299.12
        and       esp, -16                                      ;299.12
        push      esi                                           ;299.12
        push      edi                                           ;299.12
        push      ebx                                           ;299.12
        sub       esp, 52                                       ;299.12
        mov       eax, DWORD PTR [8+ebp]                        ;299.12
        imul      edi, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY+32], -76 ;301.32
        mov       esi, DWORD PTR [eax]                          ;301.8
        imul      ebx, esi, 76                                  ;301.8
        add       edi, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY] ;301.32
        movsx     edx, WORD PTR [74+ebx+edi]                    ;301.8
        test      edx, edx                                      ;301.32
        jle       .B11.5        ; Prob 79%                      ;301.32
                                ; LOE ebx esi edi
.B11.2:                         ; Preds .B11.1
        mov       eax, DWORD PTR [48+ebx+edi]                   ;302.16
        mov       edx, eax                                      ;302.5
        shr       edx, 1                                        ;302.5
        and       edx, 1                                        ;302.5
        mov       DWORD PTR [esp], eax                          ;302.16
        and       eax, 1                                        ;302.5
        shl       edx, 2                                        ;302.5
        add       eax, eax                                      ;302.5
        or        edx, 1                                        ;302.5
        or        edx, eax                                      ;302.5
        or        edx, 262144                                   ;302.5
        push      edx                                           ;302.5
        push      DWORD PTR [36+ebx+edi]                        ;302.5
        call      _for_dealloc_allocatable                      ;302.5
                                ; LOE eax ebx esi edi
.B11.11:                        ; Preds .B11.2
        add       esp, 8                                        ;302.5
                                ; LOE eax ebx esi edi
.B11.3:                         ; Preds .B11.11
        mov       edx, DWORD PTR [esp]                          ;302.5
        and       edx, -2                                       ;302.5
        mov       DWORD PTR [36+ebx+edi], 0                     ;302.5
        test      eax, eax                                      ;303.17
        mov       DWORD PTR [48+ebx+edi], edx                   ;302.5
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VECTORERROR], eax ;302.5
        jne       .B11.6        ; Prob 5%                       ;303.17
                                ; LOE ebx esi edi
.B11.13:                        ; Preds .B11.3
        xor       esi, esi                                      ;308.5
                                ; LOE ebx esi edi
.B11.4:                         ; Preds .B11.12 .B11.13
        mov       WORD PTR [74+ebx+edi], si                     ;308.5
                                ; LOE
.B11.5:                         ; Preds .B11.1 .B11.4
        add       esp, 52                                       ;310.1
        pop       ebx                                           ;310.1
        pop       edi                                           ;310.1
        pop       esi                                           ;310.1
        mov       esp, ebp                                      ;310.1
        pop       ebp                                           ;310.1
        ret                                                     ;310.1
                                ; LOE
.B11.6:                         ; Preds .B11.3                  ; Infreq
        mov       DWORD PTR [esp], 0                            ;304.4
        lea       edx, DWORD PTR [esp]                          ;304.4
        mov       DWORD PTR [32+esp], 37                        ;304.4
        lea       eax, DWORD PTR [32+esp]                       ;304.4
        mov       DWORD PTR [36+esp], OFFSET FLAT: __STRLITPACK_2 ;304.4
        push      32                                            ;304.4
        push      eax                                           ;304.4
        push      OFFSET FLAT: __STRLITPACK_66.0.11             ;304.4
        push      -2088435968                                   ;304.4
        push      911                                           ;304.4
        push      edx                                           ;304.4
        call      _for_write_seq_lis                            ;304.4
                                ; LOE ebx esi edi
.B11.7:                         ; Preds .B11.6                  ; Infreq
        mov       DWORD PTR [24+esp], 0                         ;305.4
        lea       eax, DWORD PTR [64+esp]                       ;305.4
        mov       DWORD PTR [64+esp], esi                       ;305.4
        push      32                                            ;305.4
        push      eax                                           ;305.4
        push      OFFSET FLAT: __STRLITPACK_67.0.11             ;305.4
        push      -2088435968                                   ;305.4
        push      911                                           ;305.4
        lea       edx, DWORD PTR [44+esp]                       ;305.4
        push      edx                                           ;305.4
        call      _for_write_seq_lis                            ;305.4
                                ; LOE ebx edi
.B11.8:                         ; Preds .B11.7                  ; Infreq
        push      32                                            ;306.4
        xor       esi, esi                                      ;306.4
        push      esi                                           ;306.4
        push      esi                                           ;306.4
        push      -2088435968                                   ;306.4
        push      esi                                           ;306.4
        push      OFFSET FLAT: __STRLITPACK_68                  ;306.4
        call      _for_stop_core                                ;306.4
                                ; LOE ebx esi edi
.B11.12:                        ; Preds .B11.8                  ; Infreq
        add       esp, 72                                       ;306.4
        jmp       .B11.4        ; Prob 100%                     ;306.4
        ALIGN     16
                                ; LOE ebx esi edi
; mark_end;
_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_A_REMOVE ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_A_REMOVE
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_CLEAR
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_CLEAR
_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_CLEAR	PROC NEAR 
; parameter 1: 32 + esp
; parameter 2: 36 + esp
.B12.1:                         ; Preds .B12.0
        push      esi                                           ;314.12
        push      edi                                           ;314.12
        push      ebx                                           ;314.12
        push      ebp                                           ;314.12
        sub       esp, 12                                       ;314.12
        mov       edx, DWORD PTR [32+esp]                       ;314.12
        imul      eax, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY+32], -76 ;319.6
        imul      ecx, DWORD PTR [edx], 76                      ;319.6
        mov       ebx, DWORD PTR [36+esp]                       ;314.12
        add       eax, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY] ;319.6
        mov       esi, DWORD PTR [ebx]                          ;319.6
        movsx     ebp, WORD PTR [72+ecx+eax]                    ;319.6
        sub       ebp, esi                                      ;319.6
        inc       ebp                                           ;319.6
        test      ebp, ebp                                      ;319.6
        jle       .B12.29       ; Prob 50%                      ;319.6
                                ; LOE eax edx ebx ebp esi
.B12.2:                         ; Preds .B12.1
        mov       edi, ebp                                      ;319.6
        shr       edi, 31                                       ;319.6
        add       edi, ebp                                      ;319.6
        sar       edi, 1                                        ;319.6
        test      edi, edi                                      ;319.6
        jbe       .B12.33       ; Prob 10%                      ;319.6
                                ; LOE eax edx ebx ebp esi edi
.B12.3:                         ; Preds .B12.2
        mov       DWORD PTR [4+esp], edi                        ;
        xor       ecx, ecx                                      ;
        mov       DWORD PTR [esp], ebp                          ;
        mov       DWORD PTR [8+esp], esi                        ;
                                ; LOE eax edx ecx
.B12.4:                         ; Preds .B12.4 .B12.3
        imul      ebx, DWORD PTR [edx], 76                      ;319.6
        mov       edi, DWORD PTR [8+esp]                        ;319.6
        mov       esi, DWORD PTR [32+ebx+eax]                   ;319.6
        neg       esi                                           ;319.6
        lea       ebp, DWORD PTR [edi+ecx*2]                    ;319.6
        mov       edi, DWORD PTR [ebx+eax]                      ;319.6
        add       esi, ebp                                      ;319.6
        inc       ebp                                           ;319.6
        imul      esi, DWORD PTR [28+ebx+eax]                   ;319.6
        xor       ebx, ebx                                      ;319.6
        mov       DWORD PTR [edi+esi], ebx                      ;319.6
        inc       ecx                                           ;319.6
        imul      esi, DWORD PTR [edx], 76                      ;319.6
        sub       ebp, DWORD PTR [32+esi+eax]                   ;319.6
        imul      ebp, DWORD PTR [28+esi+eax]                   ;319.6
        mov       esi, DWORD PTR [esi+eax]                      ;319.6
        cmp       ecx, DWORD PTR [4+esp]                        ;319.6
        mov       DWORD PTR [esi+ebp], ebx                      ;319.6
        jb        .B12.4        ; Prob 63%                      ;319.6
                                ; LOE eax edx ecx
.B12.5:                         ; Preds .B12.4
        mov       ebp, DWORD PTR [esp]                          ;
        lea       ecx, DWORD PTR [1+ecx+ecx]                    ;319.6
        mov       esi, DWORD PTR [8+esp]                        ;
        mov       ebx, DWORD PTR [36+esp]                       ;
                                ; LOE eax edx ecx ebx ebp esi
.B12.6:                         ; Preds .B12.5 .B12.33
        lea       edi, DWORD PTR [-1+ecx]                       ;319.6
        cmp       ebp, edi                                      ;319.6
        jbe       .B12.8        ; Prob 10%                      ;319.6
                                ; LOE eax edx ecx ebx esi
.B12.7:                         ; Preds .B12.6
        imul      ebp, DWORD PTR [edx], 76                      ;319.6
        lea       esi, DWORD PTR [-1+esi+ecx]                   ;319.6
        sub       esi, DWORD PTR [32+ebp+eax]                   ;319.6
        imul      esi, DWORD PTR [28+ebp+eax]                   ;319.6
        mov       ecx, DWORD PTR [ebp+eax]                      ;319.6
        mov       DWORD PTR [ecx+esi], 0                        ;319.6
                                ; LOE eax edx ebx
.B12.8:                         ; Preds .B12.6 .B12.7
        imul      ecx, DWORD PTR [edx], 76                      ;320.6
        mov       esi, DWORD PTR [ebx]                          ;320.6
        movsx     ebp, WORD PTR [72+ecx+eax]                    ;320.6
        sub       ebp, esi                                      ;320.6
        inc       ebp                                           ;320.6
        test      ebp, ebp                                      ;320.6
        jle       .B12.29       ; Prob 50%                      ;320.6
                                ; LOE eax edx ebx ebp esi
.B12.9:                         ; Preds .B12.8
        mov       edi, ebp                                      ;320.6
        shr       edi, 31                                       ;320.6
        add       edi, ebp                                      ;320.6
        sar       edi, 1                                        ;320.6
        test      edi, edi                                      ;320.6
        jbe       .B12.32       ; Prob 10%                      ;320.6
                                ; LOE eax edx ebx ebp esi edi
.B12.10:                        ; Preds .B12.9
        mov       DWORD PTR [4+esp], edi                        ;
        xor       ecx, ecx                                      ;
        mov       DWORD PTR [esp], ebp                          ;
        mov       DWORD PTR [8+esp], esi                        ;
                                ; LOE eax edx ecx
.B12.11:                        ; Preds .B12.11 .B12.10
        imul      ebx, DWORD PTR [edx], 76                      ;320.6
        mov       edi, DWORD PTR [8+esp]                        ;320.6
        mov       esi, DWORD PTR [68+ebx+eax]                   ;320.6
        neg       esi                                           ;320.6
        lea       ebp, DWORD PTR [edi+ecx*2]                    ;320.6
        mov       edi, DWORD PTR [36+ebx+eax]                   ;320.6
        add       esi, ebp                                      ;320.6
        inc       ebp                                           ;320.6
        imul      esi, DWORD PTR [64+ebx+eax]                   ;320.6
        xor       ebx, ebx                                      ;320.6
        mov       DWORD PTR [edi+esi], ebx                      ;320.6
        inc       ecx                                           ;320.6
        imul      esi, DWORD PTR [edx], 76                      ;320.6
        sub       ebp, DWORD PTR [68+esi+eax]                   ;320.6
        imul      ebp, DWORD PTR [64+esi+eax]                   ;320.6
        mov       esi, DWORD PTR [36+esi+eax]                   ;320.6
        cmp       ecx, DWORD PTR [4+esp]                        ;320.6
        mov       DWORD PTR [esi+ebp], ebx                      ;320.6
        jb        .B12.11       ; Prob 63%                      ;320.6
                                ; LOE eax edx ecx
.B12.12:                        ; Preds .B12.11
        mov       ebp, DWORD PTR [esp]                          ;
        lea       ecx, DWORD PTR [1+ecx+ecx]                    ;320.6
        mov       esi, DWORD PTR [8+esp]                        ;
        mov       ebx, DWORD PTR [36+esp]                       ;
                                ; LOE eax edx ecx ebx ebp esi
.B12.13:                        ; Preds .B12.12 .B12.32
        lea       edi, DWORD PTR [-1+ecx]                       ;320.6
        cmp       ebp, edi                                      ;320.6
        jbe       .B12.15       ; Prob 10%                      ;320.6
                                ; LOE eax edx ecx ebx esi
.B12.14:                        ; Preds .B12.13
        imul      ebp, DWORD PTR [edx], 76                      ;320.6
        lea       esi, DWORD PTR [-1+esi+ecx]                   ;320.6
        sub       esi, DWORD PTR [68+ebp+eax]                   ;320.6
        imul      esi, DWORD PTR [64+ebp+eax]                   ;320.6
        mov       ecx, DWORD PTR [36+ebp+eax]                   ;320.6
        mov       DWORD PTR [ecx+esi], 0                        ;320.6
                                ; LOE eax edx ebx
.B12.15:                        ; Preds .B12.13 .B12.14
        imul      ecx, DWORD PTR [edx], 76                      ;321.6
        mov       esi, DWORD PTR [ebx]                          ;321.6
        movsx     ebp, WORD PTR [72+ecx+eax]                    ;321.6
        sub       ebp, esi                                      ;321.6
        inc       ebp                                           ;321.6
        test      ebp, ebp                                      ;321.6
        jle       .B12.29       ; Prob 50%                      ;321.6
                                ; LOE eax edx ebx ebp esi
.B12.16:                        ; Preds .B12.15
        mov       edi, ebp                                      ;321.6
        shr       edi, 31                                       ;321.6
        add       edi, ebp                                      ;321.6
        sar       edi, 1                                        ;321.6
        test      edi, edi                                      ;321.6
        jbe       .B12.31       ; Prob 10%                      ;321.6
                                ; LOE eax edx ebx ebp esi edi
.B12.17:                        ; Preds .B12.16
        mov       DWORD PTR [4+esp], edi                        ;
        xor       ecx, ecx                                      ;
        mov       DWORD PTR [esp], ebp                          ;
        mov       DWORD PTR [8+esp], esi                        ;
                                ; LOE eax edx ecx
.B12.18:                        ; Preds .B12.18 .B12.17
        imul      ebx, DWORD PTR [edx], 76                      ;321.6
        mov       edi, DWORD PTR [8+esp]                        ;321.6
        mov       esi, DWORD PTR [68+ebx+eax]                   ;321.6
        neg       esi                                           ;321.6
        lea       ebp, DWORD PTR [edi+ecx*2]                    ;321.6
        mov       edi, DWORD PTR [36+ebx+eax]                   ;321.6
        add       esi, ebp                                      ;321.6
        inc       ebp                                           ;321.6
        imul      esi, DWORD PTR [64+ebx+eax]                   ;321.6
        xor       ebx, ebx                                      ;321.6
        mov       DWORD PTR [4+edi+esi], ebx                    ;321.6
        inc       ecx                                           ;321.6
        imul      esi, DWORD PTR [edx], 76                      ;321.6
        sub       ebp, DWORD PTR [68+esi+eax]                   ;321.6
        imul      ebp, DWORD PTR [64+esi+eax]                   ;321.6
        mov       esi, DWORD PTR [36+esi+eax]                   ;321.6
        cmp       ecx, DWORD PTR [4+esp]                        ;321.6
        mov       DWORD PTR [4+esi+ebp], ebx                    ;321.6
        jb        .B12.18       ; Prob 63%                      ;321.6
                                ; LOE eax edx ecx
.B12.19:                        ; Preds .B12.18
        mov       ebp, DWORD PTR [esp]                          ;
        lea       ecx, DWORD PTR [1+ecx+ecx]                    ;321.6
        mov       esi, DWORD PTR [8+esp]                        ;
        mov       ebx, DWORD PTR [36+esp]                       ;
                                ; LOE eax edx ecx ebx ebp esi
.B12.20:                        ; Preds .B12.19 .B12.31
        lea       edi, DWORD PTR [-1+ecx]                       ;321.6
        cmp       ebp, edi                                      ;321.6
        jbe       .B12.22       ; Prob 10%                      ;321.6
                                ; LOE eax edx ecx ebx esi
.B12.21:                        ; Preds .B12.20
        imul      ebp, DWORD PTR [edx], 76                      ;321.6
        lea       esi, DWORD PTR [-1+esi+ecx]                   ;321.6
        sub       esi, DWORD PTR [68+ebp+eax]                   ;321.6
        imul      esi, DWORD PTR [64+ebp+eax]                   ;321.6
        mov       ecx, DWORD PTR [36+ebp+eax]                   ;321.6
        mov       DWORD PTR [4+ecx+esi], 0                      ;321.6
                                ; LOE eax edx ebx
.B12.22:                        ; Preds .B12.20 .B12.21
        imul      ecx, DWORD PTR [edx], 76                      ;322.6
        mov       esi, DWORD PTR [ebx]                          ;322.6
        movsx     ebp, WORD PTR [72+ecx+eax]                    ;322.6
        sub       ebp, esi                                      ;322.6
        inc       ebp                                           ;322.6
        test      ebp, ebp                                      ;322.6
        jle       .B12.29       ; Prob 50%                      ;322.6
                                ; LOE eax edx ebp esi
.B12.23:                        ; Preds .B12.22
        mov       edi, ebp                                      ;322.6
        shr       edi, 31                                       ;322.6
        add       edi, ebp                                      ;322.6
        sar       edi, 1                                        ;322.6
        test      edi, edi                                      ;322.6
        jbe       .B12.30       ; Prob 10%                      ;322.6
                                ; LOE eax edx ebp esi edi
.B12.24:                        ; Preds .B12.23
        xor       ebx, ebx                                      ;
        mov       DWORD PTR [4+esp], edi                        ;
        xor       ecx, ecx                                      ;
        mov       DWORD PTR [esp], ebp                          ;
        mov       DWORD PTR [8+esp], esi                        ;
                                ; LOE eax edx ecx ebx
.B12.25:                        ; Preds .B12.25 .B12.24
        imul      edi, DWORD PTR [edx], 76                      ;322.6
        mov       ebp, DWORD PTR [8+esp]                        ;322.6
        mov       esi, DWORD PTR [68+edi+eax]                   ;322.6
        neg       esi                                           ;322.6
        lea       ebp, DWORD PTR [ebp+ebx*2]                    ;322.6
        inc       ebx                                           ;322.6
        add       esi, ebp                                      ;322.6
        inc       ebp                                           ;322.6
        imul      esi, DWORD PTR [64+edi+eax]                   ;322.6
        mov       edi, DWORD PTR [36+edi+eax]                   ;322.6
        mov       DWORD PTR [8+edi+esi], ecx                    ;322.6
        imul      esi, DWORD PTR [edx], 76                      ;322.6
        sub       ebp, DWORD PTR [68+esi+eax]                   ;322.6
        imul      ebp, DWORD PTR [64+esi+eax]                   ;322.6
        mov       esi, DWORD PTR [36+esi+eax]                   ;322.6
        cmp       ebx, DWORD PTR [4+esp]                        ;322.6
        mov       DWORD PTR [8+esi+ebp], ecx                    ;322.6
        jb        .B12.25       ; Prob 63%                      ;322.6
                                ; LOE eax edx ecx ebx
.B12.26:                        ; Preds .B12.25
        mov       ebp, DWORD PTR [esp]                          ;
        lea       ebx, DWORD PTR [1+ebx+ebx]                    ;322.6
        mov       esi, DWORD PTR [8+esp]                        ;
                                ; LOE eax edx ebx ebp esi
.B12.27:                        ; Preds .B12.26 .B12.30
        lea       ecx, DWORD PTR [-1+ebx]                       ;322.6
        cmp       ebp, ecx                                      ;322.6
        jbe       .B12.29       ; Prob 10%                      ;322.6
                                ; LOE eax edx ebx esi
.B12.28:                        ; Preds .B12.27
        imul      edx, DWORD PTR [edx], 76                      ;322.6
        lea       ecx, DWORD PTR [-1+ebx+esi]                   ;322.6
        sub       ecx, DWORD PTR [68+edx+eax]                   ;322.6
        imul      ecx, DWORD PTR [64+edx+eax]                   ;322.6
        mov       eax, DWORD PTR [36+edx+eax]                   ;322.6
        mov       DWORD PTR [8+eax+ecx], 0                      ;322.6
                                ; LOE
.B12.29:                        ; Preds .B12.15 .B12.8 .B12.1 .B12.22 .B12.27
                                ;       .B12.28
        add       esp, 12                                       ;323.1
        pop       ebp                                           ;323.1
        pop       ebx                                           ;323.1
        pop       edi                                           ;323.1
        pop       esi                                           ;323.1
        ret                                                     ;323.1
                                ; LOE
.B12.30:                        ; Preds .B12.23                 ; Infreq
        mov       ebx, 1                                        ;
        jmp       .B12.27       ; Prob 100%                     ;
                                ; LOE eax edx ebx ebp esi
.B12.31:                        ; Preds .B12.16                 ; Infreq
        mov       ecx, 1                                        ;
        jmp       .B12.20       ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx ebp esi
.B12.32:                        ; Preds .B12.9                  ; Infreq
        mov       ecx, 1                                        ;
        jmp       .B12.13       ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx ebp esi
.B12.33:                        ; Preds .B12.2                  ; Infreq
        mov       ecx, 1                                        ;
        jmp       .B12.6        ; Prob 100%                     ;
        ALIGN     16
                                ; LOE eax edx ecx ebx ebp esi
; mark_end;
_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_CLEAR ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_CLEAR
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_2DREMOVE
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_2DREMOVE
_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_2DREMOVE	PROC NEAR 
.B13.1:                         ; Preds .B13.0
        push      ebp                                           ;326.12
        mov       ebp, esp                                      ;326.12
        and       esp, -16                                      ;326.12
        push      esi                                           ;326.12
        sub       esp, 140                                      ;326.12
        mov       ecx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAYSIZE] ;330.3
        test      ecx, ecx                                      ;330.3
        jle       .B13.13       ; Prob 2%                       ;330.3
                                ; LOE ecx ebx edi
.B13.2:                         ; Preds .B13.1
        imul      eax, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY+32], -76 ;
        mov       edx, 1                                        ;
        add       eax, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY] ;
        mov       esi, 76                                       ;
        mov       DWORD PTR [8+esp], ecx                        ;
        mov       DWORD PTR [4+esp], edi                        ;
        mov       edi, edx                                      ;
        mov       DWORD PTR [esp], ebx                          ;
        mov       ebx, eax                                      ;
                                ; LOE ebx esi edi
.B13.3:                         ; Preds .B13.11 .B13.2
        movsx     ecx, WORD PTR [74+esi+ebx]                    ;331.10
        test      ecx, ecx                                      ;331.10
        jle       .B13.7        ; Prob 79%                      ;331.10
                                ; LOE ebx esi edi
.B13.4:                         ; Preds .B13.3
        mov       ecx, DWORD PTR [48+esi+ebx]                   ;331.10
        mov       eax, ecx                                      ;331.10
        shr       eax, 1                                        ;331.10
        and       ecx, 1                                        ;331.10
        and       eax, 1                                        ;331.10
        add       ecx, ecx                                      ;331.10
        shl       eax, 2                                        ;331.10
        or        eax, 1                                        ;331.10
        or        eax, ecx                                      ;331.10
        or        eax, 262144                                   ;331.10
        push      eax                                           ;331.10
        push      DWORD PTR [36+esi+ebx]                        ;331.10
        call      _for_dealloc_allocatable                      ;331.10
                                ; LOE eax ebx esi edi
.B13.27:                        ; Preds .B13.4
        add       esp, 8                                        ;331.10
                                ; LOE eax ebx esi edi
.B13.5:                         ; Preds .B13.27
        and       DWORD PTR [48+esi+ebx], -2                    ;331.10
        mov       DWORD PTR [36+esi+ebx], 0                     ;331.10
        test      eax, eax                                      ;331.10
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VECTORERROR], eax ;331.10
        jne       .B13.22       ; Prob 5%                       ;331.10
                                ; LOE ebx esi edi
.B13.6:                         ; Preds .B13.5 .B13.32
        xor       ecx, ecx                                      ;331.10
        mov       WORD PTR [74+esi+ebx], cx                     ;331.10
                                ; LOE ebx esi edi
.B13.7:                         ; Preds .B13.3 .B13.6
        movsx     ecx, WORD PTR [72+esi+ebx]                    ;332.10
        test      ecx, ecx                                      ;332.10
        jle       .B13.11       ; Prob 79%                      ;332.10
                                ; LOE ebx esi edi
.B13.8:                         ; Preds .B13.7
        mov       ecx, DWORD PTR [12+esi+ebx]                   ;332.10
        mov       eax, ecx                                      ;332.10
        shr       eax, 1                                        ;332.10
        and       ecx, 1                                        ;332.10
        and       eax, 1                                        ;332.10
        add       ecx, ecx                                      ;332.10
        shl       eax, 2                                        ;332.10
        or        eax, 1                                        ;332.10
        or        eax, ecx                                      ;332.10
        or        eax, 262144                                   ;332.10
        push      eax                                           ;332.10
        push      DWORD PTR [esi+ebx]                           ;332.10
        call      _for_dealloc_allocatable                      ;332.10
                                ; LOE eax ebx esi edi
.B13.28:                        ; Preds .B13.8
        add       esp, 8                                        ;332.10
                                ; LOE eax ebx esi edi
.B13.9:                         ; Preds .B13.28
        and       DWORD PTR [12+esi+ebx], -2                    ;332.10
        mov       DWORD PTR [esi+ebx], 0                        ;332.10
        test      eax, eax                                      ;332.10
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VECTORERROR], eax ;332.10
        jne       .B13.19       ; Prob 5%                       ;332.10
                                ; LOE ebx esi edi
.B13.10:                        ; Preds .B13.9 .B13.31
        xor       ecx, ecx                                      ;332.10
        mov       WORD PTR [72+esi+ebx], cx                     ;332.10
                                ; LOE ebx esi edi
.B13.11:                        ; Preds .B13.7 .B13.10
        inc       edi                                           ;333.3
        add       esi, 76                                       ;333.3
        cmp       edi, DWORD PTR [8+esp]                        ;333.3
        jle       .B13.3        ; Prob 82%                      ;333.3
                                ; LOE ebx esi edi
.B13.12:                        ; Preds .B13.11
        mov       edi, DWORD PTR [4+esp]                        ;
        mov       ebx, DWORD PTR [esp]                          ;
                                ; LOE ebx edi
.B13.13:                        ; Preds .B13.12 .B13.1
        mov       esi, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY+12] ;335.3
        test      esi, 1                                        ;335.7
        je        .B13.16       ; Prob 60%                      ;335.7
                                ; LOE ebx esi edi
.B13.14:                        ; Preds .B13.13
        mov       edx, esi                                      ;336.5
        mov       eax, esi                                      ;336.5
        shr       edx, 1                                        ;336.5
        and       eax, 1                                        ;336.5
        and       edx, 1                                        ;336.5
        add       eax, eax                                      ;336.5
        shl       edx, 2                                        ;336.5
        or        edx, 1                                        ;336.5
        or        edx, eax                                      ;336.5
        or        edx, 262144                                   ;336.5
        push      edx                                           ;336.5
        push      DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY] ;336.5
        call      _for_dealloc_allocatable                      ;336.5
                                ; LOE eax ebx esi edi
.B13.29:                        ; Preds .B13.14
        add       esp, 8                                        ;336.5
                                ; LOE eax ebx esi edi
.B13.15:                        ; Preds .B13.29
        and       esi, -2                                       ;336.5
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY], 0 ;336.5
        test      eax, eax                                      ;337.17
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY+12], esi ;336.5
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VECTORERROR], eax ;336.5
        jne       .B13.17       ; Prob 5%                       ;337.17
                                ; LOE ebx edi
.B13.16:                        ; Preds .B13.30 .B13.15 .B13.13
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAYSIZE], 0 ;342.3
        add       esp, 140                                      ;343.1
        pop       esi                                           ;343.1
        mov       esp, ebp                                      ;343.1
        pop       ebp                                           ;343.1
        ret                                                     ;343.1
                                ; LOE
.B13.17:                        ; Preds .B13.15                 ; Infreq
        mov       DWORD PTR [esp], 0                            ;338.5
        lea       edx, DWORD PTR [esp]                          ;338.5
        mov       DWORD PTR [32+esp], 14                        ;338.5
        lea       eax, DWORD PTR [32+esp]                       ;338.5
        mov       DWORD PTR [36+esp], OFFSET FLAT: __STRLITPACK_0 ;338.5
        push      32                                            ;338.5
        push      eax                                           ;338.5
        push      OFFSET FLAT: __STRLITPACK_69.0.13             ;338.5
        push      -2088435968                                   ;338.5
        push      911                                           ;338.5
        push      edx                                           ;338.5
        call      _for_write_seq_lis                            ;338.5
                                ; LOE ebx edi
.B13.18:                        ; Preds .B13.17                 ; Infreq
        push      32                                            ;339.5
        xor       eax, eax                                      ;339.5
        push      eax                                           ;339.5
        push      eax                                           ;339.5
        push      -2088435968                                   ;339.5
        push      eax                                           ;339.5
        push      OFFSET FLAT: __STRLITPACK_70                  ;339.5
        call      _for_stop_core                                ;339.5
                                ; LOE ebx edi
.B13.30:                        ; Preds .B13.18                 ; Infreq
        add       esp, 48                                       ;339.5
        jmp       .B13.16       ; Prob 100%                     ;339.5
                                ; LOE ebx edi
.B13.19:                        ; Preds .B13.9                  ; Infreq
        mov       DWORD PTR [80+esp], 0                         ;332.10
        lea       eax, DWORD PTR [80+esp]                       ;332.10
        mov       DWORD PTR [112+esp], 37                       ;332.10
        lea       ecx, DWORD PTR [112+esp]                      ;332.10
        mov       DWORD PTR [116+esp], OFFSET FLAT: __STRLITPACK_4 ;332.10
        push      32                                            ;332.10
        push      ecx                                           ;332.10
        push      OFFSET FLAT: __STRLITPACK_63.0.10             ;332.10
        push      -2088435968                                   ;332.10
        push      911                                           ;332.10
        push      eax                                           ;332.10
        call      _for_write_seq_lis                            ;332.10
                                ; LOE ebx esi edi
.B13.20:                        ; Preds .B13.19                 ; Infreq
        mov       DWORD PTR [104+esp], 0                        ;332.10
        lea       ecx, DWORD PTR [152+esp]                      ;332.10
        mov       DWORD PTR [152+esp], edi                      ;332.10
        push      32                                            ;332.10
        push      ecx                                           ;332.10
        push      OFFSET FLAT: __STRLITPACK_64.0.10             ;332.10
        push      -2088435968                                   ;332.10
        push      911                                           ;332.10
        lea       ecx, DWORD PTR [124+esp]                      ;332.10
        push      ecx                                           ;332.10
        call      _for_write_seq_lis                            ;332.10
                                ; LOE ebx esi edi
.B13.21:                        ; Preds .B13.20                 ; Infreq
        push      32                                            ;332.10
        xor       ecx, ecx                                      ;332.10
        push      ecx                                           ;332.10
        push      ecx                                           ;332.10
        push      -2088435968                                   ;332.10
        push      ecx                                           ;332.10
        push      OFFSET FLAT: __STRLITPACK_65                  ;332.10
        call      _for_stop_core                                ;332.10
                                ; LOE ebx esi edi
.B13.31:                        ; Preds .B13.21                 ; Infreq
        add       esp, 72                                       ;332.10
        jmp       .B13.10       ; Prob 100%                     ;332.10
                                ; LOE ebx esi edi
.B13.22:                        ; Preds .B13.5                  ; Infreq
        mov       DWORD PTR [48+esp], 0                         ;331.10
        lea       eax, DWORD PTR [48+esp]                       ;331.10
        mov       DWORD PTR [40+esp], 37                        ;331.10
        lea       ecx, DWORD PTR [40+esp]                       ;331.10
        mov       DWORD PTR [44+esp], OFFSET FLAT: __STRLITPACK_2 ;331.10
        push      32                                            ;331.10
        push      ecx                                           ;331.10
        push      OFFSET FLAT: __STRLITPACK_66.0.11             ;331.10
        push      -2088435968                                   ;331.10
        push      911                                           ;331.10
        push      eax                                           ;331.10
        call      _for_write_seq_lis                            ;331.10
                                ; LOE ebx esi edi
.B13.23:                        ; Preds .B13.22                 ; Infreq
        mov       DWORD PTR [72+esp], 0                         ;331.10
        lea       ecx, DWORD PTR [144+esp]                      ;331.10
        mov       DWORD PTR [144+esp], edi                      ;331.10
        push      32                                            ;331.10
        push      ecx                                           ;331.10
        push      OFFSET FLAT: __STRLITPACK_67.0.11             ;331.10
        push      -2088435968                                   ;331.10
        push      911                                           ;331.10
        lea       ecx, DWORD PTR [92+esp]                       ;331.10
        push      ecx                                           ;331.10
        call      _for_write_seq_lis                            ;331.10
                                ; LOE ebx esi edi
.B13.24:                        ; Preds .B13.23                 ; Infreq
        push      32                                            ;331.10
        xor       ecx, ecx                                      ;331.10
        push      ecx                                           ;331.10
        push      ecx                                           ;331.10
        push      -2088435968                                   ;331.10
        push      ecx                                           ;331.10
        push      OFFSET FLAT: __STRLITPACK_68                  ;331.10
        call      _for_stop_core                                ;331.10
                                ; LOE ebx esi edi
.B13.32:                        ; Preds .B13.24                 ; Infreq
        add       esp, 72                                       ;331.10
        jmp       .B13.6        ; Prob 100%                     ;331.10
        ALIGN     16
                                ; LOE ebx esi edi
; mark_end;
_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_2DREMOVE ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_2DREMOVE
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
	PUBLIC _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHAININCRE
_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHAININCRE	DD	10000
	PUBLIC _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHAINCOUNTER
_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHAINCOUNTER	DD	50000
	PUBLIC _DYNUST_VEH_PATH_ATT_MODULE_mp_V_INCREASESIZE
_DYNUST_VEH_PATH_ATT_MODULE_mp_V_INCREASESIZE	DD	50
	DD 1 DUP (0H)	; pad
	PUBLIC _DYNUST_VEH_PATH_ATT_MODULE_mp_DESTDEMANDOK
_DYNUST_VEH_PATH_ATT_MODULE_mp_DESTDEMANDOK	DD	0
	DD	0
	DD	0
	DD	128
	DD	1
	DD	0
	DD	0
	DD	0
	DD	0
	DD 1 DUP (0H)	; pad
	PUBLIC _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLODZONE
_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLODZONE	DD	0
	DD	0
	DD	0
	DD	128
	DD	1
	DD	0
	DD	0
	DD	0
	DD	0
	DD 1 DUP (0H)	; pad
	PUBLIC _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLODTMP
_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLODTMP	DD	0
	DD	0
	DD	0
	DD	128
	DD	2
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	PUBLIC _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLOD
_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLOD	DD	0
	DD	0
	DD	0
	DD	128
	DD	2
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	PUBLIC _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHAIN
_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHAIN	DD	0
	DD	0
	DD	0
	DD	128
	DD	2
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	PUBLIC _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY
_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY	DD	0
	DD	0
	DD	0
	DD	128
	DD	1
	DD	0
	DD	0
	DD	0
	DD	0
	DD 1 DUP (0H)	; pad
.T254_.0.3	DD	0
	DD	0
	DD	0
	DD	128
	DD	1
	DD	0
	DD	0
	DD	0
	DD	0
	DD 1 DUP (0H)	; pad
.T328_.0.4	DD	0
	DD	0
	DD	0
	DD	128
	DD	1
	DD	0
	DD	0
	DD	0
	DD	0
_DATA	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_2	DB	68
	DB	69
	DB	65
	DB	76
	DB	76
	DB	79
	DB	67
	DB	65
	DB	84
	DB	69
	DB	32
	DB	118
	DB	101
	DB	104
	DB	97
	DB	116
	DB	116
	DB	95
	DB	65
	DB	114
	DB	114
	DB	97
	DB	121
	DB	37
	DB	65
	DB	32
	DB	118
	DB	101
	DB	99
	DB	116
	DB	111
	DB	114
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_66.0.11	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_67.0.11	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_68	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_4	DB	68
	DB	69
	DB	65
	DB	76
	DB	76
	DB	79
	DB	67
	DB	65
	DB	84
	DB	69
	DB	32
	DB	118
	DB	101
	DB	104
	DB	97
	DB	116
	DB	116
	DB	95
	DB	65
	DB	114
	DB	114
	DB	97
	DB	121
	DB	37
	DB	80
	DB	32
	DB	118
	DB	101
	DB	99
	DB	116
	DB	111
	DB	114
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_63.0.10	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_64.0.10	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_65	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_0	DB	86
	DB	104
	DB	99
	DB	65
	DB	116
	DB	116
	DB	32
	DB	68
	DB	101
	DB	115
	DB	116
	DB	111
	DB	114
	DB	121
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_69.0.13	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_70	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_32	DB	86
	DB	104
	DB	99
	DB	65
	DB	116
	DB	116
	DB	32
	DB	83
	DB	101
	DB	116
	DB	117
	DB	112
	DB	32
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_35	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_24	DB	97
	DB	108
	DB	108
	DB	111
	DB	99
	DB	97
	DB	116
	DB	101
	DB	32
	DB	118
	DB	101
	DB	104
	DB	97
	DB	116
	DB	116
	DB	95
	DB	65
	DB	114
	DB	114
	DB	97
	DB	121
	DB	37
	DB	80
	DB	32
	DB	118
	DB	101
	DB	99
	DB	116
	DB	111
	DB	114
	DB	32
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_42.0.3	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_43	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_30	DB	97
	DB	108
	DB	108
	DB	111
	DB	99
	DB	97
	DB	116
	DB	101
	DB	32
	DB	116
	DB	109
	DB	112
	DB	80
	DB	32
	DB	118
	DB	101
	DB	99
	DB	116
	DB	111
	DB	114
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_36.0.3	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_37	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_28	DB	68
	DB	69
	DB	65
	DB	76
	DB	76
	DB	79
	DB	67
	DB	65
	DB	84
	DB	69
	DB	32
	DB	118
	DB	101
	DB	104
	DB	97
	DB	116
	DB	116
	DB	95
	DB	49
	DB	68
	DB	65
	DB	114
	DB	114
	DB	97
	DB	121
	DB	32
	DB	118
	DB	101
	DB	99
	DB	116
	DB	111
	DB	114
	DB	32
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_38.0.3	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_39	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_26	DB	97
	DB	108
	DB	108
	DB	111
	DB	99
	DB	97
	DB	116
	DB	101
	DB	32
	DB	118
	DB	101
	DB	104
	DB	97
	DB	116
	DB	116
	DB	95
	DB	65
	DB	114
	DB	114
	DB	97
	DB	121
	DB	37
	DB	80
	DB	32
	DB	118
	DB	101
	DB	99
	DB	116
	DB	111
	DB	114
	DB	32
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_40.0.3	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_41	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_16	DB	97
	DB	108
	DB	108
	DB	111
	DB	99
	DB	97
	DB	116
	DB	101
	DB	32
	DB	118
	DB	101
	DB	104
	DB	97
	DB	116
	DB	116
	DB	95
	DB	65
	DB	114
	DB	114
	DB	97
	DB	121
	DB	37
	DB	65
	DB	32
	DB	118
	DB	101
	DB	99
	DB	116
	DB	111
	DB	114
	DB	32
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_50.0.4	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_51	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_22	DB	97
	DB	108
	DB	108
	DB	111
	DB	99
	DB	97
	DB	116
	DB	101
	DB	32
	DB	116
	DB	109
	DB	112
	DB	65
	DB	32
	DB	118
	DB	101
	DB	99
	DB	116
	DB	111
	DB	114
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_44.0.4	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_45	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_20	DB	68
	DB	69
	DB	65
	DB	76
	DB	76
	DB	79
	DB	67
	DB	65
	DB	84
	DB	69
	DB	32
	DB	118
	DB	101
	DB	104
	DB	97
	DB	116
	DB	116
	DB	95
	DB	65
	DB	114
	DB	114
	DB	97
	DB	121
	DB	37
	DB	65
	DB	32
	DB	118
	DB	101
	DB	99
	DB	116
	DB	111
	DB	114
	DB	32
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_46.0.4	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_47	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_18	DB	97
	DB	108
	DB	108
	DB	111
	DB	99
	DB	97
	DB	116
	DB	101
	DB	32
	DB	118
	DB	101
	DB	104
	DB	97
	DB	116
	DB	116
	DB	95
	DB	65
	DB	114
	DB	114
	DB	97
	DB	121
	DB	37
	DB	65
	DB	32
	DB	118
	DB	101
	DB	99
	DB	116
	DB	111
	DB	114
	DB	32
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_48.0.4	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_49	DB	0
	DB 3 DUP ( 0H)	; pad
_2il0floatpacket.21	DD	042c80000H
_2il0floatpacket.22	DD	080000000H
_2il0floatpacket.23	DD	04b000000H
_2il0floatpacket.24	DD	03f000000H
_2il0floatpacket.25	DD	0bf000000H
__STRLITPACK_14	DB	86
	DB	104
	DB	99
	DB	65
	DB	116
	DB	116
	DB	32
	DB	71
	DB	101
	DB	116
	DB	86
	DB	97
	DB	108
	DB	117
	DB	101
	DB	32
	DB	118
	DB	101
	DB	99
	DB	116
	DB	111
	DB	114
	DB	32
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_12	DB	73
	DB	110
	DB	100
	DB	101
	DB	120
	DB	49
	DB	68
	DB	32
	DB	61
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_10	DB	118
	DB	101
	DB	104
	DB	97
	DB	116
	DB	116
	DB	95
	DB	65
	DB	114
	DB	114
	DB	97
	DB	121
	DB	40
	DB	105
	DB	116
	DB	41
	DB	37
	DB	80
	DB	83
	DB	105
	DB	122
	DB	101
	DB	32
	DB	61
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_58	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_8	DB	103
	DB	101
	DB	116
	DB	32
	DB	118
	DB	101
	DB	104
	DB	97
	DB	116
	DB	116
	DB	95
	DB	118
	DB	97
	DB	108
	DB	117
	DB	101
	DB	32
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_60	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_6	DB	82
	DB	101
	DB	116
	DB	117
	DB	114
	DB	110
	DB	32
	DB	118
	DB	101
	DB	104
	DB	97
	DB	116
	DB	116
	DB	95
	DB	86
	DB	97
	DB	108
	DB	117
	DB	101
	DB	32
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_62	DB	0
	DB 3 DUP ( 0H)	; pad
_2il0floatpacket.29	DD	03f800000H
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
	COMM _DYNUST_VEH_PATH_ATT_MODULE_mp_DEMANDMIDCOUNTER:BYTE:4
	COMM _DYNUST_VEH_PATH_ATT_MODULE_mp_MOP:BYTE:4
	COMM _DYNUST_VEH_PATH_ATT_MODULE_mp_VLGCOUNTER:BYTE:4
	COMM _DYNUST_VEH_PATH_ATT_MODULE_mp_VECTORERROR:BYTE:4
	COMM _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAYSIZE:BYTE:4
_DATA	ENDS
EXTRN	_for_write_seq_lis_xmit:PROC
EXTRN	_for_allocate:PROC
EXTRN	_for_dealloc_allocatable:PROC
EXTRN	_for_stop_core:PROC
EXTRN	_for_write_seq_lis:PROC
EXTRN	_for_alloc_allocatable:PROC
EXTRN	_for_check_mult_overflow:PROC
EXTRN	__fltused:BYTE
	INCLUDELIB <ifconsol>
	INCLUDELIB <libifcoremt>
	INCLUDELIB <libifport>
	INCLUDELIB <libmmt>
	INCLUDELIB <LIBCMT>
	INCLUDELIB <libirc>
	INCLUDELIB <svml_dispmt>
	INCLUDELIB <OLDNAMES>
	END
