      SUBROUTINE INITIALIZE 
! --
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~ Copyright (C) 2002-2011 The Arizona Board of Regents on Behalf of The University of Arizona.  ~~!
!~~ Developed by Yi-Chang Chiu, Ph.D. as par to the DynusT project.                               ~~!
!~~ This program is free software: you can redistribute it and/or modify it                       ~~!
!~~ under the terms of the GNU General Public License version 2 as published by                   ~~!
!~~ the Free Software Foundation.                                                                 ~~!
!~~ This program is distributed in the hope that it will be useful,                               ~~!
!~~ but WITHOUT ANY WARRANTY; without even the implied warranty of                                ~~!
!~~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                                          ~~!
!~~ See the GNU General Public License for more details.                                          ~~!
!~~ You should have received a copy of the GNU General Public License                             ~~!
!~~ along with this program (in the file COPYING).                                                ~~!
!~~ IF not, see <http://www.gnu.org/licenses/>.                                                   ~~!
!~~ Please see http://www.dynust.net for contact information and names                            ~~!
!~~ of individual contributors.                                                                   ~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
      USE DYNUST_MAIN_MODULE
      USE DYNUST_VEH_MODULE
      USE DYNUST_NETWORK_MODULE
      USE DYNUST_VEH_PATH_ATT_MODULE
!> > > > > > > > > >                < < < < < < < < < < <
!>>>>>>>>>>>>>>>>>>>CODES START HERE<<<<<<<<<<<<<<<<<<<<<
!> > > > > > > > > >                < < < < < < < < < < <

      IniID1 = .false.
      IniID2 = .false.      
      GuiTotalTime = 0.0
      MaxMove=0
      vehicle_length=20
	  ranct = 0

	IF(iteration == 0) THEN
       iso_ok=0
       iue_ok=0
       ier_ok=0
       jtotal = 0
	ENDIF
	justveh=0
      justveh_i=0
      multi=0
      listtotal=0
      int_d=10
      itag0=0
      itag1=0
      itag2=0
      itag3=0
      information=0
      information_1=0
      information_2=0
      information_3=0
      noinformation=0
      noinformation_1=0
      noinformation_2=0
      noinformation_3=0
      dtotal1=0.0
      dtotal2=0.0
      dtotal2_1=0.0
      dtotal2_2=0.0
      dtotal2_3=0.0
      dtotal3=0.0
      dtotal3_1=0.0
      dtotal3_2=0.0
      dtotal3_3=0.0
      vtothr1=0
      vtothr2=0
      vtothr2_1=0
      vtothr2_2=0
      vtothr2_3=0
      vtothr3=0
      vtothr3_1=0
      vtothr3_2=0
      vtothr3_3=0
      triptime1=0
      triptime2=0
      triptime2_1=0
      triptime2_2=0
      triptime2_3=0
      triptime3=0
      triptime3_1=0
      triptime3_2=0
      triptime3_3=0
      ave_trip1=0
      ave_trip2=0
      ave_trip2_1=0
      ave_trip2_2=0
      ave_trip2_3=0
      ave_trip3=0
      ave_trip3_1=0
      ave_trip3_2=0
      ave_trip3_3=0
      STOPtime=0
      STOPinfo=0
      STOPinfo_1=0
      STOPinfo_2=0
      STOPinfo_3=0
      STOPnoinfo=0
      STOPnoinfo_1=0
      STOPnoinfo_2=0
      STOPnoinfo_3=0
      aveSTOPtime=0
      aveSTOPinfo=0
      aveSTOPinfo_1=0
      aveSTOPinfo_2=0
      aveSTOPinfo_3=0
      aveSTOPnoinfo=0
      aveSTOPnoinfo_1=0
      aveSTOPnoinfo_2=0
      aveSTOPnoinfo_3=0
      entry_queue1=0
      entry_queue2=0
      entry_queue2_1=0
      entry_queue2_2=0
      entry_queue2_3=0
      entry_queue3=0
      entry_queue3_1=0
      entry_queue3_2=0
      entry_queue3_3=0
      ave_entry1=0
      ave_entry2=0
      ave_entry2_1=0
      ave_entry2_2=0
      ave_entry2_3=0
      ave_entry3=0
      ave_entry3_1=0
      ave_entry3_2=0
      ave_entry3_3=0
      avedtotal1=0
      avedtotal2=0
      avedtotal2_1=0
      avedtotal2_2=0
      avedtotal2_3=0
      avedtotal3=0
      avedtotal3_1=0
      avedtotal3_2=0
      avedtotal3_3=0
      totaldecsion=0
      totalswitch=0
      nout_tag=0
      nout_nontag=0
      nout_tag_i=0
      nout_nontag_i=0
      vavg1=0
      vavg2=0
      vavg2_1=0
      vavg2_2=0
      vavg2_3=0
      vavg3=0
      vavg3_1=0
      vavg3_2=0
      vavg3_3=0
      tt=0.0
      ttt=0.0

      t=0.0
      time_now=0.0
      isigcount=1
      ipint=1
      numcars=0
      oldnumcars=0
      NoofBuses=0
      vms_num=0
      inci_num=0
      jrestore = 1
      maxintervals=1
      int=0
      nout_tag=0
      nout_nontag=0

     IF(iteration > 0) THEN
      m_dynust_network_arc_nde(:)%ForToBackLink=0
      m_dynust_network_arc_nde(:)%TTimeOfBackLink=0.0
      m_dynust_network_arc_nde(:)%iunod=0
      m_dynust_network_arc_nde(:)%UNodeOfBackLink=0
      m_dynust_network_arcmove_nde(:,:)%move=0
      m_dynust_network_arcmove_nde(:,:)%movein=0
      m_dynust_network_arcmove_nde(:,:)%penalty=0.0
      almov(:,:)=0
      m_dynust_network_node_nde(:)%IntoOutNodeNum=0
      cma(:)=0.0
      cmalink(:)=0
      destination(:)=0
	  MasterDest(:)=0
      leftcapWb(:,:,:)=0.0
      leftcapWOb(:,:,:,:)=0.0
	  classpro(:)=0.0
      classpro2(:)=0.0
      nodetmp(:)=0
      m_dynust_veh_nde(:)%icurrnt=1

    ENDIF
      price_regular=0.0
      price_hov_lov=0.0
      price_hov_hov=0.0
      iactual_lov_hot=0
      iactual_hov_hot=0
      iactual_lov_ohot=0
      iactual_hov_ohot=0
      time_lov_hot=0.0
      time_hov_hot=0.0
      time_lov_ohot=0.0
      time_hov_ohot=0.0
      link_hot=0
      naout_ah=0
      mmtt=0
      ktotal_out=0
  
      CntDemTime = 0
      SignCount = 0

      KspMemOnCALL = .False.
	KspSubOnCALL = .False.
    mop = 1
	IF(iteration == 0) demandmidcounter = 0
    IF(iteration == 0) THEN
	  AutoCT = 0
	  TruckCT = 0
    ENDIF

    callksp1=.false.
    
    numNtag = 0
    
    MaxMove = 0
    
  
    IF(iteration > 0) m_dynust_last_stand(:)%expense = 0
    
    CallKspReadVeh = .false.
    END SUBROUTINE