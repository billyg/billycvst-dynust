; mark_description "Intel(R) Visual Fortran Compiler XE for applications running on IA-32, Version 13.1.1.171 Build 20130313";
; mark_description "-S";
	.686P
 	.387
	OPTION DOTNAME
	ASSUME	CS:FLAT,DS:FLAT,SS:FLAT
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
TXTST0:
; -- Begin  _CAL_TOLL
; mark_begin;
IF @Version GE 800
  .MMX
ELSEIF @Version GE 612
  .MMX
  MMWORD TEXTEQU <QWORD>
ENDIF
IF @Version GE 800
  .XMM
ELSEIF @Version GE 614
  .XMM
  XMMWORD TEXTEQU <OWORD>
ENDIF
       ALIGN     16
	PUBLIC _CAL_TOLL
_CAL_TOLL	PROC NEAR 
.B1.1:                          ; Preds .B1.0
        push      ebp                                           ;1.12
        mov       ebp, esp                                      ;1.12
        and       esp, -16                                      ;1.12
        push      esi                                           ;1.12
        push      edi                                           ;1.12
        push      ebx                                           ;1.12
        sub       esp, 212                                      ;1.12
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NTOLLLINK] ;29.4
        test      edi, edi                                      ;29.17
        jle       .B1.66        ; Prob 16%                      ;29.17
                                ; LOE edi
.B1.2:                          ; Preds .B1.1
        cvtss2sd  xmm2, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TOLLVOTH] ;48.100
        cvtss2sd  xmm5, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TOLLVOTT] ;47.100
        cvtss2sd  xmm3, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TOLLVOTA] ;34.46
        imul      ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32], -152 ;
        imul      edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TOLLLINK+32], -52 ;
        imul      eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32], -900 ;
        movsd     xmm6, QWORD PTR [_2il0floatpacket.1]          ;46.108
        movaps    xmm1, xmm3                                    ;46.108
        movaps    xmm0, xmm5                                    ;47.108
        movaps    xmm7, xmm2                                    ;48.108
        movss     xmm4, DWORD PTR [_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT] ;44.39
        divsd     xmm1, xmm6                                    ;46.108
        divsd     xmm0, xmm6                                    ;47.108
        divsd     xmm7, xmm6                                    ;48.108
        cvtsd2ss  xmm1, xmm1                                    ;46.108
        cvtsd2ss  xmm0, xmm0                                    ;47.108
        cvtsd2ss  xmm7, xmm7                                    ;48.108
        cvtsi2ss  xmm6, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIMPERAGG] ;
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_AGGINT] ;45.14
        mov       ebx, 1                                        ;
        add       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;
        add       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TOLLLINK] ;
        add       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;
        mov       DWORD PTR [56+esp], 52                        ;
        mov       DWORD PTR [140+esp], esi                      ;
        movss     DWORD PTR [152+esp], xmm7                     ;
        movss     DWORD PTR [156+esp], xmm0                     ;
        movss     DWORD PTR [148+esp], xmm1                     ;
        movss     DWORD PTR [164+esp], xmm6                     ;
        movsd     QWORD PTR [96+esp], xmm3                      ;
        movss     DWORD PTR [160+esp], xmm4                     ;
        movsd     QWORD PTR [88+esp], xmm5                      ;
        movsd     QWORD PTR [80+esp], xmm2                      ;
        mov       DWORD PTR [108+esp], eax                      ;
        mov       DWORD PTR [64+esp], edx                       ;
        mov       DWORD PTR [68+esp], ecx                       ;
        mov       DWORD PTR [72+esp], edi                       ;
        mov       esi, DWORD PTR [56+esp]                       ;
                                ; LOE ebx esi
.B1.3:                          ; Preds .B1.64 .B1.2
        mov       edx, DWORD PTR [64+esp]                       ;32.17
        mov       ecx, DWORD PTR [68+esp]                       ;32.13
        imul      eax, DWORD PTR [8+esi+edx], 152               ;32.17
        mov       edx, DWORD PTR [12+esi+edx]                   ;33.16
        test      edx, edx                                      ;33.16
        mov       ecx, DWORD PTR [12+ecx+eax]                   ;32.13
        jle       .B1.64        ; Prob 2%                       ;33.16
                                ; LOE edx ecx ebx esi
.B1.4:                          ; Preds .B1.3
        mov       edi, DWORD PTR [64+esp]                       ;
        mov       eax, 1                                        ;
        mov       DWORD PTR [192+esp], eax                      ;
        mov       DWORD PTR [136+esp], edx                      ;32.13
        imul      eax, DWORD PTR [48+esi+edi], -24              ;
        imul      ecx, ecx, 900                                 ;32.13
        add       eax, DWORD PTR [16+esi+edi]                   ;
        mov       DWORD PTR [104+esp], ecx                      ;32.13
        mov       DWORD PTR [132+esp], eax                      ;32.13
        mov       DWORD PTR [56+esp], esi                       ;32.13
        mov       DWORD PTR [60+esp], ebx                       ;32.13
                                ; LOE
.B1.5:                          ; Preds .B1.62 .B1.4
        mov       ebx, DWORD PTR [192+esp]                      ;34.49
        mov       eax, DWORD PTR [132+esp]                      ;34.17
        pxor      xmm0, xmm0                                    ;34.46
        lea       ebx, DWORD PTR [ebx+ebx*2]                    ;34.49
        movss     xmm1, DWORD PTR [8+eax+ebx*8]                 ;34.17
        comiss    xmm1, xmm0                                    ;34.46
        jbe       .B1.7         ; Prob 50%                      ;34.46
                                ; LOE eax ebx al ah xmm1
.B1.6:                          ; Preds .B1.5
        movsd     xmm0, QWORD PTR [_2il0floatpacket.2]          ;34.63
        comisd    xmm0, QWORD PTR [96+esp]                      ;34.63
        ja        .B1.67        ; Prob 5%                       ;34.63
                                ; LOE eax ebx al ah xmm1
.B1.7:                          ; Preds .B1.5 .B1.6
        pxor      xmm2, xmm2                                    ;37.50
        movss     xmm0, DWORD PTR [12+eax+ebx*8]                ;37.21
        comiss    xmm0, xmm2                                    ;37.50
        jbe       .B1.9         ; Prob 50%                      ;37.50
                                ; LOE eax ebx al ah xmm0 xmm1
.B1.8:                          ; Preds .B1.7
        movsd     xmm2, QWORD PTR [_2il0floatpacket.2]          ;37.67
        comisd    xmm2, QWORD PTR [88+esp]                      ;37.67
        ja        .B1.70        ; Prob 5%                       ;37.67
                                ; LOE eax ebx al ah xmm0 xmm1
.B1.9:                          ; Preds .B1.7 .B1.8
        pxor      xmm2, xmm2                                    ;40.51
        movss     xmm3, DWORD PTR [16+eax+ebx*8]                ;40.22
        comiss    xmm3, xmm2                                    ;40.51
        jbe       .B1.11        ; Prob 50%                      ;40.51
                                ; LOE ebx xmm0 xmm1 xmm3
.B1.10:                         ; Preds .B1.9
        movsd     xmm2, QWORD PTR [_2il0floatpacket.2]          ;40.68
        comisd    xmm2, QWORD PTR [80+esp]                      ;40.68
        ja        .B1.73        ; Prob 5%                       ;40.68
                                ; LOE ebx xmm0 xmm1 xmm3
.B1.11:                         ; Preds .B1.72 .B1.69 .B1.88 .B1.10 .B1.9
                                ;      
        mov       ecx, DWORD PTR [132+esp]                      ;44.34
        mov       edx, 1                                        ;44.14
        movss     DWORD PTR [204+esp], xmm3                     ;
        movss     xmm7, DWORD PTR [160+esp]                     ;44.34
        movss     xmm3, DWORD PTR [ecx+ebx*8]                   ;44.34
        divss     xmm3, xmm7                                    ;44.34
        movss     xmm2, DWORD PTR [164+esp]                     ;44.34
        divss     xmm3, xmm2                                    ;44.34
        movss     xmm6, DWORD PTR [_2il0floatpacket.3]          ;44.34
        andps     xmm6, xmm3                                    ;44.34
        pxor      xmm3, xmm6                                    ;44.34
        movss     DWORD PTR [200+esp], xmm0                     ;
        movaps    xmm5, xmm3                                    ;44.34
        movss     DWORD PTR [196+esp], xmm1                     ;
        movaps    xmm1, xmm3                                    ;44.34
        movss     xmm0, DWORD PTR [_2il0floatpacket.4]          ;44.34
        cmpltss   xmm1, xmm0                                    ;44.34
        andps     xmm1, xmm0                                    ;44.34
        movss     xmm0, DWORD PTR [_2il0floatpacket.5]          ;44.34
        addss     xmm5, xmm1                                    ;44.34
        subss     xmm5, xmm1                                    ;44.34
        movaps    xmm1, xmm0                                    ;44.34
        addss     xmm1, xmm0                                    ;44.34
        movaps    xmm4, xmm5                                    ;44.34
        subss     xmm4, xmm3                                    ;44.34
        movaps    xmm3, xmm4                                    ;44.34
        cmpless   xmm4, DWORD PTR [_2il0floatpacket.6]          ;44.34
        cmpnless  xmm3, xmm0                                    ;44.34
        andps     xmm3, xmm1                                    ;44.34
        andps     xmm4, xmm1                                    ;44.34
        subss     xmm5, xmm3                                    ;44.34
        movss     xmm3, DWORD PTR [4+ecx+ebx*8]                 ;45.34
        divss     xmm3, xmm7                                    ;45.34
        divss     xmm3, xmm2                                    ;45.34
        movss     xmm2, DWORD PTR [_2il0floatpacket.3]          ;45.34
        addss     xmm5, xmm4                                    ;44.34
        andps     xmm2, xmm3                                    ;45.34
        orps      xmm5, xmm6                                    ;44.34
        pxor      xmm3, xmm2                                    ;45.34
        cvtss2si  eax, xmm5                                     ;44.34
        movss     xmm4, DWORD PTR [_2il0floatpacket.4]          ;45.34
        movaps    xmm5, xmm3                                    ;45.34
        test      eax, eax                                      ;44.14
        cmpltss   xmm5, xmm4                                    ;45.34
        andps     xmm4, xmm5                                    ;45.34
        movaps    xmm5, xmm3                                    ;45.34
        cmovle    eax, edx                                      ;44.14
        addss     xmm5, xmm4                                    ;45.34
        mov       ebx, DWORD PTR [140+esp]                      ;45.14
        subss     xmm5, xmm4                                    ;45.34
        movaps    xmm4, xmm5                                    ;45.34
        subss     xmm4, xmm3                                    ;45.34
        movss     xmm3, DWORD PTR [204+esp]                     ;46.14
        movaps    xmm6, xmm4                                    ;45.34
        cmpless   xmm4, DWORD PTR [_2il0floatpacket.6]          ;45.34
        cmpnless  xmm6, xmm0                                    ;45.34
        andps     xmm6, xmm1                                    ;45.34
        andps     xmm4, xmm1                                    ;45.34
        movss     xmm1, DWORD PTR [196+esp]                     ;46.14
        subss     xmm5, xmm6                                    ;45.34
        divss     xmm1, DWORD PTR [148+esp]                     ;46.14
        movss     xmm0, DWORD PTR [200+esp]                     ;46.14
        addss     xmm5, xmm4                                    ;45.34
        orps      xmm5, xmm2                                    ;45.34
        cvtss2si  edx, xmm5                                     ;45.34
        cmp       edx, ebx                                      ;45.14
        cmovge    edx, ebx                                      ;45.14
        sub       edx, eax                                      ;46.43
        js        .B1.28        ; Prob 50%                      ;46.14
                                ; LOE eax edx xmm0 xmm1 xmm3
.B1.12:                         ; Preds .B1.11
        lea       ebx, DWORD PTR [1+edx]                        ;46.14
        cmp       ebx, 8                                        ;46.14
        jl        .B1.75        ; Prob 10%                      ;46.14
                                ; LOE eax edx ebx xmm0 xmm1 xmm3
.B1.13:                         ; Preds .B1.12
        mov       DWORD PTR [120+esp], ebx                      ;
        mov       ebx, DWORD PTR [104+esp]                      ;46.14
        mov       esi, DWORD PTR [108+esp]                      ;46.14
        mov       DWORD PTR [124+esp], edx                      ;
        mov       edi, DWORD PTR [848+ebx+esi]                  ;46.14
        mov       ecx, DWORD PTR [852+ebx+esi]                  ;46.14
        imul      ecx, edi                                      ;46.14
        mov       edx, DWORD PTR [840+ebx+esi]                  ;46.14
        mov       ebx, DWORD PTR [808+ebx+esi]                  ;46.14
        shl       edx, 2                                        ;46.14
        sub       ebx, ecx                                      ;46.14
        mov       ecx, edi                                      ;46.14
        sub       ecx, edx                                      ;46.14
        add       ecx, ebx                                      ;46.14
        mov       DWORD PTR [180+esp], edx                      ;46.14
        mov       DWORD PTR [172+esp], ebx                      ;46.14
        mov       edx, DWORD PTR [124+esp]                      ;46.14
        mov       ebx, DWORD PTR [120+esp]                      ;46.14
        lea       esi, DWORD PTR [ecx+eax*4]                    ;46.14
        and       esi, 15                                       ;46.14
        je        .B1.16        ; Prob 50%                      ;46.14
                                ; LOE eax edx ebx esi edi dl bl dh bh xmm0 xmm1 xmm3
.B1.14:                         ; Preds .B1.13
        test      esi, 3                                        ;46.14
        jne       .B1.75        ; Prob 10%                      ;46.14
                                ; LOE eax edx ebx esi edi dl bl dh bh xmm0 xmm1 xmm3
.B1.15:                         ; Preds .B1.14
        neg       esi                                           ;46.14
        add       esi, 16                                       ;46.14
        shr       esi, 2                                        ;46.14
                                ; LOE eax edx ebx esi edi dl bl dh bh xmm0 xmm1 xmm3
.B1.16:                         ; Preds .B1.15 .B1.13
        lea       ecx, DWORD PTR [8+esi]                        ;46.14
        cmp       ebx, ecx                                      ;46.14
        jl        .B1.75        ; Prob 10%                      ;46.14
                                ; LOE eax edx ebx esi edi dl bl dh bh xmm0 xmm1 xmm3
.B1.17:                         ; Preds .B1.16
        mov       ecx, ebx                                      ;46.14
        add       edi, DWORD PTR [172+esp]                      ;
        sub       ecx, esi                                      ;46.14
        sub       edi, DWORD PTR [180+esp]                      ;
        and       ecx, 7                                        ;46.14
        neg       ecx                                           ;46.14
        add       ecx, ebx                                      ;46.14
        test      esi, esi                                      ;46.14
        lea       edi, DWORD PTR [edi+eax*4]                    ;
        jbe       .B1.21        ; Prob 11%                      ;46.14
                                ; LOE eax edx ecx ebx esi edi dl bl dh bh xmm0 xmm1 xmm3
.B1.18:                         ; Preds .B1.17
        mov       DWORD PTR [116+esp], 0                        ;
        mov       DWORD PTR [76+esp], eax                       ;
        mov       eax, DWORD PTR [116+esp]                      ;
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1 xmm3
.B1.19:                         ; Preds .B1.19 .B1.18
        movss     DWORD PTR [edi+eax*4], xmm1                   ;46.14
        inc       eax                                           ;46.14
        cmp       eax, esi                                      ;46.14
        jb        .B1.19        ; Prob 82%                      ;46.14
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1 xmm3
.B1.20:                         ; Preds .B1.19
        mov       eax, DWORD PTR [76+esp]                       ;
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1 xmm3
.B1.21:                         ; Preds .B1.17 .B1.20
        movaps    xmm2, xmm1                                    ;46.14
        shufps    xmm2, xmm2, 0                                 ;46.14
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1 xmm2 xmm3
.B1.22:                         ; Preds .B1.22 .B1.21
        movaps    XMMWORD PTR [edi+esi*4], xmm2                 ;46.14
        movaps    XMMWORD PTR [16+edi+esi*4], xmm2              ;46.14
        add       esi, 8                                        ;46.14
        cmp       esi, ecx                                      ;46.14
        jb        .B1.22        ; Prob 82%                      ;46.14
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1 xmm2 xmm3
.B1.24:                         ; Preds .B1.22 .B1.75
        cmp       ecx, ebx                                      ;46.14
        jae       .B1.28        ; Prob 11%                      ;46.14
                                ; LOE eax edx ecx ebx xmm0 xmm1 xmm3
.B1.25:                         ; Preds .B1.24
        mov       DWORD PTR [120+esp], ebx                      ;
        mov       DWORD PTR [128+esp], ecx                      ;
        mov       ecx, DWORD PTR [104+esp]                      ;46.14
        mov       ebx, DWORD PTR [108+esp]                      ;46.14
        mov       DWORD PTR [124+esp], edx                      ;
        mov       edx, DWORD PTR [852+ecx+ebx]                  ;46.14
        imul      edx, DWORD PTR [848+ecx+ebx]                  ;
        mov       edi, DWORD PTR [840+ecx+ebx]                  ;46.14
        neg       edi                                           ;
        mov       esi, DWORD PTR [808+ecx+ebx]                  ;46.14
        add       edi, eax                                      ;
        sub       esi, edx                                      ;
        add       esi, DWORD PTR [848+ecx+ebx]                  ;
        mov       edx, DWORD PTR [124+esp]                      ;
        mov       ecx, DWORD PTR [128+esp]                      ;
        mov       ebx, DWORD PTR [120+esp]                      ;
        lea       esi, DWORD PTR [esi+edi*4]                    ;
                                ; LOE eax edx ecx ebx esi xmm0 xmm1 xmm3
.B1.26:                         ; Preds .B1.26 .B1.25
        movss     DWORD PTR [esi+ecx*4], xmm1                   ;46.14
        inc       ecx                                           ;46.14
        cmp       ecx, ebx                                      ;46.14
        jb        .B1.26        ; Prob 82%                      ;46.14
                                ; LOE eax edx ecx ebx esi xmm0 xmm1 xmm3
.B1.28:                         ; Preds .B1.26 .B1.11 .B1.24
        divss     xmm0, DWORD PTR [156+esp]                     ;47.14
        test      edx, edx                                      ;47.14
        js        .B1.45        ; Prob 50%                      ;47.14
                                ; LOE eax edx xmm0 xmm3
.B1.29:                         ; Preds .B1.28
        lea       ecx, DWORD PTR [1+edx]                        ;47.14
        mov       DWORD PTR [144+esp], ecx                      ;47.14
        cmp       ecx, 8                                        ;47.14
        jl        .B1.78        ; Prob 10%                      ;47.14
                                ; LOE eax edx xmm0 xmm3
.B1.30:                         ; Preds .B1.29
        mov       esi, DWORD PTR [104+esp]                      ;47.14
        mov       edi, DWORD PTR [108+esp]                      ;47.14
        mov       DWORD PTR [124+esp], edx                      ;
        mov       DWORD PTR [76+esp], eax                       ;
        mov       ebx, DWORD PTR [848+esi+edi]                  ;47.14
        mov       edx, DWORD PTR [852+esi+edi]                  ;47.14
        imul      edx, ebx                                      ;47.14
        mov       eax, DWORD PTR [840+esi+edi]                  ;47.14
        lea       ecx, DWORD PTR [ebx+ebx]                      ;47.14
        shl       eax, 2                                        ;47.14
        mov       DWORD PTR [188+esp], ebx                      ;47.14
        sub       ecx, eax                                      ;47.14
        mov       ebx, DWORD PTR [808+esi+edi]                  ;47.14
        sub       ebx, edx                                      ;47.14
        add       ecx, ebx                                      ;47.14
        mov       DWORD PTR [168+esp], eax                      ;47.14
        mov       eax, DWORD PTR [76+esp]                       ;47.14
        mov       edx, DWORD PTR [124+esp]                      ;47.14
        mov       DWORD PTR [176+esp], ebx                      ;47.14
        lea       ecx, DWORD PTR [ecx+eax*4]                    ;47.14
        and       ecx, 15                                       ;47.14
        je        .B1.33        ; Prob 50%                      ;47.14
                                ; LOE eax edx ecx al dl ah dh xmm0 xmm3
.B1.31:                         ; Preds .B1.30
        test      cl, 3                                         ;47.14
        jne       .B1.78        ; Prob 10%                      ;47.14
                                ; LOE eax edx ecx al dl ah dh xmm0 xmm3
.B1.32:                         ; Preds .B1.31
        neg       ecx                                           ;47.14
        add       ecx, 16                                       ;47.14
        shr       ecx, 2                                        ;47.14
                                ; LOE eax edx ecx al dl ah dh xmm0 xmm3
.B1.33:                         ; Preds .B1.32 .B1.30
        lea       ebx, DWORD PTR [8+ecx]                        ;47.14
        cmp       ebx, DWORD PTR [144+esp]                      ;47.14
        jg        .B1.78        ; Prob 10%                      ;47.14
                                ; LOE eax edx ecx al dl ah dh xmm0 xmm3
.B1.34:                         ; Preds .B1.33
        mov       esi, DWORD PTR [144+esp]                      ;47.14
        mov       ebx, esi                                      ;47.14
        sub       ebx, ecx                                      ;47.14
        and       ebx, 7                                        ;47.14
        neg       ebx                                           ;47.14
        mov       edi, DWORD PTR [176+esp]                      ;
        add       ebx, esi                                      ;47.14
        mov       esi, DWORD PTR [188+esp]                      ;
        lea       edi, DWORD PTR [edi+esi*2]                    ;
        sub       edi, DWORD PTR [168+esp]                      ;
        test      ecx, ecx                                      ;47.14
        lea       esi, DWORD PTR [edi+eax*4]                    ;
        jbe       .B1.38        ; Prob 11%                      ;47.14
                                ; LOE eax edx ecx ebx esi al dl ah dh xmm0 xmm3
.B1.35:                         ; Preds .B1.34
        mov       DWORD PTR [112+esp], 0                        ;
        mov       edi, DWORD PTR [112+esp]                      ;
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm3
.B1.36:                         ; Preds .B1.36 .B1.35
        movss     DWORD PTR [esi+edi*4], xmm0                   ;47.14
        inc       edi                                           ;47.14
        cmp       edi, ecx                                      ;47.14
        jb        .B1.36        ; Prob 82%                      ;47.14
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm3
.B1.38:                         ; Preds .B1.36 .B1.34
        movaps    xmm1, xmm0                                    ;47.14
        shufps    xmm1, xmm1, 0                                 ;47.14
                                ; LOE eax edx ecx ebx esi xmm0 xmm1 xmm3
.B1.39:                         ; Preds .B1.39 .B1.38
        movaps    XMMWORD PTR [esi+ecx*4], xmm1                 ;47.14
        movaps    XMMWORD PTR [16+esi+ecx*4], xmm1              ;47.14
        add       ecx, 8                                        ;47.14
        cmp       ecx, ebx                                      ;47.14
        jb        .B1.39        ; Prob 82%                      ;47.14
                                ; LOE eax edx ecx ebx esi xmm0 xmm1 xmm3
.B1.41:                         ; Preds .B1.39 .B1.78
        cmp       ebx, DWORD PTR [144+esp]                      ;47.14
        jae       .B1.44        ; Prob 11%                      ;47.14
                                ; LOE eax edx ebx xmm0 xmm3
.B1.42:                         ; Preds .B1.41
        mov       DWORD PTR [124+esp], edx                      ;
        mov       edx, DWORD PTR [104+esp]                      ;47.14
        mov       ecx, DWORD PTR [108+esp]                      ;47.14
        mov       esi, DWORD PTR [852+edx+ecx]                  ;47.14
        neg       esi                                           ;
        add       esi, 2                                        ;
        imul      esi, DWORD PTR [848+edx+ecx]                  ;
        mov       edi, DWORD PTR [840+edx+ecx]                  ;47.14
        neg       edi                                           ;
        add       edi, eax                                      ;
        add       esi, DWORD PTR [808+edx+ecx]                  ;
        mov       edx, DWORD PTR [124+esp]                      ;
        lea       ecx, DWORD PTR [esi+edi*4]                    ;
        mov       esi, DWORD PTR [144+esp]                      ;
                                ; LOE eax edx ecx ebx esi xmm0 xmm3
.B1.43:                         ; Preds .B1.43 .B1.42
        movss     DWORD PTR [ecx+ebx*4], xmm0                   ;47.14
        inc       ebx                                           ;47.14
        cmp       ebx, esi                                      ;47.14
        jb        .B1.43        ; Prob 82%                      ;47.14
                                ; LOE eax edx ecx ebx esi xmm0 xmm3
.B1.44:                         ; Preds .B1.41 .B1.43
        test      edx, edx                                      ;47.14
                                ; LOE eax edx xmm3
.B1.45:                         ; Preds .B1.28 .B1.44
        divss     xmm3, DWORD PTR [152+esp]                     ;48.14
        js        .B1.62        ; Prob 50%                      ;48.14
                                ; LOE eax edx xmm3
.B1.46:                         ; Preds .B1.45
        inc       edx                                           ;48.14
        cmp       edx, 8                                        ;48.14
        jl        .B1.81        ; Prob 10%                      ;48.14
                                ; LOE eax edx xmm3
.B1.47:                         ; Preds .B1.46
        mov       esi, DWORD PTR [104+esp]                      ;48.14
        mov       edi, DWORD PTR [108+esp]                      ;48.14
        mov       DWORD PTR [76+esp], eax                       ;
        mov       DWORD PTR [124+esp], edx                      ;
        mov       ebx, DWORD PTR [848+esi+edi]                  ;48.14
        mov       eax, DWORD PTR [852+esi+edi]                  ;48.14
        imul      eax, ebx                                      ;48.14
        mov       ecx, DWORD PTR [840+esi+edi]                  ;48.14
        lea       edx, DWORD PTR [ebx+ebx*2]                    ;48.14
        shl       ecx, 2                                        ;48.14
        mov       ebx, DWORD PTR [808+esi+edi]                  ;48.14
        mov       DWORD PTR [184+esp], edx                      ;48.14
        sub       ebx, eax                                      ;48.14
        sub       edx, ecx                                      ;48.14
        add       edx, ebx                                      ;48.14
        mov       eax, DWORD PTR [76+esp]                       ;48.14
        lea       esi, DWORD PTR [edx+eax*4]                    ;48.14
        mov       edx, DWORD PTR [124+esp]                      ;48.14
        and       esi, 15                                       ;48.14
        je        .B1.50        ; Prob 50%                      ;48.14
                                ; LOE eax edx ecx ebx esi al dl ah dh xmm3
.B1.48:                         ; Preds .B1.47
        test      esi, 3                                        ;48.14
        jne       .B1.81        ; Prob 10%                      ;48.14
                                ; LOE eax edx ecx ebx esi al dl ah dh xmm3
.B1.49:                         ; Preds .B1.48
        neg       esi                                           ;48.14
        add       esi, 16                                       ;48.14
        shr       esi, 2                                        ;48.14
                                ; LOE eax edx ecx ebx esi al dl ah dh xmm3
.B1.50:                         ; Preds .B1.49 .B1.47
        lea       edi, DWORD PTR [8+esi]                        ;48.14
        cmp       edx, edi                                      ;48.14
        jl        .B1.81        ; Prob 10%                      ;48.14
                                ; LOE eax edx ecx ebx esi al dl ah dh xmm3
.B1.51:                         ; Preds .B1.50
        add       ebx, DWORD PTR [184+esp]                      ;
        mov       edi, edx                                      ;48.14
        sub       edi, esi                                      ;48.14
        sub       ebx, ecx                                      ;
        and       edi, 7                                        ;48.14
        neg       edi                                           ;48.14
        add       edi, edx                                      ;48.14
        test      esi, esi                                      ;48.14
        lea       ebx, DWORD PTR [ebx+eax*4]                    ;
        jbe       .B1.55        ; Prob 11%                      ;48.14
                                ; LOE eax edx ebx esi edi al dl ah dh xmm3
.B1.52:                         ; Preds .B1.51
        xor       ecx, ecx                                      ;
                                ; LOE eax edx ecx ebx esi edi xmm3
.B1.53:                         ; Preds .B1.53 .B1.52
        movss     DWORD PTR [ebx+ecx*4], xmm3                   ;48.14
        inc       ecx                                           ;48.14
        cmp       ecx, esi                                      ;48.14
        jb        .B1.53        ; Prob 82%                      ;48.14
                                ; LOE eax edx ecx ebx esi edi xmm3
.B1.55:                         ; Preds .B1.53 .B1.51
        movaps    xmm0, xmm3                                    ;48.14
        shufps    xmm0, xmm0, 0                                 ;48.14
                                ; LOE eax edx ebx esi edi xmm0 xmm3
.B1.56:                         ; Preds .B1.56 .B1.55
        movaps    XMMWORD PTR [ebx+esi*4], xmm0                 ;48.14
        movaps    XMMWORD PTR [16+ebx+esi*4], xmm0              ;48.14
        add       esi, 8                                        ;48.14
        cmp       esi, edi                                      ;48.14
        jb        .B1.56        ; Prob 82%                      ;48.14
                                ; LOE eax edx ebx esi edi xmm0 xmm3
.B1.58:                         ; Preds .B1.56 .B1.81
        cmp       edi, edx                                      ;48.14
        jae       .B1.62        ; Prob 11%                      ;48.14
                                ; LOE eax edx edi xmm3
.B1.59:                         ; Preds .B1.58
        mov       ecx, DWORD PTR [104+esp]                      ;48.14
        mov       ebx, DWORD PTR [108+esp]                      ;
        mov       esi, DWORD PTR [852+ecx+ebx]                  ;48.14
        neg       esi                                           ;
        add       esi, 3                                        ;
        imul      esi, DWORD PTR [848+ecx+ebx]                  ;
        sub       eax, DWORD PTR [840+ecx+ebx]                  ;
        add       esi, DWORD PTR [808+ecx+ebx]                  ;
        lea       eax, DWORD PTR [esi+eax*4]                    ;
                                ; LOE eax edx edi xmm3
.B1.60:                         ; Preds .B1.60 .B1.59
        movss     DWORD PTR [eax+edi*4], xmm3                   ;48.14
        inc       edi                                           ;48.14
        cmp       edi, edx                                      ;48.14
        jb        .B1.60        ; Prob 82%                      ;48.14
                                ; LOE eax edx edi xmm3
.B1.62:                         ; Preds .B1.60 .B1.58 .B1.45
        mov       eax, DWORD PTR [192+esp]                      ;33.16
        inc       eax                                           ;33.16
        mov       DWORD PTR [192+esp], eax                      ;33.16
        cmp       eax, DWORD PTR [136+esp]                      ;33.16
        jle       .B1.5         ; Prob 82%                      ;33.16
                                ; LOE
.B1.63:                         ; Preds .B1.62
        mov       esi, DWORD PTR [56+esp]                       ;
        mov       ebx, DWORD PTR [60+esp]                       ;
                                ; LOE ebx esi
.B1.64:                         ; Preds .B1.63 .B1.3
        inc       ebx                                           ;31.7
        add       esi, 52                                       ;31.7
        cmp       ebx, DWORD PTR [72+esp]                       ;31.7
        jle       .B1.3         ; Prob 82%                      ;31.7
                                ; LOE ebx esi
.B1.66:                         ; Preds .B1.64 .B1.1
        add       esp, 212                                      ;53.1
        pop       ebx                                           ;53.1
        pop       edi                                           ;53.1
        pop       esi                                           ;53.1
        mov       esp, ebp                                      ;53.1
        pop       ebp                                           ;53.1
        ret                                                     ;53.1
                                ; LOE
.B1.67:                         ; Preds .B1.6                   ; Infreq
        mov       DWORD PTR [esp], 0                            ;35.16
        lea       edx, DWORD PTR [esp]                          ;35.16
        mov       DWORD PTR [48+esp], 35                        ;35.16
        lea       eax, DWORD PTR [48+esp]                       ;35.16
        mov       DWORD PTR [52+esp], OFFSET FLAT: __STRLITPACK_4 ;35.16
        push      32                                            ;35.16
        push      eax                                           ;35.16
        push      OFFSET FLAT: __STRLITPACK_6.0.1               ;35.16
        push      -2088435968                                   ;35.16
        push      911                                           ;35.16
        push      edx                                           ;35.16
        movss     DWORD PTR [220+esp], xmm1                     ;35.16
        call      _for_write_seq_lis                            ;35.16
                                ; LOE ebx
.B1.68:                         ; Preds .B1.67                  ; Infreq
        xor       eax, eax                                      ;36.7
        push      32                                            ;36.7
        push      eax                                           ;36.7
        push      eax                                           ;36.7
        push      -2088435968                                   ;36.7
        push      eax                                           ;36.7
        push      OFFSET FLAT: __STRLITPACK_7                   ;36.7
        call      _for_stop_core                                ;36.7
                                ; LOE ebx
.B1.86:                         ; Preds .B1.68                  ; Infreq
        movss     xmm1, DWORD PTR [244+esp]                     ;
        add       esp, 48                                       ;36.7
                                ; LOE ebx xmm1
.B1.69:                         ; Preds .B1.86                  ; Infreq
        mov       eax, DWORD PTR [132+esp]                      ;47.70
        movss     xmm0, DWORD PTR [12+eax+ebx*8]                ;47.70
        movss     xmm3, DWORD PTR [16+eax+ebx*8]                ;48.70
        jmp       .B1.11        ; Prob 100%                     ;48.70
                                ; LOE ebx xmm0 xmm1 xmm3
.B1.70:                         ; Preds .B1.8                   ; Infreq
        mov       DWORD PTR [esp], 0                            ;38.16
        lea       edx, DWORD PTR [esp]                          ;38.16
        mov       DWORD PTR [40+esp], 36                        ;38.16
        lea       eax, DWORD PTR [40+esp]                       ;38.16
        mov       DWORD PTR [44+esp], OFFSET FLAT: __STRLITPACK_2 ;38.16
        push      32                                            ;38.16
        push      eax                                           ;38.16
        push      OFFSET FLAT: __STRLITPACK_8.0.1               ;38.16
        push      -2088435968                                   ;38.16
        push      911                                           ;38.16
        push      edx                                           ;38.16
        movss     DWORD PTR [224+esp], xmm0                     ;38.16
        movss     DWORD PTR [220+esp], xmm1                     ;38.16
        call      _for_write_seq_lis                            ;38.16
                                ; LOE ebx
.B1.71:                         ; Preds .B1.70                  ; Infreq
        xor       eax, eax                                      ;39.7
        push      32                                            ;39.7
        push      eax                                           ;39.7
        push      eax                                           ;39.7
        push      -2088435968                                   ;39.7
        push      eax                                           ;39.7
        push      OFFSET FLAT: __STRLITPACK_9                   ;39.7
        call      _for_stop_core                                ;39.7
                                ; LOE ebx
.B1.87:                         ; Preds .B1.71                  ; Infreq
        movss     xmm1, DWORD PTR [244+esp]                     ;
        movss     xmm0, DWORD PTR [248+esp]                     ;
        add       esp, 48                                       ;39.7
                                ; LOE ebx xmm0 xmm1
.B1.72:                         ; Preds .B1.87                  ; Infreq
        mov       eax, DWORD PTR [132+esp]                      ;48.70
        movss     xmm3, DWORD PTR [16+eax+ebx*8]                ;48.70
        jmp       .B1.11        ; Prob 100%                     ;48.70
                                ; LOE ebx xmm0 xmm1 xmm3
.B1.73:                         ; Preds .B1.10                  ; Infreq
        mov       DWORD PTR [esp], 0                            ;41.16
        lea       edx, DWORD PTR [esp]                          ;41.16
        mov       DWORD PTR [32+esp], 36                        ;41.16
        lea       eax, DWORD PTR [32+esp]                       ;41.16
        mov       DWORD PTR [36+esp], OFFSET FLAT: __STRLITPACK_0 ;41.16
        push      32                                            ;41.16
        push      eax                                           ;41.16
        push      OFFSET FLAT: __STRLITPACK_10.0.1              ;41.16
        push      -2088435968                                   ;41.16
        push      911                                           ;41.16
        push      edx                                           ;41.16
        movss     DWORD PTR [228+esp], xmm3                     ;41.16
        movss     DWORD PTR [224+esp], xmm0                     ;41.16
        movss     DWORD PTR [220+esp], xmm1                     ;41.16
        call      _for_write_seq_lis                            ;41.16
                                ; LOE ebx
.B1.74:                         ; Preds .B1.73                  ; Infreq
        xor       eax, eax                                      ;42.7
        push      32                                            ;42.7
        push      eax                                           ;42.7
        push      eax                                           ;42.7
        push      -2088435968                                   ;42.7
        push      eax                                           ;42.7
        push      OFFSET FLAT: __STRLITPACK_11                  ;42.7
        call      _for_stop_core                                ;42.7
                                ; LOE ebx
.B1.88:                         ; Preds .B1.74                  ; Infreq
        movss     xmm1, DWORD PTR [244+esp]                     ;
        movss     xmm0, DWORD PTR [248+esp]                     ;
        movss     xmm3, DWORD PTR [252+esp]                     ;
        add       esp, 48                                       ;42.7
        jmp       .B1.11        ; Prob 100%                     ;42.7
                                ; LOE ebx xmm0 xmm1 xmm3
.B1.75:                         ; Preds .B1.12 .B1.16 .B1.14    ; Infreq
        xor       ecx, ecx                                      ;46.14
        jmp       .B1.24        ; Prob 100%                     ;46.14
                                ; LOE eax edx ecx ebx xmm0 xmm1 xmm3
.B1.78:                         ; Preds .B1.29 .B1.33 .B1.31    ; Infreq
        xor       ebx, ebx                                      ;47.14
        jmp       .B1.41        ; Prob 100%                     ;47.14
                                ; LOE eax edx ebx xmm0 xmm3
.B1.81:                         ; Preds .B1.46 .B1.50 .B1.48    ; Infreq
        xor       edi, edi                                      ;48.14
        jmp       .B1.58        ; Prob 100%                     ;48.14
        ALIGN     16
                                ; LOE eax edx edi xmm3
; mark_end;
_CAL_TOLL ENDP
_TEXT	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
__STRLITPACK_6.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_8.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_10.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _CAL_TOLL
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _TOLL_LINK_PRICING
; mark_begin;
       ALIGN     16
	PUBLIC _TOLL_LINK_PRICING
_TOLL_LINK_PRICING	PROC NEAR 
; parameter 1: 8 + ebp
; parameter 2: 12 + ebp
; parameter 3: 16 + ebp
; parameter 4: 20 + ebp
.B2.1:                          ; Preds .B2.0
        push      ebp                                           ;56.12
        mov       ebp, esp                                      ;56.12
        and       esp, -16                                      ;56.12
        push      esi                                           ;56.12
        push      edi                                           ;56.12
        push      ebx                                           ;56.12
        sub       esp, 244                                      ;56.12
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_COST+44] ;66.4
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_COST+36] ;66.4
        test      edx, edx                                      ;66.4
        mov       DWORD PTR [4+esp], eax                        ;66.4
        jle       .B2.3         ; Prob 10%                      ;66.4
                                ; LOE edx
.B2.2:                          ; Preds .B2.1
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_COST+24] ;66.4
        test      esi, esi                                      ;66.4
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_COST+32] ;66.4
        mov       ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_COST]  ;105.18
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_COST+40] ;105.18
        jg        .B2.44        ; Prob 50%                      ;66.4
                                ; LOE eax edx ebx esi edi
.B2.3:                          ; Preds .B2.71 .B2.47 .B2.2 .B2.1
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NTOLLLINK] ;68.4
        test      eax, eax                                      ;68.17
        jle       .B2.43        ; Prob 16%                      ;68.17
                                ; LOE eax
.B2.4:                          ; Preds .B2.3
        mov       esi, DWORD PTR [8+ebp]                        ;56.12
        mov       edi, DWORD PTR [16+ebp]                       ;56.12
        movss     xmm5, DWORD PTR [_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT] ;85.38
        mov       esi, DWORD PTR [esi]                          ;72.13
        mov       DWORD PTR [208+esp], esi                      ;72.13
        mov       esi, DWORD PTR [edi]                          ;85.24
        xor       edi, edi                                      ;
        dec       esi                                           ;85.24
        imul      esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_EPOCPERIOD] ;85.24
        movss     xmm4, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TOLLVOTT] ;108.100
        movss     xmm0, DWORD PTR [_2il0floatpacket.11]         ;105.108
        movaps    xmm2, xmm4                                    ;108.108
        movss     xmm1, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TOLLVOTA] ;72.25
        movss     DWORD PTR [32+esp], xmm1                      ;72.25
        cvtsi2ss  xmm3, esi                                     ;85.24
        divss     xmm2, xmm0                                    ;108.108
        divss     xmm3, xmm5                                    ;85.24
        mov       ecx, DWORD PTR [20+ebp]                       ;56.12
        divss     xmm1, xmm0                                    ;105.108
        cvtsi2ss  xmm6, DWORD PTR [ecx]                         ;93.20
        movss     DWORD PTR [152+esp], xmm6                     ;93.20
        cvtsi2ss  xmm6, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIMPERAGG] ;85.64
        divss     xmm3, xmm6                                    ;85.24
        movss     xmm7, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TOLLVOTH] ;111.100
        movss     DWORD PTR [28+esp], xmm7                      ;111.100
        divss     xmm7, xmm0                                    ;111.108
        movss     DWORD PTR [100+esp], xmm2                     ;108.108
        movss     xmm2, DWORD PTR [_2il0floatpacket.13]         ;85.24
        andps     xmm2, xmm3                                    ;85.24
        pxor      xmm3, xmm2                                    ;85.24
        movss     DWORD PTR [96+esp], xmm1                      ;105.108
        movaps    xmm1, xmm3                                    ;85.24
        movss     xmm0, DWORD PTR [_2il0floatpacket.14]         ;85.24
        movss     DWORD PTR [104+esp], xmm7                     ;111.108
        cmpltss   xmm1, xmm0                                    ;85.24
        andps     xmm0, xmm1                                    ;85.24
        movaps    xmm1, xmm3                                    ;85.24
        movss     DWORD PTR [228+esp], xmm6                     ;85.64
        addss     xmm1, xmm0                                    ;85.24
        mov       ebx, DWORD PTR [12+ebp]                       ;56.12
        subss     xmm1, xmm0                                    ;85.24
        movaps    xmm0, xmm1                                    ;85.24
        mov       ebx, DWORD PTR [ebx]                          ;83.15
        subss     xmm0, xmm3                                    ;85.24
        movss     xmm3, DWORD PTR [_2il0floatpacket.15]         ;85.24
        movaps    xmm6, xmm0                                    ;85.24
        movaps    xmm7, xmm3                                    ;85.24
        cmpnless  xmm6, xmm3                                    ;85.24
        addss     xmm7, xmm3                                    ;85.24
        cmpless   xmm0, DWORD PTR [_2il0floatpacket.16]         ;85.24
        mov       DWORD PTR [232+esp], ebx                      ;83.15
        andps     xmm6, xmm7                                    ;85.24
        imul      ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32], -152 ;
        subss     xmm1, xmm6                                    ;85.24
        mov       DWORD PTR [156+esp], edi                      ;
        andps     xmm0, xmm7                                    ;85.24
        mov       DWORD PTR [212+esp], edi                      ;
        addss     xmm1, xmm0                                    ;85.24
        imul      edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TOLLLINK+32], -52 ;
        orps      xmm1, xmm2                                    ;85.24
        add       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_COST+32] ;
        shl       ecx, 2                                        ;
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_COST+40] ;105.18
        mov       DWORD PTR [200+esp], ebx                      ;
        mov       ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_COST]  ;
        sub       ebx, ecx                                      ;
        mov       ecx, DWORD PTR [4+esp]                        ;
        imul      ecx, edx                                      ;
        movss     DWORD PTR [224+esp], xmm5                     ;86.17
        movss     DWORD PTR [24+esp], xmm4                      ;86.17
        cvtss2si  esi, xmm1                                     ;85.17
        add       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TOLLLINK] ;
        mov       DWORD PTR [196+esp], edi                      ;
        mov       edi, ebx                                      ;
        sub       edi, ecx                                      ;
        mov       DWORD PTR [168+esp], edi                      ;
        mov       DWORD PTR [192+esp], 1                        ;
        mov       DWORD PTR [220+esp], esi                      ;86.17
        mov       DWORD PTR [36+esp], edx                       ;86.17
        add       edi, edx                                      ;
        mov       DWORD PTR [164+esp], edi                      ;
        mov       edi, edx                                      ;
        neg       edi                                           ;
        sub       ecx, edi                                      ;
        sub       ebx, ecx                                      ;
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ITI_NU] ;86.17
        add       ecx, esi                                      ;86.17
        mov       DWORD PTR [216+esp], ecx                      ;86.17
        mov       DWORD PTR [160+esp], ebx                      ;86.17
        mov       DWORD PTR [188+esp], eax                      ;86.17
                                ; LOE
.B2.5:                          ; Preds .B2.41 .B2.4
        imul      ebx, DWORD PTR [192+esp], 52                  ;71.13
        mov       eax, DWORD PTR [196+esp]                      ;71.17
        mov       edx, DWORD PTR [200+esp]                      ;71.13
        imul      ecx, DWORD PTR [8+eax+ebx], 152               ;71.17
        mov       esi, DWORD PTR [12+edx+ecx]                   ;71.13
        mov       DWORD PTR [204+esp], esi                      ;71.13
        cmp       DWORD PTR [208+esp], 1                        ;72.25
        je        .B2.61        ; Prob 16%                      ;72.25
                                ; LOE ebx
.B2.6:                          ; Preds .B2.5
        cmp       DWORD PTR [208+esp], 2                        ;75.29
        je        .B2.58        ; Prob 16%                      ;75.29
                                ; LOE ebx
.B2.7:                          ; Preds .B2.6
        cmp       DWORD PTR [208+esp], 3                        ;78.20
        je        .B2.55        ; Prob 16%                      ;78.20
                                ; LOE ebx
.B2.8:                          ; Preds .B2.83 .B2.84 .B2.85 .B2.61 .B2.58
                                ;       .B2.7 .B2.55
        mov       eax, DWORD PTR [196+esp]                      ;82.16
        mov       esi, DWORD PTR [12+eax+ebx]                   ;82.16
        test      esi, esi                                      ;82.16
        jle       .B2.41        ; Prob 2%                       ;82.16
                                ; LOE eax ebx esi al ah
.B2.9:                          ; Preds .B2.8
        mov       ecx, eax                                      ;
        mov       edx, DWORD PTR [160+esp]                      ;
        mov       edi, DWORD PTR [168+esp]                      ;
        imul      eax, DWORD PTR [48+ebx+ecx], -24              ;
        add       eax, DWORD PTR [16+ebx+ecx]                   ;
        mov       ebx, DWORD PTR [204+esp]                      ;
        mov       DWORD PTR [172+esp], 1                        ;
        mov       DWORD PTR [236+esp], esi                      ;
        mov       esi, DWORD PTR [212+esp]                      ;
        lea       ecx, DWORD PTR [edx+ebx*4]                    ;
        mov       DWORD PTR [184+esp], ecx                      ;
        lea       edx, DWORD PTR [edi+ebx*4]                    ;
        mov       edi, DWORD PTR [164+esp]                      ;
        mov       DWORD PTR [180+esp], edx                      ;
        lea       ebx, DWORD PTR [edi+ebx*4]                    ;
        mov       edi, DWORD PTR [172+esp]                      ;
        mov       DWORD PTR [176+esp], ebx                      ;
        mov       ebx, DWORD PTR [156+esp]                      ;
                                ; LOE eax ebx esi edi
.B2.10:                         ; Preds .B2.39 .B2.9
        cmp       DWORD PTR [232+esp], 0                        ;83.35
        lea       ecx, DWORD PTR [edi+edi*2]                    ;
        movss     xmm0, DWORD PTR [eax+ecx*8]                   ;87.40
        jle       .B2.51        ; Prob 16%                      ;83.35
                                ; LOE eax ecx ebx esi edi xmm0
.B2.11:                         ; Preds .B2.10
        movss     xmm2, DWORD PTR [4+eax+ecx*8]                 ;88.29
        mov       ebx, 1                                        ;89.17
        movss     xmm7, DWORD PTR [224+esp]                     ;88.29
        divss     xmm2, xmm7                                    ;88.29
        divss     xmm2, DWORD PTR [228+esp]                     ;88.29
        divss     xmm0, xmm7                                    ;87.35
        movss     xmm5, DWORD PTR [_2il0floatpacket.13]         ;88.29
        andps     xmm5, xmm2                                    ;88.29
        pxor      xmm2, xmm5                                    ;88.29
        movss     xmm3, DWORD PTR [_2il0floatpacket.14]         ;88.29
        movaps    xmm1, xmm2                                    ;88.29
        divss     xmm0, DWORD PTR [228+esp]                     ;87.35
        mov       edx, DWORD PTR [220+esp]                      ;89.17
        cmpltss   xmm1, xmm3                                    ;88.29
        andps     xmm1, xmm3                                    ;88.29
        movaps    xmm3, xmm2                                    ;88.29
        addss     xmm3, xmm1                                    ;88.29
        subss     xmm3, xmm1                                    ;88.29
        movss     xmm1, DWORD PTR [_2il0floatpacket.15]         ;88.29
        movaps    xmm6, xmm3                                    ;88.29
        subss     xmm6, xmm2                                    ;88.29
        movaps    xmm2, xmm1                                    ;88.29
        addss     xmm2, xmm1                                    ;88.29
        movaps    xmm4, xmm6                                    ;88.29
        cmpless   xmm6, DWORD PTR [_2il0floatpacket.16]         ;88.29
        cmpnless  xmm4, xmm1                                    ;88.29
        andps     xmm4, xmm2                                    ;88.29
        andps     xmm6, xmm2                                    ;88.29
        subss     xmm3, xmm4                                    ;88.29
        addss     xmm3, xmm6                                    ;88.29
        movss     xmm6, DWORD PTR [_2il0floatpacket.14]         ;87.35
        orps      xmm3, xmm5                                    ;88.29
        movss     xmm5, DWORD PTR [_2il0floatpacket.13]         ;87.35
        andps     xmm5, xmm0                                    ;87.35
        pxor      xmm0, xmm5                                    ;87.35
        movaps    xmm4, xmm0                                    ;87.35
        cmpltss   xmm4, xmm6                                    ;87.35
        andps     xmm6, xmm4                                    ;87.35
        movaps    xmm4, xmm0                                    ;87.35
        addss     xmm4, xmm6                                    ;87.35
        subss     xmm4, xmm6                                    ;87.35
        movaps    xmm7, xmm4                                    ;87.35
        subss     xmm7, xmm0                                    ;87.35
        movaps    xmm0, xmm7                                    ;87.35
        cmpless   xmm7, DWORD PTR [_2il0floatpacket.16]         ;87.35
        cmpnless  xmm0, xmm1                                    ;87.35
        andps     xmm0, xmm2                                    ;87.35
        andps     xmm7, xmm2                                    ;87.35
        subss     xmm4, xmm0                                    ;87.35
        addss     xmm4, xmm7                                    ;87.35
        orps      xmm4, xmm5                                    ;87.35
        cvtss2si  esi, xmm4                                     ;87.35
        test      esi, esi                                      ;89.17
        cmovle    esi, ebx                                      ;89.17
        cmp       edx, esi                                      ;89.17
        cmovge    esi, edx                                      ;89.17
        sub       esi, edx                                      ;89.33
        test      esi, esi                                      ;89.17
        cmovle    esi, ebx                                      ;89.17
        cvtss2si  ebx, xmm3                                     ;88.17
        mov       DWORD PTR [212+esp], esi                      ;89.17
        mov       esi, DWORD PTR [216+esp]                      ;90.17
        cmp       esi, ebx                                      ;90.17
        cmovl     ebx, esi                                      ;90.17
        mov       esi, DWORD PTR [212+esp]                      ;91.17
        sub       ebx, edx                                      ;90.26
        mov       edx, -1                                       ;91.17
                                ; LOE eax edx ecx ebx esi edi
.B2.12:                         ; Preds .B2.11 .B2.54 .B2.53
        test      dl, 1                                         ;102.18
        je        .B2.39        ; Prob 60%                      ;102.18
                                ; LOE eax ecx ebx esi edi
.B2.13:                         ; Preds .B2.12
        mov       DWORD PTR [48+esp], 0                         ;104.18
        cmp       DWORD PTR [208+esp], 1                        ;103.16
        jne       .B2.22        ; Prob 67%                      ;103.16
                                ; LOE eax ecx ebx esi edi
.B2.14:                         ; Preds .B2.13
        mov       DWORD PTR [40+esp], 7                         ;104.18
        lea       edx, DWORD PTR [40+esp]                       ;104.18
        mov       DWORD PTR [44+esp], OFFSET FLAT: __STRLITPACK_16 ;104.18
        push      32                                            ;104.18
        push      edx                                           ;104.18
        push      OFFSET FLAT: __STRLITPACK_30.0.2              ;104.18
        push      -2088435968                                   ;104.18
        push      511                                           ;104.18
        lea       edx, DWORD PTR [68+esp]                       ;104.18
        push      edx                                           ;104.18
        mov       DWORD PTR [172+esp], ecx                      ;104.18
        mov       DWORD PTR [144+esp], eax                      ;104.18
        call      _for_write_seq_lis                            ;104.18
                                ; LOE ebx esi edi
.B2.79:                         ; Preds .B2.14
        mov       eax, DWORD PTR [144+esp]                      ;
        mov       ecx, DWORD PTR [172+esp]                      ;
        add       esp, 24                                       ;104.18
                                ; LOE eax ecx ebx esi edi al cl ah ch
.B2.15:                         ; Preds .B2.79
        mov       edx, ebx                                      ;105.18
        sub       edx, esi                                      ;105.18
        inc       edx                                           ;105.18
        mov       DWORD PTR [124+esp], edx                      ;105.18
        test      edx, edx                                      ;105.18
        movss     xmm1, DWORD PTR [8+eax+ecx*8]                 ;105.70
        jle       .B2.39        ; Prob 50%                      ;105.18
                                ; LOE eax edx ebx esi edi al dl ah dh xmm1
.B2.16:                         ; Preds .B2.15
        mov       ecx, edx                                      ;105.18
        shr       ecx, 31                                       ;105.18
        add       ecx, edx                                      ;105.18
        sar       ecx, 1                                        ;105.18
        mov       DWORD PTR [136+esp], ecx                      ;105.18
        test      ecx, ecx                                      ;105.18
        jbe       .B2.76        ; Prob 10%                      ;105.18
                                ; LOE eax ebx esi edi al ah xmm1
.B2.17:                         ; Preds .B2.16
        mov       edx, DWORD PTR [36+esp]                       ;
        xor       ecx, ecx                                      ;
        imul      edx, esi                                      ;
        movaps    xmm0, xmm1                                    ;105.98
        divss     xmm0, DWORD PTR [96+esp]                      ;105.98
        mov       DWORD PTR [156+esp], ebx                      ;
        mov       ebx, DWORD PTR [176+esp]                      ;
        mov       DWORD PTR [212+esp], esi                      ;
        mov       DWORD PTR [120+esp], eax                      ;
        add       ebx, edx                                      ;
        add       edx, DWORD PTR [180+esp]                      ;
        mov       DWORD PTR [108+esp], ebx                      ;
        mov       DWORD PTR [172+esp], edi                      ;
        mov       eax, ecx                                      ;
        mov       ebx, DWORD PTR [108+esp]                      ;
        mov       esi, DWORD PTR [136+esp]                      ;
        mov       edi, DWORD PTR [36+esp]                       ;
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1
.B2.18:                         ; Preds .B2.18 .B2.17
        movss     xmm2, DWORD PTR [edx+eax*2]                   ;105.18
        inc       ecx                                           ;105.18
        addss     xmm2, xmm0                                    ;105.18
        movss     DWORD PTR [edx+eax*2], xmm2                   ;105.18
        movss     xmm3, DWORD PTR [ebx+eax*2]                   ;105.18
        addss     xmm3, xmm0                                    ;105.18
        movss     DWORD PTR [ebx+eax*2], xmm3                   ;105.18
        add       eax, edi                                      ;105.18
        cmp       ecx, esi                                      ;105.18
        jb        .B2.18        ; Prob 64%                      ;105.18
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1
.B2.19:                         ; Preds .B2.18
        mov       ebx, DWORD PTR [156+esp]                      ;
        lea       edx, DWORD PTR [1+ecx+ecx]                    ;105.18
        mov       esi, DWORD PTR [212+esp]                      ;
        mov       eax, DWORD PTR [120+esp]                      ;
        mov       edi, DWORD PTR [172+esp]                      ;
                                ; LOE eax edx ebx esi edi al ah xmm1
.B2.20:                         ; Preds .B2.19 .B2.76
        lea       ecx, DWORD PTR [-1+edx]                       ;105.18
        cmp       ecx, DWORD PTR [124+esp]                      ;105.18
        jae       .B2.39        ; Prob 10%                      ;105.18
                                ; LOE eax edx ebx esi edi al ah xmm1
.B2.21:                         ; Preds .B2.20
        divss     xmm1, DWORD PTR [96+esp]                      ;105.98
        add       edx, esi                                      ;105.18
        imul      edx, DWORD PTR [36+esp]                       ;105.18
        mov       ecx, DWORD PTR [184+esp]                      ;105.18
        addss     xmm1, DWORD PTR [edx+ecx]                     ;105.18
        movss     DWORD PTR [edx+ecx], xmm1                     ;105.18
        jmp       .B2.39        ; Prob 100%                     ;105.18
                                ; LOE eax ebx esi edi
.B2.22:                         ; Preds .B2.13
        cmp       DWORD PTR [208+esp], 2                        ;103.16
        jne       .B2.31        ; Prob 50%                      ;103.16
                                ; LOE eax ecx ebx esi edi
.B2.23:                         ; Preds .B2.22
        mov       DWORD PTR [80+esp], 8                         ;107.18
        lea       edx, DWORD PTR [80+esp]                       ;107.18
        mov       DWORD PTR [84+esp], OFFSET FLAT: __STRLITPACK_14 ;107.18
        push      32                                            ;107.18
        push      edx                                           ;107.18
        push      OFFSET FLAT: __STRLITPACK_31.0.2              ;107.18
        push      -2088435968                                   ;107.18
        push      511                                           ;107.18
        lea       edx, DWORD PTR [68+esp]                       ;107.18
        push      edx                                           ;107.18
        mov       DWORD PTR [172+esp], ecx                      ;107.18
        mov       DWORD PTR [144+esp], eax                      ;107.18
        call      _for_write_seq_lis                            ;107.18
                                ; LOE ebx esi edi
.B2.80:                         ; Preds .B2.23
        mov       eax, DWORD PTR [144+esp]                      ;
        mov       ecx, DWORD PTR [172+esp]                      ;
        add       esp, 24                                       ;107.18
                                ; LOE eax ecx ebx esi edi al cl ah ch
.B2.24:                         ; Preds .B2.80
        mov       edx, ebx                                      ;108.18
        sub       edx, esi                                      ;108.18
        inc       edx                                           ;108.18
        mov       DWORD PTR [128+esp], edx                      ;108.18
        test      edx, edx                                      ;108.18
        movss     xmm1, DWORD PTR [12+eax+ecx*8]                ;108.70
        jle       .B2.39        ; Prob 50%                      ;108.18
                                ; LOE eax edx ebx esi edi al dl ah dh xmm1
.B2.25:                         ; Preds .B2.24
        mov       ecx, edx                                      ;108.18
        shr       ecx, 31                                       ;108.18
        add       ecx, edx                                      ;108.18
        sar       ecx, 1                                        ;108.18
        mov       DWORD PTR [140+esp], ecx                      ;108.18
        test      ecx, ecx                                      ;108.18
        jbe       .B2.49        ; Prob 10%                      ;108.18
                                ; LOE eax ebx esi edi al ah xmm1
.B2.26:                         ; Preds .B2.25
        mov       edx, DWORD PTR [36+esp]                       ;
        xor       ecx, ecx                                      ;
        imul      edx, esi                                      ;
        movaps    xmm0, xmm1                                    ;108.98
        divss     xmm0, DWORD PTR [100+esp]                     ;108.98
        mov       DWORD PTR [156+esp], ebx                      ;
        mov       ebx, DWORD PTR [176+esp]                      ;
        mov       DWORD PTR [212+esp], esi                      ;
        mov       DWORD PTR [120+esp], eax                      ;
        add       ebx, edx                                      ;
        add       edx, DWORD PTR [180+esp]                      ;
        mov       DWORD PTR [116+esp], ebx                      ;
        mov       DWORD PTR [172+esp], edi                      ;
        mov       eax, ecx                                      ;
        mov       ebx, DWORD PTR [116+esp]                      ;
        mov       esi, DWORD PTR [140+esp]                      ;
        mov       edi, DWORD PTR [36+esp]                       ;
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1
.B2.27:                         ; Preds .B2.27 .B2.26
        movss     xmm2, DWORD PTR [edx+eax*2]                   ;108.18
        inc       ecx                                           ;108.18
        addss     xmm2, xmm0                                    ;108.18
        movss     DWORD PTR [edx+eax*2], xmm2                   ;108.18
        movss     xmm3, DWORD PTR [ebx+eax*2]                   ;108.18
        addss     xmm3, xmm0                                    ;108.18
        movss     DWORD PTR [ebx+eax*2], xmm3                   ;108.18
        add       eax, edi                                      ;108.18
        cmp       ecx, esi                                      ;108.18
        jb        .B2.27        ; Prob 64%                      ;108.18
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1
.B2.28:                         ; Preds .B2.27
        mov       ebx, DWORD PTR [156+esp]                      ;
        lea       edx, DWORD PTR [1+ecx+ecx]                    ;108.18
        mov       esi, DWORD PTR [212+esp]                      ;
        mov       eax, DWORD PTR [120+esp]                      ;
        mov       edi, DWORD PTR [172+esp]                      ;
                                ; LOE eax edx ebx esi edi al ah xmm1
.B2.29:                         ; Preds .B2.28 .B2.49
        lea       ecx, DWORD PTR [-1+edx]                       ;108.18
        cmp       ecx, DWORD PTR [128+esp]                      ;108.18
        jae       .B2.39        ; Prob 10%                      ;108.18
                                ; LOE eax edx ebx esi edi al ah xmm1
.B2.30:                         ; Preds .B2.29
        divss     xmm1, DWORD PTR [100+esp]                     ;108.98
        add       edx, esi                                      ;108.18
        imul      edx, DWORD PTR [36+esp]                       ;108.18
        mov       ecx, DWORD PTR [184+esp]                      ;108.18
        addss     xmm1, DWORD PTR [edx+ecx]                     ;108.18
        movss     DWORD PTR [edx+ecx], xmm1                     ;108.18
        jmp       .B2.39        ; Prob 100%                     ;108.18
                                ; LOE eax ebx esi edi
.B2.31:                         ; Preds .B2.22
        mov       DWORD PTR [88+esp], 6                         ;110.18
        lea       edx, DWORD PTR [88+esp]                       ;110.18
        mov       DWORD PTR [92+esp], OFFSET FLAT: __STRLITPACK_12 ;110.18
        push      32                                            ;110.18
        push      edx                                           ;110.18
        push      OFFSET FLAT: __STRLITPACK_32.0.2              ;110.18
        push      -2088435968                                   ;110.18
        push      511                                           ;110.18
        lea       edx, DWORD PTR [68+esp]                       ;110.18
        push      edx                                           ;110.18
        mov       DWORD PTR [172+esp], ecx                      ;110.18
        mov       DWORD PTR [144+esp], eax                      ;110.18
        call      _for_write_seq_lis                            ;110.18
                                ; LOE ebx esi edi
.B2.81:                         ; Preds .B2.31
        mov       eax, DWORD PTR [144+esp]                      ;
        mov       ecx, DWORD PTR [172+esp]                      ;
        add       esp, 24                                       ;110.18
                                ; LOE eax ecx ebx esi edi al cl ah ch
.B2.32:                         ; Preds .B2.81
        mov       edx, ebx                                      ;111.18
        sub       edx, esi                                      ;111.18
        inc       edx                                           ;111.18
        mov       DWORD PTR [132+esp], edx                      ;111.18
        test      edx, edx                                      ;111.18
        movss     xmm1, DWORD PTR [16+eax+ecx*8]                ;111.70
        jle       .B2.39        ; Prob 50%                      ;111.18
                                ; LOE eax edx ebx esi edi al dl ah dh xmm1
.B2.33:                         ; Preds .B2.32
        mov       ecx, edx                                      ;111.18
        shr       ecx, 31                                       ;111.18
        add       ecx, edx                                      ;111.18
        sar       ecx, 1                                        ;111.18
        mov       DWORD PTR [144+esp], ecx                      ;111.18
        test      ecx, ecx                                      ;111.18
        jbe       .B2.50        ; Prob 10%                      ;111.18
                                ; LOE eax ebx esi edi al ah xmm1
.B2.34:                         ; Preds .B2.33
        mov       edx, DWORD PTR [36+esp]                       ;
        xor       ecx, ecx                                      ;
        imul      edx, esi                                      ;
        movaps    xmm0, xmm1                                    ;111.98
        divss     xmm0, DWORD PTR [104+esp]                     ;111.98
        mov       DWORD PTR [156+esp], ebx                      ;
        mov       ebx, DWORD PTR [176+esp]                      ;
        mov       DWORD PTR [212+esp], esi                      ;
        mov       DWORD PTR [120+esp], eax                      ;
        add       ebx, edx                                      ;
        add       edx, DWORD PTR [180+esp]                      ;
        mov       DWORD PTR [112+esp], ebx                      ;
        mov       DWORD PTR [172+esp], edi                      ;
        mov       eax, ecx                                      ;
        mov       ebx, DWORD PTR [112+esp]                      ;
        mov       esi, DWORD PTR [144+esp]                      ;
        mov       edi, DWORD PTR [36+esp]                       ;
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1
.B2.35:                         ; Preds .B2.35 .B2.34
        movss     xmm2, DWORD PTR [edx+eax*2]                   ;111.18
        inc       ecx                                           ;111.18
        addss     xmm2, xmm0                                    ;111.18
        movss     DWORD PTR [edx+eax*2], xmm2                   ;111.18
        movss     xmm3, DWORD PTR [ebx+eax*2]                   ;111.18
        addss     xmm3, xmm0                                    ;111.18
        movss     DWORD PTR [ebx+eax*2], xmm3                   ;111.18
        add       eax, edi                                      ;111.18
        cmp       ecx, esi                                      ;111.18
        jb        .B2.35        ; Prob 64%                      ;111.18
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1
.B2.36:                         ; Preds .B2.35
        mov       ebx, DWORD PTR [156+esp]                      ;
        lea       edx, DWORD PTR [1+ecx+ecx]                    ;111.18
        mov       esi, DWORD PTR [212+esp]                      ;
        mov       eax, DWORD PTR [120+esp]                      ;
        mov       edi, DWORD PTR [172+esp]                      ;
                                ; LOE eax edx ebx esi edi al ah xmm1
.B2.37:                         ; Preds .B2.36 .B2.50
        lea       ecx, DWORD PTR [-1+edx]                       ;111.18
        cmp       ecx, DWORD PTR [132+esp]                      ;111.18
        jae       .B2.39        ; Prob 10%                      ;111.18
                                ; LOE eax edx ebx esi edi al ah xmm1
.B2.38:                         ; Preds .B2.37
        divss     xmm1, DWORD PTR [104+esp]                     ;111.98
        add       edx, esi                                      ;111.18
        imul      edx, DWORD PTR [36+esp]                       ;111.18
        mov       ecx, DWORD PTR [184+esp]                      ;111.18
        addss     xmm1, DWORD PTR [edx+ecx]                     ;111.18
        movss     DWORD PTR [edx+ecx], xmm1                     ;111.18
                                ; LOE eax ebx esi edi
.B2.39:                         ; Preds .B2.37 .B2.24 .B2.29 .B2.15 .B2.12
                                ;       .B2.32 .B2.20 .B2.21 .B2.30 .B2.38
                                ;      
        inc       edi                                           ;82.16
        cmp       edi, DWORD PTR [236+esp]                      ;82.16
        jle       .B2.10        ; Prob 82%                      ;82.16
                                ; LOE eax ebx esi edi
.B2.40:                         ; Preds .B2.39
        mov       DWORD PTR [156+esp], ebx                      ;
        mov       DWORD PTR [212+esp], esi                      ;
                                ; LOE
.B2.41:                         ; Preds .B2.40 .B2.8
        mov       eax, DWORD PTR [192+esp]                      ;70.7
        inc       eax                                           ;70.7
        mov       DWORD PTR [192+esp], eax                      ;70.7
        cmp       eax, DWORD PTR [188+esp]                      ;70.7
        jle       .B2.5         ; Prob 82%                      ;70.7
                                ; LOE
.B2.43:                         ; Preds .B2.41 .B2.3
        add       esp, 244                                      ;118.1
        pop       ebx                                           ;118.1
        pop       edi                                           ;118.1
        pop       esi                                           ;118.1
        mov       esp, ebp                                      ;118.1
        pop       ebp                                           ;118.1
        ret                                                     ;118.1
                                ; LOE
.B2.44:                         ; Preds .B2.2
        mov       DWORD PTR [28+esp], edx                       ;
        mov       edx, esi                                      ;66.4
        and       edx, -8                                       ;66.4
        xor       ecx, ecx                                      ;
        mov       DWORD PTR [36+esp], edx                       ;66.4
        mov       edx, DWORD PTR [4+esp]                        ;
        pxor      xmm0, xmm0                                    ;66.4
        imul      edx, edi                                      ;
        mov       DWORD PTR [12+esp], ecx                       ;
        mov       ecx, ebx                                      ;
        shl       eax, 2                                        ;
        sub       ecx, eax                                      ;
        sub       eax, edx                                      ;
        add       ecx, eax                                      ;
        add       ecx, edx                                      ;
        lea       edx, DWORD PTR [esi*4]                        ;
        mov       DWORD PTR [32+esp], edx                       ;
        mov       eax, ecx                                      ;
        mov       edx, DWORD PTR [12+esp]                       ;
        mov       DWORD PTR [8+esp], ebx                        ;
        mov       DWORD PTR [esp], edx                          ;
        mov       DWORD PTR [20+esp], edi                       ;
        mov       DWORD PTR [24+esp], ebx                       ;
        mov       DWORD PTR [48+esp], esi                       ;
        mov       edx, DWORD PTR [28+esp]                       ;
        mov       edi, DWORD PTR [esp]                          ;
        mov       esi, DWORD PTR [8+esp]                        ;
        mov       ebx, DWORD PTR [12+esp]                       ;
        mov       DWORD PTR [16+esp], ecx                       ;
                                ; LOE eax ebx esi edi
.B2.45:                         ; Preds .B2.47 .B2.71 .B2.44
        cmp       DWORD PTR [48+esp], 24                        ;66.4
        jle       .B2.64        ; Prob 0%                       ;66.4
                                ; LOE eax ebx esi edi
.B2.46:                         ; Preds .B2.45
        push      DWORD PTR [32+esp]                            ;66.4
        push      0                                             ;66.4
        push      eax                                           ;66.4
        mov       DWORD PTR [12+esp], eax                       ;66.4
        call      __intel_fast_memset                           ;66.4
                                ; LOE ebx esi edi
.B2.82:                         ; Preds .B2.46
        mov       eax, DWORD PTR [12+esp]                       ;
        add       esp, 12                                       ;66.4
                                ; LOE eax ebx esi edi al ah
.B2.47:                         ; Preds .B2.68 .B2.82
        inc       ebx                                           ;66.4
        mov       edx, DWORD PTR [20+esp]                       ;66.4
        add       esi, edx                                      ;66.4
        add       eax, edx                                      ;66.4
        add       edi, edx                                      ;66.4
        cmp       ebx, DWORD PTR [28+esp]                       ;66.4
        jb        .B2.45        ; Prob 82%                      ;66.4
        jmp       .B2.3         ; Prob 100%                     ;66.4
                                ; LOE eax ebx esi edi
.B2.49:                         ; Preds .B2.25                  ; Infreq
        mov       edx, 1                                        ;
        jmp       .B2.29        ; Prob 100%                     ;
                                ; LOE eax edx ebx esi edi al ah xmm1
.B2.50:                         ; Preds .B2.33                  ; Infreq
        mov       edx, 1                                        ;
        jmp       .B2.37        ; Prob 100%                     ;
                                ; LOE eax edx ebx esi edi al ah xmm1
.B2.51:                         ; Preds .B2.10                  ; Infreq
        divss     xmm0, DWORD PTR [224+esp]                     ;93.58
        movss     xmm1, DWORD PTR [152+esp]                     ;93.25
        comiss    xmm1, xmm0                                    ;93.25
        jb        .B2.54        ; Prob 50%                      ;93.25
                                ; LOE eax ecx ebx esi edi
.B2.52:                         ; Preds .B2.51                  ; Infreq
        movss     xmm0, DWORD PTR [4+eax+ecx*8]                 ;93.84
        divss     xmm0, DWORD PTR [224+esp]                     ;93.114
        comiss    xmm0, DWORD PTR [152+esp]                     ;93.82
        jbe       .B2.54        ; Prob 50%                      ;93.82
                                ; LOE eax ecx ebx esi edi
.B2.53:                         ; Preds .B2.52                  ; Infreq
        mov       esi, 1                                        ;
        mov       edx, -1                                       ;
        mov       ebx, esi                                      ;
        jmp       .B2.12        ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi edi
.B2.54:                         ; Preds .B2.51 .B2.52           ; Infreq
        xor       edx, edx                                      ;98.18
        jmp       .B2.12        ; Prob 100%                     ;98.18
                                ; LOE eax edx ecx ebx esi edi
.B2.55:                         ; Preds .B2.7                   ; Infreq
        movss     xmm0, DWORD PTR [_2il0floatpacket.12]         ;78.38
        comiss    xmm0, DWORD PTR [28+esp]                      ;78.38
        jbe       .B2.8         ; Prob 95%                      ;78.38
                                ; LOE ebx
.B2.56:                         ; Preds .B2.55                  ; Infreq
        mov       DWORD PTR [48+esp], 0                         ;79.9
        lea       edx, DWORD PTR [48+esp]                       ;79.9
        mov       DWORD PTR [16+esp], 34                        ;79.9
        lea       eax, DWORD PTR [16+esp]                       ;79.9
        mov       DWORD PTR [20+esp], OFFSET FLAT: __STRLITPACK_18 ;79.9
        push      32                                            ;79.9
        push      eax                                           ;79.9
        push      OFFSET FLAT: __STRLITPACK_28.0.2              ;79.9
        push      -2088435968                                   ;79.9
        push      911                                           ;79.9
        push      edx                                           ;79.9
        call      _for_write_seq_lis                            ;79.9
                                ; LOE ebx
.B2.57:                         ; Preds .B2.56                  ; Infreq
        push      32                                            ;80.6
        xor       eax, eax                                      ;80.6
        push      eax                                           ;80.6
        push      eax                                           ;80.6
        push      -2088435968                                   ;80.6
        push      eax                                           ;80.6
        push      OFFSET FLAT: __STRLITPACK_29                  ;80.6
        call      _for_stop_core                                ;80.6
                                ; LOE ebx
.B2.83:                         ; Preds .B2.57                  ; Infreq
        add       esp, 48                                       ;80.6
        jmp       .B2.8         ; Prob 100%                     ;80.6
                                ; LOE ebx
.B2.58:                         ; Preds .B2.6                   ; Infreq
        movss     xmm0, DWORD PTR [_2il0floatpacket.12]         ;75.47
        comiss    xmm0, DWORD PTR [24+esp]                      ;75.47
        jbe       .B2.8         ; Prob 95%                      ;75.47
                                ; LOE ebx
.B2.59:                         ; Preds .B2.58                  ; Infreq
        mov       DWORD PTR [48+esp], 0                         ;76.15
        lea       edx, DWORD PTR [48+esp]                       ;76.15
        mov       DWORD PTR [8+esp], 36                         ;76.15
        lea       eax, DWORD PTR [8+esp]                        ;76.15
        mov       DWORD PTR [12+esp], OFFSET FLAT: __STRLITPACK_20 ;76.15
        push      32                                            ;76.15
        push      eax                                           ;76.15
        push      OFFSET FLAT: __STRLITPACK_26.0.2              ;76.15
        push      -2088435968                                   ;76.15
        push      911                                           ;76.15
        push      edx                                           ;76.15
        call      _for_write_seq_lis                            ;76.15
                                ; LOE ebx
.B2.60:                         ; Preds .B2.59                  ; Infreq
        push      32                                            ;77.6
        xor       eax, eax                                      ;77.6
        push      eax                                           ;77.6
        push      eax                                           ;77.6
        push      -2088435968                                   ;77.6
        push      eax                                           ;77.6
        push      OFFSET FLAT: __STRLITPACK_27                  ;77.6
        call      _for_stop_core                                ;77.6
                                ; LOE ebx
.B2.84:                         ; Preds .B2.60                  ; Infreq
        add       esp, 48                                       ;77.6
        jmp       .B2.8         ; Prob 100%                     ;77.6
                                ; LOE ebx
.B2.61:                         ; Preds .B2.5                   ; Infreq
        movss     xmm0, DWORD PTR [_2il0floatpacket.12]         ;72.43
        comiss    xmm0, DWORD PTR [32+esp]                      ;72.43
        jbe       .B2.8         ; Prob 95%                      ;72.43
                                ; LOE ebx
.B2.62:                         ; Preds .B2.61                  ; Infreq
        mov       DWORD PTR [48+esp], 0                         ;73.15
        lea       edx, DWORD PTR [48+esp]                       ;73.15
        mov       DWORD PTR [esp], 35                           ;73.15
        lea       eax, DWORD PTR [esp]                          ;73.15
        mov       DWORD PTR [4+esp], OFFSET FLAT: __STRLITPACK_22 ;73.15
        push      32                                            ;73.15
        push      eax                                           ;73.15
        push      OFFSET FLAT: __STRLITPACK_24.0.2              ;73.15
        push      -2088435968                                   ;73.15
        push      911                                           ;73.15
        push      edx                                           ;73.15
        call      _for_write_seq_lis                            ;73.15
                                ; LOE ebx
.B2.63:                         ; Preds .B2.62                  ; Infreq
        push      32                                            ;74.6
        xor       eax, eax                                      ;74.6
        push      eax                                           ;74.6
        push      eax                                           ;74.6
        push      -2088435968                                   ;74.6
        push      eax                                           ;74.6
        push      OFFSET FLAT: __STRLITPACK_25                  ;74.6
        call      _for_stop_core                                ;74.6
                                ; LOE ebx
.B2.85:                         ; Preds .B2.63                  ; Infreq
        add       esp, 48                                       ;74.6
        jmp       .B2.8         ; Prob 100%                     ;74.6
                                ; LOE ebx
.B2.64:                         ; Preds .B2.45                  ; Infreq
        cmp       DWORD PTR [48+esp], 8                         ;66.4
        jl        .B2.75        ; Prob 10%                      ;66.4
                                ; LOE eax ebx esi edi
.B2.65:                         ; Preds .B2.64                  ; Infreq
        xor       ecx, ecx                                      ;66.4
        mov       DWORD PTR [44+esp], ecx                       ;66.4
        pxor      xmm0, xmm0                                    ;
        mov       ecx, DWORD PTR [16+esp]                       ;
        mov       edx, DWORD PTR [36+esp]                       ;66.4
        mov       DWORD PTR [esp], eax                          ;
        mov       DWORD PTR [12+esp], ebx                       ;
        add       ecx, edi                                      ;
        mov       DWORD PTR [40+esp], ecx                       ;
        mov       ecx, DWORD PTR [24+esp]                       ;
        mov       ebx, DWORD PTR [44+esp]                       ;
        mov       DWORD PTR [8+esp], esi                        ;
        mov       esi, edx                                      ;
        add       ecx, edi                                      ;
        mov       eax, ecx                                      ;
        mov       ecx, DWORD PTR [40+esp]                       ;
                                ; LOE eax edx ecx ebx esi edi xmm0
.B2.66:                         ; Preds .B2.66 .B2.65           ; Infreq
        movups    XMMWORD PTR [eax+ebx*4], xmm0                 ;66.4
        movups    XMMWORD PTR [16+ecx+ebx*4], xmm0              ;66.4
        add       ebx, 8                                        ;66.4
        cmp       ebx, esi                                      ;66.4
        jb        .B2.66        ; Prob 82%                      ;66.4
                                ; LOE eax edx ecx ebx esi edi xmm0
.B2.67:                         ; Preds .B2.66                  ; Infreq
        mov       eax, DWORD PTR [esp]                          ;
        mov       esi, DWORD PTR [8+esp]                        ;
        mov       ebx, DWORD PTR [12+esp]                       ;
                                ; LOE eax edx ebx esi edi
.B2.68:                         ; Preds .B2.67 .B2.75           ; Infreq
        cmp       edx, DWORD PTR [48+esp]                       ;66.4
        jae       .B2.47        ; Prob 10%                      ;66.4
                                ; LOE eax edx ebx esi edi
.B2.69:                         ; Preds .B2.68                  ; Infreq
        mov       DWORD PTR [12+esp], ebx                       ;
        xor       ecx, ecx                                      ;
        mov       ebx, DWORD PTR [48+esp]                       ;
                                ; LOE eax edx ecx ebx esi edi
.B2.70:                         ; Preds .B2.70 .B2.69           ; Infreq
        mov       DWORD PTR [esi+edx*4], ecx                    ;66.4
        inc       edx                                           ;66.4
        cmp       edx, ebx                                      ;66.4
        jb        .B2.70        ; Prob 82%                      ;66.4
                                ; LOE eax edx ecx ebx esi edi
.B2.71:                         ; Preds .B2.70                  ; Infreq
        mov       ebx, DWORD PTR [12+esp]                       ;
        inc       ebx                                           ;66.4
        mov       edx, DWORD PTR [20+esp]                       ;66.4
        add       esi, edx                                      ;66.4
        add       eax, edx                                      ;66.4
        add       edi, edx                                      ;66.4
        cmp       ebx, DWORD PTR [28+esp]                       ;66.4
        jb        .B2.45        ; Prob 82%                      ;66.4
        jmp       .B2.3         ; Prob 100%                     ;66.4
                                ; LOE eax ebx esi edi
.B2.75:                         ; Preds .B2.64                  ; Infreq
        xor       edx, edx                                      ;66.4
        jmp       .B2.68        ; Prob 100%                     ;66.4
                                ; LOE eax edx ebx esi edi
.B2.76:                         ; Preds .B2.16                  ; Infreq
        mov       edx, 1                                        ;
        jmp       .B2.20        ; Prob 100%                     ;
        ALIGN     16
                                ; LOE eax edx ebx esi edi al ah xmm1
; mark_end;
_TOLL_LINK_PRICING ENDP
_TEXT	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_24.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_26.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_28.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_32.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_31.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_30.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _TOLL_LINK_PRICING
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DB 3 DUP ( 0H)	; pad
_2il0floatpacket.1	DD	000000000H,0404e0000H
_2il0floatpacket.2	DD	000000000H,03ff00000H
__STRLITPACK_4	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	84
	DB	111
	DB	108
	DB	108
	DB	46
	DB	100
	DB	97
	DB	116
	DB	44
	DB	32
	DB	65
	DB	117
	DB	116
	DB	111
	DB	32
	DB	86
	DB	79
	DB	84
	DB	32
	DB	105
	DB	115
	DB	32
	DB	122
	DB	101
	DB	114
	DB	111
	DB	0
__STRLITPACK_7	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_2	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	84
	DB	111
	DB	108
	DB	108
	DB	46
	DB	100
	DB	97
	DB	116
	DB	44
	DB	32
	DB	84
	DB	114
	DB	117
	DB	99
	DB	107
	DB	32
	DB	86
	DB	79
	DB	84
	DB	32
	DB	105
	DB	115
	DB	32
	DB	122
	DB	101
	DB	114
	DB	111
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_9	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_0	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	84
	DB	111
	DB	108
	DB	108
	DB	46
	DB	100
	DB	97
	DB	116
	DB	44
	DB	32
	DB	84
	DB	114
	DB	117
	DB	99
	DB	107
	DB	32
	DB	86
	DB	79
	DB	84
	DB	32
	DB	105
	DB	115
	DB	32
	DB	122
	DB	101
	DB	114
	DB	111
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_11	DB	0
	DB 3 DUP ( 0H)	; pad
_2il0floatpacket.0	DD	042700000H
_2il0floatpacket.3	DD	080000000H
_2il0floatpacket.4	DD	04b000000H
_2il0floatpacket.5	DD	03f000000H
_2il0floatpacket.6	DD	0bf000000H
__STRLITPACK_22	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	84
	DB	111
	DB	108
	DB	108
	DB	46
	DB	100
	DB	97
	DB	116
	DB	44
	DB	32
	DB	65
	DB	117
	DB	116
	DB	111
	DB	32
	DB	86
	DB	79
	DB	84
	DB	32
	DB	105
	DB	115
	DB	32
	DB	122
	DB	101
	DB	114
	DB	111
	DB	0
__STRLITPACK_25	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_20	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	84
	DB	111
	DB	108
	DB	108
	DB	46
	DB	100
	DB	97
	DB	116
	DB	44
	DB	32
	DB	84
	DB	114
	DB	117
	DB	99
	DB	107
	DB	32
	DB	86
	DB	79
	DB	84
	DB	32
	DB	105
	DB	115
	DB	32
	DB	122
	DB	101
	DB	114
	DB	111
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_27	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_18	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	84
	DB	111
	DB	108
	DB	108
	DB	46
	DB	100
	DB	97
	DB	116
	DB	44
	DB	32
	DB	72
	DB	79
	DB	86
	DB	32
	DB	86
	DB	79
	DB	84
	DB	32
	DB	105
	DB	115
	DB	32
	DB	122
	DB	101
	DB	114
	DB	111
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_29	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_12	DB	105
	DB	110
	DB	32
	DB	104
	DB	111
	DB	118
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_14	DB	105
	DB	110
	DB	32
	DB	116
	DB	114
	DB	117
	DB	99
	DB	107
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_16	DB	105
	DB	110
	DB	32
	DB	97
	DB	117
	DB	116
	DB	111
	DB	0
_2il0floatpacket.11	DD	042700000H
_2il0floatpacket.12	DD	03f800000H
_2il0floatpacket.13	DD	080000000H
_2il0floatpacket.14	DD	04b000000H
_2il0floatpacket.15	DD	03f000000H
_2il0floatpacket.16	DD	0bf000000H
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_AGGINT:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_SIMPERAGG:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TOLLVOTH:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TOLLVOTT:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TOLLVOTA:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TOLLLINK:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NTOLLLINK:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ITI_NU:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_EPOCPERIOD:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_COST:BYTE
_DATA	ENDS
EXTRN	_for_stop_core:PROC
EXTRN	_for_write_seq_lis:PROC
EXTRN	__intel_fast_memset:PROC
EXTRN	__fltused:BYTE
	INCLUDELIB <ifconsol>
	INCLUDELIB <libifcoremt>
	INCLUDELIB <libifport>
	INCLUDELIB <libmmt>
	INCLUDELIB <LIBCMT>
	INCLUDELIB <libirc>
	INCLUDELIB <svml_dispmt>
	INCLUDELIB <OLDNAMES>
	END
