SUBROUTINE WRITE_PRINT_TRAJECTORY(j,ExitID,FUnit,cutime)

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~ Copyright (C) 2002-2011 The Arizona Board of Regents on Behalf of The University of Arizona.  ~~!
!~~ Developed by Yi-Chang Chiu, Ph.D. as par to the DynusT project.                               ~~!
!~~ This program is free software: you can redistribute it and/or modify it                       ~~!
!~~ under the terms of the GNU General Public License version 2 as published by                   ~~!
!~~ the Free Software Foundation.                                                                 ~~!
!~~ This program is distributed in the hope that it will be useful,                               ~~!
!~~ but WITHOUT ANY WARRANTY; without even the implied warranty of                                ~~!
!~~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                                          ~~!
!~~ See the GNU General Public License for more details.                                          ~~!
!~~ You should have received a copy of the GNU General Public License                             ~~!
!~~ along with this program (in the file COPYING).                                                ~~!
!~~ IF not, see <http://www.gnu.org/licenses/>.                                                   ~~!
!~~ Please see http://www.dynust.net for contact information and names                            ~~!
!~~ of individual contributors.                                                                   ~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!

USE DYNUST_MAIN_MODULE
USE DYNUST_VEH_MODULE
USE DYNUST_NETWORK_MODULE
USE DYNUST_VEH_PATH_ATT_MODULE
INTEGER Index1D,ExitID, FUnit
REAL evalue, cutime
REAL www
!> > > > > > > > > >                < < < < < < < < < < <
!>>>>>>>>>>>>>>>>>>>CODES START HERE<<<<<<<<<<<<<<<<<<<<<
!> > > > > > > > > >                < < < < < < < < < < <

   IF(i18 > 0.or.reach_converg) THEN

!     IF(UCDAirOption > 0) CALL UCDavisAirModelOutput(j) ! UTEP generate air quality outputs for UC-Davis

     IF(ExitID == 1) THEN ! Vehicle has exited
	    tmp1=m_dynust_last_stand(j)%atime-m_dynust_last_stand(j)%stime
	    tmp1=m_dynust_veh(j)%ttilnow
	    NodeCount1 = vehatt_P_Size(j)-1
		NodeCount2 = vehatt_P_Size(j)-1
	 ELSE ! Vehicle still in
	    tmp1=time_now/60.0-m_dynust_last_stand(j)%stime
		NodeCount1 = m_dynust_veh_nde(j)%icurrnt
		NodeCount2 = m_dynust_veh_nde(j)%icurrnt-1
	 ENDIF
     

     WRITE(Funit,1890) j,m_dynust_veh(j)%itag,m_dynust_last_stand(j)%jorig,m_dynust_last_stand(j)%jdest,m_dynust_last_stand(j)%vehclass,m_dynust_network_node_nde(m_dynust_network_arc_nde(m_dynust_last_stand(j)%isec)%iunod)%IntoOutNodeNum,m_dynust_network_node_nde(m_dynust_network_arc_nde(m_dynust_last_stand(j)%isec)%idnod)%IntoOutNodeNum,m_dynust_network_node_nde(nint(vehatt_Value(j,vehatt_P_Size(j)-1,1)))%IntoOutNodeNum,m_dynust_last_stand(j)%stime,tmp1,NodeCount1,m_dynust_last_stand(j)%vehtype,m_dynust_veh(j)%ioc
     WRITE(Funit,1891) (m_dynust_network_node_nde(nint(vehatt_Value(j,js,1)))%IntoOutNodeNum,js=1,NodeCount1)
     IF(NodeCount2 > 0) THEN 
        WRITE(Funit,1892) ((vehatt_Value(j,jn,3)/AttScale),jn=1,NodeCount2) ! pathtime
     ELSE
        WRITE(Funit,1892)
     ENDIF
     IF(NodeCount2 > 0) THEN
        checktemp =  vehatt_Value(j,1,3)/AttScale
        WRITE(Funit,1892) checktemp,((vehatt_Value(j,jn,3)/AttScale-vehatt_Value(j,jn-1,3)/AttScale),jn=2,NodeCount2) ! timediff
     ELSE
        WRITE(Funit,1892)
     ENDIF        
     IF(NodeCount2 > 0) THEN
        WRITE(Funit,1892) ((vehatt_Value(j,jn,2)/AttScale),jn=1,NodeCount2) ! pathSTOP
     ELSE
        WRITE(Funit,1892)
     ENDIF                


1890  FORMAT('Veh #',i9,' Tag=',i2,' OrigZ=',i5,' DestZ=',i5,' Class=',i2,' UstmN=',i7,' DownN=',i7,' DestN=',i7,' STime=',f7.2,' Total Travel Time=',f7.2,' # of Nodes=',i4,' VehType',i2,' LOO',i2)
1891  FORMAT(50i8)
1892  FORMAT(50f8.2)

!     DO ibus=1,NoofBuses
!       IF(m_dynust_bus(ibus)%busid == j) THEN
!         WRITE(188,*) 'Statistics for bus number  ',ibus,m_dynust_veh(j)%distans
!         WRITE(188,1890) j,m_dynust_veh(j)%itag,m_dynust_veh(j)%jorig,m_dynust_last_stand(j)%jdest,m_dynust_last_stand(j)%vehclass,m_dynust_network_node_nde(m_dynust_network_arc_nde(m_dynust_last_stand(j)%isec)%iunod)%IntoOutNodeNum,m_dynust_network_node_nde(m_dynust_network_arc_nde(m_dynust_last_stand(j)%isec)%idnod)%IntoOutNodeNum,m_dynust_network_node_nde(nint(vehatt_Value(j,vehatt_P_Size(j)-1,1)))%IntoOutNodeNum,m_dynust_last_stand(j)%stime,tmp1,NodeCount1,m_dynust_last_stand(j)%vehtype,m_dynust_veh(j)%ioc
!         WRITE(188,1891) (m_dynust_network_node_nde(nint(vehatt_Value(j,js,1)))%IntoOutNodeNum,js=1,NodeCount1)
!         IF(NodeCount2 > 0) THEN 
!            WRITE(188,1892) ((vehatt_Value(j,jn,3)/AttScale),jn=1,NodeCount2)
!         ELSE
!            WRITE(188,1892) 0.0         
!         ENDIF            
!         IF(NodeCount2 > 0) THEN 
!            WRITE(188,1892) ((vehatt_Value(j,jn,4)/AttScale),jn=1,NodeCount2)
!         ELSE
!            WRITE(188,1892) 
!         ENDIF             
!         IF(NodeCount2 > 0) THEN 
!            WRITE(188,1892) ((vehatt_Value(j,jn,2)/AttScale),jn=1,NodeCount2)
!         ELSE
!            WRITE(188,1892) 
!         ENDIF            
!      ENDIF
!     ENDDO
   ENDIF


END SUBROUTINE


SUBROUTINE PrintICM(FUnit,l)

USE DYNUST_MAIN_MODULE
USE DYNUST_VEH_MODULE
USE DYNUST_NETWORK_MODULE
USE DYNUST_VEH_PATH_ATT_MODULE
INTEGER Index1D,ExitID, FUnit
REAL evalue
REAL www

WRITE(Funit,*) "Time ==>", l*xminPerSimInt
    DO j = 1, justveh
    IF(.not.m_dynust_veh(j)%notin) THEN
     IF(Funit == 6059) THEN
        IF(m_dynust_last_stand(j)%vehtype == 1) WRITE(Funit,'(4i8)') j,m_dynust_last_stand(j)%jorig,m_dynust_network_node_nde(nint(vehatt_Value(j,m_dynust_veh_nde(j)%icurrnt,1)))%izone,m_dynust_last_stand(j)%jdest
     ENDIF
     IF(Funit == 6060) THEN
        IF(m_dynust_last_stand(j)%vehtype == 3) WRITE(Funit,'(4i8)') j,m_dynust_last_stand(j)%jorig,m_dynust_network_node_nde(nint(vehatt_Value(j,m_dynust_veh_nde(j)%icurrnt,1)))%izone,m_dynust_last_stand(j)%jdest
     ENDIF

    ENDIF
   ENDDO

END SUBROUTINE

SUBROUTINE PrintAlt(j,cutime)
USE DYNUST_MAIN_MODULE
USE DYNUST_VEH_MODULE
USE DYNUST_NETWORK_MODULE
USE DYNUST_VEH_PATH_ATT_MODULE
INTEGER Index1D,ExitID, FUnit, inodesum
REAL nodesum
REAL evalue, cutime
REAL www

   nodesum = 0
   DO is = 1, vehatt_P_Size(j) 
      nodesum = nodesum + nint(vehatt_Value(j,is,1))
   ENDDO
   inodesum = nodesum
   
! Write internal vehicle id out to MIVA   
   WRITE(989,'(i10,i8,1000i7)') inodesum, j, m_dynust_last_stand(j)%vehtype, m_dynust_last_stand(j)%jorig,m_dynust_last_stand(j)%jdest, vehatt_P_Size(j)+1, m_dynust_network_node_nde(m_dynust_network_arc_nde(m_dynust_last_stand(j)%isec)%iunod)%IntoOutNodeNum, (m_dynust_network_node_nde(nint(vehatt_Value(j,js,1)))%IntoOutNodeNum,js=1,vehatt_P_Size(j))
   
   IF(trajalt > 0) THEN
     WRITE(988,'(1000f8.2)') m_dynust_last_stand(j)%stime,((vehatt_Value(j,jn,3)/AttScale),jn=1,vehatt_P_Size(j)-1) ! pathtime
     
     IF(m_dynust_last_stand(j)%atime > 0) THEN ! calculate entry queue time
        ttt=max(0.0,m_dynust_last_stand(j)%atime-m_dynust_last_stand(j)%stime-m_dynust_veh(j)%ttilnow)
     ELSE
        ttt=max(0.0,cutime-m_dynust_last_stand(j)%stime-m_dynust_veh(j)%ttilnow)  
     ENDIF
     IF(ttt < 0) THEN
       WRITE(911,*) "error in printing entry queue time"
       STOP
     ENDIF
     WRITE(990,'(3i7,f8.2)') j,m_dynust_last_stand(j)%jorig,m_dynust_last_stand(j)%jdest,ttt
   ENDIF

END SUBROUTINE