SUBROUTINE CAL_LINK_MOE(l,t,tend)      
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~ Copyright (C) 2002-2011 The Arizona Board of Regents on Behalf of The University of Arizona.  ~~!
!~~ Developed by Yi-Chang Chiu, Ph.D. as par to the DynusT project.                               ~~!
!~~ This program is free software: you can redistribute it and/or modify it                       ~~!
!~~ under the terms of the GNU General Public License version 2 as published by                   ~~!
!~~ the Free Software Foundation.                                                                 ~~!
!~~ This program is distributed in the hope that it will be useful,                               ~~!
!~~ but WITHOUT ANY WARRANTY; without even the implied warranty of                                ~~!
!~~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                                          ~~!
!~~ See the GNU General Public License for more details.                                          ~~!
!~~ You should have received a copy of the GNU General Public License                             ~~!
!~~ along with this program (in the file COPYING).                                                ~~!
!~~ IF not, see <http://www.gnu.org/licenses/>.                                                   ~~!
!~~ Please see http://www.dynust.net for contact information and names                            ~~!
!~~ of individual contributors.                                                                   ~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
    USE DYNUST_MAIN_MODULE
    USE DYNUST_NETWORK_MODULE
    USE DYNUST_VEH_PATH_ATT_MODULE
    USE DYNUST_LINK_VEH_LIST_MODULE

    REAL tmparr2(noofarcs)
    REAL,ALLOCATABLE::tmparr22(:,:)
	INTEGER tmpindex
	INTEGER agstime

!> > > > > > > > > >                < < < < < < < < < < <
!>>>>>>>>>>>>>>>>>>>CODES START HERE<<<<<<<<<<<<<<<<<<<<<
!> > > > > > > > > >                < < < < < < < < < < <
	tmparr2(:)=0
	ALLOCATE(tmparr22(noofarcs,maxmove+1))
	tmparr22(:,:)=0
	tmpindex = 0
    tmparr2(:) = 0

      IF(i30 == 1) THEN
        DO i=1,noofarcs
          m_dynust_network_arc_de(i)%tmp30=m_dynust_network_arc_de(i)%tmp30+m_dynust_network_arc_de(i)%vlg
        ENDDO
       IF(mod(l,i30_t) == 0) THEN
	    tmpindex = 0
	    DO i = 1, noofarcs
	      IF (m_dynust_network_arc_nde(i)%link_iden < 99) THEN
	        tmpindex = tmpindex + 1
	        tmparr2(tmpindex) = m_dynust_network_arc_de(i)%tmp30
	      ENDIF
	    ENDDO

          WRITE(30,*)
          WRITE(30,330) l*xminPerSimInt
          WRITE(30,3101) (tmparr2(i)/i30_t,i=1,tmpindex)
	      WRITE(30,*)
          m_dynust_network_arc_de(:)%tmp30=0
      ENDIF
     ENDIF

      tmparr2(:) = 0
      IF(i301 == 1) THEN
        DO is = 1, noofarcs
        m_dynust_network_arc_de(is)%tmp301=m_dynust_network_arc_de(is)%tmp301+m_dynust_network_arc_nde(is)%s/max(0.001,m_dynust_network_arc_de(is)%v)
        ENDDO
        
       IF(mod(l,i301_t) == 0) THEN
	    tmpindex = 0
	    DO i = 1, noofarcs
	      IF (m_dynust_network_arc_nde(i)%link_iden < 99) THEN
	        tmpindex = tmpindex + 1
	        tmparr2(tmpindex) = m_dynust_network_arc_de(i)%tmp301
	      ENDIF
	    ENDDO

          WRITE(301,*)
          WRITE(301,330) l*xminPerSimInt
          WRITE(301,3101) (tmparr2(i)/i301_t,i=1,tmpindex)
	      WRITE(301,*)
          m_dynust_network_arc_de(:)%tmp301=0
        ENDIF
       ENDIF


      tmparr2(:) = 0
      IF(i31 == 1) THEN
        DO i=1,noofarcs
          m_dynust_network_arc_de(i)%tmp31=m_dynust_network_arc_de(i)%tmp31+m_dynust_network_arc_de(i)%volume
        ENDDO
        
	  IF(l > 1) THEN
        IF(mod(l,i31_t) == 0) THEN 
	    tmpindex = 0
	    DO i = 1, noofarcs
	      IF (m_dynust_network_arc_nde(i)%link_iden < 99) THEN
	        tmpindex = tmpindex + 1
	        tmparr2(tmpindex) = m_dynust_network_arc_de(i)%tmp31
	      ENDIF
	    ENDDO

          WRITE(31,*)
          WRITE(31,330) l*xminPerSimInt
          WRITE(31,3101) (tmparr2(i)/i31_t,i=1,tmpindex)
	      WRITE(31,*)
          m_dynust_network_arc_de(:)%tmp31=0
        ENDIF
      ENDIF
    ENDIF
3101  FORMAT(10f11.5)

      tmparr2(:) = 0
      IF(i32 == 1) THEN
        DO i=1,noofarcs
	    m_dynust_network_arc_de(i)%tmp32=m_dynust_network_arc_de(i)%tmp32+m_dynust_network_arc_de(i)%vehicle_queue
        ENDDO

!        IF(l > 1) THEN
        IF(mod(l,i32_t) == 0) THEN
          tmpindex = 0
	    DO i = 1, noofarcs
	      IF (m_dynust_network_arc_nde(i)%link_iden < 99) THEN
	        tmpindex = tmpindex + 1
	        tmparr2(tmpindex) = m_dynust_network_arc_de(i)%tmp32
	      ENDIF
	    ENDDO

          WRITE(32,*)
          WRITE(32,330) l*xminPerSimInt
	    WRITE(32,3201) (tmparr2(i)/i32_t,i=1,tmpindex)
          WRITE(32,*)
          m_dynust_network_arc_de(:)%tmp32=0
        ENDIF
      ENDIF
3201  FORMAT(10f11.5)


      tmparr2(:) = 0
      IF(i33 == 1) THEN
        DO i=1,noofarcs
	    m_dynust_network_arc_de(i)%tmp33=m_dynust_network_arc_de(i)%tmp33+m_dynust_network_arc_de(i)%v
        ENDDO
      
        IF(mod(l,i33_t) == 0) THEN   
          tmpindex = 0
	    DO i = 1, noofarcs
	      IF (m_dynust_network_arc_nde(i)%link_iden < 99) THEN
	        tmpindex = tmpindex + 1
	        tmparr2(tmpindex) = m_dynust_network_arc_de(i)%tmp33
	      ENDIF
	    ENDDO

	    WRITE(33,*)
          WRITE(33,330) l*xminPerSimInt
	    WRITE(33,3301) (tmparr2(i)/i33_t*60.0,i=1,tmpindex)
          WRITE(33,*)
          m_dynust_network_arc_de(:)%tmp33=0
        ENDIF
      ENDIF    
3301  FORMAT(10f11.5)
330   FORMAT(f8.1)


      tmparr2(:) = 0
      IF(i34 == 1) THEN
        DO i=1,noofarcs
         m_dynust_network_arc_de(i)%tmp34=m_dynust_network_arc_de(i)%tmp34+m_dynust_network_arc_de(i)%c
        ENDDO

        IF(mod(l,i34_t) == 0) THEN
	    tmpindex = 0
	    DO i = 1, noofarcs
	      IF (m_dynust_network_arc_nde(i)%link_iden < 99) THEN
	        tmpindex = tmpindex + 1
	        tmparr2(tmpindex) = m_dynust_network_arc_de(i)%tmp34
	      ENDIF
	    ENDDO

	    WRITE(34,*)
		WRITE(34,330) l*xminPerSimInt
	    WRITE(34,3401) (tmparr2(i)/i34_t,i=1,tmpindex)
		WRITE(34,*)
          m_dynust_network_arc_de(:)%tmp34=0
        ENDIF
      ENDIF
3401  FORMAT(10f11.5)


      tmparr2(:) = 0
      IF(i35 == 1) THEN
        DO i=1,noofarcs
          m_dynust_network_arc_de(i)%tmp35=m_dynust_network_arc_de(i)%tmp35+m_dynust_network_arc_de(i)%vtmp
        ENDDO
       
        IF(mod(l,i35_t) == 0) THEN
	    tmpindex = 0
	    DO i = 1, noofarcs
	      IF (m_dynust_network_arc_nde(i)%link_iden < 99) THEN
	        tmpindex = tmpindex + 1
	        tmparr2(tmpindex) = m_dynust_network_arc_de(i)%tmp35
	      ENDIF
	    ENDDO
	 
	    WRITE(35,*)
          WRITE(35,330) l*xminPerSimInt
    	    WRITE(35,3501) (tmparr2(i)/i35_t*60.0,i=1,tmpindex)
	    WRITE(35,*)
          m_dynust_network_arc_de(:)%tmp35=0
        ENDIF
      ENDIF
3501  FORMAT(10f11.5)


      tmparr2(:) = 0
      IF(i36 == 1) THEN
        DO i=1,noofarcs
          m_dynust_network_arc_de(i)%tmp36=m_dynust_network_arc_de(i)%tmp36+m_dynust_network_arc_de(i)%ctmp
        ENDDO

        IF(mod(l,i36_t) == 0) THEN
	    tmpindex = 0
	    DO i = 1, noofarcs
	      IF (m_dynust_network_arc_nde(i)%link_iden < 99) THEN
	        tmpindex = tmpindex + 1
	        tmparr2(tmpindex) = m_dynust_network_arc_de(i)%tmp36
	      ENDIF
	    ENDDO

	    WRITE(36,*)
          WRITE(36,330) l*xminPerSimInt
		WRITE(36,3601) (tmparr2(i)/i36_t,i=1,tmpindex)
          WRITE(36,*)
          m_dynust_network_arc_de(:)%tmp36=0
        ENDIF
      ENDIF
3601  FORMAT(10f11.5)


      tmparr2(:) = 0
      IF(i37 == 1) THEN
        DO i=1,noofarcs
          m_dynust_network_arc_de(i)%tmp37=m_dynust_network_arc_de(i)%tmp37+m_dynust_network_arc_de(i)%outleft
        ENDDO

        IF(mod(l,i37_t) == 0) THEN
	    tmpindex = 0
	    DO i = 1, noofarcs
	      IF (m_dynust_network_arc_nde(i)%link_iden < 99) THEN
	        tmpindex = tmpindex + 1
	        tmparr2(tmpindex) = m_dynust_network_arc_de(i)%tmp37
	      ENDIF
	    ENDDO

	    WRITE(37,*)
        WRITE(37,330) l*xminPerSimInt
        WRITE(37,3701) (tmparr2(i)/i37_t,i=1,tmpindex)
		WRITE(37,*)
          m_dynust_network_arc_de(:)%tmp37=0
        ENDIF
      ENDIF
3701  FORMAT(10f11.5)


      tmparr2(:) = 0
      IF(i38 == 1) THEN
        DO i=1,noofarcs
          m_dynust_network_arcmove_de(i,:)%tmp38=m_dynust_network_arcmove_de(i,:)%tmp38+m_dynust_network_arcmove_de(i,:)%green
        ENDDO

     IF(mod(l,i38_t) == 0) THEN
	    tmpindex = 0
	    DO i = 1, noofarcs
	      IF (m_dynust_network_arc_nde(i)%link_iden < 99) THEN
	        tmpindex = tmpindex + 1
	        tmparr22(tmpindex,1:maxmove) = m_dynust_network_arcmove_de(i,1:maxmove)%tmp38
	        tmparr22(tmpindex,maxmove+1) = i	        
		  ENDIF
	    ENDDO

	    WRITE(38,*)
        WRITE(38,330) l*xminPerSimInt
        ENDIF
     ENDIF


      tmparr2(:) = 0
      IF(i39 == 1) THEN
        DO i=1,noofarcs
          m_dynust_network_arc_de(i)%tmp39=m_dynust_network_arc_de(i)%tmp39+m_dynust_network_arc_de(i)%outflow
        ENDDO

        IF(mod(l,i39_t) == 0) THEN
	    tmpindex = 0
	    DO i = 1, noofarcs
	      IF (m_dynust_network_arc_nde(i)%link_iden < 99) THEN
	        tmpindex = tmpindex + 1
	        tmparr2(tmpindex) = m_dynust_network_arc_de(i)%tmp39
	      ENDIF
	    ENDDO

      	WRITE(39,*)
		WRITE(39,330) l*xminPerSimInt
  		WRITE(39,3901) (tmparr2(i)/i39_t,i=1,tmpindex)
		WRITE(39,*)
          m_dynust_network_arc_de(:)%tmp39=0
        ENDIF
      ENDIF
3901  FORMAT(10f11.5)

      tmparr2(:) = 0
      IF(i40 == 1) THEN


   IF(mod(l,nint(1/xminPerSimInt)) == 0) THEN
   agstime = ifix((l-1)*xminPerSimInt)+1

	    tmpindex = 0
	    DO i = 1, noofarcs
	      IF (m_dynust_network_arc_nde(i)%link_iden < 99) THEN
	        tmpindex = tmpindex + 1
            tmparr2(tmpindex) = sum(m_dynust_network_arc_de(i)%AccuVol(1:agstime))
	      ENDIF
	    ENDDO
      	WRITE(400,*)
		WRITE(400,330) (l)*xminPerSimInt
  		WRITE(400,4011) (tmparr2(i),i=1,tmpindex)
		WRITE(400,*)

        IF(truckok)  THEN
	    tmpindex = 0
	    DO i = 1, noofarcs
	      IF (m_dynust_network_arc_nde(i)%link_iden < 99) THEN
	        tmpindex = tmpindex + 1
            tmparr2(tmpindex) = sum(m_dynust_network_arc_de(i)%AccuVolT(1:agstime))
	      ENDIF
	    ENDDO
      	WRITE(401,*)
		WRITE(401,330) (l)*xminPerSimInt
  		WRITE(401,4011) (tmparr2(i),i=1,tmpindex)
		WRITE(401,*)
		ENDIF

        ENDIF
      ENDIF
4001  FORMAT(10f11.5)
4011  FORMAT(10f14.2)


	tmpindex = 0 
	
    DO i=1,noofarcs
      IF(m_dynust_network_arc_de(i)%xl < 0.001) THEN ! link is completed blocked
        m_dynust_network_arc_de(i)%tmp600=m_dynust_network_arc_de(i)%tmp600+min(1.0,m_dynust_network_arc_de(i)%vehicle_queue/(m_dynust_network_arc_de(i)%maxden*0.001))
      ELSE
        m_dynust_network_arc_de(i)%tmp600=m_dynust_network_arc_de(i)%tmp600+min(1.0,m_dynust_network_arc_de(i)%vehicle_queue/(m_dynust_network_arc_de(i)%maxden*m_dynust_network_arc_de(i)%xl))
      ENDIF
    ENDDO
	

  IF(mod(l,nint(1/xminPerSimInt)) == 0) THEN	 ! this is to ensure the output is every minute regardless of the simulation interval
	DO i = 1, noofarcs
	  IF (m_dynust_network_arc_nde(i)%link_iden < 99) THEN
	  tmpindex = tmpindex + 1
	  tmparr2(tmpindex)=m_dynust_network_arc_de(i)%tmp600/nint(1/xminPerSimInt)
	  IF(tmparr2(tmpindex) > 1.0) THEN
	    print *, "queue > 1.0"
	  ENDIF
	  ENDIF
      ENDDO

      WRITE(600,*)
      WRITE(600,330) (l)*xminPerSimInt
	  WRITE(600,3401) (tmparr2(i),i=1,tmpindex)
      WRITE(600,*)
      m_dynust_network_arc_de(:)%tmp600 = 0.0      
   ENDIF


    DO i=1,noofarcs
      m_dynust_network_arc_de(i)%tmp700 = m_dynust_network_arc_de(i)%tmp700 + min(float(m_dynust_network_arc_de(i)%maxden),m_dynust_network_arc_de(i)%npar/m_dynust_network_arc_de(i)%xl)
    ENDDO
   
    IF(mod(l,nint(1/xminPerSimInt)) == 0) THEN	 
      tmpindex = 0
	DO i = 1, noofarcs
	  IF (m_dynust_network_arc_nde(i)%link_iden < 99) THEN
	    tmpindex = tmpindex + 1
	    IF(m_dynust_network_arc_de(i)%xl < 0.01) THEN
	      tmparr2(tmpindex) = m_dynust_network_arc_de(i)%maxden
	    ELSE
	      tmparr2(tmpindex) = m_dynust_network_arc_de(i)%tmp700/nint(1/xminPerSimInt)
	    ENDIF
	  ENDIF
	ENDDO

      WRITE(700,*)
      WRITE(700,330) (l)*xminPerSimInt
	  WRITE(700,3401) (tmparr2(i),i=1,tmpindex)
      WRITE(700,*)
      m_dynust_network_arc_de(:)%tmp700 = 0.0
    ENDIF
    m_dynust_network_arc_de(:)%tmp900 = m_dynust_network_arc_de(:)%tmp900 + m_dynust_network_arc_de(:)%v

    IF(mod(l,nint(1/xminPerSimInt)) == 0) THEN	 
      tmpindex = 0
	DO i = 1, noofarcs
	  IF (m_dynust_network_arc_nde(i)%link_iden < 99) THEN
	    tmpindex = tmpindex + 1
	    tmparr2(tmpindex) = m_dynust_network_arc_de(i)%tmp900/nint(1/xminPerSimInt)
	  ENDIF
	ENDDO

      WRITE(900,*)
      WRITE(900,330) (l)*xminPerSimInt
	  WRITE(900,3301) (tmparr2(i)*60.0,i=1,tmpindex)
      WRITE(900,*)
      m_dynust_network_arc_de(:)%tmp900 = 0
    ENDIF      

      m_dynust_network_arc_de(:)%outflow=0
      m_dynust_network_arc_de(:)%outleft=0

DEALLOCATE(tmparr22)

END SUBROUTINE