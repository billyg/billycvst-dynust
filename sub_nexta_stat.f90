      SUBROUTINE NEXTA_STAT(l)  
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~ Copyright (C) 2002-2011 The Arizona Board of Regents on Behalf of The University of Arizona.  ~~!
!~~ Developed by Yi-Chang Chiu, Ph.D. as par to the DynusT project.                               ~~!
!~~ This program is free software: you can redistribute it and/or modify it                       ~~!
!~~ under the terms of the GNU General Public License version 2 as published by                   ~~!
!~~ the Free Software Foundation.                                                                 ~~!
!~~ This program is distributed in the hope that it will be useful,                               ~~!
!~~ but WITHOUT ANY WARRANTY; without even the implied warranty of                                ~~!
!~~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                                          ~~!
!~~ See the GNU General Public License for more details.                                          ~~!
!~~ You should have received a copy of the GNU General Public License                             ~~!
!~~ along with this program (in the file COPYING).                                                ~~!
!~~ IF not, see <http://www.gnu.org/licenses/>.                                                   ~~!
!~~ Please see http://www.dynust.net for contact information and names                            ~~!
!~~ of individual contributors.                                                                   ~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
      USE DYNUST_MAIN_MODULE
      USE DYNUST_NETWORK_MODULE
!> > > > > > > > > >                < < < < < < < < < < <
!>>>>>>>>>>>>>>>>>>>CODES START HERE<<<<<<<<<<<<<<<<<<<<<
!> > > > > > > > > >                < < < < < < < < < < <

      Avg_speed=0.0
      Avg_speed_net=0.0
      Avg_speed_free=0.0
      nu_free=0
      nu_net=0

      IF((l-1) == int_d) THEN
         close(800)
         OPEN(file='fort.800', unit=800,status='unknown')
	  ENDIF
1010     FORMAT(10i5)
      WRITE(800,'("==============================")')
      WRITE(800,'("Current Time               :",f10.1)') l*xminPerSimInt
      WRITE(800,'("Total # of Gen Vehs        :",i10)') justveh

      WRITE(800,'("Total # of Out Vehs        :",i10)') justveh-numcars

      WRITE(800,'("Total # of In Vehs         :",i10)') numcars

      tt=0.0
!     IF(justveh > 0) TT = GuiTotalTime/justveh
      IF(justveh > 0) TT = triptime1/justveh
      WRITE(800,'("Avg Travel Time All Vehs   :",f10.1)') tt 

      IF((justveh-numcars) < 1) THEN
       WRITE(800,'("Avg Travel Time for out Veh:",f10.1)') 0.0
      ELSE
       tt_out=triptime1/(justveh-numcars)
      WRITE(800,'("Avg Travel Time for out Veh:",f10.1)') tt_out 
      ENDIF

      DO j1=1,noofarcs
	  IF (m_dynust_network_arc_nde(j1)%link_iden < 99) THEN
          avg_speed=avg_speed+m_dynust_network_arc_de(j1)%v
          IF(m_dynust_network_arc_nde(j1)%link_iden == 1.or.m_dynust_network_arc_nde(j1)%link_iden == 2.or.m_dynust_network_arc_nde(j1)%link_iden == 8.or.m_dynust_network_arc_nde(j1)%link_iden == 9.or.m_dynust_network_arc_nde(j1)%link_iden == 10) THEN
            avg_speed_free=avg_speed_free+m_dynust_network_arc_de(j1)%v
            nu_free=nu_free+1
          ELSE
            avg_speed_net=avg_speed_net+m_dynust_network_arc_de(j1)%v
            nu_net=nu_net+1
          ENDIF
	  ENDIF
      ENDDO

      tot_avg = (avg_speed/noofarcs_org)*60.0

      WRITE(800,'("Avg speed for all links    :",f10.1)') tot_avg 


      IF(nu_free > 0) THEN
        avg_free = (avg_speed_free/nu_free)*60.0
        WRITE(800,'("Avg speed for freeways     :",f10.1)') avg_free
	  ELSE
	    WRITE(800,'("Avg speed for freeways     :",f10.1)') 1.0 ! set to 60mph
	  ENDIF

      IF(nu_net > 0) THEN
	  avg_net = (avg_speed_net/nu_net)*60.0
        WRITE(800,'("Avg speed for arterials    :",f10.1)') avg_net
	ENDIF 

       time_min=(time_now)/60.0


100   FORMAT(i10)
101   FORMAT(f10.1)
102   FORMAT(f10.1)
   
END SUBROUTINE
