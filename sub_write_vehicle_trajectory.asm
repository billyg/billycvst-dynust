; mark_description "Intel(R) Visual Fortran Compiler XE for applications running on IA-32, Version 13.1.1.171 Build 20130313";
; mark_description "-S";
	.686P
 	.387
	OPTION DOTNAME
	ASSUME	CS:FLAT,DS:FLAT,SS:FLAT
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
TXTST0:
; -- Begin  _WRITE_VEH_TRAJECTORY
; mark_begin;
IF @Version GE 800
  .MMX
ELSEIF @Version GE 612
  .MMX
  MMWORD TEXTEQU <QWORD>
ENDIF
IF @Version GE 800
  .XMM
ELSEIF @Version GE 614
  .XMM
  XMMWORD TEXTEQU <OWORD>
ENDIF
       ALIGN     16
	PUBLIC _WRITE_VEH_TRAJECTORY
_WRITE_VEH_TRAJECTORY	PROC NEAR 
; parameter 1: 8 + ebp
.B1.1:                          ; Preds .B1.0
        push      ebp                                           ;1.18
        mov       ebp, esp                                      ;1.18
        and       esp, -16                                      ;1.18
        push      esi                                           ;1.18
        sub       esp, 220                                      ;1.18
        mov       DWORD PTR [esp], 0                            ;29.1
        lea       esi, DWORD PTR [esp]                          ;29.1
        mov       DWORD PTR [64+esp], 45                        ;29.1
        lea       eax, DWORD PTR [64+esp]                       ;29.1
        mov       DWORD PTR [68+esp], OFFSET FLAT: __STRLITPACK_4 ;29.1
        push      32                                            ;29.1
        push      eax                                           ;29.1
        push      OFFSET FLAT: __STRLITPACK_6.0.1               ;29.1
        push      -2088435968                                   ;29.1
        push      18                                            ;29.1
        push      esi                                           ;29.1
        call      _for_write_seq_lis                            ;29.1
                                ; LOE ebx esi edi
.B1.2:                          ; Preds .B1.1
        mov       DWORD PTR [24+esp], 0                         ;30.1
        lea       eax, DWORD PTR [96+esp]                       ;30.1
        mov       DWORD PTR [96+esp], 45                        ;30.1
        mov       DWORD PTR [100+esp], OFFSET FLAT: __STRLITPACK_2 ;30.1
        push      32                                            ;30.1
        push      eax                                           ;30.1
        push      OFFSET FLAT: __STRLITPACK_7.0.1               ;30.1
        push      -2088435968                                   ;30.1
        push      18                                            ;30.1
        push      esi                                           ;30.1
        call      _for_write_seq_lis                            ;30.1
                                ; LOE ebx esi edi
.B1.3:                          ; Preds .B1.2
        mov       DWORD PTR [48+esp], 0                         ;31.1
        lea       eax, DWORD PTR [128+esp]                      ;31.1
        mov       DWORD PTR [128+esp], 45                       ;31.1
        mov       DWORD PTR [132+esp], OFFSET FLAT: __STRLITPACK_0 ;31.1
        push      32                                            ;31.1
        push      eax                                           ;31.1
        push      OFFSET FLAT: __STRLITPACK_8.0.1               ;31.1
        push      -2088435968                                   ;31.1
        push      18                                            ;31.1
        push      esi                                           ;31.1
        call      _for_write_seq_lis                            ;31.1
                                ; LOE ebx edi
.B1.35:                         ; Preds .B1.3
        add       esp, 72                                       ;31.1
                                ; LOE ebx edi
.B1.4:                          ; Preds .B1.35
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_JUSTVEH] ;34.7
        test      edx, edx                                      ;34.7
        jle       .B1.31        ; Prob 2%                       ;34.7
                                ; LOE edx ebx edi
.B1.5:                          ; Preds .B1.4
        mov       eax, DWORD PTR [8+ebp]                        ;1.18
        mov       esi, 1                                        ;
        mov       DWORD PTR [140+esp], 1                        ;34.7
        movss     xmm0, DWORD PTR [_2il0floatpacket.3]          ;36.28
        mov       DWORD PTR [156+esp], edx                      ;36.28
        mov       DWORD PTR [92+esp], edi                       ;36.28
        mov       DWORD PTR [88+esp], ebx                       ;36.28
        jmp       .B1.6         ; Prob 100%                     ;36.28
                                ; LOE esi
.B1.29:                         ; Preds .B1.28
        mov       DWORD PTR [140+esp], esi                      ;34.7
                                ; LOE esi
.B1.6:                          ; Preds .B1.29 .B1.5
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_UCDAIROPTION], 0 ;36.23
        jle       .B1.22        ; Prob 41%                      ;36.23
                                ; LOE esi
.B1.7:                          ; Preds .B1.6
        mov       DWORD PTR [32+esp], 0                         ;36.28
        lea       edx, DWORD PTR [32+esp]                       ;36.28
        mov       DWORD PTR [128+esp], esi                      ;36.28
        lea       eax, DWORD PTR [128+esp]                      ;36.28
        push      32                                            ;36.28
        push      OFFSET FLAT: UCDAVISAIRMODELOUTPUT$format_pack.0.2 ;36.28
        push      eax                                           ;36.28
        push      OFFSET FLAT: __STRLITPACK_11.0.2              ;36.28
        push      -2088435968                                   ;36.28
        push      6544                                          ;36.28
        push      edx                                           ;36.28
        call      _for_write_seq_fmt                            ;36.28
                                ; LOE esi
.B1.8:                          ; Preds .B1.7
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;36.28
        lea       ecx, DWORD PTR [164+esp]                      ;36.28
        neg       eax                                           ;36.28
        add       eax, esi                                      ;36.28
        shl       eax, 8                                        ;36.28
        mov       edi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;36.28
        movzx     edx, BYTE PTR [237+edi+eax]                   ;36.28
        mov       BYTE PTR [164+esp], dl                        ;36.28
        push      ecx                                           ;36.28
        push      OFFSET FLAT: __STRLITPACK_12.0.2              ;36.28
        lea       ebx, DWORD PTR [68+esp]                       ;36.28
        push      ebx                                           ;36.28
        call      _for_write_seq_fmt_xmit                       ;36.28
                                ; LOE esi
.B1.9:                          ; Preds .B1.8
        sub       esi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;36.28
        shl       esi, 5                                        ;36.28
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;36.28
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;36.28
        mov       DWORD PTR [164+esp], eax                      ;36.28
        mov       edx, DWORD PTR [28+eax+esi]                   ;36.28
        sub       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;36.28
        imul      ecx, edx, 152                                 ;36.28
        imul      eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE+32], 44 ;36.28
        imul      edx, DWORD PTR [24+ebx+ecx], 44               ;36.28
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE] ;36.28
        add       edx, edi                                      ;36.28
        sub       edx, eax                                      ;36.28
        mov       DWORD PTR [156+esp], ecx                      ;36.28
        mov       DWORD PTR [152+esp], eax                      ;36.28
        lea       eax, DWORD PTR [184+esp]                      ;36.28
        mov       ecx, DWORD PTR [36+edx]                       ;36.28
        mov       DWORD PTR [160+esp], ecx                      ;36.28
        mov       DWORD PTR [184+esp], ecx                      ;36.28
        push      eax                                           ;36.28
        push      OFFSET FLAT: __STRLITPACK_13.0.2              ;36.28
        lea       edx, DWORD PTR [80+esp]                       ;36.28
        push      edx                                           ;36.28
        call      _for_write_seq_fmt_xmit                       ;36.28
                                ; LOE ebx esi edi
.B1.10:                         ; Preds .B1.9
        mov       eax, DWORD PTR [176+esp]                      ;36.28
        lea       ecx, DWORD PTR [204+esp]                      ;36.28
        movzx     edx, WORD PTR [20+eax+esi]                    ;36.28
        mov       WORD PTR [204+esp], dx                        ;36.28
        push      ecx                                           ;36.28
        push      OFFSET FLAT: __STRLITPACK_14.0.2              ;36.28
        lea       esi, DWORD PTR [92+esp]                       ;36.28
        push      esi                                           ;36.28
        call      _for_write_seq_fmt_xmit                       ;36.28
                                ; LOE ebx edi
.B1.11:                         ; Preds .B1.10
        mov       eax, DWORD PTR [180+esp]                      ;36.28
        imul      edx, DWORD PTR [28+ebx+eax], 44               ;36.28
        lea       ebx, DWORD PTR [224+esp]                      ;36.28
        add       edi, edx                                      ;36.28
        sub       edi, DWORD PTR [176+esp]                      ;36.28
        mov       ecx, DWORD PTR [36+edi]                       ;36.28
        mov       DWORD PTR [224+esp], ecx                      ;36.28
        push      ebx                                           ;36.28
        push      OFFSET FLAT: __STRLITPACK_15.0.2              ;36.28
        lea       esi, DWORD PTR [104+esp]                      ;36.28
        push      esi                                           ;36.28
        call      _for_write_seq_fmt_xmit                       ;36.28
                                ; LOE
.B1.12:                         ; Preds .B1.11
        mov       eax, DWORD PTR [196+esp]                      ;36.28
        lea       edx, DWORD PTR [244+esp]                      ;36.28
        mov       DWORD PTR [244+esp], eax                      ;36.28
        push      edx                                           ;36.28
        push      OFFSET FLAT: __STRLITPACK_16.0.2              ;36.28
        lea       ecx, DWORD PTR [116+esp]                      ;36.28
        push      ecx                                           ;36.28
        call      _for_write_seq_fmt_xmit                       ;36.28
                                ; LOE
.B1.13:                         ; Preds .B1.12
        lea       ebx, DWORD PTR [228+esp]                      ;36.28
        push      ebx                                           ;36.28
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_P_SIZE  ;36.28
                                ; LOE eax ebx
.B1.14:                         ; Preds .B1.13
        dec       eax                                           ;36.28
        mov       DWORD PTR [196+esp], eax                      ;36.28
        lea       eax, DWORD PTR [196+esp]                      ;36.28
        push      OFFSET FLAT: __NLITPACK_2.0.2                 ;36.28
        push      eax                                           ;36.28
        push      ebx                                           ;36.28
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_VALUE   ;36.28
                                ; LOE ebx f1
.B1.37:                         ; Preds .B1.14
        fstp      DWORD PTR [200+esp]                           ;36.28
        movss     xmm3, DWORD PTR [200+esp]                     ;36.28
                                ; LOE ebx xmm3
.B1.15:                         ; Preds .B1.37
        movss     xmm0, DWORD PTR [_2il0floatpacket.4]          ;36.28
        andps     xmm0, xmm3                                    ;36.28
        pxor      xmm3, xmm0                                    ;36.28
        movss     xmm1, DWORD PTR [_2il0floatpacket.5]          ;36.28
        movaps    xmm2, xmm3                                    ;36.28
        movaps    xmm7, xmm3                                    ;36.28
        cmpltss   xmm2, xmm1                                    ;36.28
        andps     xmm1, xmm2                                    ;36.28
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE+32] ;36.28
        addss     xmm7, xmm1                                    ;36.28
        neg       edx                                           ;36.28
        subss     xmm7, xmm1                                    ;36.28
        movaps    xmm6, xmm7                                    ;36.28
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE] ;36.28
        subss     xmm6, xmm3                                    ;36.28
        movss     xmm3, DWORD PTR [_2il0floatpacket.6]          ;36.28
        movaps    xmm4, xmm6                                    ;36.28
        movaps    xmm5, xmm3                                    ;36.28
        cmpnless  xmm4, xmm3                                    ;36.28
        addss     xmm5, xmm3                                    ;36.28
        cmpless   xmm6, DWORD PTR [_2il0floatpacket.7]          ;36.28
        andps     xmm4, xmm5                                    ;36.28
        andps     xmm6, xmm5                                    ;36.28
        subss     xmm7, xmm4                                    ;36.28
        addss     xmm7, xmm6                                    ;36.28
        orps      xmm7, xmm0                                    ;36.28
        cvtss2si  eax, xmm7                                     ;36.28
        add       edx, eax                                      ;36.28
        lea       eax, DWORD PTR [280+esp]                      ;36.28
        imul      esi, edx, 44                                  ;36.28
        mov       edi, DWORD PTR [36+ecx+esi]                   ;36.28
        mov       DWORD PTR [280+esp], edi                      ;36.28
        push      eax                                           ;36.28
        push      OFFSET FLAT: __STRLITPACK_17.0.2              ;36.28
        lea       edx, DWORD PTR [144+esp]                      ;36.28
        push      edx                                           ;36.28
        call      _for_write_seq_fmt_xmit                       ;36.28
                                ; LOE ebx
.B1.38:                         ; Preds .B1.15
        add       esp, 116                                      ;36.28
                                ; LOE ebx
.B1.16:                         ; Preds .B1.38
        mov       edx, DWORD PTR [140+esp]                      ;36.28
        lea       esi, DWORD PTR [184+esp]                      ;36.28
        sub       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;36.28
        shl       edx, 8                                        ;36.28
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;36.28
        mov       ecx, DWORD PTR [88+eax+edx]                   ;36.28
        mov       DWORD PTR [184+esp], ecx                      ;36.28
        push      esi                                           ;36.28
        push      OFFSET FLAT: __STRLITPACK_18.0.2              ;36.28
        lea       edi, DWORD PTR [40+esp]                       ;36.28
        push      edi                                           ;36.28
        call      _for_write_seq_fmt_xmit                       ;36.28
                                ; LOE ebx
.B1.17:                         ; Preds .B1.16
        push      ebx                                           ;36.28
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_A_SIZE  ;36.28
                                ; LOE eax ebx
.B1.18:                         ; Preds .B1.17
        dec       eax                                           ;36.28
        mov       DWORD PTR [124+esp], eax                      ;36.28
        lea       eax, DWORD PTR [124+esp]                      ;36.28
        push      OFFSET FLAT: __NLITPACK_3.0.2                 ;36.28
        push      eax                                           ;36.28
        push      ebx                                           ;36.28
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_VALUE   ;36.28
                                ; LOE f1
.B1.40:                         ; Preds .B1.18
        fstp      DWORD PTR [124+esp]                           ;36.28
        movss     xmm0, DWORD PTR [124+esp]                     ;36.28
                                ; LOE xmm0
.B1.19:                         ; Preds .B1.40
        divss     xmm0, DWORD PTR [_2il0floatpacket.3]          ;36.28
        movss     DWORD PTR [220+esp], xmm0                     ;36.28
        lea       eax, DWORD PTR [220+esp]                      ;36.28
        push      eax                                           ;36.28
        push      OFFSET FLAT: __STRLITPACK_19.0.2              ;36.28
        lea       edx, DWORD PTR [68+esp]                       ;36.28
        push      edx                                           ;36.28
        call      _for_write_seq_fmt_xmit                       ;36.28
                                ; LOE
.B1.20:                         ; Preds .B1.19
        mov       esi, DWORD PTR [180+esp]                      ;36.28
        mov       eax, esi                                      ;36.28
        mov       edi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;36.28
        sub       eax, edi                                      ;36.28
        shl       eax, 8                                        ;36.28
        lea       ecx, DWORD PTR [240+esp]                      ;36.28
        mov       ebx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;36.28
        mov       edx, DWORD PTR [108+ebx+eax]                  ;36.28
        mov       DWORD PTR [240+esp], edx                      ;36.28
        push      ecx                                           ;36.28
        push      OFFSET FLAT: __STRLITPACK_20.0.2              ;36.28
        lea       eax, DWORD PTR [80+esp]                       ;36.28
        push      eax                                           ;36.28
        call      _for_write_seq_fmt_xmit                       ;36.28
                                ; LOE ebx esi edi
.B1.21:                         ; Preds .B1.20
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;36.28
        neg       edx                                           ;36.28
        add       edx, esi                                      ;36.28
        shl       edx, 5                                        ;36.28
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;36.28
        mov       ecx, DWORD PTR [12+eax+edx]                   ;36.28
        lea       eax, DWORD PTR [260+esp]                      ;36.28
        mov       DWORD PTR [260+esp], ecx                      ;36.28
        push      eax                                           ;36.28
        push      OFFSET FLAT: __STRLITPACK_21.0.2              ;36.28
        lea       edx, DWORD PTR [92+esp]                       ;36.28
        push      edx                                           ;36.28
        call      _for_write_seq_fmt_xmit                       ;36.28
                                ; LOE ebx esi edi
.B1.41:                         ; Preds .B1.21
        add       esp, 64                                       ;36.28
        jmp       .B1.23        ; Prob 100%                     ;36.28
                                ; LOE ebx esi edi
.B1.22:                         ; Preds .B1.6
        mov       ebx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;38.7
        mov       edi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;38.10
                                ; LOE ebx esi edi
.B1.23:                         ; Preds .B1.41 .B1.22
        neg       edi                                           ;38.32
        add       edi, esi                                      ;38.32
        shl       edi, 8                                        ;38.32
        cmp       BYTE PTR [96+ebx+edi], 1                      ;38.32
        je        .B1.28        ; Prob 16%                      ;38.32
                                ; LOE esi
.B1.24:                         ; Preds .B1.23
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_I18], 0     ;40.11
        jle       .B1.32        ; Prob 16%                      ;40.11
                                ; LOE
.B1.25:                         ; Preds .B1.32 .B1.24
        mov       ebx, DWORD PTR [8+ebp]                        ;40.68
        lea       eax, DWORD PTR [132+esp]                      ;40.33
        cvtsi2ss  xmm0, DWORD PTR [ebx]                         ;40.68
        mulss     xmm0, DWORD PTR [_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT] ;40.69
        lea       ebx, DWORD PTR [140+esp]                      ;40.33
        movss     DWORD PTR [132+esp], xmm0                     ;40.69
        push      eax                                           ;40.33
        push      OFFSET FLAT: __NLITPACK_1.0.1                 ;40.33
        push      OFFSET FLAT: __NLITPACK_0.0.1                 ;40.33
        push      ebx                                           ;40.33
        call      _WRITE_PRINT_TRAJECTORY                       ;40.33
                                ; LOE ebx
.B1.42:                         ; Preds .B1.25
        add       esp, 16                                       ;40.33
                                ; LOE ebx
.B1.26:                         ; Preds .B1.42 .B1.44
        mov       eax, DWORD PTR [8+ebp]                        ;42.23
        lea       edx, DWORD PTR [148+esp]                      ;42.12
        cvtsi2ss  xmm0, DWORD PTR [eax]                         ;42.23
        mulss     xmm0, DWORD PTR [_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT] ;42.24
        movss     DWORD PTR [148+esp], xmm0                     ;42.24
        push      edx                                           ;42.12
        push      ebx                                           ;42.12
        call      _PRINTALT                                     ;42.12
                                ; LOE
.B1.43:                         ; Preds .B1.26
        add       esp, 8                                        ;42.12
                                ; LOE
.B1.27:                         ; Preds .B1.43
        mov       esi, DWORD PTR [140+esp]                      ;44.1
                                ; LOE esi
.B1.28:                         ; Preds .B1.27 .B1.23
        inc       esi                                           ;34.7
        cmp       esi, DWORD PTR [156+esp]                      ;34.7
        jle       .B1.29        ; Prob 82%                      ;34.7
                                ; LOE esi
.B1.30:                         ; Preds .B1.28                  ; Infreq
        mov       edi, DWORD PTR [92+esp]                       ;
        mov       ebx, DWORD PTR [88+esp]                       ;
                                ; LOE ebx edi
.B1.31:                         ; Preds .B1.30 .B1.4            ; Infreq
        add       esp, 220                                      ;46.1
        pop       esi                                           ;46.1
        mov       esp, ebp                                      ;46.1
        pop       ebp                                           ;46.1
        ret                                                     ;46.1
                                ; LOE
.B1.32:                         ; Preds .B1.24                  ; Infreq
        test      BYTE PTR [_DYNUST_MAIN_MODULE_mp_REACH_CONVERG], 1 ;40.18
        jne       .B1.25        ; Prob 40%                      ;40.18
                                ; LOE
.B1.44:                         ; Preds .B1.32                  ; Infreq
        lea       ebx, DWORD PTR [140+esp]                      ;36.28
        jmp       .B1.26        ; Prob 100%                     ;36.28
        ALIGN     16
                                ; LOE ebx
; mark_end;
_WRITE_VEH_TRAJECTORY ENDP
_TEXT	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
__STRLITPACK_6.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_7.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_8.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__NLITPACK_1.0.1	DD	18
__NLITPACK_0.0.1	DD	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _WRITE_VEH_TRAJECTORY
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _UCDAVISAIRMODELOUTPUT
; mark_begin;
       ALIGN     16
	PUBLIC _UCDAVISAIRMODELOUTPUT
_UCDAVISAIRMODELOUTPUT	PROC NEAR 
; parameter 1: 8 + ebp
.B2.1:                          ; Preds .B2.0
        push      ebp                                           ;49.12
        mov       ebp, esp                                      ;49.12
        and       esp, -16                                      ;49.12
        push      esi                                           ;49.12
        push      edi                                           ;49.12
        push      ebx                                           ;49.12
        sub       esp, 132                                      ;49.12
        mov       esi, DWORD PTR [8+ebp]                        ;49.12
        lea       edi, DWORD PTR [esp]                          ;56.1
        mov       DWORD PTR [esp], 0                            ;56.1
        lea       eax, DWORD PTR [56+esp]                       ;56.1
        mov       ebx, DWORD PTR [esi]                          ;56.1
        mov       DWORD PTR [56+esp], ebx                       ;56.1
        push      32                                            ;56.1
        push      OFFSET FLAT: UCDAVISAIRMODELOUTPUT$format_pack.0.2 ;56.1
        push      eax                                           ;56.1
        push      OFFSET FLAT: __STRLITPACK_11.0.2              ;56.1
        push      -2088435968                                   ;56.1
        push      6544                                          ;56.1
        push      edi                                           ;56.1
        call      _for_write_seq_fmt                            ;56.1
                                ; LOE ebx esi edi
.B2.2:                          ; Preds .B2.1
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;56.35
        neg       edx                                           ;56.1
        add       edx, ebx                                      ;56.1
        shl       edx, 8                                        ;56.1
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;56.1
        movzx     ecx, BYTE PTR [237+eax+edx]                   ;56.1
        lea       eax, DWORD PTR [92+esp]                       ;56.1
        mov       BYTE PTR [92+esp], cl                         ;56.1
        push      eax                                           ;56.1
        push      OFFSET FLAT: __STRLITPACK_12.0.2              ;56.1
        push      edi                                           ;56.1
        call      _for_write_seq_fmt_xmit                       ;56.1
                                ; LOE ebx esi edi
.B2.3:                          ; Preds .B2.2
        mov       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;56.1
        shl       ecx, 5                                        ;56.1
        neg       ecx                                           ;56.1
        shl       ebx, 5                                        ;56.1
        add       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;56.1
        imul      eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32], -152 ;56.1
        imul      edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE+32], -44 ;56.1
        mov       DWORD PTR [88+esp], ecx                       ;56.1
        imul      ecx, DWORD PTR [28+ebx+ecx], 152              ;56.1
        add       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;56.1
        mov       DWORD PTR [80+esp], eax                       ;56.1
        add       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE] ;56.1
        imul      eax, DWORD PTR [24+ecx+eax], 44               ;56.56
        mov       DWORD PTR [84+esp], edx                       ;56.1
        mov       edx, DWORD PTR [36+eax+edx]                   ;56.1
        lea       eax, DWORD PTR [112+esp]                      ;56.1
        mov       DWORD PTR [76+esp], ecx                       ;56.1
        mov       DWORD PTR [72+esp], edx                       ;56.1
        mov       DWORD PTR [112+esp], edx                      ;56.1
        push      eax                                           ;56.1
        push      OFFSET FLAT: __STRLITPACK_13.0.2              ;56.1
        push      edi                                           ;56.1
        call      _for_write_seq_fmt_xmit                       ;56.1
                                ; LOE ebx esi edi
.B2.4:                          ; Preds .B2.3
        mov       eax, DWORD PTR [100+esp]                      ;56.1
        lea       ecx, DWORD PTR [132+esp]                      ;56.1
        movzx     edx, WORD PTR [20+ebx+eax]                    ;56.1
        mov       WORD PTR [132+esp], dx                        ;56.1
        push      ecx                                           ;56.1
        push      OFFSET FLAT: __STRLITPACK_14.0.2              ;56.1
        push      edi                                           ;56.1
        call      _for_write_seq_fmt_xmit                       ;56.1
                                ; LOE esi edi
.B2.5:                          ; Preds .B2.4
        mov       eax, DWORD PTR [100+esp]                      ;56.187
        mov       edx, DWORD PTR [104+esp]                      ;56.187
        mov       ebx, DWORD PTR [108+esp]                      ;56.1
        imul      ecx, DWORD PTR [28+eax+edx], 44               ;56.187
        lea       edx, DWORD PTR [112+esp]                      ;56.1
        mov       eax, DWORD PTR [36+ecx+ebx]                   ;56.1
        mov       DWORD PTR [112+esp], eax                      ;56.1
        push      edx                                           ;56.1
        push      OFFSET FLAT: __STRLITPACK_15.0.2              ;56.1
        push      edi                                           ;56.1
        call      _for_write_seq_fmt_xmit                       ;56.1
                                ; LOE esi edi
.B2.6:                          ; Preds .B2.5
        mov       eax, DWORD PTR [108+esp]                      ;56.1
        lea       edx, DWORD PTR [164+esp]                      ;56.1
        mov       DWORD PTR [164+esp], eax                      ;56.1
        push      edx                                           ;56.1
        push      OFFSET FLAT: __STRLITPACK_16.0.2              ;56.1
        push      edi                                           ;56.1
        call      _for_write_seq_fmt_xmit                       ;56.1
                                ; LOE esi edi
.B2.7:                          ; Preds .B2.6
        push      esi                                           ;56.1
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_P_SIZE  ;56.1
                                ; LOE eax esi edi
.B2.8:                          ; Preds .B2.7
        dec       eax                                           ;56.391
        mov       DWORD PTR [132+esp], eax                      ;56.391
        lea       eax, DWORD PTR [132+esp]                      ;56.1
        push      OFFSET FLAT: __NLITPACK_2.0.2                 ;56.1
        push      eax                                           ;56.1
        push      esi                                           ;56.1
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_VALUE   ;56.1
                                ; LOE esi edi f1
.B2.20:                         ; Preds .B2.8
        fstp      DWORD PTR [136+esp]                           ;56.1
        movss     xmm3, DWORD PTR [136+esp]                     ;56.1
                                ; LOE esi edi xmm3
.B2.9:                          ; Preds .B2.20
        movss     xmm0, DWORD PTR [_2il0floatpacket.10]         ;56.1
        andps     xmm0, xmm3                                    ;56.1
        pxor      xmm3, xmm0                                    ;56.1
        movss     xmm1, DWORD PTR [_2il0floatpacket.11]         ;56.1
        movaps    xmm2, xmm3                                    ;56.1
        movaps    xmm7, xmm3                                    ;56.1
        cmpltss   xmm2, xmm1                                    ;56.1
        andps     xmm1, xmm2                                    ;56.1
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE+32] ;56.1
        addss     xmm7, xmm1                                    ;56.1
        neg       edx                                           ;56.1
        subss     xmm7, xmm1                                    ;56.1
        movaps    xmm6, xmm7                                    ;56.1
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE] ;56.1
        subss     xmm6, xmm3                                    ;56.1
        movss     xmm3, DWORD PTR [_2il0floatpacket.12]         ;56.1
        movaps    xmm4, xmm6                                    ;56.1
        movaps    xmm5, xmm3                                    ;56.1
        cmpnless  xmm4, xmm3                                    ;56.1
        addss     xmm5, xmm3                                    ;56.1
        cmpless   xmm6, DWORD PTR [_2il0floatpacket.13]         ;56.1
        andps     xmm4, xmm5                                    ;56.1
        andps     xmm6, xmm5                                    ;56.1
        subss     xmm7, xmm4                                    ;56.1
        addss     xmm7, xmm6                                    ;56.1
        orps      xmm7, xmm0                                    ;56.1
        cvtss2si  eax, xmm7                                     ;56.391
        add       edx, eax                                      ;56.1
        imul      ebx, edx, 44                                  ;56.1
        lea       edx, DWORD PTR [200+esp]                      ;56.1
        mov       eax, DWORD PTR [36+ecx+ebx]                   ;56.1
        mov       DWORD PTR [200+esp], eax                      ;56.1
        push      edx                                           ;56.1
        push      OFFSET FLAT: __STRLITPACK_17.0.2              ;56.1
        push      edi                                           ;56.1
        call      _for_write_seq_fmt_xmit                       ;56.1
                                ; LOE esi edi
.B2.21:                         ; Preds .B2.9
        add       esp, 116                                      ;56.1
                                ; LOE esi edi
.B2.10:                         ; Preds .B2.21
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;56.476
        lea       ebx, DWORD PTR [104+esp]                      ;56.1
        neg       edx                                           ;56.1
        add       edx, DWORD PTR [esi]                          ;56.1
        shl       edx, 8                                        ;56.1
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;56.1
        mov       ecx, DWORD PTR [88+eax+edx]                   ;56.1
        mov       DWORD PTR [104+esp], ecx                      ;56.1
        push      ebx                                           ;56.1
        push      OFFSET FLAT: __STRLITPACK_18.0.2              ;56.1
        push      edi                                           ;56.1
        call      _for_write_seq_fmt_xmit                       ;56.1
                                ; LOE esi edi
.B2.11:                         ; Preds .B2.10
        push      esi                                           ;4194305.516
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_A_SIZE  ;4194305.516
                                ; LOE eax esi edi
.B2.12:                         ; Preds .B2.11
        dec       eax                                           ;56.501
        mov       DWORD PTR [60+esp], eax                       ;56.501
        lea       eax, DWORD PTR [60+esp]                       ;56.501
        push      OFFSET FLAT: __NLITPACK_3.0.2                 ;56.501
        push      eax                                           ;56.501
        push      esi                                           ;56.501
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_VALUE   ;56.501
                                ; LOE esi edi f1
.B2.23:                         ; Preds .B2.12
        fstp      DWORD PTR [60+esp]                            ;56.501
        movss     xmm0, DWORD PTR [60+esp]                      ;56.501
                                ; LOE esi edi xmm0
.B2.13:                         ; Preds .B2.23
        divss     xmm0, DWORD PTR [_2il0floatpacket.9]          ;56.1
        movss     DWORD PTR [60+esp], xmm0                      ;56.1
        lea       eax, DWORD PTR [60+esp]                       ;56.1
        push      eax                                           ;56.1
        push      OFFSET FLAT: __STRLITPACK_19.0.2              ;56.1
        push      edi                                           ;56.1
        call      _for_write_seq_fmt_xmit                       ;56.1
                                ; LOE esi edi
.B2.14:                         ; Preds .B2.13
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;4194305.548
        neg       edx                                           ;56.1
        mov       ebx, DWORD PTR [esi]                          ;4194305.548
        add       edx, ebx                                      ;56.1
        shl       edx, 8                                        ;56.1
        lea       esi, DWORD PTR [152+esp]                      ;56.1
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;56.1
        mov       ecx, DWORD PTR [108+eax+edx]                  ;56.1
        mov       DWORD PTR [152+esp], ecx                      ;56.1
        push      esi                                           ;56.1
        push      OFFSET FLAT: __STRLITPACK_20.0.2              ;56.1
        push      edi                                           ;56.1
        call      _for_write_seq_fmt_xmit                       ;56.1
                                ; LOE ebx edi
.B2.15:                         ; Preds .B2.14
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;56.1
        lea       ecx, DWORD PTR [172+esp]                      ;56.1
        neg       eax                                           ;56.1
        add       eax, ebx                                      ;56.1
        shl       eax, 5                                        ;56.1
        mov       ebx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;56.1
        mov       edx, DWORD PTR [12+ebx+eax]                   ;56.1
        mov       DWORD PTR [172+esp], edx                      ;56.1
        push      ecx                                           ;56.1
        push      OFFSET FLAT: __STRLITPACK_21.0.2              ;56.1
        push      edi                                           ;56.1
        call      _for_write_seq_fmt_xmit                       ;56.1
                                ; LOE
.B2.16:                         ; Preds .B2.15
        add       esp, 196                                      ;58.1
        pop       ebx                                           ;58.1
        pop       edi                                           ;58.1
        pop       esi                                           ;58.1
        mov       esp, ebp                                      ;58.1
        pop       ebp                                           ;58.1
        ret                                                     ;58.1
        ALIGN     16
                                ; LOE
; mark_end;
_UCDAVISAIRMODELOUTPUT ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _UCDAVISAIRMODELOUTPUT
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
UCDAVISAIRMODELOUTPUT$format_pack.0.2	DB	54
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	8
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	3
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	5
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	3
	DB	4
	DB	0
	DB	0
	DB	0
	DB	8
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
_DATA	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
__STRLITPACK_4	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_2	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	86
	DB	101
	DB	104
	DB	105
	DB	99
	DB	108
	DB	101
	DB	115
	DB	32
	DB	115
	DB	116
	DB	105
	DB	108
	DB	108
	DB	32
	DB	105
	DB	110
	DB	32
	DB	116
	DB	104
	DB	101
	DB	32
	DB	110
	DB	101
	DB	116
	DB	119
	DB	111
	DB	114
	DB	107
	DB	32
	DB	32
	DB	32
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_0	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	35
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_11.0.2	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_12.0.2	DB	5
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_13.0.2	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_14.0.2	DB	7
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_15.0.2	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_16.0.2	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__NLITPACK_2.0.2	DD	1
__STRLITPACK_17.0.2	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_18.0.2	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__NLITPACK_3.0.2	DD	2
__STRLITPACK_19.0.2	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_20.0.2	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_21.0.2	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
_2il0floatpacket.3	DD	042c80000H
_2il0floatpacket.4	DD	080000000H
_2il0floatpacket.5	DD	04b000000H
_2il0floatpacket.6	DD	03f000000H
_2il0floatpacket.7	DD	0bf000000H
_2il0floatpacket.9	DD	042c80000H
_2il0floatpacket.10	DD	080000000H
_2il0floatpacket.11	DD	04b000000H
_2il0floatpacket.12	DD	03f000000H
_2il0floatpacket.13	DD	0bf000000H
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
EXTRN	_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_REACH_CONVERG:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_I18:BYTE
EXTRN	_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_UCDAIROPTION:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_JUSTVEH:BYTE
EXTRN	_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE:BYTE
_DATA	ENDS
EXTRN	_for_write_seq_fmt_xmit:PROC
EXTRN	_for_write_seq_fmt:PROC
EXTRN	_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_VALUE:PROC
EXTRN	_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_P_SIZE:PROC
EXTRN	_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_A_SIZE:PROC
EXTRN	_for_write_seq_lis:PROC
EXTRN	_WRITE_PRINT_TRAJECTORY:PROC
EXTRN	_PRINTALT:PROC
EXTRN	__fltused:BYTE
	INCLUDELIB <ifconsol>
	INCLUDELIB <libifcoremt>
	INCLUDELIB <libifport>
	INCLUDELIB <libmmt>
	INCLUDELIB <LIBCMT>
	INCLUDELIB <libirc>
	INCLUDELIB <svml_dispmt>
	INCLUDELIB <OLDNAMES>
	END
