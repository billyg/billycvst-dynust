MODULE WORKZONE_MODULE
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~ Copyright (C) 2002-2011 The Arizona Board of Regents on Behalf of The University of Arizona.  ~~!
!~~ Developed by Yi-Chang Chiu, Ph.D. as par to the DynusT project.                               ~~!
!~~ This program is free software: you can redistribute it and/or modify it                       ~~!
!~~ under the terms of the GNU General Public License version 2 as published by                   ~~!
!~~ the Free Software Foundation.                                                                 ~~!
!~~ This program is distributed in the hope that it will be useful,                               ~~!
!~~ but WITHOUT ANY WARRANTY; without even the implied warranty of                                ~~!
!~~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                                          ~~!
!~~ See the GNU General Public License for more details.                                          ~~!
!~~ You should have received a copy of the GNU General Public License                             ~~!
!~~ along with this program (in the file COPYING).                                                ~~!
!~~ IF not, see <http://www.gnu.org/licenses/>.                                                   ~~!
!~~ Please see http://www.dynust.net for contact information and names                            ~~!
!~~ of individual contributors.                                                                   ~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
      USE DYNUST_MAIN_MODULE
      USE DYNUST_NETWORK_MODULE

CONTAINS


SUBROUTINE WZ_SCAN(tt)
      DO i = 1, WorkZoneNum
         IF(tt >= WorkZone(i)%ST.and.tt < WorkZone(i)%ET) THEN
	     IF(.not.wzstartflag(i)) THEN
             wzstartflag(i) = .True.
             CALL WZ_ACTIVATE(i)
	     ENDIF
         ENDIF
      end do

      DO i=1,WorkZoneNum
        IF(tt > WorkZone(i)%ET) THEN
	    IF(wzstartflag(i)) THEN
	   	    wzstartflag(i) = .False.
            CALL WZ_CAPACITY_RESTORE(i)
	    ENDIF
        ENDIF 
      ENDDO

END SUBROUTINE

!~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>
SUBROUTINE WZ_ACTIVATE(i)
      seve=WorkZone(i)%CapRed  
      ilink=GetFLinkFromNode(WorkZone(i)%FNode,WorkZone(i)%TNode,5)
	  WorkZone(i)%OrigDisChg=m_dynust_network_arc_de(ilink)%MaxFlowRate
	  WorkZone(i)%OrigSpdLmt=m_dynust_network_arc_de(ilink)%SpeedLimit
	  m_dynust_network_arc_de(ilink)%xl=m_dynust_network_arc_de(ilink)%nlanes*m_dynust_network_arc_nde(ilink)%s*(1-seve)
      m_dynust_network_arc_de(ilink)%MaxFlowRate=WorkZone(i)%Discharge/3600.0*m_dynust_network_arc_de(ilink)%nlanes*(1.0-seve)
	  m_dynust_network_arc_de(ilink)%SpeedLimit=WorkZone(i)%SpeedLmt
END SUBROUTINE


!~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>
SUBROUTINE WZ_CAPACITY_RESTORE(i)
      ilink=GetFLinkFromNode(WorkZone(i)%FNode,WorkZone(i)%TNode,5)
      m_dynust_network_arc_de(ilink)%xl=m_dynust_network_arc_de(ilink)%nlanes*m_dynust_network_arc_nde(ilink)%s
      m_dynust_network_arc_de(ilink)%MaxFlowRate=WorkZone(i)%OrigDisChg 
	  m_dynust_network_arc_de(ilink)%SpeedLimit=WorkZone(i)%OrigSpdLmt

END SUBROUTINE

END MODULE