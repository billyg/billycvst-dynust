    SUBROUTINE CLOSEFILE()
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~ Copyright (C) 2002-2011 The Arizona Board of Regents on Behalf of The University of Arizona.  ~~!
!~~ Developed by Yi-Chang Chiu, Ph.D. as par to the DynusT project.                               ~~!
!~~ This program is free software: you can redistribute it and/or modify it                       ~~!
!~~ under the terms of the GNU General Public License version 2 as published by                   ~~!
!~~ the Free Software Foundation.                                                                 ~~!
!~~ This program is distributed in the hope that it will be useful,                               ~~!
!~~ but WITHOUT ANY WARRANTY; without even the implied warranty of                                ~~!
!~~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                                          ~~!
!~~ See the GNU General Public License for more details.                                          ~~!
!~~ You should have received a copy of the GNU General Public License                             ~~!
!~~ along with this program (in the file COPYING).                                                ~~!
!~~ IF not, see <http://www.gnu.org/licenses/>.                                                   ~~!
!~~ Please see http://www.dynust.net for contact information and names                            ~~!
!~~ of individual contributors.                                                                   ~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
    USE DYNUST_MAIN_MODULE
	
	CLOSE(41) 
	CLOSE(42) 
	CLOSE(43) 
	CLOSE(44) 
	CLOSE(45) 
	CLOSE(46) 
	CLOSE(47) 
	CLOSE(48) 
	CLOSE(49) 
	CLOSE(50) 
	CLOSE(51) 
	CLOSE(52) 
	CLOSE(53)
    CLOSE(54)
    CLOSE(544)
    CLOSE(545)
    CLOSE(7890)
    CLOSE(55)
    CLOSE(56)
    CLOSE(57)
    CLOSE(58)
    CLOSE(59)	
    CLOSE(60)	
	CLOSE(95)
	CLOSE(101) 
	CLOSE(97)
	CLOSE(98)
    CLOSE(18)  
	CLOSE(30) 
    CLOSE(301) 
	CLOSE(31) 
	CLOSE(32) 
	CLOSE(33) 
	CLOSE(34) 
	CLOSE(35) 
	CLOSE(36) 
	CLOSE(37) 
	CLOSE(38) 
	CLOSE(39)
    CLOSE(40)		
    CLOSE(500)
	CLOSE(550)
	CLOSE(188)
	CLOSE(600)
	CLOSE(700)
	CLOSE(800)
	CLOSE(900)
	CLOSE(911)
    CLOSE(200)	
    CLOSE(461)		
	CLOSE(6544) ! for UCDAir.dat
    CLOSE (911)
    CLOSE(62)
    CLOSE(63)
    CLOSE(97)
    CLOSE(98)
    CLOSE(511)
    CLOSE(400)
    CLOSE(401)
    IF(trajalt > 0) THEN
	  CLOSE(988)
	  CLOSE(990)
	ENDIF
    rewind(989)
	CLOSE(888)
	CLOSE(777)
	CLOSE(512)
	CLOSE(513)
	CLOSE(514)
	CLOSE(6057)
	CLOSE(6058)
	CLOSE(6059)
	CLOSE(6060)
    CLOSE(95)	  
    CLOSE(2000)
    CLOSE(2001)
    CLOSE(4567)      
    CLOSE(5543)
    CLOSE(139)
    CLOSE(8456)
    CLOSE(7099)
    CLOSE(9987)
    CLOSE(5454)
END SUBROUTINE
