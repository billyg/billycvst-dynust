; mark_description "Intel(R) Visual Fortran Compiler XE for applications running on IA-32, Version 13.1.1.171 Build 20130313";
; mark_description "-S";
	.686P
 	.387
	OPTION DOTNAME
	ASSUME	CS:FLAT,DS:FLAT,SS:FLAT
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
TXTST0:
; -- Begin  _INITIALIZE
; mark_begin;
IF @Version GE 800
  .MMX
ELSEIF @Version GE 612
  .MMX
  MMWORD TEXTEQU <QWORD>
ENDIF
IF @Version GE 800
  .XMM
ELSEIF @Version GE 614
  .XMM
  XMMWORD TEXTEQU <OWORD>
ENDIF
       ALIGN     16
	PUBLIC _INITIALIZE
_INITIALIZE	PROC NEAR 
.B1.1:                          ; Preds .B1.0
        push      ebp                                           ;1.18
        mov       ebp, esp                                      ;1.18
        and       esp, -16                                      ;1.18
        push      esi                                           ;1.18
        push      edi                                           ;1.18
        push      ebx                                           ;1.18
        sub       esp, 52                                       ;1.18
        xor       eax, eax                                      ;29.7
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ITERATION] ;36.2
        test      edx, edx                                      ;36.15
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_INIID1], eax ;29.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_INIID2], eax ;30.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_GUITOTALTIME], eax ;31.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_RANCT], eax ;34.4
        mov       DWORD PTR [12+esp], edx                       ;36.2
        jne       .B1.3         ; Prob 50%                      ;36.15
                                ; LOE
.B1.2:                          ; Preds .B1.1
        mov       eax, 0                                        ;37.8
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ISO_OK], eax ;37.8
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_IUE_OK], eax ;38.8
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_IER_OK], eax ;39.8
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_JTOTAL], eax ;40.8
                                ; LOE
.B1.3:                          ; Preds .B1.1 .B1.2
        mov       eax, 0                                        ;42.2
        mov       esi, 1                                        ;160.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_JUSTVEH], eax ;42.2
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_JUSTVEH_I], eax ;43.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_MULTI], eax ;44.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_LISTTOTAL], eax ;45.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_INT_D], 10  ;46.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ITAG0], eax ;47.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ITAG1], eax ;48.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ITAG2], eax ;49.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ITAG3], eax ;50.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_INFORMATION], eax ;51.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_INFORMATION_1], eax ;52.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_INFORMATION_2], eax ;53.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_INFORMATION_3], eax ;54.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOINFORMATION], eax ;55.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOINFORMATION_1], eax ;56.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOINFORMATION_2], eax ;57.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOINFORMATION_3], eax ;58.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_DTOTAL1], eax ;59.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_DTOTAL2], eax ;60.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_DTOTAL2_1], eax ;61.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_DTOTAL2_2], eax ;62.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_DTOTAL2_3], eax ;63.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_DTOTAL3], eax ;64.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_DTOTAL3_1], eax ;65.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_DTOTAL3_2], eax ;66.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_DTOTAL3_3], eax ;67.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_VTOTHR1], eax ;68.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_VTOTHR2], eax ;69.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_VTOTHR2_1], eax ;70.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_VTOTHR2_2], eax ;71.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_VTOTHR2_3], eax ;72.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_VTOTHR3], eax ;73.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_VTOTHR3_1], eax ;74.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_VTOTHR3_2], eax ;75.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_VTOTHR3_3], eax ;76.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRIPTIME1], eax ;77.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRIPTIME2], eax ;78.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRIPTIME2_1], eax ;79.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRIPTIME2_2], eax ;80.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRIPTIME2_3], eax ;81.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRIPTIME3], eax ;82.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRIPTIME3_1], eax ;83.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRIPTIME3_2], eax ;84.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRIPTIME3_3], eax ;85.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_AVE_TRIP1], eax ;86.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_AVE_TRIP2], eax ;87.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_AVE_TRIP2_1], eax ;88.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_AVE_TRIP2_2], eax ;89.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_AVE_TRIP2_3], eax ;90.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_AVE_TRIP3], eax ;91.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_AVE_TRIP3_1], eax ;92.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_AVE_TRIP3_2], eax ;93.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_AVE_TRIP3_3], eax ;94.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_STOPTIME], eax ;95.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_STOPINFO], eax ;96.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_STOPINFO_1], eax ;97.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_STOPINFO_2], eax ;98.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_STOPINFO_3], eax ;99.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_STOPNOINFO], eax ;100.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_STOPNOINFO_1], eax ;101.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_STOPNOINFO_2], eax ;102.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_STOPNOINFO_3], eax ;103.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_AVESTOPTIME], eax ;104.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_AVESTOPINFO], eax ;105.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_AVESTOPINFO_1], eax ;106.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_AVESTOPINFO_2], eax ;107.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_AVESTOPINFO_3], eax ;108.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_AVESTOPNOINFO], eax ;109.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_AVESTOPNOINFO_1], eax ;110.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_AVESTOPNOINFO_2], eax ;111.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_AVESTOPNOINFO_3], eax ;112.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ENTRY_QUEUE1], eax ;113.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ENTRY_QUEUE2], eax ;114.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ENTRY_QUEUE2_1], eax ;115.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ENTRY_QUEUE2_2], eax ;116.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ENTRY_QUEUE2_3], eax ;117.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ENTRY_QUEUE3], eax ;118.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ENTRY_QUEUE3_1], eax ;119.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ENTRY_QUEUE3_2], eax ;120.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ENTRY_QUEUE3_3], eax ;121.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_AVE_ENTRY1], eax ;122.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_AVE_ENTRY2], eax ;123.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_AVE_ENTRY2_1], eax ;124.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_AVE_ENTRY2_2], eax ;125.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_AVE_ENTRY2_3], eax ;126.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_AVE_ENTRY3], eax ;127.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_AVE_ENTRY3_1], eax ;128.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_AVE_ENTRY3_2], eax ;129.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_AVE_ENTRY3_3], eax ;130.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_AVEDTOTAL1], eax ;131.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_AVEDTOTAL2], eax ;132.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_AVEDTOTAL2_1], eax ;133.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_AVEDTOTAL2_2], eax ;134.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_AVEDTOTAL2_3], eax ;135.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_AVEDTOTAL3], eax ;136.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_AVEDTOTAL3_1], eax ;137.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_AVEDTOTAL3_2], eax ;138.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_AVEDTOTAL3_3], eax ;139.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_TOTALDECSION], eax ;140.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_TOTALSWITCH], eax ;141.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOUT_TAG_I], eax ;144.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOUT_NONTAG_I], eax ;145.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_VAVG1], eax ;146.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_VAVG2], eax ;147.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_VAVG2_1], eax ;148.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_VAVG2_2], eax ;149.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_VAVG2_3], eax ;150.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_VAVG3], eax ;151.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_VAVG3_1], eax ;152.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_VAVG3_2], eax ;153.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_VAVG3_3], eax ;154.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_TIME_NOW], eax ;159.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ISIGCOUNT], esi ;160.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_NUMCARS], eax ;162.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_OLDNUMCARS], eax ;163.7
        mov       DWORD PTR [_DYNUST_VEH_MODULE_mp_NOOFBUSES], eax ;164.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS_NUM], eax ;165.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_INCI_NUM], eax ;166.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_JRESTORE], esi ;167.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOUT_TAG], eax ;170.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOUT_NONTAG], eax ;171.7
        jle       .B1.60        ; Prob 16%                      ;173.19
                                ; LOE
.B1.4:                          ; Preds .B1.3
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+24] ;174.7
        test      edx, edx                                      ;174.7
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;174.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_MAXMOVE], 0 ;32.7
        jle       .B1.29        ; Prob 50%                      ;174.7
                                ; LOE edx esi
.B1.5:                          ; Preds .B1.4
        mov       ebx, edx                                      ;174.7
        shr       ebx, 31                                       ;174.7
        add       ebx, edx                                      ;174.7
        sar       ebx, 1                                        ;174.7
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;174.7
        test      ebx, ebx                                      ;174.7
        jbe       .B1.96        ; Prob 10%                      ;174.7
                                ; LOE edx ecx ebx esi
.B1.6:                          ; Preds .B1.5
        xor       eax, eax                                      ;
        mov       DWORD PTR [esp], esi                          ;
        xor       edi, edi                                      ;
        mov       esi, eax                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B1.7:                          ; Preds .B1.7 .B1.6
        inc       edi                                           ;174.7
        mov       DWORD PTR [12+esi+ecx], eax                   ;174.7
        mov       DWORD PTR [164+esi+ecx], eax                  ;174.7
        add       esi, 304                                      ;174.7
        cmp       edi, ebx                                      ;174.7
        jb        .B1.7         ; Prob 63%                      ;174.7
                                ; LOE eax edx ecx ebx esi edi
.B1.8:                          ; Preds .B1.7
        mov       esi, DWORD PTR [esp]                          ;
        lea       eax, DWORD PTR [1+edi+edi]                    ;174.7
                                ; LOE eax edx ecx ebx esi
.B1.9:                          ; Preds .B1.8 .B1.96
        lea       edi, DWORD PTR [-1+eax]                       ;174.7
        cmp       edx, edi                                      ;174.7
        jbe       .B1.11        ; Prob 10%                      ;174.7
                                ; LOE eax edx ecx ebx esi
.B1.10:                         ; Preds .B1.9
        mov       edi, esi                                      ;174.7
        add       eax, esi                                      ;174.7
        neg       edi                                           ;174.7
        add       edi, eax                                      ;174.7
        imul      eax, edi, 152                                 ;174.7
        mov       DWORD PTR [-140+ecx+eax], 0                   ;174.7
                                ; LOE edx ecx ebx esi
.B1.11:                         ; Preds .B1.9 .B1.10
        test      ebx, ebx                                      ;175.7
        jbe       .B1.95        ; Prob 10%                      ;175.7
                                ; LOE edx ecx ebx esi
.B1.12:                         ; Preds .B1.11
        xor       eax, eax                                      ;
        mov       DWORD PTR [esp], esi                          ;
        xor       edi, edi                                      ;
        mov       esi, eax                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B1.13:                         ; Preds .B1.13 .B1.12
        inc       edi                                           ;175.7
        mov       DWORD PTR [8+esi+ecx], eax                    ;175.7
        mov       DWORD PTR [160+esi+ecx], eax                  ;175.7
        add       esi, 304                                      ;175.7
        cmp       edi, ebx                                      ;175.7
        jb        .B1.13        ; Prob 63%                      ;175.7
                                ; LOE eax edx ecx ebx esi edi
.B1.14:                         ; Preds .B1.13
        mov       esi, DWORD PTR [esp]                          ;
        lea       eax, DWORD PTR [1+edi+edi]                    ;175.7
                                ; LOE eax edx ecx ebx esi
.B1.15:                         ; Preds .B1.14 .B1.95
        lea       edi, DWORD PTR [-1+eax]                       ;175.7
        cmp       edx, edi                                      ;175.7
        jbe       .B1.17        ; Prob 10%                      ;175.7
                                ; LOE eax edx ecx ebx esi
.B1.16:                         ; Preds .B1.15
        mov       edi, esi                                      ;175.7
        add       eax, esi                                      ;175.7
        neg       edi                                           ;175.7
        add       edi, eax                                      ;175.7
        imul      eax, edi, 152                                 ;175.7
        mov       DWORD PTR [-144+ecx+eax], 0                   ;175.7
                                ; LOE edx ecx ebx esi
.B1.17:                         ; Preds .B1.15 .B1.16
        test      ebx, ebx                                      ;176.7
        jbe       .B1.94        ; Prob 10%                      ;176.7
                                ; LOE edx ecx ebx esi
.B1.18:                         ; Preds .B1.17
        xor       eax, eax                                      ;
        mov       DWORD PTR [esp], esi                          ;
        xor       edi, edi                                      ;
        mov       esi, eax                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B1.19:                         ; Preds .B1.19 .B1.18
        inc       edi                                           ;176.7
        mov       DWORD PTR [28+esi+ecx], eax                   ;176.7
        mov       DWORD PTR [180+esi+ecx], eax                  ;176.7
        add       esi, 304                                      ;176.7
        cmp       edi, ebx                                      ;176.7
        jb        .B1.19        ; Prob 63%                      ;176.7
                                ; LOE eax edx ecx ebx esi edi
.B1.20:                         ; Preds .B1.19
        mov       esi, DWORD PTR [esp]                          ;
        lea       eax, DWORD PTR [1+edi+edi]                    ;176.7
                                ; LOE eax edx ecx ebx esi
.B1.21:                         ; Preds .B1.20 .B1.94
        lea       edi, DWORD PTR [-1+eax]                       ;176.7
        cmp       edx, edi                                      ;176.7
        jbe       .B1.23        ; Prob 10%                      ;176.7
                                ; LOE eax edx ecx ebx esi
.B1.22:                         ; Preds .B1.21
        mov       edi, esi                                      ;176.7
        add       eax, esi                                      ;176.7
        neg       edi                                           ;176.7
        add       edi, eax                                      ;176.7
        imul      eax, edi, 152                                 ;176.7
        mov       DWORD PTR [-124+ecx+eax], 0                   ;176.7
                                ; LOE edx ecx ebx esi
.B1.23:                         ; Preds .B1.21 .B1.22
        test      ebx, ebx                                      ;177.7
        jbe       .B1.93        ; Prob 10%                      ;177.7
                                ; LOE edx ecx ebx esi
.B1.24:                         ; Preds .B1.23
        xor       eax, eax                                      ;
        mov       DWORD PTR [esp], esi                          ;
        xor       edi, edi                                      ;
        mov       esi, eax                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B1.25:                         ; Preds .B1.25 .B1.24
        inc       edi                                           ;177.7
        mov       DWORD PTR [4+esi+ecx], eax                    ;177.7
        mov       DWORD PTR [156+esi+ecx], eax                  ;177.7
        add       esi, 304                                      ;177.7
        cmp       edi, ebx                                      ;177.7
        jb        .B1.25        ; Prob 63%                      ;177.7
                                ; LOE eax edx ecx ebx esi edi
.B1.26:                         ; Preds .B1.25
        mov       esi, DWORD PTR [esp]                          ;
        lea       ebx, DWORD PTR [1+edi+edi]                    ;177.7
                                ; LOE edx ecx ebx esi
.B1.27:                         ; Preds .B1.26 .B1.93
        lea       eax, DWORD PTR [-1+ebx]                       ;177.7
        cmp       edx, eax                                      ;177.7
        jbe       .B1.29        ; Prob 10%                      ;177.7
                                ; LOE ecx ebx esi
.B1.28:                         ; Preds .B1.27
        mov       eax, esi                                      ;177.7
        add       ebx, esi                                      ;177.7
        neg       eax                                           ;177.7
        add       eax, ebx                                      ;177.7
        imul      edx, eax, 152                                 ;177.7
        mov       DWORD PTR [-148+ecx+edx], 0                   ;177.7
                                ; LOE
.B1.29:                         ; Preds .B1.4 .B1.27 .B1.28
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+36] ;178.7
        test      eax, eax                                      ;178.7
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+44] ;178.7
        mov       DWORD PTR [16+esp], eax                       ;178.7
        jle       .B1.33        ; Prob 10%                      ;178.7
                                ; LOE ebx
.B1.30:                         ; Preds .B1.29
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE] ;180.7
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+40] ;180.7
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+24] ;178.7
        test      ecx, ecx                                      ;178.7
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+32] ;178.7
        mov       DWORD PTR [24+esp], edx                       ;180.7
        mov       DWORD PTR [20+esp], esi                       ;180.7
        jg        .B1.85        ; Prob 50%                      ;178.7
                                ; LOE eax ecx ebx
.B1.31:                         ; Preds .B1.92 .B1.137 .B1.30
        test      ecx, ecx                                      ;179.7
        jg        .B1.78        ; Prob 50%                      ;179.7
                                ; LOE eax ecx ebx
.B1.32:                         ; Preds .B1.134 .B1.31
        test      ecx, ecx                                      ;180.7
        jg        .B1.71        ; Prob 50%                      ;180.7
                                ; LOE eax ecx ebx
.B1.33:                         ; Preds .B1.129 .B1.77 .B1.29 .B1.32
        xor       eax, eax                                      ;181.7
        pxor      xmm0, xmm0                                    ;181.7
        xor       ecx, ecx                                      ;181.7
                                ; LOE eax ecx xmm0
.B1.34:                         ; Preds .B1.34 .B1.33
        lea       edx, DWORD PTR [eax+eax*4]                    ;181.7
        inc       eax                                           ;181.7
        movdqu    XMMWORD PTR [_DYNUST_MAIN_MODULE_mp_ALMOV+edx*8], xmm0 ;181.7
        movdqu    XMMWORD PTR [_DYNUST_MAIN_MODULE_mp_ALMOV+16+edx*8], xmm0 ;181.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ALMOV+32+edx*8], ecx ;181.7
        cmp       eax, 10                                       ;181.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ALMOV+36+edx*8], ecx ;181.7
        jb        .B1.34        ; Prob 89%                      ;181.7
                                ; LOE eax ecx xmm0
.B1.35:                         ; Preds .B1.34
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE+24] ;182.7
        test      ebx, ebx                                      ;182.7
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE+32] ;182.7
        jle       .B1.42        ; Prob 50%                      ;182.7
                                ; LOE ebx esi xmm0
.B1.36:                         ; Preds .B1.35
        mov       ecx, ebx                                      ;182.7
        shr       ecx, 31                                       ;182.7
        add       ecx, ebx                                      ;182.7
        sar       ecx, 1                                        ;182.7
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE] ;182.7
        test      ecx, ecx                                      ;182.7
        jbe       .B1.97        ; Prob 10%                      ;182.7
                                ; LOE edx ecx ebx esi xmm0
.B1.37:                         ; Preds .B1.36
        xor       eax, eax                                      ;
        mov       DWORD PTR [esp], esi                          ;
        xor       edi, edi                                      ;
        mov       esi, eax                                      ;
                                ; LOE eax edx ecx ebx esi edi xmm0
.B1.38:                         ; Preds .B1.38 .B1.37
        inc       edi                                           ;182.7
        mov       DWORD PTR [36+esi+edx], eax                   ;182.7
        mov       DWORD PTR [80+esi+edx], eax                   ;182.7
        add       esi, 88                                       ;182.7
        cmp       edi, ecx                                      ;182.7
        jb        .B1.38        ; Prob 63%                      ;182.7
                                ; LOE eax edx ecx ebx esi edi xmm0
.B1.39:                         ; Preds .B1.38
        mov       esi, DWORD PTR [esp]                          ;
        lea       ecx, DWORD PTR [1+edi+edi]                    ;182.7
                                ; LOE edx ecx ebx esi xmm0
.B1.40:                         ; Preds .B1.39 .B1.97
        lea       eax, DWORD PTR [-1+ecx]                       ;182.7
        cmp       ebx, eax                                      ;182.7
        jbe       .B1.42        ; Prob 10%                      ;182.7
                                ; LOE edx ecx esi xmm0
.B1.41:                         ; Preds .B1.40
        mov       eax, esi                                      ;182.7
        add       ecx, esi                                      ;182.7
        neg       eax                                           ;182.7
        add       eax, ecx                                      ;182.7
        imul      ecx, eax, 44                                  ;182.7
        mov       DWORD PTR [-8+edx+ecx], 0                     ;182.7
                                ; LOE xmm0
.B1.42:                         ; Preds .B1.35 .B1.40 .B1.41
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DESTINATION+24] ;185.7
        xor       eax, eax                                      ;183.7
        pxor      xmm1, xmm1                                    ;183.7
        test      ecx, ecx                                      ;185.7
        movups    XMMWORD PTR [_DYNUST_MAIN_MODULE_mp_CMA], xmm1 ;183.7
        movdqu    XMMWORD PTR [_DYNUST_MAIN_MODULE_mp_CMALINK], xmm0 ;184.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_CMA+16], eax ;183.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_CMA+20], eax ;183.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_CMALINK+16], eax ;184.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_CMALINK+20], eax ;184.7
        jle       .B1.45        ; Prob 50%                      ;185.7
                                ; LOE ecx xmm0 xmm1
.B1.43:                         ; Preds .B1.42
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DESTINATION] ;185.7
        cmp       ecx, 24                                       ;185.7
        jle       .B1.98        ; Prob 0%                       ;185.7
                                ; LOE edx ecx xmm0 xmm1
.B1.44:                         ; Preds .B1.43
        shl       ecx, 2                                        ;185.7
        push      ecx                                           ;185.7
        push      0                                             ;185.7
        push      edx                                           ;185.7
        call      __intel_fast_memset                           ;185.7
                                ; LOE
.B1.141:                        ; Preds .B1.44
        add       esp, 12                                       ;185.7
        pxor      xmm0, xmm0                                    ;
        pxor      xmm1, xmm1                                    ;
                                ; LOE xmm0 xmm1
.B1.45:                         ; Preds .B1.112 .B1.42 .B1.141 .B1.110
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_MASTERDEST+24] ;186.4
        test      edx, edx                                      ;186.4
        jle       .B1.48        ; Prob 50%                      ;186.4
                                ; LOE edx xmm0 xmm1
.B1.46:                         ; Preds .B1.45
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_MASTERDEST] ;186.4
        cmp       edx, 48                                       ;186.4
        jle       .B1.117       ; Prob 0%                       ;186.4
                                ; LOE eax edx xmm0 xmm1
.B1.47:                         ; Preds .B1.46
        add       edx, edx                                      ;186.4
        push      edx                                           ;186.4
        push      0                                             ;186.4
        push      eax                                           ;186.4
        call      __intel_fast_memset                           ;186.4
                                ; LOE
.B1.142:                        ; Preds .B1.47
        add       esp, 12                                       ;186.4
        pxor      xmm0, xmm0                                    ;
        pxor      xmm1, xmm1                                    ;
                                ; LOE xmm0 xmm1
.B1.48:                         ; Preds .B1.123 .B1.45 .B1.121 .B1.142
        xor       ecx, ecx                                      ;187.7
        xor       edx, edx                                      ;
        xor       eax, eax                                      ;
                                ; LOE eax edx ecx xmm0 xmm1
.B1.49:                         ; Preds .B1.51 .B1.48
        mov       ebx, eax                                      ;187.7
                                ; LOE eax edx ecx ebx xmm0 xmm1
.B1.50:                         ; Preds .B1.50 .B1.49
        lea       esi, DWORD PTR [ebx+ebx*4]                    ;187.7
        inc       ebx                                           ;187.7
        movdqu    XMMWORD PTR [_DYNUST_MAIN_MODULE_mp_LEFTCAPWB+edx+esi*4], xmm0 ;187.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_LEFTCAPWB+16+edx+esi*4], 0 ;187.7
        cmp       ebx, 3                                        ;187.7
        jb        .B1.50        ; Prob 66%                      ;187.7
                                ; LOE eax edx ecx ebx xmm0 xmm1
.B1.51:                         ; Preds .B1.50
        inc       ecx                                           ;187.7
        add       edx, 60                                       ;187.7
        cmp       ecx, 7                                        ;187.7
        jb        .B1.49        ; Prob 85%                      ;187.7
                                ; LOE eax edx ecx xmm0 xmm1
.B1.52:                         ; Preds .B1.51
        push      2940                                          ;188.7
        push      0                                             ;188.7
        push      OFFSET FLAT: _DYNUST_MAIN_MODULE_mp_LEFTCAPWOB ;188.7
        call      __intel_fast_memset                           ;188.7
                                ; LOE
.B1.143:                        ; Preds .B1.52
        add       esp, 12                                       ;188.7
        pxor      xmm0, xmm0                                    ;
        pxor      xmm1, xmm1                                    ;
                                ; LOE xmm0 xmm1
.B1.53:                         ; Preds .B1.143
        mov       ebx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE+24] ;192.7
        xor       eax, eax                                      ;189.4
        mov       esi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE+32] ;192.7
        test      ebx, ebx                                      ;192.7
        movups    XMMWORD PTR [_DYNUST_VEH_MODULE_mp_CLASSPRO], xmm1 ;189.4
        movups    XMMWORD PTR [_DYNUST_VEH_MODULE_mp_CLASSPRO2], xmm1 ;190.7
        movdqu    XMMWORD PTR [_DYNUST_MAIN_MODULE_mp_NODETMP], xmm0 ;191.7
        mov       DWORD PTR [_DYNUST_VEH_MODULE_mp_CLASSPRO+16], eax ;189.4
        mov       DWORD PTR [_DYNUST_VEH_MODULE_mp_CLASSPRO2+16], eax ;190.7
        mov       DWORD PTR [_DYNUST_VEH_MODULE_mp_CLASSPRO2+20], eax ;190.7
        mov       DWORD PTR [_DYNUST_VEH_MODULE_mp_CLASSPRO2+24], eax ;190.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_NODETMP+16], eax ;191.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_NODETMP+20], eax ;191.7
        jle       .B1.145       ; Prob 50%                      ;192.7
                                ; LOE ebx esi
.B1.54:                         ; Preds .B1.53
        mov       edx, ebx                                      ;192.7
        shr       edx, 31                                       ;192.7
        add       edx, ebx                                      ;192.7
        sar       edx, 1                                        ;192.7
        mov       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE] ;192.7
        test      edx, edx                                      ;192.7
        jbe       .B1.126       ; Prob 11%                      ;192.7
                                ; LOE edx ecx ebx esi
.B1.55:                         ; Preds .B1.54
        mov       DWORD PTR [esp], esi                          ;
        xor       eax, eax                                      ;
        mov       edi, 1                                        ;
                                ; LOE eax edx ecx ebx edi
.B1.56:                         ; Preds .B1.56 .B1.55
        mov       esi, eax                                      ;192.7
        inc       eax                                           ;192.7
        shl       esi, 7                                        ;192.7
        cmp       eax, edx                                      ;192.7
        mov       WORD PTR [12+ecx+esi], di                     ;192.7
        mov       WORD PTR [76+ecx+esi], di                     ;192.7
        jb        .B1.56        ; Prob 64%                      ;192.7
                                ; LOE eax edx ecx ebx edi
.B1.57:                         ; Preds .B1.56
        mov       esi, DWORD PTR [esp]                          ;
        lea       edx, DWORD PTR [1+eax+eax]                    ;192.7
                                ; LOE edx ecx ebx esi
.B1.58:                         ; Preds .B1.57 .B1.126
        lea       eax, DWORD PTR [-1+edx]                       ;192.7
        cmp       ebx, eax                                      ;192.7
        jbe       .B1.145       ; Prob 11%                      ;192.7
                                ; LOE edx ecx esi
.B1.59:                         ; Preds .B1.58
        mov       eax, esi                                      ;192.7
        add       edx, esi                                      ;192.7
        neg       eax                                           ;192.7
        mov       esi, 1                                        ;192.7
        add       eax, edx                                      ;192.7
        shl       eax, 6                                        ;192.7
        cmp       DWORD PTR [12+esp], 0                         ;192.7
        mov       WORD PTR [-52+ecx+eax], si                    ;192.7
        jmp       .B1.60        ; Prob 100%                     ;192.7
                                ; LOE
.B1.145:                        ; Preds .B1.58 .B1.53
        cmp       DWORD PTR [12+esp], 0                         ;36.15
                                ; LOE
.B1.60:                         ; Preds .B1.3 .B1.145 .B1.59
        mov       eax, 0                                        ;198.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_IACTUAL_LOV_HOT], eax ;198.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_IACTUAL_HOV_HOT], eax ;199.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_IACTUAL_LOV_OHOT], eax ;200.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_IACTUAL_HOV_OHOT], eax ;201.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_TIME_LOV_HOT], eax ;202.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_TIME_HOV_HOT], eax ;203.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_TIME_LOV_OHOT], eax ;204.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_TIME_HOV_OHOT], eax ;205.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_LINK_HOT], eax ;206.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_KTOTAL_OUT], eax ;209.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_CNTDEMTIME], eax ;211.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGNCOUNT], eax ;212.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_KSPMEMONCALL], eax ;214.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_KSPSUBONCALL], eax ;215.2
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_MOP], 1 ;216.5
        jne       .B1.62        ; Prob 50%                      ;217.15
                                ; LOE
.B1.61:                         ; Preds .B1.60
        xor       eax, eax                                      ;217.21
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_DEMANDMIDCOUNTER], eax ;217.21
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_AUTOCT], eax ;219.4
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRUCKCT], eax ;220.4
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_CALLKSP1], eax ;223.5
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_NUMNTAG], eax ;225.5
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_MAXMOVE], eax ;227.5
        jmp       .B1.70        ; Prob 100%                     ;227.5
                                ; LOE
.B1.62:                         ; Preds .B1.60
        mov       eax, 0                                        ;223.5
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_CALLKSP1], eax ;223.5
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_NUMNTAG], eax ;225.5
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_MAXMOVE], eax ;227.5
        jle       .B1.70        ; Prob 16%                      ;230.18
                                ; LOE
.B1.63:                         ; Preds .B1.62
        mov       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+24] ;230.23
        test      ecx, ecx                                      ;230.23
        mov       edi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;230.23
        jle       .B1.70        ; Prob 10%                      ;230.23
                                ; LOE ecx edi
.B1.64:                         ; Preds .B1.63
        mov       edx, ecx                                      ;230.23
        shr       edx, 31                                       ;230.23
        add       edx, ecx                                      ;230.23
        sar       edx, 1                                        ;230.23
        mov       ebx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;230.23
        test      edx, edx                                      ;230.23
        jbe       .B1.127       ; Prob 0%                       ;230.23
                                ; LOE edx ecx ebx edi
.B1.65:                         ; Preds .B1.64
        xor       eax, eax                                      ;
        mov       DWORD PTR [esp], edi                          ;
        xor       esi, esi                                      ;
        mov       edi, eax                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B1.66:                         ; Preds .B1.66 .B1.65
        inc       edi                                           ;230.23
        mov       DWORD PTR [8+esi+ebx], eax                    ;230.23
        mov       DWORD PTR [40+esi+ebx], eax                   ;230.23
        add       esi, 64                                       ;230.23
        cmp       edi, edx                                      ;230.23
        jb        .B1.66        ; Prob 64%                      ;230.23
                                ; LOE eax edx ecx ebx esi edi
.B1.67:                         ; Preds .B1.66
        mov       eax, edi                                      ;
        mov       edi, DWORD PTR [esp]                          ;
        lea       edx, DWORD PTR [1+eax+eax]                    ;230.23
                                ; LOE edx ecx ebx edi
.B1.68:                         ; Preds .B1.67 .B1.127
        lea       eax, DWORD PTR [-1+edx]                       ;230.23
        cmp       ecx, eax                                      ;230.23
        jbe       .B1.70        ; Prob 0%                       ;230.23
                                ; LOE edx ebx edi
.B1.69:                         ; Preds .B1.68
        mov       eax, edi                                      ;230.23
        add       edx, edi                                      ;230.23
        neg       eax                                           ;230.23
        add       eax, edx                                      ;230.23
        shl       eax, 5                                        ;230.23
        mov       DWORD PTR [-24+ebx+eax], 0                    ;230.23
                                ; LOE
.B1.70:                         ; Preds .B1.62 .B1.63 .B1.68 .B1.61 .B1.69
                                ;      
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_CALLKSPREADVEH], 0 ;232.5
        add       esp, 52                                       ;233.5
        pop       ebx                                           ;233.5
        pop       edi                                           ;233.5
        pop       esi                                           ;233.5
        mov       esp, ebp                                      ;233.5
        pop       ebp                                           ;233.5
        ret                                                     ;233.5
                                ; LOE
.B1.71:                         ; Preds .B1.32
        imul      ebx, DWORD PTR [20+esp]                       ;
        xor       edx, edx                                      ;
        shl       eax, 3                                        ;
        xor       edi, edi                                      ;
        mov       edx, DWORD PTR [24+esp]                       ;
        mov       esi, ecx                                      ;
        sub       edx, eax                                      ;
        sub       eax, ebx                                      ;
        shr       esi, 31                                       ;
        add       edx, eax                                      ;
        add       esi, ecx                                      ;
        add       edx, ebx                                      ;
        sar       esi, 1                                        ;
        xor       eax, eax                                      ;
        mov       DWORD PTR [4+esp], edx                        ;
        mov       DWORD PTR [36+esp], ecx                       ;
                                ; LOE eax esi edi
.B1.72:                         ; Preds .B1.77 .B1.129 .B1.71
        test      esi, esi                                      ;180.7
        jbe       .B1.131       ; Prob 10%                      ;180.7
                                ; LOE eax esi edi
.B1.73:                         ; Preds .B1.72
        mov       ebx, DWORD PTR [24+esp]                       ;
        xor       ecx, ecx                                      ;
        mov       DWORD PTR [esp], edi                          ;
        mov       DWORD PTR [8+esp], eax                        ;
        lea       edx, DWORD PTR [ebx+eax]                      ;
        mov       ebx, DWORD PTR [4+esp]                        ;
        mov       edi, ecx                                      ;
        add       ebx, eax                                      ;
        xor       eax, eax                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B1.74:                         ; Preds .B1.74 .B1.73
        inc       ecx                                           ;180.7
        mov       DWORD PTR [edi+edx], eax                      ;180.7
        mov       DWORD PTR [8+edi+ebx], eax                    ;180.7
        add       edi, 16                                       ;180.7
        cmp       ecx, esi                                      ;180.7
        jb        .B1.74        ; Prob 63%                      ;180.7
                                ; LOE eax edx ecx ebx esi edi
.B1.75:                         ; Preds .B1.74
        mov       eax, DWORD PTR [8+esp]                        ;
        lea       ecx, DWORD PTR [1+ecx+ecx]                    ;180.7
        mov       edi, DWORD PTR [esp]                          ;
                                ; LOE eax ecx esi edi
.B1.76:                         ; Preds .B1.75 .B1.131
        lea       ebx, DWORD PTR [-1+ecx]                       ;180.7
        cmp       ebx, DWORD PTR [36+esp]                       ;180.7
        jae       .B1.129       ; Prob 10%                      ;180.7
                                ; LOE eax ecx esi edi
.B1.77:                         ; Preds .B1.76
        mov       ebx, DWORD PTR [4+esp]                        ;180.7
        inc       edi                                           ;180.7
        lea       ecx, DWORD PTR [ebx+ecx*8]                    ;180.7
        mov       DWORD PTR [-8+ecx+eax], 0                     ;180.7
        add       eax, DWORD PTR [20+esp]                       ;180.7
        cmp       edi, DWORD PTR [16+esp]                       ;180.7
        jb        .B1.72        ; Prob 82%                      ;180.7
        jmp       .B1.33        ; Prob 100%                     ;180.7
                                ; LOE eax esi edi
.B1.78:                         ; Preds .B1.31
        mov       edi, ebx                                      ;
        mov       esi, ecx                                      ;
        imul      edi, DWORD PTR [20+esp]                       ;
        xor       edx, edx                                      ;
        shr       esi, 31                                       ;
        add       esi, ecx                                      ;
        mov       DWORD PTR [36+esp], ecx                       ;
        lea       ecx, DWORD PTR [eax*8]                        ;
        sar       esi, 1                                        ;
        mov       DWORD PTR [8+esp], esi                        ;
        mov       esi, DWORD PTR [24+esp]                       ;
        sub       esi, ecx                                      ;
        sub       ecx, edi                                      ;
        add       esi, ecx                                      ;
        mov       DWORD PTR [32+esp], edx                       ;
        add       esi, edi                                      ;
        mov       DWORD PTR [esp], ebx                          ;
        mov       DWORD PTR [28+esp], esi                       ;
        mov       DWORD PTR [4+esp], eax                        ;
        mov       ebx, DWORD PTR [8+esp]                        ;
        mov       ecx, edx                                      ;
                                ; LOE edx ecx ebx esi
.B1.79:                         ; Preds .B1.84 .B1.133 .B1.78
        test      ebx, ebx                                      ;179.7
        jbe       .B1.135       ; Prob 10%                      ;179.7
                                ; LOE edx ecx ebx esi
.B1.80:                         ; Preds .B1.79
        xor       eax, eax                                      ;
        mov       DWORD PTR [32+esp], ecx                       ;
        xor       ecx, ecx                                      ;
                                ; LOE eax edx ecx ebx esi
.B1.81:                         ; Preds .B1.81 .B1.80
        mov       edi, eax                                      ;179.7
        inc       eax                                           ;179.7
        shl       edi, 4                                        ;179.7
        cmp       eax, ebx                                      ;179.7
        mov       BYTE PTR [4+edi+esi], cl                      ;179.7
        mov       BYTE PTR [12+edi+esi], cl                     ;179.7
        jb        .B1.81        ; Prob 63%                      ;179.7
                                ; LOE eax edx ecx ebx esi
.B1.82:                         ; Preds .B1.81
        mov       ecx, DWORD PTR [32+esp]                       ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;179.7
                                ; LOE eax edx ecx ebx esi
.B1.83:                         ; Preds .B1.82 .B1.135
        lea       edi, DWORD PTR [-1+eax]                       ;179.7
        cmp       edi, DWORD PTR [36+esp]                       ;179.7
        jae       .B1.133       ; Prob 10%                      ;179.7
                                ; LOE eax edx ecx ebx esi
.B1.84:                         ; Preds .B1.83
        mov       edi, DWORD PTR [28+esp]                       ;179.7
        inc       ecx                                           ;179.7
        lea       eax, DWORD PTR [edi+eax*8]                    ;179.7
        mov       BYTE PTR [-4+eax+edx], 0                      ;179.7
        mov       eax, DWORD PTR [20+esp]                       ;179.7
        add       esi, eax                                      ;179.7
        add       edx, eax                                      ;179.7
        cmp       ecx, DWORD PTR [16+esp]                       ;179.7
        jb        .B1.79        ; Prob 82%                      ;179.7
        jmp       .B1.134       ; Prob 100%                     ;179.7
                                ; LOE edx ecx ebx esi
.B1.85:                         ; Preds .B1.30
        mov       edi, ebx                                      ;
        mov       esi, ecx                                      ;
        imul      edi, DWORD PTR [20+esp]                       ;
        xor       edx, edx                                      ;
        shr       esi, 31                                       ;
        add       esi, ecx                                      ;
        mov       DWORD PTR [36+esp], ecx                       ;
        lea       ecx, DWORD PTR [eax*8]                        ;
        sar       esi, 1                                        ;
        mov       DWORD PTR [8+esp], esi                        ;
        mov       esi, DWORD PTR [24+esp]                       ;
        sub       esi, ecx                                      ;
        sub       ecx, edi                                      ;
        add       esi, ecx                                      ;
        mov       DWORD PTR [32+esp], edx                       ;
        add       esi, edi                                      ;
        mov       DWORD PTR [esp], ebx                          ;
        mov       DWORD PTR [28+esp], esi                       ;
        mov       DWORD PTR [4+esp], eax                        ;
        mov       ebx, DWORD PTR [8+esp]                        ;
        mov       ecx, edx                                      ;
                                ; LOE edx ecx ebx esi
.B1.86:                         ; Preds .B1.91 .B1.136 .B1.85
        test      ebx, ebx                                      ;178.7
        jbe       .B1.138       ; Prob 10%                      ;178.7
                                ; LOE edx ecx ebx esi
.B1.87:                         ; Preds .B1.86
        xor       eax, eax                                      ;
        mov       DWORD PTR [32+esp], ecx                       ;
        xor       ecx, ecx                                      ;
                                ; LOE eax edx ecx ebx esi
.B1.88:                         ; Preds .B1.88 .B1.87
        mov       edi, eax                                      ;178.7
        inc       eax                                           ;178.7
        shl       edi, 4                                        ;178.7
        cmp       eax, ebx                                      ;178.7
        mov       BYTE PTR [5+edi+esi], cl                      ;178.7
        mov       BYTE PTR [13+edi+esi], cl                     ;178.7
        jb        .B1.88        ; Prob 63%                      ;178.7
                                ; LOE eax edx ecx ebx esi
.B1.89:                         ; Preds .B1.88
        mov       ecx, DWORD PTR [32+esp]                       ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;178.7
                                ; LOE eax edx ecx ebx esi
.B1.90:                         ; Preds .B1.89 .B1.138
        lea       edi, DWORD PTR [-1+eax]                       ;178.7
        cmp       edi, DWORD PTR [36+esp]                       ;178.7
        jae       .B1.136       ; Prob 10%                      ;178.7
                                ; LOE eax edx ecx ebx esi
.B1.91:                         ; Preds .B1.90
        mov       edi, DWORD PTR [28+esp]                       ;178.7
        inc       ecx                                           ;178.7
        lea       eax, DWORD PTR [edi+eax*8]                    ;178.7
        mov       BYTE PTR [-3+eax+edx], 0                      ;178.7
        mov       eax, DWORD PTR [20+esp]                       ;178.7
        add       esi, eax                                      ;178.7
        add       edx, eax                                      ;178.7
        cmp       ecx, DWORD PTR [16+esp]                       ;178.7
        jb        .B1.86        ; Prob 82%                      ;178.7
                                ; LOE edx ecx ebx esi
.B1.92:                         ; Preds .B1.91                  ; Infreq
        mov       ecx, DWORD PTR [36+esp]                       ;
        mov       eax, DWORD PTR [4+esp]                        ;
        mov       ebx, DWORD PTR [esp]                          ;
        jmp       .B1.31        ; Prob 100%                     ;
                                ; LOE eax ecx ebx
.B1.93:                         ; Preds .B1.23                  ; Infreq
        mov       ebx, 1                                        ;
        jmp       .B1.27        ; Prob 100%                     ;
                                ; LOE edx ecx ebx esi
.B1.94:                         ; Preds .B1.17                  ; Infreq
        mov       eax, 1                                        ;
        jmp       .B1.21        ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B1.95:                         ; Preds .B1.11                  ; Infreq
        mov       eax, 1                                        ;
        jmp       .B1.15        ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B1.96:                         ; Preds .B1.5                   ; Infreq
        mov       eax, 1                                        ;
        jmp       .B1.9         ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B1.97:                         ; Preds .B1.36                  ; Infreq
        mov       ecx, 1                                        ;
        jmp       .B1.40        ; Prob 100%                     ;
                                ; LOE edx ecx ebx esi xmm0
.B1.98:                         ; Preds .B1.43                  ; Infreq
        cmp       ecx, 4                                        ;185.7
        jl        .B1.114       ; Prob 10%                      ;185.7
                                ; LOE edx ecx xmm0 xmm1
.B1.99:                         ; Preds .B1.98                  ; Infreq
        mov       eax, edx                                      ;185.7
        and       eax, 15                                       ;185.7
        je        .B1.102       ; Prob 50%                      ;185.7
                                ; LOE eax edx ecx xmm0 xmm1
.B1.100:                        ; Preds .B1.99                  ; Infreq
        test      al, 3                                         ;185.7
        jne       .B1.114       ; Prob 10%                      ;185.7
                                ; LOE eax edx ecx xmm0 xmm1
.B1.101:                        ; Preds .B1.100                 ; Infreq
        neg       eax                                           ;185.7
        add       eax, 16                                       ;185.7
        shr       eax, 2                                        ;185.7
                                ; LOE eax edx ecx xmm0 xmm1
.B1.102:                        ; Preds .B1.101 .B1.99          ; Infreq
        lea       ebx, DWORD PTR [4+eax]                        ;185.7
        cmp       ecx, ebx                                      ;185.7
        jl        .B1.114       ; Prob 10%                      ;185.7
                                ; LOE eax edx ecx xmm0 xmm1
.B1.103:                        ; Preds .B1.102                 ; Infreq
        mov       esi, ecx                                      ;185.7
        sub       esi, eax                                      ;185.7
        and       esi, 3                                        ;185.7
        neg       esi                                           ;185.7
        add       esi, ecx                                      ;185.7
        test      eax, eax                                      ;185.7
        jbe       .B1.108       ; Prob 11%                      ;185.7
                                ; LOE eax edx ecx esi xmm0 xmm1
.B1.104:                        ; Preds .B1.103                 ; Infreq
        xor       ebx, ebx                                      ;
                                ; LOE eax edx ecx ebx esi xmm0 xmm1
.B1.105:                        ; Preds .B1.105 .B1.104         ; Infreq
        mov       DWORD PTR [edx+ebx*4], 0                      ;185.7
        inc       ebx                                           ;185.7
        cmp       ebx, eax                                      ;185.7
        jb        .B1.105       ; Prob 82%                      ;185.7
                                ; LOE eax edx ecx ebx esi xmm0 xmm1
.B1.108:                        ; Preds .B1.103 .B1.105 .B1.108 ; Infreq
        movdqa    XMMWORD PTR [edx+eax*4], xmm0                 ;185.7
        add       eax, 4                                        ;185.7
        cmp       eax, esi                                      ;185.7
        jb        .B1.108       ; Prob 82%                      ;185.7
                                ; LOE eax edx ecx esi xmm0 xmm1
.B1.110:                        ; Preds .B1.108 .B1.114         ; Infreq
        cmp       esi, ecx                                      ;185.7
        jae       .B1.45        ; Prob 11%                      ;185.7
                                ; LOE edx ecx esi xmm0 xmm1
.B1.112:                        ; Preds .B1.110 .B1.112         ; Infreq
        mov       DWORD PTR [edx+esi*4], 0                      ;185.7
        inc       esi                                           ;185.7
        cmp       esi, ecx                                      ;185.7
        jb        .B1.112       ; Prob 82%                      ;185.7
        jmp       .B1.45        ; Prob 100%                     ;185.7
                                ; LOE edx ecx esi xmm0 xmm1
.B1.114:                        ; Preds .B1.98 .B1.102 .B1.100  ; Infreq
        xor       eax, eax                                      ;185.7
        xor       esi, esi                                      ;185.7
        jmp       .B1.110       ; Prob 100%                     ;185.7
                                ; LOE edx ecx esi xmm0 xmm1
.B1.117:                        ; Preds .B1.46                  ; Infreq
        cmp       edx, 8                                        ;186.4
        jl        .B1.125       ; Prob 10%                      ;186.4
                                ; LOE eax edx xmm0 xmm1
.B1.118:                        ; Preds .B1.117                 ; Infreq
        mov       ebx, edx                                      ;186.4
        xor       ecx, ecx                                      ;186.4
        and       ebx, -8                                       ;186.4
                                ; LOE eax edx ecx ebx xmm0 xmm1
.B1.119:                        ; Preds .B1.119 .B1.118         ; Infreq
        movdqu    XMMWORD PTR [eax+ecx*2], xmm0                 ;186.4
        add       ecx, 8                                        ;186.4
        cmp       ecx, ebx                                      ;186.4
        jb        .B1.119       ; Prob 82%                      ;186.4
                                ; LOE eax edx ecx ebx xmm0 xmm1
.B1.121:                        ; Preds .B1.119 .B1.125         ; Infreq
        cmp       ebx, edx                                      ;186.4
        jae       .B1.48        ; Prob 11%                      ;186.4
                                ; LOE eax edx ebx xmm0 xmm1
.B1.122:                        ; Preds .B1.121                 ; Infreq
        xor       ecx, ecx                                      ;
                                ; LOE eax edx ecx ebx xmm0 xmm1
.B1.123:                        ; Preds .B1.123 .B1.122         ; Infreq
        mov       WORD PTR [eax+ebx*2], cx                      ;186.4
        inc       ebx                                           ;186.4
        cmp       ebx, edx                                      ;186.4
        jb        .B1.123       ; Prob 82%                      ;186.4
        jmp       .B1.48        ; Prob 100%                     ;186.4
                                ; LOE eax edx ecx ebx xmm0 xmm1
.B1.125:                        ; Preds .B1.117                 ; Infreq
        xor       ebx, ebx                                      ;186.4
        jmp       .B1.121       ; Prob 100%                     ;186.4
                                ; LOE eax edx ebx xmm0 xmm1
.B1.126:                        ; Preds .B1.54                  ; Infreq
        mov       edx, 1                                        ;
        jmp       .B1.58        ; Prob 100%                     ;
                                ; LOE edx ecx ebx esi
.B1.127:                        ; Preds .B1.64                  ; Infreq
        mov       edx, 1                                        ;
        jmp       .B1.68        ; Prob 100%                     ;
                                ; LOE edx ecx ebx edi
.B1.129:                        ; Preds .B1.76                  ; Infreq
        inc       edi                                           ;180.7
        add       eax, DWORD PTR [20+esp]                       ;180.7
        cmp       edi, DWORD PTR [16+esp]                       ;180.7
        jb        .B1.72        ; Prob 82%                      ;180.7
        jmp       .B1.33        ; Prob 100%                     ;180.7
                                ; LOE eax esi edi
.B1.131:                        ; Preds .B1.72                  ; Infreq
        mov       ecx, 1                                        ;
        jmp       .B1.76        ; Prob 100%                     ;
                                ; LOE eax ecx esi edi
.B1.133:                        ; Preds .B1.83                  ; Infreq
        inc       ecx                                           ;179.7
        mov       eax, DWORD PTR [20+esp]                       ;179.7
        add       esi, eax                                      ;179.7
        add       edx, eax                                      ;179.7
        cmp       ecx, DWORD PTR [16+esp]                       ;179.7
        jb        .B1.79        ; Prob 82%                      ;179.7
                                ; LOE edx ecx ebx esi
.B1.134:                        ; Preds .B1.84 .B1.133          ; Infreq
        mov       ecx, DWORD PTR [36+esp]                       ;
        mov       eax, DWORD PTR [4+esp]                        ;
        mov       ebx, DWORD PTR [esp]                          ;
        jmp       .B1.32        ; Prob 100%                     ;
                                ; LOE eax ecx ebx
.B1.135:                        ; Preds .B1.79                  ; Infreq
        mov       eax, 1                                        ;
        jmp       .B1.83        ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B1.136:                        ; Preds .B1.90                  ; Infreq
        inc       ecx                                           ;178.7
        mov       eax, DWORD PTR [20+esp]                       ;178.7
        add       esi, eax                                      ;178.7
        add       edx, eax                                      ;178.7
        cmp       ecx, DWORD PTR [16+esp]                       ;178.7
        jb        .B1.86        ; Prob 82%                      ;178.7
                                ; LOE edx ecx ebx esi
.B1.137:                        ; Preds .B1.136                 ; Infreq
        mov       ecx, DWORD PTR [36+esp]                       ;
        mov       eax, DWORD PTR [4+esp]                        ;
        mov       ebx, DWORD PTR [esp]                          ;
        jmp       .B1.31        ; Prob 100%                     ;
                                ; LOE eax ecx ebx
.B1.138:                        ; Preds .B1.86                  ; Infreq
        mov       eax, 1                                        ;
        jmp       .B1.90        ; Prob 100%                     ;
        ALIGN     16
                                ; LOE eax edx ecx ebx esi
; mark_end;
_INITIALIZE ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _INITIALIZE
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
EXTRN	_DYNUST_MAIN_MODULE_mp_CALLKSPREADVEH:BYTE
EXTRN	_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NUMNTAG:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_CALLKSP1:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TRUCKCT:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_AUTOCT:BYTE
EXTRN	_DYNUST_VEH_PATH_ATT_MODULE_mp_DEMANDMIDCOUNTER:BYTE
EXTRN	_DYNUST_VEH_PATH_ATT_MODULE_mp_MOP:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_KSPSUBONCALL:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_KSPMEMONCALL:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_SIGNCOUNT:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_CNTDEMTIME:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_KTOTAL_OUT:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_LINK_HOT:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TIME_HOV_OHOT:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TIME_LOV_OHOT:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TIME_HOV_HOT:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TIME_LOV_HOT:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_IACTUAL_HOV_OHOT:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_IACTUAL_LOV_OHOT:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_IACTUAL_HOV_HOT:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_IACTUAL_LOV_HOT:BYTE
EXTRN	_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NODETMP:BYTE
EXTRN	_DYNUST_VEH_MODULE_mp_CLASSPRO2:BYTE
EXTRN	_DYNUST_VEH_MODULE_mp_CLASSPRO:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_LEFTCAPWOB:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_LEFTCAPWB:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_MASTERDEST:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_DESTINATION:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_CMALINK:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_CMA:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ALMOV:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_JRESTORE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_INCI_NUM:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_VMS_NUM:BYTE
EXTRN	_DYNUST_VEH_MODULE_mp_NOOFBUSES:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_OLDNUMCARS:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NUMCARS:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ISIGCOUNT:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TIME_NOW:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_VAVG3_3:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_VAVG3_2:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_VAVG3_1:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_VAVG3:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_VAVG2_3:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_VAVG2_2:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_VAVG2_1:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_VAVG2:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_VAVG1:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NOUT_NONTAG_I:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NOUT_TAG_I:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NOUT_NONTAG:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NOUT_TAG:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TOTALSWITCH:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TOTALDECSION:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_AVEDTOTAL3_3:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_AVEDTOTAL3_2:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_AVEDTOTAL3_1:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_AVEDTOTAL3:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_AVEDTOTAL2_3:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_AVEDTOTAL2_2:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_AVEDTOTAL2_1:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_AVEDTOTAL2:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_AVEDTOTAL1:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_AVE_ENTRY3_3:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_AVE_ENTRY3_2:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_AVE_ENTRY3_1:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_AVE_ENTRY3:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_AVE_ENTRY2_3:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_AVE_ENTRY2_2:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_AVE_ENTRY2_1:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_AVE_ENTRY2:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_AVE_ENTRY1:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ENTRY_QUEUE3_3:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ENTRY_QUEUE3_2:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ENTRY_QUEUE3_1:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ENTRY_QUEUE3:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ENTRY_QUEUE2_3:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ENTRY_QUEUE2_2:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ENTRY_QUEUE2_1:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ENTRY_QUEUE2:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ENTRY_QUEUE1:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_AVESTOPNOINFO_3:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_AVESTOPNOINFO_2:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_AVESTOPNOINFO_1:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_AVESTOPNOINFO:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_AVESTOPINFO_3:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_AVESTOPINFO_2:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_AVESTOPINFO_1:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_AVESTOPINFO:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_AVESTOPTIME:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_STOPNOINFO_3:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_STOPNOINFO_2:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_STOPNOINFO_1:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_STOPNOINFO:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_STOPINFO_3:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_STOPINFO_2:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_STOPINFO_1:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_STOPINFO:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_STOPTIME:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_AVE_TRIP3_3:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_AVE_TRIP3_2:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_AVE_TRIP3_1:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_AVE_TRIP3:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_AVE_TRIP2_3:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_AVE_TRIP2_2:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_AVE_TRIP2_1:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_AVE_TRIP2:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_AVE_TRIP1:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TRIPTIME3_3:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TRIPTIME3_2:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TRIPTIME3_1:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TRIPTIME3:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TRIPTIME2_3:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TRIPTIME2_2:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TRIPTIME2_1:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TRIPTIME2:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TRIPTIME1:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_VTOTHR3_3:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_VTOTHR3_2:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_VTOTHR3_1:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_VTOTHR3:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_VTOTHR2_3:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_VTOTHR2_2:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_VTOTHR2_1:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_VTOTHR2:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_VTOTHR1:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_DTOTAL3_3:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_DTOTAL3_2:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_DTOTAL3_1:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_DTOTAL3:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_DTOTAL2_3:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_DTOTAL2_2:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_DTOTAL2_1:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_DTOTAL2:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_DTOTAL1:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NOINFORMATION_3:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NOINFORMATION_2:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NOINFORMATION_1:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NOINFORMATION:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_INFORMATION_3:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_INFORMATION_2:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_INFORMATION_1:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_INFORMATION:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ITAG3:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ITAG2:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ITAG1:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ITAG0:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_INT_D:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_LISTTOTAL:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_MULTI:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_JUSTVEH_I:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_JUSTVEH:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_JTOTAL:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_IER_OK:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_IUE_OK:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ISO_OK:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ITERATION:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_RANCT:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_MAXMOVE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_GUITOTALTIME:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_INIID2:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_INIID1:BYTE
_DATA	ENDS
EXTRN	__intel_fast_memset:PROC
EXTRN	__fltused:BYTE
	INCLUDELIB <ifconsol>
	INCLUDELIB <libifcoremt>
	INCLUDELIB <libifport>
	INCLUDELIB <libmmt>
	INCLUDELIB <LIBCMT>
	INCLUDELIB <libirc>
	INCLUDELIB <svml_dispmt>
	INCLUDELIB <OLDNAMES>
	END
