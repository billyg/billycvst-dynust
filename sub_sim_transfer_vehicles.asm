; mark_description "Intel(R) Visual Fortran Compiler XE for applications running on IA-32, Version 13.1.1.171 Build 20130313";
; mark_description "-S";
	.686P
 	.387
	OPTION DOTNAME
	ASSUME	CS:FLAT,DS:FLAT,SS:FLAT
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
TXTST0:
; -- Begin  _SIM_TRANSFER_VEHICLES
; mark_begin;
IF @Version GE 800
  .MMX
ELSEIF @Version GE 612
  .MMX
  MMWORD TEXTEQU <QWORD>
ENDIF
IF @Version GE 800
  .XMM
ELSEIF @Version GE 614
  .XMM
  XMMWORD TEXTEQU <OWORD>
ENDIF
       ALIGN     16
	PUBLIC _SIM_TRANSFER_VEHICLES
_SIM_TRANSFER_VEHICLES	PROC NEAR 
; parameter 1: 8 + ebp
; parameter 2: 12 + ebp
; parameter 3: 16 + ebp
; parameter 4: 20 + ebp
.B1.1:                          ; Preds .B1.0
        push      ebp                                           ;1.18
        mov       ebp, esp                                      ;1.18
        and       esp, -16                                      ;1.18
        push      esi                                           ;1.18
        push      edi                                           ;1.18
        push      ebx                                           ;1.18
        sub       esp, 692                                      ;1.18
        mov       ecx, DWORD PTR [8+ebp]                        ;1.18
        mov       ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIMPERAGG] ;42.4
        mov       eax, DWORD PTR [ecx]                          ;42.4
        cdq                                                     ;42.4
        idiv      ebx                                           ;42.4
        test      edx, edx                                      ;43.17
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+24] ;45.4
        cmove     edx, ebx                                      ;43.17
        test      eax, eax                                      ;45.4
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;45.4
        jle       .B1.8         ; Prob 50%                      ;45.4
                                ; LOE eax edx ebx
.B1.2:                          ; Preds .B1.1
        mov       esi, eax                                      ;45.4
        shr       esi, 31                                       ;45.4
        add       esi, eax                                      ;45.4
        sar       esi, 1                                        ;45.4
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;45.4
        test      esi, esi                                      ;45.4
        jbe       .B1.130       ; Prob 10%                      ;45.4
                                ; LOE eax edx ecx ebx esi
.B1.3:                          ; Preds .B1.2
        xor       edi, edi                                      ;
        mov       DWORD PTR [esp], edx                          ;
        mov       DWORD PTR [228+esp], ebx                      ;
        xor       ebx, ebx                                      ;
        mov       edx, ebx                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B1.4:                          ; Preds .B1.4 .B1.3
        inc       edi                                           ;45.4
        mov       DWORD PTR [400+edx+ecx], ebx                  ;45.4
        mov       DWORD PTR [1300+edx+ecx], ebx                 ;45.4
        add       edx, 1800                                     ;45.4
        cmp       edi, esi                                      ;45.4
        jb        .B1.4         ; Prob 63%                      ;45.4
                                ; LOE eax edx ecx ebx esi edi
.B1.5:                          ; Preds .B1.4
        mov       ebx, DWORD PTR [228+esp]                      ;
        lea       esi, DWORD PTR [1+edi+edi]                    ;45.4
        mov       edx, DWORD PTR [esp]                          ;
                                ; LOE eax edx ecx ebx esi
.B1.6:                          ; Preds .B1.5 .B1.130
        lea       edi, DWORD PTR [-1+esi]                       ;45.4
        cmp       eax, edi                                      ;45.4
        jbe       .B1.8         ; Prob 10%                      ;45.4
                                ; LOE edx ecx ebx esi
.B1.7:                          ; Preds .B1.6
        mov       eax, ebx                                      ;45.4
        add       esi, ebx                                      ;45.4
        neg       eax                                           ;45.4
        add       eax, esi                                      ;45.4
        imul      esi, eax, 900                                 ;45.4
        mov       DWORD PTR [-500+ecx+esi], 0                   ;45.4
                                ; LOE edx ebx
.B1.8:                          ; Preds .B1.1 .B1.6 .B1.7
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFARCS] ;48.7
        mov       ecx, 1                                        ;48.7
        test      eax, eax                                      ;48.7
        jle       .B1.220       ; Prob 2%                       ;48.7
                                ; LOE eax edx ecx ebx
.B1.9:                          ; Preds .B1.8
        xor       esi, esi                                      ;
        lea       edx, DWORD PTR [edx+edx*4]                    ;
        mov       DWORD PTR [668+esp], 1                        ;48.7
        movss     xmm3, DWORD PTR [_2il0floatpacket.6]          ;54.51
        mov       DWORD PTR [236+esp], esi                      ;179.78
        mov       DWORD PTR [664+esp], edx                      ;179.78
        mov       DWORD PTR [632+esp], esi                      ;179.78
        mov       DWORD PTR [608+esp], eax                      ;179.78
        jmp       .B1.10        ; Prob 100%                     ;179.78
                                ; LOE
.B1.129:                        ; Preds .B1.128
        mov       DWORD PTR [668+esp], ecx                      ;48.7
                                ; LOE
.B1.10:                         ; Preds .B1.129 .B1.9
        push      OFFSET FLAT: __NLITPACK_0.0.1                 ;53.9
        call      _RANXY                                        ;53.9
                                ; LOE f1
.B1.250:                        ; Preds .B1.10
        fstp      DWORD PTR [396+esp]                           ;53.9
        movss     xmm2, DWORD PTR [396+esp]                     ;53.9
        add       esp, 4                                        ;53.9
                                ; LOE xmm2
.B1.11:                         ; Preds .B1.250
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;54.13
        xor       esi, esi                                      ;
        neg       eax                                           ;54.10
        add       eax, DWORD PTR [668+esp]                      ;54.10
        imul      ebx, eax, 900                                 ;54.10
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;54.4
        mov       DWORD PTR [656+esp], 0                        ;64.7
        movss     xmm1, DWORD PTR [432+ecx+ebx]                 ;54.13
        mulss     xmm1, DWORD PTR [_2il0floatpacket.6]          ;54.56
        cvttss2si eax, xmm1                                     ;54.56
        cvtsi2ss  xmm0, eax                                     ;54.56
        mov       ecx, DWORD PTR [700+ecx+ebx]                  ;59.4
        lea       edx, DWORD PTR [1+eax]                        ;55.4
        subss     xmm1, xmm0                                    ;54.55
        comiss    xmm1, xmm2                                    ;57.5
        cmova     eax, edx                                      ;57.5
        test      eax, eax                                      ;65.13
        jle       .B1.103       ; Prob 16%                      ;65.13
                                ; LOE eax ecx esi
.B1.12:                         ; Preds .B1.11
        xor       edx, edx                                      ;
        mov       DWORD PTR [636+esp], esi                      ;
        mov       DWORD PTR [624+esp], edx                      ;
        mov       DWORD PTR [660+esp], ecx                      ;
        mov       DWORD PTR [628+esp], eax                      ;
                                ; LOE
.B1.13:                         ; Preds .B1.101 .B1.12
        mov       eax, DWORD PTR [656+esp]                      ;66.19
        cmp       eax, DWORD PTR [660+esp]                      ;66.34
        jge       .B1.102       ; Prob 20%                      ;66.34
                                ; LOE eax
.B1.14:                         ; Preds .B1.13
        inc       eax                                           ;67.7
        lea       ebx, DWORD PTR [656+esp]                      ;70.11
        mov       DWORD PTR [656+esp], eax                      ;67.7
        lea       eax, DWORD PTR [668+esp]                      ;70.11
        push      OFFSET FLAT: __NLITPACK_1.0.1                 ;70.11
        push      ebx                                           ;70.11
        push      eax                                           ;70.11
        call      _DYNUST_TRANSLINK_MODULE_mp_TRANLINK_VALUE    ;70.11
                                ; LOE eax ebx
.B1.15:                         ; Preds .B1.14
        mov       DWORD PTR [688+esp], eax                      ;70.6
        push      OFFSET FLAT: __NLITPACK_2.0.1                 ;71.11
        push      ebx                                           ;71.11
        lea       eax, DWORD PTR [688+esp]                      ;71.11
        push      eax                                           ;71.11
        call      _DYNUST_TRANSLINK_MODULE_mp_TRANLINK_VALUE    ;71.11
                                ; LOE eax
.B1.252:                        ; Preds .B1.15
        add       esp, 24                                       ;71.11
        mov       DWORD PTR [640+esp], eax                      ;71.11
                                ; LOE eax al ah
.B1.16:                         ; Preds .B1.252
        imul      ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32], -152 ;75.7
        mov       ebx, eax                                      ;71.6
        mov       DWORD PTR [680+esp], ebx                      ;71.6
        imul      ebx, ebx, 152                                 ;75.7
        add       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;75.7
        mov       eax, DWORD PTR [668+esp]                      ;86.8
        mov       DWORD PTR [644+esp], ecx                      ;75.7
        movsx     ecx, BYTE PTR [32+ebx+ecx]                    ;75.17
        test      ecx, ecx                                      ;75.7
        mov       DWORD PTR [648+esp], eax                      ;86.8
        jle       .B1.21        ; Prob 2%                       ;75.7
                                ; LOE ecx ebx
.B1.17:                         ; Preds .B1.16
        mov       edi, DWORD PTR [644+esp]                      ;76.13
        mov       edx, 1                                        ;
        mov       eax, DWORD PTR [64+ebx+edi]                   ;76.13
        mov       esi, DWORD PTR [68+ebx+edi]                   ;76.13
        imul      esi, eax                                      ;
        mov       ebx, DWORD PTR [36+ebx+edi]                   ;76.13
        mov       DWORD PTR [620+esp], esi                      ;
        sub       ebx, DWORD PTR [620+esp]                      ;
        mov       esi, DWORD PTR [648+esp]                      ;
        mov       edi, eax                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B1.18:                         ; Preds .B1.19 .B1.17
        cmp       esi, DWORD PTR [edi+ebx]                      ;76.56
        je        .B1.246       ; Prob 20%                      ;76.56
                                ; LOE eax edx ecx ebx esi edi
.B1.19:                         ; Preds .B1.18
        inc       edx                                           ;81.7
        add       edi, eax                                      ;81.7
        cmp       edx, ecx                                      ;81.7
        jle       .B1.18        ; Prob 82%                      ;81.7
                                ; LOE eax edx ecx ebx esi edi
.B1.21:                         ; Preds .B1.19 .B1.16
        mov       DWORD PTR [304+esp], 0                        ;85.8
        lea       ebx, DWORD PTR [304+esp]                      ;85.8
        mov       DWORD PTR [336+esp], 18                       ;85.8
        lea       eax, DWORD PTR [336+esp]                      ;85.8
        mov       DWORD PTR [340+esp], OFFSET FLAT: __STRLITPACK_42 ;85.8
        push      32                                            ;85.8
        push      eax                                           ;85.8
        push      OFFSET FLAT: __STRLITPACK_44.0.1              ;85.8
        push      -2088435968                                   ;85.8
        push      911                                           ;85.8
        push      ebx                                           ;85.8
        call      _for_write_seq_lis                            ;85.8
                                ; LOE ebx
.B1.22:                         ; Preds .B1.21
        mov       DWORD PTR [328+esp], 0                        ;86.8
        lea       eax, DWORD PTR [368+esp]                      ;86.8
        mov       DWORD PTR [368+esp], 75                       ;86.8
        mov       DWORD PTR [372+esp], OFFSET FLAT: __STRLITPACK_40 ;86.8
        push      32                                            ;86.8
        push      eax                                           ;86.8
        push      OFFSET FLAT: __STRLITPACK_45.0.1              ;86.8
        push      -2088435968                                   ;86.8
        push      911                                           ;86.8
        push      ebx                                           ;86.8
        call      _for_write_seq_lis                            ;86.8
                                ; LOE ebx
.B1.23:                         ; Preds .B1.22
        mov       esi, DWORD PTR [724+esp]                      ;86.8
        lea       eax, DWORD PTR [456+esp]                      ;86.8
        mov       DWORD PTR [456+esp], esi                      ;86.8
        push      eax                                           ;86.8
        push      OFFSET FLAT: __STRLITPACK_46.0.1              ;86.8
        push      ebx                                           ;86.8
        call      _for_write_seq_lis_xmit                       ;86.8
                                ; LOE ebx esi
.B1.24:                         ; Preds .B1.23
        mov       eax, DWORD PTR [708+esp]                      ;86.8
        lea       edx, DWORD PTR [476+esp]                      ;86.8
        mov       DWORD PTR [476+esp], eax                      ;86.8
        push      edx                                           ;86.8
        push      OFFSET FLAT: __STRLITPACK_47.0.1              ;86.8
        push      ebx                                           ;86.8
        call      _for_write_seq_lis_xmit                       ;86.8
                                ; LOE ebx esi
.B1.25:                         ; Preds .B1.24
        imul      eax, DWORD PTR [720+esp], 152                 ;86.8
        lea       edi, DWORD PTR [496+esp]                      ;86.8
        mov       edx, DWORD PTR [716+esp]                      ;86.8
        mov       ecx, DWORD PTR [24+eax+edx]                   ;86.8
        mov       DWORD PTR [496+esp], ecx                      ;86.8
        push      edi                                           ;86.8
        push      OFFSET FLAT: __STRLITPACK_48.0.1              ;86.8
        push      ebx                                           ;86.8
        call      _for_write_seq_lis_xmit                       ;86.8
                                ; LOE ebx esi
.B1.26:                         ; Preds .B1.25
        mov       DWORD PTR [516+esp], 0                        ;86.8
        lea       eax, DWORD PTR [516+esp]                      ;86.8
        push      eax                                           ;86.8
        push      OFFSET FLAT: __STRLITPACK_49.0.1              ;86.8
        push      ebx                                           ;86.8
        call      _for_write_seq_lis_xmit                       ;86.8
                                ; LOE ebx esi
.B1.27:                         ; Preds .B1.26
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;86.8
        neg       eax                                           ;86.8
        add       eax, esi                                      ;86.8
        shl       eax, 5                                        ;86.8
        mov       edi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;86.8
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DESTINATION+32] ;86.8
        neg       ecx                                           ;86.8
        mov       DWORD PTR [468+esp], edi                      ;86.8
        movsx     edi, WORD PTR [20+edi+eax]                    ;86.8
        add       ecx, edi                                      ;86.8
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DESTINATION] ;86.8
        mov       DWORD PTR [476+esp], eax                      ;86.8
        mov       eax, DWORD PTR [edx+ecx*4]                    ;86.8
        lea       edx, DWORD PTR [536+esp]                      ;86.8
        mov       DWORD PTR [536+esp], eax                      ;86.8
        push      edx                                           ;86.8
        push      OFFSET FLAT: __STRLITPACK_50.0.1              ;86.8
        push      ebx                                           ;86.8
        call      _for_write_seq_lis_xmit                       ;86.8
                                ; LOE ebx esi edi
.B1.28:                         ; Preds .B1.27
        mov       eax, DWORD PTR [480+esp]                      ;86.8
        mov       edx, DWORD PTR [488+esp]                      ;86.8
        mov       ecx, DWORD PTR [28+eax+edx]                   ;86.8
        lea       eax, DWORD PTR [556+esp]                      ;86.8
        mov       DWORD PTR [556+esp], ecx                      ;86.8
        push      eax                                           ;86.8
        push      OFFSET FLAT: __STRLITPACK_51.0.1              ;86.8
        push      ebx                                           ;86.8
        call      _for_write_seq_lis_xmit                       ;86.8
                                ; LOE ebx esi edi
.B1.253:                        ; Preds .B1.28
        add       esp, 120                                      ;86.8
                                ; LOE ebx esi edi
.B1.29:                         ; Preds .B1.253
        mov       WORD PTR [456+esp], di                        ;86.8
        lea       eax, DWORD PTR [456+esp]                      ;86.8
        push      eax                                           ;86.8
        push      OFFSET FLAT: __STRLITPACK_52.0.1              ;86.8
        push      ebx                                           ;86.8
        call      _for_write_seq_lis_xmit                       ;86.8
                                ; LOE ebx esi
.B1.30:                         ; Preds .B1.29
        mov       edi, DWORD PTR [384+esp]                      ;86.8
        lea       ecx, DWORD PTR [476+esp]                      ;86.8
        mov       eax, DWORD PTR [392+esp]                      ;86.8
        movzx     edx, BYTE PTR [5+edi+eax]                     ;86.8
        mov       BYTE PTR [476+esp], dl                        ;86.8
        push      ecx                                           ;86.8
        push      OFFSET FLAT: __STRLITPACK_53.0.1              ;86.8
        push      ebx                                           ;86.8
        call      _for_write_seq_lis_xmit                       ;86.8
                                ; LOE ebx esi
.B1.31:                         ; Preds .B1.30
        mov       edx, esi                                      ;86.8
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;86.8
        sub       edx, eax                                      ;86.8
        shl       edx, 8                                        ;86.8
        mov       edi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;86.8
        mov       DWORD PTR [676+esp], eax                      ;86.8
        lea       eax, DWORD PTR [496+esp]                      ;86.8
        mov       DWORD PTR [424+esp], edx                      ;86.8
        movzx     ecx, BYTE PTR [40+edi+edx]                    ;86.8
        mov       BYTE PTR [496+esp], cl                        ;86.8
        push      eax                                           ;86.8
        push      OFFSET FLAT: __STRLITPACK_54.0.1              ;86.8
        push      ebx                                           ;86.8
        call      _for_write_seq_lis_xmit                       ;86.8
                                ; LOE ebx esi edi
.B1.32:                         ; Preds .B1.31
        mov       eax, DWORD PTR [436+esp]                      ;86.8
        lea       ecx, DWORD PTR [516+esp]                      ;86.8
        movzx     edx, BYTE PTR [160+edi+eax]                   ;86.8
        mov       BYTE PTR [516+esp], dl                        ;86.8
        push      ecx                                           ;86.8
        push      OFFSET FLAT: __STRLITPACK_55.0.1              ;86.8
        push      ebx                                           ;86.8
        call      _for_write_seq_lis_xmit                       ;86.8
                                ; LOE ebx esi edi
.B1.33:                         ; Preds .B1.32
        mov       DWORD PTR [352+esp], 0                        ;87.8
        lea       eax, DWORD PTR [400+esp]                      ;87.8
        mov       DWORD PTR [400+esp], 17                       ;87.8
        mov       DWORD PTR [404+esp], OFFSET FLAT: __STRLITPACK_38 ;87.8
        push      32                                            ;87.8
        push      eax                                           ;87.8
        push      OFFSET FLAT: __STRLITPACK_56.0.1              ;87.8
        push      -2088435968                                   ;87.8
        push      911                                           ;87.8
        push      ebx                                           ;87.8
        call      _for_write_seq_lis                            ;87.8
                                ; LOE ebx esi edi
.B1.34:                         ; Preds .B1.33
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE+32] ;87.8
        neg       edx                                           ;87.8
        add       edx, esi                                      ;87.8
        shl       edx, 6                                        ;87.8
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE] ;87.8
        mov       DWORD PTR [460+esp], eax                      ;87.8
        mov       DWORD PTR [476+esp], edx                      ;87.8
        movzx     ecx, WORD PTR [12+eax+edx]                    ;87.8
        lea       eax, DWORD PTR [560+esp]                      ;87.8
        mov       WORD PTR [560+esp], cx                        ;87.8
        push      eax                                           ;87.8
        push      OFFSET FLAT: __STRLITPACK_57.0.1              ;87.8
        push      ebx                                           ;87.8
        call      _for_write_seq_lis_xmit                       ;87.8
                                ; LOE ebx esi edi
.B1.35:                         ; Preds .B1.34
        mov       eax, DWORD PTR [472+esp]                      ;87.8
        mov       edx, DWORD PTR [488+esp]                      ;87.8
        movzx     ecx, BYTE PTR [56+eax+edx]                    ;87.8
        lea       eax, DWORD PTR [580+esp]                      ;87.8
        mov       BYTE PTR [580+esp], cl                        ;87.8
        push      eax                                           ;87.8
        push      OFFSET FLAT: __STRLITPACK_58.0.1              ;87.8
        push      ebx                                           ;87.8
        call      _for_write_seq_lis_xmit                       ;87.8
                                ; LOE ebx esi edi
.B1.36:                         ; Preds .B1.35
        mov       eax, DWORD PTR [468+esp]                      ;87.8
        mov       edx, DWORD PTR [476+esp]                      ;87.8
        movzx     ecx, BYTE PTR [4+eax+edx]                     ;87.8
        lea       eax, DWORD PTR [600+esp]                      ;87.8
        mov       BYTE PTR [600+esp], cl                        ;87.8
        push      eax                                           ;87.8
        push      OFFSET FLAT: __STRLITPACK_59.0.1              ;87.8
        push      ebx                                           ;87.8
        call      _for_write_seq_lis_xmit                       ;87.8
                                ; LOE esi edi
.B1.254:                        ; Preds .B1.36
        add       esp, 108                                      ;87.8
                                ; LOE esi edi
.B1.37:                         ; Preds .B1.254
        push      32                                            ;88.8
        xor       eax, eax                                      ;88.8
        push      eax                                           ;88.8
        push      eax                                           ;88.8
        push      -2088435968                                   ;88.8
        push      eax                                           ;88.8
        push      OFFSET FLAT: __STRLITPACK_60                  ;88.8
        call      _for_stop_core                                ;88.8
                                ; LOE esi edi
.B1.255:                        ; Preds .B1.37
        add       esp, 24                                       ;88.8
                                ; LOE esi edi
.B1.38:                         ; Preds .B1.246 .B1.255
        sub       esi, DWORD PTR [652+esp]                      ;91.59
        shl       esi, 8                                        ;91.59
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+32] ;91.7
        neg       edx                                           ;91.59
        add       edx, DWORD PTR [640+esp]                      ;91.59
        movss     xmm0, DWORD PTR [156+edi+esi]                 ;91.62
        divss     xmm0, DWORD PTR [_2il0floatpacket.8]          ;91.83
        lea       eax, DWORD PTR [edx*4]                        ;91.59
        shl       edx, 5                                        ;91.59
        sub       edx, eax                                      ;91.59
        mov       eax, DWORD PTR [632+esp]                      ;91.59
        sub       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+44] ;91.59
        imul      eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+40] ;91.59
        add       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE] ;91.59
        movss     xmm1, DWORD PTR [20+eax+edx]                  ;91.10
        comiss    xmm1, xmm0                                    ;91.59
        jb        .B1.40        ; Prob 50%                      ;91.59
                                ; LOE eax edx xmm0 xmm1
.B1.39:                         ; Preds .B1.38
        subss     xmm1, xmm0                                    ;92.10
        movss     DWORD PTR [20+eax+edx], xmm1                  ;92.10
        jmp       .B1.45        ; Prob 100%                     ;92.10
                                ; LOE
.B1.40:                         ; Preds .B1.38
        comiss    xmm0, xmm1                                    ;93.63
        jbe       .B1.101       ; Prob 50%                      ;93.63
                                ; LOE xmm1
.B1.41:                         ; Preds .B1.40
        comiss    xmm1, DWORD PTR [_2il0floatpacket.9]          ;93.149
        jb        .B1.101       ; Prob 78%                      ;93.149
                                ; LOE
.B1.42:                         ; Preds .B1.41
        push      OFFSET FLAT: __NLITPACK_3.0.1                 ;94.15
        call      _RANXY                                        ;94.15
                                ; LOE f1
.B1.256:                        ; Preds .B1.42
        fstp      DWORD PTR [396+esp]                           ;94.15
        movss     xmm2, DWORD PTR [396+esp]                     ;94.15
        add       esp, 4                                        ;94.15
                                ; LOE xmm2
.B1.43:                         ; Preds .B1.256
        mov       ebx, DWORD PTR [676+esp]                      ;95.68
        sub       ebx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;95.16
        shl       ebx, 8                                        ;95.16
        mov       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;95.18
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+32] ;95.10
        neg       esi                                           ;95.16
        movss     xmm0, DWORD PTR [156+ecx+ebx]                 ;95.68
        divss     xmm0, DWORD PTR [_2il0floatpacket.8]          ;95.89
        mov       eax, DWORD PTR [680+esp]                      ;95.18
        add       esi, eax                                      ;95.16
        mov       edi, DWORD PTR [632+esp]                      ;95.16
        sub       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+44] ;95.16
        imul      edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+40] ;95.16
        mov       DWORD PTR [640+esp], eax                      ;95.18
        lea       edx, DWORD PTR [esi*4]                        ;95.16
        shl       esi, 5                                        ;95.16
        sub       esi, edx                                      ;95.16
        add       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE] ;95.16
        movss     xmm1, DWORD PTR [20+esi+edi]                  ;95.18
        divss     xmm1, xmm0                                    ;95.66
        comiss    xmm1, xmm2                                    ;95.16
        jbe       .B1.100       ; Prob 50%                      ;95.16
                                ; LOE esi edi
.B1.44:                         ; Preds .B1.43
        mov       eax, DWORD PTR [668+esp]                      ;106.53
        mov       DWORD PTR [20+esi+edi], 0                     ;96.9
        mov       DWORD PTR [648+esp], eax                      ;106.53
                                ; LOE
.B1.45:                         ; Preds .B1.39 .B1.44
        imul      ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32], -900 ;106.7
        imul      edx, DWORD PTR [640+esp], 900                 ;107.7
        mov       esi, DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY+32] ;108.7
        mov       ebx, DWORD PTR [648+esp]                      ;106.7
        neg       esi                                           ;108.7
        imul      eax, ebx, 900                                 ;106.7
        add       esi, ebx                                      ;108.7
        add       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;106.7
        mov       edi, DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY] ;108.7
        inc       DWORD PTR [624+esp]                           ;105.7
        dec       DWORD PTR [704+edx+ecx]                       ;107.7
        dec       DWORD PTR [700+ecx+eax]                       ;106.7
        lea       eax, DWORD PTR [esi+esi*4]                    ;108.7
        mov       edx, DWORD PTR [656+esp]                      ;108.7
        sub       edx, DWORD PTR [32+edi+eax*8]                 ;108.7
        imul      edx, DWORD PTR [28+edi+eax*8]                 ;108.7
        mov       edi, DWORD PTR [edi+eax*8]                    ;108.7
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_NSIGOPT], 0 ;112.22
        mov       DWORD PTR [edi+edx], -1                       ;108.7
        jle       .B1.47        ; Prob 16%                      ;112.22
                                ; LOE
.B1.46:                         ; Preds .B1.45
        mov       ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_MAXMOVE] ;113.16
        sub       ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGOUT+44] ;113.35
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGOUT+32] ;113.13
        neg       edx                                           ;113.35
        imul      ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGOUT+40] ;113.35
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGOUT] ;113.13
        add       edx, DWORD PTR [640+esp]                      ;113.35
        lea       ecx, DWORD PTR [eax+edx*4]                    ;113.35
        cmp       DWORD PTR [ecx+ebx], 1                        ;113.35
        je        .B1.211       ; Prob 5%                       ;113.35
                                ; LOE
.B1.47:                         ; Preds .B1.45 .B1.46 .B1.212
        lea       eax, DWORD PTR [668+esp]                      ;118.13
        push      eax                                           ;118.13
        lea       edx, DWORD PTR [684+esp]                      ;118.13
        push      edx                                           ;118.13
        call      _DYNUST_NETWORK_MODULE_mp_GETBSTMOVE          ;118.13
                                ; LOE eax
.B1.257:                        ; Preds .B1.47
        add       esp, 8                                        ;118.13
        mov       esi, eax                                      ;118.13
                                ; LOE esi
.B1.48:                         ; Preds .B1.257
        test      esi, esi                                      ;119.16
        jle       .B1.199       ; Prob 16%                      ;119.16
                                ; LOE esi
.B1.49:                         ; Preds .B1.48
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;136.6
        mov       edx, DWORD PTR [680+esp]                      ;136.6
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;136.6
        mov       DWORD PTR [536+esp], eax                      ;136.6
        mov       DWORD PTR [520+esp], edx                      ;136.6
                                ; LOE esi edi
.B1.50:                         ; Preds .B1.278 .B1.49
        mov       ecx, DWORD PTR [676+esp]                      ;127.31
        mov       DWORD PTR [476+esp], ecx                      ;127.31
        shl       ecx, 8                                        ;127.9
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;127.9
        mov       ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIMPERAGG] ;127.67
        mov       DWORD PTR [552+esp], ebx                      ;127.67
        mov       ebx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;127.9
        mov       eax, ebx                                      ;127.9
        mov       DWORD PTR [508+esp], edx                      ;127.9
        add       edx, ecx                                      ;127.9
        shl       eax, 8                                        ;127.9
        sub       edx, eax                                      ;127.9
        mov       DWORD PTR [460+esp], eax                      ;127.9
        movss     xmm0, DWORD PTR [_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT] ;127.31
        mov       DWORD PTR [468+esp], ecx                      ;127.9
        movss     xmm1, DWORD PTR [16+edx]                      ;127.31
        mov       eax, DWORD PTR [16+edx]                       ;127.31
        mov       edx, DWORD PTR [12+ebp]                       ;129.6
        mov       DWORD PTR [576+esp], eax                      ;127.31
        movss     DWORD PTR [584+esp], xmm1                     ;127.31
        movss     DWORD PTR [560+esp], xmm0                     ;127.31
        mov       eax, DWORD PTR [edx]                          ;129.6
        comiss    xmm1, DWORD PTR [edx]                         ;129.11
        jbe       .B1.56        ; Prob 50%                      ;129.11
                                ; LOE eax ebx esi edi
.B1.51:                         ; Preds .B1.50
        mov       DWORD PTR [304+esp], 0                        ;130.9
        lea       ecx, DWORD PTR [304+esp]                      ;130.9
        mov       DWORD PTR [264+esp], 21                       ;130.9
        lea       edx, DWORD PTR [264+esp]                      ;130.9
        mov       DWORD PTR [268+esp], OFFSET FLAT: __STRLITPACK_24 ;130.9
        push      32                                            ;130.9
        push      edx                                           ;130.9
        push      OFFSET FLAT: __STRLITPACK_73.0.1              ;130.9
        push      -2088435968                                   ;130.9
        push      911                                           ;130.9
        push      ecx                                           ;130.9
        mov       DWORD PTR [276+esp], eax                      ;130.9
        call      _for_write_seq_lis                            ;130.9
                                ; LOE ebx esi edi
.B1.52:                         ; Preds .B1.51
        mov       eax, DWORD PTR [276+esp]                      ;
        mov       DWORD PTR [392+esp], eax                      ;130.9
        lea       eax, DWORD PTR [392+esp]                      ;130.9
        push      eax                                           ;130.9
        push      OFFSET FLAT: __STRLITPACK_74.0.1              ;130.9
        lea       edx, DWORD PTR [336+esp]                      ;130.9
        push      edx                                           ;130.9
        call      _for_write_seq_lis_xmit                       ;130.9
                                ; LOE ebx esi edi
.B1.53:                         ; Preds .B1.52
        mov       eax, DWORD PTR [612+esp]                      ;130.9
        lea       edx, DWORD PTR [412+esp]                      ;130.9
        mov       DWORD PTR [412+esp], eax                      ;130.9
        push      edx                                           ;130.9
        push      OFFSET FLAT: __STRLITPACK_75.0.1              ;130.9
        lea       ecx, DWORD PTR [348+esp]                      ;130.9
        push      ecx                                           ;130.9
        call      _for_write_seq_lis_xmit                       ;130.9
                                ; LOE ebx esi edi
.B1.54:                         ; Preds .B1.53
        mov       DWORD PTR [352+esp], 0                        ;131.9
        lea       eax, DWORD PTR [320+esp]                      ;131.9
        mov       DWORD PTR [320+esp], 47                       ;131.9
        mov       DWORD PTR [324+esp], OFFSET FLAT: __STRLITPACK_22 ;131.9
        push      32                                            ;131.9
        push      eax                                           ;131.9
        push      OFFSET FLAT: __STRLITPACK_76.0.1              ;131.9
        push      -2088435968                                   ;131.9
        push      911                                           ;131.9
        lea       edx, DWORD PTR [372+esp]                      ;131.9
        push      edx                                           ;131.9
        call      _for_write_seq_lis                            ;131.9
                                ; LOE ebx esi edi
.B1.55:                         ; Preds .B1.54
        push      32                                            ;132.9
        xor       eax, eax                                      ;132.9
        push      eax                                           ;132.9
        push      eax                                           ;132.9
        push      -2088435968                                   ;132.9
        push      eax                                           ;132.9
        push      OFFSET FLAT: __STRLITPACK_77                  ;132.9
        call      _for_stop_core                                ;132.9
                                ; LOE ebx esi edi
.B1.258:                        ; Preds .B1.55
        add       esp, 96                                       ;132.9
                                ; LOE ebx esi edi
.B1.56:                         ; Preds .B1.258 .B1.50
        movss     xmm1, DWORD PTR [584+esp]                     ;127.51
        xor       ecx, ecx                                      ;136.111
        divss     xmm1, DWORD PTR [560+esp]                     ;127.51
        cvtsi2ss  xmm0, DWORD PTR [552+esp]                     ;127.67
        divss     xmm1, xmm0                                    ;127.25
        cvttss2si edx, xmm1                                     ;127.25
        test      edx, edx                                      ;136.111
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;136.9
        cmovl     edx, ecx                                      ;136.111
        imul      ecx, DWORD PTR [536+esp], -152                ;136.111
        add       edi, ecx                                      ;136.111
        lea       edx, DWORD PTR [5+edx+edx*4]                  ;127.9
        imul      ecx, DWORD PTR [520+esp], 152                 ;136.111
        mov       DWORD PTR [592+esp], edi                      ;136.111
        imul      ecx, DWORD PTR [12+ecx+edi], 900              ;136.9
        imul      edi, eax, -900                                ;136.111
        add       edi, ecx                                      ;136.111
        mov       DWORD PTR [444+esp], eax                      ;136.9
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;136.9
        mov       DWORD PTR [596+esp], ecx                      ;136.9
        mov       DWORD PTR [428+esp], edx                      ;127.9
        mov       ecx, DWORD PTR [120+edi+eax]                  ;136.9
        imul      edi, DWORD PTR [152+edi+eax], -40             ;136.111
        mov       DWORD PTR [548+esp], edi                      ;136.111
        lea       edx, DWORD PTR [ecx+edx*8]                    ;136.111
        movsx     ecx, WORD PTR [36+edi+edx]                    ;136.9
        test      ecx, ecx                                      ;136.111
        mov       DWORD PTR [580+esp], edx                      ;136.111
        mov       edx, DWORD PTR [596+esp]                      ;136.111
        mov       edi, DWORD PTR [592+esp]                      ;136.111
        jle       .B1.136       ; Prob 1%                       ;136.111
                                ; LOE eax edx ebx esi edi
.B1.57:                         ; Preds .B1.190 .B1.56
        imul      ecx, DWORD PTR [444+esp], -900                ;146.7
        neg       ebx                                           ;147.4
        pxor      xmm1, xmm1                                    ;147.4
        add       eax, ecx                                      ;146.7
        mov       DWORD PTR [436+esp], eax                      ;146.7
        mov       ecx, DWORD PTR [428+esp]                      ;146.7
        add       ebx, DWORD PTR [476+esp]                      ;147.4
        mov       edi, DWORD PTR [120+edx+eax]                  ;146.114
        imul      eax, DWORD PTR [152+edx+eax], -40             ;146.7
        shl       ebx, 8                                        ;147.4
        lea       edi, DWORD PTR [edi+ecx*8]                    ;146.7
        mov       ecx, DWORD PTR [32+eax+edi]                   ;146.114
        neg       ecx                                           ;146.7
        add       ecx, esi                                      ;146.7
        imul      ecx, DWORD PTR [28+eax+edi]                   ;146.7
        mov       eax, DWORD PTR [eax+edi]                      ;146.114
        inc       WORD PTR [eax+ecx]                            ;146.220
        mov       eax, DWORD PTR [436+esp]                      ;147.111
        mov       ecx, DWORD PTR [428+esp]                      ;147.4
        mov       edi, DWORD PTR [48+edx+eax]                   ;147.111
        imul      edx, DWORD PTR [80+edx+eax], -40              ;147.4
        lea       edi, DWORD PTR [edi+ecx*8]                    ;147.4
        sub       esi, DWORD PTR [32+edx+edi]                   ;147.4
        imul      esi, DWORD PTR [28+edx+edi]                   ;147.4
        mov       ecx, DWORD PTR [edx+edi]                      ;147.111
        mov       edi, DWORD PTR [12+ebp]                       ;147.111
        mov       edx, DWORD PTR [508+esp]                      ;147.228
        cvtsi2ss  xmm2, DWORD PTR [ecx+esi]                     ;147.111
        movss     xmm0, DWORD PTR [edi]                         ;147.111
        movss     xmm3, DWORD PTR [84+edx+ebx]                  ;149.28
        mov       DWORD PTR [20+edx+ebx], 0                     ;152.7
        subss     xmm0, DWORD PTR [16+edx+ebx]                  ;147.228
        subss     xmm3, DWORD PTR [16+edx+ebx]                  ;149.4
        maxss     xmm1, xmm0                                    ;147.4
        mulss     xmm1, DWORD PTR [_2il0floatpacket.8]          ;147.251
        mov       DWORD PTR [16+edx+ebx], 0                     ;151.7
        addss     xmm2, xmm1                                    ;147.217
        cvttss2si eax, xmm2                                     ;147.4
        mov       DWORD PTR [ecx+esi], eax                      ;147.4
        movss     xmm1, DWORD PTR [edi]                         ;149.28
        mov       esi, DWORD PTR [edi]                          ;149.28
        addss     xmm3, xmm1                                    ;149.50
        mov       ecx, DWORD PTR [476+esp]                      ;155.38
        shl       ecx, 6                                        ;155.38
        movss     DWORD PTR [84+edx+ebx], xmm3                  ;149.4
        mov       DWORD PTR [28+edx+ebx], esi                   ;153.4
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE] ;155.7
        add       ecx, eax                                      ;155.38
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE+32] ;155.7
        shl       edx, 6                                        ;155.38
        sub       ecx, edx                                      ;155.38
        movsx     esi, WORD PTR [12+ecx]                        ;155.10
        cmp       esi, 1                                        ;155.38
        jne       .B1.59        ; Prob 84%                      ;155.38
                                ; LOE eax edx ebx xmm1
.B1.58:                         ; Preds .B1.57
        mov       edi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;158.9
        mov       ecx, DWORD PTR [16+ebp]                       ;155.44
        neg       edi                                           ;158.9
        add       edi, DWORD PTR [476+esp]                      ;158.9
        shl       edi, 5                                        ;158.9
        movss     xmm0, DWORD PTR [ecx]                         ;155.44
        mov       ecx, DWORD PTR [508+esp]                      ;155.44
        mov       esi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;158.9
        subss     xmm0, DWORD PTR [100+ecx+ebx]                 ;155.44
        subss     xmm1, DWORD PTR [12+esi+edi]                  ;158.9
        movss     DWORD PTR [92+ecx+ebx], xmm0                  ;155.44
        movss     DWORD PTR [88+ecx+ebx], xmm1                  ;158.9
                                ; LOE eax edx
.B1.59:                         ; Preds .B1.57 .B1.58
        mov       ecx, DWORD PTR [476+esp]                      ;161.12
        add       edx, -12                                      ;161.12
        shl       ecx, 6                                        ;161.12
        lea       esi, DWORD PTR [676+esp]                      ;161.12
        add       eax, ecx                                      ;161.12
        mov       ebx, DWORD PTR [468+esp]                      ;161.12
        sub       eax, edx                                      ;161.12
        mov       edx, DWORD PTR [460+esp]                      ;161.12
        add       ebx, DWORD PTR [508+esp]                      ;161.12
        add       edx, -88                                      ;161.12
        sub       ebx, edx                                      ;161.12
        push      ebx                                           ;161.12
        push      OFFSET FLAT: __NLITPACK_4.0.1                 ;161.12
        push      eax                                           ;161.12
        push      esi                                           ;161.12
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_INSERT  ;161.12
                                ; LOE
.B1.259:                        ; Preds .B1.59
        add       esp, 16                                       ;161.12
                                ; LOE
.B1.60:                         ; Preds .B1.259
        mov       ecx, DWORD PTR [676+esp]                      ;178.30
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_RUNMODE], 1 ;172.18
        je        .B1.63        ; Prob 16%                      ;172.18
                                ; LOE ecx
.B1.61:                         ; Preds .B1.60
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;172.35
        neg       edx                                           ;172.22
        add       edx, ecx                                      ;172.22
        shl       edx, 8                                        ;172.22
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;172.35
        cmp       BYTE PTR [160+eax+edx], 1                     ;172.71
        je        .B1.63        ; Prob 16%                      ;172.71
                                ; LOE eax edx ecx
.B1.62:                         ; Preds .B1.61
        movsx     eax, BYTE PTR [40+eax+edx]                    ;175.14
        mov       DWORD PTR [540+esp], eax                      ;175.14
        jmp       .B1.64        ; Prob 100%                     ;175.14
                                ; LOE ecx
.B1.63:                         ; Preds .B1.60 .B1.61
        mov       DWORD PTR [540+esp], 1                        ;173.5
                                ; LOE ecx
.B1.64:                         ; Preds .B1.63 .B1.62
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE+32] ;178.15
        lea       ebx, DWORD PTR [412+esp]                      ;178.15
        neg       eax                                           ;178.15
        add       eax, ecx                                      ;178.15
        shl       eax, 6                                        ;178.15
        mov       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE] ;178.15
        movsx     edx, WORD PTR [12+ecx+eax]                    ;178.30
        mov       DWORD PTR [412+esp], edx                      ;178.15
        push      OFFSET FLAT: __NLITPACK_1.0.1                 ;178.15
        push      ebx                                           ;178.15
        lea       esi, DWORD PTR [684+esp]                      ;178.15
        push      esi                                           ;178.15
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_VALUE   ;178.15
                                ; LOE f1
.B1.260:                        ; Preds .B1.64
        fstp      DWORD PTR [404+esp]                           ;178.15
        movss     xmm3, DWORD PTR [404+esp]                     ;178.15
        add       esp, 12                                       ;178.15
                                ; LOE xmm3
.B1.65:                         ; Preds .B1.260
        movss     xmm0, DWORD PTR [_2il0floatpacket.13]         ;178.10
        andps     xmm0, xmm3                                    ;178.10
        pxor      xmm3, xmm0                                    ;178.10
        movss     xmm1, DWORD PTR [_2il0floatpacket.14]         ;178.10
        movaps    xmm2, xmm3                                    ;178.10
        movaps    xmm7, xmm3                                    ;178.10
        cmpltss   xmm2, xmm1                                    ;178.10
        andps     xmm1, xmm2                                    ;178.10
        mov       esi, DWORD PTR [676+esp]                      ;178.10
        mov       edx, esi                                      ;178.62
        shl       edx, 6                                        ;178.62
        addss     xmm7, xmm1                                    ;178.10
        mov       edi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE] ;178.10
        subss     xmm7, xmm1                                    ;178.10
        movaps    xmm6, xmm7                                    ;178.10
        lea       ebx, DWORD PTR [edi+edx]                      ;178.62
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE+32] ;178.10
        subss     xmm6, xmm3                                    ;178.10
        movss     xmm3, DWORD PTR [_2il0floatpacket.15]         ;178.10
        movaps    xmm4, xmm6                                    ;178.10
        movaps    xmm5, xmm3                                    ;178.10
        cmpnless  xmm4, xmm3                                    ;178.10
        addss     xmm5, xmm3                                    ;178.10
        cmpless   xmm6, DWORD PTR [_2il0floatpacket.16]         ;178.10
        andps     xmm4, xmm5                                    ;178.10
        andps     xmm6, xmm5                                    ;178.10
        shl       eax, 6                                        ;178.62
        subss     xmm7, xmm4                                    ;178.10
        sub       ebx, eax                                      ;178.62
        addss     xmm7, xmm6                                    ;178.10
        shl       esi, 8                                        ;
        orps      xmm7, xmm0                                    ;178.10
        mov       DWORD PTR [516+esp], edi                      ;178.10
        mov       DWORD PTR [604+esp], esi                      ;
        mov       edi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;179.8
        mov       esi, DWORD PTR [16+ebx]                       ;178.10
        mov       ebx, DWORD PTR [48+ebx]                       ;178.10
        add       ebx, ebx                                      ;178.62
        mov       DWORD PTR [524+esp], edi                      ;179.8
        neg       ebx                                           ;178.62
        mov       edi, DWORD PTR [540+esp]                      ;178.62
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_MASTERDEST+32] ;178.10
        neg       ecx                                           ;178.62
        mov       DWORD PTR [568+esp], eax                      ;178.62
        lea       esi, DWORD PTR [esi+edi*2]                    ;178.62
        movsx     ebx, WORD PTR [ebx+esi]                       ;178.10
        add       ecx, ebx                                      ;178.62
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;179.8
        shl       eax, 8                                        ;
        mov       DWORD PTR [512+esp], eax                      ;
        cvtss2si  eax, xmm7                                     ;178.10
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_MASTERDEST] ;178.10
        mov       DWORD PTR [600+esp], edx                      ;178.62
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DESTINATION+32] ;178.10
        neg       edx                                           ;178.62
        movsx     ecx, WORD PTR [edi+ecx*2]                     ;178.10
        add       edx, ecx                                      ;178.62
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DESTINATION] ;178.62
        cmp       eax, DWORD PTR [ecx+edx*4]                    ;178.62
        mov       eax, DWORD PTR [604+esp]                      ;178.62
        mov       edx, DWORD PTR [600+esp]                      ;178.62
        jne       .B1.67        ; Prob 50%                      ;178.62
                                ; LOE eax edx al dl ah dh
.B1.66:                         ; Preds .B1.65
        add       eax, DWORD PTR [524+esp]                      ;179.8
        sub       eax, DWORD PTR [512+esp]                      ;179.8
        mov       ebx, DWORD PTR [540+esp]                      ;179.8
        mov       esi, DWORD PTR [232+eax]                      ;179.42
        add       esi, esi                                      ;179.8
        mov       ecx, DWORD PTR [200+eax]                      ;179.42
        neg       esi                                           ;179.8
        lea       edi, DWORD PTR [ecx+ebx*2]                    ;179.8
        movsx     ecx, WORD PTR [esi+edi]                       ;179.42
        cvtsi2ss  xmm0, ecx                                     ;179.42
        divss     xmm0, DWORD PTR [_2il0floatpacket.10]         ;179.78
        addss     xmm0, DWORD PTR [84+eax]                      ;179.8
        movss     DWORD PTR [420+esp], xmm0                     ;179.8
        jmp       .B1.68        ; Prob 100%                     ;179.8
                                ; LOE edx dl dh
.B1.67:                         ; Preds .B1.65
        mov       ecx, DWORD PTR [524+esp]                      ;181.8
        sub       ecx, DWORD PTR [512+esp]                      ;181.8
        mov       eax, DWORD PTR [84+eax+ecx]                   ;181.8
        mov       DWORD PTR [420+esp], eax                      ;181.8
                                ; LOE edx dl dh
.B1.68:                         ; Preds .B1.66 .B1.67
        mov       eax, DWORD PTR [568+esp]                      ;184.12
        lea       ecx, DWORD PTR [420+esp]                      ;184.12
        add       edx, DWORD PTR [516+esp]                      ;184.12
        add       eax, -12                                      ;184.12
        push      ecx                                           ;184.12
        push      OFFSET FLAT: __NLITPACK_2.0.1                 ;184.12
        sub       edx, eax                                      ;184.12
        push      edx                                           ;184.12
        lea       edx, DWORD PTR [688+esp]                      ;184.12
        push      edx                                           ;184.12
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_INSERT  ;184.12
                                ; LOE
.B1.261:                        ; Preds .B1.68
        add       esp, 16                                       ;184.12
                                ; LOE
.B1.69:                         ; Preds .B1.261
        mov       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;186.11
        neg       ecx                                           ;186.14
        mov       eax, DWORD PTR [676+esp]                      ;186.14
        add       ecx, eax                                      ;186.14
        shl       ecx, 8                                        ;186.14
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;186.11
        mov       DWORD PTR [588+esp], eax                      ;186.14
        mov       DWORD PTR [572+esp], ecx                      ;186.14
        mov       DWORD PTR [564+esp], edx                      ;186.11
        test      BYTE PTR [112+edx+ecx], 1                     ;186.14
        je        .B1.71        ; Prob 60%                      ;186.14
                                ; LOE edx ecx dl cl dh ch
.B1.70:                         ; Preds .B1.69
        mov       eax, edx                                      ;187.14
        mov       edx, ecx                                      ;187.14
        mov       DWORD PTR [112+eax+edx], 0                    ;187.14
        mov       DWORD PTR [104+eax+edx], 0                    ;188.14
                                ; LOE
.B1.71:                         ; Preds .B1.69 .B1.70
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+44] ;195.13
        neg       eax                                           ;195.59
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+32] ;195.10
        mov       ebx, DWORD PTR [680+esp]                      ;195.13
        neg       edi                                           ;195.59
        add       eax, DWORD PTR [632+esp]                      ;195.59
        add       edi, ebx                                      ;195.59
        imul      ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32], 900 ;
        imul      edx, ebx, 900                                 ;
        imul      eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+40] ;195.59
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE] ;195.10
        sub       edx, ecx                                      ;
        mov       DWORD PTR [492+esp], ecx                      ;
        mov       DWORD PTR [484+esp], ebx                      ;195.13
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;195.59
        lea       ecx, DWORD PTR [5+esi+edi*8]                  ;195.59
        cmp       BYTE PTR [ecx+eax], 1                         ;195.59
        jne       .B1.73        ; Prob 84%                      ;195.59
                                ; LOE edx ebx
.B1.72:                         ; Preds .B1.71
        inc       DWORD PTR [408+edx+ebx]                       ;195.65
                                ; LOE edx ebx
.B1.73:                         ; Preds .B1.71 .B1.72
        inc       DWORD PTR [404+edx+ebx]                       ;196.10
        mov       eax, DWORD PTR [564+esp]                      ;198.13
        pxor      xmm0, xmm0                                    ;198.35
        mov       edx, DWORD PTR [572+esp]                      ;198.13
        movss     xmm1, DWORD PTR [100+eax+edx]                 ;198.13
        mov       esi, DWORD PTR [100+eax+edx]                  ;198.13
        movss     DWORD PTR [452+esp], xmm1                     ;198.13
        comiss    xmm0, xmm1                                    ;198.35
        jbe       .B1.77        ; Prob 50%                      ;198.35
                                ; LOE ebx esi
.B1.74:                         ; Preds .B1.73
        mov       DWORD PTR [304+esp], 0                        ;199.12
        lea       edx, DWORD PTR [304+esp]                      ;199.12
        mov       DWORD PTR [280+esp], 20                       ;199.12
        lea       eax, DWORD PTR [280+esp]                      ;199.12
        mov       DWORD PTR [284+esp], OFFSET FLAT: __STRLITPACK_20 ;199.12
        push      32                                            ;199.12
        push      eax                                           ;199.12
        push      OFFSET FLAT: __STRLITPACK_78.0.1              ;199.12
        push      -2088435968                                   ;199.12
        push      911                                           ;199.12
        push      edx                                           ;199.12
        call      _for_write_seq_lis                            ;199.12
                                ; LOE ebx esi
.B1.75:                         ; Preds .B1.74
        mov       DWORD PTR [328+esp], 0                        ;200.12
        lea       eax, DWORD PTR [312+esp]                      ;200.12
        mov       DWORD PTR [312+esp], 15                       ;200.12
        mov       DWORD PTR [316+esp], OFFSET FLAT: __STRLITPACK_18 ;200.12
        push      32                                            ;200.12
        push      eax                                           ;200.12
        push      OFFSET FLAT: __STRLITPACK_79.0.1              ;200.12
        push      -2088435968                                   ;200.12
        push      911                                           ;200.12
        lea       edx, DWORD PTR [348+esp]                      ;200.12
        push      edx                                           ;200.12
        call      _for_write_seq_lis                            ;200.12
                                ; LOE ebx esi
.B1.76:                         ; Preds .B1.75
        mov       DWORD PTR [432+esp], esi                      ;200.12
        lea       eax, DWORD PTR [432+esp]                      ;200.12
        push      eax                                           ;200.12
        push      OFFSET FLAT: __STRLITPACK_80.0.1              ;200.12
        lea       edx, DWORD PTR [360+esp]                      ;200.12
        push      edx                                           ;200.12
        call      _for_write_seq_lis_xmit                       ;200.12
                                ; LOE ebx
.B1.262:                        ; Preds .B1.76
        add       esp, 60                                       ;200.12
                                ; LOE ebx
.B1.77:                         ; Preds .B1.262 .B1.73
        imul      edx, DWORD PTR [668+esp], 152                 ;202.10
        imul      ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32], 152 ;202.10
        pxor      xmm2, xmm2                                    ;202.10
        movss     xmm4, DWORD PTR [452+esp]                     ;202.98
        mov       esi, DWORD PTR [564+esp]                      ;202.74
        mov       edi, DWORD PTR [572+esp]                      ;202.74
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;202.10
        add       edx, eax                                      ;202.10
        sub       edx, ecx                                      ;202.10
        movss     xmm1, DWORD PTR [12+esi+edi]                  ;202.74
        mulss     xmm1, xmm4                                    ;202.98
        movss     xmm3, DWORD PTR [20+edx]                      ;202.43
        movaps    xmm0, xmm3                                    ;202.72
        mov       DWORD PTR [528+esp], eax                      ;202.10
        subss     xmm0, xmm1                                    ;202.72
        mov       eax, DWORD PTR [588+esp]                      ;206.44
        maxss     xmm2, xmm0                                    ;202.10
        sub       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;206.44
        movss     xmm5, DWORD PTR [88+esi+edi]                  ;204.36
        shl       eax, 5                                        ;206.44
        addss     xmm5, xmm4                                    ;204.10
        mov       DWORD PTR [544+esp], ecx                      ;202.10
        mov       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;206.10
        movss     xmm0, DWORD PTR [108+esi+edi]                 ;203.36
        mov       DWORD PTR [588+esp], eax                      ;206.44
        addss     xmm0, xmm1                                    ;203.10
        movsx     eax, BYTE PTR [5+ecx+eax]                     ;206.13
        cmp       eax, 6                                        ;206.44
        movss     DWORD PTR [240+esi+edi], xmm2                 ;202.10
        movss     DWORD PTR [88+esi+edi], xmm5                  ;204.10
        je        .B1.79        ; Prob 16%                      ;206.44
                                ; LOE eax edx ecx ebx xmm0 xmm1 xmm2 xmm3
.B1.78:                         ; Preds .B1.77
        imul      esi, DWORD PTR [12+edx], 900                  ;207.12
        cvtsi2ss  xmm4, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIMPERAGG] ;208.52
        divss     xmm1, xmm3                                    ;209.122
        mov       edx, DWORD PTR [12+ebp]                       ;208.12
        mov       DWORD PTR [364+esp], ecx                      ;
        sub       ebx, DWORD PTR [492+esp]                      ;209.12
        movss     xmm5, DWORD PTR [edx]                         ;208.12
        divss     xmm5, DWORD PTR [_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT] ;208.36
        divss     xmm5, xmm4                                    ;208.29
        cvttss2si ecx, xmm5                                     ;208.29
        sub       eax, DWORD PTR [852+esi+ebx]                  ;209.12
        inc       ecx                                           ;208.62
        imul      eax, DWORD PTR [848+esi+ebx]                  ;209.12
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_AGGINT] ;208.12
        cmp       ecx, edi                                      ;209.12
        cmovl     edi, ecx                                      ;209.12
        sub       edi, DWORD PTR [840+esi+ebx]                  ;209.12
        add       eax, DWORD PTR [808+esi+ebx]                  ;209.12
        mov       ebx, DWORD PTR [588+esp]                      ;209.12
        mulss     xmm1, DWORD PTR [eax+edi*4]                   ;209.152
        mov       eax, DWORD PTR [364+esp]                      ;209.12
        addss     xmm1, DWORD PTR [8+eax+ebx]                   ;209.12
        movss     DWORD PTR [8+eax+ebx], xmm1                   ;209.12
                                ; LOE xmm0 xmm2 xmm3
.B1.79:                         ; Preds .B1.77 .B1.78
        movss     xmm1, DWORD PTR [_DYNUST_MAIN_MODULE_mp_GUITOTALTIME] ;212.10
        pxor      xmm4, xmm4                                    ;214.38
        addss     xmm1, DWORD PTR [452+esp]                     ;212.10
        comiss    xmm4, xmm2                                    ;214.38
        movss     DWORD PTR [_DYNUST_MAIN_MODULE_mp_GUITOTALTIME], xmm1 ;212.10
        jbe       .B1.82        ; Prob 50%                      ;214.38
                                ; LOE xmm0 xmm3
.B1.80:                         ; Preds .B1.79
        mov       eax, DWORD PTR [564+esp]                      ;217.12
        lea       ebx, DWORD PTR [304+esp]                      ;215.12
        mov       edx, DWORD PTR [572+esp]                      ;217.12
        lea       ecx, DWORD PTR [296+esp]                      ;215.12
        mov       DWORD PTR [304+esp], 0                        ;215.12
        mov       DWORD PTR [296+esp], 37                       ;215.12
        mov       DWORD PTR [300+esp], OFFSET FLAT: __STRLITPACK_17 ;215.12
        push      32                                            ;215.12
        push      ecx                                           ;215.12
        push      OFFSET FLAT: __STRLITPACK_81.0.1              ;215.12
        push      -2088435968                                   ;215.12
        push      -1                                            ;215.12
        push      ebx                                           ;215.12
        movss     DWORD PTR [108+eax+edx], xmm0                 ;217.12
        movss     DWORD PTR [284+esp], xmm0                     ;215.12
        movss     DWORD PTR [384+esp], xmm3                     ;215.12
        call      _for_write_seq_lis                            ;215.12
                                ; LOE
.B1.263:                        ; Preds .B1.80
        movss     xmm3, DWORD PTR [384+esp]                     ;
        movss     xmm0, DWORD PTR [284+esp]                     ;
        add       esp, 24                                       ;215.12
                                ; LOE xmm0 xmm3
.B1.81:                         ; Preds .B1.263
        mov       eax, DWORD PTR [564+esp]                      ;216.12
        addss     xmm0, xmm3                                    ;217.12
        mov       edx, DWORD PTR [572+esp]                      ;216.12
        mov       DWORD PTR [240+eax+edx], 0                    ;216.12
        movss     DWORD PTR [108+eax+edx], xmm0                 ;217.12
        jmp       .B1.83        ; Prob 100%                     ;217.12
                                ; LOE
.B1.82:                         ; Preds .B1.79
        imul      eax, DWORD PTR [484+esp], 152                 ;219.12
        add       eax, DWORD PTR [528+esp]                      ;219.12
        sub       eax, DWORD PTR [544+esp]                      ;219.12
        mov       edx, DWORD PTR [564+esp]                      ;219.12
        mov       ecx, DWORD PTR [572+esp]                      ;219.12
        addss     xmm0, DWORD PTR [20+eax]                      ;219.12
        movss     DWORD PTR [108+edx+ecx], xmm0                 ;219.12
                                ; LOE
.B1.83:                         ; Preds .B1.81 .B1.82
        lea       eax, DWORD PTR [676+esp]                      ;222.15
        push      eax                                           ;222.15
        lea       edx, DWORD PTR [684+esp]                      ;222.15
        push      edx                                           ;222.15
        call      _DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST_REMOVE ;222.15
                                ; LOE
.B1.264:                        ; Preds .B1.83
        add       esp, 8                                        ;222.15
                                ; LOE
.B1.84:                         ; Preds .B1.264
        mov       ecx, DWORD PTR [8+ebp]                        ;225.10
        imul      esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32], 900 ;226.10
        mov       ebx, DWORD PTR [ecx]                          ;225.10
        dec       ebx                                           ;225.26
        cvtsi2ss  xmm0, ebx                                     ;225.26
        mulss     xmm0, DWORD PTR [_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT] ;225.20
        cvttss2si edi, xmm0                                     ;225.20
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;226.10
        mov       ebx, ecx                                      ;226.10
        mov       eax, DWORD PTR [680+esp]                      ;226.55
        sub       ebx, esi                                      ;226.10
        mov       DWORD PTR [532+esp], esi                      ;226.10
        lea       edi, DWORD PTR [4+edi*4]                      ;225.10
        imul      esi, eax, 900                                 ;226.10
        mov       edx, DWORD PTR [560+esi+ebx]                  ;226.55
        shl       edx, 2                                        ;226.10
        mov       DWORD PTR [500+esp], eax                      ;226.55
        neg       edx                                           ;226.10
        mov       eax, DWORD PTR [528+esi+ebx]                  ;226.55
        add       eax, edi                                      ;226.10
        inc       DWORD PTR [edx+eax]                           ;226.10
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;227.10
        neg       eax                                           ;227.44
        mov       edx, DWORD PTR [676+esp]                      ;227.13
        add       eax, edx                                      ;227.44
        shl       eax, 5                                        ;227.44
        mov       DWORD PTR [556+esp], edx                      ;227.13
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;227.10
        movsx     eax, BYTE PTR [5+edx+eax]                     ;227.13
        cmp       eax, 2                                        ;227.44
        je        .B1.135       ; Prob 16%                      ;227.44
                                ; LOE eax ecx ebx esi edi
.B1.85:                         ; Preds .B1.84
        cmp       eax, 3                                        ;228.44
        jne       .B1.87        ; Prob 84%                      ;228.44
                                ; LOE eax ecx ebx esi edi
.B1.86:                         ; Preds .B1.85
        mov       edx, DWORD PTR [600+esi+ebx]                  ;228.96
        add       edi, edx                                      ;228.50
        mov       edx, DWORD PTR [632+esi+ebx]                  ;228.96
        shl       edx, 2                                        ;228.50
        neg       edx                                           ;228.50
        inc       DWORD PTR [edx+edi]                           ;228.50
                                ; LOE eax ecx ebx esi
.B1.87:                         ; Preds .B1.135 .B1.85 .B1.86
        mov       edx, eax                                      ;232.42
        and       edx, -5                                       ;232.42
        dec       DWORD PTR [640+esi+ebx]                       ;230.10
        dec       DWORD PTR [396+esi+ebx]                       ;231.7
        cmp       edx, 2                                        ;232.42
        je        .B1.89        ; Prob 16%                      ;232.42
                                ; LOE eax ecx ebx esi
.B1.88:                         ; Preds .B1.87
        cmp       eax, 7                                        ;232.116
        jne       .B1.90        ; Prob 84%                      ;232.116
                                ; LOE ecx ebx esi
.B1.89:                         ; Preds .B1.88 .B1.87
        dec       DWORD PTR [644+esi+ebx]                       ;233.7
                                ; LOE ecx
.B1.90:                         ; Preds .B1.88 .B1.89
        imul      eax, DWORD PTR [668+esp], 900                 ;236.13
        sub       eax, DWORD PTR [532+esp]                      ;236.47
        cmp       DWORD PTR [396+ecx+eax], 0                    ;236.47
        jl        .B1.131       ; Prob 16%                      ;236.47
                                ; LOE
.B1.91:                         ; Preds .B1.272 .B1.90
        mov       eax, DWORD PTR [556+esp]                      ;242.10
        sub       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;242.10
        shl       eax, 8                                        ;242.10
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;242.10
        lea       ecx, DWORD PTR [240+eax+edx]                  ;242.10
        push      ecx                                           ;242.10
        lea       ebx, DWORD PTR [680+esp]                      ;242.10
        push      ebx                                           ;242.10
        lea       esi, DWORD PTR [676+esp]                      ;242.10
        push      esi                                           ;242.10
        call      _DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST_INSERT ;242.10
                                ; LOE
.B1.265:                        ; Preds .B1.91
        add       esp, 12                                       ;242.10
                                ; LOE
.B1.92:                         ; Preds .B1.265
        mov       esi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;248.8
        neg       esi                                           ;248.85
        mov       ecx, DWORD PTR [676+esp]                      ;248.11
        add       esi, ecx                                      ;248.85
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;244.40
        neg       eax                                           ;244.8
        shl       esi, 5                                        ;248.85
        add       eax, DWORD PTR [668+esp]                      ;244.8
        mov       ebx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;248.8
        imul      eax, eax, 900                                 ;244.8
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;244.8
        movsx     ebx, BYTE PTR [5+ebx+esi]                     ;248.11
        mov       edi, ebx                                      ;248.46
        and       edi, -5                                       ;248.46
        inc       DWORD PTR [640+edx+eax]                       ;244.8
        inc       DWORD PTR [396+edx+eax]                       ;245.5
        cmp       edi, 2                                        ;248.46
        je        .B1.94        ; Prob 16%                      ;248.46
                                ; LOE eax edx ecx ebx
.B1.93:                         ; Preds .B1.92
        cmp       ebx, 7                                        ;248.120
        jne       .B1.95        ; Prob 84%                      ;248.120
                                ; LOE eax edx ecx
.B1.94:                         ; Preds .B1.92 .B1.93
        inc       DWORD PTR [644+edx+eax]                       ;249.7
                                ; LOE ecx
.B1.95:                         ; Preds .B1.93 .B1.94
        sub       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE+32] ;252.8
        lea       edx, DWORD PTR [676+esp]                      ;254.39
        shl       ecx, 6                                        ;252.8
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE] ;252.8
        push      edx                                           ;254.39
        inc       WORD PTR [12+eax+ecx]                         ;252.64
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_P_SIZE  ;254.39
                                ; LOE eax
.B1.266:                        ; Preds .B1.95
        add       esp, 4                                        ;254.39
                                ; LOE eax
.B1.96:                         ; Preds .B1.266
        mov       edx, DWORD PTR [676+esp]                      ;254.11
        dec       eax                                           ;254.55
        sub       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE+32] ;254.38
        shl       edx, 6                                        ;254.38
        mov       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE] ;254.8
        lea       ecx, DWORD PTR [12+edx+ecx]                   ;254.38
        movsx     ebx, WORD PTR [ecx]                           ;254.11
        cmp       ebx, eax                                      ;254.38
        jl        .B1.98        ; Prob 22%                      ;254.38
                                ; LOE ecx
.B1.97:                         ; Preds .B1.96
        mov       DWORD PTR [636+esp], -1                       ;
        jmp       .B1.101       ; Prob 100%                     ;
                                ; LOE
.B1.98:                         ; Preds .B1.96
        lea       eax, DWORD PTR [220+esp]                      ;255.15
        lea       edx, DWORD PTR [212+esp]                      ;255.15
        push      eax                                           ;255.15
        push      edx                                           ;255.15
        push      ecx                                           ;255.15
        lea       ecx, DWORD PTR [680+esp]                      ;255.15
        push      ecx                                           ;255.15
        lea       ebx, DWORD PTR [692+esp]                      ;255.15
        push      ebx                                           ;255.15
        push      DWORD PTR [12+ebp]                            ;255.15
        call      _RETRIEVE_NEXT_LINK                           ;255.15
                                ; LOE
.B1.267:                        ; Preds .B1.98
        add       esp, 24                                       ;255.15
                                ; LOE
.B1.99:                         ; Preds .B1.267
        mov       DWORD PTR [636+esp], -1                       ;
        jmp       .B1.101       ; Prob 100%                     ;
                                ; LOE
.B1.100:                        ; Preds .B1.43
        mov       DWORD PTR [20+esi+edi], 0                     ;98.9
                                ; LOE
.B1.101:                        ; Preds .B1.41 .B1.40 .B1.99 .B1.97 .B1.100
                                ;      
        mov       eax, DWORD PTR [624+esp]                      ;66.19
        cmp       eax, DWORD PTR [628+esp]                      ;66.19
        jl        .B1.13        ; Prob 82%                      ;66.19
                                ; LOE
.B1.102:                        ; Preds .B1.13 .B1.101
        mov       esi, DWORD PTR [636+esp]                      ;
                                ; LOE esi
.B1.103:                        ; Preds .B1.11 .B1.102
        test      esi, 1                                        ;263.10
        je        .B1.105       ; Prob 60%                      ;263.10
                                ; LOE
.B1.104:                        ; Preds .B1.103
        lea       eax, DWORD PTR [668+esp]                      ;264.14
        push      eax                                           ;264.14
        call      _TRANSLINK_ADD_MODULE_mp_TRANLINK_CONSOLIDATE ;264.14
                                ; LOE
.B1.268:                        ; Preds .B1.104
        add       esp, 4                                        ;264.14
                                ; LOE
.B1.105:                        ; Preds .B1.268 .B1.103
        mov       ecx, DWORD PTR [668+esp]                      ;269.10
        mov       eax, ecx                                      ;269.54
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;269.10
        sub       eax, ebx                                      ;269.54
        imul      esi, eax, 900                                 ;269.54
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;269.7
        mov       esi, DWORD PTR [700+edx+esi]                  ;269.10
        test      esi, esi                                      ;269.54
        jle       .B1.245       ; Prob 16%                      ;269.54
                                ; LOE edx ecx ebx esi
.B1.106:                        ; Preds .B1.105
        mov       DWORD PTR [672+esp], 1                        ;270.6
        mov       DWORD PTR [684+esp], esi                      ;270.6
                                ; LOE
.B1.107:                        ; Preds .B1.106 .B1.114
        push      OFFSET FLAT: __NLITPACK_1.0.1                 ;271.18
        lea       eax, DWORD PTR [676+esp]                      ;271.18
        push      eax                                           ;271.18
        lea       edx, DWORD PTR [676+esp]                      ;271.18
        push      edx                                           ;271.18
        call      _DYNUST_TRANSLINK_MODULE_mp_TRANLINK_VALUE    ;271.18
                                ; LOE eax
.B1.108:                        ; Preds .B1.107
        mov       DWORD PTR [688+esp], eax                      ;271.13
        push      OFFSET FLAT: __NLITPACK_2.0.1                 ;272.19
        lea       eax, DWORD PTR [688+esp]                      ;272.19
        push      eax                                           ;272.19
        lea       edx, DWORD PTR [688+esp]                      ;272.19
        push      edx                                           ;272.19
        call      _DYNUST_TRANSLINK_MODULE_mp_TRANLINK_VALUE    ;272.19
                                ; LOE eax
.B1.270:                        ; Preds .B1.108
        add       esp, 24                                       ;272.19
                                ; LOE eax
.B1.109:                        ; Preds .B1.270
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;273.13
        mov       edx, esi                                      ;273.8
        neg       edx                                           ;273.8
        add       edx, eax                                      ;273.8
        imul      ecx, edx, 152                                 ;273.8
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;273.5
        mov       DWORD PTR [680+esp], eax                      ;272.13
        test      BYTE PTR [ebx+ecx], 1                         ;273.13
        jne       .B1.112       ; Prob 40%                      ;273.13
                                ; LOE eax ebx esi
.B1.110:                        ; Preds .B1.109
        sub       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;274.8
        lea       ecx, DWORD PTR [668+esp]                      ;275.14
        imul      edx, eax, 900                                 ;274.8
        lea       ebx, DWORD PTR [680+esp]                      ;275.14
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;274.8
        push      ecx                                           ;275.14
        push      ebx                                           ;275.14
        inc       DWORD PTR [400+eax+edx]                       ;274.8
        call      _DYNUST_NETWORK_MODULE_mp_GETBSTMOVE          ;275.14
                                ; LOE eax
.B1.271:                        ; Preds .B1.110
        add       esp, 8                                        ;275.14
                                ; LOE eax
.B1.111:                        ; Preds .B1.271
        mov       esi, DWORD PTR [680+esp]                      ;276.16
        sub       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;276.16
        imul      edi, esi, 152                                 ;276.16
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;276.16
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;276.16
        mov       edx, DWORD PTR [12+ebx+edi]                   ;276.107
        sub       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;276.16
        imul      edx, edx, 900                                 ;276.16
        mov       esi, DWORD PTR [84+ecx+edx]                   ;276.107
        imul      ecx, DWORD PTR [116+ecx+edx], -40             ;276.16
        mov       edi, DWORD PTR [664+esp]                      ;276.16
        lea       esi, DWORD PTR [esi+edi*8]                    ;276.16
        sub       eax, DWORD PTR [32+ecx+esi]                   ;276.16
        imul      eax, DWORD PTR [28+ecx+esi]                   ;276.16
        mov       edx, DWORD PTR [ecx+esi]                      ;276.107
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;276.16
        inc       DWORD PTR [edx+eax]                           ;276.16
                                ; LOE ebx esi
.B1.112:                        ; Preds .B1.111 .B1.109
        mov       edx, DWORD PTR [676+esp]                      ;278.21
        sub       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;278.16
        shl       edx, 8                                        ;278.16
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;278.13
        test      BYTE PTR [112+eax+edx], 1                     ;278.21
        jne       .B1.114       ; Prob 40%                      ;278.21
                                ; LOE eax edx ebx esi
.B1.113:                        ; Preds .B1.112
        mov       DWORD PTR [112+eax+edx], -1                   ;279.16
        mov       DWORD PTR [240+eax+edx], 0                    ;280.16
                                ; LOE eax edx ebx esi
.B1.114:                        ; Preds .B1.112 .B1.113
        movss     xmm0, DWORD PTR [104+eax+edx]                 ;282.38
        movss     xmm1, DWORD PTR [88+eax+edx]                  ;283.39
        movss     xmm2, DWORD PTR [_DYNUST_MAIN_MODULE_mp_GUITOTALTIME] ;284.13
        addss     xmm0, DWORD PTR [100+eax+edx]                 ;282.13
        addss     xmm1, DWORD PTR [100+eax+edx]                 ;283.13
        addss     xmm2, DWORD PTR [100+eax+edx]                 ;284.13
        movss     DWORD PTR [104+eax+edx], xmm0                 ;282.13
        movss     DWORD PTR [88+eax+edx], xmm1                  ;283.13
        mov       eax, DWORD PTR [672+esp]                      ;285.9
        inc       eax                                           ;285.9
        movss     DWORD PTR [_DYNUST_MAIN_MODULE_mp_GUITOTALTIME], xmm2 ;284.13
        mov       DWORD PTR [672+esp], eax                      ;285.9
        cmp       eax, DWORD PTR [684+esp]                      ;285.9
        jle       .B1.107       ; Prob 82%                      ;285.9
                                ; LOE ebx esi
.B1.115:                        ; Preds .B1.114
        mov       DWORD PTR [612+esp], ebx                      ;
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;289.11
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;289.11
        mov       ecx, DWORD PTR [668+esp]                      ;289.11
                                ; LOE edx ecx ebx esi
.B1.116:                        ; Preds .B1.115 .B1.245
        mov       eax, ebx                                      ;
        neg       esi                                           ;289.11
        neg       eax                                           ;
        add       esi, ecx                                      ;289.11
        add       eax, ecx                                      ;
        imul      eax, eax, 900                                 ;
        mov       edi, DWORD PTR [640+edx+eax]                  ;289.11
        mov       DWORD PTR [616+esp], edi                      ;289.11
        imul      edi, esi, 152                                 ;289.11
        mov       esi, DWORD PTR [612+esp]                      ;289.11
        movsx     esi, WORD PTR [148+esi+edi]                   ;289.11
        cmp       esi, 99                                       ;289.11
        jge       .B1.127       ; Prob 50%                      ;289.11
                                ; LOE eax edx ecx ebx
.B1.117:                        ; Preds .B1.116
        cmp       DWORD PTR [616+esp], 0                        ;289.11
        jle       .B1.127       ; Prob 16%                      ;289.11
                                ; LOE eax edx ecx ebx
.B1.118:                        ; Preds .B1.117
        mov       esi, DWORD PTR [644+edx+eax]                  ;289.11
        test      esi, esi                                      ;289.11
        cvtsi2ss  xmm1, DWORD PTR [616+esp]                     ;289.11
        cvtsi2ss  xmm0, esi                                     ;289.11
        divss     xmm0, xmm1                                    ;289.11
        movss     DWORD PTR [412+edx+eax], xmm0                 ;289.11
        jle       .B1.218       ; Prob 16%                      ;289.11
                                ; LOE eax edx ecx ebx xmm0 xmm1
.B1.119:                        ; Preds .B1.118
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRUCKNUM] ;289.11
        test      esi, esi                                      ;289.11
        jle       .B1.217       ; Prob 2%                       ;289.11
                                ; LOE eax edx ecx ebx esi xmm0 xmm1
.B1.120:                        ; Preds .B1.119
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRUCKBPNT+32] ;289.11
        shl       edi, 2                                        ;
        neg       edi                                           ;
        add       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRUCKBPNT] ;
        mov       DWORD PTR [124+esp], 1                        ;
        cvtsi2ss  xmm2, DWORD PTR [4+edi]                       ;289.11
        divss     xmm2, DWORD PTR [_2il0floatpacket.8]          ;289.11
        comiss    xmm2, xmm0                                    ;289.11
        jae       .B1.216       ; Prob 20%                      ;289.11
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1
.B1.121:                        ; Preds .B1.120
        mov       DWORD PTR [228+esp], ebx                      ;
        movss     xmm3, DWORD PTR [_2il0floatpacket.8]          ;
        mov       ebx, edi                                      ;
        mov       edi, DWORD PTR [124+esp]                      ;
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1 xmm3
.B1.122:                        ; Preds .B1.123 .B1.126 .B1.125 .B1.124 .B1.121
                                ;      
        inc       edi                                           ;289.11
        cmp       edi, esi                                      ;289.11
        jg        .B1.215       ; Prob 18%                      ;289.11
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1 xmm3
.B1.123:                        ; Preds .B1.122
        cmp       edi, 1                                        ;289.11
        je        .B1.122       ; Prob 16%                      ;289.11
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1 xmm3
.B1.124:                        ; Preds .B1.123
        jle       .B1.122       ; Prob 24%                      ;289.11
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1 xmm3
.B1.125:                        ; Preds .B1.124
        cvtsi2ss  xmm2, DWORD PTR [ebx+edi*4]                   ;289.11
        divss     xmm2, xmm3                                    ;289.11
        comiss    xmm2, xmm0                                    ;289.11
        jb        .B1.122       ; Prob 50%                      ;289.11
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1 xmm3
.B1.126:                        ; Preds .B1.125
        cvtsi2ss  xmm2, DWORD PTR [-4+ebx+edi*4]                ;289.11
        divss     xmm2, xmm3                                    ;289.11
        comiss    xmm0, xmm2                                    ;289.11
        jbe       .B1.122       ; Prob 80%                      ;289.11
        jmp       .B1.213       ; Prob 100%                     ;289.11
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1 xmm3
.B1.127:                        ; Preds .B1.116 .B1.117
        movss     xmm3, DWORD PTR [_2il0floatpacket.7]          ;289.11
        mov       DWORD PTR [856+edx+eax], 1075838976           ;289.11
        cvtsi2ss  xmm1, DWORD PTR [616+esp]                     ;291.80
        movss     xmm0, DWORD PTR [412+edx+eax]                 ;291.44
                                ; LOE eax edx ecx ebx xmm0 xmm1 xmm3
.B1.128:                        ; Preds .B1.127 .B1.214 .B1.218
        movss     xmm2, DWORD PTR [_2il0floatpacket.12]         ;291.148
        inc       ecx                                           ;48.7
        mulss     xmm3, xmm0                                    ;291.111
        subss     xmm2, xmm0                                    ;291.148
        cmp       ecx, DWORD PTR [608+esp]                      ;48.7
        addss     xmm3, xmm2                                    ;291.6
        mulss     xmm3, xmm1                                    ;291.6
        movss     DWORD PTR [688+edx+eax], xmm3                 ;291.6
        jle       .B1.129       ; Prob 82%                      ;48.7
        jmp       .B1.220       ; Prob 100%                     ;48.7
                                ; LOE ecx ebx
.B1.130:                        ; Preds .B1.2                   ; Infreq
        mov       esi, 1                                        ;
        jmp       .B1.6         ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B1.131:                        ; Preds .B1.90                  ; Infreq
        mov       DWORD PTR [304+esp], 0                        ;237.12
        lea       ebx, DWORD PTR [304+esp]                      ;237.12
        mov       DWORD PTR [176+esp], 23                       ;237.12
        lea       eax, DWORD PTR [176+esp]                      ;237.12
        mov       DWORD PTR [180+esp], OFFSET FLAT: __STRLITPACK_15 ;237.12
        push      32                                            ;237.12
        push      eax                                           ;237.12
        push      OFFSET FLAT: __STRLITPACK_82.0.1              ;237.12
        push      -2088435968                                   ;237.12
        push      911                                           ;237.12
        push      ebx                                           ;237.12
        call      _for_write_seq_lis                            ;237.12
                                ; LOE ebx
.B1.132:                        ; Preds .B1.131                 ; Infreq
        mov       eax, DWORD PTR [524+esp]                      ;237.12
        lea       edx, DWORD PTR [280+esp]                      ;237.12
        mov       DWORD PTR [280+esp], eax                      ;237.12
        push      edx                                           ;237.12
        push      OFFSET FLAT: __STRLITPACK_83.0.1              ;237.12
        push      ebx                                           ;237.12
        call      _for_write_seq_lis_xmit                       ;237.12
                                ; LOE ebx
.B1.133:                        ; Preds .B1.132                 ; Infreq
        mov       DWORD PTR [340+esp], 0                        ;238.7
        lea       eax, DWORD PTR [220+esp]                      ;238.7
        mov       DWORD PTR [220+esp], 25                       ;238.7
        mov       DWORD PTR [224+esp], OFFSET FLAT: __STRLITPACK_13 ;238.7
        push      32                                            ;238.7
        push      eax                                           ;238.7
        push      OFFSET FLAT: __STRLITPACK_84.0.1              ;238.7
        push      -2088435968                                   ;238.7
        push      911                                           ;238.7
        push      ebx                                           ;238.7
        call      _for_write_seq_lis                            ;238.7
                                ; LOE
.B1.134:                        ; Preds .B1.133                 ; Infreq
        push      32                                            ;239.12
        xor       eax, eax                                      ;239.12
        push      eax                                           ;239.12
        push      eax                                           ;239.12
        push      -2088435968                                   ;239.12
        push      eax                                           ;239.12
        push      OFFSET FLAT: __STRLITPACK_85                  ;239.12
        call      _for_stop_core                                ;239.12
                                ; LOE
.B1.272:                        ; Preds .B1.134                 ; Infreq
        add       esp, 84                                       ;239.12
        jmp       .B1.91        ; Prob 100%                     ;239.12
                                ; LOE
.B1.135:                        ; Preds .B1.84                  ; Infreq
        mov       edx, DWORD PTR [564+esi+ebx]                  ;227.96
        add       edi, edx                                      ;227.50
        mov       edx, DWORD PTR [596+esi+ebx]                  ;227.96
        shl       edx, 2                                        ;227.50
        neg       edx                                           ;227.50
        inc       DWORD PTR [edx+edi]                           ;227.50
        jmp       .B1.87        ; Prob 100%                     ;227.50
                                ; LOE eax ecx ebx esi
.B1.136:                        ; Preds .B1.56                  ; Infreq
        imul      edx, DWORD PTR [668+esp], 152                 ;137.8
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_BACKPOINTR] ;137.8
        mov       edi, DWORD PTR [28+edx+edi]                   ;137.8
        mov       edx, 1                                        ;137.8
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_BACKPOINTR+32] ;137.8
        shl       ebx, 2                                        ;137.8
        lea       ecx, DWORD PTR [eax+edi*4]                    ;137.20
        mov       eax, DWORD PTR [548+esp]                      ;138.8
        sub       ecx, ebx                                      ;137.8
        xor       edi, edi                                      ;138.8
        mov       ebx, DWORD PTR [4+ecx]                        ;137.20
        sub       ebx, DWORD PTR [ecx]                          ;137.20
        test      ebx, ebx                                      ;137.8
        mov       ecx, DWORD PTR [580+esp]                      ;138.8
        cmovle    ebx, edx                                      ;137.8
        mov       edx, 2                                        ;138.8
        test      ebx, ebx                                      ;138.8
        mov       DWORD PTR [8+eax+ecx], edi                    ;138.8
        cmovge    edi, ebx                                      ;138.8
        mov       DWORD PTR [72+esp], ebx                       ;137.8
        mov       DWORD PTR [12+eax+ecx], 5                     ;138.8
        mov       DWORD PTR [4+eax+ecx], edx                    ;138.8
        mov       DWORD PTR [16+eax+ecx], 1                     ;138.8
        mov       DWORD PTR [32+eax+ecx], 1                     ;138.8
        mov       DWORD PTR [68+esp], edi                       ;138.8
        mov       DWORD PTR [24+eax+ecx], edi                   ;138.8
        mov       DWORD PTR [28+eax+ecx], edx                   ;138.8
        lea       eax, DWORD PTR [52+esp]                       ;138.8
        push      edx                                           ;138.8
        push      edi                                           ;138.8
        push      edx                                           ;138.8
        push      eax                                           ;138.8
        call      _for_check_mult_overflow                      ;138.8
                                ; LOE eax esi
.B1.273:                        ; Preds .B1.136                 ; Infreq
        mov       DWORD PTR [112+esp], eax                      ;138.8
                                ; LOE esi
.B1.137:                        ; Preds .B1.273                 ; Infreq
        mov       edx, DWORD PTR [696+esp]                      ;138.8
        sub       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;138.8
        imul      ebx, edx, 152                                 ;138.8
        imul      eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32], 900 ;138.8
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;138.8
        neg       eax                                           ;138.8
        imul      edi, DWORD PTR [12+ecx+ebx], 900              ;138.17
        add       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;138.8
        mov       ecx, DWORD PTR [112+esp]                      ;138.8
        and       ecx, 1                                        ;138.8
        imul      edx, DWORD PTR [152+eax+edi], -40             ;138.8
        add       edx, DWORD PTR [120+eax+edi]                  ;138.8
        mov       eax, DWORD PTR [444+esp]                      ;138.8
        shl       ecx, 4                                        ;138.8
        or        ecx, 262144                                   ;138.8
        push      ecx                                           ;138.8
        lea       ebx, DWORD PTR [edx+eax*8]                    ;138.8
        push      ebx                                           ;138.8
        push      DWORD PTR [76+esp]                            ;138.8
        call      _for_allocate                                 ;138.8
                                ; LOE esi
.B1.274:                        ; Preds .B1.137                 ; Infreq
        add       esp, 28                                       ;138.8
                                ; LOE esi
.B1.138:                        ; Preds .B1.274                 ; Infreq
        mov       ecx, DWORD PTR [680+esp]                      ;139.8
        sub       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;139.8
        imul      edx, ecx, 152                                 ;139.8
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;139.8
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;139.8
        neg       eax                                           ;139.8
        add       eax, DWORD PTR [12+ebx+edx]                   ;139.8
        imul      edx, eax, 900                                 ;139.8
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;139.8
        mov       DWORD PTR [60+esp], ebx                       ;139.8
        mov       ecx, DWORD PTR [428+esp]                      ;139.8
        mov       edi, DWORD PTR [120+ebx+edx]                  ;139.8
        imul      ebx, DWORD PTR [152+ebx+edx], -40             ;139.8
        mov       DWORD PTR [64+esp], edx                       ;139.8
        lea       ecx, DWORD PTR [edi+ecx*8]                    ;139.8
        mov       eax, DWORD PTR [32+ebx+ecx]                   ;139.8
        mov       edi, DWORD PTR [28+ebx+ecx]                   ;139.8
        cmp       edi, 2                                        ;139.8
        mov       edx, DWORD PTR [24+ebx+ecx]                   ;139.8
        mov       DWORD PTR [104+esp], eax                      ;139.8
        mov       DWORD PTR [100+esp], edi                      ;139.8
        jne       .B1.156       ; Prob 50%                      ;139.8
                                ; LOE edx ecx ebx esi
.B1.139:                        ; Preds .B1.138                 ; Infreq
        test      edx, edx                                      ;139.8
        jle       .B1.163       ; Prob 50%                      ;139.8
                                ; LOE edx ecx ebx esi
.B1.140:                        ; Preds .B1.139                 ; Infreq
        cmp       edx, 8                                        ;139.8
        jl        .B1.191       ; Prob 10%                      ;139.8
                                ; LOE edx ecx ebx esi
.B1.141:                        ; Preds .B1.140                 ; Infreq
        mov       edi, DWORD PTR [ebx+ecx]                      ;139.8
        mov       DWORD PTR [28+esp], edi                       ;139.8
        and       edi, 15                                       ;139.8
        je        .B1.144       ; Prob 50%                      ;139.8
                                ; LOE edx ecx ebx esi edi
.B1.142:                        ; Preds .B1.141                 ; Infreq
        test      edi, 1                                        ;139.8
        jne       .B1.191       ; Prob 10%                      ;139.8
                                ; LOE edx ecx ebx esi edi
.B1.143:                        ; Preds .B1.142                 ; Infreq
        neg       edi                                           ;139.8
        add       edi, 16                                       ;139.8
        shr       edi, 1                                        ;139.8
                                ; LOE edx ecx ebx esi edi
.B1.144:                        ; Preds .B1.143 .B1.141         ; Infreq
        lea       eax, DWORD PTR [8+edi]                        ;139.8
        cmp       edx, eax                                      ;139.8
        jl        .B1.191       ; Prob 10%                      ;139.8
                                ; LOE edx ecx ebx esi edi
.B1.145:                        ; Preds .B1.144                 ; Infreq
        mov       eax, edx                                      ;139.8
        sub       eax, edi                                      ;139.8
        and       eax, 7                                        ;139.8
        neg       eax                                           ;139.8
        add       eax, edx                                      ;139.8
        test      edi, edi                                      ;139.8
        jbe       .B1.149       ; Prob 10%                      ;139.8
                                ; LOE eax edx ecx ebx esi edi
.B1.146:                        ; Preds .B1.145                 ; Infreq
        mov       DWORD PTR [32+esp], 0                         ;
        mov       DWORD PTR [esp], ebx                          ;
        mov       DWORD PTR [4+esp], ecx                        ;
        mov       DWORD PTR [8+esp], esi                        ;
        xor       esi, esi                                      ;
        mov       ecx, DWORD PTR [32+esp]                       ;
        mov       ebx, DWORD PTR [28+esp]                       ;
                                ; LOE eax edx ecx ebx esi edi
.B1.147:                        ; Preds .B1.147 .B1.146         ; Infreq
        mov       WORD PTR [ebx+ecx*2], si                      ;139.8
        inc       ecx                                           ;139.8
        cmp       ecx, edi                                      ;139.8
        jb        .B1.147       ; Prob 82%                      ;139.8
                                ; LOE eax edx ecx ebx esi edi
.B1.148:                        ; Preds .B1.147                 ; Infreq
        mov       ebx, DWORD PTR [esp]                          ;
        mov       ecx, DWORD PTR [4+esp]                        ;
        mov       esi, DWORD PTR [8+esp]                        ;
                                ; LOE eax edx ecx ebx esi edi
.B1.149:                        ; Preds .B1.145 .B1.148         ; Infreq
        mov       DWORD PTR [8+esp], esi                        ;
        mov       esi, DWORD PTR [28+esp]                       ;
                                ; LOE eax edx ecx ebx esi edi
.B1.150:                        ; Preds .B1.150 .B1.149         ; Infreq
        pxor      xmm0, xmm0                                    ;139.8
        movdqa    XMMWORD PTR [esi+edi*2], xmm0                 ;139.8
        add       edi, 8                                        ;139.8
        cmp       edi, eax                                      ;139.8
        jb        .B1.150       ; Prob 82%                      ;139.8
                                ; LOE eax edx ecx ebx esi edi
.B1.151:                        ; Preds .B1.150                 ; Infreq
        mov       esi, DWORD PTR [8+esp]                        ;
                                ; LOE eax edx ecx ebx esi
.B1.152:                        ; Preds .B1.151 .B1.191         ; Infreq
        cmp       eax, edx                                      ;139.8
        jae       .B1.163       ; Prob 10%                      ;139.8
                                ; LOE eax edx ecx ebx esi
.B1.153:                        ; Preds .B1.152                 ; Infreq
        mov       edi, DWORD PTR [ebx+ecx]                      ;139.8
        mov       DWORD PTR [8+esp], esi                        ;139.8
        xor       esi, esi                                      ;139.8
                                ; LOE eax edx ecx ebx esi edi
.B1.154:                        ; Preds .B1.154 .B1.153         ; Infreq
        mov       WORD PTR [edi+eax*2], si                      ;139.8
        inc       eax                                           ;139.8
        cmp       eax, edx                                      ;139.8
        jb        .B1.154       ; Prob 82%                      ;139.8
                                ; LOE eax edx ecx ebx esi edi
.B1.155:                        ; Preds .B1.154                 ; Infreq
        mov       esi, DWORD PTR [8+esp]                        ;
        jmp       .B1.163       ; Prob 100%                     ;
                                ; LOE ecx ebx esi
.B1.156:                        ; Preds .B1.138                 ; Infreq
        test      edx, edx                                      ;139.8
        jle       .B1.163       ; Prob 50%                      ;139.8
                                ; LOE edx ecx ebx esi
.B1.157:                        ; Preds .B1.156                 ; Infreq
        mov       eax, edx                                      ;139.8
        shr       eax, 31                                       ;139.8
        add       eax, edx                                      ;139.8
        sar       eax, 1                                        ;139.8
        mov       DWORD PTR [116+esp], eax                      ;139.8
        test      eax, eax                                      ;139.8
        jbe       .B1.194       ; Prob 10%                      ;139.8
                                ; LOE edx ecx ebx esi
.B1.158:                        ; Preds .B1.157                 ; Infreq
        mov       edi, DWORD PTR [104+esp]                      ;
        xor       eax, eax                                      ;
        imul      edi, DWORD PTR [100+esp]                      ;
        mov       DWORD PTR [44+esp], eax                       ;
        mov       eax, DWORD PTR [ebx+ecx]                      ;139.8
        sub       eax, edi                                      ;
        mov       DWORD PTR [40+esp], eax                       ;
        xor       eax, eax                                      ;
        mov       DWORD PTR [12+esp], edx                       ;
        mov       DWORD PTR [4+esp], ecx                        ;
        mov       DWORD PTR [esp], ebx                          ;
        mov       DWORD PTR [8+esp], esi                        ;
        mov       edx, DWORD PTR [40+esp]                       ;
        mov       ecx, DWORD PTR [44+esp]                       ;
                                ; LOE eax edx ecx
.B1.159:                        ; Preds .B1.159 .B1.158         ; Infreq
        mov       ebx, DWORD PTR [104+esp]                      ;139.8
        mov       edi, DWORD PTR [100+esp]                      ;139.8
        mov       esi, edi                                      ;139.8
        lea       ebx, DWORD PTR [ebx+ecx*2]                    ;139.8
        inc       ecx                                           ;139.8
        imul      esi, ebx                                      ;139.8
        inc       ebx                                           ;139.8
        imul      ebx, edi                                      ;139.8
        mov       WORD PTR [edx+esi], ax                        ;139.8
        mov       WORD PTR [edx+ebx], ax                        ;139.8
        cmp       ecx, DWORD PTR [116+esp]                      ;139.8
        jb        .B1.159       ; Prob 64%                      ;139.8
                                ; LOE eax edx ecx
.B1.160:                        ; Preds .B1.159                 ; Infreq
        mov       DWORD PTR [44+esp], ecx                       ;
        mov       eax, ecx                                      ;139.8
        mov       edx, DWORD PTR [12+esp]                       ;
        mov       ebx, DWORD PTR [esp]                          ;
        mov       ecx, DWORD PTR [4+esp]                        ;
        lea       edi, DWORD PTR [1+eax+eax]                    ;139.8
        mov       esi, DWORD PTR [8+esp]                        ;
                                ; LOE edx ecx ebx esi edi
.B1.161:                        ; Preds .B1.160 .B1.194         ; Infreq
        lea       eax, DWORD PTR [-1+edi]                       ;139.8
        cmp       edx, eax                                      ;139.8
        jbe       .B1.163       ; Prob 10%                      ;139.8
                                ; LOE ecx ebx esi edi
.B1.162:                        ; Preds .B1.161                 ; Infreq
        mov       edx, DWORD PTR [104+esp]                      ;139.8
        mov       eax, edx                                      ;139.8
        neg       eax                                           ;139.8
        lea       edi, DWORD PTR [-1+edi+edx]                   ;139.8
        xor       edx, edx                                      ;139.8
        add       eax, edi                                      ;139.8
        imul      eax, DWORD PTR [100+esp]                      ;139.8
        mov       edi, DWORD PTR [ebx+ecx]                      ;139.8
        mov       WORD PTR [edi+eax], dx                        ;139.8
                                ; LOE ecx ebx esi
.B1.163:                        ; Preds .B1.139 .B1.156 .B1.155 .B1.161 .B1.152
                                ;       .B1.162                 ; Infreq
        mov       eax, DWORD PTR [72+esp]                       ;140.8
        mov       WORD PTR [36+ebx+ecx], ax                     ;140.8
        mov       edi, DWORD PTR [60+esp]                       ;141.17
        mov       eax, DWORD PTR [64+esp]                       ;141.17
        mov       ebx, DWORD PTR [428+esp]                      ;141.8
        mov       edx, DWORD PTR [48+edi+eax]                   ;141.17
        lea       ecx, DWORD PTR [edx+ebx*8]                    ;141.8
        mov       ebx, 4                                        ;141.8
        imul      edx, DWORD PTR [80+edi+eax], -40              ;141.8
        mov       edi, DWORD PTR [68+esp]                       ;141.8
        mov       DWORD PTR [12+edx+ecx], 5                     ;141.8
        mov       DWORD PTR [4+edx+ecx], ebx                    ;141.8
        mov       DWORD PTR [16+edx+ecx], 1                     ;141.8
        mov       DWORD PTR [8+edx+ecx], 0                      ;141.8
        mov       DWORD PTR [32+edx+ecx], 1                     ;141.8
        mov       DWORD PTR [24+edx+ecx], edi                   ;141.8
        mov       DWORD PTR [28+edx+ecx], ebx                   ;141.8
        lea       ecx, DWORD PTR [56+esp]                       ;141.8
        push      ebx                                           ;141.8
        push      edi                                           ;141.8
        push      2                                             ;141.8
        push      ecx                                           ;141.8
        call      _for_check_mult_overflow                      ;141.8
                                ; LOE eax esi
.B1.275:                        ; Preds .B1.163                 ; Infreq
        mov       DWORD PTR [108+esp], eax                      ;141.8
                                ; LOE esi
.B1.164:                        ; Preds .B1.275                 ; Infreq
        mov       edx, DWORD PTR [696+esp]                      ;141.8
        sub       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;141.8
        imul      ebx, edx, 152                                 ;141.8
        imul      eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32], 900 ;141.8
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;141.8
        neg       eax                                           ;141.8
        imul      edi, DWORD PTR [12+ecx+ebx], 900              ;141.17
        add       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;141.8
        mov       ecx, DWORD PTR [108+esp]                      ;141.8
        and       ecx, 1                                        ;141.8
        imul      edx, DWORD PTR [80+eax+edi], -40              ;141.8
        add       edx, DWORD PTR [48+eax+edi]                   ;141.8
        mov       eax, DWORD PTR [444+esp]                      ;141.8
        shl       ecx, 4                                        ;141.8
        or        ecx, 262144                                   ;141.8
        push      ecx                                           ;141.8
        lea       ebx, DWORD PTR [edx+eax*8]                    ;141.8
        push      ebx                                           ;141.8
        push      DWORD PTR [80+esp]                            ;141.8
        call      _for_allocate                                 ;141.8
                                ; LOE esi
.B1.276:                        ; Preds .B1.164                 ; Infreq
        add       esp, 28                                       ;141.8
                                ; LOE esi
.B1.165:                        ; Preds .B1.276                 ; Infreq
        mov       edx, DWORD PTR [680+esp]                      ;142.8
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;142.8
        mov       DWORD PTR [80+esp], edx                       ;142.8
        sub       edx, edi                                      ;142.8
        mov       DWORD PTR [84+esp], edi                       ;142.8
        imul      edi, edx, 152                                 ;142.8
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;142.8
        mov       DWORD PTR [88+esp], ecx                       ;142.8
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;142.8
        mov       ecx, DWORD PTR [12+ecx+edi]                   ;142.8
        sub       ecx, ebx                                      ;142.8
        imul      edi, ecx, 900                                 ;142.8
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;142.8
        mov       DWORD PTR [444+esp], ebx                      ;142.8
        mov       ecx, DWORD PTR [428+esp]                      ;142.8
        mov       ebx, DWORD PTR [48+eax+edi]                   ;142.8
        lea       edx, DWORD PTR [ebx+ecx*8]                    ;142.8
        mov       DWORD PTR [76+esp], edx                       ;142.8
        imul      ebx, DWORD PTR [80+eax+edi], -40              ;142.8
        mov       ecx, DWORD PTR [32+ebx+edx]                   ;142.8
        mov       DWORD PTR [112+esp], ecx                      ;142.8
        mov       ecx, DWORD PTR [24+ebx+edx]                   ;142.8
        mov       edx, DWORD PTR [28+ebx+edx]                   ;142.8
        cmp       edx, 4                                        ;142.8
        mov       DWORD PTR [108+esp], edx                      ;142.8
        jne       .B1.183       ; Prob 50%                      ;142.8
                                ; LOE eax ecx ebx esi
.B1.166:                        ; Preds .B1.165                 ; Infreq
        test      ecx, ecx                                      ;142.8
        jle       .B1.190       ; Prob 50%                      ;142.8
                                ; LOE eax ecx ebx esi
.B1.167:                        ; Preds .B1.166                 ; Infreq
        cmp       ecx, 4                                        ;142.8
        jl        .B1.195       ; Prob 10%                      ;142.8
                                ; LOE eax ecx ebx esi
.B1.168:                        ; Preds .B1.167                 ; Infreq
        mov       edx, DWORD PTR [76+esp]                       ;142.8
        mov       edx, DWORD PTR [ebx+edx]                      ;142.8
        mov       DWORD PTR [24+esp], edx                       ;142.8
        and       edx, 15                                       ;142.8
        je        .B1.171       ; Prob 50%                      ;142.8
                                ; LOE eax edx ecx ebx esi
.B1.169:                        ; Preds .B1.168                 ; Infreq
        test      dl, 3                                         ;142.8
        jne       .B1.195       ; Prob 10%                      ;142.8
                                ; LOE eax edx ecx ebx esi
.B1.170:                        ; Preds .B1.169                 ; Infreq
        neg       edx                                           ;142.8
        add       edx, 16                                       ;142.8
        shr       edx, 2                                        ;142.8
                                ; LOE eax edx ecx ebx esi
.B1.171:                        ; Preds .B1.170 .B1.168         ; Infreq
        lea       edi, DWORD PTR [4+edx]                        ;142.8
        cmp       ecx, edi                                      ;142.8
        jl        .B1.195       ; Prob 10%                      ;142.8
                                ; LOE eax edx ecx ebx esi
.B1.172:                        ; Preds .B1.171                 ; Infreq
        mov       edi, ecx                                      ;142.8
        sub       edi, edx                                      ;142.8
        and       edi, 3                                        ;142.8
        neg       edi                                           ;142.8
        add       edi, ecx                                      ;142.8
        test      edx, edx                                      ;142.8
        jbe       .B1.176       ; Prob 10%                      ;142.8
                                ; LOE eax edx ecx ebx esi edi
.B1.173:                        ; Preds .B1.172                 ; Infreq
        mov       DWORD PTR [36+esp], 0                         ;
        mov       DWORD PTR [436+esp], eax                      ;
        mov       DWORD PTR [8+esp], esi                        ;
        mov       eax, DWORD PTR [36+esp]                       ;
        mov       esi, DWORD PTR [24+esp]                       ;
                                ; LOE eax edx ecx ebx esi edi
.B1.174:                        ; Preds .B1.174 .B1.173         ; Infreq
        mov       DWORD PTR [esi+eax*4], 0                      ;142.8
        inc       eax                                           ;142.8
        cmp       eax, edx                                      ;142.8
        jb        .B1.174       ; Prob 82%                      ;142.8
                                ; LOE eax edx ecx ebx esi edi
.B1.175:                        ; Preds .B1.174                 ; Infreq
        mov       eax, DWORD PTR [436+esp]                      ;
        mov       esi, DWORD PTR [8+esp]                        ;
                                ; LOE eax edx ecx ebx esi edi
.B1.176:                        ; Preds .B1.172 .B1.175         ; Infreq
        mov       DWORD PTR [8+esp], esi                        ;
        mov       esi, DWORD PTR [24+esp]                       ;
                                ; LOE eax edx ecx ebx esi edi
.B1.177:                        ; Preds .B1.177 .B1.176         ; Infreq
        pxor      xmm0, xmm0                                    ;142.8
        movdqa    XMMWORD PTR [esi+edx*4], xmm0                 ;142.8
        add       edx, 4                                        ;142.8
        cmp       edx, edi                                      ;142.8
        jb        .B1.177       ; Prob 82%                      ;142.8
                                ; LOE eax edx ecx ebx esi edi
.B1.178:                        ; Preds .B1.177                 ; Infreq
        mov       esi, DWORD PTR [8+esp]                        ;
                                ; LOE eax ecx ebx esi edi
.B1.179:                        ; Preds .B1.178 .B1.195         ; Infreq
        cmp       edi, ecx                                      ;142.8
        jae       .B1.190       ; Prob 10%                      ;142.8
                                ; LOE eax ecx ebx esi edi
.B1.180:                        ; Preds .B1.179                 ; Infreq
        mov       edx, DWORD PTR [76+esp]                       ;142.8
        mov       edx, DWORD PTR [ebx+edx]                      ;142.8
                                ; LOE eax edx ecx ebx esi edi
.B1.181:                        ; Preds .B1.181 .B1.180         ; Infreq
        mov       DWORD PTR [edx+edi*4], 0                      ;142.8
        inc       edi                                           ;142.8
        cmp       edi, ecx                                      ;142.8
        jb        .B1.181       ; Prob 82%                      ;142.8
        jmp       .B1.190       ; Prob 100%                     ;142.8
                                ; LOE eax edx ecx ebx esi edi
.B1.183:                        ; Preds .B1.165                 ; Infreq
        test      ecx, ecx                                      ;142.8
        jle       .B1.190       ; Prob 50%                      ;142.8
                                ; LOE eax ecx ebx esi
.B1.184:                        ; Preds .B1.183                 ; Infreq
        mov       edx, ecx                                      ;142.8
        shr       edx, 31                                       ;142.8
        add       edx, ecx                                      ;142.8
        sar       edx, 1                                        ;142.8
        mov       DWORD PTR [120+esp], edx                      ;142.8
        test      edx, edx                                      ;142.8
        jbe       .B1.198       ; Prob 10%                      ;142.8
                                ; LOE eax ecx ebx esi
.B1.185:                        ; Preds .B1.184                 ; Infreq
        mov       edx, DWORD PTR [112+esp]                      ;
        xor       edi, edi                                      ;
        imul      edx, DWORD PTR [108+esp]                      ;
        mov       DWORD PTR [48+esp], edi                       ;
        mov       edi, DWORD PTR [76+esp]                       ;142.8
        mov       DWORD PTR [436+esp], eax                      ;
        mov       DWORD PTR [16+esp], ecx                       ;
        xor       ecx, ecx                                      ;
        mov       edi, DWORD PTR [ebx+edi]                      ;142.8
        sub       edi, edx                                      ;
        mov       edx, DWORD PTR [48+esp]                       ;
        mov       eax, edi                                      ;
        mov       DWORD PTR [20+esp], ebx                       ;
        mov       DWORD PTR [8+esp], esi                        ;
                                ; LOE eax edx ecx
.B1.186:                        ; Preds .B1.186 .B1.185         ; Infreq
        mov       ebx, DWORD PTR [112+esp]                      ;142.8
        mov       edi, DWORD PTR [108+esp]                      ;142.8
        mov       esi, edi                                      ;142.8
        lea       ebx, DWORD PTR [ebx+edx*2]                    ;142.8
        inc       edx                                           ;142.8
        imul      esi, ebx                                      ;142.8
        inc       ebx                                           ;142.8
        imul      ebx, edi                                      ;142.8
        mov       DWORD PTR [eax+esi], ecx                      ;142.8
        mov       DWORD PTR [eax+ebx], ecx                      ;142.8
        cmp       edx, DWORD PTR [120+esp]                      ;142.8
        jb        .B1.186       ; Prob 64%                      ;142.8
                                ; LOE eax edx ecx
.B1.187:                        ; Preds .B1.186                 ; Infreq
        mov       ecx, DWORD PTR [16+esp]                       ;
        lea       edi, DWORD PTR [1+edx+edx]                    ;142.8
        mov       ebx, DWORD PTR [20+esp]                       ;
        mov       eax, DWORD PTR [436+esp]                      ;
        mov       esi, DWORD PTR [8+esp]                        ;
                                ; LOE eax ecx ebx esi edi
.B1.188:                        ; Preds .B1.187 .B1.198         ; Infreq
        lea       edx, DWORD PTR [-1+edi]                       ;142.8
        cmp       ecx, edx                                      ;142.8
        jbe       .B1.190       ; Prob 10%                      ;142.8
                                ; LOE eax ebx esi edi
.B1.189:                        ; Preds .B1.188                 ; Infreq
        mov       ecx, DWORD PTR [112+esp]                      ;142.8
        mov       edx, ecx                                      ;142.8
        neg       edx                                           ;142.8
        lea       edi, DWORD PTR [-1+edi+ecx]                   ;142.8
        mov       ecx, DWORD PTR [76+esp]                       ;142.8
        add       edx, edi                                      ;142.8
        imul      edx, DWORD PTR [108+esp]                      ;142.8
        mov       edi, DWORD PTR [ebx+ecx]                      ;142.8
        mov       DWORD PTR [edi+edx], 0                        ;142.8
                                ; LOE eax ebx esi
.B1.190:                        ; Preds .B1.181 .B1.166 .B1.183 .B1.179 .B1.188
                                ;       .B1.189                 ; Infreq
        mov       ecx, DWORD PTR [76+esp]                       ;143.8
        mov       edx, DWORD PTR [72+esp]                       ;143.8
        mov       edi, DWORD PTR [676+esp]                      ;147.229
        mov       WORD PTR [36+ebx+ecx], dx                     ;143.8
        mov       ecx, DWORD PTR [84+esp]                       ;146.114
        neg       ecx                                           ;146.114
        mov       ebx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;147.111
        add       ecx, DWORD PTR [80+esp]                       ;146.114
        mov       DWORD PTR [508+esp], ebx                      ;147.111
        mov       ebx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;147.111
        mov       edx, ebx                                      ;
        imul      ecx, ecx, 152                                 ;146.114
        shl       edx, 8                                        ;
        mov       DWORD PTR [460+esp], edx                      ;
        mov       edx, DWORD PTR [88+esp]                       ;146.114
        mov       DWORD PTR [476+esp], edi                      ;147.229
        shl       edi, 8                                        ;161.58
        mov       DWORD PTR [468+esp], edi                      ;161.58
        imul      edx, DWORD PTR [12+edx+ecx], 900              ;146.114
        jmp       .B1.57        ; Prob 100%                     ;146.114
                                ; LOE eax edx ebx esi
.B1.191:                        ; Preds .B1.140 .B1.144 .B1.142 ; Infreq
        xor       eax, eax                                      ;139.8
        jmp       .B1.152       ; Prob 100%                     ;139.8
                                ; LOE eax edx ecx ebx esi
.B1.194:                        ; Preds .B1.157                 ; Infreq
        mov       edi, 1                                        ;
        jmp       .B1.161       ; Prob 100%                     ;
                                ; LOE edx ecx ebx esi edi
.B1.195:                        ; Preds .B1.167 .B1.171 .B1.169 ; Infreq
        xor       edi, edi                                      ;142.8
        jmp       .B1.179       ; Prob 100%                     ;142.8
                                ; LOE eax ecx ebx esi edi
.B1.198:                        ; Preds .B1.184                 ; Infreq
        mov       edi, 1                                        ;
        jmp       .B1.188       ; Prob 100%                     ;
                                ; LOE eax ecx ebx esi edi
.B1.199:                        ; Preds .B1.48                  ; Infreq
        mov       DWORD PTR [304+esp], 0                        ;120.5
        lea       ebx, DWORD PTR [304+esp]                      ;120.5
        mov       DWORD PTR [128+esp], 34                       ;120.5
        lea       eax, DWORD PTR [128+esp]                      ;120.5
        mov       DWORD PTR [132+esp], OFFSET FLAT: __STRLITPACK_36 ;120.5
        push      32                                            ;120.5
        push      eax                                           ;120.5
        push      OFFSET FLAT: __STRLITPACK_61.0.1              ;120.5
        push      -2088435968                                   ;120.5
        push      911                                           ;120.5
        push      ebx                                           ;120.5
        call      _for_write_seq_lis                            ;120.5
                                ; LOE ebx esi
.B1.200:                        ; Preds .B1.199                 ; Infreq
        mov       DWORD PTR [328+esp], 0                        ;121.5
        lea       eax, DWORD PTR [160+esp]                      ;121.5
        mov       DWORD PTR [160+esp], 17                       ;121.5
        mov       DWORD PTR [164+esp], OFFSET FLAT: __STRLITPACK_34 ;121.5
        push      32                                            ;121.5
        push      eax                                           ;121.5
        push      OFFSET FLAT: __STRLITPACK_62.0.1              ;121.5
        push      -2088435968                                   ;121.5
        push      911                                           ;121.5
        push      ebx                                           ;121.5
        call      _for_write_seq_lis                            ;121.5
                                ; LOE ebx esi
.B1.201:                        ; Preds .B1.200                 ; Infreq
        mov       eax, DWORD PTR [728+esp]                      ;121.5
        lea       edx, DWORD PTR [264+esp]                      ;121.5
        mov       DWORD PTR [568+esp], eax                      ;121.5
        mov       DWORD PTR [264+esp], eax                      ;121.5
        push      edx                                           ;121.5
        push      OFFSET FLAT: __STRLITPACK_63.0.1              ;121.5
        push      ebx                                           ;121.5
        call      _for_write_seq_lis_xmit                       ;121.5
                                ; LOE ebx esi
.B1.202:                        ; Preds .B1.201                 ; Infreq
        mov       eax, DWORD PTR [728+esp]                      ;121.5
        lea       edx, DWORD PTR [284+esp]                      ;121.5
        mov       DWORD PTR [264+esp], eax                      ;121.5
        mov       DWORD PTR [284+esp], eax                      ;121.5
        push      edx                                           ;121.5
        push      OFFSET FLAT: __STRLITPACK_64.0.1              ;121.5
        push      ebx                                           ;121.5
        call      _for_write_seq_lis_xmit                       ;121.5
                                ; LOE ebx esi
.B1.203:                        ; Preds .B1.202                 ; Infreq
        mov       DWORD PTR [376+esp], 0                        ;122.5
        lea       eax, DWORD PTR [216+esp]                      ;122.5
        mov       DWORD PTR [216+esp], 18                       ;122.5
        mov       DWORD PTR [220+esp], OFFSET FLAT: __STRLITPACK_32 ;122.5
        push      32                                            ;122.5
        push      eax                                           ;122.5
        push      OFFSET FLAT: __STRLITPACK_65.0.1              ;122.5
        push      -2088435968                                   ;122.5
        push      911                                           ;122.5
        push      ebx                                           ;122.5
        call      _for_write_seq_lis                            ;122.5
                                ; LOE ebx esi
.B1.204:                        ; Preds .B1.203                 ; Infreq
        mov       DWORD PTR [400+esp], 0                        ;123.5
        lea       eax, DWORD PTR [248+esp]                      ;123.5
        mov       DWORD PTR [248+esp], 5                        ;123.5
        mov       DWORD PTR [252+esp], OFFSET FLAT: __STRLITPACK_28 ;123.5
        push      32                                            ;123.5
        push      eax                                           ;123.5
        push      OFFSET FLAT: __STRLITPACK_66.0.1              ;123.5
        push      -2088435968                                   ;123.5
        push      911                                           ;123.5
        push      ebx                                           ;123.5
        call      _for_write_seq_lis                            ;123.5
                                ; LOE ebx esi
.B1.277:                        ; Preds .B1.204                 ; Infreq
        add       esp, 120                                      ;123.5
                                ; LOE ebx esi
.B1.205:                        ; Preds .B1.277                 ; Infreq
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;123.5
        imul      edx, DWORD PTR [520+esp], 152                 ;123.5
        mov       DWORD PTR [536+esp], eax                      ;123.5
        imul      eax, eax, 152                                 ;123.5
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;123.5
        add       edx, edi                                      ;123.5
        sub       edx, eax                                      ;123.5
        mov       DWORD PTR [208+esp], edx                      ;123.5
        mov       DWORD PTR [192+esp], eax                      ;123.5
        imul      eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE+32], 44 ;123.5
        imul      edx, DWORD PTR [28+edx], 44                   ;123.26
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE] ;123.5
        add       edx, ecx                                      ;123.5
        sub       edx, eax                                      ;123.5
        mov       DWORD PTR [200+esp], ecx                      ;123.5
        mov       DWORD PTR [196+esp], eax                      ;123.5
        lea       eax, DWORD PTR [232+esp]                      ;123.5
        mov       ecx, DWORD PTR [36+edx]                       ;123.5
        mov       DWORD PTR [232+esp], ecx                      ;123.5
        push      eax                                           ;123.5
        push      OFFSET FLAT: __STRLITPACK_67.0.1              ;123.5
        push      ebx                                           ;123.5
        call      _for_write_seq_lis_xmit                       ;123.5
                                ; LOE ebx esi edi
.B1.206:                        ; Preds .B1.205                 ; Infreq
        mov       DWORD PTR [172+esp], 4                        ;123.5
        lea       eax, DWORD PTR [172+esp]                      ;123.5
        mov       DWORD PTR [176+esp], OFFSET FLAT: __STRLITPACK_27 ;123.5
        push      eax                                           ;123.5
        push      OFFSET FLAT: __STRLITPACK_68.0.1              ;123.5
        push      ebx                                           ;123.5
        call      _for_write_seq_lis_xmit                       ;123.5
                                ; LOE ebx esi edi
.B1.207:                        ; Preds .B1.206                 ; Infreq
        mov       eax, DWORD PTR [232+esp]                      ;123.111
        imul      edx, DWORD PTR [24+eax], 44                   ;123.111
        lea       eax, DWORD PTR [264+esp]                      ;123.5
        add       edx, DWORD PTR [224+esp]                      ;123.5
        sub       edx, DWORD PTR [220+esp]                      ;123.5
        mov       ecx, DWORD PTR [36+edx]                       ;123.5
        mov       DWORD PTR [264+esp], ecx                      ;123.5
        push      eax                                           ;123.5
        push      OFFSET FLAT: __STRLITPACK_69.0.1              ;123.5
        push      ebx                                           ;123.5
        call      _for_write_seq_lis_xmit                       ;123.5
                                ; LOE ebx esi edi
.B1.208:                        ; Preds .B1.207                 ; Infreq
        mov       DWORD PTR [204+esp], 9                        ;123.5
        lea       eax, DWORD PTR [204+esp]                      ;123.5
        mov       DWORD PTR [208+esp], OFFSET FLAT: __STRLITPACK_26 ;123.5
        push      eax                                           ;123.5
        push      OFFSET FLAT: __STRLITPACK_70.0.1              ;123.5
        push      ebx                                           ;123.5
        call      _for_write_seq_lis_xmit                       ;123.5
                                ; LOE ebx esi edi
.B1.209:                        ; Preds .B1.208                 ; Infreq
        imul      eax, DWORD PTR [252+esp], 152                 ;123.5
        add       eax, edi                                      ;123.5
        sub       eax, DWORD PTR [240+esp]                      ;123.5
        imul      edx, DWORD PTR [24+eax], 44                   ;123.202
        lea       eax, DWORD PTR [296+esp]                      ;123.5
        add       edx, DWORD PTR [248+esp]                      ;123.5
        sub       edx, DWORD PTR [244+esp]                      ;123.5
        mov       ecx, DWORD PTR [36+edx]                       ;123.5
        mov       DWORD PTR [296+esp], ecx                      ;123.5
        push      eax                                           ;123.5
        push      OFFSET FLAT: __STRLITPACK_71.0.1              ;123.5
        push      ebx                                           ;123.5
        call      _for_write_seq_lis_xmit                       ;123.5
                                ; LOE esi edi
.B1.210:                        ; Preds .B1.209                 ; Infreq
        push      32                                            ;124.5
        xor       eax, eax                                      ;124.5
        push      eax                                           ;124.5
        push      eax                                           ;124.5
        push      -2088435968                                   ;124.5
        push      eax                                           ;124.5
        push      OFFSET FLAT: __STRLITPACK_72                  ;124.5
        call      _for_stop_core                                ;124.5
                                ; LOE esi edi
.B1.278:                        ; Preds .B1.210                 ; Infreq
        add       esp, 84                                       ;124.5
        jmp       .B1.50        ; Prob 100%                     ;124.5
                                ; LOE esi edi
.B1.211:                        ; Preds .B1.46                  ; Infreq
        lea       eax, DWORD PTR [668+esp]                      ;114.25
        lea       edx, DWORD PTR [680+esp]                      ;114.25
        push      eax                                           ;114.25
        push      edx                                           ;114.25
        call      _DYNUST_NETWORK_MODULE_mp_MOVENOFORLINKLL     ;114.25
                                ; LOE eax
.B1.279:                        ; Preds .B1.211                 ; Infreq
        add       esp, 8                                        ;114.25
                                ; LOE eax
.B1.212:                        ; Preds .B1.279                 ; Infreq
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGOUT+44] ;115.36
        sub       eax, edx                                      ;115.16
        imul      eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGOUT+40] ;115.16
        mov       ecx, DWORD PTR [680+esp]                      ;115.36
        sub       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGOUT+32] ;115.16
        add       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGOUT] ;115.16
        inc       DWORD PTR [eax+ecx*4]                         ;115.16
        jmp       .B1.47        ; Prob 100%                     ;115.16
                                ; LOE
.B1.213:                        ; Preds .B1.126                 ; Infreq
        mov       DWORD PTR [124+esp], edi                      ;
        mov       ebx, DWORD PTR [228+esp]                      ;
        mov       DWORD PTR [236+esp], edi                      ;289.11
        mov       edi, -1                                       ;289.11
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1
.B1.214:                        ; Preds .B1.215 .B1.216 .B1.213 .B1.217 ; Infreq
        test      edi, 1                                        ;289.11
        mov       DWORD PTR [228+esp], ebx                      ;
        mov       ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_PCE+44] ;289.11
        mov       DWORD PTR [244+esp], ecx                      ;
        mov       edi, DWORD PTR [236+esp]                      ;289.11
        movsx     ecx, WORD PTR [862+edx+eax]                   ;289.11
        cmove     edi, esi                                      ;289.11
        neg       ebx                                           ;289.11
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_PCE+56] ;289.11
        add       ebx, ecx                                      ;289.11
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_PCE+32] ;289.11
        neg       esi                                           ;289.11
        neg       ecx                                           ;289.11
        add       esi, edi                                      ;289.11
        mov       DWORD PTR [236+esp], edi                      ;289.11
        movsx     edi, WORD PTR [860+edx+eax]                   ;289.11
        add       ecx, edi                                      ;289.11
        imul      ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_PCE+40] ;289.11
        imul      esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_PCE+52] ;289.11
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_PCE]   ;289.11
        lea       ecx, DWORD PTR [edi+ecx*4]                    ;289.11
        add       ebx, ecx                                      ;289.11
        mov       ecx, DWORD PTR [244+esp]                      ;289.11
        movss     xmm3, DWORD PTR [ebx+esi]                     ;289.11
        movss     DWORD PTR [856+edx+eax], xmm3                 ;289.11
        mov       ebx, DWORD PTR [228+esp]                      ;289.11
        jmp       .B1.128       ; Prob 100%                     ;289.11
                                ; LOE eax edx ecx ebx xmm0 xmm1 xmm3
.B1.215:                        ; Preds .B1.122                 ; Infreq
        mov       ebx, DWORD PTR [228+esp]                      ;
        xor       edi, edi                                      ;
        jmp       .B1.214       ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1
.B1.216:                        ; Preds .B1.120                 ; Infreq
        mov       DWORD PTR [236+esp], 1                        ;289.11
        mov       edi, -1                                       ;289.11
        jmp       .B1.214       ; Prob 100%                     ;289.11
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1
.B1.217:                        ; Preds .B1.119                 ; Infreq
        xor       edi, edi                                      ;
        jmp       .B1.214       ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1
.B1.218:                        ; Preds .B1.118                 ; Infreq
        movss     xmm3, DWORD PTR [_2il0floatpacket.7]          ;289.11
        mov       DWORD PTR [856+edx+eax], 1075838976           ;289.11
        jmp       .B1.128       ; Prob 100%                     ;289.11
                                ; LOE eax edx ecx ebx xmm0 xmm1 xmm3
.B1.220:                        ; Preds .B1.128 .B1.8           ; Infreq
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NSIGOPT] ;297.7
        test      eax, eax                                      ;297.18
        mov       DWORD PTR [16+esp], eax                       ;297.7
        jle       .B1.243       ; Prob 16%                      ;297.18
                                ; LOE ecx ebx
.B1.221:                        ; Preds .B1.220                 ; Infreq
        mov       eax, DWORD PTR [12+ebp]                       ;298.13
        movss     xmm2, DWORD PTR [eax]                         ;298.13
        mov       esi, DWORD PTR [eax]                          ;298.13
        comiss    xmm2, DWORD PTR [_2il0floatpacket.11]         ;298.18
        jbe       .B1.243       ; Prob 78%                      ;298.18
                                ; LOE ecx ebx esi xmm2
.B1.222:                        ; Preds .B1.221                 ; Infreq
        movss     xmm4, DWORD PTR [_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT] ;299.20
        divss     xmm2, xmm4                                    ;299.20
        movss     xmm6, DWORD PTR [_2il0floatpacket.13]         ;299.20
        andps     xmm6, xmm2                                    ;299.20
        pxor      xmm2, xmm6                                    ;299.20
        movss     xmm0, DWORD PTR [_2il0floatpacket.14]         ;299.20
        movaps    xmm1, xmm2                                    ;299.20
        movaps    xmm7, xmm2                                    ;299.20
        cmpltss   xmm1, xmm0                                    ;299.20
        andps     xmm1, xmm0                                    ;299.20
        addss     xmm7, xmm1                                    ;299.20
        subss     xmm7, xmm1                                    ;299.20
        movss     xmm1, DWORD PTR [_2il0floatpacket.15]         ;299.20
        movaps    xmm5, xmm7                                    ;299.20
        subss     xmm5, xmm2                                    ;299.20
        movaps    xmm2, xmm1                                    ;299.20
        addss     xmm2, xmm1                                    ;299.20
        movaps    xmm3, xmm5                                    ;299.20
        cmpless   xmm5, DWORD PTR [_2il0floatpacket.16]         ;299.20
        cmpnless  xmm3, xmm1                                    ;299.20
        andps     xmm3, xmm2                                    ;299.20
        andps     xmm5, xmm2                                    ;299.20
        subss     xmm7, xmm3                                    ;299.20
        movss     xmm3, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGOPTINT] ;299.42
        divss     xmm3, xmm4                                    ;299.42
        movss     xmm4, DWORD PTR [_2il0floatpacket.13]         ;299.42
        addss     xmm7, xmm5                                    ;299.20
        andps     xmm4, xmm3                                    ;299.42
        orps      xmm7, xmm6                                    ;299.20
        pxor      xmm3, xmm4                                    ;299.42
        movaps    xmm5, xmm3                                    ;299.42
        cvtss2si  eax, xmm7                                     ;299.20
        cmpltss   xmm5, xmm0                                    ;299.42
        cdq                                                     ;299.16
        andps     xmm0, xmm5                                    ;299.42
        movaps    xmm5, xmm3                                    ;299.42
        mov       DWORD PTR [esp], eax                          ;299.20
        addss     xmm5, xmm0                                    ;299.42
        subss     xmm5, xmm0                                    ;299.42
        movaps    xmm0, xmm5                                    ;299.42
        subss     xmm0, xmm3                                    ;299.42
        movaps    xmm6, xmm0                                    ;299.42
        cmpless   xmm0, DWORD PTR [_2il0floatpacket.16]         ;299.42
        cmpnless  xmm6, xmm1                                    ;299.42
        andps     xmm6, xmm2                                    ;299.42
        andps     xmm0, xmm2                                    ;299.42
        subss     xmm5, xmm6                                    ;299.42
        addss     xmm5, xmm0                                    ;299.42
        orps      xmm5, xmm4                                    ;299.42
        cvtss2si  edi, xmm5                                     ;299.42
        idiv      edi                                           ;299.16
        test      edx, edx                                      ;299.73
        jne       .B1.242       ; Prob 50%                      ;299.73
                                ; LOE ecx ebx esi
.B1.223:                        ; Preds .B1.242 .B1.222         ; Infreq
        mov       DWORD PTR [668+esp], ecx                      ;48.7
                                ; LOE ebx esi
.B1.224:                        ; Preds .B1.223                 ; Infreq
        mov       DWORD PTR [304+esp], 0                        ;300.15
        lea       edx, DWORD PTR [304+esp]                      ;300.15
        mov       DWORD PTR [esp], esi                          ;300.15
        lea       eax, DWORD PTR [esp]                          ;300.15
        push      32                                            ;300.15
        push      OFFSET FLAT: SIM_TRANSFER_VEHICLES$format_pack.0.1 ;300.15
        push      eax                                           ;300.15
        push      OFFSET FLAT: __STRLITPACK_86.0.1              ;300.15
        push      -2088435968                                   ;300.15
        push      514                                           ;300.15
        push      edx                                           ;300.15
        call      _for_write_seq_fmt                            ;300.15
                                ; LOE ebx
.B1.280:                        ; Preds .B1.224                 ; Infreq
        add       esp, 28                                       ;300.15
                                ; LOE ebx
.B1.225:                        ; Preds .B1.280                 ; Infreq
        imul      ebx, ebx, -900                                ;
        add       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;
        mov       DWORD PTR [44+esp], ebx                       ;
        imul      ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32], -152 ;
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGOUT+44] ;305.272
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGOUT+40] ;305.272
        add       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;
        imul      edi, ecx                                      ;
        mov       DWORD PTR [92+esp], ebx                       ;
        imul      ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE+32], -44 ;
        mov       DWORD PTR [88+esp], ecx                       ;305.272
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGOUT] ;305.22
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGOUT+32] ;
        sub       esi, edi                                      ;
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_BACKPOINTR+32] ;302.17
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGOPTNID+32] ;302.17
        shl       ecx, 2                                        ;
        shl       edx, 2                                        ;
        sub       esi, ecx                                      ;
        shl       eax, 2                                        ;
        neg       edx                                           ;
        mov       DWORD PTR [4+esp], 1                          ;
        neg       eax                                           ;
        add       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE] ;
        mov       DWORD PTR [20+esp], esi                       ;
        add       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_BACKPOINTR] ;
        add       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGOPTNID] ;
        mov       DWORD PTR [84+esp], ebx                       ;
        mov       esi, DWORD PTR [4+esp]                        ;
                                ; LOE eax edx esi
.B1.226:                        ; Preds .B1.240 .B1.225         ; Infreq
        mov       ebx, DWORD PTR [eax+esi*4]                    ;302.28
        mov       edi, DWORD PTR [4+edx+ebx*4]                  ;302.55
        dec       edi                                           ;302.17
        mov       ecx, DWORD PTR [edx+ebx*4]                    ;302.17
        imul      ebx, ecx, 152                                 ;
        cmp       edi, ecx                                      ;302.17
        jl        .B1.240       ; Prob 10%                      ;302.17
                                ; LOE eax edx ecx ebx esi edi
.B1.227:                        ; Preds .B1.226                 ; Infreq
        mov       DWORD PTR [40+esp], edi                       ;
        mov       DWORD PTR [36+esp], ecx                       ;
        mov       DWORD PTR [12+esp], eax                       ;
        mov       DWORD PTR [8+esp], edx                        ;
        mov       DWORD PTR [4+esp], esi                        ;
                                ; LOE ebx
.B1.228:                        ; Preds .B1.238 .B1.227         ; Infreq
        mov       eax, DWORD PTR [92+esp]                       ;303.20
        mov       edx, DWORD PTR [84+esp]                       ;303.20
        mov       DWORD PTR [304+esp], 0                        ;303.20
        mov       edi, DWORD PTR [16+ebx+eax]                   ;303.20
        imul      esi, edi, 152                                 ;303.20
        imul      ecx, DWORD PTR [28+eax+esi], 44               ;303.94
        mov       eax, DWORD PTR [36+edx+ecx]                   ;303.20
        mov       DWORD PTR [48+esp], eax                       ;303.20
        push      32                                            ;303.20
        push      OFFSET FLAT: SIM_TRANSFER_VEHICLES$format_pack.0.1+32 ;303.20
        lea       edx, DWORD PTR [56+esp]                       ;303.20
        push      edx                                           ;303.20
        push      OFFSET FLAT: __STRLITPACK_87.0.1              ;303.20
        push      -2088435968                                   ;303.20
        push      514                                           ;303.20
        lea       ecx, DWORD PTR [328+esp]                      ;303.20
        push      ecx                                           ;303.20
        call      _for_write_seq_fmt                            ;303.20
                                ; LOE ebx esi edi
.B1.229:                        ; Preds .B1.228                 ; Infreq
        mov       eax, DWORD PTR [120+esp]                      ;303.214
        mov       edx, DWORD PTR [112+esp]                      ;303.20
        imul      ecx, DWORD PTR [24+eax+esi], 44               ;303.214
        mov       eax, DWORD PTR [36+edx+ecx]                   ;303.20
        lea       edx, DWORD PTR [92+esp]                       ;303.20
        mov       DWORD PTR [92+esp], eax                       ;303.20
        push      edx                                           ;303.20
        push      OFFSET FLAT: __STRLITPACK_88.0.1              ;303.20
        lea       ecx, DWORD PTR [340+esp]                      ;303.20
        push      ecx                                           ;303.20
        call      _for_write_seq_fmt_xmit                       ;303.20
                                ; LOE ebx esi edi
.B1.230:                        ; Preds .B1.229                 ; Infreq
        imul      edi, edi, 900                                 ;303.335
        lea       ecx, DWORD PTR [112+esp]                      ;303.20
        mov       eax, DWORD PTR [84+esp]                       ;303.20
        mov       edx, DWORD PTR [688+eax+edi]                  ;303.20
        mov       DWORD PTR [112+esp], edx                      ;303.20
        push      ecx                                           ;303.20
        push      OFFSET FLAT: __STRLITPACK_89.0.1              ;303.20
        lea       eax, DWORD PTR [352+esp]                      ;303.20
        push      eax                                           ;303.20
        call      _for_write_seq_fmt_xmit                       ;303.20
                                ; LOE ebx esi edi
.B1.231:                        ; Preds .B1.230                 ; Infreq
        mov       eax, DWORD PTR [96+esp]                       ;303.20
        lea       ecx, DWORD PTR [132+esp]                      ;303.20
        mov       edx, DWORD PTR [648+eax+edi]                  ;303.20
        mov       DWORD PTR [132+esp], edx                      ;303.20
        push      ecx                                           ;303.20
        push      OFFSET FLAT: __STRLITPACK_90.0.1              ;303.20
        lea       edi, DWORD PTR [364+esp]                      ;303.20
        push      edi                                           ;303.20
        call      _for_write_seq_fmt_xmit                       ;303.20
                                ; LOE ebx esi
.B1.281:                        ; Preds .B1.231                 ; Infreq
        add       esp, 64                                       ;303.20
                                ; LOE ebx esi
.B1.232:                        ; Preds .B1.281                 ; Infreq
        mov       eax, DWORD PTR [92+esp]                       ;304.32
        movsx     edx, BYTE PTR [32+eax+esi]                    ;304.32
        test      edx, edx                                      ;304.20
        mov       DWORD PTR [76+esp], edx                       ;304.32
        jle       .B1.238       ; Prob 2%                       ;304.20
                                ; LOE eax ebx al ah
.B1.233:                        ; Preds .B1.232                 ; Infreq
        mov       esi, eax                                      ;305.22
        mov       DWORD PTR [28+esp], 1                         ;
        mov       DWORD PTR [24+esp], ebx                       ;
        mov       edx, DWORD PTR [16+ebx+esi]                   ;305.22
        imul      eax, edx, 152                                 ;305.22
        mov       edi, DWORD PTR [36+eax+esi]                   ;305.22
        mov       ecx, DWORD PTR [68+eax+esi]                   ;305.22
        mov       eax, DWORD PTR [64+eax+esi]                   ;305.22
        imul      ecx, eax                                      ;
        sub       edi, ecx                                      ;
        mov       ecx, DWORD PTR [20+esp]                       ;
        mov       DWORD PTR [32+esp], eax                       ;
        mov       DWORD PTR [68+esp], edi                       ;
        mov       DWORD PTR [60+esp], eax                       ;
        lea       esi, DWORD PTR [ecx+edx*4]                    ;
        add       esi, DWORD PTR [88+esp]                       ;
        mov       ebx, DWORD PTR [32+esp]                       ;
        mov       edi, DWORD PTR [28+esp]                       ;
                                ; LOE ebx esi edi
.B1.234:                        ; Preds .B1.236 .B1.233         ; Infreq
        mov       eax, DWORD PTR [68+esp]                       ;305.22
        mov       edx, DWORD PTR [92+esp]                       ;305.113
        mov       DWORD PTR [304+esp], 0                        ;305.22
        imul      ecx, DWORD PTR [ebx+eax], 152                 ;305.22
        imul      edx, DWORD PTR [24+edx+ecx], 44               ;305.113
        mov       eax, DWORD PTR [84+esp]                       ;305.22
        mov       ecx, DWORD PTR [36+eax+edx]                   ;305.22
        mov       DWORD PTR [96+esp], ecx                       ;305.22
        push      32                                            ;305.22
        push      OFFSET FLAT: SIM_TRANSFER_VEHICLES$format_pack.0.1+136 ;305.22
        lea       eax, DWORD PTR [104+esp]                      ;305.22
        push      eax                                           ;305.22
        push      OFFSET FLAT: __STRLITPACK_91.0.1              ;305.22
        push      -2088435968                                   ;305.22
        push      514                                           ;305.22
        lea       edx, DWORD PTR [328+esp]                      ;305.22
        push      edx                                           ;305.22
        call      _for_write_seq_fmt                            ;305.22
                                ; LOE ebx esi edi
.B1.235:                        ; Preds .B1.234                 ; Infreq
        mov       eax, DWORD PTR [esi]                          ;305.22
        lea       edx, DWORD PTR [132+esp]                      ;305.22
        mov       DWORD PTR [132+esp], eax                      ;305.22
        push      edx                                           ;305.22
        push      OFFSET FLAT: __STRLITPACK_92.0.1              ;305.22
        lea       ecx, DWORD PTR [340+esp]                      ;305.22
        push      ecx                                           ;305.22
        call      _for_write_seq_fmt_xmit                       ;305.22
                                ; LOE ebx esi edi
.B1.282:                        ; Preds .B1.235                 ; Infreq
        add       esp, 40                                       ;305.22
                                ; LOE ebx esi edi
.B1.236:                        ; Preds .B1.282                 ; Infreq
        inc       edi                                           ;307.20
        mov       DWORD PTR [esi], 0                            ;306.22
        add       ebx, DWORD PTR [60+esp]                       ;307.20
        add       esi, DWORD PTR [88+esp]                       ;307.20
        cmp       edi, DWORD PTR [76+esp]                       ;307.20
        jle       .B1.234       ; Prob 82%                      ;307.20
                                ; LOE ebx esi edi
.B1.237:                        ; Preds .B1.236                 ; Infreq
        mov       ebx, DWORD PTR [24+esp]                       ;
                                ; LOE ebx
.B1.238:                        ; Preds .B1.237 .B1.232         ; Infreq
        mov       eax, DWORD PTR [36+esp]                       ;308.17
        add       ebx, 152                                      ;308.17
        inc       eax                                           ;308.17
        mov       DWORD PTR [36+esp], eax                       ;308.17
        cmp       eax, DWORD PTR [40+esp]                       ;308.17
        jle       .B1.228       ; Prob 82%                      ;308.17
                                ; LOE ebx
.B1.239:                        ; Preds .B1.238                 ; Infreq
        mov       eax, DWORD PTR [12+esp]                       ;
        mov       edx, DWORD PTR [8+esp]                        ;
        mov       esi, DWORD PTR [4+esp]                        ;
                                ; LOE eax edx esi
.B1.240:                        ; Preds .B1.239 .B1.226         ; Infreq
        inc       esi                                           ;309.15
        cmp       esi, DWORD PTR [16+esp]                       ;309.15
        jle       .B1.226       ; Prob 82%                      ;309.15
        jmp       .B1.243       ; Prob 100%                     ;309.15
                                ; LOE eax edx esi
.B1.242:                        ; Preds .B1.222                 ; Infreq
        mov       edi, DWORD PTR [20+ebp]                       ;1.18
        mov       eax, DWORD PTR [esp]                          ;299.85
        sub       eax, DWORD PTR [edi]                          ;299.85
        cdq                                                     ;299.81
        xor       eax, edx                                      ;299.81
        sub       eax, edx                                      ;299.81
        cmp       eax, 1                                        ;299.115
        jle       .B1.223       ; Prob 16%                      ;299.115
                                ; LOE ecx ebx esi
.B1.243:                        ; Preds .B1.240 .B1.242 .B1.221 .B1.220 ; Infreq
        add       esp, 692                                      ;314.1
        pop       ebx                                           ;314.1
        pop       edi                                           ;314.1
        pop       esi                                           ;314.1
        mov       esp, ebp                                      ;314.1
        pop       ebp                                           ;314.1
        ret                                                     ;314.1
                                ; LOE
.B1.245:                        ; Preds .B1.105                 ; Infreq
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;289.11
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;289.11
        mov       DWORD PTR [612+esp], eax                      ;289.11
        jmp       .B1.116       ; Prob 100%                     ;289.11
                                ; LOE edx ecx ebx esi
.B1.246:                        ; Preds .B1.18                  ; Infreq
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;91.10
        mov       edi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;91.10
        mov       DWORD PTR [632+esp], edx                      ;77.13
        mov       esi, DWORD PTR [676+esp]                      ;91.62
        mov       DWORD PTR [652+esp], eax                      ;91.10
        jmp       .B1.38        ; Prob 100%                     ;91.10
        ALIGN     16
                                ; LOE esi edi
; mark_end;
_SIM_TRANSFER_VEHICLES ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
SIM_TRANSFER_VEHICLES$format_pack.0.1	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	7
	DB	0
	DB	84
	DB	105
	DB	109
	DB	101
	DB	61
	DB	62
	DB	32
	DB	0
	DB	33
	DB	0
	DB	0
	DB	1
	DB	1
	DB	0
	DB	0
	DB	0
	DB	6
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	5
	DB	0
	DB	70
	DB	114
	DB	111
	DB	109
	DB	32
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	4
	DB	0
	DB	32
	DB	84
	DB	111
	DB	32
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	14
	DB	0
	DB	32
	DB	114
	DB	101
	DB	109
	DB	97
	DB	105
	DB	110
	DB	105
	DB	110
	DB	103
	DB	32
	DB	80
	DB	67
	DB	69
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	1
	DB	1
	DB	0
	DB	0
	DB	0
	DB	6
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	3
	DB	0
	DB	32
	DB	75
	DB	32
	DB	0
	DB	33
	DB	0
	DB	0
	DB	1
	DB	1
	DB	0
	DB	0
	DB	0
	DB	6
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	53
	DB	0
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	84
	DB	111
	DB	32
	DB	78
	DB	111
	DB	100
	DB	101
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	10
	DB	0
	DB	32
	DB	79
	DB	117
	DB	116
	DB	102
	DB	108
	DB	111
	DB	119
	DB	32
	DB	78
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	6
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
_DATA	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
__NLITPACK_0.0.1	DD	27
__NLITPACK_1.0.1	DD	1
__NLITPACK_2.0.1	DD	2
__STRLITPACK_44.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_45.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_46.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_47.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_48.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_49.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_50.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_51.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_52.0.1	DB	7
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_53.0.1	DB	5
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_54.0.1	DB	5
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_55.0.1	DB	5
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_56.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_57.0.1	DB	7
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_58.0.1	DB	5
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_59.0.1	DB	5
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__NLITPACK_3.0.1	DD	28
__STRLITPACK_61.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_62.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_63.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_64.0.1	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_65.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_66.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_67.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_68.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_69.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_70.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_71.0.1	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_73.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_74.0.1	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_75.0.1	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_76.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__NLITPACK_4.0.1	DD	3
__STRLITPACK_78.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_79.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_80.0.1	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_81.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_82.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_83.0.1	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_84.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_86.0.1	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_87.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_88.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_89.0.1	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_90.0.1	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_91.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_92.0.1	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _SIM_TRANSFER_VEHICLES
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _LOOKUP_TRUCK_PCE
; mark_begin;
       ALIGN     16
	PUBLIC _LOOKUP_TRUCK_PCE
_LOOKUP_TRUCK_PCE	PROC NEAR 
; parameter 1: 16 + esp
.B2.1:                          ; Preds .B2.0
        push      esi                                           ;317.12
        push      ebx                                           ;317.12
        push      ebp                                           ;317.12
        mov       eax, DWORD PTR [16+esp]                       ;317.12
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;331.52
        neg       edx                                           ;
        mov       ecx, DWORD PTR [eax]                          ;331.5
        add       edx, ecx                                      ;
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;331.5
        neg       eax                                           ;331.47
        add       eax, ecx                                      ;331.47
        imul      ecx, eax, 152                                 ;331.47
        imul      ebp, edx, 900                                 ;
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;331.2
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;331.43
        movsx     esi, WORD PTR [148+edx+ecx]                   ;331.5
        cmp       esi, 99                                       ;331.43
        jge       .B2.12        ; Prob 50%                      ;331.43
                                ; LOE ebx ebp edi
.B2.2:                          ; Preds .B2.1
        mov       eax, DWORD PTR [640+ebx+ebp]                  ;331.52
        test      eax, eax                                      ;331.84
        jle       .B2.12        ; Prob 16%                      ;331.84
                                ; LOE eax ebx ebp edi
.B2.3:                          ; Preds .B2.2
        mov       edx, DWORD PTR [644+ebx+ebp]                  ;332.47
        test      edx, edx                                      ;333.46
        cvtsi2ss  xmm1, edx                                     ;332.47
        cvtsi2ss  xmm0, eax                                     ;332.88
        divss     xmm1, xmm0                                    ;332.9
        movss     DWORD PTR [412+ebx+ebp], xmm1                 ;332.9
        jle       .B2.12        ; Prob 16%                      ;333.46
                                ; LOE ebx ebp edi xmm1
.B2.4:                          ; Preds .B2.3
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRUCKNUM] ;334.8
        test      ecx, ecx                                      ;334.8
        jle       .B2.16        ; Prob 2%                       ;334.8
                                ; LOE ecx ebx ebp edi xmm1
.B2.5:                          ; Preds .B2.4
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRUCKBPNT+32] ;336.19
        mov       eax, 1                                        ;
        shl       edx, 2                                        ;
        neg       edx                                           ;
        add       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRUCKBPNT] ;
        movss     xmm0, DWORD PTR [_2il0floatpacket.19]         ;336.70
        cvtsi2ss  xmm2, DWORD PTR [4+edx]                       ;336.58
        divss     xmm2, xmm0                                    ;336.70
        comiss    xmm2, xmm1                                    ;336.55
        jae       .B2.17        ; Prob 20%                      ;336.55
                                ; LOE eax edx ecx ebx ebp edi xmm0 xmm1
.B2.7:                          ; Preds .B2.5 .B2.8 .B2.11 .B2.10 .B2.9
                                ;      
        inc       eax                                           ;348.8
        cmp       eax, ecx                                      ;348.8
        jg        .B2.16        ; Prob 18%                      ;348.8
                                ; LOE eax edx ecx ebx ebp edi xmm0 xmm1
.B2.8:                          ; Preds .B2.7
        cmp       eax, 1                                        ;335.20
        je        .B2.7         ; Prob 16%                      ;335.20
                                ; LOE eax edx ecx ebx ebp edi xmm0 xmm1
.B2.9:                          ; Preds .B2.8
        jle       .B2.7         ; Prob 24%                      ;341.15
                                ; LOE eax edx ecx ebx ebp edi xmm0 xmm1
.B2.10:                         ; Preds .B2.9
        cvtsi2ss  xmm2, DWORD PTR [edx+eax*4]                   ;342.49
        divss     xmm2, xmm0                                    ;342.62
        comiss    xmm2, xmm1                                    ;342.46
        jb        .B2.7         ; Prob 50%                      ;342.46
                                ; LOE eax edx ecx ebx ebp edi xmm0 xmm1
.B2.11:                         ; Preds .B2.10
        cvtsi2ss  xmm2, DWORD PTR [-4+edx+eax*4]                ;342.113
        divss     xmm2, xmm0                                    ;342.128
        comiss    xmm1, xmm2                                    ;342.111
        jbe       .B2.7         ; Prob 80%                      ;342.111
        jmp       .B2.14        ; Prob 100%                     ;342.111
                                ; LOE eax edx ecx ebx ebp edi xmm0 xmm1
.B2.12:                         ; Preds .B2.3 .B2.1 .B2.2
        mov       DWORD PTR [856+ebx+ebp], 1075838976           ;355.9
                                ; LOE edi
.B2.13:                         ; Preds .B2.12
        pop       ebp                                           ;359.1
        pop       ebx                                           ;359.1
        pop       esi                                           ;359.1
        ret                                                     ;359.1
                                ; LOE
.B2.14:                         ; Preds .B2.11                  ; Infreq
        mov       edx, -1                                       ;344.16
                                ; LOE eax edx ecx ebx ebp edi
.B2.15:                         ; Preds .B2.16 .B2.17 .B2.14    ; Infreq
        test      dl, 1                                         ;349.16
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_PCE+44] ;350.47
        cmove     eax, ecx                                      ;349.23
        neg       edx                                           ;350.11
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_PCE+56] ;350.47
        neg       ecx                                           ;350.11
        add       ecx, eax                                      ;350.11
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_PCE+32] ;350.11
        movsx     esi, WORD PTR [862+ebx+ebp]                   ;350.11
        neg       eax                                           ;350.11
        add       edx, esi                                      ;350.11
        movsx     esi, WORD PTR [860+ebx+ebp]                   ;350.11
        add       eax, esi                                      ;350.11
        imul      edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_PCE+40] ;350.11
        imul      ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_PCE+52] ;350.11
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_PCE]   ;350.11
        lea       eax, DWORD PTR [esi+eax*4]                    ;350.11
        add       edx, eax                                      ;350.11
        mov       edx, DWORD PTR [edx+ecx]                      ;350.11
        mov       DWORD PTR [856+ebx+ebp], edx                  ;350.11
        pop       ebp                                           ;350.11
        pop       ebx                                           ;350.11
        pop       esi                                           ;350.11
        ret                                                     ;350.11
                                ; LOE edi
.B2.16:                         ; Preds .B2.4 .B2.7             ; Infreq
        xor       edx, edx                                      ;
        xor       eax, eax                                      ;
        jmp       .B2.15        ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx ebp edi
.B2.17:                         ; Preds .B2.5                   ; Infreq
        mov       eax, 1                                        ;337.16
        mov       edx, -1                                       ;338.16
        jmp       .B2.15        ; Prob 100%                     ;338.16
        ALIGN     16
                                ; LOE eax edx ecx ebx ebp edi
; mark_end;
_LOOKUP_TRUCK_PCE ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _LOOKUP_TRUCK_PCE
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DD 1 DUP (0H)	; pad
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_40	DB	74
	DB	32
	DB	73
	DB	32
	DB	109
	DB	95
	DB	100
	DB	121
	DB	110
	DB	117
	DB	115
	DB	116
	DB	95
	DB	110
	DB	101
	DB	116
	DB	119
	DB	111
	DB	114
	DB	107
	DB	95
	DB	97
	DB	114
	DB	99
	DB	95
	DB	110
	DB	100
	DB	101
	DB	40
	DB	105
	DB	41
	DB	37
	DB	105
	DB	100
	DB	110
	DB	111
	DB	100
	DB	32
	DB	78
	DB	76
	DB	32
	DB	109
	DB	95
	DB	100
	DB	121
	DB	110
	DB	117
	DB	115
	DB	116
	DB	95
	DB	110
	DB	101
	DB	116
	DB	119
	DB	111
	DB	114
	DB	107
	DB	95
	DB	97
	DB	114
	DB	99
	DB	95
	DB	110
	DB	100
	DB	101
	DB	40
	DB	110
	DB	108
	DB	41
	DB	37
	DB	105
	DB	117
	DB	110
	DB	111
	DB	100
	DB	0
__STRLITPACK_42	DB	69
	DB	82
	DB	82
	DB	79
	DB	82
	DB	58
	DB	32
	DB	32
	DB	32
	DB	110
	DB	101
	DB	120
	DB	116
	DB	32
	DB	108
	DB	105
	DB	110
	DB	107
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_38	DB	105
	DB	99
	DB	117
	DB	32
	DB	105
	DB	110
	DB	102
	DB	111
	DB	32
	DB	118
	DB	101
	DB	104
	DB	99
	DB	108
	DB	97
	DB	115
	DB	115
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_60	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_36	DB	109
	DB	111
	DB	118
	DB	101
	DB	116
	DB	117
	DB	114
	DB	110
	DB	32
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	118
	DB	101
	DB	104
	DB	105
	DB	99
	DB	108
	DB	101
	DB	32
	DB	116
	DB	114
	DB	97
	DB	110
	DB	115
	DB	102
	DB	101
	DB	114
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_34	DB	102
	DB	114
	DB	111
	DB	109
	DB	32
	DB	108
	DB	105
	DB	110
	DB	107
	DB	32
	DB	116
	DB	111
	DB	32
	DB	108
	DB	105
	DB	110
	DB	107
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_32	DB	99
	DB	104
	DB	101
	DB	99
	DB	107
	DB	32
	DB	109
	DB	111
	DB	118
	DB	101
	DB	109
	DB	101
	DB	110
	DB	116
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_28	DB	102
	DB	114
	DB	111
	DB	109
	DB	32
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_27	DB	32
	DB	116
	DB	111
	DB	32
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_26	DB	32
	DB	109
	DB	105
	DB	115
	DB	115
	DB	105
	DB	110
	DB	103
	DB	32
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_72	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_24	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	118
	DB	101
	DB	104
	DB	32
	DB	116
	DB	114
	DB	97
	DB	110
	DB	115
	DB	102
	DB	101
	DB	114
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_22	DB	80
	DB	108
	DB	101
	DB	97
	DB	115
	DB	101
	DB	32
	DB	99
	DB	111
	DB	110
	DB	116
	DB	97
	DB	99
	DB	116
	DB	32
	DB	100
	DB	101
	DB	118
	DB	101
	DB	108
	DB	111
	DB	112
	DB	101
	DB	114
	DB	115
	DB	32
	DB	116
	DB	111
	DB	32
	DB	114
	DB	101
	DB	115
	DB	111
	DB	108
	DB	118
	DB	101
	DB	32
	DB	116
	DB	104
	DB	105
	DB	115
	DB	32
	DB	105
	DB	115
	DB	115
	DB	117
	DB	101
	DB	0
__STRLITPACK_77	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_20	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	118
	DB	101
	DB	104
	DB	32
	DB	116
	DB	114
	DB	97
	DB	110
	DB	115
	DB	101
	DB	114
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_18	DB	116
	DB	108
	DB	101
	DB	102
	DB	116
	DB	32
	DB	105
	DB	110
	DB	99
	DB	111
	DB	114
	DB	114
	DB	101
	DB	99
	DB	116
	DB	0
__STRLITPACK_17	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	118
	DB	101
	DB	104
	DB	32
	DB	116
	DB	114
	DB	97
	DB	110
	DB	115
	DB	102
	DB	101
	DB	114
	DB	32
	DB	102
	DB	111
	DB	114
	DB	32
	DB	120
	DB	112
	DB	97
	DB	114
	DB	32
	DB	117
	DB	112
	DB	100
	DB	97
	DB	116
	DB	101
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_15	DB	78
	DB	101
	DB	103
	DB	97
	DB	116
	DB	105
	DB	118
	DB	101
	DB	32
	DB	118
	DB	111
	DB	108
	DB	117
	DB	109
	DB	101
	DB	32
	DB	111
	DB	110
	DB	32
	DB	108
	DB	105
	DB	110
	DB	107
	DB	0
__STRLITPACK_13	DB	80
	DB	108
	DB	101
	DB	97
	DB	115
	DB	101
	DB	32
	DB	99
	DB	111
	DB	110
	DB	116
	DB	97
	DB	99
	DB	116
	DB	32
	DB	100
	DB	101
	DB	118
	DB	101
	DB	108
	DB	111
	DB	112
	DB	101
	DB	114
	DB	115
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_85	DB	0
	DB 3 DUP ( 0H)	; pad
_2il0floatpacket.6	DD	040c00000H
_2il0floatpacket.7	DD	040200000H
_2il0floatpacket.8	DD	042c80000H
_2il0floatpacket.9	DD	038d1b717H
_2il0floatpacket.10	DD	042700000H
_2il0floatpacket.11	DD	03c23d70aH
_2il0floatpacket.12	DD	03f800000H
_2il0floatpacket.13	DD	080000000H
_2il0floatpacket.14	DD	04b000000H
_2il0floatpacket.15	DD	03f000000H
_2il0floatpacket.16	DD	0bf000000H
_2il0floatpacket.18	DD	040200000H
_2il0floatpacket.19	DD	042c80000H
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
EXTRN	_DYNUST_MAIN_MODULE_mp_SIGOPTNID:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_SIGOPTINT:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_GUITOTALTIME:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_AGGINT:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_MASTERDEST:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_RUNMODE:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_BACKPOINTR:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_MAXMOVE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_SIGOUT:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NSIGOPT:BYTE
EXTRN	_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE:BYTE
EXTRN	_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE:BYTE
EXTRN	_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH:BYTE
EXTRN	_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_DESTINATION:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NOOFARCS:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_SIMPERAGG:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_PCE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TRUCKBPNT:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TRUCKNUM:BYTE
_DATA	ENDS
EXTRN	_for_write_seq_fmt_xmit:PROC
EXTRN	_for_write_seq_fmt:PROC
EXTRN	_for_allocate:PROC
EXTRN	_for_check_mult_overflow:PROC
EXTRN	_for_stop_core:PROC
EXTRN	_for_write_seq_lis_xmit:PROC
EXTRN	_for_write_seq_lis:PROC
EXTRN	_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_VALUE:PROC
EXTRN	_DYNUST_NETWORK_MODULE_mp_MOVENOFORLINKLL:PROC
EXTRN	_DYNUST_NETWORK_MODULE_mp_GETBSTMOVE:PROC
EXTRN	_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_INSERT:PROC
EXTRN	_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_VALUE:PROC
EXTRN	_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST_REMOVE:PROC
EXTRN	_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST_INSERT:PROC
EXTRN	_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_P_SIZE:PROC
EXTRN	_TRANSLINK_ADD_MODULE_mp_TRANLINK_CONSOLIDATE:PROC
EXTRN	_RANXY:PROC
EXTRN	_RETRIEVE_NEXT_LINK:PROC
EXTRN	__fltused:BYTE
	INCLUDELIB <ifconsol>
	INCLUDELIB <libifcoremt>
	INCLUDELIB <libifport>
	INCLUDELIB <libmmt>
	INCLUDELIB <LIBCMT>
	INCLUDELIB <libirc>
	INCLUDELIB <svml_dispmt>
	INCLUDELIB <OLDNAMES>
	END
