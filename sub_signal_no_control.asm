; mark_description "Intel(R) Visual Fortran Compiler XE for applications running on IA-32, Version 13.1.1.171 Build 20130313";
; mark_description "-S";
	.686P
 	.387
	OPTION DOTNAME
	ASSUME	CS:FLAT,DS:FLAT,SS:FLAT
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
TXTST0:
; -- Begin  _SIGNAL_NO_CONTROL
; mark_begin;
IF @Version GE 800
  .MMX
ELSEIF @Version GE 612
  .MMX
  MMWORD TEXTEQU <QWORD>
ENDIF
IF @Version GE 800
  .XMM
ELSEIF @Version GE 614
  .XMM
  XMMWORD TEXTEQU <OWORD>
ENDIF
       ALIGN     16
	PUBLIC _SIGNAL_NO_CONTROL
_SIGNAL_NO_CONTROL	PROC NEAR 
; parameter 1: 68 + esp
.B1.1:                          ; Preds .B1.0
        push      esi                                           ;1.18
        push      edi                                           ;1.18
        push      ebx                                           ;1.18
        push      ebp                                           ;1.18
        sub       esp, 48                                       ;1.18
        mov       edx, DWORD PTR [68+esp]                       ;1.18
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_BACKPOINTR] ;28.7
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_BACKPOINTR+32] ;28.7
        mov       ecx, DWORD PTR [edx]                          ;27.7
        shl       ebx, 2                                        ;28.7
        lea       eax, DWORD PTR [eax+ecx*4]                    ;28.7
        mov       edx, eax                                      ;28.7
        sub       edx, ebx                                      ;28.7
        neg       ebx                                           ;30.10
        mov       esi, DWORD PTR [4+ebx+eax]                    ;29.10
        dec       esi                                           ;29.7
        mov       ebp, esi                                      ;30.10
        mov       edx, DWORD PTR [edx]                          ;28.7
        sub       ebp, edx                                      ;30.10
        inc       ebp                                           ;30.10
        mov       DWORD PTR [40+esp], ebp                       ;30.10
        cmp       esi, edx                                      ;30.10
        jl        .B1.12        ; Prob 10%                      ;30.10
                                ; LOE edx
.B1.2:                          ; Preds .B1.1
        imul      edx, edx, 152                                 ;
        xor       eax, eax                                      ;
        imul      ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32], 152 ;
        movss     xmm0, DWORD PTR [_2il0floatpacket.1]          ;32.99
        mulss     xmm0, DWORD PTR [_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT] ;32.99
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+40] ;32.7
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+44] ;32.7
        imul      edi, esi                                      ;
        mov       ebp, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;31.4
        mov       ecx, ebp                                      ;
        add       edx, ebp                                      ;
        sub       ecx, ebx                                      ;
        sub       edx, ebx                                      ;
        mov       DWORD PTR [32+esp], ecx                       ;
        mov       DWORD PTR [36+esp], edx                       ;
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE] ;
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+32] ;
        sub       ecx, edi                                      ;
        mov       ebp, ecx                                      ;
        mov       DWORD PTR [44+esp], eax                       ;
        cvttss2si edi, xmm0                                     ;32.7
        lea       ebx, DWORD PTR [edx*4]                        ;
        shl       edx, 5                                        ;
        sub       edx, ebx                                      ;
        lea       ebx, DWORD PTR [ecx+esi]                      ;
        sub       ebp, edx                                      ;
        lea       ecx, DWORD PTR [ecx+esi*2]                    ;
        sub       ebx, edx                                      ;
        sub       ecx, edx                                      ;
        mov       DWORD PTR [8+esp], ecx                        ;
        mov       DWORD PTR [4+esp], ebx                        ;
        mov       DWORD PTR [esp], ebp                          ;
                                ; LOE eax esi edi
.B1.3:                          ; Preds .B1.10 .B1.2
        mov       edx, DWORD PTR [36+esp]                       ;31.14
        mov       ebx, DWORD PTR [32+esp]                       ;31.14
        mov       ecx, DWORD PTR [16+eax+edx]                   ;31.14
        imul      ebp, ecx, 152                                 ;31.14
        movsx     edx, BYTE PTR [32+ebx+ebp]                    ;31.14
        test      edx, edx                                      ;31.7
        mov       DWORD PTR [28+esp], ecx                       ;31.14
        jle       .B1.10        ; Prob 50%                      ;31.7
                                ; LOE eax edx esi edi
.B1.4:                          ; Preds .B1.3
        mov       ecx, edx                                      ;31.7
        shr       ecx, 31                                       ;31.7
        add       ecx, edx                                      ;31.7
        sar       ecx, 1                                        ;31.7
        test      ecx, ecx                                      ;31.7
        jbe       .B1.13        ; Prob 3%                       ;31.7
                                ; LOE eax edx ecx esi edi
.B1.5:                          ; Preds .B1.4
        mov       ebp, DWORD PTR [36+esp]                       ;32.7
        xor       ebx, ebx                                      ;
        mov       DWORD PTR [24+esp], ecx                       ;
        mov       ecx, DWORD PTR [16+eax+ebp]                   ;32.7
        mov       DWORD PTR [16+esp], edx                       ;
        mov       DWORD PTR [12+esp], eax                       ;
        mov       eax, ebx                                      ;
        lea       ebp, DWORD PTR [ecx*4]                        ;32.7
        shl       ecx, 5                                        ;32.7
        sub       ecx, ebp                                      ;32.7
        mov       ebp, DWORD PTR [4+esp]                        ;
        add       ebp, ecx                                      ;
        add       ecx, DWORD PTR [8+esp]                        ;
        mov       DWORD PTR [20+esp], ecx                       ;
        mov       ecx, DWORD PTR [24+esp]                       ;
        mov       edx, DWORD PTR [20+esp]                       ;
                                ; LOE eax edx ecx ebx ebp esi edi
.B1.6:                          ; Preds .B1.6 .B1.5
        inc       ebx                                           ;31.7
        mov       DWORD PTR [4+ebp+eax*2], edi                  ;32.7
        mov       DWORD PTR [4+edx+eax*2], edi                  ;32.7
        add       eax, esi                                      ;31.7
        cmp       ebx, ecx                                      ;31.7
        jb        .B1.6         ; Prob 63%                      ;31.7
                                ; LOE eax edx ecx ebx ebp esi edi
.B1.7:                          ; Preds .B1.6
        mov       edx, DWORD PTR [16+esp]                       ;
        lea       ecx, DWORD PTR [1+ebx+ebx]                    ;31.7
        mov       eax, DWORD PTR [12+esp]                       ;
                                ; LOE eax edx ecx esi edi
.B1.8:                          ; Preds .B1.7 .B1.13
        lea       ebx, DWORD PTR [-1+ecx]                       ;31.7
        cmp       edx, ebx                                      ;31.7
        jbe       .B1.10        ; Prob 3%                       ;31.7
                                ; LOE eax ecx esi edi
.B1.9:                          ; Preds .B1.8
        mov       ebx, DWORD PTR [28+esp]                       ;32.7
        imul      ecx, esi                                      ;32.7
        add       ecx, DWORD PTR [esp]                          ;32.7
        lea       edx, DWORD PTR [ebx*4]                        ;32.7
        shl       ebx, 5                                        ;32.7
        sub       ebx, edx                                      ;32.7
        mov       DWORD PTR [4+ecx+ebx], edi                    ;32.7
                                ; LOE eax esi edi
.B1.10:                         ; Preds .B1.3 .B1.8 .B1.9
        mov       edx, DWORD PTR [44+esp]                       ;30.10
        add       eax, 152                                      ;30.10
        inc       edx                                           ;30.10
        mov       DWORD PTR [44+esp], edx                       ;30.10
        cmp       edx, DWORD PTR [40+esp]                       ;30.10
        jb        .B1.3         ; Prob 82%                      ;30.10
                                ; LOE eax esi edi
.B1.12:                         ; Preds .B1.10 .B1.1
        add       esp, 48                                       ;36.1
        pop       ebp                                           ;36.1
        pop       ebx                                           ;36.1
        pop       edi                                           ;36.1
        pop       esi                                           ;36.1
        ret                                                     ;36.1
                                ; LOE
.B1.13:                         ; Preds .B1.4                   ; Infreq
        mov       ecx, 1                                        ;
        jmp       .B1.8         ; Prob 100%                     ;
        ALIGN     16
                                ; LOE eax edx ecx esi edi
; mark_end;
_SIGNAL_NO_CONTROL ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _SIGNAL_NO_CONTROL
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
_2il0floatpacket.1	DD	042700000H
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
EXTRN	_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_BACKPOINTR:BYTE
_DATA	ENDS
EXTRN	__fltused:BYTE
	INCLUDELIB <ifconsol>
	INCLUDELIB <libifcoremt>
	INCLUDELIB <libifport>
	INCLUDELIB <libmmt>
	INCLUDELIB <LIBCMT>
	INCLUDELIB <libirc>
	INCLUDELIB <svml_dispmt>
	INCLUDELIB <OLDNAMES>
	END
