SUBROUTINE CAL_LINK_DISCHARGE_RATE(t)
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~ Copyright (C) 2002-2011 The Arizona Board of Regents on Behalf of The University of Arizona.  ~~!
!~~ Developed by Yi-Chang Chiu, Ph.D. as par to the DynusT project.                               ~~!
!~~ This program is free software: you can redistribute it and/or modify it                       ~~!
!~~ under the terms of the GNU General Public License version 2 as published by                   ~~!
!~~ the Free Software Foundation.                                                                 ~~!
!~~ This program is distributed in the hope that it will be useful,                               ~~!
!~~ but WITHOUT ANY WARRANTY; without even the implied warranty of                                ~~!
!~~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                                          ~~!
!~~ See the GNU General Public License for more details.                                          ~~!
!~~ You should have received a copy of the GNU General Public License                             ~~!
!~~ along with this program (in the file COPYING).                                                ~~!
!~~ IF not, see <http://www.gnu.org/licenses/>.                                                   ~~!
!~~ Please see http://www.dynust.net for contact information and names                            ~~!
!~~ of individual contributors.                                                                   ~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
      USE DYNUST_MAIN_MODULE
      USE DYNUST_NETWORK_MODULE
      USE DFPORT 
      USE RAND_GEN_INT
      INTERFACE
         SUBROUTINE SIGNAL_PERMISSIVE_LEFT_RATE(ilk,mf,ts,typeid,moveno)
            INTEGER ilk,typeid,moveno
	        REAL mf,ts
         END SUBROUTINE
      END INTERFACE

      REAL MFRtmp, r5,t
      LOGICAL IMajor,ifound
      INTEGER jun,jyc

!> > > > > > > > > >                < < < < < < < < < < <
!>>>>>>>>>>>>>>>>>>>CODES START HERE<<<<<<<<<<<<<<<<<<<<<
!> > > > > > > > > >                < < < < < < < < < < <

      INTEGER::IndCap = 0      


      DO 10 i=1,noofarcs
      

	 IF(m_dynust_network_arc_nde(i)%link_iden < 99) THEN ! only for regular links

! -----------------------------------------------------------------
      IF(m_dynust_network_node(m_dynust_network_arc_nde(i)%idnod)%node(2) == 4.or.m_dynust_network_node(m_dynust_network_arc_nde(i)%idnod)%node(2) == 5) THEN !pre-timed or actuated
! -----------------------------------------------------------------

	  jun=m_dynust_network_arc_de(i)%opp_linkP(2) ! link number of the upcoming link for link i 
	  jyc=m_dynust_network_arc_de(i)%opp_linkP(1) ! movement number of the upcoming link for link i
	  

        IF(jun > 0.and.jyc > 0) THEN
	       m_dynust_network_arc_de(i)%opp_linkS=0
		   IF(m_dynust_network_arcmove_de(jun,jyc)%green > 0) THEN
	          m_dynust_network_arc_de(i)%opp_linkS= jun
	       ENDIF
        ENDIF


         CALL SIGNAL_PERMISSIVE_LEFT_RATE(i,MFRtmp,t,2,0) ! update the right-turn sat flow rate

         IF(m_dynust_network_arc_nde(i)%llink%m(1) > 0) THEN ! found left-turn movement, update the overall SatFlowRate and left-turn capabity based on signal time (protected or permitted)
           CALL SIGNAL_PERMISSIVE_LEFT_RATE(i,MFRtmp,t,1,j) ! the sat flow rate for link i could be reduced IF there is no bay and with LT veh, or LT veh exceeds the cap of bay
         ELSE ! no left-turn
           CALL SIGNAL_PERMISSIVE_LEFT_RATE(i,MFRtmp,t,4,0) ! no change to SatFlowRate
         ENDIF           

         DO j=1,m_dynust_network_arc_nde(i)%llink%no

            IF(m_dynust_network_arcmove_nde(i,j)%move == 1) THEN
              m_dynust_network_arcmove_de(i,j)%capacity=m_dynust_network_arcmove_de(i,j)%green*m_dynust_network_arc_de(i)%left_capacity*max(1,m_dynust_network_arc_de(i)%bay) ! unit of left_cap is # of veh/sec/lane
            ELSEIF(m_dynust_network_arcmove_nde(i,j)%move == 3) THEN
              m_dynust_network_arcmove_de(i,j)%capacity=m_dynust_network_arcmove_de(i,j)%green*m_dynust_network_arc_de(i)%right_capacity*max(1,m_dynust_network_arc_de(i)%bayR) ! unit of right_cap is # of veh/sec/lane            
            ELSE
              m_dynust_network_arcmove_de(i,j)%capacity=m_dynust_network_arcmove_de(i,j)%green*MFRtmp
            ENDIF
             r5 = ranxy(5) 
             IF(r5 <= (m_dynust_network_arcmove_de(i,j)%capacity-ifix(m_dynust_network_arcmove_de(i,j)%capacity))) THEN
		        m_dynust_network_arcmove_de(i,j)%capacity=ifix(m_dynust_network_arcmove_de(i,j)%capacity)+1
             ENDIF
		 ENDDO


! -----------------------------------------------------------------
      ELSEIF(m_dynust_network_node(m_dynust_network_arc_nde(i)%idnod)%node(2) == 1) THEN ! for freeway, use Max Service Flow rate
! -----------------------------------------------------------------
        CALL SIGNAL_PERMISSIVE_LEFT_RATE(i,MFRtmp,t,0,0)

         DO j=1,m_dynust_network_arc_nde(i)%llink%no
           m_dynust_network_arcmove_de(i,j)%capacity=m_dynust_network_arcmove_de(i,j)%green*MFRtmp
    
         IF(m_dynust_network_arcmove_de(i,j)%capacity-ifix(m_dynust_network_arcmove_de(i,j)%capacity) > 0.0001) THEN
          r5 = ranxy(6)     
          
          IF(r5 <= (m_dynust_network_arcmove_de(i,j)%capacity-ifix(m_dynust_network_arcmove_de(i,j)%capacity))) m_dynust_network_arcmove_de(i,j)%capacity=ifix(m_dynust_network_arcmove_de(i,j)%capacity)+1
         ENDIF
        ENDDO
         m_dynust_network_arc_de(i)%left_capacity=m_dynust_network_arc_de(i)%SatFlowRate/m_dynust_network_arc_de(i)%nlanes*6.0 ! unit of Satflow rate is #/sec


! -----------------------------------------------------------------  
      ELSEIF(m_dynust_network_node(m_dynust_network_arc_nde(i)%idnod)%node(2) == 3) THEN ! Starting 4-way STOP Sign
! -----------------------------------------------------------------

         DO j=1,m_dynust_network_arc_nde(i)%llink%no

          IF(m_dynust_network_arcmove_nde(i,j)%move == 1) THEN
            m_dynust_network_arcmove_de(i,j)%capacity=m_dynust_network_arcmove_de(i,j)%green*m_dynust_network_arc_de(i)%nlanes*(STOPCap4w(min(4,m_dynust_network_arc_de(i)%total_count+1),1)/3600.0)
            m_dynust_network_arc_de(i)%left_capacity=m_dynust_network_arcmove_de(i,j)%capacity
          ELSE ! take average value of through and right for movements other than left
            m_dynust_network_arcmove_de(i,j)%capacity=(m_dynust_network_arcmove_de(i,j)%green*m_dynust_network_arc_de(i)%nlanes*(STOPCap4w(min(4,m_dynust_network_arc_de(i)%total_count+1),2)+STOPCap4w(min(4,m_dynust_network_arc_de(i)%total_count+1),3))/2.0/3600.0)
          ENDIF
         IF(m_dynust_network_arcmove_de(i,j)%capacity-ifix(m_dynust_network_arcmove_de(i,j)%capacity) > 0.0001) THEN
             r5 = ranxy(7) 
             IF(r5 <= (m_dynust_network_arcmove_de(i,j)%capacity-ifix(m_dynust_network_arcmove_de(i,j)%capacity))) THEN
			   m_dynust_network_arcmove_de(i,j)%capacity=ifix(m_dynust_network_arcmove_de(i,j)%capacity)+1
			 ENDIF
         ENDIF

         ENDDO


! -----------------------------------------------------------------
      ELSEIF(m_dynust_network_node(m_dynust_network_arc_nde(i)%idnod)%node(2) == 6) THEN ! two-way STOPs
! -----------------------------------------------------------------

         IF(m_dynust_network_arc_nde(i)%llink%m(1) > 0) THEN ! found left-turn movement, update the overall SatFlowRate and left-turn capabity based on signal time (protected or permitted)
           CALL SIGNAL_PERMISSIVE_LEFT_RATE(i,MFRtmp,t,1,j) ! the sat flow rate for link i could be reduced IF there is no bay and with LT veh, or LT veh exceeds the cap of bay
         ELSE ! no left-turn
           CALL SIGNAL_PERMISSIVE_LEFT_RATE(i,MFRtmp,t,4,0) ! no change to SatFlowRate
         ENDIF           

         DO ik = 1, SignCount
           IF(m_dynust_network_arc_nde(i)%idnod == SignData(ik)%node) THEN
             IndSig = ik
	       exit
	     ENDIF
	   ENDDO

         IMajor=.False.
	   DO iMM = 1, SignData(IndSig)%NofMajor
	     IF(i == SignApprh(IndSig)%major(iMM)) THEN ! link i is the major appraoch
             IMajor=.True.
	       exit
           ENDIF
         ENDDO

         IF(IMajor) THEN !link i is major approach
           DO j=1,m_dynust_network_arc_nde(i)%llink%no
              m_dynust_network_arcmove_de(i,j)%capacity=m_dynust_network_arcmove_de(i,j)%green*MFRtmp
              m_dynust_network_arc_de(i)%left_capacity=m_dynust_network_arcmove_de(i,j)%capacity      ! redundent
          
           IF(m_dynust_network_arcmove_de(i,j)%capacity-ifix(m_dynust_network_arcmove_de(i,j)%capacity) > 0.0001) THEN

             r5 = ranxy(8) 
             
             IF(r5 <= (m_dynust_network_arcmove_de(i,j)%capacity-ifix(m_dynust_network_arcmove_de(i,j)%capacity))) THEN
			   m_dynust_network_arcmove_de(i,j)%capacity=ifix(m_dynust_network_arcmove_de(i,j)%capacity)+1
			 ENDIF
           ENDIF
           
           ENDDO

 
         ELSE !link i is minor approach
	   
	   
	   
	   TotMJflow = 0.0
	   IndCap = 0
 	   DO iip = 1, SignData(IndSig)%NofMajor
	     LLT = SignApprh(IndSig)%major(iip)
           TotMJflow=TotMJflow+((m_dynust_network_arc_de(LLT)%c*m_dynust_network_arc_de(LLT)%v)*60) ! #/hr/ln
	   ENDDO
         AvgMJflow = TotMJflow/SignData(IndSig)%NofMajor
         DO ih = 1, level2N
           IF(AvgMJflow <= STOPcap2wiND(ih)) THEN
	       IndCap = ih
	       exit
	     ENDIF
	   ENDDO
       IF(IndCap < 1) IndCap = level2N
              
	   DO j=1,m_dynust_network_arc_nde(i)%llink%no
         IF(m_dynust_network_arcmove_nde(i,j)%move == 1) THEN
           m_dynust_network_arcmove_de(i,j)%capacity=(m_dynust_network_arcmove_de(i,j)%green*m_dynust_network_arc_de(i)%nlanes*STOPCap2w(IndCap,1)/3600.0)
           m_dynust_network_arc_de(i)%left_capacity=m_dynust_network_arcmove_de(i,j)%capacity
         ELSE ! take average value of through and right for movements other than left
           m_dynust_network_arcmove_de(i,j)%capacity=(m_dynust_network_arcmove_de(i,j)%green*m_dynust_network_arc_de(i)%nlanes*(STOPCap2w(IndCap,2)+STOPCap2w(IndCap,3))/2.0/3600.0)
         ENDIF
         IF(m_dynust_network_arcmove_de(i,j)%capacity-ifix(m_dynust_network_arcmove_de(i,j)%capacity) > 0.0001) THEN
              r5 = ranxy(9) 
             IF(r5 <= (m_dynust_network_arcmove_de(i,j)%capacity-ifix(m_dynust_network_arcmove_de(i,j)%capacity))) THEN
			   m_dynust_network_arcmove_de(i,j)%capacity=ifix(m_dynust_network_arcmove_de(i,j)%capacity)+1
			 ENDIF
         ENDIF

         ENDDO
         ENDIF !.not.IMajor



! -----------------------------------------------------------------
      ELSEIF(m_dynust_network_node(m_dynust_network_arc_nde(i)%idnod)%node(2) == 2) THEN ! yield
! -----------------------------------------------------------------

         IF(m_dynust_network_arc_nde(i)%llink%m(1) > 0) THEN ! found left-turn movement, update the overall SatFlowRate and left-turn capabity based on signal time (protected or permitted)
           CALL SIGNAL_PERMISSIVE_LEFT_RATE(i,MFRtmp,t,1,j) ! the sat flow rate for link i could be reduced IF there is no bay and with LT veh, or LT veh exceeds the cap of bay
         ELSE ! no left-turn
           CALL SIGNAL_PERMISSIVE_LEFT_RATE(i,MFRtmp,t,4,0) ! no change to SatFlowRate
         ENDIF           


         DO ik = 1, SignCount
           IF(m_dynust_network_arc_nde(i)%idnod == SignData(ik)%node) THEN
             IndSig = ik
	       exit
	     ENDIF
	   ENDDO

         IMajor=.False.
	   DO iMM = 1, SignData(IndSig)%NofMajor
	     IF(i == SignApprh(IndSig)%major(iMM)) THEN ! link i is the major appraoch
             IMajor=.True.
	       exit
           ENDIF
         ENDDO

         IF(IMajor) THEN !link i is major approach
           DO j=1,m_dynust_network_arc_nde(i)%llink%no
              m_dynust_network_arcmove_de(i,j)%capacity=m_dynust_network_arcmove_de(i,j)%green*MFRtmp
              m_dynust_network_arc_de(i)%left_capacity=m_dynust_network_arcmove_de(i,j)%capacity      ! redundent
           ENDDO
           IF(m_dynust_network_arcmove_de(i,j)%capacity-ifix(m_dynust_network_arcmove_de(i,j)%capacity) > 0.0001) THEN

             r5 = ranxy(10) 

             IF(r5 <= (m_dynust_network_arcmove_de(i,j)%capacity-ifix(m_dynust_network_arcmove_de(i,j)%capacity))) THEN
			   m_dynust_network_arcmove_de(i,j)%capacity=ifix(m_dynust_network_arcmove_de(i,j)%capacity)+1
			 ENDIF
           ENDIF


         ELSE !link i is minor approach
	   
   
	   TotMJflow = 0.0
 	   DO iip = 1, SignData(IndSig)%NofMajor
	     LLT = SignApprh(IndSig)%major(iip)
           TotMJflow=TotMJflow+((m_dynust_network_arc_de(LLT)%c*m_dynust_network_arc_de(LLT)%v)*60) ! #/hr/ln
	   ENDDO
         AvgMJflow = TotMJflow/SignData(IndSig)%NofMajor
         DO ih = 1, level2N
           IF(AvgMJflow <= YieldCapIND(ih)) THEN
	       IndCap = ih
	       exit
	     ENDIF
	   ENDDO

       IF(IndCap < 1.and.AvgMJflow > YieldCapIND(level2N)) IndCap = level2N
      
	   DO j=1,m_dynust_network_arc_nde(i)%llink%no
         IF(m_dynust_network_arcmove_nde(i,j)%move == 1) THEN
           m_dynust_network_arcmove_de(i,j)%capacity=(m_dynust_network_arcmove_de(i,j)%green*m_dynust_network_arc_de(i)%nlanes*YieldCap(IndCap,1)/3600.0)
           m_dynust_network_arc_de(i)%left_capacity=m_dynust_network_arcmove_de(i,j)%capacity      
         ELSE ! take average value of through and right for movements other than left
           m_dynust_network_arcmove_de(i,j)%capacity=(m_dynust_network_arcmove_de(i,j)%green*m_dynust_network_arc_de(i)%nlanes*(YieldCap(IndCap,2)+YieldCap(IndCap,3))/2.0/3600.0)
         ENDIF
         IF(m_dynust_network_arcmove_de(i,j)%capacity-ifix(m_dynust_network_arcmove_de(i,j)%capacity) > 0.0001) THEN
             r5 = ranxy(11) 

             IF(r5 <= (m_dynust_network_arcmove_de(i,j)%capacity-ifix(m_dynust_network_arcmove_de(i,j)%capacity))) THEN
			   m_dynust_network_arcmove_de(i,j)%capacity=ifix(m_dynust_network_arcmove_de(i,j)%capacity)+1
			 ENDIF
         ENDIF

         ENDDO
         ENDIF !.not.IMajor

	ENDIF ! control types
! --

        ENDIF !link_iden(i) < 99
10    CONTINUE                 
! --

END SUBROUTINE
