MODULE DYNUST_TDSP_ASTAR_MODULE
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~ Copyright (C) 2002-2011 The Arizona Board of Regents on Behalf of The University of Arizona.  ~~!
!~~ Developed by Yi-Chang Chiu, Ph.D. as par to the DynusT project.                               ~~!
!~~ This program is free software: you can redistribute it and/or modify it                       ~~!
!~~ under the terms of the GNU General Public License version 2 as published by                   ~~!
!~~ the Free Software Foundation.                                                                 ~~!
!~~ This program is distributed in the hope that it will be useful,                               ~~!
!~~ but WITHOUT ANY WARRANTY; without even the implied warranty of                                ~~!
!~~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                                          ~~!
!~~ See the GNU General Public License for more details.                                          ~~!
!~~ You should have received a copy of the GNU General Public License                             ~~!
!~~ along with this program (in the file COPYING).                                                ~~!
!~~ IF not, see <http://www.gnu.org/licenses/>.                                                   ~~!
!~~ Please see http://www.dynust.net for contact information and names                            ~~!
!~~ of individual contributors.                                                                   ~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
    USE DEQUEUE
    USE LINK_LIST
    
    PUBLIC :: Create_Scan_Eligible_Set, Create_Fixed_Label_Set
	REAL, PUBLIC, ALLOCATABLE :: TTime(:,:)
	REAL, PUBLIC, ALLOCATABLE :: TTpenalty(:,:,:)
	REAL, PUBLIC, ALLOCATABLE :: TTmarginal(:,:,:)
	REAL, PUBLIC, ALLOCATABLE :: RealLabel(:), FakeLabel(:), Ejd(:)
	INTEGER, PUBLIC, ALLOCATABLE :: Pred(:)
	TYPE (Queue_Type_LinkList), ALLOCATABLE, DIMENSION(:) :: Scan_Eligible_Set ! Two dimensional
	TYPE (Queue_Type_LinkList), ALLOCATABLE, DIMENSION(:) :: Fixed_Label_Set  ! one dimensional 
	INTEGER, ALLOCATABLE, DIMENSION(:) :: SE_Tally ! Store # of units in the leafs for Scan_Eligible_Set
	INTEGER, ALLOCATABLE, DIMENSION(:) :: In_SE ! Store the stem id where a link is currently located
	INTEGER, PARAMETER :: SEListInterval = 1 ! This A Star time interval

CONTAINS

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
SUBROUTINE Create_AStar_Scan_Eligible_Set( Size )
    INTEGER :: Size ! Size is time discretization by a user defined interval
    ALLOCATE(Scan_Eligible_Set(Size))

    ALLOCATE(SE_Tally(Size))
    SE_Tally = 0

    DO i = 1, Size
        CALL Create_Queue_LinkList( Scan_Eligible_Set( I ) )
        CALL Enqueue_LinkList (Scan_Eligible_Set( I ), 999999, 9999999.9)
        SE_Tally(i) = 1
    ENDDO
    RETURN
END SUBROUTINE Create_AStar_Scan_Eligible_Set

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
SUBROUTINE Destroy_AStar_Scan_Eligible_Set( Size )
    INTEGER :: Size ! Size is time discretization by a user defined interval
    DO i = 1, Size
        CALL Destroy_Queue_LinkList( Scan_Eligible_Set( I ) )
    ENDDO
    ! Initialize SE_Tally
    DEALLOCATE(SE_Tally)
    DEALLOCATE(Scan_Eligible_Set)
    RETURN
END SUBROUTINE Destroy_AStar_Scan_Eligible_Set

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
SUBROUTINE Create_AStar_Fixed_Label_Set ( )
    INTEGER :: Size
    ALLOCATE(Fixed_Label_Set(1))
    CALL Create_Queue_LinkList( Fixed_Label_Set(1) )
    RETURN
END SUBROUTINE Create_AStar_Fixed_Label_Set

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
SUBROUTINE Destroy_AStar_Fixed_Label_Set ( )
    INTEGER :: Size
    CALL Destroy_Queue_LinkList( Fixed_Label_Set(1) )
    DEALLOCATE(Fixed_Label_Set)
    RETURN
END SUBROUTINE Destroy_AStar_Fixed_Label_Set

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
SUBROUTINE Create_Labels (Size)
    INTEGER :: Size
    REAL, PARAMETER :: INF = 100000.0
	ALLOCATE(RealLabel(Size))
	RealLabel = INF
	ALLOCATE(FakeLabel(Size))
	FakeLabel = INF
	ALLOCATE(Ejd(Size))
	Ejd = INF
	ALLOCATE(Pred(Size))
	Pred = 0
    RETURN
END SUBROUTINE Create_Labels

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
SUBROUTINE Destroy_Labels ()
    INTEGER :: Size
	DEALLOCATE(RealLabel)
	DEALLOCATE(FakeLabel)
	DEALLOCATE(Ejd)
	DEALLOCATE(Pred)
    RETURN
END SUBROUTINE Destroy_Labels

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
SUBROUTINE AStar_Initialization( Arg_Origin, Arg_Dest, Arg_Time, Arg_Flag, N_Nodes ) 
    USE DYNUST_MAIN_MODULE
    USE DYNUST_NETWORK_MODULE
    ! Arg_Origin and Arg_Dest are all link numbers
    ! If Arg_Flag = 0, then backward* data structure is used, otherwise, forward* data structure is used. 
    INTEGER, INTENT(IN) :: Arg_Origin, Arg_Dest, Arg_Flag, N_NOdes
    REAL, INTENT(IN) :: Arg_Time
    REAL, PARAMETER :: EstSpeed = 45.0 ! Estimated speed = 30 mph
    REAL, PARAMETER :: INF = 100000.0
    ! Initialize In_SE 
    IF(.NOT.ALLOCATED(In_SE)) ALLOCATE(In_SE(NoofArcs))
    In_SE = 0
    ! Calculate Ejd
    IF(Arg_Flag == 0) THEN
        FORALL( I = 1:NoofNodes ) &
               Ejd(i) = (distscale*SQRT((x(i)-x(Arg_Dest))**2+(y(i)-y(Arg_Dest))**2))/EstSpeed*60.0
               !Ejd(i) = 0.0
    ELSE
        FORALL( I = 1:NoofNodes ) &
               Ejd(i) = (distscale*SQRT((x(i)-x(m_dynust_network_arc_nde(Arg_Origin)%iunod))**2+(y(i)-y(m_dynust_network_arc_nde(Arg_Origin)%iunod))**2))/EstSpeed*60.0
               !Ejd(i) = 0.0
    ENDIF
    ! Initialize Labels
    IF(Arg_Flag == 0) THEN
        RealLabel = INF
        FakeLabel = INF
        RealLabel(Arg_Origin) = Arg_Time ! Time is in departure time
        FakeLabel(Arg_Origin) = Arg_Time+Ejd(m_dynust_network_arc_nde(Arg_Origin)%iunod)
    ELSE
        RealLabel = INF
        FakeLabel = INF
        RealLabel(Arg_Dest) = Arg_Time ! Time is in arrival time
        FakeLabel(Arg_Dest) = Ejd(Arg_Dest)
    ENDIF
    ! Initialize Fixed Label Set
    IF(Arg_Flag == 0) THEN
        CALL Enqueue_LinkList( Fixed_Label_Set(1), Arg_Origin, RealLabel(Arg_Origin) ) ! Initialize to start with origin
    ELSE
        CALL Enqueue_LinkList( Fixed_Label_Set(1), Arg_Dest, RealLabel(Arg_Dest) ) ! Initialize to start with dest
    ENDIF        
    CLOSE(1)
END SUBROUTINE AStar_Initialization

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
SUBROUTINE AStar_Get_Path( Arg_Origin, Arg_Dest, Arg_DestLink, Arg_Time, Arg_Flag, Td_Flag )
    INTEGER :: Arg_Origin, Arg_Dest, Arg_Flag, Td_Flag, Arg_DestLink ! 0: static, 1: dynamic
    REAL :: Arg_Time
    SELECT CASE(Arg_Flag)
        CASE( 0 ) ! Time is departure time
            CALL AStar_Get_Path_Departure( Arg_Origin, Arg_Dest, Arg_DestLink, Arg_Time, Arg_Flag, Td_Flag )
                        
        CASE( 1 ) ! Time is arrival time
            CALL AStar_Get_Path_Arrival  ( Arg_Origin, Arg_Dest, Arg_DestLink, Arg_Time, Arg_Flag, Td_Flag )
            
    END SELECT
END SUBROUTINE AStar_Get_Path

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
SUBROUTINE AStar_Get_Path_Departure( Arg_Origin, Arg_Dest, Arg_DestLink, Arg_Time, Arg_Flag, Td_Flag )
    USE DYNUST_MAIN_MODULE
    USE DYNUST_NETWORK_MODULE
    
    INTEGER, INTENT(IN)  :: Arg_Origin, Arg_Dest, Arg_Flag, Td_Flag
    INTEGER, INTENT(OUT) :: Arg_DestLink
    REAL,    INTENT(IN)  :: Arg_Time
    LOGICAL :: FoundThis = .FALSE., FoundItem = .FALSE.

    INTEGER :: LinkID, ListRow_Curnt, ListRow_new, BackLink, TimeIndex, Counter
    REAL :: ALabel
    
    FoundThis = .FALSE.
    FoundItem = .FALSE.
    
    LinkID = Arg_Origin
    ListRow_Curnt = FLOOR(Arg_Time-0.05/SEListInterval) + 1
    Counter = 0
    Pred = 0
    
    DO WHILE ( .NOT. FoundThis )
        IF(Counter > 0) THEN
            DO WHILE ( SE_Tally(ListRow_Curnt) == 1.AND.ListRow_Curnt <= SimPeriod/SEListInterval - 1) ! Find the stem with leaves
                ListRow_Curnt = ListRow_Curnt + 1
            ENDDO
            CALL Disqueue_LinkList( Scan_Eligible_Set(ListRow_Curnt), LinkID, ALabel ) ! Remove it from SE List (this step may be redundent)
            In_SE(LinkID) = 0
            SE_Tally(ListRow_Curnt) = SE_Tally(ListRow_Curnt) - 1
        ENDIF
        CALL Enqueue_LinkList ( Fixed_Label_Set(1), LinkID, RealLabel(LinkID) ) ! Add this link to the S Bar set
        !SE_Tally(ListRow_Curnt) = SE_Tally(ListRow_Curnt) + 1 !!!! 
        
        IF(m_dynust_network_arc_nde(LinkID)%idnod == Arg_Dest) THEN ! Found the destination!!
            Arg_DestLink = LinkID
            CALL Enqueue_LinkList ( Fixed_Label_Set(1), LinkID, RealLabel(LinkID) )
            !CALL PrintQueue_LinkList( Fixed_Label_Set(1) )
            !CALL RetrieveTDSP(Arg_Origin, LinkID)
            FoundThis = .TRUE.
            EXIT
        ELSE ! Update real and fake labels
           DO I = 1, m_dynust_network_arc_nde(LinkID)%llink%no ! For outbound links
                mLink = m_dynust_network_arc_nde(LinkID)%llink%p(i)
                IF(Td_Flag == 0) THEN ! Use the latest time
                    !TimeIndex = FLOOR((Arg_Time-0.05)/xminPerSimInt/simPerAgg)+1
                    TimeIndex = 1
                ELSE
                    TimeIndex = MIN(FLOOR(RealLabel(LinkID)/xminPerSimInt/simPerAgg) + 1, AggInt - 1)
                ENDIF
                BackLink = m_dynust_network_arc_nde(mLink)%ForToBackLink
                MP = getBstMove(LinkID,mLink)          
                !IF(TTMarginal(TimeIndex, BackLink, MP) < 100000.0.AND.m_dynust_network_node_nde(m_dynust_network_arc_nde(mLink)%idnod)%IntoOutNodeNum < 900000) THEN ! Downstream node is not a centroid
                IF((TTMarginal(TimeIndex, BackLink, MP) < 100000.0.AND.m_dynust_network_arc_nde(mLink)%idnod < noofnodes_org).OR.m_dynust_network_arc_nde(mLink)%idnod > noofnodes_org) THEN ! Downstream node is not a centroid                
                    IF(m_dynust_network_arc_nde(mLink)%idnod == Arg_Dest) THEN ! Need to differentiate as centroids don't have physical location so Ejd is 100000
                        TempLabel = RealLabel(LinkID) + TTMarginal(TimeIndex, BackLink, MP) + Ejd(m_dynust_network_arc_nde(mLink)%iunod)
                    ELSE
                        TempLabel = RealLabel(LinkID) + TTMarginal(TimeIndex, BackLink, MP) + Ejd(m_dynust_network_arc_nde(mLink)%iunod)
                    ENDIF
                    IF( TempLabel < FakeLabel(mLink)) THEN
                        RealLabel(mLink) = RealLabel(LinkID) + TTMarginal(TimeIndex, BackLink, MP)
                        IF(m_dynust_network_arc_nde(mLink)%idnod == Arg_Dest) THEN
                            FakeLabel(mLink) = RealLabel(LinkID) + TTMarginal(TimeIndex, BackLink, MP) + + Ejd(m_dynust_network_arc_nde(mLink)%iunod)
                        ELSE
                            FakeLabel(mLink) = RealLabel(LinkID) + TTMarginal(TimeIndex, BackLink, MP) + Ejd(m_dynust_network_arc_nde(mLink)%iunod)
                        ENDIF
                        Pred(mLink) = LinkID
                        IF(In_SE(mLink) > 0) THEN ! mLink is currently in the SE List. Need to take it out of the current position
                            CALL Disqueue_Target( Scan_Eligible_Set(In_SE(mLink)), mLink, FoundItem )
                            SE_Tally(In_SE(mLink)) = SE_Tally(In_SE(mLink)) - 1
                            In_SE(mLink) = 0
                            IF(.NOT.FoundItem) THEN
                                WRITE(911,*) "Error in searching items for in Scan_Eligible_Set"
                                STOP
                            ENDIF
                        ENDIF
                        ListRow_new = MIN(NINT(SimPeriod/SEListInterval), FLOOR(FakeLabel(mLink)/SEListInterval) + 1) ! decide which stem mLink will be locaated
                        CALL Enqueue_Ordered_LinkList( Scan_Eligible_Set(ListRow_new), mLink, FakeLabel(mLink) )
                        SE_Tally(ListRow_new) = SE_Tally(ListRow_new) + 1
                        In_SE(mLink) = ListRow_new
                    ENDIF
               ENDIF
           ENDDO         
        ENDIF
        Counter = Counter + 1
    ENDDO

END SUBROUTINE AStar_Get_Path_Departure

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
SUBROUTINE AStar_Get_Path_Arrival ( Arg_Origin, Arg_Dest, Arg_DestLink, Arg_Time, Arg_Flag, Td_Flag )
    INTEGER :: Arg_Origin, Arg_Dest, Arg_Flag, Td_Flag, Arg_DestLink
    REAL :: Arg_Time

END SUBROUTINE AStar_Get_Path_Arrival

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
SUBROUTINE RetrieveTDSP( Origin, Dest, PathTmp, Counter )
    USE DYNUST_NETWORK_MODULE
    USE DYNUST_MAIN_MODULE
    INTEGER, INTENT(IN) :: Origin, Dest
    INTEGER, INTENT(OUT), DIMENSION(maxnu_pa) :: pathtmp(maxnu_pa)
    INTEGER, INTENT(OUT) :: Counter

!    OPEN(FILE = "PATHOUT.DAT", UNIT = 1, STATUS = "UNKNOWN")
!    WRITE(1,*) "Query"
    iLink = Dest
    IF(Origin == Dest) THEN
        PathTmp(1) = m_dynust_network_arc_nde(iLink)%idnod
        Counter = 0
    ELSE
        PathTmp(1) = m_dynust_network_arc_nde(iLink)%idnod
        PathTmp(2) = m_dynust_network_arc_nde(iLink)%iunod
        Counter = 1
    ENDIF
    !write(1,*) m_dynust_network_node_nde(m_dynust_network_arc_nde(iLink)%idnod)%IntoOutNodeNum
    !write(1,*) m_dynust_network_node_nde(m_dynust_network_arc_nde(iLink)%iunod)%IntoOutNodeNum
    DO WHILE (iLink /= Origin)
        Counter = Counter + 1
        PathTmp(Counter) = m_dynust_network_arc_nde(iLink)%iunod
        !write(1,*) m_dynust_network_node_nde(m_dynust_network_arc_nde(Pred(iLink))%iunod)%IntoOutNodeNum
        iLink = Pred(iLink)
    ENDDO
    
END SUBROUTINE RetrieveTDSP

END MODULE DYNUST_TDSP_ASTAR_MODULE