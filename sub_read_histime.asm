; mark_description "Intel(R) Visual Fortran Compiler XE for applications running on IA-32, Version 13.1.1.171 Build 20130313";
; mark_description "-S";
	.686P
 	.387
	OPTION DOTNAME
	ASSUME	CS:FLAT,DS:FLAT,SS:FLAT
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
TXTST0:
; -- Begin  _READ_HIST_TIME
; mark_begin;
IF @Version GE 800
  .MMX
ELSEIF @Version GE 612
  .MMX
  MMWORD TEXTEQU <QWORD>
ENDIF
IF @Version GE 800
  .XMM
ELSEIF @Version GE 614
  .XMM
  XMMWORD TEXTEQU <OWORD>
ENDIF
       ALIGN     16
	PUBLIC _READ_HIST_TIME
_READ_HIST_TIME	PROC NEAR 
.B1.1:                          ; Preds .B1.0
        push      ebp                                           ;1.12
        mov       ebp, esp                                      ;1.12
        and       esp, -16                                      ;1.12
        push      esi                                           ;1.12
        push      edi                                           ;1.12
        push      ebx                                           ;1.12
        sub       esp, 356                                      ;1.12
        xor       ebx, ebx                                      ;31.31
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_MAXVEHICLESFILE] ;33.1
        test      eax, eax                                      ;33.1
        cmovle    eax, ebx                                      ;33.1
        mov       ecx, 128                                      ;33.1
        mov       esi, 1                                        ;33.1
        mov       edx, 2                                        ;33.1
        mov       DWORD PTR [212+esp], ecx                      ;31.31
        mov       DWORD PTR [132+esp], ecx                      ;30.30
        mov       ecx, 4                                        ;33.1
        mov       DWORD PTR [200+esp], ebx                      ;31.31
        mov       DWORD PTR [204+esp], ebx                      ;31.31
        lea       edi, DWORD PTR [eax*4]                        ;33.1
        mov       DWORD PTR [208+esp], ebx                      ;31.31
        mov       DWORD PTR [216+esp], esi                      ;31.31
        mov       DWORD PTR [220+esp], ebx                      ;31.31
        mov       DWORD PTR [224+esp], ebx                      ;31.31
        mov       DWORD PTR [228+esp], ebx                      ;31.31
        mov       DWORD PTR [232+esp], ebx                      ;31.31
        mov       DWORD PTR [120+esp], ebx                      ;30.30
        mov       DWORD PTR [124+esp], ebx                      ;30.30
        mov       DWORD PTR [128+esp], ebx                      ;30.30
        mov       DWORD PTR [136+esp], esi                      ;30.30
        mov       DWORD PTR [140+esp], ebx                      ;30.30
        mov       DWORD PTR [144+esp], ebx                      ;30.30
        mov       DWORD PTR [148+esp], ebx                      ;30.30
        mov       DWORD PTR [152+esp], ebx                      ;30.30
        mov       DWORD PTR [72+esp], ebx                       ;30.22
        mov       DWORD PTR [92+esp], ebx                       ;30.22
        mov       DWORD PTR [84+esp], 133                       ;33.1
        mov       DWORD PTR [76+esp], ecx                       ;33.1
        mov       DWORD PTR [88+esp], edx                       ;33.1
        mov       DWORD PTR [80+esp], ebx                       ;33.1
        mov       DWORD PTR [104+esp], esi                      ;33.1
        mov       DWORD PTR [96+esp], eax                       ;33.1
        mov       DWORD PTR [116+esp], esi                      ;33.1
        mov       DWORD PTR [108+esp], edx                      ;33.1
        mov       DWORD PTR [100+esp], ecx                      ;33.1
        lea       ecx, DWORD PTR [164+esp]                      ;33.1
        mov       DWORD PTR [112+esp], edi                      ;33.1
        push      8                                             ;33.1
        push      eax                                           ;33.1
        push      edx                                           ;33.1
        push      ecx                                           ;33.1
        call      _for_check_mult_overflow                      ;33.1
                                ; LOE eax
.B1.2:                          ; Preds .B1.1
        mov       edx, DWORD PTR [100+esp]                      ;33.1
        and       eax, 1                                        ;33.1
        and       edx, 1                                        ;33.1
        lea       ecx, DWORD PTR [88+esp]                       ;33.1
        shl       eax, 4                                        ;33.1
        add       edx, edx                                      ;33.1
        or        edx, eax                                      ;33.1
        or        edx, 262144                                   ;33.1
        push      edx                                           ;33.1
        push      ecx                                           ;33.1
        push      DWORD PTR [188+esp]                           ;33.1
        call      _for_alloc_allocatable                        ;33.1
                                ; LOE
.B1.122:                        ; Preds .B1.2
        add       esp, 28                                       ;33.1
                                ; LOE
.B1.3:                          ; Preds .B1.122
        mov       esi, DWORD PTR [108+esp]                      ;34.1
        test      esi, esi                                      ;34.1
        mov       edi, DWORD PTR [116+esp]                      ;34.1
        jle       .B1.5         ; Prob 10%                      ;34.1
                                ; LOE esi edi
.B1.4:                          ; Preds .B1.3
        mov       ebx, DWORD PTR [112+esp]                      ;34.1
        mov       eax, DWORD PTR [96+esp]                       ;34.1
        test      eax, eax                                      ;34.1
        mov       edx, DWORD PTR [72+esp]                       ;34.1
        mov       ecx, DWORD PTR [104+esp]                      ;34.1
        mov       DWORD PTR [16+esp], ebx                       ;34.1
        jg        .B1.61        ; Prob 50%                      ;34.1
                                ; LOE eax edx ecx esi edi
.B1.5:                          ; Preds .B1.110 .B1.64 .B1.4 .B1.3
        mov       ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_MAXVEHICLESFILE] ;35.1
        xor       edx, edx                                      ;35.1
        test      ebx, ebx                                      ;35.1
        cmovle    ebx, edx                                      ;35.1
        mov       ecx, 4                                        ;35.1
        mov       eax, 1                                        ;35.1
        mov       DWORD PTR [132+esp], 133                      ;35.1
        lea       esi, DWORD PTR [168+esp]                      ;35.1
        mov       DWORD PTR [124+esp], ecx                      ;35.1
        mov       DWORD PTR [136+esp], eax                      ;35.1
        mov       DWORD PTR [128+esp], edx                      ;35.1
        mov       DWORD PTR [152+esp], eax                      ;35.1
        mov       DWORD PTR [144+esp], ebx                      ;35.1
        mov       DWORD PTR [148+esp], ecx                      ;35.1
        push      ecx                                           ;35.1
        push      ebx                                           ;35.1
        push      2                                             ;35.1
        push      esi                                           ;35.1
        call      _for_check_mult_overflow                      ;35.1
                                ; LOE eax
.B1.6:                          ; Preds .B1.5
        mov       edx, DWORD PTR [148+esp]                      ;35.1
        and       eax, 1                                        ;35.1
        and       edx, 1                                        ;35.1
        lea       ecx, DWORD PTR [136+esp]                      ;35.1
        shl       eax, 4                                        ;35.1
        add       edx, edx                                      ;35.1
        or        edx, eax                                      ;35.1
        or        edx, 262144                                   ;35.1
        push      edx                                           ;35.1
        push      ecx                                           ;35.1
        push      DWORD PTR [192+esp]                           ;35.1
        call      _for_alloc_allocatable                        ;35.1
                                ; LOE
.B1.124:                        ; Preds .B1.6
        add       esp, 28                                       ;35.1
                                ; LOE
.B1.7:                          ; Preds .B1.124
        mov       edx, DWORD PTR [144+esp]                      ;36.1
        test      edx, edx                                      ;36.1
        jle       .B1.10        ; Prob 50%                      ;36.1
                                ; LOE edx
.B1.8:                          ; Preds .B1.7
        mov       ecx, DWORD PTR [120+esp]                      ;36.1
        cmp       edx, 24                                       ;36.1
        jle       .B1.76        ; Prob 0%                       ;36.1
                                ; LOE edx ecx
.B1.9:                          ; Preds .B1.8
        shl       edx, 2                                        ;36.1
        push      edx                                           ;36.1
        push      0                                             ;36.1
        push      ecx                                           ;36.1
        call      __intel_fast_memset                           ;36.1
                                ; LOE
.B1.125:                        ; Preds .B1.9
        add       esp, 12                                       ;36.1
                                ; LOE
.B1.10:                         ; Preds .B1.90 .B1.125 .B1.88 .B1.7
        mov       ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_MAXVEHICLESFILE] ;37.1
        xor       edx, edx                                      ;37.1
        test      ebx, ebx                                      ;37.1
        cmovle    ebx, edx                                      ;37.1
        mov       ecx, 36                                       ;37.1
        mov       eax, 1                                        ;37.1
        mov       DWORD PTR [212+esp], 133                      ;37.1
        lea       esi, DWORD PTR [172+esp]                      ;37.1
        mov       DWORD PTR [204+esp], ecx                      ;37.1
        mov       DWORD PTR [216+esp], eax                      ;37.1
        mov       DWORD PTR [208+esp], edx                      ;37.1
        mov       DWORD PTR [232+esp], eax                      ;37.1
        mov       DWORD PTR [224+esp], ebx                      ;37.1
        mov       DWORD PTR [228+esp], ecx                      ;37.1
        push      ecx                                           ;37.1
        push      ebx                                           ;37.1
        push      2                                             ;37.1
        push      esi                                           ;37.1
        call      _for_check_mult_overflow                      ;37.1
                                ; LOE eax
.B1.11:                         ; Preds .B1.10
        mov       edx, DWORD PTR [228+esp]                      ;37.1
        and       eax, 1                                        ;37.1
        and       edx, 1                                        ;37.1
        lea       ecx, DWORD PTR [216+esp]                      ;37.1
        shl       eax, 4                                        ;37.1
        add       edx, edx                                      ;37.1
        or        edx, eax                                      ;37.1
        or        edx, 262144                                   ;37.1
        push      edx                                           ;37.1
        push      ecx                                           ;37.1
        push      DWORD PTR [196+esp]                           ;37.1
        call      _for_alloc_allocatable                        ;37.1
                                ; LOE
.B1.12:                         ; Preds .B1.11
        mov       DWORD PTR [268+esp], 0                        ;40.2
        lea       edx, DWORD PTR [268+esp]                      ;40.2
        mov       DWORD PTR [52+esp], 15                        ;40.2
        lea       eax, DWORD PTR [52+esp]                       ;40.2
        mov       DWORD PTR [56+esp], OFFSET FLAT: __STRLITPACK_17 ;40.2
        mov       DWORD PTR [60+esp], 3                         ;40.2
        mov       DWORD PTR [64+esp], OFFSET FLAT: __STRLITPACK_16 ;40.2
        push      32                                            ;40.2
        push      eax                                           ;40.2
        push      OFFSET FLAT: __STRLITPACK_18.0.1              ;40.2
        push      -2088435965                                   ;40.2
        push      561                                           ;40.2
        push      edx                                           ;40.2
        call      _for_open                                     ;40.2
                                ; LOE eax
.B1.127:                        ; Preds .B1.12
        add       esp, 52                                       ;40.2
                                ; LOE eax
.B1.13:                         ; Preds .B1.127
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;40.2
        test      eax, eax                                      ;41.12
        jne       .B1.74        ; Prob 5%                       ;41.12
                                ; LOE
.B1.14:                         ; Preds .B1.146 .B1.13
        mov       DWORD PTR [240+esp], 0                        ;45.2
        lea       eax, DWORD PTR [40+esp]                       ;45.2
        mov       DWORD PTR [40+esp], 15                        ;45.2
        mov       DWORD PTR [44+esp], OFFSET FLAT: __STRLITPACK_13 ;45.2
        mov       DWORD PTR [48+esp], 3                         ;45.2
        mov       DWORD PTR [52+esp], OFFSET FLAT: __STRLITPACK_12 ;45.2
        push      32                                            ;45.2
        push      eax                                           ;45.2
        push      OFFSET FLAT: __STRLITPACK_21.0.1              ;45.2
        push      -2088435965                                   ;45.2
        push      562                                           ;45.2
        lea       edx, DWORD PTR [260+esp]                      ;45.2
        push      edx                                           ;45.2
        call      _for_open                                     ;45.2
                                ; LOE eax
.B1.128:                        ; Preds .B1.14
        add       esp, 24                                       ;45.2
                                ; LOE eax
.B1.15:                         ; Preds .B1.128
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;45.2
        test      eax, eax                                      ;46.12
        jne       .B1.72        ; Prob 5%                       ;46.12
                                ; LOE
.B1.16:                         ; Preds .B1.145 .B1.15
        mov       DWORD PTR [240+esp], 0                        ;50.2
        lea       eax, DWORD PTR [56+esp]                       ;50.2
        mov       DWORD PTR [56+esp], 16                        ;50.2
        mov       DWORD PTR [60+esp], OFFSET FLAT: __STRLITPACK_9 ;50.2
        mov       DWORD PTR [64+esp], 7                         ;50.2
        mov       DWORD PTR [68+esp], OFFSET FLAT: __STRLITPACK_8 ;50.2
        push      32                                            ;50.2
        push      eax                                           ;50.2
        push      OFFSET FLAT: __STRLITPACK_24.0.1              ;50.2
        push      -2088435965                                   ;50.2
        push      560                                           ;50.2
        lea       edx, DWORD PTR [260+esp]                      ;50.2
        push      edx                                           ;50.2
        call      _for_open                                     ;50.2
                                ; LOE eax
.B1.129:                        ; Preds .B1.16
        add       esp, 24                                       ;50.2
                                ; LOE eax
.B1.17:                         ; Preds .B1.129
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;50.2
        test      eax, eax                                      ;51.12
        jne       .B1.70        ; Prob 5%                       ;51.12
                                ; LOE
.B1.18:                         ; Preds .B1.144 .B1.17
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_MAXVEHICLESFILE] ;56.3
        test      eax, eax                                      ;56.3
        jle       .B1.37        ; Prob 2%                       ;56.3
                                ; LOE eax
.B1.19:                         ; Preds .B1.18
        mov       DWORD PTR [156+esp], eax                      ;
        mov       edi, 1                                        ;
        lea       esi, DWORD PTR [240+esp]                      ;
                                ; LOE esi edi
.B1.20:                         ; Preds .B1.35 .B1.19
        mov       eax, DWORD PTR [116+esp]                      ;57.5
        lea       ecx, DWORD PTR [272+esp]                      ;58.5
        neg       eax                                           ;57.5
        add       eax, 2                                        ;57.5
        imul      eax, DWORD PTR [112+esp]                      ;57.5
        mov       edx, DWORD PTR [104+esp]                      ;57.5
        add       eax, DWORD PTR [72+esp]                       ;57.5
        neg       edx                                           ;57.5
        mov       DWORD PTR [240+esp], 0                        ;58.5
        add       edx, edi                                      ;57.5
        mov       DWORD PTR [304+esp], ecx                      ;58.5
        push      32                                            ;58.5
        push      OFFSET FLAT: READ_HIST_TIME$format_pack.0.1   ;58.5
        mov       DWORD PTR [eax+edx*4], edi                    ;57.5
        lea       ebx, DWORD PTR [312+esp]                      ;58.5
        push      ebx                                           ;58.5
        push      OFFSET FLAT: __STRLITPACK_27.0.1              ;58.5
        push      -2088435968                                   ;58.5
        push      562                                           ;58.5
        push      esi                                           ;58.5
        call      _for_read_seq_fmt                             ;58.5
                                ; LOE esi edi
.B1.21:                         ; Preds .B1.20
        mov       ecx, DWORD PTR [140+esp]                      ;58.43
        mov       eax, DWORD PTR [144+esp]                      ;58.43
        imul      eax, ecx                                      ;58.5
        mov       edx, DWORD PTR [132+esp]                      ;58.43
        shl       edx, 2                                        ;58.5
        mov       ebx, DWORD PTR [100+esp]                      ;58.5
        sub       ecx, edx                                      ;58.5
        sub       ebx, eax                                      ;58.5
        lea       edx, DWORD PTR [212+esp]                      ;58.5
        add       ebx, ecx                                      ;58.5
        mov       DWORD PTR [212+esp], 4                        ;58.5
        lea       eax, DWORD PTR [ebx+edi*4]                    ;58.5
        mov       DWORD PTR [216+esp], eax                      ;58.5
        push      edx                                           ;58.5
        push      OFFSET FLAT: __STRLITPACK_28.0.1              ;58.5
        push      esi                                           ;58.5
        call      _for_read_seq_fmt_xmit                        ;58.5
                                ; LOE esi edi
.B1.22:                         ; Preds .B1.21
        lea       edx, DWORD PTR [352+esp]                      ;58.5
        lea       eax, DWORD PTR [316+esp]                      ;58.5
        mov       DWORD PTR [352+esp], eax                      ;58.5
        push      edx                                           ;58.5
        push      OFFSET FLAT: __STRLITPACK_29.0.1              ;58.5
        push      esi                                           ;58.5
        call      _for_read_seq_fmt_xmit                        ;58.5
                                ; LOE esi edi
.B1.23:                         ; Preds .B1.22
        lea       edx, DWORD PTR [372+esp]                      ;58.5
        lea       eax, DWORD PTR [332+esp]                      ;58.5
        mov       DWORD PTR [372+esp], eax                      ;58.5
        push      edx                                           ;58.5
        push      OFFSET FLAT: __STRLITPACK_30.0.1              ;58.5
        push      esi                                           ;58.5
        call      _for_read_seq_fmt_xmit                        ;58.5
                                ; LOE esi edi
.B1.24:                         ; Preds .B1.23
        lea       edx, DWORD PTR [392+esp]                      ;58.5
        lea       eax, DWORD PTR [348+esp]                      ;58.5
        mov       DWORD PTR [392+esp], eax                      ;58.5
        push      edx                                           ;58.5
        push      OFFSET FLAT: __STRLITPACK_31.0.1              ;58.5
        push      esi                                           ;58.5
        call      _for_read_seq_fmt_xmit                        ;58.5
                                ; LOE esi edi
.B1.25:                         ; Preds .B1.24
        mov       eax, DWORD PTR [228+esp]                      ;58.78
        lea       ebx, DWORD PTR [268+esp]                      ;58.5
        shl       eax, 2                                        ;58.5
        mov       edx, DWORD PTR [196+esp]                      ;58.5
        sub       edx, eax                                      ;58.5
        mov       DWORD PTR [268+esp], 4                        ;58.5
        lea       ecx, DWORD PTR [edx+edi*4]                    ;58.5
        mov       DWORD PTR [272+esp], ecx                      ;58.5
        push      ebx                                           ;58.5
        push      OFFSET FLAT: __STRLITPACK_32.0.1              ;58.5
        push      esi                                           ;58.5
        call      _for_read_seq_fmt_xmit                        ;58.5
                                ; LOE esi edi
.B1.26:                         ; Preds .B1.25
        mov       ebx, DWORD PTR [288+esp]                      ;59.5
        lea       esi, DWORD PTR [edi*4]                        ;59.5
        mov       DWORD PTR [264+esp], edi                      ;
        lea       eax, DWORD PTR [esi+esi*8]                    ;59.5
        mov       DWORD PTR [248+esp], eax                      ;59.5
        add       ebx, eax                                      ;59.5
        imul      eax, DWORD PTR [320+esp], -36                 ;59.5
        mov       esi, 1                                        ;59.5
        mov       DWORD PTR [16+eax+ebx], esi                   ;59.5
        mov       ecx, 4                                        ;59.5
        mov       DWORD PTR [32+eax+ebx], esi                   ;59.5
        xor       edx, edx                                      ;59.5
        mov       esi, DWORD PTR [208+esp]                      ;59.5
        mov       DWORD PTR [12+eax+ebx], 5                     ;59.5
        mov       DWORD PTR [4+eax+ebx], ecx                    ;59.5
        mov       DWORD PTR [8+eax+ebx], edx                    ;59.5
        lea       esi, DWORD PTR [esi+edi*4]                    ;59.5
        mov       edi, DWORD PTR [240+esp]                      ;59.25
        shl       edi, 2                                        ;59.5
        neg       edi                                           ;59.5
        push      ecx                                           ;59.5
        mov       DWORD PTR [28+eax+ebx], ecx                   ;59.5
        mov       edi, DWORD PTR [edi+esi]                      ;59.25
        cmp       edi, 2                                        ;59.5
        lea       esi, DWORD PTR [-1+edi]                       ;59.5
        cmovl     esi, edx                                      ;59.5
        push      esi                                           ;59.5
        push      2                                             ;59.5
        mov       DWORD PTR [24+eax+ebx], esi                   ;59.5
        lea       eax, DWORD PTR [388+esp]                      ;59.5
        push      eax                                           ;59.5
        mov       edi, DWORD PTR [280+esp]                      ;59.5
        lea       esi, DWORD PTR [344+esp]                      ;59.5
        call      _for_check_mult_overflow                      ;59.5
                                ; LOE eax esi edi
.B1.27:                         ; Preds .B1.26
        imul      edx, DWORD PTR [336+esp], -36                 ;59.5
        and       eax, 1                                        ;59.5
        shl       eax, 4                                        ;59.5
        add       edx, DWORD PTR [304+esp]                      ;59.5
        or        eax, 262144                                   ;59.5
        add       edx, DWORD PTR [264+esp]                      ;59.5
        push      eax                                           ;59.5
        push      edx                                           ;59.5
        push      DWORD PTR [400+esp]                           ;59.5
        call      _for_allocate                                 ;59.5
                                ; LOE esi edi
.B1.131:                        ; Preds .B1.27
        add       esp, 116                                      ;59.5
                                ; LOE esi edi
.B1.28:                         ; Preds .B1.131
        xor       eax, eax                                      ;60.5
        mov       DWORD PTR [240+esp], eax                      ;60.5
        push      32                                            ;60.5
        push      OFFSET FLAT: READ_HIST_TIME$format_pack.0.1+44 ;60.5
        push      eax                                           ;60.5
        push      OFFSET FLAT: __STRLITPACK_33.0.1              ;60.5
        push      -2088435968                                   ;60.5
        push      561                                           ;60.5
        push      esi                                           ;60.5
        call      _for_read_seq_fmt                             ;60.5
                                ; LOE esi edi
.B1.132:                        ; Preds .B1.28
        add       esp, 28                                       ;60.5
                                ; LOE esi edi
.B1.29:                         ; Preds .B1.132
        mov       edx, edi                                      ;60.5
        sub       edx, DWORD PTR [152+esp]                      ;60.5
        mov       eax, DWORD PTR [120+esp]                      ;60.5
        mov       edx, DWORD PTR [eax+edx*4]                    ;60.47
        dec       edx                                           ;60.5
        test      edx, edx                                      ;60.5
        jle       .B1.34        ; Prob 2%                       ;60.5
                                ; LOE edx esi edi
.B1.30:                         ; Preds .B1.29
        mov       DWORD PTR [176+esp], edi                      ;
        mov       ebx, 1                                        ;
        mov       DWORD PTR [180+esp], edx                      ;
        mov       edi, DWORD PTR [160+esp]                      ;
                                ; LOE ebx esi edi
.B1.31:                         ; Preds .B1.32 .B1.30
        imul      eax, DWORD PTR [232+esp], -36                 ;60.5
        lea       ecx, DWORD PTR [336+esp]                      ;60.5
        add       eax, DWORD PTR [200+esp]                      ;60.5
        mov       DWORD PTR [336+esp], 4                        ;60.5
        mov       edx, DWORD PTR [32+edi+eax]                   ;60.29
        neg       edx                                           ;60.5
        add       edx, ebx                                      ;60.5
        imul      edx, DWORD PTR [28+edi+eax]                   ;60.5
        add       edx, DWORD PTR [edi+eax]                      ;60.5
        mov       DWORD PTR [340+esp], edx                      ;60.5
        push      ecx                                           ;60.5
        push      OFFSET FLAT: __STRLITPACK_34.0.1              ;60.5
        push      esi                                           ;60.5
        call      _for_read_seq_fmt_xmit                        ;60.5
                                ; LOE ebx esi edi
.B1.133:                        ; Preds .B1.31
        add       esp, 12                                       ;60.5
                                ; LOE ebx esi edi
.B1.32:                         ; Preds .B1.133
        inc       ebx                                           ;60.5
        cmp       ebx, DWORD PTR [180+esp]                      ;60.5
        jle       .B1.31        ; Prob 82%                      ;60.5
                                ; LOE ebx esi edi
.B1.33:                         ; Preds .B1.32
        mov       edi, DWORD PTR [176+esp]                      ;
                                ; LOE esi edi
.B1.34:                         ; Preds .B1.29 .B1.33
        push      0                                             ;60.5
        push      OFFSET FLAT: __STRLITPACK_35.0.1              ;60.5
        push      esi                                           ;60.5
        call      _for_read_seq_fmt_xmit                        ;60.5
                                ; LOE esi edi
.B1.134:                        ; Preds .B1.34
        add       esp, 12                                       ;60.5
                                ; LOE esi edi
.B1.35:                         ; Preds .B1.134
        inc       edi                                           ;61.3
        cmp       edi, DWORD PTR [156+esp]                      ;61.3
        jle       .B1.20        ; Prob 82%                      ;61.3
                                ; LOE esi edi
.B1.37:                         ; Preds .B1.35 .B1.18
        push      OFFSET FLAT: __NLITPACK_0.0.1                 ;63.8
        push      OFFSET FLAT: _DYNUST_MAIN_MODULE_mp_MAXVEHICLESFILE ;63.8
        push      DWORD PTR [80+esp]                            ;63.8
        call      _IMSLSORT                                     ;63.8
                                ; LOE
.B1.135:                        ; Preds .B1.37
        add       esp, 12                                       ;63.8
                                ; LOE
.B1.38:                         ; Preds .B1.135
        mov       eax, DWORD PTR [120+esp]                      ;73.3
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_MAXVEHICLESFILE] ;65.3
        test      edx, edx                                      ;65.3
        mov       DWORD PTR [176+esp], eax                      ;73.3
        mov       DWORD PTR [292+esp], edx                      ;65.3
        mov       edi, DWORD PTR [200+esp]                      ;73.3
        mov       eax, DWORD PTR [72+esp]                       ;73.3
        jle       .B1.52        ; Prob 2%                       ;65.3
                                ; LOE eax edi
.B1.39:                         ; Preds .B1.38
        mov       ebx, DWORD PTR [116+esp]                      ;66.5
        neg       ebx                                           ;
        add       ebx, 2                                        ;
        imul      ebx, DWORD PTR [112+esp]                      ;
        imul      edx, DWORD PTR [232+esp], -36                 ;
        mov       ecx, DWORD PTR [104+esp]                      ;66.5
        add       edx, edi                                      ;
        shl       ecx, 2                                        ;
        mov       esi, DWORD PTR [152+esp]                      ;66.54
        neg       ecx                                           ;
        shl       esi, 2                                        ;
        add       ecx, eax                                      ;
        neg       esi                                           ;
        add       ebx, ecx                                      ;
        add       esi, DWORD PTR [176+esp]                      ;
        mov       DWORD PTR [180+esp], 1                        ;
        mov       DWORD PTR [300+esp], ebx                      ;
        mov       DWORD PTR [236+esp], edx                      ;
        mov       DWORD PTR [296+esp], esi                      ;
        mov       DWORD PTR [156+esp], eax                      ;
        mov       DWORD PTR [160+esp], edi                      ;
        mov       ebx, DWORD PTR [180+esp]                      ;
                                ; LOE ebx
.B1.40:                         ; Preds .B1.47 .B1.39
        xor       eax, eax                                      ;66.5
        mov       DWORD PTR [240+esp], eax                      ;66.5
        push      32                                            ;66.5
        push      OFFSET FLAT: READ_HIST_TIME$format_pack.0.1+64 ;66.5
        push      eax                                           ;66.5
        push      OFFSET FLAT: __STRLITPACK_36.0.1              ;66.5
        push      -2088435968                                   ;66.5
        push      560                                           ;66.5
        lea       edx, DWORD PTR [264+esp]                      ;66.5
        push      edx                                           ;66.5
        call      _for_write_seq_fmt                            ;66.5
                                ; LOE ebx
.B1.136:                        ; Preds .B1.40
        add       esp, 28                                       ;66.5
                                ; LOE ebx
.B1.41:                         ; Preds .B1.136
        mov       eax, DWORD PTR [300+esp]                      ;66.54
        mov       edx, DWORD PTR [296+esp]                      ;66.54
        mov       ecx, DWORD PTR [eax+ebx*4]                    ;66.54
        mov       esi, DWORD PTR [edx+ecx*4]                    ;66.54
        dec       esi                                           ;66.5
        mov       DWORD PTR [316+esp], esi                      ;66.5
        test      esi, esi                                      ;66.5
        jle       .B1.46        ; Prob 2%                       ;66.5
                                ; LOE eax ebx al ah
.B1.42:                         ; Preds .B1.41
        mov       edi, eax                                      ;66.30
        mov       edx, 1                                        ;
        mov       DWORD PTR [180+esp], ebx                      ;
        mov       ecx, DWORD PTR [edi+ebx*4]                    ;66.30
        mov       ebx, edx                                      ;
        shl       ecx, 2                                        ;66.30
        lea       esi, DWORD PTR [ecx+ecx*8]                    ;66.30
        mov       ecx, DWORD PTR [236+esp]                      ;66.30
        mov       edi, DWORD PTR [esi+ecx]                      ;66.30
        mov       eax, DWORD PTR [32+esi+ecx]                   ;66.30
        mov       esi, DWORD PTR [28+esi+ecx]                   ;66.30
        imul      eax, esi                                      ;
        mov       DWORD PTR [308+esp], esi                      ;66.30
        sub       edi, eax                                      ;
                                ; LOE ebx esi edi
.B1.43:                         ; Preds .B1.44 .B1.42
        mov       eax, DWORD PTR [esi+edi]                      ;66.5
        lea       ecx, DWORD PTR [344+esp]                      ;66.5
        mov       DWORD PTR [344+esp], eax                      ;66.5
        push      ecx                                           ;66.5
        push      OFFSET FLAT: __STRLITPACK_37.0.1              ;66.5
        lea       eax, DWORD PTR [248+esp]                      ;66.5
        push      eax                                           ;66.5
        call      _for_write_seq_fmt_xmit                       ;66.5
                                ; LOE ebx esi edi
.B1.137:                        ; Preds .B1.43
        add       esp, 12                                       ;66.5
                                ; LOE ebx esi edi
.B1.44:                         ; Preds .B1.137
        inc       ebx                                           ;66.5
        add       esi, DWORD PTR [308+esp]                      ;66.5
        cmp       ebx, DWORD PTR [316+esp]                      ;66.5
        jle       .B1.43        ; Prob 82%                      ;66.5
                                ; LOE ebx esi edi
.B1.45:                         ; Preds .B1.44
        mov       ebx, DWORD PTR [180+esp]                      ;
                                ; LOE ebx
.B1.46:                         ; Preds .B1.41 .B1.45
        push      0                                             ;66.5
        push      OFFSET FLAT: __STRLITPACK_38.0.1              ;66.5
        lea       eax, DWORD PTR [248+esp]                      ;66.5
        push      eax                                           ;66.5
        call      _for_write_seq_fmt_xmit                       ;66.5
                                ; LOE ebx
.B1.138:                        ; Preds .B1.46
        add       esp, 12                                       ;66.5
                                ; LOE ebx
.B1.47:                         ; Preds .B1.138
        inc       ebx                                           ;67.3
        cmp       ebx, DWORD PTR [292+esp]                      ;67.3
        jle       .B1.40        ; Prob 82%                      ;67.3
                                ; LOE ebx
.B1.48:                         ; Preds .B1.47
        imul      ebx, DWORD PTR [232+esp], -36                 ;
        mov       esi, 1                                        ;
        mov       edi, DWORD PTR [160+esp]                      ;
        add       ebx, edi                                      ;
                                ; LOE ebx esi
.B1.49:                         ; Preds .B1.50 .B1.48
        lea       edi, DWORD PTR [esi*4]                        ;70.5
        lea       edi, DWORD PTR [edi+edi*8]                    ;70.5
        mov       eax, DWORD PTR [12+edi+ebx]                   ;70.16
        mov       edx, eax                                      ;70.5
        shr       edx, 1                                        ;70.5
        and       eax, 1                                        ;70.5
        and       edx, 1                                        ;70.5
        add       eax, eax                                      ;70.5
        shl       edx, 2                                        ;70.5
        or        edx, eax                                      ;70.5
        or        edx, 262144                                   ;70.5
        push      edx                                           ;70.5
        push      DWORD PTR [edi+ebx]                           ;70.5
        call      _for_dealloc_allocatable                      ;70.5
                                ; LOE ebx esi edi
.B1.139:                        ; Preds .B1.49
        add       esp, 8                                        ;70.5
                                ; LOE ebx esi edi
.B1.50:                         ; Preds .B1.139
        inc       esi                                           ;71.3
        and       DWORD PTR [12+ebx+edi], -2                    ;70.5
        mov       DWORD PTR [ebx+edi], 0                        ;70.5
        cmp       esi, DWORD PTR [292+esp]                      ;71.3
        jle       .B1.49        ; Prob 82%                      ;71.3
                                ; LOE ebx esi
.B1.51:                         ; Preds .B1.50
        mov       eax, DWORD PTR [156+esp]                      ;
        mov       edi, DWORD PTR [160+esp]                      ;
                                ; LOE eax edi
.B1.52:                         ; Preds .B1.38 .B1.51
        mov       ebx, DWORD PTR [84+esp]                       ;73.3
        mov       ecx, ebx                                      ;73.3
        shr       ecx, 1                                        ;73.3
        mov       edx, ebx                                      ;73.3
        and       ecx, 1                                        ;73.3
        and       edx, 1                                        ;73.3
        shl       ecx, 2                                        ;73.3
        add       edx, edx                                      ;73.3
        or        ecx, edx                                      ;73.3
        or        ecx, 262144                                   ;73.3
        push      ecx                                           ;73.3
        push      eax                                           ;73.3
        call      _for_dealloc_allocatable                      ;73.3
                                ; LOE ebx edi
.B1.53:                         ; Preds .B1.52
        and       ebx, -2                                       ;73.3
        mov       DWORD PTR [92+esp], ebx                       ;73.3
        mov       ebx, DWORD PTR [140+esp]                      ;73.3
        mov       edx, ebx                                      ;73.3
        shr       edx, 1                                        ;73.3
        mov       eax, ebx                                      ;73.3
        and       edx, 1                                        ;73.3
        and       eax, 1                                        ;73.3
        shl       edx, 2                                        ;73.3
        add       eax, eax                                      ;73.3
        or        edx, eax                                      ;73.3
        or        edx, 262144                                   ;73.3
        mov       DWORD PTR [80+esp], 0                         ;73.3
        push      edx                                           ;73.3
        push      DWORD PTR [188+esp]                           ;73.3
        call      _for_dealloc_allocatable                      ;73.3
                                ; LOE ebx edi
.B1.54:                         ; Preds .B1.53
        and       ebx, -2                                       ;73.3
        mov       DWORD PTR [148+esp], ebx                      ;73.3
        mov       ebx, DWORD PTR [228+esp]                      ;73.3
        mov       edx, ebx                                      ;73.3
        shr       edx, 1                                        ;73.3
        mov       eax, ebx                                      ;73.3
        and       edx, 1                                        ;73.3
        and       eax, 1                                        ;73.3
        shl       edx, 2                                        ;73.3
        add       eax, eax                                      ;73.3
        or        edx, eax                                      ;73.3
        or        edx, 262144                                   ;73.3
        mov       DWORD PTR [136+esp], 0                        ;73.3
        push      edx                                           ;73.3
        push      edi                                           ;73.3
        call      _for_dealloc_allocatable                      ;73.3
                                ; LOE ebx
.B1.55:                         ; Preds .B1.54
        xor       eax, eax                                      ;73.3
        and       ebx, -2                                       ;73.3
        mov       DWORD PTR [224+esp], eax                      ;73.3
        mov       DWORD PTR [236+esp], ebx                      ;73.3
        mov       DWORD PTR [264+esp], eax                      ;75.9
        push      32                                            ;75.9
        push      eax                                           ;75.9
        push      OFFSET FLAT: __STRLITPACK_39.0.1              ;75.9
        push      -2088435968                                   ;75.9
        push      561                                           ;75.9
        lea       edx, DWORD PTR [284+esp]                      ;75.9
        push      edx                                           ;75.9
        call      _for_close                                    ;75.9
                                ; LOE
.B1.56:                         ; Preds .B1.55
        mov       DWORD PTR [288+esp], 0                        ;76.3
        push      32                                            ;76.3
        push      -2088435968                                   ;76.3
        push      560                                           ;76.3
        lea       eax, DWORD PTR [300+esp]                      ;76.3
        push      eax                                           ;76.3
        call      _for_rewind                                   ;76.3
                                ; LOE
.B1.140:                        ; Preds .B1.56
        add       esp, 64                                       ;76.3
                                ; LOE
.B1.57:                         ; Preds .B1.140
        mov       ebx, DWORD PTR [84+esp]                       ;77.1
        test      bl, 1                                         ;77.1
        jne       .B1.68        ; Prob 3%                       ;77.1
                                ; LOE ebx
.B1.58:                         ; Preds .B1.57 .B1.69
        mov       ebx, DWORD PTR [132+esp]                      ;77.1
        test      bl, 1                                         ;77.1
        jne       .B1.66        ; Prob 3%                       ;77.1
                                ; LOE ebx
.B1.59:                         ; Preds .B1.58 .B1.67
        mov       ebx, DWORD PTR [212+esp]                      ;77.1
        test      bl, 1                                         ;77.1
        jne       .B1.117       ; Prob 3%                       ;77.1
                                ; LOE ebx
.B1.60:                         ; Preds .B1.59
        add       esp, 356                                      ;77.1
        pop       ebx                                           ;77.1
        pop       edi                                           ;77.1
        pop       esi                                           ;77.1
        mov       esp, ebp                                      ;77.1
        pop       ebp                                           ;77.1
        ret                                                     ;77.1
                                ; LOE
.B1.61:                         ; Preds .B1.4
        imul      edi, DWORD PTR [16+esp]                       ;
        xor       ebx, ebx                                      ;
        pxor      xmm0, xmm0                                    ;34.1
        mov       DWORD PTR [8+esp], esi                        ;
        mov       esi, edx                                      ;
        shl       ecx, 2                                        ;
        sub       esi, ecx                                      ;
        sub       ecx, edi                                      ;
        add       esi, ecx                                      ;
        lea       ecx, DWORD PTR [eax*4]                        ;
        add       esi, edi                                      ;
        mov       DWORD PTR [esp], esi                          ;
        mov       esi, DWORD PTR [esp]                          ;
        mov       edi, edx                                      ;
        mov       DWORD PTR [12+esp], ebx                       ;
        mov       DWORD PTR [4+esp], ecx                        ;
        mov       DWORD PTR [20+esp], eax                       ;
        mov       DWORD PTR [24+esp], edx                       ;
                                ; LOE ebx esi edi
.B1.62:                         ; Preds .B1.64 .B1.110 .B1.61
        cmp       DWORD PTR [20+esp], 24                        ;34.1
        jle       .B1.95        ; Prob 0%                       ;34.1
                                ; LOE ebx esi edi
.B1.63:                         ; Preds .B1.62
        push      DWORD PTR [4+esp]                             ;34.1
        push      0                                             ;34.1
        push      esi                                           ;34.1
        call      __intel_fast_memset                           ;34.1
                                ; LOE ebx esi edi
.B1.141:                        ; Preds .B1.63
        add       esp, 12                                       ;34.1
                                ; LOE ebx esi edi
.B1.64:                         ; Preds .B1.107 .B1.141
        mov       edx, DWORD PTR [12+esp]                       ;34.1
        inc       edx                                           ;34.1
        mov       eax, DWORD PTR [16+esp]                       ;34.1
        add       edi, eax                                      ;34.1
        add       esi, eax                                      ;34.1
        add       ebx, eax                                      ;34.1
        mov       DWORD PTR [12+esp], edx                       ;34.1
        cmp       edx, DWORD PTR [8+esp]                        ;34.1
        jb        .B1.62        ; Prob 82%                      ;34.1
        jmp       .B1.5         ; Prob 100%                     ;34.1
                                ; LOE ebx esi edi
.B1.66:                         ; Preds .B1.58                  ; Infreq
        mov       edx, ebx                                      ;77.1
        mov       eax, ebx                                      ;77.1
        shr       edx, 1                                        ;77.1
        and       eax, 1                                        ;77.1
        and       edx, 1                                        ;77.1
        add       eax, eax                                      ;77.1
        shl       edx, 2                                        ;77.1
        or        edx, eax                                      ;77.1
        or        edx, 262144                                   ;77.1
        push      edx                                           ;77.1
        push      DWORD PTR [124+esp]                           ;77.1
        call      _for_dealloc_allocatable                      ;77.1
                                ; LOE ebx
.B1.142:                        ; Preds .B1.66                  ; Infreq
        add       esp, 8                                        ;77.1
                                ; LOE ebx
.B1.67:                         ; Preds .B1.142                 ; Infreq
        and       ebx, -2                                       ;77.1
        mov       DWORD PTR [120+esp], 0                        ;77.1
        mov       DWORD PTR [132+esp], ebx                      ;77.1
        jmp       .B1.59        ; Prob 100%                     ;77.1
                                ; LOE
.B1.68:                         ; Preds .B1.57                  ; Infreq
        mov       edx, ebx                                      ;77.1
        mov       eax, ebx                                      ;77.1
        shr       edx, 1                                        ;77.1
        and       eax, 1                                        ;77.1
        and       edx, 1                                        ;77.1
        add       eax, eax                                      ;77.1
        shl       edx, 2                                        ;77.1
        or        edx, eax                                      ;77.1
        or        edx, 262144                                   ;77.1
        push      edx                                           ;77.1
        push      DWORD PTR [76+esp]                            ;77.1
        call      _for_dealloc_allocatable                      ;77.1
                                ; LOE ebx
.B1.143:                        ; Preds .B1.68                  ; Infreq
        add       esp, 8                                        ;77.1
                                ; LOE ebx
.B1.69:                         ; Preds .B1.143                 ; Infreq
        and       ebx, -2                                       ;77.1
        mov       DWORD PTR [72+esp], 0                         ;77.1
        mov       DWORD PTR [84+esp], ebx                       ;77.1
        jmp       .B1.58        ; Prob 100%                     ;77.1
                                ; LOE
.B1.70:                         ; Preds .B1.17                  ; Infreq
        mov       DWORD PTR [240+esp], 0                        ;52.5
        lea       eax, DWORD PTR [16+esp]                       ;52.5
        mov       DWORD PTR [16+esp], 35                        ;52.5
        mov       DWORD PTR [20+esp], OFFSET FLAT: __STRLITPACK_6 ;52.5
        push      32                                            ;52.5
        push      eax                                           ;52.5
        push      OFFSET FLAT: __STRLITPACK_25.0.1              ;52.5
        push      -2088435968                                   ;52.5
        push      911                                           ;52.5
        lea       edx, DWORD PTR [260+esp]                      ;52.5
        push      edx                                           ;52.5
        call      _for_write_seq_lis                            ;52.5
                                ; LOE
.B1.71:                         ; Preds .B1.70                  ; Infreq
        push      32                                            ;53.5
        xor       eax, eax                                      ;53.5
        push      eax                                           ;53.5
        push      eax                                           ;53.5
        push      -2088435968                                   ;53.5
        push      eax                                           ;53.5
        push      OFFSET FLAT: __STRLITPACK_26                  ;53.5
        call      _for_stop_core                                ;53.5
                                ; LOE
.B1.144:                        ; Preds .B1.71                  ; Infreq
        add       esp, 48                                       ;53.5
        jmp       .B1.18        ; Prob 100%                     ;53.5
                                ; LOE
.B1.72:                         ; Preds .B1.15                  ; Infreq
        mov       DWORD PTR [240+esp], 0                        ;47.5
        lea       eax, DWORD PTR [8+esp]                        ;47.5
        mov       DWORD PTR [8+esp], 30                         ;47.5
        mov       DWORD PTR [12+esp], OFFSET FLAT: __STRLITPACK_10 ;47.5
        push      32                                            ;47.5
        push      eax                                           ;47.5
        push      OFFSET FLAT: __STRLITPACK_22.0.1              ;47.5
        push      -2088435968                                   ;47.5
        push      911                                           ;47.5
        lea       edx, DWORD PTR [260+esp]                      ;47.5
        push      edx                                           ;47.5
        call      _for_write_seq_lis                            ;47.5
                                ; LOE
.B1.73:                         ; Preds .B1.72                  ; Infreq
        push      32                                            ;48.5
        xor       eax, eax                                      ;48.5
        push      eax                                           ;48.5
        push      eax                                           ;48.5
        push      -2088435968                                   ;48.5
        push      eax                                           ;48.5
        push      OFFSET FLAT: __STRLITPACK_23                  ;48.5
        call      _for_stop_core                                ;48.5
                                ; LOE
.B1.145:                        ; Preds .B1.73                  ; Infreq
        add       esp, 48                                       ;48.5
        jmp       .B1.16        ; Prob 100%                     ;48.5
                                ; LOE
.B1.74:                         ; Preds .B1.13                  ; Infreq
        mov       DWORD PTR [240+esp], 0                        ;42.5
        lea       eax, DWORD PTR [esp]                          ;42.5
        mov       DWORD PTR [esp], 30                           ;42.5
        mov       DWORD PTR [4+esp], OFFSET FLAT: __STRLITPACK_14 ;42.5
        push      32                                            ;42.5
        push      eax                                           ;42.5
        push      OFFSET FLAT: __STRLITPACK_19.0.1              ;42.5
        push      -2088435968                                   ;42.5
        push      911                                           ;42.5
        lea       edx, DWORD PTR [260+esp]                      ;42.5
        push      edx                                           ;42.5
        call      _for_write_seq_lis                            ;42.5
                                ; LOE
.B1.75:                         ; Preds .B1.74                  ; Infreq
        push      32                                            ;43.5
        xor       eax, eax                                      ;43.5
        push      eax                                           ;43.5
        push      eax                                           ;43.5
        push      -2088435968                                   ;43.5
        push      eax                                           ;43.5
        push      OFFSET FLAT: __STRLITPACK_20                  ;43.5
        call      _for_stop_core                                ;43.5
                                ; LOE
.B1.146:                        ; Preds .B1.75                  ; Infreq
        add       esp, 48                                       ;43.5
        jmp       .B1.14        ; Prob 100%                     ;43.5
                                ; LOE
.B1.76:                         ; Preds .B1.8                   ; Infreq
        cmp       edx, 4                                        ;36.1
        jl        .B1.92        ; Prob 10%                      ;36.1
                                ; LOE edx ecx
.B1.77:                         ; Preds .B1.76                  ; Infreq
        mov       eax, ecx                                      ;36.1
        and       eax, 15                                       ;36.1
        je        .B1.80        ; Prob 50%                      ;36.1
                                ; LOE eax edx ecx
.B1.78:                         ; Preds .B1.77                  ; Infreq
        test      al, 3                                         ;36.1
        jne       .B1.92        ; Prob 10%                      ;36.1
                                ; LOE eax edx ecx
.B1.79:                         ; Preds .B1.78                  ; Infreq
        neg       eax                                           ;36.1
        add       eax, 16                                       ;36.1
        shr       eax, 2                                        ;36.1
                                ; LOE eax edx ecx
.B1.80:                         ; Preds .B1.79 .B1.77           ; Infreq
        lea       ebx, DWORD PTR [4+eax]                        ;36.1
        cmp       edx, ebx                                      ;36.1
        jl        .B1.92        ; Prob 10%                      ;36.1
                                ; LOE eax edx ecx
.B1.81:                         ; Preds .B1.80                  ; Infreq
        mov       esi, edx                                      ;36.1
        sub       esi, eax                                      ;36.1
        and       esi, 3                                        ;36.1
        neg       esi                                           ;36.1
        add       esi, edx                                      ;36.1
        test      eax, eax                                      ;36.1
        jbe       .B1.85        ; Prob 10%                      ;36.1
                                ; LOE eax edx ecx esi
.B1.82:                         ; Preds .B1.81                  ; Infreq
        xor       ebx, ebx                                      ;
                                ; LOE eax edx ecx ebx esi
.B1.83:                         ; Preds .B1.83 .B1.82           ; Infreq
        mov       DWORD PTR [ecx+ebx*4], 0                      ;36.1
        inc       ebx                                           ;36.1
        cmp       ebx, eax                                      ;36.1
        jb        .B1.83        ; Prob 82%                      ;36.1
                                ; LOE eax edx ecx ebx esi
.B1.85:                         ; Preds .B1.83 .B1.81           ; Infreq
        pxor      xmm0, xmm0                                    ;36.1
                                ; LOE eax edx ecx esi xmm0
.B1.86:                         ; Preds .B1.86 .B1.85           ; Infreq
        movdqa    XMMWORD PTR [ecx+eax*4], xmm0                 ;36.1
        add       eax, 4                                        ;36.1
        cmp       eax, esi                                      ;36.1
        jb        .B1.86        ; Prob 82%                      ;36.1
                                ; LOE eax edx ecx esi xmm0
.B1.88:                         ; Preds .B1.86 .B1.92           ; Infreq
        cmp       esi, edx                                      ;36.1
        jae       .B1.10        ; Prob 10%                      ;36.1
                                ; LOE edx ecx esi
.B1.90:                         ; Preds .B1.88 .B1.90           ; Infreq
        mov       DWORD PTR [ecx+esi*4], 0                      ;36.1
        inc       esi                                           ;36.1
        cmp       esi, edx                                      ;36.1
        jb        .B1.90        ; Prob 82%                      ;36.1
        jmp       .B1.10        ; Prob 100%                     ;36.1
                                ; LOE edx ecx esi
.B1.92:                         ; Preds .B1.76 .B1.80 .B1.78    ; Infreq
        xor       esi, esi                                      ;36.1
        jmp       .B1.88        ; Prob 100%                     ;36.1
                                ; LOE edx ecx esi
.B1.95:                         ; Preds .B1.62                  ; Infreq
        cmp       DWORD PTR [20+esp], 4                         ;34.1
        jl        .B1.112       ; Prob 10%                      ;34.1
                                ; LOE ebx esi edi
.B1.96:                         ; Preds .B1.95                  ; Infreq
        mov       eax, DWORD PTR [24+esp]                       ;34.1
        add       eax, ebx                                      ;34.1
        and       eax, 15                                       ;34.1
        je        .B1.99        ; Prob 50%                      ;34.1
                                ; LOE eax ebx esi edi
.B1.97:                         ; Preds .B1.96                  ; Infreq
        test      al, 3                                         ;34.1
        jne       .B1.112       ; Prob 10%                      ;34.1
                                ; LOE eax ebx esi edi
.B1.98:                         ; Preds .B1.97                  ; Infreq
        neg       eax                                           ;34.1
        add       eax, 16                                       ;34.1
        shr       eax, 2                                        ;34.1
                                ; LOE eax ebx esi edi
.B1.99:                         ; Preds .B1.98 .B1.96           ; Infreq
        lea       edx, DWORD PTR [4+eax]                        ;34.1
        cmp       edx, DWORD PTR [20+esp]                       ;34.1
        jg        .B1.112       ; Prob 10%                      ;34.1
                                ; LOE eax ebx esi edi
.B1.100:                        ; Preds .B1.99                  ; Infreq
        mov       edx, DWORD PTR [20+esp]                       ;34.1
        mov       ecx, edx                                      ;34.1
        sub       ecx, eax                                      ;34.1
        and       ecx, 3                                        ;34.1
        neg       ecx                                           ;34.1
        add       ecx, edx                                      ;34.1
        mov       edx, DWORD PTR [24+esp]                       ;
        test      eax, eax                                      ;34.1
        mov       DWORD PTR [esp], ecx                          ;34.1
        lea       ecx, DWORD PTR [edx+ebx]                      ;
        mov       DWORD PTR [28+esp], ecx                       ;
        jbe       .B1.104       ; Prob 10%                      ;34.1
                                ; LOE eax ecx ebx esi edi cl ch
.B1.101:                        ; Preds .B1.100                 ; Infreq
        xor       edx, edx                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B1.102:                        ; Preds .B1.102 .B1.101         ; Infreq
        mov       DWORD PTR [ecx+edx*4], 0                      ;34.1
        inc       edx                                           ;34.1
        cmp       edx, eax                                      ;34.1
        jb        .B1.102       ; Prob 82%                      ;34.1
                                ; LOE eax edx ecx ebx esi edi
.B1.104:                        ; Preds .B1.102 .B1.100         ; Infreq
        mov       edx, DWORD PTR [esp]                          ;
        mov       ecx, DWORD PTR [28+esp]                       ;
        pxor      xmm0, xmm0                                    ;
                                ; LOE eax edx ecx ebx esi edi xmm0
.B1.105:                        ; Preds .B1.105 .B1.104         ; Infreq
        movdqa    XMMWORD PTR [ecx+eax*4], xmm0                 ;34.1
        add       eax, 4                                        ;34.1
        cmp       eax, edx                                      ;34.1
        jb        .B1.105       ; Prob 82%                      ;34.1
                                ; LOE eax edx ecx ebx esi edi xmm0
.B1.106:                        ; Preds .B1.105                 ; Infreq
        mov       DWORD PTR [esp], edx                          ;
                                ; LOE ebx esi edi
.B1.107:                        ; Preds .B1.106 .B1.112         ; Infreq
        mov       eax, DWORD PTR [esp]                          ;34.1
        cmp       eax, DWORD PTR [20+esp]                       ;34.1
        jae       .B1.64        ; Prob 10%                      ;34.1
                                ; LOE eax ebx esi edi al ah
.B1.108:                        ; Preds .B1.107                 ; Infreq
        mov       edx, DWORD PTR [20+esp]                       ;
                                ; LOE eax edx ebx esi edi
.B1.109:                        ; Preds .B1.109 .B1.108         ; Infreq
        mov       DWORD PTR [edi+eax*4], 0                      ;34.1
        inc       eax                                           ;34.1
        cmp       eax, edx                                      ;34.1
        jb        .B1.109       ; Prob 82%                      ;34.1
                                ; LOE eax edx ebx esi edi
.B1.110:                        ; Preds .B1.109                 ; Infreq
        mov       edx, DWORD PTR [12+esp]                       ;34.1
        inc       edx                                           ;34.1
        mov       eax, DWORD PTR [16+esp]                       ;34.1
        add       edi, eax                                      ;34.1
        add       esi, eax                                      ;34.1
        add       ebx, eax                                      ;34.1
        mov       DWORD PTR [12+esp], edx                       ;34.1
        cmp       edx, DWORD PTR [8+esp]                        ;34.1
        jb        .B1.62        ; Prob 82%                      ;34.1
        jmp       .B1.5         ; Prob 100%                     ;34.1
                                ; LOE ebx esi edi
.B1.112:                        ; Preds .B1.95 .B1.99 .B1.97    ; Infreq
        xor       eax, eax                                      ;34.1
        mov       DWORD PTR [esp], eax                          ;34.1
        jmp       .B1.107       ; Prob 100%                     ;34.1
                                ; LOE ebx esi edi
.B1.117:                        ; Preds .B1.59                  ; Infreq
        mov       edx, ebx                                      ;77.1
        mov       eax, ebx                                      ;77.1
        shr       edx, 1                                        ;77.1
        and       eax, 1                                        ;77.1
        and       edx, 1                                        ;77.1
        add       eax, eax                                      ;77.1
        shl       edx, 2                                        ;77.1
        or        edx, eax                                      ;77.1
        or        edx, 262144                                   ;77.1
        push      edx                                           ;77.1
        push      DWORD PTR [204+esp]                           ;77.1
        call      _for_dealloc_allocatable                      ;77.1
                                ; LOE ebx
.B1.147:                        ; Preds .B1.117                 ; Infreq
        add       esp, 8                                        ;77.1
                                ; LOE ebx
.B1.118:                        ; Preds .B1.147                 ; Infreq
        and       ebx, -2                                       ;77.1
        mov       DWORD PTR [200+esp], 0                        ;77.1
        mov       DWORD PTR [212+esp], ebx                      ;77.1
        add       esp, 356                                      ;77.1
        pop       ebx                                           ;77.1
        pop       edi                                           ;77.1
        pop       esi                                           ;77.1
        mov       esp, ebp                                      ;77.1
        pop       ebp                                           ;77.1
        ret                                                     ;77.1
        ALIGN     16
                                ; LOE
; mark_end;
_READ_HIST_TIME ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
READ_HIST_TIME$format_pack.0.1	DB	54
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	10
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	8
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	-24
	DB	3
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	2
	DB	-24
	DB	3
	DB	0
	DB	0
	DB	8
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	2
	DB	-24
	DB	3
	DB	0
	DB	0
	DB	8
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
_DATA	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
__STRLITPACK_18.0.1	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	26
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_19.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_21.0.1	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	26
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_22.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_24.0.1	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	26
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_25.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_27.0.1	DB	9
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_28.0.1	DB	9
	DB	5
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_29.0.1	DB	9
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_30.0.1	DB	9
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_31.0.1	DB	9
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_32.0.1	DB	9
	DB	5
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_33.0.1	DB	2
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_34.0.1	DB	26
	DB	5
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_35.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__NLITPACK_0.0.1	DD	2
__STRLITPACK_36.0.1	DB	2
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_37.0.1	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_38.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_39.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _READ_HIST_TIME
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_17	DB	65
	DB	108
	DB	116
	DB	84
	DB	105
	DB	109
	DB	101
	DB	95
	DB	112
	DB	114
	DB	101
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
__STRLITPACK_16	DB	111
	DB	108
	DB	100
	DB	0
__STRLITPACK_14	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	119
	DB	104
	DB	101
	DB	110
	DB	32
	DB	111
	DB	112
	DB	101
	DB	110
	DB	105
	DB	110
	DB	103
	DB	32
	DB	65
	DB	108
	DB	116
	DB	84
	DB	105
	DB	109
	DB	101
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_20	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_13	DB	65
	DB	108
	DB	116
	DB	80
	DB	97
	DB	116
	DB	104
	DB	95
	DB	112
	DB	114
	DB	101
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
__STRLITPACK_12	DB	111
	DB	108
	DB	100
	DB	0
__STRLITPACK_10	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	119
	DB	104
	DB	101
	DB	110
	DB	32
	DB	111
	DB	112
	DB	101
	DB	110
	DB	105
	DB	110
	DB	103
	DB	32
	DB	65
	DB	108
	DB	116
	DB	80
	DB	97
	DB	116
	DB	104
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_23	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_9	DB	65
	DB	108
	DB	116
	DB	84
	DB	105
	DB	109
	DB	101
	DB	95
	DB	104
	DB	105
	DB	115
	DB	116
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_8	DB	117
	DB	110
	DB	107
	DB	110
	DB	111
	DB	119
	DB	110
	DB	0
__STRLITPACK_6	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	119
	DB	104
	DB	101
	DB	110
	DB	32
	DB	111
	DB	112
	DB	101
	DB	110
	DB	105
	DB	110
	DB	103
	DB	32
	DB	65
	DB	108
	DB	116
	DB	84
	DB	105
	DB	109
	DB	101
	DB	95
	DB	104
	DB	105
	DB	115
	DB	116
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
__STRLITPACK_26	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
EXTRN	_DYNUST_MAIN_MODULE_mp_MAXVEHICLESFILE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ERROR:BYTE
_DATA	ENDS
EXTRN	_for_rewind:PROC
EXTRN	_for_close:PROC
EXTRN	_for_dealloc_allocatable:PROC
EXTRN	_for_write_seq_fmt_xmit:PROC
EXTRN	_for_write_seq_fmt:PROC
EXTRN	_for_allocate:PROC
EXTRN	_for_read_seq_fmt_xmit:PROC
EXTRN	_for_read_seq_fmt:PROC
EXTRN	_for_stop_core:PROC
EXTRN	_for_write_seq_lis:PROC
EXTRN	_for_open:PROC
EXTRN	_for_alloc_allocatable:PROC
EXTRN	_for_check_mult_overflow:PROC
EXTRN	_IMSLSORT:PROC
EXTRN	__intel_fast_memset:PROC
EXTRN	__fltused:BYTE
	INCLUDELIB <ifconsol>
	INCLUDELIB <libifcoremt>
	INCLUDELIB <libifport>
	INCLUDELIB <libmmt>
	INCLUDELIB <LIBCMT>
	INCLUDELIB <libirc>
	INCLUDELIB <svml_dispmt>
	INCLUDELIB <OLDNAMES>
	END
