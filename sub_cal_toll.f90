SUBROUTINE CAL_TOLL
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~ Copyright (C) 2002-2011 The Arizona Board of Regents on Behalf of The University of Arizona.  ~~!
!~~ Developed by Yi-Chang Chiu, Ph.D. as par to the DynusT project.                               ~~!
!~~ This program is free software: you can redistribute it and/or modify it                       ~~!
!~~ under the terms of the GNU General Public License version 2 as published by                   ~~!
!~~ the Free Software Foundation.                                                                 ~~!
!~~ This program is distributed in the hope that it will be useful,                               ~~!
!~~ but WITHOUT ANY WARRANTY; without even the implied warranty of                                ~~!
!~~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                                          ~~!
!~~ See the GNU General Public License for more details.                                          ~~!
!~~ You should have received a copy of the GNU General Public License                             ~~!
!~~ along with this program (in the file COPYING).                                                ~~!
!~~ IF not, see <http://www.gnu.org/licenses/>.                                                   ~~!
!~~ Please see http://www.dynust.net for contact information and names                            ~~!
!~~ of individual contributors.                                                                   ~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
! --
      USE DYNUST_MAIN_MODULE
      USE DYNUST_NETWORK_MODULE
      LOGICAL ifind
      INTEGER dynust_mode_flag,isnow,eloc,sloc,loop
!> > > > > > > > > >                < < < < < < < < < < <
!>>>>>>>>>>>>>>>>>>>CODES START HERE<<<<<<<<<<<<<<<<<<<<<
!> > > > > > > > > >                < < < < < < < < < < <
      
	  IF(NTollLink > 0) THEN
	  
	  DO 100 IP = 1, NTollLink
            iab=m_dynust_network_arc_nde(TollLink(IP)%LinkNO)%ForToBackLink
            DO 20 iok = 1, TollLink(IP)%NTD
             IF(TollLink(IP)%scheme(iok)%sov > 0.and.TollVOTA < 1) THEN
               WRITE(911,*) 'Error in Toll.dat, Auto VOT is zero'
			   STOP
             ELSEIF(TollLink(IP)%scheme(iok)%tck > 0.and.TollVOTT < 1) THEN
               WRITE(911,*) 'Error in Toll.dat, Truck VOT is zero'
			   STOP
              ELSEIF(TollLink(IP)%scheme(iok)%hov > 0.and.TollVOTH < 1) THEN
               WRITE(911,*) 'Error in Toll.dat, Truck VOT is zero'
			   STOP
             ENDIF            
             istime = max(1,     nint(TollLink(IP)%scheme(iok)%stime/xminPerSimInt/simPerAgg)) ! start time of toll
             ietime = min(aggint,nint(TollLink(IP)%scheme(iok)%etime/xminPerSimInt/simPerAgg)) ! end of toll
             m_dynust_network_arc_de(iab)%costexp(istime:ietime,1) = TollLink(IP)%scheme(iok)%sov/(TollVOTA/60.0)
             m_dynust_network_arc_de(iab)%costexp(istime:ietime,2) = TollLink(IP)%scheme(iok)%tck/(TollVOTT/60.0)
             m_dynust_network_arc_de(iab)%costexp(istime:ietime,3) = TollLink(IP)%scheme(iok)%hov/(TollVOTH/60.0)            
20			ENDDO
100      ENDDO
      ENDIF
	
END SUBROUTINE


SUBROUTINE Toll_link_pricing(IvehType,dynust_mode_flag,isnow,loop)
  

      USE DYNUST_MAIN_MODULE
      USE DYNUST_NETWORK_MODULE
      LOGICAL ifind
      INTEGER dynust_mode_flag,isnow,eloc,sloc,loop
      
      ifind=.false.

	  cost(:,:) = 0

	  IF(NTollLink > 0) THEN
	  
	  DO 100 IP = 1, NTollLink
            iab=m_dynust_network_arc_nde(TollLink(IP)%LinkNO)%ForToBackLink
            IF(IVehType == 1.and.TollVOTA < 1) THEN
              WRITE(911,*) 'Error in Toll.dat, Auto VOT is zero'
			  STOP
            ELSEIF(IVehType == 2.and.TollVOTT < 1) THEN
              WRITE(911,*) 'Error in Toll.dat, Truck VOT is zero'
			  STOP
			ELSEIF(IVehType == 3.and.TollVOTH < 1) THEN
		      WRITE(911,*) 'Error in Toll.dat, HOV VOT is zero'
			  STOP	
            ENDIF
            DO 20 iok = 1, TollLink(IP)%NTD
              IF(dynust_mode_flag > 0)THEN ! called from DUE
                ifind  =  .false.  
                sloc = nint((isnow-1)*EpocPeriod/xminPerSimInt/float(simPerAgg)) ! start time of epoc
                eloc = sloc + iti_nu ! end of the projection period
                igetstime = max(1,nint(TollLink(IP)%scheme(iok)%stime/xminPerSimInt/simPerAgg)) ! start time of toll
                igetetime = nint(TollLink(IP)%scheme(iok)%etime/xminPerSimInt/simPerAgg) ! end of toll
                istime = max(1,(max(sloc,igetstime)-sloc))
                ietime = min(eloc,igetetime)-sloc
                ifind=.true.
              ELSE !dynust_mode_flag = 0 CALL from sim
                IF(loop >= TollLink(IP)%scheme(iok)%stime/xminPerSimInt.and.loop < TollLink(IP)%scheme(iok)%etime/xminPerSimInt) THEN ! check IF this is in the toll activation time
                 istime = 1
                 ietime = 1
                 ifind=.true.
                ELSE
                 ifind=.false.              
                ENDIF
              ENDIF ! dynust_mode_flag > 0
              
              IF(ifind)  THEN
               IF(IVehType == 1) THEN !Auto only or Auto and Truck but same VOT
                 WRITE(511,*)"in auto"
                 cost(iab,istime:ietime) = cost(iab,istime:ietime) + TollLink(IP)%scheme(iok)%sov/(TollVOTA/60.0)
			   ELSEIF(IvehType == 2) THEN !Truck
                 WRITE(511,*) "in truck"
                 cost(iab,istime:ietime) = cost(iab,istime:ietime) + TollLink(IP)%scheme(iok)%tck/(TollVOTT/60.0)
               ELSE
                 WRITE(511,*) "in hov"
                 cost(iab,istime:ietime) = cost(iab,istime:ietime) + TollLink(IP)%scheme(iok)%hov/(TollVOTH/60.0)            
			   ENDIF  !IVehType
			  ENDIF  !ifind
20			ENDDO
100      ENDDO
      ENDIF
	
END SUBROUTINE
