    SUBROUTINE OPENFILE()
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~ Copyright (C) 2002-2011 The Arizona Board of Regents on Behalf of The University of Arizona.  ~~!
!~~ Developed by Yi-Chang Chiu, Ph.D. as par to the DynusT project.                               ~~!
!~~ This program is free software: you can redistribute it and/or modify it                       ~~!
!~~ under the terms of the GNU General Public License version 2 as published by                   ~~!
!~~ the Free Software Foundation.                                                                 ~~!
!~~ This program is distributed in the hope that it will be useful,                               ~~!
!~~ but WITHOUT ANY WARRANTY; without even the implied warranty of                                ~~!
!~~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                                          ~~!
!~~ See the GNU General Public License for more details.                                          ~~!
!~~ You should have received a copy of the GNU General Public License                             ~~!
!~~ along with this program (in the file COPYING).                                                ~~!
!~~ IF not, see <http://www.gnu.org/licenses/>.                                                   ~~!
!~~ Please see http://www.dynust.net for contact information and names                            ~~!
!~~ of individual contributors.                                                                   ~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
      
	USE DYNUST_MAIN_MODULE

    LOGICAL SIRExist,ext2

!> > > > > > > > > >                < < < < < < < < < < <
!>>>>>>>>>>>>>>>>>>>CODES START HERE<<<<<<<<<<<<<<<<<<<<<
!> > > > > > > > > >                < < < < < < < < < < <
    
    IF(iteration == 0) THEN
	OPEN(file='ErrorLog.dat',unit=911,status='unknown',iostat=error)
	IF(error /= 0) THEN
         WRITE(911,*) 'Error when opening ErrorLog.dat'
	   STOP
	ENDIF
	OPEN(file='EpochOut.dat',unit=5559,status='unknown',iostat=error)
	IF(error /= 0) THEN
         WRITE(911,*) 'Error when opening EpochOut.dat'
	   STOP
	ENDIF
	ENDIF

	OPEN(file='network.dat',unit=41,status='old',iostat=error) 
	IF(error /= 0) THEN
         WRITE(911,*) 'Error when opening network.dat'
	   STOP
	ENDIF

	OPEN(file='demand.dat',unit=42,status='old',iostat=error) 
	IF(error /= 0) THEN
         WRITE(911,*) 'Error when opening demand.dat'
	   STOP
	ENDIF

	OPEN(file='scenario.dat',unit=43,status='old',iostat=error) 
	IF(error /= 0) THEN
         WRITE(911,*) 'Error when opening scenario.dat'
	   STOP
	ENDIF

	OPEN(file='control.dat',unit=44,status='old',iostat=error) 
	IF(error /= 0) THEN
         WRITE(911,*) 'Error when opening control.dat'
	   STOP
	ENDIF

	OPEN(file='ramp.dat',unit=45,status='old',iostat=error) 
	IF(error /= 0) THEN
         WRITE(911,*) 'Error when opening ramp.dat'
	   STOP
	ENDIF

	OPEN(file='incident.dat',unit=46,status='old',iostat=error) 
	IF(error /= 0) THEN
         WRITE(911,*) 'Error when opening incident.dat'
	   STOP
	ENDIF

	OPEN(file='movement.dat',unit=47,status='old',iostat=error) 
	IF(error /= 0) THEN
         WRITE(911,*) 'Error when opening movement.dat'
	   STOP
	ENDIF

	OPEN(file='leftcap.dat',unit=48,status='old',iostat=error) 
	IF(error /= 0) THEN
         WRITE(911,*) 'Error when opening leftcap.dat'
	   STOP
	ENDIF

	OPEN(file='vms.dat',unit=49,status='old',iostat=error) 
	IF(error /= 0) THEN
         WRITE(911,*) 'Error when opening vms.dat'
	   STOP
	ENDIF

	OPEN(file='origin.dat',unit=52,status='old',iostat=error)
	IF(error /= 0) THEN
         WRITE(911,*) 'Error when opening origin.dat'
	   STOP
	ENDIF

	OPEN(file='destination.dat',unit=53,status='old',iostat=error)
	IF(error /= 0) THEN
         WRITE(911,*) 'Error when opening destination.dat'
	   STOP
	ENDIF
    
    INQUIRE(file='demand_truck.dat', EXIST=EXT2)
    IF(EXT2) THEN
	OPEN(file='demand_truck.dat',unit=54,status='old',iostat=error) 
	IF(error /= 0) THEN
         WRITE(911,*) 'Error when opening demand_truck.dat'
	   STOP
	ENDIF
	ENDIF

    INQUIRE(file='demand_hov.dat', EXIST=EXT2)
    IF(EXT2) THEN
	OPEN(file='demand_hov.dat',unit=544,status='old',iostat=error) 
	IF(error /= 0) THEN
         WRITE(911,*) 'Error when opening demand_hov.dat'
	   STOP
	ENDIF
	ENDIF

    INQUIRE(file='demand_superzone.dat', EXIST=EXT2)
    IF(EXT2) THEN
	OPEN(file='demand_superzone.dat',unit=545,status='old',iostat=error) 
	IF(error /= 0) THEN
         WRITE(911,*) 'Error when opening demand_superzone.dat'
	   STOP
	ENDIF
	ENDIF

	OPEN(file='STOPCap4Way.dat',unit=56,status='old',iostat=error) 
	IF(error /= 0) THEN
         WRITE(911,*) 'Error when opening STOPCap4Way.dat'
	   STOP
	ENDIF

	OPEN(file='STOPCap2Way.dat',unit=57,status='old',iostat=error) 
	IF(error /= 0) THEN
         WRITE(911,*) 'Error when opening STOPCap2Way.dat'
	   STOP
	ENDIF

	OPEN(file='YieldCap.dat',unit=60,status='old',iostat=error) 
	IF(error /= 0) THEN
         WRITE(911,*) 'Error when opening YieldCap.dat'
	   STOP
	ENDIF


	OPEN(file='WorkZone.dat',unit=58,status='old',iostat=error) 
	IF(error /= 0) THEN
         WRITE(911,*) 'Error when opening WorkZone.dat'
	   STOP
	ENDIF

    OPEN(file='GradeLengthPCE.dat',unit=59,status='old',iostat=error) 
	IF(error /= 0) THEN
         WRITE(911,*) 'Error when opening GradeLengthPCE.dat'
	   STOP
	ENDIF

	OPEN(file='system.dat',unit=95,status='old',iostat=error)
	IF(error /= 0) THEN
         WRITE(911,*) 'Error when opening system.dat'
	   STOP
	ENDIF

	OPEN(file='output_option.dat',unit=101,status='old',iostat=error)
	IF(error /= 0) THEN
         WRITE(911,*) 'Error when opening output_option.dat'
	   STOP
	ENDIF

    OPEN(file='vehicle.dat',unit=500,status='unknown',iostat=error)
	IF(error /= 0) THEN
         WRITE(911,*) 'Error when opening vehicle.dat'
	   STOP
	ENDIF

	OPEN(file='path.dat',unit=550,status='unknown',iostat=error)
	IF(error /= 0) THEN
         WRITE(911,*) 'Error when opening path.dat'
	   STOP
	ENDIF

	OPEN(file='Executing',unit=912,status='unknown',iostat=error)
	IF(error /= 0) THEN
         WRITE(911,*) 'Error when opening executing.dat'
	   STOP
	ENDIF

	WRITE(912,*) 'DYNUST IS RUNNING....' 
	close(912)

	OPEN(file='VehTrajectory.dat',unit=18,status='unknown',iostat=error)
	IF(error /= 0) THEN
         WRITE(911,*) 'Error when opening VehTrajectory.dat'
	   STOP
	ENDIF

	OPEN(file='BusTrajectory.dat',unit=188,status='unknown',iostat=error)
	IF(error /= 0) THEN
         WRITE(911,*) 'Error when opening BusTrajectory.dat'
	   STOP
	ENDIF

      OPEN(file='SummaryStat.dat',unit=666,status='unknown',iostat=error)
	IF(error /= 0) THEN
         WRITE(911,*) 'Error when opening SummaryStat.dat'
	   STOP
	ENDIF

	OPEN(file='OutMUC.dat',unit=180,status='unknown',iostat=error)
	IF(error /= 0) THEN
         WRITE(911,*) 'Error when opening Outmtc.dat'
	   STOP
	ENDIF

 	OPEN(file='fort.600',unit=600,status='unknown',iostat=error)
	IF(error /= 0) THEN
         WRITE(911,*) 'Error when opening fort.600'
	   STOP
	ENDIF

	OPEN(file='fort.700',unit=700,status='unknown',iostat=error)
	IF(error /= 0) THEN
         WRITE(911,*) 'Error when opening fort.700'
	   STOP
	ENDIF

	OPEN(file='fort.800',unit=800,status='unknown',iostat=error)
	IF(error /= 0) THEN
         WRITE(911,*) 'Error when opening fort.800'
	   STOP
	ENDIF

	OPEN(file='fort.900',unit=900,status='unknown',iostat=error)
	IF(error /= 0) THEN
         WRITE(911,*) 'Error when opening fort.900'
	   STOP
	ENDIF

	OPEN(file='hazmat.dat',unit=461,status='unknown',iostat=error)
	IF(error /= 0) THEN
         WRITE(911,*) 'Error when opening hazmat.dat'
	   STOP
	ENDIF

	INQUIRE (FILE='toll.dat', EXIST = TollExist)
        IF(TollExist) OPEN(file='toll.dat',unit=62,status='old',iostat=error)
	IF(error /= 0) THEN
         WRITE(911,*) 'Error when opening toll.dat'
	   STOP
	ENDIF
    
    OPEN(file='VehPos.dat',unit=888,status='unknown',iostat=error)
	IF(error /= 0) THEN
         WRITE(911,*) 'Error when opening VehPos.dat'
	   STOP
	ENDIF
	WRITE(888,*)
    
    OPEN(file='output_vehicle.dat',unit=97,status='unknown')
	OPEN(file='output_path.dat',unit=98,status='unknown')
    
	INQUIRE (FILE='SIR.dat', EXIST = SIRExist)
        IF(SIRExist) OPEN(file='SIR.dat',unit=63,status='old',iostat=error)
	IF(error /= 0) THEN
         WRITE(911,*) 'Error when opening SIR.dat'
	   STOP
	ENDIF


    OPEN(file='SpaceTimePar.dat',unit=881,status='unknown',iostat=error)
	IF(error /= 0) THEN
         WRITE(911,*) 'Error when opening SpaceTimePar.dat'
	   STOP
	ENDIF

   
    OPEN(file='ExecLog.dat',unit=999,status='unknown',iostat=error)
	IF(error /= 0) THEN
         WRITE(911,*) 'Error when opening execlog.dat'
	   STOP
	ENDIF

    OPEN(file='AltPath.dat',unit=989,status='unknown',iostat=error)
	IF(error /= 0) THEN
         WRITE(911,*) 'Error when opening AltPath.dat'
	   STOP
	ENDIF

    OPEN(file='AltTime.dat',unit=988,status='unknown',iostat=error)
	IF(error /= 0) THEN
         WRITE(911,*) 'Error when opening AltTime.dat'
	   STOP
	ENDIF

    OPEN(file='AltEnQ.dat',unit=990,status='unknown',iostat=error)
	IF(error /= 0) THEN
         WRITE(911,*) 'Error when opening AltEnq.dat'
	   STOP
	ENDIF

    OPEN(file='mtcph.itf',unit=6099,status='unknown',iostat=error)
	IF(error /= 0) THEN
         WRITE(911,*) 'Error when opening mtcph.ift.dat'
	   STOP
	ENDIF

    OPEN(file='xy.dat',unit=7099,status='unknown',iostat=error)
	IF(error /= 0) THEN
         WRITE(911,*) 'Error when opening xy.dat'
	   STOP
	ENDIF

    END SUBROUTINE
