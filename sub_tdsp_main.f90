      SUBROUTINE TDSP_MAIN(dynust_mode_flag,loop)
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~ Copyright (C) 2002-2011 The Arizona Board of Regents on Behalf of The University of Arizona.  ~~!
!~~ Developed by Yi-Chang Chiu, Ph.D. as par to the DynusT project.                               ~~!
!~~ This program is free software: you can redistribute it and/or modify it                       ~~!
!~~ under the terms of the GNU General Public License version 2 as published by                   ~~!
!~~ the Free Software Foundation.                                                                 ~~!
!~~ This program is distributed in the hope that it will be useful,                               ~~!
!~~ but WITHOUT ANY WARRANTY; without even the implied warranty of                                ~~!
!~~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                                          ~~!
!~~ See the GNU General Public License for more details.                                          ~~!
!~~ You should have received a copy of the GNU General Public License                             ~~!
!~~ along with this program (in the file COPYING).                                                ~~!
!~~ IF not, see <http://www.gnu.org/licenses/>.                                                   ~~!
!~~ Please see http://www.dynust.net for contact information and names                            ~~!
!~~ of individual contributors.                                                                   ~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
      USE DYNUST_MAIN_MODULE
      USE DYNUST_NETWORK_MODULE
      USE IFQWIN 
      USE DYNUST_TDSP_ASTAR_MODULE      

      INTERFACE
        SUBROUTINE printscreen(printstr,intx,inty,dx1,dy1,dx2,dy2,refresh)
         INTEGER intx, inty, refresh,dx1,dy1,dx2,dy2
         CHARACTER (80) printstr
        END SUBROUTINE 
      END INTERFACE  

      INTEGER dynust_mode_flag,iloop
      CHARACTER (80) printstr1, printstr2
!> > > > > > > > > >                < < < < < < < < < < <
!>>>>>>>>>>>>>>>>>>>CODES START HERE<<<<<<<<<<<<<<<<<<<<<
!> > > > > > > > > >                < < < < < < < < < < <
		     

    aggindex = max(1,ifix((loop-2)*1.0/simPerAgg)+1)

	IF(dynust_mode_flag == 0) THEN  !dynust
        kpaths=kay
	  DO ilink=1,noofarcs
         DO itt=1,Iti_nu
            iseethis = m_dynust_network_arc_nde(ilink)%ForToBackLink
          IF(m_dynust_network_arc_nde(ilink)%link_iden == 1) THEN
		    TTime(iseethis,itt)=TravelTime(ilink,aggindex)/AttScale*(1.0-TravelTimeWeight/100.0)
          ELSE
		    TTime(iseethis,itt)=TravelTime(ilink,aggindex)/AttScale
		  ENDIF
         ENDDO
	  ENDDO
    ELSE
      kpaths = kay	  
	  DO ilink=1,noofarcs
         DO itt=1,Iti_nu
	    IF(m_dynust_network_arc_nde(ilink)%link_iden == 1) THEN
          TTime(m_dynust_network_arc_nde(ilink)%ForToBackLink,itt)=TravelTime(ilink,itt)/AttScale*(1-TravelTimeWeight/100.0)
	    ELSE
	      TTime(m_dynust_network_arc_nde(ilink)%ForToBackLink,itt)=TravelTime(ilink,itt)/AttScale
	    ENDIF
         ENDDO
        ENDDO
    ENDIF


      IF(dynust_mode_flag == 0) THEN
         DO itt=1,Iti_nu
	         TTPenalty(:,itt,:) = m_dynust_network_arcmove_nde(:,:)%penalty
         ENDDO
      ELSE
         TTPenalty(:,:,:) = TravelPenalty(:,:,:)
	  ENDIF 

       DO ilink=1,noofarcs
        DO itt=1,Iti_nu
         DO movee=1,maxmove
	    IF(dynust_mode_flag == 2) THEN ! SO case
	    
          ELSE ! UE or static case
           TTmarginal(itt,ilink,movee)=TTime(ilink,itt)+TTPenalty(ilink,itt,movee)
	    ENDIF
          ENDDO
        ENDDO
       ENDDO

	  CALL Toll_Link_Pricing(2,dynust_mode_flag,1,loop) ! use truck toll rate in the initial sim

END SUBROUTINE
         

SUBROUTINE tdsp_core(dynust_mode_flag,iloop)

USE DYNUST_MAIN_MODULE
USE IFQWIN     
          
INTEGER dynust_mode_flag
INTEGER iloop
CHARACTER (80) printstr1, printstr2

    
	     
          ides = 1
             
          destin=destination(iloop)

	       IF(destin /= 0)THEN
            IF(dynust_mode_flag == 0) THEN
             IF(mod(iloop,100) == 0.or.iloop == noof_master_destinations) THEN
               WRITE(printstr1,'(i6)') iloop
               printstr1 = 'Now Calling SP, Dest =  '//printstr1			      
               CALL printscreen(printstr1,20,395,20,20,200,40,1)	
             ENDIF
            ENDIF
                      
             !CALL TDSP_INITIALIZE 
             !CALL TDSP_COMPUTE(dynust_mode_flag)
			 IF(dynust_mode_flag == 0.and.iteration == 0) THEN
               IF(netcheck > 0) THEN
                 IF(time_now <= 6) THEN
			      !CALL DIAGNOSE_NETWORK(iloop)
                  WRITE(printstr1,'(i6)') iloop
                  printstr1 = 'Checking Connectivity. Dest =  '//printstr1			      
                  CALL printscreen(printstr1,20,395,20,20,500,500,1)
	              !CALL CheckGenLinkOnConnectivity(iloop)			      
	             ENDIF
	           ENDIF
			 ENDIF
			 
	       ENDIF

END SUBROUTINE
