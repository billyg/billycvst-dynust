; mark_description "Intel(R) Visual Fortran Compiler XE for applications running on IA-32, Version 13.1.1.171 Build 20130313";
; mark_description "-S";
	.686P
 	.387
	OPTION DOTNAME
	ASSUME	CS:FLAT,DS:FLAT,SS:FLAT
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
TXTST0:
; -- Begin  _LINK_LIST$
; mark_begin;
IF @Version GE 800
  .MMX
ELSEIF @Version GE 612
  .MMX
  MMWORD TEXTEQU <QWORD>
ENDIF
IF @Version GE 800
  .XMM
ELSEIF @Version GE 614
  .XMM
  XMMWORD TEXTEQU <OWORD>
ENDIF
       ALIGN     16
	PUBLIC _LINK_LIST$
_LINK_LIST$	PROC NEAR 
.B1.1:                          ; Preds .B1.0
        ret                                                     ;1.8
        ALIGN     16
                                ; LOE
; mark_end;
_LINK_LIST$ ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _LINK_LIST$
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _LINK_LIST_mp_CREATE_QUEUE_LINKLIST
; mark_begin;
       ALIGN     16
	PUBLIC _LINK_LIST_mp_CREATE_QUEUE_LINKLIST
_LINK_LIST_mp_CREATE_QUEUE_LINKLIST	PROC NEAR 
; parameter 1: 4 + esp
.B2.1:                          ; Preds .B2.0
        mov       eax, DWORD PTR [4+esp]                        ;41.12
        mov       DWORD PTR [4+eax], eax                        ;44.5
        ret                                                     ;46.1
        ALIGN     16
                                ; LOE
; mark_end;
_LINK_LIST_mp_CREATE_QUEUE_LINKLIST ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _LINK_LIST_mp_CREATE_QUEUE_LINKLIST
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _LINK_LIST_mp_DESTROY_QUEUE_LINKLIST
; mark_begin;
       ALIGN     16
	PUBLIC _LINK_LIST_mp_DESTROY_QUEUE_LINKLIST
_LINK_LIST_mp_DESTROY_QUEUE_LINKLIST	PROC NEAR 
; parameter 1: 8 + ebp
.B3.1:                          ; Preds .B3.0
        push      ebp                                           ;49.12
        mov       ebp, esp                                      ;49.12
        and       esp, -16                                      ;49.12
        sub       esp, 48                                       ;49.12
        mov       edx, DWORD PTR [8+ebp]                        ;49.12
        mov       eax, DWORD PTR [edx]                          ;54.21
        test      eax, eax                                      ;54.21
        je        .B3.7         ; Prob 6%                       ;54.21
                                ; LOE eax edx ebx esi edi
.B3.2:                          ; Preds .B3.1
        mov       DWORD PTR [36+esp], esi                       ;
        mov       esi, eax                                      ;
        mov       DWORD PTR [32+esp], edi                       ;
        mov       edi, edx                                      ;
                                ; LOE ebx esi edi
.B3.3:                          ; Preds .B3.5 .B3.2
        test      esi, esi                                      ;55.14
        jne       .B3.8         ; Prob 8%                       ;55.14
                                ; LOE ebx esi edi
.B3.4:                          ; Preds .B3.3
        mov       DWORD PTR [esp], 0                            ;55.14
        lea       eax, DWORD PTR [esp]                          ;55.14
        mov       DWORD PTR [40+esp], 34                        ;55.14
        lea       ecx, DWORD PTR [40+esp]                       ;55.14
        mov       DWORD PTR [44+esp], OFFSET FLAT: __STRLITPACK_8 ;55.14
        push      32                                            ;55.14
        push      ecx                                           ;55.14
        push      OFFSET FLAT: __STRLITPACK_9.0.5               ;55.14
        push      -2088435968                                   ;55.14
        push      -1                                            ;55.14
        push      eax                                           ;55.14
        call      _for_write_seq_lis                            ;55.14
                                ; LOE ebx esi edi
.B3.13:                         ; Preds .B3.4
        add       esp, 24                                       ;55.14
                                ; LOE ebx esi edi
.B3.5:                          ; Preds .B3.13 .B3.10 .B3.9
        test      esi, esi                                      ;54.21
        jne       .B3.3         ; Prob 82%                      ;54.21
                                ; LOE ebx esi edi
.B3.6:                          ; Preds .B3.5
        mov       esi, DWORD PTR [36+esp]                       ;
        mov       edi, DWORD PTR [32+esp]                       ;
                                ; LOE ebx esi edi
.B3.7:                          ; Preds .B3.6 .B3.1
        mov       esp, ebp                                      ;59.1
        pop       ebp                                           ;59.1
        ret                                                     ;59.1
                                ; LOE
.B3.8:                          ; Preds .B3.3                   ; Infreq
        mov       DWORD PTR [_LINK_LIST_mp_DISQUEUE_LINKLIST$TEMP_NP.0.5], esi ;55.14
        mov       ecx, esi                                      ;55.14
        push      262144                                        ;55.14
        mov       esi, DWORD PTR [8+esi]                        ;55.14
        push      ecx                                           ;55.14
        mov       DWORD PTR [edi], esi                          ;55.14
        call      _for_deallocate                               ;55.14
                                ; LOE ebx esi edi
.B3.14:                         ; Preds .B3.8                   ; Infreq
        add       esp, 8                                        ;55.14
                                ; LOE ebx esi edi
.B3.9:                          ; Preds .B3.14                  ; Infreq
        mov       DWORD PTR [_LINK_LIST_mp_DISQUEUE_LINKLIST$TEMP_NP.0.5], 0 ;55.14
        test      esi, esi                                      ;55.14
        jne       .B3.5         ; Prob 60%                      ;55.14
                                ; LOE ebx esi edi
.B3.10:                         ; Preds .B3.9                   ; Infreq
        mov       DWORD PTR [4+edi], edi                        ;55.14
        jmp       .B3.5         ; Prob 100%                     ;55.14
        ALIGN     16
                                ; LOE ebx esi edi
; mark_end;
_LINK_LIST_mp_DESTROY_QUEUE_LINKLIST ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _LINK_LIST_mp_DESTROY_QUEUE_LINKLIST
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _LINK_LIST_mp_ENQUEUE_LINKLIST
; mark_begin;
       ALIGN     16
	PUBLIC _LINK_LIST_mp_ENQUEUE_LINKLIST
_LINK_LIST_mp_ENQUEUE_LINKLIST	PROC NEAR 
; parameter 1: 4 + esp
; parameter 2: 8 + esp
; parameter 3: 12 + esp
.B4.1:                          ; Preds .B4.0
        push      262144                                        ;68.5
        push      OFFSET FLAT: _LINK_LIST_mp_ENQUEUE_LINKLIST$TEMP_NP.0.4 ;68.5
        push      12                                            ;68.5
        call      _for_allocate                                 ;68.5
                                ; LOE ebx ebp esi edi
.B4.5:                          ; Preds .B4.1
        add       esp, 12                                       ;68.5
                                ; LOE ebx ebp esi edi
.B4.2:                          ; Preds .B4.5
        mov       eax, DWORD PTR [_LINK_LIST_mp_ENQUEUE_LINKLIST$TEMP_NP.0.4] ;68.5
        mov       ecx, DWORD PTR [12+esp]                       ;62.12
        mov       edx, DWORD PTR [_LINK_LIST_mp_ENQUEUE_LINKLIST$BLK..T133_+8] ;68.5
        mov       DWORD PTR [8+eax], edx                        ;68.5
        mov       edx, DWORD PTR [ecx]                          ;69.5
        mov       ecx, DWORD PTR [8+esp]                        ;70.5
        mov       DWORD PTR [eax], edx                          ;69.5
        mov       edx, DWORD PTR [ecx]                          ;70.5
        mov       DWORD PTR [4+eax], edx                        ;70.5
        mov       edx, DWORD PTR [4+esp]                        ;71.5
        mov       ecx, DWORD PTR [4+edx]                        ;71.5
        mov       DWORD PTR [ecx], eax                          ;71.5
        add       eax, 8                                        ;72.5
        mov       DWORD PTR [4+edx], eax                        ;72.5
        mov       DWORD PTR [eax], 0                            ;74.5
        ret                                                     ;76.1
        ALIGN     16
                                ; LOE
; mark_end;
_LINK_LIST_mp_ENQUEUE_LINKLIST ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _LINK_LIST_mp_ENQUEUE_LINKLIST
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _LINK_LIST_mp_DISQUEUE_LINKLIST
; mark_begin;
       ALIGN     16
	PUBLIC _LINK_LIST_mp_DISQUEUE_LINKLIST
_LINK_LIST_mp_DISQUEUE_LINKLIST	PROC NEAR 
; parameter 1: 8 + ebp
; parameter 2: 12 + ebp
; parameter 3: 16 + ebp
.B5.1:                          ; Preds .B5.0
        push      ebp                                           ;79.12
        mov       ebp, esp                                      ;79.12
        and       esp, -16                                      ;79.12
        push      esi                                           ;79.12
        push      ebx                                           ;79.12
        sub       esp, 40                                       ;79.12
        mov       esi, DWORD PTR [8+ebp]                        ;79.12
        mov       edx, DWORD PTR [esi]                          ;85.9
        test      edx, edx                                      ;85.9
        jne       .B5.4         ; Prob 8%                       ;85.9
                                ; LOE edx esi edi
.B5.2:                          ; Preds .B5.1
        mov       DWORD PTR [esp], 0                            ;86.9
        lea       edx, DWORD PTR [esp]                          ;86.9
        mov       DWORD PTR [32+esp], 34                        ;86.9
        lea       eax, DWORD PTR [32+esp]                       ;86.9
        mov       DWORD PTR [36+esp], OFFSET FLAT: __STRLITPACK_8 ;86.9
        push      32                                            ;86.9
        push      eax                                           ;86.9
        push      OFFSET FLAT: __STRLITPACK_9.0.5               ;86.9
        push      -2088435968                                   ;86.9
        push      -1                                            ;86.9
        push      edx                                           ;86.9
        call      _for_write_seq_lis                            ;86.9
                                ; LOE edi
.B5.9:                          ; Preds .B5.2
        add       esp, 24                                       ;86.9
                                ; LOE edi
.B5.3:                          ; Preds .B5.9 .B5.5
        add       esp, 40                                       ;97.1
        pop       ebx                                           ;97.1
        pop       esi                                           ;97.1
        mov       esp, ebp                                      ;97.1
        pop       ebp                                           ;97.1
        ret                                                     ;97.1
                                ; LOE
.B5.4:                          ; Preds .B5.1                   ; Infreq
        mov       ebx, DWORD PTR [16+ebp]                       ;79.12
        mov       eax, DWORD PTR [12+ebp]                       ;79.12
        mov       ecx, DWORD PTR [edx]                          ;90.9
        mov       DWORD PTR [ebx], ecx                          ;90.9
        push      262144                                        ;93.9
        mov       ecx, DWORD PTR [4+edx]                        ;91.9
        mov       ebx, DWORD PTR [8+edx]                        ;92.9
        push      edx                                           ;93.9
        mov       DWORD PTR [_LINK_LIST_mp_DISQUEUE_LINKLIST$TEMP_NP.0.5], edx ;89.9
        mov       DWORD PTR [eax], ecx                          ;91.9
        mov       DWORD PTR [esi], ebx                          ;92.9
        call      _for_deallocate                               ;93.9
                                ; LOE ebx esi edi
.B5.10:                         ; Preds .B5.4                   ; Infreq
        add       esp, 8                                        ;93.9
                                ; LOE ebx esi edi
.B5.5:                          ; Preds .B5.10                  ; Infreq
        mov       DWORD PTR [_LINK_LIST_mp_DISQUEUE_LINKLIST$TEMP_NP.0.5], 0 ;93.9
        test      ebx, ebx                                      ;94.13
        jne       .B5.3         ; Prob 60%                      ;94.13
                                ; LOE esi edi
.B5.6:                          ; Preds .B5.5                   ; Infreq
        mov       DWORD PTR [4+esi], esi                        ;94.43
        add       esp, 40                                       ;94.43
        pop       ebx                                           ;94.43
        pop       esi                                           ;94.43
        mov       esp, ebp                                      ;94.43
        pop       ebp                                           ;94.43
        ret                                                     ;94.43
        ALIGN     16
                                ; LOE edi
; mark_end;
_LINK_LIST_mp_DISQUEUE_LINKLIST ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _LINK_LIST_mp_DISQUEUE_LINKLIST
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _LINK_LIST_mp_DISQUEUE_TARGET
; mark_begin;
       ALIGN     16
	PUBLIC _LINK_LIST_mp_DISQUEUE_TARGET
_LINK_LIST_mp_DISQUEUE_TARGET	PROC NEAR 
; parameter 1: 8 + ebp
; parameter 2: 12 + ebp
; parameter 3: 16 + ebp
.B6.1:                          ; Preds .B6.0
        push      ebp                                           ;100.12
        mov       ebp, esp                                      ;100.12
        and       esp, -16                                      ;100.12
        push      edi                                           ;100.12
        push      ebx                                           ;100.12
        sub       esp, 40                                       ;100.12
        mov       edi, DWORD PTR [8+ebp]                        ;100.12
        mov       edx, DWORD PTR [16+ebp]                       ;100.12
        mov       ecx, DWORD PTR [edi]                          ;108.9
        test      ecx, ecx                                      ;108.9
        jne       .B6.3         ; Prob 60%                      ;108.9
                                ; LOE edx ecx esi edi
.B6.2:                          ; Preds .B6.1
        xor       eax, eax                                      ;115.13
        lea       ecx, DWORD PTR [esp]                          ;109.9
        mov       DWORD PTR [edx], eax                          ;115.13
        lea       edx, DWORD PTR [32+esp]                       ;109.9
        mov       DWORD PTR [esp], eax                          ;109.9
        mov       DWORD PTR [32+esp], 34                        ;109.9
        mov       DWORD PTR [36+esp], OFFSET FLAT: __STRLITPACK_7 ;109.9
        push      32                                            ;109.9
        push      edx                                           ;109.9
        push      OFFSET FLAT: __STRLITPACK_10.0.6              ;109.9
        push      -2088435968                                   ;109.9
        push      -1                                            ;109.9
        push      ecx                                           ;109.9
        call      _for_write_seq_lis                            ;109.9
                                ; LOE esi
.B6.22:                         ; Preds .B6.2
        add       esp, 24                                       ;109.9
        jmp       .B6.13        ; Prob 100%                     ;109.9
                                ; LOE esi
.B6.3:                          ; Preds .B6.1
        mov       ebx, DWORD PTR [12+ebp]                       ;100.12
        mov       eax, DWORD PTR [ebx]                          ;114.9
        cmp       eax, DWORD PTR [4+ecx]                        ;114.16
        je        .B6.17        ; Prob 10%                      ;114.16
                                ; LOE eax edx ecx ebx esi edi
.B6.4:                          ; Preds .B6.3
        mov       ebx, DWORD PTR [8+ecx]                        ;118.24
        test      ebx, ebx                                      ;118.24
        jne       .B6.6         ; Prob 50%                      ;118.24
                                ; LOE eax edx ecx ebx esi edi
.B6.5:                          ; Preds .B6.4
        mov       DWORD PTR [edx], 0                            ;115.13
        jmp       .B6.10        ; Prob 100%                     ;115.13
                                ; LOE esi edi
.B6.6:                          ; Preds .B6.4
        mov       DWORD PTR [esp], esi                          ;
                                ; LOE eax edx ecx ebx edi
.B6.7:                          ; Preds .B6.8 .B6.6
        mov       esi, ecx                                      ;119.17
        mov       ecx, ebx                                      ;120.17
        cmp       eax, DWORD PTR [4+ebx]                        ;121.24
        je        .B6.14        ; Prob 18%                      ;121.24
                                ; LOE eax edx ecx ebx esi edi
.B6.8:                          ; Preds .B6.7
        mov       ebx, DWORD PTR [8+ebx]                        ;118.24
        test      ebx, ebx                                      ;118.24
        jne       .B6.7         ; Prob 50%                      ;118.24
                                ; LOE eax edx ecx ebx edi
.B6.9:                          ; Preds .B6.8
        mov       esi, DWORD PTR [esp]                          ;
        mov       DWORD PTR [edx], 0                            ;115.13
                                ; LOE esi edi
.B6.10:                         ; Preds .B6.15 .B6.9 .B6.5
        mov       eax, -1                                       ;128.17
                                ; LOE eax esi edi
.B6.11:                         ; Preds .B6.10 .B6.16
        not       eax                                           ;128.17
        test      al, 1                                         ;128.17
        je        .B6.13        ; Prob 60%                      ;128.17
                                ; LOE esi edi
.B6.12:                         ; Preds .B6.18 .B6.11
        mov       DWORD PTR [4+edi], edi                        ;128.47
                                ; LOE esi
.B6.13:                         ; Preds .B6.18 .B6.11 .B6.22 .B6.12
        add       esp, 40                                       ;132.1
        pop       ebx                                           ;132.1
        pop       edi                                           ;132.1
        mov       esp, ebp                                      ;132.1
        pop       ebp                                           ;132.1
        ret                                                     ;132.1
                                ; LOE
.B6.14:                         ; Preds .B6.7                   ; Infreq
        mov       DWORD PTR [edx], -1                           ;122.21
        mov       eax, esi                                      ;
        mov       esi, DWORD PTR [esp]                          ;
        push      262144                                        ;124.21
        mov       edx, DWORD PTR [8+ebx]                        ;123.21
        push      ebx                                           ;124.21
        mov       DWORD PTR [8+eax], edx                        ;123.21
        call      _for_deallocate                               ;124.21
                                ; LOE esi edi
.B6.23:                         ; Preds .B6.14                  ; Infreq
        add       esp, 8                                        ;124.21
                                ; LOE esi edi
.B6.15:                         ; Preds .B6.23                  ; Infreq
        cmp       DWORD PTR [edi], 0                            ;128.17
        jne       .B6.10        ; Prob 50%                      ;128.17
                                ; LOE esi edi
.B6.16:                         ; Preds .B6.15                  ; Infreq
        xor       eax, eax                                      ;128.17
        jmp       .B6.11        ; Prob 100%                     ;128.17
                                ; LOE eax esi edi
.B6.17:                         ; Preds .B6.3                   ; Infreq
        mov       DWORD PTR [edx], -1                           ;115.13
        mov       eax, DWORD PTR [4+ecx]                        ;116.18
        mov       DWORD PTR [ebx], eax                          ;116.18
        push      262144                                        ;116.18
        mov       ebx, DWORD PTR [8+ecx]                        ;116.18
        push      ecx                                           ;116.18
        mov       DWORD PTR [_LINK_LIST_mp_DISQUEUE_LINKLIST$TEMP_NP.0.5], ecx ;116.18
        mov       DWORD PTR [edi], ebx                          ;116.18
        call      _for_deallocate                               ;116.18
                                ; LOE ebx esi edi
.B6.24:                         ; Preds .B6.17                  ; Infreq
        add       esp, 8                                        ;116.18
                                ; LOE ebx esi edi
.B6.18:                         ; Preds .B6.24                  ; Infreq
        mov       DWORD PTR [_LINK_LIST_mp_DISQUEUE_LINKLIST$TEMP_NP.0.5], 0 ;116.18
        test      ebx, ebx                                      ;116.18
        je        .B6.12        ; Prob 40%                      ;116.18
        jmp       .B6.13        ; Prob 100%                     ;116.18
        ALIGN     16
                                ; LOE esi edi
; mark_end;
_LINK_LIST_mp_DISQUEUE_TARGET ENDP
_TEXT	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
__STRLITPACK_10.0.6	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _LINK_LIST_mp_DISQUEUE_TARGET
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _LINK_LIST_mp_PRINTQUEUE_LINKLIST
; mark_begin;
       ALIGN     16
	PUBLIC _LINK_LIST_mp_PRINTQUEUE_LINKLIST
_LINK_LIST_mp_PRINTQUEUE_LINKLIST	PROC NEAR 
; parameter 1: 8 + ebp
.B7.1:                          ; Preds .B7.0
        push      ebp                                           ;135.12
        mov       ebp, esp                                      ;135.12
        and       esp, -16                                      ;135.12
        push      esi                                           ;135.12
        push      edi                                           ;135.12
        push      ebx                                           ;135.12
        sub       esp, 68                                       ;135.12
        mov       DWORD PTR [32+esp], 0                         ;140.5
        lea       edi, DWORD PTR [32+esp]                       ;140.5
        mov       DWORD PTR [esp], 12                           ;140.5
        lea       eax, DWORD PTR [esp]                          ;140.5
        mov       DWORD PTR [4+esp], OFFSET FLAT: __STRLITPACK_6 ;140.5
        mov       DWORD PTR [8+esp], 7                          ;140.5
        mov       DWORD PTR [12+esp], OFFSET FLAT: __STRLITPACK_5 ;140.5
        push      32                                            ;140.5
        push      eax                                           ;140.5
        push      OFFSET FLAT: __STRLITPACK_11.0.7              ;140.5
        push      -2088435968                                   ;140.5
        push      1                                             ;140.5
        push      edi                                           ;140.5
        call      _for_open                                     ;140.5
                                ; LOE ebx esi edi
.B7.2:                          ; Preds .B7.1
        mov       DWORD PTR [56+esp], 0                         ;141.5
        lea       eax, DWORD PTR [40+esp]                       ;141.5
        mov       DWORD PTR [40+esp], 5                         ;141.5
        mov       DWORD PTR [44+esp], OFFSET FLAT: __STRLITPACK_3 ;141.5
        push      32                                            ;141.5
        push      eax                                           ;141.5
        push      OFFSET FLAT: __STRLITPACK_12.0.7              ;141.5
        push      -2088435968                                   ;141.5
        push      1                                             ;141.5
        push      edi                                           ;141.5
        call      _for_write_seq_lis                            ;141.5
                                ; LOE ebx esi edi
.B7.12:                         ; Preds .B7.2
        add       esp, 48                                       ;141.5
                                ; LOE ebx esi edi
.B7.3:                          ; Preds .B7.12
        mov       eax, DWORD PTR [8+ebp]                        ;135.12
        mov       edx, DWORD PTR [eax]                          ;142.5
        mov       ecx, DWORD PTR [8+edx]                        ;145.16
        test      ecx, ecx                                      ;145.16
        je        .B7.9         ; Prob 50%                      ;145.16
                                ; LOE ecx ebx esi edi
.B7.4:                          ; Preds .B7.3
        imul      edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE+32], -44 ;
        mov       ebx, ecx                                      ;
        imul      eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32], -152 ;
        add       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;
        add       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE] ;
        mov       edi, eax                                      ;
        mov       esi, edx                                      ;
                                ; LOE ebx esi edi
.B7.5:                          ; Preds .B7.7 .B7.4
        imul      eax, DWORD PTR [4+ebx], 152                   ;147.9
        imul      edx, DWORD PTR [24+edi+eax], 44               ;147.50
        mov       ecx, DWORD PTR [36+esi+edx]                   ;147.9
        mov       DWORD PTR [32+esp], 0                         ;147.9
        mov       DWORD PTR [24+esp], ecx                       ;147.9
        push      32                                            ;147.9
        push      OFFSET FLAT: LINK_LIST_mp_PRINTQUEUE_LINKLIST$format_pack.0.7 ;147.9
        lea       eax, DWORD PTR [32+esp]                       ;147.9
        push      eax                                           ;147.9
        push      OFFSET FLAT: __STRLITPACK_13.0.7              ;147.9
        push      -2088435968                                   ;147.9
        push      1                                             ;147.9
        lea       edx, DWORD PTR [56+esp]                       ;147.9
        push      edx                                           ;147.9
        call      _for_write_seq_fmt                            ;147.9
                                ; LOE ebx esi edi
.B7.6:                          ; Preds .B7.5
        mov       eax, DWORD PTR [ebx]                          ;147.9
        lea       edx, DWORD PTR [92+esp]                       ;147.9
        mov       DWORD PTR [92+esp], eax                       ;147.9
        push      edx                                           ;147.9
        push      OFFSET FLAT: __STRLITPACK_14.0.7              ;147.9
        lea       ecx, DWORD PTR [68+esp]                       ;147.9
        push      ecx                                           ;147.9
        call      _for_write_seq_fmt_xmit                       ;147.9
                                ; LOE ebx esi edi
.B7.13:                         ; Preds .B7.6
        add       esp, 40                                       ;147.9
                                ; LOE ebx esi edi
.B7.7:                          ; Preds .B7.13
        mov       ebx, DWORD PTR [8+ebx]                        ;145.16
        test      ebx, ebx                                      ;145.16
        jne       .B7.5         ; Prob 50%                      ;145.16
                                ; LOE ebx esi edi
.B7.9:                          ; Preds .B7.7 .B7.3
        add       esp, 68                                       ;150.1
        pop       ebx                                           ;150.1
        pop       edi                                           ;150.1
        pop       esi                                           ;150.1
        mov       esp, ebp                                      ;150.1
        pop       ebp                                           ;150.1
        ret                                                     ;150.1
        ALIGN     16
                                ; LOE
; mark_end;
_LINK_LIST_mp_PRINTQUEUE_LINKLIST ENDP
_TEXT	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_11.0.7	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	26
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_12.0.7	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
LINK_LIST_mp_PRINTQUEUE_LINKLIST$format_pack.0.7	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	16
	DB	0
	DB	73
	DB	116
	DB	101
	DB	109
	DB	32
	DB	105
	DB	110
	DB	32
	DB	116
	DB	104
	DB	101
	DB	32
	DB	108
	DB	105
	DB	115
	DB	116
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	5
	DB	1
	DB	0
	DB	0
	DB	0
	DB	10
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
__STRLITPACK_13.0.7	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_14.0.7	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _LINK_LIST_mp_PRINTQUEUE_LINKLIST
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _LINK_LIST_mp_ENQUEUE_ORDERED_LINKLIST
; mark_begin;
       ALIGN     16
	PUBLIC _LINK_LIST_mp_ENQUEUE_ORDERED_LINKLIST
_LINK_LIST_mp_ENQUEUE_ORDERED_LINKLIST	PROC NEAR 
; parameter 1: 24 + esp
; parameter 2: 28 + esp
; parameter 3: 32 + esp
.B8.1:                          ; Preds .B8.0
        push      esi                                           ;153.12
        push      edi                                           ;153.12
        push      ebx                                           ;153.12
        push      ebp                                           ;153.12
        push      esi                                           ;153.12
        xor       edx, edx                                      ;163.9
        mov       esi, DWORD PTR [24+esp]                       ;153.12
        mov       ebp, DWORD PTR [32+esp]                       ;153.12
        mov       eax, DWORD PTR [esi]                          ;162.15
        test      eax, eax                                      ;162.15
        je        .B8.11        ; Prob 40%                      ;162.15
                                ; LOE eax edx ebp esi
.B8.2:                          ; Preds .B8.1
        movss     xmm0, DWORD PTR [ebp]                         ;166.9
        mov       edi, eax                                      ;164.9
        movss     xmm1, DWORD PTR [eax]                         ;166.21
        mov       ebx, DWORD PTR [ebp]                          ;166.9
        comiss    xmm1, xmm0                                    ;166.18
        jae       .B8.22        ; Prob 1%                       ;166.18
                                ; LOE eax ebx ebp esi edi xmm0
.B8.3:                          ; Preds .B8.2
        cmp       DWORD PTR [8+eax], 0                          ;174.24
        jne       .B8.6         ; Prob 50%                      ;174.24
                                ; LOE ebx ebp esi edi xmm0
.B8.4:                          ; Preds .B8.3
        xor       edx, edx                                      ;
        jmp       .B8.12        ; Prob 100%                     ;
                                ; LOE edx ebp esi
.B8.6:                          ; Preds .B8.3 .B8.8
        mov       ecx, DWORD PTR [8+edi]                        ;176.17
        test      ecx, ecx                                      ;177.21
        movss     xmm1, DWORD PTR [edi]                         ;175.17
        mov       eax, DWORD PTR [edi]                          ;175.17
        je        .B8.9         ; Prob 50%                      ;177.21
                                ; LOE eax ecx ebx ebp esi edi xmm0 xmm1
.B8.7:                          ; Preds .B8.6
        mov       edx, DWORD PTR [ecx]                          ;178.21
        movss     xmm2, DWORD PTR [ecx]                         ;178.21
        mov       DWORD PTR [_LINK_LIST_mp_ENQUEUE_ORDERED_LINKLIST$ITEM2.0.8], edx ;178.21
        comiss    xmm0, xmm1                                    ;179.31
        ja        .B8.19        ; Prob 18%                      ;179.31
                                ; LOE eax ecx ebx ebp esi edi xmm0 xmm2
.B8.8:                          ; Preds .B8.7 .B8.19
        mov       edi, ecx                                      ;191.17
        cmp       DWORD PTR [8+ecx], 0                          ;174.24
        jne       .B8.6         ; Prob 50%                      ;174.24
                                ; LOE eax ebx ebp esi edi xmm0
.B8.9:                          ; Preds .B8.6 .B8.8
        mov       DWORD PTR [_LINK_LIST_mp_ENQUEUE_ORDERED_LINKLIST$ITEM1.0.8], eax ;175.17
        xor       edx, edx                                      ;
        jmp       .B8.12        ; Prob 100%                     ;
                                ; LOE edx ebp esi
.B8.11:                         ; Preds .B8.23 .B8.21 .B8.1
        test      eax, eax                                      ;195.25
        je        .B8.13        ; Prob 50%                      ;195.25
                                ; LOE edx ebp esi
.B8.12:                         ; Preds .B8.9 .B8.4 .B8.11
        mov       eax, -1                                       ;195.25
        jmp       .B8.14        ; Prob 100%                     ;195.25
                                ; LOE eax edx ebp esi
.B8.13:                         ; Preds .B8.11
        xor       eax, eax                                      ;195.25
                                ; LOE eax edx ebp esi
.B8.14:                         ; Preds .B8.12 .B8.13
        test      dl, 1                                         ;195.13
        jne       .B8.17        ; Prob 40%                      ;195.13
                                ; LOE eax ebp esi
.B8.15:                         ; Preds .B8.17 .B8.14
        push      262144                                        ;196.14
        push      OFFSET FLAT: _LINK_LIST_mp_ENQUEUE_LINKLIST$TEMP_NP.0.4 ;196.14
        push      12                                            ;196.14
        call      _for_allocate                                 ;196.14
                                ; LOE ebp esi
.B8.26:                         ; Preds .B8.15
        add       esp, 12                                       ;196.14
                                ; LOE ebp esi
.B8.16:                         ; Preds .B8.26
        mov       ebx, DWORD PTR [28+esp]                       ;196.14
        mov       eax, DWORD PTR [_LINK_LIST_mp_ENQUEUE_LINKLIST$TEMP_NP.0.4] ;196.14
        mov       edi, DWORD PTR [4+esi]                        ;196.14
        mov       ecx, DWORD PTR [ebp]                          ;196.14
        mov       edx, DWORD PTR [_LINK_LIST_mp_ENQUEUE_LINKLIST$BLK..T133_+8] ;196.14
        mov       ebp, DWORD PTR [ebx]                          ;196.14
        mov       DWORD PTR [8+eax], edx                        ;196.14
        mov       DWORD PTR [eax], ecx                          ;196.14
        mov       DWORD PTR [4+eax], ebp                        ;196.14
        mov       DWORD PTR [edi], eax                          ;196.14
        add       eax, 8                                        ;196.14
        mov       DWORD PTR [4+esi], eax                        ;196.14
        mov       DWORD PTR [eax], 0                            ;196.14
        pop       ecx                                           ;196.14
        pop       ebp                                           ;196.14
        pop       ebx                                           ;196.14
        pop       edi                                           ;196.14
        pop       esi                                           ;196.14
        ret                                                     ;196.14
                                ; LOE
.B8.17:                         ; Preds .B8.14
        not       eax                                           ;195.25
        test      al, 1                                         ;195.25
        jne       .B8.15        ; Prob 15%                      ;195.25
                                ; LOE ebp esi
.B8.18:                         ; Preds .B8.17
        pop       ecx                                           ;199.1
        pop       ebp                                           ;199.1
        pop       ebx                                           ;199.1
        pop       edi                                           ;199.1
        pop       esi                                           ;199.1
        ret                                                     ;199.1
                                ; LOE
.B8.19:                         ; Preds .B8.7                   ; Infreq
        comiss    xmm2, xmm0                                    ;179.51
        jb        .B8.8         ; Prob 82%                      ;179.51
                                ; LOE eax ecx ebx ebp esi edi xmm0
.B8.20:                         ; Preds .B8.19                  ; Infreq
        mov       DWORD PTR [esp], ecx                          ;
        push      262144                                        ;180.25
        push      OFFSET FLAT: _LINK_LIST_mp_ENQUEUE_ORDERED_LINKLIST$TEMP_NP3.0.8 ;180.25
        push      12                                            ;180.25
        mov       DWORD PTR [_LINK_LIST_mp_ENQUEUE_ORDERED_LINKLIST$ITEM1.0.8], eax ;175.17
        call      _for_allocate                                 ;180.25
                                ; LOE ebx ebp esi edi
.B8.27:                         ; Preds .B8.20                  ; Infreq
        add       esp, 12                                       ;180.25
                                ; LOE ebx ebp esi edi
.B8.21:                         ; Preds .B8.27                  ; Infreq
        mov       eax, DWORD PTR [_LINK_LIST_mp_ENQUEUE_ORDERED_LINKLIST$TEMP_NP3.0.8] ;180.25
        mov       edx, DWORD PTR [28+esp]                       ;182.25
        mov       DWORD PTR [8+edi], eax                        ;183.25
        mov       DWORD PTR [eax], ebx                          ;181.25
        mov       ecx, DWORD PTR [edx]                          ;182.25
        mov       edx, -1                                       ;
        mov       ebx, DWORD PTR [esp]                          ;184.25
        mov       DWORD PTR [4+eax], ecx                        ;182.25
        mov       DWORD PTR [8+eax], ebx                        ;184.25
        mov       eax, DWORD PTR [esi]                          ;195.25
        jmp       .B8.11        ; Prob 100%                     ;195.25
                                ; LOE eax edx ebp esi
.B8.22:                         ; Preds .B8.2                   ; Infreq
        push      262144                                        ;167.13
        push      OFFSET FLAT: _LINK_LIST_mp_ENQUEUE_ORDERED_LINKLIST$TEMP_NP3.0.8 ;167.13
        push      12                                            ;167.13
        call      _for_allocate                                 ;167.13
                                ; LOE ebx ebp esi edi
.B8.28:                         ; Preds .B8.22                  ; Infreq
        add       esp, 12                                       ;167.13
                                ; LOE ebx ebp esi edi
.B8.23:                         ; Preds .B8.28                  ; Infreq
        mov       edx, DWORD PTR [28+esp]                       ;169.13
        mov       eax, DWORD PTR [_LINK_LIST_mp_ENQUEUE_ORDERED_LINKLIST$TEMP_NP3.0.8] ;167.13
        mov       DWORD PTR [esi], eax                          ;170.13
        mov       ecx, DWORD PTR [edx]                          ;169.13
        mov       edx, -1                                       ;172.13
        mov       DWORD PTR [eax], ebx                          ;168.13
        mov       DWORD PTR [4+eax], ecx                        ;169.13
        mov       DWORD PTR [8+eax], edi                        ;171.13
        jmp       .B8.11        ; Prob 100%                     ;171.13
        ALIGN     16
                                ; LOE eax edx ebp esi
; mark_end;
_LINK_LIST_mp_ENQUEUE_ORDERED_LINKLIST ENDP
_TEXT	ENDS
_BSS	SEGMENT  DWORD PUBLIC FLAT  'BSS'
	ALIGN 004H
_LINK_LIST_mp_ENQUEUE_ORDERED_LINKLIST$TEMP_NP3.0.8	DD 1 DUP (0H)	; pad
_LINK_LIST_mp_ENQUEUE_ORDERED_LINKLIST$ITEM2.0.8	DD 1 DUP (0H)	; pad
_LINK_LIST_mp_ENQUEUE_ORDERED_LINKLIST$ITEM1.0.8	DD 1 DUP (0H)	; pad
_BSS	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _LINK_LIST_mp_ENQUEUE_ORDERED_LINKLIST
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _LINK_LIST_mp_IS_EMPTY_LINKLIST
; mark_begin;
       ALIGN     16
	PUBLIC _LINK_LIST_mp_IS_EMPTY_LINKLIST
_LINK_LIST_mp_IS_EMPTY_LINKLIST	PROC NEAR 
; parameter 1: 4 + esp
.B9.1:                          ; Preds .B9.0
        xor       ecx, ecx                                      ;208.1
        mov       edx, DWORD PTR [4+esp]                        ;202.15
        mov       eax, -1                                       ;208.1
        cmp       DWORD PTR [edx], 0                            ;208.1
        cmovne    eax, ecx                                      ;208.1
        ret                                                     ;208.1
        ALIGN     16
                                ; LOE
; mark_end;
_LINK_LIST_mp_IS_EMPTY_LINKLIST ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _LINK_LIST_mp_IS_EMPTY_LINKLIST
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _LINK_LIST_mp_IS_FULL_LINKLIST
; mark_begin;
       ALIGN     16
	PUBLIC _LINK_LIST_mp_IS_FULL_LINKLIST
_LINK_LIST_mp_IS_FULL_LINKLIST	PROC NEAR 
; parameter 1: 4 + esp
.B10.1:                         ; Preds .B10.0
        xor       eax, eax                                      ;217.1
        ret                                                     ;217.1
        ALIGN     16
                                ; LOE
; mark_end;
_LINK_LIST_mp_IS_FULL_LINKLIST ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _LINK_LIST_mp_IS_FULL_LINKLIST
_BSS	SEGMENT  DWORD PUBLIC FLAT  'BSS'
	DD 1 DUP (0H)	; pad
_LINK_LIST_mp_DISQUEUE_LINKLIST$TEMP_NP.0.5	DD 1 DUP (0H)	; pad
	DD 1 DUP (0H)	; pad
_LINK_LIST_mp_ENQUEUE_LINKLIST$TEMP_NP.0.4	DD 1 DUP (0H)	; pad
	DD 1 DUP (0H)	; pad
	PUBLIC _LINK_LIST_mp_ENQUEUE_LINKLIST$BLK..T133_
_LINK_LIST_mp_ENQUEUE_LINKLIST$BLK..T133_	DD 3 DUP (0H)	; pad
	DD 1 DUP (0H)	; pad
	PUBLIC _LINK_LIST_mp_ENQUEUE_ORDERED_LINKLIST$BLK..T205_
_LINK_LIST_mp_ENQUEUE_ORDERED_LINKLIST$BLK..T205_	DD 3 DUP (0H)	; pad
	DD 1 DUP (0H)	; pad
	PUBLIC _LINK_LIST_mp_ENQUEUE_ORDERED_LINKLIST$BLK..T208_
_LINK_LIST_mp_ENQUEUE_ORDERED_LINKLIST$BLK..T208_	DD 3 DUP (0H)	; pad
_BSS	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_8	DB	32
	DB	65
	DB	116
	DB	116
	DB	101
	DB	109
	DB	112
	DB	116
	DB	32
	DB	116
	DB	111
	DB	32
	DB	112
	DB	111
	DB	112
	DB	32
	DB	102
	DB	114
	DB	111
	DB	109
	DB	32
	DB	101
	DB	109
	DB	112
	DB	116
	DB	121
	DB	32
	DB	115
	DB	116
	DB	97
	DB	99
	DB	107
	DB	46
	DB	32
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_9.0.5	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_7	DB	32
	DB	65
	DB	116
	DB	116
	DB	101
	DB	109
	DB	112
	DB	116
	DB	32
	DB	116
	DB	111
	DB	32
	DB	112
	DB	111
	DB	112
	DB	32
	DB	102
	DB	114
	DB	111
	DB	109
	DB	32
	DB	101
	DB	109
	DB	112
	DB	116
	DB	121
	DB	32
	DB	115
	DB	116
	DB	97
	DB	99
	DB	107
	DB	46
	DB	32
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_6	DB	81
	DB	85
	DB	69
	DB	85
	DB	69
	DB	79
	DB	85
	DB	84
	DB	46
	DB	68
	DB	65
	DB	84
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_5	DB	85
	DB	78
	DB	75
	DB	78
	DB	79
	DB	87
	DB	78
	DB	0
__STRLITPACK_3	DB	81
	DB	117
	DB	101
	DB	114
	DB	121
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE:BYTE
_DATA	ENDS
EXTRN	_for_write_seq_fmt_xmit:PROC
EXTRN	_for_write_seq_fmt:PROC
EXTRN	_for_open:PROC
EXTRN	_for_deallocate:PROC
EXTRN	_for_write_seq_lis:PROC
EXTRN	_for_allocate:PROC
EXTRN	__fltused:BYTE
	INCLUDELIB <ifconsol>
	INCLUDELIB <libifcoremt>
	INCLUDELIB <libifport>
	INCLUDELIB <libmmt>
	INCLUDELIB <LIBCMT>
	INCLUDELIB <libirc>
	INCLUDELIB <svml_dispmt>
	INCLUDELIB <OLDNAMES>
	END
