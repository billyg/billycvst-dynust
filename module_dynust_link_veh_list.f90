MODULE DYNUST_LINK_VEH_LIST_MODULE
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~ Copyright (C) 2002-2011 The Arizona Board of Regents on Behalf of The University of Arizona.  ~~!
!~~ Developed by Yi-Chang Chiu, Ph.D. as par to the DynusT project.                               ~~!
!~~ This program is free software: you can redistribute it and/or modify it                       ~~!
!~~ under the terms of the GNU General Public License version 2 as published by                   ~~!
!~~ the Free Software Foundation.                                                                 ~~!
!~~ This program is distributed in the hope that it will be useful,                               ~~!
!~~ but WITHOUT ANY WARRANTY; without even the implied warranty of                                ~~!
!~~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                                          ~~!
!~~ See the GNU General Public License for more details.                                          ~~!
!~~ You should have received a copy of the GNU General Public License                             ~~!
!~~ along with this program (in the file COPYING).                                                ~~!
!~~ IF not, see <http://www.gnu.org/licenses/>.                                                   ~~!
!~~ Please see http://www.dynust.net for contact information and names                            ~~!
!~~ of individual contributors.                                                                   ~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
 
TYPE Linkstruct
  INTEGER, POINTER :: P(:) ! vehicle ID on the link
  INTEGER          :: PSize ! size of the ALLOCATED P()
  INTEGER          :: FID ! number of vehielces on the link
END TYPE Linkstruct

TYPE LinkstructShort
  INTEGER, POINTER :: P(:) ! vehicle ID on the link
  INTEGER(2)          :: PSize ! size of the ALLOCATED P() ! max value 35000
  INTEGER(2)          :: FID ! number of vehielces on the link
END TYPE LinkstructShort

TYPE LinkstructLite
  INTEGER, POINTER::p(:)
  INTEGER:: PSize
ENDTYPE
! --  Define the data structure for storing the attributes of each path in the Grand Path Set
! --  LinkVehList: link list keeps all the vehicles on the link
TYPE(linkstruct),ALLOCATABLE::LinkVehList(:)
TYPE(linkstruct),ALLOCATABLE::EntQueVehList(:) ! assuming no more than 35000 vehicle stay in the queue
TYPE(linkstructShort),ALLOCATABLE::TripChainList(:)

TYPE(linkstructlite), ALLOCATABLE :: tempP(:)

INTEGER :: h_IncreaseSize = 30 ! increment for P() 
INTEGER LinkAtt_ArraySize
INTEGER EnQAtt_ArraySize
INTEGER TChainAtt_ArraySize

INTEGER vectorerror

CONTAINS


! *** Start of vehatt_Array implementation ***

! This initialize the initial path arrays for vehicles
SUBROUTINE LinkAtt_2DSetup(Size)

  INTEGER Size
  INTEGER :: vi
! Remove existing array
  IF (ALLOCATED(LinkVehList)) THEN
    CALL LinkAtt_2DRemove()
  ENDIF

! Initialize increase size.
! Setup new array
  IF (.NOT. ALLOCATED(LinkVehList)) THEN
    ALLOCATE(LinkVehList(Size),stat=vectorerror)
	IF(vectorerror /= 0) WRITE(911,*)"LinkVehList Setup error", STOP
  ENDIF
  
! Initialize new size for each element
  DO vi = 1, Size
   LinkVehList(vi)%PSize = 0
   LinkVehList(vi)%Fid = 0
  ENDDO

  vehatt_ArraySIZE = Size
  
END SUBROUTINE 


SUBROUTINE LinkTemp_2DSetup(Size)

  INTEGER Size
  INTEGER :: vi
! Remove existing array
  IF (ALLOCATED(tempP)) THEN
    CALL Linktemp_2DRemove()
  ENDIF

! Initialize increase size.
! Setup new array
  IF (.NOT. ALLOCATED(tempP)) THEN
    ALLOCATE(tempP(Size),stat=vectorerror)
	IF(vectorerror /= 0) WRITE(911,*)"tempP Setup error", STOP
  ENDIF
! initialize
  tempP(:)%PSize = 0
  
  
END SUBROUTINE 

! this SUBROUTINE copies the existing array into a longer one
SUBROUTINE LinkATT_Setup(it,NewSize)
	
 INTEGER it, NewSize
 INTEGER :: vi
 INTEGER :: OldSize
 OldSize = 0
	
! create temp pointer to store contents of array
   IF (LinkVehList(it)%PSize > 0 ) THEN
     OldSize = LinkVehList(it)%PSize
	 ALLOCATE(tempP(it)%p(OldSize),stat=vectorerror)
	 IF(vectorerror /= 0) WRITE(911,*) 'allocate tmpP vectorerror', STOP
	 tempP(it)%PSize = OldSize

! Copy content of old array to temp pointer
     tempP(it)%p(1:OldSize) = LinkVehList(it)%P(1:OldSize)
  
! Delete the old array
     IF (ASSOCIATED(LinkVehList(it)%P)) THEN
	  DEALLOCATE(LinkVehList(it)%P,stat=vectorerror)
	  IF(vectorerror /= 0) WRITE(911,*)"DEALLOCATE LinkAtt_1DArray vector error", STOP
     ENDIF
   ENDIF

! REALlocate array
   ALLOCATE(LinkVehList(it)%P(NewSize),stat=vectorerror)
	IF(vectorerror /= 0) WRITE(911,*)"allocate LinkVehList vector error", STOP
   
! Copy contents from temp back to array 
   IF(OldSize > 0) THEN
   DO vi = 1, OldSize
     LinkVehList(it)%P(vi) = tempP(it)%p(vi)
   ENDDO
   ENDIF

! initialize array for the remaining elements
   DO vi = OldSize +1, NewSize
      LinkVehList(it)%P(vi) = 0
   ENDDO

   LinkVehList(it)%PSize = NewSize
   LinkVehList(it)%Fid = OldSize
   IF(OldSize > 0) THEN
      DEALLOCATE(tempp(it)%p)
   ENDIF


END SUBROUTINE 


SUBROUTINE LinkAtt_2DRemove()
 
  INTEGER :: vi
! Remove every element
  DO vi = 1, LinkAtt_ArraySize
    CALL LinkAtt_Remove(vi) 
  ENDDO

! Remove entire array
  IF (ALLOCATED(LinkVehList)) THEN
    DEALLOCATE(LinkVehList,stat=vectorerror)
	IF(vectorerror /= 0) WRITE(911,*) "VhcAtt Destory", STOP
  ENDIF
  vehatt_ArraySize = 0
	
END SUBROUTINE 

SUBROUTINE Linktemp_2DRemove()
 
  INTEGER :: vi
! Remove every element
  DO vi = 1, LinkAtt_ArraySize
    CALL Linktemp_Remove(vi) 
  ENDDO

! Remove entire array
  IF (ALLOCATED(tempP)) THEN
    DEALLOCATE(tempP,stat=vectorerror)
	IF(vectorerror /= 0) WRITE(911,*) "tempP Destory error", STOP
  ENDIF
	
END SUBROUTINE 


SUBROUTINE LinkAtt_Remove(it)
 
  INTEGER it
  IF (LinkVehList(it)%PSize > 0) THEN
    DEALLOCATE(LinkVehList(it)%P,stat=vectorerror)
	IF(vectorerror /= 0) WRITE(911,*)"DEALLOCATE LinkAtt_Array vectorerror", it, STOP
    LinkVehList(it)%PSize = 0 
    LinkVehList(it)%Fid = 0 
  ENDIF

END SUBROUTINE 


SUBROUTINE Linktemp_Remove(it)
 
  INTEGER it
  IF (tempP(it)%PSize > 0) THEN
    DEALLOCATE(tempP(it)%P,stat=vectorerror)
	IF(vectorerror /= 0) WRITE(911,*)"DEALLOCATE tempP vectorerror", IT, STOP
    tempP(it)%PSize = 0 
  ENDIF


END SUBROUTINE 


! This subroute is the method for inserting a node into the list LinkVehList
SUBROUTINE linkVehList_insert(ilink,VehID,xpart)
  USE DYNUST_MAIN_MODULE
  USE DYNUST_VEH_MODULE
  INTEGER ilink,VehID,mg
  REAL xpart
    Icheck = 0
     IF (LinkVehList(ilink)%Fid+1 > LinkVehList(ilink)%PSize) THEN
	   NewSize = LinkVehList(ilink)%PSize + h_IncreaseSize 
       CALL LinkAtt_Setup(ilink,NewSize)
     ENDIF
     mg = LinkVehList(ilink)%Fid
     
     IF(mg < 1) THEN
       LinkVehList(ilink)%P(mg+1) = VehID
     ELSE
       DO while (xpart <= m_dynust_veh(LinkVehList(ilink)%P(mg))%position)
         mg = mg - 1
         IF(mg < 1) exit
       ENDDO
       mg = mg + 1
       IF(mg == LinkVehList(ilink)%Fid+1) THEN
         LinkVehList(ilink)%P(mg) = VehID
       ELSE
         DO ig = LinkVehList(ilink)%Fid, mg, -1
            isee = LinkVehList(ilink)%P(ig)
            LinkVehList(ilink)%P(ig+1)=LinkVehList(ilink)%P(ig)
         ENDDO
         LinkVehList(ilink)%P(mg) = VehID
       ENDIF
     ENDIF
     LinkVehList(ilink)%Fid = LinkVehList(ilink)%Fid + 1


END SUBROUTINE 


SUBROUTINE linkVehList_remove(ilink,VehID)
  INTEGER ilink,VehID
  INTEGER icheck

  icheck = 0
  DO i = 1, LinkVehList(ilink)%Fid
     IF(LinkVehList(ilink)%P(i) == VehID) THEN
	   icheck = 1
       DO j = i, LinkVehList(ilink)%Fid-1
         LinkVehList(ilink)%P(j) = LinkVehList(ilink)%P(j+1)
	   ENDDO
       exit
	 ENDIF
  ENDDO
  IF(icheck == 1) THEN
   LinkVehList(ilink)%P(LinkVehList(ilink)%Fid) = 0
   LinkVehList(ilink)%Fid = LinkVehList(ilink)%Fid - 1
  ELSE
   WRITE(911,*) "Error in mtjx_remove in link", ilink, STOP
  ENDIF
END SUBROUTINE


SUBROUTINE EnQAtt_2DSetup(Size)

  INTEGER Size
  INTEGER :: vi
! Remove existing array
  IF (ALLOCATED(EntQueVehList)) THEN
    CALL EnQAtt_2DRemove()
  ENDIF

! Initialize increase size.
! Setup new array
  IF (.NOT. ALLOCATED(EntQueVehList)) THEN
    ALLOCATE(EntQueVehList(Size),stat=vectorerror)
	IF(vectorerror /= 0) WRITE(911,*)"EntQueVehList Setup error", STOP
  ENDIF
  
! Initialize new size for each element
  DO vi = 1, Size
   EntQueVehList(vi)%PSize = 0
   EntQueVehList(vi)%Fid = 0
  ENDDO

  EnQAtt_ArraySIZE = Size
	
END SUBROUTINE 


! this SUBROUTINE copies the existing array into a longer one
SUBROUTINE EnQATT_Setup(it,NewSize)
	
 INTEGER it, NewSize
 INTEGER :: vi
 INTEGER :: OldSize
 OldSize = 0
	
! create temp pointer to store contents of array
   IF (EntQueVehList(it)%PSize > 0 ) THEN
     OldSize = EntQueVehList(it)%PSize
     ALLOCATE(tempP(it)%p(EntQueVehList(it)%PSize),stat=vectorerror)
	 IF(vectorerror /= 0) THEN
	   WRITE(911,*) 'allocate tmpP vectorerror'
	   STOP
	 ENDIF

! Copy content of old array to temp pointer
     DO vi = 1, EntQueVehList(it)%PSize
	   tempP(it)%P(vi) = EntQueVehList(it)%P(vi)
	 ENDDO
  
! Delete the old array
     IF (ASSOCIATED(EntQueVehList(it)%P)) THEN
	  DEALLOCATE(EntQueVehList(it)%P,stat=vectorerror)
	  IF(vectorerror /= 0) THEN
	    WRITE(911,*)"DEALLOCATE LinkAtt_1DArray vector error"
	    STOP
	  ENDIF
     ENDIF
   ENDIF

! REALlocate array
   ALLOCATE(EntQueVehList(it)%P(NewSize),stat=vectorerror)
	IF(vectorerror /= 0) THEN
      WRITE(911,*)"allocate EntQueVehList vector error"
	  STOP
	ENDIF
   
! Copy contents from temp back to array 
   IF(OldSize > 0) THEN
   DO vi = 1, OldSize
     EntQueVehList(it)%P(vi) = tempP(it)%P(vi)
   ENDDO
   ENDIF

! initialize array for the remaining elements
   DO vi = OldSize +1, NewSize
      EntQueVehList(it)%P(vi) = 0
   ENDDO

   EntQueVehList(it)%PSize = NewSize
   EntQueVehList(it)%Fid = OldSize
   IF(OldSize > 0) THEN
     DEALLOCATE(tempP(it)%P)
   ENDIF

END SUBROUTINE 



SUBROUTINE EnQAtt_2DRemove()
 
  INTEGER :: vi
! Remove every element
  DO vi = 1, EnQAtt_ArraySize
    CALL EnQAtt_Remove(vi) 
  ENDDO

! Remove entire array
  IF (ALLOCATED(EntQueVehList)) THEN
    DEALLOCATE(EntQueVehList,stat=vectorerror)
	IF(vectorerror /= 0) THEN
 	  WRITE(911,*) "VhcAtt Destory"
 	  STOP
	ENDIF
  ENDIF
  EnQAtt_ArraySize = 0
	
END SUBROUTINE 


SUBROUTINE EnQAtt_Remove(it)
 
  INTEGER it
  IF (EntQueVehList(it)%PSize > 0) THEN
    DEALLOCATE(EntQueVehList(it)%P,stat=vectorerror)
	IF(vectorerror /= 0) THEN
	  WRITE(911,*)"DEALLOCATE LinkAtt_Array vectorerror"
	  WRITE(911,*) it
	  STOP
	ENDIF
    EntQueVehList(it)%PSize = 0 
    EntQueVehList(it)%Fid = 0 
  ENDIF

END SUBROUTINE 



! This subroute is the method for inserting a node into the list EnQVehList
SUBROUTINE entryqueue_insert(ilink,VehID)
  INTEGER VehID, ilink

    Icheck = 0
     IF (EntQueVehList(ilink)%Fid == EntQueVehList(ilink)%PSize) THEN
	   NewSize = EntQueVehList(ilink)%PSize + h_IncreaseSize 
       CALL EnQAtt_Setup(ilink,NewSize)
     ENDIF
     EntQueVehList(ilink)%Fid = EntQueVehList(ilink)%Fid + 1
     EntQueVehList(ilink)%P(EntQueVehList(ilink)%Fid) = VehID
!    ENDIF
END SUBROUTINE 



SUBROUTINE entryqueue_remove(ilink,nVehID)
  INTEGER ilink,nVehID
  INTEGER icheck
  icheck = 0
  isee = EntQueVehList(ilink)%Fid
   IF(EntQueVehList(ilink)%Fid == nVehID) THEN
      EntQueVehList(ilink)%Fid = 0
      EntQueVehList(ilink)%P(:) = 0
   ELSE
       DO j = nVehID+1, EntQueVehList(ilink)%Fid
         EntQueVehList(ilink)%P(j-nVehID) = EntQueVehList(ilink)%P(j)
	   ENDDO
    EntQueVehList(ilink)%P(EntQueVehList(ilink)%fid-nVehID+1:EntQueVehList(ilink)%fid) = 0
    EntQueVehList(ilink)%Fid = EntQueVehList(ilink)%Fid - nVehID
   ENDIF
END SUBROUTINE


SUBROUTINE entryqueue_InsFront(ilink,VehID)
! This SUBROUTINE insert the VehID at the front of the entry queue of link ilink
! this is needed for Trip Chain when the vehicle finsihed the activity
! it is inserted at the beginning of the list
  INTEGER ilink,VehID
  DO i = 1, EntQueVehList(ilink)%PSize-1
     EntQueVehList(ilink)%P(i+1)=EntQueVehList(ilink)%P(i)
  ENDDO
  EntQueVehList(ilink)%P(1) = VehID
END SUBROUTINE


! ----------------Trip Chain -------------------

SUBROUTINE TChainAtt_2DSetup(Size)

  INTEGER Size
  INTEGER :: vi
! Remove existing array
  IF (ALLOCATED(TripChainList)) THEN
    CALL TChainAtt_2DRemove()
  ENDIF

! Initialize increase size.
! Setup new array
  IF (.NOT. ALLOCATED(TripChainList)) THEN
    ALLOCATE(TripChainList(Size),stat=vectorerror)
	IF(vectorerror /= 0) THEN
 	  WRITE(911,*)"TripChainList Setup error"
	  STOP
	ENDIF
  ENDIF
  
! Initialize new size for each element
  DO vi = 1, Size
   TripChainList(vi)%PSize = 0
   TripChainList(vi)%Fid = 0
  ENDDO

  TChainAtt_ArraySIZE = Size
	
END SUBROUTINE 

! this SUBROUTINE copies the existing array into a longer one
SUBROUTINE TChainATT_Setup(it,NewSize)
	
 INTEGER it, NewSize
 INTEGER :: vi
 INTEGER :: OldSize
 OldSize = 0
	
! create temp pointer to store contents of array
   IF (TripChainList(it)%PSize > 0 ) THEN
     OldSize = TripChainList(it)%PSize
     ALLOCATE(tempP(it)%P(TripChainList(it)%PSize),stat=vectorerror)
	 IF(vectorerror /= 0) THEN
	   WRITE(911,*) 'allocate tmpP vectorerror'
	   STOP
	 ENDIF

! Copy content of old array to temp pointer
     DO vi = 1, TripChainList(it)%PSize
	   tempP(it)%P(vi) = TripChainList(it)%P(vi)
	 ENDDO
  
! Delete the old array
     IF (ASSOCIATED(TripChainList(it)%P)) THEN
	  DEALLOCATE(TripChainList(it)%P,stat=vectorerror)
	  IF(vectorerror /= 0) THEN
	    WRITE(911,*)"DEALLOCATE TripChainList vector error"
	    STOP
	  ENDIF
     ENDIF
   ENDIF

! REALlocate array
   ALLOCATE(TripChainList(it)%P(NewSize),stat=vectorerror)
	IF(vectorerror /= 0) THEN
      WRITE(911,*)"allocate TripChainList vector error"
	  STOP
	ENDIF
   
! Copy contents from temp back to array 
   IF(OldSize > 0) THEN
   DO vi = 1, OldSize
     TripChainList(it)%P(vi) = tempP(it)%P(vi)
   ENDDO
   ENDIF

! initialize array for the remaining elements
   DO vi = OldSize +1, NewSize
      TripChainList(it)%P(vi) = 0
   ENDDO

   TripChainList(it)%PSize = NewSize
   TripChainList(it)%Fid = OldSize
   IF(OldSize > 0) THEN
     DEALLOCATE(TempP(it)%P)
   ENDIF

END SUBROUTINE 


! This subroute is the method for inserting a node into the list TChainVehList
SUBROUTINE Tripchain_insert(ilink,VehID)
  INTEGER ilink,VehID
    Icheck = 0
     IF (TripChainList(ilink)%Fid == TripChainList(ilink)%PSize) THEN
	   NewSize = TripChainList(ilink)%PSize + h_IncreaseSize 
       CALL TchainAtt_Setup(ilink,NewSize)
     ENDIF
     TripChainList(ilink)%Fid = TripChainList(ilink)%Fid + 1
     TripChainList(ilink)%P(TripChainList(ilink)%Fid) = VehID

END SUBROUTINE 



SUBROUTINE TripChain_remove(ilink,VehID)
  INTEGER ilink,VehID
  INTEGER icheck
  icheck = 0
  DO i = 1, TripChainList(ilink)%Fid
     IF(TripChainList(ilink)%P(i) == VehID) THEN
	   icheck = 1
       DO j = i, TripChainList(ilink)%PSize-1
         TripChainList(ilink)%P(j) = TripChainList(ilink)%P(j+1)
	   ENDDO
     exit
	 ENDIF
  ENDDO
  IF(icheck > 0) THEN
    TripChainList(ilink)%P(TripChainList(ilink)%PSize) = 0
    TripChainList(ilink)%Fid = TripChainList(ilink)%Fid - 1
   ELSE
    WRITE(911,*) "Error in entryqueue_remove"
	STOP
   ENDIF
END SUBROUTINE


SUBROUTINE TChainAtt_2DRemove()
 
  INTEGER :: vi
! Remove every element
  DO vi = 1, TChainAtt_ArraySize
    CALL TChainAtt_Remove(vi) 
  ENDDO

! Remove entire array
  IF (ALLOCATED(TripChainList)) THEN
    DEALLOCATE(TripChainList,stat=vectorerror)
	IF(vectorerror /= 0) THEN
 	  WRITE(911,*) "VhcAtt Destory"
 	  STOP
	ENDIF
  ENDIF
  TChainAtt_ArraySize = 0
	
END SUBROUTINE 


SUBROUTINE TChainAtt_Remove(it)
 
  INTEGER it
  IF (TripChainList(it)%PSize > 0) THEN
    DEALLOCATE(TripChainList(it)%P,stat=vectorerror)
	IF(vectorerror /= 0) THEN
	  WRITE(911,*)"DEALLOCATE LinkAtt_Array vectorerror"
	  WRITE(911,*) it
	  STOP
	ENDIF
    TripChainList(it)%PSize = 0 
    TripChainList(it)%Fid = 0 
  ENDIF

END SUBROUTINE 


END MODULE
