; mark_description "Intel(R) Visual Fortran Compiler XE for applications running on IA-32, Version 13.1.1.171 Build 20130313";
; mark_description "-S";
	.686P
 	.387
	OPTION DOTNAME
	ASSUME	CS:FLAT,DS:FLAT,SS:FLAT
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
TXTST0:
; -- Begin  _DYNUST_AMS_MODULE$
; mark_begin;
IF @Version GE 800
  .MMX
ELSEIF @Version GE 612
  .MMX
  MMWORD TEXTEQU <QWORD>
ENDIF
IF @Version GE 800
  .XMM
ELSEIF @Version GE 614
  .XMM
  XMMWORD TEXTEQU <OWORD>
ENDIF
       ALIGN     16
	PUBLIC _DYNUST_AMS_MODULE$
_DYNUST_AMS_MODULE$	PROC NEAR 
.B1.1:                          ; Preds .B1.0
        ret                                                     ;1.8
        ALIGN     16
                                ; LOE
; mark_end;
_DYNUST_AMS_MODULE$ ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_AMS_MODULE$
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_AMS_MODULE_mp_FLOW_MODEL_UPDATE_AMTS
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_AMS_MODULE_mp_FLOW_MODEL_UPDATE_AMTS
_DYNUST_AMS_MODULE_mp_FLOW_MODEL_UPDATE_AMTS	PROC NEAR 
.B2.1:                          ; Preds .B2.0
        push      ebx                                           ;26.12
        mov       ebx, esp                                      ;26.12
        and       esp, -16                                      ;26.12
        push      ebp                                           ;26.12
        push      ebp                                           ;26.12
        mov       ebp, DWORD PTR [4+ebx]                        ;26.12
        mov       DWORD PTR [4+esp], ebp                        ;26.12
        mov       ebp, esp                                      ;26.12
        sub       esp, 360                                      ;26.12
        xor       ecx, ecx                                      ;46.24
        mov       DWORD PTR [-304+ebp], esi                     ;26.12
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFARCS] ;26.12
        test      esi, esi                                      ;26.12
        mov       DWORD PTR [-352+ebp], ecx                     ;46.24
        mov       DWORD PTR [-348+ebp], ecx                     ;46.24
        mov       DWORD PTR [-344+ebp], ecx                     ;46.24
        mov       DWORD PTR [-332+ebp], ecx                     ;46.24
        mov       DWORD PTR [-328+ebp], ecx                     ;46.24
        mov       DWORD PTR [-324+ebp], ecx                     ;46.24
        mov       DWORD PTR [-320+ebp], ecx                     ;46.24
        mov       DWORD PTR [-316+ebp], ecx                     ;46.24
        mov       DWORD PTR [-312+ebp], ecx                     ;46.24
        mov       DWORD PTR [-308+ebp], ecx                     ;46.24
        cmovg     ecx, esi                                      ;26.12
        mov       DWORD PTR [-300+ebp], edi                     ;26.12
        mov       DWORD PTR [-340+ebp], 128                     ;46.24
        mov       DWORD PTR [-336+ebp], 2                       ;46.24
        mov       DWORD PTR [-296+ebp], esp                     ;26.12
        lea       edx, DWORD PTR [ecx*4]                        ;26.12
        mov       eax, edx                                      ;26.12
        call      __alloca_probe                                ;26.12
        and       esp, -16                                      ;26.12
        mov       eax, esp                                      ;26.12
                                ; LOE eax edx ecx esi
.B2.140:                        ; Preds .B2.1
        push      40                                            ;51.3
        mov       DWORD PTR [-92+ebp], eax                      ;26.12
        mov       eax, 2                                        ;51.3
        push      ecx                                           ;51.3
        mov       DWORD PTR [-312+ebp], edx                     ;51.3
        lea       edx, DWORD PTR [-356+ebp]                     ;51.3
        push      eax                                           ;51.3
        mov       DWORD PTR [-292+ebp], esi                     ;
        mov       esi, 1                                        ;51.3
        mov       DWORD PTR [-320+ebp], esi                     ;51.3
        mov       edi, 4                                        ;51.3
        mov       DWORD PTR [-308+ebp], esi                     ;51.3
        push      edx                                           ;51.3
        mov       DWORD PTR [-340+ebp], 133                     ;51.3
        mov       DWORD PTR [-348+ebp], edi                     ;51.3
        mov       DWORD PTR [-336+ebp], eax                     ;51.3
        mov       DWORD PTR [-344+ebp], 0                       ;51.3
        mov       DWORD PTR [-328+ebp], ecx                     ;51.3
        mov       DWORD PTR [-316+ebp], 10                      ;51.3
        mov       DWORD PTR [-324+ebp], edi                     ;51.3
        mov       esi, DWORD PTR [-292+ebp]                     ;51.3
        call      _for_check_mult_overflow                      ;51.3
                                ; LOE eax esi
.B2.139:                        ; Preds .B2.140
        add       esp, 16                                       ;51.3
                                ; LOE eax esi
.B2.2:                          ; Preds .B2.139
        mov       edx, DWORD PTR [-340+ebp]                     ;51.3
        and       eax, 1                                        ;51.3
        and       edx, 1                                        ;51.3
        lea       ecx, DWORD PTR [-352+ebp]                     ;51.3
        shl       eax, 4                                        ;51.3
        add       edx, edx                                      ;51.3
        or        edx, eax                                      ;51.3
        or        edx, 262144                                   ;51.3
        push      edx                                           ;51.3
        push      ecx                                           ;51.3
        push      DWORD PTR [-356+ebp]                          ;51.3
        call      _for_alloc_allocatable                        ;51.3
                                ; LOE esi
.B2.141:                        ; Preds .B2.2
        add       esp, 12                                       ;51.3
                                ; LOE esi
.B2.3:                          ; Preds .B2.141
        mov       eax, DWORD PTR [-316+ebp]                     ;52.3
        test      eax, eax                                      ;52.3
        mov       edx, DWORD PTR [-308+ebp]                     ;52.3
        jle       .B2.5         ; Prob 10%                      ;52.3
                                ; LOE eax edx esi
.B2.4:                          ; Preds .B2.3
        mov       ecx, DWORD PTR [-328+ebp]                     ;52.3
        test      ecx, ecx                                      ;52.3
        jg        .B2.79        ; Prob 50%                      ;52.3
                                ; LOE eax edx ecx esi
.B2.5:                          ; Preds .B2.130 .B2.133 .B2.3 .B2.4
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;55.3
        mov       DWORD PTR [-288+ebp], eax                     ;55.3
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+24] ;55.3
        test      eax, eax                                      ;55.3
        jle       .B2.12        ; Prob 50%                      ;55.3
                                ; LOE eax esi
.B2.6:                          ; Preds .B2.5
        mov       ecx, eax                                      ;55.3
        shr       ecx, 31                                       ;55.3
        add       ecx, eax                                      ;55.3
        sar       ecx, 1                                        ;55.3
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;55.3
        test      ecx, ecx                                      ;55.3
        jbe       .B2.136       ; Prob 10%                      ;55.3
                                ; LOE eax edx ecx esi
.B2.7:                          ; Preds .B2.6
        xor       edi, edi                                      ;
        mov       DWORD PTR [-360+ebp], eax                     ;
        mov       DWORD PTR [-292+ebp], esi                     ;
        xor       esi, esi                                      ;
        mov       eax, esi                                      ;
                                ; LOE eax edx ecx esi edi
.B2.8:                          ; Preds .B2.8 .B2.7
        inc       edi                                           ;55.3
        mov       DWORD PTR [400+eax+edx], esi                  ;55.3
        mov       DWORD PTR [1300+eax+edx], esi                 ;55.3
        add       eax, 1800                                     ;55.3
        cmp       edi, ecx                                      ;55.3
        jb        .B2.8         ; Prob 63%                      ;55.3
                                ; LOE eax edx ecx esi edi
.B2.9:                          ; Preds .B2.8
        mov       eax, DWORD PTR [-360+ebp]                     ;
        lea       ecx, DWORD PTR [1+edi+edi]                    ;55.3
        mov       esi, DWORD PTR [-292+ebp]                     ;
                                ; LOE eax edx ecx esi
.B2.10:                         ; Preds .B2.9 .B2.136
        lea       edi, DWORD PTR [-1+ecx]                       ;55.3
        cmp       eax, edi                                      ;55.3
        jbe       .B2.12        ; Prob 10%                      ;55.3
                                ; LOE edx ecx esi
.B2.11:                         ; Preds .B2.10
        mov       eax, DWORD PTR [-288+ebp]                     ;55.3
        mov       edi, eax                                      ;55.3
        neg       edi                                           ;55.3
        add       ecx, eax                                      ;55.3
        add       edi, ecx                                      ;55.3
        imul      ecx, edi, 900                                 ;55.3
        mov       DWORD PTR [-500+edx+ecx], 0                   ;55.3
                                ; LOE esi
.B2.12:                         ; Preds .B2.5 .B2.10 .B2.11
        test      esi, esi                                      ;56.3
        jle       .B2.15        ; Prob 50%                      ;56.3
                                ; LOE esi
.B2.13:                         ; Preds .B2.12
        cmp       esi, 24                                       ;56.3
        jle       .B2.84        ; Prob 0%                       ;56.3
                                ; LOE esi
.B2.14:                         ; Preds .B2.13
        shl       esi, 2                                        ;56.3
        push      esi                                           ;56.3
        push      0                                             ;56.3
        push      DWORD PTR [-92+ebp]                           ;56.3
        call      __intel_fast_memset                           ;56.3
                                ; LOE
.B2.142:                        ; Preds .B2.14
        add       esp, 12                                       ;56.3
                                ; LOE
.B2.15:                         ; Preds .B2.90 .B2.142 .B2.12 .B2.88
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFARCS] ;61.1
        mov       eax, 1                                        ;61.1
        test      esi, esi                                      ;61.1
        jle       .B2.113       ; Prob 2%                       ;61.1
                                ; LOE eax esi
.B2.16:                         ; Preds .B2.15
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC+32] ;63.12
        mov       DWORD PTR [-88+ebp], edi                      ;63.12
        pxor      xmm0, xmm0                                    ;69.4
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;62.7
        imul      edi, DWORD PTR [-288+ebp], 900                ;
        movss     xmm1, DWORD PTR [_2il0floatpacket.6]          ;99.45
        movss     xmm4, DWORD PTR [_2il0floatpacket.8]          ;81.105
        movss     xmm3, DWORD PTR [_2il0floatpacket.9]          ;74.117
        mov       DWORD PTR [-84+ebp], ecx                      ;62.7
        mov       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;74.46
        mov       DWORD PTR [-172+ebp], ecx                     ;74.46
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC] ;63.7
        mov       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;74.68
        mov       DWORD PTR [-168+ebp], edx                     ;63.7
        mov       edx, 1                                        ;
        mov       DWORD PTR [-80+ebp], edi                      ;
        mov       DWORD PTR [-76+ebp], ecx                      ;106.63
        mov       DWORD PTR [-164+ebp], esi                     ;106.63
                                ; LOE eax edx xmm0 xmm1
.B2.17:                         ; Preds .B2.100 .B2.16
        imul      esi, edx, 900                                 ;62.7
        sub       esi, DWORD PTR [-80+ebp]                      ;62.7
        add       esi, DWORD PTR [-84+ebp]                      ;62.7
        mov       ecx, DWORD PTR [-168+ebp]                     ;63.7
        movsx     edi, BYTE PTR [680+esi]                       ;62.12
        mov       DWORD PTR [-120+ebp], edi                     ;62.7
        sub       edi, DWORD PTR [-88+ebp]                      ;63.7
        shl       edi, 5                                        ;63.7
        mov       ecx, DWORD PTR [24+ecx+edi]                   ;63.7
        mov       edi, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST+32] ;72.9
        neg       edi                                           ;72.28
        add       edi, edx                                      ;72.28
        imul      edi, edi, 44                                  ;72.28
        mov       DWORD PTR [-116+ebp], ecx                     ;63.7
        mov       ecx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST] ;72.6
        mov       DWORD PTR [-64+ebp], ecx                      ;72.6
        mov       ecx, DWORD PTR [40+ecx+edi]                   ;72.9
        test      ecx, ecx                                      ;72.28
        jle       .B2.112       ; Prob 16%                      ;72.28
                                ; LOE eax edx ecx esi xmm0 xmm1
.B2.18:                         ; Preds .B2.17
        mov       DWORD PTR [-72+ebp], eax                      ;61.1
        xor       eax, eax                                      ;
        mov       edi, 1                                        ;
        movaps    xmm3, xmm0                                    ;
        movaps    xmm2, xmm0                                    ;
        mov       DWORD PTR [-52+ebp], ecx                      ;
        movss     DWORD PTR [-240+ebp], xmm2                    ;
        movss     DWORD PTR [-124+ebp], xmm3                    ;
        mov       DWORD PTR [-56+ebp], edi                      ;
        mov       DWORD PTR [-60+ebp], eax                      ;
        mov       DWORD PTR [-32+ebp], esi                      ;
        mov       DWORD PTR [-20+ebp], edx                      ;
        mov       ecx, DWORD PTR [-172+ebp]                     ;
        jmp       .B2.19        ; Prob 100%                     ;
                                ; LOE ecx
.B2.78:                         ; Preds .B2.77
        mov       eax, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST] ;207.9
        movss     DWORD PTR [-4+edx], xmm2                      ;207.9
        mov       DWORD PTR [-64+ebp], eax                      ;207.9
                                ; LOE ecx
.B2.19:                         ; Preds .B2.78 .B2.18
        mov       eax, DWORD PTR [-20+ebp]                      ;74.23
        sub       eax, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST+32] ;74.23
        imul      edi, eax, 44                                  ;74.23
        mov       esi, DWORD PTR [-64+ebp]                      ;74.26
        mov       eax, DWORD PTR [esi+edi]                      ;74.46
        mov       DWORD PTR [-8+ebp], eax                       ;74.46
        mov       edx, DWORD PTR [40+esi+edi]                   ;74.26
        mov       eax, DWORD PTR [32+esi+edi]                   ;74.46
        mov       esi, DWORD PTR [28+esi+edi]                   ;74.46
        imul      eax, esi                                      ;
        mov       DWORD PTR [-36+ebp], esi                      ;74.46
        mov       DWORD PTR [-4+ebp], edx                       ;74.26
        mov       esi, DWORD PTR [-8+ebp]                       ;74.23
        cmp       edx, DWORD PTR [-52+ebp]                      ;74.23
        je        .B2.21        ; Prob 50%                      ;74.23
                                ; LOE eax ecx esi
.B2.20:                         ; Preds .B2.19
        mov       edx, DWORD PTR [-52+ebp]                      ;74.46
        movss     xmm1, DWORD PTR [-124+ebp]                    ;74.66
        pxor      xmm2, xmm2                                    ;74.46
        lea       edi, DWORD PTR [1+edx]                        ;74.46
        imul      edi, DWORD PTR [-36+ebp]                      ;74.46
        add       edi, esi                                      ;74.46
        sub       edi, eax                                      ;74.46
        mov       edx, DWORD PTR [edi]                          ;74.68
        sub       edx, DWORD PTR [-76+ebp]                      ;74.46
        shl       edx, 8                                        ;74.46
        movss     xmm0, DWORD PTR [156+ecx+edx]                 ;74.68
        divss     xmm0, DWORD PTR [_2il0floatpacket.9]          ;74.117
        subss     xmm1, xmm0                                    ;74.66
        maxss     xmm2, xmm1                                    ;74.46
        movss     DWORD PTR [-124+ebp], xmm2                    ;74.46
                                ; LOE eax ecx esi
.B2.21:                         ; Preds .B2.19 .B2.20
        mov       edi, DWORD PTR [-52+ebp]                      ;75.4
        imul      edi, DWORD PTR [-36+ebp]                      ;75.4
        add       edi, esi                                      ;75.4
        sub       edi, eax                                      ;75.4
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;78.8
        neg       edx                                           ;78.89
        mov       edi, DWORD PTR [edi]                          ;75.4
        add       edx, edi                                      ;78.89
        shl       edx, 5                                        ;78.89
        mov       DWORD PTR [-48+ebp], edi                      ;75.4
        mov       DWORD PTR [-68+ebp], edi                      ;75.4
        mov       edi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;78.4
        movsx     edi, BYTE PTR [5+edi+edx]                     ;78.8
        mov       edx, edi                                      ;78.46
        and       edx, -5                                       ;78.46
        cmp       edx, 2                                        ;78.46
        je        .B2.23        ; Prob 16%                      ;78.46
                                ; LOE eax ecx esi edi
.B2.22:                         ; Preds .B2.21
        cmp       edi, 7                                        ;78.128
        jne       .B2.24        ; Prob 84%                      ;78.128
                                ; LOE eax ecx esi
.B2.23:                         ; Preds .B2.21 .B2.22
        mov       edx, DWORD PTR [-32+ebp]                      ;78.162
        mov       edi, DWORD PTR [-76+ebp]                      ;78.134
        neg       edi                                           ;78.134
        add       edi, DWORD PTR [-48+ebp]                      ;78.134
        movss     xmm0, DWORD PTR [856+edx]                     ;78.162
        shl       edi, 8                                        ;78.134
        mulss     xmm0, DWORD PTR [_2il0floatpacket.9]          ;78.134
        movss     DWORD PTR [156+ecx+edi], xmm0                 ;78.134
                                ; LOE eax ecx esi
.B2.24:                         ; Preds .B2.22 .B2.23
        mov       edx, DWORD PTR [-52+ebp]                      ;79.20
        mov       DWORD PTR [-8+ebp], esi                       ;
        mov       esi, edx                                      ;79.20
        neg       esi                                           ;79.20
        mov       edi, DWORD PTR [-4+ebp]                       ;79.20
        lea       esi, DWORD PTR [1+esi+edi]                    ;79.20
        mov       edi, DWORD PTR [-56+ebp]                      ;79.20
        cmp       esi, edi                                      ;79.20
        cmovge    edi, esi                                      ;79.20
        cmp       edx, 1                                        ;80.25
        mov       esi, DWORD PTR [-8+ebp]                       ;80.25
        mov       DWORD PTR [-56+ebp], edi                      ;79.20
        jle       .B2.111       ; Prob 16%                      ;80.25
                                ; LOE eax ecx esi
.B2.25:                         ; Preds .B2.24
        mov       DWORD PTR [-8+ebp], esi                       ;
        mov       esi, DWORD PTR [-76+ebp]                      ;81.14
        shl       esi, 8                                        ;81.14
        neg       esi                                           ;81.14
        add       ecx, esi                                      ;81.14
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;81.23
        neg       esi                                           ;81.14
        add       esi, DWORD PTR [-20+ebp]                      ;81.14
        imul      esi, esi, 152                                 ;81.14
        mov       edi, DWORD PTR [-48+ebp]                      ;81.14
        shl       edi, 8                                        ;81.14
        mov       DWORD PTR [-96+ebp], edi                      ;81.14
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;81.23
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_LINKIDMAP+32] ;81.23
        neg       edx                                           ;81.14
        movsx     edi, WORD PTR [148+edi+esi]                   ;81.23
        add       edx, edi                                      ;81.14
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_LINKIDMAP] ;81.23
        mov       DWORD PTR [-100+ebp], eax                     ;
        mov       eax, DWORD PTR [_DYNUST_AMS_MODULE_mp_SIR+32] ;81.23
        neg       eax                                           ;81.14
        movsx     edx, WORD PTR [esi+edx*2]                     ;81.23
        add       eax, edx                                      ;81.14
        mov       edx, DWORD PTR [_DYNUST_AMS_MODULE_mp_SIR]    ;81.23
        mov       esi, DWORD PTR [-8+ebp]                       ;83.27
        cvtsi2ss  xmm1, DWORD PTR [edx+eax*4]                   ;81.52
        divss     xmm1, DWORD PTR [_2il0floatpacket.8]          ;81.105
        mov       eax, DWORD PTR [-96+ebp]                      ;81.23
        movss     xmm0, DWORD PTR [240+eax+ecx]                 ;81.23
        mov       eax, DWORD PTR [-4+ebp]                       ;83.27
        subss     xmm0, xmm1                                    ;81.14
        cmp       eax, DWORD PTR [-56+ebp]                      ;83.27
        mov       eax, DWORD PTR [-100+ebp]                     ;83.27
        jle       .B2.33        ; Prob 50%                      ;83.27
                                ; LOE eax ecx esi al ah xmm0
.B2.26:                         ; Preds .B2.25
        sub       esi, eax                                      ;86.10
        mov       eax, DWORD PTR [-4+ebp]                       ;84.19
        sub       eax, DWORD PTR [-56+ebp]                      ;84.19
        imul      eax, DWORD PTR [-36+ebp]                      ;84.19
        mov       edx, DWORD PTR [eax+esi]                      ;84.7
        shl       edx, 8                                        ;84.7
        movss     xmm1, DWORD PTR [240+edx+ecx]                 ;86.20
        comiss    xmm1, xmm0                                    ;86.53
        jb        .B2.33        ; Prob 10%                      ;86.53
                                ; LOE ecx esi xmm0
.B2.27:                         ; Preds .B2.26
        movss     xmm2, DWORD PTR [-124+ebp]                    ;
        mov       eax, DWORD PTR [-56+ebp]                      ;
        movss     xmm3, DWORD PTR [_2il0floatpacket.9]          ;
                                ; LOE eax ecx esi xmm0 xmm2 xmm3
.B2.28:                         ; Preds .B2.31 .B2.27
        mov       edx, DWORD PTR [-4+ebp]                       ;86.69
        sub       edx, eax                                      ;86.69
        test      edx, edx                                      ;86.96
        jle       .B2.32        ; Prob 20%                      ;86.96
                                ; LOE eax edx ecx esi xmm0 xmm2 xmm3
.B2.29:                         ; Preds .B2.28
        imul      edx, DWORD PTR [-36+ebp]                      ;87.22
        mov       edx, DWORD PTR [esi+edx]                      ;87.10
        mov       edi, edx                                      ;
        shl       edi, 8                                        ;
        cmp       edx, DWORD PTR [-48+ebp]                      ;88.31
        je        .B2.31        ; Prob 50%                      ;88.31
                                ; LOE eax ecx esi edi xmm0 xmm2 xmm3
.B2.30:                         ; Preds .B2.29
        movss     xmm1, DWORD PTR [156+edi+ecx]                 ;88.77
        divss     xmm1, xmm3                                    ;88.106
        addss     xmm2, xmm1                                    ;88.63
                                ; LOE eax ecx esi edi xmm0 xmm2 xmm3
.B2.31:                         ; Preds .B2.29 .B2.30
        movss     xmm1, DWORD PTR [240+edi+ecx]                 ;86.20
        inc       eax                                           ;89.18
        comiss    xmm1, xmm0                                    ;86.53
        jae       .B2.28        ; Prob 82%                      ;86.53
                                ; LOE eax ecx esi xmm0 xmm2 xmm3
.B2.32:                         ; Preds .B2.28 .B2.31
        movss     DWORD PTR [-124+ebp], xmm2                    ;
        mov       DWORD PTR [-56+ebp], eax                      ;
                                ; LOE
.B2.33:                         ; Preds .B2.32 .B2.26 .B2.25 .B2.111
        lea       eax, DWORD PTR [-68+ebp]                      ;97.145
        push      eax                                           ;97.145
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_P_SIZE  ;97.145
                                ; LOE eax
.B2.143:                        ; Preds .B2.33
        mov       DWORD PTR [-12+ebp], eax                      ;97.145
        add       esp, 4                                        ;97.145
                                ; LOE
.B2.34:                         ; Preds .B2.143
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;97.13
        mov       DWORD PTR [-40+ebp], edi                      ;97.13
        neg       edi                                           ;97.105
        mov       ecx, DWORD PTR [-72+ebp]                      ;97.13
        add       edi, ecx                                      ;97.105
        mov       DWORD PTR [-28+ebp], ecx                      ;97.13
        imul      ecx, edi, 152                                 ;97.105
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;97.13
        mov       esi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;97.13
        mov       DWORD PTR [-16+ebp], esi                      ;97.13
        mov       eax, DWORD PTR [-68+ebp]                      ;97.105
        sub       eax, esi                                      ;97.105
        movsx     esi, WORD PTR [148+edx+ecx]                   ;97.13
        mov       DWORD PTR [-44+ebp], edx                      ;97.13
        sub       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_LINKIDMAP+32] ;97.105
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_LINKIDMAP] ;97.13
        mov       DWORD PTR [-24+ebp], ecx                      ;97.105
        shl       eax, 8                                        ;97.105
        movsx     ecx, WORD PTR [edx+esi*2]                     ;97.13
        sub       ecx, DWORD PTR [_DYNUST_AMS_MODULE_mp_SIR+32] ;97.105
        mov       edx, DWORD PTR [_DYNUST_AMS_MODULE_mp_SIR]    ;97.45
        cvtsi2ss  xmm2, DWORD PTR [edx+ecx*4]                   ;97.45
        divss     xmm2, DWORD PTR [_2il0floatpacket.8]          ;97.98
        mov       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;97.13
        mov       edx, DWORD PTR [-68+ebp]                      ;97.42
        movss     xmm0, DWORD PTR [240+ecx+eax]                 ;97.13
        comiss    xmm0, xmm2                                    ;97.42
        jae       .B2.36        ; Prob 50%                      ;97.42
                                ; LOE eax edx ecx xmm2
.B2.35:                         ; Preds .B2.34
        mov       edi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE+32] ;97.110
        neg       edi                                           ;97.105
        add       edi, edx                                      ;97.105
        shl       edi, 6                                        ;97.105
        mov       esi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE] ;97.42
        movsx     esi, WORD PTR [12+esi+edi]                    ;97.110
        mov       edi, DWORD PTR [-12+ebp]                      ;97.165
        dec       edi                                           ;97.165
        cmp       esi, edi                                      ;97.142
        jl        .B2.50        ; Prob 50%                      ;97.142
                                ; LOE eax edx ecx esi xmm2
.B2.36:                         ; Preds .B2.34 .B2.35
        mov       esi, DWORD PTR [-28+ebp]                      ;99.45
        sub       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;99.45
        imul      esi, esi, 900                                 ;99.45
        movss     xmm0, DWORD PTR [_2il0floatpacket.6]          ;99.45
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC+32] ;100.23
        shl       edi, 5                                        ;
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;99.12
        neg       edi                                           ;
        mov       edx, DWORD PTR [-120+ebp]                     ;100.23
        shl       edx, 5                                        ;100.8
        add       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC] ;
        movss     xmm1, DWORD PTR [672+eax+esi]                 ;99.15
        comiss    xmm0, xmm1                                    ;99.45
        cvtsi2ss  xmm3, DWORD PTR [12+edi+edx]                  ;100.8
        jbe       .B2.38        ; Prob 50%                      ;99.45
                                ; LOE eax esi edi xmm1 xmm2 xmm3
.B2.37:                         ; Preds .B2.36
        movaps    xmm1, xmm3                                    ;100.8
        jmp       .B2.39        ; Prob 100%                     ;100.8
                                ; LOE eax esi edi xmm1 xmm3
.B2.38:                         ; Preds .B2.36
        mov       edx, DWORD PTR [-44+ebp]                      ;102.116
        mov       ecx, DWORD PTR [-24+ebp]                      ;102.116
        movss     xmm0, DWORD PTR [-124+ebp]                    ;102.52
        divss     xmm2, DWORD PTR [20+edx+ecx]                  ;102.116
        mulss     xmm2, xmm1                                    ;102.147
        movaps    xmm1, xmm3                                    ;102.8
        divss     xmm0, xmm2                                    ;102.52
        minss     xmm1, xmm0                                    ;102.8
                                ; LOE eax esi edi xmm1 xmm3
.B2.39:                         ; Preds .B2.37 .B2.38
        test      BYTE PTR [4+eax+esi], 1                       ;105.15
        je        .B2.41        ; Prob 60%                      ;105.15
                                ; LOE eax esi edi xmm1 xmm3
.B2.40:                         ; Preds .B2.39
        mulss     xmm3, DWORD PTR [_2il0floatpacket.10]         ;106.63
        minss     xmm1, xmm3                                    ;106.15
        movss     DWORD PTR [-112+ebp], xmm1                    ;106.15
        jmp       .B2.42        ; Prob 100%                     ;106.15
                                ; LOE eax esi edi xmm1
.B2.41:                         ; Preds .B2.39
        movss     DWORD PTR [-112+ebp], xmm1                    ;168.8
                                ; LOE eax esi edi xmm1
.B2.42:                         ; Preds .B2.40 .B2.41
        pxor      xmm0, xmm0                                    ;109.22
        comiss    xmm0, xmm1                                    ;109.22
        jbe       .B2.44        ; Prob 50%                      ;109.22
                                ; LOE eax esi edi
.B2.43:                         ; Preds .B2.42
        push      32                                            ;110.15
        mov       DWORD PTR [-280+ebp], 0                       ;110.15
        lea       edx, DWORD PTR [-216+ebp]                     ;110.15
        push      edx                                           ;110.15
        push      OFFSET FLAT: __STRLITPACK_18.0.2              ;110.15
        push      -2088435968                                   ;110.15
        push      -1                                            ;110.15
        mov       DWORD PTR [-216+ebp], 16                      ;110.15
        lea       ecx, DWORD PTR [-280+ebp]                     ;110.15
        push      ecx                                           ;110.15
        mov       DWORD PTR [-212+ebp], OFFSET FLAT: __STRLITPACK_17 ;110.15
        mov       DWORD PTR [-128+ebp], eax                     ;110.15
        call      _for_write_seq_lis                            ;110.15
                                ; LOE esi edi
.B2.144:                        ; Preds .B2.43
        mov       eax, DWORD PTR [-128+ebp]                     ;
        add       esp, 24                                       ;110.15
                                ; LOE eax esi edi
.B2.44:                         ; Preds .B2.144 .B2.42
        movsx     eax, BYTE PTR [680+eax+esi]                   ;113.17
        add       esp, -24                                      ;116.14
        mov       DWORD PTR [-120+ebp], eax                     ;113.12
        lea       edx, DWORD PTR [-108+ebp]                     ;116.14
        shl       eax, 5                                        ;114.12
        lea       ecx, DWORD PTR [-112+ebp]                     ;116.14
        mov       DWORD PTR [12+esp], edx                       ;116.14
        lea       edx, DWORD PTR [-120+ebp]                     ;116.14
        mov       DWORD PTR [16+esp], ecx                       ;116.14
        lea       ecx, DWORD PTR [-116+ebp]                     ;116.14
        mov       esi, DWORD PTR [24+eax+edi]                   ;114.12
        mov       edi, DWORD PTR [28+eax+edi]                   ;115.12
        lea       eax, DWORD PTR [-72+ebp]                      ;116.14
        mov       DWORD PTR [-116+ebp], esi                     ;114.12
        lea       esi, DWORD PTR [-104+ebp]                     ;116.14
        mov       DWORD PTR [-108+ebp], edi                     ;115.12
        mov       DWORD PTR [20+esp], esi                       ;116.14
        call      _DYNUST_AMS_MODULE_mp_VEHSPEED_UPDATE.        ;116.14
                                ; LOE
.B2.145:                        ; Preds .B2.44
        add       esp, 24                                       ;116.14
                                ; LOE
.B2.45:                         ; Preds .B2.145
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;117.22
        mov       ecx, DWORD PTR [-72+ebp]                      ;117.30
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;117.30
        mov       DWORD PTR [-84+ebp], eax                      ;117.22
        mov       DWORD PTR [-20+ebp], ecx                      ;117.30
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_MOVESFLAG], 0 ;117.22
        jle       .B2.49        ; Prob 16%                      ;117.22
                                ; LOE edx ecx cl ch
.B2.46:                         ; Preds .B2.45
        mov       eax, ecx                                      ;117.68
        sub       eax, edx                                      ;117.68
        imul      esi, eax, 900                                 ;117.68
        mov       ecx, DWORD PTR [-84+ebp]                      ;117.30
        test      BYTE PTR [ecx+esi], 1                         ;117.30
        je        .B2.49        ; Prob 60%                      ;117.30
                                ; LOE edx
.B2.47:                         ; Preds .B2.46
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ITERATION] ;117.25
        mov       esi, -1                                       ;117.68
        xor       ecx, ecx                                      ;117.68
        cmp       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ITEMAX] ;117.68
        cmovne    esi, ecx                                      ;117.68
        or        esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_REACH_CONVERG] ;117.87
        test      esi, 1                                        ;117.74
        je        .B2.49        ; Prob 60%                      ;117.74
                                ; LOE edx
.B2.48:                         ; Preds .B2.47
        mov       eax, DWORD PTR [-68+ebp]                      ;118.46
        mov       esi, eax                                      ;118.11
        mov       edi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;118.46
        sub       esi, edi                                      ;118.11
        shl       esi, 8                                        ;118.11
        mov       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;118.11
        movss     xmm1, DWORD PTR [-104+ebp]                    ;119.11
        mov       DWORD PTR [-76+ebp], edi                      ;118.46
        movss     xmm0, DWORD PTR [12+ecx+esi]                  ;118.11
        mov       edi, DWORD PTR [12+ecx+esi]                   ;118.11
        subss     xmm1, xmm0                                    ;120.54
        divss     xmm1, DWORD PTR [_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT] ;120.11
        mov       DWORD PTR [8+ecx+esi], edi                    ;118.11
        mov       edi, DWORD PTR [-104+ebp]                     ;119.11
        mov       DWORD PTR [12+ecx+esi], edi                   ;119.11
        movss     DWORD PTR [4+ecx+esi], xmm1                   ;120.11
        jmp       .B2.75        ; Prob 100%                     ;120.11
                                ; LOE eax edx ecx
.B2.49:                         ; Preds .B2.47 .B2.46 .B2.45
        mov       eax, DWORD PTR [-68+ebp]                      ;122.14
        mov       esi, eax                                      ;122.14
        mov       edi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;122.14
        sub       esi, edi                                      ;122.14
        shl       esi, 8                                        ;122.14
        mov       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;122.14
        mov       DWORD PTR [-76+ebp], edi                      ;122.14
        mov       edi, DWORD PTR [-104+ebp]                     ;122.14
        mov       DWORD PTR [12+ecx+esi], edi                   ;122.14
        jmp       .B2.75        ; Prob 100%                     ;122.14
                                ; LOE eax edx ecx
.B2.50:                         ; Preds .B2.35
        mov       edi, DWORD PTR [148+ecx+eax]                  ;128.15
        shl       edi, 2                                        ;128.46
        neg       edi                                           ;128.46
        add       edi, DWORD PTR [116+ecx+eax]                  ;128.46
        cmp       DWORD PTR [4+edi], 0                          ;128.46
        jle       .B2.109       ; Prob 16%                      ;128.46
                                ; LOE edx ecx esi
.B2.51:                         ; Preds .B2.110 .B2.50
        mov       edi, DWORD PTR [-16+ebp]                      ;132.12
        shl       edi, 8                                        ;132.12
        neg       edi                                           ;132.12
        shl       edx, 8                                        ;132.12
        add       ecx, edi                                      ;132.12
        mov       eax, DWORD PTR [148+edx+ecx]                  ;132.19
        shl       eax, 2                                        ;132.12
        neg       eax                                           ;132.12
        mov       edi, DWORD PTR [116+edx+ecx]                  ;132.19
        mov       esi, DWORD PTR [4+eax+edi]                    ;132.12
        mov       DWORD PTR [-152+ebp], esi                     ;132.12
        mov       eax, DWORD PTR [8+eax+edi]                    ;133.12
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+44] ;135.15
        neg       esi                                           ;135.57
        mov       DWORD PTR [-140+ebp], eax                     ;133.12
        add       esi, eax                                      ;135.57
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+32] ;135.15
        neg       eax                                           ;135.57
        add       eax, DWORD PTR [-28+ebp]                      ;135.57
        imul      esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+40] ;135.57
        lea       edi, DWORD PTR [eax*4]                        ;135.57
        shl       eax, 5                                        ;135.57
        sub       eax, edi                                      ;135.57
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE] ;135.57
        lea       eax, DWORD PTR [4+eax+edi]                    ;135.57
        cmp       DWORD PTR [eax+esi], 0                        ;135.57
        mov       esi, DWORD PTR [-140+ebp]                     ;135.57
        jle       .B2.108       ; Prob 16%                      ;135.57
                                ; LOE edx ecx esi
.B2.52:                         ; Preds .B2.51
        mov       edi, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST+32] ;139.17
        neg       edi                                           ;139.42
        add       edi, DWORD PTR [-152+ebp]                     ;139.42
        imul      edi, edi, 44                                  ;139.42
        sub       esi, DWORD PTR [-308+ebp]                     ;
        mov       eax, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST] ;139.14
        imul      esi, DWORD PTR [-312+ebp]                     ;
        mov       DWORD PTR [-176+ebp], eax                     ;139.14
        mov       DWORD PTR [-204+ebp], edi                     ;139.42
        mov       eax, DWORD PTR [40+eax+edi]                   ;139.17
        mov       edi, DWORD PTR [-28+ebp]                      ;
        sub       edi, DWORD PTR [-320+ebp]                     ;
        add       esi, DWORD PTR [-352+ebp]                     ;
        mov       DWORD PTR [-180+ebp], edi                     ;
        test      eax, eax                                      ;139.39
        jle       .B2.59        ; Prob 16%                      ;139.39
                                ; LOE eax edx ecx esi edi
.B2.53:                         ; Preds .B2.52
        mov       DWORD PTR [-140+ebp], esi                     ;
        mov       edi, DWORD PTR [esi+edi*4]                    ;139.71
        mov       esi, edi                                      ;
        neg       esi                                           ;
        mov       DWORD PTR [-232+ebp], esi                     ;
        cmp       eax, edi                                      ;139.69
        mov       DWORD PTR [-228+ebp], edi                     ;139.71
        mov       esi, DWORD PTR [-140+ebp]                     ;139.69
        jle       .B2.59        ; Prob 50%                      ;139.69
                                ; LOE eax edx ecx esi
.B2.54:                         ; Preds .B2.53
        mov       edi, DWORD PTR [_DYNUST_AMS_MODULE_mp_SIR+32] ;140.38
        mov       DWORD PTR [-208+ebp], edi                     ;140.38
        imul      edi, DWORD PTR [-40+ebp], 152                 ;140.16
        movss     xmm2, DWORD PTR [240+edx+ecx]                 ;140.137
        mov       DWORD PTR [-220+ebp], ecx                     ;
        imul      ecx, DWORD PTR [-28+ebp], 152                 ;140.16
        mov       DWORD PTR [-140+ebp], esi                     ;
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_LINKIDMAP+32] ;140.38
        mov       DWORD PTR [-188+ebp], esi                     ;140.38
        mov       esi, DWORD PTR [-44+ebp]                      ;140.16
        mov       edx, esi                                      ;140.16
        sub       edx, edi                                      ;140.16
        add       edx, ecx                                      ;140.16
        mov       DWORD PTR [-196+ebp], edx                     ;140.16
        mov       DWORD PTR [-156+ebp], eax                     ;
        mov       eax, DWORD PTR [_DYNUST_AMS_MODULE_mp_SIR]    ;140.38
        movsx     edx, WORD PTR [148+edx]                       ;140.38
        mov       DWORD PTR [-192+ebp], eax                     ;140.38
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_LINKIDMAP] ;140.38
        mov       DWORD PTR [-200+ebp], eax                     ;140.38
        lea       ecx, DWORD PTR [edx+edx]                      ;140.38
        mov       DWORD PTR [-184+ebp], ecx                     ;140.38
        imul      ecx, DWORD PTR [-152+ebp], 152                ;132.12
        add       ecx, esi                                      ;140.16
        lea       esi, DWORD PTR [eax+edx*2]                    ;140.16
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_LINKIDMAP+32] ;140.16
        sub       ecx, edi                                      ;140.16
        mov       DWORD PTR [-284+ebp], edi                     ;140.16
        mov       edi, DWORD PTR [_DYNUST_AMS_MODULE_mp_SIR]    ;140.75
        lea       edx, DWORD PTR [eax+eax]                      ;140.16
        mov       eax, DWORD PTR [-176+ebp]                     ;141.25
        sub       esi, edx                                      ;140.16
        movss     xmm3, DWORD PTR [20+ecx]                      ;140.38
        mov       edx, DWORD PTR [-204+ebp]                     ;141.25
        movaps    xmm1, xmm3                                    ;140.71
        addss     xmm1, xmm2                                    ;140.71
        movsx     ecx, WORD PTR [esi]                           ;140.38
        movaps    xmm4, xmm1                                    ;140.71
        sub       ecx, DWORD PTR [_DYNUST_AMS_MODULE_mp_SIR+32] ;140.16
        mov       esi, DWORD PTR [32+eax+edx]                   ;141.25
        cvtsi2ss  xmm0, DWORD PTR [edi+ecx*4]                   ;140.75
        divss     xmm0, DWORD PTR [_2il0floatpacket.8]          ;140.128
        mov       edi, DWORD PTR [28+eax+edx]                   ;141.25
        subss     xmm4, xmm0                                    ;140.71
        imul      esi, edi                                      ;142.7
        pxor      xmm0, xmm0                                    ;140.16
        mov       ecx, DWORD PTR [eax+edx]                      ;141.25
        maxss     xmm0, xmm4                                    ;140.16
        mov       eax, DWORD PTR [-156+ebp]                     ;141.25
        mov       edx, eax                                      ;141.25
        sub       edx, DWORD PTR [-228+ebp]                     ;141.25
        sub       ecx, esi                                      ;142.7
        imul      edx, edi                                      ;141.25
        mov       esi, DWORD PTR [edx+ecx]                      ;141.13
        shl       esi, 8                                        ;141.13
        mov       DWORD PTR [-224+ebp], ecx                     ;142.7
        mov       ecx, DWORD PTR [-220+ebp]                     ;142.17
        mov       DWORD PTR [-236+ebp], edi                     ;141.25
        movss     xmm5, DWORD PTR [240+esi+ecx]                 ;142.17
        mov       esi, DWORD PTR [-140+ebp]                     ;142.50
        comiss    xmm5, xmm0                                    ;142.50
        jb        .B2.60        ; Prob 10%                      ;142.50
                                ; LOE eax ecx esi al cl ah ch xmm0 xmm1 xmm2 xmm3
.B2.55:                         ; Preds .B2.54
        mov       DWORD PTR [-220+ebp], ecx                     ;
        mov       DWORD PTR [-140+ebp], esi                     ;
        mov       DWORD PTR [-156+ebp], eax                     ;
        movss     xmm6, DWORD PTR [-240+ebp]                    ;
        mov       ecx, DWORD PTR [-232+ebp]                     ;
        movss     xmm7, DWORD PTR [_2il0floatpacket.9]          ;
                                ; LOE ecx xmm0 xmm1 xmm2 xmm3 xmm6 xmm7
.B2.56:                         ; Preds .B2.57 .B2.55
        mov       eax, DWORD PTR [-156+ebp]                     ;142.7
        add       eax, ecx                                      ;142.7
        test      eax, eax                                      ;142.109
        jle       .B2.58        ; Prob 20%                      ;142.109
                                ; LOE eax ecx xmm0 xmm1 xmm2 xmm3 xmm6 xmm7
.B2.57:                         ; Preds .B2.56
        imul      eax, DWORD PTR [-236+ebp]                     ;143.21
        dec       ecx                                           ;145.18
        mov       esi, DWORD PTR [-224+ebp]                     ;143.9
        mov       edi, DWORD PTR [-140+ebp]                     ;145.18
        mov       DWORD PTR [-232+ebp], ecx                     ;145.18
        mov       edx, DWORD PTR [eax+esi]                      ;143.9
        shl       edx, 8                                        ;84.7
        mov       eax, DWORD PTR [-220+ebp]                     ;144.32
        mov       esi, DWORD PTR [-180+ebp]                     ;145.18
        mov       ecx, DWORD PTR [-228+ebp]                     ;145.18
        movss     xmm4, DWORD PTR [156+edx+eax]                 ;144.32
        inc       ecx                                           ;145.18
        divss     xmm4, xmm7                                    ;144.61
        movss     xmm5, DWORD PTR [240+edx+eax]                 ;142.17
        addss     xmm6, xmm4                                    ;144.18
        comiss    xmm5, xmm0                                    ;142.50
        mov       DWORD PTR [-228+ebp], ecx                     ;145.18
        mov       DWORD PTR [edi+esi*4], ecx                    ;145.18
        mov       ecx, DWORD PTR [-232+ebp]                     ;142.50
        jae       .B2.56        ; Prob 82%                      ;142.50
                                ; LOE ecx xmm0 xmm1 xmm2 xmm3 xmm6 xmm7
.B2.58:                         ; Preds .B2.56 .B2.57
        movss     DWORD PTR [-240+ebp], xmm6                    ;
        jmp       .B2.60        ; Prob 100%                     ;
                                ; LOE xmm1 xmm2 xmm3
.B2.59:                         ; Preds .B2.52 .B2.53
        mov       eax, DWORD PTR [-180+ebp]                     ;148.16
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_LINKIDMAP] ;154.14
        pxor      xmm0, xmm0                                    ;
        movss     xmm2, DWORD PTR [240+edx+ecx]                 ;154.80
        imul      edx, DWORD PTR [-40+ebp], 152                 ;
        movss     DWORD PTR [-240+ebp], xmm0                    ;
        mov       DWORD PTR [-200+ebp], edi                     ;154.14
        imul      edi, DWORD PTR [-28+ebp], 152                 ;
        mov       DWORD PTR [esi+eax*4], 0                      ;148.16
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_LINKIDMAP+32] ;154.14
        mov       ecx, DWORD PTR [-44+ebp]                      ;
        mov       DWORD PTR [-188+ebp], eax                     ;154.14
        mov       eax, ecx                                      ;
        sub       eax, edx                                      ;
        add       eax, edi                                      ;
        imul      edi, DWORD PTR [-152+ebp], 152                ;132.12
        add       edi, ecx                                      ;154.109
        sub       edi, edx                                      ;154.109
        mov       esi, DWORD PTR [_DYNUST_AMS_MODULE_mp_SIR]    ;154.14
        mov       DWORD PTR [-192+ebp], esi                     ;154.14
        mov       esi, DWORD PTR [_DYNUST_AMS_MODULE_mp_SIR+32] ;154.14
        movss     xmm3, DWORD PTR [20+edi]                      ;154.109
        movaps    xmm1, xmm3                                    ;154.108
        mov       DWORD PTR [-208+ebp], esi                     ;154.14
        addss     xmm1, xmm2                                    ;154.108
        movsx     esi, WORD PTR [148+eax]                       ;154.14
        add       esi, esi                                      ;154.14
        mov       DWORD PTR [-284+ebp], edx                     ;
        mov       DWORD PTR [-196+ebp], eax                     ;
        mov       DWORD PTR [-184+ebp], esi                     ;154.14
                                ; LOE xmm1 xmm2 xmm3
.B2.60:                         ; Preds .B2.58 .B2.54 .B2.59
        mov       edx, DWORD PTR [-188+ebp]                     ;154.78
        add       edx, edx                                      ;154.78
        mov       ecx, DWORD PTR [-184+ebp]                     ;154.78
        neg       edx                                           ;154.78
        add       ecx, DWORD PTR [-200+ebp]                     ;154.78
        mov       edi, DWORD PTR [-192+ebp]                     ;154.17
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;155.93
        movsx     edx, WORD PTR [edx+ecx]                       ;154.14
        sub       edx, DWORD PTR [-208+ebp]                     ;154.78
        mov       DWORD PTR [-148+ebp], esi                     ;155.93
        imul      esi, esi, 900                                 ;
        cvtsi2ss  xmm6, DWORD PTR [edi+edx*4]                   ;154.17
        imul      edx, DWORD PTR [-28+ebp], 900                 ;
        divss     xmm6, DWORD PTR [_2il0floatpacket.8]          ;154.70
        mov       edi, edx                                      ;155.93
        sub       edi, esi                                      ;155.93
        mov       ecx, DWORD PTR [-196+ebp]                     ;155.62
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;155.61
        movss     xmm4, DWORD PTR [20+ecx]                      ;155.62
        imul      ecx, DWORD PTR [-152+ebp], 900                ;132.12
        movss     xmm5, DWORD PTR [672+eax+edi]                 ;155.93
        comiss    xmm1, xmm6                                    ;154.78
        jbe       .B2.62        ; Prob 50%                      ;154.78
                                ; LOE eax edx ecx esi xmm2 xmm3 xmm4 xmm5 xmm6
.B2.61:                         ; Preds .B2.60
        movaps    xmm0, xmm2                                    ;155.61
        neg       esi                                           ;155.16
        divss     xmm0, xmm4                                    ;155.61
        add       esi, ecx                                      ;155.16
        subss     xmm6, xmm2                                    ;156.79
        mulss     xmm0, xmm5                                    ;155.92
        divss     xmm6, xmm3                                    ;156.109
        mulss     xmm6, DWORD PTR [672+eax+esi]                 ;156.143
        addss     xmm0, xmm6                                    ;155.16
        jmp       .B2.63        ; Prob 100%                     ;155.16
                                ; LOE eax edx ecx xmm0
.B2.62:                         ; Preds .B2.60
        neg       esi                                           ;158.16
        movaps    xmm0, xmm2                                    ;158.61
        add       esi, ecx                                      ;158.16
        subss     xmm6, xmm2                                    ;160.77
        divss     xmm0, xmm4                                    ;158.61
        mulss     xmm6, DWORD PTR [672+eax+esi]                 ;160.107
        mulss     xmm0, xmm5                                    ;158.92
        divss     xmm6, xmm3                                    ;160.140
        addss     xmm0, xmm6                                    ;158.16
                                ; LOE eax edx ecx xmm0
.B2.63:                         ; Preds .B2.108 .B2.61 .B2.62
        mov       DWORD PTR [-136+ebp], eax                     ;
        mov       eax, DWORD PTR [-120+ebp]                     ;165.31
        shl       eax, 5                                        ;165.31
        mov       DWORD PTR [-132+ebp], edx                     ;
        imul      edx, DWORD PTR [-148+ebp], 900                ;164.15
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC+32] ;165.31
        sub       ecx, edx                                      ;164.15
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC] ;165.31
        add       eax, edi                                      ;165.31
        shl       esi, 5                                        ;
        sub       eax, esi                                      ;165.31
        mov       DWORD PTR [-144+ebp], edx                     ;164.15
        mov       edx, DWORD PTR [-132+ebp]                     ;164.15
        cvtsi2ss  xmm3, DWORD PTR [12+eax]                      ;165.31
        mov       eax, DWORD PTR [-136+ebp]                     ;164.15
        test      BYTE PTR [4+eax+ecx], 1                       ;164.15
        je        .B2.65        ; Prob 60%                      ;164.15
                                ; LOE eax edx esi edi al dl ah dh xmm0 xmm3
.B2.64:                         ; Preds .B2.63
        imul      ecx, DWORD PTR [-152+ebp], 152                ;132.12
        movss     xmm2, DWORD PTR [-240+ebp]                    ;165.15
        add       ecx, DWORD PTR [-44+ebp]                      ;165.15
        sub       ecx, DWORD PTR [-284+ebp]                     ;165.15
        movss     xmm1, DWORD PTR [20+ecx]                      ;165.52
        mulss     xmm1, DWORD PTR [_2il0floatpacket.10]         ;165.47
        mulss     xmm1, xmm3                                    ;165.51
        minss     xmm2, xmm1                                    ;165.15
        movss     DWORD PTR [-240+ebp], xmm2                    ;165.15
                                ; LOE eax edx esi edi al dl ah dh xmm0 xmm3
.B2.65:                         ; Preds .B2.63 .B2.64
        movss     xmm1, DWORD PTR [_2il0floatpacket.6]          ;167.23
        comiss    xmm1, xmm0                                    ;167.23
        jbe       .B2.67        ; Prob 50%                      ;167.23
                                ; LOE eax edx esi edi al dl ah dh xmm0 xmm3
.B2.66:                         ; Preds .B2.65
        movaps    xmm2, xmm3                                    ;168.8
        jmp       .B2.68        ; Prob 100%                     ;168.8
                                ; LOE eax edx esi edi al dl ah dh xmm2 xmm3
.B2.67:                         ; Preds .B2.65
        movss     xmm1, DWORD PTR [-124+ebp]                    ;170.53
        movaps    xmm2, xmm3                                    ;170.8
        addss     xmm1, DWORD PTR [-240+ebp]                    ;170.53
        divss     xmm1, xmm0                                    ;170.59
        minss     xmm2, xmm1                                    ;170.8
                                ; LOE eax edx esi edi al dl ah dh xmm2 xmm3
.B2.68:                         ; Preds .B2.66 .B2.67
        sub       edx, DWORD PTR [-144+ebp]                     ;172.15
        test      BYTE PTR [4+eax+edx], 1                       ;172.15
        je        .B2.70        ; Prob 60%                      ;172.15
                                ; LOE eax edx esi edi al ah xmm2 xmm3
.B2.69:                         ; Preds .B2.68
        mulss     xmm3, DWORD PTR [_2il0floatpacket.10]         ;173.63
        minss     xmm2, xmm3                                    ;173.15
        movss     DWORD PTR [-112+ebp], xmm2                    ;173.15
        jmp       .B2.71        ; Prob 100%                     ;173.15
                                ; LOE eax edx esi edi al ah xmm2
.B2.70:                         ; Preds .B2.68
        movss     DWORD PTR [-112+ebp], xmm2                    ;168.8
                                ; LOE eax edx esi edi al ah xmm2
.B2.71:                         ; Preds .B2.69 .B2.70
        pxor      xmm0, xmm0                                    ;176.22
        comiss    xmm0, xmm2                                    ;176.22
        jbe       .B2.73        ; Prob 50%                      ;176.22
                                ; LOE eax edx esi edi al ah
.B2.72:                         ; Preds .B2.71
        push      32                                            ;177.15
        mov       DWORD PTR [-280+ebp], 0                       ;177.15
        lea       ecx, DWORD PTR [-248+ebp]                     ;177.15
        push      ecx                                           ;177.15
        push      OFFSET FLAT: __STRLITPACK_19.0.2              ;177.15
        push      -2088435968                                   ;177.15
        push      -1                                            ;177.15
        mov       DWORD PTR [-248+ebp], 16                      ;177.15
        lea       ecx, DWORD PTR [-280+ebp]                     ;177.15
        push      ecx                                           ;177.15
        mov       DWORD PTR [-244+ebp], OFFSET FLAT: __STRLITPACK_16 ;177.15
        mov       DWORD PTR [-132+ebp], edx                     ;177.15
        mov       DWORD PTR [-136+ebp], eax                     ;177.15
        call      _for_write_seq_lis                            ;177.15
                                ; LOE esi edi
.B2.146:                        ; Preds .B2.72
        mov       eax, DWORD PTR [-136+ebp]                     ;
        add       esp, 24                                       ;177.15
        mov       edx, DWORD PTR [-132+ebp]                     ;
                                ; LOE eax edx esi edi al ah
.B2.73:                         ; Preds .B2.146 .B2.71
        movsx     eax, BYTE PTR [680+eax+edx]                   ;180.17
        sub       edi, esi                                      ;181.12
        mov       DWORD PTR [-120+ebp], eax                     ;180.12
        add       esp, -24                                      ;183.14
        shl       eax, 5                                        ;181.12
        lea       edx, DWORD PTR [-108+ebp]                     ;183.14
        lea       ecx, DWORD PTR [-112+ebp]                     ;183.14
        mov       DWORD PTR [12+esp], edx                       ;183.14
        lea       edx, DWORD PTR [-120+ebp]                     ;183.14
        mov       esi, DWORD PTR [24+eax+edi]                   ;181.12
        mov       edi, DWORD PTR [28+eax+edi]                   ;182.12
        lea       eax, DWORD PTR [-72+ebp]                      ;183.14
        mov       DWORD PTR [16+esp], ecx                       ;183.14
        lea       ecx, DWORD PTR [-116+ebp]                     ;183.14
        mov       DWORD PTR [-116+ebp], esi                     ;181.12
        lea       esi, DWORD PTR [-104+ebp]                     ;183.14
        mov       DWORD PTR [-108+ebp], edi                     ;182.12
        mov       DWORD PTR [20+esp], esi                       ;183.14
        call      _DYNUST_AMS_MODULE_mp_VEHSPEED_UPDATE.        ;183.14
                                ; LOE
.B2.147:                        ; Preds .B2.73
        add       esp, 24                                       ;183.14
                                ; LOE
.B2.74:                         ; Preds .B2.147
        mov       eax, DWORD PTR [-68+ebp]                      ;184.6
        mov       edi, eax                                      ;184.6
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;184.6
        sub       edi, edx                                      ;184.6
        shl       edi, 8                                        ;184.6
        mov       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;184.6
        mov       esi, DWORD PTR [-104+ebp]                     ;184.6
        mov       DWORD PTR [-76+ebp], edx                      ;184.6
        mov       DWORD PTR [12+ecx+edi], esi                   ;184.6
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;188.12
        mov       edi, DWORD PTR [-72+ebp]                      ;188.12
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;188.12
        mov       DWORD PTR [-84+ebp], esi                      ;188.12
        mov       DWORD PTR [-20+ebp], edi                      ;188.12
                                ; LOE eax edx ecx
.B2.75:                         ; Preds .B2.49 .B2.48 .B2.74
        imul      edx, edx, 900                                 ;188.41
        imul      edi, DWORD PTR [-20+ebp], 900                 ;188.41
        sub       edi, edx                                      ;188.41
        add       edi, DWORD PTR [-84+ebp]                      ;188.41
        sub       eax, DWORD PTR [-76+ebp]                      ;188.41
        shl       eax, 8                                        ;188.41
        mov       DWORD PTR [-80+ebp], edx                      ;188.41
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC+32] ;188.44
        movsx     edx, BYTE PTR [680+edi]                       ;188.12
        sub       edx, esi                                      ;188.41
        shl       edx, 5                                        ;188.41
        movss     xmm1, DWORD PTR [12+ecx+eax]                  ;188.12
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC] ;188.44
        mov       DWORD PTR [-88+ebp], esi                      ;188.44
        mov       DWORD PTR [-32+ebp], edi                      ;188.41
        cvtsi2ss  xmm0, DWORD PTR [8+eax+edx]                   ;188.44
        comiss    xmm0, xmm1                                    ;188.41
        jb        .B2.77        ; Prob 50%                      ;188.41
                                ; LOE eax ecx edi xmm1
.B2.76:                         ; Preds .B2.75
        mov       edx, edi                                      ;189.6
        inc       DWORD PTR [400+edx]                           ;189.6
                                ; LOE eax ecx xmm1
.B2.77:                         ; Preds .B2.75 .B2.76
        mov       edx, DWORD PTR [-92+ebp]                      ;192.9
        mov       esi, DWORD PTR [-20+ebp]                      ;192.9
        mov       edi, DWORD PTR [-52+ebp]                      ;203.7
        dec       edi                                           ;203.7
        inc       DWORD PTR [-60+ebp]                           ;191.3
        lea       edx, DWORD PTR [edx+esi*4]                    ;192.9
        movss     xmm2, DWORD PTR [-4+edx]                      ;192.24
        test      edi, edi                                      ;203.7
        mov       DWORD PTR [-52+ebp], edi                      ;203.7
        addss     xmm2, xmm1                                    ;192.9
        jg        .B2.78        ; Prob 82%                      ;203.7
        jmp       .B2.95        ; Prob 100%                     ;203.7
                                ; LOE eax edx ecx xmm2
.B2.79:                         ; Preds .B2.4
        xor       edi, edi                                      ;
        mov       DWORD PTR [-272+ebp], edi                     ;
        lea       edi, DWORD PTR [ecx*4]                        ;
        mov       DWORD PTR [-276+ebp], edi                     ;
        pxor      xmm0, xmm0                                    ;52.3
        mov       DWORD PTR [-360+ebp], edx                     ;
        mov       edi, DWORD PTR [-352+ebp]                     ;
        mov       DWORD PTR [-292+ebp], esi                     ;
        mov       DWORD PTR [-280+ebp], edi                     ;
        mov       DWORD PTR [-268+ebp], ecx                     ;
        mov       DWORD PTR [-288+ebp], eax                     ;
        mov       DWORD PTR [-284+ebp], edx                     ;
        mov       esi, DWORD PTR [-360+ebp]                     ;
                                ; LOE esi
.B2.80:                         ; Preds .B2.152 .B2.132 .B2.129 .B2.79
        cmp       DWORD PTR [-268+ebp], 24                      ;52.3
        jle       .B2.114       ; Prob 0%                       ;52.3
                                ; LOE esi
.B2.81:                         ; Preds .B2.80
        mov       eax, DWORD PTR [-284+ebp]                     ;52.3
        neg       eax                                           ;52.3
        add       eax, esi                                      ;52.3
        imul      eax, DWORD PTR [-312+ebp]                     ;52.3
        push      DWORD PTR [-276+ebp]                          ;52.3
        push      0                                             ;52.3
        add       eax, DWORD PTR [-280+ebp]                     ;52.3
        push      eax                                           ;52.3
        call      __intel_fast_memset                           ;52.3
                                ; LOE esi
.B2.148:                        ; Preds .B2.81
        add       esp, 12                                       ;52.3
                                ; LOE esi
.B2.82:                         ; Preds .B2.148
        mov       eax, DWORD PTR [-272+ebp]                     ;52.3
        inc       esi                                           ;52.3
        inc       eax                                           ;52.3
        mov       DWORD PTR [-272+ebp], eax                     ;52.3
        cmp       eax, DWORD PTR [-288+ebp]                     ;52.3
        jae       .B2.130       ; Prob 18%                      ;52.3
                                ; LOE esi
.B2.152:                        ; Preds .B2.82
        mov       eax, DWORD PTR [-352+ebp]                     ;52.3
        mov       DWORD PTR [-280+ebp], eax                     ;52.3
        jmp       .B2.80        ; Prob 100%                     ;52.3
                                ; LOE esi
.B2.84:                         ; Preds .B2.13                  ; Infreq
        cmp       esi, 8                                        ;56.3
        jl        .B2.92        ; Prob 10%                      ;56.3
                                ; LOE esi
.B2.85:                         ; Preds .B2.84                  ; Infreq
        mov       eax, esi                                      ;56.3
        xor       edx, edx                                      ;56.3
        mov       ecx, DWORD PTR [-92+ebp]                      ;56.3
        and       eax, -8                                       ;56.3
        pxor      xmm0, xmm0                                    ;56.3
                                ; LOE eax edx ecx esi xmm0
.B2.86:                         ; Preds .B2.86 .B2.85           ; Infreq
        movups    XMMWORD PTR [ecx+edx*4], xmm0                 ;56.3
        movups    XMMWORD PTR [16+ecx+edx*4], xmm0              ;56.3
        add       edx, 8                                        ;56.3
        cmp       edx, eax                                      ;56.3
        jb        .B2.86        ; Prob 82%                      ;56.3
                                ; LOE eax edx ecx esi xmm0
.B2.88:                         ; Preds .B2.86 .B2.92           ; Infreq
        cmp       eax, esi                                      ;56.3
        jae       .B2.15        ; Prob 10%                      ;56.3
                                ; LOE eax esi
.B2.89:                         ; Preds .B2.88                  ; Infreq
        mov       edx, DWORD PTR [-92+ebp]                      ;
        xor       ecx, ecx                                      ;
                                ; LOE eax edx ecx esi
.B2.90:                         ; Preds .B2.90 .B2.89           ; Infreq
        mov       DWORD PTR [edx+eax*4], ecx                    ;56.3
        inc       eax                                           ;56.3
        cmp       eax, esi                                      ;56.3
        jb        .B2.90        ; Prob 82%                      ;56.3
        jmp       .B2.15        ; Prob 100%                     ;56.3
                                ; LOE eax edx ecx esi
.B2.92:                         ; Preds .B2.84                  ; Infreq
        xor       eax, eax                                      ;56.3
        jmp       .B2.88        ; Prob 100%                     ;56.3
                                ; LOE eax esi
.B2.95:                         ; Preds .B2.77                  ; Infreq
        mov       DWORD PTR [-168+ebp], eax                     ;
        mov       eax, DWORD PTR [-60+ebp]                      ;
        pxor      xmm0, xmm0                                    ;
        mov       DWORD PTR [-160+ebp], edx                     ;
        test      eax, eax                                      ;206.21
        mov       DWORD PTR [-172+ebp], ecx                     ;
        movss     xmm1, DWORD PTR [_2il0floatpacket.6]          ;
        mov       esi, DWORD PTR [-32+ebp]                      ;
        mov       edx, DWORD PTR [-20+ebp]                      ;
        jle       .B2.107       ; Prob 16%                      ;206.21
                                ; LOE eax edx esi al dl ah dh xmm0 xmm1 xmm2
.B2.96:                         ; Preds .B2.95                  ; Infreq
        cvtsi2ss  xmm3, eax                                     ;209.39
        divss     xmm2, xmm3                                    ;209.38
        mov       ecx, DWORD PTR [-160+ebp]                     ;209.9
        movss     DWORD PTR [-4+ecx], xmm2                      ;209.9
                                ; LOE eax edx esi xmm0 xmm1 xmm2
.B2.97:                         ; Preds .B2.107 .B2.96          ; Infreq
        movss     DWORD PTR [652+esi], xmm2                     ;211.3
        movss     xmm2, DWORD PTR [672+esi]                     ;212.9
        comiss    xmm1, xmm2                                    ;212.39
        jbe       .B2.99        ; Prob 50%                      ;212.39
                                ; LOE eax edx esi xmm0 xmm1 xmm2
.B2.98:                         ; Preds .B2.97                  ; Infreq
        mov       ecx, DWORD PTR [-120+ebp]                     ;213.36
        sub       ecx, DWORD PTR [-88+ebp]                      ;213.5
        shl       ecx, 5                                        ;213.5
        mov       eax, DWORD PTR [-168+ebp]                     ;213.5
        cvtsi2ss  xmm2, DWORD PTR [12+eax+ecx]                  ;213.5
        movss     DWORD PTR [648+esi], xmm2                     ;213.5
        jmp       .B2.100       ; Prob 100%                     ;213.5
                                ; LOE edx xmm0 xmm1
.B2.99:                         ; Preds .B2.97                  ; Infreq
        cvtsi2ss  xmm3, eax                                     ;215.36
        divss     xmm3, xmm2                                    ;215.5
        movss     DWORD PTR [648+esi], xmm3                     ;215.5
                                ; LOE edx xmm0 xmm1
.B2.100:                        ; Preds .B2.98 .B2.99           ; Infreq
        inc       edx                                           ;217.1
        mov       eax, edx                                      ;217.1
        cmp       edx, DWORD PTR [-164+ebp]                     ;217.1
        jle       .B2.17        ; Prob 82%                      ;217.1
                                ; LOE eax edx xmm0 xmm1
.B2.101:                        ; Preds .B2.100                 ; Infreq
        mov       DWORD PTR [-72+ebp], edx                      ;61.1
                                ; LOE
.B2.102:                        ; Preds .B2.101 .B2.113         ; Infreq
        mov       esi, DWORD PTR [-340+ebp]                     ;221.1
        mov       edx, esi                                      ;221.1
        shr       edx, 1                                        ;221.1
        mov       eax, esi                                      ;221.1
        and       edx, 1                                        ;221.1
        and       eax, 1                                        ;221.1
        shl       edx, 2                                        ;221.1
        add       eax, eax                                      ;221.1
        or        edx, eax                                      ;221.1
        or        edx, 262144                                   ;221.1
        push      edx                                           ;221.1
        push      DWORD PTR [-352+ebp]                          ;221.1
        call      _for_dealloc_allocatable                      ;221.1
                                ; LOE esi
.B2.149:                        ; Preds .B2.102                 ; Infreq
        add       esp, 8                                        ;221.1
                                ; LOE esi
.B2.103:                        ; Preds .B2.149                 ; Infreq
        and       esi, -2                                       ;221.1
        mov       DWORD PTR [-352+ebp], 0                       ;221.1
        mov       DWORD PTR [-340+ebp], esi                     ;221.1
                                ; LOE
.B2.104:                        ; Preds .B2.103                 ; Infreq
        mov       eax, DWORD PTR [-296+ebp]                     ;223.1
        mov       esp, eax                                      ;223.1
                                ; LOE
.B2.150:                        ; Preds .B2.104                 ; Infreq
        mov       esi, DWORD PTR [-304+ebp]                     ;223.1
        mov       edi, DWORD PTR [-300+ebp]                     ;223.1
        mov       esp, ebp                                      ;223.1
        pop       ebp                                           ;223.1
        mov       esp, ebx                                      ;223.1
        pop       ebx                                           ;223.1
        ret                                                     ;223.1
                                ; LOE
.B2.107:                        ; Preds .B2.112 .B2.95          ; Infreq
        movzx     edi, WORD PTR [682+esi]                       ;207.62
        movsx     ecx, BYTE PTR [681+esi]                       ;207.63
        add       edi, ecx                                      ;207.62
        movsx     ecx, di                                       ;207.25
        cvtsi2ss  xmm2, ecx                                     ;207.25
        divss     xmm2, DWORD PTR [_2il0floatpacket.7]          ;207.9
        mov       edi, DWORD PTR [-160+ebp]                     ;207.9
        movss     DWORD PTR [-4+edi], xmm2                      ;207.9
        jmp       .B2.97        ; Prob 100%                     ;207.9
                                ; LOE eax edx esi xmm0 xmm1 xmm2
.B2.108:                        ; Preds .B2.51                  ; Infreq
        movss     xmm0, DWORD PTR [240+edx+ecx]                 ;136.30
        imul      edx, DWORD PTR [-40+ebp], 152                 ;136.13
        mov       edi, DWORD PTR [-44+ebp]                      ;136.13
        mov       DWORD PTR [-284+ebp], edx                     ;136.13
        sub       edi, edx                                      ;136.13
        mov       edx, DWORD PTR [-28+ebp]                      ;136.13
        imul      ecx, edx, 152                                 ;136.13
        imul      edx, edx, 900                                 ;136.13
        divss     xmm0, DWORD PTR [20+ecx+edi]                  ;136.58
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;136.90
        imul      ecx, esi, -900                                ;136.13
        add       ecx, edx                                      ;136.13
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;136.58
        mov       DWORD PTR [-148+ebp], esi                     ;136.90
        mulss     xmm0, DWORD PTR [672+eax+ecx]                 ;136.13
        imul      ecx, DWORD PTR [-152+ebp], 900                ;132.12
        jmp       .B2.63        ; Prob 100%                     ;132.12
                                ; LOE eax edx ecx xmm0
.B2.109:                        ; Preds .B2.50                  ; Infreq
        mov       DWORD PTR [-360+ebp], esi                     ;129.15
        lea       eax, DWORD PTR [-288+ebp]                     ;130.20
        push      eax                                           ;130.20
        lea       edx, DWORD PTR [-292+ebp]                     ;130.20
        push      edx                                           ;130.20
        lea       ecx, DWORD PTR [-360+ebp]                     ;130.20
        push      ecx                                           ;130.20
        lea       esi, DWORD PTR [-72+ebp]                      ;130.20
        push      esi                                           ;130.20
        lea       edi, DWORD PTR [-68+ebp]                      ;130.20
        push      edi                                           ;130.20
        push      OFFSET FLAT: __NLITPACK_0.0.2                 ;130.20
        call      _RETRIEVE_NEXT_LINK                           ;130.20
                                ; LOE
.B2.151:                        ; Preds .B2.109                 ; Infreq
        add       esp, 24                                       ;130.20
                                ; LOE
.B2.110:                        ; Preds .B2.151                 ; Infreq
        mov       edx, DWORD PTR [-72+ebp]                      ;135.15
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;132.19
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;136.30
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;136.59
        mov       DWORD PTR [-28+ebp], edx                      ;135.15
        mov       edx, DWORD PTR [-68+ebp]                      ;132.19
        mov       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;132.12
        mov       DWORD PTR [-16+ebp], eax                      ;132.19
        mov       DWORD PTR [-44+ebp], esi                      ;136.30
        mov       DWORD PTR [-40+ebp], edi                      ;136.59
        jmp       .B2.51        ; Prob 100%                     ;136.59
                                ; LOE edx ecx
.B2.111:                        ; Preds .B2.24                  ; Infreq
        pxor      xmm0, xmm0                                    ;93.8
        movss     DWORD PTR [-124+ebp], xmm0                    ;93.8
        jmp       .B2.33        ; Prob 100%                     ;93.8
                                ; LOE
.B2.112:                        ; Preds .B2.17                  ; Infreq
        mov       ecx, DWORD PTR [-92+ebp]                      ;
        xor       eax, eax                                      ;
        lea       edi, DWORD PTR [ecx+edx*4]                    ;
        mov       DWORD PTR [-160+ebp], edi                     ;
        jmp       .B2.107       ; Prob 100%                     ;
                                ; LOE eax edx esi xmm0 xmm1
.B2.113:                        ; Preds .B2.15                  ; Infreq
        mov       DWORD PTR [-72+ebp], 1                        ;61.1
        jmp       .B2.102       ; Prob 100%                     ;61.1
                                ; LOE
.B2.114:                        ; Preds .B2.80                  ; Infreq
        cmp       DWORD PTR [-268+ebp], 4                       ;52.3
        jl        .B2.131       ; Prob 10%                      ;52.3
                                ; LOE esi
.B2.115:                        ; Preds .B2.114                 ; Infreq
        mov       eax, DWORD PTR [-312+ebp]                     ;52.3
        mov       edx, DWORD PTR [-284+ebp]                     ;52.3
        imul      edx, eax                                      ;52.3
        imul      eax, esi                                      ;52.3
        neg       edx                                           ;52.3
        add       edx, DWORD PTR [-280+ebp]                     ;52.3
        mov       DWORD PTR [-360+ebp], eax                     ;52.3
        add       eax, edx                                      ;52.3
        and       eax, 15                                       ;52.3
        je        .B2.118       ; Prob 50%                      ;52.3
                                ; LOE eax edx esi
.B2.116:                        ; Preds .B2.115                 ; Infreq
        test      al, 3                                         ;52.3
        jne       .B2.131       ; Prob 10%                      ;52.3
                                ; LOE eax edx esi
.B2.117:                        ; Preds .B2.116                 ; Infreq
        neg       eax                                           ;52.3
        add       eax, 16                                       ;52.3
        shr       eax, 2                                        ;52.3
                                ; LOE eax edx esi
.B2.118:                        ; Preds .B2.117 .B2.115         ; Infreq
        lea       ecx, DWORD PTR [4+eax]                        ;52.3
        cmp       ecx, DWORD PTR [-268+ebp]                     ;52.3
        jg        .B2.131       ; Prob 10%                      ;52.3
                                ; LOE eax edx esi
.B2.119:                        ; Preds .B2.118                 ; Infreq
        mov       ecx, DWORD PTR [-268+ebp]                     ;52.3
        mov       edi, ecx                                      ;52.3
        sub       edi, eax                                      ;52.3
        and       edi, 3                                        ;52.3
        neg       edi                                           ;52.3
        add       edx, DWORD PTR [-360+ebp]                     ;
        add       edi, ecx                                      ;52.3
        mov       DWORD PTR [-264+ebp], edi                     ;52.3
        test      eax, eax                                      ;52.3
        jbe       .B2.123       ; Prob 10%                      ;52.3
                                ; LOE eax edx esi
.B2.120:                        ; Preds .B2.119                 ; Infreq
        xor       ecx, ecx                                      ;
                                ; LOE eax edx ecx esi
.B2.121:                        ; Preds .B2.121 .B2.120         ; Infreq
        mov       DWORD PTR [edx+ecx*4], 0                      ;52.3
        inc       ecx                                           ;52.3
        cmp       ecx, eax                                      ;52.3
        jb        .B2.121       ; Prob 82%                      ;52.3
                                ; LOE eax edx ecx esi
.B2.123:                        ; Preds .B2.121 .B2.119         ; Infreq
        mov       ecx, DWORD PTR [-264+ebp]                     ;
        pxor      xmm0, xmm0                                    ;
                                ; LOE eax edx ecx esi xmm0
.B2.124:                        ; Preds .B2.124 .B2.123         ; Infreq
        movdqa    XMMWORD PTR [edx+eax*4], xmm0                 ;52.3
        add       eax, 4                                        ;52.3
        cmp       eax, ecx                                      ;52.3
        jb        .B2.124       ; Prob 82%                      ;52.3
                                ; LOE eax edx ecx esi xmm0
.B2.125:                        ; Preds .B2.124                 ; Infreq
        mov       DWORD PTR [-264+ebp], ecx                     ;
                                ; LOE esi
.B2.126:                        ; Preds .B2.125 .B2.131         ; Infreq
        mov       eax, DWORD PTR [-264+ebp]                     ;52.3
        cmp       eax, DWORD PTR [-268+ebp]                     ;52.3
        jae       .B2.132       ; Prob 10%                      ;52.3
                                ; LOE esi
.B2.127:                        ; Preds .B2.126                 ; Infreq
        mov       eax, DWORD PTR [-284+ebp]                     ;
        neg       eax                                           ;
        add       eax, esi                                      ;
        imul      eax, DWORD PTR [-312+ebp]                     ;
        add       eax, DWORD PTR [-280+ebp]                     ;
        mov       edx, DWORD PTR [-264+ebp]                     ;
        mov       ecx, DWORD PTR [-268+ebp]                     ;
                                ; LOE eax edx ecx esi
.B2.128:                        ; Preds .B2.128 .B2.127         ; Infreq
        mov       DWORD PTR [eax+edx*4], 0                      ;52.3
        inc       edx                                           ;52.3
        cmp       edx, ecx                                      ;52.3
        jb        .B2.128       ; Prob 82%                      ;52.3
                                ; LOE eax edx ecx esi
.B2.129:                        ; Preds .B2.128                 ; Infreq
        mov       eax, DWORD PTR [-272+ebp]                     ;52.3
        inc       esi                                           ;52.3
        inc       eax                                           ;52.3
        mov       DWORD PTR [-272+ebp], eax                     ;52.3
        cmp       eax, DWORD PTR [-288+ebp]                     ;52.3
        jb        .B2.80        ; Prob 82%                      ;52.3
                                ; LOE esi
.B2.130:                        ; Preds .B2.82 .B2.129          ; Infreq
        mov       esi, DWORD PTR [-292+ebp]                     ;
        jmp       .B2.5         ; Prob 100%                     ;
                                ; LOE esi
.B2.131:                        ; Preds .B2.114 .B2.118 .B2.116 ; Infreq
        xor       eax, eax                                      ;52.3
        mov       DWORD PTR [-264+ebp], eax                     ;52.3
        jmp       .B2.126       ; Prob 100%                     ;52.3
                                ; LOE esi
.B2.132:                        ; Preds .B2.126                 ; Infreq
        mov       eax, DWORD PTR [-272+ebp]                     ;52.3
        inc       esi                                           ;52.3
        inc       eax                                           ;52.3
        mov       DWORD PTR [-272+ebp], eax                     ;52.3
        cmp       eax, DWORD PTR [-288+ebp]                     ;52.3
        jb        .B2.80        ; Prob 82%                      ;52.3
                                ; LOE esi
.B2.133:                        ; Preds .B2.132                 ; Infreq
        mov       esi, DWORD PTR [-292+ebp]                     ;
        jmp       .B2.5         ; Prob 100%                     ;
                                ; LOE esi
.B2.136:                        ; Preds .B2.6                   ; Infreq
        mov       ecx, 1                                        ;
        jmp       .B2.10        ; Prob 100%                     ;
        ALIGN     16
                                ; LOE eax edx ecx esi
; mark_end;
_DYNUST_AMS_MODULE_mp_FLOW_MODEL_UPDATE_AMTS ENDP
_TEXT	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
__NLITPACK_0.0.2	DD	000000000H
__STRLITPACK_19.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_18.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_AMS_MODULE_mp_FLOW_MODEL_UPDATE_AMTS
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_AMS_MODULE_mp_VEHSPEED_UPDATE
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_AMS_MODULE_mp_VEHSPEED_UPDATE
_DYNUST_AMS_MODULE_mp_VEHSPEED_UPDATE	PROC NEAR 
; parameter 1: eax
; parameter 2: edx
; parameter 3: ecx
; parameter 4: 20 + ebp
; parameter 5: 24 + ebp
; parameter 6: 28 + ebp
.B3.1:                          ; Preds .B3.0
        mov       eax, DWORD PTR [4+esp]                        ;227.12
        mov       edx, DWORD PTR [8+esp]                        ;227.12
        mov       ecx, DWORD PTR [12+esp]                       ;227.12
	PUBLIC _DYNUST_AMS_MODULE_mp_VEHSPEED_UPDATE.
_DYNUST_AMS_MODULE_mp_VEHSPEED_UPDATE.::
        push      ebp                                           ;227.12
        mov       ebp, esp                                      ;227.12
        and       esp, -16                                      ;227.12
        push      esi                                           ;227.12
        push      edi                                           ;227.12
        push      ebx                                           ;227.12
        sub       esp, 68                                       ;227.12
        imul      edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32], -900 ;241.1
        imul      eax, DWORD PTR [eax], 900                     ;241.52
        mov       ebx, DWORD PTR [24+ebp]                       ;227.12
        add       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;241.1
        mov       esi, DWORD PTR [20+ebp]                       ;227.12
        movss     xmm3, DWORD PTR [ebx]                         ;240.1
        movzx     ebx, WORD PTR [682+eax+edi]                   ;241.51
        movsx     edi, BYTE PTR [681+eax+edi]                   ;241.52
        add       ebx, edi                                      ;241.51
        movsx     eax, bx                                       ;241.1
        cvtsi2ss  xmm4, eax                                     ;241.14
        mov       ebx, DWORD PTR [esi]                          ;242.5
        cmp       ebx, 1                                        ;242.5
        divss     xmm4, DWORD PTR [_2il0floatpacket.17]         ;241.1
        jne       .B3.8         ; Prob 67%                      ;242.5
                                ; LOE eax edx ecx ebx xmm3 xmm4
.B3.2:                          ; Preds .B3.1
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC+32] ;244.32
        shl       ebx, 5                                        ;
        neg       ebx                                           ;
        mov       esi, DWORD PTR [edx]                          ;244.32
        add       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC] ;
        shl       esi, 5                                        ;244.32
        cmp       DWORD PTR [ecx], 1                            ;243.16
        je        .B3.26        ; Prob 16%                      ;243.16
                                ; LOE eax ebx esi xmm3 xmm4
.B3.3:                          ; Preds .B3.2
        movss     xmm5, DWORD PTR [_2il0floatpacket.18]         ;258.208
        pxor      xmm0, xmm0                                    ;258.208
        cvtsi2ss  xmm2, DWORD PTR [12+esi+ebx]                  ;258.189
        movss     xmm1, DWORD PTR [16+esi+ebx]                  ;258.208
        minss     xmm3, xmm2                                    ;258.208
        divss     xmm3, xmm2                                    ;258.208
        mov       DWORD PTR [32+esp], eax                       ;258.208
        subss     xmm5, xmm3                                    ;258.208
        movss     DWORD PTR [28+esp], xmm4                      ;258.208
        maxss     xmm0, xmm5                                    ;258.208
        call      ___libm_sse2_powf                             ;258.208
                                ; LOE ebx esi xmm0
.B3.38:                         ; Preds .B3.3
        mov       eax, DWORD PTR [32+esp]                       ;
        mov       edx, DWORD PTR [8+esi+ebx]                    ;258.228
        sub       eax, edx                                      ;258.77
        cvtsi2ss  xmm2, eax                                     ;258.40
        cvtsi2ss  xmm3, edx                                     ;258.228
        movss     xmm1, DWORD PTR [_2il0floatpacket.17]         ;258.242
        pxor      xmm5, xmm5                                    ;259.25
        divss     xmm2, xmm1                                    ;258.129
        divss     xmm3, xmm1                                    ;258.242
        mulss     xmm2, xmm0                                    ;258.134
        movss     xmm4, DWORD PTR [28+esp]                      ;
        addss     xmm3, xmm2                                    ;258.227
        mov       eax, DWORD PTR [28+ebp]                       ;258.13
        minss     xmm4, xmm3                                    ;258.13
        movss     DWORD PTR [eax], xmm4                         ;258.13
        comiss    xmm5, xmm4                                    ;259.25
        jbe       .B3.25        ; Prob 50%                      ;259.25
                                ; LOE xmm4
.B3.4:                          ; Preds .B3.38
        mov       DWORD PTR [32+esp], 0                         ;260.15
        lea       ebx, DWORD PTR [32+esp]                       ;260.15
        mov       DWORD PTR [esp], 37                           ;260.15
        lea       eax, DWORD PTR [esp]                          ;260.15
        mov       DWORD PTR [4+esp], OFFSET FLAT: __STRLITPACK_10 ;260.15
        push      32                                            ;260.15
        push      eax                                           ;260.15
        push      OFFSET FLAT: __STRLITPACK_24.0.3              ;260.15
        push      -2088435968                                   ;260.15
        push      911                                           ;260.15
        push      ebx                                           ;260.15
        movss     DWORD PTR [52+esp], xmm4                      ;260.15
        call      _for_write_seq_lis                            ;260.15
                                ; LOE ebx
.B3.5:                          ; Preds .B3.4
        lea       eax, DWORD PTR [32+esp]                       ;261.15
        mov       DWORD PTR [56+esp], 0                         ;261.15
        mov       DWORD PTR [32+esp], 8                         ;261.15
        mov       DWORD PTR [36+esp], OFFSET FLAT: __STRLITPACK_8 ;261.15
        push      32                                            ;261.15
        push      eax                                           ;261.15
        push      OFFSET FLAT: __STRLITPACK_25.0.3              ;261.15
        push      -2088435968                                   ;261.15
        push      911                                           ;261.15
        push      ebx                                           ;261.15
        call      _for_write_seq_lis                            ;261.15
                                ; LOE ebx
.B3.6:                          ; Preds .B3.5
        movss     xmm4, DWORD PTR [76+esp]                      ;
        lea       eax, DWORD PTR [64+esp]                       ;261.15
        movss     DWORD PTR [64+esp], xmm4                      ;261.15
        push      eax                                           ;261.15
        push      OFFSET FLAT: __STRLITPACK_26.0.3              ;261.15
        push      ebx                                           ;261.15
        call      _for_write_seq_lis_xmit                       ;261.15
                                ; LOE
.B3.7:                          ; Preds .B3.6
        push      32                                            ;262.15
        xor       eax, eax                                      ;262.15
        push      eax                                           ;262.15
        push      eax                                           ;262.15
        push      -2088435968                                   ;262.15
        push      eax                                           ;262.15
        push      OFFSET FLAT: __STRLITPACK_27                  ;262.15
        call      _for_stop_core                                ;262.15
        jmp       .B3.47        ; Prob 100%                     ;262.15
                                ; LOE
.B3.8:                          ; Preds .B3.1
        cmp       ebx, 2                                        ;242.5
        jne       .B3.25        ; Prob 50%                      ;242.5
                                ; LOE eax edx ecx xmm3 xmm4
.B3.9:                          ; Preds .B3.8
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC+32] ;267.32
        shl       ebx, 5                                        ;
        neg       ebx                                           ;
        mov       esi, DWORD PTR [edx]                          ;267.32
        add       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC] ;
        shl       esi, 5                                        ;267.32
        cmp       DWORD PTR [ecx], 1                            ;266.16
        jne       .B3.20        ; Prob 60%                      ;266.16
                                ; LOE eax ebx esi xmm3 xmm4
.B3.10:                         ; Preds .B3.9
        cvtsi2ss  xmm0, DWORD PTR [ebx+esi]                     ;267.32
        comiss    xmm0, xmm3                                    ;267.29
        jb        .B3.12        ; Prob 50%                      ;267.29
                                ; LOE eax ebx esi xmm0 xmm3 xmm4
.B3.11:                         ; Preds .B3.10
        mov       eax, DWORD PTR [28+ebp]                       ;268.9
        movss     DWORD PTR [eax], xmm4                         ;268.9
        add       esp, 68                                       ;268.9
        pop       ebx                                           ;268.9
        pop       edi                                           ;268.9
        pop       esi                                           ;268.9
        mov       esp, ebp                                      ;268.9
        pop       ebp                                           ;268.9
        ret                                                     ;268.9
                                ; LOE
.B3.12:                         ; Preds .B3.10
        comiss    xmm3, xmm0                                    ;269.25
        jbe       .B3.19        ; Prob 50%                      ;269.25
                                ; LOE eax ebx esi xmm0 xmm3 xmm4
.B3.13:                         ; Preds .B3.12
        cvtsi2ss  xmm2, DWORD PTR [12+esi+ebx]                  ;269.65
        comiss    xmm2, xmm3                                    ;269.62
        jb        .B3.19        ; Prob 78%                      ;269.62
                                ; LOE eax ebx esi xmm0 xmm2 xmm3 xmm4
.B3.14:                         ; Preds .B3.13
        divss     xmm0, xmm2                                    ;271.71
        mov       edx, DWORD PTR [8+esi+ebx]                    ;272.127
        cvtsi2ss  xmm5, edx                                     ;272.127
        movss     xmm6, DWORD PTR [16+esi+ebx]                  ;271.92
        movss     xmm1, DWORD PTR [20+esi+ebx]                  ;271.73
        mov       DWORD PTR [16+esp], edx                       ;272.127
        movss     DWORD PTR [12+esp], xmm5                      ;272.127
        movss     DWORD PTR [4+esp], xmm6                       ;271.92
        movss     DWORD PTR [8+esp], xmm1                       ;271.73
        movss     DWORD PTR [24+esp], xmm2                      ;271.71
        movss     DWORD PTR [20+esp], xmm3                      ;271.71
        mov       DWORD PTR [32+esp], eax                       ;271.71
        movss     DWORD PTR [28+esp], xmm4                      ;271.71
        call      ___libm_sse2_powf                             ;271.71
                                ; LOE xmm0
.B3.43:                         ; Preds .B3.14
        movss     xmm5, DWORD PTR [_2il0floatpacket.18]         ;271.90
        movss     xmm1, DWORD PTR [4+esp]                       ;271.90
        subss     xmm5, xmm0                                    ;271.90
        movaps    xmm0, xmm5                                    ;271.90
        call      ___libm_sse2_powf                             ;271.90
                                ; LOE xmm0
.B3.42:                         ; Preds .B3.43
        movss     xmm3, DWORD PTR [20+esp]                      ;
        movss     xmm2, DWORD PTR [24+esp]                      ;
        movss     xmm1, DWORD PTR [8+esp]                       ;273.126
        minss     xmm3, xmm2                                    ;273.126
        divss     xmm3, xmm2                                    ;273.126
        movss     DWORD PTR [esp], xmm0                         ;271.90
        movaps    xmm0, xmm3                                    ;273.126
        call      ___libm_sse2_powf                             ;273.126
                                ; LOE xmm0
.B3.41:                         ; Preds .B3.42
        movss     xmm2, DWORD PTR [_2il0floatpacket.18]         ;273.145
        movss     xmm1, DWORD PTR [4+esp]                       ;273.145
        subss     xmm2, xmm0                                    ;273.145
        movaps    xmm0, xmm2                                    ;273.145
        call      ___libm_sse2_powf                             ;273.145
                                ; LOE xmm0
.B3.40:                         ; Preds .B3.41
        mov       eax, DWORD PTR [32+esp]                       ;
        sub       eax, DWORD PTR [16+esp]                       ;272.24
        pxor      xmm3, xmm3                                    ;274.27
        cvtsi2ss  xmm1, eax                                     ;272.23
        divss     xmm1, DWORD PTR [esp]                         ;272.114
        movss     xmm2, DWORD PTR [12+esp]                      ;272.15
        movss     xmm4, DWORD PTR [28+esp]                      ;
        addss     xmm1, xmm2                                    ;272.15
        mov       eax, DWORD PTR [28+ebp]                       ;273.15
        subss     xmm1, xmm2                                    ;273.45
        mulss     xmm1, xmm0                                    ;273.61
        addss     xmm2, xmm1                                    ;273.166
        divss     xmm2, DWORD PTR [_2il0floatpacket.17]         ;273.183
        minss     xmm4, xmm2                                    ;273.15
        movss     DWORD PTR [eax], xmm4                         ;273.15
        comiss    xmm3, xmm4                                    ;274.27
        jbe       .B3.25        ; Prob 50%                      ;274.27
                                ; LOE xmm4
.B3.15:                         ; Preds .B3.40
        mov       DWORD PTR [32+esp], 0                         ;275.17
        lea       ebx, DWORD PTR [32+esp]                       ;275.17
        mov       DWORD PTR [esp], 37                           ;275.17
        lea       eax, DWORD PTR [esp]                          ;275.17
        mov       DWORD PTR [4+esp], OFFSET FLAT: __STRLITPACK_6 ;275.17
        push      32                                            ;275.17
        push      eax                                           ;275.17
        push      OFFSET FLAT: __STRLITPACK_28.0.3              ;275.17
        push      -2088435968                                   ;275.17
        push      911                                           ;275.17
        push      ebx                                           ;275.17
        movss     DWORD PTR [52+esp], xmm4                      ;275.17
        call      _for_write_seq_lis                            ;275.17
                                ; LOE ebx
.B3.16:                         ; Preds .B3.15
        lea       eax, DWORD PTR [32+esp]                       ;276.17
        mov       DWORD PTR [56+esp], 0                         ;276.17
        mov       DWORD PTR [32+esp], 8                         ;276.17
        mov       DWORD PTR [36+esp], OFFSET FLAT: __STRLITPACK_4 ;276.17
        push      32                                            ;276.17
        push      eax                                           ;276.17
        push      OFFSET FLAT: __STRLITPACK_29.0.3              ;276.17
        push      -2088435968                                   ;276.17
        push      911                                           ;276.17
        push      ebx                                           ;276.17
        call      _for_write_seq_lis                            ;276.17
                                ; LOE ebx
.B3.17:                         ; Preds .B3.16
        movss     xmm4, DWORD PTR [76+esp]                      ;
        lea       eax, DWORD PTR [64+esp]                       ;276.17
        movss     DWORD PTR [64+esp], xmm4                      ;276.17
        push      eax                                           ;276.17
        push      OFFSET FLAT: __STRLITPACK_30.0.3              ;276.17
        push      ebx                                           ;276.17
        call      _for_write_seq_lis_xmit                       ;276.17
                                ; LOE
.B3.18:                         ; Preds .B3.17
        push      32                                            ;277.17
        xor       eax, eax                                      ;277.17
        push      eax                                           ;277.17
        push      eax                                           ;277.17
        push      -2088435968                                   ;277.17
        push      eax                                           ;277.17
        push      OFFSET FLAT: __STRLITPACK_31                  ;277.17
        call      _for_stop_core                                ;277.17
        jmp       .B3.47        ; Prob 100%                     ;277.17
                                ; LOE
.B3.19:                         ; Preds .B3.28 .B3.29 .B3.13 .B3.12
        cvtsi2ss  xmm0, DWORD PTR [8+esi+ebx]                   ;280.20
        divss     xmm0, DWORD PTR [_2il0floatpacket.17]         ;280.9
        mov       eax, DWORD PTR [28+ebp]                       ;280.9
        movss     DWORD PTR [eax], xmm0                         ;280.9
        add       esp, 68                                       ;280.9
        pop       ebx                                           ;280.9
        pop       edi                                           ;280.9
        pop       esi                                           ;280.9
        mov       esp, ebp                                      ;280.9
        pop       ebp                                           ;280.9
        ret                                                     ;280.9
                                ; LOE
.B3.20:                         ; Preds .B3.9
        movss     DWORD PTR [28+esp], xmm4                      ;283.130
        cvtsi2ss  xmm2, DWORD PTR [12+esi+ebx]                  ;283.113
        movss     xmm1, DWORD PTR [20+esi+ebx]                  ;283.130
        minss     xmm3, xmm2                                    ;283.130
        divss     xmm3, xmm2                                    ;283.130
        movaps    xmm0, xmm3                                    ;283.130
        call      ___libm_sse2_powf                             ;283.130
                                ; LOE ebx esi xmm0
.B3.46:                         ; Preds .B3.20
        movss     xmm2, DWORD PTR [_2il0floatpacket.18]         ;283.149
        movss     xmm1, DWORD PTR [16+esi+ebx]                  ;283.149
        subss     xmm2, xmm0                                    ;283.149
        movaps    xmm0, xmm2                                    ;283.149
        call      ___libm_sse2_powf                             ;283.149
                                ; LOE ebx esi xmm0
.B3.45:                         ; Preds .B3.46
        cvtsi2ss  xmm2, DWORD PTR [8+esi+ebx]                   ;283.50
        movss     xmm4, DWORD PTR [28+esp]                      ;
        pxor      xmm3, xmm3                                    ;284.25
        movaps    xmm1, xmm4                                    ;283.49
        mov       eax, DWORD PTR [28+ebp]                       ;283.13
        subss     xmm1, xmm2                                    ;283.49
        mulss     xmm1, xmm0                                    ;283.65
        addss     xmm2, xmm1                                    ;283.170
        divss     xmm2, DWORD PTR [_2il0floatpacket.17]         ;283.187
        minss     xmm4, xmm2                                    ;283.13
        movss     DWORD PTR [eax], xmm4                         ;283.13
        comiss    xmm3, xmm4                                    ;284.25
        jbe       .B3.25        ; Prob 50%                      ;284.25
                                ; LOE xmm4
.B3.21:                         ; Preds .B3.45
        mov       DWORD PTR [32+esp], 0                         ;285.15
        lea       ebx, DWORD PTR [32+esp]                       ;285.15
        mov       DWORD PTR [esp], 37                           ;285.15
        lea       eax, DWORD PTR [esp]                          ;285.15
        mov       DWORD PTR [4+esp], OFFSET FLAT: __STRLITPACK_2 ;285.15
        push      32                                            ;285.15
        push      eax                                           ;285.15
        push      OFFSET FLAT: __STRLITPACK_32.0.3              ;285.15
        push      -2088435968                                   ;285.15
        push      911                                           ;285.15
        push      ebx                                           ;285.15
        movss     DWORD PTR [52+esp], xmm4                      ;285.15
        call      _for_write_seq_lis                            ;285.15
                                ; LOE ebx
.B3.22:                         ; Preds .B3.21
        lea       eax, DWORD PTR [32+esp]                       ;286.15
        mov       DWORD PTR [56+esp], 0                         ;286.15
        mov       DWORD PTR [32+esp], 8                         ;286.15
        mov       DWORD PTR [36+esp], OFFSET FLAT: __STRLITPACK_0 ;286.15
        push      32                                            ;286.15
        push      eax                                           ;286.15
        push      OFFSET FLAT: __STRLITPACK_33.0.3              ;286.15
        push      -2088435968                                   ;286.15
        push      911                                           ;286.15
        push      ebx                                           ;286.15
        call      _for_write_seq_lis                            ;286.15
                                ; LOE ebx
.B3.23:                         ; Preds .B3.22
        movss     xmm4, DWORD PTR [76+esp]                      ;
        lea       eax, DWORD PTR [64+esp]                       ;286.15
        movss     DWORD PTR [64+esp], xmm4                      ;286.15
        push      eax                                           ;286.15
        push      OFFSET FLAT: __STRLITPACK_34.0.3              ;286.15
        push      ebx                                           ;286.15
        call      _for_write_seq_lis_xmit                       ;286.15
                                ; LOE
.B3.24:                         ; Preds .B3.23
        push      32                                            ;287.15
        xor       eax, eax                                      ;287.15
        push      eax                                           ;287.15
        push      eax                                           ;287.15
        push      -2088435968                                   ;287.15
        push      eax                                           ;287.15
        push      OFFSET FLAT: __STRLITPACK_35                  ;287.15
        call      _for_stop_core                                ;287.15
                                ; LOE
.B3.47:                         ; Preds .B3.34 .B3.18 .B3.7 .B3.24
        add       esp, 84                                       ;287.15
                                ; LOE
.B3.25:                         ; Preds .B3.47 .B3.48 .B3.38 .B3.40 .B3.8
                                ;       .B3.45
        add       esp, 68                                       ;291.1
        pop       ebx                                           ;291.1
        pop       edi                                           ;291.1
        pop       esi                                           ;291.1
        mov       esp, ebp                                      ;291.1
        pop       ebp                                           ;291.1
        ret                                                     ;291.1
                                ; LOE
.B3.26:                         ; Preds .B3.2                   ; Infreq
        cvtsi2ss  xmm6, DWORD PTR [ebx+esi]                     ;244.32
        comiss    xmm6, xmm3                                    ;244.29
        jb        .B3.28        ; Prob 50%                      ;244.29
                                ; LOE eax ebx esi xmm3 xmm4 xmm6
.B3.27:                         ; Preds .B3.26                  ; Infreq
        mov       eax, DWORD PTR [28+ebp]                       ;245.9
        movss     DWORD PTR [eax], xmm4                         ;245.9
        add       esp, 68                                       ;245.9
        pop       ebx                                           ;245.9
        pop       edi                                           ;245.9
        pop       esi                                           ;245.9
        mov       esp, ebp                                      ;245.9
        pop       ebp                                           ;245.9
        ret                                                     ;245.9
                                ; LOE
.B3.28:                         ; Preds .B3.26                  ; Infreq
        comiss    xmm3, xmm6                                    ;246.25
        jbe       .B3.19        ; Prob 50%                      ;246.25
                                ; LOE eax ebx esi xmm3 xmm4 xmm6
.B3.29:                         ; Preds .B3.28                  ; Infreq
        cvtsi2ss  xmm2, DWORD PTR [12+esi+ebx]                  ;246.64
        comiss    xmm2, xmm3                                    ;246.62
        jbe       .B3.19        ; Prob 50%                      ;246.62
                                ; LOE eax ebx esi xmm2 xmm3 xmm4 xmm6
.B3.30:                         ; Preds .B3.29                  ; Infreq
        divss     xmm6, xmm2                                    ;247.170
        movss     xmm7, DWORD PTR [_2il0floatpacket.18]         ;247.170
        pxor      xmm0, xmm0                                    ;247.170
        mov       edx, DWORD PTR [8+esi+ebx]                    ;247.192
        subss     xmm7, xmm6                                    ;247.170
        cvtsi2ss  xmm5, edx                                     ;247.192
        maxss     xmm0, xmm7                                    ;247.170
        movss     xmm1, DWORD PTR [16+esi+ebx]                  ;247.172
        mov       DWORD PTR [12+esp], edx                       ;247.192
        movss     DWORD PTR [8+esp], xmm5                       ;247.192
        movss     DWORD PTR [4+esp], xmm1                       ;247.172
        movss     DWORD PTR [16+esp], xmm2                      ;247.170
        movss     DWORD PTR [20+esp], xmm3                      ;247.170
        mov       DWORD PTR [32+esp], eax                       ;247.170
        movss     DWORD PTR [28+esp], xmm4                      ;247.170
        call      ___libm_sse2_powf                             ;247.170
                                ; LOE xmm0
.B3.49:                         ; Preds .B3.30                  ; Infreq
        movss     xmm3, DWORD PTR [20+esp]                      ;
        movss     xmm2, DWORD PTR [16+esp]                      ;
        movss     DWORD PTR [esp], xmm0                         ;247.170
        minss     xmm3, xmm2                                    ;248.140
        divss     xmm3, xmm2                                    ;248.140
        movss     xmm2, DWORD PTR [_2il0floatpacket.18]         ;248.140
        pxor      xmm0, xmm0                                    ;248.140
        movss     xmm1, DWORD PTR [4+esp]                       ;248.140
        subss     xmm2, xmm3                                    ;248.140
        maxss     xmm0, xmm2                                    ;248.140
        call      ___libm_sse2_powf                             ;248.140
                                ; LOE xmm0
.B3.48:                         ; Preds .B3.49                  ; Infreq
        mov       eax, DWORD PTR [32+esp]                       ;
        sub       eax, DWORD PTR [12+esp]                       ;247.22
        pxor      xmm6, xmm6                                    ;249.27
        cvtsi2ss  xmm3, eax                                     ;247.21
        divss     xmm3, DWORD PTR [esp]                         ;247.112
        movss     xmm1, DWORD PTR [8+esp]                       ;248.176
        movss     xmm2, DWORD PTR [_2il0floatpacket.17]         ;248.176
        movaps    xmm5, xmm1                                    ;248.176
        divss     xmm5, xmm2                                    ;248.176
        movss     xmm4, DWORD PTR [28+esp]                      ;
        addss     xmm3, xmm1                                    ;247.15
        mov       eax, DWORD PTR [28+ebp]                       ;248.15
        subss     xmm3, xmm1                                    ;248.45
        divss     xmm3, xmm2                                    ;248.61
        mulss     xmm3, xmm0                                    ;248.66
        addss     xmm5, xmm3                                    ;248.160
        minss     xmm4, xmm5                                    ;248.15
        movss     DWORD PTR [eax], xmm4                         ;248.15
        comiss    xmm6, xmm4                                    ;249.27
        jbe       .B3.25        ; Prob 50%                      ;249.27
                                ; LOE xmm4
.B3.31:                         ; Preds .B3.48                  ; Infreq
        mov       DWORD PTR [32+esp], 0                         ;250.17
        lea       ebx, DWORD PTR [32+esp]                       ;250.17
        mov       DWORD PTR [esp], 37                           ;250.17
        lea       eax, DWORD PTR [esp]                          ;250.17
        mov       DWORD PTR [4+esp], OFFSET FLAT: __STRLITPACK_14 ;250.17
        push      32                                            ;250.17
        push      eax                                           ;250.17
        push      OFFSET FLAT: __STRLITPACK_20.0.3              ;250.17
        push      -2088435968                                   ;250.17
        push      911                                           ;250.17
        push      ebx                                           ;250.17
        movss     DWORD PTR [52+esp], xmm4                      ;250.17
        call      _for_write_seq_lis                            ;250.17
                                ; LOE ebx
.B3.32:                         ; Preds .B3.31                  ; Infreq
        lea       eax, DWORD PTR [32+esp]                       ;251.17
        mov       DWORD PTR [56+esp], 0                         ;251.17
        mov       DWORD PTR [32+esp], 8                         ;251.17
        mov       DWORD PTR [36+esp], OFFSET FLAT: __STRLITPACK_12 ;251.17
        push      32                                            ;251.17
        push      eax                                           ;251.17
        push      OFFSET FLAT: __STRLITPACK_21.0.3              ;251.17
        push      -2088435968                                   ;251.17
        push      911                                           ;251.17
        push      ebx                                           ;251.17
        call      _for_write_seq_lis                            ;251.17
                                ; LOE ebx
.B3.33:                         ; Preds .B3.32                  ; Infreq
        movss     xmm4, DWORD PTR [76+esp]                      ;
        lea       eax, DWORD PTR [64+esp]                       ;251.17
        movss     DWORD PTR [64+esp], xmm4                      ;251.17
        push      eax                                           ;251.17
        push      OFFSET FLAT: __STRLITPACK_22.0.3              ;251.17
        push      ebx                                           ;251.17
        call      _for_write_seq_lis_xmit                       ;251.17
                                ; LOE
.B3.34:                         ; Preds .B3.33                  ; Infreq
        push      32                                            ;252.17
        xor       eax, eax                                      ;252.17
        push      eax                                           ;252.17
        push      eax                                           ;252.17
        push      -2088435968                                   ;252.17
        push      eax                                           ;252.17
        push      OFFSET FLAT: __STRLITPACK_23                  ;252.17
        call      _for_stop_core                                ;252.17
        jmp       .B3.47        ; Prob 100%                     ;252.17
        ALIGN     16
                                ; LOE
; mark_end;
_DYNUST_AMS_MODULE_mp_VEHSPEED_UPDATE ENDP
_TEXT	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_32.0.3	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_33.0.3	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_34.0.3	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_28.0.3	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_29.0.3	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_30.0.3	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_24.0.3	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_25.0.3	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_26.0.3	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_20.0.3	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_21.0.3	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_22.0.3	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_AMS_MODULE_mp_VEHSPEED_UPDATE
_BSS	SEGMENT  DWORD PUBLIC FLAT  'BSS'
	ALIGN 004H
	PUBLIC _DYNUST_AMS_MODULE_mp_SIREXIST
_DYNUST_AMS_MODULE_mp_SIREXIST	DD 1 DUP (0H)	; pad
_BSS	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
	PUBLIC _DYNUST_AMS_MODULE_mp_SIR
_DYNUST_AMS_MODULE_mp_SIR	DD	0
	DD	0
	DD	0
	DD	128
	DD	1
	DD	0
	DD	0
	DD	0
	DD	0
_DATA	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_16	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	100
	DB	101
	DB	110
	DB	115
	DB	105
	DB	116
	DB	121
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_17	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	100
	DB	101
	DB	110
	DB	115
	DB	105
	DB	116
	DB	121
	DB	0
	DB 3 DUP ( 0H)	; pad
_2il0floatpacket.6	DD	038d1b717H
_2il0floatpacket.7	DD	042700000H
_2il0floatpacket.8	DD	045a50000H
_2il0floatpacket.9	DD	042c80000H
_2il0floatpacket.10	DD	03f000000H
__STRLITPACK_2	DB	69
	DB	82
	DB	82
	DB	79
	DB	82
	DB	32
	DB	73
	DB	78
	DB	32
	DB	71
	DB	69
	DB	84
	DB	84
	DB	73
	DB	78
	DB	71
	DB	32
	DB	86
	DB	69
	DB	72
	DB	73
	DB	67
	DB	76
	DB	69
	DB	32
	DB	83
	DB	80
	DB	69
	DB	69
	DB	68
	DB	32
	DB	73
	DB	78
	DB	32
	DB	65
	DB	77
	DB	83
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_0	DB	65
	DB	100
	DB	106
	DB	83
	DB	112
	DB	101
	DB	101
	DB	100
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_35	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_6	DB	69
	DB	82
	DB	82
	DB	79
	DB	82
	DB	32
	DB	73
	DB	78
	DB	32
	DB	71
	DB	69
	DB	84
	DB	84
	DB	73
	DB	78
	DB	71
	DB	32
	DB	86
	DB	69
	DB	72
	DB	73
	DB	67
	DB	76
	DB	69
	DB	32
	DB	83
	DB	80
	DB	69
	DB	69
	DB	68
	DB	32
	DB	73
	DB	78
	DB	32
	DB	65
	DB	77
	DB	83
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_4	DB	65
	DB	100
	DB	106
	DB	83
	DB	112
	DB	101
	DB	101
	DB	100
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_31	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_10	DB	69
	DB	82
	DB	82
	DB	79
	DB	82
	DB	32
	DB	73
	DB	78
	DB	32
	DB	71
	DB	69
	DB	84
	DB	84
	DB	73
	DB	78
	DB	71
	DB	32
	DB	86
	DB	69
	DB	72
	DB	73
	DB	67
	DB	76
	DB	69
	DB	32
	DB	83
	DB	80
	DB	69
	DB	69
	DB	68
	DB	32
	DB	73
	DB	78
	DB	32
	DB	65
	DB	77
	DB	83
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_8	DB	65
	DB	100
	DB	106
	DB	83
	DB	112
	DB	101
	DB	101
	DB	100
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_27	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_14	DB	69
	DB	82
	DB	82
	DB	79
	DB	82
	DB	32
	DB	73
	DB	78
	DB	32
	DB	71
	DB	69
	DB	84
	DB	84
	DB	73
	DB	78
	DB	71
	DB	32
	DB	86
	DB	69
	DB	72
	DB	73
	DB	67
	DB	76
	DB	69
	DB	32
	DB	83
	DB	80
	DB	69
	DB	69
	DB	68
	DB	32
	DB	73
	DB	78
	DB	32
	DB	65
	DB	77
	DB	83
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_12	DB	65
	DB	100
	DB	106
	DB	83
	DB	112
	DB	101
	DB	101
	DB	100
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_23	DB	0
	DB 3 DUP ( 0H)	; pad
_2il0floatpacket.17	DD	042700000H
_2il0floatpacket.18	DD	03f800000H
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ITEMAX:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ITERATION:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_REACH_CONVERG:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_MOVESFLAG:BYTE
EXTRN	_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_LINKIDMAP:BYTE
EXTRN	_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND:BYTE
EXTRN	_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH:BYTE
EXTRN	_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_VKFUNC:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NOOFARCS:BYTE
_DATA	ENDS
EXTRN	_for_stop_core:PROC
EXTRN	_for_write_seq_lis_xmit:PROC
EXTRN	_for_dealloc_allocatable:PROC
EXTRN	_for_write_seq_lis:PROC
EXTRN	_for_alloc_allocatable:PROC
EXTRN	_for_check_mult_overflow:PROC
EXTRN	_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_P_SIZE:PROC
EXTRN	_RETRIEVE_NEXT_LINK:PROC
EXTRN	___libm_sse2_powf:PROC
EXTRN	__intel_fast_memset:PROC
EXTRN	__alloca_probe:PROC
EXTRN	__fltused:BYTE
	INCLUDELIB <ifconsol>
	INCLUDELIB <libifcoremt>
	INCLUDELIB <libifport>
	INCLUDELIB <libmmt>
	INCLUDELIB <LIBCMT>
	INCLUDELIB <libirc>
	INCLUDELIB <svml_dispmt>
	INCLUDELIB <OLDNAMES>
	END
