SUBROUTINE RETRIEVE_VEH_PATH_AStar_MIVA(j,i,iselect,jdst,generalized_cost,pathttmp,Counter)
!SUBROUTINE RETRIEVE_VEH_PATH_AStar_MIVA(AIN(mos,4),m_dynust_last_stand((AIN(mos,4)))%isec,ipinit,j,generalized_cost,tmpuepath) ! pass 1 to select the best path
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~ Copyright (C) 2002-2011 The Arizona Board of Regents on Behalf of The University of Arizona.  ~~!
!~~ Developed by Yi-Chang Chiu, Ph.D. as par to the DynusT project.                               ~~!
!~~ This program is free software: you can redistribute it and/or modify it                       ~~!
!~~ under the terms of the GNU General Public License version 2 as published by                   ~~!
!~~ the Free Software Foundation.                                                                 ~~!
!~~ This program is distributed in the hope that it will be useful,                               ~~!
!~~ but WITHOUT ANY WARRANTY; without even the implied warranty of                                ~~!
!~~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                                          ~~!
!~~ See the GNU General Public License for more details.                                          ~~!
!~~ You should have received a copy of the GNU General Public License                             ~~!
!~~ along with this program (in the file COPYING).                                                ~~!
!~~ IF not, see <http://www.gnu.org/licenses/>.                                                   ~~!
!~~ Please see http://www.dynust.net for contact information and names                            ~~!
!~~ of individual contributors.                                                                   ~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
      USE DYNUST_MAIN_MODULE
      USE DYNUST_VEH_MODULE
      USE DYNUST_NETWORK_MODULE
      USE DYNUST_VEH_PATH_ATT_MODULE
      USE DFPORT
      USE RAND_GEN_INT
      USE DYNUST_TDSP_ASTAR_MODULE
            
      INTEGER pathttmp(maxnu_pa)
      INTEGER Index1D, ifg2,itrace
      INTEGER, INTENT(IN) ::J,I,ISELECT, jdst
      INTEGER(2) CurN
      REAL generalized_cost
      REAL evalue, TimeIndex 
      INTEGER SeeNodeSum, Size, In_Dest
      INTEGER :: LinkID, Counter, Arg_Origin  

!> > > > > > > > > >                < < < < < < < < < < <
!>>>>>>>>>>>>>>>>>>>CODES START HERE<<<<<<<<<<<<<<<<<<<<<
!> > > > > > > > > >                < < < < < < < < < < <

      PathTTmp = 0
      generalized_cost = 0.0
      
      TimIdex = FLOOR((time_now-0.05)/60.0) + 1 ! convert to min
      !In_Dest  = m_dynust_last_stand(j)%jdestNode
      In_Dest = destination(jdst)
      Size = nint(SimPeriod)
      
      CALL Create_AStar_Scan_Eligible_Set( Size )
      CALL Create_AStar_Fixed_Label_Set
      CALL Create_Labels (Noofarcs)

      CALL AStar_Initialization( i, In_Dest, TimIdex, 0, noofnodes_org ) 
      CALL AStar_Get_Path( i, In_Dest, LinkID, TimIdex, 0, 1 )
      CALL RetrieveTDSP( i, LinkID, pathttmp, Counter )

      CALL Destroy_AStar_Scan_Eligible_Set( Size )
      CALL Destroy_AStar_Fixed_Label_Set
      CALL Destroy_Labels ()

END SUBROUTINE RETRIEVE_VEH_PATH_AStar_MIVA