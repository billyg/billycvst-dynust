SUBROUTINE READ_VEHICLES(t_start,l)
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~ Copyright (C) 2002-2011 The Arizona Board of Regents on Behalf of The University of Arizona.  ~~!
!~~ Developed by Yi-Chang Chiu, Ph.D. as par to the DynusT project.                               ~~!
!~~ This program is free software: you can redistribute it and/or modify it                       ~~!
!~~ under the terms of the GNU General Public License version 2 as published by                   ~~!
!~~ the Free Software Foundation.                                                                 ~~!
!~~ This program is distributed in the hope that it will be useful,                               ~~!
!~~ but WITHOUT ANY WARRANTY; without even the implied warranty of                                ~~!
!~~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                                          ~~!
!~~ See the GNU General Public License for more details.                                          ~~!
!~~ You should have received a copy of the GNU General Public License                             ~~!
!~~ along with this program (in the file COPYING).                                                ~~!
!~~ IF not, see <http://www.gnu.org/licenses/>.                                                   ~~!
!~~ Please see http://www.dynust.net for contact information and names                            ~~!
!~~ of individual contributors.                                                                   ~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
! --
    USE DYNUST_MAIN_MODULE
	USE DYNUST_VEH_MODULE
	USE DYNUST_NETWORK_MODULE
	USE DYNUST_VEH_PATH_ATT_MODULE
    USE DYNUST_LINK_VEH_LIST_MODULE
    USE DYNUST_FUEL_MODULE
    USE RAND_GEN_INT
    USE DYNUST_ROUTE_SWITCH_MODULE    
    !USE DYNUST_MOVES_MODULE
    
    LOGICAL::BottleneckAhead
	REAL,save:: vehtmp(3)
	INTEGER(2) Index1D
	REAL value
	REAL t_start
	INTEGER,save:: jtmp,iutmp,idtmp,ndestmp,ntmp,ihovtmp,ivcltmp,ivcl2tmp,izonetmp,j_ah,jm,nlnk,mtmp,km,ievac
	INTEGER,save:: infotmp,ms,iflag1,itmp,mst
	REAL,save:: stimetp,comptmp, ribftmp,xpartmp,gastmp
	INTEGER,save:: jpath_tmp(10000)
	INTEGER,save:: jpath_tmpInt(10000)
    INTEGER,save:: jdest_tmp(1000)
    REAL,SAVE::    jtime_tmp(1000)
    INTEGER,save:: idarray(1000000,2),idarrayct
    INTEGER icn,aggtime,NodeSum
    REAL,save:: wait_tmp(10)
    LOGICAL,save:: eofid = .false.
    LOGICAL,save:: fuELSEtup = .false.

!> > > > > > > > > >                < < < < < < < < < < <
!>>>>>>>>>>>>>>>>>>>CODES START HERE<<<<<<<<<<<<<<<<<<<<<
!> > > > > > > > > >                < < < < < < < < < < <
    
    IF(l == 1) THEN
     vehtmp(1) = vehprofile(1)%vehfrac
     vehtmp(2) = vehtmp(1)+vehprofile(2)%vehfrac
     vehtmp(3) = 1.0
    ENDIF
    IF(.not.eofid) THEN ! run through this SUBROUTINE only IF not eof    
    idarray(:,:) = 0
    idarrayct = 0
    
    m_dynust_network_arc_de(:)%vlg = 0
    IF(t_start < 0.1) THEN
        ALLOCATE(VehLodtmp(tmparysize,2))
        vehlodtmp(:,:) = 0
        ALLOCATE(vehlodzone(nzones))
        vehlodzone(:) = .false.
      ELSE
        vehlodtmp(:,:) = 0
        vehlodzone(:) = .false.        
    ENDIF
    ivct = 0
    
    IF(t_start == 0.0) THEN
	  jrestore=0
	  iread_veh_count=0
      iread_veh_flag=1
	ENDIF

100 IF(iread_veh_count == MaxVehiclesFile.and..not.eofid) THEN
      eofid = .true.
      go to 777
    ENDIF

! READING VEHICLE.DAT HEREAFTER
  IF(iread_veh_flag == 1) THEN
    READ(500,301)jtmp,iutmp,idtmp,stimetp,ivcltmp,ivcl2tmp,ihovtmp,ntmp,ndestmp,infotmp,ribftmp,comptmp,izonetmp,ievac,xpartmp,gastmp
    IF(keepcls == 1.and.ivcltmp == 4) ier_ok = 1 ! recognize that class 4 exist
    IF(keepcls == 1.and.ivcltmp == 3) iue_ok = 1
  ENDIF
 
 777 IF(iread_veh_flag == 0) iread_veh_flag = 1 
   
   IF(nint(stimetp/xminPerSimInt)-nint((t_start/xminPerSimInt+tdspstep)) < 0.and..not.eofid) THEN
      justveh = justveh + 1		     
!      WRITE(9090,*) "justveh", justveh
      idarrayct = idarrayct + 1
      idarray(idarrayct,1) = jtmp
      idarray(idarrayct,2) = justveh
     
      IF(RunMode == 0.OR.RunMode == 2) THEN 
        m_dynust_veh_nde(justveh)%ID = jtmp ! vehicle ID is the first column of vehicle.dat
      ELSE
        m_dynust_veh_nde(justveh)%ID = justveh ! assigned new ID as all other RunModes involve vehicle generated from demand matrcies
        IF((RunMode == 3.OR.RunMode == 4).AND.MixedVehGenMap > 0) THEN
           WRITE(9987,'("Original ID --> New ID",2i7)') jtmp, justveh
        ENDIF
      ENDIF 
      IF(jtmp > maxid) maxid = jtmp
     

      IF(keeptype == 0) THEN ! re-distribute the TYPE only IF keeptype = 0, otherwise keep the original type setting
       rd = ranxy(15) 
       DO igo = 1, 3
         IF(rd < vehtmp(igo)) THEN
           ivcl2tmp = igo
           exit
         ENDIF
       ENDDO
      ENDIF
      IcV = ivcl2tmp
      if(ivcl2tmp == 6) icv = 3 ! transit are considered HOV
      IF(keepcls == 0) THEN ! re-distribute the MTC CLASS only IF keepcls = 0 AND NOT BUS, otherwise keep the original cls setting
800    rd = ranxy(15)
       DO igo = 1, 5 ! re-distribute the mtc class according to the scenario.dat
         IF(rd < vehprofile(icv)%mtcfrac(igo)) THEN
           ivcltmp = igo
           exit
         ENDIF
       ENDDO
       IF(ivcltmp == 4) ier_ok = 1
      ENDIF

      readveh(ivcl2tmp,ivcltmp)=readveh(ivcl2tmp,ivcltmp)+1

      IF(ivcl2tmp == 2) THEN
        truckok = .true.
      ELSEIF(ivcl2tmp == 3) THEN
        hovok = .true.
      ENDIF

      IF(jtmp == 0) THEN
	    WRITE(911,*) "Error in vehicle.dat"
	    WRITE(911,*) "The number of vehicles listed in this file is less than" 
        WRITE(911,*) "specified at the first line"
		STOP
      ENDIF	
	  IF(ndestmp > noofSTOPs) THEN
	    WRITE(911,*) 'error in tripchain.dat'
	    WRITE(911,*) 'the maximum number of STOPs is exceeded'
	    WRITE(911,*) 'check the vehicle number ', jtmp 
	    STOP
      ENDIF
	  DO j_ah=1,ndestmp
          READ(500,400) jdest_tmp(j_ah),wait_tmp(j_ah)

		  IF(j_ah >= 2) THEN
            IF(jdest_tmp(j_ah) == jdest_tmp(j_ah-1)) THEN
                 WRITE(911,*)'error in vehicle.dat'
				 WRITE(911,*)' found the same dest in consecutive order'
				 STOP
			ENDIF
		  ENDIF
      ENDDO

	  jrestore = jrestore + 1
      iread_veh_count=iread_veh_count+1
      
       
!     IF((RunMode == 2.AND.(ivcltmp == 4.or.ivcltmp == 5).and.stimetp >= InfoStartTime).or.RunMode == 0) THEN ! determine IF there is any vehicle with class 4 or 5, ivct is the counter for it
      IF(RunMode == 0.OR.RunMode == 3) THEN !INCLUDE VEHICLE HERE. IN CASE OF RUNMODE == 2 NEED TO CHECK IF THE PATH IS PASSING THROUGH INCIDENT OR NOT
        ivct = ivct + 1
        IF(ivct > tmparysize) THEN ! need to increase the buffer size
	       WRITE(911,*) 'Need to increase tmparysize in PARAMETER.dat'
           WRITE(911,*) "In GUI, please go to project->scenario data-> scenario-> advanced PARAMETER setting"
	       WRITE(911,*) 'Or one can directly modify PARAMETER.dat'
	       STOP
        ENDIF
!       VehLodtmp(ivct,2) = jtmp
        VehLodtmp(ivct,2) = justveh
        vehLodtmp(ivct,1) = jdest_tmp(1)
        vehlodzone(jdest_tmp(1))=.true.
      ENDIF

!     jm = jtmp
      jm = justveh

      m_dynust_veh(jm)%NoOfIntDst = ndestmp
      DO j_ah=1,ndestmp
            m_dynust_veh_nde(jm)%IntDestZone(j_ah)=jdest_tmp(j_ah)
            m_dynust_veh(jm)%IntDestDwell(j_ah) = wait_tmp(j_ah)*60
          ENDDO
          IF(OutToInNodeNum(iutmp) < 1.OR.OutToInNodeNum(idtmp) < 1) THEN
            WRITE(911,*) "Error in reading vehicle file"
            WRITE(911,*) "ID = ", jtmp
            WRITE(911,*) "UP AND DOWNSTREAM NODES", iutmp, idtmp
            STOP
          ENDIF
          Nlnk=GetFLinkFromNode(OutToInNodeNum(iutmp),OutToInNodeNum(idtmp),15)
          IF(nlnk < 1) THEN
             WRITE(911,*) "error in read vehicle"
             WRITE(911,*) "check vehicle #", jtmp, "upstream and downstream nodes"
             STOP
          ENDIF
          m_dynust_last_stand(jm)%isec= Nlnk

          IF(izonetmp == 0) THEN
            m_dynust_last_stand(jm)%jorig = m_dynust_network_arc_de(Nlnk)%NGenZ2I(2)
            IF(m_dynust_last_stand(jm)%jorig < 1) THEN
              WRITE(911,*) "error in reading origin zone for vehicles"
              WRITE(911,*) "Vehicle ID", jm
              WRITE(911,*) "Veh Generation link", m_dynust_network_node_nde(m_dynust_network_arc_nde(Nlnk)%iunod)%IntoOutNodeNum, m_dynust_network_node_nde(m_dynust_network_arc_nde(Nlnk)%idnod)%IntoOutNodeNum
              WRITE(911,*) "Gen Link in the Zone",(m_dynust_network_arc_de(Nlnk)%NGenZ2I(k), k = 1, m_dynust_network_arc_de(Nlnk)%NGenZ2I(1))
              STOP
            ENDIF
          ELSE
            m_dynust_last_stand(jm)%jorig = izonetmp
          ENDIF
          
          m_dynust_veh(jm)%evacsta = ievac
                   
          m_dynust_last_stand(jm)%vehclass=ivcltmp  !only solve for UE for HOV/HOT vehicle in this version

          IF(m_dynust_last_stand(jm)%vehclass == 4) THEN
             m_dynust_veh_nde(jm)%info = 1
             m_dynust_veh_nde(jm)%ribf = ribfa
             m_dynust_veh_nde(jm)%compliance = com_frac
          ELSE
             m_dynust_veh_nde(jm)%info = infotmp
		     m_dynust_veh_nde(jm)%ribf = ribftmp
		     m_dynust_veh_nde(jm)%compliance=comptmp		     
          ENDIF

		  m_dynust_last_stand(jm)%vehtype=ivcl2tmp
		  
		  IF(ivcl2tmp == 6) THEN 
		    NoofBuses = NoofBuses + 1 ! FOUND BUSES
!		    TransitRunTime(NoofBuses)%ID = jm
!		    TransitRunTime(NoofBuses)%CurrentLink = 1
		    IF(logout > 0) write(711,*) "Bus # ", jm
		  ENDIF
		  
          IF(FuelOut > 0) THEN
            IF(gastmp < 0.1) THEN
              CALL SetFuelLevel(jm) ! set fuel level
            ELSE
              VehFuel(jm)%IniGas = gastmp 
              VehFuel(jm)%CurGas = gastmp               
            ENDIF
          ENDIF
          		  
		  IF(ivcl2tmp == 2.or.ivcl2tmp == 5) THEN
		    m_dynust_veh(jm)%vhpce = nint(1.5*AttScale)
          ELSE
			m_dynust_veh(jm)%vhpce = nint(1*AttScale)
          ENDIF
          m_dynust_last_stand(jm)%stime=stimetp
          m_dynust_veh_nde(jm)%icurrnt=1
          IF(xpartmp < 0.00000001) THEN !IF no init info contained in vehicle.dat 
            IF(m_dynust_network_arc_nde(m_dynust_last_stand(jm)%isec)%link_iden == 5) THEN ! arterial  
              r0 = ranxy(155) 
              m_dynust_veh(jm)%position =     m_dynust_network_arc_nde(m_dynust_last_stand(jm)%isec)%s*r0
              m_dynust_veh_nde(jm)%xparinit = m_dynust_network_arc_nde(m_dynust_last_stand(jm)%isec)%s*r0
            ELSE 
              m_dynust_veh_nde(jm)%xparinit = m_dynust_network_arc_nde(m_dynust_last_stand(jm)%isec)%s
              m_dynust_veh(jm)%position = m_dynust_network_arc_nde(m_dynust_last_stand(jm)%isec)%s
            ENDIF       
          ELSE 
           m_dynust_veh_nde(jm)%xparinit = m_dynust_network_arc_nde(m_dynust_last_stand(jm)%isec)%s*xpartmp ! xpartmp is the decimal
           m_dynust_veh(jm)%position = m_dynust_network_arc_nde(m_dynust_last_stand(jm)%isec)%s*xpartmp ! xpartmp is the decimal
          ENDIF
                  
	      m_dynust_veh(jm)%DestVisit = 1
          m_dynust_last_stand(jm)%jdest=m_dynust_veh_nde(jm)%IntDestZone(m_dynust_veh(jm)%DestVisit)
	      mtc_veh(m_dynust_last_stand(jm)%vehclass)=mtc_veh(m_dynust_last_stand(jm)%vehclass)+1

		  m_dynust_network_arc_de(m_dynust_last_stand(jm)%isec)%vlg=m_dynust_network_arc_de(m_dynust_last_stand(jm)%isec)%vlg+1
          IF(m_dynust_last_stand(jm)%stime >= starttm.and.m_dynust_last_stand(jm)%stime < endtm) THEN
		    m_dynust_veh(jm)%itag=1
          ELSE
            m_dynust_veh(jm)%itag=0
          ENDIF
          m_dynust_veh(jm)%ioc = ihovtmp

!         READING PATH.DAT HEREAFTER ALWAYS READ HISTORICAL PATH, THEN SOME OF THEM MAY BE REPLACED LATER ON
369       IF(RunMode /= 1) THEN 
             jpath_tmp(:) = 0
!            READ PATH
1	         READ(550,700) mtmp,(jpath_tmp(km),km=1,ntmp-1)
             IF(LOGOUT > 0) WRITE(711,*) "READ VEHICLE # PATH", jtmp

	         DO itmp = 1, ntmp  - 1
                Index1D = itmp
	            evalue = float(OutToInNodeNum(jpath_tmp(itmp)))
				IF(itmp == ntmp-1) THEN !destination node
				  iflag1 = 0
				ENDIF
		       CALL vehatt_Insert(jm,Index1D,1,evalue)
	         ENDDO
             evalue =float(destination(MasterDest(m_dynust_veh_nde(jm)%IntDestZone(ndestmp))))
	         Index1D = ntmp
             CALL vehatt_Insert(jm,Index1D,1,evalue)
             
             CALL vehatt_Active_Size(jm,ntmp) 
             jpath_tmp(ntmp)=m_dynust_network_node_nde(destination(MasterDest(m_dynust_veh_nde(jm)%IntDestZone(ndestmp))))%IntoOutNodeNum
             icn = 0
             
             CALL entryqueue_insert(m_dynust_last_stand(jm)%isec,jm) 
             m_dynust_network_arc_de(m_dynust_last_stand(jm)%isec)%entryqueue=m_dynust_network_arc_de(m_dynust_last_stand(jm)%isec)%entryqueue+1		  
             
             IF(stimetp >= starttm.and.stimetp < endtm) numcars=numcars+1

!            READ HISTORICAL NODE ARRIVAL TIME
             IF(ReadHistArr > 0.and.RunMode == 2) THEN
               READ(560,'(1000f8.2)') xstime,(jtime_tmp(km),km = 1,ntmp-1)
               DO itmp = 1, ntmp-1 
                 Index1D = itmp                 
                 evalue = jtime_tmp(itmp)
                 CALL vehatt_Insert(jm,Index1D,4,evalue)
               ENDDO
               CALL SETDELAYTOLE(jm) ! SET DELAY THRESHOLD FOR VEH JM
             ENDIF

!            AFTER READING IN THE PATH, FOR ENTOURE AND PRETRIP VEHICLES CHECK IF THE PATH IS GOING THROUGH THE INCIDENT LOCATION, IF SO MARK IT TO GET A NEW PATH
             Iwill = 0
             IF(RunMode == 2.OR.RunMode == 4) THEN
               IF((ivcltmp == 4.or.ivcltmp == 5.and.ivcl2tmp /= 6).AND.stimetp >= InfoStartTime.AND.inci_num > 0) THEN
                 iculnk = 1
                 icu2 = m_dynust_last_stand(jm)%isec
                 current_time = 0
                 NodeSumOrg = 0
                 BottleneckAhead = .false.
                 CALL GET_AUTO_REMAIN_TIME(jm,icu2,iculnk,l,current_time,NodeSumOrg,BottleneckAhead)
                 IF(BottleneckAhead) THEN
                    IWill = 1
                     WRITE(8456,'("PreT Time: ",f7.1," Vehicle no",i7, " OD zone", 2i6," #Curn,UN,DN ",i4,2i6," delay tole:(min) ", f6.1," curn delay:(min) ",f6.1)') (l-1)*xminPerSimInt,jm,m_dynust_last_stand(jm)%jorig,m_dynust_last_stand(jm)%jdest,m_dynust_veh_nde(jm)%icurrnt,m_dynust_network_node_nde(m_dynust_network_arc_nde(icu2)%iunod)%IntoOutNodeNum,&
                     m_dynust_network_node_nde(m_dynust_network_arc_nde(icu2)%idnod)%IntoOutNodeNum,m_dynust_veh(jm)%delaytole/AttScale, m_dynust_veh(jm)%currentdelay/AttScale
                 ENDIF
               ENDIF
             ENDIF

!            IF THIS VEH IS DIVERTING, THEN MARK IT HERE
             IF(IWill == 1) THEN
                ivct = ivct + 1
                IF(ivct > tmparysize) THEN ! need to increase the buffer size
	              WRITE(911,*) 'Need to increase tmparysize in PARAMETER.dat'
                  WRITE(911,*) "In GUI, please go to project->scenario data-> scenario-> advanced PARAMETER setting"
	              WRITE(911,*) 'Or one can directly modify PARAMETER.dat'
	              STOP
                ENDIF
!               VehLodtmp(ivct,2) = jtmp
                VehLodtmp(ivct,2) = justveh
                vehLodtmp(ivct,1) = jdest_tmp(1)
                vehlodzone(jdest_tmp(1))=.true.
             ENDIF

     	  ENDIF  !369
!         ASSIGN MOVE TYPE ATTRIBUTE      
          !IF(MOVESFlag > 0) THEN
          !  CALL AssignMovesType(jm,itype)
          !  m_dynust_veh_nde(jm)%MovesType = itype       
          !ENDIF
     	  
900       CONTINUE

        GOTO 100 ! go back to read the next vehicle
        
ELSE !(nint(stimetp/xminPerSimInt)-nint((t_start/xminPerSimInt+tdspstep)) > 0) THEN 
      iread_veh_flag = 0 ! STOPper

!     IF THERE IS AT LEAST ONE VEH TO BE ASSIGNED WITH A NEW PATH     
      IF(ivct > 0) THEN
         IF(ALLOCATED(vehlod)) DEALLOCATE(vehlod)
         ALLOCATE(vehLod(ivct,2))
         vehlod(:,:) = 0
         vehlod(1:ivct,:)= VehLodtmp(1:ivct,:)

          IF(UETimeFlagR > 0) THEN ! CALL pen_cal sub - overwrite the traveltime and travelpenalty using the read uetraveltime and uettpenalty
            iggtime = max(1,ifix((l-2)*1.0/simPerAgg)+1)
            TravelTime(:,iggtime) = UETravelTime(:,iggtime)*AttScale
            m_dynust_network_arcmove_nde(:,:)%Penalty = UETravelPenalty(:,iggtime,:)
          ELSE
            CALL CAL_PENALTY(l)
          ENDIF
          CALL TDSP_MAIN(0,l)
          CALL IMSLSort(vehlod,ivct,2)
          
          ilook = 1          
          CallKspReadVeh = .true.
          DO j20 = 1, noof_master_destinations ! loop through the destinations
               !CALL tdsp_core(0,j20) ! CALL SP

                 IF(vms_num > 0.or.ier_ok == 1.or.(ReadHistArr > 0.and.RunMode == 2)) THEN
                   DO i1 = 1, noofnodes
 	                 IF(i1 <= noofnodes-1) THEN 
	                   imove = backpointr(i1+1)-backpointr(i1)+1
	                 ELSE
	                   imove = noofarcs - backpointr(i1-1) + 1
	                 ENDIF
                     DO i3 = 1, kay
                       DO i5 = 1, min(maxmove,imove)
                         !pathpointercopy1(j20,i1,i3,i5) = pathpointer(i1,1,i3,1)%p(i5)
                         !pathpointercopy2(j20,i1,i3,i5) = pathpointer(i1,1,i3,2)%p(i5)
                         !pathpointercopy3(j20,i1,i3,i5) = pathpointer(i1,1,i3,3)%p(i5)
                         !LabelOutCostCopy(j20,i1,1,i3,i5) = LabelCost(i1,1,i3)%p(i5)
                       ENDDO
                     ENDDO
                   ENDDO
                 ENDIF
                 DO while(vehlod(ilook,1) == j20.and.ilook <= ivct)     
                   !CALL RETRIEVE_VEH_PATH(vehlod(ilook,2),m_dynust_last_stand(vehlod(ilook,2))%isec,ipinit,1,vehlod(ilook,1),1,NodeSum)
                   CALL RETRIEVE_VEH_PATH_AStar(vehlod(ilook,2),m_dynust_last_stand(vehlod(ilook,2))%isec,ipinit,1,vehlod(ilook,1),1,NodeSum)
                   ilook = ilook + 1
                   IF(ilook > ivct) GOTO 340
                 ENDDO

!             ENDIF
          ENDDO  !j20
          340 CONTINUE
	   ENDIF !ivct gt 0
       ivct = 0 !***************
       
ENDIF !(nint(stimetp/xminPerSimInt)-nint((t_start/xminPerSimInt+tdspstep)) > 0) THEN 


	
200   CONTINUE
300   FORMAT(4i12,f10.2)
301   FORMAT(i9,2i7,f8.2,6i6,2f8.4,2i5,f12.8,f6.1)
400   FORMAT(i12,f7.2)
601   FORMAT(5i6,f8.4,i6,2f8.4,2i6)
700   FORMAT(1000i7)
!     write output_veh and output_path and 6099 for mtc codes
	  DO isee = 1, idarrayct
          CALL write_vehicles(totalveh,idarray(isee,2),idarray(isee,2),97,1,98)	
      ENDDO
      
      ENDIF ! IF not eof
      END SUBROUTINE




SUBROUTINE check_miva_vehicle
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~ Copyright (C) 2002-2009 The Arizona Board of Regents on Behalf of The University of Arizona.  ~~!
!~~ Developed by Yi-Chang Chiu, Ph.D. as par to the DynusT project.                               ~~!
!~~ This program is free software: you can redistribute it and/or modify it                       ~~!
!~~ under the terms of the GNU General Public License version 2 as published by                   ~~!
!~~ the Free Software Foundation.                                                                 ~~!
!~~ This program is distributed in the hope that it will be useful,                               ~~!
!~~ but WITHOUT ANY WARRANTY; without even the implied warranty of                                ~~!
!~~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                                          ~~!
!~~ See the GNU General Public License for more details.                                          ~~!
!~~ You should have received a copy of the GNU General Public License                             ~~!
!~~ along with this program (in the file COPYING).                                                ~~!
!~~ IF not, see <http://www.gnu.org/licenses/>.                                                   ~~!
!~~ Please see http://dynust.net/wikibin/doku.php for contact information and names               ~~!
!~~ of individual contributors.                                                                   ~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!

USE DYNUST_MAIN_MODULE


  DO i = 1, MaxVehiclesFile
     IF(iso_ok == 1.and.iue_ok == 1) exit
     READ(500,301)jtmp,iutmp,idtmp,stimetp,ivcltmp,ivcl2tmp,ihovtmp,ntmp,ndestmp,infotmp,ribftmp,comptmp,izonetmp
	 IF(ivcltmp == 2) iso_ok = 1
     IF(ivcltmp == 3) iue_ok = 1
	 DO j_ah=1,ndestmp
          READ(500,*) 
     ENDDO
  ENDDO

301   FORMAT(i9,2i7,f8.2,6i6,2f8.4,2i5,f12.8,f6.1)
  rewind(500)
  READ(500,*) !
  READ(500,*) !skip a line

    IF(iso_ok < 1.and.iue_ok < 1)THEN
      WRITE(911,*) 'Iterative mode and vehicle file loading model has been specified, but'
      WRITE(911,*) 'No UE or SO vehicles were found for the iterative mtc mode'
	  WRITE(911,*) 'Please check vehicle.dat setting'
	  STOP
    ENDIF


END SUBROUTINE
