; mark_description "Intel(R) Visual Fortran Compiler XE for applications running on IA-32, Version 13.1.1.171 Build 20130313";
; mark_description "-S";
	.686P
 	.387
	OPTION DOTNAME
	ASSUME	CS:FLAT,DS:FLAT,SS:FLAT
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
TXTST0:
; -- Begin  _READ_INTERSECTION_CONTROLS
; mark_begin;
IF @Version GE 800
  .MMX
ELSEIF @Version GE 612
  .MMX
  MMWORD TEXTEQU <QWORD>
ENDIF
IF @Version GE 800
  .XMM
ELSEIF @Version GE 614
  .XMM
  XMMWORD TEXTEQU <OWORD>
ENDIF
       ALIGN     16
	PUBLIC _READ_INTERSECTION_CONTROLS
_READ_INTERSECTION_CONTROLS	PROC NEAR 
.B1.1:                          ; Preds .B1.0
        push      ebx                                           ;1.17
        mov       ebx, esp                                      ;1.17
        and       esp, -16                                      ;1.17
        push      ebp                                           ;1.17
        push      ebp                                           ;1.17
        mov       ebp, DWORD PTR [4+ebx]                        ;1.17
        mov       DWORD PTR [4+esp], ebp                        ;1.17
        mov       ebp, esp                                      ;1.17
        sub       esp, 1112                                     ;1.17
        xor       eax, eax                                      ;1.17
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFNODES] ;1.17
        test      edx, edx                                      ;1.17
        mov       DWORD PTR [-780+ebp], esi                     ;1.17
        cmovg     eax, edx                                      ;1.17
        shl       eax, 3                                        ;1.17
        mov       DWORD PTR [-772+ebp], edi                     ;1.17
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFARCS] ;1.17
        mov       DWORD PTR [-1048+ebp], edx                    ;1.17
        mov       DWORD PTR [-788+ebp], esp                     ;1.17
        call      __alloca_probe                                ;1.17
        and       esp, -16                                      ;1.17
        mov       eax, esp                                      ;1.17
                                ; LOE eax esi
.B1.721:                        ; Preds .B1.1
        mov       edx, esi                                      ;1.17
        inc       edx                                           ;1.17
        mov       DWORD PTR [-1028+ebp], eax                    ;1.17
        mov       eax, 0                                        ;1.17
        cmovns    eax, edx                                      ;1.17
        shl       eax, 3                                        ;1.17
        mov       DWORD PTR [-1108+ebp], edx                    ;1.17
        call      __alloca_probe                                ;1.17
        and       esp, -16                                      ;1.17
        mov       eax, esp                                      ;1.17
                                ; LOE eax esi
.B1.720:                        ; Preds .B1.721
        mov       ecx, DWORD PTR [-1048+ebp]                    ;36.6
        lea       edx, DWORD PTR [4+esi*4]                      ;38.3
        mov       DWORD PTR [-1056+ebp], eax                    ;1.17
        test      ecx, ecx                                      ;36.6
        mov       DWORD PTR [-1044+ebp], edx                    ;38.3
        lea       eax, DWORD PTR [ecx*4]                        ;36.6
        mov       DWORD PTR [-1060+ebp], eax                    ;36.6
        jle       .B1.7         ; Prob 50%                      ;36.6
                                ; LOE ecx esi cl ch
.B1.2:                          ; Preds .B1.720
        xor       edx, edx                                      ;
        mov       eax, DWORD PTR [-1028+ebp]                    ;
        mov       edi, eax                                      ;
        mov       DWORD PTR [-1092+ebp], edx                    ;
        pxor      xmm0, xmm0                                    ;36.6
        mov       DWORD PTR [-1112+ebp], esi                    ;
        shl       ecx, 2                                        ;
        mov       DWORD PTR [-1104+ebp], ecx                    ;
        mov       DWORD PTR [-1100+ebp], eax                    ;
        mov       DWORD PTR [-1096+ebp], edx                    ;
        mov       ecx, DWORD PTR [-1092+ebp]                    ;
        mov       esi, DWORD PTR [-1048+ebp]                    ;
                                ; LOE ecx esi edi
.B1.3:                          ; Preds .B1.5 .B1.710 .B1.707 .B1.2
        cmp       esi, 24                                       ;36.6
        jle       .B1.692       ; Prob 0%                       ;36.6
                                ; LOE ecx esi edi
.B1.4:                          ; Preds .B1.3
        push      DWORD PTR [-1104+ebp]                         ;36.6
        push      0                                             ;36.6
        push      DWORD PTR [-1100+ebp]                         ;36.6
        mov       DWORD PTR [-1092+ebp], ecx                    ;36.6
        call      __intel_fast_memset                           ;36.6
                                ; LOE esi edi
.B1.722:                        ; Preds .B1.4
        mov       ecx, DWORD PTR [-1092+ebp]                    ;
        add       esp, 12                                       ;36.6
                                ; LOE ecx esi edi cl ch
.B1.5:                          ; Preds .B1.722
        mov       edx, DWORD PTR [-1100+ebp]                    ;36.6
        lea       edi, DWORD PTR [edi+esi*4]                    ;36.6
        lea       ecx, DWORD PTR [ecx+esi*4]                    ;36.6
        lea       edx, DWORD PTR [edx+esi*4]                    ;36.6
        mov       DWORD PTR [-1100+ebp], edx                    ;36.6
        mov       edx, DWORD PTR [-1096+ebp]                    ;36.6
        inc       edx                                           ;36.6
        mov       DWORD PTR [-1096+ebp], edx                    ;36.6
        cmp       edx, 2                                        ;36.6
        jb        .B1.3         ; Prob 50%                      ;36.6
                                ; LOE ecx esi edi
.B1.6:                          ; Preds .B1.710 .B1.707 .B1.5
        mov       esi, DWORD PTR [-1112+ebp]                    ;
                                ; LOE esi
.B1.7:                          ; Preds .B1.720 .B1.6
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_KGPOINT+24] ;37.6
        test      ecx, ecx                                      ;37.6
        jle       .B1.10        ; Prob 50%                      ;37.6
                                ; LOE ecx esi
.B1.8:                          ; Preds .B1.7
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_KGPOINT] ;37.6
        cmp       ecx, 24                                       ;37.6
        mov       DWORD PTR [-1104+ebp], eax                    ;37.6
        jle       .B1.651       ; Prob 0%                       ;37.6
                                ; LOE eax ecx esi al ah
.B1.9:                          ; Preds .B1.8
        shl       ecx, 2                                        ;37.6
        push      ecx                                           ;37.6
        push      0                                             ;37.6
        push      DWORD PTR [-1104+ebp]                         ;37.6
        call      __intel_fast_memset                           ;37.6
                                ; LOE esi
.B1.723:                        ; Preds .B1.9
        add       esp, 12                                       ;37.6
                                ; LOE esi
.B1.10:                         ; Preds .B1.665 .B1.723 .B1.7 .B1.663
        test      esi, esi                                      ;38.3
        jl        .B1.16        ; Prob 50%                      ;38.3
                                ; LOE
.B1.11:                         ; Preds .B1.10
        xor       eax, eax                                      ;
        mov       esi, DWORD PTR [-1056+ebp]                    ;
        xor       edx, edx                                      ;
        mov       eax, esi                                      ;
        pxor      xmm0, xmm0                                    ;38.3
        mov       edi, DWORD PTR [-1108+ebp]                    ;38.3
        mov       DWORD PTR [-1104+ebp], eax                    ;38.3
        mov       DWORD PTR [-1112+ebp], edx                    ;38.3
                                ; LOE esi edi
.B1.12:                         ; Preds .B1.14 .B1.11
        cmp       edi, 24                                       ;38.3
        jle       .B1.673       ; Prob 0%                       ;38.3
                                ; LOE esi edi
.B1.13:                         ; Preds .B1.12
        push      DWORD PTR [-1044+ebp]                         ;38.3
        push      0                                             ;38.3
        push      DWORD PTR [-1104+ebp]                         ;38.3
        call      __intel_fast_memset                           ;38.3
                                ; LOE esi edi
.B1.724:                        ; Preds .B1.13
        add       esp, 12                                       ;38.3
                                ; LOE esi edi
.B1.14:                         ; Preds .B1.687 .B1.724 .B1.685
        mov       eax, DWORD PTR [-1044+ebp]                    ;38.3
        add       esi, eax                                      ;38.3
        mov       edx, DWORD PTR [-1112+ebp]                    ;38.3
        inc       edx                                           ;38.3
        add       DWORD PTR [-1104+ebp], eax                    ;38.3
        mov       DWORD PTR [-1112+ebp], edx                    ;38.3
        cmp       edx, 2                                        ;38.3
        jb        .B1.12        ; Prob 50%                      ;38.3
                                ; LOE esi edi
.B1.16:                         ; Preds .B1.14 .B1.10
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+24] ;39.6
        test      esi, esi                                      ;39.6
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;39.6
        jle       .B1.23        ; Prob 50%                      ;39.6
                                ; LOE ecx esi
.B1.17:                         ; Preds .B1.16
        mov       edx, esi                                      ;39.6
        shr       edx, 31                                       ;39.6
        add       edx, esi                                      ;39.6
        sar       edx, 1                                        ;39.6
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;39.6
        test      edx, edx                                      ;39.6
        jbe       .B1.95        ; Prob 10%                      ;39.6
                                ; LOE edx ecx esi edi
.B1.18:                         ; Preds .B1.17
        xor       eax, eax                                      ;
        mov       DWORD PTR [-1112+ebp], esi                    ;
        mov       DWORD PTR [-1108+ebp], ecx                    ;
        xor       ecx, ecx                                      ;
        mov       esi, eax                                      ;
                                ; LOE eax edx ecx esi edi
.B1.19:                         ; Preds .B1.19 .B1.18
        inc       eax                                           ;39.6
        mov       DWORD PTR [348+esi+edi], ecx                  ;39.6
        mov       DWORD PTR [1248+esi+edi], ecx                 ;39.6
        add       esi, 1800                                     ;39.6
        cmp       eax, edx                                      ;39.6
        jb        .B1.19        ; Prob 63%                      ;39.6
                                ; LOE eax edx ecx esi edi
.B1.20:                         ; Preds .B1.19
        mov       esi, DWORD PTR [-1112+ebp]                    ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;39.6
        mov       ecx, DWORD PTR [-1108+ebp]                    ;
                                ; LOE eax ecx esi edi
.B1.21:                         ; Preds .B1.20 .B1.95
        lea       edx, DWORD PTR [-1+eax]                       ;39.6
        cmp       esi, edx                                      ;39.6
        jbe       .B1.23        ; Prob 10%                      ;39.6
                                ; LOE eax ecx edi
.B1.22:                         ; Preds .B1.21
        mov       edx, ecx                                      ;39.6
        add       eax, ecx                                      ;39.6
        neg       edx                                           ;39.6
        add       edx, eax                                      ;39.6
        imul      eax, edx, 900                                 ;39.6
        mov       DWORD PTR [-552+edi+eax], 0                   ;39.6
                                ; LOE
.B1.23:                         ; Preds .B1.16 .B1.21 .B1.22
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+44] ;40.6
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+36] ;40.6
        test      edx, edx                                      ;40.6
        mov       DWORD PTR [-1108+ebp], eax                    ;40.6
        mov       DWORD PTR [-1112+ebp], edx                    ;40.6
        jle       .B1.26        ; Prob 10%                      ;40.6
                                ; LOE eax al ah
.B1.24:                         ; Preds .B1.23
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+24] ;40.6
        test      edx, edx                                      ;40.6
        jg        .B1.87        ; Prob 50%                      ;40.6
                                ; LOE eax edx al ah
.B1.25:                         ; Preds .B1.94 .B1.671 .B1.24
        test      edx, edx                                      ;41.6
        jg        .B1.80        ; Prob 50%                      ;41.6
                                ; LOE edx
.B1.26:                         ; Preds .B1.648 .B1.86 .B1.23 .B1.25
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOF_MASTER_DESTINATIONS] ;47.3
        neg       eax                                           ;47.3
        add       eax, DWORD PTR [-1048+ebp]                    ;47.3
        test      eax, eax                                      ;47.3
        jle       .B1.39        ; Prob 2%                       ;47.3
                                ; LOE eax
.B1.27:                         ; Preds .B1.26
        xor       edx, edx                                      ;
        mov       ecx, 1                                        ;
        mov       DWORD PTR [-1104+ebp], edx                    ;
        mov       edi, ecx                                      ;
        mov       DWORD PTR [-1108+ebp], eax                    ;
        mov       esi, 84                                       ;
                                ; LOE esi edi
.B1.28:                         ; Preds .B1.37 .B1.27
        push      32                                            ;48.7
        xor       eax, eax                                      ;48.7
        lea       edx, DWORD PTR [-232+ebp]                     ;48.7
        push      eax                                           ;48.7
        push      OFFSET FLAT: __STRLITPACK_145.0.1             ;48.7
        push      -2088435965                                   ;48.7
        push      44                                            ;48.7
        push      edx                                           ;48.7
        mov       DWORD PTR [-232+ebp], eax                     ;48.7
        call      _for_read_seq_lis                             ;48.7
                                ; LOE eax esi edi
.B1.725:                        ; Preds .B1.28
        add       esp, 24                                       ;48.7
                                ; LOE eax esi edi
.B1.29:                         ; Preds .B1.725
        test      eax, eax                                      ;48.7
        jne       .B1.37        ; Prob 50%                      ;48.7
                                ; LOE eax esi edi
.B1.30:                         ; Preds .B1.29
        imul      eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE+32], -84 ;48.7
        add       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE] ;48.7
        mov       DWORD PTR [-164+ebp], 1                       ;48.7
        mov       DWORD PTR [-576+ebp], 4                       ;48.7
        mov       ecx, DWORD PTR [eax+esi]                      ;48.32
        mov       edx, DWORD PTR [32+eax+esi]                   ;48.32
        lea       eax, DWORD PTR [-576+ebp]                     ;48.7
        shl       edx, 2                                        ;48.7
        add       ecx, 4                                        ;48.7
        push      eax                                           ;48.7
        push      OFFSET FLAT: __STRLITPACK_146.0.1             ;48.7
        sub       ecx, edx                                      ;48.7
        lea       edx, DWORD PTR [-232+ebp]                     ;48.7
        push      edx                                           ;48.7
        mov       DWORD PTR [-572+ebp], ecx                     ;48.7
        call      _for_read_seq_lis_xmit                        ;48.7
                                ; LOE eax esi edi
.B1.726:                        ; Preds .B1.30
        add       esp, 12                                       ;48.7
                                ; LOE eax esi edi
.B1.31:                         ; Preds .B1.726
        test      eax, eax                                      ;48.7
        jne       .B1.37        ; Prob 10%                      ;48.7
                                ; LOE eax esi edi
.B1.32:                         ; Preds .B1.31
        mov       DWORD PTR [-1112+ebp], edi                    ;
        lea       edi, DWORD PTR [-576+ebp]                     ;
                                ; LOE esi edi
.B1.33:                         ; Preds .B1.32 .B1.35
        mov       ecx, DWORD PTR [-164+ebp]                     ;48.7
        inc       ecx                                           ;48.7
        mov       DWORD PTR [-164+ebp], ecx                     ;48.7
        cmp       ecx, 4                                        ;48.7
        jg        .B1.645       ; Prob 20%                      ;48.7
                                ; LOE ecx esi edi
.B1.34:                         ; Preds .B1.33
        imul      eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE+32], -84 ;48.7
        add       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE] ;48.7
        push      edi                                           ;48.7
        push      OFFSET FLAT: __STRLITPACK_146.0.1             ;48.7
        sub       ecx, DWORD PTR [32+eax+esi]                   ;48.7
        mov       edx, DWORD PTR [eax+esi]                      ;48.32
        lea       eax, DWORD PTR [-232+ebp]                     ;48.7
        push      eax                                           ;48.7
        mov       DWORD PTR [-576+ebp], 4                       ;48.7
        lea       ecx, DWORD PTR [edx+ecx*4]                    ;48.7
        mov       DWORD PTR [-572+ebp], ecx                     ;48.7
        call      _for_read_seq_lis_xmit                        ;48.7
                                ; LOE eax esi edi
.B1.727:                        ; Preds .B1.34
        add       esp, 12                                       ;48.7
                                ; LOE eax esi edi
.B1.35:                         ; Preds .B1.727
        test      eax, eax                                      ;48.7
        je        .B1.33        ; Prob 82%                      ;48.7
                                ; LOE eax esi edi
.B1.36:                         ; Preds .B1.35
        mov       edi, DWORD PTR [-1112+ebp]                    ;
                                ; LOE eax esi edi
.B1.37:                         ; Preds .B1.917 .B1.36 .B1.31 .B1.29
        imul      edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE+32], -84 ;49.9
        inc       edi                                           ;52.3
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;48.7
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE] ;49.6
        add       eax, esi                                      ;49.9
        add       esi, 84                                       ;52.3
        mov       ecx, DWORD PTR [32+edx+eax]                   ;49.9
        shl       ecx, 2                                        ;50.8
        neg       ecx                                           ;50.8
        add       ecx, DWORD PTR [edx+eax]                      ;50.8
        mov       eax, 1                                        ;50.8
        mov       edx, DWORD PTR [-1104+ebp]                    ;50.8
        cmp       DWORD PTR [8+ecx], 1                          ;50.8
        cmovg     edx, eax                                      ;50.8
        mov       DWORD PTR [-1104+ebp], edx                    ;50.8
        cmp       edi, DWORD PTR [-1108+ebp]                    ;52.3
        jle       .B1.28        ; Prob 82%                      ;52.3
                                ; LOE edx esi edi dl dh
.B1.38:                         ; Preds .B1.37
        test      edx, edx                                      ;53.12
        jne       .B1.44        ; Prob 50%                      ;53.12
                                ; LOE
.B1.39:                         ; Preds .B1.26 .B1.38
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFARCS] ;54.6
        test      ecx, ecx                                      ;54.6
        jle       .B1.44        ; Prob 3%                       ;54.6
                                ; LOE ecx
.B1.40:                         ; Preds .B1.39
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFNODES] ;56.14
        xor       eax, eax                                      ;
        sub       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOF_MASTER_DESTINATIONS] ;56.14
        xor       esi, esi                                      ;
        imul      eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32], -152 ;
        mov       edi, edx                                      ;
        shr       edi, 31                                       ;
        add       edi, edx                                      ;
        sar       edi, 1                                        ;
        mov       DWORD PTR [-1096+ebp], esi                    ;
        mov       DWORD PTR [-1092+ebp], edx                    ;
        add       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;
        mov       DWORD PTR [-1088+ebp], edi                    ;
        mov       DWORD PTR [-1040+ebp], ecx                    ;
        mov       edx, esi                                      ;
                                ; LOE eax edx esi
.B1.41:                         ; Preds .B1.42 .B1.40
        movsx     ecx, WORD PTR [300+edx+eax]                   ;55.15
        cmp       ecx, 5                                        ;55.54
        je        .B1.96        ; Prob 16%                      ;55.54
                                ; LOE eax edx esi
.B1.42:                         ; Preds .B1.41 .B1.96 .B1.105 .B1.106 .B1.107
                                ;      
        inc       esi                                           ;54.6
        add       edx, 152                                      ;54.6
        cmp       esi, DWORD PTR [-1040+ebp]                    ;54.6
        jb        .B1.41        ; Prob 82%                      ;54.6
                                ; LOE eax edx esi
.B1.44:                         ; Preds .B1.42 .B1.38 .B1.39
        push      32                                            ;65.12
        xor       eax, eax                                      ;65.12
        lea       edx, DWORD PTR [-232+ebp]                     ;65.12
        push      eax                                           ;65.12
        push      OFFSET FLAT: __STRLITPACK_148.0.1             ;65.12
        push      -2088435968                                   ;65.12
        push      678                                           ;65.12
        push      edx                                           ;65.12
        mov       DWORD PTR [-232+ebp], eax                     ;65.12
        call      _for_close                                    ;65.12
                                ; LOE
.B1.728:                        ; Preds .B1.44
        add       esp, 24                                       ;65.12
                                ; LOE
.B1.45:                         ; Preds .B1.728
        push      32                                            ;66.3
        push      -2088435968                                   ;66.3
        push      44                                            ;66.3
        mov       DWORD PTR [-232+ebp], 0                       ;66.3
        lea       eax, DWORD PTR [-232+ebp]                     ;66.3
        push      eax                                           ;66.3
        call      _for_rewind                                   ;66.3
                                ; LOE
.B1.729:                        ; Preds .B1.45
        add       esp, 16                                       ;66.3
                                ; LOE
.B1.46:                         ; Preds .B1.729
        push      32                                            ;67.3
        xor       eax, eax                                      ;67.3
        lea       edx, DWORD PTR [-232+ebp]                     ;67.3
        push      eax                                           ;67.3
        push      OFFSET FLAT: __STRLITPACK_149.0.1             ;67.3
        push      -2088435968                                   ;67.3
        push      44                                            ;67.3
        push      edx                                           ;67.3
        mov       DWORD PTR [-232+ebp], eax                     ;67.3
        call      _for_read_seq_lis                             ;67.3
                                ; LOE
.B1.730:                        ; Preds .B1.46
        add       esp, 24                                       ;67.3
                                ; LOE
.B1.47:                         ; Preds .B1.730
        push      32                                            ;68.3
        xor       eax, eax                                      ;68.3
        lea       edx, DWORD PTR [-232+ebp]                     ;68.3
        push      eax                                           ;68.3
        push      OFFSET FLAT: __STRLITPACK_150.0.1             ;68.3
        push      -2088435968                                   ;68.3
        push      44                                            ;68.3
        push      edx                                           ;68.3
        mov       DWORD PTR [-232+ebp], eax                     ;68.3
        call      _for_read_seq_lis                             ;68.3
                                ; LOE
.B1.731:                        ; Preds .B1.47
        add       esp, 24                                       ;68.3
                                ; LOE
.B1.48:                         ; Preds .B1.731
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFNODES] ;71.11
        lea       edx, DWORD PTR [-764+ebp]                     ;71.11
        sub       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOF_MASTER_DESTINATIONS] ;71.11
        push      edx                                           ;71.11
        mov       DWORD PTR [-764+ebp], eax                     ;71.11
        call      _DYNUST_SIGNAL_MODULE_mp_DECLARESIGNAL        ;71.11
                                ; LOE
.B1.732:                        ; Preds .B1.48
        add       esp, 4                                        ;71.11
                                ; LOE
.B1.49:                         ; Preds .B1.732
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOF_MASTER_DESTINATIONS] ;74.6
        mov       ecx, 1                                        ;
        neg       edx                                           ;74.6
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFNODES] ;74.6
        add       edx, eax                                      ;74.6
        test      edx, edx                                      ;74.9
        jle       .B1.114       ; Prob 2%                       ;74.9
                                ; LOE eax edx ecx
.B1.50:                         ; Preds .B1.49
        mov       esi, DWORD PTR [-1028+ebp]                    ;
        xor       eax, eax                                      ;
        sub       esi, DWORD PTR [-1060+ebp]                    ;
        mov       edi, DWORD PTR [-1048+ebp]                    ;
        mov       DWORD PTR [-1068+ebp], eax                    ;
        mov       eax, 1                                        ;
        mov       DWORD PTR [-1052+ebp], 84                     ;
        mov       DWORD PTR [-736+ebp], ecx                     ;
        lea       esi, DWORD PTR [esi+edi*8]                    ;
        mov       DWORD PTR [-1072+ebp], esi                    ;
        mov       esi, DWORD PTR [-1044+ebp]                    ;
        mov       edi, DWORD PTR [-1056+ebp]                    ;
        sub       edi, esi                                      ;
        mov       DWORD PTR [-1016+ebp], eax                    ;
        mov       DWORD PTR [-1020+ebp], edx                    ;
        lea       esi, DWORD PTR [edi+esi*2]                    ;
        mov       DWORD PTR [-1064+ebp], esi                    ;
                                ; LOE
.B1.51:                         ; Preds .B1.112 .B1.50
        push      32                                            ;77.7
        xor       edx, edx                                      ;77.7
        lea       ecx, DWORD PTR [-232+ebp]                     ;77.7
        push      edx                                           ;77.7
        mov       eax, DWORD PTR [-1016+ebp]                    ;75.10
        push      OFFSET FLAT: __STRLITPACK_151.0.1             ;77.7
        sub       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_KGPOINT+32] ;75.10
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_KGPOINT] ;75.10
        push      -2088435965                                   ;77.7
        push      44                                            ;77.7
        mov       esi, DWORD PTR [-736+ebp]                     ;75.10
        push      ecx                                           ;77.7
        mov       DWORD PTR [edi+eax*4], esi                    ;75.10
        mov       DWORD PTR [-232+ebp], edx                     ;77.7
        call      _for_read_seq_lis                             ;77.7
                                ; LOE eax
.B1.733:                        ; Preds .B1.51
        add       esp, 24                                       ;77.7
                                ; LOE eax
.B1.52:                         ; Preds .B1.733
        test      eax, eax                                      ;77.7
        jne       .B1.60        ; Prob 50%                      ;77.7
                                ; LOE eax
.B1.53:                         ; Preds .B1.52
        imul      esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE+32], -84 ;77.7
        lea       edi, DWORD PTR [-232+ebp]                     ;77.7
        add       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE] ;77.7
        mov       eax, DWORD PTR [-1052+ebp]                    ;77.32
        mov       DWORD PTR [-164+ebp], 1                       ;77.7
        mov       DWORD PTR [-568+ebp], 4                       ;77.7
        mov       ecx, DWORD PTR [esi+eax]                      ;77.32
        mov       edx, DWORD PTR [32+esi+eax]                   ;77.32
        lea       esi, DWORD PTR [-568+ebp]                     ;77.7
        push      esi                                           ;77.7
        shl       edx, 2                                        ;77.7
        add       ecx, 4                                        ;77.7
        push      OFFSET FLAT: __STRLITPACK_152.0.1             ;77.7
        push      edi                                           ;77.7
        sub       ecx, edx                                      ;77.7
        mov       DWORD PTR [-564+ebp], ecx                     ;77.7
        call      _for_read_seq_lis_xmit                        ;77.7
                                ; LOE eax esi
.B1.734:                        ; Preds .B1.53
        add       esp, 12                                       ;77.7
                                ; LOE eax esi
.B1.54:                         ; Preds .B1.734
        test      eax, eax                                      ;77.7
        jne       .B1.60        ; Prob 10%                      ;77.7
                                ; LOE eax esi
.B1.55:                         ; Preds .B1.54
        mov       edi, DWORD PTR [-1052+ebp]                    ;
                                ; LOE esi edi
.B1.56:                         ; Preds .B1.55 .B1.58
        mov       ecx, DWORD PTR [-164+ebp]                     ;77.7
        inc       ecx                                           ;77.7
        mov       DWORD PTR [-164+ebp], ecx                     ;77.7
        cmp       ecx, 4                                        ;77.7
        jg        .B1.643       ; Prob 20%                      ;77.7
                                ; LOE ecx esi edi
.B1.57:                         ; Preds .B1.56
        imul      eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE+32], -84 ;77.7
        add       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE] ;77.7
        push      esi                                           ;77.7
        push      OFFSET FLAT: __STRLITPACK_152.0.1             ;77.7
        sub       ecx, DWORD PTR [32+edi+eax]                   ;77.7
        mov       edx, DWORD PTR [edi+eax]                      ;77.32
        lea       eax, DWORD PTR [-232+ebp]                     ;77.7
        push      eax                                           ;77.7
        mov       DWORD PTR [-568+ebp], 4                       ;77.7
        lea       ecx, DWORD PTR [edx+ecx*4]                    ;77.7
        mov       DWORD PTR [-564+ebp], ecx                     ;77.7
        call      _for_read_seq_lis_xmit                        ;77.7
                                ; LOE eax esi edi
.B1.735:                        ; Preds .B1.57
        add       esp, 12                                       ;77.7
                                ; LOE eax esi edi
.B1.58:                         ; Preds .B1.735
        test      eax, eax                                      ;77.7
        je        .B1.56        ; Prob 82%                      ;77.7
                                ; LOE eax esi edi
.B1.60:                         ; Preds .B1.916 .B1.58 .B1.54 .B1.52
        imul      esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE+32], -84 ;78.90
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;77.7
        mov       eax, DWORD PTR [-1052+ebp]                    ;78.90
        add       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE] ;78.90
        mov       edi, DWORD PTR [32+esi+eax]                   ;78.11
        shl       edi, 2                                        ;78.90
        neg       edi                                           ;78.90
        mov       esi, DWORD PTR [esi+eax]                      ;78.11
        mov       edx, DWORD PTR [8+edi+esi]                    ;78.11
        and       edx, -2                                       ;78.48
        cmp       edx, 4                                        ;78.48
        je        .B1.638       ; Prob 16%                      ;78.48
                                ; LOE esi edi
.B1.61:                         ; Preds .B1.915 .B1.60 .B1.638
        movdqu    xmm0, XMMWORD PTR [4+edi+esi]                 ;83.4
        push      OFFSET FLAT: __NLITPACK_0.0.1                 ;85.9
        push      OFFSET FLAT: _READ_INTERSECTION_CONTROLS$TMPSIGDATA.0.1 ;85.9
        movdqa    XMMWORD PTR [_READ_INTERSECTION_CONTROLS$TMPSIGDATA.0.1], xmm0 ;83.4
        call      _DYNUST_SIGNAL_MODULE_mp_INSERTSIGNALPAR      ;85.9
                                ; LOE
.B1.736:                        ; Preds .B1.61
        add       esp, 8                                        ;85.9
                                ; LOE
.B1.62:                         ; Preds .B1.736
        mov       eax, DWORD PTR [_READ_INTERSECTION_CONTROLS$TMPSIGDATA.0.1+4] ;86.7
        and       eax, -2                                       ;86.25
        cmp       eax, 4                                        ;86.25
        je        .B1.637       ; Prob 5%                       ;86.25
                                ; LOE
.B1.63:                         ; Preds .B1.911 .B1.62
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], 0   ;91.16
        jne       .B1.635       ; Prob 5%                       ;91.16
                                ; LOE
.B1.64:                         ; Preds .B1.910 .B1.63
        imul      edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE+32], -84 ;95.46
        mov       eax, DWORD PTR [-1052+ebp]                    ;95.46
        add       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE] ;95.46
        mov       esi, DWORD PTR [32+edi+eax]                   ;95.13
        shl       esi, 2                                        ;95.46
        neg       esi                                           ;95.46
        mov       edi, DWORD PTR [edi+eax]                      ;95.13
        mov       edx, DWORD PTR [8+esi+edi]                    ;95.13
        test      edx, edx                                      ;95.46
        mov       DWORD PTR [-1044+ebp], edx                    ;95.13
        jle       .B1.629       ; Prob 16%                      ;95.46
                                ; LOE edx esi edi dl dh
.B1.65:                         ; Preds .B1.64
        and       edx, -2                                       ;99.49
        cmp       edx, 4                                        ;99.49
        je        .B1.622       ; Prob 16%                      ;99.49
                                ; LOE edx esi edi
.B1.66:                         ; Preds .B1.623 .B1.65
        mov       ecx, DWORD PTR [4+esi+edi]                    ;108.42
                                ; LOE edx ecx esi edi
.B1.67:                         ; Preds .B1.634 .B1.903 .B1.66
        sub       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM+32] ;108.7
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM] ;108.7
        cmp       edx, 4                                        ;109.50
        mov       ecx, DWORD PTR [eax+ecx*4]                    ;108.7
        mov       DWORD PTR [4+esi+edi], ecx                    ;108.7
        je        .B1.621       ; Prob 16%                      ;109.50
                                ; LOE ecx esi edi
.B1.68:                         ; Preds .B1.67
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_BACKPOINTR] ;117.12
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_BACKPOINTR+32] ;117.12
        shl       eax, 2                                        ;117.12
        lea       ecx, DWORD PTR [edx+ecx*4]                    ;117.12
        sub       ecx, eax                                      ;117.12
        mov       edi, DWORD PTR [4+ecx]                        ;117.65
        dec       edi                                           ;117.12
        mov       esi, edi                                      ;117.12
        mov       edx, DWORD PTR [ecx]                          ;117.12
        sub       esi, edx                                      ;117.12
        inc       esi                                           ;117.12
        mov       DWORD PTR [-1012+ebp], esi                    ;117.12
        cmp       edi, edx                                      ;117.12
        jl        .B1.110       ; Prob 11%                      ;117.12
                                ; LOE edx
.B1.69:                         ; Preds .B1.68
        imul      edx, edx, 152                                 ;
        xor       ecx, ecx                                      ;
        imul      edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32], 152 ;
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+36] ;119.17
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+40] ;432.17
        mov       DWORD PTR [-1000+ebp], eax                    ;119.17
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;118.14
        mov       DWORD PTR [-1004+ebp], esi                    ;432.17
        mov       esi, eax                                      ;
        add       eax, edx                                      ;
        sub       esi, edi                                      ;
        imul      edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+32], -28 ;
        sub       eax, edi                                      ;
        add       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE] ;
        mov       DWORD PTR [-1008+ebp], ecx                    ;
        mov       DWORD PTR [-992+ebp], edx                     ;
        mov       DWORD PTR [-996+ebp], eax                     ;
                                ; LOE ecx esi
.B1.70:                         ; Preds .B1.72 .B1.69
        mov       eax, DWORD PTR [-996+ebp]                     ;118.25
        imul      edx, DWORD PTR [16+ecx+eax], 152              ;118.25
        movsx     edx, BYTE PTR [32+esi+edx]                    ;118.25
        cmp       DWORD PTR [-1000+ebp], 0                      ;119.17
        jle       .B1.72        ; Prob 3%                       ;119.17
                                ; LOE edx ecx esi
.B1.71:                         ; Preds .B1.70
        test      edx, edx                                      ;118.14
        jg        .B1.73        ; Prob 50%                      ;118.14
                                ; LOE edx ecx esi
.B1.72:                         ; Preds .B1.70 .B1.71 .B1.614 .B1.612
        mov       eax, DWORD PTR [-1008+ebp]                    ;117.12
        add       ecx, 152                                      ;117.12
        inc       eax                                           ;117.12
        mov       DWORD PTR [-1008+ebp], eax                    ;117.12
        cmp       eax, DWORD PTR [-1012+ebp]                    ;117.12
        jb        .B1.70        ; Prob 82%                      ;117.12
        jmp       .B1.110       ; Prob 100%                     ;117.12
                                ; LOE ecx esi
.B1.73:                         ; Preds .B1.71
        mov       edi, edx                                      ;
        xor       eax, eax                                      ;
        shr       edi, 31                                       ;
        add       edi, edx                                      ;
        sar       edi, 1                                        ;
        mov       DWORD PTR [-928+ebp], edi                     ;
        mov       edi, DWORD PTR [-992+ebp]                     ;
        mov       DWORD PTR [-968+ebp], eax                     ;
        mov       DWORD PTR [-956+ebp], edi                     ;
        mov       DWORD PTR [-920+ebp], edi                     ;
        mov       DWORD PTR [-984+ebp], edx                     ;
        mov       DWORD PTR [-988+ebp], ecx                     ;
        mov       DWORD PTR [-960+ebp], esi                     ;
                                ; LOE eax
.B1.74:                         ; Preds .B1.79 .B1.616 .B1.73
        cmp       DWORD PTR [-928+ebp], 0                       ;118.14
        jbe       .B1.618       ; Prob 0%                       ;118.14
                                ; LOE eax
.B1.75:                         ; Preds .B1.74
        mov       ecx, DWORD PTR [-988+ebp]                     ;119.17
        xor       edx, edx                                      ;
        mov       edi, DWORD PTR [-996+ebp]                     ;119.17
        mov       DWORD PTR [-964+ebp], edx                     ;
        mov       DWORD PTR [-980+ebp], eax                     ;
        imul      ecx, DWORD PTR [16+ecx+edi], 152              ;119.17
        mov       edi, DWORD PTR [-960+ebp]                     ;119.17
        mov       edx, DWORD PTR [36+ecx+edi]                   ;119.17
        mov       esi, DWORD PTR [68+ecx+edi]                   ;119.17
        mov       ecx, DWORD PTR [64+ecx+edi]                   ;119.17
        imul      esi, ecx                                      ;
        sub       edx, esi                                      ;
        mov       esi, DWORD PTR [-964+ebp]                     ;
        mov       DWORD PTR [-916+ebp], edx                     ;
        mov       DWORD PTR [-924+ebp], ecx                     ;
        ALIGN     16
                                ; LOE esi
.B1.76:                         ; Preds .B1.76 .B1.75
        mov       edx, DWORD PTR [-924+ebp]                     ;119.17
        lea       ecx, DWORD PTR [1+esi+esi]                    ;119.17
        imul      ecx, edx                                      ;119.17
        mov       eax, DWORD PTR [-916+ebp]                     ;119.17
        mov       edi, DWORD PTR [eax+ecx]                      ;119.17
        mov       ecx, DWORD PTR [-920+ebp]                     ;119.17
        lea       eax, DWORD PTR [edi*4]                        ;119.17
        shl       edi, 5                                        ;119.17
        sub       edi, eax                                      ;119.17
        xor       eax, eax                                      ;119.17
        mov       BYTE PTR [9+edi+ecx], al                      ;119.17
        lea       edi, DWORD PTR [2+esi+esi]                    ;119.17
        imul      edi, edx                                      ;119.17
        inc       esi                                           ;118.14
        mov       edx, DWORD PTR [-916+ebp]                     ;119.17
        mov       edi, DWORD PTR [edx+edi]                      ;119.17
        lea       edx, DWORD PTR [edi*4]                        ;119.17
        shl       edi, 5                                        ;119.17
        sub       edi, edx                                      ;119.17
        cmp       esi, DWORD PTR [-928+ebp]                     ;118.14
        mov       BYTE PTR [9+edi+ecx], al                      ;119.17
        jb        .B1.76        ; Prob 64%                      ;118.14
                                ; LOE esi
.B1.77:                         ; Preds .B1.76
        mov       eax, DWORD PTR [-980+ebp]                     ;
        lea       ecx, DWORD PTR [1+esi+esi]                    ;118.14
                                ; LOE eax ecx
.B1.78:                         ; Preds .B1.77 .B1.618
        lea       edx, DWORD PTR [-1+ecx]                       ;118.14
        cmp       edx, DWORD PTR [-984+ebp]                     ;118.14
        jae       .B1.616       ; Prob 0%                       ;118.14
                                ; LOE eax ecx
.B1.79:                         ; Preds .B1.78
        mov       esi, DWORD PTR [-988+ebp]                     ;119.17
        inc       eax                                           ;119.17
        mov       edi, DWORD PTR [-996+ebp]                     ;119.17
        imul      esi, DWORD PTR [16+esi+edi], 152              ;119.17
        mov       edi, DWORD PTR [-960+ebp]                     ;119.17
        mov       edx, DWORD PTR [68+esi+edi]                   ;119.17
        neg       edx                                           ;119.17
        add       edx, ecx                                      ;119.17
        imul      edx, DWORD PTR [64+esi+edi]                   ;119.17
        mov       ecx, DWORD PTR [36+esi+edi]                   ;119.17
        mov       edi, DWORD PTR [-968+ebp]                     ;119.17
        mov       esi, DWORD PTR [ecx+edx]                      ;119.17
        mov       ecx, DWORD PTR [-1004+ebp]                    ;119.17
        add       DWORD PTR [-920+ebp], ecx                     ;119.17
        lea       edx, DWORD PTR [esi*4]                        ;119.17
        shl       esi, 5                                        ;119.17
        sub       esi, edx                                      ;119.17
        add       esi, DWORD PTR [-992+ebp]                     ;119.17
        mov       BYTE PTR [9+esi+edi], 0                       ;119.17
        add       edi, ecx                                      ;119.17
        mov       DWORD PTR [-968+ebp], edi                     ;119.17
        cmp       eax, DWORD PTR [-1000+ebp]                    ;119.17
        jb        .B1.74        ; Prob 82%                      ;119.17
        jmp       .B1.604       ; Prob 100%                     ;119.17
                                ; LOE eax
.B1.80:                         ; Preds .B1.25
        mov       edi, edx                                      ;
        xor       ecx, ecx                                      ;
        shr       edi, 31                                       ;
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE] ;41.6
        add       edi, edx                                      ;
        sar       edi, 1                                        ;
        mov       eax, DWORD PTR [-1108+ebp]                    ;
        mov       DWORD PTR [-1100+ebp], esi                    ;
        mov       DWORD PTR [-1104+ebp], ecx                    ;
        mov       DWORD PTR [-1092+ebp], edx                    ;
                                ; LOE eax edi
.B1.81:                         ; Preds .B1.86 .B1.648 .B1.80
        test      edi, edi                                      ;41.6
        jbe       .B1.650       ; Prob 10%                      ;41.6
                                ; LOE eax edi
.B1.82:                         ; Preds .B1.81
        mov       ecx, DWORD PTR [-1108+ebp]                    ;
        xor       edx, edx                                      ;
        neg       ecx                                           ;
        xor       esi, esi                                      ;
        add       ecx, eax                                      ;
        imul      ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+40] ;
        add       ecx, DWORD PTR [-1100+ebp]                    ;
        mov       DWORD PTR [-1096+ebp], eax                    ;
        mov       eax, 1                                        ;
                                ; LOE eax edx ecx esi edi
.B1.83:                         ; Preds .B1.83 .B1.82
        inc       esi                                           ;41.6
        mov       BYTE PTR [8+edx+ecx], al                      ;41.6
        mov       BYTE PTR [36+edx+ecx], al                     ;41.6
        add       edx, 56                                       ;41.6
        cmp       esi, edi                                      ;41.6
        jb        .B1.83        ; Prob 64%                      ;41.6
                                ; LOE eax edx ecx esi edi
.B1.84:                         ; Preds .B1.83
        mov       eax, DWORD PTR [-1096+ebp]                    ;
        lea       edx, DWORD PTR [1+esi+esi]                    ;41.6
                                ; LOE eax edx edi
.B1.85:                         ; Preds .B1.84 .B1.650
        lea       ecx, DWORD PTR [-1+edx]                       ;41.6
        cmp       ecx, DWORD PTR [-1092+ebp]                    ;41.6
        jae       .B1.648       ; Prob 10%                      ;41.6
                                ; LOE eax edx edi
.B1.86:                         ; Preds .B1.85
        mov       esi, eax                                      ;41.6
        lea       ecx, DWORD PTR [edx*4]                        ;41.6
        sub       esi, DWORD PTR [-1108+ebp]                    ;41.6
        inc       eax                                           ;41.6
        shl       edx, 5                                        ;41.6
        imul      esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+40] ;41.6
        sub       edx, ecx                                      ;41.6
        add       edx, DWORD PTR [-1100+ebp]                    ;41.6
        mov       BYTE PTR [-20+edx+esi], 1                     ;41.6
        mov       edx, DWORD PTR [-1104+ebp]                    ;41.6
        inc       edx                                           ;41.6
        mov       DWORD PTR [-1104+ebp], edx                    ;41.6
        cmp       edx, DWORD PTR [-1112+ebp]                    ;41.6
        jb        .B1.81        ; Prob 82%                      ;41.6
        jmp       .B1.26        ; Prob 100%                     ;41.6
                                ; LOE eax edi
.B1.87:                         ; Preds .B1.24
        mov       edi, edx                                      ;
        xor       ecx, ecx                                      ;
        shr       edi, 31                                       ;
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE] ;40.6
        add       edi, edx                                      ;
        sar       edi, 1                                        ;
        mov       DWORD PTR [-1100+ebp], esi                    ;
        mov       DWORD PTR [-1104+ebp], ecx                    ;
        mov       DWORD PTR [-1092+ebp], edx                    ;
                                ; LOE eax edi
.B1.88:                         ; Preds .B1.93 .B1.670 .B1.87
        test      edi, edi                                      ;40.6
        jbe       .B1.672       ; Prob 10%                      ;40.6
                                ; LOE eax edi
.B1.89:                         ; Preds .B1.88
        mov       ecx, DWORD PTR [-1108+ebp]                    ;
        xor       edx, edx                                      ;
        neg       ecx                                           ;
        xor       esi, esi                                      ;
        add       ecx, eax                                      ;
        imul      ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+40] ;
        add       ecx, DWORD PTR [-1100+ebp]                    ;
        mov       DWORD PTR [-1096+ebp], eax                    ;
        mov       eax, 1                                        ;
                                ; LOE eax edx ecx esi edi
.B1.90:                         ; Preds .B1.90 .B1.89
        inc       esi                                           ;40.6
        mov       BYTE PTR [9+edx+ecx], al                      ;40.6
        mov       BYTE PTR [37+edx+ecx], al                     ;40.6
        add       edx, 56                                       ;40.6
        cmp       esi, edi                                      ;40.6
        jb        .B1.90        ; Prob 64%                      ;40.6
                                ; LOE eax edx ecx esi edi
.B1.91:                         ; Preds .B1.90
        mov       eax, DWORD PTR [-1096+ebp]                    ;
        lea       edx, DWORD PTR [1+esi+esi]                    ;40.6
                                ; LOE eax edx edi
.B1.92:                         ; Preds .B1.91 .B1.672
        lea       ecx, DWORD PTR [-1+edx]                       ;40.6
        cmp       ecx, DWORD PTR [-1092+ebp]                    ;40.6
        jae       .B1.670       ; Prob 10%                      ;40.6
                                ; LOE eax edx edi
.B1.93:                         ; Preds .B1.92
        mov       esi, eax                                      ;40.6
        lea       ecx, DWORD PTR [edx*4]                        ;40.6
        sub       esi, DWORD PTR [-1108+ebp]                    ;40.6
        inc       eax                                           ;40.6
        shl       edx, 5                                        ;40.6
        imul      esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+40] ;40.6
        sub       edx, ecx                                      ;40.6
        add       edx, DWORD PTR [-1100+ebp]                    ;40.6
        mov       BYTE PTR [-19+edx+esi], 1                     ;40.6
        mov       edx, DWORD PTR [-1104+ebp]                    ;40.6
        inc       edx                                           ;40.6
        mov       DWORD PTR [-1104+ebp], edx                    ;40.6
        cmp       edx, DWORD PTR [-1112+ebp]                    ;40.6
        jb        .B1.88        ; Prob 82%                      ;40.6
                                ; LOE eax edi
.B1.94:                         ; Preds .B1.93                  ; Infreq
        mov       edx, DWORD PTR [-1092+ebp]                    ;
        jmp       .B1.25        ; Prob 100%                     ;
                                ; LOE edx
.B1.95:                         ; Preds .B1.17                  ; Infreq
        mov       eax, 1                                        ;
        jmp       .B1.21        ; Prob 100%                     ;
                                ; LOE eax ecx esi edi
.B1.96:                         ; Preds .B1.41                  ; Infreq
        cmp       DWORD PTR [-1092+ebp], 0                      ;56.14
        jle       .B1.42        ; Prob 50%                      ;56.14
                                ; LOE eax edx esi
.B1.97:                         ; Preds .B1.96                  ; Infreq
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE] ;57.20
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM] ;57.20
        mov       DWORD PTR [-1084+ebp], ecx                    ;57.20
        mov       DWORD PTR [-1100+ebp], edi                    ;57.20
        cmp       DWORD PTR [-1088+ebp], 0                      ;56.14
        jbe       .B1.108       ; Prob 3%                       ;56.14
                                ; LOE eax edx esi
.B1.98:                         ; Preds .B1.97                  ; Infreq
        imul      edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE+32], 84 ;
        xor       ecx, ecx                                      ;
        mov       DWORD PTR [-1108+ebp], esi                    ;
        xor       esi, esi                                      ;
        mov       ecx, DWORD PTR [-1084+ebp]                    ;
        sub       ecx, edi                                      ;
        mov       DWORD PTR [-1064+ebp], ecx                    ;
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM+32] ;
        shl       ecx, 2                                        ;
        neg       ecx                                           ;
        mov       DWORD PTR [-1052+ebp], edi                    ;
        add       ecx, DWORD PTR [-1100+ebp]                    ;
        mov       edi, DWORD PTR [176+edx+eax]                  ;57.20
        mov       DWORD PTR [-1076+ebp], esi                    ;
        mov       DWORD PTR [-1072+ebp], esi                    ;
        mov       DWORD PTR [-1068+ebp], edi                    ;57.20
        mov       DWORD PTR [-1080+ebp], ecx                    ;
        mov       DWORD PTR [-1096+ebp], edx                    ;
        mov       DWORD PTR [-1112+ebp], eax                    ;
                                ; LOE
.B1.99:                         ; Preds .B1.103 .B1.98          ; Infreq
        mov       ecx, DWORD PTR [-1064+ebp]                    ;57.20
        mov       edx, DWORD PTR [-1072+ebp]                    ;57.20
        mov       edi, DWORD PTR [-1080+ebp]                    ;57.55
        mov       esi, DWORD PTR [-1068+ebp]                    ;57.55
        mov       eax, DWORD PTR [116+edx+ecx]                  ;57.20
        shl       eax, 2                                        ;57.55
        neg       eax                                           ;57.55
        mov       ecx, DWORD PTR [84+edx+ecx]                   ;57.20
        mov       edx, DWORD PTR [4+eax+ecx]                    ;57.58
        cmp       esi, DWORD PTR [edi+edx*4]                    ;57.55
        jne       .B1.101       ; Prob 50%                      ;57.55
                                ; LOE eax ecx esi edi
.B1.100:                        ; Preds .B1.99                  ; Infreq
        mov       DWORD PTR [8+eax+ecx], 5                      ;58.8
                                ; LOE esi edi
.B1.101:                        ; Preds .B1.99 .B1.100          ; Infreq
        mov       eax, DWORD PTR [-1084+ebp]                    ;57.55
        mov       edx, DWORD PTR [-1072+ebp]                    ;57.55
        lea       ecx, DWORD PTR [eax+edx]                      ;57.55
        sub       ecx, DWORD PTR [-1052+ebp]                    ;57.55
        mov       eax, DWORD PTR [200+ecx]                      ;57.20
        shl       eax, 2                                        ;57.55
        neg       eax                                           ;57.55
        mov       ecx, DWORD PTR [168+ecx]                      ;57.20
        mov       edx, DWORD PTR [4+eax+ecx]                    ;57.58
        cmp       esi, DWORD PTR [edi+edx*4]                    ;57.55
        jne       .B1.103       ; Prob 50%                      ;57.55
                                ; LOE eax ecx
.B1.102:                        ; Preds .B1.101                 ; Infreq
        mov       DWORD PTR [8+eax+ecx], 5                      ;58.8
                                ; LOE
.B1.103:                        ; Preds .B1.101 .B1.102         ; Infreq
        mov       edx, DWORD PTR [-1076+ebp]                    ;56.14
        inc       edx                                           ;56.14
        mov       eax, DWORD PTR [-1072+ebp]                    ;56.14
        add       eax, 168                                      ;56.14
        mov       DWORD PTR [-1072+ebp], eax                    ;56.14
        mov       DWORD PTR [-1076+ebp], edx                    ;56.14
        cmp       edx, DWORD PTR [-1088+ebp]                    ;56.14
        jb        .B1.99        ; Prob 64%                      ;56.14
                                ; LOE edx dl dh
.B1.104:                        ; Preds .B1.103                 ; Infreq
        mov       ecx, edx                                      ;56.14
        mov       edx, DWORD PTR [-1096+ebp]                    ;
        mov       eax, DWORD PTR [-1112+ebp]                    ;
        mov       esi, DWORD PTR [-1108+ebp]                    ;
        lea       ecx, DWORD PTR [1+ecx+ecx]                    ;56.14
                                ; LOE eax edx ecx esi
.B1.105:                        ; Preds .B1.104 .B1.108         ; Infreq
        lea       edi, DWORD PTR [-1+ecx]                       ;56.14
        cmp       edi, DWORD PTR [-1092+ebp]                    ;56.14
        jae       .B1.42        ; Prob 3%                       ;56.14
                                ; LOE eax edx ecx esi
.B1.106:                        ; Preds .B1.105                 ; Infreq
        sub       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE+32] ;57.55
        imul      ecx, ecx, 84                                  ;57.55
        mov       edi, DWORD PTR [-1084+ebp]                    ;57.20
        mov       DWORD PTR [-1108+ebp], esi                    ;
        mov       DWORD PTR [-1096+ebp], edx                    ;
        mov       esi, DWORD PTR [32+edi+ecx]                   ;57.20
        shl       esi, 2                                        ;57.55
        neg       esi                                           ;57.55
        mov       ecx, DWORD PTR [edi+ecx]                      ;57.20
        mov       edi, DWORD PTR [176+edx+eax]                  ;57.20
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM+32] ;57.55
        neg       edx                                           ;57.55
        mov       DWORD PTR [-1104+ebp], esi                    ;57.55
        add       edx, DWORD PTR [4+esi+ecx]                    ;57.55
        mov       esi, DWORD PTR [-1100+ebp]                    ;57.55
        cmp       edi, DWORD PTR [esi+edx*4]                    ;57.55
        mov       esi, DWORD PTR [-1108+ebp]                    ;57.55
        mov       edx, DWORD PTR [-1096+ebp]                    ;57.55
        jne       .B1.42        ; Prob 50%                      ;57.55
                                ; LOE eax edx ecx esi dl cl dh ch
.B1.107:                        ; Preds .B1.106                 ; Infreq
        mov       edi, ecx                                      ;58.8
        mov       ecx, DWORD PTR [-1104+ebp]                    ;58.8
        mov       DWORD PTR [8+ecx+edi], 5                      ;58.8
        jmp       .B1.42        ; Prob 100%                     ;58.8
                                ; LOE eax edx esi
.B1.108:                        ; Preds .B1.97                  ; Infreq
        mov       ecx, 1                                        ;
        jmp       .B1.105       ; Prob 100%                     ;
                                ; LOE eax edx ecx esi
.B1.110:                        ; Preds .B1.72 .B1.68           ; Infreq
        mov       eax, DWORD PTR [-1044+ebp]                    ;124.52
        and       eax, -5                                       ;124.52
        cmp       eax, 2                                        ;124.52
        jne       .B1.112       ; Prob 84%                      ;124.52
                                ; LOE
.B1.111:                        ; Preds .B1.110                 ; Infreq
        inc       DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGNCOUNT]  ;125.11
                                ; LOE
.B1.112:                        ; Preds .B1.621 .B1.111 .B1.110 ; Infreq
        mov       edx, DWORD PTR [-1016+ebp]                    ;74.9
        inc       edx                                           ;74.9
        mov       eax, DWORD PTR [-1052+ebp]                    ;74.9
        add       eax, 84                                       ;74.9
        mov       DWORD PTR [-1052+ebp], eax                    ;74.9
        mov       DWORD PTR [-1016+ebp], edx                    ;74.9
        cmp       edx, DWORD PTR [-1020+ebp]                    ;74.9
        jle       .B1.51        ; Prob 82%                      ;74.9
                                ; LOE
.B1.113:                        ; Preds .B1.112                 ; Infreq
        mov       ecx, DWORD PTR [-736+ebp]                     ;
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFNODES] ;129.6
                                ; LOE eax ecx
.B1.114:                        ; Preds .B1.49 .B1.113          ; Infreq
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_KGPOINT+32] ;129.6
        xor       edx, edx                                      ;
        neg       edi                                           ;129.6
        add       edi, eax                                      ;129.6
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_KGPOINT] ;129.6
        mov       DWORD PTR [4+esi+edi*4], ecx                  ;129.6
        dec       ecx                                           ;131.6
        test      ecx, ecx                                      ;135.12
        jle       .B1.262       ; Prob 16%                      ;135.12
                                ; LOE edx ecx
.B1.115:                        ; Preds .B1.114                 ; Infreq
        mov       esi, DWORD PTR [-1060+ebp]                    ;
        mov       eax, 1                                        ;
        neg       esi                                           ;
        add       esi, DWORD PTR [-1028+ebp]                    ;
        pxor      xmm1, xmm1                                    ;144.9
        mov       edi, DWORD PTR [-1048+ebp]                    ;
        mov       DWORD PTR [-1108+ebp], 0                      ;
        mov       DWORD PTR [-1056+ebp], eax                    ;
        movss     xmm0, DWORD PTR [_2il0floatpacket.0]          ;217.44
        lea       esi, DWORD PTR [esi+edi*8]                    ;
        mov       DWORD PTR [-1112+ebp], esi                    ;
        mov       DWORD PTR [-732+ebp], edx                     ;217.44
        mov       DWORD PTR [-1052+ebp], eax                    ;217.44
        mov       DWORD PTR [-736+ebp], ecx                     ;217.44
                                ; LOE
.B1.116:                        ; Preds .B1.115 .B1.260         ; Infreq
        push      32                                            ;142.9
        mov       DWORD PTR [-232+ebp], 0                       ;142.9
        lea       edx, DWORD PTR [-440+ebp]                     ;142.9
        push      edx                                           ;142.9
        push      OFFSET FLAT: __STRLITPACK_169.0.1             ;142.9
        push      -2088435965                                   ;142.9
        push      44                                            ;142.9
        lea       ecx, DWORD PTR [-232+ebp]                     ;142.9
        push      ecx                                           ;142.9
        lea       eax, DWORD PTR [-308+ebp]                     ;142.9
        mov       DWORD PTR [-440+ebp], eax                     ;142.9
        call      _for_read_seq_lis                             ;142.9
                                ; LOE eax
.B1.737:                        ; Preds .B1.116                 ; Infreq
        add       esp, 24                                       ;142.9
                                ; LOE eax
.B1.117:                        ; Preds .B1.737                 ; Infreq
        test      eax, eax                                      ;142.9
        je        .B1.119       ; Prob 50%                      ;142.9
                                ; LOE eax
.B1.118:                        ; Preds .B1.117                 ; Infreq
        mov       edx, DWORD PTR [-1056+ebp]                    ;
        lea       ecx, DWORD PTR [edx*4]                        ;
        mov       DWORD PTR [-636+ebp], ecx                     ;
        jmp       .B1.121       ; Prob 100%                     ;
                                ; LOE eax
.B1.119:                        ; Preds .B1.117                 ; Infreq
        mov       esi, DWORD PTR [-1056+ebp]                    ;142.9
        mov       eax, 1                                        ;142.9
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_NSIGN+40] ;142.39
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_NSIGN] ;142.9
        mov       DWORD PTR [-880+ebp], eax                     ;142.9
        lea       edi, DWORD PTR [esi*4]                        ;142.9
        mov       DWORD PTR [-636+ebp], edi                     ;142.9
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_NSIGN+44] ;142.39
        imul      edi, ecx                                      ;142.9
        sub       edx, edi                                      ;142.9
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_NSIGN+32] ;142.9
        shl       edi, 2                                        ;142.9
        neg       edi                                           ;142.9
        add       edi, ecx                                      ;142.9
        add       edx, edi                                      ;142.9
        mov       DWORD PTR [-872+ebp], eax                     ;142.9
        lea       eax, DWORD PTR [-880+ebp]                     ;142.9
        push      eax                                           ;142.9
        mov       DWORD PTR [-864+ebp], ecx                     ;142.9
        lea       ecx, DWORD PTR [-232+ebp]                     ;142.9
        push      OFFSET FLAT: __STRLITPACK_170.0.1             ;142.9
        push      ecx                                           ;142.9
        mov       DWORD PTR [-164+ebp], 10                      ;142.9
        lea       edx, DWORD PTR [edx+esi*4]                    ;142.9
        mov       DWORD PTR [-876+ebp], edx                     ;142.9
        mov       DWORD PTR [-868+ebp], 9                       ;142.9
        call      _for_read_seq_lis_xmit                        ;142.9
                                ; LOE eax
.B1.738:                        ; Preds .B1.119                 ; Infreq
        add       esp, 12                                       ;142.9
                                ; LOE eax
.B1.121:                        ; Preds .B1.738 .B1.118         ; Infreq
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_NSIGN+40] ;146.3
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_NSIGN+44] ;146.3
        pxor      xmm0, xmm0                                    ;144.9
        imul      esi, edi                                      ;146.3
        movdqa    XMMWORD PTR [_READ_INTERSECTION_CONTROLS$TMPSIGDATA.0.1], xmm0 ;144.9
        movdqa    XMMWORD PTR [_READ_INTERSECTION_CONTROLS$TMPSIGDATA.0.1+16], xmm0 ;144.9
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;142.9
        mov       eax, esi                                      ;146.3
        mov       ecx, DWORD PTR [-636+ebp]                     ;146.3
        sub       eax, edi                                      ;146.3
        mov       edx, DWORD PTR [-308+ebp]                     ;145.3
        neg       eax                                           ;146.3
        add       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_NSIGN] ;146.3
        mov       DWORD PTR [_READ_INTERSECTION_CONTROLS$TMPSIGDATA.0.1], edx ;145.3
        add       eax, ecx                                      ;146.3
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_NSIGN+32] ;146.3
        shl       edx, 2                                        ;146.3
        sub       eax, edx                                      ;146.3
        push      OFFSET FLAT: __NLITPACK_0.0.1                 ;147.8
        push      OFFSET FLAT: _READ_INTERSECTION_CONTROLS$TMPSIGDATA.0.1 ;147.8
        mov       eax, DWORD PTR [eax]                          ;146.3
        mov       DWORD PTR [_READ_INTERSECTION_CONTROLS$TMPSIGDATA.0.1+4], eax ;146.3
        lea       eax, DWORD PTR [edi+edi]                      ;146.3
        neg       eax                                           ;146.3
        add       eax, esi                                      ;146.3
        neg       eax                                           ;146.3
        add       eax, ecx                                      ;146.3
        sub       eax, edx                                      ;146.3
        mov       eax, DWORD PTR [eax]                          ;146.3
        mov       DWORD PTR [_READ_INTERSECTION_CONTROLS$TMPSIGDATA.0.1+8], eax ;146.3
        lea       eax, DWORD PTR [edi+edi*2]                    ;146.3
        neg       eax                                           ;146.3
        add       eax, esi                                      ;146.3
        neg       eax                                           ;146.3
        add       eax, ecx                                      ;146.3
        sub       eax, edx                                      ;146.3
        mov       eax, DWORD PTR [eax]                          ;146.3
        mov       DWORD PTR [_READ_INTERSECTION_CONTROLS$TMPSIGDATA.0.1+12], eax ;146.3
        lea       eax, DWORD PTR [edi*4]                        ;146.3
        neg       eax                                           ;146.3
        add       eax, esi                                      ;146.3
        neg       eax                                           ;146.3
        add       eax, ecx                                      ;146.3
        sub       eax, edx                                      ;146.3
        mov       eax, DWORD PTR [eax]                          ;146.3
        mov       DWORD PTR [_READ_INTERSECTION_CONTROLS$TMPSIGDATA.0.1+16], eax ;146.3
        lea       eax, DWORD PTR [edi+edi*4]                    ;146.3
        mov       DWORD PTR [-748+ebp], eax                     ;146.3
        neg       eax                                           ;146.3
        add       eax, esi                                      ;146.3
        neg       eax                                           ;146.3
        add       eax, ecx                                      ;146.3
        sub       eax, edx                                      ;146.3
        mov       eax, DWORD PTR [eax]                          ;146.3
        mov       DWORD PTR [_READ_INTERSECTION_CONTROLS$TMPSIGDATA.0.1+20], eax ;146.3
        mov       eax, DWORD PTR [-748+ebp]                     ;146.3
        add       eax, edi                                      ;146.3
        neg       eax                                           ;146.3
        add       eax, esi                                      ;146.3
        neg       eax                                           ;146.3
        add       eax, ecx                                      ;146.3
        sub       eax, edx                                      ;146.3
        mov       eax, DWORD PTR [eax]                          ;146.3
        mov       DWORD PTR [_READ_INTERSECTION_CONTROLS$TMPSIGDATA.0.1+24], eax ;146.3
        lea       eax, DWORD PTR [edi*8]                        ;146.3
        sub       edi, eax                                      ;146.3
        neg       eax                                           ;146.3
        add       eax, esi                                      ;146.3
        add       edi, esi                                      ;146.3
        neg       eax                                           ;146.3
        neg       edi                                           ;146.3
        add       eax, ecx                                      ;146.3
        add       edi, ecx                                      ;146.3
        sub       eax, edx                                      ;146.3
        sub       edi, edx                                      ;146.3
        mov       eax, DWORD PTR [eax]                          ;146.3
        mov       DWORD PTR [_READ_INTERSECTION_CONTROLS$TMPSIGDATA.0.1+32], eax ;146.3
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_NSIGN+40] ;146.3
        mov       edi, DWORD PTR [edi]                          ;146.3
        mov       DWORD PTR [_READ_INTERSECTION_CONTROLS$TMPSIGDATA.0.1+28], edi ;146.3
        lea       edi, DWORD PTR [eax+eax*8]                    ;146.3
        neg       edi                                           ;146.3
        add       esi, edi                                      ;146.3
        sub       ecx, esi                                      ;146.3
        sub       ecx, edx                                      ;146.3
        mov       ecx, DWORD PTR [ecx]                          ;146.3
        mov       DWORD PTR [_READ_INTERSECTION_CONTROLS$TMPSIGDATA.0.1+36], ecx ;146.3
        call      _DYNUST_SIGNAL_MODULE_mp_INSERTSIGNALPHASE    ;147.8
                                ; LOE
.B1.739:                        ; Preds .B1.121                 ; Infreq
        add       esp, 8                                        ;147.8
                                ; LOE
.B1.122:                        ; Preds .B1.739                 ; Infreq
        push      OFFSET FLAT: __NLITPACK_0.0.1                 ;148.8
        push      OFFSET FLAT: _READ_INTERSECTION_CONTROLS$TMPSIGDATA.0.1 ;148.8
        call      _DYNUST_SIGNAL_MODULE_mp_DECLAREPHASEMOVE     ;148.8
                                ; LOE
.B1.740:                        ; Preds .B1.122                 ; Infreq
        add       esp, 8                                        ;148.8
                                ; LOE
.B1.123:                        ; Preds .B1.740                 ; Infreq
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], 0   ;150.12
        jne       .B1.602       ; Prob 5%                       ;150.12
                                ; LOE
.B1.124:                        ; Preds .B1.899 .B1.123         ; Infreq
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM+32] ;155.6
        shl       eax, 2                                        ;155.30
        neg       eax                                           ;155.30
        add       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM] ;155.30
        mov       edx, DWORD PTR [-308+ebp]                     ;155.9
        mov       esi, DWORD PTR [-1028+ebp]                    ;155.30
        mov       edi, DWORD PTR [-1052+ebp]                    ;155.30
        mov       ecx, DWORD PTR [eax+edx*4]                    ;155.9
        mov       DWORD PTR [-1060+ebp], edx                    ;155.9
        mov       DWORD PTR [-812+ebp], eax                     ;155.30
        cmp       ecx, DWORD PTR [-4+esi+edi*4]                 ;155.30
        jne       .B1.126       ; Prob 50%                      ;155.30
                                ; LOE edi
.B1.125:                        ; Preds .B1.124                 ; Infreq
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_NSIGN] ;172.8
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_NSIGN+44] ;172.12
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_NSIGN+32] ;172.8
        mov       DWORD PTR [-656+ebp], edi                     ;172.8
        inc       DWORD PTR [-1108+ebp]                         ;156.8
        mov       DWORD PTR [-756+ebp], eax                     ;172.12
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_NSIGN+40] ;172.12
        mov       DWORD PTR [-660+ebp], edx                     ;172.8
        jmp       .B1.140       ; Prob 100%                     ;172.8
                                ; LOE edi
.B1.126:                        ; Preds .B1.124                 ; Infreq
        mov       eax, DWORD PTR [-1112+ebp]                    ;158.22
        mov       edx, edi                                      ;158.22
        mov       ecx, DWORD PTR [-4+eax+edx*4]                 ;158.22
        mov       DWORD PTR [-1064+ebp], ecx                    ;158.22
        cmp       ecx, DWORD PTR [-1108+ebp]                    ;158.19
        je        .B1.138       ; Prob 62%                      ;158.19
                                ; LOE
.B1.127:                        ; Preds .B1.126                 ; Infreq
        push      32                                            ;159.4
        mov       DWORD PTR [-232+ebp], 0                       ;159.4
        lea       eax, DWORD PTR [-928+ebp]                     ;159.4
        push      eax                                           ;159.4
        push      OFFSET FLAT: __STRLITPACK_173.0.1             ;159.4
        push      -2088435968                                   ;159.4
        push      911                                           ;159.4
        mov       DWORD PTR [-928+ebp], 25                      ;159.4
        lea       edx, DWORD PTR [-232+ebp]                     ;159.4
        push      edx                                           ;159.4
        mov       DWORD PTR [-924+ebp], OFFSET FLAT: __STRLITPACK_125 ;159.4
        call      _for_write_seq_lis                            ;159.4
                                ; LOE
.B1.741:                        ; Preds .B1.127                 ; Infreq
        add       esp, 24                                       ;159.4
                                ; LOE
.B1.128:                        ; Preds .B1.741                 ; Infreq
        push      32                                            ;160.13
        mov       DWORD PTR [-232+ebp], 0                       ;160.13
        lea       eax, DWORD PTR [-920+ebp]                     ;160.13
        push      eax                                           ;160.13
        push      OFFSET FLAT: __STRLITPACK_174.0.1             ;160.13
        push      -2088435968                                   ;160.13
        push      911                                           ;160.13
        mov       DWORD PTR [-920+ebp], 4                       ;160.13
        lea       edx, DWORD PTR [-232+ebp]                     ;160.13
        push      edx                                           ;160.13
        mov       DWORD PTR [-916+ebp], OFFSET FLAT: __STRLITPACK_122 ;160.13
        call      _for_write_seq_lis                            ;160.13
                                ; LOE
.B1.742:                        ; Preds .B1.128                 ; Infreq
        add       esp, 24                                       ;160.13
                                ; LOE
.B1.129:                        ; Preds .B1.742                 ; Infreq
        mov       eax, DWORD PTR [-1060+ebp]                    ;160.13
        lea       edx, DWORD PTR [-784+ebp]                     ;160.13
        push      edx                                           ;160.13
        push      OFFSET FLAT: __STRLITPACK_175.0.1             ;160.13
        mov       DWORD PTR [-784+ebp], eax                     ;160.13
        lea       ecx, DWORD PTR [-232+ebp]                     ;160.13
        push      ecx                                           ;160.13
        call      _for_write_seq_lis_xmit                       ;160.13
                                ; LOE
.B1.743:                        ; Preds .B1.129                 ; Infreq
        add       esp, 12                                       ;160.13
                                ; LOE
.B1.130:                        ; Preds .B1.743                 ; Infreq
        mov       DWORD PTR [-912+ebp], 5                       ;160.13
        lea       eax, DWORD PTR [-912+ebp]                     ;160.13
        push      eax                                           ;160.13
        push      OFFSET FLAT: __STRLITPACK_176.0.1             ;160.13
        mov       DWORD PTR [-908+ebp], OFFSET FLAT: __STRLITPACK_121 ;160.13
        lea       edx, DWORD PTR [-232+ebp]                     ;160.13
        push      edx                                           ;160.13
        call      _for_write_seq_lis_xmit                       ;160.13
                                ; LOE
.B1.744:                        ; Preds .B1.130                 ; Infreq
        add       esp, 12                                       ;160.13
                                ; LOE
.B1.131:                        ; Preds .B1.744                 ; Infreq
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_NSIGN+44] ;160.48
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_NSIGN+40] ;160.48
        mov       DWORD PTR [-756+ebp], edx                     ;160.48
        imul      edx, edi                                      ;160.13
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_NSIGN] ;160.13
        sub       edx, edi                                      ;160.13
        mov       esi, DWORD PTR [-636+ebp]                     ;160.13
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_NSIGN+32] ;160.13
        mov       DWORD PTR [-656+ebp], eax                     ;160.13
        mov       DWORD PTR [-660+ebp], ecx                     ;160.13
        add       eax, esi                                      ;160.13
        sub       eax, edx                                      ;160.13
        shl       ecx, 2                                        ;160.13
        sub       eax, ecx                                      ;160.13
        lea       edx, DWORD PTR [-232+ebp]                     ;160.13
        mov       esi, DWORD PTR [eax]                          ;160.13
        lea       eax, DWORD PTR [-776+ebp]                     ;160.13
        push      eax                                           ;160.13
        push      OFFSET FLAT: __STRLITPACK_177.0.1             ;160.13
        push      edx                                           ;160.13
        mov       DWORD PTR [-776+ebp], esi                     ;160.13
        call      _for_write_seq_lis_xmit                       ;160.13
                                ; LOE edi
.B1.745:                        ; Preds .B1.131                 ; Infreq
        add       esp, 12                                       ;160.13
                                ; LOE edi
.B1.132:                        ; Preds .B1.745                 ; Infreq
        push      32                                            ;161.10
        mov       DWORD PTR [-232+ebp], 0                       ;161.10
        lea       eax, DWORD PTR [-904+ebp]                     ;161.10
        push      eax                                           ;161.10
        push      OFFSET FLAT: __STRLITPACK_178.0.1             ;161.10
        push      -2088435968                                   ;161.10
        push      911                                           ;161.10
        mov       DWORD PTR [-904+ebp], 31                      ;161.10
        lea       edx, DWORD PTR [-232+ebp]                     ;161.10
        push      edx                                           ;161.10
        mov       DWORD PTR [-900+ebp], OFFSET FLAT: __STRLITPACK_119 ;161.10
        call      _for_write_seq_lis                            ;161.10
                                ; LOE edi
.B1.746:                        ; Preds .B1.132                 ; Infreq
        add       esp, 24                                       ;161.10
                                ; LOE edi
.B1.133:                        ; Preds .B1.746                 ; Infreq
        mov       eax, DWORD PTR [-1108+ebp]                    ;161.10
        lea       edx, DWORD PTR [-768+ebp]                     ;161.10
        push      edx                                           ;161.10
        push      OFFSET FLAT: __STRLITPACK_179.0.1             ;161.10
        mov       DWORD PTR [-768+ebp], eax                     ;161.10
        lea       ecx, DWORD PTR [-232+ebp]                     ;161.10
        push      ecx                                           ;161.10
        call      _for_write_seq_lis_xmit                       ;161.10
                                ; LOE edi
.B1.747:                        ; Preds .B1.133                 ; Infreq
        add       esp, 12                                       ;161.10
                                ; LOE edi
.B1.134:                        ; Preds .B1.747                 ; Infreq
        mov       eax, DWORD PTR [-1064+ebp]                    ;161.10
        lea       edx, DWORD PTR [-760+ebp]                     ;161.10
        push      edx                                           ;161.10
        push      OFFSET FLAT: __STRLITPACK_180.0.1             ;161.10
        mov       DWORD PTR [-760+ebp], eax                     ;161.10
        lea       ecx, DWORD PTR [-232+ebp]                     ;161.10
        push      ecx                                           ;161.10
        call      _for_write_seq_lis_xmit                       ;161.10
                                ; LOE edi
.B1.748:                        ; Preds .B1.134                 ; Infreq
        add       esp, 12                                       ;161.10
                                ; LOE edi
.B1.135:                        ; Preds .B1.748                 ; Infreq
        push      32                                            ;162.4
        mov       DWORD PTR [-232+ebp], 0                       ;162.4
        lea       eax, DWORD PTR [-896+ebp]                     ;162.4
        push      eax                                           ;162.4
        push      OFFSET FLAT: __STRLITPACK_181.0.1             ;162.4
        push      -2088435968                                   ;162.4
        push      911                                           ;162.4
        mov       DWORD PTR [-896+ebp], 61                      ;162.4
        lea       edx, DWORD PTR [-232+ebp]                     ;162.4
        push      edx                                           ;162.4
        mov       DWORD PTR [-892+ebp], OFFSET FLAT: __STRLITPACK_117 ;162.4
        call      _for_write_seq_lis                            ;162.4
                                ; LOE edi
.B1.749:                        ; Preds .B1.135                 ; Infreq
        add       esp, 24                                       ;162.4
                                ; LOE edi
.B1.136:                        ; Preds .B1.749                 ; Infreq
        push      32                                            ;163.4
        mov       DWORD PTR [-232+ebp], 0                       ;163.4
        lea       eax, DWORD PTR [-888+ebp]                     ;163.4
        push      eax                                           ;163.4
        push      OFFSET FLAT: __STRLITPACK_182.0.1             ;163.4
        push      -2088435968                                   ;163.4
        push      911                                           ;163.4
        mov       DWORD PTR [-888+ebp], 37                      ;163.4
        lea       edx, DWORD PTR [-232+ebp]                     ;163.4
        push      edx                                           ;163.4
        mov       DWORD PTR [-884+ebp], OFFSET FLAT: __STRLITPACK_115 ;163.4
        call      _for_write_seq_lis                            ;163.4
                                ; LOE edi
.B1.750:                        ; Preds .B1.136                 ; Infreq
        add       esp, 24                                       ;163.4
                                ; LOE edi
.B1.137:                        ; Preds .B1.750                 ; Infreq
        push      32                                            ;164.4
        xor       eax, eax                                      ;164.4
        push      eax                                           ;164.4
        push      eax                                           ;164.4
        push      -2088435968                                   ;164.4
        push      eax                                           ;164.4
        push      OFFSET FLAT: __STRLITPACK_183                 ;164.4
        call      _for_stop_core                                ;164.4
                                ; LOE edi
.B1.751:                        ; Preds .B1.137                 ; Infreq
        add       esp, 24                                       ;164.4
        jmp       .B1.139       ; Prob 100%                     ;164.4
                                ; LOE edi
.B1.138:                        ; Preds .B1.126                 ; Infreq
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_NSIGN] ;172.8
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_NSIGN+44] ;172.12
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_NSIGN+32] ;172.8
        mov       DWORD PTR [-656+ebp], edi                     ;172.8
        mov       DWORD PTR [-756+ebp], eax                     ;172.12
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_NSIGN+40] ;172.12
        mov       DWORD PTR [-660+ebp], edx                     ;172.8
                                ; LOE edi
.B1.139:                        ; Preds .B1.751 .B1.138         ; Infreq
        mov       eax, 1                                        ;
        inc       DWORD PTR [-1052+ebp]                         ;167.8
        mov       DWORD PTR [-1108+ebp], eax                    ;
                                ; LOE edi
.B1.140:                        ; Preds .B1.125 .B1.139         ; Infreq
        mov       ecx, DWORD PTR [-636+ebp]                     ;172.12
        mov       eax, DWORD PTR [-656+ebp]                     ;172.12
        mov       esi, DWORD PTR [-660+ebp]                     ;172.12
        mov       DWORD PTR [-744+ebp], edi                     ;
        lea       edx, DWORD PTR [ecx+eax]                      ;172.12
        mov       eax, DWORD PTR [-756+ebp]                     ;172.12
        imul      eax, edi                                      ;172.12
        lea       ecx, DWORD PTR [esi*4]                        ;172.12
        mov       DWORD PTR [-836+ebp], edx                     ;172.12
        lea       esi, DWORD PTR [edi+edi*4]                    ;172.12
        mov       DWORD PTR [-796+ebp], esi                     ;172.12
        add       esi, edi                                      ;172.12
        neg       esi                                           ;172.12
        add       esi, eax                                      ;172.12
        neg       esi                                           ;172.12
        add       esi, edx                                      ;172.12
        sub       esi, ecx                                      ;172.12
        mov       DWORD PTR [-1044+ebp], esi                    ;172.12
        mov       DWORD PTR [-844+ebp], eax                     ;172.12
        mov       DWORD PTR [-860+ebp], ecx                     ;172.12
        mov       esi, DWORD PTR [esi]                          ;172.12
        mov       DWORD PTR [-820+ebp], esi                     ;172.12
        lea       esi, DWORD PTR [edi*8]                        ;172.12
        sub       edi, esi                                      ;172.12
        neg       esi                                           ;172.12
        add       edi, eax                                      ;172.12
        mov       eax, 1                                        ;173.10
        neg       edi                                           ;172.12
        add       edi, edx                                      ;172.12
        sub       edi, ecx                                      ;172.12
        mov       ecx, eax                                      ;173.10
        mov       DWORD PTR [-828+ebp], edi                     ;172.12
        mov       edx, DWORD PTR [edi]                          ;172.12
        mov       edi, DWORD PTR [-820+ebp]                     ;173.10
        test      edi, edi                                      ;173.10
        mov       DWORD PTR [-804+ebp], edx                     ;172.12
        cmove     ecx, edi                                      ;173.10
        test      edx, edx                                      ;173.10
        mov       edi, eax                                      ;173.10
        cmove     edi, edx                                      ;173.10
        add       ecx, edi                                      ;173.10
        mov       edi, DWORD PTR [-844+ebp]                     ;172.12
        add       esi, edi                                      ;172.12
        neg       esi                                           ;172.12
        add       esi, DWORD PTR [-836+ebp]                     ;172.12
        mov       edx, DWORD PTR [-860+ebp]                     ;172.12
        sub       esi, edx                                      ;172.12
        mov       DWORD PTR [-752+ebp], esi                     ;172.12
        mov       esi, DWORD PTR [esi]                          ;172.12
        test      esi, esi                                      ;173.10
        cmovne    esi, eax                                      ;173.10
        add       ecx, esi                                      ;173.10
        mov       esi, DWORD PTR [-744+ebp]                     ;172.12
        lea       esi, DWORD PTR [esi+esi*8]                    ;172.12
        neg       esi                                           ;172.12
        add       esi, edi                                      ;172.12
        neg       esi                                           ;172.12
        mov       edi, DWORD PTR [-836+ebp]                     ;172.12
        add       esi, edi                                      ;172.12
        sub       esi, edx                                      ;172.12
        mov       DWORD PTR [-852+ebp], esi                     ;172.12
        mov       esi, DWORD PTR [esi]                          ;172.12
        test      esi, esi                                      ;173.10
        cmovne    esi, eax                                      ;173.10
        mov       eax, DWORD PTR [-796+ebp]                     ;176.19
        add       ecx, esi                                      ;173.10
        neg       eax                                           ;176.19
        add       eax, DWORD PTR [-844+ebp]                     ;176.16
        neg       eax                                           ;176.16
        add       eax, edi                                      ;176.16
        sub       eax, edx                                      ;176.16
        mov       edi, DWORD PTR [-744+ebp]                     ;176.16
        mov       DWORD PTR [-1048+ebp], ecx                    ;173.10
        mov       DWORD PTR [-796+ebp], eax                     ;176.16
        mov       edx, DWORD PTR [eax]                          ;176.19
        cmp       ecx, edx                                      ;176.16
        mov       DWORD PTR [-1020+ebp], edx                    ;176.19
        je        .B1.151       ; Prob 50%                      ;176.16
                                ; LOE edi
.B1.141:                        ; Preds .B1.140                 ; Infreq
        mov       eax, 32                                       ;177.5
        lea       edx, DWORD PTR [-720+ebp]                     ;177.5
        push      eax                                           ;177.5
        push      edx                                           ;177.5
        push      OFFSET FLAT: __STRLITPACK_184.0.1             ;177.5
        push      -2088435968                                   ;177.5
        push      911                                           ;177.5
        mov       DWORD PTR [-232+ebp], 0                       ;177.5
        lea       ecx, DWORD PTR [-232+ebp]                     ;177.5
        push      ecx                                           ;177.5
        mov       DWORD PTR [-720+ebp], eax                     ;177.5
        mov       DWORD PTR [-716+ebp], OFFSET FLAT: __STRLITPACK_113 ;177.5
        call      _for_write_seq_lis                            ;177.5
                                ; LOE edi
.B1.752:                        ; Preds .B1.141                 ; Infreq
        add       esp, 24                                       ;177.5
                                ; LOE edi
.B1.142:                        ; Preds .B1.752                 ; Infreq
        mov       eax, DWORD PTR [-1060+ebp]                    ;177.5
        lea       edx, DWORD PTR [-616+ebp]                     ;177.5
        push      edx                                           ;177.5
        push      OFFSET FLAT: __STRLITPACK_185.0.1             ;177.5
        mov       DWORD PTR [-616+ebp], eax                     ;177.5
        lea       ecx, DWORD PTR [-232+ebp]                     ;177.5
        push      ecx                                           ;177.5
        call      _for_write_seq_lis_xmit                       ;177.5
                                ; LOE edi
.B1.753:                        ; Preds .B1.142                 ; Infreq
        add       esp, 12                                       ;177.5
                                ; LOE edi
.B1.143:                        ; Preds .B1.753                 ; Infreq
        push      32                                            ;178.11
        mov       DWORD PTR [-232+ebp], 0                       ;178.11
        lea       eax, DWORD PTR [-712+ebp]                     ;178.11
        push      eax                                           ;178.11
        push      OFFSET FLAT: __STRLITPACK_186.0.1             ;178.11
        push      -2088435968                                   ;178.11
        push      911                                           ;178.11
        mov       DWORD PTR [-712+ebp], 26                      ;178.11
        lea       edx, DWORD PTR [-232+ebp]                     ;178.11
        push      edx                                           ;178.11
        mov       DWORD PTR [-708+ebp], OFFSET FLAT: __STRLITPACK_111 ;178.11
        call      _for_write_seq_lis                            ;178.11
                                ; LOE edi
.B1.754:                        ; Preds .B1.143                 ; Infreq
        add       esp, 24                                       ;178.11
                                ; LOE edi
.B1.144:                        ; Preds .B1.754                 ; Infreq
        push      32                                            ;179.8
        mov       DWORD PTR [-232+ebp], 0                       ;179.8
        lea       eax, DWORD PTR [-704+ebp]                     ;179.8
        push      eax                                           ;179.8
        push      OFFSET FLAT: __STRLITPACK_187.0.1             ;179.8
        push      -2088435968                                   ;179.8
        push      911                                           ;179.8
        mov       DWORD PTR [-704+ebp], 5                       ;179.8
        lea       edx, DWORD PTR [-232+ebp]                     ;179.8
        push      edx                                           ;179.8
        mov       DWORD PTR [-700+ebp], OFFSET FLAT: __STRLITPACK_109 ;179.8
        call      _for_write_seq_lis                            ;179.8
                                ; LOE edi
.B1.755:                        ; Preds .B1.144                 ; Infreq
        add       esp, 24                                       ;179.8
                                ; LOE edi
.B1.145:                        ; Preds .B1.755                 ; Infreq
        mov       eax, DWORD PTR [-844+ebp]                     ;179.8
        lea       ecx, DWORD PTR [-608+ebp]                     ;179.8
        sub       eax, edi                                      ;179.8
        lea       esi, DWORD PTR [-232+ebp]                     ;179.8
        neg       eax                                           ;179.8
        add       eax, DWORD PTR [-836+ebp]                     ;179.8
        sub       eax, DWORD PTR [-860+ebp]                     ;179.8
        push      ecx                                           ;179.8
        push      OFFSET FLAT: __STRLITPACK_188.0.1             ;179.8
        mov       edx, DWORD PTR [eax]                          ;179.8
        push      esi                                           ;179.8
        mov       DWORD PTR [-608+ebp], edx                     ;179.8
        call      _for_write_seq_lis_xmit                       ;179.8
                                ; LOE edi
.B1.756:                        ; Preds .B1.145                 ; Infreq
        add       esp, 12                                       ;179.8
                                ; LOE edi
.B1.146:                        ; Preds .B1.756                 ; Infreq
        push      32                                            ;180.5
        mov       DWORD PTR [-232+ebp], 0                       ;180.5
        lea       eax, DWORD PTR [-696+ebp]                     ;180.5
        push      eax                                           ;180.5
        push      OFFSET FLAT: __STRLITPACK_189.0.1             ;180.5
        push      -2088435968                                   ;180.5
        push      911                                           ;180.5
        mov       DWORD PTR [-696+ebp], 8                       ;180.5
        lea       edx, DWORD PTR [-232+ebp]                     ;180.5
        push      edx                                           ;180.5
        mov       DWORD PTR [-692+ebp], OFFSET FLAT: __STRLITPACK_106 ;180.5
        call      _for_write_seq_lis                            ;180.5
                                ; LOE edi
.B1.757:                        ; Preds .B1.146                 ; Infreq
        add       esp, 24                                       ;180.5
                                ; LOE edi
.B1.147:                        ; Preds .B1.757                 ; Infreq
        mov       eax, DWORD PTR [-1020+ebp]                    ;180.5
        lea       edx, DWORD PTR [-600+ebp]                     ;180.5
        push      edx                                           ;180.5
        push      OFFSET FLAT: __STRLITPACK_190.0.1             ;180.5
        mov       DWORD PTR [-600+ebp], eax                     ;180.5
        lea       ecx, DWORD PTR [-232+ebp]                     ;180.5
        push      ecx                                           ;180.5
        call      _for_write_seq_lis_xmit                       ;180.5
                                ; LOE edi
.B1.758:                        ; Preds .B1.147                 ; Infreq
        add       esp, 12                                       ;180.5
                                ; LOE edi
.B1.148:                        ; Preds .B1.758                 ; Infreq
        mov       DWORD PTR [-688+ebp], 10                      ;180.5
        lea       eax, DWORD PTR [-688+ebp]                     ;180.5
        push      eax                                           ;180.5
        push      OFFSET FLAT: __STRLITPACK_191.0.1             ;180.5
        mov       DWORD PTR [-684+ebp], OFFSET FLAT: __STRLITPACK_105 ;180.5
        lea       edx, DWORD PTR [-232+ebp]                     ;180.5
        push      edx                                           ;180.5
        call      _for_write_seq_lis_xmit                       ;180.5
                                ; LOE edi
.B1.759:                        ; Preds .B1.148                 ; Infreq
        add       esp, 12                                       ;180.5
                                ; LOE edi
.B1.149:                        ; Preds .B1.759                 ; Infreq
        mov       eax, DWORD PTR [-1048+ebp]                    ;180.5
        lea       edx, DWORD PTR [-592+ebp]                     ;180.5
        push      edx                                           ;180.5
        push      OFFSET FLAT: __STRLITPACK_192.0.1             ;180.5
        mov       DWORD PTR [-592+ebp], eax                     ;180.5
        lea       ecx, DWORD PTR [-232+ebp]                     ;180.5
        push      ecx                                           ;180.5
        call      _for_write_seq_lis_xmit                       ;180.5
                                ; LOE edi
.B1.760:                        ; Preds .B1.149                 ; Infreq
        add       esp, 12                                       ;180.5
                                ; LOE edi
.B1.150:                        ; Preds .B1.760                 ; Infreq
        push      32                                            ;181.7
        xor       eax, eax                                      ;181.7
        push      eax                                           ;181.7
        push      eax                                           ;181.7
        push      -2088435968                                   ;181.7
        push      eax                                           ;181.7
        push      OFFSET FLAT: __STRLITPACK_193                 ;181.7
        call      _for_stop_core                                ;181.7
                                ; LOE edi
.B1.761:                        ; Preds .B1.150                 ; Infreq
        add       esp, 24                                       ;181.7
                                ; LOE edi
.B1.151:                        ; Preds .B1.761 .B1.140         ; Infreq
        cmp       DWORD PTR [-820+ebp], 0                       ;185.20
        jle       .B1.154       ; Prob 16%                      ;185.20
                                ; LOE edi
.B1.152:                        ; Preds .B1.151                 ; Infreq
        mov       edx, DWORD PTR [-820+ebp]                     ;186.5
        mov       eax, DWORD PTR [-812+ebp]                     ;186.5
        mov       ecx, DWORD PTR [-1044+ebp]                    ;186.5
        mov       esi, DWORD PTR [eax+edx*4]                    ;186.5
        test      esi, esi                                      ;187.25
        mov       DWORD PTR [ecx], esi                          ;186.5
        jle       .B1.596       ; Prob 16%                      ;187.25
                                ; LOE edi
.B1.153:                        ; Preds .B1.897 .B1.152         ; Infreq
        mov       eax, DWORD PTR [-828+ebp]                     ;192.9
        mov       edx, DWORD PTR [eax]                          ;192.9
        mov       DWORD PTR [-804+ebp], edx                     ;192.9
                                ; LOE edi
.B1.154:                        ; Preds .B1.153 .B1.151         ; Infreq
        cmp       DWORD PTR [-804+ebp], 0                       ;192.20
        jle       .B1.156       ; Prob 16%                      ;192.20
                                ; LOE edi
.B1.155:                        ; Preds .B1.154                 ; Infreq
        mov       eax, DWORD PTR [-812+ebp]                     ;193.5
        mov       edx, DWORD PTR [-804+ebp]                     ;193.5
        mov       ecx, DWORD PTR [-828+ebp]                     ;193.5
        mov       esi, DWORD PTR [eax+edx*4]                    ;193.5
        test      esi, esi                                      ;194.25
        mov       DWORD PTR [ecx], esi                          ;193.5
        jle       .B1.591       ; Prob 16%                      ;194.25
                                ; LOE edi
.B1.156:                        ; Preds .B1.892 .B1.154 .B1.155 ; Infreq
        mov       eax, DWORD PTR [-752+ebp]                     ;199.10
        mov       edx, DWORD PTR [eax]                          ;199.10
        test      edx, edx                                      ;199.21
        jle       .B1.158       ; Prob 16%                      ;199.21
                                ; LOE edx edi
.B1.157:                        ; Preds .B1.156                 ; Infreq
        mov       eax, DWORD PTR [-812+ebp]                     ;200.5
        mov       ecx, DWORD PTR [eax+edx*4]                    ;200.5
        test      ecx, ecx                                      ;201.25
        mov       edx, DWORD PTR [-752+ebp]                     ;200.5
        mov       DWORD PTR [edx], ecx                          ;200.5
        jle       .B1.586       ; Prob 16%                      ;201.25
                                ; LOE edi
.B1.158:                        ; Preds .B1.887 .B1.156 .B1.157 ; Infreq
        mov       eax, DWORD PTR [-852+ebp]                     ;207.9
        mov       edx, DWORD PTR [eax]                          ;207.9
        test      edx, edx                                      ;207.20
        jle       .B1.160       ; Prob 16%                      ;207.20
                                ; LOE edx edi
.B1.159:                        ; Preds .B1.158                 ; Infreq
        mov       eax, DWORD PTR [-812+ebp]                     ;208.5
        mov       ecx, DWORD PTR [eax+edx*4]                    ;208.5
        test      ecx, ecx                                      ;209.25
        mov       edx, DWORD PTR [-852+ebp]                     ;208.5
        mov       DWORD PTR [edx], ecx                          ;208.5
        jle       .B1.581       ; Prob 16%                      ;209.25
                                ; LOE edi
.B1.160:                        ; Preds .B1.882 .B1.158 .B1.159 ; Infreq
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ISIGCOUNT] ;215.11
        cmp       edx, 1                                        ;215.24
        jne       .B1.162       ; Prob 37%                      ;215.24
                                ; LOE edx edi
.B1.161:                        ; Preds .B1.160                 ; Infreq
        sub       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_STRTSIG+32] ;217.15
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_STRTSIG] ;217.15
        mov       esi, DWORD PTR [-844+ebp]                     ;217.15
        mov       DWORD PTR [-744+ebp], edi                     ;
        movss     xmm0, DWORD PTR [eax+edx*4]                   ;217.26
        lea       edx, DWORD PTR [edi*8]                        ;217.15
        mulss     xmm0, DWORD PTR [_2il0floatpacket.0]          ;217.44
        lea       ecx, DWORD PTR [edx+edi*4]                    ;217.15
        neg       ecx                                           ;217.15
        add       ecx, esi                                      ;217.15
        cvttss2si eax, xmm0                                     ;217.15
        neg       ecx                                           ;217.15
        add       ecx, DWORD PTR [-836+ebp]                     ;217.15
        mov       edx, DWORD PTR [-860+ebp]                     ;217.15
        sub       ecx, edx                                      ;217.15
        mov       DWORD PTR [-164+ebp], 15                      ;218.13
        mov       DWORD PTR [ecx], eax                          ;217.15
        imul      ecx, edi, -13                                 ;217.15
        imul      edi, edi, -14                                 ;217.15
        add       ecx, esi                                      ;217.15
        neg       ecx                                           ;217.15
        mov       esi, DWORD PTR [-836+ebp]                     ;217.15
        add       ecx, esi                                      ;217.15
        sub       ecx, edx                                      ;217.15
        mov       DWORD PTR [ecx], eax                          ;217.15
        mov       ecx, DWORD PTR [-844+ebp]                     ;217.15
        add       ecx, edi                                      ;217.15
        sub       esi, ecx                                      ;217.15
        sub       esi, edx                                      ;217.15
        mov       edi, DWORD PTR [-744+ebp]                     ;218.13
        mov       DWORD PTR [esi], eax                          ;217.15
                                ; LOE edi
.B1.162:                        ; Preds .B1.160 .B1.161         ; Infreq
        mov       eax, DWORD PTR [-796+ebp]                     ;221.16
        mov       eax, DWORD PTR [eax]                          ;221.16
        lea       ecx, DWORD PTR [5+eax]                        ;221.8
        cmp       ecx, 6                                        ;221.8
        jl        .B1.184       ; Prob 10%                      ;221.8
                                ; LOE eax ecx edi
.B1.163:                        ; Preds .B1.162                 ; Infreq
        mov       edx, 6                                        ;
        mov       DWORD PTR [-652+ebp], edx                     ;
        mov       DWORD PTR [-664+ebp], ecx                     ;
        mov       esi, DWORD PTR [-756+ebp]                     ;
                                ; LOE esi edi
.B1.164:                        ; Preds .B1.182 .B1.163         ; Infreq
        neg       esi                                           ;222.7
        mov       eax, DWORD PTR [-660+ebp]                     ;222.7
        mov       edx, DWORD PTR [-656+ebp]                     ;222.7
        shl       eax, 2                                        ;222.7
        add       esi, DWORD PTR [-652+ebp]                     ;222.7
        neg       eax                                           ;222.7
        add       edx, DWORD PTR [-636+ebp]                     ;222.7
        imul      esi, edi                                      ;222.7
        add       edx, eax                                      ;222.7
        mov       edi, DWORD PTR [-308+ebp]                     ;223.10
        mov       ecx, DWORD PTR [edx+esi]                      ;222.7
        lea       edx, DWORD PTR [-252+ebp]                     ;223.10
        sub       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM+32] ;223.10
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM] ;223.10
        push      OFFSET FLAT: __NLITPACK_1.0.1                 ;223.10
        mov       DWORD PTR [-252+ebp], ecx                     ;222.7
        lea       eax, DWORD PTR [esi+edi*4]                    ;223.10
        push      eax                                           ;223.10
        push      edx                                           ;223.10
        call      _DYNUST_NETWORK_MODULE_mp_GETFLINKFROMNODE    ;223.10
                                ; LOE eax
.B1.762:                        ; Preds .B1.164                 ; Infreq
        add       esp, 12                                       ;223.10
                                ; LOE eax
.B1.165:                        ; Preds .B1.762                 ; Infreq
        test      eax, eax                                      ;223.57
        jne       .B1.180       ; Prob 22%                      ;223.57
                                ; LOE
.B1.166:                        ; Preds .B1.165                 ; Infreq
        push      32                                            ;224.9
        mov       DWORD PTR [-232+ebp], 0                       ;224.9
        lea       eax, DWORD PTR [-360+ebp]                     ;224.9
        push      eax                                           ;224.9
        push      OFFSET FLAT: __STRLITPACK_214.0.1             ;224.9
        push      -2088435968                                   ;224.9
        push      911                                           ;224.9
        mov       DWORD PTR [-360+ebp], 39                      ;224.9
        lea       edx, DWORD PTR [-232+ebp]                     ;224.9
        push      edx                                           ;224.9
        mov       DWORD PTR [-356+ebp], OFFSET FLAT: __STRLITPACK_87 ;224.9
        call      _for_write_seq_lis                            ;224.9
                                ; LOE
.B1.763:                        ; Preds .B1.166                 ; Infreq
        add       esp, 24                                       ;224.9
                                ; LOE
.B1.167:                        ; Preds .B1.763                 ; Infreq
        push      32                                            ;225.6
        mov       DWORD PTR [-232+ebp], 0                       ;225.6
        lea       eax, DWORD PTR [-352+ebp]                     ;225.6
        push      eax                                           ;225.6
        push      OFFSET FLAT: __STRLITPACK_215.0.1             ;225.6
        push      -2088435968                                   ;225.6
        push      911                                           ;225.6
        mov       DWORD PTR [-352+ebp], 7                       ;225.6
        lea       edx, DWORD PTR [-232+ebp]                     ;225.6
        push      edx                                           ;225.6
        mov       DWORD PTR [-348+ebp], OFFSET FLAT: __STRLITPACK_84 ;225.6
        call      _for_write_seq_lis                            ;225.6
                                ; LOE
.B1.764:                        ; Preds .B1.167                 ; Infreq
        add       esp, 24                                       ;225.6
                                ; LOE
.B1.168:                        ; Preds .B1.764                 ; Infreq
        imul      ecx, DWORD PTR [-252+ebp], 44                 ;225.29
        lea       edi, DWORD PTR [-200+ebp]                     ;225.6
        imul      edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE+32], 44 ;225.6
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE] ;225.6
        add       ecx, eax                                      ;225.6
        sub       ecx, edx                                      ;225.6
        push      edi                                           ;225.6
        mov       DWORD PTR [-668+ebp], eax                     ;225.6
        lea       eax, DWORD PTR [-232+ebp]                     ;225.6
        push      OFFSET FLAT: __STRLITPACK_216.0.1             ;225.6
        mov       esi, DWORD PTR [36+ecx]                       ;225.6
        push      eax                                           ;225.6
        mov       DWORD PTR [-724+ebp], edx                     ;225.6
        mov       DWORD PTR [-200+ebp], esi                     ;225.6
        call      _for_write_seq_lis_xmit                       ;225.6
                                ; LOE
.B1.765:                        ; Preds .B1.168                 ; Infreq
        add       esp, 12                                       ;225.6
                                ; LOE
.B1.169:                        ; Preds .B1.765                 ; Infreq
        mov       DWORD PTR [-344+ebp], 11                      ;225.6
        lea       eax, DWORD PTR [-344+ebp]                     ;225.6
        push      eax                                           ;225.6
        push      OFFSET FLAT: __STRLITPACK_217.0.1             ;225.6
        mov       DWORD PTR [-340+ebp], OFFSET FLAT: __STRLITPACK_83 ;225.6
        lea       edx, DWORD PTR [-232+ebp]                     ;225.6
        push      edx                                           ;225.6
        call      _for_write_seq_lis_xmit                       ;225.6
                                ; LOE
.B1.766:                        ; Preds .B1.169                 ; Infreq
        add       esp, 12                                       ;225.6
                                ; LOE
.B1.170:                        ; Preds .B1.766                 ; Infreq
        mov       esi, DWORD PTR [-308+ebp]                     ;225.6
        lea       eax, DWORD PTR [-192+ebp]                     ;225.6
        push      eax                                           ;225.6
        push      OFFSET FLAT: __STRLITPACK_218.0.1             ;225.6
        mov       DWORD PTR [-192+ebp], esi                     ;225.6
        lea       edx, DWORD PTR [-232+ebp]                     ;225.6
        push      edx                                           ;225.6
        call      _for_write_seq_lis_xmit                       ;225.6
                                ; LOE esi
.B1.767:                        ; Preds .B1.170                 ; Infreq
        add       esp, 12                                       ;225.6
                                ; LOE esi
.B1.171:                        ; Preds .B1.767                 ; Infreq
        push      32                                            ;226.9
        push      OFFSET FLAT: READ_INTERSECTION_CONTROLS$format_pack.0.1+96 ;226.9
        mov       DWORD PTR [-232+ebp], 0                       ;226.9
        lea       eax, DWORD PTR [-184+ebp]                     ;226.9
        push      eax                                           ;226.9
        push      OFFSET FLAT: __STRLITPACK_219.0.1             ;226.9
        push      -2088435968                                   ;226.9
        push      911                                           ;226.9
        mov       DWORD PTR [-184+ebp], esi                     ;226.9
        lea       edx, DWORD PTR [-232+ebp]                     ;226.9
        push      edx                                           ;226.9
        call      _for_write_seq_fmt                            ;226.9
                                ; LOE
.B1.768:                        ; Preds .B1.171                 ; Infreq
        add       esp, 28                                       ;226.9
                                ; LOE
.B1.172:                        ; Preds .B1.768                 ; Infreq
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_NSIGN+40] ;226.57
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_NSIGN+44] ;226.9
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_NSIGN+32] ;226.9
        imul      edx, edi                                      ;226.9
        mov       DWORD PTR [-660+ebp], esi                     ;226.9
        lea       eax, DWORD PTR [esi*4]                        ;226.9
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_NSIGN] ;226.9
        mov       esi, DWORD PTR [-636+ebp]                     ;226.9
        mov       DWORD PTR [-676+ebp], edx                     ;226.9
        sub       edx, edi                                      ;226.9
        mov       DWORD PTR [-656+ebp], ecx                     ;226.9
        neg       edx                                           ;226.9
        mov       DWORD PTR [-680+ebp], eax                     ;226.9
        add       ecx, esi                                      ;226.9
        add       edx, ecx                                      ;226.9
        sub       edx, eax                                      ;226.9
        mov       DWORD PTR [-672+ebp], ecx                     ;226.9
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_NSIGN+44] ;226.9
        mov       eax, DWORD PTR [edx]                          ;226.9
        lea       edx, DWORD PTR [-232+ebp]                     ;226.9
        mov       DWORD PTR [-176+ebp], eax                     ;226.9
        lea       eax, DWORD PTR [-176+ebp]                     ;226.9
        push      eax                                           ;226.9
        push      OFFSET FLAT: __STRLITPACK_220.0.1             ;226.9
        push      edx                                           ;226.9
        call      _for_write_seq_fmt_xmit                       ;226.9
                                ; LOE esi edi
.B1.769:                        ; Preds .B1.172                 ; Infreq
        add       esp, 12                                       ;226.9
                                ; LOE esi edi
.B1.173:                        ; Preds .B1.769                 ; Infreq
        lea       eax, DWORD PTR [edi+edi*4]                    ;227.12
        add       eax, edi                                      ;227.12
        neg       eax                                           ;227.12
        add       eax, DWORD PTR [-676+ebp]                     ;227.23
        neg       eax                                           ;227.23
        add       eax, DWORD PTR [-672+ebp]                     ;227.23
        sub       eax, DWORD PTR [-680+ebp]                     ;227.23
        mov       eax, DWORD PTR [eax]                          ;227.12
        test      eax, eax                                      ;227.23
        jle       .B1.175       ; Prob 16%                      ;227.23
                                ; LOE eax esi edi
.B1.174:                        ; Preds .B1.173                 ; Infreq
        imul      eax, eax, 44                                  ;227.64
        mov       edx, DWORD PTR [-668+ebp]                     ;227.28
        sub       edx, DWORD PTR [-724+ebp]                     ;227.28
        push      32                                            ;227.28
        push      OFFSET FLAT: READ_INTERSECTION_CONTROLS$format_pack.0.1+156 ;227.28
        mov       ecx, DWORD PTR [36+eax+edx]                   ;227.28
        lea       eax, DWORD PTR [-256+ebp]                     ;227.28
        push      eax                                           ;227.28
        push      OFFSET FLAT: __STRLITPACK_221.0.1             ;227.28
        push      -2088435968                                   ;227.28
        push      911                                           ;227.28
        mov       DWORD PTR [-232+ebp], 0                       ;227.28
        lea       edx, DWORD PTR [-232+ebp]                     ;227.28
        push      edx                                           ;227.28
        mov       DWORD PTR [-256+ebp], ecx                     ;227.28
        call      _for_write_seq_fmt                            ;227.28
                                ; LOE esi edi
.B1.770:                        ; Preds .B1.174                 ; Infreq
        add       esp, 28                                       ;227.28
                                ; LOE esi edi
.B1.175:                        ; Preds .B1.770 .B1.173         ; Infreq
        lea       eax, DWORD PTR [edi*8]                        ;228.17
        neg       eax                                           ;228.17
        add       eax, edi                                      ;228.17
        add       eax, DWORD PTR [-676+ebp]                     ;228.28
        neg       eax                                           ;228.28
        add       eax, DWORD PTR [-672+ebp]                     ;228.28
        sub       eax, DWORD PTR [-680+ebp]                     ;228.28
        mov       eax, DWORD PTR [eax]                          ;228.17
        test      eax, eax                                      ;228.28
        jle       .B1.177       ; Prob 16%                      ;228.28
                                ; LOE eax esi edi
.B1.176:                        ; Preds .B1.175                 ; Infreq
        imul      eax, eax, 44                                  ;228.69
        mov       edx, DWORD PTR [-668+ebp]                     ;228.33
        sub       edx, DWORD PTR [-724+ebp]                     ;228.33
        push      32                                            ;228.33
        push      OFFSET FLAT: READ_INTERSECTION_CONTROLS$format_pack.0.1+196 ;228.33
        mov       ecx, DWORD PTR [36+eax+edx]                   ;228.33
        lea       eax, DWORD PTR [-248+ebp]                     ;228.33
        push      eax                                           ;228.33
        push      OFFSET FLAT: __STRLITPACK_222.0.1             ;228.33
        push      -2088435968                                   ;228.33
        push      911                                           ;228.33
        mov       DWORD PTR [-232+ebp], 0                       ;228.33
        lea       edx, DWORD PTR [-232+ebp]                     ;228.33
        push      edx                                           ;228.33
        mov       DWORD PTR [-248+ebp], ecx                     ;228.33
        call      _for_write_seq_fmt                            ;228.33
                                ; LOE esi edi
.B1.771:                        ; Preds .B1.176                 ; Infreq
        add       esp, 28                                       ;228.33
                                ; LOE esi edi
.B1.177:                        ; Preds .B1.771 .B1.175         ; Infreq
        mov       edx, DWORD PTR [-676+ebp]                     ;229.28
        lea       eax, DWORD PTR [edi*8]                        ;229.17
        sub       edx, eax                                      ;229.28
        mov       ecx, DWORD PTR [-672+ebp]                     ;229.28
        sub       ecx, edx                                      ;229.28
        sub       ecx, DWORD PTR [-680+ebp]                     ;229.28
        mov       eax, DWORD PTR [ecx]                          ;229.17
        test      eax, eax                                      ;229.28
        jle       .B1.179       ; Prob 16%                      ;229.28
                                ; LOE eax esi edi
.B1.178:                        ; Preds .B1.177                 ; Infreq
        imul      eax, eax, 44                                  ;229.69
        mov       edx, DWORD PTR [-668+ebp]                     ;229.33
        sub       edx, DWORD PTR [-724+ebp]                     ;229.33
        push      32                                            ;229.33
        push      OFFSET FLAT: READ_INTERSECTION_CONTROLS$format_pack.0.1+236 ;229.33
        mov       ecx, DWORD PTR [36+eax+edx]                   ;229.33
        lea       eax, DWORD PTR [-240+ebp]                     ;229.33
        push      eax                                           ;229.33
        push      OFFSET FLAT: __STRLITPACK_223.0.1             ;229.33
        push      -2088435968                                   ;229.33
        push      911                                           ;229.33
        mov       DWORD PTR [-232+ebp], 0                       ;229.33
        lea       edx, DWORD PTR [-232+ebp]                     ;229.33
        push      edx                                           ;229.33
        mov       DWORD PTR [-240+ebp], ecx                     ;229.33
        call      _for_write_seq_fmt                            ;229.33
                                ; LOE esi edi
.B1.772:                        ; Preds .B1.178                 ; Infreq
        add       esp, 28                                       ;229.33
                                ; LOE esi edi
.B1.179:                        ; Preds .B1.772 .B1.177         ; Infreq
        push      32                                            ;230.11
        xor       eax, eax                                      ;230.11
        push      eax                                           ;230.11
        push      eax                                           ;230.11
        push      -2088435968                                   ;230.11
        push      eax                                           ;230.11
        push      OFFSET FLAT: __STRLITPACK_224                 ;230.11
        call      _for_stop_core                                ;230.11
                                ; LOE esi edi
.B1.773:                        ; Preds .B1.179                 ; Infreq
        add       esp, 24                                       ;230.11
        jmp       .B1.182       ; Prob 100%                     ;230.11
                                ; LOE esi edi
.B1.180:                        ; Preds .B1.165                 ; Infreq
        mov       edx, DWORD PTR [-308+ebp]                     ;232.28
        lea       esi, DWORD PTR [-252+ebp]                     ;232.28
        sub       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM+32] ;232.28
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM] ;232.28
        push      OFFSET FLAT: __NLITPACK_1.0.1                 ;232.28
        lea       ecx, DWORD PTR [eax+edx*4]                    ;232.28
        push      ecx                                           ;232.28
        push      esi                                           ;232.28
        call      _DYNUST_NETWORK_MODULE_mp_GETFLINKFROMNODE    ;232.28
                                ; LOE eax
.B1.774:                        ; Preds .B1.180                 ; Infreq
        mov       DWORD PTR [-740+ebp], eax                     ;232.28
        add       esp, 12                                       ;232.28
                                ; LOE
.B1.181:                        ; Preds .B1.774                 ; Infreq
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_NSIGN] ;232.28
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_NSIGN+32] ;232.28
        mov       edi, DWORD PTR [-636+ebp]                     ;232.14
        mov       ecx, DWORD PTR [-652+ebp]                     ;232.14
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_NSIGN+44] ;232.14
        sub       ecx, esi                                      ;232.14
        mov       DWORD PTR [-656+ebp], eax                     ;232.28
        add       eax, edi                                      ;232.14
        mov       DWORD PTR [-660+ebp], edx                     ;232.28
        shl       edx, 2                                        ;232.14
        imul      ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_NSIGN+40] ;232.14
        sub       eax, edx                                      ;232.14
        mov       edi, DWORD PTR [-740+ebp]                     ;232.14
        mov       DWORD PTR [eax+ecx], edi                      ;232.14
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_NSIGN+40] ;232.14
                                ; LOE esi edi
.B1.182:                        ; Preds .B1.773 .B1.181         ; Infreq
        mov       eax, DWORD PTR [-652+ebp]                     ;234.8
        inc       eax                                           ;234.8
        mov       DWORD PTR [-652+ebp], eax                     ;234.8
        cmp       eax, DWORD PTR [-664+ebp]                     ;234.8
        jle       .B1.164       ; Prob 82%                      ;234.8
                                ; LOE esi edi
.B1.183:                        ; Preds .B1.182                 ; Infreq
        mov       DWORD PTR [-756+ebp], esi                     ;
        mov       edx, esi                                      ;236.7
        neg       edx                                           ;236.7
        add       edx, 5                                        ;236.7
        imul      edx, edi                                      ;236.7
        mov       edi, DWORD PTR [-660+ebp]                     ;236.7
        mov       eax, DWORD PTR [-656+ebp]                     ;236.7
        shl       edi, 2                                        ;236.7
        add       eax, DWORD PTR [-636+ebp]                     ;236.7
        neg       edi                                           ;236.7
        add       eax, edi                                      ;236.7
        mov       eax, DWORD PTR [eax+edx]                      ;236.7
                                ; LOE eax
.B1.184:                        ; Preds .B1.183 .B1.162         ; Infreq
        mov       DWORD PTR [-156+ebp], 1                       ;236.7
        test      eax, eax                                      ;236.7
        jle       .B1.260       ; Prob 2%                       ;236.7
                                ; LOE eax
.B1.185:                        ; Preds .B1.184                 ; Infreq
        mov       DWORD PTR [-612+ebp], eax                     ;237.8
                                ; LOE
.B1.186:                        ; Preds .B1.258 .B1.185         ; Infreq
        push      32                                            ;237.8
        push      OFFSET FLAT: READ_INTERSECTION_CONTROLS$format_pack.0.1+76 ;237.8
        mov       DWORD PTR [-232+ebp], 0                       ;237.8
        lea       edx, DWORD PTR [-104+ebp]                     ;237.8
        push      edx                                           ;237.8
        push      OFFSET FLAT: __STRLITPACK_225.0.1             ;237.8
        push      -2088435968                                   ;237.8
        push      44                                            ;237.8
        lea       ecx, DWORD PTR [-232+ebp]                     ;237.8
        push      ecx                                           ;237.8
        lea       eax, DWORD PTR [-196+ebp]                     ;237.8
        mov       DWORD PTR [-104+ebp], eax                     ;237.8
        call      _for_read_seq_fmt                             ;237.8
                                ; LOE
.B1.775:                        ; Preds .B1.186                 ; Infreq
        add       esp, 28                                       ;237.8
                                ; LOE
.B1.187:                        ; Preds .B1.775                 ; Infreq
        lea       edx, DWORD PTR [-96+ebp]                      ;237.8
        push      edx                                           ;237.8
        push      OFFSET FLAT: __STRLITPACK_226.0.1             ;237.8
        lea       ecx, DWORD PTR [-232+ebp]                     ;237.8
        push      ecx                                           ;237.8
        lea       eax, DWORD PTR [-188+ebp]                     ;237.8
        mov       DWORD PTR [-96+ebp], eax                      ;237.8
        call      _for_read_seq_fmt_xmit                        ;237.8
                                ; LOE
.B1.776:                        ; Preds .B1.187                 ; Infreq
        add       esp, 12                                       ;237.8
                                ; LOE
.B1.188:                        ; Preds .B1.776                 ; Infreq
        lea       edx, DWORD PTR [-88+ebp]                      ;237.8
        push      edx                                           ;237.8
        push      OFFSET FLAT: __STRLITPACK_227.0.1             ;237.8
        lea       ecx, DWORD PTR [-232+ebp]                     ;237.8
        push      ecx                                           ;237.8
        lea       eax, DWORD PTR [-180+ebp]                     ;237.8
        mov       DWORD PTR [-88+ebp], eax                      ;237.8
        call      _for_read_seq_fmt_xmit                        ;237.8
                                ; LOE
.B1.777:                        ; Preds .B1.188                 ; Infreq
        add       esp, 12                                       ;237.8
                                ; LOE
.B1.189:                        ; Preds .B1.777                 ; Infreq
        lea       edx, DWORD PTR [-80+ebp]                      ;237.8
        push      edx                                           ;237.8
        push      OFFSET FLAT: __STRLITPACK_228.0.1             ;237.8
        lea       ecx, DWORD PTR [-232+ebp]                     ;237.8
        push      ecx                                           ;237.8
        lea       eax, DWORD PTR [-172+ebp]                     ;237.8
        mov       DWORD PTR [-80+ebp], eax                      ;237.8
        call      _for_read_seq_fmt_xmit                        ;237.8
                                ; LOE
.B1.778:                        ; Preds .B1.189                 ; Infreq
        add       esp, 12                                       ;237.8
                                ; LOE
.B1.190:                        ; Preds .B1.778                 ; Infreq
        mov       eax, 1                                        ;237.8
        xor       edx, edx                                      ;237.8
        mov       ecx, DWORD PTR [-172+ebp]                     ;237.8
        test      ecx, ecx                                      ;237.8
        mov       DWORD PTR [-336+ebp], eax                     ;237.8
        mov       DWORD PTR [-328+ebp], eax                     ;237.8
        lea       eax, DWORD PTR [-336+ebp]                     ;237.8
        mov       esi, DWORD PTR [-156+ebp]                     ;237.44
        cmovl     ecx, edx                                      ;237.8
        push      eax                                           ;237.8
        push      OFFSET FLAT: __STRLITPACK_229.0.1             ;237.8
        mov       DWORD PTR [-324+ebp], ecx                     ;237.8
        lea       edx, DWORD PTR [-232+ebp]                     ;237.8
        push      edx                                           ;237.8
        mov       DWORD PTR [-320+ebp], 40                      ;237.8
        lea       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ALMOV-4+esi*4] ;237.8
        mov       DWORD PTR [-332+ebp], edi                     ;237.8
        call      _for_read_seq_fmt_xmit                        ;237.8
                                ; LOE
.B1.779:                        ; Preds .B1.190                 ; Infreq
        add       esp, 12                                       ;237.8
                                ; LOE
.B1.191:                        ; Preds .B1.779                 ; Infreq
        mov       esi, DWORD PTR [-172+ebp]                     ;238.3
        test      esi, esi                                      ;238.11
        jle       .B1.576       ; Prob 16%                      ;238.11
                                ; LOE esi
.B1.192:                        ; Preds .B1.191                 ; Infreq
        mov       edi, DWORD PTR [-188+ebp]                     ;244.3
        mov       eax, DWORD PTR [-180+ebp]                     ;245.3
                                ; LOE eax esi edi
.B1.193:                        ; Preds .B1.580 .B1.192         ; Infreq
        mov       edx, DWORD PTR [-196+ebp]                     ;243.3
        mov       DWORD PTR [_READ_INTERSECTION_CONTROLS$TMPSIGDATA.0.1], edx ;243.3
        mov       DWORD PTR [_READ_INTERSECTION_CONTROLS$TMPSIGDATA.0.1+4], edi ;244.3
        mov       DWORD PTR [_READ_INTERSECTION_CONTROLS$TMPSIGDATA.0.1+8], eax ;245.3
        mov       DWORD PTR [_READ_INTERSECTION_CONTROLS$TMPSIGDATA.0.1+12], esi ;246.3
        jl        .B1.200       ; Prob 50%                      ;247.3
                                ; LOE esi
.B1.194:                        ; Preds .B1.193                 ; Infreq
        inc       esi                                           ;247.3
        mov       ecx, esi                                      ;247.3
        shr       ecx, 31                                       ;247.3
        add       ecx, esi                                      ;247.3
        sar       ecx, 1                                        ;247.3
        mov       edx, DWORD PTR [-156+ebp]                     ;247.3
        test      ecx, ecx                                      ;247.3
        jbe       .B1.448       ; Prob 10%                      ;247.3
                                ; LOE edx ecx esi
.B1.195:                        ; Preds .B1.194                 ; Infreq
        mov       DWORD PTR [-644+ebp], esi                     ;
        xor       eax, eax                                      ;
                                ; LOE eax edx ecx
.B1.196:                        ; Preds .B1.196 .B1.195         ; Infreq
        lea       edi, DWORD PTR [eax+eax*4]                    ;247.3
        shl       edi, 4                                        ;247.3
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ALMOV-4+edi+edx*4] ;247.3
        mov       DWORD PTR [_READ_INTERSECTION_CONTROLS$TMPSIGDATA.0.1+16+eax*8], esi ;247.3
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ALMOV+36+edi+edx*4] ;247.3
        mov       DWORD PTR [_READ_INTERSECTION_CONTROLS$TMPSIGDATA.0.1+20+eax*8], esi ;247.3
        inc       eax                                           ;247.3
        cmp       eax, ecx                                      ;247.3
        jb        .B1.196       ; Prob 79%                      ;247.3
                                ; LOE eax edx ecx
.B1.197:                        ; Preds .B1.196                 ; Infreq
        mov       esi, DWORD PTR [-644+ebp]                     ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;247.3
                                ; LOE eax edx esi
.B1.198:                        ; Preds .B1.197 .B1.448         ; Infreq
        lea       ecx, DWORD PTR [-1+eax]                       ;247.3
        cmp       esi, ecx                                      ;247.3
        jbe       .B1.200       ; Prob 10%                      ;247.3
                                ; LOE eax edx
.B1.199:                        ; Preds .B1.198                 ; Infreq
        lea       ecx, DWORD PTR [eax+eax*4]                    ;247.3
        shl       ecx, 3                                        ;247.3
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ALMOV-44+ecx+edx*4] ;247.3
        mov       DWORD PTR [_READ_INTERSECTION_CONTROLS$TMPSIGDATA.0.1+12+eax*4], edx ;247.3
                                ; LOE
.B1.200:                        ; Preds .B1.193 .B1.198 .B1.199 ; Infreq
        lea       eax, DWORD PTR [-156+ebp]                     ;248.8
        push      eax                                           ;248.8
        push      OFFSET FLAT: __NLITPACK_0.0.1                 ;248.8
        push      OFFSET FLAT: _READ_INTERSECTION_CONTROLS$TMPSIGDATA.0.1 ;248.8
        call      _DYNUST_SIGNAL_MODULE_mp_DECLAREOBLINK        ;248.8
                                ; LOE
.B1.780:                        ; Preds .B1.200                 ; Infreq
        add       esp, 12                                       ;248.8
                                ; LOE
.B1.201:                        ; Preds .B1.780                 ; Infreq
        lea       eax, DWORD PTR [-156+ebp]                     ;249.14
        push      eax                                           ;249.14
        push      OFFSET FLAT: __NLITPACK_0.0.1                 ;249.14
        push      OFFSET FLAT: _READ_INTERSECTION_CONTROLS$TMPSIGDATA.0.1 ;249.14
        call      _DYNUST_SIGNAL_MODULE_mp_INSERTPHASEMOVE      ;249.14
                                ; LOE
.B1.781:                        ; Preds .B1.201                 ; Infreq
        add       esp, 12                                       ;249.14
                                ; LOE
.B1.202:                        ; Preds .B1.781                 ; Infreq
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], 0   ;251.15
        je        .B1.211       ; Prob 50%                      ;251.15
                                ; LOE
.B1.203:                        ; Preds .B1.202                 ; Infreq
        push      32                                            ;252.10
        mov       DWORD PTR [-232+ebp], 0                       ;252.10
        lea       eax, DWORD PTR [-304+ebp]                     ;252.10
        push      eax                                           ;252.10
        push      OFFSET FLAT: __STRLITPACK_234.0.1             ;252.10
        push      -2088435968                                   ;252.10
        push      911                                           ;252.10
        mov       DWORD PTR [-304+ebp], 41                      ;252.10
        lea       edx, DWORD PTR [-232+ebp]                     ;252.10
        push      edx                                           ;252.10
        mov       DWORD PTR [-300+ebp], OFFSET FLAT: __STRLITPACK_69 ;252.10
        call      _for_write_seq_lis                            ;252.10
                                ; LOE
.B1.782:                        ; Preds .B1.203                 ; Infreq
        add       esp, 24                                       ;252.10
                                ; LOE
.B1.204:                        ; Preds .B1.782                 ; Infreq
        push      32                                            ;253.10
        mov       DWORD PTR [-232+ebp], 0                       ;253.10
        lea       eax, DWORD PTR [-296+ebp]                     ;253.10
        push      eax                                           ;253.10
        push      OFFSET FLAT: __STRLITPACK_235.0.1             ;253.10
        push      -2088435968                                   ;253.10
        push      911                                           ;253.10
        mov       DWORD PTR [-296+ebp], 66                      ;253.10
        lea       edx, DWORD PTR [-232+ebp]                     ;253.10
        push      edx                                           ;253.10
        mov       DWORD PTR [-292+ebp], OFFSET FLAT: __STRLITPACK_67 ;253.10
        call      _for_write_seq_lis                            ;253.10
                                ; LOE
.B1.783:                        ; Preds .B1.204                 ; Infreq
        add       esp, 24                                       ;253.10
                                ; LOE
.B1.205:                        ; Preds .B1.783                 ; Infreq
        push      32                                            ;254.4
        mov       eax, DWORD PTR [-196+ebp]                     ;254.4
        lea       edx, DWORD PTR [-168+ebp]                     ;254.4
        push      edx                                           ;254.4
        push      OFFSET FLAT: __STRLITPACK_236.0.1             ;254.4
        push      -2088435968                                   ;254.4
        push      911                                           ;254.4
        mov       DWORD PTR [-232+ebp], 0                       ;254.4
        lea       ecx, DWORD PTR [-232+ebp]                     ;254.4
        push      ecx                                           ;254.4
        mov       DWORD PTR [-584+ebp], eax                     ;254.4
        mov       DWORD PTR [-168+ebp], eax                     ;254.4
        call      _for_write_seq_lis                            ;254.4
                                ; LOE
.B1.784:                        ; Preds .B1.205                 ; Infreq
        add       esp, 24                                       ;254.4
                                ; LOE
.B1.206:                        ; Preds .B1.784                 ; Infreq
        mov       eax, DWORD PTR [-188+ebp]                     ;254.4
        lea       edx, DWORD PTR [-160+ebp]                     ;254.4
        push      edx                                           ;254.4
        push      OFFSET FLAT: __STRLITPACK_237.0.1             ;254.4
        mov       DWORD PTR [-560+ebp], eax                     ;254.4
        lea       ecx, DWORD PTR [-232+ebp]                     ;254.4
        push      ecx                                           ;254.4
        mov       DWORD PTR [-160+ebp], eax                     ;254.4
        call      _for_write_seq_lis_xmit                       ;254.4
                                ; LOE
.B1.785:                        ; Preds .B1.206                 ; Infreq
        add       esp, 12                                       ;254.4
                                ; LOE
.B1.207:                        ; Preds .B1.785                 ; Infreq
        mov       eax, DWORD PTR [-180+ebp]                     ;254.4
        lea       edx, DWORD PTR [-152+ebp]                     ;254.4
        push      edx                                           ;254.4
        push      OFFSET FLAT: __STRLITPACK_238.0.1             ;254.4
        mov       DWORD PTR [-152+ebp], eax                     ;254.4
        lea       ecx, DWORD PTR [-232+ebp]                     ;254.4
        push      ecx                                           ;254.4
        call      _for_write_seq_lis_xmit                       ;254.4
                                ; LOE
.B1.786:                        ; Preds .B1.207                 ; Infreq
        add       esp, 12                                       ;254.4
                                ; LOE
.B1.208:                        ; Preds .B1.786                 ; Infreq
        mov       edi, DWORD PTR [-172+ebp]                     ;254.4
        lea       eax, DWORD PTR [-144+ebp]                     ;254.4
        push      eax                                           ;254.4
        push      OFFSET FLAT: __STRLITPACK_239.0.1             ;254.4
        mov       DWORD PTR [-144+ebp], edi                     ;254.4
        lea       edx, DWORD PTR [-232+ebp]                     ;254.4
        push      edx                                           ;254.4
        call      _for_write_seq_lis_xmit                       ;254.4
                                ; LOE edi
.B1.787:                        ; Preds .B1.208                 ; Infreq
        add       esp, 12                                       ;254.4
                                ; LOE edi
.B1.209:                        ; Preds .B1.787                 ; Infreq
        mov       esi, DWORD PTR [-156+ebp]                     ;254.41
        mov       eax, 1                                        ;254.4
        mov       DWORD PTR [-464+ebp], eax                     ;254.4
        xor       ecx, ecx                                      ;254.4
        test      edi, edi                                      ;254.4
        mov       DWORD PTR [-456+ebp], eax                     ;254.4
        lea       eax, DWORD PTR [-464+ebp]                     ;254.4
        push      eax                                           ;254.4
        push      OFFSET FLAT: __STRLITPACK_240.0.1             ;254.4
        lea       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ALMOV-4+esi*4] ;254.4
        mov       DWORD PTR [-460+ebp], edx                     ;254.4
        lea       edx, DWORD PTR [-232+ebp]                     ;254.4
        push      edx                                           ;254.4
        cmovge    ecx, edi                                      ;254.4
        mov       DWORD PTR [-452+ebp], ecx                     ;254.4
        mov       DWORD PTR [-448+ebp], 40                      ;254.4
        call      _for_write_seq_lis_xmit                       ;254.4
                                ; LOE esi edi
.B1.788:                        ; Preds .B1.209                 ; Infreq
        add       esp, 12                                       ;254.4
                                ; LOE esi edi
.B1.210:                        ; Preds .B1.788                 ; Infreq
        push      32                                            ;255.7
        xor       eax, eax                                      ;255.7
        push      eax                                           ;255.7
        push      eax                                           ;255.7
        push      -2088435968                                   ;255.7
        push      eax                                           ;255.7
        push      OFFSET FLAT: __STRLITPACK_241                 ;255.7
        call      _for_stop_core                                ;255.7
                                ; LOE esi edi
.B1.789:                        ; Preds .B1.210                 ; Infreq
        add       esp, 24                                       ;255.7
        jmp       .B1.212       ; Prob 100%                     ;255.7
                                ; LOE esi edi
.B1.211:                        ; Preds .B1.202                 ; Infreq
        mov       eax, DWORD PTR [-188+ebp]                     ;276.4
        mov       edx, DWORD PTR [-196+ebp]                     ;275.7
        mov       esi, DWORD PTR [-156+ebp]                     ;275.7
        mov       edi, DWORD PTR [-172+ebp]                     ;259.6
        mov       DWORD PTR [-560+ebp], eax                     ;276.4
        mov       DWORD PTR [-584+ebp], edx                     ;275.7
                                ; LOE esi edi
.B1.212:                        ; Preds .B1.789 .B1.211         ; Infreq
        test      edi, edi                                      ;259.6
        jle       .B1.229       ; Prob 2%                       ;259.6
                                ; LOE esi edi
.B1.213:                        ; Preds .B1.212                 ; Infreq
        mov       edx, DWORD PTR [-180+ebp]                     ;261.13
        mov       eax, 1                                        ;
        mov       DWORD PTR [-588+ebp], edi                     ;264.14
        lea       ecx, DWORD PTR [esi*4]                        ;264.14
        mov       DWORD PTR [-548+ebp], ecx                     ;264.14
        mov       DWORD PTR [-580+ebp], edx                     ;264.14
        mov       DWORD PTR [-472+ebp], esi                     ;264.14
        mov       esi, eax                                      ;264.14
                                ; LOE esi
.B1.214:                        ; Preds .B1.227 .B1.213         ; Infreq
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM+32] ;260.5
        lea       ecx, DWORD PTR [esi+esi*4]                    ;260.37
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM] ;260.5
        shl       edx, 2                                        ;260.37
        shl       ecx, 3                                        ;260.37
        mov       DWORD PTR [-556+ebp], edx                     ;260.37
        mov       DWORD PTR [-544+ebp], edi                     ;260.5
        sub       edi, edx                                      ;260.37
        mov       edx, DWORD PTR [-472+ebp]                     ;260.8
        mov       DWORD PTR [-540+ebp], ecx                     ;260.37
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ALMOV-44+ecx+edx*4] ;260.8
        mov       DWORD PTR [-552+ebp], edx                     ;260.8
        cmp       DWORD PTR [edi+edx*4], 0                      ;260.37
        jle       .B1.567       ; Prob 16%                      ;260.37
                                ; LOE esi edi
.B1.215:                        ; Preds .B1.873 .B1.214         ; Infreq
        mov       ecx, DWORD PTR [-544+ebp]                     ;264.14
        mov       edx, DWORD PTR [-560+ebp]                     ;264.14
        mov       eax, DWORD PTR [-540+ebp]                     ;264.14
        push      OFFSET FLAT: __NLITPACK_1.0.1                 ;264.14
        lea       edx, DWORD PTR [ecx+edx*4]                    ;264.14
        mov       ecx, DWORD PTR [-548+ebp]                     ;264.14
        sub       edx, DWORD PTR [-556+ebp]                     ;264.14
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ALMOV-44+ecx+eax] ;264.14
        lea       edi, DWORD PTR [edi+ecx*4]                    ;264.14
        push      edi                                           ;264.14
        push      edx                                           ;264.14
        call      _DYNUST_NETWORK_MODULE_mp_GETFLINKFROMNODE    ;264.14
                                ; LOE eax esi
.B1.790:                        ; Preds .B1.215                 ; Infreq
        add       esp, 12                                       ;264.14
                                ; LOE eax esi
.B1.216:                        ; Preds .B1.790                 ; Infreq
        test      eax, eax                                      ;264.84
        jg        .B1.566       ; Prob 20%                      ;264.84
                                ; LOE esi
.B1.217:                        ; Preds .B1.216                 ; Infreq
        push      32                                            ;268.13
        mov       DWORD PTR [-232+ebp], 0                       ;268.13
        lea       edx, DWORD PTR [-72+ebp]                      ;268.13
        push      edx                                           ;268.13
        push      OFFSET FLAT: __STRLITPACK_251.0.1             ;268.13
        push      -2088435968                                   ;268.13
        push      911                                           ;268.13
        mov       DWORD PTR [-72+ebp], 5                        ;268.13
        lea       ecx, DWORD PTR [-232+ebp]                     ;268.13
        push      ecx                                           ;268.13
        mov       DWORD PTR [-68+ebp], OFFSET FLAT: __STRLITPACK_55 ;268.13
        call      _for_write_seq_lis                            ;268.13
                                ; LOE esi
.B1.791:                        ; Preds .B1.217                 ; Infreq
        add       esp, 24                                       ;268.13
                                ; LOE esi
.B1.218:                        ; Preds .B1.791                 ; Infreq
        mov       edx, DWORD PTR [-196+ebp]                     ;268.13
        lea       ecx, DWORD PTR [-40+ebp]                      ;268.13
        push      ecx                                           ;268.13
        push      OFFSET FLAT: __STRLITPACK_252.0.1             ;268.13
        mov       DWORD PTR [-584+ebp], edx                     ;268.13
        lea       edi, DWORD PTR [-232+ebp]                     ;268.13
        push      edi                                           ;268.13
        mov       DWORD PTR [-40+ebp], edx                      ;268.13
        call      _for_write_seq_lis_xmit                       ;268.13
                                ; LOE esi
.B1.792:                        ; Preds .B1.218                 ; Infreq
        add       esp, 12                                       ;268.13
                                ; LOE esi
.B1.219:                        ; Preds .B1.792                 ; Infreq
        mov       DWORD PTR [-64+ebp], 3                        ;268.13
        lea       edx, DWORD PTR [-64+ebp]                      ;268.13
        push      edx                                           ;268.13
        push      OFFSET FLAT: __STRLITPACK_253.0.1             ;268.13
        mov       DWORD PTR [-60+ebp], OFFSET FLAT: __STRLITPACK_54 ;268.13
        lea       ecx, DWORD PTR [-232+ebp]                     ;268.13
        push      ecx                                           ;268.13
        call      _for_write_seq_lis_xmit                       ;268.13
                                ; LOE esi
.B1.793:                        ; Preds .B1.219                 ; Infreq
        add       esp, 12                                       ;268.13
                                ; LOE esi
.B1.220:                        ; Preds .B1.793                 ; Infreq
        mov       edx, DWORD PTR [-188+ebp]                     ;268.13
        lea       ecx, DWORD PTR [-32+ebp]                      ;268.13
        push      ecx                                           ;268.13
        push      OFFSET FLAT: __STRLITPACK_254.0.1             ;268.13
        mov       DWORD PTR [-560+ebp], edx                     ;268.13
        lea       edi, DWORD PTR [-232+ebp]                     ;268.13
        push      edi                                           ;268.13
        mov       DWORD PTR [-32+ebp], edx                      ;268.13
        call      _for_write_seq_lis_xmit                       ;268.13
                                ; LOE esi
.B1.794:                        ; Preds .B1.220                 ; Infreq
        add       esp, 12                                       ;268.13
                                ; LOE esi
.B1.221:                        ; Preds .B1.794                 ; Infreq
        mov       DWORD PTR [-56+ebp], 5                        ;268.13
        lea       edx, DWORD PTR [-56+ebp]                      ;268.13
        push      edx                                           ;268.13
        push      OFFSET FLAT: __STRLITPACK_255.0.1             ;268.13
        mov       DWORD PTR [-52+ebp], OFFSET FLAT: __STRLITPACK_53 ;268.13
        lea       ecx, DWORD PTR [-232+ebp]                     ;268.13
        push      ecx                                           ;268.13
        call      _for_write_seq_lis_xmit                       ;268.13
                                ; LOE esi
.B1.795:                        ; Preds .B1.221                 ; Infreq
        add       esp, 12                                       ;268.13
                                ; LOE esi
.B1.222:                        ; Preds .B1.795                 ; Infreq
        mov       edx, DWORD PTR [-180+ebp]                     ;268.13
        lea       ecx, DWORD PTR [-24+ebp]                      ;268.13
        push      ecx                                           ;268.13
        push      OFFSET FLAT: __STRLITPACK_256.0.1             ;268.13
        mov       DWORD PTR [-580+ebp], edx                     ;268.13
        lea       edi, DWORD PTR [-232+ebp]                     ;268.13
        push      edi                                           ;268.13
        mov       DWORD PTR [-24+ebp], edx                      ;268.13
        call      _for_write_seq_lis_xmit                       ;268.13
                                ; LOE esi
.B1.796:                        ; Preds .B1.222                 ; Infreq
        add       esp, 12                                       ;268.13
                                ; LOE esi
.B1.223:                        ; Preds .B1.796                 ; Infreq
        push      32                                            ;269.4
        mov       DWORD PTR [-232+ebp], 0                       ;269.4
        lea       edx, DWORD PTR [-48+ebp]                      ;269.4
        push      edx                                           ;269.4
        push      OFFSET FLAT: __STRLITPACK_257.0.1             ;269.4
        push      -2088435968                                   ;269.4
        push      911                                           ;269.4
        mov       DWORD PTR [-48+ebp], 34                       ;269.4
        lea       ecx, DWORD PTR [-232+ebp]                     ;269.4
        push      ecx                                           ;269.4
        mov       DWORD PTR [-44+ebp], OFFSET FLAT: __STRLITPACK_51 ;269.4
        call      _for_write_seq_lis                            ;269.4
                                ; LOE esi
.B1.797:                        ; Preds .B1.223                 ; Infreq
        add       esp, 24                                       ;269.4
                                ; LOE esi
.B1.224:                        ; Preds .B1.797                 ; Infreq
        mov       edx, DWORD PTR [-560+ebp]                     ;269.4
        lea       ecx, DWORD PTR [-16+ebp]                      ;269.4
        push      ecx                                           ;269.4
        push      OFFSET FLAT: __STRLITPACK_258.0.1             ;269.4
        mov       DWORD PTR [-16+ebp], edx                      ;269.4
        lea       edi, DWORD PTR [-232+ebp]                     ;269.4
        push      edi                                           ;269.4
        call      _for_write_seq_lis_xmit                       ;269.4
                                ; LOE esi
.B1.798:                        ; Preds .B1.224                 ; Infreq
        add       esp, 12                                       ;269.4
                                ; LOE esi
.B1.225:                        ; Preds .B1.798                 ; Infreq
        mov       edx, DWORD PTR [-156+ebp]                     ;269.59
        mov       edi, DWORD PTR [-540+ebp]                     ;269.4
        mov       DWORD PTR [-472+ebp], edx                     ;269.59
        lea       ecx, DWORD PTR [edx*4]                        ;269.4
        mov       DWORD PTR [-548+ebp], ecx                     ;269.4
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ALMOV-44+ecx+edi] ;269.4
        mov       DWORD PTR [-8+ebp], edx                       ;269.4
        lea       edx, DWORD PTR [-8+ebp]                       ;269.4
        push      edx                                           ;269.4
        push      OFFSET FLAT: __STRLITPACK_259.0.1             ;269.4
        lea       edx, DWORD PTR [-232+ebp]                     ;269.4
        push      edx                                           ;269.4
        call      _for_write_seq_lis_xmit                       ;269.4
                                ; LOE esi
.B1.799:                        ; Preds .B1.225                 ; Infreq
        add       esp, 12                                       ;269.4
                                ; LOE esi
.B1.226:                        ; Preds .B1.799                 ; Infreq
        push      32                                            ;270.4
        xor       edx, edx                                      ;270.4
        push      edx                                           ;270.4
        push      edx                                           ;270.4
        push      -2088435968                                   ;270.4
        push      edx                                           ;270.4
        push      OFFSET FLAT: __STRLITPACK_260                 ;270.4
        call      _for_stop_core                                ;270.4
                                ; LOE esi
.B1.800:                        ; Preds .B1.226                 ; Infreq
        add       esp, 24                                       ;270.4
                                ; LOE esi
.B1.227:                        ; Preds .B1.800                 ; Infreq
        inc       esi                                           ;272.9
        cmp       esi, DWORD PTR [-588+ebp]                     ;272.9
        jle       .B1.214       ; Prob 90%                      ;272.9
                                ; LOE esi
.B1.228:                        ; Preds .B1.227                 ; Infreq
        mov       esi, DWORD PTR [-472+ebp]                     ;
        mov       edi, DWORD PTR [-172+ebp]                     ;279.4
                                ; LOE esi edi
.B1.229:                        ; Preds .B1.212 .B1.566 .B1.228 ; Infreq
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM] ;275.4
        mov       DWORD PTR [-596+ebp], ecx                     ;275.4
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_NSIGN+44] ;275.29
        neg       ecx                                           ;275.29
        mov       DWORD PTR [-588+ebp], edi                     ;
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM+32] ;275.29
        mov       DWORD PTR [-472+ebp], esi                     ;
        shl       edi, 2                                        ;275.29
        mov       DWORD PTR [-604+ebp], edi                     ;275.29
        lea       edi, DWORD PTR [5+ecx+esi]                    ;275.29
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_NSIGN+32] ;275.29
        mov       ecx, DWORD PTR [-636+ebp]                     ;275.29
        shl       esi, 2                                        ;275.29
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;275.7
        neg       esi                                           ;275.29
        add       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_NSIGN] ;275.29
        imul      edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_NSIGN+40] ;275.29
        add       ecx, esi                                      ;275.29
        mov       DWORD PTR [-444+ebp], edx                     ;275.7
        imul      edx, edx, 152                                 ;275.29
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;275.7
        mov       DWORD PTR [-492+ebp], edx                     ;275.29
        mov       esi, DWORD PTR [ecx+edi]                      ;275.32
        mov       DWORD PTR [-500+ebp], eax                     ;275.7
        sub       eax, edx                                      ;275.29
        mov       ecx, DWORD PTR [-584+ebp]                     ;275.29
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM] ;275.29
        mov       DWORD PTR [-468+ebp], esi                     ;275.32
        imul      esi, esi, 152                                 ;275.32
        mov       DWORD PTR [-628+ebp], esi                     ;275.32
        lea       edi, DWORD PTR [edx+ecx*4]                    ;275.29
        sub       edi, DWORD PTR [-604+ebp]                     ;275.29
        mov       DWORD PTR [-620+ebp], eax                     ;275.29
        mov       edx, DWORD PTR [edi]                          ;275.7
        mov       edi, DWORD PTR [-588+ebp]                     ;275.29
        cmp       edx, DWORD PTR [28+esi+eax]                   ;275.29
        mov       esi, DWORD PTR [-472+ebp]                     ;275.29
        jne       .B1.562       ; Prob 5%                       ;275.29
                                ; LOE ecx esi edi cl ch
.B1.230:                        ; Preds .B1.864 .B1.229         ; Infreq
        test      edi, edi                                      ;279.4
        jle       .B1.242       ; Prob 50%                      ;279.4
                                ; LOE esi edi
.B1.231:                        ; Preds .B1.230                 ; Infreq
        mov       edx, edi                                      ;279.4
        shr       edx, 31                                       ;279.4
        add       edx, edi                                      ;279.4
        sar       edx, 1                                        ;279.4
        test      edx, edx                                      ;279.4
        jbe       .B1.449       ; Prob 3%                       ;279.4
                                ; LOE edx esi edi
.B1.232:                        ; Preds .B1.231                 ; Infreq
        mov       ecx, DWORD PTR [-596+ebp]                     ;
        xor       eax, eax                                      ;
        sub       ecx, DWORD PTR [-604+ebp]                     ;
        mov       DWORD PTR [-504+ebp], edx                     ;
        mov       DWORD PTR [-588+ebp], edi                     ;
                                ; LOE eax ecx esi
.B1.233:                        ; Preds .B1.237 .B1.232         ; Infreq
        lea       edx, DWORD PTR [eax+eax*4]                    ;280.22
        shl       edx, 4                                        ;280.22
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ALMOV-4+edx+esi*4] ;280.9
        test      edi, edi                                      ;280.22
        jle       .B1.235       ; Prob 16%                      ;280.22
                                ; LOE eax edx ecx esi edi
.B1.234:                        ; Preds .B1.233                 ; Infreq
        mov       edi, DWORD PTR [ecx+edi*4]                    ;280.27
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ALMOV-4+edx+esi*4], edi ;280.27
                                ; LOE eax edx ecx esi
.B1.235:                        ; Preds .B1.233 .B1.234         ; Infreq
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ALMOV+36+edx+esi*4] ;280.9
        test      edi, edi                                      ;280.22
        jle       .B1.237       ; Prob 16%                      ;280.22
                                ; LOE eax edx ecx esi edi
.B1.236:                        ; Preds .B1.235                 ; Infreq
        mov       edi, DWORD PTR [ecx+edi*4]                    ;280.27
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ALMOV+36+edx+esi*4], edi ;280.27
                                ; LOE eax ecx esi
.B1.237:                        ; Preds .B1.235 .B1.236         ; Infreq
        inc       eax                                           ;279.4
        cmp       eax, DWORD PTR [-504+ebp]                     ;279.4
        jb        .B1.233       ; Prob 80%                      ;279.4
                                ; LOE eax ecx esi
.B1.238:                        ; Preds .B1.237                 ; Infreq
        mov       edi, DWORD PTR [-588+ebp]                     ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;279.4
                                ; LOE eax esi edi
.B1.239:                        ; Preds .B1.238 .B1.449         ; Infreq
        lea       edx, DWORD PTR [-1+eax]                       ;279.4
        cmp       edi, edx                                      ;279.4
        jbe       .B1.242       ; Prob 3%                       ;279.4
                                ; LOE eax esi edi
.B1.240:                        ; Preds .B1.239                 ; Infreq
        lea       eax, DWORD PTR [eax+eax*4]                    ;280.22
        shl       eax, 3                                        ;280.22
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ALMOV-44+eax+esi*4] ;280.9
        test      ecx, ecx                                      ;280.22
        jle       .B1.242       ; Prob 16%                      ;280.22
                                ; LOE eax ecx esi edi
.B1.241:                        ; Preds .B1.240                 ; Infreq
        mov       edx, DWORD PTR [-596+ebp]                     ;280.27
        sub       edx, DWORD PTR [-604+ebp]                     ;280.27
        mov       ecx, DWORD PTR [edx+ecx*4]                    ;280.27
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ALMOV-44+eax+esi*4], ecx ;280.27
                                ; LOE esi edi
.B1.242:                        ; Preds .B1.239 .B1.240 .B1.230 .B1.241 ; Infreq
        mov       edx, DWORD PTR [-560+ebp]                     ;283.67
        mov       eax, DWORD PTR [-596+ebp]                     ;283.67
        lea       ecx, DWORD PTR [eax+edx*4]                    ;283.67
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE] ;283.9
        sub       ecx, DWORD PTR [-604+ebp]                     ;283.67
        mov       eax, DWORD PTR [ecx]                          ;283.12
        sub       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE+32] ;283.67
        imul      ecx, eax, 84                                  ;283.67
        mov       eax, DWORD PTR [72+edx+ecx]                   ;283.12
        add       eax, eax                                      ;283.67
        neg       eax                                           ;283.67
        add       eax, DWORD PTR [40+edx+ecx]                   ;283.67
        mov       DWORD PTR [-508+ebp], eax                     ;283.67
        movsx     edx, WORD PTR [2+eax]                         ;283.12
        test      edx, edx                                      ;283.67
        jle       .B1.246       ; Prob 16%                      ;283.67
                                ; LOE edx esi edi
.B1.243:                        ; Preds .B1.242                 ; Infreq
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DESTINATION+32] ;286.12
        lea       ecx, DWORD PTR [edi+edi*4]                    ;
        shl       ecx, 3                                        ;
        shl       eax, 2                                        ;
        mov       DWORD PTR [-648+ebp], 0                       ;
        neg       eax                                           ;
        add       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DESTINATION] ;
        mov       DWORD PTR [-536+ebp], edx                     ;
        lea       ecx, DWORD PTR [ecx+esi*4]                    ;
        mov       DWORD PTR [-512+ebp], ecx                     ;
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_MASTERDEST+32] ;
        add       ecx, ecx                                      ;
        neg       ecx                                           ;
        add       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_MASTERDEST] ;
        mov       DWORD PTR [-532+ebp], ecx                     ;
        mov       DWORD PTR [-588+ebp], edi                     ;
        mov       DWORD PTR [-472+ebp], esi                     ;
        mov       ecx, DWORD PTR [-648+ebp]                     ;
                                ; LOE eax ecx
.B1.244:                        ; Preds .B1.244 .B1.243         ; Infreq
        mov       esi, DWORD PTR [-508+ebp]                     ;286.12
        mov       edi, DWORD PTR [-532+ebp]                     ;286.12
        movsx     edx, WORD PTR [4+esi+ecx*2]                   ;286.12
        movsx     esi, WORD PTR [edi+edx*2]                     ;286.12
        lea       edx, DWORD PTR [ecx+ecx*4]                    ;286.12
        inc       ecx                                           ;284.6
        cmp       ecx, DWORD PTR [-536+ebp]                     ;284.6
        mov       edi, DWORD PTR [eax+esi*4]                    ;286.12
        mov       esi, DWORD PTR [-512+ebp]                     ;286.12
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ALMOV-4+esi+edx*8], edi ;286.12
        jb        .B1.244       ; Prob 82%                      ;284.6
                                ; LOE eax ecx
.B1.245:                        ; Preds .B1.244                 ; Infreq
        mov       edx, DWORD PTR [-536+ebp]                     ;
        mov       edi, DWORD PTR [-588+ebp]                     ;
        add       edi, edx                                      ;285.12
        mov       esi, DWORD PTR [-472+ebp]                     ;
        mov       DWORD PTR [-172+ebp], edi                     ;285.12
                                ; LOE esi
.B1.246:                        ; Preds .B1.242 .B1.245         ; Infreq
        mov       edx, DWORD PTR [-628+ebp]                     ;294.20
        mov       ecx, DWORD PTR [-620+ebp]                     ;294.20
        mov       eax, DWORD PTR [-468+ebp]                     ;293.10
        mov       DWORD PTR [-164+ebp], eax                     ;293.10
        movsx     ecx, BYTE PTR [32+edx+ecx]                    ;294.20
        test      ecx, ecx                                      ;294.12
        jle       .B1.258       ; Prob 2%                       ;294.12
                                ; LOE ecx esi
.B1.247:                        ; Preds .B1.246                 ; Infreq
        mov       edx, 1                                        ;
        mov       DWORD PTR [-472+ebp], esi                     ;
        mov       eax, edx                                      ;
                                ; LOE eax edx ecx
.B1.248:                        ; Preds .B1.256 .B1.247         ; Infreq
        mov       edi, DWORD PTR [-172+ebp]                     ;295.13
        test      edi, edi                                      ;295.13
        jle       .B1.256       ; Prob 2%                       ;295.13
                                ; LOE eax edx ecx edi
.B1.249:                        ; Preds .B1.248                 ; Infreq
        mov       DWORD PTR [-436+ebp], edi                     ;
        mov       esi, eax                                      ;
        mov       DWORD PTR [-432+ebp], edx                     ;
        mov       DWORD PTR [-496+ebp], ecx                     ;
        mov       eax, DWORD PTR [-500+ebp]                     ;
                                ; LOE eax esi
.B1.250:                        ; Preds .B1.254 .B1.249         ; Infreq
        imul      edi, DWORD PTR [-468+ebp], 152                ;296.90
        imul      edx, DWORD PTR [-444+ebp], 152                ;296.90
        add       edi, eax                                      ;296.90
        sub       edi, edx                                      ;296.90
        mov       ecx, DWORD PTR [-432+ebp]                     ;296.90
        sub       ecx, DWORD PTR [68+edi]                       ;296.90
        imul      ecx, DWORD PTR [64+edi]                       ;296.90
        mov       edi, DWORD PTR [36+edi]                       ;296.15
        imul      ecx, DWORD PTR [edi+ecx], 152                 ;296.18
        add       ecx, eax                                      ;296.90
        sub       ecx, edx                                      ;296.90
        mov       edi, DWORD PTR [-472+ebp]                     ;296.90
        mov       edx, DWORD PTR [24+ecx]                       ;296.18
        lea       ecx, DWORD PTR [esi+esi*4]                    ;296.90
        shl       ecx, 3                                        ;296.90
        cmp       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ALMOV-44+ecx+edi*4] ;296.90
        jne       .B1.254       ; Prob 78%                      ;296.90
                                ; LOE eax esi
.B1.251:                        ; Preds .B1.250                 ; Infreq
        imul      edx, DWORD PTR [-468+ebp], 152                ;298.36
        lea       edi, DWORD PTR [-164+ebp]                     ;298.20
        add       edx, eax                                      ;298.20
        mov       eax, DWORD PTR [-492+ebp]                     ;298.20
        neg       eax                                           ;298.20
        mov       ecx, DWORD PTR [68+eax+edx]                   ;298.20
        neg       ecx                                           ;298.20
        add       ecx, DWORD PTR [-432+ebp]                     ;298.20
        imul      ecx, DWORD PTR [64+eax+edx]                   ;298.20
        add       ecx, DWORD PTR [36+eax+edx]                   ;298.20
        push      ecx                                           ;298.20
        push      edi                                           ;298.20
        call      _DYNUST_NETWORK_MODULE_mp_MOVENOFORLINK       ;298.20
                                ; LOE eax esi
.B1.801:                        ; Preds .B1.251                 ; Infreq
        add       esp, 8                                        ;298.20
                                ; LOE eax esi
.B1.252:                        ; Preds .B1.801                 ; Infreq
        imul      ecx, DWORD PTR [-164+ebp], 152                ;299.13
        imul      edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32], 152 ;299.13
        add       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;299.13
        mov       DWORD PTR [-476+ebp], ecx                     ;299.13
        sub       ecx, edx                                      ;299.13
        mov       DWORD PTR [-480+ebp], edx                     ;299.13
        mov       DWORD PTR [-484+ebp], esi                     ;
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+44] ;299.13
        mov       esi, DWORD PTR [68+ecx]                       ;299.13
        neg       edx                                           ;299.13
        neg       esi                                           ;299.13
        add       edx, eax                                      ;299.13
        mov       eax, DWORD PTR [-432+ebp]                     ;299.13
        add       esi, eax                                      ;299.13
        imul      esi, DWORD PTR [64+ecx]                       ;299.13
        imul      edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+40] ;299.13
        mov       ecx, DWORD PTR [36+ecx]                       ;299.13
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+32] ;299.13
        neg       edi                                           ;299.13
        add       edi, DWORD PTR [ecx+esi]                      ;299.13
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE] ;299.13
        lea       esi, DWORD PTR [edi*4]                        ;299.13
        shl       edi, 5                                        ;299.13
        sub       edi, esi                                      ;299.13
        lea       edi, DWORD PTR [9+edi+ecx]                    ;299.13
        mov       ecx, DWORD PTR [-476+ebp]                     ;301.23
        mov       BYTE PTR [edi+edx], 0                         ;299.13
        mov       edx, DWORD PTR [-480+ebp]                     ;301.23
        neg       edx                                           ;301.23
        mov       esi, DWORD PTR [68+edx+ecx]                   ;301.23
        neg       esi                                           ;301.23
        add       esi, eax                                      ;301.23
        lea       eax, DWORD PTR [-164+ebp]                     ;301.23
        imul      esi, DWORD PTR [64+edx+ecx]                   ;301.23
        add       esi, DWORD PTR [36+edx+ecx]                   ;301.23
        push      esi                                           ;301.23
        push      eax                                           ;301.23
        mov       esi, DWORD PTR [-484+ebp]                     ;301.23
        call      _DYNUST_NETWORK_MODULE_mp_GETBSTMOVE          ;301.23
                                ; LOE eax esi
.B1.802:                        ; Preds .B1.252                 ; Infreq
        mov       DWORD PTR [-488+ebp], eax                     ;301.23
        add       esp, 8                                        ;301.23
                                ; LOE esi
.B1.253:                        ; Preds .B1.802                 ; Infreq
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;302.16
        mov       edx, DWORD PTR [-164+ebp]                     ;302.16
        mov       DWORD PTR [-444+ebp], edi                     ;302.16
        imul      ecx, edi, 152                                 ;302.16
        imul      edi, edx, 152                                 ;302.16
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;302.16
        add       edi, eax                                      ;302.16
        sub       edi, ecx                                      ;302.16
        mov       DWORD PTR [-468+ebp], edx                     ;302.16
        mov       edx, DWORD PTR [-432+ebp]                     ;302.16
        mov       DWORD PTR [-484+ebp], esi                     ;
        sub       edx, DWORD PTR [68+edi]                       ;302.16
        imul      edx, DWORD PTR [64+edi]                       ;302.16
        mov       edi, DWORD PTR [36+edi]                       ;302.16
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+32] ;302.16
        neg       esi                                           ;302.16
        imul      edx, DWORD PTR [edi+edx], 152                 ;302.16
        add       edx, eax                                      ;302.16
        sub       edx, ecx                                      ;302.16
        mov       DWORD PTR [-492+ebp], ecx                     ;302.16
        add       esi, DWORD PTR [12+edx]                       ;302.16
        mov       edx, DWORD PTR [-488+ebp]                     ;302.16
        sub       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+44] ;302.16
        imul      edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+40] ;302.16
        lea       ecx, DWORD PTR [esi*4]                        ;302.16
        shl       esi, 5                                        ;302.16
        sub       esi, ecx                                      ;302.16
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE] ;302.16
        lea       edi, DWORD PTR [8+edx+ecx]                    ;302.16
        mov       BYTE PTR [edi+esi], 0                         ;302.16
        mov       esi, DWORD PTR [-156+ebp]                     ;317.7
        mov       DWORD PTR [-472+ebp], esi                     ;317.7
        mov       esi, DWORD PTR [-484+ebp]                     ;317.7
                                ; LOE eax esi
.B1.254:                        ; Preds .B1.253 .B1.250         ; Infreq
        inc       esi                                           ;304.13
        cmp       esi, DWORD PTR [-436+ebp]                     ;304.13
        jle       .B1.250       ; Prob 82%                      ;304.13
                                ; LOE eax esi
.B1.255:                        ; Preds .B1.254                 ; Infreq
        mov       DWORD PTR [-500+ebp], eax                     ;
        mov       eax, 1                                        ;
        mov       edx, DWORD PTR [-432+ebp]                     ;
        mov       ecx, DWORD PTR [-496+ebp]                     ;
                                ; LOE eax edx ecx
.B1.256:                        ; Preds .B1.255 .B1.248         ; Infreq
        inc       edx                                           ;316.9
        cmp       edx, ecx                                      ;316.9
        jle       .B1.248       ; Prob 82%                      ;316.9
                                ; LOE eax edx ecx
.B1.257:                        ; Preds .B1.256                 ; Infreq
        mov       esi, DWORD PTR [-472+ebp]                     ;
                                ; LOE esi
.B1.258:                        ; Preds .B1.257 .B1.246         ; Infreq
        inc       esi                                           ;317.7
        mov       DWORD PTR [-156+ebp], esi                     ;317.7
        cmp       esi, DWORD PTR [-612+ebp]                     ;317.7
        jle       .B1.186       ; Prob 90%                      ;317.7
                                ; LOE
.B1.260:                        ; Preds .B1.258 .B1.184         ; Infreq
        mov       eax, DWORD PTR [-1056+ebp]                    ;141.7
        inc       eax                                           ;141.7
        mov       DWORD PTR [-1056+ebp], eax                    ;141.7
        cmp       eax, DWORD PTR [-736+ebp]                     ;141.7
        jle       .B1.116       ; Prob 82%                      ;141.7
                                ; LOE
.B1.261:                        ; Preds .B1.260                 ; Infreq
        mov       edx, DWORD PTR [-732+ebp]                     ;
                                ; LOE edx
.B1.262:                        ; Preds .B1.114 .B1.261         ; Infreq
        test      dl, 1                                         ;325.10
        jne       .B1.561       ; Prob 3%                       ;325.10
                                ; LOE
.B1.263:                        ; Preds .B1.860 .B1.262         ; Infreq
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGNCOUNT], 0 ;326.19
        jle       .B1.413       ; Prob 16%                      ;326.19
                                ; LOE
.B1.264:                        ; Preds .B1.263                 ; Infreq
        push      32                                            ;327.6
        xor       eax, eax                                      ;327.6
        lea       edx, DWORD PTR [-232+ebp]                     ;327.6
        push      eax                                           ;327.6
        push      OFFSET FLAT: __STRLITPACK_266.0.1             ;327.6
        push      -2088435965                                   ;327.6
        push      44                                            ;327.6
        push      edx                                           ;327.6
        mov       DWORD PTR [-232+ebp], eax                     ;327.6
        call      _for_read_seq_lis                             ;327.6
                                ; LOE eax
.B1.803:                        ; Preds .B1.264                 ; Infreq
        add       esp, 24                                       ;327.6
                                ; LOE eax
.B1.265:                        ; Preds .B1.803                 ; Infreq
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;327.6
        test      eax, eax                                      ;328.15
        jne       .B1.559       ; Prob 5%                       ;328.15
                                ; LOE
.B1.266:                        ; Preds .B1.859 .B1.265         ; Infreq
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGNCOUNT] ;333.7
        xor       eax, eax                                      ;333.7
        test      edx, edx                                      ;333.7
        lea       ecx, DWORD PTR [-860+ebp]                     ;333.7
        push      12                                            ;333.7
        cmovle    edx, eax                                      ;333.7
        push      edx                                           ;333.7
        push      2                                             ;333.7
        push      ecx                                           ;333.7
        call      _for_check_mult_overflow                      ;333.7
                                ; LOE eax
.B1.804:                        ; Preds .B1.266                 ; Infreq
        add       esp, 16                                       ;333.7
                                ; LOE eax
.B1.267:                        ; Preds .B1.804                 ; Infreq
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGNDATA+12] ;333.7
        and       eax, 1                                        ;333.7
        and       edx, 1                                        ;333.7
        add       edx, edx                                      ;333.7
        shl       eax, 4                                        ;333.7
        or        edx, 1                                        ;333.7
        or        edx, eax                                      ;333.7
        or        edx, 262144                                   ;333.7
        push      edx                                           ;333.7
        push      OFFSET FLAT: _DYNUST_MAIN_MODULE_mp_SIGNDATA  ;333.7
        push      DWORD PTR [-860+ebp]                          ;333.7
        call      _for_alloc_allocatable                        ;333.7
                                ; LOE eax
.B1.805:                        ; Preds .B1.267                 ; Infreq
        add       esp, 12                                       ;333.7
        mov       esi, eax                                      ;333.7
                                ; LOE esi
.B1.268:                        ; Preds .B1.805                 ; Infreq
        test      esi, esi                                      ;333.7
        jne       .B1.270       ; Prob 50%                      ;333.7
                                ; LOE esi
.B1.269:                        ; Preds .B1.268                 ; Infreq
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGNCOUNT] ;333.7
        mov       ecx, 12                                       ;333.7
        xor       edx, edx                                      ;333.7
        test      edi, edi                                      ;333.7
        push      ecx                                           ;333.7
        cmovle    edi, edx                                      ;333.7
        mov       eax, 1                                        ;333.7
        push      edi                                           ;333.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGNDATA+16], eax ;333.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGNDATA+32], eax ;333.7
        lea       eax, DWORD PTR [-1028+ebp]                    ;333.7
        push      2                                             ;333.7
        push      eax                                           ;333.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGNDATA+12], 133 ;333.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGNDATA+4], ecx ;333.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGNDATA+8], edx ;333.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGNDATA+24], edi ;333.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGNDATA+28], ecx ;333.7
        call      _for_check_mult_overflow                      ;333.7
                                ; LOE esi
.B1.806:                        ; Preds .B1.269                 ; Infreq
        add       esp, 16                                       ;333.7
                                ; LOE esi
.B1.270:                        ; Preds .B1.806 .B1.268         ; Infreq
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGNDATA+24] ;334.4
        test      ecx, ecx                                      ;334.4
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], esi ;333.7
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGNDATA+32] ;334.4
        jle       .B1.289       ; Prob 50%                      ;334.4
                                ; LOE ecx esi
.B1.271:                        ; Preds .B1.270                 ; Infreq
        mov       eax, ecx                                      ;334.4
        shr       eax, 31                                       ;334.4
        add       eax, ecx                                      ;334.4
        sar       eax, 1                                        ;334.4
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGNDATA] ;334.4
        test      eax, eax                                      ;334.4
        mov       DWORD PTR [-1056+ebp], eax                    ;334.4
        jbe       .B1.452       ; Prob 10%                      ;334.4
                                ; LOE edx ecx esi
.B1.272:                        ; Preds .B1.271                 ; Infreq
        xor       eax, eax                                      ;
        mov       DWORD PTR [-1064+ebp], ecx                    ;
        xor       ecx, ecx                                      ;
        mov       DWORD PTR [-1060+ebp], esi                    ;
        mov       edi, DWORD PTR [-1056+ebp]                    ;
                                ; LOE eax edx ecx edi
.B1.273:                        ; Preds .B1.273 .B1.272         ; Infreq
        lea       esi, DWORD PTR [eax+eax*2]                    ;334.4
        inc       eax                                           ;334.4
        mov       DWORD PTR [edx+esi*8], ecx                    ;334.4
        cmp       eax, edi                                      ;334.4
        mov       DWORD PTR [12+edx+esi*8], ecx                 ;334.4
        jb        .B1.273       ; Prob 64%                      ;334.4
                                ; LOE eax edx ecx edi
.B1.274:                        ; Preds .B1.273                 ; Infreq
        mov       ecx, DWORD PTR [-1064+ebp]                    ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;334.4
        mov       esi, DWORD PTR [-1060+ebp]                    ;
                                ; LOE eax edx ecx esi
.B1.275:                        ; Preds .B1.274 .B1.452         ; Infreq
        lea       edi, DWORD PTR [-1+eax]                       ;334.4
        cmp       ecx, edi                                      ;334.4
        jbe       .B1.277       ; Prob 10%                      ;334.4
                                ; LOE eax edx ecx esi
.B1.276:                        ; Preds .B1.275                 ; Infreq
        mov       edi, esi                                      ;334.4
        add       eax, esi                                      ;334.4
        neg       edi                                           ;334.4
        add       edi, eax                                      ;334.4
        lea       eax, DWORD PTR [edi*8]                        ;334.4
        lea       eax, DWORD PTR [eax+edi*4]                    ;334.4
        mov       DWORD PTR [-12+edx+eax], 0                    ;334.4
                                ; LOE edx ecx esi
.B1.277:                        ; Preds .B1.275 .B1.276         ; Infreq
        cmp       DWORD PTR [-1056+ebp], 0                      ;335.4
        jbe       .B1.451       ; Prob 10%                      ;335.4
                                ; LOE edx ecx esi
.B1.278:                        ; Preds .B1.277                 ; Infreq
        xor       eax, eax                                      ;
        mov       DWORD PTR [-1064+ebp], ecx                    ;
        xor       ecx, ecx                                      ;
        mov       DWORD PTR [-1060+ebp], esi                    ;
        mov       edi, DWORD PTR [-1056+ebp]                    ;
                                ; LOE eax edx ecx edi
.B1.279:                        ; Preds .B1.279 .B1.278         ; Infreq
        lea       esi, DWORD PTR [eax+eax*2]                    ;335.4
        inc       eax                                           ;335.4
        mov       DWORD PTR [4+edx+esi*8], ecx                  ;335.4
        cmp       eax, edi                                      ;335.4
        mov       DWORD PTR [16+edx+esi*8], ecx                 ;335.4
        jb        .B1.279       ; Prob 64%                      ;335.4
                                ; LOE eax edx ecx edi
.B1.280:                        ; Preds .B1.279                 ; Infreq
        mov       ecx, DWORD PTR [-1064+ebp]                    ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;335.4
        mov       esi, DWORD PTR [-1060+ebp]                    ;
                                ; LOE eax edx ecx esi
.B1.281:                        ; Preds .B1.280 .B1.451         ; Infreq
        lea       edi, DWORD PTR [-1+eax]                       ;335.4
        cmp       ecx, edi                                      ;335.4
        jbe       .B1.283       ; Prob 10%                      ;335.4
                                ; LOE eax edx ecx esi
.B1.282:                        ; Preds .B1.281                 ; Infreq
        mov       edi, esi                                      ;335.4
        add       eax, esi                                      ;335.4
        neg       edi                                           ;335.4
        add       edi, eax                                      ;335.4
        lea       eax, DWORD PTR [edi*8]                        ;335.4
        lea       eax, DWORD PTR [eax+edi*4]                    ;335.4
        mov       DWORD PTR [-8+edx+eax], 0                     ;335.4
                                ; LOE edx ecx esi
.B1.283:                        ; Preds .B1.281 .B1.282         ; Infreq
        cmp       DWORD PTR [-1056+ebp], 0                      ;336.4
        jbe       .B1.450       ; Prob 10%                      ;336.4
                                ; LOE edx ecx esi
.B1.284:                        ; Preds .B1.283                 ; Infreq
        xor       eax, eax                                      ;
        mov       DWORD PTR [-1064+ebp], ecx                    ;
        xor       ecx, ecx                                      ;
        mov       DWORD PTR [-1060+ebp], esi                    ;
        mov       edi, DWORD PTR [-1056+ebp]                    ;
                                ; LOE eax edx ecx edi
.B1.285:                        ; Preds .B1.285 .B1.284         ; Infreq
        lea       esi, DWORD PTR [eax+eax*2]                    ;336.4
        inc       eax                                           ;336.4
        mov       DWORD PTR [8+edx+esi*8], ecx                  ;336.4
        cmp       eax, edi                                      ;336.4
        mov       DWORD PTR [20+edx+esi*8], ecx                 ;336.4
        jb        .B1.285       ; Prob 64%                      ;336.4
                                ; LOE eax edx ecx edi
.B1.286:                        ; Preds .B1.285                 ; Infreq
        mov       ecx, DWORD PTR [-1064+ebp]                    ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;336.4
        mov       esi, DWORD PTR [-1060+ebp]                    ;
                                ; LOE eax edx ecx esi
.B1.287:                        ; Preds .B1.286 .B1.450         ; Infreq
        lea       edi, DWORD PTR [-1+eax]                       ;336.4
        cmp       ecx, edi                                      ;336.4
        jbe       .B1.289       ; Prob 10%                      ;336.4
                                ; LOE eax edx esi
.B1.288:                        ; Preds .B1.287                 ; Infreq
        mov       ecx, esi                                      ;336.4
        add       eax, esi                                      ;336.4
        neg       ecx                                           ;336.4
        add       ecx, eax                                      ;336.4
        lea       eax, DWORD PTR [ecx*8]                        ;336.4
        lea       esi, DWORD PTR [eax+ecx*4]                    ;336.4
        mov       DWORD PTR [-4+edx+esi], 0                     ;336.4
                                ; LOE
.B1.289:                        ; Preds .B1.270 .B1.287 .B1.288 ; Infreq
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGNCOUNT] ;339.4
        xor       eax, eax                                      ;339.4
        test      edx, edx                                      ;339.4
        lea       ecx, DWORD PTR [-852+ebp]                     ;339.4
        push      32                                            ;339.4
        cmovle    edx, eax                                      ;339.4
        push      edx                                           ;339.4
        push      2                                             ;339.4
        push      ecx                                           ;339.4
        call      _for_check_mult_overflow                      ;339.4
                                ; LOE eax
.B1.807:                        ; Preds .B1.289                 ; Infreq
        add       esp, 16                                       ;339.4
                                ; LOE eax
.B1.290:                        ; Preds .B1.807                 ; Infreq
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGNAPPRH+12] ;339.4
        and       eax, 1                                        ;339.4
        and       edx, 1                                        ;339.4
        add       edx, edx                                      ;339.4
        shl       eax, 4                                        ;339.4
        or        edx, 1                                        ;339.4
        or        edx, eax                                      ;339.4
        or        edx, 262144                                   ;339.4
        push      edx                                           ;339.4
        push      OFFSET FLAT: _DYNUST_MAIN_MODULE_mp_SIGNAPPRH ;339.4
        push      DWORD PTR [-852+ebp]                          ;339.4
        call      _for_alloc_allocatable                        ;339.4
                                ; LOE eax
.B1.808:                        ; Preds .B1.290                 ; Infreq
        add       esp, 12                                       ;339.4
        mov       esi, eax                                      ;339.4
                                ; LOE esi
.B1.291:                        ; Preds .B1.808                 ; Infreq
        test      esi, esi                                      ;339.4
        jne       .B1.293       ; Prob 50%                      ;339.4
                                ; LOE esi
.B1.292:                        ; Preds .B1.291                 ; Infreq
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGNCOUNT] ;339.4
        mov       ecx, 32                                       ;339.4
        xor       edx, edx                                      ;339.4
        test      edi, edi                                      ;339.4
        push      ecx                                           ;339.4
        cmovle    edi, edx                                      ;339.4
        mov       eax, 1                                        ;339.4
        push      edi                                           ;339.4
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGNAPPRH+16], eax ;339.4
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGNAPPRH+32], eax ;339.4
        lea       eax, DWORD PTR [-1020+ebp]                    ;339.4
        push      2                                             ;339.4
        push      eax                                           ;339.4
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGNAPPRH+12], 133 ;339.4
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGNAPPRH+4], ecx ;339.4
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGNAPPRH+8], edx ;339.4
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGNAPPRH+24], edi ;339.4
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGNAPPRH+28], ecx ;339.4
        call      _for_check_mult_overflow                      ;339.4
                                ; LOE esi
.B1.809:                        ; Preds .B1.292                 ; Infreq
        add       esp, 16                                       ;339.4
                                ; LOE esi
.B1.293:                        ; Preds .B1.809 .B1.291         ; Infreq
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGNAPPRH+24] ;340.7
        test      ecx, ecx                                      ;340.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], esi ;339.4
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGNAPPRH+32] ;340.7
        jle       .B1.342       ; Prob 50%                      ;340.7
                                ; LOE ecx esi
.B1.294:                        ; Preds .B1.293                 ; Infreq
        mov       eax, ecx                                      ;340.7
        shr       eax, 31                                       ;340.7
        add       eax, ecx                                      ;340.7
        sar       eax, 1                                        ;340.7
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGNAPPRH] ;340.7
        test      eax, eax                                      ;340.7
        mov       DWORD PTR [-1056+ebp], eax                    ;340.7
        jbe       .B1.460       ; Prob 11%                      ;340.7
                                ; LOE edx ecx esi
.B1.295:                        ; Preds .B1.294                 ; Infreq
        xor       eax, eax                                      ;
        mov       DWORD PTR [-1064+ebp], ecx                    ;
        xor       ecx, ecx                                      ;
        mov       DWORD PTR [-1060+ebp], esi                    ;
        mov       edi, DWORD PTR [-1056+ebp]                    ;
                                ; LOE eax edx ecx edi
.B1.296:                        ; Preds .B1.296 .B1.295         ; Infreq
        mov       esi, eax                                      ;340.7
        inc       eax                                           ;340.7
        shl       esi, 6                                        ;340.7
        cmp       eax, edi                                      ;340.7
        mov       DWORD PTR [edx+esi], ecx                      ;340.7
        mov       DWORD PTR [32+edx+esi], ecx                   ;340.7
        jb        .B1.296       ; Prob 64%                      ;340.7
                                ; LOE eax edx ecx edi
.B1.297:                        ; Preds .B1.296                 ; Infreq
        mov       ecx, DWORD PTR [-1064+ebp]                    ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;340.7
        mov       esi, DWORD PTR [-1060+ebp]                    ;
                                ; LOE eax edx ecx esi
.B1.298:                        ; Preds .B1.297 .B1.460         ; Infreq
        lea       edi, DWORD PTR [-1+eax]                       ;340.7
        cmp       ecx, edi                                      ;340.7
        jbe       .B1.300       ; Prob 11%                      ;340.7
                                ; LOE eax edx ecx esi
.B1.299:                        ; Preds .B1.298                 ; Infreq
        mov       edi, esi                                      ;340.7
        add       eax, esi                                      ;340.7
        neg       edi                                           ;340.7
        add       edi, eax                                      ;340.7
        shl       edi, 5                                        ;340.7
        mov       DWORD PTR [-32+edx+edi], 0                    ;340.7
                                ; LOE edx ecx esi
.B1.300:                        ; Preds .B1.298 .B1.299         ; Infreq
        cmp       DWORD PTR [-1056+ebp], 0                      ;341.7
        jbe       .B1.459       ; Prob 11%                      ;341.7
                                ; LOE edx ecx esi
.B1.301:                        ; Preds .B1.300                 ; Infreq
        xor       eax, eax                                      ;
        mov       DWORD PTR [-1064+ebp], ecx                    ;
        xor       ecx, ecx                                      ;
        mov       DWORD PTR [-1060+ebp], esi                    ;
        mov       edi, DWORD PTR [-1056+ebp]                    ;
                                ; LOE eax edx ecx edi
.B1.302:                        ; Preds .B1.302 .B1.301         ; Infreq
        mov       esi, eax                                      ;341.7
        inc       eax                                           ;341.7
        shl       esi, 6                                        ;341.7
        cmp       eax, edi                                      ;341.7
        mov       DWORD PTR [4+edx+esi], ecx                    ;341.7
        mov       DWORD PTR [36+edx+esi], ecx                   ;341.7
        jb        .B1.302       ; Prob 64%                      ;341.7
                                ; LOE eax edx ecx edi
.B1.303:                        ; Preds .B1.302                 ; Infreq
        mov       ecx, DWORD PTR [-1064+ebp]                    ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;341.7
        mov       esi, DWORD PTR [-1060+ebp]                    ;
                                ; LOE eax edx ecx esi
.B1.304:                        ; Preds .B1.303 .B1.459         ; Infreq
        lea       edi, DWORD PTR [-1+eax]                       ;341.7
        cmp       ecx, edi                                      ;341.7
        jbe       .B1.306       ; Prob 11%                      ;341.7
                                ; LOE eax edx ecx esi
.B1.305:                        ; Preds .B1.304                 ; Infreq
        mov       edi, esi                                      ;341.7
        add       eax, esi                                      ;341.7
        neg       edi                                           ;341.7
        add       edi, eax                                      ;341.7
        shl       edi, 5                                        ;341.7
        mov       DWORD PTR [-28+edx+edi], 0                    ;341.7
                                ; LOE edx ecx esi
.B1.306:                        ; Preds .B1.304 .B1.305         ; Infreq
        cmp       DWORD PTR [-1056+ebp], 0                      ;342.4
        jbe       .B1.458       ; Prob 11%                      ;342.4
                                ; LOE edx ecx esi
.B1.307:                        ; Preds .B1.306                 ; Infreq
        xor       eax, eax                                      ;
        mov       DWORD PTR [-1064+ebp], ecx                    ;
        xor       ecx, ecx                                      ;
        mov       DWORD PTR [-1060+ebp], esi                    ;
        mov       edi, DWORD PTR [-1056+ebp]                    ;
                                ; LOE eax edx ecx edi
.B1.308:                        ; Preds .B1.308 .B1.307         ; Infreq
        mov       esi, eax                                      ;342.4
        inc       eax                                           ;342.4
        shl       esi, 6                                        ;342.4
        cmp       eax, edi                                      ;342.4
        mov       DWORD PTR [8+edx+esi], ecx                    ;342.4
        mov       DWORD PTR [40+edx+esi], ecx                   ;342.4
        jb        .B1.308       ; Prob 64%                      ;342.4
                                ; LOE eax edx ecx edi
.B1.309:                        ; Preds .B1.308                 ; Infreq
        mov       ecx, DWORD PTR [-1064+ebp]                    ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;342.4
        mov       esi, DWORD PTR [-1060+ebp]                    ;
                                ; LOE eax edx ecx esi
.B1.310:                        ; Preds .B1.309 .B1.458         ; Infreq
        lea       edi, DWORD PTR [-1+eax]                       ;342.4
        cmp       ecx, edi                                      ;342.4
        jbe       .B1.312       ; Prob 11%                      ;342.4
                                ; LOE eax edx ecx esi
.B1.311:                        ; Preds .B1.310                 ; Infreq
        mov       edi, esi                                      ;342.4
        add       eax, esi                                      ;342.4
        neg       edi                                           ;342.4
        add       edi, eax                                      ;342.4
        shl       edi, 5                                        ;342.4
        mov       DWORD PTR [-24+edx+edi], 0                    ;342.4
                                ; LOE edx ecx esi
.B1.312:                        ; Preds .B1.310 .B1.311         ; Infreq
        cmp       DWORD PTR [-1056+ebp], 0                      ;343.7
        jbe       .B1.457       ; Prob 11%                      ;343.7
                                ; LOE edx ecx esi
.B1.313:                        ; Preds .B1.312                 ; Infreq
        xor       eax, eax                                      ;
        mov       DWORD PTR [-1064+ebp], ecx                    ;
        xor       ecx, ecx                                      ;
        mov       DWORD PTR [-1060+ebp], esi                    ;
        mov       edi, DWORD PTR [-1056+ebp]                    ;
                                ; LOE eax edx ecx edi
.B1.314:                        ; Preds .B1.314 .B1.313         ; Infreq
        mov       esi, eax                                      ;343.7
        inc       eax                                           ;343.7
        shl       esi, 6                                        ;343.7
        cmp       eax, edi                                      ;343.7
        mov       DWORD PTR [12+edx+esi], ecx                   ;343.7
        mov       DWORD PTR [44+edx+esi], ecx                   ;343.7
        jb        .B1.314       ; Prob 64%                      ;343.7
                                ; LOE eax edx ecx edi
.B1.315:                        ; Preds .B1.314                 ; Infreq
        mov       ecx, DWORD PTR [-1064+ebp]                    ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;343.7
        mov       esi, DWORD PTR [-1060+ebp]                    ;
                                ; LOE eax edx ecx esi
.B1.316:                        ; Preds .B1.315 .B1.457         ; Infreq
        lea       edi, DWORD PTR [-1+eax]                       ;343.7
        cmp       ecx, edi                                      ;343.7
        jbe       .B1.318       ; Prob 11%                      ;343.7
                                ; LOE eax edx ecx esi
.B1.317:                        ; Preds .B1.316                 ; Infreq
        mov       edi, esi                                      ;343.7
        add       eax, esi                                      ;343.7
        neg       edi                                           ;343.7
        add       edi, eax                                      ;343.7
        shl       edi, 5                                        ;343.7
        mov       DWORD PTR [-20+edx+edi], 0                    ;343.7
                                ; LOE edx ecx esi
.B1.318:                        ; Preds .B1.316 .B1.317         ; Infreq
        cmp       DWORD PTR [-1056+ebp], 0                      ;344.7
        jbe       .B1.456       ; Prob 11%                      ;344.7
                                ; LOE edx ecx esi
.B1.319:                        ; Preds .B1.318                 ; Infreq
        xor       eax, eax                                      ;
        mov       DWORD PTR [-1064+ebp], ecx                    ;
        xor       ecx, ecx                                      ;
        mov       DWORD PTR [-1060+ebp], esi                    ;
        mov       edi, DWORD PTR [-1056+ebp]                    ;
                                ; LOE eax edx ecx edi
.B1.320:                        ; Preds .B1.320 .B1.319         ; Infreq
        mov       esi, eax                                      ;344.7
        inc       eax                                           ;344.7
        shl       esi, 6                                        ;344.7
        cmp       eax, edi                                      ;344.7
        mov       DWORD PTR [16+edx+esi], ecx                   ;344.7
        mov       DWORD PTR [48+edx+esi], ecx                   ;344.7
        jb        .B1.320       ; Prob 64%                      ;344.7
                                ; LOE eax edx ecx edi
.B1.321:                        ; Preds .B1.320                 ; Infreq
        mov       ecx, DWORD PTR [-1064+ebp]                    ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;344.7
        mov       esi, DWORD PTR [-1060+ebp]                    ;
                                ; LOE eax edx ecx esi
.B1.322:                        ; Preds .B1.321 .B1.456         ; Infreq
        lea       edi, DWORD PTR [-1+eax]                       ;344.7
        cmp       ecx, edi                                      ;344.7
        jbe       .B1.324       ; Prob 11%                      ;344.7
                                ; LOE eax edx ecx esi
.B1.323:                        ; Preds .B1.322                 ; Infreq
        mov       edi, esi                                      ;344.7
        add       eax, esi                                      ;344.7
        neg       edi                                           ;344.7
        add       edi, eax                                      ;344.7
        shl       edi, 5                                        ;344.7
        mov       DWORD PTR [-16+edx+edi], 0                    ;344.7
                                ; LOE edx ecx esi
.B1.324:                        ; Preds .B1.322 .B1.323         ; Infreq
        cmp       DWORD PTR [-1056+ebp], 0                      ;345.7
        jbe       .B1.455       ; Prob 11%                      ;345.7
                                ; LOE edx ecx esi
.B1.325:                        ; Preds .B1.324                 ; Infreq
        xor       eax, eax                                      ;
        mov       DWORD PTR [-1064+ebp], ecx                    ;
        xor       ecx, ecx                                      ;
        mov       DWORD PTR [-1060+ebp], esi                    ;
        mov       edi, DWORD PTR [-1056+ebp]                    ;
                                ; LOE eax edx ecx edi
.B1.326:                        ; Preds .B1.326 .B1.325         ; Infreq
        mov       esi, eax                                      ;345.7
        inc       eax                                           ;345.7
        shl       esi, 6                                        ;345.7
        cmp       eax, edi                                      ;345.7
        mov       DWORD PTR [20+edx+esi], ecx                   ;345.7
        mov       DWORD PTR [52+edx+esi], ecx                   ;345.7
        jb        .B1.326       ; Prob 64%                      ;345.7
                                ; LOE eax edx ecx edi
.B1.327:                        ; Preds .B1.326                 ; Infreq
        mov       ecx, DWORD PTR [-1064+ebp]                    ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;345.7
        mov       esi, DWORD PTR [-1060+ebp]                    ;
                                ; LOE eax edx ecx esi
.B1.328:                        ; Preds .B1.327 .B1.455         ; Infreq
        lea       edi, DWORD PTR [-1+eax]                       ;345.7
        cmp       ecx, edi                                      ;345.7
        jbe       .B1.330       ; Prob 11%                      ;345.7
                                ; LOE eax edx ecx esi
.B1.329:                        ; Preds .B1.328                 ; Infreq
        mov       edi, esi                                      ;345.7
        add       eax, esi                                      ;345.7
        neg       edi                                           ;345.7
        add       edi, eax                                      ;345.7
        shl       edi, 5                                        ;345.7
        mov       DWORD PTR [-12+edx+edi], 0                    ;345.7
                                ; LOE edx ecx esi
.B1.330:                        ; Preds .B1.328 .B1.329         ; Infreq
        cmp       DWORD PTR [-1056+ebp], 0                      ;346.4
        jbe       .B1.454       ; Prob 11%                      ;346.4
                                ; LOE edx ecx esi
.B1.331:                        ; Preds .B1.330                 ; Infreq
        xor       eax, eax                                      ;
        mov       DWORD PTR [-1064+ebp], ecx                    ;
        xor       ecx, ecx                                      ;
        mov       DWORD PTR [-1060+ebp], esi                    ;
        mov       edi, DWORD PTR [-1056+ebp]                    ;
                                ; LOE eax edx ecx edi
.B1.332:                        ; Preds .B1.332 .B1.331         ; Infreq
        mov       esi, eax                                      ;346.4
        inc       eax                                           ;346.4
        shl       esi, 6                                        ;346.4
        cmp       eax, edi                                      ;346.4
        mov       DWORD PTR [24+edx+esi], ecx                   ;346.4
        mov       DWORD PTR [56+edx+esi], ecx                   ;346.4
        jb        .B1.332       ; Prob 64%                      ;346.4
                                ; LOE eax edx ecx edi
.B1.333:                        ; Preds .B1.332                 ; Infreq
        mov       ecx, DWORD PTR [-1064+ebp]                    ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;346.4
        mov       esi, DWORD PTR [-1060+ebp]                    ;
                                ; LOE eax edx ecx esi
.B1.334:                        ; Preds .B1.333 .B1.454         ; Infreq
        lea       edi, DWORD PTR [-1+eax]                       ;346.4
        cmp       ecx, edi                                      ;346.4
        jbe       .B1.336       ; Prob 11%                      ;346.4
                                ; LOE eax edx ecx esi
.B1.335:                        ; Preds .B1.334                 ; Infreq
        mov       edi, esi                                      ;346.4
        add       eax, esi                                      ;346.4
        neg       edi                                           ;346.4
        add       edi, eax                                      ;346.4
        shl       edi, 5                                        ;346.4
        mov       DWORD PTR [-8+edx+edi], 0                     ;346.4
                                ; LOE edx ecx esi
.B1.336:                        ; Preds .B1.334 .B1.335         ; Infreq
        cmp       DWORD PTR [-1056+ebp], 0                      ;347.7
        jbe       .B1.453       ; Prob 11%                      ;347.7
                                ; LOE edx ecx esi
.B1.337:                        ; Preds .B1.336                 ; Infreq
        xor       eax, eax                                      ;
        mov       DWORD PTR [-1064+ebp], ecx                    ;
        xor       ecx, ecx                                      ;
        mov       DWORD PTR [-1060+ebp], esi                    ;
        mov       edi, DWORD PTR [-1056+ebp]                    ;
                                ; LOE eax edx ecx edi
.B1.338:                        ; Preds .B1.338 .B1.337         ; Infreq
        mov       esi, eax                                      ;347.7
        inc       eax                                           ;347.7
        shl       esi, 6                                        ;347.7
        cmp       eax, edi                                      ;347.7
        mov       DWORD PTR [28+edx+esi], ecx                   ;347.7
        mov       DWORD PTR [60+edx+esi], ecx                   ;347.7
        jb        .B1.338       ; Prob 64%                      ;347.7
                                ; LOE eax edx ecx edi
.B1.339:                        ; Preds .B1.338                 ; Infreq
        mov       ecx, DWORD PTR [-1064+ebp]                    ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;347.7
        mov       esi, DWORD PTR [-1060+ebp]                    ;
                                ; LOE eax edx ecx esi
.B1.340:                        ; Preds .B1.339 .B1.453         ; Infreq
        lea       edi, DWORD PTR [-1+eax]                       ;347.7
        cmp       ecx, edi                                      ;347.7
        jbe       .B1.342       ; Prob 11%                      ;347.7
                                ; LOE eax edx esi
.B1.341:                        ; Preds .B1.340                 ; Infreq
        mov       ecx, esi                                      ;347.7
        add       eax, esi                                      ;347.7
        neg       ecx                                           ;347.7
        add       ecx, eax                                      ;347.7
        shl       ecx, 5                                        ;347.7
        mov       DWORD PTR [-4+edx+ecx], 0                     ;347.7
                                ; LOE
.B1.342:                        ; Preds .B1.293 .B1.340 .B1.341 ; Infreq
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGNCOUNT] ;349.7
        test      ecx, ecx                                      ;349.7
        jle       .B1.413       ; Prob 2%                       ;349.7
                                ; LOE ecx
.B1.343:                        ; Preds .B1.342                 ; Infreq
        mov       eax, 1                                        ;
        mov       DWORD PTR [-820+ebp], eax                     ;
        mov       DWORD PTR [-828+ebp], ecx                     ;
                                ; LOE
.B1.344:                        ; Preds .B1.411 .B1.343         ; Infreq
        mov       edx, DWORD PTR [-820+ebp]                     ;350.9
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGNDATA+32] ;350.9
        push      32                                            ;350.9
        mov       DWORD PTR [-232+ebp], 0                       ;350.9
        lea       eax, DWORD PTR [edx*8]                        ;350.9
        lea       edi, DWORD PTR [eax+edx*4]                    ;350.9
        mov       DWORD PTR [-752+ebp], edi                     ;350.9
        lea       edx, DWORD PTR [-432+ebp]                     ;350.9
        push      edx                                           ;350.9
        push      OFFSET FLAT: __STRLITPACK_269.0.1             ;350.9
        push      -2088435965                                   ;350.9
        push      44                                            ;350.9
        lea       ecx, DWORD PTR [esi*8]                        ;350.9
        lea       eax, DWORD PTR [ecx+esi*4]                    ;350.9
        neg       eax                                           ;350.9
        lea       ecx, DWORD PTR [-232+ebp]                     ;350.9
        add       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGNDATA] ;350.9
        push      ecx                                           ;350.9
        add       eax, edi                                      ;350.9
        mov       DWORD PTR [-432+ebp], eax                     ;350.9
        call      _for_read_seq_lis                             ;350.9
                                ; LOE eax edi
.B1.810:                        ; Preds .B1.344                 ; Infreq
        add       esp, 24                                       ;350.9
                                ; LOE eax edi
.B1.345:                        ; Preds .B1.810                 ; Infreq
        test      eax, eax                                      ;350.9
        jne       .B1.350       ; Prob 50%                      ;350.9
                                ; LOE eax edi
.B1.346:                        ; Preds .B1.345                 ; Infreq
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGNDATA+32] ;350.9
        mov       esi, edi                                      ;350.9
        lea       eax, DWORD PTR [edx*8]                        ;350.9
        lea       ecx, DWORD PTR [eax+edx*4]                    ;350.9
        neg       ecx                                           ;350.9
        lea       eax, DWORD PTR [-584+ebp]                     ;350.9
        add       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGNDATA] ;350.9
        lea       edx, DWORD PTR [-232+ebp]                     ;350.9
        push      eax                                           ;350.9
        push      OFFSET FLAT: __STRLITPACK_270.0.1             ;350.9
        push      edx                                           ;350.9
        lea       edi, DWORD PTR [4+ecx+esi]                    ;350.9
        mov       DWORD PTR [-584+ebp], edi                     ;350.9
        call      _for_read_seq_lis_xmit                        ;350.9
                                ; LOE eax esi
.B1.811:                        ; Preds .B1.346                 ; Infreq
        add       esp, 12                                       ;350.9
                                ; LOE eax esi
.B1.347:                        ; Preds .B1.811                 ; Infreq
        test      eax, eax                                      ;350.9
        jne       .B1.350       ; Prob 50%                      ;350.9
                                ; LOE eax esi
.B1.348:                        ; Preds .B1.347                 ; Infreq
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGNDATA+32] ;350.9
        lea       eax, DWORD PTR [edx*8]                        ;350.9
        lea       ecx, DWORD PTR [eax+edx*4]                    ;350.9
        neg       ecx                                           ;350.9
        lea       eax, DWORD PTR [-680+ebp]                     ;350.9
        add       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGNDATA] ;350.9
        lea       edx, DWORD PTR [-232+ebp]                     ;350.9
        push      eax                                           ;350.9
        push      OFFSET FLAT: __STRLITPACK_271.0.1             ;350.9
        push      edx                                           ;350.9
        lea       edi, DWORD PTR [8+ecx+esi]                    ;350.9
        mov       DWORD PTR [-680+ebp], edi                     ;350.9
        call      _for_read_seq_lis_xmit                        ;350.9
                                ; LOE eax
.B1.812:                        ; Preds .B1.348                 ; Infreq
        add       esp, 12                                       ;350.9
                                ; LOE eax
.B1.350:                        ; Preds .B1.812 .B1.347 .B1.345 ; Infreq
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGNDATA+32] ;351.9
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGNDATA] ;351.9
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM] ;351.9
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;350.9
        lea       edi, DWORD PTR [edx*8]                        ;351.9
        lea       edx, DWORD PTR [edi+edx*4]                    ;351.9
        neg       edx                                           ;351.9
        add       edx, DWORD PTR [-752+ebp]                     ;351.9
        mov       edi, DWORD PTR [ecx+edx]                      ;351.28
        sub       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM+32] ;351.9
        test      eax, eax                                      ;352.12
        mov       esi, DWORD PTR [esi+edi*4]                    ;351.9
        mov       DWORD PTR [ecx+edx], esi                      ;351.9
        jne       .B1.557       ; Prob 5%                       ;352.12
                                ; LOE
.B1.351:                        ; Preds .B1.857 .B1.350         ; Infreq
        push      32                                            ;357.9
        xor       eax, eax                                      ;357.9
        lea       edx, DWORD PTR [-232+ebp]                     ;357.9
        push      eax                                           ;357.9
        push      OFFSET FLAT: __STRLITPACK_274.0.1             ;357.9
        push      -2088435965                                   ;357.9
        push      44                                            ;357.9
        push      edx                                           ;357.9
        mov       DWORD PTR [-232+ebp], eax                     ;357.9
        call      _for_read_seq_lis                             ;357.9
                                ; LOE eax
.B1.813:                        ; Preds .B1.351                 ; Infreq
        add       esp, 24                                       ;357.9
                                ; LOE eax
.B1.352:                        ; Preds .B1.813                 ; Infreq
        test      eax, eax                                      ;357.9
        jne       .B1.361       ; Prob 50%                      ;357.9
                                ; LOE eax
.B1.353:                        ; Preds .B1.352                 ; Infreq
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGNDATA+32] ;357.9
        mov       esi, DWORD PTR [-752+ebp]                     ;357.52
        mov       DWORD PTR [-164+ebp], 1                       ;357.9
        lea       eax, DWORD PTR [edx*8]                        ;357.9
        lea       ecx, DWORD PTR [eax+edx*4]                    ;357.9
        neg       ecx                                           ;357.9
        add       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGNDATA] ;357.9
        mov       esi, DWORD PTR [4+ecx+esi]                    ;357.52
        add       esi, esi                                      ;357.9
        test      esi, esi                                      ;357.9
        jle       .B1.555       ; Prob 16%                      ;357.9
                                ; LOE esi
.B1.354:                        ; Preds .B1.353                 ; Infreq
        mov       DWORD PTR [-424+ebp], 4                       ;357.9
        lea       eax, DWORD PTR [-424+ebp]                     ;357.9
        push      eax                                           ;357.9
        push      OFFSET FLAT: __STRLITPACK_275.0.1             ;357.9
        mov       DWORD PTR [-420+ebp], OFFSET FLAT: _READ_INTERSECTION_CONTROLS$TMPMJAPH.0.1 ;357.9
        lea       edx, DWORD PTR [-232+ebp]                     ;357.9
        push      edx                                           ;357.9
        call      _for_read_seq_lis_xmit                        ;357.9
                                ; LOE eax esi
.B1.814:                        ; Preds .B1.354                 ; Infreq
        add       esp, 12                                       ;357.9
                                ; LOE eax esi
.B1.355:                        ; Preds .B1.814                 ; Infreq
        test      eax, eax                                      ;357.9
        jne       .B1.361       ; Prob 10%                      ;357.9
                                ; LOE eax esi
.B1.356:                        ; Preds .B1.355                 ; Infreq
        lea       edi, DWORD PTR [-232+ebp]                     ;
                                ; LOE esi edi
.B1.357:                        ; Preds .B1.356 .B1.359         ; Infreq
        mov       edx, DWORD PTR [-164+ebp]                     ;357.9
        lea       eax, DWORD PTR [1+edx]                        ;357.9
        mov       DWORD PTR [-164+ebp], eax                     ;357.9
        cmp       eax, esi                                      ;357.9
        jg        .B1.555       ; Prob 1%                       ;357.9
                                ; LOE edx esi edi
.B1.358:                        ; Preds .B1.357                 ; Infreq
        mov       DWORD PTR [-424+ebp], 4                       ;357.9
        lea       eax, DWORD PTR [_READ_INTERSECTION_CONTROLS$TMPMJAPH.0.1+edx*4] ;357.9
        mov       DWORD PTR [-420+ebp], eax                     ;357.9
        lea       edx, DWORD PTR [-424+ebp]                     ;357.9
        push      edx                                           ;357.9
        push      OFFSET FLAT: __STRLITPACK_275.0.1             ;357.9
        push      edi                                           ;357.9
        call      _for_read_seq_lis_xmit                        ;357.9
                                ; LOE eax esi edi
.B1.815:                        ; Preds .B1.358                 ; Infreq
        add       esp, 12                                       ;357.9
                                ; LOE eax esi edi
.B1.359:                        ; Preds .B1.815                 ; Infreq
        test      eax, eax                                      ;357.9
        je        .B1.357       ; Prob 82%                      ;357.9
                                ; LOE eax esi edi
.B1.361:                        ; Preds .B1.855 .B1.359 .B1.355 .B1.352 ; Infreq
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;357.9
        test      eax, eax                                      ;358.18
        jne       .B1.552       ; Prob 5%                       ;358.18
                                ; LOE
.B1.362:                        ; Preds .B1.854 .B1.361         ; Infreq
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGNDATA+32] ;362.3
        lea       eax, DWORD PTR [edx*8]                        ;362.3
        lea       eax, DWORD PTR [eax+edx*4]                    ;362.3
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGNDATA] ;362.3
        neg       eax                                           ;362.3
        add       eax, DWORD PTR [-752+ebp]                     ;362.3
        mov       edi, DWORD PTR [4+edx+eax]                    ;362.16
        lea       ecx, DWORD PTR [edi+edi]                      ;362.3
        test      ecx, ecx                                      ;362.3
        jle       .B1.372       ; Prob 2%                       ;362.3
                                ; LOE eax edx ecx edi
.B1.363:                        ; Preds .B1.362                 ; Infreq
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM] ;373.9
        mov       DWORD PTR [-676+ebp], esi                     ;373.9
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM+32] ;373.9
        mov       DWORD PTR [-728+ebp], esi                     ;373.9
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE] ;377.6
        mov       DWORD PTR [-796+ebp], esi                     ;377.6
        imul      esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE+32], -44 ;
        mov       DWORD PTR [-836+ebp], 1                       ;
        mov       DWORD PTR [-620+ebp], edi                     ;
        mov       DWORD PTR [-812+ebp], esi                     ;
        mov       DWORD PTR [-604+ebp], ecx                     ;
        mov       DWORD PTR [-736+ebp], edx                     ;
        mov       DWORD PTR [-732+ebp], eax                     ;
        mov       edi, DWORD PTR [-836+ebp]                     ;
                                ; LOE edi
.B1.364:                        ; Preds .B1.370 .B1.363         ; Infreq
        mov       eax, edi                                      ;363.13
        and       eax, -2147483647                              ;363.13
        jge       .B1.716       ; Prob 50%                      ;363.13
                                ; LOE eax edi
.B1.717:                        ; Preds .B1.364                 ; Infreq
        sub       eax, 1                                        ;363.13
        or        eax, -2                                       ;363.13
        inc       eax                                           ;363.13
                                ; LOE eax edi
.B1.716:                        ; Preds .B1.364 .B1.717         ; Infreq
        test      eax, eax                                      ;363.24
        jne       .B1.370       ; Prob 50%                      ;363.24
                                ; LOE edi
.B1.365:                        ; Preds .B1.716                 ; Infreq
        mov       esi, DWORD PTR [-736+ebp]                     ;364.48
        mov       eax, DWORD PTR [-732+ebp]                     ;364.48
        mov       ecx, DWORD PTR [_READ_INTERSECTION_CONTROLS$TMPMJAPH.0.1-4+edi*4] ;364.15
        sub       ecx, DWORD PTR [-728+ebp]                     ;364.45
        mov       edx, DWORD PTR [-676+ebp]                     ;364.45
        mov       esi, DWORD PTR [esi+eax]                      ;364.48
        cmp       esi, DWORD PTR [edx+ecx*4]                    ;364.45
        je        .B1.370       ; Prob 50%                      ;364.45
                                ; LOE esi edi
.B1.366:                        ; Preds .B1.365                 ; Infreq
        push      32                                            ;365.7
        mov       DWORD PTR [-232+ebp], 0                       ;365.7
        lea       eax, DWORD PTR [-560+ebp]                     ;365.7
        push      eax                                           ;365.7
        push      OFFSET FLAT: __STRLITPACK_279.0.1             ;365.7
        push      -2088435968                                   ;365.7
        push      911                                           ;365.7
        mov       DWORD PTR [-560+ebp], 39                      ;365.7
        lea       edx, DWORD PTR [-232+ebp]                     ;365.7
        push      edx                                           ;365.7
        mov       DWORD PTR [-556+ebp], OFFSET FLAT: __STRLITPACK_41 ;365.7
        call      _for_write_seq_lis                            ;365.7
                                ; LOE esi edi
.B1.816:                        ; Preds .B1.366                 ; Infreq
        add       esp, 24                                       ;365.7
                                ; LOE esi edi
.B1.367:                        ; Preds .B1.816                 ; Infreq
        push      32                                            ;366.4
        mov       DWORD PTR [-232+ebp], 0                       ;366.4
        lea       eax, DWORD PTR [-552+ebp]                     ;366.4
        push      eax                                           ;366.4
        push      OFFSET FLAT: __STRLITPACK_280.0.1             ;366.4
        push      -2088435968                                   ;366.4
        push      911                                           ;366.4
        mov       DWORD PTR [-552+ebp], 12                      ;366.4
        lea       edx, DWORD PTR [-232+ebp]                     ;366.4
        push      edx                                           ;366.4
        mov       DWORD PTR [-548+ebp], OFFSET FLAT: __STRLITPACK_39 ;366.4
        call      _for_write_seq_lis                            ;366.4
                                ; LOE esi edi
.B1.817:                        ; Preds .B1.367                 ; Infreq
        add       esp, 24                                       ;366.4
                                ; LOE esi edi
.B1.368:                        ; Preds .B1.817                 ; Infreq
        imul      edx, esi, 44                                  ;366.33
        lea       esi, DWORD PTR [-408+ebp]                     ;366.4
        add       edx, DWORD PTR [-796+ebp]                     ;366.4
        mov       eax, DWORD PTR [-812+ebp]                     ;366.4
        push      esi                                           ;366.4
        push      OFFSET FLAT: __STRLITPACK_281.0.1             ;366.4
        mov       ecx, DWORD PTR [36+eax+edx]                   ;366.4
        lea       eax, DWORD PTR [-232+ebp]                     ;366.4
        push      eax                                           ;366.4
        mov       DWORD PTR [-408+ebp], ecx                     ;366.4
        call      _for_write_seq_lis_xmit                       ;366.4
                                ; LOE edi
.B1.818:                        ; Preds .B1.368                 ; Infreq
        add       esp, 12                                       ;366.4
                                ; LOE edi
.B1.369:                        ; Preds .B1.818                 ; Infreq
        push      32                                            ;367.4
        xor       eax, eax                                      ;367.4
        push      eax                                           ;367.4
        push      eax                                           ;367.4
        push      -2088435968                                   ;367.4
        push      eax                                           ;367.4
        push      OFFSET FLAT: __STRLITPACK_282                 ;367.4
        call      _for_stop_core                                ;367.4
                                ; LOE edi
.B1.819:                        ; Preds .B1.369                 ; Infreq
        add       esp, 24                                       ;367.4
                                ; LOE edi
.B1.370:                        ; Preds .B1.819 .B1.716 .B1.365 ; Infreq
        inc       edi                                           ;370.3
        cmp       edi, DWORD PTR [-604+ebp]                     ;370.3
        jle       .B1.364       ; Prob 82%                      ;370.3
                                ; LOE edi
.B1.371:                        ; Preds .B1.370                 ; Infreq
        mov       edi, DWORD PTR [-620+ebp]                     ;
                                ; LOE edi
.B1.372:                        ; Preds .B1.362 .B1.371         ; Infreq
        test      edi, edi                                      ;372.3
        jle       .B1.381       ; Prob 2%                       ;372.3
                                ; LOE edi
.B1.373:                        ; Preds .B1.372                 ; Infreq
        mov       eax, 1                                        ;
        mov       esi, eax                                      ;
        mov       eax, DWORD PTR [-820+ebp]                     ;
        shl       eax, 5                                        ;
        mov       DWORD PTR [-636+ebp], eax                     ;
        mov       DWORD PTR [-620+ebp], edi                     ;
                                ; LOE esi
.B1.374:                        ; Preds .B1.379 .B1.373         ; Infreq
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM] ;373.9
        mov       edi, DWORD PTR [_READ_INTERSECTION_CONTROLS$TMPMJAPH.0.1-8+esi*8] ;373.19
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM+32] ;373.9
        shl       ecx, 2                                        ;373.9
        lea       eax, DWORD PTR [edx+edi*4]                    ;373.9
        mov       edi, DWORD PTR [_READ_INTERSECTION_CONTROLS$TMPMJAPH.0.1-4+esi*8] ;374.13
        sub       eax, ecx                                      ;373.9
        lea       edx, DWORD PTR [edx+edi*4]                    ;374.3
        sub       edx, ecx                                      ;374.3
        mov       eax, DWORD PTR [eax]                          ;373.9
        test      eax, eax                                      ;375.14
        mov       DWORD PTR [-244+ebp], eax                     ;373.9
        mov       edx, DWORD PTR [edx]                          ;374.3
        mov       DWORD PTR [-236+ebp], edx                     ;374.3
        jle       .B1.548       ; Prob 16%                      ;375.14
                                ; LOE edx esi
.B1.375:                        ; Preds .B1.374                 ; Infreq
        test      edx, edx                                      ;375.29
        jle       .B1.548       ; Prob 16%                      ;375.29
                                ; LOE esi
.B1.376:                        ; Preds .B1.852 .B1.375         ; Infreq
        push      OFFSET FLAT: __NLITPACK_1.0.1                 ;380.20
        lea       eax, DWORD PTR [-236+ebp]                     ;380.20
        push      eax                                           ;380.20
        lea       edx, DWORD PTR [-244+ebp]                     ;380.20
        push      edx                                           ;380.20
        call      _DYNUST_NETWORK_MODULE_mp_GETFLINKFROMNODE    ;380.20
                                ; LOE eax esi
.B1.820:                        ; Preds .B1.376                 ; Infreq
        add       esp, 12                                       ;380.20
                                ; LOE eax esi
.B1.377:                        ; Preds .B1.820                 ; Infreq
        test      eax, eax                                      ;381.15
        jle       .B1.544       ; Prob 16%                      ;381.15
                                ; LOE eax esi
.B1.378:                        ; Preds .B1.377                 ; Infreq
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGNAPPRH+32] ;386.12
        shl       edx, 5                                        ;386.12
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGNAPPRH] ;386.12
        sub       ecx, edx                                      ;386.12
        add       ecx, DWORD PTR [-636+ebp]                     ;386.12
        mov       DWORD PTR [-4+ecx+esi*4], eax                 ;386.12
                                ; LOE esi
.B1.379:                        ; Preds .B1.848 .B1.378         ; Infreq
        inc       esi                                           ;388.6
        cmp       esi, DWORD PTR [-620+ebp]                     ;388.6
        jle       .B1.374       ; Prob 82%                      ;388.6
                                ; LOE esi
.B1.381:                        ; Preds .B1.379 .B1.372         ; Infreq
        push      32                                            ;391.6
        xor       eax, eax                                      ;391.6
        lea       edx, DWORD PTR [-232+ebp]                     ;391.6
        push      eax                                           ;391.6
        push      OFFSET FLAT: __STRLITPACK_291.0.1             ;391.6
        push      -2088435965                                   ;391.6
        push      44                                            ;391.6
        push      edx                                           ;391.6
        mov       DWORD PTR [-232+ebp], eax                     ;391.6
        call      _for_read_seq_lis                             ;391.6
                                ; LOE eax
.B1.821:                        ; Preds .B1.381                 ; Infreq
        add       esp, 24                                       ;391.6
                                ; LOE eax
.B1.382:                        ; Preds .B1.821                 ; Infreq
        test      eax, eax                                      ;391.6
        jne       .B1.391       ; Prob 50%                      ;391.6
                                ; LOE eax
.B1.383:                        ; Preds .B1.382                 ; Infreq
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGNDATA+32] ;391.6
        mov       esi, DWORD PTR [-752+ebp]                     ;391.49
        mov       DWORD PTR [-164+ebp], 1                       ;391.6
        lea       eax, DWORD PTR [edx*8]                        ;391.6
        lea       ecx, DWORD PTR [eax+edx*4]                    ;391.6
        neg       ecx                                           ;391.6
        add       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGNDATA] ;391.6
        mov       esi, DWORD PTR [8+ecx+esi]                    ;391.49
        add       esi, esi                                      ;391.6
        test      esi, esi                                      ;391.6
        jle       .B1.542       ; Prob 16%                      ;391.6
                                ; LOE esi
.B1.384:                        ; Preds .B1.383                 ; Infreq
        mov       DWORD PTR [-416+ebp], 4                       ;391.6
        lea       eax, DWORD PTR [-416+ebp]                     ;391.6
        push      eax                                           ;391.6
        push      OFFSET FLAT: __STRLITPACK_292.0.1             ;391.6
        mov       DWORD PTR [-412+ebp], OFFSET FLAT: _READ_INTERSECTION_CONTROLS$TMPMNAPH.0.1 ;391.6
        lea       edx, DWORD PTR [-232+ebp]                     ;391.6
        push      edx                                           ;391.6
        call      _for_read_seq_lis_xmit                        ;391.6
                                ; LOE eax esi
.B1.822:                        ; Preds .B1.384                 ; Infreq
        add       esp, 12                                       ;391.6
                                ; LOE eax esi
.B1.385:                        ; Preds .B1.822                 ; Infreq
        test      eax, eax                                      ;391.6
        jne       .B1.391       ; Prob 10%                      ;391.6
                                ; LOE eax esi
.B1.386:                        ; Preds .B1.385                 ; Infreq
        lea       edi, DWORD PTR [-232+ebp]                     ;
                                ; LOE esi edi
.B1.387:                        ; Preds .B1.386 .B1.389         ; Infreq
        mov       edx, DWORD PTR [-164+ebp]                     ;391.6
        lea       eax, DWORD PTR [1+edx]                        ;391.6
        mov       DWORD PTR [-164+ebp], eax                     ;391.6
        cmp       eax, esi                                      ;391.6
        jg        .B1.542       ; Prob 1%                       ;391.6
                                ; LOE edx esi edi
.B1.388:                        ; Preds .B1.387                 ; Infreq
        mov       DWORD PTR [-416+ebp], 4                       ;391.6
        lea       eax, DWORD PTR [_READ_INTERSECTION_CONTROLS$TMPMNAPH.0.1+edx*4] ;391.6
        mov       DWORD PTR [-412+ebp], eax                     ;391.6
        lea       edx, DWORD PTR [-416+ebp]                     ;391.6
        push      edx                                           ;391.6
        push      OFFSET FLAT: __STRLITPACK_292.0.1             ;391.6
        push      edi                                           ;391.6
        call      _for_read_seq_lis_xmit                        ;391.6
                                ; LOE eax esi edi
.B1.823:                        ; Preds .B1.388                 ; Infreq
        add       esp, 12                                       ;391.6
                                ; LOE eax esi edi
.B1.389:                        ; Preds .B1.823                 ; Infreq
        test      eax, eax                                      ;391.6
        je        .B1.387       ; Prob 82%                      ;391.6
                                ; LOE eax esi edi
.B1.391:                        ; Preds .B1.844 .B1.389 .B1.385 .B1.382 ; Infreq
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;391.6
        test      eax, eax                                      ;392.18
        jne       .B1.539       ; Prob 5%                       ;392.18
                                ; LOE
.B1.392:                        ; Preds .B1.843 .B1.391         ; Infreq
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGNDATA+32] ;397.3
        lea       edx, DWORD PTR [ecx*8]                        ;397.3
        lea       edx, DWORD PTR [edx+ecx*4]                    ;397.3
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGNDATA] ;397.3
        neg       edx                                           ;397.3
        add       edx, DWORD PTR [-752+ebp]                     ;397.3
        mov       esi, DWORD PTR [8+ecx+edx]                    ;397.16
        lea       eax, DWORD PTR [esi+esi]                      ;397.3
        mov       DWORD PTR [-596+ebp], eax                     ;397.3
        test      eax, eax                                      ;397.3
        jle       .B1.402       ; Prob 2%                       ;397.3
                                ; LOE edx ecx esi
.B1.393:                        ; Preds .B1.392                 ; Infreq
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE] ;413.6
        mov       DWORD PTR [-804+ebp], edi                     ;413.6
        imul      edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE+32], -44 ;
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM] ;409.9
        mov       DWORD PTR [-724+ebp], eax                     ;409.9
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM+32] ;409.9
        shl       eax, 2                                        ;
        mov       DWORD PTR [-844+ebp], 1                       ;
        neg       eax                                           ;
        mov       DWORD PTR [-756+ebp], edi                     ;
        mov       DWORD PTR [-740+ebp], eax                     ;
        mov       DWORD PTR [-612+ebp], esi                     ;
        mov       DWORD PTR [-748+ebp], ecx                     ;
        mov       DWORD PTR [-744+ebp], edx                     ;
        mov       edi, DWORD PTR [-844+ebp]                     ;
                                ; LOE edi
.B1.394:                        ; Preds .B1.400 .B1.393         ; Infreq
        mov       eax, edi                                      ;398.13
        and       eax, -2147483647                              ;398.13
        jge       .B1.718       ; Prob 50%                      ;398.13
                                ; LOE eax edi
.B1.719:                        ; Preds .B1.394                 ; Infreq
        sub       eax, 1                                        ;398.13
        or        eax, -2                                       ;398.13
        inc       eax                                           ;398.13
                                ; LOE eax edi
.B1.718:                        ; Preds .B1.394 .B1.719         ; Infreq
        test      eax, eax                                      ;398.24
        jne       .B1.400       ; Prob 50%                      ;398.24
                                ; LOE edi
.B1.395:                        ; Preds .B1.718                 ; Infreq
        mov       esi, DWORD PTR [-748+ebp]                     ;399.48
        mov       edx, DWORD PTR [-744+ebp]                     ;399.48
        mov       ecx, DWORD PTR [-724+ebp]                     ;399.45
        mov       eax, DWORD PTR [_READ_INTERSECTION_CONTROLS$TMPMNAPH.0.1-4+edi*4] ;399.15
        mov       esi, DWORD PTR [esi+edx]                      ;399.48
        mov       edx, DWORD PTR [-740+ebp]                     ;399.45
        add       ecx, edx                                      ;399.45
        cmp       esi, DWORD PTR [ecx+eax*4]                    ;399.45
        je        .B1.400       ; Prob 50%                      ;399.45
                                ; LOE esi edi
.B1.396:                        ; Preds .B1.395                 ; Infreq
        push      32                                            ;400.7
        mov       DWORD PTR [-232+ebp], 0                       ;400.7
        lea       eax, DWORD PTR [-544+ebp]                     ;400.7
        push      eax                                           ;400.7
        push      OFFSET FLAT: __STRLITPACK_296.0.1             ;400.7
        push      -2088435968                                   ;400.7
        push      911                                           ;400.7
        mov       DWORD PTR [-544+ebp], 38                      ;400.7
        lea       edx, DWORD PTR [-232+ebp]                     ;400.7
        push      edx                                           ;400.7
        mov       DWORD PTR [-540+ebp], OFFSET FLAT: __STRLITPACK_27 ;400.7
        call      _for_write_seq_lis                            ;400.7
                                ; LOE esi edi
.B1.824:                        ; Preds .B1.396                 ; Infreq
        add       esp, 24                                       ;400.7
                                ; LOE esi edi
.B1.397:                        ; Preds .B1.824                 ; Infreq
        push      32                                            ;401.4
        mov       DWORD PTR [-232+ebp], 0                       ;401.4
        lea       eax, DWORD PTR [-536+ebp]                     ;401.4
        push      eax                                           ;401.4
        push      OFFSET FLAT: __STRLITPACK_297.0.1             ;401.4
        push      -2088435968                                   ;401.4
        push      911                                           ;401.4
        mov       DWORD PTR [-536+ebp], 12                      ;401.4
        lea       edx, DWORD PTR [-232+ebp]                     ;401.4
        push      edx                                           ;401.4
        mov       DWORD PTR [-532+ebp], OFFSET FLAT: __STRLITPACK_25 ;401.4
        call      _for_write_seq_lis                            ;401.4
                                ; LOE esi edi
.B1.825:                        ; Preds .B1.397                 ; Infreq
        add       esp, 24                                       ;401.4
                                ; LOE esi edi
.B1.398:                        ; Preds .B1.825                 ; Infreq
        imul      edx, esi, 44                                  ;401.33
        lea       esi, DWORD PTR [-400+ebp]                     ;401.4
        add       edx, DWORD PTR [-804+ebp]                     ;401.4
        mov       eax, DWORD PTR [-756+ebp]                     ;401.4
        push      esi                                           ;401.4
        push      OFFSET FLAT: __STRLITPACK_298.0.1             ;401.4
        mov       ecx, DWORD PTR [36+eax+edx]                   ;401.4
        lea       eax, DWORD PTR [-232+ebp]                     ;401.4
        push      eax                                           ;401.4
        mov       DWORD PTR [-400+ebp], ecx                     ;401.4
        call      _for_write_seq_lis_xmit                       ;401.4
                                ; LOE edi
.B1.826:                        ; Preds .B1.398                 ; Infreq
        add       esp, 12                                       ;401.4
                                ; LOE edi
.B1.399:                        ; Preds .B1.826                 ; Infreq
        push      32                                            ;402.4
        xor       eax, eax                                      ;402.4
        push      eax                                           ;402.4
        push      eax                                           ;402.4
        push      -2088435968                                   ;402.4
        push      eax                                           ;402.4
        push      OFFSET FLAT: __STRLITPACK_299                 ;402.4
        call      _for_stop_core                                ;402.4
                                ; LOE edi
.B1.827:                        ; Preds .B1.399                 ; Infreq
        add       esp, 24                                       ;402.4
                                ; LOE edi
.B1.400:                        ; Preds .B1.827 .B1.718 .B1.395 ; Infreq
        inc       edi                                           ;405.3
        cmp       edi, DWORD PTR [-596+ebp]                     ;405.3
        jle       .B1.394       ; Prob 82%                      ;405.3
                                ; LOE edi
.B1.401:                        ; Preds .B1.400                 ; Infreq
        mov       esi, DWORD PTR [-612+ebp]                     ;
                                ; LOE esi
.B1.402:                        ; Preds .B1.401 .B1.392         ; Infreq
        test      esi, esi                                      ;407.6
        jle       .B1.411       ; Prob 2%                       ;407.6
                                ; LOE esi
.B1.403:                        ; Preds .B1.402                 ; Infreq
        mov       eax, 1                                        ;
        mov       edi, eax                                      ;
        mov       eax, DWORD PTR [-820+ebp]                     ;
        shl       eax, 5                                        ;
        mov       DWORD PTR [-628+ebp], eax                     ;
        mov       DWORD PTR [-612+ebp], esi                     ;
        lea       esi, DWORD PTR [-232+ebp]                     ;
                                ; LOE esi edi
.B1.404:                        ; Preds .B1.409 .B1.403         ; Infreq
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM+32] ;409.9
        shl       edx, 2                                        ;409.9
        neg       edx                                           ;409.9
        add       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM] ;409.9
        mov       ecx, DWORD PTR [_READ_INTERSECTION_CONTROLS$TMPMNAPH.0.1-8+edi*8] ;409.19
        mov       eax, DWORD PTR [edx+ecx*4]                    ;409.9
        test      eax, eax                                      ;411.14
        mov       ecx, DWORD PTR [_READ_INTERSECTION_CONTROLS$TMPMNAPH.0.1-4+edi*8] ;410.13
        mov       DWORD PTR [-244+ebp], eax                     ;409.9
        mov       edx, DWORD PTR [edx+ecx*4]                    ;410.3
        mov       DWORD PTR [-236+ebp], edx                     ;410.3
        jle       .B1.535       ; Prob 16%                      ;411.14
                                ; LOE edx esi edi
.B1.405:                        ; Preds .B1.404                 ; Infreq
        test      edx, edx                                      ;411.29
        jle       .B1.535       ; Prob 16%                      ;411.29
                                ; LOE esi edi
.B1.406:                        ; Preds .B1.841 .B1.405         ; Infreq
        push      OFFSET FLAT: __NLITPACK_1.0.1                 ;416.21
        lea       eax, DWORD PTR [-236+ebp]                     ;416.21
        push      eax                                           ;416.21
        lea       edx, DWORD PTR [-244+ebp]                     ;416.21
        push      edx                                           ;416.21
        call      _DYNUST_NETWORK_MODULE_mp_GETFLINKFROMNODE    ;416.21
                                ; LOE eax esi edi
.B1.828:                        ; Preds .B1.406                 ; Infreq
        add       esp, 12                                       ;416.21
                                ; LOE eax esi edi
.B1.407:                        ; Preds .B1.828                 ; Infreq
        test      eax, eax                                      ;417.16
        jle       .B1.531       ; Prob 16%                      ;417.16
                                ; LOE eax esi edi
.B1.408:                        ; Preds .B1.407                 ; Infreq
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGNAPPRH+32] ;422.10
        shl       edx, 5                                        ;422.10
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGNAPPRH] ;422.10
        sub       ecx, edx                                      ;422.10
        add       ecx, DWORD PTR [-628+ebp]                     ;422.10
        mov       DWORD PTR [12+ecx+edi*4], eax                 ;422.10
                                ; LOE esi edi
.B1.409:                        ; Preds .B1.837 .B1.408         ; Infreq
        inc       edi                                           ;424.6
        cmp       edi, DWORD PTR [-612+ebp]                     ;424.6
        jle       .B1.404       ; Prob 82%                      ;424.6
                                ; LOE esi edi
.B1.411:                        ; Preds .B1.409 .B1.402         ; Infreq
        mov       eax, DWORD PTR [-820+ebp]                     ;425.7
        inc       eax                                           ;425.7
        mov       DWORD PTR [-820+ebp], eax                     ;425.7
        cmp       eax, DWORD PTR [-828+ebp]                     ;425.7
        jle       .B1.344       ; Prob 82%                      ;425.7
                                ; LOE
.B1.413:                        ; Preds .B1.411 .B1.263 .B1.342 ; Infreq
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFARCS] ;430.10
        test      eax, eax                                      ;430.10
        mov       DWORD PTR [-620+ebp], eax                     ;430.10
        jle       .B1.447       ; Prob 0%                       ;430.10
                                ; LOE
.B1.414:                        ; Preds .B1.413                 ; Infreq
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;471.9
        mov       DWORD PTR [-804+ebp], edi                     ;471.9
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+44] ;437.17
        mov       DWORD PTR [-828+ebp], edi                     ;437.17
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+40] ;437.17
        mov       DWORD PTR [-748+ebp], edi                     ;437.17
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+32] ;437.17
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+44] ;432.17
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+32] ;432.14
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+40] ;432.17
        mov       DWORD PTR [-812+ebp], edi                     ;437.17
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_MAXMOVE] ;431.12
        test      eax, eax                                      ;431.12
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_PENFORPREVENTMOVE] ;433.11
        mov       DWORD PTR [-836+ebp], ecx                     ;432.17
        mov       DWORD PTR [-820+ebp], esi                     ;432.14
        mov       DWORD PTR [-744+ebp], edx                     ;432.17
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE] ;432.14
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;471.9
        mov       DWORD PTR [-756+ebp], eax                     ;431.12
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE] ;437.17
        mov       DWORD PTR [-796+ebp], edi                     ;433.11
        jle       .B1.435       ; Prob 3%                       ;431.12
                                ; LOE edx ecx esi
.B1.415:                        ; Preds .B1.414                 ; Infreq
        imul      edi, DWORD PTR [-804+ebp], -152               ;
        xor       eax, eax                                      ;
        add       esi, edi                                      ;
        mov       DWORD PTR [-676+ebp], esi                     ;
        imul      esi, DWORD PTR [-820+ebp], -28                ;
        mov       edi, DWORD PTR [-836+ebp]                     ;
        add       ecx, esi                                      ;
        mov       esi, DWORD PTR [-744+ebp]                     ;
        imul      edi, esi                                      ;
        sub       ecx, edi                                      ;
        mov       edi, DWORD PTR [-620+ebp]                     ;
        add       ecx, esi                                      ;
        mov       esi, edi                                      ;
        shr       esi, 31                                       ;
        add       esi, edi                                      ;
        sar       esi, 1                                        ;
        mov       DWORD PTR [-636+ebp], esi                     ;
        mov       esi, DWORD PTR [-748+ebp]                     ;
        mov       edi, DWORD PTR [-828+ebp]                     ;
        imul      edi, esi                                      ;
        sub       edx, edi                                      ;
        mov       edi, DWORD PTR [-812+ebp]                     ;
        shl       edi, 3                                        ;
        neg       edi                                           ;
        add       edi, esi                                      ;
        add       edx, edi                                      ;
        mov       DWORD PTR [-724+ebp], ecx                     ;
        mov       edi, edx                                      ;
        mov       DWORD PTR [-740+ebp], eax                     ;
        mov       DWORD PTR [-736+ebp], ecx                     ;
        mov       DWORD PTR [-752+ebp], eax                     ;
        mov       DWORD PTR [-732+ebp], eax                     ;
        mov       DWORD PTR [-728+ebp], edi                     ;
        mov       DWORD PTR [-844+ebp], edx                     ;
        mov       esi, DWORD PTR [-740+ebp]                     ;
        mov       ecx, DWORD PTR [-724+ebp]                     ;
                                ; LOE ecx esi
.B1.416:                        ; Preds .B1.433 .B1.415         ; Infreq
        cmp       DWORD PTR [-636+ebp], 0                       ;430.10
        jbe       .B1.530       ; Prob 0%                       ;430.10
                                ; LOE ecx esi
.B1.417:                        ; Preds .B1.416                 ; Infreq
        xor       eax, eax                                      ;
        xor       edi, edi                                      ;
        mov       eax, ecx                                      ;
        mov       DWORD PTR [-836+ebp], edi                     ;
        xor       edx, edx                                      ;
        mov       DWORD PTR [-740+ebp], esi                     ;
        mov       DWORD PTR [-724+ebp], ecx                     ;
                                ; LOE eax edx
.B1.418:                        ; Preds .B1.426 .B1.417         ; Infreq
        cmp       BYTE PTR [37+eax], 1                          ;432.68
        jne       .B1.420       ; Prob 84%                      ;432.68
                                ; LOE eax edx
.B1.419:                        ; Preds .B1.418                 ; Infreq
        mov       ecx, DWORD PTR [-796+ebp]                     ;433.11
        mov       DWORD PTR [28+eax], ecx                       ;433.11
                                ; LOE eax edx
.B1.420:                        ; Preds .B1.418 .B1.419         ; Infreq
        mov       ecx, DWORD PTR [-676+ebp]                     ;436.17
        mov       ecx, DWORD PTR [164+edx+ecx]                  ;436.17
        mov       esi, ecx                                      ;436.17
        shl       esi, 5                                        ;436.17
        lea       edi, DWORD PTR [ecx*4]                        ;436.17
        sub       esi, edi                                      ;436.17
        mov       edi, DWORD PTR [-724+ebp]                     ;436.109
        cmp       BYTE PTR [8+esi+edi], 1                       ;436.109
        jne       .B1.422       ; Prob 84%                      ;436.109
                                ; LOE eax edx ecx
.B1.421:                        ; Preds .B1.420                 ; Infreq
        mov       edi, DWORD PTR [-728+ebp]                     ;437.17
        mov       esi, DWORD PTR [-796+ebp]                     ;437.17
        mov       DWORD PTR [edi+ecx*8], esi                    ;437.17
                                ; LOE eax edx
.B1.422:                        ; Preds .B1.420 .B1.421         ; Infreq
        cmp       BYTE PTR [65+eax], 1                          ;432.68
        jne       .B1.424       ; Prob 84%                      ;432.68
                                ; LOE eax edx
.B1.423:                        ; Preds .B1.422                 ; Infreq
        mov       ecx, DWORD PTR [-796+ebp]                     ;433.11
        mov       DWORD PTR [56+eax], ecx                       ;433.11
                                ; LOE eax edx
.B1.424:                        ; Preds .B1.422 .B1.423         ; Infreq
        mov       ecx, DWORD PTR [-676+ebp]                     ;436.17
        mov       ecx, DWORD PTR [316+edx+ecx]                  ;436.17
        mov       esi, ecx                                      ;436.17
        shl       esi, 5                                        ;436.17
        lea       edi, DWORD PTR [ecx*4]                        ;436.17
        sub       esi, edi                                      ;436.17
        mov       edi, DWORD PTR [-724+ebp]                     ;436.109
        cmp       BYTE PTR [8+esi+edi], 1                       ;436.109
        jne       .B1.426       ; Prob 84%                      ;436.109
                                ; LOE eax edx ecx
.B1.425:                        ; Preds .B1.424                 ; Infreq
        mov       edi, DWORD PTR [-728+ebp]                     ;437.17
        mov       esi, DWORD PTR [-796+ebp]                     ;437.17
        mov       DWORD PTR [edi+ecx*8], esi                    ;437.17
                                ; LOE eax edx
.B1.426:                        ; Preds .B1.424 .B1.425         ; Infreq
        mov       ecx, DWORD PTR [-836+ebp]                     ;430.10
        add       eax, 56                                       ;430.10
        inc       ecx                                           ;430.10
        add       edx, 304                                      ;430.10
        mov       DWORD PTR [-836+ebp], ecx                     ;430.10
        cmp       ecx, DWORD PTR [-636+ebp]                     ;430.10
        jb        .B1.418       ; Prob 63%                      ;430.10
                                ; LOE eax edx ecx cl ch
.B1.427:                        ; Preds .B1.426                 ; Infreq
        mov       edi, ecx                                      ;
        lea       edx, DWORD PTR [1+edi+edi]                    ;430.10
        mov       esi, DWORD PTR [-740+ebp]                     ;
        mov       ecx, DWORD PTR [-724+ebp]                     ;
                                ; LOE edx ecx esi
.B1.428:                        ; Preds .B1.427 .B1.530         ; Infreq
        lea       eax, DWORD PTR [-1+edx]                       ;430.10
        cmp       eax, DWORD PTR [-620+ebp]                     ;430.10
        jae       .B1.433       ; Prob 0%                       ;430.10
                                ; LOE edx ecx esi
.B1.429:                        ; Preds .B1.428                 ; Infreq
        mov       eax, edx                                      ;432.68
        lea       edi, DWORD PTR [edx*4]                        ;432.68
        shl       eax, 5                                        ;432.68
        sub       eax, edi                                      ;432.68
        add       eax, DWORD PTR [-736+ebp]                     ;432.68
        cmp       BYTE PTR [9+eax+esi], 1                       ;432.68
        jne       .B1.431       ; Prob 84%                      ;432.68
                                ; LOE eax edx ecx esi
.B1.430:                        ; Preds .B1.429                 ; Infreq
        mov       edi, DWORD PTR [-796+ebp]                     ;433.11
        mov       DWORD PTR [eax+esi], edi                      ;433.11
                                ; LOE edx ecx esi
.B1.431:                        ; Preds .B1.429 .B1.430         ; Infreq
        imul      eax, edx, 152                                 ;436.109
        mov       edx, DWORD PTR [-676+ebp]                     ;436.17
        mov       eax, DWORD PTR [12+eax+edx]                   ;436.17
        mov       edx, eax                                      ;436.17
        shl       edx, 5                                        ;436.17
        lea       edi, DWORD PTR [eax*4]                        ;436.17
        sub       edx, edi                                      ;436.17
        add       edx, DWORD PTR [-736+ebp]                     ;436.109
        cmp       BYTE PTR [8+edx+esi], 1                       ;436.109
        jne       .B1.433       ; Prob 84%                      ;436.109
                                ; LOE eax ecx esi
.B1.432:                        ; Preds .B1.431                 ; Infreq
        mov       edx, DWORD PTR [-844+ebp]                     ;437.17
        lea       edi, DWORD PTR [edx+eax*8]                    ;437.17
        mov       eax, DWORD PTR [-796+ebp]                     ;437.17
        mov       edx, DWORD PTR [-732+ebp]                     ;437.17
        mov       DWORD PTR [edi+edx], eax                      ;437.17
                                ; LOE ecx esi
.B1.433:                        ; Preds .B1.431 .B1.428 .B1.432 ; Infreq
        mov       eax, DWORD PTR [-748+ebp]                     ;431.12
        mov       edi, DWORD PTR [-752+ebp]                     ;431.12
        inc       edi                                           ;431.12
        mov       edx, DWORD PTR [-744+ebp]                     ;431.12
        add       ecx, edx                                      ;431.12
        add       DWORD PTR [-728+ebp], eax                     ;431.12
        add       esi, edx                                      ;431.12
        add       DWORD PTR [-732+ebp], eax                     ;431.12
        mov       DWORD PTR [-752+ebp], edi                     ;431.12
        cmp       edi, DWORD PTR [-756+ebp]                     ;431.12
        jb        .B1.416       ; Prob 82%                      ;431.12
                                ; LOE ecx esi
.B1.435:                        ; Preds .B1.433 .B1.414         ; Infreq
        imul      eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE+32], -84 ;
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_NSIGN+40] ;446.23
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_NSIGN+44] ;446.23
        imul      edx, edi                                      ;
        add       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE] ;
        mov       DWORD PTR [-596+ebp], eax                     ;
        imul      eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32], -152 ;
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_NSIGN] ;446.16
        sub       ecx, edx                                      ;
        add       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_NSIGN+32] ;446.16
        mov       DWORD PTR [-612+ebp], eax                     ;
        lea       eax, DWORD PTR [edi+edi*4]                    ;446.23
        shl       esi, 2                                        ;
        lea       edx, DWORD PTR [ecx+eax]                      ;
        sub       eax, esi                                      ;
        sub       edx, esi                                      ;
        mov       DWORD PTR [-844+ebp], edx                     ;
        add       eax, ecx                                      ;
        mov       DWORD PTR [-676+ebp], eax                     ;
        lea       edx, DWORD PTR [ecx+edi*2]                    ;
        sub       edx, esi                                      ;
        lea       eax, DWORD PTR [edi+edi*2]                    ;
        mov       DWORD PTR [-836+ebp], edx                     ;
        lea       edx, DWORD PTR [ecx+eax*2]                    ;
        sub       edx, esi                                      ;
        mov       DWORD PTR [-828+ebp], edx                     ;
        lea       edx, DWORD PTR [edi*8]                        ;
        sub       edx, edi                                      ;
        add       edx, ecx                                      ;
        add       ecx, eax                                      ;
        sub       edx, esi                                      ;
        sub       ecx, esi                                      ;
        mov       DWORD PTR [-820+ebp], edx                     ;
        imul      edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32], -900 ;
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_KGPOINT+32] ;
        shl       eax, 2                                        ;
        neg       eax                                           ;
        add       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_KGPOINT] ;
        mov       DWORD PTR [-796+ebp], 0                       ;
        add       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;
        mov       DWORD PTR [-812+ebp], ecx                     ;
        mov       DWORD PTR [-804+ebp], eax                     ;
        mov       DWORD PTR [-604+ebp], edx                     ;
        mov       DWORD PTR [-404+ebp], edi                     ;
        mov       ecx, DWORD PTR [-796+ebp]                     ;
                                ; LOE ecx
.B1.436:                        ; Preds .B1.438 .B1.435         ; Infreq
        imul      eax, ecx, 152                                 ;444.79
        mov       edx, DWORD PTR [-612+ebp]                     ;444.14
        mov       esi, DWORD PTR [-596+ebp]                     ;444.14
        mov       DWORD PTR [-428+ebp], eax                     ;444.79
        mov       edx, DWORD PTR [176+edx+eax]                  ;444.14
        imul      edi, edx, 84                                  ;444.14
        mov       eax, DWORD PTR [32+esi+edi]                   ;444.14
        shl       eax, 2                                        ;444.79
        neg       eax                                           ;444.79
        add       eax, DWORD PTR [esi+edi]                      ;444.79
        mov       eax, DWORD PTR [8+eax]                        ;444.14
        cmp       eax, 4                                        ;444.79
        je        .B1.513       ; Prob 16%                      ;444.79
                                ; LOE eax edx ecx
.B1.437:                        ; Preds .B1.436                 ; Infreq
        cmp       eax, 5                                        ;455.79
        lea       esi, DWORD PTR [1+ecx]                        ;
        je        .B1.496       ; Prob 16%                      ;455.79
                                ; LOE edx ecx esi
.B1.438:                        ; Preds .B1.528 .B1.513 .B1.511 .B1.496 .B1.437
                                ;                               ; Infreq
        mov       ecx, esi                                      ;443.9
        cmp       esi, DWORD PTR [-620+ebp]                     ;443.9
        jb        .B1.436       ; Prob 81%                      ;443.9
                                ; LOE ecx
.B1.439:                        ; Preds .B1.438                 ; Infreq
        imul      ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE+32], -84 ;
        mov       eax, 1                                        ;
        imul      edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE+32], -44 ;
        imul      edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32], -152 ;
        add       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE] ;
        mov       esi, 152                                      ;
        add       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE] ;
        add       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;
        mov       DWORD PTR [-844+ebp], edx                     ;
        mov       DWORD PTR [-756+ebp], ecx                     ;
        mov       DWORD PTR [-740+ebp], eax                     ;
                                ; LOE esi edi
.B1.440:                        ; Preds .B1.445 .B1.439         ; Infreq
        movsx     eax, WORD PTR [148+esi+edi]                   ;471.12
        cmp       eax, 1                                        ;471.50
        je        .B1.461       ; Prob 16%                      ;471.50
                                ; LOE eax esi edi
.B1.441:                        ; Preds .B1.440                 ; Infreq
        cmp       eax, 2                                        ;471.96
        je        .B1.461       ; Prob 16%                      ;471.96
                                ; LOE eax esi edi
.B1.442:                        ; Preds .B1.441                 ; Infreq
        cmp       eax, 7                                        ;471.142
        je        .B1.461       ; Prob 16%                      ;471.142
                                ; LOE eax esi edi
.B1.443:                        ; Preds .B1.442                 ; Infreq
        cmp       eax, 9                                        ;471.188
        je        .B1.461       ; Prob 16%                      ;471.188
                                ; LOE eax esi edi
.B1.444:                        ; Preds .B1.443                 ; Infreq
        cmp       eax, 10                                       ;471.234
        je        .B1.461       ; Prob 16%                      ;471.234
                                ; LOE esi edi
.B1.445:                        ; Preds .B1.833 .B1.491 .B1.479 .B1.478 .B1.444
                                ;                               ; Infreq
        mov       eax, DWORD PTR [-740+ebp]                     ;485.4
        add       esi, 152                                      ;485.4
        inc       eax                                           ;485.4
        mov       DWORD PTR [-740+ebp], eax                     ;485.4
        cmp       eax, DWORD PTR [-620+ebp]                     ;485.4
        jle       .B1.440       ; Prob 82%                      ;485.4
                                ; LOE esi edi
.B1.447:                        ; Preds .B1.445 .B1.413         ; Infreq
        mov       eax, DWORD PTR [-788+ebp]                     ;490.2
        mov       esp, eax                                      ;490.2
                                ; LOE
.B1.829:                        ; Preds .B1.447                 ; Infreq
        mov       esi, DWORD PTR [-780+ebp]                     ;490.2
        mov       edi, DWORD PTR [-772+ebp]                     ;490.2
        mov       esp, ebp                                      ;490.2
        pop       ebp                                           ;490.2
        mov       esp, ebx                                      ;490.2
        pop       ebx                                           ;490.2
        ret                                                     ;490.2
                                ; LOE
.B1.448:                        ; Preds .B1.194                 ; Infreq
        mov       eax, 1                                        ;
        jmp       .B1.198       ; Prob 100%                     ;
                                ; LOE eax edx esi
.B1.449:                        ; Preds .B1.231                 ; Infreq
        mov       eax, 1                                        ;
        jmp       .B1.239       ; Prob 100%                     ;
                                ; LOE eax esi edi
.B1.450:                        ; Preds .B1.283                 ; Infreq
        mov       eax, 1                                        ;
        jmp       .B1.287       ; Prob 100%                     ;
                                ; LOE eax edx ecx esi
.B1.451:                        ; Preds .B1.277                 ; Infreq
        mov       eax, 1                                        ;
        jmp       .B1.281       ; Prob 100%                     ;
                                ; LOE eax edx ecx esi
.B1.452:                        ; Preds .B1.271                 ; Infreq
        mov       eax, 1                                        ;
        jmp       .B1.275       ; Prob 100%                     ;
                                ; LOE eax edx ecx esi
.B1.453:                        ; Preds .B1.336                 ; Infreq
        mov       eax, 1                                        ;
        jmp       .B1.340       ; Prob 100%                     ;
                                ; LOE eax edx ecx esi
.B1.454:                        ; Preds .B1.330                 ; Infreq
        mov       eax, 1                                        ;
        jmp       .B1.334       ; Prob 100%                     ;
                                ; LOE eax edx ecx esi
.B1.455:                        ; Preds .B1.324                 ; Infreq
        mov       eax, 1                                        ;
        jmp       .B1.328       ; Prob 100%                     ;
                                ; LOE eax edx ecx esi
.B1.456:                        ; Preds .B1.318                 ; Infreq
        mov       eax, 1                                        ;
        jmp       .B1.322       ; Prob 100%                     ;
                                ; LOE eax edx ecx esi
.B1.457:                        ; Preds .B1.312                 ; Infreq
        mov       eax, 1                                        ;
        jmp       .B1.316       ; Prob 100%                     ;
                                ; LOE eax edx ecx esi
.B1.458:                        ; Preds .B1.306                 ; Infreq
        mov       eax, 1                                        ;
        jmp       .B1.310       ; Prob 100%                     ;
                                ; LOE eax edx ecx esi
.B1.459:                        ; Preds .B1.300                 ; Infreq
        mov       eax, 1                                        ;
        jmp       .B1.304       ; Prob 100%                     ;
                                ; LOE eax edx ecx esi
.B1.460:                        ; Preds .B1.294                 ; Infreq
        mov       eax, 1                                        ;
        jmp       .B1.298       ; Prob 100%                     ;
                                ; LOE eax edx ecx esi
.B1.461:                        ; Preds .B1.444 .B1.443 .B1.442 .B1.441 .B1.440
                                ;                               ; Infreq
        mov       eax, DWORD PTR [24+esi+edi]                   ;472.8
        imul      ecx, eax, 84                                  ;472.8
        mov       edx, DWORD PTR [-756+ebp]                     ;472.8
        mov       DWORD PTR [-796+ebp], eax                     ;472.8
        mov       eax, DWORD PTR [32+edx+ecx]                   ;472.8
        shl       eax, 2                                        ;472.77
        neg       eax                                           ;472.77
        add       eax, DWORD PTR [edx+ecx]                      ;472.77
        mov       eax, DWORD PTR [8+eax]                        ;472.8
        cmp       eax, 2                                        ;472.73
        je        .B1.477       ; Prob 16%                      ;472.73
                                ; LOE eax esi edi
.B1.462:                        ; Preds .B1.461                 ; Infreq
        cmp       eax, 1                                        ;472.147
        je        .B1.477       ; Prob 16%                      ;472.147
                                ; LOE esi edi
.B1.463:                        ; Preds .B1.462                 ; Infreq
        movsx     edx, BYTE PTR [32+esi+edi]                    ;473.18
        xor       eax, eax                                      ;
        mov       DWORD PTR [-612+ebp], edx                     ;473.18
        test      edx, edx                                      ;473.7
        jle       .B1.474       ; Prob 2%                       ;473.7
                                ; LOE eax esi edi
.B1.464:                        ; Preds .B1.463                 ; Infreq
        mov       ecx, DWORD PTR [64+esi+edi]                   ;474.15
        mov       edx, DWORD PTR [68+esi+edi]                   ;474.15
        imul      edx, ecx                                      ;
        mov       DWORD PTR [-820+ebp], 1                       ;
        mov       DWORD PTR [-828+ebp], ecx                     ;
        mov       DWORD PTR [-628+ebp], ecx                     ;474.15
        mov       ecx, DWORD PTR [36+esi+edi]                   ;474.15
        sub       ecx, edx                                      ;
        mov       DWORD PTR [-636+ebp], ecx                     ;
        mov       DWORD PTR [-836+ebp], esi                     ;
        mov       ecx, DWORD PTR [-828+ebp]                     ;
        mov       edx, DWORD PTR [-820+ebp]                     ;
                                ; LOE eax edx ecx edi
.B1.465:                        ; Preds .B1.472 .B1.464         ; Infreq
        mov       esi, DWORD PTR [-636+ebp]                     ;474.18
        imul      esi, DWORD PTR [ecx+esi], 152                 ;474.18
        movsx     esi, WORD PTR [148+edi+esi]                   ;474.18
        cmp       esi, 1                                        ;474.94
        je        .B1.471       ; Prob 16%                      ;474.94
                                ; LOE eax edx ecx esi edi
.B1.466:                        ; Preds .B1.465                 ; Infreq
        cmp       esi, 2                                        ;474.178
        je        .B1.471       ; Prob 16%                      ;474.178
                                ; LOE eax edx ecx esi edi
.B1.467:                        ; Preds .B1.466                 ; Infreq
        cmp       esi, 3                                        ;474.262
        je        .B1.471       ; Prob 16%                      ;474.262
                                ; LOE eax edx ecx esi edi
.B1.468:                        ; Preds .B1.467                 ; Infreq
        cmp       esi, 4                                        ;474.346
        je        .B1.471       ; Prob 16%                      ;474.346
                                ; LOE eax edx ecx esi edi
.B1.469:                        ; Preds .B1.468                 ; Infreq
        cmp       esi, 9                                        ;474.430
        je        .B1.471       ; Prob 16%                      ;474.430
                                ; LOE eax edx ecx esi edi
.B1.470:                        ; Preds .B1.469                 ; Infreq
        cmp       esi, 10                                       ;4194305.514
        jne       .B1.472       ; Prob 84%                      ;4194305.514
                                ; LOE eax edx ecx edi
.B1.471:                        ; Preds .B1.470 .B1.469 .B1.468 .B1.467 .B1.466
                                ;       .B1.465                 ; Infreq
        mov       eax, -1                                       ;4194305.521
                                ; LOE eax edx ecx edi
.B1.472:                        ; Preds .B1.470 .B1.471         ; Infreq
        inc       edx                                           ;475.4
        add       ecx, DWORD PTR [-628+ebp]                     ;475.4
        cmp       edx, DWORD PTR [-612+ebp]                     ;475.4
        jle       .B1.465       ; Prob 82%                      ;475.4
                                ; LOE eax edx ecx edi
.B1.473:                        ; Preds .B1.472                 ; Infreq
        mov       esi, DWORD PTR [-836+ebp]                     ;
                                ; LOE eax esi edi
.B1.474:                        ; Preds .B1.463 .B1.473         ; Infreq
        test      al, 1                                         ;476.10
        je        .B1.477       ; Prob 60%                      ;476.10
                                ; LOE esi edi
.B1.475:                        ; Preds .B1.474                 ; Infreq
        mov       eax, DWORD PTR [28+esi+edi]                   ;476.94
        imul      ecx, eax, 44                                  ;476.94
        mov       edx, DWORD PTR [-844+ebp]                     ;476.20
        push      32                                            ;476.20
        mov       DWORD PTR [-748+ebp], eax                     ;476.94
        mov       eax, DWORD PTR [36+edx+ecx]                   ;476.20
        lea       edx, DWORD PTR [-752+ebp]                     ;476.20
        push      OFFSET FLAT: READ_INTERSECTION_CONTROLS$format_pack.0.1+388 ;476.20
        push      edx                                           ;476.20
        push      OFFSET FLAT: __STRLITPACK_308.0.1             ;476.20
        push      -2088435968                                   ;476.20
        push      511                                           ;476.20
        mov       DWORD PTR [-232+ebp], 0                       ;476.20
        lea       ecx, DWORD PTR [-232+ebp]                     ;476.20
        push      ecx                                           ;476.20
        mov       DWORD PTR [-752+ebp], eax                     ;476.20
        call      _for_write_seq_fmt                            ;476.20
                                ; LOE esi edi
.B1.830:                        ; Preds .B1.475                 ; Infreq
        add       esp, 28                                       ;476.20
                                ; LOE esi edi
.B1.476:                        ; Preds .B1.830                 ; Infreq
        imul      edx, DWORD PTR [-796+ebp], 44                 ;476.170
        mov       eax, DWORD PTR [-844+ebp]                     ;476.20
        mov       ecx, DWORD PTR [36+eax+edx]                   ;476.20
        lea       eax, DWORD PTR [-744+ebp]                     ;476.20
        push      eax                                           ;476.20
        push      OFFSET FLAT: __STRLITPACK_309.0.1             ;476.20
        mov       DWORD PTR [-744+ebp], ecx                     ;476.20
        lea       edx, DWORD PTR [-232+ebp]                     ;476.20
        push      edx                                           ;476.20
        call      _for_write_seq_fmt_xmit                       ;476.20
                                ; LOE esi edi
.B1.831:                        ; Preds .B1.476                 ; Infreq
        add       esp, 12                                       ;476.20
        jmp       .B1.478       ; Prob 100%                     ;476.20
                                ; LOE esi edi
.B1.477:                        ; Preds .B1.461 .B1.462 .B1.474 ; Infreq
        mov       eax, DWORD PTR [28+esi+edi]                   ;478.8
        mov       DWORD PTR [-748+ebp], eax                     ;478.8
                                ; LOE esi edi
.B1.478:                        ; Preds .B1.831 .B1.477         ; Infreq
        imul      edx, DWORD PTR [-748+ebp], 84                 ;478.8
        mov       eax, DWORD PTR [-756+ebp]                     ;478.8
        mov       ecx, DWORD PTR [32+eax+edx]                   ;478.8
        shl       ecx, 2                                        ;478.77
        neg       ecx                                           ;478.77
        add       ecx, DWORD PTR [eax+edx]                      ;478.77
        mov       eax, DWORD PTR [8+ecx]                        ;478.8
        cmp       eax, 2                                        ;478.73
        je        .B1.445       ; Prob 16%                      ;478.73
                                ; LOE eax esi edi
.B1.479:                        ; Preds .B1.478                 ; Infreq
        cmp       eax, 1                                        ;478.147
        je        .B1.445       ; Prob 16%                      ;478.147
                                ; LOE esi edi
.B1.480:                        ; Preds .B1.479                 ; Infreq
        movsx     eax, BYTE PTR [108+esi+edi]                   ;479.18
        xor       edx, edx                                      ;
        mov       DWORD PTR [-676+ebp], eax                     ;479.18
        test      eax, eax                                      ;479.7
        jle       .B1.491       ; Prob 2%                       ;479.7
                                ; LOE edx esi edi
.B1.481:                        ; Preds .B1.480                 ; Infreq
        mov       eax, 1                                        ;
        mov       DWORD PTR [-804+ebp], eax                     ;
        mov       eax, DWORD PTR [140+esi+edi]                  ;480.15
        mov       ecx, DWORD PTR [144+esi+edi]                  ;480.15
        imul      ecx, eax                                      ;
        mov       DWORD PTR [-812+ebp], eax                     ;
        mov       DWORD PTR [-724+ebp], eax                     ;480.15
        mov       eax, DWORD PTR [112+esi+edi]                  ;480.15
        sub       eax, ecx                                      ;
        mov       DWORD PTR [-732+ebp], eax                     ;
        mov       DWORD PTR [-836+ebp], esi                     ;
        mov       DWORD PTR [-604+ebp], edi                     ;
        mov       eax, DWORD PTR [-812+ebp]                     ;
        mov       ecx, DWORD PTR [-804+ebp]                     ;
                                ; LOE eax edx ecx
.B1.482:                        ; Preds .B1.489 .B1.481         ; Infreq
        mov       esi, DWORD PTR [-732+ebp]                     ;480.18
        mov       edi, DWORD PTR [-604+ebp]                     ;480.18
        imul      esi, DWORD PTR [eax+esi], 152                 ;480.18
        movsx     esi, WORD PTR [148+edi+esi]                   ;480.18
        cmp       esi, 1                                        ;480.95
        je        .B1.488       ; Prob 16%                      ;480.95
                                ; LOE eax edx ecx esi edi
.B1.483:                        ; Preds .B1.482                 ; Infreq
        cmp       esi, 2                                        ;480.180
        je        .B1.488       ; Prob 16%                      ;480.180
                                ; LOE eax edx ecx esi edi
.B1.484:                        ; Preds .B1.483                 ; Infreq
        cmp       esi, 3                                        ;480.265
        je        .B1.488       ; Prob 16%                      ;480.265
                                ; LOE eax edx ecx esi edi
.B1.485:                        ; Preds .B1.484                 ; Infreq
        cmp       esi, 4                                        ;480.350
        je        .B1.488       ; Prob 16%                      ;480.350
                                ; LOE eax edx ecx esi edi
.B1.486:                        ; Preds .B1.485                 ; Infreq
        cmp       esi, 9                                        ;480.435
        je        .B1.488       ; Prob 16%                      ;480.435
                                ; LOE eax edx ecx esi edi
.B1.487:                        ; Preds .B1.486                 ; Infreq
        cmp       esi, 10                                       ;4194305.520
        jne       .B1.489       ; Prob 84%                      ;4194305.520
                                ; LOE eax edx ecx edi
.B1.488:                        ; Preds .B1.487 .B1.486 .B1.485 .B1.484 .B1.483
                                ;       .B1.482                 ; Infreq
        mov       edx, -1                                       ;4194305.527
                                ; LOE eax edx ecx edi
.B1.489:                        ; Preds .B1.487 .B1.488         ; Infreq
        inc       ecx                                           ;481.4
        add       eax, DWORD PTR [-724+ebp]                     ;481.4
        cmp       ecx, DWORD PTR [-676+ebp]                     ;481.4
        jle       .B1.482       ; Prob 82%                      ;481.4
                                ; LOE eax edx ecx edi
.B1.490:                        ; Preds .B1.489                 ; Infreq
        mov       esi, DWORD PTR [-836+ebp]                     ;
                                ; LOE edx esi edi
.B1.491:                        ; Preds .B1.480 .B1.490         ; Infreq
        test      dl, 1                                         ;482.16
        je        .B1.445       ; Prob 60%                      ;482.16
                                ; LOE esi edi
.B1.492:                        ; Preds .B1.491                 ; Infreq
        imul      edx, DWORD PTR [-748+ebp], 44                 ;482.100
        mov       eax, DWORD PTR [-844+ebp]                     ;482.26
        push      32                                            ;482.26
        push      OFFSET FLAT: READ_INTERSECTION_CONTROLS$format_pack.0.1+476 ;482.26
        mov       ecx, DWORD PTR [36+eax+edx]                   ;482.26
        lea       eax, DWORD PTR [-736+ebp]                     ;482.26
        push      eax                                           ;482.26
        push      OFFSET FLAT: __STRLITPACK_310.0.1             ;482.26
        push      -2088435968                                   ;482.26
        push      511                                           ;482.26
        mov       DWORD PTR [-232+ebp], 0                       ;482.26
        lea       edx, DWORD PTR [-232+ebp]                     ;482.26
        push      edx                                           ;482.26
        mov       DWORD PTR [-736+ebp], ecx                     ;482.26
        call      _for_write_seq_fmt                            ;482.26
                                ; LOE esi edi
.B1.832:                        ; Preds .B1.492                 ; Infreq
        add       esp, 28                                       ;482.26
                                ; LOE esi edi
.B1.493:                        ; Preds .B1.832                 ; Infreq
        imul      edx, DWORD PTR [-796+ebp], 44                 ;482.176
        mov       eax, DWORD PTR [-844+ebp]                     ;482.26
        mov       ecx, DWORD PTR [36+eax+edx]                   ;482.26
        lea       eax, DWORD PTR [-728+ebp]                     ;482.26
        push      eax                                           ;482.26
        push      OFFSET FLAT: __STRLITPACK_311.0.1             ;482.26
        mov       DWORD PTR [-728+ebp], ecx                     ;482.26
        lea       edx, DWORD PTR [-232+ebp]                     ;482.26
        push      edx                                           ;482.26
        call      _for_write_seq_fmt_xmit                       ;482.26
                                ; LOE esi edi
.B1.833:                        ; Preds .B1.493                 ; Infreq
        add       esp, 12                                       ;482.26
        jmp       .B1.445       ; Prob 100%                     ;482.26
                                ; LOE esi edi
.B1.496:                        ; Preds .B1.437                 ; Infreq
        mov       edi, DWORD PTR [-804+ebp]                     ;456.14
        mov       eax, DWORD PTR [edi+edx*4]                    ;456.14
        mov       edi, DWORD PTR [4+edi+edx*4]                  ;456.62
        dec       edi                                           ;456.14
        mov       edx, edi                                      ;456.14
        sub       edx, eax                                      ;456.14
        inc       edx                                           ;456.14
        mov       DWORD PTR [-796+ebp], eax                     ;456.14
        cmp       edi, eax                                      ;456.14
        mov       DWORD PTR [-628+ebp], edx                     ;456.14
        jl        .B1.438       ; Prob 10%                      ;456.14
                                ; LOE eax ecx esi al ah
.B1.497:                        ; Preds .B1.496                 ; Infreq
        mov       edx, DWORD PTR [-844+ebp]                     ;
        mov       DWORD PTR [-588+ebp], 0                       ;
        mov       DWORD PTR [-396+ebp], esi                     ;
        mov       esi, eax                                      ;
        lea       edi, DWORD PTR [edx+eax*4]                    ;
        mov       edx, DWORD PTR [-836+ebp]                     ;
        mov       DWORD PTR [-728+ebp], edi                     ;
        lea       edi, DWORD PTR [edx+eax*4]                    ;
        mov       edx, DWORD PTR [-828+ebp]                     ;
        mov       DWORD PTR [-436+ebp], edi                     ;
        lea       edi, DWORD PTR [edx+eax*4]                    ;
        mov       DWORD PTR [-740+ebp], edi                     ;
        mov       edx, DWORD PTR [-820+ebp]                     ;
        lea       eax, DWORD PTR [edx+eax*4]                    ;
        mov       DWORD PTR [-744+ebp], eax                     ;
        imul      eax, ecx, 900                                 ;
        mov       DWORD PTR [-580+ebp], eax                     ;
        mov       ecx, DWORD PTR [-588+ebp]                     ;
                                ; LOE ecx esi
.B1.498:                        ; Preds .B1.510 .B1.497         ; Infreq
        mov       edx, DWORD PTR [-676+ebp]                     ;457.24
        mov       edx, DWORD PTR [edx+esi*4]                    ;457.24
        lea       eax, DWORD PTR [5+edx]                        ;457.17
        cmp       eax, 6                                        ;457.17
        jl        .B1.510       ; Prob 50%                      ;457.17
                                ; LOE edx ecx esi
.B1.499:                        ; Preds .B1.498                 ; Infreq
        mov       eax, edx                                      ;457.17
        shr       eax, 31                                       ;457.17
        add       eax, edx                                      ;457.17
        sar       eax, 1                                        ;457.17
        mov       DWORD PTR [-372+ebp], eax                     ;457.17
        test      eax, eax                                      ;457.17
        jbe       .B1.512       ; Prob 11%                      ;457.17
                                ; LOE edx ecx esi
.B1.500:                        ; Preds .B1.499                 ; Infreq
        mov       edi, DWORD PTR [-740+ebp]                     ;
        xor       eax, eax                                      ;
        mov       DWORD PTR [-588+ebp], ecx                     ;
        mov       DWORD PTR [-756+ebp], edx                     ;
        lea       edi, DWORD PTR [edi+ecx*4]                    ;
        mov       DWORD PTR [-380+ebp], edi                     ;
        mov       edi, DWORD PTR [-744+ebp]                     ;
        mov       DWORD PTR [-796+ebp], esi                     ;
        lea       edi, DWORD PTR [edi+ecx*4]                    ;
        mov       DWORD PTR [-388+ebp], edi                     ;
        mov       ecx, eax                                      ;
                                ; LOE eax ecx
.B1.501:                        ; Preds .B1.505 .B1.500         ; Infreq
        mov       esi, DWORD PTR [-380+ebp]                     ;458.25
        mov       edx, DWORD PTR [-396+ebp]                     ;458.25
        cmp       edx, DWORD PTR [esi+ecx*2]                    ;458.25
        jne       .B1.503       ; Prob 50%                      ;458.25
                                ; LOE eax ecx
.B1.502:                        ; Preds .B1.501                 ; Infreq
        mov       edi, DWORD PTR [-612+ebp]                     ;459.113
        mov       esi, DWORD PTR [-428+ebp]                     ;459.113
        imul      edx, DWORD PTR [176+esi+edi], 84              ;459.113
        mov       edi, DWORD PTR [-588+ebp]                     ;459.95
        mov       esi, DWORD PTR [-436+ebp]                     ;459.95
        cvtsi2ss  xmm1, DWORD PTR [esi+edi*4]                   ;459.95
        mov       esi, DWORD PTR [-596+ebp]                     ;459.113
        mov       edi, DWORD PTR [32+edx+esi]                   ;459.113
        shl       edi, 2                                        ;459.22
        neg       edi                                           ;459.22
        add       edi, DWORD PTR [edx+esi]                      ;459.22
        mov       esi, DWORD PTR [-604+ebp]                     ;459.22
        mov       edx, DWORD PTR [-580+ebp]                     ;459.22
        cvtsi2ss  xmm0, DWORD PTR [16+edi]                      ;459.113
        divss     xmm1, xmm0                                    ;459.112
        addss     xmm1, DWORD PTR [1248+edx+esi]                ;459.22
        movss     DWORD PTR [1248+edx+esi], xmm1                ;459.22
                                ; LOE eax ecx
.B1.503:                        ; Preds .B1.501 .B1.502         ; Infreq
        mov       esi, DWORD PTR [-388+ebp]                     ;458.25
        mov       edx, DWORD PTR [-396+ebp]                     ;458.25
        cmp       edx, DWORD PTR [esi+ecx*2]                    ;458.25
        jne       .B1.505       ; Prob 50%                      ;458.25
                                ; LOE eax ecx
.B1.504:                        ; Preds .B1.503                 ; Infreq
        mov       edi, DWORD PTR [-612+ebp]                     ;459.113
        mov       esi, DWORD PTR [-428+ebp]                     ;459.113
        imul      edx, DWORD PTR [176+esi+edi], 84              ;459.113
        mov       edi, DWORD PTR [-588+ebp]                     ;459.95
        mov       esi, DWORD PTR [-436+ebp]                     ;459.95
        cvtsi2ss  xmm1, DWORD PTR [esi+edi*4]                   ;459.95
        mov       esi, DWORD PTR [-596+ebp]                     ;459.113
        mov       edi, DWORD PTR [32+edx+esi]                   ;459.113
        shl       edi, 2                                        ;459.22
        neg       edi                                           ;459.22
        add       edi, DWORD PTR [edx+esi]                      ;459.22
        mov       esi, DWORD PTR [-604+ebp]                     ;459.22
        mov       edx, DWORD PTR [-580+ebp]                     ;459.22
        cvtsi2ss  xmm0, DWORD PTR [16+edi]                      ;459.113
        divss     xmm1, xmm0                                    ;459.112
        addss     xmm1, DWORD PTR [1248+edx+esi]                ;459.22
        movss     DWORD PTR [1248+edx+esi], xmm1                ;459.22
                                ; LOE eax ecx
.B1.505:                        ; Preds .B1.503 .B1.504         ; Infreq
        inc       eax                                           ;457.17
        add       ecx, DWORD PTR [-404+ebp]                     ;457.17
        cmp       eax, DWORD PTR [-372+ebp]                     ;457.17
        jb        .B1.501       ; Prob 64%                      ;457.17
                                ; LOE eax ecx
.B1.506:                        ; Preds .B1.505                 ; Infreq
        mov       edx, DWORD PTR [-756+ebp]                     ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;457.17
        mov       ecx, DWORD PTR [-588+ebp]                     ;
        mov       esi, DWORD PTR [-796+ebp]                     ;
                                ; LOE eax edx ecx esi
.B1.507:                        ; Preds .B1.506 .B1.512         ; Infreq
        lea       edi, DWORD PTR [-1+eax]                       ;457.17
        cmp       edx, edi                                      ;457.17
        jbe       .B1.510       ; Prob 11%                      ;457.17
                                ; LOE eax ecx esi
.B1.508:                        ; Preds .B1.507                 ; Infreq
        imul      eax, DWORD PTR [-404+ebp]                     ;458.25
        add       eax, DWORD PTR [-728+ebp]                     ;458.25
        mov       edx, DWORD PTR [-396+ebp]                     ;458.25
        cmp       edx, DWORD PTR [eax+ecx*4]                    ;458.25
        jne       .B1.510       ; Prob 50%                      ;458.25
                                ; LOE ecx esi
.B1.509:                        ; Preds .B1.508                 ; Infreq
        mov       edx, DWORD PTR [-428+ebp]                     ;459.113
        mov       edi, DWORD PTR [-612+ebp]                     ;459.113
        imul      eax, DWORD PTR [176+edx+edi], 84              ;459.113
        mov       edx, DWORD PTR [-436+ebp]                     ;459.95
        mov       edi, DWORD PTR [-596+ebp]                     ;459.113
        cvtsi2ss  xmm1, DWORD PTR [edx+ecx*4]                   ;459.95
        mov       edx, DWORD PTR [32+eax+edi]                   ;459.113
        shl       edx, 2                                        ;459.22
        neg       edx                                           ;459.22
        add       edx, DWORD PTR [eax+edi]                      ;459.22
        mov       eax, DWORD PTR [-580+ebp]                     ;459.22
        cvtsi2ss  xmm0, DWORD PTR [16+edx]                      ;459.113
        divss     xmm1, xmm0                                    ;459.112
        mov       edx, DWORD PTR [-604+ebp]                     ;459.22
        addss     xmm1, DWORD PTR [1248+eax+edx]                ;459.22
        movss     DWORD PTR [1248+eax+edx], xmm1                ;459.22
                                ; LOE ecx esi
.B1.510:                        ; Preds .B1.508 .B1.507 .B1.498 .B1.509 ; Infreq
        inc       ecx                                           ;456.14
        inc       esi                                           ;462.14
        cmp       ecx, DWORD PTR [-628+ebp]                     ;456.14
        jb        .B1.498       ; Prob 82%                      ;456.14
                                ; LOE ecx esi
.B1.511:                        ; Preds .B1.510                 ; Infreq
        mov       esi, DWORD PTR [-396+ebp]                     ;
        jmp       .B1.438       ; Prob 100%                     ;
                                ; LOE esi
.B1.512:                        ; Preds .B1.499                 ; Infreq
        mov       eax, 1                                        ;
        jmp       .B1.507       ; Prob 100%                     ;
                                ; LOE eax edx ecx esi
.B1.513:                        ; Preds .B1.436                 ; Infreq
        mov       esi, DWORD PTR [-804+ebp]                     ;445.13
        mov       edi, DWORD PTR [4+esi+edx*4]                  ;445.61
        dec       edi                                           ;445.13
        mov       eax, DWORD PTR [esi+edx*4]                    ;445.13
        mov       edx, edi                                      ;445.13
        sub       edx, eax                                      ;445.13
        lea       esi, DWORD PTR [1+ecx]                        ;
        inc       edx                                           ;445.13
        mov       DWORD PTR [-636+ebp], edx                     ;445.13
        cmp       edi, eax                                      ;445.13
        jl        .B1.438       ; Prob 10%                      ;445.13
                                ; LOE eax ecx esi
.B1.514:                        ; Preds .B1.513                 ; Infreq
        mov       edi, DWORD PTR [-844+ebp]                     ;
        xor       edx, edx                                      ;
        mov       DWORD PTR [-476+ebp], edx                     ;
        mov       DWORD PTR [-396+ebp], esi                     ;
        mov       esi, edx                                      ;
        lea       edx, DWORD PTR [edi+eax*4]                    ;
        mov       edi, DWORD PTR [-828+ebp]                     ;
        mov       DWORD PTR [-724+ebp], edx                     ;
        lea       edx, DWORD PTR [edi+eax*4]                    ;
        mov       edi, DWORD PTR [-820+ebp]                     ;
        mov       DWORD PTR [-732+ebp], edx                     ;
        lea       edx, DWORD PTR [edi+eax*4]                    ;
        mov       DWORD PTR [-736+ebp], edx                     ;
        mov       edi, DWORD PTR [-812+ebp]                     ;
        lea       edx, DWORD PTR [edi+eax*4]                    ;
        mov       DWORD PTR [-444+ebp], edx                     ;
        imul      edx, ecx, 900                                 ;
        mov       DWORD PTR [-468+ebp], edx                     ;
                                ; LOE eax esi
.B1.515:                        ; Preds .B1.527 .B1.514         ; Infreq
        mov       ecx, DWORD PTR [-676+ebp]                     ;446.23
        mov       ecx, DWORD PTR [ecx+eax*4]                    ;446.23
        lea       edx, DWORD PTR [5+ecx]                        ;446.16
        cmp       edx, 6                                        ;446.16
        jl        .B1.527       ; Prob 50%                      ;446.16
                                ; LOE eax ecx esi
.B1.516:                        ; Preds .B1.515                 ; Infreq
        mov       edx, ecx                                      ;446.16
        shr       edx, 31                                       ;446.16
        add       edx, ecx                                      ;446.16
        sar       edx, 1                                        ;446.16
        mov       DWORD PTR [-312+ebp], edx                     ;446.16
        test      edx, edx                                      ;446.16
        jbe       .B1.529       ; Prob 11%                      ;446.16
                                ; LOE eax ecx esi
.B1.517:                        ; Preds .B1.516                 ; Infreq
        mov       edi, DWORD PTR [-732+ebp]                     ;
        xor       edx, edx                                      ;
        mov       DWORD PTR [-748+ebp], ecx                     ;
        mov       DWORD PTR [-476+ebp], esi                     ;
        lea       edi, DWORD PTR [edi+esi*4]                    ;
        mov       DWORD PTR [-316+ebp], edi                     ;
        mov       edi, DWORD PTR [-736+ebp]                     ;
        mov       ecx, edx                                      ;
        mov       DWORD PTR [-752+ebp], eax                     ;
        lea       edi, DWORD PTR [edi+esi*4]                    ;
        mov       DWORD PTR [-364+ebp], edi                     ;
                                ; LOE edx ecx
.B1.518:                        ; Preds .B1.522 .B1.517         ; Infreq
        mov       esi, DWORD PTR [-316+ebp]                     ;447.24
        mov       eax, DWORD PTR [-396+ebp]                     ;447.24
        cmp       eax, DWORD PTR [esi+ecx*2]                    ;447.24
        jne       .B1.520       ; Prob 50%                      ;447.24
                                ; LOE edx ecx
.B1.519:                        ; Preds .B1.518                 ; Infreq
        mov       edi, DWORD PTR [-612+ebp]                     ;448.113
        mov       esi, DWORD PTR [-428+ebp]                     ;448.113
        imul      eax, DWORD PTR [176+esi+edi], 84              ;448.113
        mov       edi, DWORD PTR [-476+ebp]                     ;448.95
        mov       esi, DWORD PTR [-444+ebp]                     ;448.95
        cvtsi2ss  xmm1, DWORD PTR [esi+edi*4]                   ;448.95
        mov       esi, DWORD PTR [-596+ebp]                     ;448.113
        mov       edi, DWORD PTR [32+eax+esi]                   ;448.113
        shl       edi, 2                                        ;448.22
        neg       edi                                           ;448.22
        add       edi, DWORD PTR [eax+esi]                      ;448.22
        mov       esi, DWORD PTR [-604+ebp]                     ;448.22
        mov       eax, DWORD PTR [-468+ebp]                     ;448.22
        cvtsi2ss  xmm0, DWORD PTR [16+edi]                      ;448.113
        divss     xmm1, xmm0                                    ;448.112
        addss     xmm1, DWORD PTR [1248+eax+esi]                ;448.22
        movss     DWORD PTR [1248+eax+esi], xmm1                ;448.22
                                ; LOE edx ecx
.B1.520:                        ; Preds .B1.518 .B1.519         ; Infreq
        mov       esi, DWORD PTR [-364+ebp]                     ;447.24
        mov       eax, DWORD PTR [-396+ebp]                     ;447.24
        cmp       eax, DWORD PTR [esi+ecx*2]                    ;447.24
        jne       .B1.522       ; Prob 50%                      ;447.24
                                ; LOE edx ecx
.B1.521:                        ; Preds .B1.520                 ; Infreq
        mov       edi, DWORD PTR [-612+ebp]                     ;448.113
        mov       esi, DWORD PTR [-428+ebp]                     ;448.113
        imul      eax, DWORD PTR [176+esi+edi], 84              ;448.113
        mov       edi, DWORD PTR [-476+ebp]                     ;448.95
        mov       esi, DWORD PTR [-444+ebp]                     ;448.95
        cvtsi2ss  xmm1, DWORD PTR [esi+edi*4]                   ;448.95
        mov       esi, DWORD PTR [-596+ebp]                     ;448.113
        mov       edi, DWORD PTR [32+eax+esi]                   ;448.113
        shl       edi, 2                                        ;448.22
        neg       edi                                           ;448.22
        add       edi, DWORD PTR [eax+esi]                      ;448.22
        mov       esi, DWORD PTR [-604+ebp]                     ;448.22
        mov       eax, DWORD PTR [-468+ebp]                     ;448.22
        cvtsi2ss  xmm0, DWORD PTR [16+edi]                      ;448.113
        divss     xmm1, xmm0                                    ;448.112
        addss     xmm1, DWORD PTR [1248+eax+esi]                ;448.22
        movss     DWORD PTR [1248+eax+esi], xmm1                ;448.22
                                ; LOE edx ecx
.B1.522:                        ; Preds .B1.520 .B1.521         ; Infreq
        inc       edx                                           ;446.16
        add       ecx, DWORD PTR [-404+ebp]                     ;446.16
        cmp       edx, DWORD PTR [-312+ebp]                     ;446.16
        jb        .B1.518       ; Prob 64%                      ;446.16
                                ; LOE edx ecx
.B1.523:                        ; Preds .B1.522                 ; Infreq
        mov       ecx, DWORD PTR [-748+ebp]                     ;
        lea       edx, DWORD PTR [1+edx+edx]                    ;446.16
        mov       esi, DWORD PTR [-476+ebp]                     ;
        mov       eax, DWORD PTR [-752+ebp]                     ;
                                ; LOE eax edx ecx esi
.B1.524:                        ; Preds .B1.523 .B1.529         ; Infreq
        lea       edi, DWORD PTR [-1+edx]                       ;446.16
        cmp       ecx, edi                                      ;446.16
        jbe       .B1.527       ; Prob 11%                      ;446.16
                                ; LOE eax edx esi
.B1.525:                        ; Preds .B1.524                 ; Infreq
        imul      edx, DWORD PTR [-404+ebp]                     ;447.24
        add       edx, DWORD PTR [-724+ebp]                     ;447.24
        mov       ecx, DWORD PTR [-396+ebp]                     ;447.24
        cmp       ecx, DWORD PTR [edx+esi*4]                    ;447.24
        jne       .B1.527       ; Prob 50%                      ;447.24
                                ; LOE eax esi
.B1.526:                        ; Preds .B1.525                 ; Infreq
        mov       ecx, DWORD PTR [-428+ebp]                     ;448.113
        mov       edi, DWORD PTR [-612+ebp]                     ;448.113
        imul      edx, DWORD PTR [176+ecx+edi], 84              ;448.113
        mov       ecx, DWORD PTR [-444+ebp]                     ;448.95
        mov       edi, DWORD PTR [-596+ebp]                     ;448.113
        cvtsi2ss  xmm1, DWORD PTR [ecx+esi*4]                   ;448.95
        mov       ecx, DWORD PTR [32+edx+edi]                   ;448.113
        shl       ecx, 2                                        ;448.22
        neg       ecx                                           ;448.22
        add       ecx, DWORD PTR [edx+edi]                      ;448.22
        mov       edx, DWORD PTR [-468+ebp]                     ;448.22
        cvtsi2ss  xmm0, DWORD PTR [16+ecx]                      ;448.113
        divss     xmm1, xmm0                                    ;448.112
        mov       ecx, DWORD PTR [-604+ebp]                     ;448.22
        addss     xmm1, DWORD PTR [1248+edx+ecx]                ;448.22
        movss     DWORD PTR [1248+edx+ecx], xmm1                ;448.22
                                ; LOE eax esi
.B1.527:                        ; Preds .B1.525 .B1.524 .B1.515 .B1.526 ; Infreq
        inc       esi                                           ;445.13
        inc       eax                                           ;451.13
        cmp       esi, DWORD PTR [-636+ebp]                     ;445.13
        jb        .B1.515       ; Prob 82%                      ;445.13
                                ; LOE eax esi
.B1.528:                        ; Preds .B1.527                 ; Infreq
        mov       esi, DWORD PTR [-396+ebp]                     ;
        jmp       .B1.438       ; Prob 100%                     ;
                                ; LOE esi
.B1.529:                        ; Preds .B1.516                 ; Infreq
        mov       edx, 1                                        ;
        jmp       .B1.524       ; Prob 100%                     ;
                                ; LOE eax edx ecx esi
.B1.530:                        ; Preds .B1.416                 ; Infreq
        mov       edx, 1                                        ;
        jmp       .B1.428       ; Prob 100%                     ;
                                ; LOE edx ecx esi
.B1.531:                        ; Preds .B1.407                 ; Infreq
        push      32                                            ;418.7
        mov       DWORD PTR [-232+ebp], 0                       ;418.7
        lea       eax, DWORD PTR [-656+ebp]                     ;418.7
        push      eax                                           ;418.7
        push      OFFSET FLAT: __STRLITPACK_304.0.1             ;418.7
        push      -2088435968                                   ;418.7
        push      911                                           ;418.7
        push      esi                                           ;418.7
        mov       DWORD PTR [-656+ebp], 21                      ;418.7
        mov       DWORD PTR [-652+ebp], OFFSET FLAT: __STRLITPACK_19 ;418.7
        call      _for_write_seq_lis                            ;418.7
                                ; LOE esi edi
.B1.834:                        ; Preds .B1.531                 ; Infreq
        add       esp, 24                                       ;418.7
                                ; LOE esi edi
.B1.532:                        ; Preds .B1.834                 ; Infreq
        push      32                                            ;419.4
        mov       DWORD PTR [-232+ebp], 0                       ;419.4
        lea       eax, DWORD PTR [-648+ebp]                     ;419.4
        push      eax                                           ;419.4
        push      OFFSET FLAT: __STRLITPACK_305.0.1             ;419.4
        push      -2088435968                                   ;419.4
        push      911                                           ;419.4
        push      esi                                           ;419.4
        mov       DWORD PTR [-648+ebp], 26                      ;419.4
        mov       DWORD PTR [-644+ebp], OFFSET FLAT: __STRLITPACK_17 ;419.4
        call      _for_write_seq_lis                            ;419.4
                                ; LOE esi edi
.B1.835:                        ; Preds .B1.532                 ; Infreq
        add       esp, 24                                       ;419.4
                                ; LOE esi edi
.B1.533:                        ; Preds .B1.835                 ; Infreq
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGNDATA+32] ;419.4
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE+32] ;419.4
        neg       eax                                           ;419.4
        lea       edx, DWORD PTR [ecx*8]                        ;419.4
        lea       ecx, DWORD PTR [edx+ecx*4]                    ;419.4
        mov       edx, DWORD PTR [-752+ebp]                     ;419.4
        neg       ecx                                           ;419.4
        add       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGNDATA] ;419.4
        add       eax, DWORD PTR [edx+ecx]                      ;419.4
        lea       ecx, DWORD PTR [-472+ebp]                     ;419.4
        imul      edx, eax, 44                                  ;419.4
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE] ;419.4
        push      ecx                                           ;419.4
        push      OFFSET FLAT: __STRLITPACK_306.0.1             ;419.4
        mov       eax, DWORD PTR [36+eax+edx]                   ;419.4
        push      esi                                           ;419.4
        mov       DWORD PTR [-472+ebp], eax                     ;419.4
        call      _for_write_seq_lis_xmit                       ;419.4
                                ; LOE esi edi
.B1.836:                        ; Preds .B1.533                 ; Infreq
        add       esp, 12                                       ;419.4
                                ; LOE esi edi
.B1.534:                        ; Preds .B1.836                 ; Infreq
        push      32                                            ;420.4
        xor       eax, eax                                      ;420.4
        push      eax                                           ;420.4
        push      eax                                           ;420.4
        push      -2088435968                                   ;420.4
        push      eax                                           ;420.4
        push      OFFSET FLAT: __STRLITPACK_307                 ;420.4
        call      _for_stop_core                                ;420.4
                                ; LOE esi edi
.B1.837:                        ; Preds .B1.534                 ; Infreq
        add       esp, 24                                       ;420.4
        jmp       .B1.409       ; Prob 100%                     ;420.4
                                ; LOE esi edi
.B1.535:                        ; Preds .B1.405 .B1.404         ; Infreq
        push      32                                            ;412.12
        mov       DWORD PTR [-232+ebp], 0                       ;412.12
        lea       eax, DWORD PTR [-496+ebp]                     ;412.12
        push      eax                                           ;412.12
        push      OFFSET FLAT: __STRLITPACK_300.0.1             ;412.12
        push      -2088435968                                   ;412.12
        push      911                                           ;412.12
        push      esi                                           ;412.12
        mov       DWORD PTR [-496+ebp], 21                      ;412.12
        mov       DWORD PTR [-492+ebp], OFFSET FLAT: __STRLITPACK_23 ;412.12
        call      _for_write_seq_lis                            ;412.12
                                ; LOE esi edi
.B1.838:                        ; Preds .B1.535                 ; Infreq
        add       esp, 24                                       ;412.12
                                ; LOE esi edi
.B1.536:                        ; Preds .B1.838                 ; Infreq
        push      32                                            ;413.6
        mov       DWORD PTR [-232+ebp], 0                       ;413.6
        lea       eax, DWORD PTR [-488+ebp]                     ;413.6
        push      eax                                           ;413.6
        push      OFFSET FLAT: __STRLITPACK_301.0.1             ;413.6
        push      -2088435968                                   ;413.6
        push      911                                           ;413.6
        push      esi                                           ;413.6
        mov       DWORD PTR [-488+ebp], 28                      ;413.6
        mov       DWORD PTR [-484+ebp], OFFSET FLAT: __STRLITPACK_21 ;413.6
        call      _for_write_seq_lis                            ;413.6
                                ; LOE esi edi
.B1.839:                        ; Preds .B1.536                 ; Infreq
        add       esp, 24                                       ;413.6
                                ; LOE esi edi
.B1.537:                        ; Preds .B1.839                 ; Infreq
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGNDATA+32] ;413.6
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE+32] ;413.6
        neg       eax                                           ;413.6
        lea       edx, DWORD PTR [ecx*8]                        ;413.6
        lea       ecx, DWORD PTR [edx+ecx*4]                    ;413.6
        mov       edx, DWORD PTR [-752+ebp]                     ;413.6
        neg       ecx                                           ;413.6
        add       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGNDATA] ;413.6
        add       eax, DWORD PTR [edx+ecx]                      ;413.6
        lea       ecx, DWORD PTR [-368+ebp]                     ;413.6
        imul      edx, eax, 44                                  ;413.6
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE] ;413.6
        push      ecx                                           ;413.6
        push      OFFSET FLAT: __STRLITPACK_302.0.1             ;413.6
        mov       eax, DWORD PTR [36+eax+edx]                   ;413.6
        push      esi                                           ;413.6
        mov       DWORD PTR [-368+ebp], eax                     ;413.6
        call      _for_write_seq_lis_xmit                       ;413.6
                                ; LOE esi edi
.B1.840:                        ; Preds .B1.537                 ; Infreq
        add       esp, 12                                       ;413.6
                                ; LOE esi edi
.B1.538:                        ; Preds .B1.840                 ; Infreq
        push      32                                            ;414.6
        xor       eax, eax                                      ;414.6
        push      eax                                           ;414.6
        push      eax                                           ;414.6
        push      -2088435968                                   ;414.6
        push      eax                                           ;414.6
        push      OFFSET FLAT: __STRLITPACK_303                 ;414.6
        call      _for_stop_core                                ;414.6
                                ; LOE esi edi
.B1.841:                        ; Preds .B1.538                 ; Infreq
        add       esp, 24                                       ;414.6
        jmp       .B1.406       ; Prob 100%                     ;414.6
                                ; LOE esi edi
.B1.539:                        ; Preds .B1.391                 ; Infreq
        push      32                                            ;393.11
        mov       DWORD PTR [-232+ebp], 0                       ;393.11
        lea       eax, DWORD PTR [-1048+ebp]                    ;393.11
        push      eax                                           ;393.11
        push      OFFSET FLAT: __STRLITPACK_294.0.1             ;393.11
        push      -2088435968                                   ;393.11
        push      911                                           ;393.11
        mov       DWORD PTR [-1048+ebp], 45                     ;393.11
        lea       edx, DWORD PTR [-232+ebp]                     ;393.11
        push      edx                                           ;393.11
        mov       DWORD PTR [-1044+ebp], OFFSET FLAT: __STRLITPACK_29 ;393.11
        call      _for_write_seq_lis                            ;393.11
                                ; LOE
.B1.842:                        ; Preds .B1.539                 ; Infreq
        add       esp, 24                                       ;393.11
                                ; LOE
.B1.540:                        ; Preds .B1.842                 ; Infreq
        push      32                                            ;394.8
        xor       eax, eax                                      ;394.8
        push      eax                                           ;394.8
        push      eax                                           ;394.8
        push      -2088435968                                   ;394.8
        push      eax                                           ;394.8
        push      OFFSET FLAT: __STRLITPACK_295                 ;394.8
        call      _for_stop_core                                ;394.8
                                ; LOE
.B1.843:                        ; Preds .B1.540                 ; Infreq
        add       esp, 24                                       ;394.8
        jmp       .B1.392       ; Prob 100%                     ;394.8
                                ; LOE
.B1.542:                        ; Preds .B1.387 .B1.383         ; Infreq
        push      0                                             ;391.6
        push      OFFSET FLAT: __STRLITPACK_293.0.1             ;391.6
        lea       eax, DWORD PTR [-232+ebp]                     ;391.6
        push      eax                                           ;391.6
        call      _for_read_seq_lis_xmit                        ;391.6
                                ; LOE eax
.B1.844:                        ; Preds .B1.542                 ; Infreq
        add       esp, 12                                       ;391.6
        jmp       .B1.391       ; Prob 100%                     ;391.6
                                ; LOE eax
.B1.544:                        ; Preds .B1.377                 ; Infreq
        push      32                                            ;382.7
        mov       DWORD PTR [-232+ebp], 0                       ;382.7
        lea       eax, DWORD PTR [-672+ebp]                     ;382.7
        push      eax                                           ;382.7
        push      OFFSET FLAT: __STRLITPACK_287.0.1             ;382.7
        push      -2088435968                                   ;382.7
        push      911                                           ;382.7
        mov       DWORD PTR [-672+ebp], 21                      ;382.7
        lea       edx, DWORD PTR [-232+ebp]                     ;382.7
        push      edx                                           ;382.7
        mov       DWORD PTR [-668+ebp], OFFSET FLAT: __STRLITPACK_33 ;382.7
        call      _for_write_seq_lis                            ;382.7
                                ; LOE esi
.B1.845:                        ; Preds .B1.544                 ; Infreq
        add       esp, 24                                       ;382.7
                                ; LOE esi
.B1.545:                        ; Preds .B1.845                 ; Infreq
        push      32                                            ;383.4
        mov       DWORD PTR [-232+ebp], 0                       ;383.4
        lea       eax, DWORD PTR [-664+ebp]                     ;383.4
        push      eax                                           ;383.4
        push      OFFSET FLAT: __STRLITPACK_288.0.1             ;383.4
        push      -2088435968                                   ;383.4
        push      911                                           ;383.4
        mov       DWORD PTR [-664+ebp], 26                      ;383.4
        lea       edx, DWORD PTR [-232+ebp]                     ;383.4
        push      edx                                           ;383.4
        mov       DWORD PTR [-660+ebp], OFFSET FLAT: __STRLITPACK_31 ;383.4
        call      _for_write_seq_lis                            ;383.4
                                ; LOE esi
.B1.846:                        ; Preds .B1.545                 ; Infreq
        add       esp, 24                                       ;383.4
                                ; LOE esi
.B1.546:                        ; Preds .B1.846                 ; Infreq
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGNDATA+32] ;383.4
        mov       edi, DWORD PTR [-752+ebp]                     ;383.4
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE+32] ;383.4
        neg       eax                                           ;383.4
        lea       edx, DWORD PTR [ecx*8]                        ;383.4
        lea       edx, DWORD PTR [edx+ecx*4]                    ;383.4
        neg       edx                                           ;383.4
        add       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGNDATA] ;383.4
        add       eax, DWORD PTR [edi+edx]                      ;383.4
        lea       edx, DWORD PTR [-232+ebp]                     ;383.4
        imul      ecx, eax, 44                                  ;383.4
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE] ;383.4
        mov       edi, DWORD PTR [36+eax+ecx]                   ;383.4
        lea       eax, DWORD PTR [-480+ebp]                     ;383.4
        push      eax                                           ;383.4
        push      OFFSET FLAT: __STRLITPACK_289.0.1             ;383.4
        push      edx                                           ;383.4
        mov       DWORD PTR [-480+ebp], edi                     ;383.4
        call      _for_write_seq_lis_xmit                       ;383.4
                                ; LOE esi
.B1.847:                        ; Preds .B1.546                 ; Infreq
        add       esp, 12                                       ;383.4
                                ; LOE esi
.B1.547:                        ; Preds .B1.847                 ; Infreq
        push      32                                            ;384.4
        xor       eax, eax                                      ;384.4
        push      eax                                           ;384.4
        push      eax                                           ;384.4
        push      -2088435968                                   ;384.4
        push      eax                                           ;384.4
        push      OFFSET FLAT: __STRLITPACK_290                 ;384.4
        call      _for_stop_core                                ;384.4
                                ; LOE esi
.B1.848:                        ; Preds .B1.547                 ; Infreq
        add       esp, 24                                       ;384.4
        jmp       .B1.379       ; Prob 100%                     ;384.4
                                ; LOE esi
.B1.548:                        ; Preds .B1.374 .B1.375         ; Infreq
        push      32                                            ;376.12
        mov       DWORD PTR [-232+ebp], 0                       ;376.12
        lea       eax, DWORD PTR [-512+ebp]                     ;376.12
        push      eax                                           ;376.12
        push      OFFSET FLAT: __STRLITPACK_283.0.1             ;376.12
        push      -2088435968                                   ;376.12
        push      911                                           ;376.12
        mov       DWORD PTR [-512+ebp], 21                      ;376.12
        lea       edx, DWORD PTR [-232+ebp]                     ;376.12
        push      edx                                           ;376.12
        mov       DWORD PTR [-508+ebp], OFFSET FLAT: __STRLITPACK_37 ;376.12
        call      _for_write_seq_lis                            ;376.12
                                ; LOE esi
.B1.849:                        ; Preds .B1.548                 ; Infreq
        add       esp, 24                                       ;376.12
                                ; LOE esi
.B1.549:                        ; Preds .B1.849                 ; Infreq
        push      32                                            ;377.6
        mov       DWORD PTR [-232+ebp], 0                       ;377.6
        lea       eax, DWORD PTR [-504+ebp]                     ;377.6
        push      eax                                           ;377.6
        push      OFFSET FLAT: __STRLITPACK_284.0.1             ;377.6
        push      -2088435968                                   ;377.6
        push      911                                           ;377.6
        mov       DWORD PTR [-504+ebp], 28                      ;377.6
        lea       edx, DWORD PTR [-232+ebp]                     ;377.6
        push      edx                                           ;377.6
        mov       DWORD PTR [-500+ebp], OFFSET FLAT: __STRLITPACK_35 ;377.6
        call      _for_write_seq_lis                            ;377.6
                                ; LOE esi
.B1.850:                        ; Preds .B1.549                 ; Infreq
        add       esp, 24                                       ;377.6
                                ; LOE esi
.B1.550:                        ; Preds .B1.850                 ; Infreq
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGNDATA+32] ;377.6
        mov       edi, DWORD PTR [-752+ebp]                     ;377.6
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE+32] ;377.6
        neg       eax                                           ;377.6
        lea       edx, DWORD PTR [ecx*8]                        ;377.6
        lea       edx, DWORD PTR [edx+ecx*4]                    ;377.6
        neg       edx                                           ;377.6
        add       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGNDATA] ;377.6
        add       eax, DWORD PTR [edi+edx]                      ;377.6
        lea       edx, DWORD PTR [-232+ebp]                     ;377.6
        imul      ecx, eax, 44                                  ;377.6
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE] ;377.6
        mov       edi, DWORD PTR [36+eax+ecx]                   ;377.6
        lea       eax, DWORD PTR [-376+ebp]                     ;377.6
        push      eax                                           ;377.6
        push      OFFSET FLAT: __STRLITPACK_285.0.1             ;377.6
        push      edx                                           ;377.6
        mov       DWORD PTR [-376+ebp], edi                     ;377.6
        call      _for_write_seq_lis_xmit                       ;377.6
                                ; LOE esi
.B1.851:                        ; Preds .B1.550                 ; Infreq
        add       esp, 12                                       ;377.6
                                ; LOE esi
.B1.551:                        ; Preds .B1.851                 ; Infreq
        push      32                                            ;378.6
        xor       eax, eax                                      ;378.6
        push      eax                                           ;378.6
        push      eax                                           ;378.6
        push      -2088435968                                   ;378.6
        push      eax                                           ;378.6
        push      OFFSET FLAT: __STRLITPACK_286                 ;378.6
        call      _for_stop_core                                ;378.6
                                ; LOE esi
.B1.852:                        ; Preds .B1.551                 ; Infreq
        add       esp, 24                                       ;378.6
        jmp       .B1.376       ; Prob 100%                     ;378.6
                                ; LOE esi
.B1.552:                        ; Preds .B1.361                 ; Infreq
        push      32                                            ;359.11
        mov       DWORD PTR [-232+ebp], 0                       ;359.11
        lea       eax, DWORD PTR [-1056+ebp]                    ;359.11
        push      eax                                           ;359.11
        push      OFFSET FLAT: __STRLITPACK_277.0.1             ;359.11
        push      -2088435968                                   ;359.11
        push      911                                           ;359.11
        mov       DWORD PTR [-1056+ebp], 45                     ;359.11
        lea       edx, DWORD PTR [-232+ebp]                     ;359.11
        push      edx                                           ;359.11
        mov       DWORD PTR [-1052+ebp], OFFSET FLAT: __STRLITPACK_43 ;359.11
        call      _for_write_seq_lis                            ;359.11
                                ; LOE
.B1.853:                        ; Preds .B1.552                 ; Infreq
        add       esp, 24                                       ;359.11
                                ; LOE
.B1.553:                        ; Preds .B1.853                 ; Infreq
        push      32                                            ;360.8
        xor       eax, eax                                      ;360.8
        push      eax                                           ;360.8
        push      eax                                           ;360.8
        push      -2088435968                                   ;360.8
        push      eax                                           ;360.8
        push      OFFSET FLAT: __STRLITPACK_278                 ;360.8
        call      _for_stop_core                                ;360.8
                                ; LOE
.B1.854:                        ; Preds .B1.553                 ; Infreq
        add       esp, 24                                       ;360.8
        jmp       .B1.362       ; Prob 100%                     ;360.8
                                ; LOE
.B1.555:                        ; Preds .B1.357 .B1.353         ; Infreq
        push      0                                             ;357.9
        push      OFFSET FLAT: __STRLITPACK_276.0.1             ;357.9
        lea       eax, DWORD PTR [-232+ebp]                     ;357.9
        push      eax                                           ;357.9
        call      _for_read_seq_lis_xmit                        ;357.9
                                ; LOE eax
.B1.855:                        ; Preds .B1.555                 ; Infreq
        add       esp, 12                                       ;357.9
        jmp       .B1.361       ; Prob 100%                     ;357.9
                                ; LOE eax
.B1.557:                        ; Preds .B1.350                 ; Infreq
        push      32                                            ;353.11
        mov       DWORD PTR [-232+ebp], 0                       ;353.11
        lea       eax, DWORD PTR [-1064+ebp]                    ;353.11
        push      eax                                           ;353.11
        push      OFFSET FLAT: __STRLITPACK_272.0.1             ;353.11
        push      -2088435968                                   ;353.11
        push      911                                           ;353.11
        mov       DWORD PTR [-1064+ebp], 45                     ;353.11
        lea       edx, DWORD PTR [-232+ebp]                     ;353.11
        push      edx                                           ;353.11
        mov       DWORD PTR [-1060+ebp], OFFSET FLAT: __STRLITPACK_45 ;353.11
        call      _for_write_seq_lis                            ;353.11
                                ; LOE
.B1.856:                        ; Preds .B1.557                 ; Infreq
        add       esp, 24                                       ;353.11
                                ; LOE
.B1.558:                        ; Preds .B1.856                 ; Infreq
        push      32                                            ;354.8
        xor       eax, eax                                      ;354.8
        push      eax                                           ;354.8
        push      eax                                           ;354.8
        push      -2088435968                                   ;354.8
        push      eax                                           ;354.8
        push      OFFSET FLAT: __STRLITPACK_273                 ;354.8
        call      _for_stop_core                                ;354.8
                                ; LOE
.B1.857:                        ; Preds .B1.558                 ; Infreq
        add       esp, 24                                       ;354.8
        jmp       .B1.351       ; Prob 100%                     ;354.8
                                ; LOE
.B1.559:                        ; Preds .B1.265                 ; Infreq
        push      32                                            ;329.8
        mov       DWORD PTR [-232+ebp], 0                       ;329.8
        lea       eax, DWORD PTR [-1112+ebp]                    ;329.8
        push      eax                                           ;329.8
        push      OFFSET FLAT: __STRLITPACK_267.0.1             ;329.8
        push      -2088435968                                   ;329.8
        push      911                                           ;329.8
        mov       DWORD PTR [-1112+ebp], 45                     ;329.8
        lea       edx, DWORD PTR [-232+ebp]                     ;329.8
        push      edx                                           ;329.8
        mov       DWORD PTR [-1108+ebp], OFFSET FLAT: __STRLITPACK_47 ;329.8
        call      _for_write_seq_lis                            ;329.8
                                ; LOE
.B1.858:                        ; Preds .B1.559                 ; Infreq
        add       esp, 24                                       ;329.8
                                ; LOE
.B1.560:                        ; Preds .B1.858                 ; Infreq
        push      32                                            ;330.5
        xor       eax, eax                                      ;330.5
        push      eax                                           ;330.5
        push      eax                                           ;330.5
        push      -2088435968                                   ;330.5
        push      eax                                           ;330.5
        push      OFFSET FLAT: __STRLITPACK_268                 ;330.5
        call      _for_stop_core                                ;330.5
                                ; LOE
.B1.859:                        ; Preds .B1.560                 ; Infreq
        add       esp, 24                                       ;330.5
        jmp       .B1.266       ; Prob 100%                     ;330.5
                                ; LOE
.B1.561:                        ; Preds .B1.262                 ; Infreq
        push      32                                            ;325.20
        xor       eax, eax                                      ;325.20
        push      eax                                           ;325.20
        push      eax                                           ;325.20
        push      -2088435968                                   ;325.20
        push      eax                                           ;325.20
        push      OFFSET FLAT: __STRLITPACK_265                 ;325.20
        call      _for_stop_core                                ;325.20
                                ; LOE
.B1.860:                        ; Preds .B1.561                 ; Infreq
        add       esp, 24                                       ;325.20
        jmp       .B1.263       ; Prob 100%                     ;325.20
                                ; LOE
.B1.562:                        ; Preds .B1.229                 ; Infreq
        push      32                                            ;276.4
        push      OFFSET FLAT: READ_INTERSECTION_CONTROLS$format_pack.0.1+276 ;276.4
        mov       eax, ecx                                      ;276.4
        lea       edx, DWORD PTR [-640+ebp]                     ;276.4
        push      edx                                           ;276.4
        push      OFFSET FLAT: __STRLITPACK_261.0.1             ;276.4
        push      -2088435968                                   ;276.4
        push      911                                           ;276.4
        mov       DWORD PTR [-232+ebp], 0                       ;276.4
        lea       ecx, DWORD PTR [-232+ebp]                     ;276.4
        push      ecx                                           ;276.4
        mov       DWORD PTR [-640+ebp], eax                     ;276.4
        call      _for_write_seq_fmt                            ;276.4
                                ; LOE esi edi
.B1.861:                        ; Preds .B1.562                 ; Infreq
        add       esp, 28                                       ;276.4
                                ; LOE esi edi
.B1.563:                        ; Preds .B1.861                 ; Infreq
        mov       eax, DWORD PTR [-560+ebp]                     ;276.4
        lea       edx, DWORD PTR [-632+ebp]                     ;276.4
        push      edx                                           ;276.4
        push      OFFSET FLAT: __STRLITPACK_262.0.1             ;276.4
        mov       DWORD PTR [-632+ebp], eax                     ;276.4
        lea       ecx, DWORD PTR [-232+ebp]                     ;276.4
        push      ecx                                           ;276.4
        call      _for_write_seq_fmt_xmit                       ;276.4
                                ; LOE esi edi
.B1.862:                        ; Preds .B1.563                 ; Infreq
        add       esp, 12                                       ;276.4
                                ; LOE esi edi
.B1.564:                        ; Preds .B1.862                 ; Infreq
        mov       eax, DWORD PTR [-180+ebp]                     ;276.4
        lea       edx, DWORD PTR [-624+ebp]                     ;276.4
        push      edx                                           ;276.4
        push      OFFSET FLAT: __STRLITPACK_263.0.1             ;276.4
        mov       DWORD PTR [-624+ebp], eax                     ;276.4
        lea       ecx, DWORD PTR [-232+ebp]                     ;276.4
        push      ecx                                           ;276.4
        call      _for_write_seq_fmt_xmit                       ;276.4
                                ; LOE esi edi
.B1.863:                        ; Preds .B1.564                 ; Infreq
        add       esp, 12                                       ;276.4
                                ; LOE esi edi
.B1.565:                        ; Preds .B1.863                 ; Infreq
        push      32                                            ;277.7
        xor       eax, eax                                      ;277.7
        push      eax                                           ;277.7
        push      eax                                           ;277.7
        push      -2088435968                                   ;277.7
        push      eax                                           ;277.7
        push      OFFSET FLAT: __STRLITPACK_264                 ;277.7
        call      _for_stop_core                                ;277.7
                                ; LOE esi edi
.B1.864:                        ; Preds .B1.565                 ; Infreq
        add       esp, 24                                       ;277.7
        jmp       .B1.230       ; Prob 100%                     ;277.7
                                ; LOE esi edi
.B1.566:                        ; Preds .B1.216                 ; Infreq
        mov       eax, DWORD PTR [-196+ebp]                     ;275.7
        mov       edx, DWORD PTR [-188+ebp]                     ;276.4
        mov       esi, DWORD PTR [-156+ebp]                     ;275.7
        mov       edi, DWORD PTR [-172+ebp]                     ;279.4
        mov       DWORD PTR [-584+ebp], eax                     ;275.7
        mov       DWORD PTR [-560+ebp], edx                     ;276.4
        jmp       .B1.229       ; Prob 100%                     ;276.4
                                ; LOE esi edi
.B1.567:                        ; Preds .B1.214                 ; Infreq
        push      32                                            ;261.13
        mov       DWORD PTR [-232+ebp], 0                       ;261.13
        lea       edx, DWORD PTR [-288+ebp]                     ;261.13
        push      edx                                           ;261.13
        push      OFFSET FLAT: __STRLITPACK_242.0.1             ;261.13
        push      -2088435968                                   ;261.13
        push      911                                           ;261.13
        mov       DWORD PTR [-288+ebp], 26                      ;261.13
        lea       ecx, DWORD PTR [-232+ebp]                     ;261.13
        push      ecx                                           ;261.13
        mov       DWORD PTR [-284+ebp], OFFSET FLAT: __STRLITPACK_62 ;261.13
        call      _for_write_seq_lis                            ;261.13
                                ; LOE esi edi
.B1.865:                        ; Preds .B1.567                 ; Infreq
        add       esp, 24                                       ;261.13
                                ; LOE esi edi
.B1.568:                        ; Preds .B1.865                 ; Infreq
        mov       edx, DWORD PTR [-584+ebp]                     ;261.13
        lea       ecx, DWORD PTR [-136+ebp]                     ;261.13
        push      ecx                                           ;261.13
        mov       DWORD PTR [-136+ebp], edx                     ;261.13
        lea       edx, DWORD PTR [-232+ebp]                     ;261.13
        push      OFFSET FLAT: __STRLITPACK_243.0.1             ;261.13
        push      edx                                           ;261.13
        call      _for_write_seq_lis_xmit                       ;261.13
                                ; LOE esi edi
.B1.866:                        ; Preds .B1.568                 ; Infreq
        add       esp, 12                                       ;261.13
                                ; LOE esi edi
.B1.569:                        ; Preds .B1.866                 ; Infreq
        mov       DWORD PTR [-280+ebp], 3                       ;261.13
        lea       edx, DWORD PTR [-280+ebp]                     ;261.13
        push      edx                                           ;261.13
        push      OFFSET FLAT: __STRLITPACK_244.0.1             ;261.13
        mov       DWORD PTR [-276+ebp], OFFSET FLAT: __STRLITPACK_61 ;261.13
        lea       ecx, DWORD PTR [-232+ebp]                     ;261.13
        push      ecx                                           ;261.13
        call      _for_write_seq_lis_xmit                       ;261.13
                                ; LOE esi edi
.B1.867:                        ; Preds .B1.569                 ; Infreq
        add       esp, 12                                       ;261.13
                                ; LOE esi edi
.B1.570:                        ; Preds .B1.867                 ; Infreq
        mov       edx, DWORD PTR [-560+ebp]                     ;261.13
        lea       ecx, DWORD PTR [-128+ebp]                     ;261.13
        push      ecx                                           ;261.13
        mov       DWORD PTR [-128+ebp], edx                     ;261.13
        lea       edx, DWORD PTR [-232+ebp]                     ;261.13
        push      OFFSET FLAT: __STRLITPACK_245.0.1             ;261.13
        push      edx                                           ;261.13
        call      _for_write_seq_lis_xmit                       ;261.13
                                ; LOE esi edi
.B1.868:                        ; Preds .B1.570                 ; Infreq
        add       esp, 12                                       ;261.13
                                ; LOE esi edi
.B1.571:                        ; Preds .B1.868                 ; Infreq
        mov       DWORD PTR [-272+ebp], 5                       ;261.13
        lea       edx, DWORD PTR [-272+ebp]                     ;261.13
        push      edx                                           ;261.13
        push      OFFSET FLAT: __STRLITPACK_246.0.1             ;261.13
        mov       DWORD PTR [-268+ebp], OFFSET FLAT: __STRLITPACK_60 ;261.13
        lea       ecx, DWORD PTR [-232+ebp]                     ;261.13
        push      ecx                                           ;261.13
        call      _for_write_seq_lis_xmit                       ;261.13
                                ; LOE esi edi
.B1.869:                        ; Preds .B1.571                 ; Infreq
        add       esp, 12                                       ;261.13
                                ; LOE esi edi
.B1.572:                        ; Preds .B1.869                 ; Infreq
        mov       edx, DWORD PTR [-580+ebp]                     ;261.13
        lea       ecx, DWORD PTR [-120+ebp]                     ;261.13
        push      ecx                                           ;261.13
        mov       DWORD PTR [-120+ebp], edx                     ;261.13
        lea       edx, DWORD PTR [-232+ebp]                     ;261.13
        push      OFFSET FLAT: __STRLITPACK_247.0.1             ;261.13
        push      edx                                           ;261.13
        call      _for_write_seq_lis_xmit                       ;261.13
                                ; LOE esi edi
.B1.870:                        ; Preds .B1.572                 ; Infreq
        add       esp, 12                                       ;261.13
                                ; LOE esi edi
.B1.573:                        ; Preds .B1.870                 ; Infreq
        mov       DWORD PTR [-264+ebp], 13                      ;261.13
        lea       edx, DWORD PTR [-264+ebp]                     ;261.13
        push      edx                                           ;261.13
        push      OFFSET FLAT: __STRLITPACK_248.0.1             ;261.13
        mov       DWORD PTR [-260+ebp], OFFSET FLAT: __STRLITPACK_59 ;261.13
        lea       ecx, DWORD PTR [-232+ebp]                     ;261.13
        push      ecx                                           ;261.13
        call      _for_write_seq_lis_xmit                       ;261.13
                                ; LOE esi edi
.B1.871:                        ; Preds .B1.573                 ; Infreq
        add       esp, 12                                       ;261.13
                                ; LOE esi edi
.B1.574:                        ; Preds .B1.871                 ; Infreq
        mov       edx, DWORD PTR [-552+ebp]                     ;261.13
        lea       ecx, DWORD PTR [-112+ebp]                     ;261.13
        push      ecx                                           ;261.13
        mov       DWORD PTR [-112+ebp], edx                     ;261.13
        lea       edx, DWORD PTR [-232+ebp]                     ;261.13
        push      OFFSET FLAT: __STRLITPACK_249.0.1             ;261.13
        push      edx                                           ;261.13
        call      _for_write_seq_lis_xmit                       ;261.13
                                ; LOE esi edi
.B1.872:                        ; Preds .B1.574                 ; Infreq
        add       esp, 12                                       ;261.13
                                ; LOE esi edi
.B1.575:                        ; Preds .B1.872                 ; Infreq
        push      32                                            ;262.13
        xor       edx, edx                                      ;262.13
        push      edx                                           ;262.13
        push      edx                                           ;262.13
        push      -2088435968                                   ;262.13
        push      edx                                           ;262.13
        push      OFFSET FLAT: __STRLITPACK_250                 ;262.13
        call      _for_stop_core                                ;262.13
                                ; LOE esi edi
.B1.873:                        ; Preds .B1.575                 ; Infreq
        add       esp, 24                                       ;262.13
        jmp       .B1.215       ; Prob 100%                     ;262.13
                                ; LOE esi edi
.B1.576:                        ; Preds .B1.191                 ; Infreq
        mov       eax, 32                                       ;239.10
        lea       edx, DWORD PTR [-528+ebp]                     ;239.10
        push      eax                                           ;239.10
        push      edx                                           ;239.10
        push      OFFSET FLAT: __STRLITPACK_230.0.1             ;239.10
        push      -2088435968                                   ;239.10
        push      911                                           ;239.10
        mov       DWORD PTR [-232+ebp], 0                       ;239.10
        lea       ecx, DWORD PTR [-232+ebp]                     ;239.10
        push      ecx                                           ;239.10
        mov       DWORD PTR [-528+ebp], eax                     ;239.10
        mov       DWORD PTR [-524+ebp], OFFSET FLAT: __STRLITPACK_72 ;239.10
        call      _for_write_seq_lis                            ;239.10
                                ; LOE esi
.B1.874:                        ; Preds .B1.576                 ; Infreq
        add       esp, 24                                       ;239.10
                                ; LOE esi
.B1.577:                        ; Preds .B1.874                 ; Infreq
        mov       edi, DWORD PTR [-188+ebp]                     ;239.10
        lea       eax, DWORD PTR [-392+ebp]                     ;239.10
        push      eax                                           ;239.10
        push      OFFSET FLAT: __STRLITPACK_231.0.1             ;239.10
        mov       DWORD PTR [-392+ebp], edi                     ;239.10
        lea       edx, DWORD PTR [-232+ebp]                     ;239.10
        push      edx                                           ;239.10
        call      _for_write_seq_lis_xmit                       ;239.10
                                ; LOE esi edi
.B1.875:                        ; Preds .B1.577                 ; Infreq
        add       esp, 12                                       ;239.10
                                ; LOE esi edi
.B1.578:                        ; Preds .B1.875                 ; Infreq
        mov       DWORD PTR [-520+ebp], 6                       ;239.10
        lea       eax, DWORD PTR [-520+ebp]                     ;239.10
        push      eax                                           ;239.10
        push      OFFSET FLAT: __STRLITPACK_232.0.1             ;239.10
        mov       DWORD PTR [-516+ebp], OFFSET FLAT: __STRLITPACK_71 ;239.10
        lea       edx, DWORD PTR [-232+ebp]                     ;239.10
        push      edx                                           ;239.10
        call      _for_write_seq_lis_xmit                       ;239.10
                                ; LOE esi edi
.B1.876:                        ; Preds .B1.578                 ; Infreq
        add       esp, 12                                       ;239.10
                                ; LOE esi edi
.B1.579:                        ; Preds .B1.876                 ; Infreq
        mov       eax, DWORD PTR [-180+ebp]                     ;239.10
        lea       edx, DWORD PTR [-384+ebp]                     ;239.10
        push      edx                                           ;239.10
        push      OFFSET FLAT: __STRLITPACK_233.0.1             ;239.10
        mov       DWORD PTR [-384+ebp], eax                     ;239.10
        lea       ecx, DWORD PTR [-232+ebp]                     ;239.10
        push      ecx                                           ;239.10
        mov       DWORD PTR [-728+ebp], eax                     ;239.10
        call      _for_write_seq_lis_xmit                       ;239.10
                                ; LOE esi edi
.B1.877:                        ; Preds .B1.579                 ; Infreq
        mov       eax, DWORD PTR [-728+ebp]                     ;
        add       esp, 12                                       ;239.10
                                ; LOE eax esi edi al ah
.B1.580:                        ; Preds .B1.877                 ; Infreq
        mov       DWORD PTR [-732+ebp], -1                      ;240.4
        test      esi, esi                                      ;240.4
        jmp       .B1.193       ; Prob 100%                     ;240.4
                                ; LOE eax esi edi
.B1.581:                        ; Preds .B1.159                 ; Infreq
        push      32                                            ;210.6
        mov       DWORD PTR [-232+ebp], 0                       ;210.6
        lea       eax, DWORD PTR [-976+ebp]                     ;210.6
        push      eax                                           ;210.6
        push      OFFSET FLAT: __STRLITPACK_209.0.1             ;210.6
        push      -2088435968                                   ;210.6
        push      911                                           ;210.6
        mov       DWORD PTR [-976+ebp], 33                      ;210.6
        lea       edx, DWORD PTR [-232+ebp]                     ;210.6
        push      edx                                           ;210.6
        mov       DWORD PTR [-972+ebp], OFFSET FLAT: __STRLITPACK_90 ;210.6
        call      _for_write_seq_lis                            ;210.6
                                ; LOE edi
.B1.878:                        ; Preds .B1.581                 ; Infreq
        add       esp, 24                                       ;210.6
                                ; LOE edi
.B1.582:                        ; Preds .B1.878                 ; Infreq
        mov       eax, DWORD PTR [-844+ebp]                     ;210.6
        lea       ecx, DWORD PTR [-816+ebp]                     ;210.6
        sub       eax, edi                                      ;210.6
        lea       esi, DWORD PTR [-232+ebp]                     ;210.6
        neg       eax                                           ;210.6
        add       eax, DWORD PTR [-836+ebp]                     ;210.6
        sub       eax, DWORD PTR [-860+ebp]                     ;210.6
        push      ecx                                           ;210.6
        push      OFFSET FLAT: __STRLITPACK_210.0.1             ;210.6
        mov       edx, DWORD PTR [eax]                          ;210.6
        push      esi                                           ;210.6
        mov       DWORD PTR [-816+ebp], edx                     ;210.6
        call      _for_write_seq_lis_xmit                       ;210.6
                                ; LOE edi
.B1.879:                        ; Preds .B1.582                 ; Infreq
        add       esp, 12                                       ;210.6
                                ; LOE edi
.B1.583:                        ; Preds .B1.879                 ; Infreq
        mov       DWORD PTR [-960+ebp], 9                       ;210.6
        lea       eax, DWORD PTR [-960+ebp]                     ;210.6
        push      eax                                           ;210.6
        push      OFFSET FLAT: __STRLITPACK_211.0.1             ;210.6
        mov       DWORD PTR [-956+ebp], OFFSET FLAT: __STRLITPACK_89 ;210.6
        lea       edx, DWORD PTR [-232+ebp]                     ;210.6
        push      edx                                           ;210.6
        call      _for_write_seq_lis_xmit                       ;210.6
                                ; LOE edi
.B1.880:                        ; Preds .B1.583                 ; Infreq
        add       esp, 12                                       ;210.6
                                ; LOE edi
.B1.584:                        ; Preds .B1.880                 ; Infreq
        mov       eax, DWORD PTR [-1060+ebp]                    ;210.6
        lea       edx, DWORD PTR [-800+ebp]                     ;210.6
        push      edx                                           ;210.6
        push      OFFSET FLAT: __STRLITPACK_212.0.1             ;210.6
        mov       DWORD PTR [-800+ebp], eax                     ;210.6
        lea       ecx, DWORD PTR [-232+ebp]                     ;210.6
        push      ecx                                           ;210.6
        call      _for_write_seq_lis_xmit                       ;210.6
                                ; LOE edi
.B1.881:                        ; Preds .B1.584                 ; Infreq
        add       esp, 12                                       ;210.6
                                ; LOE edi
.B1.585:                        ; Preds .B1.881                 ; Infreq
        push      32                                            ;211.6
        xor       eax, eax                                      ;211.6
        push      eax                                           ;211.6
        push      eax                                           ;211.6
        push      -2088435968                                   ;211.6
        push      eax                                           ;211.6
        push      OFFSET FLAT: __STRLITPACK_213                 ;211.6
        call      _for_stop_core                                ;211.6
                                ; LOE edi
.B1.882:                        ; Preds .B1.585                 ; Infreq
        add       esp, 24                                       ;211.6
        jmp       .B1.160       ; Prob 100%                     ;211.6
                                ; LOE edi
.B1.586:                        ; Preds .B1.157                 ; Infreq
        push      32                                            ;202.10
        mov       DWORD PTR [-232+ebp], 0                       ;202.10
        lea       eax, DWORD PTR [-992+ebp]                     ;202.10
        push      eax                                           ;202.10
        push      OFFSET FLAT: __STRLITPACK_204.0.1             ;202.10
        push      -2088435968                                   ;202.10
        push      911                                           ;202.10
        mov       DWORD PTR [-992+ebp], 33                      ;202.10
        lea       edx, DWORD PTR [-232+ebp]                     ;202.10
        push      edx                                           ;202.10
        mov       DWORD PTR [-988+ebp], OFFSET FLAT: __STRLITPACK_94 ;202.10
        call      _for_write_seq_lis                            ;202.10
                                ; LOE edi
.B1.883:                        ; Preds .B1.586                 ; Infreq
        add       esp, 24                                       ;202.10
                                ; LOE edi
.B1.587:                        ; Preds .B1.883                 ; Infreq
        mov       eax, DWORD PTR [-844+ebp]                     ;202.10
        lea       ecx, DWORD PTR [-832+ebp]                     ;202.10
        sub       eax, edi                                      ;202.10
        lea       esi, DWORD PTR [-232+ebp]                     ;202.10
        neg       eax                                           ;202.10
        add       eax, DWORD PTR [-836+ebp]                     ;202.10
        sub       eax, DWORD PTR [-860+ebp]                     ;202.10
        push      ecx                                           ;202.10
        push      OFFSET FLAT: __STRLITPACK_205.0.1             ;202.10
        mov       edx, DWORD PTR [eax]                          ;202.10
        push      esi                                           ;202.10
        mov       DWORD PTR [-832+ebp], edx                     ;202.10
        call      _for_write_seq_lis_xmit                       ;202.10
                                ; LOE edi
.B1.884:                        ; Preds .B1.587                 ; Infreq
        add       esp, 12                                       ;202.10
                                ; LOE edi
.B1.588:                        ; Preds .B1.884                 ; Infreq
        mov       DWORD PTR [-968+ebp], 9                       ;202.10
        lea       eax, DWORD PTR [-968+ebp]                     ;202.10
        push      eax                                           ;202.10
        push      OFFSET FLAT: __STRLITPACK_206.0.1             ;202.10
        mov       DWORD PTR [-964+ebp], OFFSET FLAT: __STRLITPACK_93 ;202.10
        lea       edx, DWORD PTR [-232+ebp]                     ;202.10
        push      edx                                           ;202.10
        call      _for_write_seq_lis_xmit                       ;202.10
                                ; LOE edi
.B1.885:                        ; Preds .B1.588                 ; Infreq
        add       esp, 12                                       ;202.10
                                ; LOE edi
.B1.589:                        ; Preds .B1.885                 ; Infreq
        mov       eax, DWORD PTR [-1060+ebp]                    ;202.10
        lea       edx, DWORD PTR [-808+ebp]                     ;202.10
        push      edx                                           ;202.10
        push      OFFSET FLAT: __STRLITPACK_207.0.1             ;202.10
        mov       DWORD PTR [-808+ebp], eax                     ;202.10
        lea       ecx, DWORD PTR [-232+ebp]                     ;202.10
        push      ecx                                           ;202.10
        call      _for_write_seq_lis_xmit                       ;202.10
                                ; LOE edi
.B1.886:                        ; Preds .B1.589                 ; Infreq
        add       esp, 12                                       ;202.10
                                ; LOE edi
.B1.590:                        ; Preds .B1.886                 ; Infreq
        push      32                                            ;203.7
        xor       eax, eax                                      ;203.7
        push      eax                                           ;203.7
        push      eax                                           ;203.7
        push      -2088435968                                   ;203.7
        push      eax                                           ;203.7
        push      OFFSET FLAT: __STRLITPACK_208                 ;203.7
        call      _for_stop_core                                ;203.7
                                ; LOE edi
.B1.887:                        ; Preds .B1.590                 ; Infreq
        add       esp, 24                                       ;203.7
        jmp       .B1.158       ; Prob 100%                     ;203.7
                                ; LOE edi
.B1.591:                        ; Preds .B1.155                 ; Infreq
        push      32                                            ;195.8
        mov       DWORD PTR [-232+ebp], 0                       ;195.8
        lea       eax, DWORD PTR [-1008+ebp]                    ;195.8
        push      eax                                           ;195.8
        push      OFFSET FLAT: __STRLITPACK_199.0.1             ;195.8
        push      -2088435968                                   ;195.8
        push      911                                           ;195.8
        mov       DWORD PTR [-1008+ebp], 33                     ;195.8
        lea       edx, DWORD PTR [-232+ebp]                     ;195.8
        push      edx                                           ;195.8
        mov       DWORD PTR [-1004+ebp], OFFSET FLAT: __STRLITPACK_98 ;195.8
        call      _for_write_seq_lis                            ;195.8
                                ; LOE edi
.B1.888:                        ; Preds .B1.591                 ; Infreq
        add       esp, 24                                       ;195.8
                                ; LOE edi
.B1.592:                        ; Preds .B1.888                 ; Infreq
        mov       eax, DWORD PTR [-844+ebp]                     ;195.8
        lea       ecx, DWORD PTR [-848+ebp]                     ;195.8
        sub       eax, edi                                      ;195.8
        lea       esi, DWORD PTR [-232+ebp]                     ;195.8
        neg       eax                                           ;195.8
        add       eax, DWORD PTR [-836+ebp]                     ;195.8
        sub       eax, DWORD PTR [-860+ebp]                     ;195.8
        push      ecx                                           ;195.8
        push      OFFSET FLAT: __STRLITPACK_200.0.1             ;195.8
        mov       edx, DWORD PTR [eax]                          ;195.8
        push      esi                                           ;195.8
        mov       DWORD PTR [-848+ebp], edx                     ;195.8
        call      _for_write_seq_lis_xmit                       ;195.8
                                ; LOE edi
.B1.889:                        ; Preds .B1.592                 ; Infreq
        add       esp, 12                                       ;195.8
                                ; LOE edi
.B1.593:                        ; Preds .B1.889                 ; Infreq
        mov       DWORD PTR [-984+ebp], 9                       ;195.8
        lea       eax, DWORD PTR [-984+ebp]                     ;195.8
        push      eax                                           ;195.8
        push      OFFSET FLAT: __STRLITPACK_201.0.1             ;195.8
        mov       DWORD PTR [-980+ebp], OFFSET FLAT: __STRLITPACK_97 ;195.8
        lea       edx, DWORD PTR [-232+ebp]                     ;195.8
        push      edx                                           ;195.8
        call      _for_write_seq_lis_xmit                       ;195.8
                                ; LOE edi
.B1.890:                        ; Preds .B1.593                 ; Infreq
        add       esp, 12                                       ;195.8
                                ; LOE edi
.B1.594:                        ; Preds .B1.890                 ; Infreq
        mov       eax, DWORD PTR [-1060+ebp]                    ;195.8
        lea       edx, DWORD PTR [-824+ebp]                     ;195.8
        push      edx                                           ;195.8
        push      OFFSET FLAT: __STRLITPACK_202.0.1             ;195.8
        mov       DWORD PTR [-824+ebp], eax                     ;195.8
        lea       ecx, DWORD PTR [-232+ebp]                     ;195.8
        push      ecx                                           ;195.8
        call      _for_write_seq_lis_xmit                       ;195.8
                                ; LOE edi
.B1.891:                        ; Preds .B1.594                 ; Infreq
        add       esp, 12                                       ;195.8
                                ; LOE edi
.B1.595:                        ; Preds .B1.891                 ; Infreq
        push      32                                            ;196.8
        xor       eax, eax                                      ;196.8
        push      eax                                           ;196.8
        push      eax                                           ;196.8
        push      -2088435968                                   ;196.8
        push      eax                                           ;196.8
        push      OFFSET FLAT: __STRLITPACK_203                 ;196.8
        call      _for_stop_core                                ;196.8
                                ; LOE edi
.B1.892:                        ; Preds .B1.595                 ; Infreq
        add       esp, 24                                       ;196.8
        jmp       .B1.156       ; Prob 100%                     ;196.8
                                ; LOE edi
.B1.596:                        ; Preds .B1.152                 ; Infreq
        push      32                                            ;188.6
        mov       DWORD PTR [-232+ebp], 0                       ;188.6
        lea       eax, DWORD PTR [-1016+ebp]                    ;188.6
        push      eax                                           ;188.6
        push      OFFSET FLAT: __STRLITPACK_194.0.1             ;188.6
        push      -2088435968                                   ;188.6
        push      911                                           ;188.6
        mov       DWORD PTR [-1016+ebp], 33                     ;188.6
        lea       edx, DWORD PTR [-232+ebp]                     ;188.6
        push      edx                                           ;188.6
        mov       DWORD PTR [-1012+ebp], OFFSET FLAT: __STRLITPACK_102 ;188.6
        call      _for_write_seq_lis                            ;188.6
                                ; LOE edi
.B1.893:                        ; Preds .B1.596                 ; Infreq
        add       esp, 24                                       ;188.6
                                ; LOE edi
.B1.597:                        ; Preds .B1.893                 ; Infreq
        mov       eax, DWORD PTR [-844+ebp]                     ;188.6
        lea       ecx, DWORD PTR [-856+ebp]                     ;188.6
        sub       eax, edi                                      ;188.6
        lea       esi, DWORD PTR [-232+ebp]                     ;188.6
        neg       eax                                           ;188.6
        add       eax, DWORD PTR [-836+ebp]                     ;188.6
        sub       eax, DWORD PTR [-860+ebp]                     ;188.6
        push      ecx                                           ;188.6
        push      OFFSET FLAT: __STRLITPACK_195.0.1             ;188.6
        mov       edx, DWORD PTR [eax]                          ;188.6
        push      esi                                           ;188.6
        mov       DWORD PTR [-856+ebp], edx                     ;188.6
        call      _for_write_seq_lis_xmit                       ;188.6
                                ; LOE edi
.B1.894:                        ; Preds .B1.597                 ; Infreq
        add       esp, 12                                       ;188.6
                                ; LOE edi
.B1.598:                        ; Preds .B1.894                 ; Infreq
        mov       DWORD PTR [-1000+ebp], 9                      ;188.6
        lea       eax, DWORD PTR [-1000+ebp]                    ;188.6
        push      eax                                           ;188.6
        push      OFFSET FLAT: __STRLITPACK_196.0.1             ;188.6
        mov       DWORD PTR [-996+ebp], OFFSET FLAT: __STRLITPACK_101 ;188.6
        lea       edx, DWORD PTR [-232+ebp]                     ;188.6
        push      edx                                           ;188.6
        call      _for_write_seq_lis_xmit                       ;188.6
                                ; LOE edi
.B1.895:                        ; Preds .B1.598                 ; Infreq
        add       esp, 12                                       ;188.6
                                ; LOE edi
.B1.599:                        ; Preds .B1.895                 ; Infreq
        mov       eax, DWORD PTR [-1060+ebp]                    ;188.6
        lea       edx, DWORD PTR [-840+ebp]                     ;188.6
        push      edx                                           ;188.6
        push      OFFSET FLAT: __STRLITPACK_197.0.1             ;188.6
        mov       DWORD PTR [-840+ebp], eax                     ;188.6
        lea       ecx, DWORD PTR [-232+ebp]                     ;188.6
        push      ecx                                           ;188.6
        call      _for_write_seq_lis_xmit                       ;188.6
                                ; LOE edi
.B1.896:                        ; Preds .B1.599                 ; Infreq
        add       esp, 12                                       ;188.6
                                ; LOE edi
.B1.600:                        ; Preds .B1.896                 ; Infreq
        push      32                                            ;189.6
        xor       eax, eax                                      ;189.6
        push      eax                                           ;189.6
        push      eax                                           ;189.6
        push      -2088435968                                   ;189.6
        push      eax                                           ;189.6
        push      OFFSET FLAT: __STRLITPACK_198                 ;189.6
        call      _for_stop_core                                ;189.6
                                ; LOE edi
.B1.897:                        ; Preds .B1.600                 ; Infreq
        add       esp, 24                                       ;189.6
        jmp       .B1.153       ; Prob 100%                     ;189.6
                                ; LOE edi
.B1.602:                        ; Preds .B1.123                 ; Infreq
        push      32                                            ;151.11
        mov       DWORD PTR [-232+ebp], 0                       ;151.11
        lea       eax, DWORD PTR [-1072+ebp]                    ;151.11
        push      eax                                           ;151.11
        push      OFFSET FLAT: __STRLITPACK_171.0.1             ;151.11
        push      -2088435968                                   ;151.11
        push      911                                           ;151.11
        mov       DWORD PTR [-1072+ebp], 29                     ;151.11
        lea       edx, DWORD PTR [-232+ebp]                     ;151.11
        push      edx                                           ;151.11
        mov       DWORD PTR [-1068+ebp], OFFSET FLAT: __STRLITPACK_127 ;151.11
        call      _for_write_seq_lis                            ;151.11
                                ; LOE
.B1.898:                        ; Preds .B1.602                 ; Infreq
        add       esp, 24                                       ;151.11
                                ; LOE
.B1.603:                        ; Preds .B1.898                 ; Infreq
        push      32                                            ;152.8
        xor       eax, eax                                      ;152.8
        push      eax                                           ;152.8
        push      eax                                           ;152.8
        push      -2088435968                                   ;152.8
        push      eax                                           ;152.8
        push      OFFSET FLAT: __STRLITPACK_172                 ;152.8
        call      _for_stop_core                                ;152.8
                                ; LOE
.B1.899:                        ; Preds .B1.603                 ; Infreq
        add       esp, 24                                       ;152.8
        jmp       .B1.124       ; Prob 100%                     ;152.8
                                ; LOE
.B1.604:                        ; Preds .B1.616 .B1.79          ; Infreq
        mov       edx, DWORD PTR [-984+ebp]                     ;
        mov       ecx, DWORD PTR [-988+ebp]                     ;
        mov       esi, DWORD PTR [-960+ebp]                     ;
                                ; LOE edx ecx esi dl cl dh ch
.B1.605:                        ; Preds .B1.604                 ; Infreq
        xor       eax, eax                                      ;
        mov       DWORD PTR [-972+ebp], eax                     ;
        mov       DWORD PTR [-976+ebp], eax                     ;
        mov       DWORD PTR [-984+ebp], edx                     ;
        mov       DWORD PTR [-988+ebp], ecx                     ;
                                ; LOE esi
.B1.606:                        ; Preds .B1.611 .B1.613 .B1.605 ; Infreq
        cmp       DWORD PTR [-928+ebp], 0                       ;118.14
        jbe       .B1.615       ; Prob 0%                       ;118.14
                                ; LOE esi
.B1.607:                        ; Preds .B1.606                 ; Infreq
        mov       ecx, DWORD PTR [-988+ebp]                     ;121.17
        xor       edx, edx                                      ;
        mov       eax, DWORD PTR [-996+ebp]                     ;121.17
        xor       edi, edi                                      ;
        mov       DWORD PTR [-960+ebp], esi                     ;
        imul      ecx, DWORD PTR [16+ecx+eax], 152              ;121.17
        mov       edx, DWORD PTR [36+ecx+esi]                   ;121.17
        mov       eax, DWORD PTR [68+ecx+esi]                   ;121.17
        mov       ecx, DWORD PTR [64+ecx+esi]                   ;121.17
        imul      eax, ecx                                      ;
        sub       edx, eax                                      ;
        mov       DWORD PTR [-908+ebp], edx                     ;
        mov       DWORD PTR [-912+ebp], ecx                     ;
        ALIGN     16
                                ; LOE edi
.B1.608:                        ; Preds .B1.608 .B1.607         ; Infreq
        mov       edx, DWORD PTR [-912+ebp]                     ;121.17
        lea       ecx, DWORD PTR [1+edi+edi]                    ;121.17
        imul      ecx, edx                                      ;121.17
        mov       eax, DWORD PTR [-908+ebp]                     ;121.17
        imul      ecx, DWORD PTR [eax+ecx], 152                 ;121.17
        mov       eax, DWORD PTR [-960+ebp]                     ;121.17
        mov       esi, DWORD PTR [12+eax+ecx]                   ;121.17
        mov       ecx, DWORD PTR [-956+ebp]                     ;121.17
        lea       eax, DWORD PTR [esi*4]                        ;121.17
        shl       esi, 5                                        ;121.17
        sub       esi, eax                                      ;121.17
        xor       eax, eax                                      ;121.17
        mov       BYTE PTR [8+esi+ecx], al                      ;121.17
        lea       esi, DWORD PTR [2+edi+edi]                    ;121.17
        imul      esi, edx                                      ;121.17
        inc       edi                                           ;118.14
        mov       edx, DWORD PTR [-908+ebp]                     ;121.17
        imul      esi, DWORD PTR [edx+esi], 152                 ;121.17
        mov       edx, DWORD PTR [-960+ebp]                     ;121.17
        mov       esi, DWORD PTR [12+edx+esi]                   ;121.17
        lea       edx, DWORD PTR [esi*4]                        ;121.17
        shl       esi, 5                                        ;121.17
        sub       esi, edx                                      ;121.17
        cmp       edi, DWORD PTR [-928+ebp]                     ;118.14
        mov       BYTE PTR [8+esi+ecx], al                      ;121.17
        jb        .B1.608       ; Prob 64%                      ;118.14
                                ; LOE edi
.B1.609:                        ; Preds .B1.608                 ; Infreq
        mov       esi, DWORD PTR [-960+ebp]                     ;
        lea       eax, DWORD PTR [1+edi+edi]                    ;118.14
                                ; LOE eax esi
.B1.610:                        ; Preds .B1.609 .B1.615         ; Infreq
        lea       edx, DWORD PTR [-1+eax]                       ;118.14
        cmp       edx, DWORD PTR [-984+ebp]                     ;118.14
        jae       .B1.613       ; Prob 0%                       ;118.14
                                ; LOE eax esi
.B1.611:                        ; Preds .B1.610                 ; Infreq
        mov       edx, DWORD PTR [-988+ebp]                     ;121.17
        mov       ecx, DWORD PTR [-996+ebp]                     ;121.17
        imul      edi, DWORD PTR [16+edx+ecx], 152              ;121.17
        mov       edx, DWORD PTR [68+edi+esi]                   ;121.17
        neg       edx                                           ;121.17
        add       edx, eax                                      ;121.17
        imul      edx, DWORD PTR [64+edi+esi]                   ;121.17
        mov       eax, DWORD PTR [36+edi+esi]                   ;121.17
        imul      eax, DWORD PTR [eax+edx], 152                 ;121.17
        mov       edi, DWORD PTR [12+eax+esi]                   ;121.17
        mov       eax, DWORD PTR [-972+ebp]                     ;121.17
        mov       edx, DWORD PTR [-976+ebp]                     ;121.17
        inc       edx                                           ;121.17
        lea       ecx, DWORD PTR [edi*4]                        ;121.17
        shl       edi, 5                                        ;121.17
        sub       edi, ecx                                      ;121.17
        add       edi, DWORD PTR [-992+ebp]                     ;121.17
        mov       DWORD PTR [-976+ebp], edx                     ;121.17
        mov       BYTE PTR [8+edi+eax], 0                       ;121.17
        mov       edi, DWORD PTR [-1004+ebp]                    ;121.17
        add       eax, edi                                      ;121.17
        add       DWORD PTR [-956+ebp], edi                     ;121.17
        mov       DWORD PTR [-972+ebp], eax                     ;121.17
        cmp       edx, DWORD PTR [-1000+ebp]                    ;121.17
        jb        .B1.606       ; Prob 82%                      ;121.17
                                ; LOE esi
.B1.612:                        ; Preds .B1.611                 ; Infreq
        mov       ecx, DWORD PTR [-988+ebp]                     ;
        jmp       .B1.72        ; Prob 100%                     ;
                                ; LOE ecx esi
.B1.613:                        ; Preds .B1.610                 ; Infreq
        mov       eax, DWORD PTR [-1004+ebp]                    ;121.17
        mov       edx, DWORD PTR [-976+ebp]                     ;121.17
        inc       edx                                           ;121.17
        add       DWORD PTR [-956+ebp], eax                     ;121.17
        add       DWORD PTR [-972+ebp], eax                     ;121.17
        mov       DWORD PTR [-976+ebp], edx                     ;121.17
        cmp       edx, DWORD PTR [-1000+ebp]                    ;121.17
        jb        .B1.606       ; Prob 82%                      ;121.17
                                ; LOE esi
.B1.614:                        ; Preds .B1.613                 ; Infreq
        mov       ecx, DWORD PTR [-988+ebp]                     ;
        jmp       .B1.72        ; Prob 100%                     ;
                                ; LOE ecx esi
.B1.615:                        ; Preds .B1.606                 ; Infreq
        mov       eax, 1                                        ;
        jmp       .B1.610       ; Prob 100%                     ;
                                ; LOE eax esi
.B1.616:                        ; Preds .B1.78                  ; Infreq
        mov       edx, DWORD PTR [-1004+ebp]                    ;119.17
        inc       eax                                           ;119.17
        add       DWORD PTR [-920+ebp], edx                     ;119.17
        add       DWORD PTR [-968+ebp], edx                     ;119.17
        cmp       eax, DWORD PTR [-1000+ebp]                    ;119.17
        jb        .B1.74        ; Prob 82%                      ;119.17
        jmp       .B1.604       ; Prob 100%                     ;119.17
                                ; LOE eax
.B1.618:                        ; Preds .B1.74                  ; Infreq
        mov       ecx, 1                                        ;
        jmp       .B1.78        ; Prob 100%                     ;
                                ; LOE eax ecx
.B1.621:                        ; Preds .B1.67                  ; Infreq
        mov       eax, DWORD PTR [-1068+ebp]                    ;110.9
        inc       eax                                           ;110.9
        mov       edx, DWORD PTR [-1028+ebp]                    ;111.9
        mov       DWORD PTR [-1068+ebp], eax                    ;110.9
        mov       DWORD PTR [-4+edx+eax*4], ecx                 ;111.9
        mov       edx, DWORD PTR [-1072+ebp]                    ;112.9
        mov       ecx, DWORD PTR [12+esi+edi]                   ;112.9
        mov       DWORD PTR [-4+edx+eax*4], ecx                 ;112.9
        mov       edx, DWORD PTR [-1056+ebp]                    ;113.9
        mov       ecx, DWORD PTR [-1016+ebp]                    ;113.9
        mov       DWORD PTR [-4+edx+eax*4], ecx                 ;113.9
        mov       ecx, DWORD PTR [-1064+ebp]                    ;114.9
        mov       edx, DWORD PTR [-736+ebp]                     ;114.9
        mov       DWORD PTR [-4+ecx+eax*4], edx                 ;114.9
        add       edx, DWORD PTR [12+esi+edi]                   ;115.12
        mov       DWORD PTR [-736+ebp], edx                     ;115.12
        jmp       .B1.112       ; Prob 100%                     ;115.12
                                ; LOE
.B1.622:                        ; Preds .B1.65                  ; Infreq
        cmp       DWORD PTR [12+esi+edi], 0                     ;100.42
        jle       .B1.625       ; Prob 16%                      ;100.42
                                ; LOE edx esi edi
.B1.623:                        ; Preds .B1.622                 ; Infreq
        cmp       DWORD PTR [16+esi+edi], 0                     ;100.82
        jg        .B1.66        ; Prob 84%                      ;100.82
                                ; LOE edx esi edi
.B1.625:                        ; Preds .B1.622 .B1.623         ; Infreq
        push      32                                            ;101.12
        mov       DWORD PTR [-232+ebp], 0                       ;101.12
        lea       eax, DWORD PTR [-1088+ebp]                    ;101.12
        push      eax                                           ;101.12
        push      OFFSET FLAT: __STRLITPACK_165.0.1             ;101.12
        push      -2088435968                                   ;101.12
        push      911                                           ;101.12
        mov       DWORD PTR [-1088+ebp], 34                     ;101.12
        lea       ecx, DWORD PTR [-232+ebp]                     ;101.12
        push      ecx                                           ;101.12
        mov       DWORD PTR [-1084+ebp], OFFSET FLAT: __STRLITPACK_131 ;101.12
        mov       DWORD PTR [-1112+ebp], edx                    ;101.12
        call      _for_write_seq_lis                            ;101.12
                                ; LOE esi edi
.B1.900:                        ; Preds .B1.625                 ; Infreq
        mov       edx, DWORD PTR [-1112+ebp]                    ;
        add       esp, 24                                       ;101.12
                                ; LOE edx esi edi dl dh
.B1.626:                        ; Preds .B1.900                 ; Infreq
        push      32                                            ;102.6
        mov       DWORD PTR [-232+ebp], 0                       ;102.6
        lea       eax, DWORD PTR [-1080+ebp]                    ;102.6
        push      eax                                           ;102.6
        push      OFFSET FLAT: __STRLITPACK_166.0.1             ;102.6
        push      -2088435968                                   ;102.6
        push      911                                           ;102.6
        mov       DWORD PTR [-1080+ebp], 18                     ;102.6
        lea       ecx, DWORD PTR [-232+ebp]                     ;102.6
        push      ecx                                           ;102.6
        mov       DWORD PTR [-1076+ebp], OFFSET FLAT: __STRLITPACK_129 ;102.6
        mov       DWORD PTR [-1112+ebp], edx                    ;102.6
        call      _for_write_seq_lis                            ;102.6
                                ; LOE esi edi
.B1.901:                        ; Preds .B1.626                 ; Infreq
        mov       edx, DWORD PTR [-1112+ebp]                    ;
        add       esp, 24                                       ;102.6
                                ; LOE edx esi edi dl dh
.B1.627:                        ; Preds .B1.901                 ; Infreq
        mov       ecx, DWORD PTR [4+esi+edi]                    ;102.6
        lea       eax, DWORD PTR [-1024+ebp]                    ;102.6
        push      eax                                           ;102.6
        push      OFFSET FLAT: __STRLITPACK_167.0.1             ;102.6
        mov       DWORD PTR [-1024+ebp], ecx                    ;102.6
        lea       eax, DWORD PTR [-232+ebp]                     ;102.6
        push      eax                                           ;102.6
        mov       DWORD PTR [-1112+ebp], edx                    ;102.6
        mov       DWORD PTR [-1108+ebp], ecx                    ;102.6
        call      _for_write_seq_lis_xmit                       ;102.6
                                ; LOE esi edi
.B1.902:                        ; Preds .B1.627                 ; Infreq
        mov       ecx, DWORD PTR [-1108+ebp]                    ;
        add       esp, 12                                       ;102.6
        mov       edx, DWORD PTR [-1112+ebp]                    ;
                                ; LOE edx ecx esi edi dl cl dh ch
.B1.628:                        ; Preds .B1.902                 ; Infreq
        push      32                                            ;103.6
        xor       eax, eax                                      ;103.6
        push      eax                                           ;103.6
        push      eax                                           ;103.6
        push      -2088435968                                   ;103.6
        push      eax                                           ;103.6
        push      OFFSET FLAT: __STRLITPACK_168                 ;103.6
        mov       DWORD PTR [-1112+ebp], edx                    ;103.6
        mov       DWORD PTR [-1108+ebp], ecx                    ;103.6
        call      _for_stop_core                                ;103.6
                                ; LOE esi edi
.B1.903:                        ; Preds .B1.628                 ; Infreq
        mov       ecx, DWORD PTR [-1108+ebp]                    ;
        add       esp, 24                                       ;103.6
        mov       edx, DWORD PTR [-1112+ebp]                    ;
        jmp       .B1.67        ; Prob 100%                     ;
                                ; LOE edx ecx esi edi
.B1.629:                        ; Preds .B1.64                  ; Infreq
        push      32                                            ;96.12
        mov       DWORD PTR [-232+ebp], 0                       ;96.12
        lea       eax, DWORD PTR [-952+ebp]                     ;96.12
        push      eax                                           ;96.12
        push      OFFSET FLAT: __STRLITPACK_160.0.1             ;96.12
        push      -2088435968                                   ;96.12
        push      911                                           ;96.12
        mov       DWORD PTR [-952+ebp], 20                      ;96.12
        lea       edx, DWORD PTR [-232+ebp]                     ;96.12
        push      edx                                           ;96.12
        mov       DWORD PTR [-948+ebp], OFFSET FLAT: __STRLITPACK_137 ;96.12
        call      _for_write_seq_lis                            ;96.12
                                ; LOE esi edi
.B1.904:                        ; Preds .B1.629                 ; Infreq
        add       esp, 24                                       ;96.12
                                ; LOE esi edi
.B1.630:                        ; Preds .B1.904                 ; Infreq
        push      32                                            ;97.12
        mov       DWORD PTR [-232+ebp], 0                       ;97.12
        lea       eax, DWORD PTR [-944+ebp]                     ;97.12
        push      eax                                           ;97.12
        push      OFFSET FLAT: __STRLITPACK_161.0.1             ;97.12
        push      -2088435968                                   ;97.12
        push      911                                           ;97.12
        mov       DWORD PTR [-944+ebp], 4                       ;97.12
        lea       edx, DWORD PTR [-232+ebp]                     ;97.12
        push      edx                                           ;97.12
        mov       DWORD PTR [-940+ebp], OFFSET FLAT: __STRLITPACK_134 ;97.12
        call      _for_write_seq_lis                            ;97.12
                                ; LOE esi edi
.B1.905:                        ; Preds .B1.630                 ; Infreq
        add       esp, 24                                       ;97.12
                                ; LOE esi edi
.B1.631:                        ; Preds .B1.905                 ; Infreq
        mov       ecx, DWORD PTR [4+esi+edi]                    ;97.12
        lea       eax, DWORD PTR [-792+ebp]                     ;97.12
        push      eax                                           ;97.12
        push      OFFSET FLAT: __STRLITPACK_162.0.1             ;97.12
        mov       DWORD PTR [-792+ebp], ecx                     ;97.12
        lea       edx, DWORD PTR [-232+ebp]                     ;97.12
        push      edx                                           ;97.12
        mov       DWORD PTR [-1108+ebp], ecx                    ;97.12
        call      _for_write_seq_lis_xmit                       ;97.12
                                ; LOE esi edi
.B1.906:                        ; Preds .B1.631                 ; Infreq
        mov       ecx, DWORD PTR [-1108+ebp]                    ;
        add       esp, 12                                       ;97.12
                                ; LOE ecx esi edi cl ch
.B1.632:                        ; Preds .B1.906                 ; Infreq
        mov       DWORD PTR [-936+ebp], 19                      ;97.12
        lea       eax, DWORD PTR [-936+ebp]                     ;97.12
        push      eax                                           ;97.12
        push      OFFSET FLAT: __STRLITPACK_163.0.1             ;97.12
        mov       DWORD PTR [-932+ebp], OFFSET FLAT: __STRLITPACK_133 ;97.12
        lea       edx, DWORD PTR [-232+ebp]                     ;97.12
        push      edx                                           ;97.12
        mov       DWORD PTR [-1108+ebp], ecx                    ;97.12
        call      _for_write_seq_lis_xmit                       ;97.12
                                ; LOE esi edi
.B1.907:                        ; Preds .B1.632                 ; Infreq
        mov       ecx, DWORD PTR [-1108+ebp]                    ;
        add       esp, 12                                       ;97.12
                                ; LOE ecx esi edi cl ch
.B1.633:                        ; Preds .B1.907                 ; Infreq
        push      32                                            ;98.6
        xor       eax, eax                                      ;98.6
        push      eax                                           ;98.6
        push      eax                                           ;98.6
        push      -2088435968                                   ;98.6
        push      eax                                           ;98.6
        push      OFFSET FLAT: __STRLITPACK_164                 ;98.6
        mov       DWORD PTR [-1108+ebp], ecx                    ;98.6
        call      _for_stop_core                                ;98.6
                                ; LOE esi edi
.B1.908:                        ; Preds .B1.633                 ; Infreq
        mov       ecx, DWORD PTR [-1108+ebp]                    ;
        add       esp, 24                                       ;98.6
                                ; LOE ecx esi edi cl ch
.B1.634:                        ; Preds .B1.908                 ; Infreq
        mov       edx, DWORD PTR [-1044+ebp]                    ;109.50
        and       edx, -2                                       ;109.50
        jmp       .B1.67        ; Prob 100%                     ;109.50
                                ; LOE edx ecx esi edi
.B1.635:                        ; Preds .B1.63                  ; Infreq
        push      32                                            ;92.12
        mov       DWORD PTR [-232+ebp], 0                       ;92.12
        lea       eax, DWORD PTR [-1040+ebp]                    ;92.12
        push      eax                                           ;92.12
        push      OFFSET FLAT: __STRLITPACK_158.0.1             ;92.12
        push      -2088435968                                   ;92.12
        push      911                                           ;92.12
        mov       DWORD PTR [-1040+ebp], 30                     ;92.12
        lea       edx, DWORD PTR [-232+ebp]                     ;92.12
        push      edx                                           ;92.12
        mov       DWORD PTR [-1036+ebp], OFFSET FLAT: __STRLITPACK_139 ;92.12
        call      _for_write_seq_lis                            ;92.12
                                ; LOE
.B1.909:                        ; Preds .B1.635                 ; Infreq
        add       esp, 24                                       ;92.12
                                ; LOE
.B1.636:                        ; Preds .B1.909                 ; Infreq
        push      32                                            ;93.9
        xor       eax, eax                                      ;93.9
        push      eax                                           ;93.9
        push      eax                                           ;93.9
        push      -2088435968                                   ;93.9
        push      eax                                           ;93.9
        push      OFFSET FLAT: __STRLITPACK_159                 ;93.9
        call      _for_stop_core                                ;93.9
                                ; LOE
.B1.910:                        ; Preds .B1.636                 ; Infreq
        add       esp, 24                                       ;93.9
        jmp       .B1.64        ; Prob 100%                     ;93.9
                                ; LOE
.B1.637:                        ; Preds .B1.62                  ; Infreq
        mov       edx, DWORD PTR [_READ_INTERSECTION_CONTROLS$TMPSIGDATA.0.1] ;87.12
        mov       ecx, OFFSET FLAT: _READ_INTERSECTION_CONTROLS$TMPSIGDATA.0.1+8 ;87.12
        sub       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM+32] ;87.12
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM] ;87.12
        push      ecx                                           ;87.12
        lea       esi, DWORD PTR [eax+edx*4]                    ;87.12
        push      esi                                           ;87.12
        call      _DYNUST_SIGNAL_MODULE_mp_DECLAREPHASE         ;87.12
                                ; LOE
.B1.911:                        ; Preds .B1.637                 ; Infreq
        add       esp, 8                                        ;87.12
        jmp       .B1.63        ; Prob 100%                     ;87.12
                                ; LOE
.B1.638:                        ; Preds .B1.60                  ; Infreq
        cmp       DWORD PTR [12+edi+esi], 0                     ;78.128
        jg        .B1.61        ; Prob 84%                      ;78.128
                                ; LOE esi edi
.B1.639:                        ; Preds .B1.638                 ; Infreq
        push      32                                            ;79.10
        mov       DWORD PTR [-232+ebp], 0                       ;79.10
        lea       eax, DWORD PTR [-1104+ebp]                    ;79.10
        push      eax                                           ;79.10
        push      OFFSET FLAT: __STRLITPACK_154.0.1             ;79.10
        push      -2088435968                                   ;79.10
        push      911                                           ;79.10
        mov       DWORD PTR [-1104+ebp], 20                     ;79.10
        lea       edx, DWORD PTR [-232+ebp]                     ;79.10
        push      edx                                           ;79.10
        mov       DWORD PTR [-1100+ebp], OFFSET FLAT: __STRLITPACK_143 ;79.10
        call      _for_write_seq_lis                            ;79.10
                                ; LOE esi edi
.B1.912:                        ; Preds .B1.639                 ; Infreq
        add       esp, 24                                       ;79.10
                                ; LOE esi edi
.B1.640:                        ; Preds .B1.912                 ; Infreq
        push      32                                            ;80.10
        mov       DWORD PTR [-232+ebp], 0                       ;80.10
        lea       eax, DWORD PTR [-1096+ebp]                    ;80.10
        push      eax                                           ;80.10
        push      OFFSET FLAT: __STRLITPACK_155.0.1             ;80.10
        push      -2088435968                                   ;80.10
        push      911                                           ;80.10
        mov       DWORD PTR [-1096+ebp], 17                     ;80.10
        lea       edx, DWORD PTR [-232+ebp]                     ;80.10
        push      edx                                           ;80.10
        mov       DWORD PTR [-1092+ebp], OFFSET FLAT: __STRLITPACK_141 ;80.10
        call      _for_write_seq_lis                            ;80.10
                                ; LOE esi edi
.B1.913:                        ; Preds .B1.640                 ; Infreq
        add       esp, 24                                       ;80.10
                                ; LOE esi edi
.B1.641:                        ; Preds .B1.913                 ; Infreq
        mov       eax, DWORD PTR [4+edi+esi]                    ;80.10
        lea       edx, DWORD PTR [-1032+ebp]                    ;80.10
        push      edx                                           ;80.10
        push      OFFSET FLAT: __STRLITPACK_156.0.1             ;80.10
        mov       DWORD PTR [-1032+ebp], eax                    ;80.10
        lea       ecx, DWORD PTR [-232+ebp]                     ;80.10
        push      ecx                                           ;80.10
        call      _for_write_seq_lis_xmit                       ;80.10
                                ; LOE esi edi
.B1.914:                        ; Preds .B1.641                 ; Infreq
        add       esp, 12                                       ;80.10
                                ; LOE esi edi
.B1.642:                        ; Preds .B1.914                 ; Infreq
        push      32                                            ;81.10
        xor       eax, eax                                      ;81.10
        push      eax                                           ;81.10
        push      eax                                           ;81.10
        push      -2088435968                                   ;81.10
        push      eax                                           ;81.10
        push      OFFSET FLAT: __STRLITPACK_157                 ;81.10
        call      _for_stop_core                                ;81.10
                                ; LOE esi edi
.B1.915:                        ; Preds .B1.642                 ; Infreq
        add       esp, 24                                       ;81.10
        jmp       .B1.61        ; Prob 100%                     ;81.10
                                ; LOE esi edi
.B1.643:                        ; Preds .B1.56                  ; Infreq
        push      0                                             ;77.7
        push      OFFSET FLAT: __STRLITPACK_153.0.1             ;77.7
        lea       eax, DWORD PTR [-232+ebp]                     ;77.7
        push      eax                                           ;77.7
        call      _for_read_seq_lis_xmit                        ;77.7
                                ; LOE eax
.B1.916:                        ; Preds .B1.643                 ; Infreq
        add       esp, 12                                       ;77.7
        jmp       .B1.60        ; Prob 100%                     ;77.7
                                ; LOE eax
.B1.645:                        ; Preds .B1.33                  ; Infreq
        push      0                                             ;48.7
        push      OFFSET FLAT: __STRLITPACK_147.0.1             ;48.7
        mov       edi, DWORD PTR [-1112+ebp]                    ;
        lea       eax, DWORD PTR [-232+ebp]                     ;48.7
        push      eax                                           ;48.7
        call      _for_read_seq_lis_xmit                        ;48.7
                                ; LOE eax esi edi
.B1.917:                        ; Preds .B1.645                 ; Infreq
        add       esp, 12                                       ;48.7
        jmp       .B1.37        ; Prob 100%                     ;48.7
                                ; LOE eax esi edi
.B1.648:                        ; Preds .B1.85                  ; Infreq
        mov       edx, DWORD PTR [-1104+ebp]                    ;41.6
        inc       eax                                           ;41.6
        inc       edx                                           ;41.6
        mov       DWORD PTR [-1104+ebp], edx                    ;41.6
        cmp       edx, DWORD PTR [-1112+ebp]                    ;41.6
        jb        .B1.81        ; Prob 82%                      ;41.6
        jmp       .B1.26        ; Prob 100%                     ;41.6
                                ; LOE eax edi
.B1.650:                        ; Preds .B1.81                  ; Infreq
        mov       edx, 1                                        ;
        jmp       .B1.85        ; Prob 100%                     ;
                                ; LOE eax edx edi
.B1.651:                        ; Preds .B1.8                   ; Infreq
        cmp       ecx, 4                                        ;37.6
        jl        .B1.667       ; Prob 10%                      ;37.6
                                ; LOE eax ecx esi al ah
.B1.652:                        ; Preds .B1.651                 ; Infreq
        mov       edx, eax                                      ;37.6
        and       edx, 15                                       ;37.6
        je        .B1.655       ; Prob 50%                      ;37.6
                                ; LOE edx ecx esi
.B1.653:                        ; Preds .B1.652                 ; Infreq
        test      dl, 3                                         ;37.6
        jne       .B1.667       ; Prob 10%                      ;37.6
                                ; LOE edx ecx esi
.B1.654:                        ; Preds .B1.653                 ; Infreq
        neg       edx                                           ;37.6
        add       edx, 16                                       ;37.6
        shr       edx, 2                                        ;37.6
                                ; LOE edx ecx esi
.B1.655:                        ; Preds .B1.654 .B1.652         ; Infreq
        lea       eax, DWORD PTR [4+edx]                        ;37.6
        cmp       ecx, eax                                      ;37.6
        jl        .B1.667       ; Prob 10%                      ;37.6
                                ; LOE edx ecx esi
.B1.656:                        ; Preds .B1.655                 ; Infreq
        mov       edi, ecx                                      ;37.6
        sub       edi, edx                                      ;37.6
        and       edi, 3                                        ;37.6
        neg       edi                                           ;37.6
        add       edi, ecx                                      ;37.6
        test      edx, edx                                      ;37.6
        jbe       .B1.660       ; Prob 10%                      ;37.6
                                ; LOE edx ecx esi edi
.B1.657:                        ; Preds .B1.656                 ; Infreq
        mov       DWORD PTR [-1112+ebp], esi                    ;
        xor       eax, eax                                      ;
        mov       esi, DWORD PTR [-1104+ebp]                    ;
                                ; LOE eax edx ecx esi edi
.B1.658:                        ; Preds .B1.658 .B1.657         ; Infreq
        mov       DWORD PTR [esi+eax*4], 0                      ;37.6
        inc       eax                                           ;37.6
        cmp       eax, edx                                      ;37.6
        jb        .B1.658       ; Prob 82%                      ;37.6
                                ; LOE eax edx ecx esi edi
.B1.659:                        ; Preds .B1.658                 ; Infreq
        mov       esi, DWORD PTR [-1112+ebp]                    ;
                                ; LOE edx ecx esi edi
.B1.660:                        ; Preds .B1.656 .B1.659         ; Infreq
        mov       eax, DWORD PTR [-1104+ebp]                    ;37.6
        pxor      xmm0, xmm0                                    ;37.6
                                ; LOE eax edx ecx esi edi xmm0
.B1.661:                        ; Preds .B1.661 .B1.660         ; Infreq
        movdqa    XMMWORD PTR [eax+edx*4], xmm0                 ;37.6
        add       edx, 4                                        ;37.6
        cmp       edx, edi                                      ;37.6
        jb        .B1.661       ; Prob 82%                      ;37.6
                                ; LOE eax edx ecx esi edi xmm0
.B1.663:                        ; Preds .B1.661 .B1.667         ; Infreq
        cmp       edi, ecx                                      ;37.6
        jae       .B1.10        ; Prob 10%                      ;37.6
                                ; LOE ecx esi edi
.B1.664:                        ; Preds .B1.663                 ; Infreq
        mov       eax, DWORD PTR [-1104+ebp]                    ;
                                ; LOE eax ecx esi edi
.B1.665:                        ; Preds .B1.665 .B1.664         ; Infreq
        mov       DWORD PTR [eax+edi*4], 0                      ;37.6
        inc       edi                                           ;37.6
        cmp       edi, ecx                                      ;37.6
        jb        .B1.665       ; Prob 82%                      ;37.6
        jmp       .B1.10        ; Prob 100%                     ;37.6
                                ; LOE eax ecx esi edi
.B1.667:                        ; Preds .B1.651 .B1.655 .B1.653 ; Infreq
        xor       eax, eax                                      ;37.6
        xor       edi, edi                                      ;37.6
        jmp       .B1.663       ; Prob 100%                     ;37.6
                                ; LOE ecx esi edi
.B1.670:                        ; Preds .B1.92                  ; Infreq
        mov       edx, DWORD PTR [-1104+ebp]                    ;40.6
        inc       eax                                           ;40.6
        inc       edx                                           ;40.6
        mov       DWORD PTR [-1104+ebp], edx                    ;40.6
        cmp       edx, DWORD PTR [-1112+ebp]                    ;40.6
        jb        .B1.88        ; Prob 82%                      ;40.6
                                ; LOE eax edi
.B1.671:                        ; Preds .B1.670                 ; Infreq
        mov       edx, DWORD PTR [-1092+ebp]                    ;
        jmp       .B1.25        ; Prob 100%                     ;
                                ; LOE edx
.B1.672:                        ; Preds .B1.88                  ; Infreq
        mov       edx, 1                                        ;
        jmp       .B1.92        ; Prob 100%                     ;
                                ; LOE eax edx edi
.B1.673:                        ; Preds .B1.12                  ; Infreq
        cmp       edi, 4                                        ;38.3
        jl        .B1.689       ; Prob 10%                      ;38.3
                                ; LOE esi edi
.B1.674:                        ; Preds .B1.673                 ; Infreq
        mov       ecx, DWORD PTR [-1104+ebp]                    ;38.3
        and       ecx, 15                                       ;38.3
        je        .B1.677       ; Prob 50%                      ;38.3
                                ; LOE ecx esi edi
.B1.675:                        ; Preds .B1.674                 ; Infreq
        test      cl, 3                                         ;38.3
        jne       .B1.689       ; Prob 10%                      ;38.3
                                ; LOE ecx esi edi
.B1.676:                        ; Preds .B1.675                 ; Infreq
        neg       ecx                                           ;38.3
        add       ecx, 16                                       ;38.3
        shr       ecx, 2                                        ;38.3
                                ; LOE ecx esi edi
.B1.677:                        ; Preds .B1.676 .B1.674         ; Infreq
        lea       eax, DWORD PTR [4+ecx]                        ;38.3
        cmp       edi, eax                                      ;38.3
        jl        .B1.689       ; Prob 10%                      ;38.3
                                ; LOE ecx esi edi
.B1.678:                        ; Preds .B1.677                 ; Infreq
        mov       edx, edi                                      ;38.3
        sub       edx, ecx                                      ;38.3
        and       edx, 3                                        ;38.3
        neg       edx                                           ;38.3
        add       edx, edi                                      ;38.3
        test      ecx, ecx                                      ;38.3
        jbe       .B1.682       ; Prob 10%                      ;38.3
                                ; LOE edx ecx esi edi
.B1.679:                        ; Preds .B1.678                 ; Infreq
        xor       eax, eax                                      ;
                                ; LOE eax edx ecx esi edi
.B1.680:                        ; Preds .B1.680 .B1.679         ; Infreq
        mov       DWORD PTR [esi+eax*4], 0                      ;38.3
        inc       eax                                           ;38.3
        cmp       eax, ecx                                      ;38.3
        jb        .B1.680       ; Prob 82%                      ;38.3
                                ; LOE eax edx ecx esi edi
.B1.682:                        ; Preds .B1.680 .B1.678         ; Infreq
        pxor      xmm0, xmm0                                    ;
                                ; LOE edx ecx esi edi xmm0
.B1.683:                        ; Preds .B1.683 .B1.682         ; Infreq
        movdqa    XMMWORD PTR [esi+ecx*4], xmm0                 ;38.3
        add       ecx, 4                                        ;38.3
        cmp       ecx, edx                                      ;38.3
        jb        .B1.683       ; Prob 82%                      ;38.3
                                ; LOE edx ecx esi edi xmm0
.B1.685:                        ; Preds .B1.683 .B1.689         ; Infreq
        cmp       edx, edi                                      ;38.3
        jae       .B1.14        ; Prob 10%                      ;38.3
                                ; LOE edx esi edi
.B1.687:                        ; Preds .B1.685 .B1.687         ; Infreq
        mov       DWORD PTR [esi+edx*4], 0                      ;38.3
        inc       edx                                           ;38.3
        cmp       edx, edi                                      ;38.3
        jb        .B1.687       ; Prob 82%                      ;38.3
        jmp       .B1.14        ; Prob 100%                     ;38.3
                                ; LOE edx esi edi
.B1.689:                        ; Preds .B1.673 .B1.677 .B1.675 ; Infreq
        xor       edx, edx                                      ;38.3
        jmp       .B1.685       ; Prob 100%                     ;38.3
                                ; LOE edx esi edi
.B1.692:                        ; Preds .B1.3                   ; Infreq
        cmp       esi, 4                                        ;36.6
        jl        .B1.709       ; Prob 10%                      ;36.6
                                ; LOE ecx esi edi
.B1.693:                        ; Preds .B1.692                 ; Infreq
        mov       edx, DWORD PTR [-1028+ebp]                    ;36.6
        add       edx, ecx                                      ;36.6
        and       edx, 15                                       ;36.6
        je        .B1.696       ; Prob 50%                      ;36.6
                                ; LOE edx ecx esi edi
.B1.694:                        ; Preds .B1.693                 ; Infreq
        test      dl, 3                                         ;36.6
        jne       .B1.709       ; Prob 10%                      ;36.6
                                ; LOE edx ecx esi edi
.B1.695:                        ; Preds .B1.694                 ; Infreq
        neg       edx                                           ;36.6
        add       edx, 16                                       ;36.6
        shr       edx, 2                                        ;36.6
                                ; LOE edx ecx esi edi
.B1.696:                        ; Preds .B1.695 .B1.693         ; Infreq
        mov       DWORD PTR [-1088+ebp], edx                    ;
        lea       edx, DWORD PTR [4+edx]                        ;36.6
        cmp       esi, edx                                      ;36.6
        mov       edx, DWORD PTR [-1088+ebp]                    ;36.6
        jl        .B1.709       ; Prob 10%                      ;36.6
                                ; LOE edx ecx esi edi dl dh
.B1.697:                        ; Preds .B1.696                 ; Infreq
        mov       eax, esi                                      ;36.6
        sub       eax, edx                                      ;36.6
        and       eax, 3                                        ;36.6
        neg       eax                                           ;36.6
        add       eax, esi                                      ;36.6
        mov       DWORD PTR [-1084+ebp], eax                    ;36.6
        test      edx, edx                                      ;36.6
        mov       eax, DWORD PTR [-1028+ebp]                    ;36.6
        lea       eax, DWORD PTR [eax+ecx]                      ;36.6
        mov       DWORD PTR [-1080+ebp], eax                    ;36.6
        jbe       .B1.701       ; Prob 10%                      ;36.6
                                ; LOE edx ecx esi edi dl dh
.B1.698:                        ; Preds .B1.697                 ; Infreq
        xor       eax, eax                                      ;
        mov       esi, eax                                      ;
        mov       eax, DWORD PTR [-1080+ebp]                    ;
                                ; LOE eax edx ecx esi edi
.B1.699:                        ; Preds .B1.699 .B1.698         ; Infreq
        mov       DWORD PTR [eax+esi*4], 0                      ;36.6
        inc       esi                                           ;36.6
        cmp       esi, edx                                      ;36.6
        jb        .B1.699       ; Prob 82%                      ;36.6
                                ; LOE eax edx ecx esi edi
.B1.700:                        ; Preds .B1.699                 ; Infreq
        mov       esi, DWORD PTR [-1048+ebp]                    ;
                                ; LOE edx ecx esi edi
.B1.701:                        ; Preds .B1.697 .B1.700         ; Infreq
        mov       esi, DWORD PTR [-1084+ebp]                    ;
        mov       eax, DWORD PTR [-1080+ebp]                    ;
        pxor      xmm0, xmm0                                    ;
                                ; LOE eax edx ecx esi edi xmm0
.B1.702:                        ; Preds .B1.702 .B1.701         ; Infreq
        movdqa    XMMWORD PTR [eax+edx*4], xmm0                 ;36.6
        add       edx, 4                                        ;36.6
        cmp       edx, esi                                      ;36.6
        jb        .B1.702       ; Prob 82%                      ;36.6
                                ; LOE eax edx ecx esi edi xmm0
.B1.703:                        ; Preds .B1.702                 ; Infreq
        mov       DWORD PTR [-1084+ebp], esi                    ;
        mov       esi, DWORD PTR [-1048+ebp]                    ;
                                ; LOE ecx esi edi
.B1.704:                        ; Preds .B1.703 .B1.709         ; Infreq
        cmp       esi, DWORD PTR [-1084+ebp]                    ;36.6
        jbe       .B1.710       ; Prob 10%                      ;36.6
                                ; LOE ecx esi edi
.B1.705:                        ; Preds .B1.704                 ; Infreq
        mov       edx, DWORD PTR [-1084+ebp]                    ;
                                ; LOE edx ecx esi edi
.B1.706:                        ; Preds .B1.706 .B1.705         ; Infreq
        mov       DWORD PTR [edi+edx*4], 0                      ;36.6
        inc       edx                                           ;36.6
        cmp       edx, esi                                      ;36.6
        jb        .B1.706       ; Prob 82%                      ;36.6
                                ; LOE edx ecx esi edi
.B1.707:                        ; Preds .B1.706                 ; Infreq
        mov       edx, DWORD PTR [-1100+ebp]                    ;36.6
        lea       edi, DWORD PTR [edi+esi*4]                    ;36.6
        lea       ecx, DWORD PTR [ecx+esi*4]                    ;36.6
        lea       edx, DWORD PTR [edx+esi*4]                    ;36.6
        mov       DWORD PTR [-1100+ebp], edx                    ;36.6
        mov       edx, DWORD PTR [-1096+ebp]                    ;36.6
        inc       edx                                           ;36.6
        mov       DWORD PTR [-1096+ebp], edx                    ;36.6
        cmp       edx, 2                                        ;36.6
        jb        .B1.3         ; Prob 50%                      ;36.6
        jmp       .B1.6         ; Prob 100%                     ;36.6
                                ; LOE ecx esi edi
.B1.709:                        ; Preds .B1.692 .B1.696 .B1.694 ; Infreq
        xor       eax, eax                                      ;36.6
        mov       DWORD PTR [-1084+ebp], eax                    ;36.6
        jmp       .B1.704       ; Prob 100%                     ;36.6
                                ; LOE ecx esi edi
.B1.710:                        ; Preds .B1.704                 ; Infreq
        mov       edx, DWORD PTR [-1100+ebp]                    ;36.6
        lea       edi, DWORD PTR [edi+esi*4]                    ;36.6
        lea       ecx, DWORD PTR [ecx+esi*4]                    ;36.6
        lea       edx, DWORD PTR [edx+esi*4]                    ;36.6
        mov       DWORD PTR [-1100+ebp], edx                    ;36.6
        mov       edx, DWORD PTR [-1096+ebp]                    ;36.6
        inc       edx                                           ;36.6
        mov       DWORD PTR [-1096+ebp], edx                    ;36.6
        cmp       edx, 2                                        ;36.6
        jb        .B1.3         ; Prob 50%                      ;36.6
        jmp       .B1.6         ; Prob 100%                     ;36.6
        ALIGN     16
                                ; LOE ecx esi edi
; mark_end;
_READ_INTERSECTION_CONTROLS ENDP
_TEXT	ENDS
_BSS	SEGMENT  DWORD PUBLIC FLAT  'BSS'
	ALIGN 004H
_READ_INTERSECTION_CONTROLS$TMPMJAPH.0.1	DB ?	; pad
	ORG $+398	; pad
	DB ?	; pad
	DD 4 DUP (0H)	; pad
_READ_INTERSECTION_CONTROLS$TMPMNAPH.0.1	DB ?	; pad
	ORG $+398	; pad
	DB ?	; pad
_READ_INTERSECTION_CONTROLS$TMPSIGDATA.0.1	DD 10 DUP (0H)	; pad
_BSS	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
READ_INTERSECTION_CONTROLS$format_pack.0.1	DB	54
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	5
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	2
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	2
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	4
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	11
	DB	0
	DB	0
	DB	0
	DB	4
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	15
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	11
	DB	0
	DB	115
	DB	105
	DB	103
	DB	110
	DB	97
	DB	108
	DB	32
	DB	110
	DB	111
	DB	100
	DB	101
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	6
	DB	0
	DB	32
	DB	80
	DB	104
	DB	97
	DB	115
	DB	101
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	3
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	14
	DB	0
	DB	32
	DB	105
	DB	110
	DB	98
	DB	111
	DB	117
	DB	110
	DB	100
	DB	32
	DB	110
	DB	111
	DB	100
	DB	101
	DB	115
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	10
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	14
	DB	0
	DB	32
	DB	105
	DB	110
	DB	98
	DB	111
	DB	117
	DB	110
	DB	100
	DB	32
	DB	110
	DB	111
	DB	100
	DB	101
	DB	115
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	10
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	14
	DB	0
	DB	32
	DB	105
	DB	110
	DB	98
	DB	111
	DB	117
	DB	110
	DB	100
	DB	32
	DB	110
	DB	111
	DB	100
	DB	101
	DB	115
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	10
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	12
	DB	0
	DB	105
	DB	110
	DB	98
	DB	111
	DB	117
	DB	110
	DB	100
	DB	32
	DB	110
	DB	111
	DB	100
	DB	101
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	5
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	9
	DB	0
	DB	32
	DB	102
	DB	111
	DB	114
	DB	32
	DB	110
	DB	111
	DB	100
	DB	101
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	5
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	9
	DB	0
	DB	32
	DB	97
	DB	116
	DB	32
	DB	112
	DB	104
	DB	97
	DB	115
	DB	101
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	5
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	15
	DB	0
	DB	32
	DB	105
	DB	115
	DB	32
	DB	110
	DB	111
	DB	116
	DB	32
	DB	99
	DB	111
	DB	114
	DB	114
	DB	101
	DB	99
	DB	116
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	43
	DB	0
	DB	83
	DB	105
	DB	103
	DB	110
	DB	115
	DB	47
	DB	83
	DB	105
	DB	103
	DB	110
	DB	97
	DB	108
	DB	115
	DB	32
	DB	102
	DB	111
	DB	117
	DB	110
	DB	100
	DB	32
	DB	111
	DB	110
	DB	32
	DB	102
	DB	114
	DB	101
	DB	101
	DB	119
	DB	97
	DB	121
	DB	32
	DB	115
	DB	101
	DB	99
	DB	116
	DB	105
	DB	111
	DB	110
	DB	32
	DB	102
	DB	114
	DB	111
	DB	109
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	4
	DB	0
	DB	32
	DB	32
	DB	116
	DB	111
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	43
	DB	0
	DB	83
	DB	105
	DB	103
	DB	110
	DB	115
	DB	47
	DB	83
	DB	105
	DB	103
	DB	110
	DB	97
	DB	108
	DB	115
	DB	32
	DB	102
	DB	111
	DB	117
	DB	110
	DB	100
	DB	32
	DB	111
	DB	110
	DB	32
	DB	102
	DB	114
	DB	101
	DB	101
	DB	119
	DB	97
	DB	121
	DB	32
	DB	115
	DB	101
	DB	99
	DB	116
	DB	105
	DB	111
	DB	110
	DB	32
	DB	102
	DB	114
	DB	111
	DB	109
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	4
	DB	0
	DB	32
	DB	32
	DB	116
	DB	111
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
_DATA	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
__STRLITPACK_145.0.1	DB	2
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_146.0.1	DB	9
	DB	5
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_147.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_148.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_149.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_150.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_151.0.1	DB	2
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_152.0.1	DB	9
	DB	5
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_153.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_154.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_155.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_156.0.1	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__NLITPACK_0.0.1	DD	10
__STRLITPACK_158.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_165.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_166.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_167.0.1	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_160.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_161.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_162.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_163.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_169.0.1	DB	9
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_170.0.1	DB	9
	DB	7
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_171.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_173.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_174.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_175.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_176.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_177.0.1	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_178.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_179.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_180.0.1	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_181.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_182.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_184.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_185.0.1	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_186.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_187.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_188.0.1	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_189.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_190.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_191.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_192.0.1	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_194.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_195.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_196.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_197.0.1	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_199.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_200.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_201.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_202.0.1	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_204.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_205.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_206.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_207.0.1	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_209.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_210.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_211.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_212.0.1	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__NLITPACK_1.0.1	DD	13
__STRLITPACK_214.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_215.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_216.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_217.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_218.0.1	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_219.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_220.0.1	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_221.0.1	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_222.0.1	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_223.0.1	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_225.0.1	DB	9
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_226.0.1	DB	9
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_227.0.1	DB	9
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_228.0.1	DB	9
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_229.0.1	DB	9
	DB	7
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_230.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_231.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_232.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_233.0.1	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_234.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_235.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_236.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_237.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_238.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_239.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_240.0.1	DB	9
	DB	7
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_242.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_243.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_244.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_245.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_246.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_247.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_248.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_249.0.1	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_251.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_252.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_253.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_254.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_255.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_256.0.1	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_257.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_258.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_259.0.1	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_261.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_262.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_263.0.1	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_266.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_267.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_269.0.1	DB	9
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_270.0.1	DB	9
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_271.0.1	DB	9
	DB	3
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_272.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_274.0.1	DB	2
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_275.0.1	DB	9
	DB	5
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_276.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_277.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_279.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_280.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_281.0.1	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_283.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_284.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_285.0.1	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_287.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_288.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_289.0.1	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_291.0.1	DB	2
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_292.0.1	DB	9
	DB	5
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_293.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_294.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_296.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_297.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_298.0.1	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_300.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_301.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_302.0.1	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_304.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_305.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_306.0.1	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_308.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_309.0.1	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_310.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_311.0.1	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _READ_INTERSECTION_CONTROLS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DD 1 DUP (0H)	; pad
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_67	DB	102
	DB	114
	DB	111
	DB	109
	DB	44
	DB	116
	DB	111
	DB	44
	DB	112
	DB	104
	DB	97
	DB	115
	DB	101
	DB	107
	DB	32
	DB	110
	DB	117
	DB	109
	DB	98
	DB	101
	DB	114
	DB	44
	DB	35
	DB	32
	DB	111
	DB	102
	DB	32
	DB	111
	DB	117
	DB	116
	DB	98
	DB	111
	DB	117
	DB	110
	DB	100
	DB	32
	DB	108
	DB	105
	DB	110
	DB	107
	DB	115
	DB	44
	DB	32
	DB	111
	DB	117
	DB	116
	DB	98
	DB	111
	DB	117
	DB	110
	DB	100
	DB	32
	DB	110
	DB	111
	DB	100
	DB	101
	DB	32
	DB	110
	DB	117
	DB	109
	DB	98
	DB	101
	DB	114
	DB	46
	DB	46
	DB	46
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_143	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	99
	DB	111
	DB	110
	DB	116
	DB	114
	DB	111
	DB	108
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_141	DB	80
	DB	108
	DB	101
	DB	97
	DB	115
	DB	101
	DB	32
	DB	99
	DB	104
	DB	101
	DB	99
	DB	107
	DB	32
	DB	110
	DB	111
	DB	100
	DB	101
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_157	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_139	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	119
	DB	104
	DB	101
	DB	110
	DB	32
	DB	114
	DB	101
	DB	97
	DB	100
	DB	105
	DB	110
	DB	103
	DB	32
	DB	99
	DB	111
	DB	110
	DB	116
	DB	114
	DB	111
	DB	108
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_159	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_131	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	112
	DB	104
	DB	97
	DB	115
	DB	105
	DB	110
	DB	103
	DB	32
	DB	97
	DB	110
	DB	100
	DB	32
	DB	99
	DB	121
	DB	99
	DB	108
	DB	101
	DB	32
	DB	115
	DB	101
	DB	116
	DB	116
	DB	105
	DB	110
	DB	103
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_129	DB	80
	DB	108
	DB	101
	DB	97
	DB	115
	DB	101
	DB	32
	DB	99
	DB	104
	DB	101
	DB	99
	DB	107
	DB	32
	DB	110
	DB	111
	DB	100
	DB	101
	DB	32
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_168	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_137	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	99
	DB	111
	DB	110
	DB	116
	DB	114
	DB	111
	DB	108
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_134	DB	110
	DB	111
	DB	100
	DB	101
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_133	DB	115
	DB	105
	DB	103
	DB	110
	DB	97
	DB	108
	DB	32
	DB	116
	DB	121
	DB	112
	DB	101
	DB	32
	DB	105
	DB	110
	DB	118
	DB	97
	DB	108
	DB	105
	DB	100
	DB	0
__STRLITPACK_164	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_127	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	119
	DB	104
	DB	101
	DB	110
	DB	32
	DB	114
	DB	101
	DB	97
	DB	100
	DB	105
	DB	110
	DB	103
	DB	32
	DB	115
	DB	105
	DB	103
	DB	110
	DB	97
	DB	108
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_172	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_125	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	109
	DB	97
	DB	116
	DB	99
	DB	104
	DB	105
	DB	110
	DB	103
	DB	32
	DB	115
	DB	105
	DB	103
	DB	110
	DB	97
	DB	108
	DB	115
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_122	DB	110
	DB	111
	DB	100
	DB	101
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_121	DB	112
	DB	104
	DB	97
	DB	115
	DB	101
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_119	DB	105
	DB	99
	DB	111
	DB	117
	DB	110
	DB	116
	DB	51
	DB	44
	DB	32
	DB	67
	DB	104
	DB	101
	DB	99
	DB	107
	DB	83
	DB	105
	DB	103
	DB	110
	DB	97
	DB	108
	DB	40
	DB	105
	DB	99
	DB	111
	DB	117
	DB	110
	DB	116
	DB	50
	DB	44
	DB	50
	DB	41
	DB	0
__STRLITPACK_117	DB	112
	DB	111
	DB	115
	DB	115
	DB	105
	DB	98
	DB	108
	DB	101
	DB	32
	DB	114
	DB	101
	DB	97
	DB	115
	DB	111
	DB	110
	DB	58
	DB	32
	DB	109
	DB	105
	DB	115
	DB	115
	DB	105
	DB	110
	DB	103
	DB	32
	DB	112
	DB	104
	DB	97
	DB	115
	DB	105
	DB	110
	DB	103
	DB	32
	DB	102
	DB	111
	DB	114
	DB	32
	DB	112
	DB	114
	DB	101
	DB	118
	DB	105
	DB	111
	DB	117
	DB	115
	DB	32
	DB	115
	DB	105
	DB	103
	DB	110
	DB	97
	DB	108
	DB	105
	DB	122
	DB	101
	DB	100
	DB	32
	DB	110
	DB	111
	DB	100
	DB	101
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_115	DB	99
	DB	104
	DB	101
	DB	99
	DB	107
	DB	32
	DB	115
	DB	105
	DB	103
	DB	110
	DB	97
	DB	108
	DB	32
	DB	116
	DB	121
	DB	112
	DB	101
	DB	115
	DB	32
	DB	105
	DB	110
	DB	32
	DB	116
	DB	104
	DB	101
	DB	32
	DB	102
	DB	105
	DB	114
	DB	115
	DB	116
	DB	32
	DB	98
	DB	108
	DB	111
	DB	99
	DB	107
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_183	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_113	DB	78
	DB	117
	DB	109
	DB	98
	DB	101
	DB	114
	DB	32
	DB	111
	DB	102
	DB	32
	DB	105
	DB	110
	DB	98
	DB	111
	DB	117
	DB	110
	DB	100
	DB	32
	DB	108
	DB	105
	DB	110
	DB	107
	DB	115
	DB	32
	DB	102
	DB	111
	DB	114
	DB	32
	DB	110
	DB	111
	DB	100
	DB	101
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_111	DB	105
	DB	115
	DB	32
	DB	110
	DB	111
	DB	116
	DB	32
	DB	115
	DB	112
	DB	101
	DB	99
	DB	105
	DB	102
	DB	105
	DB	101
	DB	100
	DB	32
	DB	99
	DB	111
	DB	114
	DB	114
	DB	101
	DB	99
	DB	116
	DB	108
	DB	121
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_109	DB	80
	DB	104
	DB	97
	DB	115
	DB	101
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_106	DB	82
	DB	101
	DB	97
	DB	100
	DB	32
	DB	97
	DB	115
	DB	32
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_105	DB	99
	DB	111
	DB	117
	DB	110
	DB	116
	DB	101
	DB	100
	DB	32
	DB	97
	DB	115
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_193	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_102	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	110
	DB	111
	DB	100
	DB	101
	DB	32
	DB	110
	DB	117
	DB	109
	DB	98
	DB	101
	DB	114
	DB	32
	DB	102
	DB	111
	DB	114
	DB	32
	DB	112
	DB	104
	DB	97
	DB	115
	DB	101
	DB	32
	DB	35
	DB	54
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_101	DB	102
	DB	111
	DB	114
	DB	32
	DB	110
	DB	111
	DB	100
	DB	101
	DB	32
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_198	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_98	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	110
	DB	111
	DB	100
	DB	101
	DB	32
	DB	110
	DB	117
	DB	109
	DB	98
	DB	101
	DB	114
	DB	32
	DB	102
	DB	111
	DB	114
	DB	32
	DB	112
	DB	104
	DB	97
	DB	115
	DB	101
	DB	32
	DB	35
	DB	55
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_97	DB	102
	DB	111
	DB	114
	DB	32
	DB	110
	DB	111
	DB	100
	DB	101
	DB	32
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_203	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_94	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	110
	DB	111
	DB	100
	DB	101
	DB	32
	DB	110
	DB	117
	DB	109
	DB	98
	DB	101
	DB	114
	DB	32
	DB	102
	DB	111
	DB	114
	DB	32
	DB	112
	DB	104
	DB	97
	DB	115
	DB	101
	DB	32
	DB	35
	DB	56
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_93	DB	102
	DB	111
	DB	114
	DB	32
	DB	110
	DB	111
	DB	100
	DB	101
	DB	32
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_208	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_90	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	110
	DB	111
	DB	100
	DB	101
	DB	32
	DB	110
	DB	117
	DB	109
	DB	98
	DB	101
	DB	114
	DB	32
	DB	102
	DB	111
	DB	114
	DB	32
	DB	112
	DB	104
	DB	97
	DB	115
	DB	101
	DB	32
	DB	35
	DB	57
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_89	DB	102
	DB	111
	DB	114
	DB	32
	DB	110
	DB	111
	DB	100
	DB	101
	DB	32
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_213	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_87	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	115
	DB	105
	DB	103
	DB	110
	DB	97
	DB	108
	DB	32
	DB	102
	DB	105
	DB	108
	DB	101
	DB	44
	DB	32
	DB	108
	DB	105
	DB	110
	DB	107
	DB	32
	DB	100
	DB	111
	DB	101
	DB	115
	DB	110
	DB	116
	DB	32
	DB	101
	DB	120
	DB	105
	DB	115
	DB	116
	DB	0
__STRLITPACK_84	DB	105
	DB	110
	DB	98
	DB	111
	DB	117
	DB	110
	DB	100
	DB	0
__STRLITPACK_83	DB	115
	DB	105
	DB	103
	DB	110
	DB	97
	DB	108
	DB	32
	DB	110
	DB	111
	DB	100
	DB	101
	DB	0
__STRLITPACK_224	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_72	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	112
	DB	104
	DB	97
	DB	115
	DB	101
	DB	32
	DB	109
	DB	111
	DB	118
	DB	101
	DB	109
	DB	101
	DB	110
	DB	116
	DB	32
	DB	102
	DB	111
	DB	114
	DB	32
	DB	110
	DB	111
	DB	100
	DB	101
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_71	DB	112
	DB	104
	DB	97
	DB	115
	DB	101
	DB	32
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_69	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	119
	DB	104
	DB	101
	DB	110
	DB	32
	DB	114
	DB	101
	DB	97
	DB	100
	DB	105
	DB	110
	DB	103
	DB	32
	DB	112
	DB	104
	DB	97
	DB	115
	DB	105
	DB	110
	DB	103
	DB	32
	DB	105
	DB	110
	DB	32
	DB	99
	DB	111
	DB	110
	DB	116
	DB	114
	DB	111
	DB	108
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_241	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_62	DB	110
	DB	111
	DB	100
	DB	101
	DB	32
	DB	100
	DB	111
	DB	101
	DB	115
	DB	32
	DB	110
	DB	111
	DB	116
	DB	32
	DB	101
	DB	120
	DB	105
	DB	115
	DB	116
	DB	44
	DB	32
	DB	102
	DB	114
	DB	111
	DB	109
	DB	32
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_61	DB	116
	DB	111
	DB	32
	DB	0
__STRLITPACK_60	DB	112
	DB	104
	DB	97
	DB	115
	DB	101
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_59	DB	111
	DB	117
	DB	116
	DB	98
	DB	111
	DB	117
	DB	110
	DB	100
	DB	32
	DB	110
	DB	111
	DB	100
	DB	101
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_250	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_55	DB	102
	DB	114
	DB	111
	DB	109
	DB	32
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_54	DB	116
	DB	111
	DB	32
	DB	0
__STRLITPACK_53	DB	112
	DB	104
	DB	97
	DB	115
	DB	101
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_51	DB	108
	DB	105
	DB	110
	DB	107
	DB	32
	DB	85
	DB	112
	DB	78
	DB	111
	DB	100
	DB	101
	DB	32
	DB	45
	DB	62
	DB	32
	DB	68
	DB	78
	DB	111
	DB	100
	DB	101
	DB	32
	DB	100
	DB	111
	DB	101
	DB	115
	DB	32
	DB	110
	DB	111
	DB	116
	DB	32
	DB	101
	DB	120
	DB	105
	DB	116
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_260	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_264	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_265	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_47	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	119
	DB	104
	DB	101
	DB	110
	DB	32
	DB	114
	DB	101
	DB	97
	DB	100
	DB	105
	DB	110
	DB	103
	DB	32
	DB	50
	DB	45
	DB	119
	DB	97
	DB	121
	DB	47
	DB	121
	DB	105
	DB	101
	DB	108
	DB	100
	DB	32
	DB	105
	DB	110
	DB	32
	DB	99
	DB	111
	DB	110
	DB	116
	DB	114
	DB	111
	DB	108
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_268	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_45	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	119
	DB	104
	DB	101
	DB	110
	DB	32
	DB	114
	DB	101
	DB	97
	DB	100
	DB	105
	DB	110
	DB	103
	DB	32
	DB	50
	DB	45
	DB	119
	DB	97
	DB	121
	DB	47
	DB	121
	DB	105
	DB	101
	DB	108
	DB	100
	DB	32
	DB	105
	DB	110
	DB	32
	DB	99
	DB	111
	DB	110
	DB	116
	DB	114
	DB	111
	DB	108
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_273	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_43	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	119
	DB	104
	DB	101
	DB	110
	DB	32
	DB	114
	DB	101
	DB	97
	DB	100
	DB	105
	DB	110
	DB	103
	DB	32
	DB	50
	DB	45
	DB	119
	DB	97
	DB	121
	DB	47
	DB	121
	DB	105
	DB	101
	DB	108
	DB	100
	DB	32
	DB	105
	DB	110
	DB	32
	DB	99
	DB	111
	DB	110
	DB	116
	DB	114
	DB	111
	DB	108
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_278	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_41	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	116
	DB	119
	DB	111
	DB	45
	DB	119
	DB	97
	DB	121
	DB	32
	DB	115
	DB	105
	DB	103
	DB	110
	DB	97
	DB	108
	DB	32
	DB	109
	DB	97
	DB	106
	DB	111
	DB	114
	DB	32
	DB	115
	DB	116
	DB	46
	DB	32
	DB	115
	DB	112
	DB	101
	DB	99
	DB	46
	DB	0
__STRLITPACK_39	DB	32
	DB	99
	DB	104
	DB	101
	DB	99
	DB	107
	DB	32
	DB	110
	DB	111
	DB	100
	DB	101
	DB	32
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_282	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_37	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	116
	DB	119
	DB	111
	DB	45
	DB	119
	DB	97
	DB	121
	DB	32
	DB	83
	DB	84
	DB	79
	DB	80
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_35	DB	99
	DB	104
	DB	101
	DB	99
	DB	107
	DB	32
	DB	109
	DB	97
	DB	106
	DB	111
	DB	114
	DB	32
	DB	115
	DB	116
	DB	32
	DB	115
	DB	112
	DB	101
	DB	99
	DB	32
	DB	102
	DB	111
	DB	114
	DB	32
	DB	110
	DB	111
	DB	100
	DB	101
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_286	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_33	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	116
	DB	119
	DB	111
	DB	45
	DB	116
	DB	119
	DB	121
	DB	32
	DB	83
	DB	84
	DB	79
	DB	80
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_31	DB	99
	DB	104
	DB	101
	DB	99
	DB	107
	DB	32
	DB	109
	DB	97
	DB	106
	DB	111
	DB	114
	DB	32
	DB	115
	DB	112
	DB	101
	DB	99
	DB	32
	DB	102
	DB	111
	DB	114
	DB	32
	DB	110
	DB	111
	DB	100
	DB	101
	DB	32
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_290	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_29	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	119
	DB	104
	DB	101
	DB	110
	DB	32
	DB	114
	DB	101
	DB	97
	DB	100
	DB	105
	DB	110
	DB	103
	DB	32
	DB	50
	DB	45
	DB	119
	DB	97
	DB	121
	DB	47
	DB	121
	DB	105
	DB	101
	DB	108
	DB	100
	DB	32
	DB	105
	DB	110
	DB	32
	DB	99
	DB	111
	DB	110
	DB	116
	DB	114
	DB	111
	DB	108
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_295	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_27	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	116
	DB	119
	DB	111
	DB	45
	DB	119
	DB	97
	DB	121
	DB	32
	DB	115
	DB	105
	DB	103
	DB	110
	DB	97
	DB	108
	DB	32
	DB	109
	DB	105
	DB	110
	DB	111
	DB	114
	DB	32
	DB	115
	DB	116
	DB	46
	DB	32
	DB	115
	DB	112
	DB	101
	DB	99
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_25	DB	32
	DB	99
	DB	104
	DB	101
	DB	99
	DB	107
	DB	32
	DB	110
	DB	111
	DB	100
	DB	101
	DB	32
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_299	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_23	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	116
	DB	119
	DB	111
	DB	45
	DB	119
	DB	97
	DB	121
	DB	32
	DB	83
	DB	84
	DB	79
	DB	80
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_21	DB	99
	DB	104
	DB	101
	DB	99
	DB	107
	DB	32
	DB	109
	DB	105
	DB	110
	DB	111
	DB	114
	DB	32
	DB	115
	DB	116
	DB	32
	DB	115
	DB	112
	DB	101
	DB	99
	DB	32
	DB	102
	DB	111
	DB	114
	DB	32
	DB	110
	DB	111
	DB	100
	DB	101
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_303	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_19	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	116
	DB	119
	DB	111
	DB	45
	DB	116
	DB	119
	DB	121
	DB	32
	DB	83
	DB	84
	DB	79
	DB	80
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_17	DB	99
	DB	104
	DB	101
	DB	99
	DB	107
	DB	32
	DB	109
	DB	105
	DB	110
	DB	111
	DB	114
	DB	32
	DB	115
	DB	112
	DB	101
	DB	99
	DB	32
	DB	102
	DB	111
	DB	114
	DB	32
	DB	110
	DB	111
	DB	100
	DB	101
	DB	32
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_307	DB	0
	DB 3 DUP ( 0H)	; pad
_2il0floatpacket.0	DD	042700000H
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_PENFORPREVENTMOVE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_MAXMOVE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_SIGNAPPRH:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_SIGNDATA:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_MASTERDEST:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_DESTINATION:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ALMOV:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_STRTSIG:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ISIGCOUNT:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_NSIGN:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_SIGNCOUNT:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_BACKPOINTR:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NOOF_MASTER_DESTINATIONS:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_KGPOINT:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NOOFNODES:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NOOFARCS:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ERROR:BYTE
_DATA	ENDS
EXTRN	_for_alloc_allocatable:PROC
EXTRN	_for_check_mult_overflow:PROC
EXTRN	_for_read_seq_fmt_xmit:PROC
EXTRN	_for_read_seq_fmt:PROC
EXTRN	_for_write_seq_fmt_xmit:PROC
EXTRN	_for_write_seq_fmt:PROC
EXTRN	_for_stop_core:PROC
EXTRN	_for_write_seq_lis_xmit:PROC
EXTRN	_for_write_seq_lis:PROC
EXTRN	_for_rewind:PROC
EXTRN	_for_close:PROC
EXTRN	_for_read_seq_lis_xmit:PROC
EXTRN	_for_read_seq_lis:PROC
EXTRN	_DYNUST_SIGNAL_MODULE_mp_DECLARESIGNAL:PROC
EXTRN	_DYNUST_SIGNAL_MODULE_mp_INSERTSIGNALPAR:PROC
EXTRN	_DYNUST_SIGNAL_MODULE_mp_DECLAREPHASE:PROC
EXTRN	_DYNUST_SIGNAL_MODULE_mp_INSERTSIGNALPHASE:PROC
EXTRN	_DYNUST_SIGNAL_MODULE_mp_DECLAREPHASEMOVE:PROC
EXTRN	_DYNUST_NETWORK_MODULE_mp_GETFLINKFROMNODE:PROC
EXTRN	_DYNUST_SIGNAL_MODULE_mp_DECLAREOBLINK:PROC
EXTRN	_DYNUST_SIGNAL_MODULE_mp_INSERTPHASEMOVE:PROC
EXTRN	_DYNUST_NETWORK_MODULE_mp_MOVENOFORLINK:PROC
EXTRN	_DYNUST_NETWORK_MODULE_mp_GETBSTMOVE:PROC
EXTRN	__intel_fast_memset:PROC
EXTRN	__alloca_probe:PROC
EXTRN	__fltused:BYTE
	INCLUDELIB <ifconsol>
	INCLUDELIB <libifcoremt>
	INCLUDELIB <libifport>
	INCLUDELIB <libmmt>
	INCLUDELIB <LIBCMT>
	INCLUDELIB <libirc>
	INCLUDELIB <svml_dispmt>
	INCLUDELIB <OLDNAMES>
	END
