; mark_description "Intel(R) Visual Fortran Compiler XE for applications running on IA-32, Version 13.1.1.171 Build 20130313";
; mark_description "-S";
	.686P
 	.387
	OPTION DOTNAME
	ASSUME	CS:FLAT,DS:FLAT,SS:FLAT
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
TXTST0:
; -- Begin  _SIGNAL_PRETIMED
; mark_begin;
IF @Version GE 800
  .MMX
ELSEIF @Version GE 612
  .MMX
  MMWORD TEXTEQU <QWORD>
ENDIF
IF @Version GE 800
  .XMM
ELSEIF @Version GE 614
  .XMM
  XMMWORD TEXTEQU <OWORD>
ENDIF
       ALIGN     16
	PUBLIC _SIGNAL_PRETIMED
_SIGNAL_PRETIMED	PROC NEAR 
; parameter 1: 40 + esp
; parameter 2: 44 + esp
.B1.1:                          ; Preds .B1.0
        push      esi                                           ;1.18
        push      edi                                           ;1.18
        push      ebx                                           ;1.18
        push      ebp                                           ;1.18
        sub       esp, 20                                       ;1.18
        movss     xmm3, DWORD PTR [_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT] ;31.18
        mulss     xmm3, DWORD PTR [_2il0floatpacket.2]          ;31.18
        movss     xmm0, DWORD PTR [_2il0floatpacket.3]          ;31.18
        andps     xmm0, xmm3                                    ;31.18
        pxor      xmm3, xmm0                                    ;31.18
        movss     xmm1, DWORD PTR [_2il0floatpacket.4]          ;31.18
        movaps    xmm2, xmm3                                    ;31.18
        movss     xmm4, DWORD PTR [_2il0floatpacket.5]          ;31.18
        cmpltss   xmm2, xmm1                                    ;31.18
        andps     xmm1, xmm2                                    ;31.18
        movaps    xmm2, xmm3                                    ;31.18
        movaps    xmm6, xmm4                                    ;31.18
        addss     xmm2, xmm1                                    ;31.18
        addss     xmm6, xmm4                                    ;31.18
        subss     xmm2, xmm1                                    ;31.18
        movaps    xmm7, xmm2                                    ;31.18
        mov       esi, DWORD PTR [40+esp]                       ;1.18
        subss     xmm7, xmm3                                    ;31.18
        movaps    xmm5, xmm7                                    ;31.18
        cmpless   xmm7, DWORD PTR [_2il0floatpacket.6]          ;31.18
        cmpnless  xmm5, xmm4                                    ;31.18
        andps     xmm5, xmm6                                    ;31.18
        andps     xmm7, xmm6                                    ;31.18
        mov       edx, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY+32] ;34.7
        subss     xmm2, xmm5                                    ;31.18
        neg       edx                                           ;34.7
        addss     xmm2, xmm7                                    ;31.18
        orps      xmm2, xmm0                                    ;31.18
        cvtss2si  eax, xmm2                                     ;31.18
        add       edx, DWORD PTR [esi]                          ;34.7
        mov       BYTE PTR [_DYNUST_SIGNAL_MODULE_mp_GREENEXT], al ;31.7
        mov       ecx, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY] ;34.7
        lea       eax, DWORD PTR [edx+edx*2]                    ;34.7
        shl       eax, 4                                        ;34.7
        movsx     edx, BYTE PTR [8+ecx+eax]                     ;34.25
        lea       ecx, DWORD PTR [12+esp]                       ;35.23
        mov       DWORD PTR [12+esp], edx                       ;34.7
        push      OFFSET FLAT: __NLITPACK_0.0.1                 ;35.23
        push      ecx                                           ;35.23
        push      esi                                           ;35.23
        call      _DYNUST_SIGNAL_MODULE_mp_GETSIGNALPAR         ;35.23
                                ; LOE esi
.B1.2:                          ; Preds .B1.1
        push      OFFSET FLAT: __NLITPACK_1.0.1                 ;36.23
        lea       eax, DWORD PTR [28+esp]                       ;36.23
        push      eax                                           ;36.23
        push      esi                                           ;36.23
        call      _DYNUST_SIGNAL_MODULE_mp_GETSIGNALPAR         ;36.23
                                ; LOE eax esi
.B1.19:                         ; Preds .B1.2
        mov       ebp, eax                                      ;36.23
                                ; LOE ebp esi
.B1.3:                          ; Preds .B1.19
        push      OFFSET FLAT: __NLITPACK_2.0.1                 ;37.23
        lea       eax, DWORD PTR [40+esp]                       ;37.23
        push      eax                                           ;37.23
        push      esi                                           ;37.23
        call      _DYNUST_SIGNAL_MODULE_mp_GETSIGNALPAR         ;37.23
                                ; LOE eax ebp esi
.B1.20:                         ; Preds .B1.3
        mov       ebx, eax                                      ;37.23
                                ; LOE ebx ebp esi
.B1.4:                          ; Preds .B1.20
        push      OFFSET FLAT: __NLITPACK_3.0.1                 ;38.23
        lea       eax, DWORD PTR [52+esp]                       ;38.23
        push      eax                                           ;38.23
        push      esi                                           ;38.23
        call      _DYNUST_SIGNAL_MODULE_mp_GETSIGNALPAR         ;38.23
                                ; LOE eax ebx ebp esi
.B1.21:                         ; Preds .B1.4
        mov       edi, eax                                      ;38.23
                                ; LOE ebx ebp esi edi
.B1.5:                          ; Preds .B1.21
        push      OFFSET FLAT: __NLITPACK_4.0.1                 ;39.23
        lea       eax, DWORD PTR [64+esp]                       ;39.23
        push      eax                                           ;39.23
        push      esi                                           ;39.23
        call      _DYNUST_SIGNAL_MODULE_mp_GETSIGNALPAR         ;39.23
                                ; LOE eax ebx ebp esi edi
.B1.22:                         ; Preds .B1.5
        add       esp, 60                                       ;39.23
                                ; LOE eax ebx ebp esi edi
.B1.6:                          ; Preds .B1.22
        mov       edx, DWORD PTR [44+esp]                       ;1.18
        cvtsi2ss  xmm1, edi                                     ;41.20
        movss     xmm0, DWORD PTR [edx]                         ;41.7
        mulss     xmm0, DWORD PTR [_2il0floatpacket.2]          ;41.11
        comiss    xmm0, xmm1                                    ;41.17
        jb        .B1.16        ; Prob 50%                      ;41.17
                                ; LOE eax ebx ebp esi
.B1.7:                          ; Preds .B1.6
        mov       DWORD PTR [8+esp], eax                        ;39.7
        movsx     eax, BYTE PTR [_DYNUST_SIGNAL_MODULE_mp_GREENEXT] ;43.16
        add       eax, ebp                                      ;43.15
        cmp       eax, ebx                                      ;43.25
        jl        .B1.14        ; Prob 22%                      ;43.25
                                ; LOE ebx ebp esi
.B1.8:                          ; Preds .B1.7
        mov       eax, ebx                                      ;47.9
        lea       edx, DWORD PTR [16+esp]                       ;48.20
        sub       eax, ebp                                      ;47.9
        lea       ecx, DWORD PTR [8+esp]                        ;48.20
        mov       BYTE PTR [16+esp], al                         ;47.9
        push      edx                                           ;48.20
        push      ecx                                           ;48.20
        lea       edi, DWORD PTR [20+esp]                       ;48.20
        push      edi                                           ;48.20
        push      esi                                           ;48.20
        call      _DYNUST_SIGNAL_MODULE_mp_ASSIGNGREEN          ;48.20
                                ; LOE ebx ebp esi
.B1.23:                         ; Preds .B1.8
        add       esp, 16                                       ;48.20
                                ; LOE ebx ebp esi
.B1.9:                          ; Preds .B1.23
        mov       ecx, DWORD PTR [esi]                          ;49.6
        imul      edx, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY+32], -48 ;49.6
        add       edx, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY] ;49.6
        lea       eax, DWORD PTR [ecx+ecx*2]                    ;49.6
        shl       eax, 4                                        ;49.6
        mov       DWORD PTR [esp], ebx                          ;
        mov       ebx, DWORD PTR [12+esp]                       ;49.6
        mov       edi, DWORD PTR [44+eax+edx]                   ;49.6
        neg       edi                                           ;49.6
        add       edi, ebx                                      ;49.6
        imul      edi, DWORD PTR [40+eax+edx]                   ;49.6
        mov       ecx, DWORD PTR [12+eax+edx]                   ;49.6
        xor       eax, eax                                      ;49.6
        mov       WORD PTR [8+ecx+edi], ax                      ;49.6
        mov       edi, DWORD PTR [esi]                          ;50.6
        lea       edi, DWORD PTR [edi+edi*2]                    ;50.6
        shl       edi, 4                                        ;50.6
        mov       ecx, DWORD PTR [44+edi+edx]                   ;50.6
        neg       ecx                                           ;50.6
        add       ecx, ebx                                      ;50.6
        imul      ecx, DWORD PTR [40+edi+edx]                   ;50.6
        mov       edi, DWORD PTR [12+edi+edx]                   ;50.6
        mov       BYTE PTR [10+edi+ecx], al                     ;50.6
        mov       ecx, DWORD PTR [esi]                          ;52.28
        lea       ecx, DWORD PTR [ecx+ecx*2]                    ;52.28
        shl       ecx, 4                                        ;52.28
        movsx     eax, BYTE PTR [1+ecx+edx]                     ;52.28
        cmp       ebx, eax                                      ;52.25
        mov       ebx, DWORD PTR [esp]                          ;52.25
        jne       .B1.11        ; Prob 50%                      ;52.25
                                ; LOE edx ecx ebx ebp esi bl bh
.B1.10:                         ; Preds .B1.9
        mov       DWORD PTR [esp], 1                            ;53.18
        jmp       .B1.12        ; Prob 100%                     ;53.18
                                ; LOE edx ecx ebx ebp esi bl bh
.B1.11:                         ; Preds .B1.9
        movzx     eax, BYTE PTR [8+ecx+edx]                     ;55.65
        inc       eax                                           ;55.110
        mov       DWORD PTR [esp], eax                          ;55.110
                                ; LOE edx ecx ebx ebp esi bl bh
.B1.12:                         ; Preds .B1.10 .B1.11
        movss     xmm1, DWORD PTR [_2il0floatpacket.2]          ;59.24
        mulss     xmm1, DWORD PTR [_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT] ;59.24
        movss     xmm0, DWORD PTR [_2il0floatpacket.3]          ;59.24
        andps     xmm0, xmm1                                    ;59.24
        pxor      xmm1, xmm0                                    ;59.24
        movss     xmm2, DWORD PTR [_2il0floatpacket.4]          ;59.24
        movaps    xmm3, xmm1                                    ;59.24
        movss     xmm4, DWORD PTR [_2il0floatpacket.5]          ;59.24
        cmpltss   xmm3, xmm2                                    ;59.24
        andps     xmm2, xmm3                                    ;59.24
        movaps    xmm3, xmm1                                    ;59.24
        movaps    xmm6, xmm4                                    ;59.24
        addss     xmm3, xmm2                                    ;59.24
        addss     xmm6, xmm4                                    ;59.24
        subss     xmm3, xmm2                                    ;59.24
        movaps    xmm7, xmm3                                    ;59.24
        mov       eax, DWORD PTR [esp]                          ;53.18
        subss     xmm7, xmm1                                    ;59.24
        movaps    xmm5, xmm7                                    ;59.24
        cmpless   xmm7, DWORD PTR [_2il0floatpacket.6]          ;59.24
        cmpnless  xmm5, xmm4                                    ;59.24
        andps     xmm5, xmm6                                    ;59.24
        andps     xmm7, xmm6                                    ;59.24
        mov       BYTE PTR [8+ecx+edx], al                      ;53.18
        subss     xmm3, xmm5                                    ;59.24
        movsx     eax, al                                       ;57.6
        addss     xmm3, xmm7                                    ;59.24
        sub       eax, DWORD PTR [44+ecx+edx]                   ;57.6
        orps      xmm3, xmm0                                    ;59.24
        imul      eax, DWORD PTR [40+ecx+edx]                   ;57.6
        mov       ecx, DWORD PTR [12+ecx+edx]                   ;57.6
        mov       BYTE PTR [10+ecx+eax], 1                      ;57.6
        mov       edi, DWORD PTR [esi]                          ;58.24
        movsx     ecx, BYTE PTR [_DYNUST_SIGNAL_MODULE_mp_GREENEXT] ;59.57
        add       ecx, ebp                                      ;59.56
        sub       ecx, ebx                                      ;59.56
        lea       eax, DWORD PTR [edi+edi*2]                    ;58.24
        cvtss2si  ebx, xmm3                                     ;59.65
        shl       eax, 4                                        ;58.24
        cmp       ecx, ebx                                      ;59.9
        mov       ebp, eax                                      ;60.15
        cmovl     ebx, ecx                                      ;59.9
        mov       DWORD PTR [4+esp], ebx                        ;59.9
        movsx     edi, BYTE PTR [8+eax+edx]                     ;58.24
        mov       DWORD PTR [12+esp], edi                       ;58.6
        sub       edi, DWORD PTR [44+ebp+edx]                   ;60.15
        imul      edi, DWORD PTR [40+ebp+edx]                   ;60.15
        mov       eax, DWORD PTR [12+ebp+edx]                   ;60.75
        mov       BYTE PTR [16+esp], bl                         ;59.9
        movsx     edx, BYTE PTR [4+esp]                         ;60.133
        push      OFFSET FLAT: __NLITPACK_4.0.1                 ;61.25
        add       WORD PTR [8+eax+edi], dx                      ;60.133
        lea       edx, DWORD PTR [16+esp]                       ;61.25
        push      edx                                           ;61.25
        push      esi                                           ;61.25
        call      _DYNUST_SIGNAL_MODULE_mp_GETSIGNALPAR         ;61.25
                                ; LOE eax esi
.B1.13:                         ; Preds .B1.12
        mov       DWORD PTR [20+esp], eax                       ;61.15
        lea       eax, DWORD PTR [28+esp]                       ;62.11
        push      eax                                           ;62.11
        lea       edx, DWORD PTR [24+esp]                       ;62.11
        push      edx                                           ;62.11
        lea       ecx, DWORD PTR [32+esp]                       ;62.11
        push      ecx                                           ;62.11
        push      esi                                           ;62.11
        call      _DYNUST_SIGNAL_MODULE_mp_ASSIGNGREEN          ;62.11
                                ; LOE
.B1.25:                         ; Preds .B1.13
        add       esp, 48                                       ;62.11
        pop       ebp                                           ;62.11
        pop       ebx                                           ;62.11
        pop       edi                                           ;62.11
        pop       esi                                           ;62.11
        ret                                                     ;62.11
                                ; LOE
.B1.14:                         ; Preds .B1.7
        lea       eax, DWORD PTR [8+esp]                        ;44.20
        push      OFFSET FLAT: _DYNUST_SIGNAL_MODULE_mp_GREENEXT ;44.20
        push      eax                                           ;44.20
        lea       edx, DWORD PTR [20+esp]                       ;44.20
        push      edx                                           ;44.20
        push      esi                                           ;44.20
        call      _DYNUST_SIGNAL_MODULE_mp_ASSIGNGREEN          ;44.20
                                ; LOE esi
.B1.26:                         ; Preds .B1.14
        add       esp, 16                                       ;44.20
                                ; LOE esi
.B1.15:                         ; Preds .B1.26
        mov       esi, DWORD PTR [esi]                          ;45.75
        imul      edx, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY+32], -48 ;45.15
        add       edx, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY] ;45.15
        lea       eax, DWORD PTR [esi+esi*2]                    ;45.75
        mov       ebp, DWORD PTR [12+esp]                       ;45.75
        movsx     ecx, BYTE PTR [_DYNUST_SIGNAL_MODULE_mp_GREENEXT] ;45.135
        shl       eax, 4                                        ;45.75
        sub       ebp, DWORD PTR [44+eax+edx]                   ;45.15
        imul      ebp, DWORD PTR [40+eax+edx]                   ;45.15
        mov       ebx, DWORD PTR [12+eax+edx]                   ;45.75
        add       WORD PTR [8+ebx+ebp], cx                      ;45.133
                                ; LOE
.B1.16:                         ; Preds .B1.6 .B1.15
        add       esp, 20                                       ;66.1
        pop       ebp                                           ;66.1
        pop       ebx                                           ;66.1
        pop       edi                                           ;66.1
        pop       esi                                           ;66.1
        ret                                                     ;66.1
        ALIGN     16
                                ; LOE
; mark_end;
_SIGNAL_PRETIMED ENDP
_TEXT	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
__NLITPACK_0.0.1	DD	1
__NLITPACK_1.0.1	DD	2
__NLITPACK_2.0.1	DD	3
__NLITPACK_3.0.1	DD	4
__NLITPACK_4.0.1	DD	5
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _SIGNAL_PRETIMED
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
_2il0floatpacket.2	DD	042700000H
_2il0floatpacket.3	DD	080000000H
_2il0floatpacket.4	DD	04b000000H
_2il0floatpacket.5	DD	03f000000H
_2il0floatpacket.6	DD	0bf000000H
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
EXTRN	_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT:BYTE
EXTRN	_DYNUST_SIGNAL_MODULE_mp_GREENEXT:BYTE
_DATA	ENDS
EXTRN	_DYNUST_SIGNAL_MODULE_mp_GETSIGNALPAR:PROC
EXTRN	_DYNUST_SIGNAL_MODULE_mp_ASSIGNGREEN:PROC
EXTRN	__fltused:BYTE
	INCLUDELIB <ifconsol>
	INCLUDELIB <libifcoremt>
	INCLUDELIB <libifport>
	INCLUDELIB <libmmt>
	INCLUDELIB <LIBCMT>
	INCLUDELIB <libirc>
	INCLUDELIB <svml_dispmt>
	INCLUDELIB <OLDNAMES>
	END
