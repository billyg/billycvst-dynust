; mark_description "Intel(R) Visual Fortran Compiler XE for applications running on IA-32, Version 13.1.1.171 Build 20130313";
; mark_description "-S";
	.686P
 	.387
	OPTION DOTNAME
	ASSUME	CS:FLAT,DS:FLAT,SS:FLAT
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
TXTST0:
; -- Begin  _SIGNAL_INTERSECTION_CONTROL
; mark_begin;
IF @Version GE 800
  .MMX
ELSEIF @Version GE 612
  .MMX
  MMWORD TEXTEQU <QWORD>
ENDIF
IF @Version GE 800
  .XMM
ELSEIF @Version GE 614
  .XMM
  XMMWORD TEXTEQU <OWORD>
ENDIF
       ALIGN     16
	PUBLIC _SIGNAL_INTERSECTION_CONTROL
_SIGNAL_INTERSECTION_CONTROL	PROC NEAR 
; parameter 1: 36 + esp
.B1.1:                          ; Preds .B1.0
        push      esi                                           ;1.18
        push      edi                                           ;1.18
        push      ebx                                           ;1.18
        push      ebp                                           ;1.18
        sub       esp, 16                                       ;1.18
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+36] ;29.7
        test      eax, eax                                      ;29.7
        mov       ebp, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+44] ;29.7
        mov       DWORD PTR [4+esp], eax                        ;29.7
        jle       .B1.3         ; Prob 10%                      ;29.7
                                ; LOE ebp
.B1.2:                          ; Preds .B1.1
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+24] ;29.7
        test      eax, eax                                      ;29.7
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+32] ;29.7
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE] ;29.7
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+40] ;29.7
        jg        .B1.14        ; Prob 50%                      ;29.7
                                ; LOE eax edx ecx ebx ebp
.B1.3:                          ; Preds .B1.1 .B1.2 .B1.32 .B1.20
        mov       ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFNODES] ;31.10
        mov       edx, 1                                        ;31.10
        test      ebx, ebx                                      ;31.10
        jle       .B1.13        ; Prob 2%                       ;31.10
                                ; LOE edx ebx
.B1.4:                          ; Preds .B1.3
        mov       eax, 1                                        ;
                                ; LOE eax edx ebx
.B1.5:                          ; Preds .B1.11 .B1.4
        mov       ecx, eax                                      ;32.43
        sub       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE+32] ;32.43
        imul      esi, ecx, 84                                  ;32.43
        mov       ebp, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE] ;32.7
        mov       edi, DWORD PTR [32+ebp+esi]                   ;32.10
        shl       edi, 2                                        ;32.43
        neg       edi                                           ;32.43
        add       edi, DWORD PTR [ebp+esi]                      ;32.43
        mov       ecx, DWORD PTR [8+edi]                        ;32.10
        cmp       ecx, 1                                        ;32.43
        je        .B1.28        ; Prob 5%                       ;32.43
                                ; LOE eax edx ecx ebx
.B1.6:                          ; Preds .B1.5
        cmp       ecx, 2                                        ;34.47
        je        .B1.26        ; Prob 5%                       ;34.47
                                ; LOE eax edx ecx ebx
.B1.7:                          ; Preds .B1.6
        cmp       ecx, 3                                        ;36.47
        je        .B1.24        ; Prob 1%                       ;36.47
                                ; LOE eax edx ecx ebx
.B1.8:                          ; Preds .B1.7
        cmp       ecx, 4                                        ;38.47
        je        .B1.22        ; Prob 5%                       ;38.47
                                ; LOE eax edx ecx ebx
.B1.9:                          ; Preds .B1.8
        cmp       ecx, 5                                        ;41.47
        je        .B1.35        ; Prob 5%                       ;41.47
                                ; LOE eax edx ecx ebx
.B1.10:                         ; Preds .B1.9
        cmp       ecx, 6                                        ;44.47
        je        .B1.30        ; Prob 1%                       ;44.47
                                ; LOE eax edx ebx
.B1.11:                         ; Preds .B1.29 .B1.27 .B1.25 .B1.23 .B1.36
                                ;       .B1.31 .B1.10
        inc       eax                                           ;31.10
        mov       edx, eax                                      ;31.10
        cmp       eax, ebx                                      ;31.10
        jle       .B1.5         ; Prob 82%                      ;31.10
                                ; LOE eax edx ebx
.B1.13:                         ; Preds .B1.11 .B1.3
        add       esp, 16                                       ;51.1
        pop       ebp                                           ;51.1
        pop       ebx                                           ;51.1
        pop       edi                                           ;51.1
        pop       esi                                           ;51.1
        ret                                                     ;51.1
                                ; LOE
.B1.14:                         ; Preds .B1.2
        mov       edi, eax                                      ;
        xor       esi, esi                                      ;
        shr       edi, 31                                       ;
        imul      ebp, ebx                                      ;
        add       edi, eax                                      ;
        sar       edi, 1                                        ;
        mov       DWORD PTR [esp], edi                          ;
        lea       edi, DWORD PTR [ecx*4]                        ;
        shl       ecx, 5                                        ;
        sub       ecx, edi                                      ;
        sub       edx, ecx                                      ;
        sub       ecx, ebp                                      ;
        add       edx, ecx                                      ;
        xor       ecx, ecx                                      ;
        add       edx, ebp                                      ;
        mov       DWORD PTR [12+esp], eax                       ;
        mov       ebp, edx                                      ;
        mov       DWORD PTR [8+esp], edx                        ;
        mov       eax, DWORD PTR [esp]                          ;
                                ; LOE eax ecx ebx ebp esi
.B1.15:                         ; Preds .B1.20 .B1.32 .B1.14
        test      eax, eax                                      ;29.7
        jbe       .B1.34        ; Prob 10%                      ;29.7
                                ; LOE eax ecx ebx ebp esi
.B1.16:                         ; Preds .B1.15
        xor       edi, edi                                      ;
        mov       edx, ebp                                      ;
        mov       DWORD PTR [esp], ebx                          ;
        xor       ebx, ebx                                      ;
                                ; LOE eax edx ecx ebx ebp esi edi
.B1.17:                         ; Preds .B1.17 .B1.16
        inc       edi                                           ;29.7
        mov       DWORD PTR [4+edx], ebx                        ;29.7
        mov       DWORD PTR [32+edx], ebx                       ;29.7
        add       edx, 56                                       ;29.7
        cmp       edi, eax                                      ;29.7
        jb        .B1.17        ; Prob 64%                      ;29.7
                                ; LOE eax edx ecx ebx ebp esi edi
.B1.18:                         ; Preds .B1.17
        mov       ebx, DWORD PTR [esp]                          ;
        lea       edx, DWORD PTR [1+edi+edi]                    ;29.7
                                ; LOE eax edx ecx ebx ebp esi
.B1.19:                         ; Preds .B1.18 .B1.34
        lea       edi, DWORD PTR [-1+edx]                       ;29.7
        cmp       edi, DWORD PTR [12+esp]                       ;29.7
        jae       .B1.32        ; Prob 10%                      ;29.7
                                ; LOE eax edx ecx ebx ebp esi
.B1.20:                         ; Preds .B1.19
        inc       esi                                           ;29.7
        lea       edi, DWORD PTR [edx*4]                        ;29.7
        shl       edx, 5                                        ;29.7
        add       ebp, ebx                                      ;29.7
        sub       edx, edi                                      ;29.7
        add       edx, DWORD PTR [8+esp]                        ;29.7
        mov       DWORD PTR [-24+edx+ecx], 0                    ;29.7
        add       ecx, ebx                                      ;29.7
        cmp       esi, DWORD PTR [4+esp]                        ;29.7
        jb        .B1.15        ; Prob 82%                      ;29.7
        jmp       .B1.3         ; Prob 100%                     ;29.7
                                ; LOE eax ecx ebx ebp esi
.B1.22:                         ; Preds .B1.8                   ; Infreq
        mov       eax, DWORD PTR [36+esp]                       ;39.19
        mov       DWORD PTR [esp], edx                          ;31.10
        lea       edx, DWORD PTR [esp]                          ;39.19
        push      eax                                           ;39.19
        push      edx                                           ;39.19
        call      _SIGNAL_PRETIMED                              ;39.19
                                ; LOE ebx
.B1.39:                         ; Preds .B1.22                  ; Infreq
        add       esp, 8                                        ;39.19
                                ; LOE ebx
.B1.23:                         ; Preds .B1.39                  ; Infreq
        mov       eax, DWORD PTR [esp]                          ;47.1
        jmp       .B1.11        ; Prob 100%                     ;47.1
                                ; LOE eax ebx
.B1.24:                         ; Preds .B1.7                   ; Infreq
        mov       DWORD PTR [esp], edx                          ;31.10
        lea       eax, DWORD PTR [esp]                          ;37.19
        push      eax                                           ;37.19
        call      _SIGNAL_STOP                                  ;37.19
                                ; LOE ebx
.B1.40:                         ; Preds .B1.24                  ; Infreq
        add       esp, 4                                        ;37.19
                                ; LOE ebx
.B1.25:                         ; Preds .B1.40                  ; Infreq
        mov       eax, DWORD PTR [esp]                          ;47.1
        jmp       .B1.11        ; Prob 100%                     ;47.1
                                ; LOE eax ebx
.B1.26:                         ; Preds .B1.6                   ; Infreq
        mov       DWORD PTR [esp], edx                          ;31.10
        lea       eax, DWORD PTR [esp]                          ;35.19
        push      eax                                           ;35.19
        call      _SIGNAL_YIELD                                 ;35.19
                                ; LOE ebx
.B1.41:                         ; Preds .B1.26                  ; Infreq
        add       esp, 4                                        ;35.19
                                ; LOE ebx
.B1.27:                         ; Preds .B1.41                  ; Infreq
        mov       eax, DWORD PTR [esp]                          ;47.1
        jmp       .B1.11        ; Prob 100%                     ;47.1
                                ; LOE eax ebx
.B1.28:                         ; Preds .B1.5                   ; Infreq
        mov       DWORD PTR [esp], edx                          ;31.10
        lea       eax, DWORD PTR [esp]                          ;33.19
        push      eax                                           ;33.19
        call      _SIGNAL_NO_CONTROL                            ;33.19
                                ; LOE ebx
.B1.42:                         ; Preds .B1.28                  ; Infreq
        add       esp, 4                                        ;33.19
                                ; LOE ebx
.B1.29:                         ; Preds .B1.42                  ; Infreq
        mov       eax, DWORD PTR [esp]                          ;47.1
        jmp       .B1.11        ; Prob 100%                     ;47.1
                                ; LOE eax ebx
.B1.30:                         ; Preds .B1.10                  ; Infreq
        mov       DWORD PTR [esp], edx                          ;31.10
        lea       eax, DWORD PTR [esp]                          ;45.19
        push      eax                                           ;45.19
        call      _SIGNAL_STOP                                  ;45.19
                                ; LOE ebx
.B1.43:                         ; Preds .B1.30                  ; Infreq
        add       esp, 4                                        ;45.19
                                ; LOE ebx
.B1.31:                         ; Preds .B1.43                  ; Infreq
        mov       eax, DWORD PTR [esp]                          ;47.1
        jmp       .B1.11        ; Prob 100%                     ;47.1
                                ; LOE eax ebx
.B1.32:                         ; Preds .B1.19                  ; Infreq
        inc       esi                                           ;29.7
        add       ebp, ebx                                      ;29.7
        add       ecx, ebx                                      ;29.7
        cmp       esi, DWORD PTR [4+esp]                        ;29.7
        jb        .B1.15        ; Prob 82%                      ;29.7
        jmp       .B1.3         ; Prob 100%                     ;29.7
                                ; LOE eax ecx ebx ebp esi
.B1.34:                         ; Preds .B1.15                  ; Infreq
        mov       edx, 1                                        ;
        jmp       .B1.19        ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx ebp esi
.B1.35:                         ; Preds .B1.9                   ; Infreq
        mov       eax, DWORD PTR [36+esp]                       ;42.19
        mov       DWORD PTR [esp], edx                          ;31.10
        lea       edx, DWORD PTR [esp]                          ;42.19
        push      eax                                           ;42.19
        push      edx                                           ;42.19
        call      _SIGNAL_ACTUATED                              ;42.19
                                ; LOE ebx
.B1.44:                         ; Preds .B1.35                  ; Infreq
        add       esp, 8                                        ;42.19
                                ; LOE ebx
.B1.36:                         ; Preds .B1.44                  ; Infreq
        mov       eax, DWORD PTR [esp]                          ;47.1
        jmp       .B1.11        ; Prob 100%                     ;47.1
        ALIGN     16
                                ; LOE eax ebx
; mark_end;
_SIGNAL_INTERSECTION_CONTROL ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _SIGNAL_INTERSECTION_CONTROL
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _QUASI_CONTROL
; mark_begin;
       ALIGN     16
	PUBLIC _QUASI_CONTROL
_QUASI_CONTROL	PROC NEAR 
; parameter 1: 120 + esp
.B2.1:                          ; Preds .B2.0
        push      esi                                           ;54.12
        push      edi                                           ;54.12
        push      ebx                                           ;54.12
        push      ebp                                           ;54.12
        sub       esp, 100                                      ;54.12
        mov       eax, DWORD PTR [120+esp]                      ;54.12
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_BACKPOINTR] ;81.7
        mov       ecx, DWORD PTR [eax]                          ;81.10
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_BACKPOINTR+32] ;81.7
        shl       eax, 2                                        ;81.7
        lea       edx, DWORD PTR [edx+ecx*4]                    ;81.7
        mov       ebx, edx                                      ;81.7
        sub       ebx, eax                                      ;81.7
        neg       eax                                           ;82.10
        mov       ebp, DWORD PTR [4+eax+edx]                    ;82.10
        dec       ebp                                           ;82.7
        mov       ebx, DWORD PTR [ebx]                          ;81.7
        cmp       ebp, ebx                                      ;85.2
        mov       DWORD PTR [84+esp], ebp                       ;82.7
        jl        .B2.8         ; Prob 50%                      ;85.2
                                ; LOE ebx ebp
.B2.2:                          ; Preds .B2.1
        mov       edx, ebp                                      ;85.2
        sub       edx, ebx                                      ;85.2
        pxor      xmm1, xmm1                                    ;83.7
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;86.10
        inc       edx                                           ;85.2
        mov       DWORD PTR [16+esp], eax                       ;86.10
        mov       eax, edx                                      ;85.2
        shr       eax, 31                                       ;85.2
        movaps    xmm0, xmm1                                    ;
        add       eax, edx                                      ;85.2
        sar       eax, 1                                        ;85.2
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;86.10
        test      eax, eax                                      ;85.2
        mov       ebp, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;87.10
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;87.10
        mov       DWORD PTR [20+esp], edx                       ;85.2
        jbe       .B2.38        ; Prob 10%                      ;85.2
                                ; LOE eax ecx ebx ebp esi xmm0 xmm1
.B2.3:                          ; Preds .B2.2
        imul      edi, ecx, -900                                ;
        xor       edx, edx                                      ;
        add       edi, ebp                                      ;
        mov       DWORD PTR [12+esp], edi                       ;
        mov       edi, ebx                                      ;
        sub       edi, esi                                      ;
        imul      edi, edi, 152                                 ;
        mov       DWORD PTR [4+esp], ebp                        ;
        mov       DWORD PTR [80+esp], ebx                       ;
        add       edi, DWORD PTR [16+esp]                       ;
        mov       ebx, edx                                      ;
        mov       ebp, DWORD PTR [12+esp]                       ;
        mov       DWORD PTR [8+esp], ecx                        ;
        mov       ecx, eax                                      ;
        mov       DWORD PTR [esp], esi                          ;
        ALIGN     16
                                ; LOE edx ecx ebx ebp edi xmm0 xmm1
.B2.4:                          ; Preds .B2.4 .B2.3
        imul      esi, DWORD PTR [16+ebx+edi], 900              ;87.10
        pxor      xmm2, xmm2                                    ;87.32
        pxor      xmm3, xmm3                                    ;87.32
        mov       eax, DWORD PTR [168+ebx+edi]                  ;86.10
        inc       edx                                           ;85.2
        cvtsi2ss  xmm2, DWORD PTR [704+ebp+esi]                 ;87.32
        imul      esi, eax, 900                                 ;87.10
        addss     xmm0, xmm2                                    ;87.10
        cvtsi2ss  xmm3, DWORD PTR [704+esi+ebp]                 ;87.32
        add       ebx, 304                                      ;85.2
        addss     xmm1, xmm3                                    ;87.10
        cmp       edx, ecx                                      ;85.2
        jb        .B2.4         ; Prob 63%                      ;85.2
                                ; LOE eax edx ecx ebx ebp edi xmm0 xmm1
.B2.5:                          ; Preds .B2.4
        mov       ecx, DWORD PTR [8+esp]                        ;
        lea       edi, DWORD PTR [1+edx+edx]                    ;85.2
        mov       ebp, DWORD PTR [4+esp]                        ;
        addss     xmm0, xmm1                                    ;85.2
        mov       esi, DWORD PTR [esp]                          ;
        mov       ebx, DWORD PTR [80+esp]                       ;
                                ; LOE eax ecx ebx ebp esi edi xmm0
.B2.6:                          ; Preds .B2.5 .B2.38
        lea       edx, DWORD PTR [-1+edi]                       ;85.2
        cmp       edx, DWORD PTR [20+esp]                       ;85.2
        jae       .B2.9         ; Prob 10%                      ;85.2
                                ; LOE eax ecx ebx ebp esi edi xmm0
.B2.7:                          ; Preds .B2.6
        neg       esi                                           ;86.10
        add       edi, ebx                                      ;86.10
        add       esi, edi                                      ;86.10
        neg       ecx                                           ;87.10
        imul      edx, esi, 152                                 ;86.10
        mov       eax, DWORD PTR [16+esp]                       ;86.10
        mov       eax, DWORD PTR [-136+eax+edx]                 ;86.10
        add       ecx, eax                                      ;87.10
        imul      ecx, ecx, 900                                 ;87.10
        cvtsi2ss  xmm1, DWORD PTR [704+ebp+ecx]                 ;87.32
        addss     xmm0, xmm1                                    ;87.10
        jmp       .B2.9         ; Prob 100%                     ;87.10
                                ; LOE eax ebx xmm0
.B2.8:                          ; Preds .B2.1
        xor       eax, eax                                      ;
        pxor      xmm0, xmm0                                    ;
                                ; LOE eax ebx xmm0
.B2.9:                          ; Preds .B2.7 .B2.6 .B2.8
        movss     xmm2, DWORD PTR [_2il0floatpacket.3]          ;90.21
        comiss    xmm0, xmm2                                    ;90.21
        jb        .B2.30        ; Prob 50%                      ;90.21
                                ; LOE eax ebx xmm0 xmm2
.B2.10:                         ; Preds .B2.9
        imul      eax, ebx, 152                                 ;
        cmp       ebx, DWORD PTR [84+esp]                       ;91.9
        jg        .B2.37        ; Prob 10%                      ;91.9
                                ; LOE eax ebx xmm0 xmm2
.B2.11:                         ; Preds .B2.10
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;92.10
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;92.10
        movss     DWORD PTR [92+esp], xmm0                      ;93.26
        mov       DWORD PTR [88+esp], eax                       ;93.26
        mov       DWORD PTR [80+esp], ebx                       ;93.26
                                ; LOE edx ecx
.B2.12:                         ; Preds .B2.28 .B2.11
        imul      ebx, edx, -152                                ;92.10
        add       ecx, ebx                                      ;92.10
        mov       eax, DWORD PTR [88+esp]                       ;92.10
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;93.26
        neg       edx                                           ;93.10
        mov       ebx, DWORD PTR [16+ecx+eax]                   ;92.10
        add       edx, ebx                                      ;93.10
        imul      ebp, edx, 900                                 ;93.10
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;93.26
        cvtsi2ss  xmm0, DWORD PTR [704+ecx+ebp]                 ;93.32
        mulss     xmm0, DWORD PTR [_2il0floatpacket.2]          ;93.26
        mulss     xmm0, DWORD PTR [_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT] ;93.31
        divss     xmm0, DWORD PTR [92+esp]                      ;93.10
        movss     DWORD PTR [96+esp], xmm0                      ;93.10
        push      OFFSET FLAT: __NLITPACK_0.0.2                 ;95.15
        call      _RANXY                                        ;95.15
                                ; LOE ebx f1
.B2.44:                         ; Preds .B2.12
        fstp      DWORD PTR [76+esp]                            ;95.15
        movss     xmm2, DWORD PTR [76+esp]                      ;95.15
        add       esp, 4                                        ;95.15
                                ; LOE ebx xmm2
.B2.13:                         ; Preds .B2.44
        movss     xmm0, DWORD PTR [96+esp]                      ;94.10
        mov       ebp, ebx                                      ;97.12
        cvttss2si eax, xmm0                                     ;94.10
        cvtsi2ss  xmm1, eax                                     ;96.23
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;97.12
        sub       ebp, edx                                      ;97.12
        imul      esi, ebp, 152                                 ;97.12
        subss     xmm0, xmm1                                    ;96.22
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;97.12
        comiss    xmm0, xmm2                                    ;96.16
        movsx     eax, BYTE PTR [32+ecx+esi]                    ;97.12
        jb        .B2.21        ; Prob 50%                      ;96.16
                                ; LOE eax edx ecx ebx xmm1
.B2.14:                         ; Preds .B2.13
        test      eax, eax                                      ;97.12
        addss     xmm1, DWORD PTR [_2il0floatpacket.3]          ;97.92
        cvttss2si ebp, xmm1                                     ;97.12
        mov       DWORD PTR [64+esp], ebp                       ;97.12
        jle       .B2.28        ; Prob 50%                      ;97.12
                                ; LOE eax edx ecx ebx
.B2.15:                         ; Preds .B2.14
        mov       ebp, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+44] ;97.12
        mov       DWORD PTR [36+esp], ebp                       ;97.12
        mov       ebp, eax                                      ;97.12
        shr       ebp, 31                                       ;97.12
        add       ebp, eax                                      ;97.12
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE] ;97.12
        sar       ebp, 1                                        ;97.12
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+32] ;97.12
        test      ebp, ebp                                      ;97.12
        mov       DWORD PTR [40+esp], esi                       ;97.12
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+40] ;97.12
        mov       DWORD PTR [32+esp], edi                       ;97.12
        mov       DWORD PTR [60+esp], ebp                       ;97.12
        jbe       .B2.39        ; Prob 10%                      ;97.12
                                ; LOE eax edx ecx ebx esi edi
.B2.16:                         ; Preds .B2.15
        mov       DWORD PTR [esp], eax                          ;
        xor       ebp, ebp                                      ;
        mov       eax, edi                                      ;
        mov       DWORD PTR [4+esp], ebx                        ;
        mov       DWORD PTR [8+esp], edx                        ;
        mov       DWORD PTR [12+esp], ecx                       ;
        lea       edi, DWORD PTR [eax*4]                        ;
        shl       eax, 5                                        ;
        sub       eax, edi                                      ;
        lea       edi, DWORD PTR [ebx*4]                        ;86.10
        shl       ebx, 5                                        ;86.10
        sub       ebx, edi                                      ;86.10
        mov       edi, DWORD PTR [36+esp]                       ;
        imul      edi, esi                                      ;
        add       ebx, DWORD PTR [40+esp]                       ;
        sub       ebx, edi                                      ;
        mov       edi, esi                                      ;
        sub       edi, eax                                      ;
        add       edi, ebx                                      ;
        mov       DWORD PTR [24+esp], edi                       ;
        lea       edi, DWORD PTR [esi+esi]                      ;
        sub       edi, eax                                      ;
        add       edi, ebx                                      ;
        mov       DWORD PTR [20+esp], edi                       ;
        xor       edi, edi                                      ;
        mov       eax, DWORD PTR [20+esp]                       ;
        mov       edx, DWORD PTR [24+esp]                       ;
        mov       ecx, DWORD PTR [60+esp]                       ;
        mov       ebx, DWORD PTR [64+esp]                       ;
                                ; LOE eax edx ecx ebx ebp esi edi
.B2.17:                         ; Preds .B2.17 .B2.16
        inc       ebp                                           ;97.12
        mov       DWORD PTR [4+edx+edi*2], ebx                  ;97.12
        mov       DWORD PTR [4+eax+edi*2], ebx                  ;97.12
        add       edi, esi                                      ;97.12
        cmp       ebp, ecx                                      ;97.12
        jb        .B2.17        ; Prob 64%                      ;97.12
                                ; LOE eax edx ecx ebx ebp esi edi
.B2.18:                         ; Preds .B2.17
        mov       eax, DWORD PTR [esp]                          ;
        lea       ebp, DWORD PTR [1+ebp+ebp]                    ;97.12
        mov       ebx, DWORD PTR [4+esp]                        ;
        mov       edx, DWORD PTR [8+esp]                        ;
        mov       ecx, DWORD PTR [12+esp]                       ;
                                ; LOE eax edx ecx ebx ebp esi
.B2.19:                         ; Preds .B2.18 .B2.39
        lea       edi, DWORD PTR [-1+ebp]                       ;97.12
        cmp       eax, edi                                      ;97.12
        jbe       .B2.28        ; Prob 10%                      ;97.12
                                ; LOE edx ecx ebx ebp esi
.B2.20:                         ; Preds .B2.19
        mov       edi, DWORD PTR [36+esp]                       ;97.12
        neg       edi                                           ;97.12
        sub       ebx, DWORD PTR [32+esp]                       ;97.12
        add       edi, ebp                                      ;97.12
        imul      edi, esi                                      ;97.12
        mov       ebp, DWORD PTR [40+esp]                       ;97.12
        lea       eax, DWORD PTR [ebx*4]                        ;97.12
        shl       ebx, 5                                        ;97.12
        sub       ebx, eax                                      ;97.12
        mov       esi, DWORD PTR [64+esp]                       ;97.12
        lea       eax, DWORD PTR [4+edi+ebp]                    ;97.12
        mov       DWORD PTR [eax+ebx], esi                      ;97.12
        jmp       .B2.28        ; Prob 100%                     ;97.12
                                ; LOE edx ecx
.B2.21:                         ; Preds .B2.13
        cvttss2si ebp, xmm1                                     ;99.12
        mov       DWORD PTR [68+esp], ebp                       ;99.12
        test      eax, eax                                      ;99.12
        jle       .B2.28        ; Prob 50%                      ;99.12
                                ; LOE eax edx ecx ebx
.B2.22:                         ; Preds .B2.21
        mov       ebp, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+44] ;99.12
        mov       DWORD PTR [48+esp], ebp                       ;99.12
        mov       ebp, eax                                      ;99.12
        shr       ebp, 31                                       ;99.12
        add       ebp, eax                                      ;99.12
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE] ;99.12
        sar       ebp, 1                                        ;99.12
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+32] ;99.12
        test      ebp, ebp                                      ;99.12
        mov       DWORD PTR [44+esp], esi                       ;99.12
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+40] ;99.12
        mov       DWORD PTR [52+esp], edi                       ;99.12
        mov       DWORD PTR [56+esp], ebp                       ;99.12
        jbe       .B2.40        ; Prob 10%                      ;99.12
                                ; LOE eax edx ecx ebx esi edi
.B2.23:                         ; Preds .B2.22
        mov       DWORD PTR [esp], eax                          ;
        xor       ebp, ebp                                      ;
        mov       eax, edi                                      ;
        mov       DWORD PTR [4+esp], ebx                        ;
        mov       DWORD PTR [8+esp], edx                        ;
        mov       DWORD PTR [12+esp], ecx                       ;
        lea       edi, DWORD PTR [eax*4]                        ;
        shl       eax, 5                                        ;
        sub       eax, edi                                      ;
        lea       edi, DWORD PTR [ebx*4]                        ;86.10
        shl       ebx, 5                                        ;86.10
        sub       ebx, edi                                      ;86.10
        mov       edi, DWORD PTR [48+esp]                       ;
        imul      edi, esi                                      ;
        add       ebx, DWORD PTR [44+esp]                       ;
        sub       ebx, edi                                      ;
        mov       edi, esi                                      ;
        sub       edi, eax                                      ;
        add       edi, ebx                                      ;
        mov       DWORD PTR [28+esp], edi                       ;
        lea       edi, DWORD PTR [esi+esi]                      ;
        sub       edi, eax                                      ;
        add       edi, ebx                                      ;
        mov       DWORD PTR [16+esp], edi                       ;
        xor       edi, edi                                      ;
        mov       eax, DWORD PTR [16+esp]                       ;
        mov       edx, DWORD PTR [28+esp]                       ;
        mov       ecx, DWORD PTR [56+esp]                       ;
        mov       ebx, DWORD PTR [68+esp]                       ;
                                ; LOE eax edx ecx ebx ebp esi edi
.B2.24:                         ; Preds .B2.24 .B2.23
        inc       ebp                                           ;99.12
        mov       DWORD PTR [4+edx+edi*2], ebx                  ;99.12
        mov       DWORD PTR [4+eax+edi*2], ebx                  ;99.12
        add       edi, esi                                      ;99.12
        cmp       ebp, ecx                                      ;99.12
        jb        .B2.24        ; Prob 64%                      ;99.12
                                ; LOE eax edx ecx ebx ebp esi edi
.B2.25:                         ; Preds .B2.24
        mov       eax, DWORD PTR [esp]                          ;
        lea       ebp, DWORD PTR [1+ebp+ebp]                    ;99.12
        mov       ebx, DWORD PTR [4+esp]                        ;
        mov       edx, DWORD PTR [8+esp]                        ;
        mov       ecx, DWORD PTR [12+esp]                       ;
                                ; LOE eax edx ecx ebx ebp esi
.B2.26:                         ; Preds .B2.25 .B2.40
        lea       edi, DWORD PTR [-1+ebp]                       ;99.12
        cmp       eax, edi                                      ;99.12
        jbe       .B2.28        ; Prob 10%                      ;99.12
                                ; LOE edx ecx ebx ebp esi
.B2.27:                         ; Preds .B2.26
        mov       edi, DWORD PTR [48+esp]                       ;99.12
        neg       edi                                           ;99.12
        sub       ebx, DWORD PTR [52+esp]                       ;99.12
        add       edi, ebp                                      ;99.12
        imul      edi, esi                                      ;99.12
        mov       ebp, DWORD PTR [44+esp]                       ;99.12
        lea       eax, DWORD PTR [ebx*4]                        ;99.12
        shl       ebx, 5                                        ;99.12
        sub       ebx, eax                                      ;99.12
        mov       esi, DWORD PTR [68+esp]                       ;99.12
        lea       eax, DWORD PTR [4+edi+ebp]                    ;99.12
        mov       DWORD PTR [eax+ebx], esi                      ;99.12
                                ; LOE edx ecx
.B2.28:                         ; Preds .B2.21 .B2.14 .B2.19 .B2.26 .B2.20
                                ;       .B2.27
        mov       ebx, DWORD PTR [80+esp]                       ;101.9
        inc       ebx                                           ;101.9
        mov       eax, DWORD PTR [88+esp]                       ;101.9
        add       eax, 152                                      ;101.9
        mov       DWORD PTR [88+esp], eax                       ;101.9
        mov       DWORD PTR [80+esp], ebx                       ;101.9
        cmp       ebx, DWORD PTR [84+esp]                       ;101.9
        jle       .B2.12        ; Prob 82%                      ;101.9
        jmp       .B2.37        ; Prob 100%                     ;101.9
                                ; LOE edx ecx
.B2.30:                         ; Preds .B2.9
        movss     xmm0, DWORD PTR [_2il0floatpacket.2]          ;103.100
        mulss     xmm0, DWORD PTR [_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT] ;103.100
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;103.9
        neg       edi                                           ;103.9
        cvttss2si ebx, xmm0                                     ;103.9
        add       edi, eax                                      ;103.9
        imul      ecx, edi, 152                                 ;103.9
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;103.9
        mov       DWORD PTR [28+esp], ebx                       ;103.9
        movsx     edi, BYTE PTR [32+edx+ecx]                    ;103.9
        test      edi, edi                                      ;103.9
        jle       .B2.37        ; Prob 10%                      ;103.9
                                ; LOE eax edi
.B2.31:                         ; Preds .B2.30
        mov       edx, edi                                      ;103.9
        shr       edx, 31                                       ;103.9
        add       edx, edi                                      ;103.9
        mov       ebp, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE] ;103.9
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+44] ;103.9
        sar       edx, 1                                        ;103.9
        mov       DWORD PTR [16+esp], ebp                       ;103.9
        test      edx, edx                                      ;103.9
        mov       DWORD PTR [12+esp], esi                       ;103.9
        mov       ebp, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+40] ;103.9
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+32] ;103.9
        mov       DWORD PTR [20+esp], edx                       ;103.9
        jbe       .B2.41        ; Prob 0%                       ;103.9
                                ; LOE eax ebp esi edi
.B2.32:                         ; Preds .B2.31
        mov       edx, eax                                      ;
        lea       ebx, DWORD PTR [eax*4]                        ;
        shl       edx, 5                                        ;
        xor       ecx, ecx                                      ;
        sub       edx, ebx                                      ;
        mov       ebx, DWORD PTR [12+esp]                       ;
        imul      ebx, ebp                                      ;
        add       edx, DWORD PTR [16+esp]                       ;
        mov       DWORD PTR [esp], esi                          ;
        sub       edx, ebx                                      ;
        lea       ebx, DWORD PTR [esi*4]                        ;
        shl       esi, 5                                        ;
        sub       esi, ebx                                      ;
        mov       ebx, ebp                                      ;
        sub       ebx, esi                                      ;
        add       ebx, edx                                      ;
        mov       DWORD PTR [24+esp], ebx                       ;
        lea       ebx, DWORD PTR [ebp+ebp]                      ;
        sub       ebx, esi                                      ;
        add       edx, ebx                                      ;
        mov       DWORD PTR [4+esp], edi                        ;
        mov       DWORD PTR [8+esp], eax                        ;
        mov       eax, DWORD PTR [24+esp]                       ;
        mov       ebx, ecx                                      ;
        mov       esi, DWORD PTR [20+esp]                       ;
        mov       edi, DWORD PTR [28+esp]                       ;
                                ; LOE eax edx ecx ebx ebp esi edi
.B2.33:                         ; Preds .B2.33 .B2.32
        inc       ebx                                           ;103.9
        mov       DWORD PTR [4+eax+ecx*2], edi                  ;103.9
        mov       DWORD PTR [4+edx+ecx*2], edi                  ;103.9
        add       ecx, ebp                                      ;103.9
        cmp       ebx, esi                                      ;103.9
        jb        .B2.33        ; Prob 63%                      ;103.9
                                ; LOE eax edx ecx ebx ebp esi edi
.B2.34:                         ; Preds .B2.33
        mov       edx, ebx                                      ;103.9
        mov       esi, DWORD PTR [esp]                          ;
        mov       edi, DWORD PTR [4+esp]                        ;
        mov       eax, DWORD PTR [8+esp]                        ;
        lea       edx, DWORD PTR [1+edx+edx]                    ;103.9
                                ; LOE eax edx ebp esi edi
.B2.35:                         ; Preds .B2.34 .B2.41
        lea       ecx, DWORD PTR [-1+edx]                       ;103.9
        cmp       edi, ecx                                      ;103.9
        jbe       .B2.37        ; Prob 0%                       ;103.9
                                ; LOE eax edx ebp esi
.B2.36:                         ; Preds .B2.35
        sub       eax, esi                                      ;103.9
        mov       ebx, DWORD PTR [12+esp]                       ;103.9
        neg       ebx                                           ;103.9
        add       ebx, edx                                      ;103.9
        imul      ebx, ebp                                      ;103.9
        lea       ecx, DWORD PTR [eax*4]                        ;103.9
        shl       eax, 5                                        ;103.9
        mov       edx, DWORD PTR [16+esp]                       ;103.9
        sub       eax, ecx                                      ;103.9
        mov       ebp, DWORD PTR [28+esp]                       ;103.9
        lea       esi, DWORD PTR [4+ebx+edx]                    ;103.9
        mov       DWORD PTR [esi+eax], ebp                      ;103.9
                                ; LOE
.B2.37:                         ; Preds .B2.28 .B2.10 .B2.35 .B2.30 .B2.36
                                ;      
        add       esp, 100                                      ;107.1
        pop       ebp                                           ;107.1
        pop       ebx                                           ;107.1
        pop       edi                                           ;107.1
        pop       esi                                           ;107.1
        ret                                                     ;107.1
                                ; LOE
.B2.38:                         ; Preds .B2.2                   ; Infreq
        xor       eax, eax                                      ;
        mov       edi, 1                                        ;
        jmp       .B2.6         ; Prob 100%                     ;
                                ; LOE eax ecx ebx ebp esi edi xmm0
.B2.39:                         ; Preds .B2.15                  ; Infreq
        mov       ebp, 1                                        ;
        jmp       .B2.19        ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx ebp esi
.B2.40:                         ; Preds .B2.22                  ; Infreq
        mov       ebp, 1                                        ;
        jmp       .B2.26        ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx ebp esi
.B2.41:                         ; Preds .B2.31                  ; Infreq
        mov       edx, 1                                        ;
        jmp       .B2.35        ; Prob 100%                     ;
        ALIGN     16
                                ; LOE eax edx ebp esi edi
; mark_end;
_QUASI_CONTROL ENDP
_TEXT	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
__NLITPACK_0.0.2	DD	14
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _QUASI_CONTROL
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
_2il0floatpacket.2	DD	042700000H
_2il0floatpacket.3	DD	03f800000H
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NOOFNODES:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_BACKPOINTR:BYTE
_DATA	ENDS
EXTRN	_RANXY:PROC
EXTRN	_SIGNAL_NO_CONTROL:PROC
EXTRN	_SIGNAL_YIELD:PROC
EXTRN	_SIGNAL_STOP:PROC
EXTRN	_SIGNAL_PRETIMED:PROC
EXTRN	_SIGNAL_ACTUATED:PROC
EXTRN	__fltused:BYTE
	INCLUDELIB <ifconsol>
	INCLUDELIB <libifcoremt>
	INCLUDELIB <libifport>
	INCLUDELIB <libmmt>
	INCLUDELIB <LIBCMT>
	INCLUDELIB <libirc>
	INCLUDELIB <svml_dispmt>
	INCLUDELIB <OLDNAMES>
	END
