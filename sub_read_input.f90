      SUBROUTINE READ_INPUT
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~ Copyright (C) 2002-2011 The Arizona Board of Regents on Behalf of The University of Arizona.  ~~!
!~~ Developed by Yi-Chang Chiu, Ph.D. as par to the DynusT project.                               ~~!
!~~ This program is free software: you can redistribute it and/or modify it                       ~~!
!~~ under the terms of the GNU General Public License version 2 as published by                   ~~!
!~~ the Free Software Foundation.                                                                 ~~!
!~~ This program is distributed in the hope that it will be useful,                               ~~!
!~~ but WITHOUT ANY WARRANTY; without even the implied warranty of                                ~~!
!~~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                                          ~~!
!~~ See the GNU General Public License for more details.                                          ~~!
!~~ You should have received a copy of the GNU General Public License                             ~~!
!~~ along with this program (in the file COPYING).                                                ~~!
!~~ IF not, see <http://www.gnu.org/licenses/>.                                                   ~~!
!~~ Please see http://www.dynust.net for contact information and names                            ~~!
!~~ of individual contributors.                                                                   ~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
      USE DYNUST_MAIN_MODULE
      USE DYNUST_VEH_MODULE
      USE DYNUST_NETWORK_MODULE
      USE DYNUST_VEH_PATH_ATT_MODULE
      USE DYNUST_AMS_MODULE
      USE DYNUST_CONGESTIONPRICING_MODULE
      USE DYNUST_FUEL_MODULE
      USE INCIDENT_MODULE
      USE MEMORY_MODULE
      USE DYNUST_TRANSIT_MODULE
      USE RAND_GEN_INT
      !USE DYNUST_MOVES_MODULE
      USE VARIABLE_MESSAGE_SIGN_MODULE      
      
      INTERFACE
         SUBROUTINE printscreen(printstr,intx,inty,dx1,dy1,dx2,dy2,refresh)
          INTEGER intx, inty, refresh,dx1,dy1,dx2,dy2
          CHARACTER (80) printstr
         END SUBROUTINE 
      END INTERFACE  
      
!      REAL(kind(1e0)), PARAMETER :: one=1.0e0, zero=0.0e0       
!      type(s_options) :: iopti(2)=s_options(0,zero) 

      INTEGER tolltmp(10000,4)
      INTEGER::iseeUvio = 0
      LOGICAL FoundFlag,EOFResult,foundFlag2
	  INTEGER OrigZone,vtpid
      INTEGER::i11 = 0
	  INTEGER::iseed(1) = 0
      LOGICAL::CState,iZoneFlag
      INTEGER vmaxtp, sattp, mfrtp
      INTEGER j
	  !INTEGER, ALLOCATABLE:: ConZoneTmp(:,:)
      REAL LWTmp
      REAL, ALLOCATABLE::demtmp(:)
      INTEGER :: FSize = 0
      CHARACTER *1 reply
      LOGICAL::Fexist=.False.
      LOGICAL::EXT1=.false.
      LOGICAL::Lenpnt=.False.
	  LOGICAL::Grdpnt=.False.    
      INTEGER FNodetmp, TNodetmp
	  INTEGER YLevel2N,YMove2N
      CHARACTER *20 ErString
      CHARACTER(80) printstr1, printstr2      
      INTEGER SeedSize
!     INTEGER, ALLOCATABLE :: SEED(:) ! THIS PROGRAM ASSUMES K = 1
      LOGICAL EXT, EXT2
      INTEGER,ALLOCATABLE::TempArray(:)

!> > > > > > > > > >                < < < < < < < < < < <
!>>>>>>>>>>>>>>>>>>>CODES START HERE<<<<<<<<<<<<<<<<<<<<<
!> > > > > > > > > >                < < < < < < < < < < <

     printstr1 = 'Reading Input Files'
     CALL printscreen(printstr1,20,395,20,20,500,500,1)

     INQUIRE(UNIT = 511, OPENED = Fexist)
     IF(.not. Fexist) THEN
         OPEN(file='Warning.dat',unit=511,status='unknown',iostat=error)
         IF(error /= 0) THEN
           WRITE(911,*) 'Error when opening Warning.dat'
           STOP
         ENDIF
     ENDIF

     IF(logout > 0) WRITE(711,*) 'read system.dat'  
     READ(95,*) SimPeriod
     READ(95,*) iteMax,RunMode
     IF(RunMode < 0.OR.RunMode > 4) THEN
       WRITE(911,*) "Incorrect Running Mode. check system.dat"
       WRITE(911,*) "only loading from OD or vehicle+path modes are allowed."
       WRITE(911,*) "RunMode takes value between 0 and 4."
       STOP
     ENDIF
     
     IF(NoofPS > 0) THEN ! if departure choice is to be modeled then expand the number of iterations
       iteMax = iteMax*NoofPS 
     ENDIF 
     READ(95,*) simPerAgg,simPerAsg,mtc_diff,no_via
     IF(iteration == 0) THEN
     INQUIRE (FILE='epoch.dat', EXIST = EXT2)
	 IF(EXT2) THEN
	   OPEN(unit = 6057, File ='epoch.dat',status='unknown')
	   READ(6057,*) EpocNum, ProjPeriod
       IF(ProjPeriod > 1.0.or.ProjPeriod < 0.0) THEN
         WRITE(911,*) "Error in epoch.dat"
         WRITE(911,*) "2nd number need to be between 0 and 1.0"
         STOP
       ENDIF	   
	   
	   ALLOCATE(EpocNull(EpocNum))
       EOFResult = EOF(6057)
	   IF(EOFResult) THEN
         WRITE(911,*) 'need the second line in the Epoch.dat'
	     STOP
	   ENDIF
	   READ(6057,*) EpocNullNm,(EpocNull(j),j=1,EpocNullNm)

	   ELSE

	   EpocNum = 1
	   EpocNullNm = 0
	   ProjPeriod = 0.95
	   
	   ALLOCATE(EpocNull(EpocNum))
	   ENDIF
    ENDIF

   
     INQUIRE(file="PARAMETER.dat", EXIST = Fexist)     
     IF(Fexist) THEN
         OPEN(file='PARAMETER.dat',unit=512,status='unknown',iostat=error)
         IF(error /= 0) THEN
           WRITE(911,*) 'Error when opening PARAMETER.dat'
           STOP
         ENDIF
         ALLOCATE(SIR(4),stat=error) !AMS
         sir(:) = 0
         DO i = 1, 4
           READ(512,*) sir(i)
         ENDDO
         READ(512,*,iostat=error) vhpcefactor
         READ(512,*,iostat=error) entrymx
         READ(512,*,iostat=error) VehJO
         READ(512,*,iostat=error) multf
         READ(512,*,iostat=error) i18
         READ(512,*,iostat=error) amsflag
         READ(512,*,iostat=error) asgflg
         READ(512,*,iostat=error) entrymeter
         READ(512,*,iostat=error) netcheck
         READ(512,*,iostat=error) vhpcereturn
         READ(512,*,iostat=error) JamSwitch
         READ(512,*,iostat=error) JamSWBar
         READ(512,*,iostat=error) JamPar
         READ(512,*,iostat=error) IterSub
         IF(Itersub < 1) Itersub = 1
         READ(512,*,iostat=error) genCheck
          IF(error /= 0) THEN
	         WRITE(911,*) 'Reading gencheck in PARAMETER.dat error'
	         WRITE(911,*) 'Please delete the PARAMETER.dat in the data folder'	         
	         WRITE(911,*) 'Re-run through GUI'
	         WRITE(911,*) 'IF directly run from dynust.exe, copy PARAMETER.dat from the program folder'
	         STOP
	      ENDIF

         READ(512,*,iostat=error) keepcls
         IF(RunMode == 1) keepcls = 0 ! otherwise it won't engage assignment even with od iteration
          IF(error /= 0) THEN
	         WRITE(911,*) 'Reading keepcls in PARAMETER.dat error'
	         WRITE(911,*) 'Please delete the PARAMETER.dat in the data folder'	         
	         WRITE(911,*) 'Re-run through GUI'
	         WRITE(911,*) 'IF directly run from dynust.exe, copy PARAMETER.dat from the program folder'
	         STOP
	      ENDIF

         READ(512,*,iostat=error) keeptype
         IF(RunMode == 1) keeptype = 0 ! otherwise it won't engage assignment even with od iteration
          IF(error /= 0) THEN
	         WRITE(911,*) 'Reading keeptype in PARAMETER.dat error'
	         WRITE(911,*) 'Please delete the PARAMETER.dat in the data folder'	         
	         WRITE(911,*) 'Re-run through GUI'
	         WRITE(911,*) 'IF directly run from dynust.exe, copy PARAMETER.dat from the program folder'
	         STOP
	      ENDIF

         READ(512,*,iostat=error) tmparysize
          IF(error /= 0) THEN
	         WRITE(911,*) 'Reading temparysize in PARAMETER.dat error'
	         WRITE(911,*) 'Please delete the PARAMETER.dat in the data folder'	         
	         WRITE(911,*) 'Re-run through GUI'
	         WRITE(911,*) 'IF directly run from dynust.exe, copy PARAMETER.dat from the program folder'
	         STOP
	      ENDIF

         READ(512,*,iostat=error) nv_vebuffer
          IF(error /= 0) THEN
	         WRITE(911,*) 'Reading NoofVehbuffer in PARAMETER.dat error'
	         WRITE(911,*) 'Please delete the PARAMETER.dat in the data folder'	         
	         WRITE(911,*) 'Re-run through GUI'
	         WRITE(911,*) 'IF directly run from dynust.exe, copy PARAMETER.dat from the program folder'
	         STOP
	      ENDIF
         
         READ(512,*,iostat=error) logout
          IF(error /= 0) THEN
	         WRITE(911,*) 'Reading logout PARAMETER.dat error'
	         WRITE(911,*) 'Please delete the PARAMETER.dat in the data folder'	         
	         WRITE(911,*) 'Re-run through GUI'
	         WRITE(911,*) 'IF directly run from dynust.exe, copy PARAMETER.dat from the program folder'
	         STOP
	      ENDIF

         READ(512,*,iostat=error) trajalt
          IF(error /= 0) THEN
	         WRITE(911,*) 'Reading logout PARAMETER.dat error'
	         WRITE(911,*) 'Please delete the PARAMETER.dat in the data folder'	         
	         WRITE(911,*) 'Re-run through GUI'
	         WRITE(911,*) 'IF directly run from dynust.exe, copy PARAMETER.dat from the program folder'
	         STOP
	      ENDIF

         READ(512,*,iostat=error) IncClsFlag
          IF(error /= 0) THEN
	         WRITE(911,*) 'Reading IncClsFlag PARAMETER.dat error'
	         WRITE(911,*) 'Please delete the PARAMETER.dat in the data folder'	         
	         WRITE(911,*) 'Re-run through GUI'
	         WRITE(911,*) 'IF directly run from dynust.exe, copy PARAMETER.dat from the program folder'
	         STOP
	      ENDIF

         READ(512,*,iostat=error) ibaytmp
          Baylength = float(ibaytmp) ! in feet
          IF(error /= 0) THEN
	         WRITE(911,*) 'Reading Baylength in PARAMETER.dat error'
	         WRITE(911,*) 'Please delete the PARAMETER.dat in the data folder'	         
	         WRITE(911,*) 'Re-run through GUI'
	         WRITE(911,*) 'IF directly run from dynust.exe, copy PARAMETER.dat from the program folder'
	         STOP
	      ENDIF

         READ(512,*,iostat=error) LTSat
          IF(error /= 0) THEN
	         WRITE(911,*) 'Reading LTSat in PARAMETER.dat error'
	         WRITE(911,*) 'Please delete the PARAMETER.dat in the data folder'	         
	         WRITE(911,*) 'Re-run through GUI'
	         WRITE(911,*) 'IF directly run from dynust.exe, copy PARAMETER.dat from the program folder'
	         STOP
	      ENDIF

         READ(512,*,iostat=error) RTSat
          IF(error /= 0) THEN
	         WRITE(911,*) 'Reading RTSat in PARAMETER.dat error'
	         WRITE(911,*) 'Please delete the PARAMETER.dat in the data folder'	         
	         WRITE(911,*) 'Re-run through GUI'
	         WRITE(911,*) 'IF directly run from dynust.exe, copy PARAMETER.dat from the program folder'
	         STOP
	      ENDIF

         READ(512,*,iostat=error) t_cr
          IF(error /= 0) THEN
	         WRITE(911,*) 'Reading t_cr in PARAMETER.dat error'
	         WRITE(911,*) 'Please delete the PARAMETER.dat in the data folder'	         
	         WRITE(911,*) 'Re-run through GUI'
	         WRITE(911,*) 'IF directly run from dynust.exe, copy PARAMETER.dat from the program folder'
	         STOP
	      ENDIF

         READ(512,*,iostat=error) scale
          IF(error /= 0) THEN
	         WRITE(911,*) 'Reading scale in PARAMETER.dat error'
	         WRITE(911,*) 'Please delete the PARAMETER.dat in the data folder'	         
	         WRITE(911,*) 'Re-run through GUI'
	         WRITE(911,*) 'IF directly run from dynust.exe, copy PARAMETER.dat from the program folder'
	         STOP
	      ENDIF

         READ(512,*,iostat=error) factorcap
          IF(error /= 0) THEN
	         WRITE(911,*) 'Reading factorcap in PARAMETER.dat error'
	         WRITE(911,*) 'Please delete the PARAMETER.dat in the data folder'	         
	         WRITE(911,*) 'Re-run through GUI'
	         WRITE(911,*) 'IF directly run from dynust.exe, copy PARAMETER.dat from the program folder'
	         STOP
	      ENDIF

          READ(512,*,iostat=error) AssMtd
          IF(error /= 0) THEN
	         WRITE(911,*) 'Reading AssMtd in PARAMETER.dat error'
	         WRITE(911,*) 'Please delete the PARAMETER.dat in the data folder'	         
	         WRITE(911,*) 'Re-run through GUI'
	         WRITE(911,*) 'IF directly run from dynust.exe, copy PARAMETER.dat from the program folder'
	         STOP
	      ENDIF

          READ(512,*,iostat=error) UETimeFlagW
          IF(error /= 0) THEN
	         WRITE(911,*) 'Reading UETimeFlagR in PARAMETER.dat error'
	         WRITE(911,*) 'Please delete the PARAMETER.dat in the data folder'	         
	         WRITE(911,*) 'Re-run through GUI'
	         WRITE(911,*) 'IF directly run from dynust.exe, copy PARAMETER.dat from the program folder'
	         STOP
	      ENDIF

          READ(512,*,iostat=error) UETimeFlagR
          IF(error /= 0) THEN
	         WRITE(911,*) 'Reading UETimeFlagW in PARAMETER.dat error'
	         WRITE(911,*) 'Please delete the PARAMETER.dat in the data folder'	         
	         WRITE(911,*) 'Re-run through GUI'
	         WRITE(911,*) 'IF directly run from dynust.exe, copy PARAMETER.dat from the program folder'
	         STOP
	      ENDIF

          READ(512,*,iostat=error) shelter_use
          IF(error /= 0) THEN
	         WRITE(911,*) 'Reading shelter_use in PARAMETER.dat error'
	         WRITE(911,*) 'Please delete the PARAMETER.dat in the data folder'	         
	         WRITE(911,*) 'Re-run through GUI'
	         WRITE(911,*) 'IF directly run from dynust.exe, copy PARAMETER.dat from the program folder'
	         STOP
	      ENDIF

          READ(512,*,iostat=error) nThread
          IF(error /= 0) THEN
	         WRITE(911,*) 'Reading number of threads in PARAMETER.dat error'
	         WRITE(911,*) 'Please delete the PARAMETER.dat in the data folder'	         
	         WRITE(911,*) 'Re-run through GUI'
	         WRITE(911,*) 'IF directly run from dynust.exe, copy PARAMETER.dat from the program folder'
	         STOP
	      ENDIF

          READ(512,*,iostat=error) FuelOut
          IF(error /= 0) THEN
	         WRITE(911,*) 'Reading fuel consumption output switch in PARAMETER.dat error'
	         WRITE(911,*) 'Please delete the PARAMETER.dat in the data folder'	         
	         WRITE(911,*) 'Re-run through GUI'
	         WRITE(911,*) 'IF directly run from dynust.exe, copy PARAMETER.dat from the program folder'
	         STOP
	      ENDIF

          READ(512,*,iostat=error) DemFormat
          IF(error /= 0) THEN
	         WRITE(911,*) 'Reading demand format in PARAMETER.dat error'
	         WRITE(911,*) 'Please delete the PARAMETER.dat in the data folder'	         
	         WRITE(911,*) 'Re-run through GUI'
	         WRITE(911,*) 'IF directly run from dynust.exe, copy PARAMETER.dat from the program folder'
	         STOP
	      ENDIF
          Inputcode = DemFormat
          
          READ(512,*,iostat=error) ReadTransit
          IF(error /= 0) THEN
	         WRITE(911,*) 'Reading ReadTransit flag in PARAMETER.dat error'
	         WRITE(911,*) 'Please delete the PARAMETER.dat in the data folder'	         
	         WRITE(911,*) 'Re-run through GUI'
	         WRITE(911,*) 'IF directly run from dynust.exe, copy PARAMETER.dat from the program folder'
	         STOP
	      ENDIF

          READ(512,*,iostat=error) GenTransit
          IF(error /= 0) THEN
	         WRITE(911,*) 'Reading GenTransit flag in PARAMETER.dat error'
	         WRITE(911,*) 'Please delete the PARAMETER.dat in the data folder'	         
	         WRITE(911,*) 'Re-run through GUI'
	         WRITE(911,*) 'IF directly run from dynust.exe, copy PARAMETER.dat from the program folder'
	         STOP
	      ENDIF

          READ(512,*,iostat=error) Tflswitch
          IF(error /= 0) THEN
	         WRITE(911,*) 'Reading tflswitch in PARAMETER.dat error'
	         WRITE(911,*) 'Please delete the PARAMETER.dat in the data folder'	         
	         WRITE(911,*) 'Re-run through GUI'
	         WRITE(911,*) 'IF directly run from dynust.exe, copy PARAMETER.dat from the program folder'
	         STOP
	      ENDIF

          READ(512,*,iostat=error) ReadHistArr
          IF(error /= 0) THEN
	         WRITE(911,*) 'Reading tflswitch in PARAMETER.dat error'
	         WRITE(911,*) 'Please delete the PARAMETER.dat in the data folder'	         
	         WRITE(911,*) 'Re-run through GUI'
	         WRITE(911,*) 'IF directly run from dynust.exe, copy PARAMETER.dat from the program folder'
	         STOP
	      ENDIF

          READ(512,*,iostat=error) DllFlagRM
          IF(error /= 0) THEN
	         WRITE(911,*) 'Reading DllFlagRM in PARAMETER.dat error'
	         WRITE(911,*) 'Please delete the PARAMETER.dat in the data folder'	         
	         WRITE(911,*) 'Re-run through GUI'
	         WRITE(911,*) 'IF directly run from dynust.exe, copy PARAMETER.dat from the program folder'
	         STOP
	      ENDIF

          READ(512,*,iostat=error) DelTolMean
          IF(error /= 0) THEN
	         WRITE(911,*) 'Reading DelToleMean in PARAMETER.dat error'
	         WRITE(911,*) 'Please delete the PARAMETER.dat in the data folder'	         
	         WRITE(911,*) 'Re-run through GUI'
	         WRITE(911,*) 'IF directly run from dynust.exe, copy PARAMETER.dat from the program folder'
	         STOP
	      ENDIF

          READ(512,*,iostat=error) DelTolStd
          IF(error /= 0) THEN
	         WRITE(911,*) 'Reading DelToleStd in PARAMETER.dat error'
	         WRITE(911,*) 'Please delete the PARAMETER.dat in the data folder'	         
	         WRITE(911,*) 'Re-run through GUI'
	         WRITE(911,*) 'IF directly run from dynust.exe, copy PARAMETER.dat from the program folder'
	         STOP
	      ENDIF

          READ(512,*,iostat=error) stop_count_threshold
          IF(error /= 0) THEN
	         WRITE(911,*) 'Reading stop_count_threshold in PARAMETER.dat error'
	         WRITE(911,*) 'Please delete the PARAMETER.dat in the data folder'	         
	         WRITE(911,*) 'Re-run through GUI'
	         WRITE(911,*) 'IF directly run from dynust.exe, copy PARAMETER.dat from the program folder'
	         STOP
	      ENDIF

          READ(512,*,iostat=error) maxnu_pa
          IF(error /= 0) THEN
	         WRITE(911,*) 'Reading size for temporary path arry in PARAMETER.dat error'
	         WRITE(911,*) 'Please delete the PARAMETER.dat in the data folder'	         
	         WRITE(911,*) 'Re-run through GUI'
	         WRITE(911,*) 'IF directly run from dynust.exe, copy PARAMETER.dat from the program folder'
	         STOP
	      ENDIF

          READ(512,*,iostat=error) KnowTemp
          IF(error /= 0) THEN
	         WRITE(911,*) 'Reading ParkNRide Capacity aware flag in PARAMETER.dat error'
	         WRITE(911,*) 'Please delete the PARAMETER.dat in the data folder'	         
	         WRITE(911,*) 'Re-run through GUI'
	         WRITE(911,*) 'IF directly run from dynust.exe, copy PARAMETER.dat from the program folder'
	         STOP
	      ENDIF
	      IF(KnowTemp > 0) THEN
	         CapKnow = .true.
	      ELSE
	         CapKnow = .false.
	      ENDIF

          READ(512,*,iostat=error) InfoStartTime
          IF(error /= 0) THEN
	         WRITE(911,*) 'Reading Information Start Time in PARAMETER.dat error'
	         WRITE(911,*) 'Please delete the PARAMETER.dat in the data folder'	         
	         WRITE(911,*) 'Re-run through GUI'
	         WRITE(911,*) 'IF directly run from dynust.exe, copy PARAMETER.dat from the program folder'
	         STOP
	      ENDIF 

          READ(512,*,iostat=error) TransitEnrouteSwitchFlag
          IF(error /= 0) THEN
	         WRITE(911,*) 'Reading Transit Enroute Switch Flag in PARAMETER.dat error'
	         WRITE(911,*) 'Please delete the PARAMETER.dat in the data folder'	         
	         WRITE(911,*) 'Re-run through GUI'
	         WRITE(911,*) 'IF directly run from dynust.exe, copy PARAMETER.dat from the program folder'
	         STOP
	      ENDIF

          READ(512,*,iostat=error) PeakSpreadFlag
          IF(error /= 0) THEN
	         WRITE(911,*) 'Reading Peak Spreak Switch Flag in PARAMETER.dat error'
	         WRITE(911,*) 'Please delete the PARAMETER.dat in the data folder'	         
	         WRITE(911,*) 'Re-run through GUI'
	         WRITE(911,*) 'IF directly run from dynust.exe, copy PARAMETER.dat from the program folder'
	         STOP
	      ENDIF

          READ(512,*,iostat=error) NoofPS
          IF(error /= 0) THEN
	         WRITE(911,*) 'Reading # of departure time choice iterations in PARAMETER.dat error'
	         WRITE(911,*) 'Please delete the PARAMETER.dat in the data folder'	         
	         WRITE(911,*) 'Re-run through GUI'
	         WRITE(911,*) 'IF directly run from dynust.exe, copy PARAMETER.dat from the program folder'
	         STOP
	      ENDIF
	      
          READ(512,*,iostat=error) SkimFlag
          IF(error /= 0) THEN
	         WRITE(911,*) 'Reading skim output switch in PARAMETER.dat error'
	         WRITE(911,*) 'Please delete the PARAMETER.dat in the data folder'	         
	         WRITE(911,*) 'Re-run through GUI'
	         WRITE(911,*) 'IF directly run from dynust.exe, copy PARAMETER.dat from the program folder'
	         STOP
	      ENDIF
	      
          READ(512,*,iostat=error) SkimOutInt
          IF(error /= 0) THEN
	         WRITE(911,*) 'Reading skim output interval in PARAMETER.dat error'
	         WRITE(911,*) 'Please delete the PARAMETER.dat in the data folder'	         
	         WRITE(911,*) 'Re-run through GUI'
	         WRITE(911,*) 'IF directly run from dynust.exe, copy PARAMETER.dat from the program folder'
	         STOP
	      ENDIF

          READ(512,*,iostat=error) MixedVehGenMap
          IF(error /= 0) THEN
	         WRITE(911,*) 'Reading Mixed Vehicle generation mapping in PARAMETER.dat error'
	         WRITE(911,*) 'Please delete the PARAMETER.dat in the data folder'	         
	         WRITE(911,*) 'Re-run through GUI'
	         WRITE(911,*) 'IF directly run from dynust.exe, copy PARAMETER.dat from the program folder'
	         STOP
	      ENDIF
!         In the following case, vehicles in vehicle'dat have different external and internal id  
!         This file provides the mapping     
          IF((RunMode == 3.OR.RunMode == 4).AND.MixedVehGenMap > 0) THEN
            OPEN(file='output_vehicle_map.dat',unit=9987,status='unknown',iostat=error)
	        IF(error /= 0) THEN
              WRITE(911,*) 'Error when opening output_vehicle_map.dat'
	          STOP
	        ENDIF      
          ELSE
            INQUIRE (FILE='output_vehicle_map.dat', EXIST = EXT2)
	        IF(EXT2) THEN 
	            ice = system('del .\output_vehicle_map.dat')     
	        ENDIF
          ENDIF

          READ(512,*,iostat=error) MOVESFlag
          IF(error /= 0) THEN
	         WRITE(911,*) 'Reading MOVES output switch in PARAMETER.dat error'
	         WRITE(911,*) 'Please delete the PARAMETER.dat in the data folder'	         
	         WRITE(911,*) 'Re-run through GUI'
	         WRITE(911,*) 'IF directly run from dynust.exe, copy PARAMETER.dat from the program folder'
	         STOP
	      ENDIF
	      
     

     ELSE
        sir(:) = 800
        vhpcefactor = 1.0
        entrymx = 80000
        VehJO = 1
        multf = 1.0
        i18 = 1
        amsflag = 0    
        asgflg = 0
        entrymeter = 0.99
        netcheck = 0 
        vhpcereturn = 1.0
        JamSwitch = 0
        JamSWBar = 1.0
        JamPar = 0.0
        itersub = 1
        genCheck = 1
        keepcls = 0
        keeptype = 0
        tmparysize = 100000
        nv_vebuffer = 1.05
        logout = 0
        trajalt = 0
        IncClsFlag = 0
        Baylength = 200.0/5280.0 ! default =  200 ft for bay length
        LTBay = 1500.0
        RTBay = 1600.0
        t_cr = 5.0
        scale = 5.0
        factorcap = 0.2
        AssMtd = 0
        UETimeFlagR = 0
        UETimeFlagW = 0        
        shelter_use = 0
        nThread = 4 
        FuelOut = 0
        DemFormat = 0
        Inputcode = DemFormat
        ReadTransit = 0
        GenTransit = 0
        ReadHistArr = 0
        DllFlagRM = 0
        DelTolMean = 10.0
        DelTolStd = 0.0
        stop_count_threshold = 100000 ! a large number
        maxnu_pa = 1000
        CapKnow = .false.
        InfoStartTime = 0.0
        TransitEnrouteSwitchFlag = 0.0
        PeakSpreadFlag = 0
        NoofPS = 0
        SkimFlag = 0
        SkimOutInt = 0
        MixedVehGenMap = 0
        MOVESFlag = 0
     ENDIF
     
     IF(iteration == 0) THEN
     IF(logout > 0) THEN
     OPEN(file='runLog.dat',unit=711,status='unknown',iostat=error)
      IF(error /= 0) THEN
         WRITE(911,*) 'Error when opening runlog.dat'
	   STOP
	  ENDIF
	 ENDIF
     ENDIF

     IF(mod(iteMax,itersub) /= 0) THEN
        WRITE(911,*) "number of iterations should be multiple of Itersub"
        WRITE(911,*) "please check both PARAMETER.dat and system.dat"
        STOP 
     ENDIF
! --
     READ(41,*,iostat=error) nzones,noofnodes,noofarcs,kay,SuperZoneSwitch

     IF(.NOT.ALLOCATED(ConZoneTmp)) ALLOCATE(ConZoneTmp(nzones,1000)) ! Chiu 2007 
     ConZoneTmp = 0



     IF(RunMode == 1.OR.RunMode == 3.OR.RunMode == 4) THEN ! &&&      

     READ(42,*,iostat=error) nints,multi
     multi = multi * multf !chiu2007777
   
     INQUIRE (FILE='demand_truck.dat', EXIST = EXT2)
     IF(EXT2) THEN
	   READ(54,*,iostat=error) nintsT, multiT
       multiT = multiT*multf ! Chiu 200777777
     ENDIF

     INQUIRE (FILE='demand_hov.dat', EXIST = EXT2)
     IF(EXT2) THEN
	   READ(544,*,iostat=error) nintsH, multiH
       multiH = multiH*multf ! Chiu 200777777
     ENDIF

     INQUIRE (FILE='bg_demand_adjust.dat', EXIST = EXT2)    
     IF(EXT2) THEN
       OPEN(file='bg_demand_adjust.dat',unit=7890,status='old',iostat=error)
       READ(7890,*) bg_num
       
       IF(superzoneswitch > 0) THEN            ! use super zone feature
         loadszdem = .true. ! this is the flag to tell is super zone is to be loaded
         ALLOCATE(bg_factor(nints),stat=error)
         bg_factor(:)=0       
         ALLOCATE(DemCellS(nzones,nzones),stat=error)
         DemCellS(:,:)=0
         READ(545,*,iostat=error) nintsS, multiS
         READ(545,*)
         multiS = multiS*multf ! Chiu 2007
         
         IF(bg_num == 0) THEN                   ! regular super zone feature
            bg_factor(:) = 1.0                  ! use 100% background demand - demand.dat
         ELSE                                   ! evac application
           DO i = 1, bg_num
              READ(7890,*) bg_factor(i)
           ENDDO
	       IF(nintsS /= nints) THEN
	          WRITE(911,*) "number of superzone demand tables should be same as demand.dat"
	          STOP
	       ENDIF         
         ENDIF !IF(bg_num == 0) THEN   
       ENDIF !IF(superzoneswitch > 0) THEN
       ELSE
         WRITE(911,*) "Missing bg_demand_adjust.dat. Please save the dataset from GUI"
         STOP
       ENDIF


     ENDIF ! RunMode == 1.OR.RunMode == 3.OR.RunMode == 4
     
     
     READ(44,*,iostat=error) isig

     READ(45,*,iostat=error) dec_num, nrate
     
     READ(46,*,iostat=error) inci_num

     READ(49,*,iostat=error) vms_num

     READ(56,*,iostat=error) NLevel, NMove

     READ(57,*,iostat=error) Level2N,Move2N

     READ(58,*,iostat=error) WorkZoneNum

     READ(59,*,iostat=error) GradeNum,LenNum,TruckNum

     READ(60,*,iostat=error) YLevel2N,YMove2N

    IF(RunMode /= 1.and.iteration == 0) THEN
     READ(500,*,iostat=error) MaxVehiclesFile, noofSTOPs
     totalveh = MaxVehiclesFile
     READ(500,*,iostat=error) !skip a line
     LoadTripChain = 0
    ENDIF
	
     IF((RunMode == 1.and.multi == 0..and.multiT == 0..and.multiH == 0.and.NoofBuses == 0).or.(RunMode /= 1.and.MaxVehiclesFile < 1.and.NoofBuses < 1)) THEN
	 WRITE(911,*) 'INPUT ERROR : '
	 WRITE(911,*) 'Total number of vehicles to be loaded is zero'
	 WRITE(911,*) 'Please check the following files depending on'
	 WRITE(911,*) 'the demand generation mode'
	 WRITE(911,*) 'demand.dat'
	 WRITE(911,*) 'vehicle.dat'
	 WRITE(911,*) 'bus.dat'
     ENDIF

	nu_switch = 1000



    IF(SuperZoneSwitch == 0) THEN
	  noof_master_destinations = nzones
	ELSE
      INQUIRE (FILE='superzone.dat',EXIST = EXT1)
      IF(EXT1)THEN
	   OPEN(file='SuperZone.dat',unit=913,status='old')
       READ(913,*,iostat=error) noof_master_destinations
       IF(error /= 0) THEN
         WRITE(911,*) 'Error when opening superzone.dat or superzone '
         STOP
       ENDIF
      ELSE
       WRITE(911,*) 'superzone.dat is requested but non-existent '
       STOP
      ENDIF
    ENDIF
    noofarcs_org=noofarcs
    noofnodes_org=noofnodes

    CALL MEMALLOCATE_SIM
  
 
      IF(logout > 0) WRITE(711,*) 'read scenario.dat'
      READ(43,*,iostat=error) ribfa,bound, istrm, ipinit, InfoPM
      
      iopti(1) = s_options(s_rand_gen_generator_seed,zero) 
      iopti(2) = s_options(istrm,zero) 

      CALL RAND_GEN(ranx,iopt=iopti)
      
!      CALL RANDOM_SEED(SIZE=SeedSize) ! RETURN THE NUMBER OF SEEDS USED AND SET IT TO K
!	  ALLOCATE(SEED(SeedSize))
!	  IF(ISTRM > 0) THEN
!	    SEED(:) = istrm
!	    CALL RANDOM_SEED(PUT=SEED(1:SeedSize)) ! FORCE TO USE THE SEED SET IN SEED ARRAY
!	  ELSE
!	    CALL RANDOM_SEED(SIZE=SeedSize)
!	  ENDIF
!	  CALL RANDOM_NUMBER(ranx) ! PRINT THE RANDOM NUMBER
	  
      IF(ipinit < 0.or.ipinit > 1) THEN
        WRITE(911,*) 'INPUT ERROR : scenario data file'
        WRITE(911,*) 'ipinit is out of the possible range'
        WRITE(911,*) 'the value should be either 0 or 1'
        STOP
      ENDIF

      READ(43,*,iostat=error) com_frac

      IF(com_frac < 0.or.com_frac > 1) THEN
        WRITE(911,*) 'INPUT ERROR : scenario data file'
        WRITE(911,*) 'com_frac is out of the possible range'
        WRITE(911,*) 'the value should be between 0 and 1'
        STOP
      ENDIF

      READ(43,*,iostat=error) ixminPerSimInt
      xminPerSimInt = ixminPerSimInt/60.0               ! convert simulation interval from second to minute. xminPerSimInt is used throughout the program

      stagest = 0.0
      starttime = nint(stagest/xminPerSimInt)+1
      endtime = nint((stagest+SimPeriod)/xminPerSimInt)

! AFTER READING XMINPERSIMINT, CHECK ITS RELATION WITH SIMOUTINT      
	  IF(SkimFlag > 0) THEN
	    IF(MOD(nint(SkimOutInt/xminPerSimInt),simPerAgg) /= 0) THEN
	      WRITE(911,*) 'The skim output interval needs to be multiple of aggregation interval'
	      WRITE(911,*) 'Please revise.'
	      WRITE(911,'("skim output interval = ", f6.1, " no of sim interval per aggregation interval = ",i6)') SkimOutInt,simPerAgg
	      STOP
	    ENDIF	      
	    IF(MOD(nint(SimPeriod),nint(SkimOutInt)) /= 0) THEN ! the simulation period needs to be multiple of the skim output interval
	      WRITE(911,*) 'The simulation period needs to be multiple of the skim output interval'
	      WRITE(911,*) 'Please revise.'
	      STOP
	    ENDIF
	  ENDIF      
	  IF(MOD((NINT(SimPeriod/EpocNum)),NINT(skimOutInt)) /= 0.AND.skimFlag == 1) THEN
	     WRITE(911,*) "Epoch lenght (min) needs to be multiple of skim output interval"
	     WRITE(911,*) "Please revise."
	     STOP
	  ENDIF
	  
4319  FORMAT(2I5)

      READ(43,*,iostat=error) tdspstep,tupstep
4343  FORMAT(2i5)

      IF(tupstep >= tdspstep) THEN
        WRITE(911,*) 'INPUT ERROR : scenario data file'
        WRITE(911,*) 'tupstep is greater or equal to tdspstep'
        WRITE(911,*) 'tupstep should be < tdspstep'
        STOP
      ENDIF

      READ(43,*,iostat=error) starttm, endtm
      
	IF(starttm >= SimPeriod) THEN
	WRITE(911,*) 'Simulation period is shorter than start time'
	WRITE(911,*) 'For collecting statistics'
	WRITE(911,*) 'Please correct'
	STOP
	ENDIF

4342  FORMAT(2f7.3)

      IF(starttm > SimPeriod) THEN
        WRITE(911,*) 'INPUT ERROR : scenario data file'
        WRITE(911,*) 'Warmup time is >= planning horizon'
        STOP
      ENDIF

      IF(starttm >= endtm) THEN
        WRITE(911,*) 'INPUT ERROR : scenario data file'
        WRITE(911,*) 'Warmup time is >= end of stats collection time'
        STOP
      ENDIF

      fracinf1 = 0.0
	  total_hov = 0.0
	  READ(43,*) vtpid ! number of vehicle types listed 
	  IF(vtpid > 3) THEN
	    WRITE(911,*) "Error in scenario.dat"
	    WRITE(911,*) "Only three vehicle types are allowed"
	  ENDIF
	  ALLOCATE(VehProfile(3)) ! total 3 types
	    VehProfile(:)%typeid = 0
	    vehprofile(:)%odorfile = 0
	    vehprofile(:)%vehfrac = 0
	    vehprofile(:)%mtcmode = 0
	  DO is = 1, vtpid
	   vehprofile(is)%mtcfrac(:) = 0
	  ENDDO
      

	  DO 399 is = 1, vtpid

	    READ(43,'(2i5,f6.3,i3,5f6.3)') VehProfile(is)%typeid,VehProfile(is)%odorfile,vehprofile(is)%vehfrac,vehprofile(is)%mtcmode,(vehprofile(is)%mtcfrac(ik),ik=1,5)

	    IF(is == 1) THEN
          checksum = sum(vehprofile(is)%mtcfrac(:))
          IF(abs(checksum-1.0)>0.0001)THEN
            WRITE(911,*) "Error in scenario.dat"
            WRITE(911,*) "Sum of 5 classes not equal to 1.0"
            STOP
          ENDIF
          IF(vehprofile(is)%mtcfrac(2) > 0.0001.and.keepcls == 0) iso_ok = 1 !keepcls = 0 is to load with new demadn profile
          IF(vehprofile(is)%mtcfrac(3) > 0.0001.and.keepcls == 0) iue_ok = 1
          IF(vehprofile(is)%mtcfrac(4) > 0.0001.and.keepcls == 0) ier_ok = 1
	    ELSEIF(is == 2) THEN
	      IF(VehProfile(is)%odorfile == 1.or.VehProfile(is)%vehfrac > 0.001) truckok=.true.
          IF(VehProfile(is)%mtcmode == 1) THEN
            IF(vehprofile(is)%mtcfrac(2) > 0.0001.and.keepcls == 0) iso_ok = 1
            IF(vehprofile(is)%mtcfrac(3) > 0.0001.and.keepcls == 0) iue_ok = 1
            IF(vehprofile(is)%mtcfrac(4) > 0.0001.and.keepcls == 0) ier_ok = 1
          ENDIF          
        ELSEIF(is == 3) THEN
          IF(VehProfile(is)%odorfile == 1.or.VehProfile(is)%vehfrac > 0.001) hovok=.true.
          IF(VehProfile(is)%mtcmode == 1) THEN
            IF(vehprofile(is)%mtcfrac(2) > 0.0001.and.keepcls == 0) iso_ok = 1
            IF(vehprofile(is)%mtcfrac(3) > 0.0001.and.keepcls == 0) iue_ok = 1
            IF(vehprofile(is)%mtcfrac(4) > 0.0001.and.keepcls == 0) ier_ok = 1
          ENDIF          
        ENDIF

        IF(iteMax == 0) THEN ! one-shot
          IF((RunMode == 2.OR.RunMode == 4).and.(iso_ok == 1.or.iue_ok == 1)) THEN
            WRITE(911,*) "UE/SO vehicles are not allowed in the one-shot, veh+path loading mode"
            WRITE(911,*) "Only Historical(class 1), Enroute(class 4) and Pretrip(class 5) are allowed"
            STOP
          ELSEIF(RunMode == 1.and.(iso_ok == 1.or.iue_ok == 1.or.vehprofile(1)%mtcfrac(1) > 0.001)) THEN
            WRITE(911,*) "Histoical or UE/SO vehicles are not allowed in the one-shot" 
            WRITE(911,*) "OD Table loading mode"
            WRITE(911,*) "Enroute(class 4) and Pretrip(class 5) are allowed"
            STOP
          ENDIF        
        ELSEIF(iteMax > 0) THEN
          IF(RunMode == 1.and.vehprofile(1)%mtcfrac(1) > 0.001) THEN
            WRITE(911,*) "Histoical vehicles are not allowed in the mtc" 
            WRITE(911,*) "OD Table loading mode"
            WRITE(911,*) "UE(class 2), SO(class 3, Enroute(class 4) and Pretrip(class 5) are allowed"
            STOP
          ENDIF 
        ENDIF

        IF(vehprofile(is)%mtcmode == 0) THEN ! apply the sov mtc profile
           vehprofile(is)%mtcfrac(:) = vehprofile(1)%mtcfrac(:)
        ENDIF

		fracinf1 = fracinf1+vehprofile(is)%vehfrac*vehprofile(is)%mtcfrac(4)
		  
399     ENDDO
        IF(iteMax > 0.and.iue_ok == 0.and.iso_ok == 0.and.keepcls == 0) THEN
          WRITE(911,*) "No UE or SO specified but DUE/DSO is specified"
          WRITE(911,*) "This could happen IF you:"
          WRITE(911,*) "1. Load with vehicle file, but none of the vehicles are specified as either SO(2) or UE(3)"
          WRITE(911,*) "2. Load with demand but doing UE/SO without specifying respective vehilces in the scenario=>scenario tab"
          STOP
        ENDIF
        DO is2 = 1, vtpid
          DO iff = 2,5 ! calculate cumulative prob.
           vehprofile(is2)%mtcfrac(iff)=vehprofile(is2)%mtcfrac(iff-1)+vehprofile(is2)%mtcfrac(iff)
          ENDDO
        ENDDO
        vehprofile(:)%mtcfrac(5) = 1.0
         
        IF(vtpid == 1) Vehprofile(2)%mtcfrac(:) = vehprofile(1)%mtcfrac(:)

        Vehprofile(3)%mtcfrac(:) = vehprofile(1)%mtcfrac(:)                  
      
        tempfrac = 0
        DO js = 1, vtpid
          IF(vtpid == 1) THEN
            tempfrac = tempfrac + vehprofile(js)%vehfrac          
          ELSEIF(vtpid /= 1.and.VehProfile(js)%odorfile == 0) THEN
            tempfrac = tempfrac + vehprofile(js)%vehfrac
          ENDIF
        ENDDO
        IF(abs(tempfrac-1.0) > 0.001) THEN
          WRITE(911,*) "Error in scenario.dat"
          WRITE(911,*) "vehicles types generated from demand.dat should have respective percentage summed up to 1.0"
          STOP
        ENDIF


	   fracnoinf1 = 1.0 - fracinf1
       IF(vtpid == 3) total_hov = vehprofile(3)%vehfrac

       IF(iso_ok > 0) THEN
	    WRITE(911,*) 'System Optimal Assignment Capability is currently under testing'
		STOP
	   ENDIF

       IF(abs((fracinf1+fracnoinf1)-1.0) > 0.0005) THEN
        WRITE(911,*) 'INPUT ERROR : scenario data file'
        WRITE(911,*) 'sum of percentage of vehicle types' 
        WRITE(911,*) 'is not equal to 1.00'
        STOP
       ENDIF
     aggint = ifix((SimPeriod/xminPerSimInt)/simPerAgg)+1


14    FORMAT(15i5)
! --
! -- Read the master destination (zone centroid) for each zone
	IF(SuperZoneSwitch == 1) THEN
      IF(logout > 0) WRITE(711,*) 'read superzone.dat'
      READ(913,*,iostat=error)
	  READ(913,14) (OrigZone,i=1,nzones) ! this reading just for skipping
      READ(913,*,iostat=error)
	  READ(913,14,iostat=error) (MasterDest(i),i=1,nzones)
	  DO mo = 1, nzones
	    IF(MasterDest(mo) > nzones) THEN
	       WRITE(911,*) "Error in superzone.dat"
	       WRITE(911,*) "The zone number should not exceed max number of zones which is ", nzones
	       STOP
	    ENDIF
	  ENDDO
	  close(913)
	ELSE
      DO i = 1, nzones
	   MasterDest(i) = i
      ENDDO
	ENDIF

 	destination(:)=0

	noofnodes=noofnodes+noof_master_destinations
	IF(logout > 0) WRITE(711,*) 'allocate_dynust_network_node'
	CALL allocate_dynust_network_node()
	IF(iteration == 0) CALL allocate_dynust_network_node_nde()
    IF(logout > 0) WRITE(711,*) 'read network.dat'
    DO i = 1,noofnodes_org
       READ(41,*,iostat=error) m_dynust_network_node_nde(i)%IntoOutNodeNum, m_dynust_network_node_nde(i)%izone !%%%
	   IF(m_dynust_network_node_nde(i)%izone < 1) THEN
	     WRITE(511,*) "network.dat no zone assigned to node", m_dynust_network_node_nde(i)%IntoOutNodeNum
	   ENDIF
	   OutToInNodeNum(m_dynust_network_node_nde(i)%IntoOutNodeNum) = i
	   IF(m_dynust_network_node_nde(i)%IntoOutNodeNum < 0.or.OutToInNodeNum(m_dynust_network_node_nde(i)%IntoOutNodeNum) < 0) THEN
         WRITE(911,*) 'error in reading node numbers'
	     WRITE(911,*)'i, IntoOutNodeNum and OutToInNodeNum =',i,m_dynust_network_node_nde(i)%IntoOutNodeNum,OutToInNodeNum(m_dynust_network_node_nde(i)%IntoOutNodeNum)
	     STOP
	   ENDIF
	   IF(error /= 0) THEN
         WRITE(911,*) 'error in reading nodes in network.dat', OutToInNodeNum(i)
	     STOP
       ENDIF
       READ(7099,*) nodett, m_dynust_network_node(OutToInNodeNum(nodett))%xcor, m_dynust_network_node(OutToInNodeNum(nodett))%ycor
    ENDDO
13  FORMAT(2i5)

	DO i=1,noof_master_destinations
      destination(i) = noofnodes_org + i
	  m_dynust_network_node_nde(destination(i))%IntoOutNodeNum= 900000 + i ! give centroids external numbers starting from 9000
	  OutToInNodeNum(m_dynust_network_node_nde(destination(i))%IntoOutNodeNum)=noofnodes_org+i !G
      m_dynust_network_node_nde(destination(i))%izone=i
	ENDDO


!   Read xy.dat for AStar TDSP    
    OPEN (File = "xy.dat", unit = 1999, status = "unknown")
    IF(.NOT.ALLOCATED(X)) ALLOCATE(X(noofnodes))
    IF(.NOT.ALLOCATED(Y)) ALLOCATE(Y(noofnodes))
    X = 0
    Y = 0
    DO WHILE (.NOT.EOF(1999))
        READ(1999, *) id, xtemp, ytemp
        IF(OuttoInNodeNum(id) < 1) THEN
            WRITE(911,*) "ERROR IN ASTART INI"
            STOP
        ENDIF
        X(OuttoInNodeNum(id)) = xtemp
        Y(OuttoInNodeNum(id)) = ytemp
    ENDDO
    CLOSE(1999)
    
      isit = 0
      IF(logout > 0) WRITE(711,*) 'read destination.dat'
      DO ia = 1, nzones
	    READ(53,*,iostat=error) mzonetmp, NoofConsPerZone(ia),(ConZoneTmp(ia,MP),MP=1,NoofConsPerZone(ia))	  
	    IF(mzonetmp /= ia) THEN
	      WRITE(911,*) "error in destination zone # ", ia
	      STOP
        ENDIF
        
	    noofarcs = noofarcs + NoofConsPerZone(ia)

!       ASSIGN DESTINATION ZONE FOR NODE	    
	    FORALL( k = 1 : NoofConsPerZone(ia) )
	         m_dynust_network_node(OutToInNodeNum(ConZoneTmp(ia,k)))%dzone = ia
	    END FORALL
	    IF(NoofConsPerZone(ia) < 1) THEN
          WRITE(911,*) 'Error in reading destination.dat'
	      WRITE(911,*) 'Each zone needs to have at least one dest'
	      WRITE(911,*) 'Please check zone', ia
	      isit = 1
        ENDIF

        ! approximate centroid coordinates
        totalx = 0.0
        totaly = 0.0
        DO M = 1, NoofConsPerZone(ia)
            totalx = totalx + X(OutToInNodeNum(ConZoneTmp(ia,M)))
            totaly = totaly + Y(OutToInNodeNum(ConZoneTmp(ia,M)))
        ENDDO
        X(destination(ia)) = totalx/NoofConsPerZone(ia)
        Y(destination(ia)) = totaly/NoofConsPerZone(ia)
      ENDDO    
     
      IF(isit == 1) STOP
      close(53)


    
    
	IF(logout > 0) WRITE(711,*) 'read toll.dat'
	IF(TollExist) THEN
       READ(62,*) TollVoTA, TollVoTH, TollVoTT
    ELSE
	   TollVoTA = 0.0
	   TollVoTH = 0.0
	   TollVoTT = 0.0
	ENDIF

     	
	IF(logout > 0) WRITE(711,*) 'CALL allocate network arc'
	CALL allocate_dynust_network_arc
    

	IF(logout > 0) WRITE(711,*) 'truckpce toll.dat'
      READ(59,59591,iostat=error) (TruckBPnt(i),i=1,TruckNum)
      DO i = 1, GradeNum
        READ(59,59591,iostat=error) GradeBPnt(i)
	    DO j = 1, LenNum
	      READ(59,5959,iostat=error) LengthBPnt(i,j),(PCE(i,j,k),k=1,TruckNum)
        ENDDO
	  ENDDO
59591 FORMAT(10i4)
5959  FORMAT(f12.5,10f5.1)


      Longest_link=0

	IF(logout > 0) WRITE(711,*) 'read network.dat'
	DO 221 i=1,noofarcs_org

      READ(41,*,iostat=error)iu,id,MTbay,MTbayR,i3,m_dynust_network_arc_de(i)%nlanes,m_dynust_network_arc_de(i)%FlowModelNum,m_dynust_network_arc_de(i)%Vfadjust,m_dynust_network_arc_de(i)%SpeedLimit,mfrtp,sattp,m_dynust_network_arc_nde(i)%link_iden, m_dynust_network_arc_de(i)%LGrade
      IF(i == 1) THEN ! COMPUTE DISTANCE SCALE - MILE
         distscale = (i3/5280.0)/SQRT((m_dynust_network_node(OutToInNodeNum(iu))%xcor-m_dynust_network_node(OutToInNodeNum(id))%xcor)**2+(m_dynust_network_node(OutToInNodeNum(iu))%ycor-m_dynust_network_node(OutToInNodeNum(id))%ycor)**2)
      ENDIF
      IF(m_dynust_network_arc_de(i)%FlowModelNum > NoOfFlowModel) THEN
        WRITE(911,*) "MISMATCH OF LINK ATTRIBUTE AND TRAFFIC FLOW MODEL"
        WRITE(911,*) "LINK ", IU," TO ",ID, "IS FOUND TO HAVE FLOW MODEL # ",m_dynust_network_arc_de(i)%FlowModelNum
        WRITE(911,*) "BUT NO SUFFICIENT # OF MODELS TO SUPPORT IT"
        STOP
      ENDIF
      IF(amsflag == 1.and.(m_dynust_network_arc_nde(i)%link_iden == 1.or.m_dynust_network_arc_nde(i)%link_iden == 2.or.m_dynust_network_arc_nde(i)%link_iden == 8.or.m_dynust_network_arc_nde(i)%link_iden == 9.or.m_dynust_network_arc_nde(i)%link_iden == 10)) m_dynust_network_arc_nde(i)%link_ams=.true.

	IF(error /= 0) THEN
       WRITE(911,*) 'error in reading network.dat at up/down node',iu,id
	 STOP
	ENDIF

11    FORMAT(2i7,i5,i7,2i2,2i4,2i6,i2,i4)
    IF(m_dynust_network_arc_de(i)%speedlimit < 0.001) THEN
      WRITE(911,*) "Error in speed limit"
      WRITE(911,'("Please check link", i7, " --> ", i7 )') iu, id
      STOP
    ENDIF
    IF(MTbay > 0) m_dynust_network_arc_de(i)%bay=MTbay
    IF(MTBayR > 0) m_dynust_network_arc_de(i)%bayR = MTBayR
!	m_dynust_network_arc_de(i)%MaxFlowRateOrig =  float(mfrtp)/3600.0*m_dynust_network_arc_de(i)%nlanes ! number of vehicles per second !commented out by i70!   THE ABOVE PART HAS BEEN REPLACED BY THE CALCULATION IN THE "SUBROUTINE READ_TRAFFIC_FLOW_MODELS" by i70
!	m_dynust_network_arc_de(i)%MaxFlowRate =      float(mfrtp)/3600.0*m_dynust_network_arc_de(i)%nlanes ! number of vehicles per second !i70
	m_dynust_network_arc_de(i)%SatFlowRate = float(sattp)/3600.0*m_dynust_network_arc_de(i)%nlanes


	IF(i3 < (m_dynust_network_arc_de(i)%SpeedLimit+m_dynust_network_arc_de(i)%Vfadjust)/60.0*528.0) THEN
 	 WRITE(511,'("shortL",i7,"-> ",i7," LinkL",i6," VMAX",f7.1,"Min L",i6)') iu,id,i3,float(m_dynust_network_arc_de(i)%SpeedLimit+m_dynust_network_arc_de(i)%Vfadjust),ifix((m_dynust_network_arc_de(i)%SpeedLimit+m_dynust_network_arc_de(i)%Vfadjust)/60.0*528.0)
!     i3 = (m_dynust_network_arc_de(i)%SpeedLimit+m_dynust_network_arc_de(i)%Vfadjust)/60.0*528.0*1.01
     m_dynust_network_arc_de(i)%ShortLink = .true.     
	ENDIF

     IF((m_dynust_network_arc_nde(i)%link_iden == 3.or.m_dynust_network_arc_nde(i)%link_iden == 4).and.(((m_dynust_network_arc_de(i)%SpeedLimit+m_dynust_network_arc_de(i)%Vfadjust)/60.0) > 0.67)) THEN
      WRITE(511,*) "Link", iu, " ->", id, " is a ramp, but the speed is too high"
	  WRITE(511,*) "Please correct it at network.dat other it will be reset to 40 mph"
      m_dynust_network_arc_de(i)%SpeedLimit = 40 
	  m_dynust_network_arc_de(i)%Vfadjust = 0.0
	 ENDIF


!	IF(m_dynust_network_arc_de(i)%MaxFlowRate <= 0.0001) THEN
!        WRITE(911,'("Check Saturation Flow for link #",i4)')i
!	  STOP
!	ENDIF
	IF(m_dynust_network_arc_de(i)%nlanes < 1) THEN
        WRITE(911,'("Check Number of Lanes for link #",i4)')i
	  STOP
	ENDIF


	m_dynust_network_arc_nde(i)%iunod = OutToInNodeNum(iu) !G
	m_dynust_network_arc_nde(i)%idnod = OutToInNodeNum(id) !G


      IF(m_dynust_network_arc_nde(i)%link_iden == 6.or.m_dynust_network_arc_nde(i)%link_iden == 8.or.m_dynust_network_arc_nde(i)%link_iden == 9.or.m_dynust_network_arc_nde(i)%link_iden == 10) THEN
        link_hot=link_hot+1
      ENDIF

	IF(i3*m_dynust_network_arc_de(i)%nlanes/5280.0 > longest_link) longest_link=i3*m_dynust_network_arc_de(i)%nlanes/5280.0

    m_dynust_network_arc_nde(i)%s=float(i3)/5280.0

    Lenpnt=.False.
	Grdpnt=.False.           
        DO ik = 1, GradeNum
          IF(m_dynust_network_arc_de(i)%LGrade <= GradeBPnt(ik)) THEN
            m_dynust_network_arc_de(i)%GRDInd = ik
	      Grdpnt=.True.
	      exit
		ENDIF
	  ENDDO
	  IF(.not.Grdpnt) m_dynust_network_arc_de(i)%GRDInd = GradeNum
        
	  DO ik = 1, LenNum
          IF(m_dynust_network_arc_nde(i)%s < LengthBPnt(m_dynust_network_arc_de(i)%GRDInd,ik)) THEN
             m_dynust_network_arc_de(i)%LENInd = ik
	       Lenpnt=.True.
		   exit
		ENDIF
	  ENDDO
        IF(.not.Lenpnt) m_dynust_network_arc_de(i)%LENInd = LenNum

       DO ijk=1,nu_de
         m_dynust_network_arc_de(i)%entry_service(ijk)=entrymx*m_dynust_network_arc_de(i)%nlanes*xminPerSimInt/60.0
       ENDDO

221   CONTINUE ! end of reading link loop

     
	  IF(logout > 0) WRITE(711,*) 'allocate_dynust_network_maxlinkveh'       
      CALL allocate_dynust_network_maxlinkveh


     IF(shelter_use > 0) THEN
     INQUIRE (FILE='shelter.dat',EXIST = EXT)
     IF(EXT)THEN
      OPEN(file='shelter.dat',unit=5543,status='unknown',iostat=error)
      IF(error /= 0) THEN
        WRITE(911,*) 'Error when opening Shelter.dat'
        STOP
      ENDIF
     ELSE
      WRITE(911,*) "shelter.dat does not exist when requested"
      STOP
     ENDIF
     READ(5543,*) shelter_num, shelter_mode
     IF(shelter_num > 0) THEN
       ALLOCATE(shelter(shelter_num))
       shelter(:)%node = 0
       shelter(:)%cap = 0
       shelter(:)%prob = 0
     ELSE
       WRITE(911,*) "no shelter specified in shelter.dat, please check"
       STOP
     ENDIF
     DO is = 1, shelter_num
        READ(5543,*) iseenode, shelter(is)%cap, shelter(is)%prob
        shelter(is)%node = OutToInNodeNum(iseenode)
     ENDDO
     IF(shelter_mode == 0) THEN
        shelter(:)%prob = 1.0/shelter_num
     ELSE
        totalshelterprob = sum(shelter(:)%prob)
        IF(totalshelterprob < 1.0) THEN
          WRITE(911,*) "error in shelter prob data"
          STOP
        ENDIF
     ENDIF
     ENDIF ! shelter_use > 0 


    IF(logout > 0) WRITE(711,*) 'read destination.dat'       
    OPEN(file='destination.dat',unit=53,status='old')      
    icount = 0
        DO ia = 1, nzones
        mzonetmp = ia
            DO IJ = 1, NoofConsPerZone(ia)
                IF(OutToInNodeNum(ConZoneTmp(ia,IJ)) < 1) THEN
                    WRITE(911,*) "Error in destination.dat"
                    WRITE(911,*) "Zone  ", ia
	                WRITE(911,*) "Node ", ConZoneTmp(ia,IJ)," does not exist"
	                STOP
	            ENDIF
   	        ENDDO
	        IF(error /= 0) THEN
                WRITE(911,*) 'Error when reading destination.dat'
	            STOP
	        ENDIF
            IF(NoofConsPerZone(ia) < 1) THEN
                WRITE(911,'("Error in destination.dat")') 
	            WRITE(911,'("Found zone",i4," contains no destination")')ia
	        ENDIF
            DO MM = 1, NoofConsPerZone(ia)
                IF(ConZoneTmp(ia,MM) < 1) THEN
                    WRITE(911,*) 'Error in destination.dat, zone',ia
	                WRITE(911,*) 'check number of zones and zone numbers'
	                STOP
	            ENDIF
	        ENDDO
      
      IF(NoofConsPerZone(ia) > 0) THEN	  
	 DO j = 1, NoofConsPerZone(ia)
         ConNoInZone(ia,j) = ConZoneTmp(ia,j)
         icount = icount + 1
	     iline = noofarcs_org + icount
         m_dynust_network_arc_nde(iline)%iunod= OutToInNodeNum(ConNoInZone(ia,j))
	     m_dynust_network_arc_nde(iline)%idnod= destination(MasterDest(mzonetmp))
         m_dynust_network_arc_nde(iline)%s= 1.0
         m_dynust_network_arc_de(iline)%SpeedLimit = 100
	     m_dynust_network_arc_de(iline)%FlowModelNum = 1
         m_dynust_network_arc_de(iline)%Vfadjust = 0
         m_dynust_network_arc_de(iline)%bay=5
         m_dynust_network_arc_de(iline)%nlanes= 5
         m_dynust_network_arc_nde(iline)%link_iden= 99   
         m_dynust_network_arc_de(iline)%MaxFlowRate= 100.0
         m_dynust_network_arc_de(iline)%SatFlowRate= 100.0
         m_dynust_network_arc_de(iline)%entry_service(1:nu_de)=1
         m_dynust_network_arc_de(iline)%LoadWeight = 0.0
         m_dynust_network_arc_de(iline)%LGrade = 0.0
         m_dynust_network_arc_de(iline)%GRDInd = 1
	     m_dynust_network_arc_de(iline)%LENInd = 1
         m_dynust_network_arc_de(iline)%ShortLink= .false.
	 ENDDO
	ENDIF
	ENDDO    

      DO i = 1, noofarcs - 1
	  DO j = i + 1, noofarcs
	    IF(m_dynust_network_arc_nde(j)%iunod < m_dynust_network_arc_nde(i)%iunod) THEN
	        CALL SwapIntArray(m_dynust_network_arc_nde(i)%iunod,m_dynust_network_arc_nde(j)%iunod)
            CALL SwapIntArray(m_dynust_network_arc_nde(i)%idnod,m_dynust_network_arc_nde(j)%idnod)
	        CALL SwapIntArray1B(m_dynust_network_arc_de(i)%bay,m_dynust_network_arc_de(j)%bay)
            CALL SwapIntArray1B(m_dynust_network_arc_de(i)%nlanes,m_dynust_network_arc_de(j)%nlanes)
            CALL SwapIntArray2B(m_dynust_network_arc_nde(i)%link_iden,m_dynust_network_arc_nde(j)%link_iden)
            CALL SwapLOGICALArray(m_dynust_network_arc_nde(i)%link_ams,m_dynust_network_arc_nde(j)%link_ams)            
            CALL SwapREALArray(m_dynust_network_arc_de(i)%MaxFlowRate,m_dynust_network_arc_de(j)%MaxFlowRate)
            CALL SwapREALArray(m_dynust_network_arc_de(i)%MaxFlowRateOrig,m_dynust_network_arc_de(j)%MaxFlowRateOrig)
            CALL SwapREALArray(m_dynust_network_arc_de(i)%SatFlowRate,m_dynust_network_arc_de(j)%SatFlowRate)
            CALL SwapREALArray(m_dynust_network_arc_nde(i)%s,m_dynust_network_arc_nde(j)%s)
	        CALL SwapIntArray1B(m_dynust_network_arc_de(i)%FlowModelNum,m_dynust_network_arc_de(j)%FlowModelNum)
            CALL SwapIntArray1B(m_dynust_network_arc_de(i)%Vfadjust,m_dynust_network_arc_de(j)%Vfadjust)
            CALL SwapIntArray2B(m_dynust_network_arc_de(i)%SpeedLimit,m_dynust_network_arc_de(j)%SpeedLimit)
            CALL SwapIntArray1B(m_dynust_network_arc_de(i)%LGrade,m_dynust_network_arc_de(j)%LGrade)
            CALL SwapIntArray2B(m_dynust_network_arc_de(i)%GRDInd,m_dynust_network_arc_de(j)%GRDInd)
            CALL SwapIntArray2B(m_dynust_network_arc_de(i)%LENInd,m_dynust_network_arc_de(j)%LENInd)
            CALL SwapLOGICALArray(m_dynust_network_arc_de(i)%ShortLink,m_dynust_network_arc_de(j)%ShortLink)
	    ENDIF
	  ENDDO
	ENDDO 

      DO i = 1, noofarcs - 1
        DO j = i + 1, noofarcs
            IF (m_dynust_network_arc_nde(j)%iunod == m_dynust_network_arc_nde(i)%iunod) THEN
		    IF (m_dynust_network_arc_nde(j)%idnod < m_dynust_network_arc_nde(i)%idnod) THEN
 	           CALL SwapIntArray(m_dynust_network_arc_nde(i)%iunod,m_dynust_network_arc_nde(j)%iunod)
               CALL SwapIntArray(m_dynust_network_arc_nde(i)%idnod,m_dynust_network_arc_nde(j)%idnod)
	           CALL SwapIntArray1B(m_dynust_network_arc_de(i)%bay,m_dynust_network_arc_de(j)%bay)
               CALL SwapIntArray1B(m_dynust_network_arc_de(i)%nlanes,m_dynust_network_arc_de(j)%nlanes)
               CALL SwapIntArray2B(m_dynust_network_arc_nde(i)%link_iden,m_dynust_network_arc_nde(j)%link_iden)
               CALL SwapLOGICALArray(m_dynust_network_arc_nde(i)%link_ams,m_dynust_network_arc_nde(j)%link_ams)                           
               CALL SwapREALArray(m_dynust_network_arc_de(i)%MaxFlowRate,m_dynust_network_arc_de(j)%MaxFlowRate)
               CALL SwapREALArray(m_dynust_network_arc_de(i)%MaxFlowRateOrig,m_dynust_network_arc_de(j)%MaxFlowRateOrig)
               CALL SwapREALArray(m_dynust_network_arc_de(i)%SatFlowRate,m_dynust_network_arc_de(j)%SatFlowRate)
               CALL SwapREALArray(m_dynust_network_arc_nde(i)%s,m_dynust_network_arc_nde(j)%s)
	           CALL SwapIntArray1B(m_dynust_network_arc_de(i)%FlowModelNum,m_dynust_network_arc_de(j)%FlowModelNum)
               CALL SwapIntArray1B(m_dynust_network_arc_de(i)%Vfadjust,m_dynust_network_arc_de(j)%Vfadjust)
               CALL SwapIntArray2B(m_dynust_network_arc_de(i)%SpeedLimit,m_dynust_network_arc_de(j)%SpeedLimit)
               CALL SwapIntArray1B(m_dynust_network_arc_de(i)%LGrade,m_dynust_network_arc_de(j)%LGrade)
               CALL SwapIntArray2B(m_dynust_network_arc_de(i)%GRDInd,m_dynust_network_arc_de(j)%GRDInd)
               CALL SwapIntArray2B(m_dynust_network_arc_de(i)%LENInd,m_dynust_network_arc_de(j)%LENInd)
              ENDIF
	      ELSE
	        exit
            ENDIF
	  ENDDO
	ENDDO

      DO i = 1, noofnodes
        DO j = 1, nzones
	    DO k = 1, NoofConsPerZone(j)
            IF(i == OutToInNodeNum(ConNoInZone(j,k))) THEN
              m_dynust_network_node(i)%iConZone(1)=m_dynust_network_node(i)%iConZone(1)+1
	          IF(m_dynust_network_node(i)%iConZone(1) >= iConZonePar) THEN
	            WRITE(911,*) "Exceeded centroids that a connector"
	            WRITE(911,*) "Can connect to = ", iConZonePar
                WRITE(911,*) "Review zone",j," node", m_dynust_network_node_nde(i)%IntoOutNodeNum 
			    WRITE(911,*) "in your destination.dat"
	            STOP
	          ENDIF
	          m_dynust_network_node(i)%iConZone(m_dynust_network_node(i)%iConZone(1)+1) = j
	        ENDIF
	    ENDDO
	  ENDDO
	ENDDO
   
	IF(logout > 0) WRITE(711,*) 'PREP_DATA_STRUCTURE'       
    CALL PREP_DATA_STRUCTURE()

    IF(logout > 0) WRITE(711,*) 'arcmove'
    CALL allocate_dynust_network_arcmove()

    ! Set turning penality to centroid connector to be zero
    DO I = 1, NoofArcs
        IF(m_dynust_network_arc_nde(i)%idnod > NoofNodes_org) THEN ! FOUND CENTROID
            DO M = 1, m_dynust_network_arc_nde(i)%inlink%no
                mlink = m_dynust_network_arc_nde(i)%inlink%p(m)
                MP = getBstMove(mLink,i)
                nlink = m_dynust_network_arc_nde(i)%ForToBackLink
                m_dynust_network_arcmove_de(nLink,MP)%GeoPreventBack = 0
            ENDDO
        ENDIF         
    ENDDO
    
    IF(logout > 0) WRITE(711,*) 'read toll.dat'
    IF(iteration == 0) THEN
    IF(TollExist) THEN
    READ(62,*) TravelTimeWeight, NTollLinktmp
        
    IF(NTollLinktmp > 0) THEN
    ALLOCATE(TollLink(1000),stat=error)
    TollLink(:)%Unode = 0
    TollLink(:)%Dnode = 0
    TollLink(:)%NTD = 0

    id1 = 1
    id2 = 1
    
    DO isee = 1, NTollLinktmp
      READ(62,*) (tolltmp(isee,k),k=1,4)
      IF(isee >= 2) THEN
        IF(tolltmp(isee,1) == tolltmp(isee-1,1).and.tolltmp(isee,2) == tolltmp(isee-1,2)) THEN
           id1=id1+1
        ELSE
           TollLink(id2)%NTD = id1
           id2 = id2 + 1
           id1 = 1
        ENDIF
      ENDIF
    ENDDO       
    TollLink(id2)%NTD = id1    
    NTollLink = id2
    DO ik = 1, NTollLink
        ALLOCATE(TollLink(ik)%scheme(TollLink(ik)%NTD))
        TollLink(ik)%scheme(:)%stime = 0
        TollLink(ik)%scheme(:)%etime = 0
        TollLink(ik)%scheme(:)%sov = 0
        TollLink(ik)%scheme(:)%tck = 0
        TollLink(ik)%scheme(:)%hov = 0
    ENDDO

    NonZeroToll=.false.
    rewind(62)
    READ(62,*)
    READ(62,*)
	DO i = 1, NTollLink
	 DO isee = 1, TollLink(i)%NTD
      READ(62,*) IUNode,IDNode,TollLink(i)%scheme(isee)%stime,TollLink(i)%scheme(isee)%etime,TollLink(i)%scheme(isee)%switch,TollLink(i)%scheme(isee)%sov,TollLink(i)%scheme(isee)%hov,TollLink(i)%scheme(isee)%tck
	  ICheck = 0
	  IF(OutToInNodeNum(iunode) < 1.or.OutToInNodeNum(idnode) < 1) THEN
	     WRITE(911,*) "error in toll.dat"
	     WRITE(911,*) "Please check from ", iunode, " to ", idnode
         STOP
   	  ENDIF
   	  IF(OutToInNodeNum(iunode) < 1.or.OutToInNodeNum(idnode) < 1) THEN
   	    WRITE(911,*) "error in toll.dat"
   	    WRITE(911,*) "please check number ", i
   	    STOP
   	  ENDIF
	  Icheck = GetFLinkFromNode(OutToInNodeNum(IUnode),OutToInNodeNum(IDnode),6)
	  IF(m_dynust_network_arc_nde(iCheck)%link_iden == 1) m_dynust_network_arc_nde(Icheck)%link_iden = 9 ! changed to freeway HOT
	  IF(m_dynust_network_arc_nde(iCheck)%link_iden /= 1) m_dynust_network_arc_nde(Icheck)%link_iden = 6 ! changed to general HOT
	  
	  IF(Icheck < 1) THEN
          WRITE(911,'("toll Link", i7,"==>",i7," does not exist")') IUNode,IDNode
          STOP
      ELSE
	    TollLink(i)%UNode = OutToInNodeNum(IUNode)
	    TollLink(i)%DNode = OutToInNodeNum(IDNode)
	    TollLink(i)%LinkNO = ICheck
          IF(TollLink(i)%scheme(isee)%sov > 0.or.TollLink(i)%scheme(isee)%tck > 0.or.TollLink(i)%scheme(isee)%hov > 0) NonZeroToll=.true.
          IF(TollLink(i)%scheme(isee)%switch == 0) THEN ! input is distance-based. Below is to convert all the total cost per link
            IF(TollLink(i)%scheme(isee)%sov > 99) THEN
              TollLink(i)%scheme(isee)%sov = 100
            ELSE   
              TollLink(i)%scheme(isee)%sov = min(100.0,TollLink(i)%scheme(isee)%sov*m_dynust_network_arc_nde(icheck)%s)
            ENDIF
	        IF(TollLink(i)%scheme(isee)%hov > 99) THEN
	          TollLink(i)%scheme(isee)%hov = 100
	        ELSE
	          TollLink(i)%scheme(isee)%hov = min(100.0,TollLink(i)%scheme(isee)%hov*m_dynust_network_arc_nde(icheck)%s)
	        ENDIF
	        IF(TollLink(i)%scheme(isee)%tck > 99) THEN
	          TollLink(i)%scheme(isee)%tck = 100
	        ELSE !  link based 
	          TollLink(i)%scheme(isee)%tck = min(100.0,TollLink(i)%scheme(isee)%tck*m_dynust_network_arc_nde(icheck)%s)	        
	        ENDIF
	      ELSE ! switch  = 1, input is link-based
            TollLink(i)%scheme(isee)%sov = min(100.0,TollLink(i)%scheme(isee)%sov)
	        TollLink(i)%scheme(isee)%tck = min(100.0,TollLink(i)%scheme(isee)%tck)
	        TollLink(i)%scheme(isee)%hov = min(100.0,TollLink(i)%scheme(isee)%hov)
	      ENDIF
	  ENDIF
	 ENDDO !DO isee = 1, TollLink(i)%NTD
	ENDDO !DO i = 1, NTollLink
	ELSE
	 NTollLink = 0
	ENDIF !(NTolllinktmp > 0)
	ENDIF !IF(TollExist) THEN
	ENDIF !IF(iteration == 0) THEN

  CALL CAL_TOLL

IF(RunMode == 1.OR.RunMode == 3.OR.RunMode == 4) THEN ! &&&&

      READ(42,*,iostat=error) (begint(i),i=1,nints+1)
      DO is = 1, nints
        IF(begint(is) < SimPeriod.and.begint(is+1) >= SimPeriod) exit
      ENDDO
      demandcut = is

239   FORMAT(2i5)
231   FORMAT(30f6.1)

        DO k=2,nints+1
         IF(begint(k-1) >= begint(k)) THEN
         WRITE(911,*) 'INPUT ERROR : demand data file'
         WRITE(911,*) 'check the start of the', k-1,'th demand interval'
         WRITE(911,*) 'and the', k, 'th interval.'
         STOP
         ENDIF
       ENDDO

      allocate(demtmp(nzones))

      demandsum = 0
      
	  DO 223 int=1,nints
      IF(int > demandcut) exit
	  READ(42,*,iostat=error)
      DO 223 iz=1,nzones
      
      IF(InputCode == 0) THEN
        READ(42,224,iostat=error) (demtmp(izz),izz=1,nzones)
      ELSEIF(InputCode > 0) THEN
        READ(42,*,iostat=error) (demtmp(izz),izz=1,nzones)
	  ELSE
        WRITE(911,*) 'error in reading demand.dat'
	    STOP
	  ENDIF
	  IF(error /= 0) THEN
         WRITE(911,*) 'Error when reading demand.dat'
	   STOP
	  ENDIF
	  DO ioz = 1, nzones
	    demandsum = demandsum + demtmp(ioz)*multi
	  ENDDO
223   CONTINUE
224   FORMAT(6f10.4)
! --

      rewind(42)
	  READ(42,*)
	  READ(42,*)


	 demandsumT = 0.0
	 demtmp(:) = 0
   IF(vehprofile(2)%odorfile == 1) THEN

     INQUIRE (FILE='demand_truck.dat', EXIST = EXT2)
     IF(.not.EXT2) THEN
        WRITE(911,*) "The user specified to load demand_truck.dat but this file is nonexistent"
        STOP
     ENDIF

     READ(54,*) (begintT(i),i=1,nintsT+1)	  

	 IF(nintsT > 0) THEN
      DO 2233 int=1,nintsT
     IF(int > demandcut) exit
     READ(54,*,iostat=error)
     DO 2233 iz=1,nzones
      IF(InputCode == 0) THEN
        READ(54,224,iostat=error) (demtmp(izz),izz=1,nzones)
      ELSEIF(InputCode > 0) THEN
        READ(54,*,iostat=error) (demtmp(izz),izz=1,nzones)
	  ELSE
        WRITE(911,*) 'error in reading demand.dat'
	    STOP
	  ENDIF

	 IF(error /= 0) THEN
         WRITE(911,*) 'Error in input when reading demand_truck.dat'
	   STOP
	 ENDIF
	  DO ioz = 1, nzones
	    demandsumT = demandsumT + demtmp(ioz)*multiT
	  ENDDO
2233  CONTINUE
2244  FORMAT(6f10.4)

      rewind(54)
	READ(54,*,iostat=error)
	READ(54,*,iostat=error)
    
	ENDIF ! nintsT gt 0
    ENDIF


	 demandsumH = 0.0
	 demtmp(:) = 0
   IF(vehprofile(3)%odorfile == 1) THEN
     INQUIRE (FILE='demand_hov.dat', EXIST = EXT2)
     IF(.not.EXT2) THEN
        WRITE(911,*) "The user specified to load demand_hov.dat but this file is nonexistent"
        STOP
     ENDIF

     READ(544,*) (begintH(i),i=1,nintsH+1)	  

	 IF(nintsH > 0) THEN
      DO 22337 int=1,nintsH
      IF(int > demandcut) exit     
     READ(544,*,iostat=error)
     DO 22337 iz=1,nzones
      IF(InputCode == 0) THEN
        READ(544,224,iostat=error) (demtmp(izz),izz=1,nzones)
      ELSEIF(InputCode > 0) THEN
        READ(544,*,iostat=error) (demtmp(izz),izz=1,nzones)
	  ELSE
        WRITE(911,*) 'error in reading demand.dat'
	    STOP
	  ENDIF

	 IF(error /= 0) THEN
         WRITE(911,*) 'Error in input when reading demand_hov.dat, zone ', iz
	   STOP
	 ENDIF
	  DO ioz = 1, nzones
	    demandsumH = demandsumH + demtmp(ioz)*multiH
	  ENDDO
22337 CONTINUE
    rewind(544)
	READ(544,*,iostat=error)
	READ(544,*,iostat=error)
    
	ENDIF ! nintsT gt 0
    ENDIF

      demandsumS = 0
      demtmp(:) = 0
   IF(superzoneswitch > 0) THEN
   IF(bg_num > 0) THEN ! demand_superzone.dat exist meaningful data - evac application      

	  DO 2238 int=1,nintsS
      IF(int > demandcut) EXIT
	  READ(545,*,iostat=error)
      DO 2238 iz=1,nzones

       IF(InputCode == 0) THEN
         READ(545,224,iostat=error) (demtmp(izz),izz=1,nzones)
       ELSEIF(InputCode > 0) THEN
         READ(545,*,iostat=error) (demtmp(izz),izz=1,nzones)
	   ELSE
         WRITE(911,*) 'error in reading demand.dat'
	     STOP
	   ENDIF
	   IF(error /= 0) THEN
         WRITE(911,*) 'Error when reading demand_superzone.dat'
	     STOP
	   ENDIF
	   DO ioz = 1, nzones
	     demandsumS = demandsumS + demtmp(ioz)*multiS
	   ENDDO
2238  CONTINUE

      REWIND(545)
	  READ(545,*)
	  READ(545,*)
   ELSE ! not using super zone for evac, but regluar purpose
      demandsumS = 0
   ENDIF !IF(bg_num > 0) THEN
   ENDIF ! IF(superzoneswitch > 0) THEN
ENDIF !&&&&

      IF(RunMode == 1.OR.RunMode == 3.OR.RunMode == 4) THEN
       IF(vehprofile(2)%odorfile < 1.and.vehprofile(3)%odorfile < 1) THEN ! read only demand.dat and distribute
         classpro2(1) = vehprofile(1)%vehfrac
         classpro2(2) = vehprofile(2)%vehfrac+vehprofile(1)%vehfrac
         classpro2(3:nu_types) = 1.0
         demandsumT = demandsum*((classpro2(2)-classpro2(1))/classpro2(1))
         demandsumH = demandsum*((classpro2(3)-classpro2(2))/classpro2(1))
         MaxVehiclesTable = demandsum + demandsumT + demandsumH
         IF(superzoneswitch > 0) THEN  ! considering using super zone
          IF(nintsS > 0) THEN          ! using evac application
            bgtmp = 0
            bgtmp = sum(bg_factor(1:nintsS))
            bgtmp = bgtmp/nintsS*1.05
            MaxVehiclesTable = bgtmp*MaxVehiclesTable + demandsumS
          ELSE                          ! regular super zone application
            bg_factor(:) = 1.0          ! use all from demand.dat
            MaxVehiclesTable = MaxVehiclesTable
          ENDIF
         ENDIF
       ELSEIF(vehprofile(2)%odorfile == 1.and.vehprofile(3)%odorfile == 0) THEN ! only truck read from file, HOV from combined demand with sov
         demandsumH = demandsum*vehprofile(3)%vehfrac/vehprofile(1)%vehfrac
         MaxVehiclesTable = demandsum + demandsumT + demandsumH
         IF(superzoneswitch > 0) THEN  ! considering using super zone
          IF(nintsS > 0) THEN          ! using evac application
            bgtmp = 0
            bgtmp = sum(bg_factor(1:nintsS))
            bgtmp = bgtmp/nintsS*1.05
            MaxVehiclesTable = bgtmp*MaxVehiclesTable + demandsumS
          ELSE                          ! regular super zone application
            bg_factor(:) = 1.0          ! use all from demand.dat
            MaxVehiclesTable = MaxVehiclesTable
          ENDIF
         ENDIF
         classpro2(1)=demandsum/MaxVehiclesTable
	     classpro2(2)=demandsumT/MaxVehiclesTable+classpro2(1)
	     classpro2(3:nu_types) = 1.0
	     IF(classpro2(3) > 1.0) THEN
	       WRITE(911,*) "error in calculating HOV percentage"
	       STOP
	     ENDIF
       ELSEIF(vehprofile(2)%odorfile == 0.and.vehprofile(3)%odorfile == 1) THEN ! only hov read from file, truck from combined demand with sov
         demandsumT = demandsum*vehprofile(2)%vehfrac/vehprofile(1)%vehfrac
         MaxVehiclesTable = demandsum + demandsumT + demandsumH         
         IF(superzoneswitch > 0) THEN  ! considering using super zone
          IF(nintsS > 0) THEN          ! using evac application
            bgtmp = 0
            bgtmp = sum(bg_factor(1:nintsS))
            bgtmp = bgtmp/nintsS*1.05
            MaxVehiclesTable = bgtmp*MaxVehiclesTable + demandsumS
          ELSE                          ! regular super zone application
            bg_factor(:) = 1.0          ! use all from demand.dat
            MaxVehiclesTable = MaxVehiclesTable
          ENDIF
         ENDIF
         classpro2(1)=demandsum/MaxVehiclesTable
	     classpro2(2)=demandsumT/MaxVehiclesTable + classpro2(1)
	     classpro2(3:nu_types) = 1.0
	     IF(classpro2(3) > 1.0) THEN
	       WRITE(911,*) "error in calculating HOV percentage"
	       STOP
	     ENDIF
	   ELSE ! both turck and hov are read from files
         MaxVehiclesTable = demandsum + demandsumT + demandsumH         	   
         IF(superzoneswitch > 0) THEN  ! considering using super zone
          IF(nintsS > 0) THEN          ! using evac application
            bgtmp = 0
            bgtmp = sum(bg_factor(1:nintsS))
            bgtmp = bgtmp/nintsS*1.05
            MaxVehiclesTable = bgtmp*MaxVehiclesTable + demandsumS
          ELSE                          ! regular super zone application
            bg_factor(:) = 1.0          ! use all from demand.dat
            MaxVehiclesTable = MaxVehiclesTable
          ENDIF
         ENDIF

         classpro2(1)=demandsum/MaxVehiclesTable
	     classpro2(2)=demandsumT/MaxVehiclesTable + classpro2(1)
	     classpro2(3:nu_types) = 1.0
	     IF(classpro2(3) > 1.0) THEN
	       WRITE(911,*) "error in calculating HOV percentage"
	       STOP
	     ENDIF
	  ENDIF
	  ENDIF

    IF(logout > 0) WRITE(711,*) 'allocate_dynust_vehicle'
	CALL allocate_dynust_veh

	READ(44,1223,iostat=error) (strtsig(i),i=1,isig)
         strtsig(isig+1)=2*SimPeriod
1223    FORMAT(50f6.2)
	isigcount=1

    IF(logout > 0) WRITE(711,*) 'read ramp.dat'
    IF(dec_num > 0) THEN
    DO 455 i=1,dec_num
      READ(45,*,iostat=error)(detector(i,j),j=1,7),(ramp_par(i,j),j=1,4), ramp_start(i),ramp_end(i)
       IF(error /= 0) THEN
         WRITE(911,*) 'Error when reading ramp.dat'
	     STOP
       ENDIF

!       IF(ramp_par(i,4) == 2.or.ramp_par(i,4) == 3) THEN ! feedback ramp control
        detector(i,2) = OutToInNodeNum(detector(i,2))!G
        detector(i,3) = OutToInNodeNum(detector(i,3)) !G
!       ENDIF
        detector(i,6) = OutToInNodeNum(detector(i,6))!G
        detector(i,7) = OutToInNodeNum(detector(i,7)) !G
        detector_length(i)=detector(i,4)-detector(i,5)
        
       IF(detector_length(i) <= 0.and.ramp_par(i,4) /= 2.and.ramp_par(i,4) /= 3) THEN
         WRITE(911,*) 'INPUT ERROR : Ramp metering data'
         WRITE(911,*) 'the detector length for ramp number ',i
         WRITE(911,*) 'is <= zero.  Please check the input file'
         STOP
       ENDIF
       
      IF(detector(i,2) < 1.or.detector(i,3) < 1.or.detector(i,6) < 1.or.detector(i,7) < 1) THEN       
         WRITE(911,*) 'INPUT ERROR : Ramp metering data'
         WRITE(911,*) 'node number for mainlane or ramps are incorrect, printing the incorrect one'
         WRITE(911,*) 'detector number : ',i
         IF(detector(i,2) < 1) THEN
           WRITE(911,*) '2nd entry error'         
         ELSEIF(detector(i,3) < 1) THEN
           WRITE(911,*) '3rd entry error'
         ELSEIF(detector(i,6) < 1) THEN
           WRITE(911,*) '6th entry error'         
         ELSEIF(detector(i,7) < 1) THEN         
           WRITE(911,*) '7th entry error'
         ENDIF
         STOP
      ENDIF
       

455   CONTINUE
      ENDIF
451   FORMAT(7i6,f7.3,2f6.2)
452   FORMAT(2i6)
453   FORMAT(2f8.2)


      IF(dec_num > 0) THEN
      DO i=1,dec_num
        det_link(i) = GetFlinkFromNode(detector(i,2),detector(i,3),7)
        IF(det_link(i) < 1) THEN
          WRITE(911,*) "Error in Ramp.dat"
          WRITE(911,*) "Please check the node number data for ramp # ",i
          STOP
        ENDIF
        m_dynust_network_arc_de(GetFlinkFromNode(detector(i,2),detector(i,3),7))%link_detector=i

        detector_ramp(i)=GetFlinkFromNode(detector(i,6),detector(i,7),7)
        IF(detector_ramp(i) < 1) THEN
           WRITE(911,*) "error in finding ramp link, please check ramp.dat #  ",i
           STOP
        ENDIF

        IF(ramp_par(i,4) == 2) THEN ! feedback control
          IF(detector(i,2) < 1.or.detector(i,3) < 1) THEN
   	        WRITE(911,*) "error in ramp.dat"
   	        WRITE(911,*) "please check number ", i
   	        STOP
   	      ENDIF
        ENDIF   
        IF(detector(i,6) < 1.or.detector(i,6) < 1) THEN
           WRITE(911,*) "error in ramp.dat"
   	       WRITE(911,*) "please check number ", i
   	       STOP
   	    ENDIF

        IF(ramp_par(i,4) == 2) THEN 
         IF(det_link(i) == 0) THEN 
          WRITE(911,*) 'INPUT ERROR : ramp data file'
          WRITE(911,*) 'check the detector link for metered' 
          WRITE(911,*) 'ramp number',i
          STOP
         ENDIF
        ENDIF
      ENDDO
	ENDIF


    IF(logout > 0) WRITE(711,*) 'read movement.dat'
       DO i=1,noofarcs
          m_dynust_network_arcmove_nde(i,maxmove)%move=m_dynust_network_arc_nde(i)%llink%no
          m_dynust_network_arcmove_nde(i,maxmove)%movein=m_dynust_network_arc_nde(i)%inlink%no
       end do

      DO 100 i=1,noofarcs_org
       ifrom=0
       ito=0
       mlink=0
       mleft=0
       mst=0
       mright=0
       mother1=0
       mother2=0
       mother3=0
       mother4=0
       muturn=0
10    FORMAT(10i7)

      READ(47,*,iostat=error) ifrom,ito,ileft,ist,iright,iother1,iother2, iUTFlag
	
    IF(ileft < 1.and.ist < 1.and.iright < 1.and.iother1 < 1.and.iother2 < 1.and.iUTFlag > 0) THEN
      IGP = GetFLinkFromNode(OutToInNodeNum(ito),OutToInNodeNum(ifrom),8)
	  IF(IGP > 0) THEN
        ist = ifrom
		ELSE
		iseeUvio = 1
		WRITE(911,*) "Error in link from ",ifrom," to ",ito, " in movement.dat"
		WRITE(911,*) "U-Turn link does not exist but the U-Turn flag has been specified"
		WRITE(911,*) "Please turn the U-turn flag in movement.dat for this link to zero"
 	  ENDIF
	ENDIF

	IF(error /= 0) THEN
         WRITE(911,*) 'Error when reading movement.dat'
	   WRITE(911,*) 'in line', i
	   WRITE(911,*) 'Possible reason: having tab instead of space'
	   WRITE(911,*) 'Best solution: re-enter the whole line'
	   STOP
	ENDIF

      IF(ifrom > 0.and.OutToInNodeNum(ifrom) < 1) THEN
	  WRITE(911,*) 'Error when reading movement.dat'
        WRITE(911,*) 'Line', i
	  WRITE(911,*) 'The first number is incorrect'
	  STOP
	ENDIF

      IF(ito > 0.and.OutToInNodeNum(ito) < 1) THEN
	  WRITE(911,*) 'Error when reading movement.dat'
        WRITE(911,*) 'Line', i
	  WRITE(911,*) 'The 2nd number is incorrect'
	  STOP
	ENDIF

      IF(ileft > 0) THEN
       IF(OutToInNodeNum(ileft) < 1) THEN
	  WRITE(911,*) 'Error when reading movement.dat'
        WRITE(911,*) 'Line', i
	  WRITE(911,*) 'The 3rd number is incorrect'
	  STOP
	  ENDIF
	  ENDIF

      IF(ist > 0) THEN
       IF(OutToInNodeNum(ist) < 1) THEN
	     WRITE(911,*) 'Error when reading movement.dat'
         WRITE(911,*) 'Line', i
	     WRITE(911,*) 'The 4th number is incorrect'
	     STOP
	  ENDIF
	  ENDIF

      IF(iright > 0) THEN
      IF(OutToInNodeNum(iright) < 1) THEN
	  WRITE(911,*) 'Error when reading movement.dat'
        WRITE(911,*) 'Line', i
	  WRITE(911,*) 'The 5th number is incorrect'
	  STOP
	  ENDIF
	  ENDIF

      IF(iother1 > 0) THEN
       IF(OutToInNodeNum(iother1) < 1) THEN
	  WRITE(911,*) 'Error when reading movement.dat'
        WRITE(911,*) 'Line', i
	  WRITE(911,*) 'The 6th number is incorrect'
	  STOP
	  ENDIF
	  ENDIF

      IF(iother2 > 0) THEN
       IF(OutToInNodeNum(iother2) < 1) THEN
	  WRITE(911,*) 'Error when reading movement.dat'
        WRITE(911,*) 'Line', i
	  WRITE(911,*) 'The 7th number is incorrect'
	  STOP
	  ENDIF
	ENDIF

    IF(iUTFlag /= 1.and.iUTFlag /= 0) THEN
	  WRITE(911,*) 'Error when reading movement.dat'
        WRITE(911,*) 'Line', i
	  WRITE(911,*) 'The 8th number is incorrect'
	  WRITE(911,*) 'The 8th number should be 0 or 1'
	  STOP
	ENDIF


     mflag = 0
	 iother5 = 0
	 iother4 = 0
       DO mp = 1, nzones
         DO mn = 1, NoofConsPerZone(mp)
            IF(ito == ConNoInZone(mp,mn)) THEN
	         mflag = mflag + 1
	         IF(mflag == 1) THEN
	           iother5 = m_dynust_network_node_nde(destination(MasterDest(mp)))%IntoOutNodeNum
	         ELSEIF(mflag == 2) THEN
	           iother4 = m_dynust_network_node_nde(destination(MasterDest(mp)))%IntoOutNodeNum
               ELSEIF(mflag > 2) THEN
	         ENDIF
	      ENDIF
         ENDDO
	 ENDDO

  IF(iUTFlag > 0) THEN ! U-Turn allowed
    jfind = 0
      
	KK = GetBLinkFromNode(OutToInNodeNum(ito),OutToInNodeNum(ifrom))
	IF(KK > 0) THEN
        IF(OutToInNodeNum(ito) == m_dynust_network_arc_nde(KK)%UNodeOfBackLink) THEN
         jfind = 1
	  ENDIF
	ENDIF
	IF(jfind > 0) THEN
      IF(m_dynust_network_arc_nde(GetFLinkFromNode(OutToInNodeNum(ifrom),OutToInNodeNum(ito),8))%link_iden /= 1.or.m_dynust_network_arc_nde(GetFLinkFromNode(OutToInNodeNum(ifrom),OutToInNodeNum(ito),8))%link_iden /= 2) THEN
          muturn = GetFLinkFromNode(OutToInNodeNum(ito),OutToInNodeNum(ifrom),8) !Forward * link # of ifrom->ito is not freeway
      ENDIF
	ENDIF
  ELSE ! U-Turn not allowed
      muturn = 0
  ENDIF
        
      IF(ifrom /= 0.and.ito /= 0)   mlink =   GetFLinkFromNode(OutToInNodeNum(ifrom),OutToInNodeNum(ito),8)
      IF(ito /= 0.and.ileft /= 0)   mleft =   GetFLinkFromNode(OutToInNodeNum(ito),OutToInNodeNum(ileft),8)
      IF(ito /= 0.and.ist /= 0)     mst =     GetFLinkFromNode(OutToInNodeNum(ito),OutToInNodeNum(ist),8)
      IF(ito /= 0.and.iright /= 0)  mright =  GetFLinkFromNode(OutToInNodeNum(ito),OutToInNodeNum(iright),8)
      IF(ito /= 0.and.iother1 /= 0) mother1 = GetFLinkFromNode(OutToInNodeNum(ito),OutToInNodeNum(iother1),8)
      IF(ito /= 0.and.iother2 /= 0) mother2 = GetFLinkFromNode(OutToInNodeNum(ito),OutToInNodeNum(iother2),8)

         IF(mlink == 0) THEN 
          WRITE(911,*) 'INPUT ERROR : movement data file'
          WRITE(911,*) 'check the upstream and down stream' 
          WRITE(911,*) 'nodes for line number',i
	      WRITE(911,*) m_dynust_network_arc_nde(i)%iunod,m_dynust_network_arc_nde(i)%idnod
          STOP
         ENDIF

         IF(mleft == 0.and.ileft /= 0) THEN 
          WRITE(911,*) 'INPUT ERROR : movement data file'
          WRITE(911,*) 'check the left turning movement' 
          WRITE(911,*) 'for line number',i
          WRITE(911,*) mleft,ileft
		  STOP
         ENDIF

         IF(mst == 0.and.ist /= 0) THEN 
          WRITE(911,*) 'INPUT ERROR : movement data file'
          WRITE(911,*) 'check the through movement' 
          WRITE(911,*) 'for line number',i
          WRITE(911,*) mst,ist
		  STOP
         ENDIF

         IF(mright == 0.and.iright /= 0) THEN 
          WRITE(911,*) 'INPUT ERROR : movement data file'
          WRITE(911,*) 'check the right turning movement' 
          WRITE(911,*) 'for line number',i
          WRITE(911,*) mright,iright
		  STOP
         ENDIF

         IF(mother1 == 0.and.iother1 /= 0) THEN 
          WRITE(911,*) 'INPUT ERROR : movement data file'
          WRITE(911,*) 'check the other movement' 
          WRITE(911,*) 'for line number',i
	      WRITE(911,*) ifrom,ito, mother1,iother1 
          STOP
         ENDIF

         IF(mother2 == 0.and.iother2 /= 0) THEN 
          WRITE(911,*) 'INPUT ERROR : movement data file'
          WRITE(911,*) 'check the other_i1 movement' 
          WRITE(911,*) 'for line number',i
	      WRITE(911,*) ifrom,ito, mother2, iother2
          STOP
         ENDIF

        j=mlink
        FoundFlag=.false.
        FoundFlag=.false.

        DO k=1,m_dynust_network_arc_nde(j)%llink%no
		 isee=m_dynust_network_arc_nde(m_dynust_network_arc_nde(j)%llink%p(k))%ForToBackLink
         MVPB = getBstMove(j,m_dynust_network_arc_nde(j)%llink%p(k))
         IF(m_dynust_network_node_nde(m_dynust_network_arc_nde(m_dynust_network_arc_nde(j)%llink%p(k))%idnod)%IntoOutNodeNum > 900000) THEN ! the downstream node is the centroid  Chiu 2006

  		    m_dynust_network_arcmove_nde(j,k)%move=2

            FoundFlag=.true.
		    m_dynust_network_arcmove_de(j,k)%GeoPreventFor = 0 ! allowed
            m_dynust_network_arcmove_de(isee,MVPB)%GeoPreventBack = 0 ! allowed
            m_dynust_network_arcmove_nde(m_dynust_network_arc_nde(j)%llink%p(k),MVPB)%movein = 1
			   IF(m_dynust_network_arc_nde(m_dynust_network_arc_nde(j)%llink%p(k))%inlink%p(MVPB) < 1) THEN
                 WRITE(911,*) "error movement to centroid turn not match"
                 STOP
			   ENDIF

		 ELSEIF(m_dynust_network_arc_nde(j)%llink%p(k) == mleft) THEN

		    m_dynust_network_arcmove_nde(j,k)%move=1
		    m_dynust_network_arc_nde(j)%llink%m(1) = mleft
            FoundFlag=.true.
		    m_dynust_network_arcmove_de(j,k)%GeoPreventFor = 0 ! allowed
!            MVPB = MoveNoBackLink(j,llink(j)%p(k))
            m_dynust_network_arcmove_de(isee,MVPB)%GeoPreventBack = 0 ! allowed
			m_dynust_network_arcmove_nde(m_dynust_network_arc_nde(j)%llink%p(k),MVPB)%movein = 1
			   IF(m_dynust_network_arc_nde(m_dynust_network_arc_nde(j)%llink%p(k))%inlink%p(MVPB) < 1) THEN
                 WRITE(911,*)"error left not match"
                 STOP
			   ENDIF

         ELSEIF(m_dynust_network_arc_nde(j)%llink%p(k) == mst) THEN

		    m_dynust_network_arcmove_nde(j,k)%move=2
		    m_dynust_network_arc_nde(j)%llink%m(2) = mst
            FoundFlag=.true.
		    m_dynust_network_arcmove_de(j,k)%GeoPreventFor = 0 ! allowed
!            MVPB = MoveNoBackLink(j,llink(j)%p(k))
            m_dynust_network_arcmove_de(isee,MVPB)%GeoPreventBack = 0 ! allowed
            m_dynust_network_arcmove_nde(m_dynust_network_arc_nde(j)%llink%p(k),MVPB)%movein = 2
			   IF(m_dynust_network_arc_nde(m_dynust_network_arc_nde(j)%llink%p(k))%inlink%p(MVPB) < 1) THEN
                 WRITE(911,*) "error straight not match"
                 STOP
			   ENDIF

         ELSEIF(m_dynust_network_arc_nde(j)%llink%p(k) == mright) THEN

			m_dynust_network_arcmove_nde(j,k)%move=3
		    m_dynust_network_arc_nde(j)%llink%m(3) = mright
			m_dynust_network_arc_de(j)%RightLane = m_dynust_network_arc_nde(j)%llink%p(k)
            FoundFlag=.true.
		    m_dynust_network_arcmove_de(j,k)%GeoPreventFor = 0 ! allowed
!            MVPB = MoveNoBackLink(j,llink(j)%p(k))
            m_dynust_network_arcmove_de(isee,MVPB)%GeoPreventBack = 0 ! allowed
            m_dynust_network_arcmove_nde(m_dynust_network_arc_nde(j)%llink%p(k),MVPB)%movein = 3
			   IF(m_dynust_network_arc_nde(m_dynust_network_arc_nde(j)%llink%p(k))%inlink%p(MVPB) < 1) THEN
                 WRITE(911,*) "error right turn not match"
                 STOP
			   ENDIF

         ELSEIF(m_dynust_network_arc_nde(j)%llink%p(k) == mother1) THEN

			m_dynust_network_arcmove_nde(j,k)%move=4
		    m_dynust_network_arc_nde(j)%llink%m(4) = mother1
            FoundFlag=.true.
		    m_dynust_network_arcmove_de(j,k)%GeoPreventFor = 0 ! allowed
!            MVPB = MoveNoBackLink(j,llink(j)%p(k))
            m_dynust_network_arcmove_de(isee,MVPB)%GeoPreventBack = 0 ! allowed
            m_dynust_network_arcmove_nde(m_dynust_network_arc_nde(j)%llink%p(k),MVPB)%movein = 4
			   IF(m_dynust_network_arc_nde(m_dynust_network_arc_nde(j)%llink%p(k))%inlink%p(MVPB) < 1) THEN
                 WRITE(911,*) "error other 1 not match"
                 STOP
			   ENDIF

         ELSEIF(m_dynust_network_arc_nde(j)%llink%p(k) == mother2) THEN
			
			m_dynust_network_arcmove_nde(j,k)%move=5
		    m_dynust_network_arc_nde(j)%llink%m(5) = mother2
            FoundFlag=.true.
		    m_dynust_network_arcmove_de(j,k)%GeoPreventFor = 0 ! allowed
!            MVPB = MoveNoBackLink(j,llink(j)%p(k))
            m_dynust_network_arcmove_de(isee,MVPB)%GeoPreventBack = 0 ! allowed
            m_dynust_network_arcmove_nde(m_dynust_network_arc_nde(j)%llink%p(k),MVPB)%movein = 5
			   IF(m_dynust_network_arc_nde(m_dynust_network_arc_nde(j)%llink%p(k))%inlink%p(MVPB) < 1) THEN
                 WRITE(911,*) "error other 2 not match"
                 STOP
			   ENDIF

         ELSEIF(m_dynust_network_arc_nde(j)%llink%p(k) == muturn) THEN

			m_dynust_network_arcmove_nde(j,k)%move = 6
		    m_dynust_network_arc_nde(j)%llink%m(6) = muturn
            FoundFlag=.true.
		    m_dynust_network_arcmove_de(j,k)%GeoPreventFor = 0 ! allowed
!            MVPB = MoveNoBackLink(j,llink(j)%p(k))
            m_dynust_network_arcmove_de(isee,MVPB)%GeoPreventBack = 0 ! allowed
            m_dynust_network_arcmove_nde(m_dynust_network_arc_nde(j)%llink%p(k),MVPB)%movein = 6
			   IF(m_dynust_network_arc_nde(m_dynust_network_arc_nde(j)%llink%p(k))%inlink%p(MVPB) < 1) THEN
                 WRITE(911,*) "error uturn not match"
                 STOP
			   ENDIF

		  ENDIF


          ENDDO

          DO js = 1, m_dynust_network_arc_nde(j)%llink%no
             IF(m_dynust_network_arc_nde(m_dynust_network_arc_nde(j)%llink%p(js))%iunod == m_dynust_network_arc_nde(j)%idnod.and.&
                m_dynust_network_arc_nde(m_dynust_network_arc_nde(j)%llink%p(js))%idnod == m_dynust_network_arc_nde(j)%iunod) THEN
 		       m_dynust_network_arcmove_nde(j,js)%move=6
		       m_dynust_network_arcmove_de(j,js)%GeoPreventFor = 0 ! allowed
!              MVPB = MoveNoBackLink(j,llink(j)%p(js))
               MVPB = getBstMove(j,m_dynust_network_arc_nde(j)%llink%p(js))
               m_dynust_network_arcmove_de(m_dynust_network_arc_nde(m_dynust_network_arc_nde(j)%llink%p(js))%ForToBackLink,MVPB)%GeoPreventBack = 0 ! allowed
               m_dynust_network_arcmove_nde(m_dynust_network_arc_nde(j)%llink%p(js),MVPB)%movein = 6
			   IF(m_dynust_network_arc_nde(m_dynust_network_arc_nde(j)%llink%p(js))%inlink%p(MVPB) < 1) THEN
                 WRITE(911,*) "error uturn not match"
                 STOP
			   ENDIF
			   WRITE(511,'("Found network edge link without U-turn enabled. Enforcing U-Turn","From ",i7," --> ",i7)') ifrom, ito
		     ENDIF
		  ENDDO

	
! --

100   CONTINUE

IF(iseeUvio > 0) STOP

isee = 0
      DO is = 1, noofarcs
        DO js = 1,m_dynust_network_arc_nde(is)%llink%no
           moveturn = GetMoveTurn_LL(is,m_dynust_network_arc_nde(is)%llink%p(js))
		  IF(moveturn < 1) THEN
		  WRITE(911,*) "check movement.dat"
		  WRITE(911,*) "from ",m_dynust_network_node_nde(m_dynust_network_arc_nde(is)%iunod)%InToOutNodeNum, &
		               " to ", m_dynust_network_node_nde(m_dynust_network_arc_nde(is)%idnod)%IntoOutNodeNum, &
		               " missing ", m_dynust_network_node_nde(m_dynust_network_arc_nde(js)%idnod)%IntoOutNodeNum
		  STOP
		  isee = 1
		  ENDIF            
        ENDDO
      ENDDO

      READ(48,*,iostat=error)
      DO 400 k=1,5
        READ(48,311,iostat=error) fgcratio
311    FORMAT(4x,f3.1)
        IF(fgcratio == 0.3) igc=1
        IF(fgcratio == 0.4) igc=2
        IF(fgcratio == 0.5) igc=3
        IF(fgcratio == 0.6) igc=4
        IF(fgcratio == 0.7) igc=5
      DO i=1,3
        READ(48,262,iostat=error) itmp,(leftcapWb(igc,itmp,j),j=1,7)
      ENDDO
262    FORMAT(i1,3x,7i5)
400   CONTINUE
      READ(48,*,iostat=error)
      DO i=1,5
        READ(48,311,iostat=error) fgcratio
        IF(fgcratio == 0.3) igc=1
        IF(fgcratio == 0.4) igc=2
        IF(fgcratio == 0.5) igc=3
        IF(fgcratio == 0.6) igc=4
        IF(fgcratio == 0.7) igc=5
        DO k=1,ifix(fgcratio*10+0.01)*3
        READ(48,313,iostat=error) ivolume,itmp,(leftcapWOb(igc,itmp,ivolume,j),j=1,7)
        ENDDO
313    FORMAT(i1,i4,i4,6i5)
      ENDDO


     DO i = 1, NLevel
       READ(56,5656,iostat=error) (STOPcap4w(i,j),j=1,NMove)
      ENDDO
5656  FORMAT(12x,5i7) 

      DO i = 1, Level2N
       READ(57,*,iostat=error) STOPcap2wIND(i),(STOPcap2w(i,j),j=1,Move2N)
      ENDDO
5657  FORMAT(4i5) 


      DO i = 1, Level2N
       READ(60,*,iostat=error) YieldCapIND(i),(YieldCap(i,j),j=1,Move2N)
      ENDDO

      
    IF(vms_num > 0) THEN    
       CALL VMS_READINPUT
	ENDIF ! vms_num > 0

!      IF(NoofBuses > 0) THEN
!  	    WRITE(911,*) 'BUS is temporarily unavailable'
!		STOP
!
!	  tlatest_bus=0.0
!        DO i=1,NoofBuses
!          READ(50,*,iostat=error) i1,i2,m_dynust_bus(i)%busstart,m_dynust_bus(i)%busdwell,m_dynust_bus(i)%NoBusNode
!          READ(50,'(50i7)',iostat=error)(buspathtmp(k),k=1,m_dynust_bus(i)%NoBusNode)
!          READ(50,'(50i7)',iostat=error)(busSTOPtmp(k),k=1,m_dynust_bus(i)%NoBusNode)
!          DO kk = 1, m_dynust_bus(i)%NoBusNode-1
!            NodeTest = GetFLinkFromNode(OutToInNodeNum(buspathtmp(kk)),OutToInNodeNum(buspathtmp(kk+1)))
!            IF(NodeTest < 1) THEN
!	       WRITE(911,'("Error in bus.dat between",i4,"th and next node")')kk
!             STOP
!            ELSE
!             NodeTest = 0
!	      ENDIF
!	    ENDDO
!          DO kk = 1, m_dynust_bus(i)%NoBusNode
!            CALL BusAtt_Insert(i,kk,1,OutToInNodeNum(buspathtmp(kk)))
!            CALL BusAtt_Insert(i,kk,2,busSTOPtmp(kk))
!	    ENDDO
!	    mpzone = OutToInNodeNum(buspathtmp(m_dynust_bus(i)%NoBusNode))
!        itmp=destination(MasterDest(m_dynust_network_node(mpzone)%iConZone(2)))
!	    Index1D = m_dynust_bus(i)%NoBusNode + 1
!          CALL BusAtt_Insert(i,Index1D,1,itmp)
!          CALL BusAtt_Insert(i,Index1D,2,0)
!
!          IF(m_dynust_bus(i)%busstart > tlatest_bus) tlatest_bus=m_dynust_bus(i)%busstart
!          DO j=1,noofarcs
!            IF(m_dynust_network_arc_nde(j)%iunod == OutToInNodeNum(i1).and.m_dynust_network_arc_nde(j)%idnod == OutToInNodeNum(i2)) THEN
!               m_dynust_bus(i)%buslink=j !G
!	         exit
!	      ENDIF
!          end do

!         IF(m_dynust_bus(i)%buslink == 0) THEN 
!          WRITE(911,*) 'INPUT ERROR : bus data file'
!          WRITE(911,*) 'check the strating link' 
!          WRITE(911,*) 'for bus number',i
!          STOP
!         ENDIF
!
!        ENDDO ! end of read bus loop 
!	 ENDIF
150   FORMAT(20i5)

      IF(inci_num > 0) THEN
        DO i=1,inci_num
         READ(46,*) i1,i2,inci(i,1),inci(i,2),inci(i,3)
           IF(OutToInNodeNum(i1) < 1.or.OutToInNodeNum(i2) < 1) THEN
             WRITE(911,*) "error in incident.dat"
             WRITE(911,*) "check number ", i
           ENDIF
           KL = GetFLinkFromNode(OutToInNodeNum(i1),OutToInNodeNum(i2),10)
         IF(KL < 1) THEN 
          WRITE(911,*) 'INPUT ERROR : incident data file'
          WRITE(911,*) 'check the incident link' 
          WRITE(911,*) 'for incident number',i
          STOP
         ENDIF

		    incil(i)=KL !G
	        m_dynust_network_arc_de(KL)%LinkStatus%inci = 1 ! this link has incident and the current status is before the incident
            m_dynust_network_arc_de(KL)%LinkStatus%incistarttime = inci(i,1)
            m_dynust_network_arc_de(KL)%LinkStatus%inciendtime   = inci(i,2)
       end do
      CALL INCI_SCAN(0.0) 

      ENDIF


      timeinterval = simPerAgg*xminPerSimInt
         
	IF(iteration == 0.or.vms_num > 0.or.ier_ok == 1) THEN
	  iti_nu = 1
	  CALL MEMALLOCATE_SP(0)
	  callksp1 = .true.
	ENDIF
	 	 
    IF(vms_num > 0.OR.ier_ok == 1.OR.(ReadHistArr > 0.AND.(RunMode == 2.OR.RunMode == 4))) THEN
      !IF(.not.ALLOCATED(pathpointercopy1)) THEN
         !ALLOCATE(PathPointerCopy1(noof_master_destinations,noofnodes,kay,maxmove),stat=error)
         !pathpointerCopy1(:,:,:,:) = 0
         !ALLOCATE(PathPointerCopy2(noof_master_destinations,noofnodes,kay,maxmove),stat=error)
         !pathpointerCopy2(:,:,:,:) = 0
         !ALLOCATE(PathPointerCopy3(noof_master_destinations,noofnodes,kay,maxmove),stat=error)
         !pathpointerCopy3(:,:,:,:) = 0
         !itmp = 1
         !ALLOCATE(LabelOutCostCopy(noof_master_destinations,noofnodes,itmp,kay,MaxMove),stat=error)
         !LabelOutCostCopy(:,:,:,:,:) = 0
      !ENDIF
	ENDIF
 	
      DO i=1,noofarcs
        DO j=1,m_dynust_network_arc_nde(i)%llink%no
           IF(m_dynust_network_arcmove_nde(i,j)%move == 2) THEN
              il=m_dynust_network_arc_nde(i)%llink%p(j)
			  FoundFlag=.false.
             DO ii=1,noofarcs
               IF(m_dynust_network_arc_nde(il)%idnod == m_dynust_network_arc_nde(ii)%iunod.and.m_dynust_network_arc_nde(il)%iunod == m_dynust_network_arc_nde(ii)%idnod) THEN
				  FoundFlag = .true.
	              exit
               ENDIF
             end do
	        IF(FoundFlag) THEN ! found ii is the opposite link number
                m_dynust_network_arc_de(i)%opp_linkP(2)=ii ! the link number, not movement number
                m_dynust_network_arc_de(i)%opp_lane=m_dynust_network_arc_de(ii)%nlanes
	            DO ms = 1, m_dynust_network_arc_nde(ii)%llink%no ! determine the movement number from link ii
	              mouth = m_dynust_network_arc_nde(ii)%llink%p(ms)
	              IF(m_dynust_network_arc_nde(mouth)%iunod == m_dynust_network_arc_nde(i)%idnod.and.m_dynust_network_arc_nde(mouth)%idnod == m_dynust_network_arc_nde(i)%iunod) THEN
                    m_dynust_network_arc_de(i)%opp_linkP(1) = ms ! the movement number, not link number	                
	                exit
	              ENDIF
	            ENDDO
	        ENDIF
           ENDIF
        ENDDO  
      ENDDO   

    CALL READ_TRAFFIC_FLOW_MODELS
   
    CALL READ_INTERSECTION_CONTROLS

    DO js = 1, noofarcs
       IF (m_dynust_network_arc_nde(js)%llink%no > 6.and.m_dynust_network_node(m_dynust_network_arc_nde(js)%idnod)%node(2) > 3) THEN
	     WRITE(511,*) "Possibly too many outbound links for controled node", m_dynust_network_node_nde(m_dynust_network_arc_nde(js)%idnod)%IntoOutNodeNum
	   ENDIF
    ENDDO
    DO js = 1, noofarcs
       IF(m_dynust_network_arc_nde(js)%inlink%no > 6.and.m_dynust_network_node(m_dynust_network_arc_nde(js)%iunod)%node(2) > 3) THEN
	   WRITE(511,*) "Possibly too many inbound links for node", m_dynust_network_node_nde(m_dynust_network_arc_nde(js)%iunod)%IntoOutNodeNum
       ENDIF
    ENDDO

! REAING ORIGIN.DAT BELOW
  isit = 0
  DO i = 1, nzones
      excludexl = 0.0
	  SumLoadWeight = 0.0
	  READ(52,*,iostat=error) izonetmp, NoofGenLinksPerZone(i), IDGen
	  IF(NoofGenLinksPerZone(i) < 1) THEN
	    isit = 1
	    WRITE(911,'("Zone ",i7," has no generation link")') i
	    WRITE(911,*) "Please check"
	  ENDIF

	  IF(IDGen > 0) LoadWeightID(i)=.True.
	  IF(error /= 0) THEN
        WRITE(911,*) 'Error when reading origin.dat'
	    STOP
	  ENDIF


	ALLOCATE(TempArray(NoofGenLinksPerZone(i)))
	DO j = 1, NoofGenLinksPerZone(i)
       READ(52,*,iostat=error) IUpNode,IDnNode, LWTmp !LWTmp is a temp var for LWTmp
	   IF(error /= 0) WRITE(911,*) 'Error when reading origin.dat', STOP

	   IF(m_dynust_network_node_nde(OutToInNodeNum(IUpNode))%izone /= i.and.m_dynust_network_node_nde(OutToInNodeNum(IDnNode))%izone /= i) THEN
         INQUIRE(UNIT = 511, OPENED = Fexist)
	     IF(.not. Fexist) THEN
	       OPEN(file='Warning.dat',unit=511,status='unknown',iostat=error)
	       IF(error /= 0) WRITE(911,*) 'Error when opening Warning.dat', STOP
         ENDIF
         WRITE(511,'("Link",i7,"  -->",i7," receives demand from zone",i6," not a physical zone for both nodes")')IUpNode,IDnNode,i
       ENDIF

       IF(OutToInNodeNum(IUpNode) < 1.OR.OutToInNodeNum(IDnNode)<1) THEN
            WRITE(911,'("Error in origin.dat")') 
            WRITE(911,'("Zone, GenLink#, Upnode, Dnnode",5I7)') i, j, iUpNode, IDnNode
            STOP 
       ENDIF 
	   LinkNo = GetFLinkFromNode(OutToInNodeNum(IUpNode),OutToInNodeNum(IDnNode),11)
	   TempArray(j) = LinkNo
        
	   IF(LinkNo < 1) THEN
           WRITE(911,*) 'Error in origin.dat'
	     WRITE(911,*) 'when reading zone', i
           WRITE(911,'(" Link",i7,"  -->",i7)') IUpNode,IDnNode 
           STOP
	   ENDIF
	   IF(m_dynust_network_arc_nde(LinkNo)%link_iden == 6) THEN
           WRITE(911,'("Error in origin.dat")')
	     WRITE(911,'("link ",i5," -->",i5," is highway/freeway")') IUpNode,IDnNode
	     WRITE(911,'("It cannt be a generation link in zone",i3)') i 
!	     STOP
	   ENDIF
	   IF(LinkNo < 1) THEN
	      WRITE(911,*) 'Error in origin.dat'
            WRITE(911,*) 'Link doesnt exit'
            WRITE(911,*) 'zone, ud, nd', i, IUpNode, IDnNode
	      STOP 
	   ENDIF
       LinkNoInZone(i,j) = LinkNo
!	   IF(LoadWeightID(i)) THEN !User-specified Loading weight is used
         SumLoadWeight = SumLoadWeight + LWTmp
	     m_dynust_network_arc_de(LinkNo)%LoadWeight=LWTmp
	     TotalLinkLenPerZone(i)=TotalLinkLenPerZone(i)+m_dynust_network_arc_de(LinkNo)%xl
!       ELSE
!	     TotalLinkLenPerZone(i)=TotalLinkLenPerZone(i)+m_dynust_network_arc_de(LinkNo)%xl
!	   ENDIF
	ENDDO !j = 1, NoofGenLinksPerZone(i)
	 
	IF(LoadWeightID(i)) THEN ! THIS PART IS TO ADJUST THE LOADING WEIGHT FOR THOSE LINKS NOT SPECIFIED WEIGHT BY USER IN TEH USER SPECIFIED MODE
        checkvalue = 0.0
	    residue = 0.0
        IF(abs(SumLoadWeight-1.0) > 0.0001) THEN ! GET IN IF USER WEIGHTS DON'T SUM UP TO 1.0
           checkvalue = 0
           DO j = 1, NoofGenLinksPerZone(i)
	         IF(m_dynust_network_arc_de(TempArray(j))%LoadWeight > 0.0001) THEN
	            checkvalue = checkvalue + m_dynust_network_arc_de(TempArray(j))%LoadWeight
	            excludexl = excludexl + m_dynust_network_arc_de(TempArray(j))%xl
	         ENDIF
           ENDDO
           residue = 1.0 - checkvalue
           IF(residue < 0.0) THEN
             WRITE(911,*) "Error in specifying user defined loading weight for zone ", i
             WRITE(911,*) "Total user specified loading weights exceeds 1.0"
             STOP
           ENDIF
           remainxl = TotalLinkLenPerZone(i)-excludexl
           IF(remainxl < 0.0) THEN
             WRITE(911,*) "Error in specifying user defined loading weight for zone ", i
             STOP
           ENDIF
           xspan = 0.0
           DO j = 1, NoofGenLinksPerZone(i)
	         IF(m_dynust_network_arc_de(TempArray(j))%LoadWeight < 0.0001) THEN
	            IF(remainxl < 0.0001) THEN
	              m_dynust_network_arc_de(TempArray(j))%LoadWeight = 0
	            ELSE
	              m_dynust_network_arc_de(TempArray(j))%LoadWeight = residue*m_dynust_network_arc_de(TempArray(j))%xl/remainxl
	            ENDIF
	         ENDIF
             xspan = xspan + m_dynust_network_arc_de(TempArray(j))%LoadWeight
           ENDDO
           IF(ABS(xspan-1.0) >0.0001) THEN
             WRITE(911,*) "ERROR IN CALCUALTING THE USER SPECIFIED LOADING WEIGHTS FOR ZONE", I, xspan
             STOP
           ENDIF
	    ENDIF	 
	 ENDIF
     DEALLOCATE(TempArray)
   ENDDO !i = 1, nzones

    IF(isit > 0.and.genCheck == 1) STOP

      DO i = 1, noofarcs
        DO j = 1, nzones
	    DO k = 1, NoofGenLinksPerZone(j)
            IF(i == LinkNoInZone(j,k)) THEN
              m_dynust_network_arc_de(i)%NGenZ2I(1)=m_dynust_network_arc_de(i)%NGenZ2I(1) + 1
              IF(m_dynust_network_arc_de(i)%NGenZ2I(1) >= nu_iGen) THEN
                WRITE(911,'("too many zones from which link ",2i7," receives demand from")') m_dynust_network_node_nde(m_dynust_network_arc_nde(i)%iunod)%IntoOutNodeNum,m_dynust_network_node_nde(m_dynust_network_arc_nde(i)%idnod)%IntoOutNodeNum
                STOP
              ENDIF
	        m_dynust_network_arc_de(i)%NGenZ2I(m_dynust_network_arc_de(i)%NGenZ2I(1)+1)= j !NGenZ2I should keep the original zone number
	      ENDIF
	    ENDDO
	  ENDDO
	ENDDO

      DO i = 1, noofarcs
	   IF(m_dynust_network_arc_de(i)%NGenZ2I(1) > 0.and.m_dynust_network_arc_nde(i)%idnod > noofnodes_org) THEN
         WRITE(911,*) 'errors: NGenZ2I contains connectors'
      ENDIF
	ENDDO

  DO isee = 1, noofnodes
     iZoneFlag = .false.
     IF(m_dynust_network_node_nde(isee)%izone < 1) THEN
     IF(m_dynust_network_node_nde(isee)%IntoOutNodeNum <= 900000) THEN
       DO iz = 1, nzones
         DO iz1 = 1, NoofGenLinksPerZone(iz)
           IF(m_dynust_network_node_nde(isee)%IntoOutNodeNum == m_dynust_network_node_nde(m_dynust_network_arc_nde(LinkNoinZone(iz,iz1))%iunod)%IntoOutNodeNum.or.m_dynust_network_node_nde(isee)%IntoOutNodeNum ==  m_dynust_network_node_nde(m_dynust_network_arc_nde(LinkNoinZone(iz,iz1))%idnod)%IntoOutNodeNum) THEN
             m_dynust_network_node_nde(isee)%izone= iz
             iZoneFlag = .true.
             exit
           ENDIF
         ENDDO
       ENDDO
      IF(.not.iZoneFlag) THEN
        DO isee2 = 1, nzones
          DO isee3 = 1, NoofConsPerZone(isee2)
            IF(m_dynust_network_node_nde(isee)%IntoOutNodeNum == ConZoneTmp(isee2,isee3))THEN
             m_dynust_network_node_nde(isee)%izone= isee2
             iZoneFlag = .true.
             exit
            ENDIF 
          ENDDO
        ENDDO 
      ENDIF
      IF(.not.iZoneFlag) THEN
        WRITE(511,*) "node ", isee, "zone not found" 
      ENDIF
     ELSE !IF(IntoOutNodeNum(isee) < 900000) THEN
       m_dynust_network_node_nde(isee)%izone= m_dynust_network_node_nde(isee)%IntoOutNodeNum-900000 ! this condition is redundent for completness. In fact centroids have been assigned izone when they are generated
     ENDIF !IF(IntoOutNodeNum(isee) < 900000) THEN
     ENDIF !IF(izone(isee) < 1) THEN
  ENDDO
  

    READ(101,*) i30,i30_t
    READ(101,*) i31,i31_t
    READ(101,*) i32,i32_t
    READ(101,*) i33,i33_t
    READ(101,*) i34,i34_t
    READ(101,*) i35,i35_t
    READ(101,*) i36,i36_t
    READ(101,*) i37,i37_t
    READ(101,*) i38,i38_t
    READ(101,*) i39,i39_t
	READ(101,*) idemand_info
    i301 = i33
	i301_t = i33_t
    i40 = 1
	i40_t = 10 ! hard-wire them for now


	IF(i301 > 0) OPEN(file='OutLinkTravelTime.dat',unit=301,status='unknown')
	IF(i30 > 0)  OPEN(file='OutLinkGen.dat',unit=30,status='unknown')
	IF(i31 > 0)  OPEN(file='OutLinkVeh.dat',unit=31,status='unknown')
	IF(i32 > 0)  OPEN(file='OutLinkQue.dat',unit=32,status='unknown')
	IF(i33 > 0)  OPEN(file='OutLinkSpeedAll.dat',unit=33,status='unknown')
	IF(i34 > 0)  OPEN(file='OutLinkDent.dat',unit=34,status='unknown')
	IF(i35 > 0)  OPEN(file='OutLinkSpeedFree.dat',unit=35,status='unknown')
	IF(i36 > 0)  OPEN(file='OutLinkDentFree.dat',unit=36,status='unknown')
	IF(i37 > 0)  OPEN(file='OutLeftFlow.dat',unit=37,status='unknown')
	IF(i38 > 0)  OPEN(file='OutGreen.dat',unit=38,status='unknown')
	IF(i39 > 0)  OPEN(file='OutFlow.dat',unit=39,status='unknown')
	IF(i40 > 0)  OPEN(file='OutAccuVol.dat',unit=400,status='unknown')
	IF(i40 > 0)  OPEN(file='OutAccuVolT.dat',unit=401,status='unknown')

      IF(WorkZoneNum > 0) THEN
      
	     DO i = 1, WorkZoneNum
           READ(58,*) FNodetmp,TNodetmp,WorkZone(i)%ST,WorkZone(i)%ET,WorkZone(i)%CapRed,WorkZone(i)%SpeedLmt,WorkZone(i)%Discharge

           WorkZone(i)%FNode=OutToInNodeNum(FNodeTmp)
           WorkZone(i)%TNode=OutToInNodeNum(TNodetmp)
         
           isee=GetFLinkFromNode(WorkZone(i)%FNode,WorkZone(i)%TNode,12)
		   m_dynust_network_arc_de(isee)%SpeedLimit = WorkZone(i)%SpeedLmt
		 ENDDO
      
	  ENDIF


      IF(iteration == 0) THEN

      INQUIRE (FILE='UCDAirOption.dat', EXIST = EXT)
      IF(EXT) THEN
        OPEN(FILE='UCDAirOption.dat', unit = 8889, status='old')       
        READ(8889,*) UCDAirOption
	ENDIF

  ENDIF


  IF(ALLOCATED(demtmp)) DEALLOCATE(demtmp)

    OPEN(file='SpaceTimePar.dat',unit=881,status='unknown',iostat=error)
	IF(error /= 0) THEN
         WRITE(911,*) 'Error when opening SpaceTimePar.dat'
	   STOP
	ENDIF

     NSigOpt = 0               
     INQUIRE(file='SigOpt.dat', Exist = Fexist)
     IF(Fexist) THEN
         OPEN(file='SigOptOut.dat',unit=514,status='unknown',iostat=error)       
         OPEN(file='SigOpt.dat',unit=513,status='unknown',iostat=error)       
         READ(513,*) NsigOpt, SigOptInt
         IF(NSigOpt > 0) THEN
           ALLOCATE(SigOptNid(NSigOpt))
           ALLOCATE(SigOut(noofarcs,maxmove+1))
           SigOut(:,:) = 0
           SigOptNid(:) = 0
           DO is = 1, NSigOpt
             READ(513,*) SigOptNid(is)
             SigOptNid(is) = OutToInNodeNum(SigOptNid(is))
             IF(SigOptNid(is) < 1) THEN
               WRITE(911,*) "error in reading SigOpt.dat"
               WRITE(911,*) "Please check"
               STOP
             ENDIF
             DO isee = 1, noofarcs
                IF(SigOptNid(is) == m_dynust_network_arc_nde(isee)%idnod) THEN
                   SigOut(isee,maxmove)=1
!                   exit
                ENDIF
             ENDDO
           ENDDO
         ENDIF
     ENDIF

    IF(UETimeFlagR == 1) CALL ReadUETravelTime

    CALL ReadCongPricingConfig    

    !DEALLOCATE(ConZoneTmp) 
    
    IF(FuelOut == 1.and.(iteration == iteMax.or.reach_converg == .true.)) THEN  ! read fuel consumption files
       CALL ReadNAllocate_fuel(NoofVeh)
    ENDIF

! READ TRANSIT
    IF((GenTransit == 1.OR.ReadTransit == 1).and.iteration == 0) CALL InputTransit ! READ TRANSIT ROUTE AND SCHEDULE BASED ON THE SWITCH IN PARAMETER.DAT

!   READ HISTORICAL PATH ARRIVAL TIME
    OPEN(file = "DivertedVeh.dat", unit = 8456, status = "unknown")
    WRITE(8456,*)
    IF(ReadHistArr > 0.and.(RunMode == 2.OR.RunMode == 4)) THEN
      CALL READ_HIST_TIME
    ENDIF
    
    IF(logout > 0) WRITE(711,*) 'input() finished'          

!   Read MOVES input if the switch is on
    !IF(MOVESFlag > 0) THEN
    !    CALL ReadMOVESInput()
    !ENDIF


END SUBROUTINE


