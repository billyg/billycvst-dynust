      SUBROUTINE SIGNAL_PRETIMED(IntoOutNodeNumber,t)
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~ Copyright (C) 2002-2011 The Arizona Board of Regents on Behalf of The University of Arizona.  ~~!
!~~ Developed by Yi-Chang Chiu, Ph.D. as par to the DynusT project.                               ~~!
!~~ This program is free software: you can redistribute it and/or modify it                       ~~!
!~~ under the terms of the GNU General Public License version 2 as published by                   ~~!
!~~ the Free Software Foundation.                                                                 ~~!
!~~ This program is distributed in the hope that it will be useful,                               ~~!
!~~ but WITHOUT ANY WARRANTY; without even the implied warranty of                                ~~!
!~~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                                          ~~!
!~~ See the GNU General Public License for more details.                                          ~~!
!~~ You should have received a copy of the GNU General Public License                             ~~!
!~~ along with this program (in the file COPYING).                                                ~~!
!~~ IF not, see <http://www.gnu.org/licenses/>.                                                   ~~!
!~~ Please see http://www.dynust.net for contact information and names                            ~~!
!~~ of individual contributors.                                                                   ~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!

      USE DYNUST_MAIN_MODULE
      USE DYNUST_SIGNAL_MODULE

	  INTEGER(1) GreenTmp
	  INTEGER CurrentActPhase,currentStatus,IntroGreen,AvaG,OffSet,NIBLink,AssignG
	  LOGICAL time_needed
!> > > > > > > > > >                < < < < < < < < < < <
!>>>>>>>>>>>>>>>>>>>CODES START HERE<<<<<<<<<<<<<<<<<<<<<
!> > > > > > > > > >                < < < < < < < < < < <
    
      GreenExt = nint(60*xminPerSimInt) ! greenext is set to be the sim interval. Will relax in the future


      CurrentActPhase = control_array(IntoOutNodeNumber)%ActivePhase
      CurrentStatus = GetSignalPar(IntoOutNodeNumber,CurrentActPhase,1)
      IntoGreen     = GetSignalPar(IntoOutNodeNumber,CurrentActPhase,2)
      AvaG          = GetSignalPar(IntoOutNodeNumber,CurrentActPhase,3)
      OffSet        = GetSignalPar(IntoOutNodeNumber,CurrentActPhase,4)
      NIBLink       = GetSignalPar(IntoOutNodeNumber,CurrentActPhase,5)

      IF(t*60.0 >= offset) THEN ! considering offset. run below only IF t is greater than or equal to the offset

		IF(IntoGreen+GreenExt < AvaG) THEN !simple update
              CALL AssignGreen(IntoOutNodeNumber,CurrentActPhase,NIBlink,GreenExt)  !update Green into this phase
              control_array(IntoOutNodeNumber)%F(CurrentActPhase)%UsedG = control_array(IntoOutNodeNumber)%F(CurrentActPhase)%UsedG + GreenExt !assign green to movements			
		ELSE
		      GreenTmp = AvaG - IntoGreen! update green for current phase
              CALL AssignGreen(IntoOutNodeNumber,CurrentActPhase,NIBlink,GreenTmp)  !update Green into this phase
			  control_array(IntoOutNodeNumber)%F(CurrentActPhase)%UsedG = 0 ! reset intogreen
			  control_array(IntoOutNodeNumber)%F(CurrentActPhase)%Status = 0
              ! update phase and update info for this new active phase
			  IF(CurrentActPhase == control_array(IntoOutNodeNumber)%NPhase) THEN
                 control_array(IntoOutNodeNumber)%ActivePhase = 1
			  ELSE
                 control_array(IntoOutNodeNumber)%ActivePhase = control_array(IntoOutNodeNumber)%ActivePhase + 1
			  ENDIF
			  control_array(IntoOutNodeNumber)%F(control_array(IntoOutNodeNumber)%ActivePhase)%Status = 1
			  CurrentActPhase = control_array(IntoOutNodeNumber)%ActivePhase
    		  GreenTmp = min(nint(60*xminPerSimInt),IntoGreen+GreenExt-AvaG) ! update green for next phase
              control_array(IntoOutNodeNumber)%F(CurrentActPhase)%UsedG = control_array(IntoOutNodeNumber)%F(CurrentActPhase)%UsedG + GreenTmp
              NIBLink = GetSignalPar(IntoOutNodeNumber,CurrentActPhase,5)
			  CALL AssignGreen(IntoOutNodeNumber,CurrentActPhase,NIBlink,GreenTmp)  !update Green into this phase
		ENDIF
		
	  ENDIF ! offset
END SUBROUTINE