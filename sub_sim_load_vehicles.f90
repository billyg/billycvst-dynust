SUBROUTINE SIM_LOAD_VEHICLES(t)     
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~ Copyright (C) 2002-2011 The Arizona Board of Regents on Behalf of The University of Arizona.  ~~!
!~~ Developed by Yi-Chang Chiu, Ph.D. as par to the DynusT project.                               ~~!
!~~ This program is free software: you can redistribute it and/or modify it                       ~~!
!~~ under the terms of the GNU General Public License version 2 as published by                   ~~!
!~~ the Free Software Foundation.                                                                 ~~!
!~~ This program is distributed in the hope that it will be useful,                               ~~!
!~~ but WITHOUT ANY WARRANTY; without even the implied warranty of                                ~~!
!~~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                                          ~~!
!~~ See the GNU General Public License for more details.                                          ~~!
!~~ You should have received a copy of the GNU General Public License                             ~~!
!~~ along with this program (in the file COPYING).                                                ~~!
!~~ IF not, see <http://www.gnu.org/licenses/>.                                                   ~~!
!~~ Please see http://www.dynust.net for contact information and names                            ~~!
!~~ of individual contributors.                                                                   ~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
USE DYNUST_MAIN_MODULE
USE DYNUST_VEH_MODULE
USE DYNUST_NETWORK_MODULE
USE DYNUST_LINK_VEH_LIST_MODULE
USE DYNUST_VEH_PATH_ATT_MODULE
USE DYNUST_FUEL_MODULE
USE RAND_GEN_INT
USE DFPORT

INTERFACE
      SUBROUTINE printscreen(printstr,intx,inty,dx1,dy1,dx2,dy2,refresh)
       INTEGER intx, inty, refresh,dx1,dy1,dx2,dy2
       CHARACTER (80) printstr
      END SUBROUTINE 
END INTERFACE  

CHARACTER(80) printstr1,printstr2
INTEGER mnum,Nlnkmv,NodeSum
INTEGER, ALLOCATABLE::AinTmp(:,:)
INTEGER, save:: pathin(10000)
INTEGER,save :: jfo
INTEGER(2) is
REAL sg2,limentr

!> > > > > > > > > >                < < < < < < < < < < <
!>>>>>>>>>>>>>>>>>>>CODES START HERE<<<<<<<<<<<<<<<<<<<<<
!> > > > > > > > > >                < < < < < < < < < < <

IF(t < 0.001) THEN
  jfo = 0
  pathin(:) = 0
ENDIF

IF((RunMode == 1.OR.RunMode == 3.OR.RunMode == 4).and.iteration == 0) THEN 

  IF(mod(nint(t/xminPerSimInt),tdspstep) == 0) THEN
  ALLOCATE(AinTmp(vlgcounter,5))

  AinTmp(1:vlgcounter,1) = VehAin(1:vlgcounter,2)
  AinTmp(1:vlgcounter,2) = VehAin(1:vlgcounter,3)
  AinTmp(1:vlgcounter,3) = VehAin(1:vlgcounter,1)
  AinTmp(1:vlgcounter,4) = VehAin(1:vlgcounter,4)
  AinTmp(1:vlgcounter,5) = VehAin(1:vlgcounter,5)

  CALL IMSLSort(AinTmp,vlgcounter,5)	  

  VehAin(1:vlgcounter,2) = AinTmp(1:vlgcounter,1)
  VehAin(1:vlgcounter,3) = AinTmp(1:vlgcounter,2)
  VehAin(1:vlgcounter,1) = AinTmp(1:vlgcounter,3)
  VehAin(1:vlgcounter,4) = AinTmp(1:vlgcounter,4)
  VehAin(1:vlgcounter,5) = AinTmp(1:vlgcounter,5)
  DEALLOCATE(AinTmp)
ENDIF

DO while (VehAin(mop,2)-1 == nint(t/xminPerSimInt))
  !justveh = justveh + 1
  CALL write_vehicles(justveh,VehAin(mop,4),VehAin(mop,4),97,1,98)

  IF(t >= starttm.and.t < endtm) numcars = numcars + 1

  CALL entryqueue_insert(VehAin(mop,3),VehAin(mop,4))

  m_dynust_network_arc_de(VehAin(mop,3))%entryqueue=m_dynust_network_arc_de(VehAin(mop,3))%entryqueue+1
  mop = mop + 1
ENDDO

ELSEIF(iteration > 0) THEN 
 
  DO while(justveh < numGenVeh)
!   IF(mop > 0.and.nint(m_dynust_last_stand(mop)%stime/xminPerSimInt) > nint(t/xminPerSimInt)) go to 400
   IF(jfo == 1) THEN
      jfo = 0
      justveh = justveh + 1
      go to 365 
   ELSE
    READ(6099,'(i9,1000i7)') pathin(1), pathin(2), (pathin(is),is=3,pathin(2)+2)
     justveh = justveh + 1
     mop = pathin(1)
     IF(FuelOut > 0.and.(iteration == iteMax.or.reach_converg == .true.).and.(RunMode == 1.OR.RunMode == 3.OR.RunMode == 4)) THEN
       CALL SetFuelLevel(justveh)
     ENDIF
   ENDIF
   IF (nint(m_dynust_last_stand(mop)%stime/xminPerSimInt) > nint(t/xminPerSimInt)) THEN
     justveh = justveh - 1
     jfo = 1
     go to 400
   ENDIF

365 CONTINUE   
   IF(pathin(2)+3 > maxnu_pa) THEN
      WRITE(911,*) "maxnu_pa error, please increase the maxnu_path setting in PARAMETER.dat"
      STOP
   ENDIF
   
   pathin(pathin(2)+3:maxnu_pa) = 0
   m_dynust_veh(mop)%position = m_dynust_veh_nde(mop)%xparinit

    CALL vehatt_P_setup(mop,pathin(2))
    CALL vehatt_A_setup(mop,pathin(2))

	DO is = 1, pathin(2)
	   value = float(OutToInNodeNum(pathin(is+2)))
	   CALL vehatt_Insert(mop,is,1,value)
    ENDDO	
    
    CALL vehatt_Active_Size(mop,pathin(2))

    IF(m_dynust_last_stand(mop)%stime >= starttm.and.m_dynust_last_stand(mop)%stime < endtm) THEN
       numcars=numcars+1
       m_dynust_veh(mop)%itag=1
    ELSE 
       m_dynust_veh(mop)%itag=0
       numNtag = numNtag + 1	        
    ENDIF

    CALL entryqueue_insert(m_dynust_last_stand(mop)%isec,mop)
    m_dynust_network_arc_de(m_dynust_last_stand(mop)%isec)%entryqueue=m_dynust_network_arc_de(m_dynust_last_stand(mop)%isec)%entryqueue+1

    CALL write_vehicles(totalveh,justveh,mop,97,1,98)

  ENDDO 
400 CONTINUE

ENDIF !RunMode == 1 and iteration = 0

!$OMP PARALLEL FIRSTPRIVATE(xminPerSimInt,iteration,RunMode,ipinit,t,Nlnk,Nlnkmv,entrymeter,r4,NoofBuses,TotalBusGen) PRIVATE(mnum,iveh,j,printstr1,genm,isee,sg2,limentr,slimentr,ips1,ips2,ips3,j1) NUM_THREADS(nThread)
!$OMP DO SCHEDULE(DYNAMIC)

DO 7 i=1,noofarcs

IF(m_dynust_network_arc_nde(i)%link_iden /= 99) THEN

   m_dynust_network_arc_de(i)%nout=0
   mnum=0.0

   IF(TripChainList(i)%Fid > 0) THEN
       DO iveh = 1, TripChainList(i)%Fid
         j=TripChainList(i)%P(iveh)
         IF(m_dynust_veh(j)%RemainDwell < xminPerSimInt) THEN 
            m_dynust_veh(j)%RemainDwell = 0
            m_dynust_veh(j)%DestVisit=m_dynust_veh(j)%DestVisit + 1
            m_dynust_last_stand(j)%jdest=m_dynust_veh_nde(j)%IntDestZone(m_dynust_veh(j)%DestVisit)
			CALL TripChain_remove(i,j) ! Remove j from TripChainList
			CALL entryqueue_InsFront(i,j)
			m_dynust_network_arc_de(i)%entryqueue=m_dynust_network_arc_de(i)%entryqueue+1
			m_dynust_veh(j)%position = 0.0

            IF(iteration == 0.or.(iteration > 0.and.m_dynust_last_stand(j)%vehclass == 4)) THEN
              IF(RunMode == 1.or.RunMode == 0.or.RunMode == 3) THEN
                 printstr1 = 'CALL get veh in veh load, RunMode = '
                 CALL printscreen(printstr1,20,440,0,0,0,0,0)                 
                 CALL RETRIEVE_VEH_PATH_AStar(j,i,ipinit,m_dynust_veh_nde(j)%icurrnt,m_dynust_last_stand(j)%jdest,1,NodeSum) ! pass 1 to select the best path
                 !CALL RETRIEVE_VEH_PATH_AStar(j,i,ipinit,m_dynust_veh_nde(j)%icurrnt,m_dynust_last_stand(j)%jdestNode,1,NodeSum) ! pass 1 to select the best path
                 !CALL RETRIEVE_VEH_PATH(j,i,ipinit,m_dynust_veh_nde(j)%icurrnt,m_dynust_last_stand(j)%jdest,1,NodeSum) ! pass 1 to select the best path
                 CALL RETRIEVE_NEXT_LINK(t,j,i,m_dynust_veh_nde(j)%icurrnt,Nlnk,Nlnkmv)
              ENDIF
1000      CONTINUE
            ELSEIF(iteration > 0) THEN
            ! LEAVE THIS FOR NOW
            ENDIF
         ELSE
            m_dynust_veh(j)%RemainDwell=m_dynust_veh(j)%RemainDwell-xminPerSimInt
         ENDIF
	  ENDDO
   ENDIF


! Start Loading Vehicles
! Check # of vehicles allowed to be loaded
   genm=max(0.0,(m_dynust_network_arc_de(i)%maxden*m_dynust_network_arc_de(i)%xl)-m_dynust_network_arc_de(i)%pcetotal)
   isee = backpointr(m_dynust_network_arc_nde(i)%iunod+1)-backpointr(m_dynust_network_arc_nde(i)%iunod)
!   IF(isee > 0.and.(m_dynust_network_arc_nde(i)%link_iden /= 1.and.m_dynust_network_arc_nde(i)%link_iden /= 2.and.m_dynust_network_arc_nde(i)%link_iden /= 8.and.m_dynust_network_arc_nde(i)%link_iden /= 9.and.m_dynust_network_arc_nde(i)%link_iden /= 10)) THEN
!	 sg2 =     1.0-(m_dynust_network_arc_de(i)%pcetotal/(m_dynust_network_arc_de(i)%maxden*m_dynust_network_arc_de(i)%xl*entrymeter))
!     IF(sg2 > 0.5) THEN
!	   limentr=((entrymx*m_dynust_network_arc_de(i)%xl*xminPerSimInt/60.0))
!	 ELSE
!       limentr=((entrymx*m_dynust_network_arc_de(i)%xl*xminPerSimInt/60.0)*max(0.0,sg2))
!	 ENDIF
!     r4 = ranxy(25) 
!	 IF(r4 <= (limentr-ifix(limentr))) limentr = limentr + 1
!
!     mnum=max(0,min(nint(genm),m_dynust_network_arc_de(i)%entryqueue,nint(limentr)))
!   ELSE ! loading on freeway (external loading should not be constrained as above)
     slimentr=m_dynust_network_arc_de(i)%MaxFlowRate*6.0
     mnum=max(1,min(nint(genm),m_dynust_network_arc_de(i)%entryqueue,nint(slimentr)))
!   ENDIF
   
   IF(mnum < 0) THEN
      WRITE(911,'("Error, oversaturation on generation link",i7,"->",i7)') m_dynust_network_node_nde(m_dynust_network_arc_nde(i)%idnod)%IntoOutNodeNum,m_dynust_network_node_nde(m_dynust_network_arc_nde(i)%iunod)%IntoOutNodeNum
      STOP
   ENDIF

   m_dynust_network_arc_de(i)%link_entry_time = 0

!-----------------------------------------------
! Start loading vehicles from the entry queue
   IF(mnum > 0.and.EntQueVehList(i)%fid > 0) THEN

      ips1 = EntQueVehList(i)%fid
      DO isee = 1, ips1
        IF(nint(m_dynust_last_stand(EntQueVehList(i)%P(isee))%stime/xminPerSimInt) > nint(t/xminPerSimInt)) exit 
      ENDDO
      isee = isee - 1
      ips2 = min(mnum,ips1,isee) 
      DO ips3 = 1, ips2

	    j1=EntQueVehList(i)%P(ips3) ! always take from the head

		IF(j1 < 1) THEN 
		  WRITE(911,*) 'error in finding j1'
		  WRITE(911,*) 'please contact developers to resolve this issue'
		  STOP
		ENDIF

        CALL linkVehList_insert(i,j1,m_dynust_veh(j1)%position) !LST
        m_dynust_network_arc_de(i)%npar=m_dynust_network_arc_de(i)%npar+1
		m_dynust_network_arc_de(i)%volume=m_dynust_network_arc_de(i)%volume+1
        m_dynust_network_arc_de(i)%pcetotal=m_dynust_network_arc_de(i)%pcetotal+m_dynust_veh(j1)%vhpce/AttScale
        m_dynust_veh(j1)%inpar = t

        IF(m_dynust_last_stand(j1)%vehtype == 2.or.m_dynust_last_stand(j1)%vehtype == 5.or.m_dynust_last_stand(j1)%vehtype == 7) THEN
		  m_dynust_network_arc_de(i)%nTruck=m_dynust_network_arc_de(i)%nTruck+1
        ENDIF

        m_dynust_network_arc_de(i)%link_entry_time=m_dynust_network_arc_de(i)%link_entry_time+(t-m_dynust_last_stand(j1)%stime)
     ENDDO

	 CALL entryqueue_remove(i,ips3-1)
	 m_dynust_network_arc_de(i)%link_entry_time=m_dynust_network_arc_de(i)%link_entry_time/(ips3-1)
     m_dynust_network_arc_de(i)%entryqueue=m_dynust_network_arc_de(i)%entryqueue-(ips3-1)
   ENDIF
   
!   IF(NoofBuses > 0) THEN
!     IF(iteration == 0) THEN
!       IF(TotalBusGen < NoofBuses) CALL TRANSIT_GENERATION(i,t)
!     ENDIF
!   ENDIF
ENDIF ! screen for link_iden
7 CONTINUE

!$OMP END DO
!$OMP END PARALLEL

IF(ALLOCATED(AinTmp)) DEALLOCATE(AinTmp)


END SUBROUTINE