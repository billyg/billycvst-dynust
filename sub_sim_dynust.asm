; mark_description "Intel(R) Visual Fortran Compiler XE for applications running on IA-32, Version 13.1.1.171 Build 20130313";
; mark_description "-S";
	.686P
 	.387
	OPTION DOTNAME
	ASSUME	CS:FLAT,DS:FLAT,SS:FLAT
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
TXTST0:
; -- Begin  _SIM_DYNUST
; mark_begin;
IF @Version GE 800
  .MMX
ELSEIF @Version GE 612
  .MMX
  MMWORD TEXTEQU <QWORD>
ENDIF
IF @Version GE 800
  .XMM
ELSEIF @Version GE 614
  .XMM
  XMMWORD TEXTEQU <OWORD>
ENDIF
       ALIGN     16
	PUBLIC _SIM_DYNUST
_SIM_DYNUST	PROC NEAR 
; parameter 1: 8 + ebp
.B1.1:                          ; Preds .B1.0
        push      ebp                                           ;1.18
        mov       ebp, esp                                      ;1.18
        and       esp, -16                                      ;1.18
        push      esi                                           ;1.18
        push      edi                                           ;1.18
        push      ebx                                           ;1.18
        sub       esp, 212                                      ;1.18
        call      _OPENFILE                                     ;42.12
                                ; LOE
.B1.2:                          ; Preds .B1.1
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_LOGOUT], 0  ;44.17
        jle       .B1.93        ; Prob 16%                      ;44.17
                                ; LOE
.B1.3:                          ; Preds .B1.2
        mov       DWORD PTR [16+esp], 0                         ;44.22
        lea       esi, DWORD PTR [16+esp]                       ;44.22
        mov       DWORD PTR [136+esp], 16                       ;44.22
        lea       eax, DWORD PTR [136+esp]                      ;44.22
        mov       DWORD PTR [140+esp], OFFSET FLAT: __STRLITPACK_32 ;44.22
        push      32                                            ;44.22
        push      eax                                           ;44.22
        push      OFFSET FLAT: __STRLITPACK_34.0.1              ;44.22
        push      -2088435968                                   ;44.22
        push      711                                           ;44.22
        push      esi                                           ;44.22
        call      _for_write_seq_lis                            ;44.22
                                ; LOE esi
.B1.77:                         ; Preds .B1.3
        add       esp, 24                                       ;44.22
                                ; LOE esi
.B1.4:                          ; Preds .B1.77 .B1.93
        call      _INITIALIZE                                   ;45.12
                                ; LOE esi
.B1.5:                          ; Preds .B1.4
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_LOGOUT], 0  ;47.17
        jle       .B1.7         ; Prob 16%                      ;47.17
                                ; LOE esi
.B1.6:                          ; Preds .B1.5
        mov       DWORD PTR [16+esp], 0                         ;47.22
        lea       eax, DWORD PTR [144+esp]                      ;47.22
        mov       DWORD PTR [144+esp], 17                       ;47.22
        mov       DWORD PTR [148+esp], OFFSET FLAT: __STRLITPACK_30 ;47.22
        push      32                                            ;47.22
        push      eax                                           ;47.22
        push      OFFSET FLAT: __STRLITPACK_35.0.1              ;47.22
        push      -2088435968                                   ;47.22
        push      711                                           ;47.22
        push      esi                                           ;47.22
        call      _for_write_seq_lis                            ;47.22
                                ; LOE esi
.B1.78:                         ; Preds .B1.6
        add       esp, 24                                       ;47.22
                                ; LOE esi
.B1.7:                          ; Preds .B1.78 .B1.5
        call      _READ_INPUT                                   ;49.12
                                ; LOE esi
.B1.8:                          ; Preds .B1.7
        movss     xmm2, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIMPERIOD] ;53.11
        movss     xmm1, DWORD PTR [_2il0floatpacket.1]          ;53.11
        andps     xmm1, xmm2                                    ;53.11
        movss     DWORD PTR [52+esp], xmm2                      ;53.11
        pxor      xmm2, xmm1                                    ;53.11
        movss     xmm3, DWORD PTR [_2il0floatpacket.2]          ;53.11
        movaps    xmm4, xmm2                                    ;53.11
        movaps    xmm0, xmm2                                    ;53.11
        cmpltss   xmm4, xmm3                                    ;53.11
        andps     xmm4, xmm3                                    ;53.11
        mov       ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIMPERIOD] ;53.11
        addss     xmm0, xmm4                                    ;53.11
        mov       DWORD PTR [60+esp], ebx                       ;53.11
        subss     xmm0, xmm4                                    ;53.11
        movaps    xmm7, xmm0                                    ;53.11
        mov       ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_EPOCNUM] ;53.11
        subss     xmm7, xmm2                                    ;53.11
        movss     xmm2, DWORD PTR [_2il0floatpacket.3]          ;53.11
        movaps    xmm5, xmm7                                    ;53.11
        movaps    xmm6, xmm2                                    ;53.11
        cmpnless  xmm5, xmm2                                    ;53.11
        addss     xmm6, xmm2                                    ;53.11
        cmpless   xmm7, DWORD PTR [_2il0floatpacket.4]          ;53.11
        andps     xmm5, xmm6                                    ;53.11
        andps     xmm7, xmm6                                    ;53.11
        subss     xmm0, xmm5                                    ;53.11
        addss     xmm0, xmm7                                    ;53.11
        orps      xmm0, xmm1                                    ;53.11
        cvtss2si  edi, xmm0                                     ;53.11
        mov       eax, edi                                      ;53.7
        cdq                                                     ;53.7
        idiv      ebx                                           ;53.7
        test      edx, edx                                      ;53.36
        jne       .B1.72        ; Prob 5%                       ;53.36
                                ; LOE ebx esi edi xmm2 xmm3
.B1.9:                          ; Preds .B1.8 .B1.91
        mov       eax, edi                                      ;60.14
        cdq                                                     ;60.14
        idiv      ebx                                           ;60.14
        cvtsi2ss  xmm6, eax                                     ;60.14
        movss     xmm4, DWORD PTR [_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT] ;60.34
        divss     xmm6, xmm4                                    ;60.14
        movss     xmm1, DWORD PTR [_2il0floatpacket.1]          ;60.14
        andps     xmm1, xmm6                                    ;60.14
        pxor      xmm6, xmm1                                    ;60.14
        movaps    xmm5, xmm6                                    ;60.14
        movaps    xmm0, xmm6                                    ;60.14
        movss     DWORD PTR [48+esp], xmm4                      ;60.34
        cmpltss   xmm5, xmm3                                    ;60.14
        andps     xmm5, xmm3                                    ;60.14
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIMPERAGG] ;60.14
        addss     xmm0, xmm5                                    ;60.14
        subss     xmm0, xmm5                                    ;60.14
        movaps    xmm4, xmm0                                    ;60.14
        subss     xmm4, xmm6                                    ;60.14
        movaps    xmm6, xmm2                                    ;60.14
        addss     xmm6, xmm2                                    ;60.14
        movaps    xmm7, xmm4                                    ;60.14
        cmpless   xmm4, DWORD PTR [_2il0floatpacket.4]          ;60.14
        cmpnless  xmm7, xmm2                                    ;60.14
        andps     xmm7, xmm6                                    ;60.14
        andps     xmm4, xmm6                                    ;60.14
        subss     xmm0, xmm7                                    ;60.14
        addss     xmm0, xmm4                                    ;60.14
        orps      xmm0, xmm1                                    ;60.14
        cvtss2si  eax, xmm0                                     ;60.14
        cdq                                                     ;60.10
        mov       DWORD PTR [56+esp], eax                       ;60.14
        idiv      edi                                           ;60.10
        test      edx, edx                                      ;60.69
        je        .B1.22        ; Prob 50%                      ;60.69
                                ; LOE ebx esi edi xmm2 xmm3
.B1.10:                         ; Preds .B1.9
        mov       DWORD PTR [16+esp], 0                         ;61.9
        lea       eax, DWORD PTR [80+esp]                       ;61.9
        mov       DWORD PTR [80+esp], 72                        ;61.9
        mov       DWORD PTR [84+esp], OFFSET FLAT: __STRLITPACK_24 ;61.9
        push      32                                            ;61.9
        push      eax                                           ;61.9
        push      OFFSET FLAT: __STRLITPACK_39.0.1              ;61.9
        push      -2088435968                                   ;61.9
        push      911                                           ;61.9
        push      esi                                           ;61.9
        call      _for_write_seq_lis                            ;61.9
                                ; LOE ebx esi edi
.B1.11:                         ; Preds .B1.10
        mov       DWORD PTR [40+esp], 0                         ;62.6
        lea       eax, DWORD PTR [112+esp]                      ;62.6
        mov       DWORD PTR [112+esp], 71                       ;62.6
        mov       DWORD PTR [116+esp], OFFSET FLAT: __STRLITPACK_22 ;62.6
        push      32                                            ;62.6
        push      eax                                           ;62.6
        push      OFFSET FLAT: __STRLITPACK_40.0.1              ;62.6
        push      -2088435968                                   ;62.6
        push      911                                           ;62.6
        push      esi                                           ;62.6
        call      _for_write_seq_lis                            ;62.6
                                ; LOE ebx esi edi
.B1.12:                         ; Preds .B1.11
        mov       DWORD PTR [64+esp], 0                         ;63.6
        lea       eax, DWORD PTR [144+esp]                      ;63.6
        mov       DWORD PTR [144+esp], 50                       ;63.6
        mov       DWORD PTR [148+esp], OFFSET FLAT: __STRLITPACK_20 ;63.6
        push      32                                            ;63.6
        push      eax                                           ;63.6
        push      OFFSET FLAT: __STRLITPACK_41.0.1              ;63.6
        push      -2088435968                                   ;63.6
        push      911                                           ;63.6
        push      esi                                           ;63.6
        call      _for_write_seq_lis                            ;63.6
                                ; LOE ebx esi edi
.B1.13:                         ; Preds .B1.12
        mov       DWORD PTR [88+esp], 0                         ;64.3
        lea       eax, DWORD PTR [176+esp]                      ;64.3
        mov       DWORD PTR [176+esp], 12                       ;64.3
        mov       DWORD PTR [180+esp], OFFSET FLAT: __STRLITPACK_18 ;64.3
        push      32                                            ;64.3
        push      eax                                           ;64.3
        push      OFFSET FLAT: __STRLITPACK_42.0.1              ;64.3
        push      -2088435968                                   ;64.3
        push      911                                           ;64.3
        push      esi                                           ;64.3
        call      _for_write_seq_lis                            ;64.3
                                ; LOE ebx esi edi
.B1.14:                         ; Preds .B1.13
        mov       eax, DWORD PTR [156+esp]                      ;64.3
        lea       edx, DWORD PTR [272+esp]                      ;64.3
        mov       DWORD PTR [272+esp], eax                      ;64.3
        push      edx                                           ;64.3
        push      OFFSET FLAT: __STRLITPACK_43.0.1              ;64.3
        push      esi                                           ;64.3
        call      _for_write_seq_lis_xmit                       ;64.3
                                ; LOE ebx esi edi
.B1.79:                         ; Preds .B1.14
        add       esp, 108                                      ;64.3
                                ; LOE ebx esi edi
.B1.15:                         ; Preds .B1.79
        mov       DWORD PTR [16+esp], 0                         ;65.3
        lea       eax, DWORD PTR [112+esp]                      ;65.3
        mov       DWORD PTR [112+esp], 14                       ;65.3
        mov       DWORD PTR [116+esp], OFFSET FLAT: __STRLITPACK_16 ;65.3
        push      32                                            ;65.3
        push      eax                                           ;65.3
        push      OFFSET FLAT: __STRLITPACK_44.0.1              ;65.3
        push      -2088435968                                   ;65.3
        push      911                                           ;65.3
        push      esi                                           ;65.3
        call      _for_write_seq_lis                            ;65.3
                                ; LOE ebx esi edi
.B1.16:                         ; Preds .B1.15
        mov       DWORD PTR [208+esp], ebx                      ;65.3
        lea       eax, DWORD PTR [208+esp]                      ;65.3
        push      eax                                           ;65.3
        push      OFFSET FLAT: __STRLITPACK_45.0.1              ;65.3
        push      esi                                           ;65.3
        call      _for_write_seq_lis_xmit                       ;65.3
                                ; LOE ebx esi edi
.B1.17:                         ; Preds .B1.16
        mov       DWORD PTR [52+esp], 0                         ;66.3
        lea       eax, DWORD PTR [156+esp]                      ;66.3
        mov       DWORD PTR [156+esp], 23                       ;66.3
        mov       DWORD PTR [160+esp], OFFSET FLAT: __STRLITPACK_14 ;66.3
        push      32                                            ;66.3
        push      eax                                           ;66.3
        push      OFFSET FLAT: __STRLITPACK_46.0.1              ;66.3
        push      -2088435968                                   ;66.3
        push      911                                           ;66.3
        push      esi                                           ;66.3
        call      _for_write_seq_lis                            ;66.3
                                ; LOE ebx esi edi
.B1.18:                         ; Preds .B1.17
        mov       DWORD PTR [252+esp], edi                      ;66.3
        lea       eax, DWORD PTR [252+esp]                      ;66.3
        push      eax                                           ;66.3
        push      OFFSET FLAT: __STRLITPACK_47.0.1              ;66.3
        push      esi                                           ;66.3
        call      _for_write_seq_lis_xmit                       ;66.3
                                ; LOE ebx esi edi
.B1.19:                         ; Preds .B1.18
        mov       DWORD PTR [88+esp], 0                         ;67.9
        lea       eax, DWORD PTR [200+esp]                      ;67.9
        mov       DWORD PTR [200+esp], 46                       ;67.9
        mov       DWORD PTR [204+esp], OFFSET FLAT: __STRLITPACK_12 ;67.9
        push      32                                            ;67.9
        push      eax                                           ;67.9
        push      OFFSET FLAT: __STRLITPACK_48.0.1              ;67.9
        push      -2088435968                                   ;67.9
        push      911                                           ;67.9
        push      esi                                           ;67.9
        call      _for_write_seq_lis                            ;67.9
                                ; LOE ebx esi edi
.B1.20:                         ; Preds .B1.19
        mov       eax, DWORD PTR [152+esp]                      ;67.9
        lea       edx, DWORD PTR [296+esp]                      ;67.9
        mov       DWORD PTR [296+esp], eax                      ;67.9
        push      edx                                           ;67.9
        push      OFFSET FLAT: __STRLITPACK_49.0.1              ;67.9
        push      esi                                           ;67.9
        call      _for_write_seq_lis_xmit                       ;67.9
                                ; LOE ebx esi edi
.B1.80:                         ; Preds .B1.20
        add       esp, 108                                      ;67.9
                                ; LOE ebx esi edi
.B1.21:                         ; Preds .B1.80
        push      32                                            ;68.6
        xor       eax, eax                                      ;68.6
        push      eax                                           ;68.6
        push      eax                                           ;68.6
        push      -2088435968                                   ;68.6
        push      eax                                           ;68.6
        push      OFFSET FLAT: __STRLITPACK_50                  ;68.6
        call      _for_stop_core                                ;68.6
                                ; LOE ebx esi edi
.B1.81:                         ; Preds .B1.21
        movss     xmm3, DWORD PTR [_2il0floatpacket.2]          ;68.6
        add       esp, 24                                       ;68.6
        movss     xmm2, DWORD PTR [_2il0floatpacket.3]          ;68.6
                                ; LOE ebx esi edi xmm2 xmm3
.B1.22:                         ; Preds .B1.81 .B1.9
        cvtsi2ss  xmm4, ebx                                     ;72.34
        cvtsi2ss  xmm6, edi                                     ;73.14
        movss     xmm5, DWORD PTR [52+esp]                      ;72.19
        divss     xmm5, xmm4                                    ;72.19
        cvttss2si eax, xmm5                                     ;72.6
        cvtsi2ss  xmm0, eax                                     ;73.14
        divss     xmm0, DWORD PTR [48+esp]                      ;73.14
        divss     xmm0, xmm6                                    ;73.14
        movss     xmm1, DWORD PTR [_2il0floatpacket.1]          ;73.14
        movaps    xmm4, xmm2                                    ;73.14
        andps     xmm1, xmm0                                    ;73.14
        addss     xmm4, xmm2                                    ;73.14
        pxor      xmm0, xmm1                                    ;73.14
        movaps    xmm7, xmm0                                    ;73.14
        movaps    xmm6, xmm0                                    ;73.14
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_AGGINT] ;75.4
        cmp       edi, ebx                                      ;75.14
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_EPOCPERIOD], eax ;72.6
        cmpltss   xmm7, xmm3                                    ;73.14
        andps     xmm3, xmm7                                    ;73.14
        addss     xmm6, xmm3                                    ;73.14
        subss     xmm6, xmm3                                    ;73.14
        movaps    xmm5, xmm6                                    ;73.14
        subss     xmm5, xmm0                                    ;73.14
        movaps    xmm0, xmm5                                    ;73.14
        cmpless   xmm5, DWORD PTR [_2il0floatpacket.4]          ;73.14
        cmpnless  xmm0, xmm2                                    ;73.14
        andps     xmm0, xmm4                                    ;73.14
        andps     xmm5, xmm4                                    ;73.14
        subss     xmm6, xmm0                                    ;73.14
        addss     xmm6, xmm5                                    ;73.14
        orps      xmm6, xmm1                                    ;73.14
        cvtss2si  edx, xmm6                                     ;73.3
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ITI_NUEP], edx ;73.3
        jge       .B1.31        ; Prob 50%                      ;75.14
                                ; LOE ebx esi edi
.B1.23:                         ; Preds .B1.22
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ITEMAX], 0  ;75.35
        jle       .B1.31        ; Prob 16%                      ;75.35
                                ; LOE ebx esi edi
.B1.24:                         ; Preds .B1.23
        mov       DWORD PTR [16+esp], 0                         ;76.11
        lea       eax, DWORD PTR [48+esp]                       ;76.11
        mov       DWORD PTR [48+esp], 57                        ;76.11
        mov       DWORD PTR [52+esp], OFFSET FLAT: __STRLITPACK_10 ;76.11
        push      32                                            ;76.11
        push      eax                                           ;76.11
        push      OFFSET FLAT: __STRLITPACK_51.0.1              ;76.11
        push      -2088435968                                   ;76.11
        push      911                                           ;76.11
        push      esi                                           ;76.11
        call      _for_write_seq_lis                            ;76.11
                                ; LOE ebx esi edi
.B1.25:                         ; Preds .B1.24
        mov       DWORD PTR [40+esp], 0                         ;77.5
        lea       eax, DWORD PTR [80+esp]                       ;77.5
        mov       DWORD PTR [80+esp], 62                        ;77.5
        mov       DWORD PTR [84+esp], OFFSET FLAT: __STRLITPACK_8 ;77.5
        push      32                                            ;77.5
        push      eax                                           ;77.5
        push      OFFSET FLAT: __STRLITPACK_52.0.1              ;77.5
        push      -2088435968                                   ;77.5
        push      911                                           ;77.5
        push      esi                                           ;77.5
        call      _for_write_seq_lis                            ;77.5
                                ; LOE ebx esi edi
.B1.26:                         ; Preds .B1.25
        mov       DWORD PTR [64+esp], 0                         ;78.5
        lea       eax, DWORD PTR [112+esp]                      ;78.5
        mov       DWORD PTR [112+esp], 15                       ;78.5
        mov       DWORD PTR [116+esp], OFFSET FLAT: __STRLITPACK_6 ;78.5
        push      32                                            ;78.5
        push      eax                                           ;78.5
        push      OFFSET FLAT: __STRLITPACK_53.0.1              ;78.5
        push      -2088435968                                   ;78.5
        push      911                                           ;78.5
        push      esi                                           ;78.5
        call      _for_write_seq_lis                            ;78.5
                                ; LOE ebx esi edi
.B1.27:                         ; Preds .B1.26
        mov       DWORD PTR [224+esp], ebx                      ;78.5
        lea       eax, DWORD PTR [224+esp]                      ;78.5
        push      eax                                           ;78.5
        push      OFFSET FLAT: __STRLITPACK_54.0.1              ;78.5
        push      esi                                           ;78.5
        call      _for_write_seq_lis_xmit                       ;78.5
                                ; LOE esi edi
.B1.28:                         ; Preds .B1.27
        mov       DWORD PTR [100+esp], 0                        ;79.5
        lea       eax, DWORD PTR [156+esp]                      ;79.5
        mov       DWORD PTR [156+esp], 29                       ;79.5
        mov       DWORD PTR [160+esp], OFFSET FLAT: __STRLITPACK_4 ;79.5
        push      32                                            ;79.5
        push      eax                                           ;79.5
        push      OFFSET FLAT: __STRLITPACK_55.0.1              ;79.5
        push      -2088435968                                   ;79.5
        push      911                                           ;79.5
        push      esi                                           ;79.5
        call      _for_write_seq_lis                            ;79.5
                                ; LOE esi edi
.B1.29:                         ; Preds .B1.28
        mov       DWORD PTR [268+esp], edi                      ;79.5
        lea       eax, DWORD PTR [268+esp]                      ;79.5
        push      eax                                           ;79.5
        push      OFFSET FLAT: __STRLITPACK_56.0.1              ;79.5
        push      esi                                           ;79.5
        call      _for_write_seq_lis_xmit                       ;79.5
                                ; LOE esi
.B1.82:                         ; Preds .B1.29
        add       esp, 120                                      ;79.5
                                ; LOE esi
.B1.30:                         ; Preds .B1.82
        push      32                                            ;80.11
        xor       eax, eax                                      ;80.11
        push      eax                                           ;80.11
        push      eax                                           ;80.11
        push      -2088435968                                   ;80.11
        push      eax                                           ;80.11
        push      OFFSET FLAT: __STRLITPACK_57                  ;80.11
        call      _for_stop_core                                ;80.11
                                ; LOE esi
.B1.83:                         ; Preds .B1.30
        add       esp, 24                                       ;80.11
                                ; LOE esi
.B1.31:                         ; Preds .B1.83 .B1.23 .B1.22
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ISO_OK], 1  ;87.17
        je        .B1.33        ; Prob 16%                      ;87.17
                                ; LOE esi
.B1.32:                         ; Preds .B1.31
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_IUE_OK], 1  ;87.32
        jne       .B1.34        ; Prob 84%                      ;87.32
                                ; LOE esi
.B1.33:                         ; Preds .B1.32 .B1.31
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_TIME_NOW], 0 ;87.38
                                ; LOE esi
.B1.34:                         ; Preds .B1.32 .B1.33
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_RUNMODE], 1 ;89.15
        je        .B1.38        ; Prob 16%                      ;89.15
                                ; LOE esi
.B1.35:                         ; Preds .B1.34
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ITERATION], 0 ;89.34
        jne       .B1.38        ; Prob 50%                      ;89.34
                                ; LOE esi
.B1.36:                         ; Preds .B1.35
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ITEMAX], 0  ;89.50
        jle       .B1.38        ; Prob 41%                      ;89.50
                                ; LOE esi
.B1.37:                         ; Preds .B1.36
        call      _CHECK_MIVA_VEHICLE                           ;90.10
                                ; LOE esi
.B1.38:                         ; Preds .B1.37 .B1.36 .B1.35 .B1.34
        call      _PRINTHEADER1                                 ;93.12
                                ; LOE esi
.B1.39:                         ; Preds .B1.38
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_LOGOUT], 0  ;95.17
        jle       .B1.41        ; Prob 16%                      ;95.17
                                ; LOE esi
.B1.40:                         ; Preds .B1.39
        mov       DWORD PTR [16+esp], 0                         ;95.22
        lea       eax, DWORD PTR [168+esp]                      ;95.22
        mov       DWORD PTR [168+esp], 9                        ;95.22
        mov       DWORD PTR [172+esp], OFFSET FLAT: __STRLITPACK_2 ;95.22
        push      32                                            ;95.22
        push      eax                                           ;95.22
        push      OFFSET FLAT: __STRLITPACK_58.0.1              ;95.22
        push      -2088435968                                   ;95.22
        push      711                                           ;95.22
        push      esi                                           ;95.22
        call      _for_write_seq_lis                            ;95.22
                                ; LOE esi
.B1.84:                         ; Preds .B1.40
        add       esp, 24                                       ;95.22
                                ; LOE esi
.B1.41:                         ; Preds .B1.84 .B1.39
        mov       ebx, DWORD PTR [8+ebp]                        ;1.18
        push      ebx                                           ;99.12
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ICALLKSP], 0 ;97.7
        call      _SIM_MAIN_LOOP                                ;99.12
                                ; LOE ebx esi
.B1.85:                         ; Preds .B1.41
        add       esp, 4                                        ;99.12
                                ; LOE ebx esi
.B1.42:                         ; Preds .B1.85
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_RUNMODE] ;102.7
        mov       eax, edx                                      ;102.22
        and       eax, -3                                       ;102.22
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_JUSTVEH] ;101.7
        cmp       eax, 1                                        ;102.22
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_TOTALVEH], ecx ;101.7
        je        .B1.44        ; Prob 16%                      ;102.22
                                ; LOE edx ecx ebx esi
.B1.43:                         ; Preds .B1.42
        cmp       edx, 4                                        ;102.50
        jne       .B1.45        ; Prob 84%                      ;102.50
                                ; LOE ecx ebx esi
.B1.44:                         ; Preds .B1.43 .B1.42
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_MAXID], ecx ;102.56
                                ; LOE ebx esi
.B1.45:                         ; Preds .B1.43 .B1.44
        xor       eax, eax                                      ;104.13
        mov       DWORD PTR [16+esp], eax                       ;104.13
        push      32                                            ;104.13
        push      eax                                           ;104.13
        push      OFFSET FLAT: __STRLITPACK_59.0.1              ;104.13
        push      -2088435968                                   ;104.13
        push      97                                            ;104.13
        push      esi                                           ;104.13
        call      _for_close                                    ;104.13
                                ; LOE ebx esi
.B1.86:                         ; Preds .B1.45
        add       esp, 24                                       ;104.13
                                ; LOE ebx esi
.B1.46:                         ; Preds .B1.86
        call      _RESORT_OUTPUT_VEH                            ;105.12
                                ; LOE ebx esi
.B1.47:                         ; Preds .B1.46
        call      _PRINTHEADER2                                 ;107.12
                                ; LOE ebx esi
.B1.48:                         ; Preds .B1.47
        push      ebx                                           ;109.12
        call      _WRITE_SUMMARY_STAT                           ;109.12
                                ; LOE ebx esi
.B1.87:                         ; Preds .B1.48
        add       esp, 4                                        ;109.12
                                ; LOE ebx esi
.B1.49:                         ; Preds .B1.87
        call      _CAL_PENALTY_MIVA                             ;111.12
                                ; LOE ebx esi
.B1.50:                         ; Preds .B1.49
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ITEMAX] ;113.7
        test      eax, eax                                      ;113.17
        je        .B1.54        ; Prob 50%                      ;113.17
                                ; LOE eax ebx esi
.B1.51:                         ; Preds .B1.50
        jle       .B1.56        ; Prob 16%                      ;113.33
                                ; LOE eax ebx esi
.B1.52:                         ; Preds .B1.51
        test      BYTE PTR [_DYNUST_MAIN_MODULE_mp_REACH_CONVERG], 1 ;113.42
        jne       .B1.54        ; Prob 40%                      ;113.42
                                ; LOE eax ebx esi
.B1.53:                         ; Preds .B1.52
        cmp       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ITERATION] ;113.69
        jne       .B1.56        ; Prob 50%                      ;113.69
                                ; LOE ebx esi
.B1.54:                         ; Preds .B1.53 .B1.52 .B1.50
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_UETIMEFLAGW], 0 ;115.23
        jle       .B1.56        ; Prob 41%                      ;115.23
                                ; LOE ebx esi
.B1.55:                         ; Preds .B1.54
        call      _WRITEUETRAVELTIME                            ;115.28
                                ; LOE ebx esi
.B1.56:                         ; Preds .B1.55 .B1.54 .B1.53 .B1.51
        push      ebx                                           ;118.12
        call      _WRITE_TOLL_REVENUE                           ;118.12
                                ; LOE esi
.B1.88:                         ; Preds .B1.56
        add       esp, 4                                        ;118.12
                                ; LOE esi
.B1.57:                         ; Preds .B1.88
        cmp       DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_NSEG], 0 ;121.15
        jle       .B1.64        ; Prob 41%                      ;121.15
                                ; LOE esi
.B1.58:                         ; Preds .B1.57
        call      _DYNUST_CONGESTIONPRICING_MODULE_mp_PRICECAL  ;122.14
                                ; LOE esi
.B1.59:                         ; Preds .B1.58
        call      _DYNUST_CONGESTIONPRICING_MODULE_mp_PRICEOUTPUT ;123.14
                                ; LOE esi
.B1.60:                         ; Preds .B1.59
        xor       eax, eax                                      ;124.15
        mov       DWORD PTR [16+esp], eax                       ;124.15
        push      32                                            ;124.15
        push      eax                                           ;124.15
        push      OFFSET FLAT: __STRLITPACK_60.0.1              ;124.15
        push      -2088435968                                   ;124.15
        push      62                                            ;124.15
        push      esi                                           ;124.15
        call      _for_close                                    ;124.15
                                ; LOE esi
.B1.61:                         ; Preds .B1.60
        xor       eax, eax                                      ;125.15
        mov       DWORD PTR [40+esp], eax                       ;125.15
        push      32                                            ;125.15
        push      eax                                           ;125.15
        push      OFFSET FLAT: __STRLITPACK_61.0.1              ;125.15
        push      -2088435968                                   ;125.15
        push      7778                                          ;125.15
        push      esi                                           ;125.15
        call      _for_close                                    ;125.15
                                ; LOE esi
.B1.62:                         ; Preds .B1.61
        push      14                                            ;126.14
        push      OFFSET FLAT: __STRLITPACK_1                   ;126.14
        call      _SYSTEM                                       ;126.14
                                ; LOE esi
.B1.63:                         ; Preds .B1.62
        push      32                                            ;127.14
        push      OFFSET FLAT: __STRLITPACK_0                   ;127.14
        call      _SYSTEM                                       ;127.14
                                ; LOE esi
.B1.89:                         ; Preds .B1.63
        add       esp, 64                                       ;127.14
                                ; LOE esi
.B1.64:                         ; Preds .B1.89 .B1.57
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_FUELOUT], 1 ;130.18
        je        .B1.68        ; Prob 16%                      ;130.18
                                ; LOE esi
.B1.65:                         ; Preds .B1.70 .B1.71 .B1.64
        call      _MEMORY_MODULE_mp_DEALLOCATE_SIM              ;135.12
                                ; LOE esi
.B1.66:                         ; Preds .B1.65
        xor       eax, eax                                      ;138.13
        mov       DWORD PTR [16+esp], eax                       ;138.13
        push      32                                            ;138.13
        push      eax                                           ;138.13
        push      OFFSET FLAT: __STRLITPACK_62.0.1              ;138.13
        push      -2088435968                                   ;138.13
        push      6099                                          ;138.13
        push      esi                                           ;138.13
        call      _for_close                                    ;138.13
                                ; LOE
.B1.67:                         ; Preds .B1.66
        add       esp, 236                                      ;140.1
        pop       ebx                                           ;140.1
        pop       edi                                           ;140.1
        pop       esi                                           ;140.1
        mov       esp, ebp                                      ;140.1
        pop       ebp                                           ;140.1
        ret                                                     ;140.1
                                ; LOE
.B1.68:                         ; Preds .B1.64                  ; Infreq
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ITERATION] ;130.18
        cmp       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ITEMAX] ;130.38
        jne       .B1.71        ; Prob 50%                      ;130.38
                                ; LOE esi
.B1.69:                         ; Preds .B1.71 .B1.68           ; Infreq
        call      _DYNUST_FUEL_MODULE_mp_WRITEOUTFUELCONSUM     ;131.14
                                ; LOE esi
.B1.70:                         ; Preds .B1.69                  ; Infreq
        call      _DYNUST_FUEL_MODULE_mp_WRITEOUTFUELCONSUM_CITY ;132.14
        jmp       .B1.65        ; Prob 100%                     ;132.14
                                ; LOE esi
.B1.71:                         ; Preds .B1.68                  ; Infreq
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_REACH_CONVERG], -1 ;130.65
        je        .B1.69        ; Prob 5%                       ;130.65
        jmp       .B1.65        ; Prob 100%                     ;130.65
                                ; LOE esi
.B1.72:                         ; Preds .B1.8                   ; Infreq
        mov       DWORD PTR [16+esp], 0                         ;54.9
        lea       eax, DWORD PTR [esp]                          ;54.9
        mov       DWORD PTR [esp], 53                           ;54.9
        mov       DWORD PTR [4+esp], OFFSET FLAT: __STRLITPACK_28 ;54.9
        push      32                                            ;54.9
        push      eax                                           ;54.9
        push      OFFSET FLAT: __STRLITPACK_36.0.1              ;54.9
        push      -2088435968                                   ;54.9
        push      911                                           ;54.9
        push      esi                                           ;54.9
        call      _for_write_seq_lis                            ;54.9
                                ; LOE ebx esi edi
.B1.73:                         ; Preds .B1.72                  ; Infreq
        mov       DWORD PTR [40+esp], 0                         ;55.6
        lea       eax, DWORD PTR [32+esp]                       ;55.6
        mov       DWORD PTR [32+esp], 54                        ;55.6
        mov       DWORD PTR [36+esp], OFFSET FLAT: __STRLITPACK_26 ;55.6
        push      32                                            ;55.6
        push      eax                                           ;55.6
        push      OFFSET FLAT: __STRLITPACK_37.0.1              ;55.6
        push      -2088435968                                   ;55.6
        push      911                                           ;55.6
        push      esi                                           ;55.6
        call      _for_write_seq_lis                            ;55.6
                                ; LOE ebx esi edi
.B1.74:                         ; Preds .B1.73                  ; Infreq
        push      32                                            ;56.6
        xor       eax, eax                                      ;56.6
        push      eax                                           ;56.6
        push      eax                                           ;56.6
        push      -2088435968                                   ;56.6
        push      eax                                           ;56.6
        push      OFFSET FLAT: __STRLITPACK_38                  ;56.6
        call      _for_stop_core                                ;56.6
                                ; LOE ebx esi edi
.B1.91:                         ; Preds .B1.74                  ; Infreq
        movss     xmm3, DWORD PTR [_2il0floatpacket.2]          ;56.6
        add       esp, 72                                       ;56.6
        movss     xmm2, DWORD PTR [_2il0floatpacket.3]          ;56.6
        jmp       .B1.9         ; Prob 100%                     ;56.6
                                ; LOE ebx esi edi xmm2 xmm3
.B1.93:                         ; Preds .B1.2                   ; Infreq
        lea       esi, DWORD PTR [16+esp]                       ;44.22
        jmp       .B1.4         ; Prob 100%                     ;44.22
        ALIGN     16
                                ; LOE esi
; mark_end;
_SIM_DYNUST ENDP
_TEXT	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
__STRLITPACK_34.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_35.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_36.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_37.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_39.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_40.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_41.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_42.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_43.0.1	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_44.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_45.0.1	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_46.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_47.0.1	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_48.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_49.0.1	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_51.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_52.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_53.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_54.0.1	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_55.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_56.0.1	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_58.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_59.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_60.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_61.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_62.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _SIM_DYNUST
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _PRINTHEADER1
; mark_begin;
       ALIGN     16
	PUBLIC _PRINTHEADER1
_PRINTHEADER1	PROC NEAR 
.B2.1:                          ; Preds .B2.0
        push      ebp                                           ;143.12
        mov       ebp, esp                                      ;143.12
        and       esp, -16                                      ;143.12
        push      esi                                           ;143.12
        push      edi                                           ;143.12
        push      ebx                                           ;143.12
        sub       esp, 708                                      ;143.12
        call      _WRITE_TITLE                                  ;157.12
                                ; LOE
.B2.2:                          ; Preds .B2.1
        mov       DWORD PTR [352+esp], 0                        ;161.7
        lea       ebx, DWORD PTR [352+esp]                      ;161.7
        mov       DWORD PTR [32+esp], 1                         ;161.7
        lea       edx, DWORD PTR [32+esp]                       ;161.7
        mov       DWORD PTR [36+esp], OFFSET FLAT: __STRLITPACK_287 ;161.7
        push      32                                            ;161.7
        push      edx                                           ;161.7
        push      OFFSET FLAT: __STRLITPACK_289.0.2             ;161.7
        push      -2088435968                                   ;161.7
        push      666                                           ;161.7
        push      ebx                                           ;161.7
        call      _for_write_seq_lis                            ;161.7
                                ; LOE ebx
.B2.3:                          ; Preds .B2.2
        mov       DWORD PTR [376+esp], 0                        ;162.7
        lea       edx, DWORD PTR [64+esp]                       ;162.7
        mov       DWORD PTR [64+esp], 40                        ;162.7
        mov       DWORD PTR [68+esp], OFFSET FLAT: __STRLITPACK_285 ;162.7
        push      32                                            ;162.7
        push      edx                                           ;162.7
        push      OFFSET FLAT: __STRLITPACK_290.0.2             ;162.7
        push      -2088435968                                   ;162.7
        push      666                                           ;162.7
        push      ebx                                           ;162.7
        call      _for_write_seq_lis                            ;162.7
                                ; LOE ebx
.B2.4:                          ; Preds .B2.3
        mov       DWORD PTR [400+esp], 0                        ;163.7
        lea       edx, DWORD PTR [96+esp]                       ;163.7
        mov       DWORD PTR [96+esp], 40                        ;163.7
        mov       DWORD PTR [100+esp], OFFSET FLAT: __STRLITPACK_283 ;163.7
        push      32                                            ;163.7
        push      edx                                           ;163.7
        push      OFFSET FLAT: __STRLITPACK_291.0.2             ;163.7
        push      -2088435968                                   ;163.7
        push      666                                           ;163.7
        push      ebx                                           ;163.7
        call      _for_write_seq_lis                            ;163.7
                                ; LOE ebx
.B2.5:                          ; Preds .B2.4
        mov       DWORD PTR [424+esp], 0                        ;164.7
        lea       edx, DWORD PTR [128+esp]                      ;164.7
        mov       DWORD PTR [128+esp], 40                       ;164.7
        mov       DWORD PTR [132+esp], OFFSET FLAT: __STRLITPACK_281 ;164.7
        push      32                                            ;164.7
        push      edx                                           ;164.7
        push      OFFSET FLAT: __STRLITPACK_292.0.2             ;164.7
        push      -2088435968                                   ;164.7
        push      666                                           ;164.7
        push      ebx                                           ;164.7
        call      _for_write_seq_lis                            ;164.7
                                ; LOE ebx
.B2.6:                          ; Preds .B2.5
        xor       edx, edx                                      ;165.7
        mov       DWORD PTR [448+esp], edx                      ;165.7
        push      32                                            ;165.7
        push      edx                                           ;165.7
        push      OFFSET FLAT: __STRLITPACK_293.0.2             ;165.7
        push      -2088435968                                   ;165.7
        push      666                                           ;165.7
        push      ebx                                           ;165.7
        call      _for_write_seq_lis                            ;165.7
                                ; LOE ebx
.B2.167:                        ; Preds .B2.6
        add       esp, 120                                      ;165.7
                                ; LOE ebx
.B2.7:                          ; Preds .B2.167
        mov       DWORD PTR [352+esp], 0                        ;166.7
        lea       edx, DWORD PTR [64+esp]                       ;166.7
        mov       DWORD PTR [64+esp], 13                        ;166.7
        mov       DWORD PTR [68+esp], OFFSET FLAT: __STRLITPACK_279 ;166.7
        push      32                                            ;166.7
        push      edx                                           ;166.7
        push      OFFSET FLAT: __STRLITPACK_294.0.2             ;166.7
        push      -2088435968                                   ;166.7
        push      666                                           ;166.7
        push      ebx                                           ;166.7
        call      _for_write_seq_lis                            ;166.7
                                ; LOE ebx
.B2.8:                          ; Preds .B2.7
        mov       DWORD PTR [376+esp], 0                        ;167.7
        lea       edx, DWORD PTR [96+esp]                       ;167.7
        mov       DWORD PTR [96+esp], 13                        ;167.7
        mov       DWORD PTR [100+esp], OFFSET FLAT: __STRLITPACK_277 ;167.7
        push      32                                            ;167.7
        push      edx                                           ;167.7
        push      OFFSET FLAT: __STRLITPACK_295.0.2             ;167.7
        push      -2088435968                                   ;167.7
        push      666                                           ;167.7
        push      ebx                                           ;167.7
        call      _for_write_seq_lis                            ;167.7
                                ; LOE ebx
.B2.9:                          ; Preds .B2.8
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFNODES_ORG] ;168.7
        lea       ecx, DWORD PTR [536+esp]                      ;168.7
        mov       DWORD PTR [400+esp], 0                        ;168.7
        mov       DWORD PTR [536+esp], edx                      ;168.7
        push      32                                            ;168.7
        push      OFFSET FLAT: PRINTHEADER1$format_pack.0.2     ;168.7
        push      ecx                                           ;168.7
        push      OFFSET FLAT: __STRLITPACK_296.0.2             ;168.7
        push      -2088435968                                   ;168.7
        push      666                                           ;168.7
        push      ebx                                           ;168.7
        call      _for_write_seq_fmt                            ;168.7
                                ; LOE ebx
.B2.10:                         ; Preds .B2.9
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFARCS_ORG] ;169.7
        lea       ecx, DWORD PTR [572+esp]                      ;169.7
        mov       DWORD PTR [428+esp], 0                        ;169.7
        mov       DWORD PTR [572+esp], edx                      ;169.7
        push      32                                            ;169.7
        push      OFFSET FLAT: PRINTHEADER1$format_pack.0.2+72  ;169.7
        push      ecx                                           ;169.7
        push      OFFSET FLAT: __STRLITPACK_297.0.2             ;169.7
        push      -2088435968                                   ;169.7
        push      666                                           ;169.7
        push      ebx                                           ;169.7
        call      _for_write_seq_fmt                            ;169.7
                                ; LOE ebx
.B2.168:                        ; Preds .B2.10
        add       esp, 104                                      ;169.7
                                ; LOE ebx
.B2.11:                         ; Preds .B2.168
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NZONES] ;170.7
        lea       ecx, DWORD PTR [504+esp]                      ;170.7
        mov       DWORD PTR [352+esp], 0                        ;170.7
        mov       DWORD PTR [504+esp], edx                      ;170.7
        push      32                                            ;170.7
        push      OFFSET FLAT: PRINTHEADER1$format_pack.0.2+144 ;170.7
        push      ecx                                           ;170.7
        push      OFFSET FLAT: __STRLITPACK_298.0.2             ;170.7
        push      -2088435968                                   ;170.7
        push      666                                           ;170.7
        push      ebx                                           ;170.7
        call      _for_write_seq_fmt                            ;170.7
                                ; LOE ebx
.B2.12:                         ; Preds .B2.11
        mov       DWORD PTR [380+esp], 0                        ;171.7
        lea       edx, DWORD PTR [108+esp]                      ;171.7
        mov       DWORD PTR [108+esp], 39                       ;171.7
        mov       DWORD PTR [112+esp], OFFSET FLAT: __STRLITPACK_269 ;171.7
        push      32                                            ;171.7
        push      edx                                           ;171.7
        push      OFFSET FLAT: __STRLITPACK_299.0.2             ;171.7
        push      -2088435968                                   ;171.7
        push      666                                           ;171.7
        push      ebx                                           ;171.7
        call      _for_write_seq_lis                            ;171.7
                                ; LOE ebx
.B2.13:                         ; Preds .B2.12
        xor       edx, edx                                      ;172.7
        mov       DWORD PTR [404+esp], edx                      ;172.7
        push      32                                            ;172.7
        push      edx                                           ;172.7
        push      OFFSET FLAT: __STRLITPACK_300.0.2             ;172.7
        push      -2088435968                                   ;172.7
        push      666                                           ;172.7
        push      ebx                                           ;172.7
        call      _for_write_seq_lis                            ;172.7
                                ; LOE ebx
.B2.14:                         ; Preds .B2.13
        mov       DWORD PTR [428+esp], 0                        ;173.7
        lea       edx, DWORD PTR [164+esp]                      ;173.7
        mov       DWORD PTR [164+esp], 25                       ;173.7
        mov       DWORD PTR [168+esp], OFFSET FLAT: __STRLITPACK_267 ;173.7
        push      32                                            ;173.7
        push      edx                                           ;173.7
        push      OFFSET FLAT: __STRLITPACK_301.0.2             ;173.7
        push      -2088435968                                   ;173.7
        push      666                                           ;173.7
        push      ebx                                           ;173.7
        call      _for_write_seq_lis                            ;173.7
                                ; LOE ebx
.B2.15:                         ; Preds .B2.14
        mov       DWORD PTR [452+esp], 0                        ;174.7
        lea       edx, DWORD PTR [196+esp]                      ;174.7
        mov       DWORD PTR [196+esp], 25                       ;174.7
        mov       DWORD PTR [200+esp], OFFSET FLAT: __STRLITPACK_265 ;174.7
        push      32                                            ;174.7
        push      edx                                           ;174.7
        push      OFFSET FLAT: __STRLITPACK_302.0.2             ;174.7
        push      -2088435968                                   ;174.7
        push      666                                           ;174.7
        push      ebx                                           ;174.7
        call      _for_write_seq_lis                            ;174.7
                                ; LOE ebx
.B2.169:                        ; Preds .B2.15
        add       esp, 124                                      ;174.7
                                ; LOE ebx
.B2.16:                         ; Preds .B2.169
        xor       edi, edi                                      ;175.7
        pxor      xmm0, xmm0                                    ;175.7
        pxor      xmm3, xmm3                                    ;175.7
        pxor      xmm2, xmm2                                    ;175.7
        pxor      xmm1, xmm1                                    ;175.7
        xor       esi, esi                                      ;175.7
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFNODES] ;177.7
        xor       eax, eax                                      ;175.7
        movdqu    XMMWORD PTR [_DYNUST_MAIN_MODULE_mp_NODETMP], xmm0 ;175.7
        psrldq    xmm3, 12                                      ;175.7
        test      edx, edx                                      ;177.7
        psrldq    xmm2, 8                                       ;175.7
        psrldq    xmm1, 4                                       ;175.7
        jle       .B2.164       ; Prob 3%                       ;177.7
                                ; LOE eax edx ebx esi edi xmm1 xmm2 xmm3
.B2.17:                         ; Preds .B2.16
        imul      ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE+32], -84 ;
        pxor      xmm0, xmm0                                    ;179.65
        movd      DWORD PTR [20+esp], xmm0                      ;179.65
        movd      DWORD PTR [16+esp], xmm1                      ;179.65
        movd      DWORD PTR [12+esp], xmm2                      ;179.65
        mov       DWORD PTR [104+esp], edx                      ;
        xor       edx, edx                                      ;
        add       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE] ;
        mov       DWORD PTR [4+esp], edx                        ;
        mov       DWORD PTR [esp], edx                          ;
        mov       DWORD PTR [28+esp], ecx                       ;
        movd      DWORD PTR [8+esp], xmm3                       ;179.65
        mov       ebx, DWORD PTR [esp]                          ;
        mov       ecx, DWORD PTR [4+esp]                        ;
        mov       DWORD PTR [24+esp], edi                       ;
                                ; LOE eax ecx ebx esi
.B2.18:                         ; Preds .B2.29 .B2.17
        mov       edx, DWORD PTR [28+esp]                       ;179.15
        mov       edi, DWORD PTR [116+ebx+edx]                  ;179.15
        shl       edi, 2                                        ;179.48
        neg       edi                                           ;179.48
        add       edi, DWORD PTR [84+ebx+edx]                   ;179.48
        mov       edx, DWORD PTR [8+edi]                        ;179.15
        cmp       edx, 1                                        ;179.48
        jne       .B2.20        ; Prob 62%                      ;179.48
                                ; LOE eax edx ecx ebx esi
.B2.19:                         ; Preds .B2.18
        mov       edi, DWORD PTR [20+esp]                       ;179.54
        inc       edi                                           ;179.54
        mov       DWORD PTR [20+esp], edi                       ;179.54
        cmp       edx, 2                                        ;179.48
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_NODETMP], edi ;179.54
        je        .B2.21        ; Prob 38%                      ;179.48
        jmp       .B2.29        ; Prob 100%                     ;179.48
                                ; LOE eax edx ecx ebx esi
.B2.20:                         ; Preds .B2.18
        cmp       edx, 2                                        ;179.48
        jne       .B2.22        ; Prob 62%                      ;179.48
                                ; LOE eax edx ecx ebx esi
.B2.21:                         ; Preds .B2.19 .B2.20
        mov       edi, DWORD PTR [16+esp]                       ;179.54
        inc       edi                                           ;179.54
        mov       DWORD PTR [16+esp], edi                       ;179.54
        cmp       edx, 3                                        ;179.48
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_NODETMP+4], edi ;179.54
        je        .B2.23        ; Prob 38%                      ;179.48
        jmp       .B2.29        ; Prob 100%                     ;179.48
                                ; LOE eax edx ecx ebx esi
.B2.22:                         ; Preds .B2.20
        cmp       edx, 3                                        ;179.48
        jne       .B2.24        ; Prob 62%                      ;179.48
                                ; LOE eax edx ecx ebx esi
.B2.23:                         ; Preds .B2.21 .B2.22
        mov       edi, DWORD PTR [12+esp]                       ;179.54
        inc       edi                                           ;179.54
        mov       DWORD PTR [12+esp], edi                       ;179.54
        cmp       edx, 4                                        ;179.48
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_NODETMP+8], edi ;179.54
        je        .B2.25        ; Prob 38%                      ;179.48
        jmp       .B2.29        ; Prob 100%                     ;179.48
                                ; LOE eax edx ecx ebx esi
.B2.24:                         ; Preds .B2.22
        cmp       edx, 4                                        ;179.48
        jne       .B2.26        ; Prob 62%                      ;179.48
                                ; LOE eax edx ecx ebx esi
.B2.25:                         ; Preds .B2.23 .B2.24
        mov       edi, DWORD PTR [8+esp]                        ;179.54
        inc       edi                                           ;179.54
        mov       DWORD PTR [8+esp], edi                        ;179.54
        cmp       edx, 5                                        ;179.48
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_NODETMP+12], edi ;179.54
        je        .B2.27        ; Prob 38%                      ;179.48
        jmp       .B2.29        ; Prob 100%                     ;179.48
                                ; LOE eax ecx ebx esi
.B2.26:                         ; Preds .B2.24
        cmp       edx, 5                                        ;179.48
        jne       .B2.28        ; Prob 62%                      ;179.48
                                ; LOE eax edx ecx ebx esi
.B2.27:                         ; Preds .B2.25 .B2.26
        inc       DWORD PTR [24+esp]                            ;179.54
        jmp       .B2.29        ; Prob 100%                     ;179.54
                                ; LOE eax ecx ebx esi
.B2.28:                         ; Preds .B2.26
        cmp       edx, 6                                        ;179.54
        lea       edi, DWORD PTR [1+esi]                        ;179.54
        cmove     esi, edi                                      ;179.54
        cmove     eax, edi                                      ;179.54
                                ; LOE eax ecx ebx esi
.B2.29:                         ; Preds .B2.27 .B2.25 .B2.23 .B2.21 .B2.19
                                ;       .B2.28
        inc       ecx                                           ;177.7
        add       ebx, 84                                       ;177.7
        cmp       ecx, DWORD PTR [104+esp]                      ;177.7
        jb        .B2.18        ; Prob 82%                      ;177.7
                                ; LOE eax ecx ebx esi
.B2.30:                         ; Preds .B2.29
        mov       edi, DWORD PTR [24+esp]                       ;
        lea       ebx, DWORD PTR [352+esp]                      ;
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_NODETMP+20], eax ;179.54
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_NODETMP+16], edi ;179.54
                                ; LOE ebx esi edi
.B2.31:                         ; Preds .B2.30 .B2.164
        mov       edx, DWORD PTR [20+esp]                       ;183.7
        lea       ecx, DWORD PTR [512+esp]                      ;183.7
        mov       DWORD PTR [352+esp], 0                        ;183.7
        mov       DWORD PTR [512+esp], edx                      ;183.7
        push      32                                            ;183.7
        push      OFFSET FLAT: PRINTHEADER1$format_pack.0.2+216 ;183.7
        push      ecx                                           ;183.7
        push      OFFSET FLAT: __STRLITPACK_303.0.2             ;183.7
        push      -2088435968                                   ;183.7
        push      666                                           ;183.7
        push      ebx                                           ;183.7
        call      _for_write_seq_fmt                            ;183.7
                                ; LOE ebx esi edi
.B2.32:                         ; Preds .B2.31
        mov       edx, DWORD PTR [44+esp]                       ;184.7
        lea       ecx, DWORD PTR [548+esp]                      ;184.7
        mov       DWORD PTR [380+esp], 0                        ;184.7
        mov       DWORD PTR [548+esp], edx                      ;184.7
        push      32                                            ;184.7
        push      OFFSET FLAT: PRINTHEADER1$format_pack.0.2+288 ;184.7
        push      ecx                                           ;184.7
        push      OFFSET FLAT: __STRLITPACK_304.0.2             ;184.7
        push      -2088435968                                   ;184.7
        push      666                                           ;184.7
        push      ebx                                           ;184.7
        call      _for_write_seq_fmt                            ;184.7
                                ; LOE ebx esi edi
.B2.33:                         ; Preds .B2.32
        mov       edx, DWORD PTR [68+esp]                       ;185.7
        lea       ecx, DWORD PTR [584+esp]                      ;185.7
        mov       DWORD PTR [408+esp], 0                        ;185.7
        mov       DWORD PTR [584+esp], edx                      ;185.7
        push      32                                            ;185.7
        push      OFFSET FLAT: PRINTHEADER1$format_pack.0.2+360 ;185.7
        push      ecx                                           ;185.7
        push      OFFSET FLAT: __STRLITPACK_305.0.2             ;185.7
        push      -2088435968                                   ;185.7
        push      666                                           ;185.7
        push      ebx                                           ;185.7
        call      _for_write_seq_fmt                            ;185.7
                                ; LOE ebx esi edi
.B2.34:                         ; Preds .B2.33
        mov       DWORD PTR [436+esp], 0                        ;186.7
        lea       edx, DWORD PTR [620+esp]                      ;186.7
        mov       DWORD PTR [620+esp], esi                      ;186.7
        push      32                                            ;186.7
        push      OFFSET FLAT: PRINTHEADER1$format_pack.0.2+432 ;186.7
        push      edx                                           ;186.7
        push      OFFSET FLAT: __STRLITPACK_306.0.2             ;186.7
        push      -2088435968                                   ;186.7
        push      666                                           ;186.7
        push      ebx                                           ;186.7
        call      _for_write_seq_fmt                            ;186.7
                                ; LOE ebx edi
.B2.170:                        ; Preds .B2.34
        add       esp, 112                                      ;186.7
                                ; LOE ebx edi
.B2.35:                         ; Preds .B2.170
        mov       edx, DWORD PTR [8+esp]                        ;187.7
        lea       ecx, DWORD PTR [544+esp]                      ;187.7
        mov       DWORD PTR [352+esp], 0                        ;187.7
        mov       DWORD PTR [544+esp], edx                      ;187.7
        push      32                                            ;187.7
        push      OFFSET FLAT: PRINTHEADER1$format_pack.0.2+504 ;187.7
        push      ecx                                           ;187.7
        push      OFFSET FLAT: __STRLITPACK_307.0.2             ;187.7
        push      -2088435968                                   ;187.7
        push      666                                           ;187.7
        push      ebx                                           ;187.7
        call      _for_write_seq_fmt                            ;187.7
                                ; LOE ebx edi
.B2.36:                         ; Preds .B2.35
        mov       DWORD PTR [380+esp], 0                        ;188.7
        lea       edx, DWORD PTR [580+esp]                      ;188.7
        mov       DWORD PTR [580+esp], edi                      ;188.7
        push      32                                            ;188.7
        push      OFFSET FLAT: PRINTHEADER1$format_pack.0.2+576 ;188.7
        push      edx                                           ;188.7
        push      OFFSET FLAT: __STRLITPACK_308.0.2             ;188.7
        push      -2088435968                                   ;188.7
        push      666                                           ;188.7
        push      ebx                                           ;188.7
        call      _for_write_seq_fmt                            ;188.7
                                ; LOE ebx
.B2.37:                         ; Preds .B2.36
        mov       DWORD PTR [408+esp], 0                        ;189.7
        lea       edx, DWORD PTR [176+esp]                      ;189.7
        mov       DWORD PTR [176+esp], 39                       ;189.7
        mov       DWORD PTR [180+esp], OFFSET FLAT: __STRLITPACK_251 ;189.7
        push      32                                            ;189.7
        push      edx                                           ;189.7
        push      OFFSET FLAT: __STRLITPACK_309.0.2             ;189.7
        push      -2088435968                                   ;189.7
        push      666                                           ;189.7
        push      ebx                                           ;189.7
        call      _for_write_seq_lis                            ;189.7
                                ; LOE ebx
.B2.38:                         ; Preds .B2.37
        xor       edx, edx                                      ;190.7
        mov       DWORD PTR [432+esp], edx                      ;190.7
        push      32                                            ;190.7
        push      edx                                           ;190.7
        push      OFFSET FLAT: __STRLITPACK_310.0.2             ;190.7
        push      -2088435968                                   ;190.7
        push      666                                           ;190.7
        push      ebx                                           ;190.7
        call      _for_write_seq_lis                            ;190.7
                                ; LOE ebx
.B2.171:                        ; Preds .B2.38
        add       esp, 104                                      ;190.7
                                ; LOE ebx
.B2.39:                         ; Preds .B2.171
        mov       DWORD PTR [352+esp], 0                        ;191.7
        lea       edx, DWORD PTR [128+esp]                      ;191.7
        mov       DWORD PTR [128+esp], 10                       ;191.7
        mov       DWORD PTR [132+esp], OFFSET FLAT: __STRLITPACK_249 ;191.7
        push      32                                            ;191.7
        push      edx                                           ;191.7
        push      OFFSET FLAT: __STRLITPACK_311.0.2             ;191.7
        push      -2088435968                                   ;191.7
        push      666                                           ;191.7
        push      ebx                                           ;191.7
        call      _for_write_seq_lis                            ;191.7
                                ; LOE ebx
.B2.40:                         ; Preds .B2.39
        mov       DWORD PTR [376+esp], 0                        ;192.7
        lea       edx, DWORD PTR [160+esp]                      ;192.7
        mov       DWORD PTR [160+esp], 9                        ;192.7
        mov       DWORD PTR [164+esp], OFFSET FLAT: __STRLITPACK_247 ;192.7
        push      32                                            ;192.7
        push      edx                                           ;192.7
        push      OFFSET FLAT: __STRLITPACK_312.0.2             ;192.7
        push      -2088435968                                   ;192.7
        push      666                                           ;192.7
        push      ebx                                           ;192.7
        call      _for_write_seq_lis                            ;192.7
                                ; LOE ebx
.B2.41:                         ; Preds .B2.40
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DEC_NUM] ;193.7
        lea       ecx, DWORD PTR [608+esp]                      ;193.7
        mov       DWORD PTR [400+esp], 0                        ;193.7
        mov       DWORD PTR [608+esp], edx                      ;193.7
        push      32                                            ;193.7
        push      OFFSET FLAT: PRINTHEADER1$format_pack.0.2+648 ;193.7
        push      ecx                                           ;193.7
        push      OFFSET FLAT: __STRLITPACK_313.0.2             ;193.7
        push      -2088435968                                   ;193.7
        push      666                                           ;193.7
        push      ebx                                           ;193.7
        call      _for_write_seq_fmt                            ;193.7
                                ; LOE ebx
.B2.42:                         ; Preds .B2.41
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS_NUM] ;194.7
        lea       edx, DWORD PTR [644+esp]                      ;194.7
        mov       DWORD PTR [428+esp], 0                        ;194.7
        mov       DWORD PTR [644+esp], esi                      ;194.7
        push      32                                            ;194.7
        push      OFFSET FLAT: PRINTHEADER1$format_pack.0.2+720 ;194.7
        push      edx                                           ;194.7
        push      OFFSET FLAT: __STRLITPACK_314.0.2             ;194.7
        push      -2088435968                                   ;194.7
        push      666                                           ;194.7
        push      ebx                                           ;194.7
        call      _for_write_seq_fmt                            ;194.7
                                ; LOE ebx esi
.B2.172:                        ; Preds .B2.42
        add       esp, 104                                      ;194.7
                                ; LOE ebx esi
.B2.43:                         ; Preds .B2.172
        mov       DWORD PTR [352+esp], 0                        ;195.7
        lea       edx, DWORD PTR [144+esp]                      ;195.7
        mov       DWORD PTR [144+esp], 39                       ;195.7
        mov       DWORD PTR [148+esp], OFFSET FLAT: __STRLITPACK_241 ;195.7
        push      32                                            ;195.7
        push      edx                                           ;195.7
        push      OFFSET FLAT: __STRLITPACK_315.0.2             ;195.7
        push      -2088435968                                   ;195.7
        push      666                                           ;195.7
        push      ebx                                           ;195.7
        call      _for_write_seq_lis                            ;195.7
                                ; LOE ebx esi
.B2.44:                         ; Preds .B2.43
        xor       edx, edx                                      ;196.7
        mov       DWORD PTR [376+esp], edx                      ;196.7
        push      32                                            ;196.7
        push      edx                                           ;196.7
        push      OFFSET FLAT: __STRLITPACK_316.0.2             ;196.7
        push      -2088435968                                   ;196.7
        push      666                                           ;196.7
        push      ebx                                           ;196.7
        call      _for_write_seq_lis                            ;196.7
                                ; LOE ebx esi
.B2.45:                         ; Preds .B2.44
        mov       DWORD PTR [400+esp], 0                        ;197.7
        lea       edx, DWORD PTR [200+esp]                      ;197.7
        mov       DWORD PTR [200+esp], 14                       ;197.7
        mov       DWORD PTR [204+esp], OFFSET FLAT: __STRLITPACK_239 ;197.7
        push      32                                            ;197.7
        push      edx                                           ;197.7
        push      OFFSET FLAT: __STRLITPACK_317.0.2             ;197.7
        push      -2088435968                                   ;197.7
        push      666                                           ;197.7
        push      ebx                                           ;197.7
        call      _for_write_seq_lis                            ;197.7
                                ; LOE ebx esi
.B2.46:                         ; Preds .B2.45
        mov       DWORD PTR [424+esp], 0                        ;198.7
        lea       edx, DWORD PTR [232+esp]                      ;198.7
        mov       DWORD PTR [232+esp], 13                       ;198.7
        mov       DWORD PTR [236+esp], OFFSET FLAT: __STRLITPACK_237 ;198.7
        push      32                                            ;198.7
        push      edx                                           ;198.7
        push      OFFSET FLAT: __STRLITPACK_318.0.2             ;198.7
        push      -2088435968                                   ;198.7
        push      666                                           ;198.7
        push      ebx                                           ;198.7
        call      _for_write_seq_lis                            ;198.7
                                ; LOE ebx esi
.B2.173:                        ; Preds .B2.46
        add       esp, 96                                       ;198.7
                                ; LOE ebx esi
.B2.47:                         ; Preds .B2.173
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ITEMAX] ;199.7
        test      edi, edi                                      ;199.17
        mov       DWORD PTR [352+esp], 0                        ;200.9
        jne       .B2.49        ; Prob 50%                      ;199.17
                                ; LOE ebx esi edi
.B2.48:                         ; Preds .B2.47
        mov       DWORD PTR [esp], 34                           ;200.9
        lea       edx, DWORD PTR [esp]                          ;200.9
        mov       DWORD PTR [4+esp], OFFSET FLAT: __STRLITPACK_235 ;200.9
        push      32                                            ;200.9
        push      edx                                           ;200.9
        push      OFFSET FLAT: __STRLITPACK_319.0.2             ;200.9
        push      -2088435968                                   ;200.9
        push      666                                           ;200.9
        push      ebx                                           ;200.9
        call      _for_write_seq_lis                            ;200.9
                                ; LOE ebx esi edi
.B2.174:                        ; Preds .B2.48
        add       esp, 24                                       ;200.9
        jmp       .B2.52        ; Prob 100%                     ;200.9
                                ; LOE ebx esi edi
.B2.49:                         ; Preds .B2.47
        mov       DWORD PTR [8+esp], 55                         ;202.4
        lea       edx, DWORD PTR [8+esp]                        ;202.4
        mov       DWORD PTR [12+esp], OFFSET FLAT: __STRLITPACK_233 ;202.4
        push      32                                            ;202.4
        push      edx                                           ;202.4
        push      OFFSET FLAT: __STRLITPACK_320.0.2             ;202.4
        push      -2088435968                                   ;202.4
        push      666                                           ;202.4
        push      ebx                                           ;202.4
        call      _for_write_seq_lis                            ;202.4
                                ; LOE ebx esi edi
.B2.50:                         ; Preds .B2.49
        mov       DWORD PTR [376+esp], 0                        ;203.4
        lea       edx, DWORD PTR [128+esp]                      ;203.4
        mov       DWORD PTR [128+esp], edi                      ;203.4
        push      32                                            ;203.4
        push      OFFSET FLAT: PRINTHEADER1$format_pack.0.2+792 ;203.4
        push      edx                                           ;203.4
        push      OFFSET FLAT: __STRLITPACK_321.0.2             ;203.4
        push      -2088435968                                   ;203.4
        push      666                                           ;203.4
        push      ebx                                           ;203.4
        call      _for_write_seq_fmt                            ;203.4
                                ; LOE ebx esi edi
.B2.51:                         ; Preds .B2.50
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ITERATION] ;204.4
        lea       ecx, DWORD PTR [164+esp]                      ;204.4
        mov       DWORD PTR [404+esp], 0                        ;204.4
        mov       DWORD PTR [164+esp], edx                      ;204.4
        push      32                                            ;204.4
        push      OFFSET FLAT: PRINTHEADER1$format_pack.0.2+864 ;204.4
        push      ecx                                           ;204.4
        push      OFFSET FLAT: __STRLITPACK_322.0.2             ;204.4
        push      -2088435968                                   ;204.4
        push      666                                           ;204.4
        push      ebx                                           ;204.4
        call      _for_write_seq_fmt                            ;204.4
                                ; LOE ebx esi edi
.B2.175:                        ; Preds .B2.51
        add       esp, 80                                       ;204.4
                                ; LOE ebx esi edi
.B2.52:                         ; Preds .B2.174 .B2.175
        mov       DWORD PTR [352+esp], 0                        ;206.7
        lea       edx, DWORD PTR [168+esp]                      ;206.7
        mov       DWORD PTR [168+esp], 39                       ;206.7
        mov       DWORD PTR [172+esp], OFFSET FLAT: __STRLITPACK_227 ;206.7
        push      32                                            ;206.7
        push      edx                                           ;206.7
        push      OFFSET FLAT: __STRLITPACK_323.0.2             ;206.7
        push      -2088435968                                   ;206.7
        push      666                                           ;206.7
        push      ebx                                           ;206.7
        call      _for_write_seq_lis                            ;206.7
                                ; LOE ebx esi edi
.B2.53:                         ; Preds .B2.52
        xor       edx, edx                                      ;207.7
        mov       DWORD PTR [376+esp], edx                      ;207.7
        push      32                                            ;207.7
        push      edx                                           ;207.7
        push      OFFSET FLAT: __STRLITPACK_324.0.2             ;207.7
        push      -2088435968                                   ;207.7
        push      666                                           ;207.7
        push      ebx                                           ;207.7
        call      _for_write_seq_lis                            ;207.7
                                ; LOE ebx esi edi
.B2.54:                         ; Preds .B2.53
        mov       DWORD PTR [400+esp], 0                        ;208.7
        lea       edx, DWORD PTR [224+esp]                      ;208.7
        mov       DWORD PTR [224+esp], 13                       ;208.7
        mov       DWORD PTR [228+esp], OFFSET FLAT: __STRLITPACK_225 ;208.7
        push      32                                            ;208.7
        push      edx                                           ;208.7
        push      OFFSET FLAT: __STRLITPACK_325.0.2             ;208.7
        push      -2088435968                                   ;208.7
        push      666                                           ;208.7
        push      ebx                                           ;208.7
        call      _for_write_seq_lis                            ;208.7
                                ; LOE ebx esi edi
.B2.55:                         ; Preds .B2.54
        mov       DWORD PTR [424+esp], 0                        ;209.7
        lea       edx, DWORD PTR [256+esp]                      ;209.7
        mov       DWORD PTR [256+esp], 12                       ;209.7
        mov       DWORD PTR [260+esp], OFFSET FLAT: __STRLITPACK_223 ;209.7
        push      32                                            ;209.7
        push      edx                                           ;209.7
        push      OFFSET FLAT: __STRLITPACK_326.0.2             ;209.7
        push      -2088435968                                   ;209.7
        push      666                                           ;209.7
        push      ebx                                           ;209.7
        call      _for_write_seq_lis                            ;209.7
                                ; LOE ebx esi edi
.B2.56:                         ; Preds .B2.55
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIMPERIOD] ;210.7
        lea       ecx, DWORD PTR [672+esp]                      ;210.7
        mov       DWORD PTR [448+esp], 0                        ;210.7
        mov       DWORD PTR [672+esp], edx                      ;210.7
        push      32                                            ;210.7
        push      OFFSET FLAT: PRINTHEADER1$format_pack.0.2+936 ;210.7
        push      ecx                                           ;210.7
        push      OFFSET FLAT: __STRLITPACK_327.0.2             ;210.7
        push      -2088435968                                   ;210.7
        push      666                                           ;210.7
        push      ebx                                           ;210.7
        call      _for_write_seq_fmt                            ;210.7
                                ; LOE ebx esi edi
.B2.176:                        ; Preds .B2.56
        add       esp, 124                                      ;210.7
                                ; LOE ebx esi edi
.B2.57:                         ; Preds .B2.176
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIMPERAGG] ;211.7
        lea       ecx, DWORD PTR [584+esp]                      ;211.7
        mov       DWORD PTR [352+esp], 0                        ;211.7
        mov       DWORD PTR [584+esp], edx                      ;211.7
        push      32                                            ;211.7
        push      OFFSET FLAT: PRINTHEADER1$format_pack.0.2+1008 ;211.7
        push      ecx                                           ;211.7
        push      OFFSET FLAT: __STRLITPACK_328.0.2             ;211.7
        push      -2088435968                                   ;211.7
        push      666                                           ;211.7
        push      ebx                                           ;211.7
        call      _for_write_seq_fmt                            ;211.7
                                ; LOE ebx esi edi
.B2.58:                         ; Preds .B2.57
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIMPERASG] ;212.7
        lea       ecx, DWORD PTR [620+esp]                      ;212.7
        mov       DWORD PTR [380+esp], 0                        ;212.7
        mov       DWORD PTR [620+esp], edx                      ;212.7
        push      32                                            ;212.7
        push      OFFSET FLAT: PRINTHEADER1$format_pack.0.2+1080 ;212.7
        push      ecx                                           ;212.7
        push      OFFSET FLAT: __STRLITPACK_329.0.2             ;212.7
        push      -2088435968                                   ;212.7
        push      666                                           ;212.7
        push      ebx                                           ;212.7
        call      _for_write_seq_fmt                            ;212.7
                                ; LOE ebx esi edi
.B2.59:                         ; Preds .B2.58
        mov       DWORD PTR [408+esp], 0                        ;213.4
        lea       edx, DWORD PTR [656+esp]                      ;213.4
        mov       DWORD PTR [656+esp], edi                      ;213.4
        push      32                                            ;213.4
        push      OFFSET FLAT: PRINTHEADER1$format_pack.0.2+1152 ;213.4
        push      edx                                           ;213.4
        push      OFFSET FLAT: __STRLITPACK_330.0.2             ;213.4
        push      -2088435968                                   ;213.4
        push      666                                           ;213.4
        push      ebx                                           ;213.4
        call      _for_write_seq_fmt                            ;213.4
                                ; LOE ebx esi
.B2.60:                         ; Preds .B2.59
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_MTC_DIFF] ;214.4
        lea       ecx, DWORD PTR [692+esp]                      ;214.4
        mov       DWORD PTR [436+esp], 0                        ;214.4
        mov       DWORD PTR [692+esp], edx                      ;214.4
        push      32                                            ;214.4
        push      OFFSET FLAT: PRINTHEADER1$format_pack.0.2+1224 ;214.4
        push      ecx                                           ;214.4
        push      OFFSET FLAT: __STRLITPACK_331.0.2             ;214.4
        push      -2088435968                                   ;214.4
        push      666                                           ;214.4
        push      ebx                                           ;214.4
        call      _for_write_seq_fmt                            ;214.4
                                ; LOE ebx esi
.B2.177:                        ; Preds .B2.60
        add       esp, 112                                      ;214.4
                                ; LOE ebx esi
.B2.61:                         ; Preds .B2.177
        movss     xmm0, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NO_VIA] ;215.4
        lea       edx, DWORD PTR [616+esp]                      ;215.4
        divss     xmm0, DWORD PTR [_2il0floatpacket.5]          ;215.4
        mov       DWORD PTR [352+esp], 0                        ;215.4
        movss     DWORD PTR [616+esp], xmm0                     ;215.4
        push      32                                            ;215.4
        push      OFFSET FLAT: PRINTHEADER1$format_pack.0.2+1296 ;215.4
        push      edx                                           ;215.4
        push      OFFSET FLAT: __STRLITPACK_332.0.2             ;215.4
        push      -2088435968                                   ;215.4
        push      666                                           ;215.4
        push      ebx                                           ;215.4
        call      _for_write_seq_fmt                            ;215.4
                                ; LOE ebx esi
.B2.62:                         ; Preds .B2.61
        mov       DWORD PTR [380+esp], 0                        ;216.7
        lea       edx, DWORD PTR [220+esp]                      ;216.7
        mov       DWORD PTR [220+esp], 39                       ;216.7
        mov       DWORD PTR [224+esp], OFFSET FLAT: __STRLITPACK_209 ;216.7
        push      32                                            ;216.7
        push      edx                                           ;216.7
        push      OFFSET FLAT: __STRLITPACK_333.0.2             ;216.7
        push      -2088435968                                   ;216.7
        push      666                                           ;216.7
        push      ebx                                           ;216.7
        call      _for_write_seq_lis                            ;216.7
                                ; LOE ebx esi
.B2.63:                         ; Preds .B2.62
        xor       edx, edx                                      ;217.7
        mov       DWORD PTR [404+esp], edx                      ;217.7
        push      32                                            ;217.7
        push      edx                                           ;217.7
        push      OFFSET FLAT: __STRLITPACK_334.0.2             ;217.7
        push      -2088435968                                   ;217.7
        push      666                                           ;217.7
        push      ebx                                           ;217.7
        call      _for_write_seq_lis                            ;217.7
                                ; LOE ebx esi
.B2.64:                         ; Preds .B2.63
        mov       DWORD PTR [428+esp], 0                        ;218.7
        lea       edx, DWORD PTR [276+esp]                      ;218.7
        mov       DWORD PTR [276+esp], 46                       ;218.7
        mov       DWORD PTR [280+esp], OFFSET FLAT: __STRLITPACK_207 ;218.7
        push      32                                            ;218.7
        push      edx                                           ;218.7
        push      OFFSET FLAT: __STRLITPACK_335.0.2             ;218.7
        push      -2088435968                                   ;218.7
        push      666                                           ;218.7
        push      ebx                                           ;218.7
        call      _for_write_seq_lis                            ;218.7
                                ; LOE ebx esi
.B2.65:                         ; Preds .B2.64
        mov       DWORD PTR [452+esp], 0                        ;219.7
        lea       edx, DWORD PTR [308+esp]                      ;219.7
        mov       DWORD PTR [308+esp], 18                       ;219.7
        mov       DWORD PTR [312+esp], OFFSET FLAT: __STRLITPACK_205 ;219.7
        push      32                                            ;219.7
        push      edx                                           ;219.7
        push      OFFSET FLAT: __STRLITPACK_336.0.2             ;219.7
        push      -2088435968                                   ;219.7
        push      666                                           ;219.7
        push      ebx                                           ;219.7
        call      _for_write_seq_lis                            ;219.7
                                ; LOE ebx esi
.B2.178:                        ; Preds .B2.65
        add       esp, 124                                      ;219.7
                                ; LOE ebx esi
.B2.66:                         ; Preds .B2.178
        xor       edx, edx                                      ;220.7
        mov       DWORD PTR [352+esp], edx                      ;220.7
        push      32                                            ;220.7
        push      edx                                           ;220.7
        push      OFFSET FLAT: __STRLITPACK_337.0.2             ;220.7
        push      -2088435968                                   ;220.7
        push      666                                           ;220.7
        push      ebx                                           ;220.7
        call      _for_write_seq_lis                            ;220.7
                                ; LOE ebx esi
.B2.67:                         ; Preds .B2.66
        mov       DWORD PTR [376+esp], 0                        ;221.7
        lea       edx, DWORD PTR [240+esp]                      ;221.7
        mov       DWORD PTR [240+esp], 21                       ;221.7
        mov       DWORD PTR [244+esp], OFFSET FLAT: __STRLITPACK_203 ;221.7
        push      32                                            ;221.7
        push      edx                                           ;221.7
        push      OFFSET FLAT: __STRLITPACK_338.0.2             ;221.7
        push      -2088435968                                   ;221.7
        push      666                                           ;221.7
        push      ebx                                           ;221.7
        call      _for_write_seq_lis                            ;221.7
                                ; LOE ebx esi
.B2.68:                         ; Preds .B2.67
        mov       DWORD PTR [400+esp], 0                        ;222.7
        lea       edx, DWORD PTR [272+esp]                      ;222.7
        mov       DWORD PTR [272+esp], 20                       ;222.7
        mov       DWORD PTR [276+esp], OFFSET FLAT: __STRLITPACK_201 ;222.7
        push      32                                            ;222.7
        push      edx                                           ;222.7
        push      OFFSET FLAT: __STRLITPACK_339.0.2             ;222.7
        push      -2088435968                                   ;222.7
        push      666                                           ;222.7
        push      ebx                                           ;222.7
        call      _for_write_seq_lis                            ;222.7
                                ; LOE ebx esi
.B2.179:                        ; Preds .B2.68
        add       esp, 72                                       ;222.7
                                ; LOE ebx esi
.B2.69:                         ; Preds .B2.179
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_RUNMODE] ;223.7
        cmp       edi, 4                                        ;223.18
        ja        .B2.81        ; Prob 50%                      ;223.18
                                ; LOE ebx esi edi
.B2.70:                         ; Preds .B2.69
        jmp       DWORD PTR [..1..TPKT.2_0.0.2+edi*4]           ;223.18
                                ; LOE ebx esi edi
..1.2_0.TAG.04.0.2::
.B2.72:                         ; Preds .B2.70
        xor       edx, edx                                      ;232.4
        mov       DWORD PTR [352+esp], edx                      ;232.4
        push      32                                            ;232.4
        push      OFFSET FLAT: PRINTHEADER1$format_pack.0.2+1636 ;232.4
        push      edx                                           ;232.4
        push      OFFSET FLAT: __STRLITPACK_344.0.2             ;232.4
        push      -2088435968                                   ;232.4
        push      666                                           ;232.4
        push      ebx                                           ;232.4
        call      _for_write_seq_fmt                            ;232.4
        jmp       .B2.184       ; Prob 100%                     ;232.4
                                ; LOE ebx esi edi
..1.2_0.TAG.03.0.2::
.B2.74:                         ; Preds .B2.70
        xor       edx, edx                                      ;230.4
        mov       DWORD PTR [352+esp], edx                      ;230.4
        push      32                                            ;230.4
        push      OFFSET FLAT: PRINTHEADER1$format_pack.0.2+1568 ;230.4
        push      edx                                           ;230.4
        push      OFFSET FLAT: __STRLITPACK_343.0.2             ;230.4
        push      -2088435968                                   ;230.4
        push      666                                           ;230.4
        push      ebx                                           ;230.4
        call      _for_write_seq_fmt                            ;230.4
        jmp       .B2.184       ; Prob 100%                     ;230.4
                                ; LOE ebx esi edi
..1.2_0.TAG.02.0.2::
.B2.76:                         ; Preds .B2.70
        xor       edx, edx                                      ;228.4
        mov       DWORD PTR [352+esp], edx                      ;228.4
        push      32                                            ;228.4
        push      OFFSET FLAT: PRINTHEADER1$format_pack.0.2+1500 ;228.4
        push      edx                                           ;228.4
        push      OFFSET FLAT: __STRLITPACK_342.0.2             ;228.4
        push      -2088435968                                   ;228.4
        push      666                                           ;228.4
        push      ebx                                           ;228.4
        call      _for_write_seq_fmt                            ;228.4
        jmp       .B2.184       ; Prob 100%                     ;228.4
                                ; LOE ebx esi edi
..1.2_0.TAG.01.0.2::
.B2.78:                         ; Preds .B2.70
        xor       edx, edx                                      ;224.4
        mov       DWORD PTR [352+esp], edx                      ;224.4
        push      32                                            ;224.4
        push      OFFSET FLAT: PRINTHEADER1$format_pack.0.2+1368 ;224.4
        push      edx                                           ;224.4
        push      OFFSET FLAT: __STRLITPACK_340.0.2             ;224.4
        push      -2088435968                                   ;224.4
        push      666                                           ;224.4
        push      ebx                                           ;224.4
        call      _for_write_seq_fmt                            ;224.4
        jmp       .B2.184       ; Prob 100%                     ;224.4
                                ; LOE ebx esi edi
..1.2_0.TAG.00.0.2::
.B2.80:                         ; Preds .B2.70
        xor       edx, edx                                      ;226.4
        mov       DWORD PTR [352+esp], edx                      ;226.4
        push      32                                            ;226.4
        push      OFFSET FLAT: PRINTHEADER1$format_pack.0.2+1436 ;226.4
        push      edx                                           ;226.4
        push      OFFSET FLAT: __STRLITPACK_341.0.2             ;226.4
        push      -2088435968                                   ;226.4
        push      666                                           ;226.4
        push      ebx                                           ;226.4
        call      _for_write_seq_fmt                            ;226.4
                                ; LOE ebx esi edi
.B2.184:                        ; Preds .B2.72 .B2.74 .B2.76 .B2.78 .B2.80
                                ;      
        add       esp, 28                                       ;226.4
                                ; LOE ebx esi edi
.B2.81:                         ; Preds .B2.184 .B2.69
        xor       edx, edx                                      ;236.7
        mov       DWORD PTR [352+esp], edx                      ;236.7
        push      32                                            ;236.7
        push      edx                                           ;236.7
        push      OFFSET FLAT: __STRLITPACK_345.0.2             ;236.7
        push      -2088435968                                   ;236.7
        push      666                                           ;236.7
        push      ebx                                           ;236.7
        call      _for_write_seq_lis                            ;236.7
                                ; LOE ebx esi edi
.B2.82:                         ; Preds .B2.81
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELTIMEWEIGHT] ;237.7
        lea       ecx, DWORD PTR [648+esp]                      ;237.7
        mov       DWORD PTR [376+esp], 0                        ;237.7
        mov       DWORD PTR [648+esp], edx                      ;237.7
        push      32                                            ;237.7
        push      OFFSET FLAT: PRINTHEADER1$format_pack.0.2+1704 ;237.7
        push      ecx                                           ;237.7
        push      OFFSET FLAT: __STRLITPACK_346.0.2             ;237.7
        push      -2088435968                                   ;237.7
        push      666                                           ;237.7
        push      ebx                                           ;237.7
        call      _for_write_seq_fmt                            ;237.7
                                ; LOE ebx esi edi
.B2.83:                         ; Preds .B2.82
        xor       edx, edx                                      ;238.7
        mov       DWORD PTR [404+esp], edx                      ;238.7
        push      32                                            ;238.7
        push      edx                                           ;238.7
        push      OFFSET FLAT: __STRLITPACK_347.0.2             ;238.7
        push      -2088435968                                   ;238.7
        push      666                                           ;238.7
        push      ebx                                           ;238.7
        call      _for_write_seq_lis                            ;238.7
                                ; LOE ebx esi edi
.B2.84:                         ; Preds .B2.83
        mov       DWORD PTR [428+esp], 0                        ;239.7
        lea       edx, DWORD PTR [308+esp]                      ;239.7
        mov       DWORD PTR [308+esp], 39                       ;239.7
        mov       DWORD PTR [312+esp], OFFSET FLAT: __STRLITPACK_187 ;239.7
        push      32                                            ;239.7
        push      edx                                           ;239.7
        push      OFFSET FLAT: __STRLITPACK_348.0.2             ;239.7
        push      -2088435968                                   ;239.7
        push      666                                           ;239.7
        push      ebx                                           ;239.7
        call      _for_write_seq_lis                            ;239.7
                                ; LOE ebx esi edi
.B2.85:                         ; Preds .B2.84
        xor       edx, edx                                      ;240.7
        mov       DWORD PTR [452+esp], edx                      ;240.7
        push      32                                            ;240.7
        push      edx                                           ;240.7
        push      OFFSET FLAT: __STRLITPACK_349.0.2             ;240.7
        push      -2088435968                                   ;240.7
        push      666                                           ;240.7
        push      ebx                                           ;240.7
        call      _for_write_seq_lis                            ;240.7
                                ; LOE ebx esi edi
.B2.185:                        ; Preds .B2.85
        add       esp, 124                                      ;240.7
                                ; LOE ebx esi edi
.B2.86:                         ; Preds .B2.185
        mov       DWORD PTR [352+esp], 0                        ;241.7
        lea       edx, DWORD PTR [240+esp]                      ;241.7
        mov       DWORD PTR [240+esp], 30                       ;241.7
        mov       DWORD PTR [244+esp], OFFSET FLAT: __STRLITPACK_185 ;241.7
        push      32                                            ;241.7
        push      edx                                           ;241.7
        push      OFFSET FLAT: __STRLITPACK_350.0.2             ;241.7
        push      -2088435968                                   ;241.7
        push      666                                           ;241.7
        push      ebx                                           ;241.7
        call      _for_write_seq_lis                            ;241.7
                                ; LOE ebx esi edi
.B2.87:                         ; Preds .B2.86
        mov       DWORD PTR [376+esp], 0                        ;242.7
        lea       edx, DWORD PTR [272+esp]                      ;242.7
        mov       DWORD PTR [272+esp], 29                       ;242.7
        mov       DWORD PTR [276+esp], OFFSET FLAT: __STRLITPACK_183 ;242.7
        push      32                                            ;242.7
        push      edx                                           ;242.7
        push      OFFSET FLAT: __STRLITPACK_351.0.2             ;242.7
        push      -2088435968                                   ;242.7
        push      666                                           ;242.7
        push      ebx                                           ;242.7
        call      _for_write_seq_lis                            ;242.7
                                ; LOE ebx esi edi
.B2.186:                        ; Preds .B2.87
        add       esp, 48                                       ;242.7
                                ; LOE ebx esi edi
.B2.88:                         ; Preds .B2.186
        mov       DWORD PTR [352+esp], 0                        ;244.7
        test      esi, esi                                      ;243.15
        jle       .B2.163       ; Prob 16%                      ;243.15
                                ; LOE ebx esi edi
.B2.89:                         ; Preds .B2.88
        mov       DWORD PTR [480+esp], esi                      ;244.7
        lea       edx, DWORD PTR [480+esp]                      ;244.7
        push      32                                            ;244.7
        push      OFFSET FLAT: PRINTHEADER1$format_pack.0.2+1804 ;244.7
        push      edx                                           ;244.7
        push      OFFSET FLAT: __STRLITPACK_352.0.2             ;244.7
        push      -2088435968                                   ;244.7
        push      666                                           ;244.7
        push      ebx                                           ;244.7
        call      _for_write_seq_fmt                            ;244.7
                                ; LOE ebx esi edi
.B2.187:                        ; Preds .B2.89
        add       esp, 28                                       ;244.7
                                ; LOE ebx esi edi
.B2.90:                         ; Preds .B2.187
        imul      edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE+32], -44 ;
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPE+32] ;246.4
        shl       ecx, 2                                        ;
        neg       ecx                                           ;
        add       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE] ;
        add       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPE] ;
        mov       DWORD PTR [28+esp], edx                       ;
        imul      edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32], -152 ;
        mov       DWORD PTR [276+esp], ecx                      ;
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS+40] ;
        mov       DWORD PTR [108+esp], edi                      ;
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS+44] ;249.6
        imul      edi, ecx                                      ;
        add       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;
        mov       DWORD PTR [116+esp], edx                      ;
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS]   ;
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS+32] ;
        sub       eax, edi                                      ;
        shl       edx, 2                                        ;
        mov       edi, ecx                                      ;
        sub       edi, edx                                      ;
        add       edi, eax                                      ;
        mov       DWORD PTR [260+esp], edi                      ;
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS_START+32] ;
        shl       edi, 2                                        ;
        neg       edi                                           ;
        add       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS_START] ;
        mov       DWORD PTR [268+esp], edi                      ;
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS_END+32] ;
        shl       edi, 2                                        ;
        neg       edi                                           ;
        add       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS_END] ;
        mov       DWORD PTR [272+esp], edi                      ;
        lea       edi, DWORD PTR [ecx+ecx*2]                    ;250.127
        add       ecx, ecx                                      ;250.110
        sub       edi, edx                                      ;
        sub       ecx, edx                                      ;
        add       edi, eax                                      ;
        mov       DWORD PTR [16+esp], 1                         ;
        add       eax, ecx                                      ;
        mov       DWORD PTR [256+esp], edi                      ;
        mov       ecx, DWORD PTR [268+esp]                      ;
        mov       edi, DWORD PTR [16+esp]                       ;
        mov       DWORD PTR [264+esp], eax                      ;
                                ; LOE ebx esi edi
.B2.91:                         ; Preds .B2.94 .B2.90
        mov       edx, DWORD PTR [276+esp]                      ;246.7
        mov       edx, DWORD PTR [edx+edi*4]                    ;246.7
        cmp       edx, 1                                        ;246.19
        je        .B2.155       ; Prob 16%                      ;246.19
                                ; LOE edx ebx esi edi
.B2.92:                         ; Preds .B2.91
        cmp       edx, 2                                        ;252.23
        je        .B2.148       ; Prob 16%                      ;252.23
                                ; LOE edx ebx esi edi
.B2.93:                         ; Preds .B2.92
        cmp       edx, 3                                        ;259.23
        je        .B2.138       ; Prob 16%                      ;259.23
                                ; LOE ebx esi edi
.B2.94:                         ; Preds .B2.207 .B2.205 .B2.202 .B2.93
        inc       edi                                           ;273.4
        cmp       edi, esi                                      ;273.4
        jle       .B2.91        ; Prob 82%                      ;273.4
                                ; LOE ebx esi edi
.B2.95:                         ; Preds .B2.94
        mov       edi, DWORD PTR [108+esp]                      ;
                                ; LOE ebx edi
.B2.96:                         ; Preds .B2.208 .B2.95
        mov       DWORD PTR [352+esp], 0                        ;278.7
        lea       edx, DWORD PTR [256+esp]                      ;278.7
        mov       DWORD PTR [256+esp], 39                       ;278.7
        mov       DWORD PTR [260+esp], OFFSET FLAT: __STRLITPACK_155 ;278.7
        push      32                                            ;278.7
        push      edx                                           ;278.7
        push      OFFSET FLAT: __STRLITPACK_378.0.2             ;278.7
        push      -2088435968                                   ;278.7
        push      666                                           ;278.7
        push      ebx                                           ;278.7
        call      _for_write_seq_lis                            ;278.7
                                ; LOE ebx edi
.B2.97:                         ; Preds .B2.96
        xor       edx, edx                                      ;279.7
        mov       DWORD PTR [376+esp], edx                      ;279.7
        push      32                                            ;279.7
        push      edx                                           ;279.7
        push      OFFSET FLAT: __STRLITPACK_379.0.2             ;279.7
        push      -2088435968                                   ;279.7
        push      666                                           ;279.7
        push      ebx                                           ;279.7
        call      _for_write_seq_lis                            ;279.7
                                ; LOE ebx edi
.B2.98:                         ; Preds .B2.97
        mov       DWORD PTR [400+esp], 0                        ;280.7
        lea       edx, DWORD PTR [312+esp]                      ;280.7
        mov       DWORD PTR [312+esp], 19                       ;280.7
        mov       DWORD PTR [316+esp], OFFSET FLAT: __STRLITPACK_153 ;280.7
        push      32                                            ;280.7
        push      edx                                           ;280.7
        push      OFFSET FLAT: __STRLITPACK_380.0.2             ;280.7
        push      -2088435968                                   ;280.7
        push      666                                           ;280.7
        push      ebx                                           ;280.7
        call      _for_write_seq_lis                            ;280.7
                                ; LOE ebx edi
.B2.99:                         ; Preds .B2.98
        mov       DWORD PTR [424+esp], 0                        ;281.7
        lea       edx, DWORD PTR [344+esp]                      ;281.7
        mov       DWORD PTR [344+esp], 19                       ;281.7
        mov       DWORD PTR [348+esp], OFFSET FLAT: __STRLITPACK_151 ;281.7
        push      32                                            ;281.7
        push      edx                                           ;281.7
        push      OFFSET FLAT: __STRLITPACK_381.0.2             ;281.7
        push      -2088435968                                   ;281.7
        push      666                                           ;281.7
        push      ebx                                           ;281.7
        call      _for_write_seq_lis                            ;281.7
                                ; LOE ebx edi
.B2.188:                        ; Preds .B2.99
        add       esp, 96                                       ;281.7
                                ; LOE ebx edi
.B2.100:                        ; Preds .B2.188
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_INCI_NUM] ;282.4
        test      edx, edx                                      ;282.16
        mov       DWORD PTR [116+esp], edx                      ;282.4
        jle       .B2.110       ; Prob 16%                      ;282.16
                                ; LOE ebx edi
.B2.101:                        ; Preds .B2.100
        mov       DWORD PTR [352+esp], 0                        ;283.8
        lea       edx, DWORD PTR [16+esp]                       ;283.8
        mov       DWORD PTR [16+esp], 18                        ;283.8
        mov       DWORD PTR [20+esp], OFFSET FLAT: __STRLITPACK_149 ;283.8
        push      32                                            ;283.8
        push      edx                                           ;283.8
        push      OFFSET FLAT: __STRLITPACK_382.0.2             ;283.8
        push      -2088435968                                   ;283.8
        push      666                                           ;283.8
        push      ebx                                           ;283.8
        call      _for_write_seq_lis                            ;283.8
                                ; LOE ebx edi
.B2.189:                        ; Preds .B2.101
        add       esp, 24                                       ;283.8
                                ; LOE ebx edi
.B2.102:                        ; Preds .B2.189
        imul      esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE+32], -44 ;
        add       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE] ;
        mov       DWORD PTR [288+esp], esi                      ;
        imul      esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32], -152 ;
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_INCI+40] ;285.294
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_INCI+44] ;285.294
        imul      ecx, edx                                      ;
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_INCIL+32] ;285.8
        shl       eax, 2                                        ;
        neg       eax                                           ;
        add       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_INCIL] ;
        add       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;
        mov       DWORD PTR [284+esp], esi                      ;
        mov       DWORD PTR [292+esp], eax                      ;
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_INCI]  ;
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_INCI+32] ;
        sub       eax, ecx                                      ;
        shl       esi, 2                                        ;
        mov       ecx, edx                                      ;
        sub       ecx, esi                                      ;
        add       ecx, eax                                      ;
        mov       DWORD PTR [296+esp], ecx                      ;
        lea       ecx, DWORD PTR [edx+edx]                      ;285.305
        sub       ecx, esi                                      ;
        lea       edx, DWORD PTR [edx+edx*2]                    ;285.316
        sub       edx, esi                                      ;
        add       ecx, eax                                      ;
        mov       DWORD PTR [24+esp], 1                         ;
        add       eax, edx                                      ;
        mov       DWORD PTR [108+esp], edi                      ;
        mov       DWORD PTR [280+esp], eax                      ;
        mov       DWORD PTR [28+esp], ecx                       ;
        mov       edi, DWORD PTR [24+esp]                       ;
                                ; LOE ebx edi
.B2.103:                        ; Preds .B2.108 .B2.102
        mov       esi, DWORD PTR [292+esp]                      ;285.8
        mov       edx, DWORD PTR [284+esp]                      ;285.126
        mov       ecx, DWORD PTR [288+esp]                      ;285.8
        imul      esi, DWORD PTR [esi+edi*4], 152               ;285.8
        imul      edx, DWORD PTR [28+edx+esi], 44               ;285.126
        mov       edx, DWORD PTR [36+ecx+edx]                   ;285.8
        mov       DWORD PTR [352+esp], 0                        ;285.8
        mov       DWORD PTR [632+esp], edx                      ;285.8
        push      32                                            ;285.8
        push      OFFSET FLAT: PRINTHEADER1$format_pack.0.2+2872 ;285.8
        lea       edx, DWORD PTR [640+esp]                      ;285.8
        push      edx                                           ;285.8
        push      OFFSET FLAT: __STRLITPACK_383.0.2             ;285.8
        push      -2088435968                                   ;285.8
        push      666                                           ;285.8
        push      ebx                                           ;285.8
        call      _for_write_seq_fmt                            ;285.8
                                ; LOE ebx esi edi
.B2.104:                        ; Preds .B2.103
        mov       edx, DWORD PTR [312+esp]                      ;285.210
        mov       ecx, DWORD PTR [316+esp]                      ;285.8
        imul      esi, DWORD PTR [24+edx+esi], 44               ;285.210
        mov       edx, DWORD PTR [36+ecx+esi]                   ;285.8
        mov       DWORD PTR [668+esp], edx                      ;285.8
        lea       edx, DWORD PTR [668+esp]                      ;285.8
        push      edx                                           ;285.8
        push      OFFSET FLAT: __STRLITPACK_384.0.2             ;285.8
        push      ebx                                           ;285.8
        call      _for_write_seq_fmt_xmit                       ;285.8
                                ; LOE ebx edi
.B2.105:                        ; Preds .B2.104
        mov       edx, DWORD PTR [336+esp]                      ;285.8
        lea       esi, DWORD PTR [688+esp]                      ;285.8
        mov       ecx, DWORD PTR [edx+edi*4]                    ;285.8
        mov       DWORD PTR [688+esp], ecx                      ;285.8
        push      esi                                           ;285.8
        push      OFFSET FLAT: __STRLITPACK_385.0.2             ;285.8
        push      ebx                                           ;285.8
        call      _for_write_seq_fmt_xmit                       ;285.8
                                ; LOE ebx edi
.B2.106:                        ; Preds .B2.105
        mov       edx, DWORD PTR [80+esp]                       ;285.8
        lea       esi, DWORD PTR [708+esp]                      ;285.8
        mov       ecx, DWORD PTR [edx+edi*4]                    ;285.8
        mov       DWORD PTR [708+esp], ecx                      ;285.8
        push      esi                                           ;285.8
        push      OFFSET FLAT: __STRLITPACK_386.0.2             ;285.8
        push      ebx                                           ;285.8
        call      _for_write_seq_fmt_xmit                       ;285.8
                                ; LOE ebx edi
.B2.107:                        ; Preds .B2.106
        mov       edx, DWORD PTR [344+esp]                      ;285.316
        lea       ecx, DWORD PTR [728+esp]                      ;285.8
        movss     xmm0, DWORD PTR [edx+edi*4]                   ;285.316
        mulss     xmm0, DWORD PTR [_2il0floatpacket.6]          ;285.8
        movss     DWORD PTR [728+esp], xmm0                     ;285.8
        push      ecx                                           ;285.8
        push      OFFSET FLAT: __STRLITPACK_387.0.2             ;285.8
        push      ebx                                           ;285.8
        call      _for_write_seq_fmt_xmit                       ;285.8
                                ; LOE ebx edi
.B2.190:                        ; Preds .B2.107
        add       esp, 76                                       ;285.8
                                ; LOE ebx edi
.B2.108:                        ; Preds .B2.190
        inc       edi                                           ;286.5
        cmp       edi, DWORD PTR [116+esp]                      ;286.5
        jle       .B2.103       ; Prob 82%                      ;286.5
                                ; LOE ebx edi
.B2.109:                        ; Preds .B2.108
        mov       edi, DWORD PTR [108+esp]                      ;
                                ; LOE ebx edi
.B2.110:                        ; Preds .B2.100 .B2.109
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_WORKZONENUM] ;288.4
        test      esi, esi                                      ;288.19
        jle       .B2.120       ; Prob 16%                      ;288.19
                                ; LOE ebx esi edi
.B2.111:                        ; Preds .B2.110
        mov       DWORD PTR [352+esp], 0                        ;289.8
        lea       edx, DWORD PTR [24+esp]                       ;289.8
        mov       DWORD PTR [24+esp], 18                        ;289.8
        mov       DWORD PTR [28+esp], OFFSET FLAT: __STRLITPACK_145 ;289.8
        push      32                                            ;289.8
        push      edx                                           ;289.8
        push      OFFSET FLAT: __STRLITPACK_388.0.2             ;289.8
        push      -2088435968                                   ;289.8
        push      666                                           ;289.8
        push      ebx                                           ;289.8
        call      _for_write_seq_lis                            ;289.8
                                ; LOE ebx esi edi
.B2.191:                        ; Preds .B2.111
        add       esp, 24                                       ;289.8
                                ; LOE ebx esi edi
.B2.112:                        ; Preds .B2.191
        imul      ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE+32], -44 ;
        mov       edx, 1                                        ;
        imul      eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_WORKZONE+32], -36 ;
        add       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE] ;
        add       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_WORKZONE] ;
        mov       DWORD PTR [284+esp], ecx                      ;291.299
        mov       DWORD PTR [280+esp], esi                      ;291.299
        mov       esi, eax                                      ;291.299
        mov       DWORD PTR [108+esp], edi                      ;291.299
        mov       eax, ebx                                      ;291.299
        mov       ebx, edx                                      ;291.299
                                ; LOE eax ebx esi
.B2.113:                        ; Preds .B2.118 .B2.112
        mov       ecx, DWORD PTR [284+esp]                      ;291.8
        lea       edi, DWORD PTR [ebx*4]                        ;291.8
        mov       DWORD PTR [352+esp], 0                        ;291.8
        lea       edi, DWORD PTR [edi+edi*8]                    ;291.8
        imul      edx, DWORD PTR [esi+edi], 44                  ;291.126
        mov       ecx, DWORD PTR [36+ecx+edx]                   ;291.8
        mov       DWORD PTR [672+esp], ecx                      ;291.8
        push      32                                            ;291.8
        push      OFFSET FLAT: PRINTHEADER1$format_pack.0.2+3032 ;291.8
        lea       ecx, DWORD PTR [680+esp]                      ;291.8
        push      ecx                                           ;291.8
        push      OFFSET FLAT: __STRLITPACK_389.0.2             ;291.8
        push      -2088435968                                   ;291.8
        push      666                                           ;291.8
        push      eax                                           ;291.8
        call      _for_write_seq_fmt                            ;291.8
                                ; LOE ebx esi edi
.B2.114:                        ; Preds .B2.113
        imul      edx, DWORD PTR [4+esi+edi], 44                ;291.187
        lea       eax, DWORD PTR [380+esp]                      ;
        mov       ecx, DWORD PTR [312+esp]                      ;291.8
        mov       ecx, DWORD PTR [36+ecx+edx]                   ;291.8
        mov       DWORD PTR [708+esp], ecx                      ;291.8
        lea       ecx, DWORD PTR [708+esp]                      ;291.8
        push      ecx                                           ;291.8
        push      OFFSET FLAT: __STRLITPACK_390.0.2             ;291.8
        push      eax                                           ;291.8
        call      _for_write_seq_fmt_xmit                       ;291.8
                                ; LOE ebx esi edi
.B2.115:                        ; Preds .B2.114
        mov       ecx, DWORD PTR [8+esi+edi]                    ;291.8
        lea       eax, DWORD PTR [392+esp]                      ;
        mov       DWORD PTR [728+esp], ecx                      ;291.8
        lea       ecx, DWORD PTR [728+esp]                      ;291.8
        push      ecx                                           ;291.8
        push      OFFSET FLAT: __STRLITPACK_391.0.2             ;291.8
        push      eax                                           ;291.8
        call      _for_write_seq_fmt_xmit                       ;291.8
                                ; LOE ebx esi edi
.B2.116:                        ; Preds .B2.115
        mov       ecx, DWORD PTR [12+esi+edi]                   ;291.8
        lea       eax, DWORD PTR [404+esp]                      ;
        mov       DWORD PTR [748+esp], ecx                      ;291.8
        lea       ecx, DWORD PTR [748+esp]                      ;291.8
        push      ecx                                           ;291.8
        push      OFFSET FLAT: __STRLITPACK_392.0.2             ;291.8
        push      eax                                           ;291.8
        call      _for_write_seq_fmt_xmit                       ;291.8
                                ; LOE ebx esi edi
.B2.117:                        ; Preds .B2.116
        movss     xmm0, DWORD PTR [16+esi+edi]                  ;291.280
        lea       ecx, DWORD PTR [768+esp]                      ;291.8
        mulss     xmm0, DWORD PTR [_2il0floatpacket.6]          ;291.8
        lea       eax, DWORD PTR [416+esp]                      ;
        movss     DWORD PTR [768+esp], xmm0                     ;291.8
        push      ecx                                           ;291.8
        push      OFFSET FLAT: __STRLITPACK_393.0.2             ;291.8
        push      eax                                           ;291.8
        call      _for_write_seq_fmt_xmit                       ;291.8
                                ; LOE ebx esi
.B2.192:                        ; Preds .B2.117
        lea       eax, DWORD PTR [428+esp]                      ;
        add       esp, 76                                       ;291.8
                                ; LOE eax ebx esi
.B2.118:                        ; Preds .B2.192
        inc       ebx                                           ;293.5
        cmp       ebx, DWORD PTR [280+esp]                      ;293.5
        jle       .B2.113       ; Prob 82%                      ;293.5
                                ; LOE eax ebx esi
.B2.119:                        ; Preds .B2.118
        mov       esi, DWORD PTR [280+esp]                      ;
        mov       ebx, eax                                      ;
        mov       edi, DWORD PTR [108+esp]                      ;
                                ; LOE ebx esi edi
.B2.120:                        ; Preds .B2.110 .B2.119
        cmp       DWORD PTR [116+esp], 0                        ;295.16
        jne       .B2.123       ; Prob 50%                      ;295.16
                                ; LOE ebx esi edi
.B2.121:                        ; Preds .B2.120
        test      esi, esi                                      ;295.37
        jne       .B2.123       ; Prob 50%                      ;295.37
                                ; LOE ebx edi
.B2.122:                        ; Preds .B2.121
        xor       edx, edx                                      ;296.8
        mov       DWORD PTR [352+esp], edx                      ;296.8
        push      32                                            ;296.8
        push      OFFSET FLAT: PRINTHEADER1$format_pack.0.2+3192 ;296.8
        push      edx                                           ;296.8
        push      OFFSET FLAT: __STRLITPACK_394.0.2             ;296.8
        push      -2088435968                                   ;296.8
        push      666                                           ;296.8
        push      ebx                                           ;296.8
        call      _for_write_seq_fmt                            ;296.8
                                ; LOE ebx edi
.B2.193:                        ; Preds .B2.122
        add       esp, 28                                       ;296.8
                                ; LOE ebx edi
.B2.123:                        ; Preds .B2.193 .B2.121 .B2.120
        cmp       edi, 1                                        ;299.7
        je        .B2.137       ; Prob 20%                      ;299.7
                                ; LOE ebx edi
.B2.124:                        ; Preds .B2.123
        test      edi, edi                                      ;299.7
        je        .B2.130       ; Prob 25%                      ;299.7
                                ; LOE ebx edi
.B2.125:                        ; Preds .B2.124
        cmp       edi, 2                                        ;299.7
        jne       .B2.127       ; Prob 67%                      ;299.7
                                ; LOE ebx edi
.B2.126:                        ; Preds .B2.125
        xor       edx, edx                                      ;304.9
        push      edx                                           ;304.9
        push      edx                                           ;304.9
        push      17                                            ;304.9
        push      OFFSET FLAT: __STRLITPACK_138                 ;304.9
        push      edx                                           ;304.9
        push      80                                            ;304.9
        push      OFFSET FLAT: _PRINTHEADER1$PRINTSTR1.0.2      ;304.9
        call      _for_cpystr                                   ;304.9
        jmp       .B2.197       ; Prob 100%                     ;304.9
                                ; LOE ebx
.B2.127:                        ; Preds .B2.125
        cmp       edi, 3                                        ;299.7
        jne       .B2.129       ; Prob 50%                      ;299.7
                                ; LOE ebx
.B2.128:                        ; Preds .B2.127
        xor       edx, edx                                      ;306.9
        push      edx                                           ;306.9
        push      edx                                           ;306.9
        push      18                                            ;306.9
        push      OFFSET FLAT: __STRLITPACK_137                 ;306.9
        push      edx                                           ;306.9
        push      80                                            ;306.9
        push      OFFSET FLAT: _PRINTHEADER1$PRINTSTR1.0.2      ;306.9
        call      _for_cpystr                                   ;306.9
        jmp       .B2.197       ; Prob 100%                     ;306.9
                                ; LOE ebx
.B2.129:                        ; Preds .B2.127
        xor       edx, edx                                      ;308.9
        push      edx                                           ;308.9
        push      edx                                           ;308.9
        push      21                                            ;308.9
        push      OFFSET FLAT: __STRLITPACK_136                 ;308.9
        push      edx                                           ;308.9
        push      80                                            ;308.9
        push      OFFSET FLAT: _PRINTHEADER1$PRINTSTR1.0.2      ;308.9
        call      _for_cpystr                                   ;308.9
        jmp       .B2.197       ; Prob 100%                     ;308.9
                                ; LOE ebx
.B2.130:                        ; Preds .B2.124
        xor       edx, edx                                      ;302.9
        push      edx                                           ;302.9
        push      edx                                           ;302.9
        push      12                                            ;302.9
        push      OFFSET FLAT: __STRLITPACK_139                 ;302.9
        push      edx                                           ;302.9
        push      80                                            ;302.9
        push      OFFSET FLAT: _PRINTHEADER1$PRINTSTR1.0.2      ;302.9
        call      _for_cpystr                                   ;302.9
                                ; LOE ebx
.B2.197:                        ; Preds .B2.126 .B2.128 .B2.129 .B2.137 .B2.130
                                ;      
        add       esp, 28                                       ;302.9
                                ; LOE ebx
.B2.131:                        ; Preds .B2.197
        xor       edx, edx                                      ;311.7
        mov       DWORD PTR [352+esp], edx                      ;311.7
        push      32                                            ;311.7
        push      edx                                           ;311.7
        push      OFFSET FLAT: __STRLITPACK_395.0.2             ;311.7
        push      -2088435968                                   ;311.7
        push      666                                           ;311.7
        push      ebx                                           ;311.7
        call      _for_write_seq_lis                            ;311.7
                                ; LOE ebx
.B2.132:                        ; Preds .B2.131
        mov       DWORD PTR [376+esp], 0                        ;312.7
        lea       edx, DWORD PTR [304+esp]                      ;312.7
        mov       DWORD PTR [304+esp], 40                       ;312.7
        mov       DWORD PTR [308+esp], OFFSET FLAT: __STRLITPACK_134 ;312.7
        push      32                                            ;312.7
        push      edx                                           ;312.7
        push      OFFSET FLAT: __STRLITPACK_396.0.2             ;312.7
        push      -2088435968                                   ;312.7
        push      666                                           ;312.7
        push      ebx                                           ;312.7
        call      _for_write_seq_lis                            ;312.7
                                ; LOE ebx
.B2.133:                        ; Preds .B2.132
        mov       DWORD PTR [400+esp], 0                        ;313.7
        lea       edx, DWORD PTR [336+esp]                      ;313.7
        mov       DWORD PTR [336+esp], 40                       ;313.7
        mov       DWORD PTR [340+esp], OFFSET FLAT: __STRLITPACK_132 ;313.7
        push      32                                            ;313.7
        push      edx                                           ;313.7
        push      OFFSET FLAT: __STRLITPACK_397.0.2             ;313.7
        push      -2088435968                                   ;313.7
        push      666                                           ;313.7
        push      ebx                                           ;313.7
        call      _for_write_seq_lis                            ;313.7
                                ; LOE ebx
.B2.134:                        ; Preds .B2.133
        mov       DWORD PTR [424+esp], 0                        ;314.7
        lea       edx, DWORD PTR [368+esp]                      ;314.7
        mov       DWORD PTR [368+esp], 40                       ;314.7
        mov       DWORD PTR [372+esp], OFFSET FLAT: __STRLITPACK_130 ;314.7
        push      32                                            ;314.7
        push      edx                                           ;314.7
        push      OFFSET FLAT: __STRLITPACK_398.0.2             ;314.7
        push      -2088435968                                   ;314.7
        push      666                                           ;314.7
        push      ebx                                           ;314.7
        call      _for_write_seq_lis                            ;314.7
                                ; LOE ebx
.B2.135:                        ; Preds .B2.134
        xor       edx, edx                                      ;315.7
        mov       DWORD PTR [448+esp], edx                      ;315.7
        push      32                                            ;315.7
        push      edx                                           ;315.7
        push      OFFSET FLAT: __STRLITPACK_399.0.2             ;315.7
        push      -2088435968                                   ;315.7
        push      666                                           ;315.7
        push      ebx                                           ;315.7
        call      _for_write_seq_lis                            ;315.7
                                ; LOE
.B2.136:                        ; Preds .B2.135
        add       esp, 828                                      ;318.1
        pop       ebx                                           ;318.1
        pop       edi                                           ;318.1
        pop       esi                                           ;318.1
        mov       esp, ebp                                      ;318.1
        pop       ebp                                           ;318.1
        ret                                                     ;318.1
                                ; LOE
.B2.137:                        ; Preds .B2.123                 ; Infreq
        xor       edx, edx                                      ;300.9
        push      edx                                           ;300.9
        push      edx                                           ;300.9
        push      8                                             ;300.9
        push      OFFSET FLAT: __STRLITPACK_140                 ;300.9
        push      edx                                           ;300.9
        push      80                                            ;300.9
        push      OFFSET FLAT: _PRINTHEADER1$PRINTSTR1.0.2      ;300.9
        call      _for_cpystr                                   ;300.9
        jmp       .B2.197       ; Prob 100%                     ;300.9
                                ; LOE ebx
.B2.138:                        ; Preds .B2.93                  ; Infreq
        xor       edx, edx                                      ;261.4
        mov       DWORD PTR [352+esp], edx                      ;261.4
        push      32                                            ;261.4
        push      edx                                           ;261.4
        push      OFFSET FLAT: __STRLITPACK_368.0.2             ;261.4
        push      -2088435968                                   ;261.4
        push      666                                           ;261.4
        push      ebx                                           ;261.4
        call      _for_write_seq_lis                            ;261.4
                                ; LOE ebx esi edi
.B2.139:                        ; Preds .B2.138                 ; Infreq
        mov       DWORD PTR [376+esp], 0                        ;262.7
        lea       edx, DWORD PTR [328+esp]                      ;262.7
        mov       DWORD PTR [328+esp], edi                      ;262.7
        push      32                                            ;262.7
        push      OFFSET FLAT: PRINTHEADER1$format_pack.0.2+2400 ;262.7
        push      edx                                           ;262.7
        push      OFFSET FLAT: __STRLITPACK_369.0.2             ;262.7
        push      -2088435968                                   ;262.7
        push      666                                           ;262.7
        push      ebx                                           ;262.7
        call      _for_write_seq_fmt                            ;262.7
                                ; LOE ebx esi edi
.B2.140:                        ; Preds .B2.139                 ; Infreq
        mov       edx, DWORD PTR [312+esp]                      ;263.6
        mov       ecx, DWORD PTR [168+esp]                      ;263.89
        mov       DWORD PTR [404+esp], 0                        ;263.6
        imul      edx, DWORD PTR [edx+edi*4], 152               ;263.6
        imul      ecx, DWORD PTR [28+ecx+edx], 44               ;263.89
        mov       DWORD PTR [68+esp], edx                       ;263.6
        mov       edx, DWORD PTR [80+esp]                       ;263.6
        mov       edx, DWORD PTR [36+edx+ecx]                   ;263.6
        mov       DWORD PTR [364+esp], edx                      ;263.6
        lea       edx, DWORD PTR [364+esp]                      ;263.6
        push      32                                            ;263.6
        push      OFFSET FLAT: PRINTHEADER1$format_pack.0.2+2472 ;263.6
        push      edx                                           ;263.6
        push      OFFSET FLAT: __STRLITPACK_370.0.2             ;263.6
        push      -2088435968                                   ;263.6
        push      666                                           ;263.6
        push      ebx                                           ;263.6
        call      _for_write_seq_fmt                            ;263.6
                                ; LOE ebx esi edi
.B2.141:                        ; Preds .B2.140                 ; Infreq
        mov       ecx, DWORD PTR [96+esp]                       ;263.173
        mov       edx, DWORD PTR [196+esp]                      ;263.173
        imul      ecx, DWORD PTR [24+edx+ecx], 44               ;263.173
        mov       edx, DWORD PTR [108+esp]                      ;263.6
        mov       edx, DWORD PTR [36+edx+ecx]                   ;263.6
        mov       DWORD PTR [400+esp], edx                      ;263.6
        lea       edx, DWORD PTR [400+esp]                      ;263.6
        push      edx                                           ;263.6
        push      OFFSET FLAT: __STRLITPACK_371.0.2             ;263.6
        push      ebx                                           ;263.6
        call      _for_write_seq_fmt_xmit                       ;263.6
                                ; LOE ebx esi edi
.B2.142:                        ; Preds .B2.141                 ; Infreq
        mov       edx, DWORD PTR [360+esp]                      ;263.6
        mov       ecx, DWORD PTR [edx+edi*4]                    ;263.6
        lea       edx, DWORD PTR [420+esp]                      ;263.6
        mov       DWORD PTR [420+esp], ecx                      ;263.6
        push      edx                                           ;263.6
        push      OFFSET FLAT: __STRLITPACK_372.0.2             ;263.6
        push      ebx                                           ;263.6
        call      _for_write_seq_fmt_xmit                       ;263.6
                                ; LOE ebx esi edi
.B2.143:                        ; Preds .B2.142                 ; Infreq
        mov       edx, DWORD PTR [376+esp]                      ;263.6
        mov       ecx, DWORD PTR [edx+edi*4]                    ;263.6
        lea       edx, DWORD PTR [440+esp]                      ;263.6
        mov       DWORD PTR [440+esp], ecx                      ;263.6
        push      edx                                           ;263.6
        push      OFFSET FLAT: __STRLITPACK_373.0.2             ;263.6
        push      ebx                                           ;263.6
        call      _for_write_seq_fmt_xmit                       ;263.6
                                ; LOE ebx esi edi
.B2.200:                        ; Preds .B2.143                 ; Infreq
        add       esp, 116                                      ;263.6
                                ; LOE ebx esi edi
.B2.144:                        ; Preds .B2.200                 ; Infreq
        mov       edx, DWORD PTR [256+esp]                      ;264.20
        mov       DWORD PTR [352+esp], 0                        ;265.4
        cmp       DWORD PTR [edx+edi*4], 1                      ;264.20
        je        .B2.147       ; Prob 16%                      ;264.20
                                ; LOE ebx esi edi
.B2.145:                        ; Preds .B2.144                 ; Infreq
        push      32                                            ;269.4
        push      OFFSET FLAT: PRINTHEADER1$format_pack.0.2+2652 ;269.4
        push      0                                             ;269.4
        push      OFFSET FLAT: __STRLITPACK_375.0.2             ;269.4
        push      -2088435968                                   ;269.4
        push      666                                           ;269.4
        push      ebx                                           ;269.4
        call      _for_write_seq_fmt                            ;269.4
                                ; LOE ebx esi edi
.B2.201:                        ; Preds .B2.147 .B2.145         ; Infreq
        add       esp, 28                                       ;269.4
                                ; LOE ebx esi edi
.B2.146:                        ; Preds .B2.201                 ; Infreq
        mov       edx, DWORD PTR [264+esp]                      ;271.4
        mov       DWORD PTR [352+esp], 0                        ;271.4
        mov       ecx, DWORD PTR [edx+edi*4]                    ;271.4
        lea       edx, DWORD PTR [344+esp]                      ;271.4
        mov       DWORD PTR [344+esp], ecx                      ;271.4
        push      32                                            ;271.4
        push      OFFSET FLAT: PRINTHEADER1$format_pack.0.2+2720 ;271.4
        push      edx                                           ;271.4
        push      OFFSET FLAT: __STRLITPACK_376.0.2             ;271.4
        push      -2088435968                                   ;271.4
        push      666                                           ;271.4
        push      ebx                                           ;271.4
        call      _for_write_seq_fmt                            ;271.4
                                ; LOE ebx esi edi
.B2.202:                        ; Preds .B2.146                 ; Infreq
        add       esp, 28                                       ;271.4
        jmp       .B2.94        ; Prob 100%                     ;271.4
                                ; LOE ebx esi edi
.B2.147:                        ; Preds .B2.144                 ; Infreq
        push      32                                            ;265.4
        push      OFFSET FLAT: PRINTHEADER1$format_pack.0.2+2584 ;265.4
        push      0                                             ;265.4
        push      OFFSET FLAT: __STRLITPACK_374.0.2             ;265.4
        push      -2088435968                                   ;265.4
        push      666                                           ;265.4
        push      ebx                                           ;265.4
        call      _for_write_seq_fmt                            ;265.4
        jmp       .B2.201       ; Prob 100%                     ;265.4
                                ; LOE ebx esi edi
.B2.148:                        ; Preds .B2.92                  ; Infreq
        xor       edx, edx                                      ;254.7
        mov       DWORD PTR [352+esp], edx                      ;254.7
        push      32                                            ;254.7
        push      edx                                           ;254.7
        push      OFFSET FLAT: __STRLITPACK_361.0.2             ;254.7
        push      -2088435968                                   ;254.7
        push      666                                           ;254.7
        push      ebx                                           ;254.7
        call      _for_write_seq_lis                            ;254.7
                                ; LOE ebx esi edi
.B2.149:                        ; Preds .B2.148                 ; Infreq
        mov       DWORD PTR [376+esp], 0                        ;255.4
        lea       edx, DWORD PTR [408+esp]                      ;255.4
        mov       DWORD PTR [408+esp], edi                      ;255.4
        push      32                                            ;255.4
        push      OFFSET FLAT: PRINTHEADER1$format_pack.0.2+2168 ;255.4
        push      edx                                           ;255.4
        push      OFFSET FLAT: __STRLITPACK_362.0.2             ;255.4
        push      -2088435968                                   ;255.4
        push      666                                           ;255.4
        push      ebx                                           ;255.4
        call      _for_write_seq_fmt                            ;255.4
                                ; LOE ebx esi edi
.B2.150:                        ; Preds .B2.149                 ; Infreq
        mov       edx, DWORD PTR [312+esp]                      ;256.6
        mov       ecx, DWORD PTR [168+esp]                      ;256.89
        mov       DWORD PTR [404+esp], 0                        ;256.6
        imul      edx, DWORD PTR [edx+edi*4], 152               ;256.6
        imul      ecx, DWORD PTR [28+ecx+edx], 44               ;256.89
        mov       DWORD PTR [72+esp], edx                       ;256.6
        mov       edx, DWORD PTR [80+esp]                       ;256.6
        mov       edx, DWORD PTR [36+edx+ecx]                   ;256.6
        mov       DWORD PTR [444+esp], edx                      ;256.6
        lea       edx, DWORD PTR [444+esp]                      ;256.6
        push      32                                            ;256.6
        push      OFFSET FLAT: PRINTHEADER1$format_pack.0.2+2236 ;256.6
        push      edx                                           ;256.6
        push      OFFSET FLAT: __STRLITPACK_363.0.2             ;256.6
        push      -2088435968                                   ;256.6
        push      666                                           ;256.6
        push      ebx                                           ;256.6
        call      _for_write_seq_fmt                            ;256.6
                                ; LOE ebx esi edi
.B2.151:                        ; Preds .B2.150                 ; Infreq
        mov       ecx, DWORD PTR [100+esp]                      ;256.173
        mov       edx, DWORD PTR [196+esp]                      ;256.173
        imul      ecx, DWORD PTR [24+edx+ecx], 44               ;256.173
        mov       edx, DWORD PTR [108+esp]                      ;256.6
        mov       edx, DWORD PTR [36+edx+ecx]                   ;256.6
        mov       DWORD PTR [480+esp], edx                      ;256.6
        lea       edx, DWORD PTR [480+esp]                      ;256.6
        push      edx                                           ;256.6
        push      OFFSET FLAT: __STRLITPACK_364.0.2             ;256.6
        push      ebx                                           ;256.6
        call      _for_write_seq_fmt_xmit                       ;256.6
                                ; LOE ebx esi edi
.B2.152:                        ; Preds .B2.151                 ; Infreq
        mov       edx, DWORD PTR [360+esp]                      ;256.6
        mov       ecx, DWORD PTR [edx+edi*4]                    ;256.6
        lea       edx, DWORD PTR [500+esp]                      ;256.6
        mov       DWORD PTR [500+esp], ecx                      ;256.6
        push      edx                                           ;256.6
        push      OFFSET FLAT: __STRLITPACK_365.0.2             ;256.6
        push      ebx                                           ;256.6
        call      _for_write_seq_fmt_xmit                       ;256.6
                                ; LOE ebx esi edi
.B2.153:                        ; Preds .B2.152                 ; Infreq
        mov       edx, DWORD PTR [376+esp]                      ;256.6
        mov       ecx, DWORD PTR [edx+edi*4]                    ;256.6
        lea       edx, DWORD PTR [520+esp]                      ;256.6
        mov       DWORD PTR [520+esp], ecx                      ;256.6
        push      edx                                           ;256.6
        push      OFFSET FLAT: __STRLITPACK_366.0.2             ;256.6
        push      ebx                                           ;256.6
        call      _for_write_seq_fmt_xmit                       ;256.6
                                ; LOE ebx esi edi
.B2.204:                        ; Preds .B2.153                 ; Infreq
        add       esp, 116                                      ;256.6
                                ; LOE ebx esi edi
.B2.154:                        ; Preds .B2.204                 ; Infreq
        xor       edx, edx                                      ;257.4
        mov       DWORD PTR [352+esp], edx                      ;257.4
        push      32                                            ;257.4
        push      OFFSET FLAT: PRINTHEADER1$format_pack.0.2+2348 ;257.4
        push      edx                                           ;257.4
        push      OFFSET FLAT: __STRLITPACK_367.0.2             ;257.4
        push      -2088435968                                   ;257.4
        push      666                                           ;257.4
        push      ebx                                           ;257.4
        call      _for_write_seq_fmt                            ;257.4
                                ; LOE ebx esi edi
.B2.205:                        ; Preds .B2.154                 ; Infreq
        add       esp, 28                                       ;257.4
        jmp       .B2.94        ; Prob 100%                     ;257.4
                                ; LOE ebx esi edi
.B2.155:                        ; Preds .B2.91                  ; Infreq
        xor       edx, edx                                      ;247.7
        mov       DWORD PTR [352+esp], edx                      ;247.7
        push      32                                            ;247.7
        push      edx                                           ;247.7
        push      OFFSET FLAT: __STRLITPACK_353.0.2             ;247.7
        push      -2088435968                                   ;247.7
        push      666                                           ;247.7
        push      ebx                                           ;247.7
        call      _for_write_seq_lis                            ;247.7
                                ; LOE ebx esi edi
.B2.156:                        ; Preds .B2.155                 ; Infreq
        mov       DWORD PTR [376+esp], 0                        ;248.4
        lea       edx, DWORD PTR [448+esp]                      ;248.4
        mov       DWORD PTR [448+esp], edi                      ;248.4
        push      32                                            ;248.4
        push      OFFSET FLAT: PRINTHEADER1$format_pack.0.2+1876 ;248.4
        push      edx                                           ;248.4
        push      OFFSET FLAT: __STRLITPACK_354.0.2             ;248.4
        push      -2088435968                                   ;248.4
        push      666                                           ;248.4
        push      ebx                                           ;248.4
        call      _for_write_seq_fmt                            ;248.4
                                ; LOE ebx esi edi
.B2.157:                        ; Preds .B2.156                 ; Infreq
        mov       edx, DWORD PTR [312+esp]                      ;249.6
        mov       ecx, DWORD PTR [168+esp]                      ;249.89
        mov       DWORD PTR [404+esp], 0                        ;249.6
        imul      edx, DWORD PTR [edx+edi*4], 152               ;249.6
        imul      ecx, DWORD PTR [28+ecx+edx], 44               ;249.89
        mov       DWORD PTR [76+esp], edx                       ;249.6
        mov       edx, DWORD PTR [80+esp]                       ;249.6
        mov       edx, DWORD PTR [36+edx+ecx]                   ;249.6
        mov       DWORD PTR [484+esp], edx                      ;249.6
        lea       edx, DWORD PTR [484+esp]                      ;249.6
        push      32                                            ;249.6
        push      OFFSET FLAT: PRINTHEADER1$format_pack.0.2+1944 ;249.6
        push      edx                                           ;249.6
        push      OFFSET FLAT: __STRLITPACK_355.0.2             ;249.6
        push      -2088435968                                   ;249.6
        push      666                                           ;249.6
        push      ebx                                           ;249.6
        call      _for_write_seq_fmt                            ;249.6
                                ; LOE ebx esi edi
.B2.158:                        ; Preds .B2.157                 ; Infreq
        mov       ecx, DWORD PTR [104+esp]                      ;249.173
        mov       edx, DWORD PTR [196+esp]                      ;249.173
        imul      ecx, DWORD PTR [24+edx+ecx], 44               ;249.173
        mov       edx, DWORD PTR [108+esp]                      ;249.6
        mov       edx, DWORD PTR [36+edx+ecx]                   ;249.6
        mov       DWORD PTR [520+esp], edx                      ;249.6
        lea       edx, DWORD PTR [520+esp]                      ;249.6
        push      edx                                           ;249.6
        push      OFFSET FLAT: __STRLITPACK_356.0.2             ;249.6
        push      ebx                                           ;249.6
        call      _for_write_seq_fmt_xmit                       ;249.6
                                ; LOE ebx esi edi
.B2.159:                        ; Preds .B2.158                 ; Infreq
        mov       edx, DWORD PTR [360+esp]                      ;249.6
        mov       ecx, DWORD PTR [edx+edi*4]                    ;249.6
        lea       edx, DWORD PTR [540+esp]                      ;249.6
        mov       DWORD PTR [540+esp], ecx                      ;249.6
        push      edx                                           ;249.6
        push      OFFSET FLAT: __STRLITPACK_357.0.2             ;249.6
        push      ebx                                           ;249.6
        call      _for_write_seq_fmt_xmit                       ;249.6
                                ; LOE ebx esi edi
.B2.160:                        ; Preds .B2.159                 ; Infreq
        mov       edx, DWORD PTR [376+esp]                      ;249.6
        mov       ecx, DWORD PTR [edx+edi*4]                    ;249.6
        lea       edx, DWORD PTR [560+esp]                      ;249.6
        mov       DWORD PTR [560+esp], ecx                      ;249.6
        push      edx                                           ;249.6
        push      OFFSET FLAT: __STRLITPACK_358.0.2             ;249.6
        push      ebx                                           ;249.6
        call      _for_write_seq_fmt_xmit                       ;249.6
                                ; LOE ebx esi edi
.B2.206:                        ; Preds .B2.160                 ; Infreq
        add       esp, 116                                      ;249.6
                                ; LOE ebx esi edi
.B2.161:                        ; Preds .B2.206                 ; Infreq
        mov       edx, DWORD PTR [264+esp]                      ;250.6
        lea       ecx, DWORD PTR [464+esp]                      ;250.6
        mov       DWORD PTR [352+esp], 0                        ;250.6
        cvtsi2ss  xmm0, DWORD PTR [edx+edi*4]                   ;250.6
        movss     DWORD PTR [464+esp], xmm0                     ;250.6
        push      32                                            ;250.6
        push      OFFSET FLAT: PRINTHEADER1$format_pack.0.2+2056 ;250.6
        push      ecx                                           ;250.6
        push      OFFSET FLAT: __STRLITPACK_359.0.2             ;250.6
        push      -2088435968                                   ;250.6
        push      666                                           ;250.6
        push      ebx                                           ;250.6
        call      _for_write_seq_fmt                            ;250.6
                                ; LOE ebx esi edi
.B2.162:                        ; Preds .B2.161                 ; Infreq
        mov       edx, DWORD PTR [284+esp]                      ;250.6
        lea       ecx, DWORD PTR [500+esp]                      ;250.6
        cvtsi2ss  xmm0, DWORD PTR [edx+edi*4]                   ;250.6
        movss     DWORD PTR [500+esp], xmm0                     ;250.6
        push      ecx                                           ;250.6
        push      OFFSET FLAT: __STRLITPACK_360.0.2             ;250.6
        push      ebx                                           ;250.6
        call      _for_write_seq_fmt_xmit                       ;250.6
                                ; LOE ebx esi edi
.B2.207:                        ; Preds .B2.162                 ; Infreq
        add       esp, 40                                       ;250.6
        jmp       .B2.94        ; Prob 100%                     ;250.6
                                ; LOE ebx esi edi
.B2.163:                        ; Preds .B2.88                  ; Infreq
        push      32                                            ;275.4
        push      OFFSET FLAT: PRINTHEADER1$format_pack.0.2+2812 ;275.4
        push      0                                             ;275.4
        push      OFFSET FLAT: __STRLITPACK_377.0.2             ;275.4
        push      -2088435968                                   ;275.4
        push      666                                           ;275.4
        push      ebx                                           ;275.4
        call      _for_write_seq_fmt                            ;275.4
                                ; LOE ebx edi
.B2.208:                        ; Preds .B2.163                 ; Infreq
        add       esp, 28                                       ;275.4
        jmp       .B2.96        ; Prob 100%                     ;275.4
                                ; LOE ebx edi
.B2.164:                        ; Preds .B2.16                  ; Infreq
        xor       edx, edx                                      ;179.54
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_NODETMP+20], edx ;179.54
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_NODETMP+16], edx ;179.54
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NODETMP+4] ;184.7
        mov       DWORD PTR [16+esp], edx                       ;184.7
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NODETMP+8] ;185.7
        mov       DWORD PTR [12+esp], edx                       ;185.7
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NODETMP] ;183.7
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NODETMP+12] ;187.7
        mov       DWORD PTR [20+esp], ecx                       ;183.7
        mov       DWORD PTR [8+esp], edx                        ;187.7
        jmp       .B2.31        ; Prob 100%                     ;187.7
        ALIGN     16
                                ; LOE ebx esi edi
; mark_end;
_PRINTHEADER1 ENDP
_TEXT	ENDS
_BSS	SEGMENT  DWORD PUBLIC FLAT  'BSS'
	ALIGN 004H
_PRINTHEADER1$PRINTSTR1.0.2	DB ?	; pad
	ORG $+78	; pad
	DB ?	; pad
_BSS	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
PRINTHEADER1$format_pack.0.2	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	47
	DB	0
	DB	32
	DB	32
	DB	32
	DB	32
	DB	78
	DB	117
	DB	109
	DB	98
	DB	101
	DB	114
	DB	32
	DB	111
	DB	102
	DB	32
	DB	78
	DB	111
	DB	100
	DB	101
	DB	115
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	58
	DB	32
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	47
	DB	0
	DB	32
	DB	32
	DB	32
	DB	32
	DB	78
	DB	117
	DB	109
	DB	98
	DB	101
	DB	114
	DB	32
	DB	111
	DB	102
	DB	32
	DB	76
	DB	105
	DB	110
	DB	107
	DB	115
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	58
	DB	32
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	47
	DB	0
	DB	32
	DB	32
	DB	32
	DB	32
	DB	78
	DB	117
	DB	109
	DB	98
	DB	101
	DB	114
	DB	32
	DB	111
	DB	102
	DB	32
	DB	90
	DB	111
	DB	110
	DB	101
	DB	115
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	58
	DB	32
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	47
	DB	0
	DB	32
	DB	32
	DB	32
	DB	32
	DB	78
	DB	117
	DB	109
	DB	98
	DB	101
	DB	114
	DB	32
	DB	111
	DB	102
	DB	32
	DB	78
	DB	111
	DB	32
	DB	67
	DB	111
	DB	110
	DB	116
	DB	114
	DB	111
	DB	108
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	58
	DB	32
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	47
	DB	0
	DB	32
	DB	32
	DB	32
	DB	32
	DB	78
	DB	117
	DB	109
	DB	98
	DB	101
	DB	114
	DB	32
	DB	111
	DB	102
	DB	32
	DB	89
	DB	105
	DB	101
	DB	108
	DB	100
	DB	32
	DB	83
	DB	105
	DB	103
	DB	110
	DB	115
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	58
	DB	32
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	47
	DB	0
	DB	32
	DB	32
	DB	32
	DB	32
	DB	78
	DB	117
	DB	109
	DB	98
	DB	101
	DB	114
	DB	32
	DB	111
	DB	102
	DB	32
	DB	52
	DB	45
	DB	87
	DB	97
	DB	121
	DB	32
	DB	83
	DB	84
	DB	79
	DB	80
	DB	32
	DB	83
	DB	105
	DB	103
	DB	110
	DB	115
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	58
	DB	32
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	47
	DB	0
	DB	32
	DB	32
	DB	32
	DB	32
	DB	78
	DB	117
	DB	109
	DB	98
	DB	101
	DB	114
	DB	32
	DB	111
	DB	102
	DB	32
	DB	50
	DB	45
	DB	87
	DB	97
	DB	121
	DB	32
	DB	83
	DB	84
	DB	79
	DB	80
	DB	32
	DB	83
	DB	105
	DB	103
	DB	110
	DB	115
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	58
	DB	32
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	47
	DB	0
	DB	32
	DB	32
	DB	32
	DB	32
	DB	78
	DB	117
	DB	109
	DB	98
	DB	101
	DB	114
	DB	32
	DB	111
	DB	102
	DB	32
	DB	80
	DB	114
	DB	101
	DB	116
	DB	105
	DB	109
	DB	101
	DB	100
	DB	32
	DB	67
	DB	111
	DB	110
	DB	116
	DB	114
	DB	111
	DB	108
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	58
	DB	32
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	47
	DB	0
	DB	32
	DB	32
	DB	32
	DB	32
	DB	78
	DB	117
	DB	109
	DB	98
	DB	101
	DB	114
	DB	32
	DB	111
	DB	102
	DB	32
	DB	65
	DB	99
	DB	116
	DB	117
	DB	97
	DB	116
	DB	101
	DB	100
	DB	32
	DB	67
	DB	111
	DB	110
	DB	116
	DB	114
	DB	111
	DB	108
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	58
	DB	32
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	47
	DB	0
	DB	32
	DB	32
	DB	32
	DB	32
	DB	78
	DB	117
	DB	109
	DB	98
	DB	101
	DB	114
	DB	32
	DB	111
	DB	102
	DB	32
	DB	82
	DB	97
	DB	109
	DB	112
	DB	32
	DB	67
	DB	111
	DB	110
	DB	116
	DB	114
	DB	111
	DB	108
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	58
	DB	32
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	47
	DB	0
	DB	32
	DB	32
	DB	32
	DB	32
	DB	78
	DB	117
	DB	109
	DB	98
	DB	101
	DB	114
	DB	32
	DB	111
	DB	102
	DB	32
	DB	86
	DB	77
	DB	83
	DB	32
	DB	67
	DB	111
	DB	110
	DB	116
	DB	114
	DB	111
	DB	108
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	58
	DB	32
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	47
	DB	0
	DB	32
	DB	32
	DB	32
	DB	32
	DB	77
	DB	97
	DB	120
	DB	46
	DB	32
	DB	78
	DB	117
	DB	109
	DB	98
	DB	101
	DB	114
	DB	32
	DB	111
	DB	102
	DB	32
	DB	73
	DB	116
	DB	101
	DB	114
	DB	97
	DB	116
	DB	105
	DB	111
	DB	110
	DB	115
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	58
	DB	32
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	47
	DB	0
	DB	32
	DB	32
	DB	32
	DB	32
	DB	67
	DB	117
	DB	114
	DB	114
	DB	101
	DB	110
	DB	116
	DB	32
	DB	73
	DB	116
	DB	101
	DB	114
	DB	97
	DB	116
	DB	105
	DB	111
	DB	110
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	58
	DB	32
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	47
	DB	0
	DB	32
	DB	32
	DB	32
	DB	32
	DB	80
	DB	108
	DB	97
	DB	110
	DB	110
	DB	105
	DB	110
	DB	103
	DB	32
	DB	72
	DB	111
	DB	114
	DB	105
	DB	122
	DB	111
	DB	110
	DB	40
	DB	109
	DB	105
	DB	110
	DB	41
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	58
	DB	32
	DB	0
	DB	33
	DB	0
	DB	0
	DB	1
	DB	1
	DB	0
	DB	0
	DB	0
	DB	9
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	47
	DB	0
	DB	32
	DB	32
	DB	32
	DB	32
	DB	65
	DB	103
	DB	103
	DB	114
	DB	101
	DB	103
	DB	97
	DB	116
	DB	105
	DB	111
	DB	110
	DB	32
	DB	73
	DB	110
	DB	116
	DB	101
	DB	114
	DB	118
	DB	97
	DB	108
	DB	40
	DB	35
	DB	32
	DB	111
	DB	102
	DB	32
	DB	83
	DB	105
	DB	109
	DB	32
	DB	73
	DB	110
	DB	116
	DB	41
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	58
	DB	32
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	47
	DB	0
	DB	32
	DB	32
	DB	32
	DB	32
	DB	65
	DB	115
	DB	115
	DB	105
	DB	103
	DB	110
	DB	109
	DB	101
	DB	110
	DB	116
	DB	32
	DB	73
	DB	110
	DB	116
	DB	101
	DB	114
	DB	118
	DB	97
	DB	108
	DB	40
	DB	35
	DB	32
	DB	111
	DB	102
	DB	32
	DB	83
	DB	105
	DB	109
	DB	32
	DB	73
	DB	110
	DB	116
	DB	41
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	58
	DB	32
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	47
	DB	0
	DB	32
	DB	32
	DB	32
	DB	32
	DB	77
	DB	97
	DB	120
	DB	32
	DB	35
	DB	32
	DB	111
	DB	102
	DB	32
	DB	73
	DB	116
	DB	101
	DB	114
	DB	97
	DB	116
	DB	105
	DB	111
	DB	110
	DB	115
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	58
	DB	32
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	47
	DB	0
	DB	32
	DB	32
	DB	32
	DB	32
	DB	109
	DB	116
	DB	99
	DB	32
	DB	84
	DB	104
	DB	114
	DB	101
	DB	115
	DB	104
	DB	111
	DB	108
	DB	100
	DB	32
	DB	40
	DB	35
	DB	32
	DB	111
	DB	102
	DB	32
	DB	86
	DB	101
	DB	104
	DB	105
	DB	99
	DB	108
	DB	101
	DB	115
	DB	41
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	58
	DB	32
	DB	0
	DB	33
	DB	0
	DB	0
	DB	1
	DB	1
	DB	0
	DB	0
	DB	0
	DB	9
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	47
	DB	0
	DB	32
	DB	32
	DB	32
	DB	32
	DB	67
	DB	111
	DB	110
	DB	118
	DB	101
	DB	114
	DB	103
	DB	101
	DB	110
	DB	99
	DB	101
	DB	32
	DB	84
	DB	104
	DB	114
	DB	101
	DB	115
	DB	104
	DB	111
	DB	108
	DB	100
	DB	32
	DB	45
	DB	32
	DB	71
	DB	97
	DB	112
	DB	32
	DB	70
	DB	117
	DB	110
	DB	99
	DB	116
	DB	105
	DB	111
	DB	110
	DB	32
	DB	37
	DB	32
	DB	32
	DB	32
	DB	58
	DB	32
	DB	0
	DB	33
	DB	0
	DB	0
	DB	2
	DB	1
	DB	0
	DB	0
	DB	0
	DB	5
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	54
	DB	0
	DB	32
	DB	32
	DB	32
	DB	32
	DB	79
	DB	45
	DB	68
	DB	32
	DB	68
	DB	101
	DB	109
	DB	97
	DB	110
	DB	100
	DB	32
	DB	84
	DB	97
	DB	98
	DB	108
	DB	101
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	51
	DB	0
	DB	32
	DB	32
	DB	32
	DB	32
	DB	86
	DB	101
	DB	104
	DB	105
	DB	99
	DB	108
	DB	101
	DB	32
	DB	70
	DB	105
	DB	108
	DB	101
	DB	44
	DB	32
	DB	73
	DB	110
	DB	105
	DB	116
	DB	105
	DB	97
	DB	108
	DB	32
	DB	80
	DB	97
	DB	116
	DB	104
	DB	32
	DB	71
	DB	101
	DB	110
	DB	101
	DB	114
	DB	97
	DB	116
	DB	101
	DB	100
	DB	32
	DB	98
	DB	121
	DB	32
	DB	68
	DB	121
	DB	110
	DB	117
	DB	115
	DB	84
	DB	32
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	54
	DB	0
	DB	32
	DB	32
	DB	32
	DB	32
	DB	86
	DB	101
	DB	104
	DB	105
	DB	99
	DB	108
	DB	101
	DB	32
	DB	43
	DB	32
	DB	80
	DB	97
	DB	116
	DB	104
	DB	32
	DB	70
	DB	105
	DB	108
	DB	101
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	54
	DB	0
	DB	32
	DB	32
	DB	32
	DB	32
	DB	100
	DB	101
	DB	109
	DB	97
	DB	110
	DB	100
	DB	32
	DB	43
	DB	32
	DB	86
	DB	101
	DB	104
	DB	105
	DB	99
	DB	108
	DB	101
	DB	32
	DB	105
	DB	108
	DB	101
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	54
	DB	0
	DB	32
	DB	32
	DB	32
	DB	32
	DB	100
	DB	101
	DB	109
	DB	97
	DB	110
	DB	100
	DB	43
	DB	86
	DB	101
	DB	104
	DB	105
	DB	99
	DB	108
	DB	101
	DB	43
	DB	80
	DB	97
	DB	116
	DB	104
	DB	32
	DB	70
	DB	105
	DB	108
	DB	101
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	45
	DB	0
	DB	84
	DB	114
	DB	105
	DB	112
	DB	109
	DB	97
	DB	107
	DB	101
	DB	114
	DB	115
	DB	32
	DB	112
	DB	101
	DB	114
	DB	99
	DB	101
	DB	105
	DB	118
	DB	101
	DB	32
	DB	102
	DB	114
	DB	101
	DB	101
	DB	119
	DB	97
	DB	121
	DB	32
	DB	116
	DB	114
	DB	97
	DB	118
	DB	101
	DB	108
	DB	32
	DB	116
	DB	105
	DB	109
	DB	101
	DB	32
	DB	116
	DB	111
	DB	32
	DB	98
	DB	101
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	1
	DB	1
	DB	0
	DB	0
	DB	0
	DB	6
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	23
	DB	0
	DB	32
	DB	37
	DB	32
	DB	108
	DB	111
	DB	119
	DB	101
	DB	114
	DB	32
	DB	116
	DB	104
	DB	97
	DB	110
	DB	32
	DB	97
	DB	114
	DB	116
	DB	101
	DB	114
	DB	105
	DB	97
	DB	108
	DB	115
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	47
	DB	0
	DB	32
	DB	32
	DB	32
	DB	32
	DB	78
	DB	117
	DB	109
	DB	98
	DB	101
	DB	114
	DB	32
	DB	111
	DB	102
	DB	32
	DB	65
	DB	99
	DB	116
	DB	105
	DB	118
	DB	97
	DB	116
	DB	101
	DB	100
	DB	32
	DB	68
	DB	121
	DB	110
	DB	97
	DB	109
	DB	105
	DB	99
	DB	32
	DB	77
	DB	101
	DB	115
	DB	115
	DB	97
	DB	103
	DB	101
	DB	32
	DB	83
	DB	105
	DB	103
	DB	110
	DB	115
	DB	58
	DB	32
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	4
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	12
	DB	0
	DB	32
	DB	32
	DB	32
	DB	32
	DB	86
	DB	77
	DB	83
	DB	32
	DB	35
	DB	32
	DB	32
	DB	32
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	3
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	25
	DB	0
	DB	32
	DB	32
	DB	32
	DB	32
	DB	84
	DB	121
	DB	112
	DB	101
	DB	58
	DB	32
	DB	32
	DB	83
	DB	112
	DB	101
	DB	101
	DB	100
	DB	32
	DB	65
	DB	100
	DB	118
	DB	105
	DB	115
	DB	111
	DB	114
	DB	121
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	15
	DB	0
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	76
	DB	111
	DB	99
	DB	97
	DB	116
	DB	105
	DB	111
	DB	110
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	5
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	4
	DB	0
	DB	32
	DB	32
	DB	45
	DB	45
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	5
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	12
	DB	0
	DB	32
	DB	32
	DB	32
	DB	70
	DB	114
	DB	111
	DB	109
	DB	32
	DB	109
	DB	105
	DB	110
	DB	32
	DB	33
	DB	0
	DB	0
	DB	1
	DB	1
	DB	0
	DB	0
	DB	0
	DB	6
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	8
	DB	0
	DB	32
	DB	84
	DB	111
	DB	32
	DB	109
	DB	105
	DB	110
	DB	32
	DB	33
	DB	0
	DB	0
	DB	1
	DB	1
	DB	0
	DB	0
	DB	0
	DB	6
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	24
	DB	0
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	83
	DB	112
	DB	101
	DB	101
	DB	100
	DB	32
	DB	84
	DB	104
	DB	114
	DB	101
	DB	115
	DB	104
	DB	111
	DB	108
	DB	100
	DB	32
	DB	32
	DB	33
	DB	0
	DB	0
	DB	1
	DB	1
	DB	0
	DB	0
	DB	0
	DB	5
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	37
	DB	0
	DB	32
	DB	109
	DB	112
	DB	104
	DB	46
	DB	32
	DB	32
	DB	32
	DB	83
	DB	112
	DB	101
	DB	101
	DB	100
	DB	32
	DB	65
	DB	100
	DB	106
	DB	117
	DB	115
	DB	116
	DB	109
	DB	101
	DB	110
	DB	116
	DB	32
	DB	80
	DB	101
	DB	114
	DB	99
	DB	101
	DB	110
	DB	116
	DB	97
	DB	103
	DB	101
	DB	58
	DB	32
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	1
	DB	1
	DB	0
	DB	0
	DB	0
	DB	5
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	2
	DB	0
	DB	32
	DB	37
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	12
	DB	0
	DB	32
	DB	32
	DB	32
	DB	32
	DB	86
	DB	77
	DB	83
	DB	32
	DB	35
	DB	32
	DB	32
	DB	32
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	3
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	27
	DB	0
	DB	32
	DB	32
	DB	32
	DB	32
	DB	84
	DB	121
	DB	112
	DB	101
	DB	58
	DB	32
	DB	32
	DB	77
	DB	97
	DB	110
	DB	100
	DB	97
	DB	116
	DB	111
	DB	114
	DB	121
	DB	32
	DB	68
	DB	101
	DB	116
	DB	111
	DB	117
	DB	114
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	15
	DB	0
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	76
	DB	111
	DB	99
	DB	97
	DB	116
	DB	105
	DB	111
	DB	110
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	5
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	4
	DB	0
	DB	32
	DB	32
	DB	45
	DB	45
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	5
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	12
	DB	0
	DB	32
	DB	32
	DB	32
	DB	70
	DB	114
	DB	111
	DB	109
	DB	32
	DB	109
	DB	105
	DB	110
	DB	32
	DB	33
	DB	0
	DB	0
	DB	1
	DB	1
	DB	0
	DB	0
	DB	0
	DB	6
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	8
	DB	0
	DB	32
	DB	84
	DB	111
	DB	32
	DB	109
	DB	105
	DB	110
	DB	32
	DB	33
	DB	0
	DB	0
	DB	1
	DB	1
	DB	0
	DB	0
	DB	0
	DB	6
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	40
	DB	0
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	68
	DB	105
	DB	118
	DB	101
	DB	114
	DB	115
	DB	105
	DB	111
	DB	110
	DB	32
	DB	65
	DB	112
	DB	112
	DB	108
	DB	105
	DB	101
	DB	115
	DB	32
	DB	84
	DB	111
	DB	32
	DB	65
	DB	108
	DB	108
	DB	32
	DB	86
	DB	101
	DB	104
	DB	105
	DB	99
	DB	108
	DB	101
	DB	115
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	12
	DB	0
	DB	32
	DB	32
	DB	32
	DB	32
	DB	86
	DB	77
	DB	83
	DB	32
	DB	35
	DB	32
	DB	32
	DB	32
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	3
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	29
	DB	0
	DB	32
	DB	32
	DB	32
	DB	32
	DB	84
	DB	121
	DB	112
	DB	101
	DB	58
	DB	32
	DB	32
	DB	67
	DB	111
	DB	110
	DB	103
	DB	101
	DB	115
	DB	116
	DB	105
	DB	111
	DB	110
	DB	32
	DB	87
	DB	97
	DB	114
	DB	110
	DB	105
	DB	110
	DB	103
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	15
	DB	0
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	76
	DB	111
	DB	99
	DB	97
	DB	116
	DB	105
	DB	111
	DB	110
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	5
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	4
	DB	0
	DB	32
	DB	32
	DB	45
	DB	45
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	5
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	12
	DB	0
	DB	32
	DB	32
	DB	32
	DB	70
	DB	114
	DB	111
	DB	109
	DB	32
	DB	109
	DB	105
	DB	110
	DB	32
	DB	33
	DB	0
	DB	0
	DB	1
	DB	1
	DB	0
	DB	0
	DB	0
	DB	6
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	8
	DB	0
	DB	32
	DB	84
	DB	111
	DB	32
	DB	109
	DB	105
	DB	110
	DB	32
	DB	33
	DB	0
	DB	0
	DB	1
	DB	1
	DB	0
	DB	0
	DB	0
	DB	6
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	54
	DB	0
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	84
	DB	104
	DB	101
	DB	32
	DB	66
	DB	101
	DB	115
	DB	116
	DB	32
	DB	80
	DB	97
	DB	116
	DB	104
	DB	32
	DB	105
	DB	115
	DB	32
	DB	65
	DB	115
	DB	115
	DB	105
	DB	103
	DB	110
	DB	101
	DB	100
	DB	32
	DB	116
	DB	111
	DB	32
	DB	82
	DB	101
	DB	115
	DB	112
	DB	111
	DB	110
	DB	100
	DB	101
	DB	100
	DB	32
	DB	86
	DB	101
	DB	104
	DB	105
	DB	99
	DB	108
	DB	101
	DB	115
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	54
	DB	0
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	82
	DB	97
	DB	110
	DB	100
	DB	111
	DB	109
	DB	32
	DB	80
	DB	97
	DB	116
	DB	104
	DB	115
	DB	32
	DB	65
	DB	114
	DB	101
	DB	32
	DB	65
	DB	115
	DB	115
	DB	105
	DB	103
	DB	110
	DB	101
	DB	100
	DB	32
	DB	116
	DB	111
	DB	32
	DB	82
	DB	101
	DB	115
	DB	112
	DB	111
	DB	110
	DB	100
	DB	101
	DB	100
	DB	32
	DB	86
	DB	101
	DB	104
	DB	105
	DB	99
	DB	108
	DB	101
	DB	115
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	5
	DB	0
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	5
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	55
	DB	0
	DB	32
	DB	37
	DB	32
	DB	111
	DB	102
	DB	32
	DB	79
	DB	117
	DB	116
	DB	45
	DB	111
	DB	102
	DB	45
	DB	86
	DB	101
	DB	104
	DB	105
	DB	99
	DB	108
	DB	101
	DB	32
	DB	82
	DB	101
	DB	115
	DB	112
	DB	111
	DB	110
	DB	115
	DB	105
	DB	118
	DB	101
	DB	32
	DB	86
	DB	101
	DB	104
	DB	105
	DB	99
	DB	108
	DB	101
	DB	115
	DB	32
	DB	82
	DB	101
	DB	115
	DB	112
	DB	111
	DB	110
	DB	100
	DB	32
	DB	116
	DB	111
	DB	32
	DB	86
	DB	77
	DB	83
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	48
	DB	0
	DB	32
	DB	32
	DB	32
	DB	32
	DB	78
	DB	111
	DB	32
	DB	84
	DB	114
	DB	97
	DB	102
	DB	102
	DB	105
	DB	99
	DB	32
	DB	77
	DB	97
	DB	110
	DB	97
	DB	103
	DB	101
	DB	109
	DB	101
	DB	110
	DB	116
	DB	32
	DB	83
	DB	116
	DB	114
	DB	97
	DB	116
	DB	101
	DB	103
	DB	121
	DB	32
	DB	87
	DB	97
	DB	115
	DB	32
	DB	83
	DB	112
	DB	101
	DB	99
	DB	105
	DB	102
	DB	105
	DB	101
	DB	100
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	15
	DB	0
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	76
	DB	111
	DB	99
	DB	97
	DB	116
	DB	105
	DB	111
	DB	110
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	4
	DB	0
	DB	32
	DB	32
	DB	45
	DB	45
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	12
	DB	0
	DB	32
	DB	32
	DB	32
	DB	70
	DB	114
	DB	111
	DB	109
	DB	32
	DB	109
	DB	105
	DB	110
	DB	32
	DB	33
	DB	0
	DB	0
	DB	1
	DB	1
	DB	0
	DB	0
	DB	0
	DB	8
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	8
	DB	0
	DB	32
	DB	84
	DB	111
	DB	32
	DB	109
	DB	105
	DB	110
	DB	32
	DB	33
	DB	0
	DB	0
	DB	1
	DB	1
	DB	0
	DB	0
	DB	0
	DB	8
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	3
	DB	0
	DB	44
	DB	32
	DB	32
	DB	0
	DB	33
	DB	0
	DB	0
	DB	1
	DB	1
	DB	0
	DB	0
	DB	0
	DB	6
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	21
	DB	0
	DB	32
	DB	37
	DB	32
	DB	67
	DB	97
	DB	112
	DB	97
	DB	99
	DB	105
	DB	116
	DB	121
	DB	32
	DB	82
	DB	101
	DB	100
	DB	117
	DB	99
	DB	116
	DB	105
	DB	111
	DB	110
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	15
	DB	0
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	76
	DB	111
	DB	99
	DB	97
	DB	116
	DB	105
	DB	111
	DB	110
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	4
	DB	0
	DB	32
	DB	32
	DB	45
	DB	45
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	12
	DB	0
	DB	32
	DB	32
	DB	32
	DB	70
	DB	114
	DB	111
	DB	109
	DB	32
	DB	109
	DB	105
	DB	110
	DB	32
	DB	33
	DB	0
	DB	0
	DB	1
	DB	1
	DB	0
	DB	0
	DB	0
	DB	8
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	8
	DB	0
	DB	32
	DB	84
	DB	111
	DB	32
	DB	109
	DB	105
	DB	110
	DB	32
	DB	33
	DB	0
	DB	0
	DB	1
	DB	1
	DB	0
	DB	0
	DB	0
	DB	8
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	3
	DB	0
	DB	44
	DB	32
	DB	32
	DB	0
	DB	33
	DB	0
	DB	0
	DB	1
	DB	1
	DB	0
	DB	0
	DB	0
	DB	4
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	21
	DB	0
	DB	32
	DB	37
	DB	32
	DB	67
	DB	97
	DB	112
	DB	97
	DB	99
	DB	105
	DB	116
	DB	121
	DB	32
	DB	82
	DB	101
	DB	100
	DB	117
	DB	99
	DB	116
	DB	105
	DB	111
	DB	110
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	49
	DB	0
	DB	32
	DB	32
	DB	32
	DB	32
	DB	78
	DB	111
	DB	32
	DB	67
	DB	97
	DB	112
	DB	97
	DB	99
	DB	105
	DB	116
	DB	121
	DB	32
	DB	82
	DB	101
	DB	100
	DB	117
	DB	99
	DB	116
	DB	105
	DB	111
	DB	110
	DB	32
	DB	83
	DB	99
	DB	101
	DB	110
	DB	97
	DB	114
	DB	105
	DB	111
	DB	32
	DB	87
	DB	97
	DB	115
	DB	32
	DB	83
	DB	112
	DB	101
	DB	99
	DB	105
	DB	102
	DB	105
	DB	101
	DB	100
	DB	32
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
_DATA	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_289.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_290.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_291.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_292.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_293.0.2	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_294.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_295.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_296.0.2	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_297.0.2	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_298.0.2	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_299.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_300.0.2	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_301.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_302.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_303.0.2	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_304.0.2	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_305.0.2	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_306.0.2	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_307.0.2	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_308.0.2	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_309.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_310.0.2	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_311.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_312.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_313.0.2	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_314.0.2	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_315.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_316.0.2	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_317.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_318.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_320.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_321.0.2	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_322.0.2	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_319.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_323.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_324.0.2	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_325.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_326.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_327.0.2	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_328.0.2	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_329.0.2	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_330.0.2	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_331.0.2	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_332.0.2	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_333.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_334.0.2	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_335.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_336.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_337.0.2	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_338.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_339.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
..1..TPKT.2_0.0.2	DD	OFFSET FLAT: ..1.2_0.TAG.00.0.2
	DD	OFFSET FLAT: ..1.2_0.TAG.01.0.2
	DD	OFFSET FLAT: ..1.2_0.TAG.02.0.2
	DD	OFFSET FLAT: ..1.2_0.TAG.03.0.2
	DD	OFFSET FLAT: ..1.2_0.TAG.04.0.2
__STRLITPACK_344.0.2	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_343.0.2	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_342.0.2	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_340.0.2	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_341.0.2	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_345.0.2	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_346.0.2	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_347.0.2	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_348.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_349.0.2	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_350.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_351.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_377.0.2	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_352.0.2	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_368.0.2	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_369.0.2	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_370.0.2	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_371.0.2	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_372.0.2	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_373.0.2	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_375.0.2	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_374.0.2	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_376.0.2	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_361.0.2	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_362.0.2	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_363.0.2	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_364.0.2	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_365.0.2	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_366.0.2	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_367.0.2	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_353.0.2	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_354.0.2	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_355.0.2	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_356.0.2	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_357.0.2	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_358.0.2	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_359.0.2	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_360.0.2	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_378.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_379.0.2	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_380.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_381.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_382.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_383.0.2	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_384.0.2	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_385.0.2	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_386.0.2	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_387.0.2	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_388.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_389.0.2	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_390.0.2	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_391.0.2	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_392.0.2	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_393.0.2	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_394.0.2	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_395.0.2	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_396.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_397.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_398.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_399.0.2	DB	1
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _PRINTHEADER1
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _PRINTHEADER2
; mark_begin;
       ALIGN     16
	PUBLIC _PRINTHEADER2
_PRINTHEADER2	PROC NEAR 
.B3.1:                          ; Preds .B3.0
        push      ebp                                           ;321.12
        mov       ebp, esp                                      ;321.12
        and       esp, -16                                      ;321.12
        push      esi                                           ;321.12
        push      edi                                           ;321.12
        push      ebx                                           ;321.12
        sub       esp, 292                                      ;321.12
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_READVEH+44] ;341.16
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_READVEH+36] ;341.16
        test      edx, edx                                      ;341.16
        mov       DWORD PTR [84+esp], eax                       ;341.16
        mov       DWORD PTR [116+esp], edx                      ;341.16
        jle       .B3.355       ; Prob 0%                       ;341.16
                                ; LOE
.B3.2:                          ; Preds .B3.1
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_READVEH+24] ;341.16
        xor       esi, esi                                      ;
        mov       ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_READVEH] ;351.107
        pxor      xmm0, xmm0                                    ;
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_READVEH+40] ;351.107
        test      edx, edx                                      ;341.16
        jg        .B3.284       ; Prob 50%                      ;341.16
                                ; LOE edx ecx ebx esi xmm0
.B3.3:                          ; Preds .B3.2 .B3.301 .B3.352 .B3.355
        movdqa    xmm1, xmm0                                    ;341.16
        lea       ecx, DWORD PTR [esp]                          ;343.7
        psrldq    xmm1, 8                                       ;341.16
        lea       edx, DWORD PTR [56+esp]                       ;343.7
        paddd     xmm0, xmm1                                    ;341.16
        movdqa    xmm2, xmm0                                    ;341.16
        psrldq    xmm2, 4                                       ;341.16
        paddd     xmm0, xmm2                                    ;341.16
        movd      eax, xmm0                                     ;341.16
        add       esi, eax                                      ;341.16
        cvtsi2ss  xmm0, esi                                     ;341.7
        movss     DWORD PTR [92+esp], xmm0                      ;341.7
        mov       DWORD PTR [esp], 0                            ;343.7
        mov       DWORD PTR [56+esp], 39                        ;343.7
        mov       DWORD PTR [60+esp], OFFSET FLAT: __STRLITPACK_482 ;343.7
        push      32                                            ;343.7
        push      edx                                           ;343.7
        push      OFFSET FLAT: __STRLITPACK_484.0.3             ;343.7
        push      -2088435968                                   ;343.7
        push      666                                           ;343.7
        push      ecx                                           ;343.7
        call      _for_write_seq_lis                            ;343.7
                                ; LOE
.B3.4:                          ; Preds .B3.3
        xor       eax, eax                                      ;344.7
        mov       DWORD PTR [24+esp], eax                       ;344.7
        push      32                                            ;344.7
        push      eax                                           ;344.7
        push      OFFSET FLAT: __STRLITPACK_485.0.3             ;344.7
        push      -2088435968                                   ;344.7
        push      666                                           ;344.7
        lea       edx, DWORD PTR [44+esp]                       ;344.7
        push      edx                                           ;344.7
        call      _for_write_seq_lis                            ;344.7
                                ; LOE
.B3.5:                          ; Preds .B3.4
        mov       DWORD PTR [48+esp], 0                         ;345.7
        lea       eax, DWORD PTR [112+esp]                      ;345.7
        mov       DWORD PTR [112+esp], 24                       ;345.7
        mov       DWORD PTR [116+esp], OFFSET FLAT: __STRLITPACK_480 ;345.7
        push      32                                            ;345.7
        push      eax                                           ;345.7
        push      OFFSET FLAT: __STRLITPACK_486.0.3             ;345.7
        push      -2088435968                                   ;345.7
        push      666                                           ;345.7
        lea       edx, DWORD PTR [68+esp]                       ;345.7
        push      edx                                           ;345.7
        call      _for_write_seq_lis                            ;345.7
                                ; LOE
.B3.6:                          ; Preds .B3.5
        mov       DWORD PTR [72+esp], 0                         ;346.7
        lea       eax, DWORD PTR [144+esp]                      ;346.7
        mov       DWORD PTR [144+esp], 24                       ;346.7
        mov       DWORD PTR [148+esp], OFFSET FLAT: __STRLITPACK_478 ;346.7
        push      32                                            ;346.7
        push      eax                                           ;346.7
        push      OFFSET FLAT: __STRLITPACK_487.0.3             ;346.7
        push      -2088435968                                   ;346.7
        push      666                                           ;346.7
        lea       edx, DWORD PTR [92+esp]                       ;346.7
        push      edx                                           ;346.7
        call      _for_write_seq_lis                            ;346.7
                                ; LOE
.B3.358:                        ; Preds .B3.6
        add       esp, 96                                       ;346.7
                                ; LOE
.B3.7:                          ; Preds .B3.358
        movss     xmm0, DWORD PTR [92+esp]                      ;347.17
        pxor      xmm1, xmm1                                    ;347.17
        comiss    xmm0, xmm1                                    ;347.17
        jbe       .B3.80        ; Prob 50%                      ;347.17
                                ; LOE
.B3.8:                          ; Preds .B3.7
        cmp       DWORD PTR [116+esp], 0                        ;348.89
        jle       .B3.15        ; Prob 50%                      ;348.89
                                ; LOE
.B3.9:                          ; Preds .B3.8
        mov       edx, DWORD PTR [116+esp]                      ;348.89
        mov       esi, edx                                      ;348.89
        shr       esi, 31                                       ;348.89
        add       esi, edx                                      ;348.89
        sar       esi, 1                                        ;348.89
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_READVEH+40] ;348.89
        test      esi, esi                                      ;348.89
        mov       ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_READVEH+32] ;348.89
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_READVEH] ;348.89
        mov       DWORD PTR [52+esp], eax                       ;348.89
        mov       eax, 0                                        ;
        mov       DWORD PTR [80+esp], esi                       ;348.89
        jbe       .B3.302       ; Prob 10%                      ;348.89
                                ; LOE eax ecx ebx
.B3.10:                         ; Preds .B3.9
        mov       DWORD PTR [44+esp], eax                       ;
        lea       edi, DWORD PTR [ebx*4]                        ;
        mov       eax, DWORD PTR [84+esp]                       ;
        neg       edi                                           ;
        mov       esi, DWORD PTR [52+esp]                       ;
        add       edi, ecx                                      ;
        imul      eax, esi                                      ;
        xor       edx, edx                                      ;
        mov       DWORD PTR [40+esp], edi                       ;
        sub       edi, eax                                      ;
        add       eax, esi                                      ;
        add       edi, eax                                      ;
        mov       DWORD PTR [48+esp], edx                       ;
        mov       DWORD PTR [32+esp], ecx                       ;
        mov       DWORD PTR [36+esp], ebx                       ;
        mov       esi, DWORD PTR [48+esp]                       ;
        mov       eax, DWORD PTR [44+esp]                       ;
        mov       ecx, edx                                      ;
        mov       ebx, edi                                      ;
        mov       edi, DWORD PTR [40+esp]                       ;
                                ; LOE eax edx ecx ebx esi edi
.B3.11:                         ; Preds .B3.11 .B3.10
        inc       esi                                           ;348.89
        add       eax, DWORD PTR [4+edi+ecx*2]                  ;348.89
        add       edx, DWORD PTR [4+ebx+ecx*2]                  ;348.89
        add       ecx, DWORD PTR [52+esp]                       ;348.89
        cmp       esi, DWORD PTR [80+esp]                       ;348.89
        jb        .B3.11        ; Prob 63%                      ;348.89
                                ; LOE eax edx ecx ebx esi edi
.B3.12:                         ; Preds .B3.11
        mov       ecx, DWORD PTR [32+esp]                       ;
        add       eax, edx                                      ;348.89
        mov       ebx, DWORD PTR [36+esp]                       ;
        lea       esi, DWORD PTR [1+esi+esi]                    ;348.89
                                ; LOE eax ecx ebx esi
.B3.13:                         ; Preds .B3.12 .B3.302
        lea       edx, DWORD PTR [-1+esi]                       ;348.89
        cmp       edx, DWORD PTR [116+esp]                      ;348.89
        jae       .B3.16        ; Prob 10%                      ;348.89
                                ; LOE eax ecx ebx esi
.B3.14:                         ; Preds .B3.13
        mov       edi, DWORD PTR [84+esp]                       ;348.89
        mov       edx, edi                                      ;348.89
        neg       edx                                           ;348.89
        shl       ebx, 2                                        ;348.89
        lea       esi, DWORD PTR [-1+edi+esi]                   ;348.89
        add       edx, esi                                      ;348.89
        sub       ecx, ebx                                      ;348.89
        imul      edx, DWORD PTR [52+esp]                       ;348.89
        add       eax, DWORD PTR [4+ecx+edx]                    ;348.89
        jmp       .B3.16        ; Prob 100%                     ;348.89
                                ; LOE eax
.B3.15:                         ; Preds .B3.8
        xor       eax, eax                                      ;
                                ; LOE eax
.B3.16:                         ; Preds .B3.14 .B3.13 .B3.15
        mov       DWORD PTR [esp], 0                            ;348.9
        mov       DWORD PTR [80+esp], eax                       ;348.9
        lea       eax, DWORD PTR [80+esp]                       ;348.9
        push      32                                            ;348.9
        push      OFFSET FLAT: PRINTHEADER2$format_pack.0.3     ;348.9
        push      eax                                           ;348.9
        push      OFFSET FLAT: __STRLITPACK_488.0.3             ;348.9
        push      -2088435968                                   ;348.9
        push      666                                           ;348.9
        lea       edx, DWORD PTR [24+esp]                       ;348.9
        push      edx                                           ;348.9
        call      _for_write_seq_fmt                            ;348.9
                                ; LOE
.B3.359:                        ; Preds .B3.16
        add       esp, 28                                       ;348.9
                                ; LOE
.B3.17:                         ; Preds .B3.359
        cmp       DWORD PTR [116+esp], 0                        ;348.107
        jle       .B3.24        ; Prob 50%                      ;348.107
                                ; LOE
.B3.18:                         ; Preds .B3.17
        mov       eax, DWORD PTR [116+esp]                      ;348.107
        mov       ebx, eax                                      ;348.107
        shr       ebx, 31                                       ;348.107
        add       ebx, eax                                      ;348.107
        xor       eax, eax                                      ;
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_READVEH] ;348.107
        sar       ebx, 1                                        ;348.107
        mov       DWORD PTR [52+esp], ecx                       ;348.107
        test      ebx, ebx                                      ;348.107
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_READVEH+32] ;348.107
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_READVEH+40] ;348.107
        mov       DWORD PTR [96+esp], ebx                       ;348.107
        jbe       .B3.303       ; Prob 10%                      ;348.107
                                ; LOE eax edx ecx
.B3.19:                         ; Preds .B3.18
        mov       esi, DWORD PTR [84+esp]                       ;
        xor       ebx, ebx                                      ;
        imul      esi, ecx                                      ;
        mov       DWORD PTR [44+esp], eax                       ;
        lea       eax, DWORD PTR [edx*4]                        ;
        neg       eax                                           ;
        add       eax, DWORD PTR [52+esp]                       ;
        mov       DWORD PTR [40+esp], eax                       ;
        sub       eax, esi                                      ;
        add       esi, ecx                                      ;
        add       eax, esi                                      ;
        mov       DWORD PTR [48+esp], ebx                       ;
        mov       DWORD PTR [36+esp], eax                       ;
        mov       DWORD PTR [88+esp], ecx                       ;
        mov       DWORD PTR [32+esp], edx                       ;
        mov       esi, DWORD PTR [48+esp]                       ;
        mov       eax, DWORD PTR [44+esp]                       ;
        mov       edx, ebx                                      ;
        mov       ecx, DWORD PTR [36+esp]                       ;
        mov       edi, DWORD PTR [40+esp]                       ;
                                ; LOE eax edx ecx ebx esi edi
.B3.20:                         ; Preds .B3.20 .B3.19
        inc       esi                                           ;348.107
        add       eax, DWORD PTR [4+edi+edx*2]                  ;348.107
        add       ebx, DWORD PTR [4+ecx+edx*2]                  ;348.107
        add       edx, DWORD PTR [88+esp]                       ;348.107
        cmp       esi, DWORD PTR [96+esp]                       ;348.107
        jb        .B3.20        ; Prob 63%                      ;348.107
                                ; LOE eax edx ecx ebx esi edi
.B3.21:                         ; Preds .B3.20
        mov       ecx, DWORD PTR [88+esp]                       ;
        add       eax, ebx                                      ;348.107
        mov       edx, DWORD PTR [32+esp]                       ;
        lea       esi, DWORD PTR [1+esi+esi]                    ;348.107
                                ; LOE eax edx ecx esi
.B3.22:                         ; Preds .B3.21 .B3.303
        lea       ebx, DWORD PTR [-1+esi]                       ;348.107
        cmp       ebx, DWORD PTR [116+esp]                      ;348.107
        jae       .B3.25        ; Prob 10%                      ;348.107
                                ; LOE eax edx ecx esi
.B3.23:                         ; Preds .B3.22
        mov       edi, DWORD PTR [84+esp]                       ;348.107
        mov       ebx, edi                                      ;348.107
        neg       ebx                                           ;348.107
        shl       edx, 2                                        ;348.107
        lea       esi, DWORD PTR [-1+edi+esi]                   ;348.107
        add       ebx, esi                                      ;348.107
        imul      ebx, ecx                                      ;348.107
        mov       ecx, DWORD PTR [52+esp]                       ;348.107
        sub       ecx, edx                                      ;348.107
        add       eax, DWORD PTR [4+ecx+ebx]                    ;348.107
        jmp       .B3.25        ; Prob 100%                     ;348.107
                                ; LOE eax
.B3.24:                         ; Preds .B3.17
        xor       eax, eax                                      ;
                                ; LOE eax
.B3.25:                         ; Preds .B3.23 .B3.22 .B3.24
        cvtsi2ss  xmm0, eax                                     ;348.107
        divss     xmm0, DWORD PTR [92+esp]                      ;348.124
        mulss     xmm0, DWORD PTR [_2il0floatpacket.7]          ;348.9
        lea       eax, DWORD PTR [88+esp]                       ;348.9
        movss     DWORD PTR [88+esp], xmm0                      ;348.9
        push      eax                                           ;348.9
        push      OFFSET FLAT: __STRLITPACK_489.0.3             ;348.9
        lea       edx, DWORD PTR [8+esp]                        ;348.9
        push      edx                                           ;348.9
        call      _for_write_seq_fmt_xmit                       ;348.9
                                ; LOE
.B3.360:                        ; Preds .B3.25
        add       esp, 12                                       ;348.9
                                ; LOE
.B3.26:                         ; Preds .B3.360
        cmp       DWORD PTR [116+esp], 0                        ;349.89
        jle       .B3.33        ; Prob 50%                      ;349.89
                                ; LOE
.B3.27:                         ; Preds .B3.26
        mov       eax, DWORD PTR [116+esp]                      ;349.89
        mov       ebx, eax                                      ;349.89
        shr       ebx, 31                                       ;349.89
        add       ebx, eax                                      ;349.89
        xor       eax, eax                                      ;
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_READVEH] ;349.89
        sar       ebx, 1                                        ;349.89
        mov       DWORD PTR [52+esp], ecx                       ;349.89
        test      ebx, ebx                                      ;349.89
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_READVEH+32] ;349.89
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_READVEH+40] ;349.89
        mov       DWORD PTR [100+esp], ebx                      ;349.89
        jbe       .B3.304       ; Prob 10%                      ;349.89
                                ; LOE eax edx ecx
.B3.28:                         ; Preds .B3.27
        mov       esi, DWORD PTR [84+esp]                       ;
        xor       ebx, ebx                                      ;
        imul      esi, ecx                                      ;
        mov       DWORD PTR [44+esp], eax                       ;
        lea       eax, DWORD PTR [edx*4]                        ;
        neg       eax                                           ;
        add       eax, DWORD PTR [52+esp]                       ;
        mov       DWORD PTR [40+esp], eax                       ;
        sub       eax, esi                                      ;
        add       esi, ecx                                      ;
        add       eax, esi                                      ;
        mov       DWORD PTR [48+esp], ebx                       ;
        mov       DWORD PTR [36+esp], eax                       ;
        mov       DWORD PTR [96+esp], ecx                       ;
        mov       DWORD PTR [32+esp], edx                       ;
        mov       esi, DWORD PTR [48+esp]                       ;
        mov       eax, DWORD PTR [44+esp]                       ;
        mov       edx, ebx                                      ;
        mov       ecx, DWORD PTR [36+esp]                       ;
        mov       edi, DWORD PTR [40+esp]                       ;
                                ; LOE eax edx ecx ebx esi edi
.B3.29:                         ; Preds .B3.29 .B3.28
        inc       esi                                           ;349.89
        add       eax, DWORD PTR [8+edi+edx*2]                  ;349.89
        add       ebx, DWORD PTR [8+ecx+edx*2]                  ;349.89
        add       edx, DWORD PTR [96+esp]                       ;349.89
        cmp       esi, DWORD PTR [100+esp]                      ;349.89
        jb        .B3.29        ; Prob 63%                      ;349.89
                                ; LOE eax edx ecx ebx esi edi
.B3.30:                         ; Preds .B3.29
        mov       ecx, DWORD PTR [96+esp]                       ;
        add       eax, ebx                                      ;349.89
        mov       edx, DWORD PTR [32+esp]                       ;
        lea       esi, DWORD PTR [1+esi+esi]                    ;349.89
                                ; LOE eax edx ecx esi
.B3.31:                         ; Preds .B3.30 .B3.304
        lea       ebx, DWORD PTR [-1+esi]                       ;349.89
        cmp       ebx, DWORD PTR [116+esp]                      ;349.89
        jae       .B3.34        ; Prob 10%                      ;349.89
                                ; LOE eax edx ecx esi
.B3.32:                         ; Preds .B3.31
        mov       edi, DWORD PTR [84+esp]                       ;349.89
        mov       ebx, edi                                      ;349.89
        neg       ebx                                           ;349.89
        shl       edx, 2                                        ;349.89
        lea       esi, DWORD PTR [-1+edi+esi]                   ;349.89
        add       ebx, esi                                      ;349.89
        imul      ebx, ecx                                      ;349.89
        mov       ecx, DWORD PTR [52+esp]                       ;349.89
        sub       ecx, edx                                      ;349.89
        add       eax, DWORD PTR [8+ecx+ebx]                    ;349.89
        jmp       .B3.34        ; Prob 100%                     ;349.89
                                ; LOE eax
.B3.33:                         ; Preds .B3.26
        xor       eax, eax                                      ;
                                ; LOE eax
.B3.34:                         ; Preds .B3.32 .B3.31 .B3.33
        mov       DWORD PTR [esp], 0                            ;349.9
        mov       DWORD PTR [96+esp], eax                       ;349.9
        lea       eax, DWORD PTR [96+esp]                       ;349.9
        push      32                                            ;349.9
        push      OFFSET FLAT: PRINTHEADER2$format_pack.0.3+92  ;349.9
        push      eax                                           ;349.9
        push      OFFSET FLAT: __STRLITPACK_490.0.3             ;349.9
        push      -2088435968                                   ;349.9
        push      666                                           ;349.9
        lea       edx, DWORD PTR [24+esp]                       ;349.9
        push      edx                                           ;349.9
        call      _for_write_seq_fmt                            ;349.9
                                ; LOE
.B3.361:                        ; Preds .B3.34
        add       esp, 28                                       ;349.9
                                ; LOE
.B3.35:                         ; Preds .B3.361
        cmp       DWORD PTR [116+esp], 0                        ;349.107
        jle       .B3.42        ; Prob 50%                      ;349.107
                                ; LOE
.B3.36:                         ; Preds .B3.35
        mov       eax, DWORD PTR [116+esp]                      ;349.107
        mov       ebx, eax                                      ;349.107
        shr       ebx, 31                                       ;349.107
        add       ebx, eax                                      ;349.107
        xor       eax, eax                                      ;
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_READVEH] ;349.107
        sar       ebx, 1                                        ;349.107
        mov       DWORD PTR [52+esp], ecx                       ;349.107
        test      ebx, ebx                                      ;349.107
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_READVEH+32] ;349.107
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_READVEH+40] ;349.107
        mov       DWORD PTR [104+esp], ebx                      ;349.107
        jbe       .B3.305       ; Prob 10%                      ;349.107
                                ; LOE eax edx ecx
.B3.37:                         ; Preds .B3.36
        mov       esi, DWORD PTR [84+esp]                       ;
        xor       ebx, ebx                                      ;
        imul      esi, ecx                                      ;
        mov       DWORD PTR [44+esp], eax                       ;
        lea       eax, DWORD PTR [edx*4]                        ;
        neg       eax                                           ;
        add       eax, DWORD PTR [52+esp]                       ;
        mov       DWORD PTR [40+esp], eax                       ;
        sub       eax, esi                                      ;
        add       esi, ecx                                      ;
        add       eax, esi                                      ;
        mov       DWORD PTR [48+esp], ebx                       ;
        mov       DWORD PTR [36+esp], eax                       ;
        mov       DWORD PTR [100+esp], ecx                      ;
        mov       DWORD PTR [32+esp], edx                       ;
        mov       esi, DWORD PTR [48+esp]                       ;
        mov       eax, DWORD PTR [44+esp]                       ;
        mov       edx, ebx                                      ;
        mov       ecx, DWORD PTR [36+esp]                       ;
        mov       edi, DWORD PTR [40+esp]                       ;
                                ; LOE eax edx ecx ebx esi edi
.B3.38:                         ; Preds .B3.38 .B3.37
        inc       esi                                           ;349.107
        add       eax, DWORD PTR [8+edi+edx*2]                  ;349.107
        add       ebx, DWORD PTR [8+ecx+edx*2]                  ;349.107
        add       edx, DWORD PTR [100+esp]                      ;349.107
        cmp       esi, DWORD PTR [104+esp]                      ;349.107
        jb        .B3.38        ; Prob 63%                      ;349.107
                                ; LOE eax edx ecx ebx esi edi
.B3.39:                         ; Preds .B3.38
        mov       ecx, DWORD PTR [100+esp]                      ;
        add       eax, ebx                                      ;349.107
        mov       edx, DWORD PTR [32+esp]                       ;
        lea       esi, DWORD PTR [1+esi+esi]                    ;349.107
                                ; LOE eax edx ecx esi
.B3.40:                         ; Preds .B3.39 .B3.305
        lea       ebx, DWORD PTR [-1+esi]                       ;349.107
        cmp       ebx, DWORD PTR [116+esp]                      ;349.107
        jae       .B3.43        ; Prob 10%                      ;349.107
                                ; LOE eax edx ecx esi
.B3.41:                         ; Preds .B3.40
        mov       edi, DWORD PTR [84+esp]                       ;349.107
        mov       ebx, edi                                      ;349.107
        neg       ebx                                           ;349.107
        shl       edx, 2                                        ;349.107
        lea       esi, DWORD PTR [-1+edi+esi]                   ;349.107
        add       ebx, esi                                      ;349.107
        imul      ebx, ecx                                      ;349.107
        mov       ecx, DWORD PTR [52+esp]                       ;349.107
        sub       ecx, edx                                      ;349.107
        add       eax, DWORD PTR [8+ecx+ebx]                    ;349.107
        jmp       .B3.43        ; Prob 100%                     ;349.107
                                ; LOE eax
.B3.42:                         ; Preds .B3.35
        xor       eax, eax                                      ;
                                ; LOE eax
.B3.43:                         ; Preds .B3.41 .B3.40 .B3.42
        cvtsi2ss  xmm0, eax                                     ;349.107
        divss     xmm0, DWORD PTR [92+esp]                      ;349.124
        mulss     xmm0, DWORD PTR [_2il0floatpacket.7]          ;349.9
        lea       eax, DWORD PTR [104+esp]                      ;349.9
        movss     DWORD PTR [104+esp], xmm0                     ;349.9
        push      eax                                           ;349.9
        push      OFFSET FLAT: __STRLITPACK_491.0.3             ;349.9
        lea       edx, DWORD PTR [8+esp]                        ;349.9
        push      edx                                           ;349.9
        call      _for_write_seq_fmt_xmit                       ;349.9
                                ; LOE
.B3.362:                        ; Preds .B3.43
        add       esp, 12                                       ;349.9
                                ; LOE
.B3.44:                         ; Preds .B3.362
        cmp       DWORD PTR [116+esp], 0                        ;350.89
        jle       .B3.51        ; Prob 50%                      ;350.89
                                ; LOE
.B3.45:                         ; Preds .B3.44
        mov       eax, DWORD PTR [116+esp]                      ;350.89
        mov       ebx, eax                                      ;350.89
        shr       ebx, 31                                       ;350.89
        add       ebx, eax                                      ;350.89
        xor       eax, eax                                      ;
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_READVEH] ;350.89
        sar       ebx, 1                                        ;350.89
        mov       DWORD PTR [52+esp], ecx                       ;350.89
        test      ebx, ebx                                      ;350.89
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_READVEH+32] ;350.89
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_READVEH+40] ;350.89
        mov       DWORD PTR [108+esp], ebx                      ;350.89
        jbe       .B3.306       ; Prob 10%                      ;350.89
                                ; LOE eax edx ecx
.B3.46:                         ; Preds .B3.45
        mov       esi, DWORD PTR [84+esp]                       ;
        xor       ebx, ebx                                      ;
        imul      esi, ecx                                      ;
        mov       DWORD PTR [44+esp], eax                       ;
        lea       eax, DWORD PTR [edx*4]                        ;
        neg       eax                                           ;
        add       eax, DWORD PTR [52+esp]                       ;
        mov       DWORD PTR [40+esp], eax                       ;
        sub       eax, esi                                      ;
        add       esi, ecx                                      ;
        add       eax, esi                                      ;
        mov       DWORD PTR [48+esp], ebx                       ;
        mov       DWORD PTR [36+esp], eax                       ;
        mov       DWORD PTR [100+esp], ecx                      ;
        mov       DWORD PTR [32+esp], edx                       ;
        mov       esi, DWORD PTR [48+esp]                       ;
        mov       eax, DWORD PTR [44+esp]                       ;
        mov       edx, ebx                                      ;
        mov       ecx, DWORD PTR [36+esp]                       ;
        mov       edi, DWORD PTR [40+esp]                       ;
                                ; LOE eax edx ecx ebx esi edi
.B3.47:                         ; Preds .B3.47 .B3.46
        inc       esi                                           ;350.89
        add       eax, DWORD PTR [12+edi+edx*2]                 ;350.89
        add       ebx, DWORD PTR [12+ecx+edx*2]                 ;350.89
        add       edx, DWORD PTR [100+esp]                      ;350.89
        cmp       esi, DWORD PTR [108+esp]                      ;350.89
        jb        .B3.47        ; Prob 63%                      ;350.89
                                ; LOE eax edx ecx ebx esi edi
.B3.48:                         ; Preds .B3.47
        mov       ecx, DWORD PTR [100+esp]                      ;
        add       eax, ebx                                      ;350.89
        mov       edx, DWORD PTR [32+esp]                       ;
        lea       esi, DWORD PTR [1+esi+esi]                    ;350.89
                                ; LOE eax edx ecx esi
.B3.49:                         ; Preds .B3.48 .B3.306
        lea       ebx, DWORD PTR [-1+esi]                       ;350.89
        cmp       ebx, DWORD PTR [116+esp]                      ;350.89
        jae       .B3.52        ; Prob 10%                      ;350.89
                                ; LOE eax edx ecx esi
.B3.50:                         ; Preds .B3.49
        mov       edi, DWORD PTR [84+esp]                       ;350.89
        mov       ebx, edi                                      ;350.89
        neg       ebx                                           ;350.89
        shl       edx, 2                                        ;350.89
        lea       esi, DWORD PTR [-1+edi+esi]                   ;350.89
        add       ebx, esi                                      ;350.89
        imul      ebx, ecx                                      ;350.89
        mov       ecx, DWORD PTR [52+esp]                       ;350.89
        sub       ecx, edx                                      ;350.89
        add       eax, DWORD PTR [12+ecx+ebx]                   ;350.89
        jmp       .B3.52        ; Prob 100%                     ;350.89
                                ; LOE eax
.B3.51:                         ; Preds .B3.44
        xor       eax, eax                                      ;
                                ; LOE eax
.B3.52:                         ; Preds .B3.50 .B3.49 .B3.51
        mov       DWORD PTR [esp], 0                            ;350.9
        mov       DWORD PTR [112+esp], eax                      ;350.9
        lea       eax, DWORD PTR [112+esp]                      ;350.9
        push      32                                            ;350.9
        push      OFFSET FLAT: PRINTHEADER2$format_pack.0.3+184 ;350.9
        push      eax                                           ;350.9
        push      OFFSET FLAT: __STRLITPACK_492.0.3             ;350.9
        push      -2088435968                                   ;350.9
        push      666                                           ;350.9
        lea       edx, DWORD PTR [24+esp]                       ;350.9
        push      edx                                           ;350.9
        call      _for_write_seq_fmt                            ;350.9
                                ; LOE
.B3.363:                        ; Preds .B3.52
        add       esp, 28                                       ;350.9
                                ; LOE
.B3.53:                         ; Preds .B3.363
        cmp       DWORD PTR [116+esp], 0                        ;350.107
        jle       .B3.60        ; Prob 50%                      ;350.107
                                ; LOE
.B3.54:                         ; Preds .B3.53
        mov       eax, DWORD PTR [116+esp]                      ;350.107
        mov       ebx, eax                                      ;350.107
        shr       ebx, 31                                       ;350.107
        add       ebx, eax                                      ;350.107
        xor       eax, eax                                      ;
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_READVEH] ;350.107
        sar       ebx, 1                                        ;350.107
        mov       DWORD PTR [52+esp], ecx                       ;350.107
        test      ebx, ebx                                      ;350.107
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_READVEH+32] ;350.107
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_READVEH+40] ;350.107
        mov       DWORD PTR [108+esp], ebx                      ;350.107
        jbe       .B3.307       ; Prob 10%                      ;350.107
                                ; LOE eax edx ecx
.B3.55:                         ; Preds .B3.54
        mov       esi, DWORD PTR [84+esp]                       ;
        xor       ebx, ebx                                      ;
        imul      esi, ecx                                      ;
        mov       DWORD PTR [44+esp], eax                       ;
        lea       eax, DWORD PTR [edx*4]                        ;
        neg       eax                                           ;
        add       eax, DWORD PTR [52+esp]                       ;
        mov       DWORD PTR [40+esp], eax                       ;
        sub       eax, esi                                      ;
        add       esi, ecx                                      ;
        add       eax, esi                                      ;
        mov       DWORD PTR [48+esp], ebx                       ;
        mov       DWORD PTR [36+esp], eax                       ;
        mov       DWORD PTR [100+esp], ecx                      ;
        mov       DWORD PTR [32+esp], edx                       ;
        mov       esi, DWORD PTR [48+esp]                       ;
        mov       eax, DWORD PTR [44+esp]                       ;
        mov       edx, ebx                                      ;
        mov       ecx, DWORD PTR [36+esp]                       ;
        mov       edi, DWORD PTR [40+esp]                       ;
                                ; LOE eax edx ecx ebx esi edi
.B3.56:                         ; Preds .B3.56 .B3.55
        inc       esi                                           ;350.107
        add       eax, DWORD PTR [12+edi+edx*2]                 ;350.107
        add       ebx, DWORD PTR [12+ecx+edx*2]                 ;350.107
        add       edx, DWORD PTR [100+esp]                      ;350.107
        cmp       esi, DWORD PTR [108+esp]                      ;350.107
        jb        .B3.56        ; Prob 63%                      ;350.107
                                ; LOE eax edx ecx ebx esi edi
.B3.57:                         ; Preds .B3.56
        mov       ecx, DWORD PTR [100+esp]                      ;
        add       eax, ebx                                      ;350.107
        mov       edx, DWORD PTR [32+esp]                       ;
        lea       esi, DWORD PTR [1+esi+esi]                    ;350.107
                                ; LOE eax edx ecx esi
.B3.58:                         ; Preds .B3.57 .B3.307
        lea       ebx, DWORD PTR [-1+esi]                       ;350.107
        cmp       ebx, DWORD PTR [116+esp]                      ;350.107
        jae       .B3.61        ; Prob 10%                      ;350.107
                                ; LOE eax edx ecx esi
.B3.59:                         ; Preds .B3.58
        mov       edi, DWORD PTR [84+esp]                       ;350.107
        mov       ebx, edi                                      ;350.107
        neg       ebx                                           ;350.107
        shl       edx, 2                                        ;350.107
        lea       esi, DWORD PTR [-1+edi+esi]                   ;350.107
        add       ebx, esi                                      ;350.107
        imul      ebx, ecx                                      ;350.107
        mov       ecx, DWORD PTR [52+esp]                       ;350.107
        sub       ecx, edx                                      ;350.107
        add       eax, DWORD PTR [12+ecx+ebx]                   ;350.107
        jmp       .B3.61        ; Prob 100%                     ;350.107
                                ; LOE eax
.B3.60:                         ; Preds .B3.53
        xor       eax, eax                                      ;
                                ; LOE eax
.B3.61:                         ; Preds .B3.59 .B3.58 .B3.60
        cvtsi2ss  xmm0, eax                                     ;350.107
        divss     xmm0, DWORD PTR [92+esp]                      ;350.124
        mulss     xmm0, DWORD PTR [_2il0floatpacket.7]          ;350.9
        lea       eax, DWORD PTR [120+esp]                      ;350.9
        movss     DWORD PTR [120+esp], xmm0                     ;350.9
        push      eax                                           ;350.9
        push      OFFSET FLAT: __STRLITPACK_493.0.3             ;350.9
        lea       edx, DWORD PTR [8+esp]                        ;350.9
        push      edx                                           ;350.9
        call      _for_write_seq_fmt_xmit                       ;350.9
                                ; LOE
.B3.364:                        ; Preds .B3.61
        add       esp, 12                                       ;350.9
                                ; LOE
.B3.62:                         ; Preds .B3.364
        cmp       DWORD PTR [116+esp], 0                        ;351.89
        jle       .B3.69        ; Prob 50%                      ;351.89
                                ; LOE
.B3.63:                         ; Preds .B3.62
        mov       edx, DWORD PTR [116+esp]                      ;351.89
        mov       esi, edx                                      ;351.89
        shr       esi, 31                                       ;351.89
        add       esi, edx                                      ;351.89
        sar       esi, 1                                        ;351.89
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_READVEH+40] ;351.89
        test      esi, esi                                      ;351.89
        mov       ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_READVEH+32] ;351.89
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_READVEH] ;351.89
        mov       DWORD PTR [52+esp], eax                       ;351.89
        mov       eax, 0                                        ;
        mov       DWORD PTR [100+esp], esi                      ;351.89
        jbe       .B3.308       ; Prob 10%                      ;351.89
                                ; LOE eax ecx ebx
.B3.64:                         ; Preds .B3.63
        mov       DWORD PTR [44+esp], eax                       ;
        lea       edi, DWORD PTR [ebx*4]                        ;
        mov       eax, DWORD PTR [84+esp]                       ;
        neg       edi                                           ;
        mov       esi, DWORD PTR [52+esp]                       ;
        add       edi, ecx                                      ;
        imul      eax, esi                                      ;
        xor       edx, edx                                      ;
        mov       DWORD PTR [40+esp], edi                       ;
        sub       edi, eax                                      ;
        add       eax, esi                                      ;
        add       edi, eax                                      ;
        mov       DWORD PTR [48+esp], edx                       ;
        mov       DWORD PTR [32+esp], ecx                       ;
        mov       DWORD PTR [36+esp], ebx                       ;
        mov       esi, DWORD PTR [48+esp]                       ;
        mov       eax, DWORD PTR [44+esp]                       ;
        mov       ecx, edx                                      ;
        mov       ebx, edi                                      ;
        mov       edi, DWORD PTR [40+esp]                       ;
                                ; LOE eax edx ecx ebx esi edi
.B3.65:                         ; Preds .B3.65 .B3.64
        inc       esi                                           ;351.89
        add       eax, DWORD PTR [24+edi+ecx*2]                 ;351.89
        add       edx, DWORD PTR [24+ebx+ecx*2]                 ;351.89
        add       ecx, DWORD PTR [52+esp]                       ;351.89
        cmp       esi, DWORD PTR [100+esp]                      ;351.89
        jb        .B3.65        ; Prob 63%                      ;351.89
                                ; LOE eax edx ecx ebx esi edi
.B3.66:                         ; Preds .B3.65
        mov       ecx, DWORD PTR [32+esp]                       ;
        add       eax, edx                                      ;351.89
        mov       ebx, DWORD PTR [36+esp]                       ;
        lea       esi, DWORD PTR [1+esi+esi]                    ;351.89
                                ; LOE eax ecx ebx esi
.B3.67:                         ; Preds .B3.66 .B3.308
        lea       edx, DWORD PTR [-1+esi]                       ;351.89
        cmp       edx, DWORD PTR [116+esp]                      ;351.89
        jae       .B3.70        ; Prob 10%                      ;351.89
                                ; LOE eax ecx ebx esi
.B3.68:                         ; Preds .B3.67
        mov       edi, DWORD PTR [84+esp]                       ;351.89
        mov       edx, edi                                      ;351.89
        neg       edx                                           ;351.89
        shl       ebx, 2                                        ;351.89
        lea       esi, DWORD PTR [-1+edi+esi]                   ;351.89
        add       edx, esi                                      ;351.89
        sub       ecx, ebx                                      ;351.89
        imul      edx, DWORD PTR [52+esp]                       ;351.89
        add       eax, DWORD PTR [24+ecx+edx]                   ;351.89
        jmp       .B3.70        ; Prob 100%                     ;351.89
                                ; LOE eax
.B3.69:                         ; Preds .B3.62
        xor       eax, eax                                      ;
                                ; LOE eax
.B3.70:                         ; Preds .B3.68 .B3.67 .B3.69
        mov       DWORD PTR [esp], 0                            ;351.9
        mov       DWORD PTR [128+esp], eax                      ;351.9
        lea       eax, DWORD PTR [128+esp]                      ;351.9
        push      32                                            ;351.9
        push      OFFSET FLAT: PRINTHEADER2$format_pack.0.3+276 ;351.9
        push      eax                                           ;351.9
        push      OFFSET FLAT: __STRLITPACK_494.0.3             ;351.9
        push      -2088435968                                   ;351.9
        push      666                                           ;351.9
        lea       edx, DWORD PTR [24+esp]                       ;351.9
        push      edx                                           ;351.9
        call      _for_write_seq_fmt                            ;351.9
                                ; LOE
.B3.365:                        ; Preds .B3.70
        add       esp, 28                                       ;351.9
                                ; LOE
.B3.71:                         ; Preds .B3.365
        cmp       DWORD PTR [116+esp], 0                        ;351.107
        jle       .B3.78        ; Prob 50%                      ;351.107
                                ; LOE
.B3.72:                         ; Preds .B3.71
        mov       eax, DWORD PTR [116+esp]                      ;351.107
        mov       ebx, eax                                      ;351.107
        shr       ebx, 31                                       ;351.107
        add       ebx, eax                                      ;351.107
        xor       eax, eax                                      ;
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_READVEH] ;351.107
        sar       ebx, 1                                        ;351.107
        mov       DWORD PTR [52+esp], ecx                       ;351.107
        test      ebx, ebx                                      ;351.107
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_READVEH+32] ;351.107
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_READVEH+40] ;351.107
        mov       DWORD PTR [108+esp], ebx                      ;351.107
        jbe       .B3.309       ; Prob 10%                      ;351.107
                                ; LOE eax edx ecx
.B3.73:                         ; Preds .B3.72
        mov       esi, DWORD PTR [84+esp]                       ;
        xor       ebx, ebx                                      ;
        imul      esi, ecx                                      ;
        mov       DWORD PTR [44+esp], eax                       ;
        lea       eax, DWORD PTR [edx*4]                        ;
        neg       eax                                           ;
        add       eax, DWORD PTR [52+esp]                       ;
        mov       DWORD PTR [40+esp], eax                       ;
        sub       eax, esi                                      ;
        add       esi, ecx                                      ;
        add       eax, esi                                      ;
        mov       DWORD PTR [48+esp], ebx                       ;
        mov       DWORD PTR [36+esp], eax                       ;
        mov       DWORD PTR [100+esp], ecx                      ;
        mov       DWORD PTR [32+esp], edx                       ;
        mov       esi, DWORD PTR [48+esp]                       ;
        mov       eax, DWORD PTR [44+esp]                       ;
        mov       edx, ebx                                      ;
        mov       ecx, DWORD PTR [36+esp]                       ;
        mov       edi, DWORD PTR [40+esp]                       ;
                                ; LOE eax edx ecx ebx esi edi
.B3.74:                         ; Preds .B3.74 .B3.73
        inc       esi                                           ;351.107
        add       eax, DWORD PTR [24+edi+edx*2]                 ;351.107
        add       ebx, DWORD PTR [24+ecx+edx*2]                 ;351.107
        add       edx, DWORD PTR [100+esp]                      ;351.107
        cmp       esi, DWORD PTR [108+esp]                      ;351.107
        jb        .B3.74        ; Prob 63%                      ;351.107
                                ; LOE eax edx ecx ebx esi edi
.B3.75:                         ; Preds .B3.74
        mov       ecx, DWORD PTR [100+esp]                      ;
        add       eax, ebx                                      ;351.107
        mov       edx, DWORD PTR [32+esp]                       ;
        lea       esi, DWORD PTR [1+esi+esi]                    ;351.107
                                ; LOE eax edx ecx esi
.B3.76:                         ; Preds .B3.75 .B3.309
        lea       ebx, DWORD PTR [-1+esi]                       ;351.107
        cmp       ebx, DWORD PTR [116+esp]                      ;351.107
        jae       .B3.79        ; Prob 10%                      ;351.107
                                ; LOE eax edx ecx esi
.B3.77:                         ; Preds .B3.76
        mov       edi, DWORD PTR [84+esp]                       ;351.107
        mov       ebx, edi                                      ;351.107
        neg       ebx                                           ;351.107
        shl       edx, 2                                        ;351.107
        lea       esi, DWORD PTR [-1+edi+esi]                   ;351.107
        add       ebx, esi                                      ;351.107
        imul      ebx, ecx                                      ;351.107
        mov       ecx, DWORD PTR [52+esp]                       ;351.107
        sub       ecx, edx                                      ;351.107
        add       eax, DWORD PTR [24+ecx+ebx]                   ;351.107
        jmp       .B3.79        ; Prob 100%                     ;351.107
                                ; LOE eax
.B3.78:                         ; Preds .B3.71
        xor       eax, eax                                      ;
                                ; LOE eax
.B3.79:                         ; Preds .B3.77 .B3.76 .B3.78
        cvtsi2ss  xmm1, eax                                     ;351.107
        divss     xmm1, DWORD PTR [92+esp]                      ;351.124
        movss     xmm0, DWORD PTR [_2il0floatpacket.7]          ;351.9
        lea       eax, DWORD PTR [192+esp]                      ;351.9
        mulss     xmm0, xmm1                                    ;351.9
        movss     DWORD PTR [192+esp], xmm0                     ;351.9
        push      eax                                           ;351.9
        push      OFFSET FLAT: __STRLITPACK_495.0.3             ;351.9
        lea       edx, DWORD PTR [8+esp]                        ;351.9
        push      edx                                           ;351.9
        call      _for_write_seq_fmt_xmit                       ;351.9
                                ; LOE
.B3.366:                        ; Preds .B3.79
        add       esp, 12                                       ;351.9
        jmp       .B3.88        ; Prob 100%                     ;351.9
                                ; LOE
.B3.80:                         ; Preds .B3.7
        xor       eax, eax                                      ;353.9
        lea       edx, DWORD PTR [48+esp]                       ;353.9
        mov       DWORD PTR [esp], eax                          ;353.9
        mov       DWORD PTR [48+esp], eax                       ;353.9
        push      32                                            ;353.9
        push      OFFSET FLAT: PRINTHEADER2$format_pack.0.3+368 ;353.9
        push      edx                                           ;353.9
        push      OFFSET FLAT: __STRLITPACK_496.0.3             ;353.9
        push      -2088435968                                   ;353.9
        push      666                                           ;353.9
        lea       ecx, DWORD PTR [24+esp]                       ;353.9
        push      ecx                                           ;353.9
        call      _for_write_seq_fmt                            ;353.9
                                ; LOE
.B3.81:                         ; Preds .B3.80
        mov       DWORD PTR [164+esp], 0                        ;353.9
        lea       eax, DWORD PTR [164+esp]                      ;353.9
        push      eax                                           ;353.9
        push      OFFSET FLAT: __STRLITPACK_497.0.3             ;353.9
        lea       edx, DWORD PTR [36+esp]                       ;353.9
        push      edx                                           ;353.9
        call      _for_write_seq_fmt_xmit                       ;353.9
                                ; LOE
.B3.82:                         ; Preds .B3.81
        xor       eax, eax                                      ;354.9
        lea       edx, DWORD PTR [184+esp]                      ;354.9
        mov       DWORD PTR [40+esp], eax                       ;354.9
        mov       DWORD PTR [184+esp], eax                      ;354.9
        push      32                                            ;354.9
        push      OFFSET FLAT: PRINTHEADER2$format_pack.0.3+460 ;354.9
        push      edx                                           ;354.9
        push      OFFSET FLAT: __STRLITPACK_498.0.3             ;354.9
        push      -2088435968                                   ;354.9
        push      666                                           ;354.9
        lea       ecx, DWORD PTR [64+esp]                       ;354.9
        push      ecx                                           ;354.9
        call      _for_write_seq_fmt                            ;354.9
                                ; LOE
.B3.83:                         ; Preds .B3.82
        mov       DWORD PTR [220+esp], 0                        ;354.9
        lea       eax, DWORD PTR [220+esp]                      ;354.9
        push      eax                                           ;354.9
        push      OFFSET FLAT: __STRLITPACK_499.0.3             ;354.9
        lea       edx, DWORD PTR [76+esp]                       ;354.9
        push      edx                                           ;354.9
        call      _for_write_seq_fmt_xmit                       ;354.9
                                ; LOE
.B3.84:                         ; Preds .B3.83
        xor       eax, eax                                      ;355.9
        lea       edx, DWORD PTR [240+esp]                      ;355.9
        mov       DWORD PTR [80+esp], eax                       ;355.9
        mov       DWORD PTR [240+esp], eax                      ;355.9
        push      32                                            ;355.9
        push      OFFSET FLAT: PRINTHEADER2$format_pack.0.3+552 ;355.9
        push      edx                                           ;355.9
        push      OFFSET FLAT: __STRLITPACK_500.0.3             ;355.9
        push      -2088435968                                   ;355.9
        push      666                                           ;355.9
        lea       ecx, DWORD PTR [104+esp]                      ;355.9
        push      ecx                                           ;355.9
        call      _for_write_seq_fmt                            ;355.9
                                ; LOE
.B3.85:                         ; Preds .B3.84
        mov       DWORD PTR [276+esp], 0                        ;355.9
        lea       eax, DWORD PTR [276+esp]                      ;355.9
        push      eax                                           ;355.9
        push      OFFSET FLAT: __STRLITPACK_501.0.3             ;355.9
        lea       edx, DWORD PTR [116+esp]                      ;355.9
        push      edx                                           ;355.9
        call      _for_write_seq_fmt_xmit                       ;355.9
                                ; LOE
.B3.367:                        ; Preds .B3.85
        add       esp, 120                                      ;355.9
                                ; LOE
.B3.86:                         ; Preds .B3.367
        xor       eax, eax                                      ;356.9
        lea       edx, DWORD PTR [176+esp]                      ;356.9
        mov       DWORD PTR [esp], eax                          ;356.9
        mov       DWORD PTR [176+esp], eax                      ;356.9
        push      32                                            ;356.9
        push      OFFSET FLAT: PRINTHEADER2$format_pack.0.3+644 ;356.9
        push      edx                                           ;356.9
        push      OFFSET FLAT: __STRLITPACK_502.0.3             ;356.9
        push      -2088435968                                   ;356.9
        push      666                                           ;356.9
        lea       ecx, DWORD PTR [24+esp]                       ;356.9
        push      ecx                                           ;356.9
        call      _for_write_seq_fmt                            ;356.9
                                ; LOE
.B3.87:                         ; Preds .B3.86
        mov       DWORD PTR [212+esp], 0                        ;356.9
        lea       eax, DWORD PTR [212+esp]                      ;356.9
        push      eax                                           ;356.9
        push      OFFSET FLAT: __STRLITPACK_503.0.3             ;356.9
        lea       edx, DWORD PTR [36+esp]                       ;356.9
        push      edx                                           ;356.9
        call      _for_write_seq_fmt_xmit                       ;356.9
                                ; LOE
.B3.368:                        ; Preds .B3.87
        add       esp, 40                                       ;356.9
                                ; LOE
.B3.88:                         ; Preds .B3.368 .B3.366
        mov       DWORD PTR [esp], 0                            ;359.7
        lea       eax, DWORD PTR [200+esp]                      ;359.7
        mov       DWORD PTR [200+esp], 39                       ;359.7
        mov       DWORD PTR [204+esp], OFFSET FLAT: __STRLITPACK_460 ;359.7
        push      32                                            ;359.7
        push      eax                                           ;359.7
        push      OFFSET FLAT: __STRLITPACK_504.0.3             ;359.7
        push      -2088435968                                   ;359.7
        push      666                                           ;359.7
        lea       edx, DWORD PTR [20+esp]                       ;359.7
        push      edx                                           ;359.7
        call      _for_write_seq_lis                            ;359.7
                                ; LOE
.B3.89:                         ; Preds .B3.88
        xor       eax, eax                                      ;360.7
        mov       DWORD PTR [24+esp], eax                       ;360.7
        push      32                                            ;360.7
        push      eax                                           ;360.7
        push      OFFSET FLAT: __STRLITPACK_505.0.3             ;360.7
        push      -2088435968                                   ;360.7
        push      666                                           ;360.7
        lea       edx, DWORD PTR [44+esp]                       ;360.7
        push      edx                                           ;360.7
        call      _for_write_seq_lis                            ;360.7
                                ; LOE
.B3.90:                         ; Preds .B3.89
        mov       DWORD PTR [48+esp], 0                         ;361.7
        lea       eax, DWORD PTR [256+esp]                      ;361.7
        mov       DWORD PTR [256+esp], 21                       ;361.7
        mov       DWORD PTR [260+esp], OFFSET FLAT: __STRLITPACK_458 ;361.7
        push      32                                            ;361.7
        push      eax                                           ;361.7
        push      OFFSET FLAT: __STRLITPACK_506.0.3             ;361.7
        push      -2088435968                                   ;361.7
        push      666                                           ;361.7
        lea       edx, DWORD PTR [68+esp]                       ;361.7
        push      edx                                           ;361.7
        call      _for_write_seq_lis                            ;361.7
                                ; LOE
.B3.91:                         ; Preds .B3.90
        mov       DWORD PTR [72+esp], 0                         ;362.7
        lea       eax, DWORD PTR [288+esp]                      ;362.7
        mov       DWORD PTR [288+esp], 21                       ;362.7
        mov       DWORD PTR [292+esp], OFFSET FLAT: __STRLITPACK_456 ;362.7
        push      32                                            ;362.7
        push      eax                                           ;362.7
        push      OFFSET FLAT: __STRLITPACK_507.0.3             ;362.7
        push      -2088435968                                   ;362.7
        push      666                                           ;362.7
        lea       edx, DWORD PTR [92+esp]                       ;362.7
        push      edx                                           ;362.7
        call      _for_write_seq_lis                            ;362.7
                                ; LOE
.B3.369:                        ; Preds .B3.91
        add       esp, 96                                       ;362.7
                                ; LOE
.B3.92:                         ; Preds .B3.369
        movss     xmm0, DWORD PTR [92+esp]                      ;363.17
        pxor      xmm1, xmm1                                    ;363.17
        comiss    xmm0, xmm1                                    ;363.17
        jbe       .B3.273       ; Prob 50%                      ;363.17
                                ; LOE
.B3.93:                         ; Preds .B3.92
        mov       ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_READVEH+24] ;364.88
        test      ebx, ebx                                      ;364.88
        jle       .B3.349       ; Prob 0%                       ;364.88
                                ; LOE ebx
.B3.94:                         ; Preds .B3.93
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_READVEH+40] ;364.88
        cmp       ebx, 4                                        ;364.88
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_READVEH] ;364.88
        mov       DWORD PTR [36+esp], edx                       ;364.88
        jl        .B3.310       ; Prob 10%                      ;364.88
                                ; LOE eax edx ebx dl dh
.B3.95:                         ; Preds .B3.94
        mov       esi, DWORD PTR [84+esp]                       ;364.88
        mov       ecx, eax                                      ;364.88
        imul      esi, edx                                      ;364.88
        sub       ecx, esi                                      ;364.88
        add       ecx, edx                                      ;364.88
        and       ecx, 15                                       ;364.88
        je        .B3.98        ; Prob 50%                      ;364.88
                                ; LOE eax ecx ebx esi
.B3.96:                         ; Preds .B3.95
        test      cl, 3                                         ;364.88
        jne       .B3.310       ; Prob 10%                      ;364.88
                                ; LOE eax ecx ebx esi
.B3.97:                         ; Preds .B3.96
        neg       ecx                                           ;364.88
        add       ecx, 16                                       ;364.88
        shr       ecx, 2                                        ;364.88
                                ; LOE eax ecx ebx esi
.B3.98:                         ; Preds .B3.97 .B3.95
        lea       edx, DWORD PTR [4+ecx]                        ;364.88
        cmp       ebx, edx                                      ;364.88
        jl        .B3.310       ; Prob 10%                      ;364.88
                                ; LOE eax ecx ebx esi
.B3.99:                         ; Preds .B3.98
        mov       edx, ebx                                      ;364.88
        sub       edx, ecx                                      ;364.88
        sub       esi, DWORD PTR [36+esp]                       ;
        and       edx, 3                                        ;364.88
        neg       edx                                           ;364.88
        neg       esi                                           ;
        add       edx, ebx                                      ;364.88
        add       esi, eax                                      ;
        mov       DWORD PTR [40+esp], 0                         ;
        test      ecx, ecx                                      ;364.88
        jbe       .B3.103       ; Prob 10%                      ;364.88
                                ; LOE eax edx ecx ebx esi
.B3.100:                        ; Preds .B3.99
        xor       edi, edi                                      ;
        mov       DWORD PTR [32+esp], ebx                       ;
        mov       ebx, edi                                      ;
        mov       edi, DWORD PTR [40+esp]                       ;
                                ; LOE eax edx ecx ebx esi edi
.B3.101:                        ; Preds .B3.101 .B3.100
        add       edi, DWORD PTR [esi+ebx*4]                    ;364.88
        inc       ebx                                           ;364.88
        cmp       ebx, ecx                                      ;364.88
        jb        .B3.101       ; Prob 82%                      ;364.88
                                ; LOE eax edx ecx ebx esi edi
.B3.102:                        ; Preds .B3.101
        mov       DWORD PTR [40+esp], edi                       ;
        mov       ebx, DWORD PTR [32+esp]                       ;
                                ; LOE eax edx ecx ebx esi
.B3.103:                        ; Preds .B3.99 .B3.102
        movd      xmm0, DWORD PTR [40+esp]                      ;364.88
                                ; LOE eax edx ecx ebx esi xmm0
.B3.104:                        ; Preds .B3.104 .B3.103
        paddd     xmm0, XMMWORD PTR [esi+ecx*4]                 ;364.88
        add       ecx, 4                                        ;364.88
        cmp       ecx, edx                                      ;364.88
        jb        .B3.104       ; Prob 82%                      ;364.88
                                ; LOE eax edx ecx ebx esi xmm0
.B3.105:                        ; Preds .B3.104
        movdqa    xmm1, xmm0                                    ;364.88
        psrldq    xmm1, 8                                       ;364.88
        paddd     xmm0, xmm1                                    ;364.88
        movdqa    xmm2, xmm0                                    ;364.88
        psrldq    xmm2, 4                                       ;364.88
        paddd     xmm0, xmm2                                    ;364.88
        movd      ecx, xmm0                                     ;364.88
                                ; LOE eax edx ecx ebx
.B3.106:                        ; Preds .B3.105 .B3.310
        cmp       edx, ebx                                      ;364.88
        jae       .B3.110       ; Prob 10%                      ;364.88
                                ; LOE eax edx ecx ebx
.B3.107:                        ; Preds .B3.106
        mov       edi, DWORD PTR [84+esp]                       ;
        mov       esi, DWORD PTR [36+esp]                       ;
        imul      edi, esi                                      ;
        sub       edi, esi                                      ;
        sub       eax, edi                                      ;
                                ; LOE eax edx ecx ebx
.B3.108:                        ; Preds .B3.108 .B3.107
        add       ecx, DWORD PTR [eax+edx*4]                    ;364.88
        inc       edx                                           ;364.88
        cmp       edx, ebx                                      ;364.88
        jb        .B3.108       ; Prob 82%                      ;364.88
                                ; LOE eax edx ecx ebx
.B3.110:                        ; Preds .B3.108 .B3.349 .B3.106
        mov       DWORD PTR [esp], 0                            ;364.9
        lea       eax, DWORD PTR [224+esp]                      ;364.9
        mov       DWORD PTR [224+esp], ecx                      ;364.9
        push      32                                            ;364.9
        push      OFFSET FLAT: PRINTHEADER2$format_pack.0.3+736 ;364.9
        push      eax                                           ;364.9
        push      OFFSET FLAT: __STRLITPACK_508.0.3             ;364.9
        push      -2088435968                                   ;364.9
        push      666                                           ;364.9
        lea       edx, DWORD PTR [24+esp]                       ;364.9
        push      edx                                           ;364.9
        call      _for_write_seq_fmt                            ;364.9
                                ; LOE ebx
.B3.370:                        ; Preds .B3.110
        add       esp, 28                                       ;364.9
                                ; LOE ebx
.B3.111:                        ; Preds .B3.370
        test      ebx, ebx                                      ;364.106
        jle       .B3.346       ; Prob 0%                       ;364.106
                                ; LOE ebx
.B3.112:                        ; Preds .B3.111
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_READVEH+40] ;364.106
        cmp       ebx, 4                                        ;364.106
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_READVEH] ;364.106
        mov       DWORD PTR [36+esp], edx                       ;364.106
        jl        .B3.311       ; Prob 10%                      ;364.106
                                ; LOE eax edx ebx dl dh
.B3.113:                        ; Preds .B3.112
        mov       esi, DWORD PTR [84+esp]                       ;364.106
        mov       ecx, eax                                      ;364.106
        imul      esi, edx                                      ;364.106
        sub       ecx, esi                                      ;364.106
        add       ecx, edx                                      ;364.106
        and       ecx, 15                                       ;364.106
        je        .B3.116       ; Prob 50%                      ;364.106
                                ; LOE eax ecx ebx esi
.B3.114:                        ; Preds .B3.113
        test      cl, 3                                         ;364.106
        jne       .B3.311       ; Prob 10%                      ;364.106
                                ; LOE eax ecx ebx esi
.B3.115:                        ; Preds .B3.114
        neg       ecx                                           ;364.106
        add       ecx, 16                                       ;364.106
        shr       ecx, 2                                        ;364.106
                                ; LOE eax ecx ebx esi
.B3.116:                        ; Preds .B3.115 .B3.113
        lea       edx, DWORD PTR [4+ecx]                        ;364.106
        cmp       ebx, edx                                      ;364.106
        jl        .B3.311       ; Prob 10%                      ;364.106
                                ; LOE eax ecx ebx esi
.B3.117:                        ; Preds .B3.116
        mov       edx, ebx                                      ;364.106
        sub       edx, ecx                                      ;364.106
        sub       esi, DWORD PTR [36+esp]                       ;
        and       edx, 3                                        ;364.106
        neg       edx                                           ;364.106
        neg       esi                                           ;
        add       edx, ebx                                      ;364.106
        add       esi, eax                                      ;
        mov       DWORD PTR [40+esp], 0                         ;
        test      ecx, ecx                                      ;364.106
        jbe       .B3.121       ; Prob 10%                      ;364.106
                                ; LOE eax edx ecx ebx esi
.B3.118:                        ; Preds .B3.117
        xor       edi, edi                                      ;
        mov       DWORD PTR [32+esp], ebx                       ;
        mov       ebx, edi                                      ;
        mov       edi, DWORD PTR [40+esp]                       ;
                                ; LOE eax edx ecx ebx esi edi
.B3.119:                        ; Preds .B3.119 .B3.118
        add       edi, DWORD PTR [esi+ebx*4]                    ;364.106
        inc       ebx                                           ;364.106
        cmp       ebx, ecx                                      ;364.106
        jb        .B3.119       ; Prob 82%                      ;364.106
                                ; LOE eax edx ecx ebx esi edi
.B3.120:                        ; Preds .B3.119
        mov       DWORD PTR [40+esp], edi                       ;
        mov       ebx, DWORD PTR [32+esp]                       ;
                                ; LOE eax edx ecx ebx esi
.B3.121:                        ; Preds .B3.117 .B3.120
        movd      xmm0, DWORD PTR [40+esp]                      ;364.106
                                ; LOE eax edx ecx ebx esi xmm0
.B3.122:                        ; Preds .B3.122 .B3.121
        paddd     xmm0, XMMWORD PTR [esi+ecx*4]                 ;364.106
        add       ecx, 4                                        ;364.106
        cmp       ecx, edx                                      ;364.106
        jb        .B3.122       ; Prob 82%                      ;364.106
                                ; LOE eax edx ecx ebx esi xmm0
.B3.123:                        ; Preds .B3.122
        movdqa    xmm1, xmm0                                    ;364.106
        psrldq    xmm1, 8                                       ;364.106
        paddd     xmm0, xmm1                                    ;364.106
        movdqa    xmm2, xmm0                                    ;364.106
        psrldq    xmm2, 4                                       ;364.106
        paddd     xmm0, xmm2                                    ;364.106
        movd      ecx, xmm0                                     ;364.106
                                ; LOE eax edx ecx ebx
.B3.124:                        ; Preds .B3.123 .B3.311
        cmp       edx, ebx                                      ;364.106
        jae       .B3.128       ; Prob 10%                      ;364.106
                                ; LOE eax edx ecx ebx
.B3.125:                        ; Preds .B3.124
        mov       edi, DWORD PTR [84+esp]                       ;
        mov       esi, DWORD PTR [36+esp]                       ;
        imul      edi, esi                                      ;
        sub       edi, esi                                      ;
        sub       eax, edi                                      ;
                                ; LOE eax edx ecx ebx
.B3.126:                        ; Preds .B3.126 .B3.125
        add       ecx, DWORD PTR [eax+edx*4]                    ;364.106
        inc       edx                                           ;364.106
        cmp       edx, ebx                                      ;364.106
        jb        .B3.126       ; Prob 82%                      ;364.106
                                ; LOE eax edx ecx ebx
.B3.128:                        ; Preds .B3.126 .B3.346 .B3.124
        cvtsi2ss  xmm0, ecx                                     ;364.106
        divss     xmm0, DWORD PTR [92+esp]                      ;364.123
        mulss     xmm0, DWORD PTR [_2il0floatpacket.7]          ;364.9
        lea       eax, DWORD PTR [232+esp]                      ;364.9
        movss     DWORD PTR [232+esp], xmm0                     ;364.9
        push      eax                                           ;364.9
        push      OFFSET FLAT: __STRLITPACK_509.0.3             ;364.9
        lea       edx, DWORD PTR [8+esp]                        ;364.9
        push      edx                                           ;364.9
        call      _for_write_seq_fmt_xmit                       ;364.9
                                ; LOE ebx
.B3.371:                        ; Preds .B3.128
        add       esp, 12                                       ;364.9
                                ; LOE ebx
.B3.129:                        ; Preds .B3.371
        test      ebx, ebx                                      ;365.85
        jle       .B3.343       ; Prob 0%                       ;365.85
                                ; LOE ebx
.B3.130:                        ; Preds .B3.129
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_READVEH] ;365.85
        cmp       ebx, 4                                        ;365.85
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_READVEH+40] ;365.85
        mov       DWORD PTR [36+esp], eax                       ;365.85
        jl        .B3.312       ; Prob 10%                      ;365.85
                                ; LOE edx ebx
.B3.131:                        ; Preds .B3.130
        mov       esi, DWORD PTR [84+esp]                       ;365.85
        lea       eax, DWORD PTR [edx+edx]                      ;365.85
        imul      esi, edx                                      ;365.85
        mov       ecx, DWORD PTR [36+esp]                       ;365.85
        sub       ecx, esi                                      ;365.85
        mov       DWORD PTR [40+esp], eax                       ;365.85
        lea       ecx, DWORD PTR [ecx+edx*2]                    ;365.85
        and       ecx, 15                                       ;365.85
        je        .B3.134       ; Prob 50%                      ;365.85
                                ; LOE edx ecx ebx esi
.B3.132:                        ; Preds .B3.131
        test      cl, 3                                         ;365.85
        jne       .B3.312       ; Prob 10%                      ;365.85
                                ; LOE edx ecx ebx esi
.B3.133:                        ; Preds .B3.132
        neg       ecx                                           ;365.85
        add       ecx, 16                                       ;365.85
        shr       ecx, 2                                        ;365.85
                                ; LOE edx ecx ebx esi
.B3.134:                        ; Preds .B3.133 .B3.131
        lea       eax, DWORD PTR [4+ecx]                        ;365.85
        cmp       ebx, eax                                      ;365.85
        jl        .B3.312       ; Prob 10%                      ;365.85
                                ; LOE edx ecx ebx esi
.B3.135:                        ; Preds .B3.134
        mov       eax, ebx                                      ;365.85
        sub       eax, ecx                                      ;365.85
        sub       esi, DWORD PTR [40+esp]                       ;
        and       eax, 3                                        ;365.85
        neg       eax                                           ;365.85
        neg       esi                                           ;
        add       esi, DWORD PTR [36+esp]                       ;
        add       eax, ebx                                      ;365.85
        mov       DWORD PTR [44+esp], 0                         ;
        test      ecx, ecx                                      ;365.85
        jbe       .B3.139       ; Prob 10%                      ;365.85
                                ; LOE eax edx ecx ebx esi
.B3.136:                        ; Preds .B3.135
        xor       edi, edi                                      ;
        mov       DWORD PTR [32+esp], ebx                       ;
        mov       ebx, edi                                      ;
        mov       edi, DWORD PTR [44+esp]                       ;
                                ; LOE eax edx ecx ebx esi edi
.B3.137:                        ; Preds .B3.137 .B3.136
        add       edi, DWORD PTR [esi+ebx*4]                    ;365.85
        inc       ebx                                           ;365.85
        cmp       ebx, ecx                                      ;365.85
        jb        .B3.137       ; Prob 82%                      ;365.85
                                ; LOE eax edx ecx ebx esi edi
.B3.138:                        ; Preds .B3.137
        mov       DWORD PTR [44+esp], edi                       ;
        mov       ebx, DWORD PTR [32+esp]                       ;
                                ; LOE eax edx ecx ebx esi
.B3.139:                        ; Preds .B3.135 .B3.138
        movd      xmm0, DWORD PTR [44+esp]                      ;365.85
                                ; LOE eax edx ecx ebx esi xmm0
.B3.140:                        ; Preds .B3.140 .B3.139
        paddd     xmm0, XMMWORD PTR [esi+ecx*4]                 ;365.85
        add       ecx, 4                                        ;365.85
        cmp       ecx, eax                                      ;365.85
        jb        .B3.140       ; Prob 82%                      ;365.85
                                ; LOE eax edx ecx ebx esi xmm0
.B3.141:                        ; Preds .B3.140
        movdqa    xmm1, xmm0                                    ;365.85
        psrldq    xmm1, 8                                       ;365.85
        paddd     xmm0, xmm1                                    ;365.85
        movdqa    xmm2, xmm0                                    ;365.85
        psrldq    xmm2, 4                                       ;365.85
        paddd     xmm0, xmm2                                    ;365.85
        movd      esi, xmm0                                     ;365.85
                                ; LOE eax edx ebx esi
.B3.142:                        ; Preds .B3.141 .B3.312
        cmp       eax, ebx                                      ;365.85
        jae       .B3.146       ; Prob 10%                      ;365.85
                                ; LOE eax edx ebx esi
.B3.143:                        ; Preds .B3.142
        mov       ecx, DWORD PTR [84+esp]                       ;
        neg       ecx                                           ;
        add       ecx, 2                                        ;
        imul      ecx, edx                                      ;
        add       ecx, DWORD PTR [36+esp]                       ;
                                ; LOE eax ecx ebx esi
.B3.144:                        ; Preds .B3.144 .B3.143
        add       esi, DWORD PTR [ecx+eax*4]                    ;365.85
        inc       eax                                           ;365.85
        cmp       eax, ebx                                      ;365.85
        jb        .B3.144       ; Prob 82%                      ;365.85
                                ; LOE eax ecx ebx esi
.B3.146:                        ; Preds .B3.144 .B3.343 .B3.142
        mov       DWORD PTR [esp], 0                            ;365.6
        lea       eax, DWORD PTR [240+esp]                      ;365.6
        mov       DWORD PTR [240+esp], esi                      ;365.6
        push      32                                            ;365.6
        push      OFFSET FLAT: PRINTHEADER2$format_pack.0.3+828 ;365.6
        push      eax                                           ;365.6
        push      OFFSET FLAT: __STRLITPACK_510.0.3             ;365.6
        push      -2088435968                                   ;365.6
        push      666                                           ;365.6
        lea       edx, DWORD PTR [24+esp]                       ;365.6
        push      edx                                           ;365.6
        call      _for_write_seq_fmt                            ;365.6
                                ; LOE ebx
.B3.372:                        ; Preds .B3.146
        add       esp, 28                                       ;365.6
                                ; LOE ebx
.B3.147:                        ; Preds .B3.372
        test      ebx, ebx                                      ;365.103
        jle       .B3.340       ; Prob 0%                       ;365.103
                                ; LOE ebx
.B3.148:                        ; Preds .B3.147
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_READVEH] ;365.103
        cmp       ebx, 4                                        ;365.103
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_READVEH+40] ;365.103
        mov       DWORD PTR [36+esp], eax                       ;365.103
        jl        .B3.313       ; Prob 10%                      ;365.103
                                ; LOE edx ebx
.B3.149:                        ; Preds .B3.148
        mov       esi, DWORD PTR [84+esp]                       ;365.103
        lea       eax, DWORD PTR [edx+edx]                      ;365.103
        imul      esi, edx                                      ;365.103
        mov       ecx, DWORD PTR [36+esp]                       ;365.103
        sub       ecx, esi                                      ;365.103
        mov       DWORD PTR [40+esp], eax                       ;365.103
        lea       ecx, DWORD PTR [ecx+edx*2]                    ;365.103
        and       ecx, 15                                       ;365.103
        je        .B3.152       ; Prob 50%                      ;365.103
                                ; LOE edx ecx ebx esi
.B3.150:                        ; Preds .B3.149
        test      cl, 3                                         ;365.103
        jne       .B3.313       ; Prob 10%                      ;365.103
                                ; LOE edx ecx ebx esi
.B3.151:                        ; Preds .B3.150
        neg       ecx                                           ;365.103
        add       ecx, 16                                       ;365.103
        shr       ecx, 2                                        ;365.103
                                ; LOE edx ecx ebx esi
.B3.152:                        ; Preds .B3.151 .B3.149
        lea       eax, DWORD PTR [4+ecx]                        ;365.103
        cmp       ebx, eax                                      ;365.103
        jl        .B3.313       ; Prob 10%                      ;365.103
                                ; LOE edx ecx ebx esi
.B3.153:                        ; Preds .B3.152
        mov       eax, ebx                                      ;365.103
        sub       eax, ecx                                      ;365.103
        sub       esi, DWORD PTR [40+esp]                       ;
        and       eax, 3                                        ;365.103
        neg       eax                                           ;365.103
        neg       esi                                           ;
        add       esi, DWORD PTR [36+esp]                       ;
        add       eax, ebx                                      ;365.103
        mov       DWORD PTR [44+esp], 0                         ;
        test      ecx, ecx                                      ;365.103
        jbe       .B3.157       ; Prob 10%                      ;365.103
                                ; LOE eax edx ecx ebx esi
.B3.154:                        ; Preds .B3.153
        xor       edi, edi                                      ;
        mov       DWORD PTR [32+esp], ebx                       ;
        mov       ebx, edi                                      ;
        mov       edi, DWORD PTR [44+esp]                       ;
                                ; LOE eax edx ecx ebx esi edi
.B3.155:                        ; Preds .B3.155 .B3.154
        add       edi, DWORD PTR [esi+ebx*4]                    ;365.103
        inc       ebx                                           ;365.103
        cmp       ebx, ecx                                      ;365.103
        jb        .B3.155       ; Prob 82%                      ;365.103
                                ; LOE eax edx ecx ebx esi edi
.B3.156:                        ; Preds .B3.155
        mov       DWORD PTR [44+esp], edi                       ;
        mov       ebx, DWORD PTR [32+esp]                       ;
                                ; LOE eax edx ecx ebx esi
.B3.157:                        ; Preds .B3.153 .B3.156
        movd      xmm0, DWORD PTR [44+esp]                      ;365.103
                                ; LOE eax edx ecx ebx esi xmm0
.B3.158:                        ; Preds .B3.158 .B3.157
        paddd     xmm0, XMMWORD PTR [esi+ecx*4]                 ;365.103
        add       ecx, 4                                        ;365.103
        cmp       ecx, eax                                      ;365.103
        jb        .B3.158       ; Prob 82%                      ;365.103
                                ; LOE eax edx ecx ebx esi xmm0
.B3.159:                        ; Preds .B3.158
        movdqa    xmm1, xmm0                                    ;365.103
        psrldq    xmm1, 8                                       ;365.103
        paddd     xmm0, xmm1                                    ;365.103
        movdqa    xmm2, xmm0                                    ;365.103
        psrldq    xmm2, 4                                       ;365.103
        paddd     xmm0, xmm2                                    ;365.103
        movd      esi, xmm0                                     ;365.103
                                ; LOE eax edx ebx esi
.B3.160:                        ; Preds .B3.159 .B3.313
        cmp       eax, ebx                                      ;365.103
        jae       .B3.164       ; Prob 10%                      ;365.103
                                ; LOE eax edx ebx esi
.B3.161:                        ; Preds .B3.160
        mov       ecx, DWORD PTR [84+esp]                       ;
        neg       ecx                                           ;
        add       ecx, 2                                        ;
        imul      ecx, edx                                      ;
        add       ecx, DWORD PTR [36+esp]                       ;
                                ; LOE eax ecx ebx esi
.B3.162:                        ; Preds .B3.162 .B3.161
        add       esi, DWORD PTR [ecx+eax*4]                    ;365.103
        inc       eax                                           ;365.103
        cmp       eax, ebx                                      ;365.103
        jb        .B3.162       ; Prob 82%                      ;365.103
                                ; LOE eax ecx ebx esi
.B3.164:                        ; Preds .B3.162 .B3.340 .B3.160
        cvtsi2ss  xmm0, esi                                     ;365.103
        divss     xmm0, DWORD PTR [92+esp]                      ;365.120
        mulss     xmm0, DWORD PTR [_2il0floatpacket.7]          ;365.6
        lea       eax, DWORD PTR [248+esp]                      ;365.6
        movss     DWORD PTR [248+esp], xmm0                     ;365.6
        push      eax                                           ;365.6
        push      OFFSET FLAT: __STRLITPACK_511.0.3             ;365.6
        lea       edx, DWORD PTR [8+esp]                        ;365.6
        push      edx                                           ;365.6
        call      _for_write_seq_fmt_xmit                       ;365.6
                                ; LOE ebx
.B3.373:                        ; Preds .B3.164
        add       esp, 12                                       ;365.6
                                ; LOE ebx
.B3.165:                        ; Preds .B3.373
        test      ebx, ebx                                      ;366.85
        jle       .B3.337       ; Prob 0%                       ;366.85
                                ; LOE ebx
.B3.166:                        ; Preds .B3.165
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_READVEH+40] ;366.85
        cmp       ebx, 4                                        ;366.85
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_READVEH] ;366.85
        mov       DWORD PTR [36+esp], eax                       ;366.85
        jl        .B3.314       ; Prob 10%                      ;366.85
                                ; LOE eax edx ebx al ah
.B3.167:                        ; Preds .B3.166
        mov       ecx, eax                                      ;366.85
        mov       esi, DWORD PTR [84+esp]                       ;366.85
        imul      esi, ecx                                      ;366.85
        lea       eax, DWORD PTR [ecx+ecx*2]                    ;366.85
        mov       ecx, edx                                      ;366.85
        sub       ecx, esi                                      ;366.85
        add       ecx, eax                                      ;366.85
        mov       DWORD PTR [40+esp], eax                       ;366.85
        and       ecx, 15                                       ;366.85
        je        .B3.170       ; Prob 50%                      ;366.85
                                ; LOE edx ecx ebx esi
.B3.168:                        ; Preds .B3.167
        test      cl, 3                                         ;366.85
        jne       .B3.314       ; Prob 10%                      ;366.85
                                ; LOE edx ecx ebx esi
.B3.169:                        ; Preds .B3.168
        neg       ecx                                           ;366.85
        add       ecx, 16                                       ;366.85
        shr       ecx, 2                                        ;366.85
                                ; LOE edx ecx ebx esi
.B3.170:                        ; Preds .B3.169 .B3.167
        lea       eax, DWORD PTR [4+ecx]                        ;366.85
        cmp       ebx, eax                                      ;366.85
        jl        .B3.314       ; Prob 10%                      ;366.85
                                ; LOE edx ecx ebx esi
.B3.171:                        ; Preds .B3.170
        mov       eax, ebx                                      ;366.85
        xor       edi, edi                                      ;
        sub       eax, ecx                                      ;366.85
        sub       esi, DWORD PTR [40+esp]                       ;
        and       eax, 3                                        ;366.85
        neg       eax                                           ;366.85
        neg       esi                                           ;
        add       eax, ebx                                      ;366.85
        add       esi, edx                                      ;
        mov       DWORD PTR [44+esp], edi                       ;
        test      ecx, ecx                                      ;366.85
        jbe       .B3.175       ; Prob 10%                      ;366.85
                                ; LOE eax edx ecx ebx esi
.B3.172:                        ; Preds .B3.171
        xor       edi, edi                                      ;
        mov       DWORD PTR [32+esp], ebx                       ;
        mov       ebx, edi                                      ;
        mov       edi, DWORD PTR [44+esp]                       ;
                                ; LOE eax edx ecx ebx esi edi
.B3.173:                        ; Preds .B3.173 .B3.172
        add       edi, DWORD PTR [esi+ebx*4]                    ;366.85
        inc       ebx                                           ;366.85
        cmp       ebx, ecx                                      ;366.85
        jb        .B3.173       ; Prob 82%                      ;366.85
                                ; LOE eax edx ecx ebx esi edi
.B3.174:                        ; Preds .B3.173
        mov       DWORD PTR [44+esp], edi                       ;
        mov       ebx, DWORD PTR [32+esp]                       ;
                                ; LOE eax edx ecx ebx esi
.B3.175:                        ; Preds .B3.171 .B3.174
        movd      xmm0, DWORD PTR [44+esp]                      ;366.85
                                ; LOE eax edx ecx ebx esi xmm0
.B3.176:                        ; Preds .B3.176 .B3.175
        paddd     xmm0, XMMWORD PTR [esi+ecx*4]                 ;366.85
        add       ecx, 4                                        ;366.85
        cmp       ecx, eax                                      ;366.85
        jb        .B3.176       ; Prob 82%                      ;366.85
                                ; LOE eax edx ecx ebx esi xmm0
.B3.177:                        ; Preds .B3.176
        movdqa    xmm1, xmm0                                    ;366.85
        psrldq    xmm1, 8                                       ;366.85
        paddd     xmm0, xmm1                                    ;366.85
        movdqa    xmm2, xmm0                                    ;366.85
        psrldq    xmm2, 4                                       ;366.85
        paddd     xmm0, xmm2                                    ;366.85
        movd      esi, xmm0                                     ;366.85
                                ; LOE eax edx ebx esi
.B3.178:                        ; Preds .B3.177 .B3.314
        cmp       eax, ebx                                      ;366.85
        jae       .B3.182       ; Prob 10%                      ;366.85
                                ; LOE eax edx ebx esi
.B3.179:                        ; Preds .B3.178
        mov       ecx, DWORD PTR [84+esp]                       ;
        neg       ecx                                           ;
        add       ecx, 3                                        ;
        imul      ecx, DWORD PTR [36+esp]                       ;
        add       ecx, edx                                      ;
                                ; LOE eax ecx ebx esi
.B3.180:                        ; Preds .B3.180 .B3.179
        add       esi, DWORD PTR [ecx+eax*4]                    ;366.85
        inc       eax                                           ;366.85
        cmp       eax, ebx                                      ;366.85
        jb        .B3.180       ; Prob 82%                      ;366.85
                                ; LOE eax ecx ebx esi
.B3.182:                        ; Preds .B3.180 .B3.337 .B3.178
        mov       DWORD PTR [esp], 0                            ;366.6
        lea       eax, DWORD PTR [256+esp]                      ;366.6
        mov       DWORD PTR [256+esp], esi                      ;366.6
        push      32                                            ;366.6
        push      OFFSET FLAT: PRINTHEADER2$format_pack.0.3+920 ;366.6
        push      eax                                           ;366.6
        push      OFFSET FLAT: __STRLITPACK_512.0.3             ;366.6
        push      -2088435968                                   ;366.6
        push      666                                           ;366.6
        lea       edx, DWORD PTR [24+esp]                       ;366.6
        push      edx                                           ;366.6
        call      _for_write_seq_fmt                            ;366.6
                                ; LOE ebx
.B3.374:                        ; Preds .B3.182
        add       esp, 28                                       ;366.6
                                ; LOE ebx
.B3.183:                        ; Preds .B3.374
        test      ebx, ebx                                      ;366.103
        jle       .B3.334       ; Prob 0%                       ;366.103
                                ; LOE ebx
.B3.184:                        ; Preds .B3.183
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_READVEH+40] ;366.103
        cmp       ebx, 4                                        ;366.103
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_READVEH] ;366.103
        mov       DWORD PTR [36+esp], eax                       ;366.103
        jl        .B3.315       ; Prob 10%                      ;366.103
                                ; LOE eax edx ebx al ah
.B3.185:                        ; Preds .B3.184
        mov       ecx, eax                                      ;366.103
        mov       esi, DWORD PTR [84+esp]                       ;366.103
        imul      esi, ecx                                      ;366.103
        lea       eax, DWORD PTR [ecx+ecx*2]                    ;366.103
        mov       ecx, edx                                      ;366.103
        sub       ecx, esi                                      ;366.103
        add       ecx, eax                                      ;366.103
        mov       DWORD PTR [40+esp], eax                       ;366.103
        and       ecx, 15                                       ;366.103
        je        .B3.188       ; Prob 50%                      ;366.103
                                ; LOE edx ecx ebx esi
.B3.186:                        ; Preds .B3.185
        test      cl, 3                                         ;366.103
        jne       .B3.315       ; Prob 10%                      ;366.103
                                ; LOE edx ecx ebx esi
.B3.187:                        ; Preds .B3.186
        neg       ecx                                           ;366.103
        add       ecx, 16                                       ;366.103
        shr       ecx, 2                                        ;366.103
                                ; LOE edx ecx ebx esi
.B3.188:                        ; Preds .B3.187 .B3.185
        lea       eax, DWORD PTR [4+ecx]                        ;366.103
        cmp       ebx, eax                                      ;366.103
        jl        .B3.315       ; Prob 10%                      ;366.103
                                ; LOE edx ecx ebx esi
.B3.189:                        ; Preds .B3.188
        mov       eax, ebx                                      ;366.103
        xor       edi, edi                                      ;
        sub       eax, ecx                                      ;366.103
        sub       esi, DWORD PTR [40+esp]                       ;
        and       eax, 3                                        ;366.103
        neg       eax                                           ;366.103
        neg       esi                                           ;
        add       eax, ebx                                      ;366.103
        add       esi, edx                                      ;
        mov       DWORD PTR [44+esp], edi                       ;
        test      ecx, ecx                                      ;366.103
        jbe       .B3.193       ; Prob 10%                      ;366.103
                                ; LOE eax edx ecx ebx esi
.B3.190:                        ; Preds .B3.189
        xor       edi, edi                                      ;
        mov       DWORD PTR [32+esp], ebx                       ;
        mov       ebx, edi                                      ;
        mov       edi, DWORD PTR [44+esp]                       ;
                                ; LOE eax edx ecx ebx esi edi
.B3.191:                        ; Preds .B3.191 .B3.190
        add       edi, DWORD PTR [esi+ebx*4]                    ;366.103
        inc       ebx                                           ;366.103
        cmp       ebx, ecx                                      ;366.103
        jb        .B3.191       ; Prob 82%                      ;366.103
                                ; LOE eax edx ecx ebx esi edi
.B3.192:                        ; Preds .B3.191
        mov       DWORD PTR [44+esp], edi                       ;
        mov       ebx, DWORD PTR [32+esp]                       ;
                                ; LOE eax edx ecx ebx esi
.B3.193:                        ; Preds .B3.189 .B3.192
        movd      xmm0, DWORD PTR [44+esp]                      ;366.103
                                ; LOE eax edx ecx ebx esi xmm0
.B3.194:                        ; Preds .B3.194 .B3.193
        paddd     xmm0, XMMWORD PTR [esi+ecx*4]                 ;366.103
        add       ecx, 4                                        ;366.103
        cmp       ecx, eax                                      ;366.103
        jb        .B3.194       ; Prob 82%                      ;366.103
                                ; LOE eax edx ecx ebx esi xmm0
.B3.195:                        ; Preds .B3.194
        movdqa    xmm1, xmm0                                    ;366.103
        psrldq    xmm1, 8                                       ;366.103
        paddd     xmm0, xmm1                                    ;366.103
        movdqa    xmm2, xmm0                                    ;366.103
        psrldq    xmm2, 4                                       ;366.103
        paddd     xmm0, xmm2                                    ;366.103
        movd      esi, xmm0                                     ;366.103
                                ; LOE eax edx ebx esi
.B3.196:                        ; Preds .B3.195 .B3.315
        cmp       eax, ebx                                      ;366.103
        jae       .B3.200       ; Prob 10%                      ;366.103
                                ; LOE eax edx ebx esi
.B3.197:                        ; Preds .B3.196
        mov       ecx, DWORD PTR [84+esp]                       ;
        neg       ecx                                           ;
        add       ecx, 3                                        ;
        imul      ecx, DWORD PTR [36+esp]                       ;
        add       ecx, edx                                      ;
                                ; LOE eax ecx ebx esi
.B3.198:                        ; Preds .B3.198 .B3.197
        add       esi, DWORD PTR [ecx+eax*4]                    ;366.103
        inc       eax                                           ;366.103
        cmp       eax, ebx                                      ;366.103
        jb        .B3.198       ; Prob 82%                      ;366.103
                                ; LOE eax ecx ebx esi
.B3.200:                        ; Preds .B3.198 .B3.334 .B3.196
        cvtsi2ss  xmm0, esi                                     ;366.103
        divss     xmm0, DWORD PTR [92+esp]                      ;366.120
        mulss     xmm0, DWORD PTR [_2il0floatpacket.7]          ;366.6
        lea       eax, DWORD PTR [264+esp]                      ;366.6
        movss     DWORD PTR [264+esp], xmm0                     ;366.6
        push      eax                                           ;366.6
        push      OFFSET FLAT: __STRLITPACK_513.0.3             ;366.6
        lea       edx, DWORD PTR [8+esp]                        ;366.6
        push      edx                                           ;366.6
        call      _for_write_seq_fmt_xmit                       ;366.6
                                ; LOE ebx
.B3.375:                        ; Preds .B3.200
        add       esp, 12                                       ;366.6
                                ; LOE ebx
.B3.201:                        ; Preds .B3.375
        test      ebx, ebx                                      ;367.85
        jle       .B3.331       ; Prob 0%                       ;367.85
                                ; LOE ebx
.B3.202:                        ; Preds .B3.201
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_READVEH] ;367.85
        cmp       ebx, 4                                        ;367.85
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_READVEH+40] ;367.85
        mov       DWORD PTR [36+esp], eax                       ;367.85
        jl        .B3.316       ; Prob 10%                      ;367.85
                                ; LOE edx ebx
.B3.203:                        ; Preds .B3.202
        mov       esi, DWORD PTR [84+esp]                       ;367.85
        lea       eax, DWORD PTR [edx*4]                        ;367.85
        imul      esi, edx                                      ;367.85
        mov       ecx, DWORD PTR [36+esp]                       ;367.85
        sub       ecx, esi                                      ;367.85
        mov       DWORD PTR [40+esp], eax                       ;367.85
        lea       ecx, DWORD PTR [ecx+edx*4]                    ;367.85
        and       ecx, 15                                       ;367.85
        je        .B3.206       ; Prob 50%                      ;367.85
                                ; LOE edx ecx ebx esi
.B3.204:                        ; Preds .B3.203
        test      cl, 3                                         ;367.85
        jne       .B3.316       ; Prob 10%                      ;367.85
                                ; LOE edx ecx ebx esi
.B3.205:                        ; Preds .B3.204
        neg       ecx                                           ;367.85
        add       ecx, 16                                       ;367.85
        shr       ecx, 2                                        ;367.85
                                ; LOE edx ecx ebx esi
.B3.206:                        ; Preds .B3.205 .B3.203
        lea       eax, DWORD PTR [4+ecx]                        ;367.85
        cmp       ebx, eax                                      ;367.85
        jl        .B3.316       ; Prob 10%                      ;367.85
                                ; LOE edx ecx ebx esi
.B3.207:                        ; Preds .B3.206
        mov       eax, ebx                                      ;367.85
        sub       eax, ecx                                      ;367.85
        sub       esi, DWORD PTR [40+esp]                       ;
        and       eax, 3                                        ;367.85
        neg       eax                                           ;367.85
        neg       esi                                           ;
        add       esi, DWORD PTR [36+esp]                       ;
        add       eax, ebx                                      ;367.85
        mov       DWORD PTR [44+esp], 0                         ;
        test      ecx, ecx                                      ;367.85
        jbe       .B3.211       ; Prob 10%                      ;367.85
                                ; LOE eax edx ecx ebx esi
.B3.208:                        ; Preds .B3.207
        xor       edi, edi                                      ;
        mov       DWORD PTR [32+esp], ebx                       ;
        mov       ebx, edi                                      ;
        mov       edi, DWORD PTR [44+esp]                       ;
                                ; LOE eax edx ecx ebx esi edi
.B3.209:                        ; Preds .B3.209 .B3.208
        add       edi, DWORD PTR [esi+ebx*4]                    ;367.85
        inc       ebx                                           ;367.85
        cmp       ebx, ecx                                      ;367.85
        jb        .B3.209       ; Prob 82%                      ;367.85
                                ; LOE eax edx ecx ebx esi edi
.B3.210:                        ; Preds .B3.209
        mov       DWORD PTR [44+esp], edi                       ;
        mov       ebx, DWORD PTR [32+esp]                       ;
                                ; LOE eax edx ecx ebx esi
.B3.211:                        ; Preds .B3.207 .B3.210
        movd      xmm0, DWORD PTR [44+esp]                      ;367.85
                                ; LOE eax edx ecx ebx esi xmm0
.B3.212:                        ; Preds .B3.212 .B3.211
        paddd     xmm0, XMMWORD PTR [esi+ecx*4]                 ;367.85
        add       ecx, 4                                        ;367.85
        cmp       ecx, eax                                      ;367.85
        jb        .B3.212       ; Prob 82%                      ;367.85
                                ; LOE eax edx ecx ebx esi xmm0
.B3.213:                        ; Preds .B3.212
        movdqa    xmm1, xmm0                                    ;367.85
        psrldq    xmm1, 8                                       ;367.85
        paddd     xmm0, xmm1                                    ;367.85
        movdqa    xmm2, xmm0                                    ;367.85
        psrldq    xmm2, 4                                       ;367.85
        paddd     xmm0, xmm2                                    ;367.85
        movd      esi, xmm0                                     ;367.85
                                ; LOE eax edx ebx esi
.B3.214:                        ; Preds .B3.213 .B3.316
        cmp       eax, ebx                                      ;367.85
        jae       .B3.218       ; Prob 10%                      ;367.85
                                ; LOE eax edx ebx esi
.B3.215:                        ; Preds .B3.214
        mov       ecx, DWORD PTR [84+esp]                       ;
        neg       ecx                                           ;
        add       ecx, 4                                        ;
        imul      ecx, edx                                      ;
        add       ecx, DWORD PTR [36+esp]                       ;
                                ; LOE eax ecx ebx esi
.B3.216:                        ; Preds .B3.216 .B3.215
        add       esi, DWORD PTR [ecx+eax*4]                    ;367.85
        inc       eax                                           ;367.85
        cmp       eax, ebx                                      ;367.85
        jb        .B3.216       ; Prob 82%                      ;367.85
                                ; LOE eax ecx ebx esi
.B3.218:                        ; Preds .B3.216 .B3.331 .B3.214
        mov       DWORD PTR [esp], 0                            ;367.6
        lea       eax, DWORD PTR [272+esp]                      ;367.6
        mov       DWORD PTR [272+esp], esi                      ;367.6
        push      32                                            ;367.6
        push      OFFSET FLAT: PRINTHEADER2$format_pack.0.3+1012 ;367.6
        push      eax                                           ;367.6
        push      OFFSET FLAT: __STRLITPACK_514.0.3             ;367.6
        push      -2088435968                                   ;367.6
        push      666                                           ;367.6
        lea       edx, DWORD PTR [24+esp]                       ;367.6
        push      edx                                           ;367.6
        call      _for_write_seq_fmt                            ;367.6
                                ; LOE ebx
.B3.376:                        ; Preds .B3.218
        add       esp, 28                                       ;367.6
                                ; LOE ebx
.B3.219:                        ; Preds .B3.376
        test      ebx, ebx                                      ;367.103
        jle       .B3.328       ; Prob 0%                       ;367.103
                                ; LOE ebx
.B3.220:                        ; Preds .B3.219
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_READVEH] ;367.103
        cmp       ebx, 4                                        ;367.103
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_READVEH+40] ;367.103
        mov       DWORD PTR [36+esp], eax                       ;367.103
        jl        .B3.317       ; Prob 10%                      ;367.103
                                ; LOE edx ebx
.B3.221:                        ; Preds .B3.220
        mov       esi, DWORD PTR [84+esp]                       ;367.103
        lea       eax, DWORD PTR [edx*4]                        ;367.103
        imul      esi, edx                                      ;367.103
        mov       ecx, DWORD PTR [36+esp]                       ;367.103
        sub       ecx, esi                                      ;367.103
        mov       DWORD PTR [40+esp], eax                       ;367.103
        lea       ecx, DWORD PTR [ecx+edx*4]                    ;367.103
        and       ecx, 15                                       ;367.103
        je        .B3.224       ; Prob 50%                      ;367.103
                                ; LOE edx ecx ebx esi
.B3.222:                        ; Preds .B3.221
        test      cl, 3                                         ;367.103
        jne       .B3.317       ; Prob 10%                      ;367.103
                                ; LOE edx ecx ebx esi
.B3.223:                        ; Preds .B3.222
        neg       ecx                                           ;367.103
        add       ecx, 16                                       ;367.103
        shr       ecx, 2                                        ;367.103
                                ; LOE edx ecx ebx esi
.B3.224:                        ; Preds .B3.223 .B3.221
        lea       eax, DWORD PTR [4+ecx]                        ;367.103
        cmp       ebx, eax                                      ;367.103
        jl        .B3.317       ; Prob 10%                      ;367.103
                                ; LOE edx ecx ebx esi
.B3.225:                        ; Preds .B3.224
        mov       eax, ebx                                      ;367.103
        sub       eax, ecx                                      ;367.103
        sub       esi, DWORD PTR [40+esp]                       ;
        and       eax, 3                                        ;367.103
        neg       eax                                           ;367.103
        neg       esi                                           ;
        add       esi, DWORD PTR [36+esp]                       ;
        add       eax, ebx                                      ;367.103
        mov       DWORD PTR [44+esp], 0                         ;
        test      ecx, ecx                                      ;367.103
        jbe       .B3.229       ; Prob 10%                      ;367.103
                                ; LOE eax edx ecx ebx esi
.B3.226:                        ; Preds .B3.225
        xor       edi, edi                                      ;
        mov       DWORD PTR [32+esp], ebx                       ;
        mov       ebx, edi                                      ;
        mov       edi, DWORD PTR [44+esp]                       ;
                                ; LOE eax edx ecx ebx esi edi
.B3.227:                        ; Preds .B3.227 .B3.226
        add       edi, DWORD PTR [esi+ebx*4]                    ;367.103
        inc       ebx                                           ;367.103
        cmp       ebx, ecx                                      ;367.103
        jb        .B3.227       ; Prob 82%                      ;367.103
                                ; LOE eax edx ecx ebx esi edi
.B3.228:                        ; Preds .B3.227
        mov       DWORD PTR [44+esp], edi                       ;
        mov       ebx, DWORD PTR [32+esp]                       ;
                                ; LOE eax edx ecx ebx esi
.B3.229:                        ; Preds .B3.225 .B3.228
        movd      xmm0, DWORD PTR [44+esp]                      ;367.103
                                ; LOE eax edx ecx ebx esi xmm0
.B3.230:                        ; Preds .B3.230 .B3.229
        paddd     xmm0, XMMWORD PTR [esi+ecx*4]                 ;367.103
        add       ecx, 4                                        ;367.103
        cmp       ecx, eax                                      ;367.103
        jb        .B3.230       ; Prob 82%                      ;367.103
                                ; LOE eax edx ecx ebx esi xmm0
.B3.231:                        ; Preds .B3.230
        movdqa    xmm1, xmm0                                    ;367.103
        psrldq    xmm1, 8                                       ;367.103
        paddd     xmm0, xmm1                                    ;367.103
        movdqa    xmm2, xmm0                                    ;367.103
        psrldq    xmm2, 4                                       ;367.103
        paddd     xmm0, xmm2                                    ;367.103
        movd      esi, xmm0                                     ;367.103
                                ; LOE eax edx ebx esi
.B3.232:                        ; Preds .B3.231 .B3.317
        cmp       eax, ebx                                      ;367.103
        jae       .B3.236       ; Prob 10%                      ;367.103
                                ; LOE eax edx ebx esi
.B3.233:                        ; Preds .B3.232
        mov       ecx, DWORD PTR [84+esp]                       ;
        neg       ecx                                           ;
        add       ecx, 4                                        ;
        imul      ecx, edx                                      ;
        add       ecx, DWORD PTR [36+esp]                       ;
                                ; LOE eax ecx ebx esi
.B3.234:                        ; Preds .B3.234 .B3.233
        add       esi, DWORD PTR [ecx+eax*4]                    ;367.103
        inc       eax                                           ;367.103
        cmp       eax, ebx                                      ;367.103
        jb        .B3.234       ; Prob 82%                      ;367.103
                                ; LOE eax ecx ebx esi
.B3.236:                        ; Preds .B3.234 .B3.328 .B3.232
        cvtsi2ss  xmm0, esi                                     ;367.103
        divss     xmm0, DWORD PTR [92+esp]                      ;367.120
        mulss     xmm0, DWORD PTR [_2il0floatpacket.7]          ;367.6
        lea       eax, DWORD PTR [280+esp]                      ;367.6
        movss     DWORD PTR [280+esp], xmm0                     ;367.6
        push      eax                                           ;367.6
        push      OFFSET FLAT: __STRLITPACK_515.0.3             ;367.6
        lea       edx, DWORD PTR [8+esp]                        ;367.6
        push      edx                                           ;367.6
        call      _for_write_seq_fmt_xmit                       ;367.6
                                ; LOE ebx
.B3.377:                        ; Preds .B3.236
        add       esp, 12                                       ;367.6
                                ; LOE ebx
.B3.237:                        ; Preds .B3.377
        test      ebx, ebx                                      ;368.85
        jle       .B3.325       ; Prob 0%                       ;368.85
                                ; LOE ebx
.B3.238:                        ; Preds .B3.237
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_READVEH+40] ;368.85
        cmp       ebx, 4                                        ;368.85
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_READVEH] ;368.85
        mov       DWORD PTR [36+esp], eax                       ;368.85
        jl        .B3.318       ; Prob 10%                      ;368.85
                                ; LOE eax edx ebx al ah
.B3.239:                        ; Preds .B3.238
        mov       ecx, eax                                      ;368.85
        mov       esi, DWORD PTR [84+esp]                       ;368.85
        imul      esi, ecx                                      ;368.85
        lea       eax, DWORD PTR [ecx+ecx*4]                    ;368.85
        mov       ecx, edx                                      ;368.85
        sub       ecx, esi                                      ;368.85
        add       ecx, eax                                      ;368.85
        mov       DWORD PTR [40+esp], eax                       ;368.85
        and       ecx, 15                                       ;368.85
        je        .B3.242       ; Prob 50%                      ;368.85
                                ; LOE edx ecx ebx esi
.B3.240:                        ; Preds .B3.239
        test      cl, 3                                         ;368.85
        jne       .B3.318       ; Prob 10%                      ;368.85
                                ; LOE edx ecx ebx esi
.B3.241:                        ; Preds .B3.240
        neg       ecx                                           ;368.85
        add       ecx, 16                                       ;368.85
        shr       ecx, 2                                        ;368.85
                                ; LOE edx ecx ebx esi
.B3.242:                        ; Preds .B3.241 .B3.239
        lea       eax, DWORD PTR [4+ecx]                        ;368.85
        cmp       ebx, eax                                      ;368.85
        jl        .B3.318       ; Prob 10%                      ;368.85
                                ; LOE edx ecx ebx esi
.B3.243:                        ; Preds .B3.242
        mov       eax, ebx                                      ;368.85
        xor       edi, edi                                      ;
        sub       eax, ecx                                      ;368.85
        sub       esi, DWORD PTR [40+esp]                       ;
        and       eax, 3                                        ;368.85
        neg       eax                                           ;368.85
        neg       esi                                           ;
        add       eax, ebx                                      ;368.85
        add       esi, edx                                      ;
        mov       DWORD PTR [44+esp], edi                       ;
        test      ecx, ecx                                      ;368.85
        jbe       .B3.247       ; Prob 10%                      ;368.85
                                ; LOE eax edx ecx ebx esi
.B3.244:                        ; Preds .B3.243
        xor       edi, edi                                      ;
        mov       DWORD PTR [32+esp], ebx                       ;
        mov       ebx, edi                                      ;
        mov       edi, DWORD PTR [44+esp]                       ;
                                ; LOE eax edx ecx ebx esi edi
.B3.245:                        ; Preds .B3.245 .B3.244
        add       edi, DWORD PTR [esi+ebx*4]                    ;368.85
        inc       ebx                                           ;368.85
        cmp       ebx, ecx                                      ;368.85
        jb        .B3.245       ; Prob 82%                      ;368.85
                                ; LOE eax edx ecx ebx esi edi
.B3.246:                        ; Preds .B3.245
        mov       DWORD PTR [44+esp], edi                       ;
        mov       ebx, DWORD PTR [32+esp]                       ;
                                ; LOE eax edx ecx ebx esi
.B3.247:                        ; Preds .B3.243 .B3.246
        movd      xmm0, DWORD PTR [44+esp]                      ;368.85
                                ; LOE eax edx ecx ebx esi xmm0
.B3.248:                        ; Preds .B3.248 .B3.247
        paddd     xmm0, XMMWORD PTR [esi+ecx*4]                 ;368.85
        add       ecx, 4                                        ;368.85
        cmp       ecx, eax                                      ;368.85
        jb        .B3.248       ; Prob 82%                      ;368.85
                                ; LOE eax edx ecx ebx esi xmm0
.B3.249:                        ; Preds .B3.248
        movdqa    xmm1, xmm0                                    ;368.85
        psrldq    xmm1, 8                                       ;368.85
        paddd     xmm0, xmm1                                    ;368.85
        movdqa    xmm2, xmm0                                    ;368.85
        psrldq    xmm2, 4                                       ;368.85
        paddd     xmm0, xmm2                                    ;368.85
        movd      esi, xmm0                                     ;368.85
                                ; LOE eax edx ebx esi
.B3.250:                        ; Preds .B3.249 .B3.318
        cmp       eax, ebx                                      ;368.85
        jae       .B3.254       ; Prob 10%                      ;368.85
                                ; LOE eax edx ebx esi
.B3.251:                        ; Preds .B3.250
        mov       ecx, DWORD PTR [84+esp]                       ;
        neg       ecx                                           ;
        add       ecx, 5                                        ;
        imul      ecx, DWORD PTR [36+esp]                       ;
        add       ecx, edx                                      ;
                                ; LOE eax ecx ebx esi
.B3.252:                        ; Preds .B3.252 .B3.251
        add       esi, DWORD PTR [ecx+eax*4]                    ;368.85
        inc       eax                                           ;368.85
        cmp       eax, ebx                                      ;368.85
        jb        .B3.252       ; Prob 82%                      ;368.85
                                ; LOE eax ecx ebx esi
.B3.254:                        ; Preds .B3.252 .B3.325 .B3.250
        mov       DWORD PTR [esp], 0                            ;368.6
        lea       eax, DWORD PTR [288+esp]                      ;368.6
        mov       DWORD PTR [288+esp], esi                      ;368.6
        push      32                                            ;368.6
        push      OFFSET FLAT: PRINTHEADER2$format_pack.0.3+1104 ;368.6
        push      eax                                           ;368.6
        push      OFFSET FLAT: __STRLITPACK_516.0.3             ;368.6
        push      -2088435968                                   ;368.6
        push      666                                           ;368.6
        lea       edx, DWORD PTR [24+esp]                       ;368.6
        push      edx                                           ;368.6
        call      _for_write_seq_fmt                            ;368.6
                                ; LOE ebx
.B3.378:                        ; Preds .B3.254
        add       esp, 28                                       ;368.6
                                ; LOE ebx
.B3.255:                        ; Preds .B3.378
        test      ebx, ebx                                      ;368.103
        jle       .B3.322       ; Prob 0%                       ;368.103
                                ; LOE ebx
.B3.256:                        ; Preds .B3.255
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_READVEH+40] ;368.103
        cmp       ebx, 4                                        ;368.103
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_READVEH] ;368.103
        mov       DWORD PTR [36+esp], eax                       ;368.103
        jl        .B3.319       ; Prob 10%                      ;368.103
                                ; LOE eax edx ebx al ah
.B3.257:                        ; Preds .B3.256
        mov       ecx, eax                                      ;368.103
        mov       esi, DWORD PTR [84+esp]                       ;368.103
        imul      esi, ecx                                      ;368.103
        lea       eax, DWORD PTR [ecx+ecx*4]                    ;368.103
        mov       ecx, edx                                      ;368.103
        sub       ecx, esi                                      ;368.103
        add       ecx, eax                                      ;368.103
        mov       DWORD PTR [40+esp], eax                       ;368.103
        and       ecx, 15                                       ;368.103
        je        .B3.260       ; Prob 50%                      ;368.103
                                ; LOE edx ecx ebx esi
.B3.258:                        ; Preds .B3.257
        test      cl, 3                                         ;368.103
        jne       .B3.319       ; Prob 10%                      ;368.103
                                ; LOE edx ecx ebx esi
.B3.259:                        ; Preds .B3.258
        neg       ecx                                           ;368.103
        add       ecx, 16                                       ;368.103
        shr       ecx, 2                                        ;368.103
                                ; LOE edx ecx ebx esi
.B3.260:                        ; Preds .B3.259 .B3.257
        lea       eax, DWORD PTR [4+ecx]                        ;368.103
        cmp       ebx, eax                                      ;368.103
        jl        .B3.319       ; Prob 10%                      ;368.103
                                ; LOE edx ecx ebx esi
.B3.261:                        ; Preds .B3.260
        mov       eax, ebx                                      ;368.103
        xor       edi, edi                                      ;
        sub       eax, ecx                                      ;368.103
        sub       esi, DWORD PTR [40+esp]                       ;
        and       eax, 3                                        ;368.103
        neg       eax                                           ;368.103
        neg       esi                                           ;
        add       eax, ebx                                      ;368.103
        add       esi, edx                                      ;
        mov       DWORD PTR [44+esp], edi                       ;
        test      ecx, ecx                                      ;368.103
        jbe       .B3.265       ; Prob 10%                      ;368.103
                                ; LOE eax edx ecx ebx esi
.B3.262:                        ; Preds .B3.261
        xor       edi, edi                                      ;
        mov       DWORD PTR [32+esp], ebx                       ;
        mov       ebx, edi                                      ;
        mov       edi, DWORD PTR [44+esp]                       ;
                                ; LOE eax edx ecx ebx esi edi
.B3.263:                        ; Preds .B3.263 .B3.262
        add       edi, DWORD PTR [esi+ebx*4]                    ;368.103
        inc       ebx                                           ;368.103
        cmp       ebx, ecx                                      ;368.103
        jb        .B3.263       ; Prob 82%                      ;368.103
                                ; LOE eax edx ecx ebx esi edi
.B3.264:                        ; Preds .B3.263
        mov       DWORD PTR [44+esp], edi                       ;
        mov       ebx, DWORD PTR [32+esp]                       ;
                                ; LOE eax edx ecx ebx esi
.B3.265:                        ; Preds .B3.261 .B3.264
        movd      xmm0, DWORD PTR [44+esp]                      ;368.103
                                ; LOE eax edx ecx ebx esi xmm0
.B3.266:                        ; Preds .B3.266 .B3.265
        paddd     xmm0, XMMWORD PTR [esi+ecx*4]                 ;368.103
        add       ecx, 4                                        ;368.103
        cmp       ecx, eax                                      ;368.103
        jb        .B3.266       ; Prob 82%                      ;368.103
                                ; LOE eax edx ecx ebx esi xmm0
.B3.267:                        ; Preds .B3.266
        movdqa    xmm1, xmm0                                    ;368.103
        psrldq    xmm1, 8                                       ;368.103
        paddd     xmm0, xmm1                                    ;368.103
        movdqa    xmm2, xmm0                                    ;368.103
        psrldq    xmm2, 4                                       ;368.103
        paddd     xmm0, xmm2                                    ;368.103
        movd      ecx, xmm0                                     ;368.103
                                ; LOE eax edx ecx ebx
.B3.268:                        ; Preds .B3.267 .B3.319
        cmp       eax, ebx                                      ;368.103
        jae       .B3.272       ; Prob 10%                      ;368.103
                                ; LOE eax edx ecx ebx
.B3.269:                        ; Preds .B3.268
        mov       esi, DWORD PTR [84+esp]                       ;
        neg       esi                                           ;
        add       esi, 5                                        ;
        imul      esi, DWORD PTR [36+esp]                       ;
        add       esi, edx                                      ;
        mov       DWORD PTR [84+esp], esi                       ;
        mov       edx, esi                                      ;
                                ; LOE eax edx ecx ebx
.B3.270:                        ; Preds .B3.270 .B3.269
        add       ecx, DWORD PTR [edx+eax*4]                    ;368.103
        inc       eax                                           ;368.103
        cmp       eax, ebx                                      ;368.103
        jb        .B3.270       ; Prob 82%                      ;368.103
                                ; LOE eax edx ecx ebx
.B3.272:                        ; Preds .B3.270 .B3.322 .B3.268
        cvtsi2ss  xmm1, ecx                                     ;368.103
        divss     xmm1, DWORD PTR [92+esp]                      ;368.120
        movss     xmm0, DWORD PTR [_2il0floatpacket.7]          ;368.6
        lea       eax, DWORD PTR [32+esp]                       ;368.6
        mulss     xmm0, xmm1                                    ;368.6
        movss     DWORD PTR [32+esp], xmm0                      ;368.6
        push      eax                                           ;368.6
        push      OFFSET FLAT: __STRLITPACK_517.0.3             ;368.6
        lea       edx, DWORD PTR [8+esp]                        ;368.6
        push      edx                                           ;368.6
        call      _for_write_seq_fmt_xmit                       ;368.6
                                ; LOE
.B3.379:                        ; Preds .B3.272
        add       esp, 304                                      ;368.6
        pop       ebx                                           ;368.6
        pop       edi                                           ;368.6
        pop       esi                                           ;368.6
        mov       esp, ebp                                      ;368.6
        pop       ebp                                           ;368.6
        ret                                                     ;368.6
                                ; LOE
.B3.273:                        ; Preds .B3.92
        xor       eax, eax                                      ;370.9
        lea       edx, DWORD PTR [32+esp]                       ;370.9
        mov       DWORD PTR [esp], eax                          ;370.9
        mov       DWORD PTR [32+esp], eax                       ;370.9
        push      32                                            ;370.9
        push      OFFSET FLAT: PRINTHEADER2$format_pack.0.3+1196 ;370.9
        push      edx                                           ;370.9
        push      OFFSET FLAT: __STRLITPACK_518.0.3             ;370.9
        push      -2088435968                                   ;370.9
        push      666                                           ;370.9
        lea       ecx, DWORD PTR [24+esp]                       ;370.9
        push      ecx                                           ;370.9
        call      _for_write_seq_fmt                            ;370.9
                                ; LOE
.B3.274:                        ; Preds .B3.273
        mov       DWORD PTR [68+esp], 0                         ;370.9
        lea       eax, DWORD PTR [68+esp]                       ;370.9
        push      eax                                           ;370.9
        push      OFFSET FLAT: __STRLITPACK_519.0.3             ;370.9
        lea       edx, DWORD PTR [36+esp]                       ;370.9
        push      edx                                           ;370.9
        call      _for_write_seq_fmt_xmit                       ;370.9
                                ; LOE
.B3.275:                        ; Preds .B3.274
        xor       eax, eax                                      ;371.6
        lea       edx, DWORD PTR [264+esp]                      ;371.6
        mov       DWORD PTR [40+esp], eax                       ;371.6
        mov       DWORD PTR [264+esp], eax                      ;371.6
        push      32                                            ;371.6
        push      OFFSET FLAT: PRINTHEADER2$format_pack.0.3+1288 ;371.6
        push      edx                                           ;371.6
        push      OFFSET FLAT: __STRLITPACK_520.0.3             ;371.6
        push      -2088435968                                   ;371.6
        push      666                                           ;371.6
        lea       ecx, DWORD PTR [64+esp]                       ;371.6
        push      ecx                                           ;371.6
        call      _for_write_seq_fmt                            ;371.6
                                ; LOE
.B3.276:                        ; Preds .B3.275
        mov       DWORD PTR [300+esp], 0                        ;371.6
        lea       eax, DWORD PTR [300+esp]                      ;371.6
        push      eax                                           ;371.6
        push      OFFSET FLAT: __STRLITPACK_521.0.3             ;371.6
        lea       edx, DWORD PTR [76+esp]                       ;371.6
        push      edx                                           ;371.6
        call      _for_write_seq_fmt_xmit                       ;371.6
                                ; LOE
.B3.277:                        ; Preds .B3.276
        xor       eax, eax                                      ;372.6
        lea       edx, DWORD PTR [320+esp]                      ;372.6
        mov       DWORD PTR [80+esp], eax                       ;372.6
        mov       DWORD PTR [320+esp], eax                      ;372.6
        push      32                                            ;372.6
        push      OFFSET FLAT: PRINTHEADER2$format_pack.0.3+1380 ;372.6
        push      edx                                           ;372.6
        push      OFFSET FLAT: __STRLITPACK_522.0.3             ;372.6
        push      -2088435968                                   ;372.6
        push      666                                           ;372.6
        lea       ecx, DWORD PTR [104+esp]                      ;372.6
        push      ecx                                           ;372.6
        call      _for_write_seq_fmt                            ;372.6
                                ; LOE
.B3.278:                        ; Preds .B3.277
        mov       DWORD PTR [356+esp], 0                        ;372.6
        lea       eax, DWORD PTR [356+esp]                      ;372.6
        push      eax                                           ;372.6
        push      OFFSET FLAT: __STRLITPACK_523.0.3             ;372.6
        lea       edx, DWORD PTR [116+esp]                      ;372.6
        push      edx                                           ;372.6
        call      _for_write_seq_fmt_xmit                       ;372.6
                                ; LOE
.B3.380:                        ; Preds .B3.278
        add       esp, 120                                      ;372.6
                                ; LOE
.B3.279:                        ; Preds .B3.380
        xor       eax, eax                                      ;373.6
        lea       edx, DWORD PTR [256+esp]                      ;373.6
        mov       DWORD PTR [esp], eax                          ;373.6
        mov       DWORD PTR [256+esp], eax                      ;373.6
        push      32                                            ;373.6
        push      OFFSET FLAT: PRINTHEADER2$format_pack.0.3+1472 ;373.6
        push      edx                                           ;373.6
        push      OFFSET FLAT: __STRLITPACK_524.0.3             ;373.6
        push      -2088435968                                   ;373.6
        push      666                                           ;373.6
        lea       ecx, DWORD PTR [24+esp]                       ;373.6
        push      ecx                                           ;373.6
        call      _for_write_seq_fmt                            ;373.6
                                ; LOE
.B3.280:                        ; Preds .B3.279
        mov       DWORD PTR [292+esp], 0                        ;373.6
        lea       eax, DWORD PTR [292+esp]                      ;373.6
        push      eax                                           ;373.6
        push      OFFSET FLAT: __STRLITPACK_525.0.3             ;373.6
        lea       edx, DWORD PTR [36+esp]                       ;373.6
        push      edx                                           ;373.6
        call      _for_write_seq_fmt_xmit                       ;373.6
                                ; LOE
.B3.281:                        ; Preds .B3.280
        xor       eax, eax                                      ;374.6
        lea       edx, DWORD PTR [312+esp]                      ;374.6
        mov       DWORD PTR [40+esp], eax                       ;374.6
        mov       DWORD PTR [312+esp], eax                      ;374.6
        push      32                                            ;374.6
        push      OFFSET FLAT: PRINTHEADER2$format_pack.0.3+1564 ;374.6
        push      edx                                           ;374.6
        push      OFFSET FLAT: __STRLITPACK_526.0.3             ;374.6
        push      -2088435968                                   ;374.6
        push      666                                           ;374.6
        lea       ecx, DWORD PTR [64+esp]                       ;374.6
        push      ecx                                           ;374.6
        call      _for_write_seq_fmt                            ;374.6
                                ; LOE
.B3.282:                        ; Preds .B3.281
        mov       DWORD PTR [348+esp], 0                        ;374.6
        lea       eax, DWORD PTR [348+esp]                      ;374.6
        push      eax                                           ;374.6
        push      OFFSET FLAT: __STRLITPACK_527.0.3             ;374.6
        lea       edx, DWORD PTR [76+esp]                       ;374.6
        push      edx                                           ;374.6
        call      _for_write_seq_fmt_xmit                       ;374.6
                                ; LOE
.B3.283:                        ; Preds .B3.282
        add       esp, 372                                      ;376.1
        pop       ebx                                           ;376.1
        pop       edi                                           ;376.1
        pop       esi                                           ;376.1
        mov       esp, ebp                                      ;376.1
        pop       ebp                                           ;376.1
        ret                                                     ;376.1
                                ; LOE
.B3.284:                        ; Preds .B3.2
        mov       DWORD PTR [esp], ebx                          ;
        xor       edi, edi                                      ;
        mov       DWORD PTR [12+esp], ecx                       ;
        xor       eax, eax                                      ;
        mov       DWORD PTR [8+esp], edi                        ;
        mov       DWORD PTR [16+esp], esi                       ;
        mov       DWORD PTR [20+esp], ebx                       ;
        mov       ecx, DWORD PTR [esp]                          ;
                                ; LOE eax edx ecx xmm0
.B3.285:                        ; Preds .B3.300 .B3.351 .B3.284
        cmp       edx, 4                                        ;341.16
        jl        .B3.350       ; Prob 10%                      ;341.16
                                ; LOE eax edx ecx xmm0
.B3.286:                        ; Preds .B3.285
        mov       ebx, DWORD PTR [20+esp]                       ;341.16
        add       ebx, eax                                      ;341.16
        and       ebx, 15                                       ;341.16
        je        .B3.289       ; Prob 50%                      ;341.16
                                ; LOE eax edx ecx ebx xmm0
.B3.287:                        ; Preds .B3.286
        test      bl, 3                                         ;341.16
        jne       .B3.350       ; Prob 10%                      ;341.16
                                ; LOE eax edx ecx ebx xmm0
.B3.288:                        ; Preds .B3.287
        neg       ebx                                           ;341.16
        add       ebx, 16                                       ;341.16
        shr       ebx, 2                                        ;341.16
                                ; LOE eax edx ecx ebx xmm0
.B3.289:                        ; Preds .B3.288 .B3.286
        lea       esi, DWORD PTR [4+ebx]                        ;341.16
        cmp       edx, esi                                      ;341.16
        jl        .B3.350       ; Prob 10%                      ;341.16
                                ; LOE eax edx ecx ebx xmm0
.B3.290:                        ; Preds .B3.289
        mov       esi, edx                                      ;341.16
        sub       esi, ebx                                      ;341.16
        mov       edi, DWORD PTR [20+esp]                       ;
        and       esi, 3                                        ;341.16
        neg       esi                                           ;341.16
        add       esi, edx                                      ;341.16
        test      ebx, ebx                                      ;341.16
        lea       edi, DWORD PTR [edi+eax]                      ;
        mov       DWORD PTR [24+esp], edi                       ;
        jbe       .B3.294       ; Prob 10%                      ;341.16
                                ; LOE eax edx ecx ebx esi xmm0
.B3.291:                        ; Preds .B3.290
        xor       edi, edi                                      ;
        mov       DWORD PTR [esp], ecx                          ;
        mov       DWORD PTR [4+esp], edx                        ;
        mov       edx, edi                                      ;
        mov       ecx, DWORD PTR [24+esp]                       ;
        mov       edi, DWORD PTR [16+esp]                       ;
                                ; LOE eax edx ecx ebx esi edi xmm0
.B3.292:                        ; Preds .B3.292 .B3.291
        add       edi, DWORD PTR [ecx+edx*4]                    ;341.16
        inc       edx                                           ;341.16
        cmp       edx, ebx                                      ;341.16
        jb        .B3.292       ; Prob 82%                      ;341.16
                                ; LOE eax edx ecx ebx esi edi xmm0
.B3.293:                        ; Preds .B3.292
        mov       DWORD PTR [16+esp], edi                       ;
        mov       ecx, DWORD PTR [esp]                          ;
        mov       edx, DWORD PTR [4+esp]                        ;
                                ; LOE eax edx ecx ebx esi xmm0
.B3.294:                        ; Preds .B3.290 .B3.293
        mov       edi, DWORD PTR [24+esp]                       ;
                                ; LOE eax edx ecx ebx esi edi xmm0
.B3.295:                        ; Preds .B3.295 .B3.294
        paddd     xmm0, XMMWORD PTR [edi+ebx*4]                 ;341.16
        add       ebx, 4                                        ;341.16
        cmp       ebx, esi                                      ;341.16
        jb        .B3.295       ; Prob 82%                      ;341.16
                                ; LOE eax edx ecx ebx esi edi xmm0
.B3.297:                        ; Preds .B3.295 .B3.350
        cmp       esi, edx                                      ;341.16
        jae       .B3.351       ; Prob 10%                      ;341.16
                                ; LOE eax edx ecx esi xmm0
.B3.298:                        ; Preds .B3.297
        mov       ebx, DWORD PTR [16+esp]                       ;
                                ; LOE eax edx ecx ebx esi xmm0
.B3.299:                        ; Preds .B3.299 .B3.298
        add       ebx, DWORD PTR [ecx+esi*4]                    ;341.16
        inc       esi                                           ;341.16
        cmp       esi, edx                                      ;341.16
        jb        .B3.299       ; Prob 82%                      ;341.16
                                ; LOE eax edx ecx ebx esi xmm0
.B3.300:                        ; Preds .B3.299
        mov       esi, DWORD PTR [8+esp]                        ;341.16
        inc       esi                                           ;341.16
        mov       DWORD PTR [16+esp], ebx                       ;
        mov       ebx, DWORD PTR [12+esp]                       ;341.16
        add       ecx, ebx                                      ;341.16
        add       eax, ebx                                      ;341.16
        mov       DWORD PTR [8+esp], esi                        ;341.16
        cmp       esi, DWORD PTR [116+esp]                      ;341.16
        jb        .B3.285       ; Prob 82%                      ;341.16
                                ; LOE eax edx ecx xmm0
.B3.301:                        ; Preds .B3.300                 ; Infreq
        mov       esi, DWORD PTR [16+esp]                       ;
        jmp       .B3.3         ; Prob 100%                     ;
                                ; LOE esi xmm0
.B3.302:                        ; Preds .B3.9                   ; Infreq
        mov       esi, 1                                        ;
        jmp       .B3.13        ; Prob 100%                     ;
                                ; LOE eax ecx ebx esi
.B3.303:                        ; Preds .B3.18                  ; Infreq
        mov       esi, 1                                        ;
        jmp       .B3.22        ; Prob 100%                     ;
                                ; LOE eax edx ecx esi
.B3.304:                        ; Preds .B3.27                  ; Infreq
        mov       esi, 1                                        ;
        jmp       .B3.31        ; Prob 100%                     ;
                                ; LOE eax edx ecx esi
.B3.305:                        ; Preds .B3.36                  ; Infreq
        mov       esi, 1                                        ;
        jmp       .B3.40        ; Prob 100%                     ;
                                ; LOE eax edx ecx esi
.B3.306:                        ; Preds .B3.45                  ; Infreq
        mov       esi, 1                                        ;
        jmp       .B3.49        ; Prob 100%                     ;
                                ; LOE eax edx ecx esi
.B3.307:                        ; Preds .B3.54                  ; Infreq
        mov       esi, 1                                        ;
        jmp       .B3.58        ; Prob 100%                     ;
                                ; LOE eax edx ecx esi
.B3.308:                        ; Preds .B3.63                  ; Infreq
        mov       esi, 1                                        ;
        jmp       .B3.67        ; Prob 100%                     ;
                                ; LOE eax ecx ebx esi
.B3.309:                        ; Preds .B3.72                  ; Infreq
        mov       esi, 1                                        ;
        jmp       .B3.76        ; Prob 100%                     ;
                                ; LOE eax edx ecx esi
.B3.310:                        ; Preds .B3.94 .B3.98 .B3.96    ; Infreq
        xor       ecx, ecx                                      ;
        xor       edx, edx                                      ;
        jmp       .B3.106       ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx
.B3.311:                        ; Preds .B3.112 .B3.116 .B3.114 ; Infreq
        xor       ecx, ecx                                      ;
        xor       edx, edx                                      ;
        jmp       .B3.124       ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx
.B3.312:                        ; Preds .B3.130 .B3.134 .B3.132 ; Infreq
        xor       esi, esi                                      ;
        xor       eax, eax                                      ;
        jmp       .B3.142       ; Prob 100%                     ;
                                ; LOE eax edx ebx esi
.B3.313:                        ; Preds .B3.148 .B3.152 .B3.150 ; Infreq
        xor       esi, esi                                      ;
        xor       eax, eax                                      ;
        jmp       .B3.160       ; Prob 100%                     ;
                                ; LOE eax edx ebx esi
.B3.314:                        ; Preds .B3.166 .B3.170 .B3.168 ; Infreq
        xor       esi, esi                                      ;
        xor       eax, eax                                      ;
        jmp       .B3.178       ; Prob 100%                     ;
                                ; LOE eax edx ebx esi
.B3.315:                        ; Preds .B3.184 .B3.188 .B3.186 ; Infreq
        xor       esi, esi                                      ;
        xor       eax, eax                                      ;
        jmp       .B3.196       ; Prob 100%                     ;
                                ; LOE eax edx ebx esi
.B3.316:                        ; Preds .B3.202 .B3.206 .B3.204 ; Infreq
        xor       esi, esi                                      ;
        xor       eax, eax                                      ;
        jmp       .B3.214       ; Prob 100%                     ;
                                ; LOE eax edx ebx esi
.B3.317:                        ; Preds .B3.220 .B3.224 .B3.222 ; Infreq
        xor       esi, esi                                      ;
        xor       eax, eax                                      ;
        jmp       .B3.232       ; Prob 100%                     ;
                                ; LOE eax edx ebx esi
.B3.318:                        ; Preds .B3.238 .B3.242 .B3.240 ; Infreq
        xor       esi, esi                                      ;
        xor       eax, eax                                      ;
        jmp       .B3.250       ; Prob 100%                     ;
                                ; LOE eax edx ebx esi
.B3.319:                        ; Preds .B3.256 .B3.260 .B3.258 ; Infreq
        xor       ecx, ecx                                      ;
        xor       eax, eax                                      ;
        jmp       .B3.268       ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx
.B3.322:                        ; Preds .B3.255                 ; Infreq
        xor       ecx, ecx                                      ;
        jmp       .B3.272       ; Prob 100%                     ;
                                ; LOE ecx
.B3.325:                        ; Preds .B3.237                 ; Infreq
        xor       esi, esi                                      ;
        jmp       .B3.254       ; Prob 100%                     ;
                                ; LOE ebx esi
.B3.328:                        ; Preds .B3.219                 ; Infreq
        xor       esi, esi                                      ;
        jmp       .B3.236       ; Prob 100%                     ;
                                ; LOE ebx esi
.B3.331:                        ; Preds .B3.201                 ; Infreq
        xor       esi, esi                                      ;
        jmp       .B3.218       ; Prob 100%                     ;
                                ; LOE ebx esi
.B3.334:                        ; Preds .B3.183                 ; Infreq
        xor       esi, esi                                      ;
        jmp       .B3.200       ; Prob 100%                     ;
                                ; LOE ebx esi
.B3.337:                        ; Preds .B3.165                 ; Infreq
        xor       esi, esi                                      ;
        jmp       .B3.182       ; Prob 100%                     ;
                                ; LOE ebx esi
.B3.340:                        ; Preds .B3.147                 ; Infreq
        xor       esi, esi                                      ;
        jmp       .B3.164       ; Prob 100%                     ;
                                ; LOE ebx esi
.B3.343:                        ; Preds .B3.129                 ; Infreq
        xor       esi, esi                                      ;
        jmp       .B3.146       ; Prob 100%                     ;
                                ; LOE ebx esi
.B3.346:                        ; Preds .B3.111                 ; Infreq
        xor       ecx, ecx                                      ;
        jmp       .B3.128       ; Prob 100%                     ;
                                ; LOE ecx ebx
.B3.349:                        ; Preds .B3.93                  ; Infreq
        xor       ecx, ecx                                      ;
        jmp       .B3.110       ; Prob 100%                     ;
                                ; LOE ecx ebx
.B3.350:                        ; Preds .B3.285 .B3.289 .B3.287 ; Infreq
        xor       esi, esi                                      ;341.16
        jmp       .B3.297       ; Prob 100%                     ;341.16
                                ; LOE eax edx ecx esi xmm0
.B3.351:                        ; Preds .B3.297                 ; Infreq
        mov       esi, DWORD PTR [8+esp]                        ;341.16
        inc       esi                                           ;341.16
        mov       ebx, DWORD PTR [12+esp]                       ;341.16
        add       ecx, ebx                                      ;341.16
        add       eax, ebx                                      ;341.16
        mov       DWORD PTR [8+esp], esi                        ;341.16
        cmp       esi, DWORD PTR [116+esp]                      ;341.16
        jb        .B3.285       ; Prob 82%                      ;341.16
                                ; LOE eax edx ecx xmm0
.B3.352:                        ; Preds .B3.351                 ; Infreq
        mov       esi, DWORD PTR [16+esp]                       ;
        jmp       .B3.3         ; Prob 100%                     ;
                                ; LOE esi xmm0
.B3.355:                        ; Preds .B3.1                   ; Infreq
        xor       esi, esi                                      ;
        pxor      xmm0, xmm0                                    ;
        jmp       .B3.3         ; Prob 100%                     ;
        ALIGN     16
                                ; LOE esi xmm0
; mark_end;
_PRINTHEADER2 ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
	DD 2 DUP (0H)	; pad
PRINTHEADER2$format_pack.0.3	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	46
	DB	0
	DB	32
	DB	32
	DB	32
	DB	32
	DB	80
	DB	67
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	58
	DB	32
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	10
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	2
	DB	1
	DB	0
	DB	0
	DB	0
	DB	9
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	2
	DB	0
	DB	32
	DB	37
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	46
	DB	0
	DB	32
	DB	32
	DB	32
	DB	32
	DB	84
	DB	82
	DB	85
	DB	67
	DB	75
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	58
	DB	32
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	10
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	2
	DB	1
	DB	0
	DB	0
	DB	0
	DB	9
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	2
	DB	0
	DB	32
	DB	37
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	46
	DB	0
	DB	32
	DB	32
	DB	32
	DB	32
	DB	72
	DB	79
	DB	86
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	58
	DB	32
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	10
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	2
	DB	1
	DB	0
	DB	0
	DB	0
	DB	9
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	2
	DB	0
	DB	32
	DB	37
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	46
	DB	0
	DB	32
	DB	32
	DB	32
	DB	32
	DB	66
	DB	85
	DB	83
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	58
	DB	32
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	10
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	2
	DB	1
	DB	0
	DB	0
	DB	0
	DB	9
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	2
	DB	0
	DB	32
	DB	37
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	46
	DB	0
	DB	32
	DB	32
	DB	32
	DB	32
	DB	80
	DB	67
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	58
	DB	32
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	10
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	2
	DB	1
	DB	0
	DB	0
	DB	0
	DB	9
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	2
	DB	0
	DB	32
	DB	37
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	46
	DB	0
	DB	32
	DB	32
	DB	32
	DB	32
	DB	84
	DB	82
	DB	85
	DB	67
	DB	75
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	58
	DB	32
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	10
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	2
	DB	1
	DB	0
	DB	0
	DB	0
	DB	9
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	2
	DB	0
	DB	32
	DB	37
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	46
	DB	0
	DB	32
	DB	32
	DB	32
	DB	32
	DB	72
	DB	79
	DB	86
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	58
	DB	32
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	10
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	2
	DB	1
	DB	0
	DB	0
	DB	0
	DB	9
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	2
	DB	0
	DB	32
	DB	37
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	46
	DB	0
	DB	32
	DB	32
	DB	32
	DB	32
	DB	66
	DB	85
	DB	83
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	58
	DB	32
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	10
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	2
	DB	1
	DB	0
	DB	0
	DB	0
	DB	9
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	2
	DB	0
	DB	32
	DB	37
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	46
	DB	0
	DB	32
	DB	32
	DB	32
	DB	32
	DB	72
	DB	105
	DB	115
	DB	116
	DB	111
	DB	105
	DB	99
	DB	97
	DB	108
	DB	47
	DB	72
	DB	97
	DB	98
	DB	105
	DB	116
	DB	117
	DB	97
	DB	108
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	58
	DB	32
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	10
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	2
	DB	1
	DB	0
	DB	0
	DB	0
	DB	9
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	2
	DB	0
	DB	32
	DB	37
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	46
	DB	0
	DB	32
	DB	32
	DB	32
	DB	32
	DB	83
	DB	121
	DB	115
	DB	116
	DB	101
	DB	109
	DB	32
	DB	79
	DB	112
	DB	116
	DB	105
	DB	109
	DB	97
	DB	108
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	58
	DB	32
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	10
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	2
	DB	1
	DB	0
	DB	0
	DB	0
	DB	9
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	2
	DB	0
	DB	32
	DB	37
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	46
	DB	0
	DB	32
	DB	32
	DB	32
	DB	32
	DB	85
	DB	115
	DB	101
	DB	114
	DB	32
	DB	79
	DB	112
	DB	116
	DB	105
	DB	109
	DB	97
	DB	108
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	58
	DB	32
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	10
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	2
	DB	1
	DB	0
	DB	0
	DB	0
	DB	9
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	2
	DB	0
	DB	32
	DB	37
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	46
	DB	0
	DB	32
	DB	32
	DB	32
	DB	32
	DB	69
	DB	110
	DB	114
	DB	111
	DB	117
	DB	116
	DB	101
	DB	32
	DB	73
	DB	110
	DB	102
	DB	111
	DB	114
	DB	109
	DB	97
	DB	116
	DB	105
	DB	111
	DB	110
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	58
	DB	32
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	10
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	2
	DB	1
	DB	0
	DB	0
	DB	0
	DB	9
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	2
	DB	0
	DB	32
	DB	37
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	46
	DB	0
	DB	32
	DB	32
	DB	32
	DB	32
	DB	80
	DB	114
	DB	101
	DB	45
	DB	84
	DB	114
	DB	105
	DB	112
	DB	32
	DB	73
	DB	110
	DB	102
	DB	111
	DB	114
	DB	109
	DB	97
	DB	116
	DB	105
	DB	111
	DB	110
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	58
	DB	32
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	10
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	2
	DB	1
	DB	0
	DB	0
	DB	0
	DB	9
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	2
	DB	0
	DB	32
	DB	37
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	46
	DB	0
	DB	32
	DB	32
	DB	32
	DB	32
	DB	72
	DB	105
	DB	115
	DB	116
	DB	111
	DB	105
	DB	99
	DB	97
	DB	108
	DB	47
	DB	72
	DB	97
	DB	98
	DB	105
	DB	116
	DB	117
	DB	97
	DB	108
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	58
	DB	32
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	10
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	2
	DB	1
	DB	0
	DB	0
	DB	0
	DB	9
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	2
	DB	0
	DB	32
	DB	37
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	46
	DB	0
	DB	32
	DB	32
	DB	32
	DB	32
	DB	83
	DB	121
	DB	115
	DB	116
	DB	101
	DB	109
	DB	32
	DB	79
	DB	112
	DB	116
	DB	105
	DB	109
	DB	97
	DB	108
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	58
	DB	32
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	10
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	2
	DB	1
	DB	0
	DB	0
	DB	0
	DB	9
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	2
	DB	0
	DB	32
	DB	37
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	46
	DB	0
	DB	32
	DB	32
	DB	32
	DB	32
	DB	85
	DB	115
	DB	101
	DB	114
	DB	32
	DB	79
	DB	112
	DB	116
	DB	105
	DB	109
	DB	97
	DB	108
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	58
	DB	32
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	10
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	2
	DB	1
	DB	0
	DB	0
	DB	0
	DB	9
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	2
	DB	0
	DB	32
	DB	37
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	46
	DB	0
	DB	32
	DB	32
	DB	32
	DB	32
	DB	69
	DB	110
	DB	114
	DB	111
	DB	117
	DB	116
	DB	101
	DB	32
	DB	73
	DB	110
	DB	102
	DB	111
	DB	114
	DB	109
	DB	97
	DB	116
	DB	105
	DB	111
	DB	110
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	58
	DB	32
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	10
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	2
	DB	1
	DB	0
	DB	0
	DB	0
	DB	9
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	2
	DB	0
	DB	32
	DB	37
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	46
	DB	0
	DB	32
	DB	32
	DB	32
	DB	32
	DB	80
	DB	114
	DB	101
	DB	45
	DB	84
	DB	114
	DB	105
	DB	112
	DB	32
	DB	73
	DB	110
	DB	102
	DB	111
	DB	114
	DB	109
	DB	97
	DB	116
	DB	105
	DB	111
	DB	110
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	58
	DB	32
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	10
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	2
	DB	1
	DB	0
	DB	0
	DB	0
	DB	9
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	2
	DB	0
	DB	32
	DB	37
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
_DATA	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_484.0.3	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_485.0.3	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_486.0.3	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_487.0.3	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_496.0.3	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_497.0.3	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_498.0.3	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_499.0.3	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_500.0.3	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_501.0.3	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_502.0.3	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_503.0.3	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_488.0.3	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_489.0.3	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_490.0.3	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_491.0.3	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_492.0.3	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_493.0.3	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_494.0.3	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_495.0.3	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_504.0.3	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_505.0.3	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_506.0.3	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_507.0.3	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_518.0.3	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_519.0.3	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_520.0.3	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_521.0.3	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_522.0.3	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_523.0.3	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_524.0.3	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_525.0.3	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_526.0.3	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_527.0.3	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_508.0.3	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_509.0.3	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_510.0.3	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_511.0.3	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_512.0.3	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_513.0.3	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_514.0.3	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_515.0.3	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_516.0.3	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_517.0.3	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _PRINTHEADER2
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DD 5 DUP (0H)	; pad
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_24	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	115
	DB	101
	DB	116
	DB	116
	DB	105
	DB	110
	DB	103
	DB	32
	DB	117
	DB	112
	DB	32
	DB	116
	DB	104
	DB	101
	DB	32
	DB	69
	DB	112
	DB	111
	DB	99
	DB	104
	DB	46
	DB	100
	DB	97
	DB	116
	DB	32
	DB	97
	DB	110
	DB	100
	DB	32
	DB	97
	DB	103
	DB	103
	DB	114
	DB	101
	DB	103
	DB	97
	DB	116
	DB	105
	DB	111
	DB	110
	DB	32
	DB	105
	DB	110
	DB	116
	DB	101
	DB	114
	DB	118
	DB	97
	DB	108
	DB	32
	DB	105
	DB	110
	DB	32
	DB	115
	DB	121
	DB	115
	DB	116
	DB	101
	DB	109
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DD 5 DUP (0H)	; pad
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_22	DB	69
	DB	112
	DB	111
	DB	99
	DB	104
	DB	32
	DB	112
	DB	101
	DB	114
	DB	105
	DB	111
	DB	100
	DB	32
	DB	114
	DB	101
	DB	115
	DB	117
	DB	108
	DB	116
	DB	101
	DB	100
	DB	32
	DB	102
	DB	114
	DB	111
	DB	109
	DB	32
	DB	116
	DB	104
	DB	101
	DB	32
	DB	100
	DB	105
	DB	118
	DB	105
	DB	115
	DB	105
	DB	111
	DB	110
	DB	32
	DB	111
	DB	102
	DB	32
	DB	115
	DB	116
	DB	97
	DB	103
	DB	101
	DB	32
	DB	108
	DB	101
	DB	110
	DB	103
	DB	116
	DB	104
	DB	32
	DB	97
	DB	110
	DB	100
	DB	32
	DB	35
	DB	32
	DB	111
	DB	102
	DB	32
	DB	101
	DB	112
	DB	111
	DB	99
	DB	104
	DB	115
	DB	0
__STRLITPACK_32	DB	105
	DB	110
	DB	105
	DB	116
	DB	44
	DB	32
	DB	110
	DB	101
	DB	120
	DB	116
	DB	32
	DB	105
	DB	110
	DB	112
	DB	117
	DB	116
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_30	DB	105
	DB	110
	DB	112
	DB	117
	DB	116
	DB	44
	DB	32
	DB	110
	DB	101
	DB	120
	DB	116
	DB	32
	DB	116
	DB	105
	DB	116
	DB	108
	DB	101
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_28	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	115
	DB	101
	DB	116
	DB	116
	DB	105
	DB	110
	DB	103
	DB	32
	DB	117
	DB	112
	DB	32
	DB	115
	DB	116
	DB	97
	DB	103
	DB	101
	DB	108
	DB	101
	DB	110
	DB	103
	DB	104
	DB	116
	DB	32
	DB	97
	DB	110
	DB	100
	DB	32
	DB	110
	DB	117
	DB	109
	DB	98
	DB	101
	DB	114
	DB	32
	DB	111
	DB	102
	DB	32
	DB	69
	DB	112
	DB	111
	DB	99
	DB	104
	DB	101
	DB	115
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_26	DB	83
	DB	105
	DB	109
	DB	80
	DB	101
	DB	114
	DB	105
	DB	111
	DB	100
	DB	32
	DB	115
	DB	104
	DB	111
	DB	117
	DB	108
	DB	100
	DB	32
	DB	98
	DB	101
	DB	32
	DB	109
	DB	117
	DB	108
	DB	116
	DB	105
	DB	112
	DB	108
	DB	101
	DB	115
	DB	32
	DB	111
	DB	102
	DB	32
	DB	116
	DB	104
	DB	101
	DB	32
	DB	110
	DB	117
	DB	109
	DB	98
	DB	101
	DB	114
	DB	32
	DB	111
	DB	102
	DB	32
	DB	69
	DB	112
	DB	111
	DB	99
	DB	104
	DB	101
	DB	115
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_38	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_20	DB	115
	DB	104
	DB	111
	DB	117
	DB	108
	DB	100
	DB	32
	DB	98
	DB	101
	DB	32
	DB	116
	DB	104
	DB	101
	DB	32
	DB	109
	DB	117
	DB	108
	DB	116
	DB	105
	DB	112
	DB	108
	DB	101
	DB	32
	DB	111
	DB	102
	DB	32
	DB	116
	DB	104
	DB	101
	DB	32
	DB	97
	DB	103
	DB	103
	DB	114
	DB	101
	DB	103
	DB	97
	DB	116
	DB	105
	DB	111
	DB	110
	DB	32
	DB	105
	DB	110
	DB	116
	DB	101
	DB	114
	DB	118
	DB	97
	DB	108
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_18	DB	83
	DB	105
	DB	109
	DB	80
	DB	101
	DB	114
	DB	105
	DB	111
	DB	100
	DB	32
	DB	61
	DB	32
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_16	DB	35
	DB	32
	DB	111
	DB	102
	DB	32
	DB	69
	DB	112
	DB	111
	DB	99
	DB	104
	DB	115
	DB	32
	DB	61
	DB	32
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_14	DB	97
	DB	103
	DB	103
	DB	114
	DB	101
	DB	103
	DB	97
	DB	116
	DB	105
	DB	111
	DB	110
	DB	32
	DB	105
	DB	110
	DB	116
	DB	101
	DB	114
	DB	118
	DB	97
	DB	108
	DB	32
	DB	61
	DB	32
	DB	0
__STRLITPACK_12	DB	110
	DB	105
	DB	110
	DB	116
	DB	40
	DB	110
	DB	105
	DB	110
	DB	116
	DB	40
	DB	83
	DB	105
	DB	109
	DB	80
	DB	101
	DB	114
	DB	105
	DB	111
	DB	100
	DB	41
	DB	47
	DB	69
	DB	112
	DB	111
	DB	99
	DB	78
	DB	117
	DB	109
	DB	47
	DB	120
	DB	109
	DB	105
	DB	110
	DB	80
	DB	101
	DB	114
	DB	83
	DB	105
	DB	109
	DB	73
	DB	110
	DB	116
	DB	41
	DB	32
	DB	61
	DB	32
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_50	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_10	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	115
	DB	101
	DB	116
	DB	116
	DB	105
	DB	110
	DB	103
	DB	32
	DB	117
	DB	112
	DB	32
	DB	97
	DB	103
	DB	103
	DB	114
	DB	101
	DB	103
	DB	97
	DB	116
	DB	105
	DB	111
	DB	110
	DB	32
	DB	105
	DB	110
	DB	116
	DB	101
	DB	114
	DB	118
	DB	97
	DB	108
	DB	32
	DB	97
	DB	110
	DB	100
	DB	32
	DB	35
	DB	32
	DB	111
	DB	102
	DB	32
	DB	69
	DB	112
	DB	111
	DB	99
	DB	104
	DB	101
	DB	115
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_8	DB	35
	DB	32
	DB	111
	DB	102
	DB	32
	DB	97
	DB	103
	DB	103
	DB	114
	DB	101
	DB	103
	DB	97
	DB	116
	DB	105
	DB	111
	DB	110
	DB	32
	DB	105
	DB	110
	DB	116
	DB	101
	DB	114
	DB	118
	DB	97
	DB	108
	DB	115
	DB	32
	DB	115
	DB	104
	DB	111
	DB	117
	DB	108
	DB	100
	DB	32
	DB	98
	DB	101
	DB	32
	DB	103
	DB	114
	DB	101
	DB	97
	DB	116
	DB	101
	DB	114
	DB	32
	DB	116
	DB	104
	DB	97
	DB	110
	DB	32
	DB	35
	DB	32
	DB	111
	DB	102
	DB	32
	DB	69
	DB	112
	DB	111
	DB	99
	DB	104
	DB	101
	DB	115
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_6	DB	35
	DB	32
	DB	111
	DB	102
	DB	32
	DB	101
	DB	112
	DB	111
	DB	99
	DB	104
	DB	101
	DB	115
	DB	32
	DB	61
	DB	32
	DB	0
__STRLITPACK_4	DB	35
	DB	32
	DB	111
	DB	102
	DB	32
	DB	97
	DB	103
	DB	103
	DB	114
	DB	101
	DB	103
	DB	97
	DB	116
	DB	105
	DB	111
	DB	110
	DB	32
	DB	105
	DB	110
	DB	116
	DB	101
	DB	114
	DB	118
	DB	97
	DB	108
	DB	115
	DB	32
	DB	61
	DB	32
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_57	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_2	DB	67
	DB	65
	DB	76
	DB	76
	DB	32
	DB	108
	DB	111
	DB	111
	DB	112
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_1	DB	100
	DB	101
	DB	108
	DB	32
	DB	46
	DB	92
	DB	116
	DB	111
	DB	108
	DB	108
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_0	DB	114
	DB	101
	DB	110
	DB	97
	DB	109
	DB	101
	DB	32
	DB	46
	DB	92
	DB	116
	DB	111
	DB	108
	DB	108
	DB	95
	DB	67
	DB	111
	DB	110
	DB	103
	DB	80
	DB	46
	DB	100
	DB	97
	DB	116
	DB	32
	DB	116
	DB	111
	DB	108
	DB	108
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 3 DUP ( 0H)	; pad
_2il0floatpacket.1	DD	080000000H
_2il0floatpacket.2	DD	04b000000H
_2il0floatpacket.3	DD	03f000000H
_2il0floatpacket.4	DD	0bf000000H
__STRLITPACK_287	DB	32
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_285	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_283	DB	42
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	66
	DB	97
	DB	115
	DB	105
	DB	99
	DB	32
	DB	73
	DB	110
	DB	102
	DB	111
	DB	114
	DB	109
	DB	97
	DB	116
	DB	105
	DB	111
	DB	110
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	42
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_281	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_279	DB	78
	DB	69
	DB	84
	DB	87
	DB	79
	DB	82
	DB	75
	DB	32
	DB	68
	DB	65
	DB	84
	DB	65
	DB	32
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_277	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	32
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_269	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	0
__STRLITPACK_267	DB	73
	DB	78
	DB	84
	DB	69
	DB	82
	DB	83
	DB	69
	DB	67
	DB	84
	DB	73
	DB	79
	DB	78
	DB	32
	DB	67
	DB	79
	DB	78
	DB	84
	DB	82
	DB	79
	DB	76
	DB	32
	DB	68
	DB	65
	DB	84
	DB	65
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_265	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_251	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	0
__STRLITPACK_249	DB	82
	DB	65
	DB	77
	DB	80
	DB	32
	DB	68
	DB	65
	DB	84
	DB	65
	DB	32
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_247	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_241	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	0
__STRLITPACK_239	DB	83
	DB	79
	DB	76
	DB	85
	DB	84
	DB	73
	DB	79
	DB	78
	DB	32
	DB	77
	DB	79
	DB	68
	DB	69
	DB	32
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_237	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_233	DB	32
	DB	32
	DB	32
	DB	69
	DB	120
	DB	101
	DB	99
	DB	117
	DB	116
	DB	101
	DB	32
	DB	73
	DB	116
	DB	101
	DB	114
	DB	97
	DB	116
	DB	105
	DB	118
	DB	101
	DB	32
	DB	67
	DB	111
	DB	110
	DB	115
	DB	105
	DB	115
	DB	116
	DB	101
	DB	110
	DB	99
	DB	121
	DB	32
	DB	65
	DB	108
	DB	103
	DB	111
	DB	114
	DB	105
	DB	116
	DB	104
	DB	109
	DB	40
	DB	69
	DB	113
	DB	117
	DB	108
	DB	105
	DB	108
	DB	98
	DB	114
	DB	105
	DB	117
	DB	109
	DB	41
	DB	0
__STRLITPACK_235	DB	32
	DB	32
	DB	69
	DB	120
	DB	101
	DB	99
	DB	117
	DB	116
	DB	101
	DB	32
	DB	79
	DB	110
	DB	101
	DB	45
	DB	83
	DB	104
	DB	111
	DB	116
	DB	32
	DB	83
	DB	105
	DB	109
	DB	117
	DB	108
	DB	97
	DB	116
	DB	105
	DB	111
	DB	110
	DB	32
	DB	77
	DB	111
	DB	100
	DB	101
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_227	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	0
__STRLITPACK_225	DB	84
	DB	73
	DB	77
	DB	69
	DB	32
	DB	80
	DB	69
	DB	82
	DB	73
	DB	79
	DB	68
	DB	83
	DB	32
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_223	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_209	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	0
__STRLITPACK_207	DB	70
	DB	79
	DB	82
	DB	32
	DB	67
	DB	79
	DB	78
	DB	71
	DB	69
	DB	83
	DB	84
	DB	73
	DB	79
	DB	78
	DB	32
	DB	80
	DB	82
	DB	73
	DB	67
	DB	73
	DB	78
	DB	71
	DB	32
	DB	68
	DB	69
	DB	84
	DB	65
	DB	73
	DB	76
	DB	83
	DB	32
	DB	67
	DB	72
	DB	69
	DB	67
	DB	75
	DB	32
	DB	84
	DB	79
	DB	76
	DB	76
	DB	46
	DB	68
	DB	65
	DB	84
	DB	32
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_205	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_203	DB	86
	DB	69
	DB	72
	DB	73
	DB	67
	DB	76
	DB	69
	DB	32
	DB	76
	DB	79
	DB	65
	DB	68
	DB	73
	DB	78
	DB	71
	DB	32
	DB	77
	DB	79
	DB	68
	DB	69
	DB	32
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_201	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_187	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	0
__STRLITPACK_185	DB	84
	DB	82
	DB	65
	DB	70
	DB	70
	DB	73
	DB	67
	DB	32
	DB	77
	DB	65
	DB	78
	DB	65
	DB	71
	DB	69
	DB	77
	DB	69
	DB	78
	DB	84
	DB	32
	DB	83
	DB	84
	DB	82
	DB	65
	DB	84
	DB	69
	DB	71
	DB	73
	DB	69
	DB	83
	DB	32
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_183	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_155	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	0
__STRLITPACK_153	DB	67
	DB	65
	DB	80
	DB	65
	DB	67
	DB	73
	DB	84
	DB	89
	DB	32
	DB	82
	DB	69
	DB	68
	DB	85
	DB	67
	DB	84
	DB	73
	DB	79
	DB	78
	DB	32
	DB	0
__STRLITPACK_151	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	32
	DB	0
__STRLITPACK_149	DB	32
	DB	32
	DB	32
	DB	45
	DB	45
	DB	32
	DB	73
	DB	110
	DB	99
	DB	105
	DB	100
	DB	101
	DB	110
	DB	116
	DB	32
	DB	32
	DB	45
	DB	45
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_145	DB	32
	DB	32
	DB	32
	DB	45
	DB	45
	DB	32
	DB	87
	DB	111
	DB	114
	DB	107
	DB	32
	DB	90
	DB	111
	DB	110
	DB	101
	DB	32
	DB	45
	DB	45
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_136	DB	100
	DB	101
	DB	109
	DB	97
	DB	110
	DB	100
	DB	43
	DB	86
	DB	101
	DB	104
	DB	43
	DB	80
	DB	97
	DB	116
	DB	104
	DB	32
	DB	70
	DB	105
	DB	108
	DB	101
	DB	115
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_137	DB	100
	DB	101
	DB	109
	DB	97
	DB	110
	DB	43
	DB	86
	DB	101
	DB	104
	DB	105
	DB	99
	DB	108
	DB	101
	DB	32
	DB	70
	DB	105
	DB	108
	DB	101
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_138	DB	86
	DB	101
	DB	104
	DB	105
	DB	99
	DB	108
	DB	101
	DB	43
	DB	80
	DB	97
	DB	116
	DB	104
	DB	32
	DB	70
	DB	105
	DB	108
	DB	101
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_139	DB	86
	DB	101
	DB	104
	DB	105
	DB	99
	DB	108
	DB	101
	DB	32
	DB	70
	DB	105
	DB	108
	DB	101
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_140	DB	79
	DB	68
	DB	32
	DB	84
	DB	97
	DB	98
	DB	108
	DB	101
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_134	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_132	DB	42
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	76
	DB	111
	DB	97
	DB	100
	DB	105
	DB	110
	DB	103
	DB	32
	DB	73
	DB	110
	DB	102
	DB	111
	DB	114
	DB	109
	DB	97
	DB	116
	DB	105
	DB	111
	DB	110
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	42
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_130	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	0
	DB 3 DUP ( 0H)	; pad
_2il0floatpacket.5	DD	041200000H
_2il0floatpacket.6	DD	042c80000H
__STRLITPACK_482	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	0
__STRLITPACK_480	DB	86
	DB	69
	DB	72
	DB	73
	DB	67
	DB	76
	DB	69
	DB	32
	DB	84
	DB	89
	DB	80
	DB	69
	DB	32
	DB	80
	DB	69
	DB	82
	DB	67
	DB	69
	DB	78
	DB	84
	DB	65
	DB	71
	DB	69
	DB	83
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_478	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_460	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	0
__STRLITPACK_458	DB	77
	DB	84
	DB	67
	DB	32
	DB	67
	DB	76
	DB	65
	DB	83
	DB	83
	DB	32
	DB	80
	DB	69
	DB	82
	DB	67
	DB	69
	DB	78
	DB	84
	DB	65
	DB	71
	DB	69
	DB	83
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_456	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	45
	DB	0
	DB 2 DUP ( 0H)	; pad
_2il0floatpacket.7	DD	042c80000H
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
EXTRN	_DYNUST_MAIN_MODULE_mp_NZONES:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_FUELOUT:BYTE
EXTRN	_DYNUST_CONGESTIONPRICING_MODULE_mp_NSEG:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_UETIMEFLAGW:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_REACH_CONVERG:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_MAXID:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_JUSTVEH:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TOTALVEH:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ICALLKSP:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ITERATION:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_RUNMODE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TIME_NOW:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_IUE_OK:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ISO_OK:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ITEMAX:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_AGGINT:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ITI_NUEP:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_EPOCPERIOD:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_SIMPERAGG:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_EPOCNUM:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_SIMPERIOD:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_LOGOUT:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_WORKZONE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_WORKZONENUM:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_INCI:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_INCIL:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_INCI_NUM:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_VMS_END:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_VMS_START:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_VMS:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_VMSTYPE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TRAVELTIMEWEIGHT:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NO_VIA:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_MTC_DIFF:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_SIMPERASG:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_VMS_NUM:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_DEC_NUM:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NOOFNODES:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NODETMP:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NOOFARCS_ORG:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NOOFNODES_ORG:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_READVEH:BYTE
_DATA	ENDS
EXTRN	_for_cpystr:PROC
EXTRN	_for_write_seq_fmt_xmit:PROC
EXTRN	_for_write_seq_fmt:PROC
EXTRN	_WRITE_TITLE:PROC
EXTRN	_for_close:PROC
EXTRN	_for_write_seq_lis_xmit:PROC
EXTRN	_for_stop_core:PROC
EXTRN	_for_write_seq_lis:PROC
EXTRN	_DYNUST_CONGESTIONPRICING_MODULE_mp_PRICECAL:PROC
EXTRN	_DYNUST_CONGESTIONPRICING_MODULE_mp_PRICEOUTPUT:PROC
EXTRN	_DYNUST_FUEL_MODULE_mp_WRITEOUTFUELCONSUM:PROC
EXTRN	_DYNUST_FUEL_MODULE_mp_WRITEOUTFUELCONSUM_CITY:PROC
EXTRN	_MEMORY_MODULE_mp_DEALLOCATE_SIM:PROC
EXTRN	_OPENFILE:PROC
EXTRN	_INITIALIZE:PROC
EXTRN	_READ_INPUT:PROC
EXTRN	_CHECK_MIVA_VEHICLE:PROC
EXTRN	_SIM_MAIN_LOOP:PROC
EXTRN	_RESORT_OUTPUT_VEH:PROC
EXTRN	_WRITE_SUMMARY_STAT:PROC
EXTRN	_CAL_PENALTY_MIVA:PROC
EXTRN	_WRITEUETRAVELTIME:PROC
EXTRN	_WRITE_TOLL_REVENUE:PROC
EXTRN	_SYSTEM:PROC
EXTRN	__fltused:BYTE
	INCLUDELIB <ifconsol>
	INCLUDELIB <libifcoremt>
	INCLUDELIB <libifport>
	INCLUDELIB <libmmt>
	INCLUDELIB <LIBCMT>
	INCLUDELIB <libirc>
	INCLUDELIB <svml_dispmt>
	INCLUDELIB <OLDNAMES>
	END
