SUBROUTINE write_TransitVehicles(maxveh  ,isj     ,kj      ,fu1,isee,fu2, nodeU, nodeD, nodeE, deptime, RouteSize, RouteArray)
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~ Copyright (C) 2002-2011 The Arizona Board of Regents on Behalf of The University of Arizona.  ~~!
!~~ Developed by Yi-Chang Chiu, Ph.D. as par to the DynusT project.                               ~~!
!~~ This program is free software: you can redistribute it and/or modify it                       ~~!
!~~ under the terms of the GNU General Public License version 2 as published by                   ~~!
!~~ the Free Software Foundation.                                                                 ~~!
!~~ This program is distributed in the hope that it will be useful,                               ~~!
!~~ but WITHOUT ANY WARRANTY; without even the implied warranty of                                ~~!
!~~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                                          ~~!
!~~ See the GNU General Public License for more details.                                          ~~!
!~~ You should have received a copy of the GNU General Public License                             ~~!
!~~ along with this program (in the file COPYING).                                                ~~!
!~~ IF not, see <http://www.gnu.org/licenses/>.                                                   ~~!
!~~ Please see http://www.dynust.net for contact information and names                            ~~!
!~~ of individual contributors.                                                                   ~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
    USE DYNUST_MAIN_MODULE
    USE DYNUST_VEH_MODULE
    USE DYNUST_NETWORK_MODULE
	USE DYNUST_VEH_PATH_ATT_MODULE
	USE DYNUST_FUEL_MODULE
 
    INTEGER, intent(in):: kj,isj,RouteArray(1000),RouteSize
    INTEGER, intent(in):: maxveh,fu1,fu2,isee
    REAL, intent(in):: deptime
!> > > > > > > > > >                < < < < < < < < < < <
!>>>>>>>>>>>>>>>>>>>CODES START HERE<<<<<<<<<<<<<<<<<<<<<
!> > > > > > > > > >                < < < < < < < < < < <

    WRITE(fu1,301) kj, nodeU, nodeD, deptime, 1, 4,1,routesize, 1, 0, 0, 0, m_dynust_network_node(OutToInNodeNum(inodeU))%iConZone(2),0,1.0,0

    WRITE(fu1,400) m_dynust_network_node_nde(OutToInNodeNum(nodeE))%izone,m_dynust_veh(kj)%IntDestDwell(j_ah)/60.0
    IF(isee == 1) WRITE(fu2,700) m_dynust_network_node_nde(m_dynust_network_arc_nde(m_dynust_last_stand(kj)%isec)%iunod)%IntoOutNodeNum,(m_dynust_network_node_nde(nint(vehatt_Value(kj,js,1)))%IntoOutNodeNum,js=1,vehatt_P_Size(kj)-1)

301   FORMAT(i9,2i7,f8.2,6i6,2f8.4,2i5,f12.8,f6.1)
400   FORMAT(i12,f7.2)
700   FORMAT(1000i7)
302	  FORMAT(i6)

END SUBROUTINE
    
