	SUBROUTINE WRITE_SUMMARY_STAT(l)
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~ Copyright (C) 2002-2011 The Arizona Board of Regents on Behalf of The University of Arizona.  ~~!
!~~ Developed by Yi-Chang Chiu, Ph.D. as par to the DynusT project.                               ~~!
!~~ This program is free software: you can redistribute it and/or modify it                       ~~!
!~~ under the terms of the GNU General Public License version 2 as published by                   ~~!
!~~ the Free Software Foundation.                                                                 ~~!
!~~ This program is distributed in the hope that it will be useful,                               ~~!
!~~ but WITHOUT ANY WARRANTY; without even the implied warranty of                                ~~!
!~~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                                          ~~!
!~~ See the GNU General Public License for more details.                                          ~~!
!~~ You should have received a copy of the GNU General Public License                             ~~!
!~~ along with this program (in the file COPYING).                                                ~~!
!~~ IF not, see <http://www.gnu.org/licenses/>.                                                   ~~!
!~~ Please see http://www.dynust.net for contact information and names                            ~~!
!~~ of individual contributors.                                                                   ~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
    USE DYNUST_MAIN_MODULE
    USE DYNUST_VEH_MODULE
	USE DYNUST_VEH_PATH_ATT_MODULE
	USE DYNUST_NETWORK_MODULE
    LOGICAL EndID
	REAL::www = 0
	
!> > > > > > > > > >                < < < < < < < < < < <
!>>>>>>>>>>>>>>>>>>>CODES START HERE<<<<<<<<<<<<<<<<<<<<<
!> > > > > > > > > >                < < < < < < < < < < <

	  CALL WRITE_VEH_TRAJECTORY(l)


164   FORMAT('FRACTION WITH INFO =',F6.3,'  AVG.IB-FRACTION =',F5.2,'   BOUND =',F5.2/)


      WRITE(666,*) 'NOTE : There are', numcars,'  target vehicles still in the network'
      number_cars_total=0
      IF(justveh >= NoofVeh) THEN
        number_cars_total=NoofVeh
      ELSE
        number_cars_total=justveh
      ENDIF

      DO 171 j=1,number_cars_total

      www = 0

      IF(m_dynust_veh(j)%notin == 1) go to 171 ! veh has left the network. THE FOLLOWING IS TO PROCESS THOSE WHICH ARE STILL IN THE NETWORK
!     GET THE CURRENT LINK # AND 
      is = m_dynust_veh_nde(j)%icurrnt
      IF(is > 1) THEN ! VEHICLE TRAVELS AT LEAST ONE LINK
        iud = vehatt_value(j,is-1,1)
        idd = vehatt_value(j,is  ,1)
        ilink = GetFlinkFromNode(iud,idd,17)
      ELSE ! STILL ON THE GENERATION LINK
        ilink = m_dynust_last_stand(j)%isec
      ENDIF
      m_dynust_veh(j)%distans = m_dynust_veh(j)%distans + max(0.0,m_dynust_network_arc_nde(ilink)%s - m_dynust_veh(j)%position)
      
      CALL vehatt_A_Remove(j) ! remove the P and A array
      CALL vehatt_P_Remove(j) 

      EndID = .false.
	  CALL WRITE_SUMMARY(j,EndID,m_dynust_last_stand(j)%stime)
	  CALL WRITE_SUMMARY_TYPEBASED(j,EndID,m_dynust_last_stand(j)%stime)

      IF(m_dynust_veh(j)%itag == 1)THEN
        itag1=itag1+1
        IF(m_dynust_veh(j)%NoOfIntDst > 1) THEN
	    DO ka=1,m_dynust_veh(j)%NoOfIntDst-1
            www=www+m_dynust_veh(j)%IntDestDwell(ka)/60.0
          ENDDO
        ENDIF
        STOPtemp = m_dynust_veh(j)%ttSTOP
	    STOPtime=STOPtime+m_dynust_veh(j)%ttSTOP
        IF(m_dynust_last_stand(j)%atime > 0) THEN
          ttt=max(0.0,m_dynust_last_stand(j)%atime-m_dynust_last_stand(j)%stime-m_dynust_veh(j)%ttilnow-www)  ! only difference 
	      tt =max(0.0,m_dynust_last_stand(j)%atime-m_dynust_last_stand(j)%stime-www)             ! with CAL_ACCUMU_STAT
        ELSE                                             !
          ttt=max(0.0,l*xminPerSimInt-m_dynust_last_stand(j)%stime-m_dynust_veh(j)%ttilnow-www)     !
	      tt =max(0.0,l*xminPerSimInt-m_dynust_last_stand(j)%stime-www)                ! 
        ENDIF                                            !
        entry_queue1=entry_queue1+ttt
        triptime1=triptime1+tt
        dtotal1=dtotal1+m_dynust_veh(j)%distans
        vtothr1=vtothr1+m_dynust_veh(j)%ttilnow
        itag2=itag2+1

        IF(m_dynust_veh_nde(j)%info == 1) THEN
	     entry_queue2=entry_queue2+ttt
           triptime2=triptime2+tt
           information=information+1
           vtothr2=vtothr2+m_dynust_veh(j)%ttilnow
           dtotal2=dtotal2+m_dynust_veh(j)%distans
           STOPinfo=STOPinfo+STOPtemp
!           m_dynust_veh(j)%switch=iabs(m_dynust_veh(j)%switch)-1
           totaldecision=m_dynust_veh(j)%decision(1)+totaldecision
!           totalswitch=m_dynust_veh(j)%switch+totalswitch
           IF(m_dynust_veh(j)%NoOfIntDst == 1) THEN
                             entry_queue2_1=entry_queue2_1+ttt
                             triptime2_1=triptime2_1+tt
                             information_1=information_1+1
                             vtothr2_1=vtothr2_1+m_dynust_veh(j)%ttilnow
                             dtotal2_1=dtotal2_1+m_dynust_veh(j)%distans
                             STOPinfo_1=STOPinfo_1+STOPtemp
           ELSEIF(m_dynust_veh(j)%NoOfIntDst == 2) THEN
                             entry_queue2_2=entry_queue2_2+ttt
                             triptime2_2=triptime2_2+tt
                             information_2=information_2+1
                             vtothr2_2=vtothr2_2+m_dynust_veh(j)%ttilnow
                             dtotal2_2=dtotal2_2+m_dynust_veh(j)%distans
                             STOPinfo_2=STOPinfo_2+STOPtemp
           ELSEIF(m_dynust_veh(j)%NoOfIntDst == 3) THEN
                             entry_queue2_3=entry_queue2_3+ttt
                             triptime2_3=triptime2_3+tt
                             information_3=information_3+1
                             vtothr2_3=vtothr2_3+m_dynust_veh(j)%ttilnow
                             dtotal2_3=dtotal2_3+m_dynust_veh(j)%distans
                             STOPinfo_3=STOPinfo_3+STOPtemp
           ENDIF
!           DO is=1,nu_switch+1
!             IF(m_dynust_veh(j)%switch == is-1) switchnum(is)=switchnum(is)+1
!           ENDDO
!           DO is=1,nu_switch+1
!             IF(m_dynust_veh(j)%decision(1) == is-1) decisionnum(is)=decisionnum(is)+1
!           ENDDO
!           IF(m_dynust_veh(j)%switch > nu_switch) switchnum(nu_switch+1)=switchnum(nu_switch+1)+1
!           IF(m_dynust_veh(j)%decision(1) > nu_switch) decisionnum(nu_switch+1)=decisionnum(nu_switch+1)+1

        ELSEIF(m_dynust_veh_nde(j)%info == 0) THEN
	     entry_queue3=entry_queue3+ttt
           triptime3=triptime3+tt
           noinformation=noinformation+1
           vtothr3=vtothr3+m_dynust_veh(j)%ttilnow
           dtotal3=dtotal3+m_dynust_veh(j)%distans
           STOPnoinfo=STOPnoinfo+STOPtemp
           IF(m_dynust_veh(j)%NoOfIntDst == 1) THEN
                             entry_queue3_1=entry_queue3_1+ttt
                             triptime3_1=triptime3_1+tt
                             noinformation_1=noinformation_1+1
                             vtothr3_1=vtothr3_1+m_dynust_veh(j)%ttilnow
                             dtotal3_1=dtotal3_1+m_dynust_veh(j)%distans
                             STOPnoinfo_1=STOPnoinfo_1+STOPtemp
           ELSEIF(m_dynust_veh(j)%NoOfIntDst == 2) THEN
                             entry_queue3_2=entry_queue3_2+ttt
                             triptime3_2=triptime3_2+tt
                             noinformation_2=noinformation_2+1
                             vtothr3_2=vtothr3_2+m_dynust_veh(j)%ttilnow
                             dtotal3_2=dtotal3_2+m_dynust_veh(j)%distans
                             STOPnoinfo_2=STOPnoinfo_2+STOPtemp
           ELSEIF(m_dynust_veh(j)%NoOfIntDst == 3) THEN
                             entry_queue3_3=entry_queue3_3+ttt
                             triptime3_3=triptime3_3+tt
                             noinformation_3=noinformation_3+1
                             vtothr3_3=vtothr3_3+m_dynust_veh(j)%ttilnow
                             dtotal3_3=dtotal3_3+m_dynust_veh(j)%distans
                             STOPnoinfo_2=STOPnoinfo_2+STOPtemp
           ENDIF
        ENDIF

	ELSEIF(m_dynust_veh(j)%itag == 0) THEN

        itag0=itag0+1

	ENDIF


171   CONTINUE
172   CONTINUE


      EndID=.true.
      IF(number_cars_total > 0) CALL WRITE_SUMMARY(number_cars_total,EndID,m_dynust_last_stand(number_cars_total)%stime)
      IF(number_cars_total > 0) CALL WRITE_SUMMARY_TYPEBASED(number_cars_total,EndID,m_dynust_last_stand(number_cars_total)%stime)      


        IF((noinformation+information) > 0) THEN
           vavg1=vtothr1/float(information+noinformation)
           avedtotal1=dtotal1/(noinformation+information)
           ave_entry1=entry_queue1/float(noinformation+information)
           ave_trip1=triptime1/float(noinformation+information)
           aveSTOPtime=STOPtime/(noinformation+information)
        ENDIF

        IF(information > 0) THEN
          vavg2=vtothr2/float(information)
          avedtotal2=dtotal2/information
          ave_entry2=entry_queue2/float(information)
          ave_trip2=triptime2/float(information)
          aveSTOPinfo=STOPinfo/information
        ENDIF

        IF(information_1 > 0) THEN
          vavg2_1=vtothr2_1/float(information_1)
          avedtotal2_1=dtotal2_1/float(information_1)
          ave_entry2_1=entry_queue2_1/float(information_1)
          ave_trip2_1=triptime2_1/float(information_1)
          aveSTOPinfo_1=STOPinfo_1/float(information_1)
        ENDIF

        IF(information_2 > 0) THEN
          vavg2_2=vtothr2_2/float(information_2)
          avedtotal2_2=dtotal2_2/float(information_2)
          ave_entry2_2=entry_queue2_2/float(information_2)
          ave_trip2_2=triptime2_2/float(information_2)
          aveSTOPinfo_2=STOPinfo_2/float(information_2)
        ENDIF

        IF(information_3 > 0) THEN
          vavg2_3=vtothr2_3/float(information_3)
          avedtotal2_3=dtotal2_3/float(information_3)
          ave_entry2_3=entry_queue2_3/float(information_3)
          ave_trip2_3=triptime2_3/float(information_3)
          aveSTOPinfo_3=STOPinfo_3/float(information_3)
        ENDIF

        IF(noinformation > 0) THEN 
          vavg3=vtothr3/float(noinformation)
          avedtotal3=dtotal3/float(noinformation)
          ave_entry3=entry_queue3/float(noinformation)
          ave_trip3=triptime3/float(noinformation)
          aveSTOPnoinfo=STOPnoinfo/float(noinformation)
        ENDIF

        IF(noinformation_1 > 0) THEN 
          vavg3_1=vtothr3_1/float(noinformation_1)
          avedtotal3_1=dtotal3_1/float(noinformation_1)
          ave_entry3_1=entry_queue3_1/float(noinformation_1)
          ave_trip3_1=triptime3_1/float(noinformation_1)
          aveSTOPnoinfo_1=STOPnoinfo_1/float(noinformation_1)
        ENDIF

        IF(noinformation_2 > 0) THEN 
          vavg3_2=vtothr3_2/float(noinformation_2)
          avedtotal3_2=dtotal3_2/float(noinformation_2)
          ave_entry3_2=entry_queue3_2/float(noinformation_2)
          ave_trip3_2=triptime3_2/float(noinformation_2)
          aveSTOPnoinfo_2=STOPnoinfo_2/float(noinformation_2)
        ENDIF

        IF(noinformation_3 > 0) THEN 
          vavg3_3=vtothr3_3/float(noinformation_3)
          avedtotal3_3=dtotal3_3/float(noinformation_3)
          ave_entry3_3=entry_queue3_3/float(noinformation_3)
          ave_trip3_3=triptime3_3/float(noinformation_3)
          aveSTOPnoinfo_3=STOPnoinfo_3/float(noinformation_3)
        ENDIF


      WRITE(666,*) ' '
      WRITE(666,*) ' ******* VEHICLE INFORMATION ******* '
      WRITE(666,*) ' 	TOTAL VEHICLES        : ',justveh
      WRITE(666,*) ' 	NON-TAGGED VEHICLES   : ',itag0
      WRITE(666,*) ' 	TAGGED VEHICLES (IN)  : ',itag1
      WRITE(666,*) ' 	TAGGED VEHICLES (OUT) : ',itag2
      WRITE(666,*) ' 	OTHERS                : ',itag3
      WRITE(666,*)

      WRITE(666,*) ' ******* HOT LANE(S) INFORMATION SEE TOLLREVENUE.DAT ********** '

      WRITE(666,*)
      WRITE(666,*) '***************************************'
      WRITE(666,*) '*  OVERALL STATISTICS REPORT          *'
      WRITE(666,*) '***************************************'
      WRITE(666,'( "    Iteration Number                           : ",i10)')   iteration
      WRITE(666,'( "    Max Simulation Time (min)                  : ",f10.1)') SimPeriod
      WRITE(666,'( "    Actual Sim. Intervals                      : ",i8)') L
      WRITE(666,'( "    Simulation Time     (min)                  : ",f10.1)') L*xminPerSimInt
      WRITE(666,'( "    Start Time in Which Veh Stat are Collected : ",f10.1)') starttm
      WRITE(666,'( "    End   Time in Which Veh Stat are Collected : ",f10.1)') endtm
      WRITE(666,'( "    Total Number of Vehicles of Interest       : ",i8)') INFORMATION+NOINFORMATION
      WRITE(666,'( "                            With    Info       : ",i8)') INFORMATION
      WRITE(666,'( "                            Without Info       : ",i8)') NOINFORMATION
      WRITE(666,*) '------------------------------------------------'
      WRITE(666,*) 'TOTAL TRAVEL TIMES (HRS)'
      WRITE(666,'( "    OVERALL    : ",f14.4)')VTOTHR1/60.0
      if(iteration > 0) totaltime(iteration) = VTOTHR1/60.0
      WRITE(666,'( "    NOINFO     : ",f14.4)')VTOTHR3/60.0
      WRITE(666,'( "    1 STOP     : ",f14.4)')VTOTHR3_1/60.0
      WRITE(666,'( "    2 STOPs    : ",f14.4)')VTOTHR3_2/60.0
      WRITE(666,'( "    3 STOPs    : ",f14.4)')VTOTHR3_3/60.0
      WRITE(666,'( "    INFO       : ",f14.4)')VTOTHR2/60.0
      WRITE(666,'( "    1 STOP     : ",f14.4)')VTOTHR2_1/60.0
      WRITE(666,'( "    2 STOPs    : ",f14.4)')VTOTHR2_2/60.0
      WRITE(666,'( "    3 STOPs    : ",f14.4)')VTOTHR2_3/60.0
      WRITE(666,*) 
      WRITE(666,*) 'AVERAGE TRAVEL TIMES (MINS)'
      WRITE(666,'( "    OVERALL    : ",f14.4)')VAVG1
      WRITE(666,'( "    NOINFO     : ",f14.4)')VAVG3
      WRITE(666,'( "    1 STOP     : ",f14.4)')VAVG3_1
      WRITE(666,'( "    2 STOPs    : ",f14.4)')VAVG3_2
      WRITE(666,'( "    3 STOPs    : ",f14.4)')VAVG3_3
      WRITE(666,'( "    INFO       : ",f14.4)')VAVG2
      WRITE(666,'( "    1 STOP     : ",f14.4)')VAVG2_1
      WRITE(666,'( "    2 STOPs    : ",f14.4)')VAVG2_2
      WRITE(666,'( "    3 STOPs    : ",f14.4)')VAVG2_3
      WRITE(666,*) 
      WRITE(666,*) '------------------------------------------------'
      WRITE(666,*) 'TOTAL TRIP TIMES (INCLUDING ENTRY QUEUE TIME) (HRS) '
      WRITE(666,'( "    OVERALL    : ",f14.4)')triptime1/60
      WRITE(666,'( "    NOINFO     : ",f14.4)')triptime3/60
      WRITE(666,'( "    1 STOP     : ",f14.4)')triptime3_1/60.0
      WRITE(666,'( "    2 STOPs    : ",f14.4)')triptime3_2/60.0
      WRITE(666,'( "    3 STOPs    : ",f14.4)')triptime3_3/60.0
      WRITE(666,'( "    INFO       : ",f14.4)')triptime2/60
      WRITE(666,'( "    1 STOP     : ",f14.4)')triptime2_1/60.0
      WRITE(666,'( "    2 STOPs    : ",f14.4)')triptime2_2/60.0
      WRITE(666,'( "    3 STOPs    : ",f14.4)')triptime2_3/60.0
      WRITE(666,*) 'AVERAGE TRIP TIMES (INCLUDING ENTRY QUEUE TIME) (MINS)'
      WRITE(666,'( "    OVERALL    : ",f14.4)')ave_trip1
      WRITE(666,'( "    NOINFO     : ",f14.4)')ave_trip3
      WRITE(666,'( "    1 STOP     : ",f14.4)')ave_trip3_1
      WRITE(666,'( "    2 STOPs    : ",f14.4)')ave_trip3_2
      WRITE(666,'( "    3 STOPs    : ",f14.4)')ave_trip3_3
      WRITE(666,'( "    INFO       : ",f14.4)')ave_trip2
      WRITE(666,'( "    1 STOP     : ",f14.4)')ave_trip2_1
      WRITE(666,'( "    2 STOPs    : ",f14.4)')ave_trip2_2
      WRITE(666,'( "    3 STOPs    : ",f14.4)')ave_trip2_3
      WRITE(666,*) 
      WRITE(666,*) '------------------------------------------------'
      WRITE(666,*) 'TOTAL ENTRY QUEUE TIMES (HRS)'
      WRITE(666,'( "    OVERALL    : ",f14.4)')entry_queue1/60
      WRITE(666,'( "    NOINFO     : ",f14.4)')entry_queue3/60
      WRITE(666,'( "    1 STOP     : ",f14.4)')entry_queue3_1/60.0
      WRITE(666,'( "    2 STOPs    : ",f14.4)')entry_queue3_2/60.0
      WRITE(666,'( "    3 STOPs    : ",f14.4)')entry_queue3_3/60.0
      WRITE(666,'( "    INFO       : ",f14.4)')entry_queue2/60
      WRITE(666,'( "    1 STOP     : ",f14.4)')entry_queue2_1/60.0
      WRITE(666,'( "    2 STOPs    : ",f14.4)')entry_queue2_2/60.0
      WRITE(666,'( "    3 STOPs    : ",f14.4)')entry_queue2_3/60.0
      WRITE(666,*) 'AVERAGE ENTRY QUEUE TIMES (MINS)'
      WRITE(666,'( "    OVERALL    : ",f14.4)')ave_entry1
      WRITE(666,'( "    NOINFO     : ",f14.4)')ave_entry3
      WRITE(666,'( "    1 STOP     : ",f14.4)')ave_entry3_1
      WRITE(666,'( "    2 STOPs    : ",f14.4)')ave_entry3_2
      WRITE(666,'( "    3 STOPs    : ",f14.4)')ave_entry3_3
      WRITE(666,'( "    INFO       : ",f14.4)')ave_entry2
      WRITE(666,'( "    1 STOP     : ",f14.4)')ave_entry2_1
      WRITE(666,'( "    2 STOPs    : ",f14.4)')ave_entry2_2
      WRITE(666,'( "    3 STOPs    : ",f14.4)')ave_entry2_3
      WRITE(666,*) 
      WRITE(666,*) '-------------------------------------------------'
      WRITE(666,*) 'TOTAL STOP TIME ( HRS )'
      WRITE(666,'( "    OVERALL    : ",f14.4)')STOPtime/60.0
      WRITE(666,'( "    NOINFO     : ",f14.4)')STOPnoinfo/60.0
      WRITE(666,'( "    1 STOP     : ",f14.4)')STOPnoinfo_1/60.0
      WRITE(666,'( "    2 STOPs    : ",f14.4)')STOPnoinfo_2/60.0
      WRITE(666,'( "    3 STOPs    : ",f14.4)')STOPnoinfo_3/60.0
      WRITE(666,'( "    INFO       : ",f14.4)')STOPinfo/60.0
      WRITE(666,'( "    1 STOP     : ",f14.4)')STOPinfo_1/60.0
      WRITE(666,'( "    2 STOPs    : ",f14.4)')STOPinfo_2/60.0
      WRITE(666,'( "    3 STOPs    : ",f14.4)')STOPinfo_3/60.0
      WRITE(666,*) 'AVERAGE STOP TIME ( MINS )'
      WRITE(666,'( "    OVERALL    : ",f14.4)')aveSTOPtime
      WRITE(666,'( "    NOINFO     : ",f14.4)')aveSTOPnoinfo
      WRITE(666,'( "    1 STOP     : ",f14.4)')aveSTOPnoinfo_1
      WRITE(666,'( "    2 STOPs    : ",f14.4)')aveSTOPnoinfo_2
      WRITE(666,'( "    3 STOPs    : ",f14.4)')aveSTOPnoinfo_3
      WRITE(666,'( "    INFO       : ",f14.4)')aveSTOPinfo
      WRITE(666,'( "    1 STOP     : ",f14.4)')aveSTOPinfo_1
      WRITE(666,'( "    2 STOPs    : ",f14.4)')aveSTOPinfo_2
      WRITE(666,'( "    3 STOPs    : ",f14.4)')aveSTOPinfo_3
      WRITE(666,*) 
      WRITE(666,*) '-------------------------------------------------'
      WRITE(666,*) 'TOTAL TRIP DISTANCE ( MILES )'
      WRITE(666,'( "    OVERALL    : ",f14.4)')dtotal1
      WRITE(666,'( "    NOINFO     : ",f14.4)')dtotal3
      WRITE(666,'( "    1 STOP     : ",f14.4)')dtotal3_1
      WRITE(666,'( "    2 STOPs    : ",f14.4)')dtotal3_2
      WRITE(666,'( "    3 STOPs    : ",f14.4)')dtotal3_3
      WRITE(666,'( "    INFO       : ",f14.4)')dtotal2
      WRITE(666,'( "    1 STOP     : ",f14.4)')dtotal2_1
      WRITE(666,'( "    2 STOPs    : ",f14.4)')dtotal2_2
      WRITE(666,'( "    3 STOPs    : ",f14.4)')dtotal2_3
      WRITE(666,*) 'AVERAGE TRIP DISTANCE ( MILES )'
      WRITE(666,'( "    OVERALL    : ",f14.4)')avedtotal1
      WRITE(666,'( "    NOINFO     : ",f14.4)')avedtotal3
      WRITE(666,'( "    1 STOP     : ",f14.4)')avedtotal3_1
      WRITE(666,'( "    2 STOPs    : ",f14.4)')avedtotal3_2
      WRITE(666,'( "    3 STOPs    : ",f14.4)')avedtotal3_3
      WRITE(666,'( "    INFO       : ",f14.4)')avedtotal2
      WRITE(666,'( "    1 STOP     : ",f14.4)')avedtotal2_1
      WRITE(666,'( "    2 STOPs    : ",f14.4)')avedtotal2_2
      WRITE(666,'( "    3 STOPs    : ",f14.4)')avedtotal2_3
      WRITE(666,*)
      WRITE(666,*) '-------------------------------------------------'

      close(666)

END SUBROUTINE

