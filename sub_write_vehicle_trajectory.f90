      SUBROUTINE WRITE_VEH_TRAJECTORY(l)
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~ Copyright (C) 2002-2011 The Arizona Board of Regents on Behalf of The University of Arizona.  ~~!
!~~ Developed by Yi-Chang Chiu, Ph.D. as par to the DynusT project.                               ~~!
!~~ This program is free software: you can redistribute it and/or modify it                       ~~!
!~~ under the terms of the GNU General Public License version 2 as published by                   ~~!
!~~ the Free Software Foundation.                                                                 ~~!
!~~ This program is distributed in the hope that it will be useful,                               ~~!
!~~ but WITHOUT ANY WARRANTY; without even the implied warranty of                                ~~!
!~~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                                          ~~!
!~~ See the GNU General Public License for more details.                                          ~~!
!~~ You should have received a copy of the GNU General Public License                             ~~!
!~~ along with this program (in the file COPYING).                                                ~~!
!~~ IF not, see <http://www.gnu.org/licenses/>.                                                   ~~!
!~~ Please see http://www.dynust.net for contact information and names                            ~~!
!~~ of individual contributors.                                                                   ~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
USE DYNUST_MAIN_MODULE
USE DYNUST_VEH_MODULE
USE DYNUST_NETWORK_MODULE
USE DYNUST_VEH_PATH_ATT_MODULE

!> > > > > > > > > >                < < < < < < < < < < <
!>>>>>>>>>>>>>>>>>>>CODES START HERE<<<<<<<<<<<<<<<<<<<<<
!> > > > > > > > > >                < < < < < < < < < < <

WRITE(18,*) "#############################################"
WRITE(18,*) "######Vehicles still in the network   #######"
WRITE(18,*) "#############################################"


   DO 1899 j=1,justveh   

      IF(UCDAirOption > 0) CALL UCDavisAirModelOutput(j) ! UTEP generate air quality outputs for UC-Davis

      IF(m_dynust_veh(j)%notin == 1) go to 1899
      
	  IF(i18 > 0.or.reach_converg) CALL WRITE_PRINT_TRAJECTORY(j,0,18,l*xminPerSimInt) !vehicles still in network

      CALL PrintAlt(j,l*xminPerSimInt)      

1899  CONTINUE
            
END SUBROUTINE


SUBROUTINE UCDavisAirModelOutput(j) ! UTEP
USE DYNUST_MAIN_MODULE
USE DYNUST_VEH_MODULE
USE DYNUST_NETWORK_MODULE
USE DYNUST_VEH_PATH_ATT_MODULE
INTEGER j

WRITE(6544,'(i8,i3,5i7,4f8.3)') j,m_dynust_veh(j)%itag,m_dynust_network_node_nde(m_dynust_network_arc_nde(m_dynust_last_stand(j)%isec)%idnod)%IntoOutNodeNum,m_dynust_last_stand(j)%jdest,m_dynust_network_node_nde(m_dynust_network_arc_nde(m_dynust_last_stand(j)%isec)%iunod)%IntoOutNodeNum,m_dynust_network_node_nde(m_dynust_network_arc_nde(m_dynust_last_stand(j)%isec)%idnod)%IntoOutNodeNum,m_dynust_network_node_nde(nint(vehatt_Value(j,vehatt_P_Size(j)-1,1)))%IntoOutNodeNum,m_dynust_veh(j)%ttilnow,(vehatt_Value(j,vehatt_A_Size(j)-1,2)/AttScale),m_dynust_veh(j)%distans,m_dynust_last_stand(j)%stime

END SUBROUTINE