; mark_description "Intel(R) Visual Fortran Compiler XE for applications running on IA-32, Version 13.1.1.171 Build 20130313";
; mark_description "-S";
	.686P
 	.387
	OPTION DOTNAME
	ASSUME	CS:FLAT,DS:FLAT,SS:FLAT
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
TXTST0:
; -- Begin  _DYNUST_TDSP_MODULE$
; mark_begin;
IF @Version GE 800
  .MMX
ELSEIF @Version GE 612
  .MMX
  MMWORD TEXTEQU <QWORD>
ENDIF
IF @Version GE 800
  .XMM
ELSEIF @Version GE 614
  .XMM
  XMMWORD TEXTEQU <OWORD>
ENDIF
       ALIGN     16
	PUBLIC _DYNUST_TDSP_MODULE$
_DYNUST_TDSP_MODULE$	PROC NEAR 
.B1.1:                          ; Preds .B1.0
        ret                                                     ;1.8
        ALIGN     16
                                ; LOE
; mark_end;
_DYNUST_TDSP_MODULE$ ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_TDSP_MODULE$
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
	PUBLIC _DYNUST_TDSP_MODULE_mp_TTMARGINAL
_DYNUST_TDSP_MODULE_mp_TTMARGINAL	DD	0
	DD	0
	DD	0
	DD	128
	DD	3
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD 1 DUP (0H)	; pad
	PUBLIC _DYNUST_TDSP_MODULE_mp_TTPENALTY
_DYNUST_TDSP_MODULE_mp_TTPENALTY	DD	0
	DD	0
	DD	0
	DD	128
	DD	3
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD 1 DUP (0H)	; pad
	PUBLIC _DYNUST_TDSP_MODULE_mp_TTIME
_DYNUST_TDSP_MODULE_mp_TTIME	DD	0
	DD	0
	DD	0
	DD	128
	DD	2
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
_DATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
	INCLUDELIB <ifconsol>
	INCLUDELIB <libifcoremt>
	INCLUDELIB <libifport>
	INCLUDELIB <libmmt>
	INCLUDELIB <LIBCMT>
	INCLUDELIB <libirc>
	INCLUDELIB <svml_dispmt>
	INCLUDELIB <OLDNAMES>
	END
