MODULE LINK_LIST
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~ Copyright (C) 2002-2011 The Arizona Board of Regents on Behalf of The University of Arizona.  ~~!
!~~ Developed by Yi-Chang Chiu, Ph.D. as par to the DynusT project.                               ~~!
!~~ This program is free software: you can redistribute it and/or modify it                       ~~!
!~~ under the terms of the GNU General Public License version 2 as published by                   ~~!
!~~ the Free Software Foundation.                                                                 ~~!
!~~ This program is distributed in the hope that it will be useful,                               ~~!
!~~ but WITHOUT ANY WARRANTY; without even the implied warranty of                                ~~!
!~~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                                          ~~!
!~~ See the GNU General Public License for more details.                                          ~~!
!~~ You should have received a copy of the GNU General Public License                             ~~!
!~~ along with this program (in the file COPYING).                                                ~~!
!~~ IF not, see <http://www.gnu.org/licenses/>.                                                   ~~!
!~~ Please see http://www.dynust.net for contact information and names                            ~~!
!~~ of individual contributors.                                                                   ~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!

    
    TYPE, PRIVATE :: UNIT_TYPE
        TYPE (Node_Type), POINTER :: Pr_Next => Null( )
    END TYPE UNIT_TYPE
    
    TYPE, PRIVATE :: Node_Type
        REAL :: Label
        INTEGER :: LinkID
        TYPE (UNIT_TYPE) :: BOX
    END TYPE Node_Type
    
    TYPE, public :: Queue_Type_LinkList
        PRIVATE
        TYPE (UNIT_TYPE) :: Root
        TYPE (UNIT_TYPE), POINTER :: Pr_Tail => null( )
    END TYPE Queue_Type_LinkList

CONTAINS

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
SUBROUTINE Create_Queue_LinkList( InternQ )
    TYPE (Queue_Type_LinkList), target, INTENT(in out) :: InternQ
    ! start SUBROUTINE Create_Queue
    InternQ%Pr_Tail =>InternQ%Root
RETURN
END SUBROUTINE Create_Queue_LinkList

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
SUBROUTINE Destroy_Queue_LinkList( InternQ )
    TYPE (Queue_Type_LinkList), INTENT(in out) :: InternQ
    REAL :: Label
    INTEGER :: LinkID
    ! start SUBROUTINE Destroy_Queue
    do while (.not. Is_Empty_LinkList( InternQ ))
        CALL Disqueue_LinkList( InternQ, LinkID, Label )
        !PRINT *, " Left on Queue: ", Label
    END do
RETURN
END SUBROUTINE Destroy_Queue_LinkList

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
SUBROUTINE Enqueue_LinkList( InternQ, LinkID, Label )
    TYPE (Queue_Type_LinkList), INTENT(in out) :: InternQ
    REAL, INTENT(in) :: label
    INTEGER, INTENT(in) :: LinkID
    TYPE (Node_Type), POINTER :: Temp_NP
    ! start SUBROUTINE Enqueue
    ALLOCATE( Temp_NP )
    Temp_NP % Label = Label
    Temp_NP % LinkID = LinkID
    InternQ % Pr_Tail % Pr_Next => Temp_NP ! Link new node into list.
    InternQ % Pr_Tail => Temp_NP % BOX ! New node becomes new tail.
    !NULLIFY( InternQ % Pr_Tail % Pr_Next )
    InternQ % Pr_Tail % Pr_Next => Null()
RETURN
END SUBROUTINE Enqueue_LinkList

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
SUBROUTINE Disqueue_LinkList( InternQ, LinkID, Label )
    TYPE (Queue_Type_LinkList), target, INTENT(in out) :: InternQ
    INTEGER, INTENT(out) :: LinkID
    REAL, INTENT(out) :: Label
    TYPE (Node_Type), POINTER :: Temp_NP
    ! start SUBROUTINE Dequeue
    IF (Is_Empty_LinkList( InternQ )) then
        PRINT *, " Attempt to pop from empty stack. "
        Item = 0.0
    ELSE
        Temp_NP => InternQ % Root % Pr_Next
        Label = Temp_NP % Label
        LinkID = Temp_NP % LinkID
        InternQ % Root = Temp_NP % BOX
        DEALLOCATE( Temp_NP )
        IF (Is_Empty_LinkList( InternQ )) InternQ % Pr_Tail => InternQ % Root
    ENDIF
RETURN
END SUBROUTINE Disqueue_LinkList

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
SUBROUTINE Disqueue_Target( InternQ, Key, Found )
    TYPE (Queue_Type_LinkList), target, INTENT(in out) :: InternQ
    Integer :: Key
    TYPE (Node_Type), POINTER :: Temp_NP1, Temp_NP2
    LOGICAL, INTENT(OUT) :: Found
    REAL :: LSLabel
    
    Found = .FALSE.
    IF (Is_Empty_LinkList( InternQ )) then
        PRINT *, " Attempt to pop from empty stack. "
        Item = 0.0
    ELSE
        ! Find the Item in the list
        Temp_NP1 => InternQ % Root % Pr_Next
        IF(Key == Temp_NP1%LinkID) THEN
            Found = .TRUE.
            CALL Disqueue_LinkList( InternQ, Key, LSLabel )
        ELSE
            DO WHILE ( associated (Temp_NP1 % BOX % Pr_Next)) 
                Temp_NP2 => Temp_NP1
                Temp_NP1 => Temp_NP1 % Box % Pr_Next
                IF(Key == Temp_NP1%LinkID) THEN
                    Found = .TRUE.
                    Temp_NP2 % BOX % Pr_Next => Temp_NP1 % Box % Pr_Next
                    DEALLOCATE( Temp_NP1 )
                    EXIT
                ENDIF
            ENDDO
            IF (Is_Empty_LinkList( InternQ )) InternQ % Pr_Tail => InternQ % Root
        ENDIF
    ENDIF
RETURN
END SUBROUTINE

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
SUBROUTINE PrintQueue_LinkList( InternQ )
    USE DYNUST_NETWORK_MODULE
    TYPE (Queue_Type_LinkList), target, INTENT(in out) :: InternQ
    TYPE (Node_Type), POINTER :: Temp_NP
    REAL :: Item
    OPEN(FILE = "QUEUEOUT.DAT", UNIT = 1, STATUS = "UNKNOWN")
    WRITE(1,*) "Query"
    Temp_NP => InternQ % Root % Pr_Next
    Item = Temp_NP % Label
    !Print *, "Item in the list", Item
    DO WHILE ( associated (Temp_NP%BOX%Pr_Next)) 
        Temp_NP => Temp_NP % Box % Pr_Next
        WRITE(1,'("Item in the list",i7,f10.5)') m_dynust_network_node_nde(m_dynust_network_arc_nde(Temp_NP%LinkID)%idnod)%IntoOutNodeNum, Temp_NP%Label
    ENDDO
    
END SUBROUTINE PrintQueue_LinkList

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
SUBROUTINE Enqueue_Ordered_LinkList( InternQ, LinkID, Label )
    TYPE (Queue_Type_LinkList), INTENT(in out) :: InternQ
    REAL, INTENT(in) :: Label
    INTEGER, INTENT(in) :: LinkID
    REAL :: Item1 = 0, Item2 = 0
    TYPE (Node_Type), POINTER :: Temp_NP1, Temp_NP2, Temp_NP3, Temp_NP4
    LOGICAL :: Inserted 
    Inserted = .FALSE.
    
    IF (.NOT. Is_Empty_LinkList( InternQ )) then
        Inserted = .FALSE.
        Temp_NP1 => InternQ % Root % Pr_Next
        Temp_NP2 => Temp_NP1
        IF(Label <= Temp_NP1 % Label) THEN ! Item is less than the first item
            ALLOCATE( Temp_NP3 )
            Temp_NP3 % Label = Label
            Temp_NP3 % LinkID = LinkID
            InternQ % Root % Pr_Next => Temp_NP3
            Temp_NP3 % Box % Pr_Next => Temp_NP2
            Inserted = .TRUE.
        ELSE
            DO WHILE ( associated (Temp_NP2 % Box % Pr_Next))
                Item1 = Temp_NP1 % Label
                Temp_NP2 => Temp_NP1 % Box % Pr_Next
                IF( associated (Temp_NP2)) THEN
                    Item2 = Temp_NP2 % Label
                    IF((Item1 < Label .AND. Label <= Item2)) THEN ! Insert
                        ALLOCATE( Temp_NP3 )
                        Temp_NP3 % Label = Label
                        Temp_NP3 % LinkID = LInkID
                        Temp_NP1 % Box % Pr_Next => Temp_NP3 ! previous item linked to the new item
                        Temp_NP3 % Box % Pr_Next => Temp_NP2 ! new item linked to next item
                        Inserted = .TRUE.
                        EXIT
                    ENDIF
                ELSE
                    EXIT
                ENDIF
                Temp_NP1 => Temp_NP1 % Box % Pr_Next
            ENDDO
        ENDIF
    ENDIF
    IF(.NOT.Inserted.OR.Is_Empty_LinkList( InternQ )) THEN ! insert to the tail
        CALL Enqueue_LinkList( InternQ, LinkID, Label )
    ENDIF
RETURN
END SUBROUTINE Enqueue_Ordered_LinkList

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
pure function Is_Empty_LinkList( InternQ ) result( X )
    TYPE (Queue_Type_LinkList), INTENT(in) :: InternQ
    logical :: X
    ! start function Is_Empty
    X = (.not. associated (InternQ % Root % Pr_Next))
RETURN
END function Is_Empty_LinkList

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
pure function Is_Full_LinkList( InternQ ) result( X )
    TYPE (Queue_Type_LinkList), INTENT(in) :: InternQ
    logical :: X
    ! start function Is_Full
    X = .false.
RETURN
END function Is_Full_LinkList

END MODULE LINK_LIST
