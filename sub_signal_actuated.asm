; mark_description "Intel(R) Visual Fortran Compiler XE for applications running on IA-32, Version 13.1.1.171 Build 20130313";
; mark_description "-S";
	.686P
 	.387
	OPTION DOTNAME
	ASSUME	CS:FLAT,DS:FLAT,SS:FLAT
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
TXTST0:
; -- Begin  _SIGNAL_ACTUATED
; mark_begin;
IF @Version GE 800
  .MMX
ELSEIF @Version GE 612
  .MMX
  MMWORD TEXTEQU <QWORD>
ENDIF
IF @Version GE 800
  .XMM
ELSEIF @Version GE 614
  .XMM
  XMMWORD TEXTEQU <OWORD>
ENDIF
       ALIGN     16
	PUBLIC _SIGNAL_ACTUATED
_SIGNAL_ACTUATED	PROC NEAR 
; parameter 1: 8 + ebp
; parameter 2: 12 + ebp
.B1.1:                          ; Preds .B1.0
        push      ebp                                           ;1.18
        mov       ebp, esp                                      ;1.18
        and       esp, -16                                      ;1.18
        push      esi                                           ;1.18
        push      edi                                           ;1.18
        push      ebx                                           ;1.18
        sub       esp, 100                                      ;1.18
        movss     xmm3, DWORD PTR [_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT] ;39.18
        mulss     xmm3, DWORD PTR [_2il0floatpacket.2]          ;39.18
        movss     xmm0, DWORD PTR [_2il0floatpacket.3]          ;39.18
        andps     xmm0, xmm3                                    ;39.18
        pxor      xmm3, xmm0                                    ;39.18
        movss     xmm1, DWORD PTR [_2il0floatpacket.4]          ;39.18
        movaps    xmm2, xmm3                                    ;39.18
        movss     xmm4, DWORD PTR [_2il0floatpacket.5]          ;39.18
        cmpltss   xmm2, xmm1                                    ;39.18
        andps     xmm1, xmm2                                    ;39.18
        movaps    xmm2, xmm3                                    ;39.18
        movaps    xmm6, xmm4                                    ;39.18
        addss     xmm2, xmm1                                    ;39.18
        addss     xmm6, xmm4                                    ;39.18
        subss     xmm2, xmm1                                    ;39.18
        movaps    xmm7, xmm2                                    ;39.18
        mov       ebx, DWORD PTR [8+ebp]                        ;1.18
        subss     xmm7, xmm3                                    ;39.18
        movaps    xmm5, xmm7                                    ;39.18
        cmpless   xmm7, DWORD PTR [_2il0floatpacket.6]          ;39.18
        cmpnless  xmm5, xmm4                                    ;39.18
        andps     xmm5, xmm6                                    ;39.18
        andps     xmm7, xmm6                                    ;39.18
        mov       edx, DWORD PTR [ebx]                          ;52.20
        subss     xmm2, xmm5                                    ;39.18
        mov       esi, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY] ;52.7
        lea       edx, DWORD PTR [edx+edx*2]                    ;52.7
        shl       edx, 4                                        ;52.7
        addss     xmm2, xmm7                                    ;39.18
        orps      xmm2, xmm0                                    ;39.18
        lea       edi, DWORD PTR [esi+edx]                      ;52.7
        cvtss2si  ecx, xmm2                                     ;39.18
        mov       BYTE PTR [_DYNUST_SIGNAL_MODULE_mp_GREENEXT], cl ;39.7
        mov       ecx, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY+32] ;52.7
        imul      eax, ecx, -48                                 ;52.7
        movsx     eax, BYTE PTR [1+eax+edi]                     ;52.20
        test      eax, eax                                      ;52.7
        mov       DWORD PTR [84+esp], 1                         ;52.7
        jle       .B1.19        ; Prob 2%                       ;52.7
                                ; LOE eax edx ecx ebx esi
.B1.2:                          ; Preds .B1.1
        xor       ecx, ecx                                      ;
        lea       edx, DWORD PTR [84+esp]                       ;
        mov       DWORD PTR [60+esp], ecx                       ;
        mov       DWORD PTR [esp], eax                          ;
                                ; LOE
.B1.3:                          ; Preds .B1.17 .B1.2
        push      OFFSET FLAT: __NLITPACK_0.0.1                 ;53.25
        lea       eax, DWORD PTR [88+esp]                       ;53.25
        push      eax                                           ;53.25
        push      DWORD PTR [8+ebp]                             ;53.25
        call      _DYNUST_SIGNAL_MODULE_mp_GETSIGNALPAR         ;53.25
                                ; LOE eax
.B1.92:                         ; Preds .B1.3
        mov       DWORD PTR [48+esp], eax                       ;53.25
                                ; LOE
.B1.4:                          ; Preds .B1.92
        push      OFFSET FLAT: __NLITPACK_1.0.1                 ;54.21
        lea       eax, DWORD PTR [100+esp]                      ;54.21
        push      eax                                           ;54.21
        push      DWORD PTR [8+ebp]                             ;54.21
        call      _DYNUST_SIGNAL_MODULE_mp_GETSIGNALPAR         ;54.21
                                ; LOE eax
.B1.93:                         ; Preds .B1.4
        add       esp, 24                                       ;54.21
        mov       DWORD PTR [40+esp], eax                       ;54.21
                                ; LOE
.B1.5:                          ; Preds .B1.93
        mov       eax, DWORD PTR [84+esp]                       ;74.7
        mov       DWORD PTR [4+esp], eax                        ;74.7
        cmp       DWORD PTR [40+esp], 0                         ;54.9
        jle       .B1.17        ; Prob 2%                       ;54.9
                                ; LOE
.B1.6:                          ; Preds .B1.5
        mov       edi, DWORD PTR [8+ebp]                        ;55.22
        movss     xmm0, DWORD PTR [_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT] ;63.13
        mulss     xmm0, DWORD PTR [_2il0floatpacket.2]          ;63.68
        mov       ebx, DWORD PTR [edi]                          ;55.22
        sub       ebx, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY+32] ;
        cvttss2si esi, xmm0                                     ;63.13
        mov       ecx, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY] ;55.11
        lea       edi, DWORD PTR [ebx+ebx*2]                    ;
        shl       edi, 4                                        ;
        mov       DWORD PTR [16+esp], esi                       ;63.13
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+44] ;63.13
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+40] ;63.13
        mov       esi, DWORD PTR [44+ecx+edi]                   ;55.22
        neg       esi                                           ;
        add       esi, DWORD PTR [4+esp]                        ;
        imul      esi, DWORD PTR [40+ecx+edi]                   ;
        imul      edx, eax                                      ;
        mov       edi, DWORD PTR [12+ecx+edi]                   ;55.22
        mov       DWORD PTR [20+esp], eax                       ;63.13
        imul      eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+32], -28 ;
        mov       ecx, DWORD PTR [12+edi+esi]                   ;55.22
        mov       ebx, DWORD PTR [44+edi+esi]                   ;55.22
        mov       esi, DWORD PTR [40+edi+esi]                   ;55.22
        imul      ebx, esi                                      ;
        imul      edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32], -900 ;
        sub       ecx, ebx                                      ;
        imul      ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32], -152 ;
        add       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;
        mov       DWORD PTR [24+esp], edi                       ;
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE] ;
        sub       edi, edx                                      ;
        add       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;
        add       edi, eax                                      ;
        mov       DWORD PTR [32+esp], 1                         ;
        mov       DWORD PTR [28+esp], esi                       ;
        mov       DWORD PTR [12+esp], edi                       ;
        mov       DWORD PTR [52+esp], ebx                       ;
        mov       DWORD PTR [48+esp], ecx                       ;
        mov       DWORD PTR [44+esp], esi                       ;
                                ; LOE
.B1.7:                          ; Preds .B1.15 .B1.6
        mov       edx, DWORD PTR [48+esp]                       ;55.11
        mov       eax, DWORD PTR [28+esp]                       ;55.11
        mov       ebx, DWORD PTR [52+esp]                       ;56.22
        mov       ecx, DWORD PTR [eax+edx]                      ;55.11
        imul      eax, ecx, 152                                 ;55.11
        mov       DWORD PTR [56+esp], ecx                       ;55.11
        movsx     ecx, BYTE PTR [32+eax+ebx]                    ;56.22
        test      ecx, ecx                                      ;56.11
        jle       .B1.12        ; Prob 2%                       ;56.11
                                ; LOE eax ecx ebx bl bh
.B1.8:                          ; Preds .B1.7
        mov       edi, ebx                                      ;57.17
        mov       ebx, 1                                        ;
        mov       esi, DWORD PTR [64+eax+edi]                   ;57.17
        mov       edx, DWORD PTR [68+eax+edi]                   ;57.17
        imul      edx, esi                                      ;
        mov       eax, DWORD PTR [36+eax+edi]                   ;57.17
        sub       eax, edx                                      ;
        imul      edx, DWORD PTR [56+esp], 900                  ;55.11
        mov       edi, DWORD PTR [24+esp]                       ;57.67
        movsx     edx, BYTE PTR [428+edx+edi]                   ;57.67
        mov       edi, esi                                      ;57.67
                                ; LOE eax edx ecx ebx esi edi
.B1.9:                          ; Preds .B1.10 .B1.8
        cmp       edx, DWORD PTR [edi+eax]                      ;57.64
        je        .B1.89        ; Prob 20%                      ;57.64
                                ; LOE eax edx ecx ebx esi edi
.B1.10:                         ; Preds .B1.9
        inc       ebx                                           ;61.11
        add       edi, esi                                      ;61.11
        cmp       ebx, ecx                                      ;61.11
        jle       .B1.9         ; Prob 82%                      ;61.11
                                ; LOE eax edx ecx ebx esi edi
.B1.12:                         ; Preds .B1.10 .B1.89 .B1.7
        cmp       DWORD PTR [36+esp], 0                         ;62.28
        jle       .B1.83        ; Prob 16%                      ;62.28
                                ; LOE
.B1.13:                         ; Preds .B1.12
        cmp       DWORD PTR [60+esp], 0                         ;62.43
        jle       .B1.83        ; Prob 16%                      ;62.43
                                ; LOE
.B1.14:                         ; Preds .B1.13
        mov       ebx, DWORD PTR [56+esp]                       ;55.11
        mov       ecx, DWORD PTR [20+esp]                       ;63.13
        imul      ecx, DWORD PTR [60+esp]                       ;63.13
        mov       edx, DWORD PTR [16+esp]                       ;63.13
        lea       eax, DWORD PTR [ebx*4]                        ;55.11
        shl       ebx, 5                                        ;55.11
        sub       ebx, eax                                      ;55.11
        add       ebx, DWORD PTR [12+esp]                       ;63.13
        mov       DWORD PTR [4+ecx+ebx], edx                    ;63.13
                                ; LOE
.B1.15:                         ; Preds .B1.87 .B1.83 .B1.14
        mov       eax, DWORD PTR [28+esp]                       ;73.9
        mov       edx, DWORD PTR [32+esp]                       ;73.9
        inc       edx                                           ;73.9
        add       eax, DWORD PTR [44+esp]                       ;73.9
        mov       DWORD PTR [28+esp], eax                       ;73.9
        mov       DWORD PTR [32+esp], edx                       ;73.9
        cmp       edx, DWORD PTR [40+esp]                       ;73.9
        jle       .B1.7         ; Prob 82%                      ;73.9
                                ; LOE
.B1.17:                         ; Preds .B1.15 .B1.5
        DB        102                                           ;74.7
        DB        144                                           ;74.7
        mov       eax, DWORD PTR [4+esp]                        ;74.7
        inc       eax                                           ;74.7
        mov       DWORD PTR [84+esp], eax                       ;74.7
        cmp       eax, DWORD PTR [esp]                          ;74.7
        jle       .B1.3         ; Prob 82%                      ;74.7
                                ; LOE
.B1.18:                         ; Preds .B1.17
        mov       ebx, DWORD PTR [8+ebp]                        ;
        mov       eax, DWORD PTR [ebx]                          ;76.25
        mov       esi, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY] ;76.7
        mov       ecx, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY+32] ;76.7
        lea       edx, DWORD PTR [eax+eax*2]                    ;
        shl       edx, 4                                        ;
                                ; LOE edx ecx ebx esi
.B1.19:                         ; Preds .B1.18 .B1.1
        imul      eax, ecx, -48                                 ;76.7
        add       esi, edx                                      ;76.7
        movsx     edx, BYTE PTR [8+eax+esi]                     ;76.25
        lea       ecx, DWORD PTR [72+esp]                       ;78.23
        mov       DWORD PTR [72+esp], edx                       ;76.7
        push      OFFSET FLAT: __NLITPACK_0.0.1                 ;78.23
        push      ecx                                           ;78.23
        push      ebx                                           ;78.23
        call      _DYNUST_SIGNAL_MODULE_mp_GETSIGNALPAR         ;78.23
                                ; LOE eax ebx
.B1.94:                         ; Preds .B1.19
        mov       edi, eax                                      ;78.23
                                ; LOE ebx edi
.B1.20:                         ; Preds .B1.94
        push      OFFSET FLAT: __NLITPACK_2.0.1                 ;79.23
        lea       eax, DWORD PTR [88+esp]                       ;79.23
        push      eax                                           ;79.23
        push      ebx                                           ;79.23
        call      _DYNUST_SIGNAL_MODULE_mp_GETSIGNALPAR         ;79.23
                                ; LOE eax ebx edi
.B1.95:                         ; Preds .B1.20
        mov       DWORD PTR [104+esp], eax                      ;79.23
                                ; LOE ebx edi
.B1.21:                         ; Preds .B1.95
        movss     xmm3, DWORD PTR [_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT] ;80.27
        mulss     xmm3, DWORD PTR [_2il0floatpacket.2]          ;80.27
        movss     xmm0, DWORD PTR [_2il0floatpacket.3]          ;80.27
        andps     xmm0, xmm3                                    ;80.27
        pxor      xmm3, xmm0                                    ;80.27
        movss     xmm1, DWORD PTR [_2il0floatpacket.4]          ;80.27
        movaps    xmm2, xmm3                                    ;80.27
        movss     xmm4, DWORD PTR [_2il0floatpacket.5]          ;80.27
        cmpltss   xmm2, xmm1                                    ;80.27
        andps     xmm1, xmm2                                    ;80.27
        movaps    xmm2, xmm3                                    ;80.27
        movaps    xmm6, xmm4                                    ;80.27
        addss     xmm2, xmm1                                    ;80.27
        addss     xmm6, xmm4                                    ;80.27
        subss     xmm2, xmm1                                    ;80.27
        movaps    xmm7, xmm2                                    ;80.27
        subss     xmm7, xmm3                                    ;80.27
        movaps    xmm5, xmm7                                    ;80.27
        cmpless   xmm7, DWORD PTR [_2il0floatpacket.6]          ;80.27
        cmpnless  xmm5, xmm4                                    ;80.27
        andps     xmm5, xmm6                                    ;80.27
        andps     xmm7, xmm6                                    ;80.27
        subss     xmm2, xmm5                                    ;80.27
        addss     xmm2, xmm7                                    ;80.27
        orps      xmm2, xmm0                                    ;80.27
        movss     DWORD PTR [24+esp], xmm2                      ;80.27
        push      OFFSET FLAT: __NLITPACK_3.0.1                 ;80.50
        lea       eax, DWORD PTR [100+esp]                      ;80.50
        push      eax                                           ;80.50
        push      ebx                                           ;80.50
        call      _DYNUST_SIGNAL_MODULE_mp_GETSIGNALPAR         ;80.50
                                ; LOE eax ebx edi
.B1.96:                         ; Preds .B1.21
        mov       esi, eax                                      ;80.50
                                ; LOE ebx esi edi
.B1.22:                         ; Preds .B1.96
        cvtss2si  eax, DWORD PTR [36+esp]                       ;80.27
        push      OFFSET FLAT: __NLITPACK_4.0.1                 ;81.23
        cmp       eax, esi                                      ;80.7
        lea       edx, DWORD PTR [112+esp]                      ;81.23
        push      edx                                           ;81.23
        push      ebx                                           ;81.23
        cmovge    esi, eax                                      ;80.7
        call      _DYNUST_SIGNAL_MODULE_mp_GETSIGNALPAR         ;81.23
                                ; LOE eax ebx esi edi
.B1.97:                         ; Preds .B1.22
        add       esp, 48                                       ;81.23
        mov       DWORD PTR [44+esp], eax                       ;81.23
                                ; LOE ebx esi edi
.B1.23:                         ; Preds .B1.97
        cmp       DWORD PTR [44+esp], 0                         ;82.15
        jl        .B1.79        ; Prob 16%                      ;82.15
                                ; LOE ebx esi edi
.B1.24:                         ; Preds .B1.118 .B1.23
        push      OFFSET FLAT: __NLITPACK_1.0.1                 ;87.23
        lea       eax, DWORD PTR [76+esp]                       ;87.23
        push      eax                                           ;87.23
        push      ebx                                           ;87.23
        call      _DYNUST_SIGNAL_MODULE_mp_GETSIGNALPAR         ;87.23
                                ; LOE eax ebx esi edi
.B1.25:                         ; Preds .B1.24
        mov       DWORD PTR [88+esp], eax                       ;87.7
        lea       eax, DWORD PTR [84+esp]                       ;89.21
        push      eax                                           ;89.21
        push      ebx                                           ;89.21
        call      _DYNUST_SIGNAL_MODULE_mp_FINDTIMENEEDED       ;89.21
                                ; LOE eax ebx esi edi
.B1.99:                         ; Preds .B1.25
        add       esp, 20                                       ;89.21
                                ; LOE eax ebx esi edi
.B1.26:                         ; Preds .B1.99
        cmp       edi, 1                                        ;91.17
        jne       .B1.45        ; Prob 67%                      ;91.17
                                ; LOE eax ebx esi edi
.B1.27:                         ; Preds .B1.26
        movsx     edx, BYTE PTR [_DYNUST_SIGNAL_MODULE_mp_GREENEXT] ;93.16
        add       edx, DWORD PTR [80+esp]                       ;93.15
        cmp       esi, edx                                      ;93.25
        jge       .B1.43        ; Prob 22%                      ;93.25
                                ; LOE eax edx ebx esi
.B1.28:                         ; Preds .B1.27
        cmp       edx, DWORD PTR [44+esp]                       ;96.59
        jg        .B1.30        ; Prob 50%                      ;96.59
                                ; LOE eax ebx esi
.B1.29:                         ; Preds .B1.28
        test      al, 1                                         ;96.71
        jne       .B1.77        ; Prob 15%                      ;96.71
                                ; LOE ebx esi
.B1.30:                         ; Preds .B1.29 .B1.28
        movss     xmm3, DWORD PTR [_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT] ;101.28
        mov       eax, esi                                      ;101.28
        mulss     xmm3, DWORD PTR [_2il0floatpacket.2]          ;101.28
        movss     xmm0, DWORD PTR [_2il0floatpacket.3]          ;101.28
        andps     xmm0, xmm3                                    ;101.28
        pxor      xmm3, xmm0                                    ;101.28
        movss     xmm1, DWORD PTR [_2il0floatpacket.4]          ;101.28
        movaps    xmm2, xmm3                                    ;101.28
        movss     xmm4, DWORD PTR [_2il0floatpacket.5]          ;101.28
        cmpltss   xmm2, xmm1                                    ;101.28
        andps     xmm1, xmm2                                    ;101.28
        movaps    xmm2, xmm3                                    ;101.28
        movaps    xmm6, xmm4                                    ;101.28
        addss     xmm2, xmm1                                    ;101.28
        addss     xmm6, xmm4                                    ;101.28
        subss     xmm2, xmm1                                    ;101.28
        movaps    xmm7, xmm2                                    ;101.28
        sub       eax, DWORD PTR [80+esp]                       ;101.28
        subss     xmm7, xmm3                                    ;101.28
        movaps    xmm5, xmm7                                    ;101.28
        cmpless   xmm7, DWORD PTR [_2il0floatpacket.6]          ;101.28
        cmpnless  xmm5, xmm4                                    ;101.28
        andps     xmm5, xmm6                                    ;101.28
        andps     xmm7, xmm6                                    ;101.28
        subss     xmm2, xmm5                                    ;101.28
        addss     xmm2, xmm7                                    ;101.28
        orps      xmm2, xmm0                                    ;101.28
        cvtss2si  edx, xmm2                                     ;101.56
        cmp       edx, eax                                      ;101.13
        cmovl     eax, edx                                      ;101.13
        mov       BYTE PTR [68+esp], al                         ;101.13
        movsx     eax, al                                       ;102.16
        test      eax, eax                                      ;102.25
        jl        .B1.32        ; Prob 16%                      ;102.25
                                ; LOE eax edx ebx esi
.B1.31:                         ; Preds .B1.30
        cmp       eax, edx                                      ;102.41
        jle       .B1.33        ; Prob 50%                      ;102.41
                                ; LOE ebx esi
.B1.32:                         ; Preds .B1.31 .B1.30
        mov       DWORD PTR [esp], 0                            ;103.16
        lea       edx, DWORD PTR [esp]                          ;103.16
        mov       DWORD PTR [32+esp], 17                        ;103.16
        lea       eax, DWORD PTR [32+esp]                       ;103.16
        mov       DWORD PTR [36+esp], OFFSET FLAT: __STRLITPACK_2 ;103.16
        push      32                                            ;103.16
        push      eax                                           ;103.16
        push      OFFSET FLAT: __STRLITPACK_11.0.1              ;103.16
        push      -2088435968                                   ;103.16
        push      -1                                            ;103.16
        push      edx                                           ;103.16
        call      _for_write_seq_lis                            ;103.16
                                ; LOE ebx esi
.B1.100:                        ; Preds .B1.32
        add       esp, 24                                       ;103.16
                                ; LOE ebx esi
.B1.33:                         ; Preds .B1.100 .B1.31
        lea       eax, DWORD PTR [68+esp]                       ;105.18
        lea       edx, DWORD PTR [76+esp]                       ;105.18
        push      eax                                           ;105.18
        push      edx                                           ;105.18
        lea       ecx, DWORD PTR [80+esp]                       ;105.18
        push      ecx                                           ;105.18
        push      ebx                                           ;105.18
        call      _DYNUST_SIGNAL_MODULE_mp_ASSIGNGREEN          ;105.18
                                ; LOE ebx esi
.B1.101:                        ; Preds .B1.33
        add       esp, 16                                       ;105.18
                                ; LOE ebx esi
.B1.34:                         ; Preds .B1.101
        mov       DWORD PTR [40+esp], esi                       ;
        mov       esi, DWORD PTR [ebx]                          ;106.13
        mov       eax, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY+32] ;106.13
        mov       edx, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY] ;106.13
        lea       edi, DWORD PTR [eax+eax*2]                    ;106.13
        shl       edi, 4                                        ;106.13
        lea       eax, DWORD PTR [esi+esi*2]                    ;106.13
        shl       eax, 4                                        ;106.13
        add       eax, edx                                      ;106.13
        sub       eax, edi                                      ;106.13
        mov       DWORD PTR [44+esp], edi                       ;106.13
        mov       ecx, DWORD PTR [44+eax]                       ;106.13
        neg       ecx                                           ;106.13
        add       ecx, DWORD PTR [72+esp]                       ;106.13
        imul      ecx, DWORD PTR [40+eax]                       ;106.13
        mov       esi, DWORD PTR [12+eax]                       ;106.13
        xor       eax, eax                                      ;106.13
        mov       WORD PTR [8+esi+ecx], ax                      ;106.13
        mov       ecx, DWORD PTR [ebx]                          ;107.4
        lea       esi, DWORD PTR [ecx+ecx*2]                    ;107.4
        shl       esi, 4                                        ;107.4
        add       esi, edx                                      ;107.4
        sub       esi, edi                                      ;107.4
        mov       edi, DWORD PTR [72+esp]                       ;107.4
        mov       ecx, DWORD PTR [44+esi]                       ;107.4
        neg       ecx                                           ;107.4
        add       ecx, edi                                      ;107.4
        imul      ecx, DWORD PTR [40+esi]                       ;107.4
        mov       esi, DWORD PTR [12+esi]                       ;107.4
        mov       BYTE PTR [10+esi+ecx], al                     ;107.4
        mov       eax, DWORD PTR [ebx]                          ;109.26
        mov       esi, DWORD PTR [40+esp]                       ;109.23
        lea       ecx, DWORD PTR [eax+eax*2]                    ;109.23
        shl       ecx, 4                                        ;109.23
        add       edx, ecx                                      ;109.23
        sub       edx, DWORD PTR [44+esp]                       ;109.23
        movsx     eax, BYTE PTR [1+edx]                         ;109.26
        cmp       edi, eax                                      ;109.23
        jne       .B1.36        ; Prob 50%                      ;109.23
                                ; LOE edx ebx esi
.B1.35:                         ; Preds .B1.34
        mov       eax, 1                                        ;110.16
        jmp       .B1.37        ; Prob 100%                     ;110.16
                                ; LOE eax edx ebx esi
.B1.36:                         ; Preds .B1.34
        movzx     eax, BYTE PTR [8+edx]                         ;112.63
        inc       eax                                           ;112.108
                                ; LOE eax edx ebx esi
.B1.37:                         ; Preds .B1.35 .B1.36
        movss     xmm3, DWORD PTR [_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT] ;118.28
        mulss     xmm3, DWORD PTR [_2il0floatpacket.2]          ;118.28
        movss     xmm0, DWORD PTR [_2il0floatpacket.3]          ;118.28
        andps     xmm0, xmm3                                    ;118.28
        pxor      xmm3, xmm0                                    ;118.28
        movss     xmm1, DWORD PTR [_2il0floatpacket.4]          ;118.28
        movaps    xmm2, xmm3                                    ;118.28
        movss     xmm4, DWORD PTR [_2il0floatpacket.5]          ;118.28
        cmpltss   xmm2, xmm1                                    ;118.28
        andps     xmm1, xmm2                                    ;118.28
        movaps    xmm2, xmm3                                    ;118.28
        movaps    xmm6, xmm4                                    ;118.28
        addss     xmm2, xmm1                                    ;118.28
        addss     xmm6, xmm4                                    ;118.28
        subss     xmm2, xmm1                                    ;118.28
        movaps    xmm7, xmm2                                    ;118.28
        mov       BYTE PTR [8+edx], al                          ;110.16
        subss     xmm7, xmm3                                    ;118.28
        movaps    xmm5, xmm7                                    ;118.28
        cmpless   xmm7, DWORD PTR [_2il0floatpacket.6]          ;118.28
        cmpnless  xmm5, xmm4                                    ;118.28
        andps     xmm5, xmm6                                    ;118.28
        andps     xmm7, xmm6                                    ;118.28
        movsx     edi, al                                       ;115.4
        subss     xmm2, xmm5                                    ;118.28
        movsx     ecx, BYTE PTR [_DYNUST_SIGNAL_MODULE_mp_GREENEXT] ;118.61
        addss     xmm2, xmm7                                    ;118.28
        orps      xmm2, xmm0                                    ;118.28
        cvtss2si  eax, xmm2                                     ;118.69
        add       ecx, DWORD PTR [80+esp]                       ;118.60
        sub       ecx, esi                                      ;118.60
        cmp       ecx, eax                                      ;118.13
        mov       DWORD PTR [72+esp], edi                       ;115.4
        cmovl     eax, ecx                                      ;118.13
        mov       BYTE PTR [68+esp], al                         ;118.13
        movsx     esi, al                                       ;120.13
        mov       eax, DWORD PTR [44+edx]                       ;120.13
        neg       eax                                           ;120.13
        add       eax, edi                                      ;120.13
        imul      eax, DWORD PTR [40+edx]                       ;120.13
        mov       edx, DWORD PTR [12+edx]                       ;120.13
        push      OFFSET FLAT: __NLITPACK_1.0.1                 ;121.23
        mov       WORD PTR [8+edx+eax], si                      ;120.13
        lea       edx, DWORD PTR [76+esp]                       ;121.23
        push      edx                                           ;121.23
        push      ebx                                           ;121.23
        call      _DYNUST_SIGNAL_MODULE_mp_GETSIGNALPAR         ;121.23
                                ; LOE eax ebx
.B1.38:                         ; Preds .B1.37
        movss     xmm1, DWORD PTR [_2il0floatpacket.2]          ;123.24
        mulss     xmm1, DWORD PTR [_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT] ;123.24
        movss     xmm0, DWORD PTR [_2il0floatpacket.3]          ;123.24
        andps     xmm0, xmm1                                    ;123.24
        pxor      xmm1, xmm0                                    ;123.24
        movss     xmm2, DWORD PTR [_2il0floatpacket.4]          ;123.24
        movaps    xmm3, xmm1                                    ;123.24
        movss     xmm4, DWORD PTR [_2il0floatpacket.5]          ;123.24
        cmpltss   xmm3, xmm2                                    ;123.24
        andps     xmm2, xmm3                                    ;123.24
        movaps    xmm3, xmm1                                    ;123.24
        movaps    xmm6, xmm4                                    ;123.24
        addss     xmm3, xmm2                                    ;123.24
        addss     xmm6, xmm4                                    ;123.24
        subss     xmm3, xmm2                                    ;123.24
        movaps    xmm7, xmm3                                    ;123.24
        mov       DWORD PTR [88+esp], eax                       ;121.13
        subss     xmm7, xmm1                                    ;123.24
        movaps    xmm5, xmm7                                    ;123.24
        cmpless   xmm7, DWORD PTR [_2il0floatpacket.6]          ;123.24
        cmpnless  xmm5, xmm4                                    ;123.24
        andps     xmm5, xmm6                                    ;123.24
        andps     xmm7, xmm6                                    ;123.24
        subss     xmm3, xmm5                                    ;123.24
        addss     xmm3, xmm7                                    ;123.24
        orps      xmm3, xmm0                                    ;123.24
        movss     DWORD PTR [52+esp], xmm3                      ;123.24
        push      OFFSET FLAT: __NLITPACK_3.0.1                 ;123.47
        lea       eax, DWORD PTR [88+esp]                       ;123.47
        push      eax                                           ;123.47
        push      ebx                                           ;123.47
        call      _DYNUST_SIGNAL_MODULE_mp_GETSIGNALPAR         ;123.47
                                ; LOE eax ebx
.B1.103:                        ; Preds .B1.38
        add       esp, 24                                       ;123.47
                                ; LOE eax ebx
.B1.39:                         ; Preds .B1.103
        mov       edx, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY+32] ;125.6
        cvtss2si  esi, DWORD PTR [40+esp]                       ;123.24
        cmp       esi, eax                                      ;124.16
        lea       edx, DWORD PTR [edx+edx*2]                    ;
        movsx     edi, BYTE PTR [68+esp]                        ;124.7
        cmovge    eax, esi                                      ;124.16
        shl       edx, 4                                        ;
        mov       ecx, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY] ;125.6
        cmp       edi, eax                                      ;124.16
        jge       .B1.41        ; Prob 50%                      ;124.16
                                ; LOE edx ecx ebx
.B1.40:                         ; Preds .B1.39
        mov       eax, DWORD PTR [ebx]                          ;125.6
        lea       esi, DWORD PTR [eax+eax*2]                    ;125.6
        shl       esi, 4                                        ;125.6
        add       ecx, esi                                      ;125.6
        sub       ecx, edx                                      ;125.6
        movsx     edx, BYTE PTR [8+ecx]                         ;125.6
        sub       edx, DWORD PTR [44+ecx]                       ;125.6
        imul      edx, DWORD PTR [40+ecx]                       ;125.6
        mov       ecx, DWORD PTR [12+ecx]                       ;125.6
        mov       BYTE PTR [10+ecx+edx], 1                      ;125.6
        jmp       .B1.75        ; Prob 100%                     ;125.6
                                ; LOE ebx
.B1.41:                         ; Preds .B1.39
        mov       eax, DWORD PTR [ebx]                          ;127.7
        lea       esi, DWORD PTR [eax+eax*2]                    ;127.7
        shl       esi, 4                                        ;127.7
        add       ecx, esi                                      ;127.7
        sub       ecx, edx                                      ;127.7
        movsx     edx, BYTE PTR [8+ecx]                         ;127.7
        sub       edx, DWORD PTR [44+ecx]                       ;127.7
        imul      edx, DWORD PTR [40+ecx]                       ;127.7
        mov       ecx, DWORD PTR [12+ecx]                       ;127.7
        mov       BYTE PTR [10+ecx+edx], 2                      ;127.7
        jmp       .B1.75        ; Prob 100%                     ;127.7
                                ; LOE ebx
.B1.43:                         ; Preds .B1.27
        lea       eax, DWORD PTR [76+esp]                       ;94.20
        push      OFFSET FLAT: _DYNUST_SIGNAL_MODULE_mp_GREENEXT ;94.20
        push      eax                                           ;94.20
        lea       edx, DWORD PTR [80+esp]                       ;94.20
        push      edx                                           ;94.20
        push      ebx                                           ;94.20
        call      _DYNUST_SIGNAL_MODULE_mp_ASSIGNGREEN          ;94.20
                                ; LOE ebx
.B1.105:                        ; Preds .B1.43
        add       esp, 16                                       ;94.20
                                ; LOE ebx
.B1.44:                         ; Preds .B1.105
        mov       ebx, DWORD PTR [ebx]                          ;95.75
        sub       ebx, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY+32] ;95.15
        mov       eax, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY] ;95.15
        movsx     ecx, BYTE PTR [_DYNUST_SIGNAL_MODULE_mp_GREENEXT] ;95.135
        lea       edx, DWORD PTR [ebx+ebx*2]                    ;95.15
        shl       edx, 4                                        ;95.15
        mov       edi, DWORD PTR [44+eax+edx]                   ;95.75
        neg       edi                                           ;95.15
        add       edi, DWORD PTR [72+esp]                       ;95.15
        imul      edi, DWORD PTR [40+eax+edx]                   ;95.15
        mov       esi, DWORD PTR [12+eax+edx]                   ;95.75
        add       WORD PTR [8+esi+edi], cx                      ;95.133
        add       esp, 100                                      ;95.133
        pop       ebx                                           ;95.133
        pop       edi                                           ;95.133
        pop       esi                                           ;95.133
        mov       esp, ebp                                      ;95.133
        pop       ebp                                           ;95.133
        ret                                                     ;95.133
                                ; LOE
.B1.45:                         ; Preds .B1.26
        cmp       edi, 2                                        ;91.17
        jne       .B1.76        ; Prob 50%                      ;91.17
                                ; LOE eax ebx
.B1.46:                         ; Preds .B1.45
        test      al, 1                                         ;135.12
        je        .B1.66        ; Prob 60%                      ;135.12
                                ; LOE ebx
.B1.47:                         ; Preds .B1.46
        movsx     eax, BYTE PTR [_DYNUST_SIGNAL_MODULE_mp_GREENEXT] ;136.18
        add       eax, DWORD PTR [80+esp]                       ;136.17
        cmp       eax, DWORD PTR [44+esp]                       ;136.27
        jl        .B1.64        ; Prob 22%                      ;136.27
                                ; LOE ebx
.B1.48:                         ; Preds .B1.47
        mov       eax, DWORD PTR [44+esp]                       ;141.9
        sub       eax, DWORD PTR [80+esp]                       ;141.9
        mov       BYTE PTR [68+esp], al                         ;141.9
        test      al, al                                        ;142.27
        jl        .B1.50        ; Prob 16%                      ;142.27
                                ; LOE eax ebx
.B1.49:                         ; Preds .B1.48
        movsx     eax, al                                       ;141.9
        cvtsi2ss  xmm0, eax                                     ;142.34
        movss     xmm1, DWORD PTR [_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT] ;142.34
        mulss     xmm1, DWORD PTR [_2il0floatpacket.2]          ;142.47
        comiss    xmm0, xmm1                                    ;142.43
        jbe       .B1.51        ; Prob 50%                      ;142.43
                                ; LOE ebx
.B1.50:                         ; Preds .B1.49 .B1.48
        mov       DWORD PTR [esp], 0                            ;143.18
        lea       edx, DWORD PTR [esp]                          ;143.18
        mov       DWORD PTR [32+esp], 17                        ;143.18
        lea       eax, DWORD PTR [32+esp]                       ;143.18
        mov       DWORD PTR [36+esp], OFFSET FLAT: __STRLITPACK_1 ;143.18
        push      32                                            ;143.18
        push      eax                                           ;143.18
        push      OFFSET FLAT: __STRLITPACK_12.0.1              ;143.18
        push      -2088435968                                   ;143.18
        push      -1                                            ;143.18
        push      edx                                           ;143.18
        call      _for_write_seq_lis                            ;143.18
                                ; LOE ebx
.B1.106:                        ; Preds .B1.50
        add       esp, 24                                       ;143.18
                                ; LOE ebx
.B1.51:                         ; Preds .B1.106 .B1.49
        lea       eax, DWORD PTR [68+esp]                       ;145.20
        lea       edx, DWORD PTR [76+esp]                       ;145.20
        push      eax                                           ;145.20
        push      edx                                           ;145.20
        lea       ecx, DWORD PTR [80+esp]                       ;145.20
        push      ecx                                           ;145.20
        push      ebx                                           ;145.20
        call      _DYNUST_SIGNAL_MODULE_mp_ASSIGNGREEN          ;145.20
                                ; LOE ebx
.B1.107:                        ; Preds .B1.51
        add       esp, 16                                       ;145.20
                                ; LOE ebx
.B1.52:                         ; Preds .B1.107
        mov       eax, DWORD PTR [ebx]                          ;146.6
        mov       edx, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY+32] ;146.6
        mov       edi, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY] ;146.6
        lea       esi, DWORD PTR [edx+edx*2]                    ;146.6
        shl       esi, 4                                        ;146.6
        lea       edx, DWORD PTR [eax+eax*2]                    ;146.6
        shl       edx, 4                                        ;146.6
        add       edx, edi                                      ;146.6
        sub       edx, esi                                      ;146.6
        mov       ecx, DWORD PTR [44+edx]                       ;146.6
        neg       ecx                                           ;146.6
        add       ecx, DWORD PTR [72+esp]                       ;146.6
        imul      ecx, DWORD PTR [40+edx]                       ;146.6
        mov       eax, DWORD PTR [12+edx]                       ;146.6
        xor       edx, edx                                      ;146.6
        mov       WORD PTR [8+eax+ecx], dx                      ;146.6
        mov       ecx, DWORD PTR [ebx]                          ;147.6
        mov       edx, DWORD PTR [72+esp]                       ;147.6
        lea       ecx, DWORD PTR [ecx+ecx*2]                    ;147.6
        shl       ecx, 4                                        ;147.6
        add       ecx, edi                                      ;147.6
        sub       ecx, esi                                      ;147.6
        mov       eax, DWORD PTR [44+ecx]                       ;147.6
        neg       eax                                           ;147.6
        add       eax, edx                                      ;147.6
        imul      eax, DWORD PTR [40+ecx]                       ;147.6
        mov       ecx, DWORD PTR [12+ecx]                       ;147.6
        mov       BYTE PTR [10+ecx+eax], 1                      ;147.6
        mov       eax, DWORD PTR [ebx]                          ;149.28
        lea       ecx, DWORD PTR [eax+eax*2]                    ;149.25
        shl       ecx, 4                                        ;149.25
        add       edi, ecx                                      ;149.25
        sub       edi, esi                                      ;149.25
        movsx     esi, BYTE PTR [1+edi]                         ;149.28
        cmp       edx, esi                                      ;149.25
        jne       .B1.54        ; Prob 50%                      ;149.25
                                ; LOE ebx edi
.B1.53:                         ; Preds .B1.52
        mov       eax, 1                                        ;150.18
        jmp       .B1.55        ; Prob 100%                     ;150.18
                                ; LOE eax ebx edi
.B1.54:                         ; Preds .B1.52
        movzx     eax, BYTE PTR [8+edi]                         ;152.65
        inc       eax                                           ;152.110
                                ; LOE eax ebx edi
.B1.55:                         ; Preds .B1.53 .B1.54
        movss     xmm1, DWORD PTR [_2il0floatpacket.2]          ;155.21
        mulss     xmm1, DWORD PTR [_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT] ;155.21
        movss     xmm0, DWORD PTR [_2il0floatpacket.3]          ;155.21
        andps     xmm0, xmm1                                    ;155.21
        pxor      xmm1, xmm0                                    ;155.21
        movss     xmm2, DWORD PTR [_2il0floatpacket.4]          ;155.21
        movaps    xmm3, xmm1                                    ;155.21
        movss     xmm4, DWORD PTR [_2il0floatpacket.5]          ;155.21
        cmpltss   xmm3, xmm2                                    ;155.21
        andps     xmm2, xmm3                                    ;155.21
        movaps    xmm3, xmm1                                    ;155.21
        movaps    xmm6, xmm4                                    ;155.21
        addss     xmm3, xmm2                                    ;155.21
        addss     xmm6, xmm4                                    ;155.21
        subss     xmm3, xmm2                                    ;155.21
        movaps    xmm7, xmm3                                    ;155.21
        mov       BYTE PTR [8+edi], al                          ;150.18
        subss     xmm7, xmm1                                    ;155.21
        movaps    xmm5, xmm7                                    ;155.21
        cmpless   xmm7, DWORD PTR [_2il0floatpacket.6]          ;155.21
        cmpnless  xmm5, xmm4                                    ;155.21
        andps     xmm5, xmm6                                    ;155.21
        andps     xmm7, xmm6                                    ;155.21
        movsx     eax, BYTE PTR [_DYNUST_SIGNAL_MODULE_mp_GREENEXT] ;155.54
        subss     xmm3, xmm5                                    ;155.21
        add       eax, DWORD PTR [80+esp]                       ;155.53
        addss     xmm3, xmm7                                    ;155.21
        orps      xmm3, xmm0                                    ;155.21
        cvtss2si  esi, xmm3                                     ;155.62
        sub       eax, DWORD PTR [44+esp]                       ;155.53
        cmp       eax, esi                                      ;155.6
        mov       DWORD PTR [40+esp], esi                       ;155.62
        cmovge    eax, esi                                      ;155.6
        mov       BYTE PTR [68+esp], al                         ;155.6
        movsx     eax, al                                       ;156.18
        test      eax, eax                                      ;156.27
        jl        .B1.57        ; Prob 16%                      ;156.27
                                ; LOE eax ebx esi
.B1.56:                         ; Preds .B1.55
        cmp       eax, DWORD PTR [40+esp]                       ;156.43
        jle       .B1.58        ; Prob 50%                      ;156.43
                                ; LOE ebx esi
.B1.57:                         ; Preds .B1.56 .B1.55
        mov       DWORD PTR [esp], 0                            ;157.17
        lea       edx, DWORD PTR [esp]                          ;157.17
        mov       DWORD PTR [40+esp], 17                        ;157.17
        lea       eax, DWORD PTR [40+esp]                       ;157.17
        mov       DWORD PTR [44+esp], OFFSET FLAT: __STRLITPACK_0 ;157.17
        push      32                                            ;157.17
        push      eax                                           ;157.17
        push      OFFSET FLAT: __STRLITPACK_13.0.1              ;157.17
        push      -2088435968                                   ;157.17
        push      -1                                            ;157.17
        push      edx                                           ;157.17
        call      _for_write_seq_lis                            ;157.17
                                ; LOE ebx esi
.B1.108:                        ; Preds .B1.57
        add       esp, 24                                       ;157.17
                                ; LOE ebx esi
.B1.58:                         ; Preds .B1.108 .B1.56
        push      OFFSET FLAT: __NLITPACK_3.0.1                 ;160.49
        lea       eax, DWORD PTR [76+esp]                       ;160.49
        push      eax                                           ;160.49
        push      ebx                                           ;160.49
        call      _DYNUST_SIGNAL_MODULE_mp_GETSIGNALPAR         ;160.49
                                ; LOE eax ebx esi
.B1.109:                        ; Preds .B1.58
        add       esp, 12                                       ;160.49
                                ; LOE eax ebx esi
.B1.59:                         ; Preds .B1.109
        mov       edx, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY+32] ;163.8
        mov       edi, esi                                      ;162.18
        cmp       edi, eax                                      ;162.18
        movsx     esi, BYTE PTR [68+esp]                        ;162.9
        cmovge    eax, edi                                      ;162.18
        lea       edx, DWORD PTR [edx+edx*2]                    ;
        shl       edx, 4                                        ;
        mov       ecx, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY] ;163.8
        cmp       esi, eax                                      ;162.18
        jge       .B1.61        ; Prob 50%                      ;162.18
                                ; LOE edx ecx ebx esi
.B1.60:                         ; Preds .B1.59
        mov       edi, DWORD PTR [ebx]                          ;163.8
        lea       edi, DWORD PTR [edi+edi*2]                    ;163.8
        shl       edi, 4                                        ;163.8
        add       edi, ecx                                      ;163.8
        sub       edi, edx                                      ;163.8
        movsx     eax, BYTE PTR [8+edi]                         ;163.8
        sub       eax, DWORD PTR [44+edi]                       ;163.8
        imul      eax, DWORD PTR [40+edi]                       ;163.8
        mov       edi, DWORD PTR [12+edi]                       ;163.8
        mov       BYTE PTR [10+edi+eax], 1                      ;163.8
        jmp       .B1.62        ; Prob 100%                     ;163.8
                                ; LOE edx ecx ebx esi
.B1.61:                         ; Preds .B1.59
        mov       edi, DWORD PTR [ebx]                          ;165.8
        lea       edi, DWORD PTR [edi+edi*2]                    ;165.8
        shl       edi, 4                                        ;165.8
        add       edi, ecx                                      ;165.8
        sub       edi, edx                                      ;165.8
        movsx     eax, BYTE PTR [8+edi]                         ;165.8
        sub       eax, DWORD PTR [44+edi]                       ;165.8
        imul      eax, DWORD PTR [40+edi]                       ;165.8
        mov       edi, DWORD PTR [12+edi]                       ;165.8
        mov       BYTE PTR [10+edi+eax], 2                      ;165.8
                                ; LOE edx ecx ebx esi
.B1.62:                         ; Preds .B1.60 .B1.61
        mov       eax, DWORD PTR [ebx]                          ;168.24
        lea       edi, DWORD PTR [eax+eax*2]                    ;168.6
        shl       edi, 4                                        ;168.6
        add       ecx, edi                                      ;168.6
        sub       ecx, edx                                      ;168.6
        mov       eax, DWORD PTR [44+ecx]                       ;170.15
        neg       eax                                           ;170.15
        movsx     edx, BYTE PTR [8+ecx]                         ;168.24
        add       eax, edx                                      ;170.15
        imul      eax, DWORD PTR [40+ecx]                       ;170.15
        mov       ecx, DWORD PTR [12+ecx]                       ;170.15
        mov       DWORD PTR [72+esp], edx                       ;168.6
        push      OFFSET FLAT: __NLITPACK_1.0.1                 ;171.25
        mov       WORD PTR [8+ecx+eax], si                      ;170.15
        lea       esi, DWORD PTR [76+esp]                       ;171.25
        push      esi                                           ;171.25
        push      ebx                                           ;171.25
        call      _DYNUST_SIGNAL_MODULE_mp_GETSIGNALPAR         ;171.25
                                ; LOE eax ebx
.B1.63:                         ; Preds .B1.62
        mov       DWORD PTR [88+esp], eax                       ;171.15
        lea       eax, DWORD PTR [80+esp]                       ;172.11
        push      eax                                           ;172.11
        lea       edx, DWORD PTR [92+esp]                       ;172.11
        push      edx                                           ;172.11
        lea       ecx, DWORD PTR [92+esp]                       ;172.11
        push      ecx                                           ;172.11
        push      ebx                                           ;172.11
        call      _DYNUST_SIGNAL_MODULE_mp_ASSIGNGREEN          ;172.11
                                ; LOE
.B1.111:                        ; Preds .B1.63
        add       esp, 128                                      ;172.11
        pop       ebx                                           ;172.11
        pop       edi                                           ;172.11
        pop       esi                                           ;172.11
        mov       esp, ebp                                      ;172.11
        pop       ebp                                           ;172.11
        ret                                                     ;172.11
                                ; LOE
.B1.64:                         ; Preds .B1.47
        lea       eax, DWORD PTR [76+esp]                       ;137.18
        push      OFFSET FLAT: _DYNUST_SIGNAL_MODULE_mp_GREENEXT ;137.18
        push      eax                                           ;137.18
        lea       edx, DWORD PTR [80+esp]                       ;137.18
        push      edx                                           ;137.18
        push      ebx                                           ;137.18
        call      _DYNUST_SIGNAL_MODULE_mp_ASSIGNGREEN          ;137.18
                                ; LOE ebx
.B1.112:                        ; Preds .B1.64
        add       esp, 16                                       ;137.18
                                ; LOE ebx
.B1.65:                         ; Preds .B1.112
        mov       ebx, DWORD PTR [ebx]                          ;138.64
        sub       ebx, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY+32] ;138.4
        mov       eax, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY] ;138.4
        movsx     ecx, BYTE PTR [_DYNUST_SIGNAL_MODULE_mp_GREENEXT] ;138.124
        lea       edx, DWORD PTR [ebx+ebx*2]                    ;138.4
        shl       edx, 4                                        ;138.4
        mov       edi, DWORD PTR [44+eax+edx]                   ;138.64
        neg       edi                                           ;138.4
        add       edi, DWORD PTR [72+esp]                       ;138.4
        imul      edi, DWORD PTR [40+eax+edx]                   ;138.4
        mov       esi, DWORD PTR [12+eax+edx]                   ;138.64
        add       WORD PTR [8+esi+edi], cx                      ;138.122
        add       esp, 100                                      ;138.122
        pop       ebx                                           ;138.122
        pop       edi                                           ;138.122
        pop       esi                                           ;138.122
        mov       esp, ebp                                      ;138.122
        pop       ebp                                           ;138.122
        ret                                                     ;138.122
                                ; LOE
.B1.66:                         ; Preds .B1.46
        mov       eax, DWORD PTR [ebx]                          ;176.15
        mov       edi, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY+32] ;176.15
        mov       esi, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY] ;176.15
        lea       ecx, DWORD PTR [edi+edi*2]                    ;176.15
        shl       ecx, 4                                        ;176.15
        lea       edi, DWORD PTR [eax+eax*2]                    ;176.15
        shl       edi, 4                                        ;176.15
        xor       eax, eax                                      ;176.15
        add       edi, esi                                      ;176.15
        sub       edi, ecx                                      ;176.15
        mov       BYTE PTR [68+esp], al                         ;179.15
        mov       edx, DWORD PTR [44+edi]                       ;176.15
        neg       edx                                           ;176.15
        add       edx, DWORD PTR [72+esp]                       ;176.15
        imul      edx, DWORD PTR [40+edi]                       ;176.15
        mov       edi, DWORD PTR [12+edi]                       ;176.15
        mov       WORD PTR [8+edi+edx], ax                      ;176.15
        mov       edx, DWORD PTR [ebx]                          ;177.6
        lea       edi, DWORD PTR [edx+edx*2]                    ;177.6
        shl       edi, 4                                        ;177.6
        lea       edx, DWORD PTR [76+esp]                       ;180.11
        add       esi, edi                                      ;177.6
        sub       esi, ecx                                      ;177.6
        mov       ecx, DWORD PTR [44+esi]                       ;177.6
        neg       ecx                                           ;177.6
        add       ecx, DWORD PTR [72+esp]                       ;177.6
        imul      ecx, DWORD PTR [40+esi]                       ;177.6
        mov       esi, DWORD PTR [12+esi]                       ;177.6
        mov       BYTE PTR [10+esi+ecx], al                     ;177.6
        lea       eax, DWORD PTR [68+esp]                       ;180.11
        push      eax                                           ;180.11
        push      edx                                           ;180.11
        lea       ecx, DWORD PTR [80+esp]                       ;180.11
        push      ecx                                           ;180.11
        push      ebx                                           ;180.11
        call      _DYNUST_SIGNAL_MODULE_mp_ASSIGNGREEN          ;180.11
                                ; LOE ebx
.B1.113:                        ; Preds .B1.66
        add       esp, 16                                       ;180.11
                                ; LOE ebx
.B1.67:                         ; Preds .B1.113
        mov       eax, DWORD PTR [ebx]                          ;182.28
        sub       eax, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY+32] ;182.25
        mov       ecx, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY] ;182.6
        lea       esi, DWORD PTR [eax+eax*2]                    ;182.25
        shl       esi, 4                                        ;182.25
        movsx     edx, BYTE PTR [1+ecx+esi]                     ;182.28
        cmp       edx, DWORD PTR [72+esp]                       ;182.25
        jne       .B1.69        ; Prob 50%                      ;182.25
                                ; LOE ecx ebx esi
.B1.68:                         ; Preds .B1.67
        mov       eax, 1                                        ;183.18
        jmp       .B1.70        ; Prob 100%                     ;183.18
                                ; LOE eax ecx ebx esi
.B1.69:                         ; Preds .B1.67
        movzx     eax, BYTE PTR [8+ecx+esi]                     ;185.65
        inc       eax                                           ;185.110
                                ; LOE eax ecx ebx esi
.B1.70:                         ; Preds .B1.68 .B1.69
        movss     xmm1, DWORD PTR [_2il0floatpacket.2]          ;190.26
        mulss     xmm1, DWORD PTR [_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT] ;190.26
        movss     xmm0, DWORD PTR [_2il0floatpacket.3]          ;190.26
        andps     xmm0, xmm1                                    ;190.26
        pxor      xmm1, xmm0                                    ;190.26
        movss     xmm2, DWORD PTR [_2il0floatpacket.4]          ;190.26
        movaps    xmm3, xmm1                                    ;190.26
        movss     xmm4, DWORD PTR [_2il0floatpacket.5]          ;190.26
        cmpltss   xmm3, xmm2                                    ;190.26
        andps     xmm2, xmm3                                    ;190.26
        movaps    xmm3, xmm1                                    ;190.26
        movaps    xmm6, xmm4                                    ;190.26
        addss     xmm3, xmm2                                    ;190.26
        addss     xmm6, xmm4                                    ;190.26
        subss     xmm3, xmm2                                    ;190.26
        movaps    xmm7, xmm3                                    ;190.26
        movzx     edx, BYTE PTR [_DYNUST_SIGNAL_MODULE_mp_GREENEXT] ;188.15
        subss     xmm7, xmm1                                    ;190.26
        movaps    xmm5, xmm7                                    ;190.26
        cmpless   xmm7, DWORD PTR [_2il0floatpacket.6]          ;190.26
        cmpnless  xmm5, xmm4                                    ;190.26
        andps     xmm5, xmm6                                    ;190.26
        andps     xmm7, xmm6                                    ;190.26
        mov       BYTE PTR [8+ecx+esi], al                      ;183.18
        subss     xmm3, xmm5                                    ;190.26
        movsx     eax, al                                       ;189.15
        addss     xmm3, xmm7                                    ;190.26
        orps      xmm3, xmm0                                    ;190.26
        mov       BYTE PTR [68+esp], dl                         ;188.15
        mov       DWORD PTR [72+esp], eax                       ;189.15
        movss     DWORD PTR [32+esp], xmm3                      ;190.26
        push      OFFSET FLAT: __NLITPACK_3.0.1                 ;190.49
        lea       ecx, DWORD PTR [76+esp]                       ;190.49
        push      ecx                                           ;190.49
        push      ebx                                           ;190.49
        call      _DYNUST_SIGNAL_MODULE_mp_GETSIGNALPAR         ;190.49
                                ; LOE eax ebx
.B1.114:                        ; Preds .B1.70
        mov       DWORD PTR [48+esp], eax                       ;190.49
                                ; LOE ebx
.B1.71:                         ; Preds .B1.114
        mov       eax, DWORD PTR [ebx]                          ;193.75
        sub       eax, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY+32] ;193.15
        mov       edx, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY] ;193.15
        movsx     esi, BYTE PTR [80+esp]                        ;193.135
        lea       ecx, DWORD PTR [eax+eax*2]                    ;193.15
        shl       ecx, 4                                        ;193.15
        mov       eax, DWORD PTR [44+edx+ecx]                   ;193.75
        neg       eax                                           ;193.15
        add       eax, DWORD PTR [84+esp]                       ;193.15
        imul      eax, DWORD PTR [40+edx+ecx]                   ;193.15
        push      OFFSET FLAT: __NLITPACK_1.0.1                 ;194.25
        mov       edi, DWORD PTR [12+edx+ecx]                   ;193.75
        lea       edx, DWORD PTR [88+esp]                       ;194.25
        push      edx                                           ;194.25
        push      ebx                                           ;194.25
        add       WORD PTR [8+edi+eax], si                      ;193.133
        call      _DYNUST_SIGNAL_MODULE_mp_GETSIGNALPAR         ;194.25
                                ; LOE eax ebx
.B1.115:                        ; Preds .B1.71
        add       esp, 24                                       ;194.25
                                ; LOE eax ebx
.B1.72:                         ; Preds .B1.115
        mov       DWORD PTR [76+esp], eax                       ;194.15
        mov       eax, DWORD PTR [ebx]                          ;196.9
        sub       eax, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY+32] ;196.67
        mov       edx, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY] ;196.6
        mov       edi, DWORD PTR [72+esp]                       ;196.9
        lea       ecx, DWORD PTR [eax+eax*2]                    ;196.67
        shl       ecx, 4                                        ;196.67
        movsx     eax, BYTE PTR [8+edx+ecx]                     ;197.8
        mov       esi, DWORD PTR [44+edx+ecx]                   ;196.9
        imul      edi, DWORD PTR [40+edx+ecx]                   ;196.9
        imul      eax, DWORD PTR [40+edx+ecx]                   ;197.8
        imul      esi, DWORD PTR [40+edx+ecx]                   ;196.67
        add       edi, DWORD PTR [12+edx+ecx]                   ;196.67
        add       eax, DWORD PTR [12+edx+ecx]                   ;
        sub       edi, esi                                      ;196.67
        cvtss2si  edx, DWORD PTR [32+esp]                       ;190.26
        sub       eax, esi                                      ;
        mov       esi, DWORD PTR [36+esp]                       ;196.67
        cmp       edx, esi                                      ;196.67
        movsx     ecx, WORD PTR [8+edi]                         ;196.9
        cmovge    esi, edx                                      ;196.67
        cmp       ecx, esi                                      ;196.67
        jge       .B1.74        ; Prob 50%                      ;196.67
                                ; LOE eax ebx
.B1.73:                         ; Preds .B1.72
        mov       BYTE PTR [10+eax], 1                          ;197.8
        jmp       .B1.75        ; Prob 100%                     ;197.8
                                ; LOE ebx
.B1.74:                         ; Preds .B1.72
        mov       BYTE PTR [10+eax], 2                          ;199.8
                                ; LOE ebx
.B1.75:                         ; Preds .B1.41 .B1.40 .B1.73 .B1.74
        lea       eax, DWORD PTR [68+esp]                       ;202.11
        push      eax                                           ;202.11
        lea       edx, DWORD PTR [80+esp]                       ;202.11
        push      edx                                           ;202.11
        lea       ecx, DWORD PTR [80+esp]                       ;202.11
        push      ecx                                           ;202.11
        push      ebx                                           ;202.11
        call      _DYNUST_SIGNAL_MODULE_mp_ASSIGNGREEN          ;202.11
                                ; LOE
.B1.116:                        ; Preds .B1.75
        add       esp, 16                                       ;202.11
                                ; LOE
.B1.76:                         ; Preds .B1.116 .B1.45
        add       esp, 100                                      ;207.1
        pop       ebx                                           ;207.1
        pop       edi                                           ;207.1
        pop       esi                                           ;207.1
        mov       esp, ebp                                      ;207.1
        pop       ebp                                           ;207.1
        ret                                                     ;207.1
                                ; LOE
.B1.77:                         ; Preds .B1.29                  ; Infreq
        lea       eax, DWORD PTR [76+esp]                       ;97.18
        push      OFFSET FLAT: _DYNUST_SIGNAL_MODULE_mp_GREENEXT ;97.18
        push      eax                                           ;97.18
        lea       edx, DWORD PTR [80+esp]                       ;97.18
        push      edx                                           ;97.18
        push      ebx                                           ;97.18
        call      _DYNUST_SIGNAL_MODULE_mp_ASSIGNGREEN          ;97.18
                                ; LOE ebx
.B1.117:                        ; Preds .B1.77                  ; Infreq
        add       esp, 16                                       ;97.18
                                ; LOE ebx
.B1.78:                         ; Preds .B1.117                 ; Infreq
        mov       eax, DWORD PTR [ebx]                          ;98.4
        mov       edi, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY+32] ;98.4
        mov       ecx, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY] ;98.4
        mov       esi, DWORD PTR [72+esp]                       ;98.4
        lea       edx, DWORD PTR [edi+edi*2]                    ;98.4
        shl       edx, 4                                        ;98.4
        lea       edi, DWORD PTR [eax+eax*2]                    ;98.4
        shl       edi, 4                                        ;98.4
        add       edi, ecx                                      ;98.4
        sub       edi, edx                                      ;98.4
        mov       eax, DWORD PTR [44+edi]                       ;98.4
        neg       eax                                           ;98.4
        add       eax, esi                                      ;98.4
        imul      eax, DWORD PTR [40+edi]                       ;98.4
        mov       edi, DWORD PTR [12+edi]                       ;98.4
        mov       BYTE PTR [10+edi+eax], 2                      ;98.4
        mov       ebx, DWORD PTR [ebx]                          ;99.64
        lea       eax, DWORD PTR [ebx+ebx*2]                    ;99.4
        shl       eax, 4                                        ;99.4
        add       ecx, eax                                      ;99.4
        sub       ecx, edx                                      ;99.4
        mov       edi, DWORD PTR [44+ecx]                       ;99.64
        neg       edi                                           ;99.4
        add       edi, esi                                      ;99.4
        imul      edi, DWORD PTR [40+ecx]                       ;99.4
        mov       edx, DWORD PTR [12+ecx]                       ;99.64
        movsx     ecx, BYTE PTR [_DYNUST_SIGNAL_MODULE_mp_GREENEXT] ;99.124
        add       WORD PTR [8+edx+edi], cx                      ;99.122
        add       esp, 100                                      ;99.122
        pop       ebx                                           ;99.122
        pop       edi                                           ;99.122
        pop       esi                                           ;99.122
        mov       esp, ebp                                      ;99.122
        pop       ebp                                           ;99.122
        ret                                                     ;99.122
                                ; LOE
.B1.79:                         ; Preds .B1.23                  ; Infreq
        mov       eax, 32                                       ;83.9
        lea       ecx, DWORD PTR [esp]                          ;83.9
        mov       DWORD PTR [esp], 0                            ;83.9
        lea       edx, DWORD PTR [48+esp]                       ;83.9
        mov       DWORD PTR [48+esp], eax                       ;83.9
        mov       DWORD PTR [52+esp], OFFSET FLAT: __STRLITPACK_5 ;83.9
        push      eax                                           ;83.9
        push      edx                                           ;83.9
        push      OFFSET FLAT: __STRLITPACK_7.0.1               ;83.9
        push      -2088435968                                   ;83.9
        push      911                                           ;83.9
        push      ecx                                           ;83.9
        call      _for_write_seq_lis                            ;83.9
                                ; LOE ebx esi edi
.B1.80:                         ; Preds .B1.79                  ; Infreq
        mov       DWORD PTR [24+esp], 0                         ;84.9
        lea       eax, DWORD PTR [80+esp]                       ;84.9
        mov       DWORD PTR [80+esp], 17                        ;84.9
        mov       DWORD PTR [84+esp], OFFSET FLAT: __STRLITPACK_3 ;84.9
        push      32                                            ;84.9
        push      eax                                           ;84.9
        push      OFFSET FLAT: __STRLITPACK_8.0.1               ;84.9
        push      -2088435968                                   ;84.9
        push      911                                           ;84.9
        lea       edx, DWORD PTR [44+esp]                       ;84.9
        push      edx                                           ;84.9
        call      _for_write_seq_lis                            ;84.9
                                ; LOE ebx esi edi
.B1.81:                         ; Preds .B1.80                  ; Infreq
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE+32] ;84.9
        neg       eax                                           ;84.9
        add       eax, DWORD PTR [ebx]                          ;84.9
        imul      ecx, eax, 44                                  ;84.9
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE] ;84.9
        mov       eax, DWORD PTR [36+edx+ecx]                   ;84.9
        lea       edx, DWORD PTR [112+esp]                      ;84.9
        mov       DWORD PTR [112+esp], eax                      ;84.9
        push      edx                                           ;84.9
        push      OFFSET FLAT: __STRLITPACK_9.0.1               ;84.9
        lea       ecx, DWORD PTR [56+esp]                       ;84.9
        push      ecx                                           ;84.9
        call      _for_write_seq_lis_xmit                       ;84.9
                                ; LOE ebx esi edi
.B1.82:                         ; Preds .B1.81                  ; Infreq
        push      32                                            ;85.9
        xor       eax, eax                                      ;85.9
        push      eax                                           ;85.9
        push      eax                                           ;85.9
        push      -2088435968                                   ;85.9
        push      eax                                           ;85.9
        push      OFFSET FLAT: __STRLITPACK_10                  ;85.9
        call      _for_stop_core                                ;85.9
                                ; LOE ebx esi edi
.B1.118:                        ; Preds .B1.82                  ; Infreq
        add       esp, 84                                       ;85.9
        jmp       .B1.24        ; Prob 100%                     ;85.9
                                ; LOE ebx esi edi
.B1.83:                         ; Preds .B1.12 .B1.13           ; Infreq
        mov       edx, DWORD PTR [48+esp]                       ;65.21
        mov       eax, DWORD PTR [28+esp]                       ;65.21
        movsx     ecx, BYTE PTR [4+eax+edx]                     ;65.21
        cvtsi2ss  xmm0, ecx                                     ;65.13
        cvttss2si edx, xmm0                                     ;66.13
        test      edx, edx                                      ;66.13
        jle       .B1.15        ; Prob 2%                       ;66.13
                                ; LOE eax edx al ah
.B1.84:                         ; Preds .B1.83                  ; Infreq
        mov       edi, DWORD PTR [48+esp]                       ;67.22
        mov       esi, 1                                        ;
        mov       DWORD PTR [68+esp], edx                       ;
        mov       ebx, DWORD PTR [8+eax+edi]                    ;67.22
        mov       ecx, DWORD PTR [40+eax+edi]                   ;67.22
        mov       eax, DWORD PTR [36+eax+edi]                   ;67.22
        imul      ecx, eax                                      ;
        mov       edi, DWORD PTR [56+esp]                       ;55.11
        sub       ebx, ecx                                      ;
        mov       DWORD PTR [8+esp], eax                        ;
        mov       DWORD PTR [64+esp], eax                       ;
        lea       ecx, DWORD PTR [edi*4]                        ;55.11
        shl       edi, 5                                        ;55.11
        sub       edi, ecx                                      ;55.11
        add       edi, DWORD PTR [12+esp]                       ;
        mov       DWORD PTR [56+esp], edi                       ;
        mov       edi, DWORD PTR [8+esp]                        ;
                                ; LOE ebx esi edi
.B1.85:                         ; Preds .B1.87 .B1.84           ; Infreq
        mov       edx, DWORD PTR [edi+ebx]                      ;67.14
        cmp       edx, DWORD PTR [60+esp]                       ;68.25
        jne       .B1.87        ; Prob 50%                      ;68.25
                                ; LOE edx ebx esi edi
.B1.86:                         ; Preds .B1.85                  ; Infreq
        imul      edx, DWORD PTR [20+esp]                       ;67.14
        mov       ecx, DWORD PTR [56+esp]                       ;69.19
        mov       eax, DWORD PTR [16+esp]                       ;69.19
        mov       DWORD PTR [4+edx+ecx], eax                    ;69.19
                                ; LOE ebx esi edi
.B1.87:                         ; Preds .B1.85 .B1.86           ; Infreq
        inc       esi                                           ;71.13
        add       edi, DWORD PTR [64+esp]                       ;71.13
        cmp       esi, DWORD PTR [68+esp]                       ;71.13
        jle       .B1.85        ; Prob 82%                      ;71.13
        jmp       .B1.15        ; Prob 100%                     ;71.13
                                ; LOE ebx esi edi
.B1.89:                         ; Preds .B1.9                   ; Infreq
        mov       DWORD PTR [60+esp], ebx                       ;58.16
        jmp       .B1.12        ; Prob 100%                     ;58.16
        ALIGN     16
                                ; LOE
; mark_end;
_SIGNAL_ACTUATED ENDP
_TEXT	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
__NLITPACK_0.0.1	DD	1
__NLITPACK_1.0.1	DD	5
__NLITPACK_2.0.1	DD	2
__NLITPACK_3.0.1	DD	3
__NLITPACK_4.0.1	DD	4
__STRLITPACK_7.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_8.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_9.0.1	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_12.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_13.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_11.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _SIGNAL_ACTUATED
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_5	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	77
	DB	97
	DB	120
	DB	32
	DB	71
	DB	114
	DB	101
	DB	101
	DB	110
	DB	32
	DB	115
	DB	112
	DB	101
	DB	99
	DB	105
	DB	102
	DB	105
	DB	99
	DB	97
	DB	116
	DB	105
	DB	111
	DB	110
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_3	DB	99
	DB	104
	DB	101
	DB	99
	DB	107
	DB	32
	DB	110
	DB	111
	DB	100
	DB	101
	DB	32
	DB	110
	DB	117
	DB	109
	DB	98
	DB	101
	DB	114
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_10	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_1	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	71
	DB	114
	DB	101
	DB	101
	DB	110
	DB	116
	DB	109
	DB	112
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_0	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	71
	DB	114
	DB	101
	DB	101
	DB	110
	DB	116
	DB	109
	DB	112
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_2	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	71
	DB	114
	DB	101
	DB	101
	DB	110
	DB	116
	DB	109
	DB	112
	DB	0
	DB 2 DUP ( 0H)	; pad
_2il0floatpacket.2	DD	042700000H
_2il0floatpacket.3	DD	080000000H
_2il0floatpacket.4	DD	04b000000H
_2il0floatpacket.5	DD	03f000000H
_2il0floatpacket.6	DD	0bf000000H
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE:BYTE
EXTRN	_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT:BYTE
EXTRN	_DYNUST_SIGNAL_MODULE_mp_GREENEXT:BYTE
_DATA	ENDS
EXTRN	_for_stop_core:PROC
EXTRN	_for_write_seq_lis_xmit:PROC
EXTRN	_for_write_seq_lis:PROC
EXTRN	_DYNUST_SIGNAL_MODULE_mp_GETSIGNALPAR:PROC
EXTRN	_DYNUST_SIGNAL_MODULE_mp_FINDTIMENEEDED:PROC
EXTRN	_DYNUST_SIGNAL_MODULE_mp_ASSIGNGREEN:PROC
EXTRN	__fltused:BYTE
	INCLUDELIB <ifconsol>
	INCLUDELIB <libifcoremt>
	INCLUDELIB <libifport>
	INCLUDELIB <libmmt>
	INCLUDELIB <LIBCMT>
	INCLUDELIB <libirc>
	INCLUDELIB <svml_dispmt>
	INCLUDELIB <OLDNAMES>
	END
