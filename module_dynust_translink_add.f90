MODULE TransLink_add_module
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~ Copyright (C) 2002-2011 The Arizona Board of Regents on Behalf of The University of Arizona.  ~~!
!~~ Developed by Yi-Chang Chiu, Ph.D. as par to the DynusT project.                               ~~!
!~~ This program is free software: you can redistribute it and/or modify it                       ~~!
!~~ under the terms of the GNU General Public License version 2 as published by                   ~~!
!~~ the Free Software Foundation.                                                                 ~~!
!~~ This program is distributed in the hope that it will be useful,                               ~~!
!~~ but WITHOUT ANY WARRANTY; without even the implied warranty of                                ~~!
!~~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                                          ~~!
!~~ See the GNU General Public License for more details.                                          ~~!
!~~ You should have received a copy of the GNU General Public License                             ~~!
!~~ along with this program (in the file COPYING).                                                ~~!
!~~ IF not, see <http://www.gnu.org/licenses/>.                                                   ~~!
!~~ Please see http://www.dynust.net for contact information and names                            ~~!
!~~ of individual contributors.                                                                   ~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
IMPLICIT NONE
   
CONTAINS
    
SUBROUTINE TranLink_Consolidate(it)
 
            USE DYNUST_TRANSLINK_MODULE
            USE DYNUST_MAIN_MODULE
            USE DYNUST_NETWORK_MODULE
            INTEGER it, NewSize
            TYPE(TRANSFER), POINTER :: tempP2(:)
            INTEGER :: vi,nb,vectorerror1
            INTEGER :: OldSize
            OldSize = 0
	
            ! create temp pointer to store contents of array
            IF( TranLink_Array(it)%PSize > 0 ) THEN
                OldSize = TranLink_Array(it)%PSize
                ALLOCATE(tempP2(TranLink_Array(it)%PSize),stat=vectorerror1)
	            IF(vectorerror1 /= 0) THEN
	                WRITE(911,*) 'error in allocating tmpP vectorerror in TranLink_Setup'
	                STOP
	            ENDIF
                nb = 0
                ! copy the non-zero vehid to the temp array by taking out zeros
                DO vi = 1, TranLink_Array(it)%AvSize
	                IF(TranLink_Array(it)%P(vi)%VehID > 0) THEN ! -1 is the vehicle taken out
	                    nb = nb + 1
	                    tempP2(nb)%VehID  = TranLink_Array(it)%P(vi)%VehID 
	                    tempP2(nb)%InLink = TranLink_Array(it)%P(vi)%InLink 
	                    tempP2(nb)%AvTime = TranLink_Array(it)%P(vi)%AvTime
	                ENDIF
	            ENDDO

                IF(nb > 0) THEN
                    TranLink_Array(it)%P(1:nb)%VehID  = tempP2(1:nb)%VehID
                    TranLink_Array(it)%P(1:nb)%InLink = tempP2(1:nb)%InLink
                    TranLink_Array(it)%P(1:nb)%AvTime = tempP2(1:nb)%AvTime
                    TranLink_Array(it)%P(nb+1:vi-1)%VehID  = 0
                    TranLink_Array(it)%P(nb+1:vi-1)%InLink = 0
                    TranLink_Array(it)%P(nb+1:vi-1)%AvTime = 0
                ELSE ! nb = 0
                    TranLink_Array(it)%P(:)%VehID  = 0
                    TranLink_Array(it)%P(:)%InLink = 0
                    TranLink_Array(it)%P(:)%AvTime = 0
                ENDIF
            
                TranLink_Array(it)%AvSize = nb
                m_dynust_network_arc_de(it)%inoutbuff%NVehIN = nb
            ENDIF 
   
            IF(ASSOCIATED(tempP2)) DEALLOCATE(tempP2)   

 END SUBROUTINE 
        
 !~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>
 SUBROUTINE TranLink_Consolidate_new(it)
 
       USE DYNUST_TRANSLINK_MODULE
       USE DYNUST_NETWORK_MODULE
       INTEGER it, NewSize,i,number_data
       INTEGER :: vi,nb,vectorerror1
       INTEGER :: OldSize
       LOGICAL,ALLOCATABLE::accepted(:)
       INTEGER,ALLOCATABLE::a1temp(:),a2temp(:),a3temp(:)
       INTEGER,ALLOCATABLE::b1temp(:),b2temp(:),b3temp(:)

       ALLOCATE(accepted(TranLink_Array(it)%PSize))
       accepted(:) = .false.
       ALLOCATE(a1temp(TranLink_Array(it)%PSize),a2temp(TranLink_Array(it)%PSize),a3temp(TranLink_Array(it)%PSize))
       a1temp(:) = 0
       a2temp(:) = 0
       a3temp(:) = 0
       FORALL(i = 1:TranLink_Array(it)%PSize)
         a1temp(i)=TranLink_Array(it)%P(i)%VehID
         a2temp(i)=TranLink_Array(it)%P(i)%InLink
         a3temp(i)=TranLink_Array(it)%P(i)%AvTime
       END FORALL
       FORALL(i = 1:TranLink_Array(it)%PSize)
         WHERE(a1temp > 0) accepted = .true.
       END FORALL
       TranLink_Array(it)%P(:)%VehID  = 0
       TranLink_Array(it)%P(:)%InLink = 0
       TranLink_Array(it)%P(:)%AvTime = 0

       number_data = COUNT(accepted)
       IF(number_data > 0) THEN
         ALLOCATE(b1temp(number_data),b2temp(number_data),b3temp(number_data))
         b1temp = PACK(a1temp, accepted)
         b2temp = PACK(a2temp, accepted)
         b3temp = PACK(a3temp, accepted)
         TranLink_Array(it)%P(1:number_data)%VehID  = b1temp(1:number_data)
         TranLink_Array(it)%P(1:number_data)%InLink = b2temp(1:number_data)
         TranLink_Array(it)%P(1:number_data)%AvTime = b3temp(1:number_data)
       ENDIF          
       TranLink_Array(it)%AvSize = number_data
       m_dynust_network_arc_de(it)%inoutbuff%NVehIN = number_data
       
       DEALLOCATE(a1temp,a2temp,a3temp,accepted)
       IF(ALLOCATED(b1temp)) DEALLOCATE(b1temp,b2temp,b3temp)   

END SUBROUTINE 
END MODULE 