; mark_description "Intel(R) Visual Fortran Compiler XE for applications running on IA-32, Version 13.1.1.171 Build 20130313";
; mark_description "-S";
	.686P
 	.387
	OPTION DOTNAME
	ASSUME	CS:FLAT,DS:FLAT,SS:FLAT
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
TXTST0:
; -- Begin  _RETRIEVE_VEH_PATH_ASTAR_MIVA
; mark_begin;
IF @Version GE 800
  .MMX
ELSEIF @Version GE 612
  .MMX
  MMWORD TEXTEQU <QWORD>
ENDIF
IF @Version GE 800
  .XMM
ELSEIF @Version GE 614
  .XMM
  XMMWORD TEXTEQU <OWORD>
ENDIF
       ALIGN     16
	PUBLIC _RETRIEVE_VEH_PATH_ASTAR_MIVA
_RETRIEVE_VEH_PATH_ASTAR_MIVA	PROC NEAR 
; parameter 1: 8 + ebp
; parameter 2: 12 + ebp
; parameter 3: 16 + ebp
; parameter 4: 20 + ebp
; parameter 5: 24 + ebp
; parameter 6: 28 + ebp
; parameter 7: 32 + ebp
.B1.1:                          ; Preds .B1.0
        push      ebp                                           ;1.12
        mov       ebp, esp                                      ;1.12
        and       esp, -16                                      ;1.12
        push      esi                                           ;1.12
        push      edi                                           ;1.12
        push      ebx                                           ;1.12
        sub       esp, 20                                       ;1.12
        mov       ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_MAXNU_PA] ;1.12
        test      ebx, ebx                                      ;42.7
        mov       edi, DWORD PTR [28+ebp]                       ;1.12
        jle       .B1.4         ; Prob 50%                      ;42.7
                                ; LOE ebx edi
.B1.2:                          ; Preds .B1.1
        cmp       ebx, 24                                       ;42.7
        jle       .B1.14        ; Prob 0%                       ;42.7
                                ; LOE ebx edi
.B1.3:                          ; Preds .B1.2
        shl       ebx, 2                                        ;42.7
        push      ebx                                           ;42.7
        push      0                                             ;42.7
        push      edi                                           ;42.7
        call      __intel_fast_memset                           ;42.7
                                ; LOE edi
.B1.35:                         ; Preds .B1.3
        add       esp, 12                                       ;42.7
                                ; LOE edi
.B1.4:                          ; Preds .B1.28 .B1.1 .B1.26 .B1.35
        movss     xmm3, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TIME_NOW] ;45.17
        movss     xmm4, DWORD PTR [_2il0floatpacket.9]          ;45.17
        movss     xmm1, DWORD PTR [_2il0floatpacket.10]         ;45.17
        movss     xmm5, DWORD PTR [_2il0floatpacket.11]         ;45.17
        mov       ecx, DWORD PTR [20+ebp]                       ;1.12
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DESTINATION+32] ;47.7
        neg       esi                                           ;47.7
        subss     xmm3, DWORD PTR [_2il0floatpacket.7]          ;45.17
        divss     xmm3, DWORD PTR [_2il0floatpacket.8]          ;45.17
        andps     xmm4, xmm3                                    ;45.17
        movaps    xmm2, xmm4                                    ;45.17
        pxor      xmm2, xmm3                                    ;45.17
        mov       eax, DWORD PTR [24+ebp]                       ;1.12
        cmpltss   xmm2, xmm1                                    ;45.17
        andps     xmm2, xmm1                                    ;45.17
        orps      xmm2, xmm4                                    ;45.17
        movaps    xmm7, xmm2                                    ;45.17
        add       esi, DWORD PTR [ecx]                          ;47.7
        lea       ecx, DWORD PTR [8+esp]                        ;50.12
        mov       ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DESTINATION] ;47.7
        addss     xmm7, xmm3                                    ;45.17
        mov       DWORD PTR [eax], 0                            ;43.7
        subss     xmm7, xmm2                                    ;45.17
        movaps    xmm6, xmm7                                    ;45.17
        mov       eax, DWORD PTR [ebx+esi*4]                    ;47.7
        subss     xmm6, xmm3                                    ;45.17
        movss     xmm3, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIMPERIOD] ;48.14
        cmpnless  xmm6, xmm4                                    ;45.17
        andps     xmm6, xmm5                                    ;45.17
        movss     xmm4, DWORD PTR [_2il0floatpacket.12]         ;48.14
        subss     xmm7, xmm6                                    ;45.17
        cvtss2si  edx, xmm7                                     ;45.17
        inc       edx                                           ;45.45
        movaps    xmm6, xmm4                                    ;48.14
        cvtsi2ss  xmm0, edx                                     ;45.7
        addss     xmm6, xmm4                                    ;48.14
        movss     DWORD PTR [esp], xmm0                         ;45.7
        movss     xmm0, DWORD PTR [_2il0floatpacket.9]          ;48.14
        andps     xmm0, xmm3                                    ;48.14
        pxor      xmm3, xmm0                                    ;48.14
        movaps    xmm2, xmm3                                    ;48.14
        mov       DWORD PTR [4+esp], eax                        ;47.7
        cmpltss   xmm2, xmm1                                    ;48.14
        andps     xmm1, xmm2                                    ;48.14
        movaps    xmm2, xmm3                                    ;48.14
        addss     xmm2, xmm1                                    ;48.14
        subss     xmm2, xmm1                                    ;48.14
        movaps    xmm1, xmm2                                    ;48.14
        subss     xmm1, xmm3                                    ;48.14
        movaps    xmm5, xmm1                                    ;48.14
        cmpless   xmm1, DWORD PTR [_2il0floatpacket.13]         ;48.14
        cmpnless  xmm5, xmm4                                    ;48.14
        andps     xmm5, xmm6                                    ;48.14
        andps     xmm1, xmm6                                    ;48.14
        subss     xmm2, xmm5                                    ;48.14
        addss     xmm2, xmm1                                    ;48.14
        orps      xmm2, xmm0                                    ;48.14
        cvtss2si  edx, xmm2                                     ;48.7
        mov       DWORD PTR [8+esp], edx                        ;48.7
        push      ecx                                           ;50.12
        call      _DYNUST_TDSP_ASTAR_MODULE_mp_CREATE_ASTAR_SCAN_ELIGIBLE_SET ;50.12
                                ; LOE edi
.B1.36:                         ; Preds .B1.4
        add       esp, 4                                        ;50.12
                                ; LOE edi
.B1.5:                          ; Preds .B1.36
        call      _DYNUST_TDSP_ASTAR_MODULE_mp_CREATE_ASTAR_FIXED_LABEL_SET ;51.12
                                ; LOE edi
.B1.6:                          ; Preds .B1.5
        push      OFFSET FLAT: _DYNUST_MAIN_MODULE_mp_NOOFARCS  ;52.12
        call      _DYNUST_TDSP_ASTAR_MODULE_mp_CREATE_LABELS    ;52.12
                                ; LOE edi
.B1.7:                          ; Preds .B1.6
        mov       ebx, DWORD PTR [12+ebp]                       ;1.12
        lea       esi, DWORD PTR [8+esp]                        ;54.12
        lea       eax, DWORD PTR [4+esp]                        ;54.12
        push      OFFSET FLAT: _DYNUST_MAIN_MODULE_mp_NOOFNODES_ORG ;54.12
        push      OFFSET FLAT: __NLITPACK_0.0.1                 ;54.12
        push      eax                                           ;54.12
        push      esi                                           ;54.12
        push      ebx                                           ;54.12
        call      _DYNUST_TDSP_ASTAR_MODULE_mp_ASTAR_INITIALIZATION ;54.12
                                ; LOE ebx esi edi
.B1.8:                          ; Preds .B1.7
        lea       edx, DWORD PTR [36+esp]                       ;55.12
        push      OFFSET FLAT: __NLITPACK_1.0.1                 ;55.12
        push      OFFSET FLAT: __NLITPACK_0.0.1                 ;55.12
        lea       eax, DWORD PTR [32+esp]                       ;55.12
        push      eax                                           ;55.12
        push      edx                                           ;55.12
        push      esi                                           ;55.12
        push      ebx                                           ;55.12
        call      _DYNUST_TDSP_ASTAR_MODULE_mp_ASTAR_GET_PATH   ;55.12
                                ; LOE ebx edi
.B1.9:                          ; Preds .B1.8
        push      DWORD PTR [32+ebp]                            ;56.12
        push      edi                                           ;56.12
        lea       eax, DWORD PTR [68+esp]                       ;56.12
        push      eax                                           ;56.12
        push      ebx                                           ;56.12
        call      _DYNUST_TDSP_ASTAR_MODULE_mp_RETRIEVETDSP     ;56.12
                                ; LOE
.B1.10:                         ; Preds .B1.9
        lea       eax, DWORD PTR [72+esp]                       ;58.12
        push      eax                                           ;58.12
        call      _DYNUST_TDSP_ASTAR_MODULE_mp_DESTROY_ASTAR_SCAN_ELIGIBLE_SET ;58.12
                                ; LOE
.B1.37:                         ; Preds .B1.10
        add       esp, 68                                       ;58.12
                                ; LOE
.B1.11:                         ; Preds .B1.37
        call      _DYNUST_TDSP_ASTAR_MODULE_mp_DESTROY_ASTAR_FIXED_LABEL_SET ;59.12
                                ; LOE
.B1.12:                         ; Preds .B1.11
        call      _DYNUST_TDSP_ASTAR_MODULE_mp_DESTROY_LABELS   ;60.12
                                ; LOE
.B1.13:                         ; Preds .B1.12
        add       esp, 20                                       ;62.1
        pop       ebx                                           ;62.1
        pop       edi                                           ;62.1
        pop       esi                                           ;62.1
        mov       esp, ebp                                      ;62.1
        pop       ebp                                           ;62.1
        ret                                                     ;62.1
                                ; LOE
.B1.14:                         ; Preds .B1.2                   ; Infreq
        cmp       ebx, 4                                        ;42.7
        jl        .B1.30        ; Prob 10%                      ;42.7
                                ; LOE ebx edi
.B1.15:                         ; Preds .B1.14                  ; Infreq
        mov       edx, edi                                      ;42.7
        and       edx, 15                                       ;42.7
        je        .B1.18        ; Prob 50%                      ;42.7
                                ; LOE edx ebx edi
.B1.16:                         ; Preds .B1.15                  ; Infreq
        test      dl, 3                                         ;42.7
        jne       .B1.30        ; Prob 10%                      ;42.7
                                ; LOE edx ebx edi
.B1.17:                         ; Preds .B1.16                  ; Infreq
        neg       edx                                           ;42.7
        add       edx, 16                                       ;42.7
        shr       edx, 2                                        ;42.7
                                ; LOE edx ebx edi
.B1.18:                         ; Preds .B1.17 .B1.15           ; Infreq
        lea       eax, DWORD PTR [4+edx]                        ;42.7
        cmp       ebx, eax                                      ;42.7
        jl        .B1.30        ; Prob 10%                      ;42.7
                                ; LOE edx ebx edi
.B1.19:                         ; Preds .B1.18                  ; Infreq
        mov       ecx, ebx                                      ;42.7
        sub       ecx, edx                                      ;42.7
        and       ecx, 3                                        ;42.7
        neg       ecx                                           ;42.7
        add       ecx, ebx                                      ;42.7
        test      edx, edx                                      ;42.7
        jbe       .B1.23        ; Prob 10%                      ;42.7
                                ; LOE edx ecx ebx edi
.B1.20:                         ; Preds .B1.19                  ; Infreq
        xor       eax, eax                                      ;
                                ; LOE eax edx ecx ebx edi
.B1.21:                         ; Preds .B1.21 .B1.20           ; Infreq
        mov       DWORD PTR [edi+eax*4], 0                      ;42.7
        inc       eax                                           ;42.7
        cmp       eax, edx                                      ;42.7
        jb        .B1.21        ; Prob 82%                      ;42.7
                                ; LOE eax edx ecx ebx edi
.B1.23:                         ; Preds .B1.21 .B1.19           ; Infreq
        pxor      xmm0, xmm0                                    ;42.7
                                ; LOE edx ecx ebx edi xmm0
.B1.24:                         ; Preds .B1.24 .B1.23           ; Infreq
        movdqa    XMMWORD PTR [edi+edx*4], xmm0                 ;42.7
        add       edx, 4                                        ;42.7
        cmp       edx, ecx                                      ;42.7
        jb        .B1.24        ; Prob 82%                      ;42.7
                                ; LOE edx ecx ebx edi xmm0
.B1.26:                         ; Preds .B1.24 .B1.30           ; Infreq
        cmp       ecx, ebx                                      ;42.7
        jae       .B1.4         ; Prob 10%                      ;42.7
                                ; LOE ecx ebx edi
.B1.28:                         ; Preds .B1.26 .B1.28           ; Infreq
        mov       DWORD PTR [edi+ecx*4], 0                      ;42.7
        inc       ecx                                           ;42.7
        cmp       ecx, ebx                                      ;42.7
        jb        .B1.28        ; Prob 82%                      ;42.7
        jmp       .B1.4         ; Prob 100%                     ;42.7
                                ; LOE ecx ebx edi
.B1.30:                         ; Preds .B1.14 .B1.18 .B1.16    ; Infreq
        xor       ecx, ecx                                      ;42.7
        jmp       .B1.26        ; Prob 100%                     ;42.7
        ALIGN     16
                                ; LOE ecx ebx edi
; mark_end;
_RETRIEVE_VEH_PATH_ASTAR_MIVA ENDP
_TEXT	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
__NLITPACK_0.0.1	DD	0
__NLITPACK_1.0.1	DD	1
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _RETRIEVE_VEH_PATH_ASTAR_MIVA
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
_2il0floatpacket.7	DD	03d4ccccdH
_2il0floatpacket.8	DD	042700000H
_2il0floatpacket.9	DD	080000000H
_2il0floatpacket.10	DD	04b000000H
_2il0floatpacket.11	DD	03f800000H
_2il0floatpacket.12	DD	03f000000H
_2il0floatpacket.13	DD	0bf000000H
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
EXTRN	_DYNUST_MAIN_MODULE_mp_NOOFNODES_ORG:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NOOFARCS:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_SIMPERIOD:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_DESTINATION:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TIME_NOW:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_MAXNU_PA:BYTE
_DATA	ENDS
EXTRN	_DYNUST_TDSP_ASTAR_MODULE_mp_CREATE_ASTAR_SCAN_ELIGIBLE_SET:PROC
EXTRN	_DYNUST_TDSP_ASTAR_MODULE_mp_CREATE_ASTAR_FIXED_LABEL_SET:PROC
EXTRN	_DYNUST_TDSP_ASTAR_MODULE_mp_CREATE_LABELS:PROC
EXTRN	_DYNUST_TDSP_ASTAR_MODULE_mp_ASTAR_INITIALIZATION:PROC
EXTRN	_DYNUST_TDSP_ASTAR_MODULE_mp_ASTAR_GET_PATH:PROC
EXTRN	_DYNUST_TDSP_ASTAR_MODULE_mp_RETRIEVETDSP:PROC
EXTRN	_DYNUST_TDSP_ASTAR_MODULE_mp_DESTROY_ASTAR_SCAN_ELIGIBLE_SET:PROC
EXTRN	_DYNUST_TDSP_ASTAR_MODULE_mp_DESTROY_ASTAR_FIXED_LABEL_SET:PROC
EXTRN	_DYNUST_TDSP_ASTAR_MODULE_mp_DESTROY_LABELS:PROC
EXTRN	__intel_fast_memset:PROC
EXTRN	__fltused:BYTE
	INCLUDELIB <ifconsol>
	INCLUDELIB <libifcoremt>
	INCLUDELIB <libifport>
	INCLUDELIB <libmmt>
	INCLUDELIB <LIBCMT>
	INCLUDELIB <libirc>
	INCLUDELIB <svml_dispmt>
	INCLUDELIB <OLDNAMES>
	END
