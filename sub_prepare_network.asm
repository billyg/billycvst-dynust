; mark_description "Intel(R) Visual Fortran Compiler XE for applications running on IA-32, Version 13.1.1.171 Build 20130313";
; mark_description "-S";
	.686P
 	.387
	OPTION DOTNAME
	ASSUME	CS:FLAT,DS:FLAT,SS:FLAT
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
TXTST0:
; -- Begin  _PREP_DATA_STRUCTURE
; mark_begin;
IF @Version GE 800
  .MMX
ELSEIF @Version GE 612
  .MMX
  MMWORD TEXTEQU <QWORD>
ENDIF
IF @Version GE 800
  .XMM
ELSEIF @Version GE 614
  .XMM
  XMMWORD TEXTEQU <OWORD>
ENDIF
       ALIGN     16
	PUBLIC _PREP_DATA_STRUCTURE
_PREP_DATA_STRUCTURE	PROC NEAR 
.B1.1:                          ; Preds .B1.0
        push      ebp                                           ;1.16
        mov       ebp, esp                                      ;1.16
        and       esp, -16                                      ;1.16
        push      esi                                           ;1.16
        push      edi                                           ;1.16
        push      ebx                                           ;1.16
        sub       esp, 292                                      ;1.16
        mov       eax, 1                                        ;32.7
        mov       DWORD PTR [88+esp], eax                       ;32.7
        xor       edx, edx                                      ;27.29
        mov       DWORD PTR [104+esp], eax                      ;32.7
        mov       ecx, 4                                        ;32.7
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFARCS] ;32.7
        test      eax, eax                                      ;32.7
        mov       DWORD PTR [72+esp], edx                       ;27.29
        cmovle    eax, edx                                      ;32.7
        mov       DWORD PTR [92+esp], edx                       ;27.29
        mov       DWORD PTR [84+esp], 133                       ;32.7
        mov       DWORD PTR [76+esp], ecx                       ;32.7
        mov       DWORD PTR [80+esp], edx                       ;32.7
        lea       edx, DWORD PTR [64+esp]                       ;32.7
        mov       DWORD PTR [96+esp], eax                       ;32.7
        mov       DWORD PTR [100+esp], ecx                      ;32.7
        push      ecx                                           ;32.7
        push      eax                                           ;32.7
        push      2                                             ;32.7
        push      edx                                           ;32.7
        call      _for_check_mult_overflow                      ;32.7
                                ; LOE eax
.B1.2:                          ; Preds .B1.1
        mov       edx, DWORD PTR [100+esp]                      ;32.7
        and       eax, 1                                        ;32.7
        and       edx, 1                                        ;32.7
        lea       ecx, DWORD PTR [88+esp]                       ;32.7
        shl       eax, 4                                        ;32.7
        add       edx, edx                                      ;32.7
        or        edx, eax                                      ;32.7
        or        edx, 262144                                   ;32.7
        push      edx                                           ;32.7
        push      ecx                                           ;32.7
        push      DWORD PTR [88+esp]                            ;32.7
        call      _for_alloc_allocatable                        ;32.7
                                ; LOE
.B1.191:                        ; Preds .B1.2
        add       esp, 28                                       ;32.7
                                ; LOE
.B1.3:                          ; Preds .B1.191
        mov       edx, DWORD PTR [96+esp]                       ;33.7
        test      edx, edx                                      ;33.7
        jle       .B1.6         ; Prob 50%                      ;33.7
                                ; LOE edx
.B1.4:                          ; Preds .B1.3
        mov       ecx, DWORD PTR [72+esp]                       ;33.7
        cmp       edx, 24                                       ;33.7
        jle       .B1.164       ; Prob 0%                       ;33.7
                                ; LOE edx ecx
.B1.5:                          ; Preds .B1.4
        shl       edx, 2                                        ;33.7
        push      edx                                           ;33.7
        push      0                                             ;33.7
        push      ecx                                           ;33.7
        call      __intel_fast_memset                           ;33.7
                                ; LOE
.B1.192:                        ; Preds .B1.5
        add       esp, 12                                       ;33.7
                                ; LOE
.B1.6:                          ; Preds .B1.178 .B1.192 .B1.3 .B1.176
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFARCS] ;35.7
        xor       ecx, ecx                                      ;34.7
        test      eax, eax                                      ;35.7
        jle       .B1.187       ; Prob 3%                       ;35.7
                                ; LOE eax ecx
.B1.7:                          ; Preds .B1.6
        mov       ebx, eax                                      ;
        xor       esi, esi                                      ;
        shr       ebx, 31                                       ;
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;130.9
        add       ebx, eax                                      ;
        mov       DWORD PTR [68+esp], edx                       ;130.9
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;130.9
        sar       ebx, 1                                        ;
        mov       DWORD PTR [4+esp], esi                        ;
        mov       DWORD PTR [esp], ecx                          ;
        mov       DWORD PTR [148+esp], eax                      ;
        mov       DWORD PTR [60+esp], edx                       ;
        mov       DWORD PTR [16+esp], ebx                       ;
        mov       DWORD PTR [8+esp], esi                        ;
        mov       ecx, DWORD PTR [4+esp]                        ;
                                ; LOE ecx esi
.B1.8:                          ; Preds .B1.25 .B1.7
        xor       ebx, ebx                                      ;
        mov       DWORD PTR [12+esp], ebx                       ;
        cmp       DWORD PTR [16+esp], 0                         ;38.9
        jbe       .B1.186       ; Prob 3%                       ;38.9
                                ; LOE ecx ebx esi bl bh
.B1.9:                          ; Preds .B1.8
        imul      edx, DWORD PTR [60+esp], -152                 ;
        xor       edi, edi                                      ;
        add       edx, DWORD PTR [68+esp]                       ;
        mov       DWORD PTR [4+esp], ecx                        ;
        mov       DWORD PTR [28+esp], edi                       ;
        mov       eax, DWORD PTR [180+ecx+edx]                  ;43.14
        mov       DWORD PTR [24+esp], eax                       ;43.14
        mov       eax, DWORD PTR [176+ecx+edx]                  ;39.51
        mov       DWORD PTR [20+esp], eax                       ;39.51
        xor       eax, eax                                      ;
        mov       ecx, DWORD PTR [esp]                          ;
                                ; LOE eax edx ecx ebx esi
.B1.10:                         ; Preds .B1.18 .B1.9
        mov       edi, DWORD PTR [20+esp]                       ;39.48
        cmp       edi, DWORD PTR [180+eax+edx]                  ;39.48
        jne       .B1.12        ; Prob 50%                      ;39.48
                                ; LOE eax edx ecx ebx esi
.B1.11:                         ; Preds .B1.10
        inc       ebx                                           ;40.13
        cmp       ebx, esi                                      ;41.22
        cmovge    esi, ebx                                      ;41.22
        mov       ecx, esi                                      ;41.22
                                ; LOE eax edx ecx ebx esi
.B1.12:                         ; Preds .B1.11 .B1.10
        mov       edi, DWORD PTR [24+esp]                       ;43.48
        cmp       edi, DWORD PTR [176+eax+edx]                  ;43.48
        jne       .B1.14        ; Prob 50%                      ;43.48
                                ; LOE eax edx ecx ebx esi
.B1.13:                         ; Preds .B1.12
        mov       ecx, DWORD PTR [12+esp]                       ;44.13
        inc       ecx                                           ;44.13
        cmp       ecx, esi                                      ;45.22
        mov       DWORD PTR [12+esp], ecx                       ;44.13
        cmovge    esi, ecx                                      ;45.22
        mov       ecx, esi                                      ;45.22
                                ; LOE eax edx ecx ebx esi
.B1.14:                         ; Preds .B1.13 .B1.12
        mov       edi, DWORD PTR [20+esp]                       ;39.48
        cmp       edi, DWORD PTR [332+eax+edx]                  ;39.48
        jne       .B1.16        ; Prob 50%                      ;39.48
                                ; LOE eax edx ecx ebx esi
.B1.15:                         ; Preds .B1.14
        inc       ebx                                           ;40.13
        cmp       ebx, esi                                      ;41.22
        cmovge    esi, ebx                                      ;41.22
        mov       ecx, esi                                      ;41.22
                                ; LOE eax edx ecx ebx esi
.B1.16:                         ; Preds .B1.15 .B1.14
        mov       edi, DWORD PTR [24+esp]                       ;43.48
        cmp       edi, DWORD PTR [328+eax+edx]                  ;43.48
        jne       .B1.18        ; Prob 50%                      ;43.48
                                ; LOE eax edx ecx ebx esi
.B1.17:                         ; Preds .B1.16
        mov       ecx, DWORD PTR [12+esp]                       ;44.13
        inc       ecx                                           ;44.13
        cmp       ecx, esi                                      ;45.22
        mov       DWORD PTR [12+esp], ecx                       ;44.13
        cmovge    esi, ecx                                      ;45.22
        mov       ecx, esi                                      ;45.22
                                ; LOE eax edx ecx ebx esi
.B1.18:                         ; Preds .B1.17 .B1.16
        mov       edi, DWORD PTR [28+esp]                       ;38.9
        add       eax, 304                                      ;38.9
        inc       edi                                           ;38.9
        mov       DWORD PTR [28+esp], edi                       ;38.9
        cmp       edi, DWORD PTR [16+esp]                       ;38.9
        jb        .B1.10        ; Prob 64%                      ;38.9
                                ; LOE eax edx ecx ebx esi edi
.B1.19:                         ; Preds .B1.18
        mov       DWORD PTR [esp], ecx                          ;
        lea       eax, DWORD PTR [1+edi+edi]                    ;38.9
        mov       ecx, DWORD PTR [4+esp]                        ;
                                ; LOE eax ecx ebx esi
.B1.20:                         ; Preds .B1.19 .B1.186
        lea       edx, DWORD PTR [-1+eax]                       ;38.9
        cmp       edx, DWORD PTR [148+esp]                      ;38.9
        jae       .B1.25        ; Prob 3%                       ;38.9
                                ; LOE eax ecx ebx esi
.B1.21:                         ; Preds .B1.20
        imul      edx, DWORD PTR [60+esp], -152                 ;39.48
        imul      eax, eax, 152                                 ;39.48
        add       edx, DWORD PTR [68+esp]                       ;39.48
        mov       edi, DWORD PTR [28+eax+edx]                   ;39.14
        cmp       edi, DWORD PTR [176+edx+ecx]                  ;39.48
        jne       .B1.23        ; Prob 50%                      ;39.48
                                ; LOE eax edx ecx ebx esi
.B1.22:                         ; Preds .B1.21
        inc       ebx                                           ;40.13
        cmp       ebx, esi                                      ;41.22
        cmovge    esi, ebx                                      ;41.22
        mov       DWORD PTR [esp], esi                          ;41.22
                                ; LOE eax edx ecx esi
.B1.23:                         ; Preds .B1.22 .B1.21
        mov       ebx, DWORD PTR [180+edx+ecx]                  ;43.14
        cmp       ebx, DWORD PTR [24+eax+edx]                   ;43.48
        jne       .B1.25        ; Prob 50%                      ;43.48
                                ; LOE ecx esi
.B1.24:                         ; Preds .B1.23
        mov       eax, DWORD PTR [12+esp]                       ;44.13
        inc       eax                                           ;44.13
        cmp       eax, esi                                      ;45.22
        cmovge    esi, eax                                      ;45.22
        mov       DWORD PTR [esp], esi                          ;45.22
                                ; LOE ecx esi
.B1.25:                         ; Preds .B1.24 .B1.23 .B1.20
        mov       eax, DWORD PTR [8+esp]                        ;35.7
        add       ecx, 152                                      ;35.7
        inc       eax                                           ;35.7
        mov       DWORD PTR [8+esp], eax                        ;35.7
        cmp       eax, DWORD PTR [148+esp]                      ;35.7
        jb        .B1.8         ; Prob 82%                      ;35.7
                                ; LOE ecx esi
.B1.26:                         ; Preds .B1.25
        mov       ecx, DWORD PTR [esp]                          ;
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_MAXMOVE], ecx ;34.7
        xor       ecx, ecx                                      ;
        mov       DWORD PTR [52+esp], ecx                       ;
        mov       DWORD PTR [56+esp], ecx                       ;
                                ; LOE
.B1.27:                         ; Preds .B1.54 .B1.26
        imul      edx, DWORD PTR [60+esp], -152                 ;51.9
        mov       edi, 4                                        ;51.9
        mov       eax, DWORD PTR [68+esp]                       ;51.9
        xor       ebx, ebx                                      ;51.9
        add       eax, edx                                      ;51.9
        mov       ecx, 1                                        ;51.9
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_MAXMOVE] ;51.9
        test      edx, edx                                      ;51.9
        mov       esi, DWORD PTR [52+esp]                       ;51.9
        push      edi                                           ;51.9
        cmovle    edx, ebx                                      ;51.9
        push      edx                                           ;51.9
        push      2                                             ;51.9
        mov       DWORD PTR [200+eax+esi], 5                    ;51.9
        mov       DWORD PTR [192+eax+esi], edi                  ;51.9
        mov       DWORD PTR [204+eax+esi], ecx                  ;51.9
        mov       DWORD PTR [196+eax+esi], ebx                  ;51.9
        mov       DWORD PTR [220+eax+esi], ecx                  ;51.9
        mov       DWORD PTR [212+eax+esi], edx                  ;51.9
        mov       DWORD PTR [216+eax+esi], edi                  ;51.9
        lea       eax, DWORD PTR [136+esp]                      ;51.9
        push      eax                                           ;51.9
        call      _for_check_mult_overflow                      ;51.9
                                ; LOE eax esi
.B1.28:                         ; Preds .B1.27
        imul      ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32], -152 ;51.9
        and       eax, 1                                        ;51.9
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;51.9
        shl       eax, 4                                        ;51.9
        or        eax, 262144                                   ;51.9
        lea       ebx, DWORD PTR [188+edx+ecx]                  ;51.9
        add       ebx, DWORD PTR [68+esp]                       ;51.9
        push      eax                                           ;51.9
        push      ebx                                           ;51.9
        push      DWORD PTR [148+esp]                           ;51.9
        call      _for_allocate                                 ;51.9
                                ; LOE esi
.B1.194:                        ; Preds .B1.28
        add       esp, 28                                       ;51.9
                                ; LOE esi
.B1.29:                         ; Preds .B1.194
        imul      ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32], -152 ;52.9
        mov       edx, esi                                      ;52.9
        add       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;52.9
        mov       eax, DWORD PTR [212+ecx+edx]                  ;52.9
        test      eax, eax                                      ;52.9
        mov       ebx, DWORD PTR [220+ecx+edx]                  ;52.9
        jle       .B1.36        ; Prob 50%                      ;52.9
                                ; LOE eax edx ecx ebx
.B1.30:                         ; Preds .B1.29
        mov       esi, eax                                      ;52.9
        shr       esi, 31                                       ;52.9
        add       esi, eax                                      ;52.9
        sar       esi, 1                                        ;52.9
        mov       DWORD PTR [156+esp], esi                      ;52.9
        test      esi, esi                                      ;52.9
        jbe       .B1.157       ; Prob 10%                      ;52.9
                                ; LOE eax edx ecx ebx
.B1.31:                         ; Preds .B1.30
        mov       esi, ebx                                      ;
        mov       DWORD PTR [esp], eax                          ;
        mov       eax, DWORD PTR [216+ecx+edx]                  ;52.9
        imul      esi, eax                                      ;
        mov       edi, DWORD PTR [188+ecx+edx]                  ;52.9
        sub       edi, esi                                      ;
        mov       DWORD PTR [40+esp], 0                         ;
        mov       DWORD PTR [128+esp], eax                      ;52.9
        mov       DWORD PTR [8+esp], edx                        ;
        mov       DWORD PTR [108+esp], ebx                      ;
        mov       DWORD PTR [4+esp], ecx                        ;
        mov       eax, edi                                      ;
        mov       edx, DWORD PTR [40+esp]                       ;
                                ; LOE eax edx
.B1.32:                         ; Preds .B1.32 .B1.31
        mov       ecx, DWORD PTR [108+esp]                      ;52.9
        xor       edi, edi                                      ;52.9
        mov       esi, DWORD PTR [128+esp]                      ;52.9
        mov       ebx, esi                                      ;52.9
        lea       ecx, DWORD PTR [ecx+edx*2]                    ;52.9
        inc       edx                                           ;52.9
        imul      ebx, ecx                                      ;52.9
        inc       ecx                                           ;52.9
        imul      ecx, esi                                      ;52.9
        mov       DWORD PTR [eax+ebx], edi                      ;52.9
        mov       DWORD PTR [eax+ecx], edi                      ;52.9
        cmp       edx, DWORD PTR [156+esp]                      ;52.9
        jb        .B1.32        ; Prob 64%                      ;52.9
                                ; LOE eax edx
.B1.33:                         ; Preds .B1.32
        mov       DWORD PTR [40+esp], edx                       ;
        mov       esi, edx                                      ;52.9
        mov       eax, DWORD PTR [esp]                          ;
        mov       ebx, DWORD PTR [108+esp]                      ;
        mov       ecx, DWORD PTR [4+esp]                        ;
        lea       esi, DWORD PTR [1+esi+esi]                    ;52.9
        mov       edx, DWORD PTR [8+esp]                        ;
                                ; LOE eax edx ecx ebx esi
.B1.34:                         ; Preds .B1.33 .B1.157
        lea       edi, DWORD PTR [-1+esi]                       ;52.9
        cmp       eax, edi                                      ;52.9
        jbe       .B1.36        ; Prob 10%                      ;52.9
                                ; LOE edx ecx ebx esi
.B1.35:                         ; Preds .B1.34
        lea       eax, DWORD PTR [-1+esi+ebx]                   ;52.9
        sub       eax, ebx                                      ;52.9
        imul      eax, DWORD PTR [216+ecx+edx]                  ;52.9
        mov       ebx, DWORD PTR [188+ecx+edx]                  ;52.9
        mov       DWORD PTR [ebx+eax], 0                        ;52.9
                                ; LOE edx ecx
.B1.36:                         ; Preds .B1.29 .B1.34 .B1.35
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_MAXMOVE] ;53.9
        mov       esi, 4                                        ;53.9
        mov       ebx, 6                                        ;53.9
        cmp       edi, 6                                        ;53.9
        push      esi                                           ;53.9
        cmovle    edi, ebx                                      ;53.9
        mov       eax, 1                                        ;53.9
        push      edi                                           ;53.9
        push      2                                             ;53.9
        mov       DWORD PTR [236+ecx+edx], 5                    ;53.9
        mov       DWORD PTR [228+ecx+edx], esi                  ;53.9
        mov       DWORD PTR [240+ecx+edx], eax                  ;53.9
        mov       DWORD PTR [232+ecx+edx], 0                    ;53.9
        mov       DWORD PTR [256+ecx+edx], eax                  ;53.9
        mov       DWORD PTR [248+ecx+edx], edi                  ;53.9
        mov       DWORD PTR [252+ecx+edx], esi                  ;53.9
        lea       edx, DWORD PTR [144+esp]                      ;53.9
        push      edx                                           ;53.9
        call      _for_check_mult_overflow                      ;53.9
                                ; LOE eax
.B1.37:                         ; Preds .B1.36
        imul      ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32], -152 ;53.9
        and       eax, 1                                        ;53.9
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;53.9
        shl       eax, 4                                        ;53.9
        or        eax, 262144                                   ;53.9
        lea       ebx, DWORD PTR [224+edx+ecx]                  ;53.9
        add       ebx, DWORD PTR [68+esp]                       ;53.9
        push      eax                                           ;53.9
        push      ebx                                           ;53.9
        push      DWORD PTR [156+esp]                           ;53.9
        call      _for_allocate                                 ;53.9
                                ; LOE
.B1.196:                        ; Preds .B1.37
        add       esp, 28                                       ;53.9
                                ; LOE
.B1.38:                         ; Preds .B1.196
        imul      ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32], -152 ;54.9
        mov       edx, DWORD PTR [52+esp]                       ;54.9
        add       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;54.9
        mov       eax, DWORD PTR [248+ecx+edx]                  ;54.9
        test      eax, eax                                      ;54.9
        mov       ebx, DWORD PTR [256+ecx+edx]                  ;54.9
        jle       .B1.45        ; Prob 50%                      ;54.9
                                ; LOE eax edx ecx ebx
.B1.39:                         ; Preds .B1.38
        mov       esi, eax                                      ;54.9
        shr       esi, 31                                       ;54.9
        add       esi, eax                                      ;54.9
        sar       esi, 1                                        ;54.9
        mov       DWORD PTR [144+esp], esi                      ;54.9
        test      esi, esi                                      ;54.9
        jbe       .B1.158       ; Prob 10%                      ;54.9
                                ; LOE eax edx ecx ebx
.B1.40:                         ; Preds .B1.39
        mov       esi, ebx                                      ;
        mov       DWORD PTR [12+esp], eax                       ;
        mov       eax, DWORD PTR [252+ecx+edx]                  ;54.9
        imul      esi, eax                                      ;
        mov       edi, DWORD PTR [224+ecx+edx]                  ;54.9
        sub       edi, esi                                      ;
        mov       DWORD PTR [44+esp], 0                         ;
        mov       DWORD PTR [136+esp], eax                      ;54.9
        mov       DWORD PTR [20+esp], edx                       ;
        mov       DWORD PTR [112+esp], ebx                      ;
        mov       DWORD PTR [16+esp], ecx                       ;
        mov       eax, edi                                      ;
        mov       edx, DWORD PTR [44+esp]                       ;
                                ; LOE eax edx
.B1.41:                         ; Preds .B1.41 .B1.40
        mov       ecx, DWORD PTR [112+esp]                      ;54.9
        xor       edi, edi                                      ;54.9
        mov       esi, DWORD PTR [136+esp]                      ;54.9
        mov       ebx, esi                                      ;54.9
        lea       ecx, DWORD PTR [ecx+edx*2]                    ;54.9
        inc       edx                                           ;54.9
        imul      ebx, ecx                                      ;54.9
        inc       ecx                                           ;54.9
        imul      ecx, esi                                      ;54.9
        mov       DWORD PTR [eax+ebx], edi                      ;54.9
        mov       DWORD PTR [eax+ecx], edi                      ;54.9
        cmp       edx, DWORD PTR [144+esp]                      ;54.9
        jb        .B1.41        ; Prob 64%                      ;54.9
                                ; LOE eax edx
.B1.42:                         ; Preds .B1.41
        mov       DWORD PTR [44+esp], edx                       ;
        mov       esi, edx                                      ;54.9
        mov       eax, DWORD PTR [12+esp]                       ;
        mov       ebx, DWORD PTR [112+esp]                      ;
        mov       ecx, DWORD PTR [16+esp]                       ;
        lea       esi, DWORD PTR [1+esi+esi]                    ;54.9
        mov       edx, DWORD PTR [20+esp]                       ;
                                ; LOE eax edx ecx ebx esi
.B1.43:                         ; Preds .B1.42 .B1.158
        lea       edi, DWORD PTR [-1+esi]                       ;54.9
        cmp       eax, edi                                      ;54.9
        jbe       .B1.45        ; Prob 10%                      ;54.9
                                ; LOE edx ecx ebx esi
.B1.44:                         ; Preds .B1.43
        lea       eax, DWORD PTR [-1+esi+ebx]                   ;54.9
        sub       eax, ebx                                      ;54.9
        imul      eax, DWORD PTR [252+ecx+edx]                  ;54.9
        mov       ebx, DWORD PTR [224+ecx+edx]                  ;54.9
        mov       DWORD PTR [ebx+eax], 0                        ;54.9
                                ; LOE edx ecx
.B1.45:                         ; Preds .B1.38 .B1.43 .B1.44
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_MAXMOVE] ;55.9
        mov       esi, 4                                        ;55.9
        xor       ebx, ebx                                      ;55.9
        test      edi, edi                                      ;55.9
        push      esi                                           ;55.9
        cmovle    edi, ebx                                      ;55.9
        mov       eax, 1                                        ;55.9
        push      edi                                           ;55.9
        push      2                                             ;55.9
        mov       DWORD PTR [276+ecx+edx], 5                    ;55.9
        mov       DWORD PTR [268+ecx+edx], esi                  ;55.9
        mov       DWORD PTR [280+ecx+edx], eax                  ;55.9
        mov       DWORD PTR [272+ecx+edx], ebx                  ;55.9
        mov       DWORD PTR [296+ecx+edx], eax                  ;55.9
        mov       DWORD PTR [288+ecx+edx], edi                  ;55.9
        mov       DWORD PTR [292+ecx+edx], esi                  ;55.9
        lea       edx, DWORD PTR [152+esp]                      ;55.9
        push      edx                                           ;55.9
        call      _for_check_mult_overflow                      ;55.9
                                ; LOE eax
.B1.46:                         ; Preds .B1.45
        imul      ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32], -152 ;55.9
        and       eax, 1                                        ;55.9
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;55.9
        shl       eax, 4                                        ;55.9
        or        eax, 262144                                   ;55.9
        lea       ebx, DWORD PTR [264+edx+ecx]                  ;55.9
        add       ebx, DWORD PTR [68+esp]                       ;55.9
        push      eax                                           ;55.9
        push      ebx                                           ;55.9
        push      DWORD PTR [164+esp]                           ;55.9
        call      _for_allocate                                 ;55.9
                                ; LOE
.B1.198:                        ; Preds .B1.46
        add       esp, 28                                       ;55.9
                                ; LOE
.B1.47:                         ; Preds .B1.198
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;56.9
        mov       ecx, DWORD PTR [52+esp]                       ;56.9
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;56.9
        mov       DWORD PTR [68+esp], edx                       ;56.9
        mov       DWORD PTR [60+esp], eax                       ;56.9
        add       ecx, edx                                      ;56.9
        imul      edx, eax, -152                                ;56.9
        mov       ebx, DWORD PTR [296+edx+ecx]                  ;56.9
        mov       eax, DWORD PTR [288+edx+ecx]                  ;56.9
        test      eax, eax                                      ;56.9
        mov       DWORD PTR [116+esp], ebx                      ;56.9
        jle       .B1.54        ; Prob 50%                      ;56.9
                                ; LOE eax edx ecx
.B1.48:                         ; Preds .B1.47
        mov       ebx, eax                                      ;56.9
        shr       ebx, 31                                       ;56.9
        add       ebx, eax                                      ;56.9
        sar       ebx, 1                                        ;56.9
        mov       DWORD PTR [152+esp], ebx                      ;56.9
        test      ebx, ebx                                      ;56.9
        jbe       .B1.159       ; Prob 10%                      ;56.9
                                ; LOE eax edx ecx
.B1.49:                         ; Preds .B1.48
        mov       esi, DWORD PTR [292+edx+ecx]                  ;56.9
        xor       ebx, ebx                                      ;
        mov       edi, DWORD PTR [116+esp]                      ;
        imul      edi, esi                                      ;
        mov       DWORD PTR [48+esp], ebx                       ;
        mov       ebx, DWORD PTR [264+edx+ecx]                  ;56.9
        sub       ebx, edi                                      ;
        mov       DWORD PTR [36+esp], ebx                       ;
        mov       DWORD PTR [32+esp], eax                       ;
        mov       DWORD PTR [120+esp], esi                      ;56.9
        mov       ebx, DWORD PTR [48+esp]                       ;
        mov       eax, DWORD PTR [36+esp]                       ;
        mov       DWORD PTR [28+esp], edx                       ;
        mov       DWORD PTR [24+esp], ecx                       ;
                                ; LOE eax ebx
.B1.50:                         ; Preds .B1.50 .B1.49
        mov       edx, DWORD PTR [116+esp]                      ;56.9
        xor       edi, edi                                      ;56.9
        mov       esi, DWORD PTR [120+esp]                      ;56.9
        mov       ecx, esi                                      ;56.9
        lea       edx, DWORD PTR [edx+ebx*2]                    ;56.9
        inc       ebx                                           ;56.9
        imul      ecx, edx                                      ;56.9
        inc       edx                                           ;56.9
        imul      edx, esi                                      ;56.9
        mov       DWORD PTR [eax+ecx], edi                      ;56.9
        mov       DWORD PTR [eax+edx], edi                      ;56.9
        cmp       ebx, DWORD PTR [152+esp]                      ;56.9
        jb        .B1.50        ; Prob 64%                      ;56.9
                                ; LOE eax ebx
.B1.51:                         ; Preds .B1.50
        mov       eax, DWORD PTR [32+esp]                       ;
        lea       ebx, DWORD PTR [1+ebx+ebx]                    ;56.9
        mov       edx, DWORD PTR [28+esp]                       ;
        mov       ecx, DWORD PTR [24+esp]                       ;
                                ; LOE eax edx ecx ebx
.B1.52:                         ; Preds .B1.51 .B1.159
        lea       esi, DWORD PTR [-1+ebx]                       ;56.9
        cmp       eax, esi                                      ;56.9
        jbe       .B1.54        ; Prob 10%                      ;56.9
                                ; LOE edx ecx ebx
.B1.53:                         ; Preds .B1.52
        mov       eax, DWORD PTR [116+esp]                      ;56.9
        lea       ebx, DWORD PTR [-1+ebx+eax]                   ;56.9
        sub       ebx, eax                                      ;56.9
        imul      ebx, DWORD PTR [292+edx+ecx]                  ;56.9
        mov       edx, DWORD PTR [264+edx+ecx]                  ;56.9
        mov       DWORD PTR [edx+ebx], 0                        ;56.9
                                ; LOE
.B1.54:                         ; Preds .B1.47 .B1.52 .B1.53
        mov       edx, DWORD PTR [56+esp]                       ;50.7
        inc       edx                                           ;50.7
        mov       eax, DWORD PTR [52+esp]                       ;50.7
        add       eax, 152                                      ;50.7
        mov       DWORD PTR [52+esp], eax                       ;50.7
        mov       DWORD PTR [56+esp], edx                       ;50.7
        cmp       edx, DWORD PTR [148+esp]                      ;50.7
        jb        .B1.27        ; Prob 82%                      ;50.7
                                ; LOE
.B1.55:                         ; Preds .B1.54
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFARCS] ;59.7
                                ; LOE eax
.B1.56:                         ; Preds .B1.187 .B1.55
        test      eax, eax                                      ;59.7
        jle       .B1.101       ; Prob 3%                       ;59.7
                                ; LOE eax
.B1.57:                         ; Preds .B1.56
        imul      edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32], -152 ;
        mov       esi, eax                                      ;
        shr       esi, 31                                       ;
        xor       ecx, ecx                                      ;
        add       esi, eax                                      ;
        xor       ebx, ebx                                      ;
        sar       esi, 1                                        ;
        add       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;
        mov       DWORD PTR [148+esp], eax                      ;
        mov       DWORD PTR [20+esp], esi                       ;
        mov       DWORD PTR [esp], ecx                          ;
                                ; LOE edx ebx
.B1.58:                         ; Preds .B1.75 .B1.57
        xor       eax, eax                                      ;
        mov       DWORD PTR [8+esp], eax                        ;
        mov       DWORD PTR [4+esp], eax                        ;
        cmp       DWORD PTR [20+esp], 0                         ;62.9
        jbe       .B1.185       ; Prob 3%                       ;62.9
                                ; LOE edx ebx
.B1.59:                         ; Preds .B1.58
        mov       edi, DWORD PTR [180+ebx+edx]                  ;68.14
        xor       eax, eax                                      ;
        mov       esi, DWORD PTR [176+ebx+edx]                  ;63.51
        xor       ecx, ecx                                      ;
        mov       DWORD PTR [24+esp], esi                       ;
        mov       DWORD PTR [28+esp], edi                       ;
        mov       DWORD PTR [12+esp], ebx                       ;
                                ; LOE eax edx ecx
.B1.60:                         ; Preds .B1.68 .B1.59
        mov       ebx, DWORD PTR [24+esp]                       ;63.48
        cmp       ebx, DWORD PTR [180+ecx+edx]                  ;63.48
        jne       .B1.62        ; Prob 50%                      ;63.48
                                ; LOE eax edx ecx
.B1.61:                         ; Preds .B1.60
        mov       edi, DWORD PTR [12+esp]                       ;65.13
        lea       esi, DWORD PTR [1+eax+eax]                    ;65.13
        mov       DWORD PTR [16+esp], ecx                       ;
        mov       ecx, DWORD PTR [8+esp]                        ;64.13
        mov       ebx, DWORD PTR [220+edi+edx]                  ;65.13
        inc       ecx                                           ;64.13
        neg       ebx                                           ;65.13
        add       ebx, ecx                                      ;65.13
        imul      ebx, DWORD PTR [216+edi+edx]                  ;65.13
        mov       DWORD PTR [8+esp], ecx                        ;64.13
        mov       ecx, DWORD PTR [188+edi+edx]                  ;65.13
        mov       DWORD PTR [ecx+ebx], esi                      ;65.13
        mov       ecx, DWORD PTR [16+esp]                       ;65.13
                                ; LOE eax edx ecx
.B1.62:                         ; Preds .B1.61 .B1.60
        mov       ebx, DWORD PTR [28+esp]                       ;68.48
        cmp       ebx, DWORD PTR [176+ecx+edx]                  ;68.48
        jne       .B1.64        ; Prob 50%                      ;68.48
                                ; LOE eax edx ecx
.B1.63:                         ; Preds .B1.62
        mov       edi, DWORD PTR [12+esp]                       ;70.13
        lea       esi, DWORD PTR [1+eax+eax]                    ;70.13
        mov       DWORD PTR [16+esp], ecx                       ;
        mov       ecx, DWORD PTR [4+esp]                        ;69.13
        mov       ebx, DWORD PTR [296+edi+edx]                  ;70.13
        inc       ecx                                           ;69.13
        neg       ebx                                           ;70.13
        add       ebx, ecx                                      ;70.13
        imul      ebx, DWORD PTR [292+edi+edx]                  ;70.13
        mov       DWORD PTR [4+esp], ecx                        ;69.13
        mov       ecx, DWORD PTR [264+edi+edx]                  ;70.13
        mov       DWORD PTR [ecx+ebx], esi                      ;70.13
        mov       ecx, DWORD PTR [16+esp]                       ;70.13
                                ; LOE eax edx ecx
.B1.64:                         ; Preds .B1.63 .B1.62
        mov       ebx, DWORD PTR [24+esp]                       ;63.48
        cmp       ebx, DWORD PTR [332+ecx+edx]                  ;63.48
        jne       .B1.66        ; Prob 50%                      ;63.48
                                ; LOE eax edx ecx
.B1.65:                         ; Preds .B1.64
        mov       edi, DWORD PTR [12+esp]                       ;65.13
        lea       esi, DWORD PTR [2+eax+eax]                    ;65.13
        mov       DWORD PTR [16+esp], ecx                       ;
        mov       ecx, DWORD PTR [8+esp]                        ;64.13
        mov       ebx, DWORD PTR [220+edi+edx]                  ;65.13
        inc       ecx                                           ;64.13
        neg       ebx                                           ;65.13
        add       ebx, ecx                                      ;65.13
        imul      ebx, DWORD PTR [216+edi+edx]                  ;65.13
        mov       DWORD PTR [8+esp], ecx                        ;64.13
        mov       ecx, DWORD PTR [188+edi+edx]                  ;65.13
        mov       DWORD PTR [ecx+ebx], esi                      ;65.13
        mov       ecx, DWORD PTR [16+esp]                       ;65.13
                                ; LOE eax edx ecx
.B1.66:                         ; Preds .B1.65 .B1.64
        mov       ebx, DWORD PTR [28+esp]                       ;68.48
        cmp       ebx, DWORD PTR [328+ecx+edx]                  ;68.48
        jne       .B1.68        ; Prob 50%                      ;68.48
                                ; LOE eax edx ecx
.B1.67:                         ; Preds .B1.66
        mov       edi, DWORD PTR [12+esp]                       ;70.13
        lea       esi, DWORD PTR [2+eax+eax]                    ;70.13
        mov       DWORD PTR [16+esp], ecx                       ;
        mov       ecx, DWORD PTR [4+esp]                        ;69.13
        mov       ebx, DWORD PTR [296+edi+edx]                  ;70.13
        inc       ecx                                           ;69.13
        neg       ebx                                           ;70.13
        add       ebx, ecx                                      ;70.13
        imul      ebx, DWORD PTR [292+edi+edx]                  ;70.13
        mov       DWORD PTR [4+esp], ecx                        ;69.13
        mov       ecx, DWORD PTR [264+edi+edx]                  ;70.13
        mov       DWORD PTR [ecx+ebx], esi                      ;70.13
        mov       ecx, DWORD PTR [16+esp]                       ;70.13
                                ; LOE eax edx ecx
.B1.68:                         ; Preds .B1.67 .B1.66
        inc       eax                                           ;62.9
        add       ecx, 304                                      ;62.9
        cmp       eax, DWORD PTR [20+esp]                       ;62.9
        jb        .B1.60        ; Prob 64%                      ;62.9
                                ; LOE eax edx ecx
.B1.69:                         ; Preds .B1.68
        mov       ebx, DWORD PTR [12+esp]                       ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;62.9
                                ; LOE eax edx ebx
.B1.70:                         ; Preds .B1.69 .B1.185
        lea       ecx, DWORD PTR [-1+eax]                       ;62.9
        cmp       ecx, DWORD PTR [148+esp]                      ;62.9
        jae       .B1.75        ; Prob 3%                       ;62.9
                                ; LOE eax edx ebx
.B1.71:                         ; Preds .B1.70
        imul      edi, eax, 152                                 ;63.48
        mov       ecx, DWORD PTR [28+edi+edx]                   ;63.14
        cmp       ecx, DWORD PTR [176+edx+ebx]                  ;63.48
        jne       .B1.73        ; Prob 50%                      ;63.48
                                ; LOE eax edx ebx edi
.B1.72:                         ; Preds .B1.71
        mov       ecx, DWORD PTR [8+esp]                        ;64.13
        mov       esi, DWORD PTR [220+edx+ebx]                  ;65.13
        inc       ecx                                           ;64.13
        neg       esi                                           ;65.13
        add       esi, ecx                                      ;65.13
        imul      esi, DWORD PTR [216+edx+ebx]                  ;65.13
        mov       DWORD PTR [8+esp], ecx                        ;64.13
        mov       ecx, DWORD PTR [188+edx+ebx]                  ;65.13
        mov       DWORD PTR [ecx+esi], eax                      ;65.13
                                ; LOE eax edx ebx edi
.B1.73:                         ; Preds .B1.72 .B1.71
        mov       ecx, DWORD PTR [180+edx+ebx]                  ;68.14
        cmp       ecx, DWORD PTR [24+edi+edx]                   ;68.48
        jne       .B1.75        ; Prob 50%                      ;68.48
                                ; LOE eax edx ebx
.B1.74:                         ; Preds .B1.73
        mov       ecx, DWORD PTR [4+esp]                        ;69.13
        mov       edi, DWORD PTR [296+edx+ebx]                  ;70.13
        inc       ecx                                           ;69.13
        neg       edi                                           ;70.13
        add       edi, ecx                                      ;70.13
        imul      edi, DWORD PTR [292+edx+ebx]                  ;70.13
        mov       esi, DWORD PTR [264+edx+ebx]                  ;70.13
        mov       DWORD PTR [4+esp], ecx                        ;69.13
        mov       DWORD PTR [esi+edi], eax                      ;70.13
                                ; LOE edx ebx
.B1.75:                         ; Preds .B1.74 .B1.73 .B1.70
        mov       eax, DWORD PTR [8+esp]                        ;73.9
        mov       ecx, DWORD PTR [4+esp]                        ;74.9
        mov       esi, DWORD PTR [esp]                          ;59.7
        inc       esi                                           ;59.7
        mov       BYTE PTR [184+edx+ebx], al                    ;73.9
        mov       BYTE PTR [260+edx+ebx], cl                    ;74.9
        add       ebx, 152                                      ;59.7
        mov       DWORD PTR [esp], esi                          ;59.7
        cmp       esi, DWORD PTR [148+esp]                      ;59.7
        jb        .B1.58        ; Prob 82%                      ;59.7
                                ; LOE edx ebx
.B1.76:                         ; Preds .B1.75
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;81.9
        xor       edi, edi                                      ;
        mov       DWORD PTR [180+esp], edx                      ;81.9
        xor       esi, esi                                      ;
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;81.9
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;91.7
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;91.7
        movss     xmm3, DWORD PTR [_2il0floatpacket.4]          ;91.98
        movss     xmm2, DWORD PTR [_2il0floatpacket.5]          ;91.104
        mov       DWORD PTR [176+esp], edi                      ;
        mov       DWORD PTR [172+esp], edx                      ;
        mov       DWORD PTR [164+esp], ecx                      ;
        mov       DWORD PTR [156+esp], ebx                      ;
                                ; LOE esi
.B1.77:                         ; Preds .B1.99 .B1.76
        imul      eax, DWORD PTR [180+esp], -152                ;81.9
        xor       ebx, ebx                                      ;
        movss     xmm1, DWORD PTR [_2il0floatpacket.3]          ;
        add       eax, DWORD PTR [156+esp]                      ;81.9
        movsx     edx, BYTE PTR [184+eax+esi]                   ;81.18
        test      edx, edx                                      ;81.9
        mov       DWORD PTR [192+esp], edx                      ;81.18
        jle       .B1.86        ; Prob 3%                       ;81.9
                                ; LOE eax ebx esi xmm1
.B1.78:                         ; Preds .B1.77
        mov       ecx, DWORD PTR [216+eax+esi]                  ;82.16
        xor       edx, edx                                      ;
        mov       edi, DWORD PTR [220+eax+esi]                  ;82.16
        imul      edi, ecx                                      ;
        movss     xmm0, DWORD PTR [172+eax+esi]                 ;82.16
        mov       DWORD PTR [188+esp], ecx                      ;82.16
        mov       ecx, DWORD PTR [188+eax+esi]                  ;82.16
        mov       DWORD PTR [184+esp], esi                      ;
        sub       ecx, edi                                      ;
        xor       edi, edi                                      ;
                                ; LOE eax edx ecx ebx edi xmm0 xmm1
.B1.79:                         ; Preds .B1.84 .B1.78
        add       edi, DWORD PTR [188+esp]                      ;82.113
        inc       edx                                           ;82.113
        imul      esi, DWORD PTR [edi+ecx], 152                 ;82.46
        movss     xmm2, DWORD PTR [20+eax+esi]                  ;82.46
        addss     xmm2, xmm0                                    ;82.45
        comiss    xmm1, xmm2                                    ;82.113
        jbe       .B1.84        ; Prob 38%                      ;82.113
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1 xmm2
.B1.80:                         ; Preds .B1.79
        mov       ebx, DWORD PTR [184+esp]                      ;84.13
        movaps    xmm1, xmm2                                    ;83.7
        mov       ebx, DWORD PTR [180+eax+ebx]                  ;84.13
        cmp       ebx, DWORD PTR [24+eax+esi]                   ;84.47
        jne       .B1.83        ; Prob 50%                      ;84.47
                                ; LOE eax edx ecx esi edi xmm0 xmm1
.B1.81:                         ; Preds .B1.80
        mov       ebx, DWORD PTR [184+esp]                      ;84.125
        mov       ebx, DWORD PTR [176+eax+ebx]                  ;84.125
        cmp       ebx, DWORD PTR [28+eax+esi]                   ;84.159
        jne       .B1.83        ; Prob 50%                      ;84.159
                                ; LOE eax edx ecx edi xmm0 xmm1
.B1.82:                         ; Preds .B1.81
        mov       ebx, -1                                       ;85.10
        jmp       .B1.84        ; Prob 100%                     ;85.10
                                ; LOE eax edx ecx ebx edi xmm0 xmm1
.B1.83:                         ; Preds .B1.80 .B1.81
        xor       ebx, ebx                                      ;87.7
                                ; LOE eax edx ecx ebx edi xmm0 xmm1
.B1.84:                         ; Preds .B1.83 .B1.82 .B1.79
        cmp       edx, DWORD PTR [192+esp]                      ;81.9
        jb        .B1.79        ; Prob 82%                      ;81.9
                                ; LOE eax edx ecx ebx edi xmm0 xmm1
.B1.85:                         ; Preds .B1.84
        mov       esi, DWORD PTR [184+esp]                      ;
                                ; LOE ebx esi xmm1
.B1.86:                         ; Preds .B1.77 .B1.85
        imul      eax, DWORD PTR [172+esp], -900                ;91.20
        imul      edi, DWORD PTR [176+esp], 900                 ;91.20
        add       eax, DWORD PTR [164+esp]                      ;91.20
        movzx     ecx, WORD PTR [1582+eax+edi]                  ;91.61
        movsx     edx, BYTE PTR [1581+eax+edi]                  ;91.62
        add       ecx, edx                                      ;91.61
        movsx     eax, cx                                       ;91.24
        cvtsi2ss  xmm0, eax                                     ;91.24
        divss     xmm0, DWORD PTR [_2il0floatpacket.4]          ;91.98
        divss     xmm0, DWORD PTR [_2il0floatpacket.5]          ;91.104
        comiss    xmm0, xmm1                                    ;91.20
        jbe       .B1.99        ; Prob 50%                      ;91.20
                                ; LOE ebx esi edi xmm1
.B1.87:                         ; Preds .B1.86
        mov       DWORD PTR [16+esp], 0                         ;92.16
        lea       eax, DWORD PTR [108+esp]                      ;92.16
        mov       DWORD PTR [128+esp], eax                      ;92.16
        lea       ecx, DWORD PTR [16+esp]                       ;92.16
        lea       edx, DWORD PTR [128+esp]                      ;92.16
        push      32                                            ;92.16
        push      edx                                           ;92.16
        push      OFFSET FLAT: __STRLITPACK_13.0.1              ;92.16
        push      -2088435968                                   ;92.16
        push      511                                           ;92.16
        push      ecx                                           ;92.16
        movss     DWORD PTR [140+esp], xmm1                     ;92.16
        call      _for_inquire                                  ;92.16
                                ; LOE ebx esi edi
.B1.199:                        ; Preds .B1.87
        movss     xmm1, DWORD PTR [140+esp]                     ;
        add       esp, 24                                       ;92.16
                                ; LOE ebx esi edi xmm1
.B1.88:                         ; Preds .B1.199
        test      BYTE PTR [108+esp], 1                         ;93.16
        jne       .B1.91        ; Prob 40%                      ;93.16
                                ; LOE ebx esi edi xmm1
.B1.89:                         ; Preds .B1.88
        mov       DWORD PTR [16+esp], 0                         ;94.11
        lea       eax, DWORD PTR [48+esp]                       ;94.11
        mov       DWORD PTR [48+esp], 11                        ;94.11
        mov       DWORD PTR [52+esp], OFFSET FLAT: __STRLITPACK_12 ;94.11
        mov       DWORD PTR [56+esp], 7                         ;94.11
        mov       DWORD PTR [60+esp], OFFSET FLAT: __STRLITPACK_11 ;94.11
        push      32                                            ;94.11
        push      eax                                           ;94.11
        push      OFFSET FLAT: __STRLITPACK_14.0.1              ;94.11
        push      -2088435965                                   ;94.11
        push      511                                           ;94.11
        lea       edx, DWORD PTR [36+esp]                       ;94.11
        push      edx                                           ;94.11
        movss     DWORD PTR [140+esp], xmm1                     ;94.11
        call      _for_open                                     ;94.11
                                ; LOE eax ebx esi edi
.B1.200:                        ; Preds .B1.89
        movss     xmm1, DWORD PTR [140+esp]                     ;
        add       esp, 24                                       ;94.11
                                ; LOE eax ebx esi edi xmm1
.B1.90:                         ; Preds .B1.200
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;94.11
        test      eax, eax                                      ;95.20
        jne       .B1.160       ; Prob 5%                       ;95.20
                                ; LOE ebx esi edi xmm1
.B1.91:                         ; Preds .B1.208 .B1.88 .B1.90
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;100.10
        imul      eax, ecx, -152                                ;100.10
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;100.10
        mov       DWORD PTR [156+esp], edx                      ;100.10
        mov       DWORD PTR [180+esp], ecx                      ;100.10
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;102.37
        add       edx, esi                                      ;100.10
        mov       DWORD PTR [68+esp], eax                       ;100.10
        add       edi, ecx                                      ;
        movss     xmm0, DWORD PTR [172+eax+edx]                 ;100.10
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;102.37
        mov       DWORD PTR [164+esp], ecx                      ;102.37
        imul      ecx, eax, -900                                ;
        movss     DWORD PTR [8+esp], xmm0                       ;100.10
        mov       DWORD PTR [172+esp], eax                      ;102.37
        movzx     eax, WORD PTR [1582+ecx+edi]                  ;102.108
        movsx     edi, BYTE PTR [1581+ecx+edi]                  ;102.109
        add       eax, edi                                      ;102.108
        movsx     eax, ax                                       ;102.71
        test      bl, 1                                         ;101.15
        cvtsi2ss  xmm2, eax                                     ;102.71
        movss     DWORD PTR [12+esp], xmm2                      ;102.71
        jne       .B1.93        ; Prob 40%                      ;101.15
                                ; LOE edx esi xmm1
.B1.92:                         ; Preds .B1.91
        movss     xmm0, DWORD PTR [8+esp]                       ;102.5
        subss     xmm0, xmm1                                    ;102.5
        movss     xmm1, DWORD PTR [12+esp]                      ;102.145
        divss     xmm1, DWORD PTR [_2il0floatpacket.4]          ;102.145
        divss     xmm1, DWORD PTR [_2il0floatpacket.5]          ;102.151
        addss     xmm0, xmm1                                    ;102.67
        movss     DWORD PTR [112+esp], xmm0                     ;102.67
        jmp       .B1.94        ; Prob 100%                     ;102.67
                                ; LOE edx esi
.B1.93:                         ; Preds .B1.91
        movss     xmm0, DWORD PTR [12+esp]                      ;104.120
        divss     xmm0, DWORD PTR [_2il0floatpacket.4]          ;104.120
        divss     xmm0, DWORD PTR [_2il0floatpacket.5]          ;104.126
        mulss     xmm0, DWORD PTR [_2il0floatpacket.6]          ;104.11
        movss     DWORD PTR [112+esp], xmm0                     ;104.11
                                ; LOE edx esi
.B1.94:                         ; Preds .B1.92 .B1.93
        mov       eax, DWORD PTR [68+esp]                       ;102.5
        lea       ebx, DWORD PTR [136+esp]                      ;106.7
        movss     xmm0, DWORD PTR [112+esp]                     ;102.5
        mov       DWORD PTR [16+esp], 0                         ;106.7
        mov       ecx, DWORD PTR [180+eax+edx]                  ;106.7
        mov       DWORD PTR [136+esp], ecx                      ;106.7
        push      32                                            ;106.7
        push      OFFSET FLAT: PREP_DATA_STRUCTURE$format_pack.0.1+20 ;106.7
        push      ebx                                           ;106.7
        push      OFFSET FLAT: __STRLITPACK_17.0.1              ;106.7
        push      -2088435968                                   ;106.7
        push      511                                           ;106.7
        movss     DWORD PTR [172+eax+edx], xmm0                 ;102.5
        lea       edi, DWORD PTR [40+esp]                       ;106.7
        push      edi                                           ;106.7
        mov       DWORD PTR [148+esp], edx                      ;106.7
        call      _for_write_seq_fmt                            ;106.7
                                ; LOE esi
.B1.95:                         ; Preds .B1.94
        mov       eax, DWORD PTR [96+esp]                       ;106.7
        lea       ecx, DWORD PTR [172+esp]                      ;106.7
        mov       edx, DWORD PTR [148+esp]                      ;
        mov       edx, DWORD PTR [176+eax+edx]                  ;106.7
        mov       DWORD PTR [172+esp], edx                      ;106.7
        push      ecx                                           ;106.7
        push      OFFSET FLAT: __STRLITPACK_18.0.1              ;106.7
        lea       ebx, DWORD PTR [52+esp]                       ;106.7
        push      ebx                                           ;106.7
        call      _for_write_seq_fmt_xmit                       ;106.7
                                ; LOE esi
.B1.96:                         ; Preds .B1.95
        movss     xmm0, DWORD PTR [48+esp]                      ;106.7
        lea       eax, DWORD PTR [192+esp]                      ;106.7
        mulss     xmm0, DWORD PTR [_2il0floatpacket.7]          ;106.7
        movss     DWORD PTR [192+esp], xmm0                     ;106.7
        push      eax                                           ;106.7
        push      OFFSET FLAT: __STRLITPACK_19.0.1              ;106.7
        lea       edx, DWORD PTR [64+esp]                       ;106.7
        push      edx                                           ;106.7
        call      _for_write_seq_fmt_xmit                       ;106.7
                                ; LOE esi
.B1.97:                         ; Preds .B1.96
        movss     xmm0, DWORD PTR [64+esp]                      ;106.7
        lea       eax, DWORD PTR [212+esp]                      ;106.7
        movss     DWORD PTR [212+esp], xmm0                     ;106.7
        push      eax                                           ;106.7
        push      OFFSET FLAT: __STRLITPACK_20.0.1              ;106.7
        lea       edx, DWORD PTR [76+esp]                       ;106.7
        push      edx                                           ;106.7
        call      _for_write_seq_fmt_xmit                       ;106.7
                                ; LOE esi
.B1.98:                         ; Preds .B1.97
        movss     xmm0, DWORD PTR [176+esp]                     ;106.7
        lea       eax, DWORD PTR [232+esp]                      ;106.7
        mulss     xmm0, DWORD PTR [_2il0floatpacket.7]          ;106.7
        movss     DWORD PTR [232+esp], xmm0                     ;106.7
        push      eax                                           ;106.7
        push      OFFSET FLAT: __STRLITPACK_21.0.1              ;106.7
        lea       edx, DWORD PTR [88+esp]                       ;106.7
        push      edx                                           ;106.7
        call      _for_write_seq_fmt_xmit                       ;106.7
                                ; LOE esi
.B1.201:                        ; Preds .B1.98
        add       esp, 76                                       ;106.7
                                ; LOE esi
.B1.99:                         ; Preds .B1.201 .B1.86
        mov       eax, DWORD PTR [176+esp]                      ;78.7
        add       esi, 152                                      ;78.7
        inc       eax                                           ;78.7
        mov       DWORD PTR [176+esp], eax                      ;78.7
        cmp       eax, DWORD PTR [148+esp]                      ;78.7
        jb        .B1.77        ; Prob 82%                      ;78.7
                                ; LOE esi
.B1.100:                        ; Preds .B1.99
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFARCS] ;128.7
                                ; LOE eax
.B1.101:                        ; Preds .B1.56 .B1.100
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_BACKPOINTR+32] ;125.7
        mov       edx, 1                                        ;
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFNODES] ;114.7
        test      esi, esi                                      ;114.7
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_BACKPOINTR] ;125.7
        lea       edi, DWORD PTR [ecx*4]                        ;
        mov       DWORD PTR [68+esp], edi                       ;
        jle       .B1.118       ; Prob 2%                       ;114.7
                                ; LOE eax edx ecx ebx esi
.B1.102:                        ; Preds .B1.101
        mov       edi, ebx                                      ;
        sub       edi, DWORD PTR [68+esp]                       ;
        mov       DWORD PTR [156+esp], edi                      ;
        mov       edi, eax                                      ;
        shr       edi, 31                                       ;
        add       edi, eax                                      ;
        sar       edi, 1                                        ;
        mov       DWORD PTR [172+esp], edi                      ;
        imul      edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32], -152 ;
        add       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;
        mov       DWORD PTR [164+esp], 0                        ;
        mov       DWORD PTR [120+esp], edi                      ;
        mov       DWORD PTR [116+esp], 152                      ;
        mov       DWORD PTR [12+esp], ecx                       ;
        mov       DWORD PTR [8+esp], ebx                        ;
        mov       DWORD PTR [112+esp], esi                      ;
        mov       DWORD PTR [148+esp], eax                      ;
        mov       edi, DWORD PTR [164+esp]                      ;
                                ; LOE edx edi
.B1.103:                        ; Preds .B1.115 .B1.183 .B1.116 .B1.102
        mov       eax, DWORD PTR [156+esp]                      ;115.10
        cmp       DWORD PTR [148+esp], 0                        ;116.9
        mov       DWORD PTR [4+eax+edi*4], edx                  ;115.10
        jle       .B1.116       ; Prob 50%                      ;116.9
                                ; LOE edx edi
.B1.104:                        ; Preds .B1.103
        inc       edi                                           ;
        cmp       DWORD PTR [172+esp], 0                        ;116.9
        jbe       .B1.163       ; Prob 3%                       ;116.9
                                ; LOE edx edi
.B1.105:                        ; Preds .B1.104
        xor       esi, esi                                      ;
        mov       DWORD PTR [164+esp], edi                      ;
        xor       ebx, ebx                                      ;
        mov       ecx, DWORD PTR [116+esp]                      ;
        mov       eax, DWORD PTR [120+esp]                      ;
                                ; LOE eax edx ecx ebx esi
.B1.106:                        ; Preds .B1.110 .B1.105
        mov       edi, DWORD PTR [164+esp]                      ;117.16
        cmp       edi, DWORD PTR [176+ebx+eax]                  ;117.16
        jne       .B1.108       ; Prob 50%                      ;117.16
                                ; LOE eax edx ecx ebx esi
.B1.107:                        ; Preds .B1.106
        mov       edi, DWORD PTR [180+ebx+eax]                  ;118.13
        mov       DWORD PTR [4+ecx+eax], edi                    ;118.13
        lea       edi, DWORD PTR [1+esi+esi]                    ;119.13
        mov       DWORD PTR [16+ecx+eax], edi                   ;119.13
        add       ecx, 152                                      ;121.13
        mov       DWORD PTR [164+ebx+eax], edx                  ;120.13
        inc       edx                                           ;121.13
                                ; LOE eax edx ecx ebx esi
.B1.108:                        ; Preds .B1.107 .B1.106
        mov       edi, DWORD PTR [164+esp]                      ;117.16
        cmp       edi, DWORD PTR [328+ebx+eax]                  ;117.16
        jne       .B1.110       ; Prob 50%                      ;117.16
                                ; LOE eax edx ecx ebx esi
.B1.109:                        ; Preds .B1.108
        mov       edi, DWORD PTR [332+ebx+eax]                  ;118.13
        mov       DWORD PTR [4+ecx+eax], edi                    ;118.13
        lea       edi, DWORD PTR [2+esi+esi]                    ;119.13
        mov       DWORD PTR [16+ecx+eax], edi                   ;119.13
        add       ecx, 152                                      ;121.13
        mov       DWORD PTR [316+ebx+eax], edx                  ;120.13
        inc       edx                                           ;121.13
                                ; LOE eax edx ecx ebx esi
.B1.110:                        ; Preds .B1.109 .B1.108
        inc       esi                                           ;116.9
        add       ebx, 304                                      ;116.9
        cmp       esi, DWORD PTR [172+esp]                      ;116.9
        jb        .B1.106       ; Prob 64%                      ;116.9
                                ; LOE eax edx ecx ebx esi
.B1.111:                        ; Preds .B1.110
        mov       DWORD PTR [116+esp], ecx                      ;
        lea       ecx, DWORD PTR [1+esi+esi]                    ;116.9
        mov       edi, DWORD PTR [164+esp]                      ;
                                ; LOE edx ecx edi
.B1.112:                        ; Preds .B1.111 .B1.163
        lea       eax, DWORD PTR [-1+ecx]                       ;116.9
        cmp       eax, DWORD PTR [148+esp]                      ;116.9
        jae       .B1.183       ; Prob 3%                       ;116.9
                                ; LOE edx ecx edi
.B1.113:                        ; Preds .B1.112
        imul      eax, ecx, 152                                 ;117.16
        mov       ebx, DWORD PTR [120+esp]                      ;117.16
        cmp       edi, DWORD PTR [24+eax+ebx]                   ;117.16
        jne       .B1.115       ; Prob 50%                      ;117.16
                                ; LOE eax edx ecx ebx edi bl bh
.B1.114:                        ; Preds .B1.113
        mov       esi, ebx                                      ;118.13
        mov       DWORD PTR [164+esp], edi                      ;
        mov       edi, DWORD PTR [116+esp]                      ;118.13
        mov       ebx, DWORD PTR [28+eax+esi]                   ;118.13
        mov       DWORD PTR [12+eax+esi], edx                   ;120.13
        inc       edx                                           ;121.13
        mov       DWORD PTR [4+edi+esi], ebx                    ;118.13
        mov       DWORD PTR [16+edi+esi], ecx                   ;119.13
        add       edi, 152                                      ;121.13
        mov       DWORD PTR [116+esp], edi                      ;121.13
        mov       edi, DWORD PTR [164+esp]                      ;121.13
                                ; LOE edx edi
.B1.115:                        ; Preds .B1.114 .B1.113
        cmp       edi, DWORD PTR [112+esp]                      ;115.10
        jb        .B1.103       ; Prob 82%                      ;115.10
        jmp       .B1.117       ; Prob 100%                     ;115.10
                                ; LOE edx edi
.B1.116:                        ; Preds .B1.103
        inc       edi                                           ;115.10
        cmp       edi, DWORD PTR [112+esp]                      ;115.10
        jb        .B1.103       ; Prob 82%                      ;115.10
                                ; LOE edx edi
.B1.117:                        ; Preds .B1.183 .B1.115 .B1.116 ; Infreq
        mov       ecx, DWORD PTR [12+esp]                       ;
        mov       ebx, DWORD PTR [8+esp]                        ;
        mov       esi, DWORD PTR [112+esp]                      ;
        mov       eax, DWORD PTR [148+esp]                      ;
                                ; LOE eax edx ecx ebx esi
.B1.118:                        ; Preds .B1.101 .B1.117         ; Infreq
        lea       ebx, DWORD PTR [ebx+esi*4]                    ;125.7
        mov       esi, DWORD PTR [72+esp]                       ;158.7
        sub       ebx, DWORD PTR [68+esp]                       ;125.7
        test      eax, eax                                      ;128.7
        mov       DWORD PTR [4+ebx], edx                        ;125.7
        jle       .B1.138       ; Prob 2%                       ;128.7
                                ; LOE eax ecx esi
.B1.119:                        ; Preds .B1.118                 ; Infreq
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;148.10
        mov       edi, 1                                        ;
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;148.10
        mov       DWORD PTR [212+esp], ebx                      ;148.10
        mov       ebx, 152                                      ;
        mov       DWORD PTR [232+esp], edx                      ;
        mov       DWORD PTR [68+esp], edi                       ;
        mov       DWORD PTR [248+esp], esi                      ;
        mov       DWORD PTR [148+esp], eax                      ;
                                ; LOE ecx ebx
.B1.120:                        ; Preds .B1.209 .B1.119         ; Infreq
        imul      esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32], -152 ;130.9
        mov       edi, ebx                                      ;130.9
        add       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;130.9
        shl       ecx, 2                                        ;130.9
        neg       ecx                                           ;130.9
        add       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_BACKPOINTR] ;130.9
        mov       edx, DWORD PTR [28+esi+edi]                   ;130.69
        mov       eax, DWORD PTR [4+ecx+edx*4]                  ;130.21
        sub       eax, DWORD PTR [ecx+edx*4]                    ;130.21
        mov       ecx, 1                                        ;130.9
        test      eax, eax                                      ;130.9
        cmovle    eax, ecx                                      ;130.9
        mov       DWORD PTR [216+esp], eax                      ;130.9
        lea       eax, DWORD PTR [-1+eax]                       ;131.20
        cmp       eax, 9                                        ;131.20
        jbe       .B1.122       ; Prob 50%                      ;131.20
                                ; LOE ebx esi edi
.B1.121:                        ; Preds .B1.120                 ; Infreq
        mov       DWORD PTR [16+esp], 0                         ;132.11
        lea       edx, DWORD PTR [16+esp]                       ;132.11
        mov       DWORD PTR [8+esp], 24                         ;132.11
        lea       eax, DWORD PTR [8+esp]                        ;132.11
        mov       DWORD PTR [12+esp], OFFSET FLAT: __STRLITPACK_6 ;132.11
        push      32                                            ;132.11
        push      eax                                           ;132.11
        push      OFFSET FLAT: __STRLITPACK_22.0.1              ;132.11
        push      -2088435968                                   ;132.11
        push      -1                                            ;132.11
        push      edx                                           ;132.11
        call      _for_write_seq_lis                            ;132.11
                                ; LOE ebx esi edi
.B1.202:                        ; Preds .B1.121                 ; Infreq
        add       esp, 24                                       ;132.11
                                ; LOE ebx esi edi
.B1.122:                        ; Preds .B1.202 .B1.120         ; Infreq
        movsx     esi, BYTE PTR [108+esi+edi]                   ;134.16
        test      esi, esi                                      ;134.6
        jle       .B1.136       ; Prob 2%                       ;134.6
                                ; LOE ebx esi
.B1.123:                        ; Preds .B1.122                 ; Infreq
        mov       ecx, DWORD PTR [216+esp]                      ;148.10
        xor       edi, edi                                      ;148.10
        test      ecx, ecx                                      ;148.10
        cmovg     edi, ecx                                      ;148.10
        mov       eax, 1                                        ;148.10
        mov       edx, DWORD PTR [104+esp]                      ;153.9
        mov       DWORD PTR [220+esp], edi                      ;148.10
        mov       DWORD PTR [252+esp], eax                      ;148.10
        mov       DWORD PTR [256+esp], esi                      ;148.10
        mov       DWORD PTR [260+esp], ebx                      ;148.10
                                ; LOE edx
.B1.124:                        ; Preds .B1.134 .B1.123         ; Infreq
        imul      ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32], 152 ;136.11
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;136.11
        mov       eax, DWORD PTR [260+esp]                      ;136.11
        lea       esi, DWORD PTR [eax+ecx]                      ;136.11
        mov       eax, DWORD PTR [252+esp]                      ;136.11
        sub       esi, ebx                                      ;136.11
        sub       eax, DWORD PTR [144+esi]                      ;136.11
        imul      eax, DWORD PTR [140+esi]                      ;136.11
        mov       edi, DWORD PTR [112+esi]                      ;136.11
        imul      esi, DWORD PTR [edi+eax], 152                 ;136.19
        add       ecx, esi                                      ;136.11
        lea       esi, DWORD PTR [edx*4]                        ;
        sub       ecx, ebx                                      ;136.11
        mov       edi, DWORD PTR [248+esp]                      ;
        mov       ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_AGGINT] ;139.9
        mov       ecx, DWORD PTR [12+ecx]                       ;136.11
        lea       eax, DWORD PTR [edi+ecx*4]                    ;
        sub       eax, esi                                      ;
        test      ebx, ebx                                      ;139.9
        jle       .B1.129       ; Prob 2%                       ;139.9
                                ; LOE eax edx ecx ebx
.B1.125:                        ; Preds .B1.124                 ; Infreq
        imul      edi, ecx, 900                                 ;136.11
        mov       esi, 1                                        ;
        mov       DWORD PTR [240+esp], ecx                      ;136.11
        mov       DWORD PTR [244+esp], edi                      ;136.11
        mov       ecx, DWORD PTR [248+esp]                      ;136.11
                                ; LOE eax edx ecx ebx esi
.B1.126:                        ; Preds .B1.127 .B1.125         ; Infreq
        test      BYTE PTR [eax], 1                             ;140.18
        je        .B1.150       ; Prob 7%                       ;140.18
                                ; LOE eax edx ecx ebx esi
.B1.127:                        ; Preds .B1.156 .B1.126         ; Infreq
        inc       esi                                           ;145.9
        cmp       esi, ebx                                      ;145.9
        jle       .B1.126       ; Prob 82%                      ;145.9
                                ; LOE eax edx ecx ebx esi
.B1.128:                        ; Preds .B1.127                 ; Infreq
        mov       DWORD PTR [248+esp], ecx                      ;
        mov       ecx, DWORD PTR [240+esp]                      ;
                                ; LOE eax edx ecx
.B1.129:                        ; Preds .B1.124 .B1.128         ; Infreq
        mov       ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIMPERAGG] ;146.9
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TDSPSTEP] ;146.9
        cmp       ebx, esi                                      ;146.9
        cmovge    esi, ebx                                      ;146.9
        test      esi, esi                                      ;146.9
        jle       .B1.134       ; Prob 2%                       ;146.9
                                ; LOE eax edx ecx esi
.B1.130:                        ; Preds .B1.129                 ; Infreq
        imul      edi, ecx, 900                                 ;136.11
        mov       ebx, 1                                        ;
        mov       DWORD PTR [240+esp], ecx                      ;136.11
        mov       DWORD PTR [236+esp], edi                      ;136.11
        mov       DWORD PTR [276+esp], esi                      ;136.11
        mov       ecx, DWORD PTR [232+esp]                      ;136.11
                                ; LOE eax edx ecx ebx
.B1.131:                        ; Preds .B1.132 .B1.130         ; Infreq
        test      BYTE PTR [eax], 1                             ;147.18
        je        .B1.143       ; Prob 7%                       ;147.18
                                ; LOE eax edx ecx ebx
.B1.132:                        ; Preds .B1.149 .B1.131         ; Infreq
        inc       ebx                                           ;152.9
        cmp       ebx, DWORD PTR [276+esp]                      ;152.9
        jle       .B1.131       ; Prob 82%                      ;152.9
                                ; LOE eax edx ecx ebx
.B1.133:                        ; Preds .B1.132                 ; Infreq
        mov       DWORD PTR [232+esp], ecx                      ;
                                ; LOE eax edx
.B1.134:                        ; Preds .B1.133 .B1.129         ; Infreq
        mov       DWORD PTR [eax], -1                           ;153.9
        mov       eax, DWORD PTR [252+esp]                      ;154.6
        inc       eax                                           ;154.6
        mov       DWORD PTR [252+esp], eax                      ;154.6
        cmp       eax, DWORD PTR [256+esp]                      ;154.6
        jle       .B1.124       ; Prob 82%                      ;154.6
                                ; LOE edx
.B1.135:                        ; Preds .B1.134                 ; Infreq
        mov       ebx, DWORD PTR [260+esp]                      ;
                                ; LOE ebx
.B1.136:                        ; Preds .B1.135 .B1.122         ; Infreq
        mov       eax, DWORD PTR [68+esp]                       ;156.7
        add       ebx, 152                                      ;156.7
        inc       eax                                           ;156.7
        mov       DWORD PTR [68+esp], eax                       ;156.7
        cmp       eax, DWORD PTR [148+esp]                      ;156.7
        jg        .B1.137       ; Prob 18%                      ;156.7
                                ; LOE ebx
.B1.209:                        ; Preds .B1.136                 ; Infreq
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_BACKPOINTR+32] ;125.7
        jmp       .B1.120       ; Prob 100%                     ;125.7
                                ; LOE ecx ebx
.B1.137:                        ; Preds .B1.136                 ; Infreq
        mov       esi, DWORD PTR [248+esp]                      ;
                                ; LOE esi
.B1.138:                        ; Preds .B1.118 .B1.137         ; Infreq
        mov       eax, DWORD PTR [84+esp]                       ;158.7
        mov       edx, eax                                      ;158.7
        shr       edx, 1                                        ;158.7
        and       eax, 1                                        ;158.7
        and       edx, 1                                        ;158.7
        add       eax, eax                                      ;158.7
        shl       edx, 2                                        ;158.7
        or        edx, eax                                      ;158.7
        or        edx, 262144                                   ;158.7
        push      edx                                           ;158.7
        push      esi                                           ;158.7
        call      _for_dealloc_allocatable                      ;158.7
                                ; LOE
.B1.140:                        ; Preds .B1.138                 ; Infreq
        add       esp, 300                                      ;167.1
        pop       ebx                                           ;167.1
        pop       edi                                           ;167.1
        pop       esi                                           ;167.1
        mov       esp, ebp                                      ;167.1
        pop       ebp                                           ;167.1
        ret                                                     ;167.1
                                ; LOE
.B1.143:                        ; Preds .B1.131                 ; Infreq
        imul      edi, DWORD PTR [212+esp], -900                ;148.10
        lea       esi, DWORD PTR [ebx+ebx*4]                    ;148.10
        add       ecx, DWORD PTR [236+esp]                      ;148.10
        mov       DWORD PTR [228+esp], esi                      ;148.10
        mov       edx, DWORD PTR [84+edi+ecx]                   ;148.19
        lea       eax, DWORD PTR [edx+esi*8]                    ;148.10
        mov       edx, 1                                        ;148.10
        imul      esi, DWORD PTR [116+edi+ecx], -40             ;148.10
        mov       ecx, 4                                        ;148.10
        mov       edi, DWORD PTR [220+esp]                      ;148.10
        mov       DWORD PTR [12+esi+eax], 5                     ;148.10
        mov       DWORD PTR [4+esi+eax], ecx                    ;148.10
        mov       DWORD PTR [16+esi+eax], edx                   ;148.10
        mov       DWORD PTR [8+esi+eax], 0                      ;148.10
        mov       DWORD PTR [32+esi+eax], edx                   ;148.10
        mov       DWORD PTR [24+esi+eax], edi                   ;148.10
        mov       DWORD PTR [28+esi+eax], ecx                   ;148.10
        lea       eax, DWORD PTR [208+esp]                      ;148.10
        push      ecx                                           ;148.10
        push      edi                                           ;148.10
        push      2                                             ;148.10
        push      eax                                           ;148.10
        call      _for_check_mult_overflow                      ;148.10
                                ; LOE eax ebx
.B1.144:                        ; Preds .B1.143                 ; Infreq
        imul      edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32], -900 ;148.10
        and       eax, 1                                        ;148.10
        add       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;148.10
        mov       ecx, DWORD PTR [252+esp]                      ;148.10
        mov       edi, DWORD PTR [244+esp]                      ;148.10
        shl       eax, 4                                        ;148.10
        imul      esi, DWORD PTR [116+edx+ecx], -40             ;148.10
        or        eax, 262144                                   ;148.10
        add       esi, DWORD PTR [84+edx+ecx]                   ;148.10
        push      eax                                           ;148.10
        lea       edx, DWORD PTR [esi+edi*8]                    ;148.10
        push      edx                                           ;148.10
        push      DWORD PTR [232+esp]                           ;148.10
        call      _for_allocate                                 ;148.10
                                ; LOE ebx
.B1.205:                        ; Preds .B1.144                 ; Infreq
        add       esp, 28                                       ;148.10
                                ; LOE ebx
.B1.145:                        ; Preds .B1.205                 ; Infreq
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;149.10
        imul      edx, edi, -900                                ;149.10
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;149.10
        mov       eax, DWORD PTR [236+esp]                      ;149.10
        mov       DWORD PTR [212+esp], edi                      ;149.10
        mov       edi, DWORD PTR [228+esp]                      ;149.10
        lea       esi, DWORD PTR [eax+ecx]                      ;149.10
        mov       eax, DWORD PTR [84+edx+esi]                   ;149.10
        imul      edx, DWORD PTR [116+edx+esi], -40             ;149.10
        lea       eax, DWORD PTR [eax+edi*8]                    ;149.10
        mov       edi, DWORD PTR [24+edx+eax]                   ;149.10
        test      edi, edi                                      ;149.10
        mov       esi, DWORD PTR [32+edx+eax]                   ;149.10
        mov       DWORD PTR [272+esp], edi                      ;149.10
        jle       .B1.149       ; Prob 10%                      ;149.10
                                ; LOE eax edx ecx ebx esi
.B1.146:                        ; Preds .B1.145                 ; Infreq
        mov       edi, DWORD PTR [edx+eax]                      ;149.10
        mov       DWORD PTR [192+esp], edi                      ;149.10
        mov       DWORD PTR [112+esp], edx                      ;
        mov       edi, DWORD PTR [28+edx+eax]                   ;149.10
        mov       edx, esi                                      ;
        imul      edx, edi                                      ;
        imul      esi, edi                                      ;
        mov       DWORD PTR [196+esp], 1                        ;
        mov       DWORD PTR [200+esp], edx                      ;
        mov       DWORD PTR [232+esp], ecx                      ;
        mov       DWORD PTR [120+esp], ebx                      ;
        mov       DWORD PTR [116+esp], eax                      ;
        mov       edx, DWORD PTR [200+esp]                      ;
        mov       ecx, edi                                      ;
        mov       ebx, DWORD PTR [192+esp]                      ;
        mov       edi, DWORD PTR [196+esp]                      ;
                                ; LOE edx ecx ebx esi edi
.B1.147:                        ; Preds .B1.147 .B1.146         ; Infreq
        mov       eax, ebx                                      ;149.10
        inc       edi                                           ;149.10
        sub       eax, esi                                      ;149.10
        mov       DWORD PTR [edx+eax], 0                        ;149.10
        add       edx, ecx                                      ;149.10
        cmp       edi, DWORD PTR [272+esp]                      ;149.10
        jle       .B1.147       ; Prob 82%                      ;149.10
                                ; LOE edx ecx ebx esi edi
.B1.148:                        ; Preds .B1.147                 ; Infreq
        mov       edx, DWORD PTR [112+esp]                      ;
        mov       eax, DWORD PTR [116+esp]                      ;
        mov       ecx, DWORD PTR [232+esp]                      ;
        mov       ebx, DWORD PTR [120+esp]                      ;
                                ; LOE eax edx ecx ebx
.B1.149:                        ; Preds .B1.148 .B1.145         ; Infreq
        mov       esi, DWORD PTR [216+esp]                      ;150.10
        mov       WORD PTR [36+edx+eax], si                     ;150.10
        mov       eax, DWORD PTR [72+esp]                       ;153.9
        mov       edx, DWORD PTR [104+esp]                      ;153.9
        mov       edi, DWORD PTR [240+esp]                      ;
        mov       DWORD PTR [248+esp], eax                      ;153.9
        lea       esi, DWORD PTR [edx*4]                        ;
        lea       eax, DWORD PTR [eax+edi*4]                    ;
        sub       eax, esi                                      ;
        jmp       .B1.132       ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx
.B1.150:                        ; Preds .B1.126                 ; Infreq
        imul      edx, DWORD PTR [212+esp], -900                ;141.10
        lea       ecx, DWORD PTR [esi+esi*4]                    ;141.10
        mov       eax, DWORD PTR [232+esp]                      ;141.10
        add       eax, DWORD PTR [244+esp]                      ;141.10
        mov       DWORD PTR [224+esp], ecx                      ;141.10
        imul      edi, DWORD PTR [188+edx+eax], -40             ;141.10
        add       edi, DWORD PTR [156+edx+eax]                  ;141.10
        mov       eax, 1                                        ;141.10
        mov       edx, 2                                        ;141.10
        mov       DWORD PTR [16+edi+ecx*8], eax                 ;141.10
        mov       DWORD PTR [32+edi+ecx*8], eax                 ;141.10
        mov       eax, DWORD PTR [220+esp]                      ;141.10
        mov       DWORD PTR [12+edi+ecx*8], 5                   ;141.10
        mov       DWORD PTR [4+edi+ecx*8], edx                  ;141.10
        mov       DWORD PTR [8+edi+ecx*8], 0                    ;141.10
        mov       DWORD PTR [24+edi+ecx*8], eax                 ;141.10
        mov       DWORD PTR [28+edi+ecx*8], edx                 ;141.10
        lea       ecx, DWORD PTR [204+esp]                      ;141.10
        push      edx                                           ;141.10
        push      eax                                           ;141.10
        push      edx                                           ;141.10
        push      ecx                                           ;141.10
        call      _for_check_mult_overflow                      ;141.10
                                ; LOE eax ebx esi
.B1.151:                        ; Preds .B1.150                 ; Infreq
        imul      ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32], -900 ;141.10
        and       eax, 1                                        ;141.10
        add       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;141.10
        mov       edi, DWORD PTR [260+esp]                      ;141.10
        shl       eax, 4                                        ;141.10
        or        eax, 262144                                   ;141.10
        imul      edx, DWORD PTR [188+ecx+edi], -40             ;141.10
        add       edx, DWORD PTR [156+ecx+edi]                  ;141.10
        mov       ecx, DWORD PTR [240+esp]                      ;141.10
        push      eax                                           ;141.10
        lea       edx, DWORD PTR [edx+ecx*8]                    ;141.10
        push      edx                                           ;141.10
        push      DWORD PTR [228+esp]                           ;141.10
        call      _for_allocate                                 ;141.10
                                ; LOE ebx esi
.B1.207:                        ; Preds .B1.151                 ; Infreq
        add       esp, 28                                       ;141.10
                                ; LOE ebx esi
.B1.152:                        ; Preds .B1.207                 ; Infreq
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;142.10
        mov       DWORD PTR [212+esp], edx                      ;142.10
        imul      edx, edx, -900                                ;142.10
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;142.10
        mov       eax, DWORD PTR [244+esp]                      ;142.10
        mov       DWORD PTR [232+esp], edi                      ;142.10
        lea       ecx, DWORD PTR [eax+edi]                      ;142.10
        mov       edi, DWORD PTR [224+esp]                      ;142.10
        mov       eax, DWORD PTR [156+edx+ecx]                  ;142.10
        imul      ecx, DWORD PTR [188+edx+ecx], -40             ;142.10
        lea       eax, DWORD PTR [eax+edi*8]                    ;142.10
        mov       edi, DWORD PTR [24+ecx+eax]                   ;142.10
        test      edi, edi                                      ;142.10
        mov       edx, DWORD PTR [32+ecx+eax]                   ;142.10
        mov       DWORD PTR [268+esp], edi                      ;142.10
        jle       .B1.156       ; Prob 10%                      ;142.10
                                ; LOE eax edx ecx ebx esi
.B1.153:                        ; Preds .B1.152                 ; Infreq
        mov       edi, DWORD PTR [ecx+eax]                      ;142.10
        mov       DWORD PTR [180+esp], edi                      ;142.10
        mov       DWORD PTR [156+esp], ecx                      ;
        mov       edi, DWORD PTR [28+ecx+eax]                   ;142.10
        mov       ecx, edx                                      ;
        imul      ecx, edi                                      ;
        imul      edx, edi                                      ;
        mov       DWORD PTR [184+esp], 1                        ;
        mov       DWORD PTR [188+esp], ecx                      ;
        mov       DWORD PTR [172+esp], esi                      ;
        mov       DWORD PTR [176+esp], ebx                      ;
        mov       DWORD PTR [264+esp], edx                      ;
        mov       DWORD PTR [164+esp], eax                      ;
        mov       ecx, DWORD PTR [188+esp]                      ;
        mov       ebx, edi                                      ;
        mov       esi, DWORD PTR [180+esp]                      ;
        mov       edi, DWORD PTR [184+esp]                      ;
                                ; LOE ecx ebx esi edi
.B1.154:                        ; Preds .B1.154 .B1.153         ; Infreq
        mov       edx, esi                                      ;142.10
        inc       edi                                           ;142.10
        sub       edx, DWORD PTR [264+esp]                      ;142.10
        xor       eax, eax                                      ;142.10
        mov       WORD PTR [ecx+edx], ax                        ;142.10
        add       ecx, ebx                                      ;142.10
        cmp       edi, DWORD PTR [268+esp]                      ;142.10
        jle       .B1.154       ; Prob 82%                      ;142.10
                                ; LOE ecx ebx esi edi
.B1.155:                        ; Preds .B1.154                 ; Infreq
        mov       ecx, DWORD PTR [156+esp]                      ;
        mov       eax, DWORD PTR [164+esp]                      ;
        mov       esi, DWORD PTR [172+esp]                      ;
        mov       ebx, DWORD PTR [176+esp]                      ;
                                ; LOE eax ecx ebx esi
.B1.156:                        ; Preds .B1.155 .B1.152         ; Infreq
        mov       edx, DWORD PTR [216+esp]                      ;143.10
        mov       WORD PTR [36+ecx+eax], dx                     ;143.10
        mov       ecx, DWORD PTR [72+esp]                       ;153.9
        mov       eax, DWORD PTR [240+esp]                      ;
        mov       edx, DWORD PTR [104+esp]                      ;153.9
        lea       eax, DWORD PTR [ecx+eax*4]                    ;
        lea       edi, DWORD PTR [edx*4]                        ;
        sub       eax, edi                                      ;
        jmp       .B1.127       ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B1.157:                        ; Preds .B1.30                  ; Infreq
        mov       esi, 1                                        ;
        jmp       .B1.34        ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B1.158:                        ; Preds .B1.39                  ; Infreq
        mov       esi, 1                                        ;
        jmp       .B1.43        ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B1.159:                        ; Preds .B1.48                  ; Infreq
        mov       ebx, 1                                        ;
        jmp       .B1.52        ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx
.B1.160:                        ; Preds .B1.90                  ; Infreq
        mov       DWORD PTR [16+esp], 0                         ;96.16
        lea       eax, DWORD PTR [esp]                          ;96.16
        mov       DWORD PTR [esp], 30                           ;96.16
        mov       DWORD PTR [4+esp], OFFSET FLAT: __STRLITPACK_9 ;96.16
        push      32                                            ;96.16
        push      eax                                           ;96.16
        push      OFFSET FLAT: __STRLITPACK_15.0.1              ;96.16
        push      -2088435968                                   ;96.16
        push      911                                           ;96.16
        lea       edx, DWORD PTR [36+esp]                       ;96.16
        push      edx                                           ;96.16
        movss     DWORD PTR [140+esp], xmm1                     ;96.16
        call      _for_write_seq_lis                            ;96.16
                                ; LOE ebx esi edi
.B1.161:                        ; Preds .B1.160                 ; Infreq
        xor       eax, eax                                      ;97.13
        push      32                                            ;97.13
        push      eax                                           ;97.13
        push      eax                                           ;97.13
        push      -2088435968                                   ;97.13
        push      eax                                           ;97.13
        push      OFFSET FLAT: __STRLITPACK_16                  ;97.13
        call      _for_stop_core                                ;97.13
                                ; LOE ebx esi edi
.B1.208:                        ; Preds .B1.161                 ; Infreq
        movss     xmm1, DWORD PTR [164+esp]                     ;
        add       esp, 48                                       ;97.13
        jmp       .B1.91        ; Prob 100%                     ;97.13
                                ; LOE ebx esi edi xmm1
.B1.163:                        ; Preds .B1.104                 ; Infreq
        mov       ecx, 1                                        ;
        jmp       .B1.112       ; Prob 100%                     ;
                                ; LOE edx ecx edi
.B1.164:                        ; Preds .B1.4                   ; Infreq
        cmp       edx, 4                                        ;33.7
        jl        .B1.180       ; Prob 10%                      ;33.7
                                ; LOE edx ecx
.B1.165:                        ; Preds .B1.164                 ; Infreq
        mov       eax, ecx                                      ;33.7
        and       eax, 15                                       ;33.7
        je        .B1.168       ; Prob 50%                      ;33.7
                                ; LOE eax edx ecx
.B1.166:                        ; Preds .B1.165                 ; Infreq
        test      al, 3                                         ;33.7
        jne       .B1.180       ; Prob 10%                      ;33.7
                                ; LOE eax edx ecx
.B1.167:                        ; Preds .B1.166                 ; Infreq
        neg       eax                                           ;33.7
        add       eax, 16                                       ;33.7
        shr       eax, 2                                        ;33.7
                                ; LOE eax edx ecx
.B1.168:                        ; Preds .B1.167 .B1.165         ; Infreq
        lea       ebx, DWORD PTR [4+eax]                        ;33.7
        cmp       edx, ebx                                      ;33.7
        jl        .B1.180       ; Prob 10%                      ;33.7
                                ; LOE eax edx ecx
.B1.169:                        ; Preds .B1.168                 ; Infreq
        mov       esi, edx                                      ;33.7
        sub       esi, eax                                      ;33.7
        and       esi, 3                                        ;33.7
        neg       esi                                           ;33.7
        add       esi, edx                                      ;33.7
        test      eax, eax                                      ;33.7
        jbe       .B1.173       ; Prob 10%                      ;33.7
                                ; LOE eax edx ecx esi
.B1.170:                        ; Preds .B1.169                 ; Infreq
        xor       ebx, ebx                                      ;
                                ; LOE eax edx ecx ebx esi
.B1.171:                        ; Preds .B1.171 .B1.170         ; Infreq
        mov       DWORD PTR [ecx+ebx*4], 0                      ;33.7
        inc       ebx                                           ;33.7
        cmp       ebx, eax                                      ;33.7
        jb        .B1.171       ; Prob 82%                      ;33.7
                                ; LOE eax edx ecx ebx esi
.B1.173:                        ; Preds .B1.171 .B1.169         ; Infreq
        pxor      xmm0, xmm0                                    ;33.7
                                ; LOE eax edx ecx esi xmm0
.B1.174:                        ; Preds .B1.174 .B1.173         ; Infreq
        movdqa    XMMWORD PTR [ecx+eax*4], xmm0                 ;33.7
        add       eax, 4                                        ;33.7
        cmp       eax, esi                                      ;33.7
        jb        .B1.174       ; Prob 82%                      ;33.7
                                ; LOE eax edx ecx esi xmm0
.B1.176:                        ; Preds .B1.174 .B1.180         ; Infreq
        cmp       esi, edx                                      ;33.7
        jae       .B1.6         ; Prob 10%                      ;33.7
                                ; LOE edx ecx esi
.B1.178:                        ; Preds .B1.176 .B1.178         ; Infreq
        mov       DWORD PTR [ecx+esi*4], 0                      ;33.7
        inc       esi                                           ;33.7
        cmp       esi, edx                                      ;33.7
        jb        .B1.178       ; Prob 82%                      ;33.7
        jmp       .B1.6         ; Prob 100%                     ;33.7
                                ; LOE edx ecx esi
.B1.180:                        ; Preds .B1.164 .B1.168 .B1.166 ; Infreq
        xor       esi, esi                                      ;33.7
        jmp       .B1.176       ; Prob 100%                     ;33.7
                                ; LOE edx ecx esi
.B1.183:                        ; Preds .B1.112                 ; Infreq
        cmp       edi, DWORD PTR [112+esp]                      ;115.10
        jb        .B1.103       ; Prob 82%                      ;115.10
        jmp       .B1.117       ; Prob 100%                     ;115.10
                                ; LOE edx edi
.B1.185:                        ; Preds .B1.58                  ; Infreq
        mov       eax, 1                                        ;
        jmp       .B1.70        ; Prob 100%                     ;
                                ; LOE eax edx ebx
.B1.186:                        ; Preds .B1.8                   ; Infreq
        mov       eax, 1                                        ;
        jmp       .B1.20        ; Prob 100%                     ;
                                ; LOE eax ecx ebx esi
.B1.187:                        ; Preds .B1.6                   ; Infreq
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_MAXMOVE], 0 ;34.7
        jmp       .B1.56        ; Prob 100%                     ;34.7
        ALIGN     16
                                ; LOE eax
; mark_end;
_PREP_DATA_STRUCTURE ENDP
_TEXT	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
PREP_DATA_STRUCTURE$format_pack.0.1	DB	54
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	20
	DB	0
	DB	0
	DB	0
	DB	4
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	12
	DB	0
	DB	83
	DB	104
	DB	111
	DB	114
	DB	116
	DB	32
	DB	108
	DB	105
	DB	110
	DB	107
	DB	32
	DB	40
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	4
	DB	0
	DB	32
	DB	45
	DB	62
	DB	32
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	10
	DB	0
	DB	41
	DB	32
	DB	108
	DB	101
	DB	110
	DB	103
	DB	116
	DB	104
	DB	61
	DB	32
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	1
	DB	1
	DB	0
	DB	0
	DB	0
	DB	8
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	11
	DB	0
	DB	32
	DB	98
	DB	99
	DB	115
	DB	32
	DB	86
	DB	77
	DB	65
	DB	88
	DB	61
	DB	32
	DB	0
	DB	33
	DB	0
	DB	0
	DB	1
	DB	1
	DB	0
	DB	0
	DB	0
	DB	8
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	12
	DB	0
	DB	32
	DB	44
	DB	32
	DB	82
	DB	101
	DB	115
	DB	101
	DB	116
	DB	32
	DB	116
	DB	111
	DB	32
	DB	33
	DB	0
	DB	0
	DB	1
	DB	1
	DB	0
	DB	0
	DB	0
	DB	8
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	6
	DB	0
	DB	32
	DB	32
	DB	102
	DB	101
	DB	101
	DB	116
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
__STRLITPACK_13.0.1	DB	9
	DB	3
	DB	35
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_14.0.1	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	26
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_15.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_17.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_18.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_19.0.1	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_20.0.1	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_21.0.1	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_22.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _PREP_DATA_STRUCTURE
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_12	DB	87
	DB	97
	DB	114
	DB	110
	DB	105
	DB	110
	DB	103
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
__STRLITPACK_11	DB	117
	DB	110
	DB	107
	DB	110
	DB	111
	DB	119
	DB	110
	DB	0
__STRLITPACK_9	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	119
	DB	104
	DB	101
	DB	110
	DB	32
	DB	111
	DB	112
	DB	101
	DB	110
	DB	105
	DB	110
	DB	103
	DB	32
	DB	87
	DB	97
	DB	114
	DB	110
	DB	105
	DB	110
	DB	103
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_16	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_6	DB	104
	DB	105
	DB	103
	DB	104
	DB	32
	DB	110
	DB	117
	DB	109
	DB	98
	DB	101
	DB	114
	DB	32
	DB	111
	DB	102
	DB	32
	DB	109
	DB	111
	DB	118
	DB	101
	DB	109
	DB	101
	DB	110
	DB	116
	DB	115
	DB	0
	DB 3 DUP ( 0H)	; pad
_2il0floatpacket.3	DD	0447a0000H
_2il0floatpacket.4	DD	042700000H
_2il0floatpacket.5	DD	041200000H
_2il0floatpacket.6	DD	03f000000H
_2il0floatpacket.7	DD	045a50000H
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
EXTRN	_DYNUST_MAIN_MODULE_mp_TDSPSTEP:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_SIMPERAGG:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_AGGINT:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_BACKPOINTR:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NOOFNODES:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_MAXMOVE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NOOFARCS:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ERROR:BYTE
_DATA	ENDS
EXTRN	_for_dealloc_allocatable:PROC
EXTRN	_for_write_seq_fmt_xmit:PROC
EXTRN	_for_write_seq_fmt:PROC
EXTRN	_for_stop_core:PROC
EXTRN	_for_write_seq_lis:PROC
EXTRN	_for_open:PROC
EXTRN	_for_inquire:PROC
EXTRN	_for_allocate:PROC
EXTRN	_for_alloc_allocatable:PROC
EXTRN	_for_check_mult_overflow:PROC
EXTRN	__intel_fast_memset:PROC
EXTRN	__fltused:BYTE
	INCLUDELIB <ifconsol>
	INCLUDELIB <libifcoremt>
	INCLUDELIB <libifport>
	INCLUDELIB <libmmt>
	INCLUDELIB <LIBCMT>
	INCLUDELIB <libirc>
	INCLUDELIB <svml_dispmt>
	INCLUDELIB <OLDNAMES>
	END
