; mark_description "Intel(R) Visual Fortran Compiler XE for applications running on IA-32, Version 13.1.1.171 Build 20130313";
; mark_description "-S";
	.686P
 	.387
	OPTION DOTNAME
	ASSUME	CS:FLAT,DS:FLAT,SS:FLAT
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
TXTST0:
; -- Begin  _WRITE_TRANSITVEHICLES
; mark_begin;
IF @Version GE 800
  .MMX
ELSEIF @Version GE 612
  .MMX
  MMWORD TEXTEQU <QWORD>
ENDIF
IF @Version GE 800
  .XMM
ELSEIF @Version GE 614
  .XMM
  XMMWORD TEXTEQU <OWORD>
ENDIF
       ALIGN     16
	PUBLIC _WRITE_TRANSITVEHICLES
_WRITE_TRANSITVEHICLES	PROC NEAR 
; parameter 1: 8 + ebp
; parameter 2: 12 + ebp
; parameter 3: 16 + ebp
; parameter 4: 20 + ebp
; parameter 5: 24 + ebp
; parameter 6: 28 + ebp
; parameter 7: 32 + ebp
; parameter 8: 36 + ebp
; parameter 9: 40 + ebp
; parameter 10: 44 + ebp
; parameter 11: 48 + ebp
; parameter 12: 52 + ebp
.B1.1:                          ; Preds .B1.0
        push      ebp                                           ;1.12
        mov       ebp, esp                                      ;1.12
        and       esp, -16                                      ;1.12
        push      esi                                           ;1.12
        push      edi                                           ;1.12
        push      ebx                                           ;1.12
        sub       esp, 212                                      ;1.12
        mov       eax, DWORD PTR [16+ebp]                       ;1.12
        lea       edi, DWORD PTR [esp]                          ;33.5
        mov       DWORD PTR [esp], 0                            ;33.5
        mov       ecx, DWORD PTR [20+ebp]                       ;1.12
        mov       edx, DWORD PTR [eax]                          ;33.5
        lea       eax, DWORD PTR [64+esp]                       ;33.5
        mov       DWORD PTR [36+esp], edx                       ;33.5
        mov       DWORD PTR [64+esp], edx                       ;33.5
        push      32                                            ;33.5
        push      OFFSET FLAT: WRITE_TRANSITVEHICLES$format_pack.0.1+72 ;33.5
        push      eax                                           ;33.5
        push      OFFSET FLAT: __STRLITPACK_0.0.1               ;33.5
        push      -2088435968                                   ;33.5
        mov       ebx, DWORD PTR [ecx]                          ;33.5
        push      ebx                                           ;33.5
        push      edi                                           ;33.5
        call      _for_write_seq_fmt                            ;33.5
                                ; LOE ebx edi
.B1.2:                          ; Preds .B1.1
        mov       eax, DWORD PTR [32+ebp]                       ;1.12
        lea       ecx, DWORD PTR [100+esp]                      ;33.5
        mov       edx, DWORD PTR [eax]                          ;33.5
        mov       DWORD PTR [100+esp], edx                      ;33.5
        push      ecx                                           ;33.5
        push      OFFSET FLAT: __STRLITPACK_1.0.1               ;33.5
        push      edi                                           ;33.5
        call      _for_write_seq_fmt_xmit                       ;33.5
                                ; LOE ebx edi
.B1.3:                          ; Preds .B1.2
        mov       eax, DWORD PTR [36+ebp]                       ;1.12
        lea       ecx, DWORD PTR [120+esp]                      ;33.5
        mov       edx, DWORD PTR [eax]                          ;33.5
        mov       DWORD PTR [120+esp], edx                      ;33.5
        push      ecx                                           ;33.5
        push      OFFSET FLAT: __STRLITPACK_2.0.1               ;33.5
        push      edi                                           ;33.5
        call      _for_write_seq_fmt_xmit                       ;33.5
                                ; LOE ebx edi
.B1.4:                          ; Preds .B1.3
        mov       eax, DWORD PTR [44+ebp]                       ;1.12
        lea       ecx, DWORD PTR [140+esp]                      ;33.5
        mov       edx, DWORD PTR [eax]                          ;33.5
        mov       DWORD PTR [140+esp], edx                      ;33.5
        push      ecx                                           ;33.5
        push      OFFSET FLAT: __STRLITPACK_3.0.1               ;33.5
        push      edi                                           ;33.5
        call      _for_write_seq_fmt_xmit                       ;33.5
                                ; LOE ebx edi
.B1.5:                          ; Preds .B1.4
        mov       DWORD PTR [160+esp], 1                        ;33.5
        lea       eax, DWORD PTR [160+esp]                      ;33.5
        push      eax                                           ;33.5
        push      OFFSET FLAT: __STRLITPACK_4.0.1               ;33.5
        push      edi                                           ;33.5
        call      _for_write_seq_fmt_xmit                       ;33.5
                                ; LOE ebx edi
.B1.6:                          ; Preds .B1.5
        mov       DWORD PTR [180+esp], 4                        ;33.5
        lea       eax, DWORD PTR [180+esp]                      ;33.5
        push      eax                                           ;33.5
        push      OFFSET FLAT: __STRLITPACK_5.0.1               ;33.5
        push      edi                                           ;33.5
        call      _for_write_seq_fmt_xmit                       ;33.5
                                ; LOE ebx edi
.B1.7:                          ; Preds .B1.6
        mov       DWORD PTR [200+esp], 1                        ;33.5
        lea       eax, DWORD PTR [200+esp]                      ;33.5
        push      eax                                           ;33.5
        push      OFFSET FLAT: __STRLITPACK_6.0.1               ;33.5
        push      edi                                           ;33.5
        call      _for_write_seq_fmt_xmit                       ;33.5
                                ; LOE ebx edi
.B1.8:                          ; Preds .B1.7
        mov       eax, DWORD PTR [48+ebp]                       ;1.12
        lea       ecx, DWORD PTR [220+esp]                      ;33.5
        mov       edx, DWORD PTR [eax]                          ;33.5
        mov       DWORD PTR [220+esp], edx                      ;33.5
        push      ecx                                           ;33.5
        push      OFFSET FLAT: __STRLITPACK_7.0.1               ;33.5
        push      edi                                           ;33.5
        call      _for_write_seq_fmt_xmit                       ;33.5
                                ; LOE ebx edi
.B1.9:                          ; Preds .B1.8
        mov       DWORD PTR [240+esp], 1                        ;33.5
        lea       eax, DWORD PTR [240+esp]                      ;33.5
        push      eax                                           ;33.5
        push      OFFSET FLAT: __STRLITPACK_8.0.1               ;33.5
        push      edi                                           ;33.5
        call      _for_write_seq_fmt_xmit                       ;33.5
                                ; LOE ebx edi
.B1.33:                         ; Preds .B1.9
        add       esp, 124                                      ;33.5
                                ; LOE ebx edi
.B1.10:                         ; Preds .B1.33
        mov       DWORD PTR [136+esp], 0                        ;33.5
        lea       eax, DWORD PTR [136+esp]                      ;33.5
        push      eax                                           ;33.5
        push      OFFSET FLAT: __STRLITPACK_9.0.1               ;33.5
        push      edi                                           ;33.5
        call      _for_write_seq_fmt_xmit                       ;33.5
                                ; LOE ebx edi
.B1.11:                         ; Preds .B1.10
        mov       DWORD PTR [156+esp], 0                        ;33.5
        lea       eax, DWORD PTR [156+esp]                      ;33.5
        push      eax                                           ;33.5
        push      OFFSET FLAT: __STRLITPACK_10.0.1              ;33.5
        push      edi                                           ;33.5
        call      _for_write_seq_fmt_xmit                       ;33.5
                                ; LOE ebx edi
.B1.12:                         ; Preds .B1.11
        mov       DWORD PTR [176+esp], 0                        ;33.5
        lea       eax, DWORD PTR [176+esp]                      ;33.5
        push      eax                                           ;33.5
        push      OFFSET FLAT: __STRLITPACK_11.0.1              ;33.5
        push      edi                                           ;33.5
        call      _for_write_seq_fmt_xmit                       ;33.5
                                ; LOE ebx edi
.B1.13:                         ; Preds .B1.12
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM] ;33.5
        mov       ecx, esi                                      ;33.5
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM+32] ;33.5
        shl       edx, 2                                        ;33.5
        sub       ecx, edx                                      ;33.5
        mov       DWORD PTR [68+esp], edx                       ;33.5
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE] ;33.5
        mov       edx, DWORD PTR [ecx]                          ;33.77
        sub       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE+32] ;33.5
        imul      ecx, edx, 84                                  ;33.5
        mov       edx, DWORD PTR [72+eax+ecx]                   ;33.77
        add       edx, edx                                      ;33.5
        neg       edx                                           ;33.5
        add       edx, DWORD PTR [40+eax+ecx]                   ;33.5
        movzx     eax, WORD PTR [4+edx]                         ;33.5
        mov       WORD PTR [196+esp], ax                        ;33.5
        lea       eax, DWORD PTR [196+esp]                      ;33.5
        push      eax                                           ;33.5
        push      OFFSET FLAT: __STRLITPACK_12.0.1              ;33.5
        push      edi                                           ;33.5
        call      _for_write_seq_fmt_xmit                       ;33.5
                                ; LOE ebx esi edi
.B1.14:                         ; Preds .B1.13
        mov       DWORD PTR [216+esp], 0                        ;33.5
        lea       eax, DWORD PTR [216+esp]                      ;33.5
        push      eax                                           ;33.5
        push      OFFSET FLAT: __STRLITPACK_13.0.1              ;33.5
        push      edi                                           ;33.5
        call      _for_write_seq_fmt_xmit                       ;33.5
                                ; LOE ebx esi edi
.B1.15:                         ; Preds .B1.14
        mov       DWORD PTR [236+esp], 1065353216               ;33.5
        lea       eax, DWORD PTR [236+esp]                      ;33.5
        push      eax                                           ;33.5
        push      OFFSET FLAT: __STRLITPACK_14.0.1              ;33.5
        push      edi                                           ;33.5
        call      _for_write_seq_fmt_xmit                       ;33.5
                                ; LOE ebx esi edi
.B1.16:                         ; Preds .B1.15
        mov       DWORD PTR [256+esp], 0                        ;33.5
        lea       eax, DWORD PTR [256+esp]                      ;33.5
        push      eax                                           ;33.5
        push      OFFSET FLAT: __STRLITPACK_15.0.1              ;33.5
        push      edi                                           ;33.5
        call      _for_write_seq_fmt_xmit                       ;33.5
                                ; LOE ebx esi edi
.B1.17:                         ; Preds .B1.16
        mov       ecx, DWORD PTR [40+ebp]                       ;1.12
        imul      edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE+32], 44 ;35.5
        mov       ecx, DWORD PTR [ecx]                          ;35.5
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE] ;35.5
        mov       DWORD PTR [124+esp], eax                      ;35.5
        mov       DWORD PTR [84+esp], 0                         ;35.5
        lea       esi, DWORD PTR [esi+ecx*4]                    ;35.5
        sub       esi, DWORD PTR [116+esp]                      ;35.5
        mov       DWORD PTR [128+esp], edx                      ;35.5
        imul      ecx, DWORD PTR [esi], 44                      ;35.20
        add       ecx, eax                                      ;35.5
        sub       ecx, edx                                      ;35.5
        lea       edx, DWORD PTR [276+esp]                      ;35.5
        movzx     eax, WORD PTR [40+ecx]                        ;35.5
        mov       WORD PTR [276+esp], ax                        ;35.5
        push      32                                            ;35.5
        push      OFFSET FLAT: WRITE_TRANSITVEHICLES$format_pack.0.1+40 ;35.5
        push      edx                                           ;35.5
        push      OFFSET FLAT: __STRLITPACK_16.0.1              ;35.5
        push      -2088435968                                   ;35.5
        push      ebx                                           ;35.5
        push      edi                                           ;35.5
        call      _for_write_seq_fmt                            ;35.5
                                ; LOE edi
.B1.18:                         ; Preds .B1.17
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;35.5
        neg       edx                                           ;35.5
        add       edx, DWORD PTR [148+esp]                      ;35.5
        shl       edx, 8                                        ;35.5
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;35.5
        mov       ecx, DWORD PTR [232+eax+edx]                  ;35.75
        add       ecx, ecx                                      ;35.5
        neg       ecx                                           ;35.5
        mov       ebx, DWORD PTR [200+eax+edx]                  ;35.75
        lea       eax, DWORD PTR [312+esp]                      ;35.5
        movsx     esi, WORD PTR [ecx+ebx]                       ;35.75
        cvtsi2ss  xmm0, esi                                     ;35.75
        divss     xmm0, DWORD PTR [_2il0floatpacket.12]         ;35.5
        movss     DWORD PTR [312+esp], xmm0                     ;35.5
        push      eax                                           ;35.5
        push      OFFSET FLAT: __STRLITPACK_17.0.1              ;35.5
        push      edi                                           ;35.5
        call      _for_write_seq_fmt_xmit                       ;35.5
                                ; LOE edi
.B1.34:                         ; Preds .B1.18
        add       esp, 124                                      ;35.5
                                ; LOE edi
.B1.19:                         ; Preds .B1.34
        mov       eax, DWORD PTR [24+ebp]                       ;1.12
        cmp       DWORD PTR [eax], 1                            ;36.13
        je        .B1.21        ; Prob 16%                      ;36.13
                                ; LOE edi
.B1.20:                         ; Preds .B1.19
        add       esp, 212                                      ;43.1
        pop       ebx                                           ;43.1
        pop       edi                                           ;43.1
        pop       esi                                           ;43.1
        mov       esp, ebp                                      ;43.1
        pop       ebp                                           ;43.1
        ret                                                     ;43.1
                                ; LOE
.B1.21:                         ; Preds .B1.19                  ; Infreq
        mov       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;36.19
        neg       ecx                                           ;36.19
        add       ecx, DWORD PTR [36+esp]                       ;36.19
        shl       ecx, 5                                        ;36.19
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;36.19
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;36.19
        neg       ebx                                           ;36.19
        add       ebx, DWORD PTR [28+edx+ecx]                   ;36.19
        imul      edx, ebx, 152                                 ;36.19
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;36.19
        mov       DWORD PTR [esp], 0                            ;36.19
        mov       eax, DWORD PTR [28+ebp]                       ;1.12
        imul      ecx, DWORD PTR [28+esi+edx], 44               ;36.34
        lea       esi, DWORD PTR [32+esp]                       ;36.19
        add       ecx, DWORD PTR [40+esp]                       ;36.19
        sub       ecx, DWORD PTR [44+esp]                       ;36.19
        mov       ebx, DWORD PTR [36+ecx]                       ;36.19
        mov       DWORD PTR [32+esp], ebx                       ;36.19
        push      32                                            ;36.19
        push      OFFSET FLAT: WRITE_TRANSITVEHICLES$format_pack.0.1+20 ;36.19
        push      esi                                           ;36.19
        push      OFFSET FLAT: __STRLITPACK_18.0.1              ;36.19
        push      -2088435968                                   ;36.19
        push      DWORD PTR [eax]                               ;36.19
        push      edi                                           ;36.19
        call      _for_write_seq_fmt                            ;36.19
                                ; LOE edi
.B1.22:                         ; Preds .B1.21                  ; Infreq
        push      0                                             ;36.19
        push      OFFSET FLAT: __STRLITPACK_19.0.1              ;36.19
        push      edi                                           ;36.19
        call      _for_write_seq_fmt_xmit                       ;36.19
                                ; LOE edi
.B1.23:                         ; Preds .B1.22                  ; Infreq
        push      DWORD PTR [16+ebp]                            ;36.213
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_P_SIZE  ;36.213
                                ; LOE eax edi
.B1.35:                         ; Preds .B1.23                  ; Infreq
        add       esp, 44                                       ;36.213
                                ; LOE eax edi
.B1.24:                         ; Preds .B1.35                  ; Infreq
        dec       eax                                           ;36.19
        mov       DWORD PTR [36+esp], 1                         ;36.19
        test      eax, eax                                      ;36.19
        jle       .B1.30        ; Prob 2%                       ;36.19
                                ; LOE eax edi
.B1.25:                         ; Preds .B1.24                  ; Infreq
        mov       DWORD PTR [48+esp], eax                       ;36.19
        lea       ebx, DWORD PTR [36+esp]                       ;36.19
        mov       esi, DWORD PTR [16+ebp]                       ;36.19
                                ; LOE ebx esi edi
.B1.26:                         ; Preds .B1.28 .B1.25           ; Infreq
        push      OFFSET FLAT: __NLITPACK_0.0.1                 ;36.19
        push      ebx                                           ;36.19
        push      esi                                           ;36.19
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_VALUE   ;36.19
                                ; LOE ebx esi edi f1
.B1.36:                         ; Preds .B1.26                  ; Infreq
        fstp      DWORD PTR [52+esp]                            ;36.19
        movss     xmm3, DWORD PTR [52+esp]                      ;36.19
                                ; LOE ebx esi edi xmm3
.B1.27:                         ; Preds .B1.36                  ; Infreq
        movss     xmm0, DWORD PTR [_2il0floatpacket.14]         ;36.19
        andps     xmm0, xmm3                                    ;36.19
        pxor      xmm3, xmm0                                    ;36.19
        movss     xmm1, DWORD PTR [_2il0floatpacket.15]         ;36.19
        movaps    xmm2, xmm3                                    ;36.19
        movaps    xmm7, xmm3                                    ;36.19
        cmpltss   xmm2, xmm1                                    ;36.19
        andps     xmm1, xmm2                                    ;36.19
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE+32] ;36.19
        addss     xmm7, xmm1                                    ;36.19
        neg       edx                                           ;36.19
        subss     xmm7, xmm1                                    ;36.19
        movaps    xmm6, xmm7                                    ;36.19
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE] ;36.19
        subss     xmm6, xmm3                                    ;36.19
        movss     xmm3, DWORD PTR [_2il0floatpacket.16]         ;36.19
        movaps    xmm4, xmm6                                    ;36.19
        movaps    xmm5, xmm3                                    ;36.19
        cmpnless  xmm4, xmm3                                    ;36.19
        addss     xmm5, xmm3                                    ;36.19
        cmpless   xmm6, DWORD PTR [_2il0floatpacket.17]         ;36.19
        andps     xmm4, xmm5                                    ;36.19
        andps     xmm6, xmm5                                    ;36.19
        subss     xmm7, xmm4                                    ;36.19
        addss     xmm7, xmm6                                    ;36.19
        orps      xmm7, xmm0                                    ;36.19
        cvtss2si  eax, xmm7                                     ;36.138
        add       edx, eax                                      ;36.19
        imul      eax, edx, 44                                  ;36.19
        mov       edx, DWORD PTR [36+ecx+eax]                   ;36.19
        lea       ecx, DWORD PTR [68+esp]                       ;36.19
        mov       DWORD PTR [68+esp], edx                       ;36.19
        push      ecx                                           ;36.19
        push      OFFSET FLAT: __STRLITPACK_20.0.1              ;36.19
        push      edi                                           ;36.19
        call      _for_write_seq_fmt_xmit                       ;36.19
                                ; LOE ebx esi edi
.B1.37:                         ; Preds .B1.27                  ; Infreq
        add       esp, 24                                       ;36.19
                                ; LOE ebx esi edi
.B1.28:                         ; Preds .B1.37                  ; Infreq
        mov       eax, DWORD PTR [36+esp]                       ;36.19
        inc       eax                                           ;36.19
        mov       DWORD PTR [36+esp], eax                       ;36.19
        cmp       eax, DWORD PTR [48+esp]                       ;36.19
        jle       .B1.26        ; Prob 82%                      ;36.19
                                ; LOE ebx esi edi
.B1.30:                         ; Preds .B1.28 .B1.24           ; Infreq
        push      0                                             ;36.19
        push      OFFSET FLAT: __STRLITPACK_21.0.1              ;36.19
        push      edi                                           ;36.19
        call      _for_write_seq_fmt_xmit                       ;36.19
                                ; LOE
.B1.38:                         ; Preds .B1.30                  ; Infreq
        add       esp, 224                                      ;36.19
        pop       ebx                                           ;36.19
        pop       edi                                           ;36.19
        pop       esi                                           ;36.19
        mov       esp, ebp                                      ;36.19
        pop       ebp                                           ;36.19
        ret                                                     ;36.19
        ALIGN     16
                                ; LOE
; mark_end;
_WRITE_TRANSITVEHICLES ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
WRITE_TRANSITVEHICLES$format_pack.0.1	DB	54
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	6
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	-24
	DB	3
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	12
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	2
	DB	1
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	9
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	2
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	2
	DB	1
	DB	0
	DB	0
	DB	0
	DB	8
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	6
	DB	0
	DB	0
	DB	0
	DB	6
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	4
	DB	2
	DB	0
	DB	0
	DB	0
	DB	8
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	2
	DB	0
	DB	0
	DB	0
	DB	5
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	8
	DB	1
	DB	0
	DB	0
	DB	0
	DB	12
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	1
	DB	1
	DB	0
	DB	0
	DB	0
	DB	6
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
_DATA	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
__STRLITPACK_0.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_1.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_2.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_3.0.1	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_4.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_5.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_6.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_7.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_8.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_9.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_10.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_11.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_12.0.1	DB	7
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_13.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_14.0.1	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_15.0.1	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_16.0.1	DB	7
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_17.0.1	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_18.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_19.0.1	DB	2
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__NLITPACK_0.0.1	DD	1
__STRLITPACK_20.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_21.0.1	DB	1
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _WRITE_TRANSITVEHICLES
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DB 1 DUP ( 0H)	; pad
_2il0floatpacket.12	DD	042700000H
_2il0floatpacket.13	DD	03f800000H
_2il0floatpacket.14	DD	080000000H
_2il0floatpacket.15	DD	04b000000H
_2il0floatpacket.16	DD	03f000000H
_2il0floatpacket.17	DD	0bf000000H
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
EXTRN	_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE:BYTE
EXTRN	_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE:BYTE
_DATA	ENDS
EXTRN	_for_write_seq_fmt_xmit:PROC
EXTRN	_for_write_seq_fmt:PROC
EXTRN	_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_VALUE:PROC
EXTRN	_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_P_SIZE:PROC
EXTRN	__fltused:BYTE
	INCLUDELIB <ifconsol>
	INCLUDELIB <libifcoremt>
	INCLUDELIB <libifport>
	INCLUDELIB <libmmt>
	INCLUDELIB <LIBCMT>
	INCLUDELIB <libirc>
	INCLUDELIB <svml_dispmt>
	INCLUDELIB <OLDNAMES>
	END
