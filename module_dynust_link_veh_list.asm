; mark_description "Intel(R) Visual Fortran Compiler XE for applications running on IA-32, Version 13.1.1.171 Build 20130313";
; mark_description "-S";
	.686P
 	.387
	OPTION DOTNAME
	ASSUME	CS:FLAT,DS:FLAT,SS:FLAT
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
TXTST0:
; -- Begin  _DYNUST_LINK_VEH_LIST_MODULE$
; mark_begin;
IF @Version GE 800
  .MMX
ELSEIF @Version GE 612
  .MMX
  MMWORD TEXTEQU <QWORD>
ENDIF
IF @Version GE 800
  .XMM
ELSEIF @Version GE 614
  .XMM
  XMMWORD TEXTEQU <OWORD>
ENDIF
       ALIGN     16
	PUBLIC _DYNUST_LINK_VEH_LIST_MODULE$
_DYNUST_LINK_VEH_LIST_MODULE$	PROC NEAR 
.B1.1:                          ; Preds .B1.0
        ret                                                     ;1.8
        ALIGN     16
                                ; LOE
; mark_end;
_DYNUST_LINK_VEH_LIST_MODULE$ ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_LINK_VEH_LIST_MODULE$
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_LINK_VEH_LIST_MODULE_mp_LINKATT_2DSETUP
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_LINK_VEH_LIST_MODULE_mp_LINKATT_2DSETUP
_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKATT_2DSETUP	PROC NEAR 
; parameter 1: 8 + ebp
.B2.1:                          ; Preds .B2.0
        push      ebp                                           ;58.12
        mov       ebp, esp                                      ;58.12
        and       esp, -16                                      ;58.12
        push      esi                                           ;58.12
        push      edi                                           ;58.12
        push      ebx                                           ;58.12
        sub       esp, 148                                      ;58.12
        mov       esi, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST+12] ;63.3
        test      esi, 1                                        ;63.7
        je        .B2.17        ; Prob 60%                      ;63.7
                                ; LOE esi
.B2.2:                          ; Preds .B2.1
        mov       ecx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKATT_ARRAYSIZE] ;64.10
        test      ecx, ecx                                      ;64.10
        jle       .B2.13        ; Prob 2%                       ;64.10
                                ; LOE ecx esi
.B2.3:                          ; Preds .B2.2
        imul      edi, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST+32], -44 ;
        mov       eax, 1                                        ;
        add       edi, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST] ;
        xor       edx, edx                                      ;64.10
        mov       DWORD PTR [4+esp], ecx                        ;
        mov       ebx, 44                                       ;
        mov       DWORD PTR [esp], esi                          ;
        mov       esi, eax                                      ;
                                ; LOE ebx esi edi
.B2.4:                          ; Preds .B2.11 .B2.3
        cmp       DWORD PTR [36+ebx+edi], 0                     ;64.10
        jle       .B2.11        ; Prob 79%                      ;64.10
                                ; LOE ebx esi edi
.B2.5:                          ; Preds .B2.4
        mov       edx, DWORD PTR [12+ebx+edi]                   ;64.10
        mov       ecx, edx                                      ;64.10
        shr       ecx, 1                                        ;64.10
        and       edx, 1                                        ;64.10
        and       ecx, 1                                        ;64.10
        add       edx, edx                                      ;64.10
        shl       ecx, 2                                        ;64.10
        or        ecx, 1                                        ;64.10
        or        ecx, edx                                      ;64.10
        or        ecx, 262144                                   ;64.10
        push      ecx                                           ;64.10
        push      DWORD PTR [ebx+edi]                           ;64.10
        call      _for_dealloc_allocatable                      ;64.10
                                ; LOE eax ebx esi edi
.B2.32:                         ; Preds .B2.5
        add       esp, 8                                        ;64.10
                                ; LOE eax ebx esi edi
.B2.6:                          ; Preds .B2.32
        and       DWORD PTR [12+ebx+edi], -2                    ;64.10
        mov       DWORD PTR [ebx+edi], 0                        ;64.10
        test      eax, eax                                      ;64.10
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_VECTORERROR], eax ;64.10
        je        .B2.10        ; Prob 50%                      ;64.10
                                ; LOE ebx esi edi
.B2.7:                          ; Preds .B2.6
        mov       DWORD PTR [80+esp], 0                         ;64.10
        lea       ecx, DWORD PTR [80+esp]                       ;64.10
        mov       DWORD PTR [128+esp], 36                       ;64.10
        lea       edx, DWORD PTR [128+esp]                      ;64.10
        mov       DWORD PTR [132+esp], OFFSET FLAT: __STRLITPACK_30 ;64.10
        push      32                                            ;64.10
        push      edx                                           ;64.10
        push      OFFSET FLAT: __STRLITPACK_60.0.7              ;64.10
        push      -2088435968                                   ;64.10
        push      911                                           ;64.10
        push      ecx                                           ;64.10
        call      _for_write_seq_lis                            ;64.10
                                ; LOE ebx esi edi
.B2.8:                          ; Preds .B2.7
        mov       DWORD PTR [160+esp], esi                      ;64.10
        lea       edx, DWORD PTR [160+esp]                      ;64.10
        push      edx                                           ;64.10
        push      OFFSET FLAT: __STRLITPACK_61.0.7              ;64.10
        lea       ecx, DWORD PTR [112+esp]                      ;64.10
        push      ecx                                           ;64.10
        call      _for_write_seq_lis_xmit                       ;64.10
                                ; LOE ebx esi edi
.B2.9:                          ; Preds .B2.8
        mov       DWORD PTR [180+esp], 0                        ;64.10
        lea       edx, DWORD PTR [180+esp]                      ;64.10
        push      edx                                           ;64.10
        push      OFFSET FLAT: __STRLITPACK_62.0.7              ;64.10
        lea       ecx, DWORD PTR [124+esp]                      ;64.10
        push      ecx                                           ;64.10
        call      _for_write_seq_lis_xmit                       ;64.10
                                ; LOE ebx esi edi
.B2.33:                         ; Preds .B2.9
        add       esp, 48                                       ;64.10
                                ; LOE ebx esi edi
.B2.10:                         ; Preds .B2.33 .B2.6
        xor       edx, edx                                      ;64.10
        mov       DWORD PTR [36+ebx+edi], edx                   ;64.10
        mov       DWORD PTR [40+ebx+edi], edx                   ;64.10
                                ; LOE ebx esi edi
.B2.11:                         ; Preds .B2.4 .B2.10
        inc       esi                                           ;64.10
        add       ebx, 44                                       ;64.10
        cmp       esi, DWORD PTR [4+esp]                        ;64.10
        jle       .B2.4         ; Prob 82%                      ;64.10
                                ; LOE ebx esi edi
.B2.12:                         ; Preds .B2.11
        mov       esi, DWORD PTR [esp]                          ;
                                ; LOE esi
.B2.13:                         ; Preds .B2.12 .B2.2
        mov       edx, esi                                      ;64.10
        mov       eax, esi                                      ;64.10
        shr       edx, 1                                        ;64.10
        and       eax, 1                                        ;64.10
        and       edx, 1                                        ;64.10
        add       eax, eax                                      ;64.10
        shl       edx, 2                                        ;64.10
        or        edx, 1                                        ;64.10
        or        edx, eax                                      ;64.10
        or        edx, 262144                                   ;64.10
        push      edx                                           ;64.10
        push      DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST] ;64.10
        call      _for_dealloc_allocatable                      ;64.10
                                ; LOE eax esi
.B2.34:                         ; Preds .B2.13
        add       esp, 8                                        ;64.10
                                ; LOE eax esi
.B2.14:                         ; Preds .B2.34
        and       esi, -2                                       ;64.10
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST], 0 ;64.10
        test      eax, eax                                      ;64.10
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST+12], esi ;64.10
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_VECTORERROR], eax ;64.10
        je        .B2.17        ; Prob 50%                      ;64.10
                                ; LOE esi
.B2.15:                         ; Preds .B2.14
        mov       DWORD PTR [esp], 0                            ;64.10
        lea       ebx, DWORD PTR [esp]                          ;64.10
        mov       DWORD PTR [32+esp], 14                        ;64.10
        lea       eax, DWORD PTR [32+esp]                       ;64.10
        mov       DWORD PTR [36+esp], OFFSET FLAT: __STRLITPACK_34 ;64.10
        push      32                                            ;64.10
        push      eax                                           ;64.10
        push      OFFSET FLAT: __STRLITPACK_56.0.5              ;64.10
        push      -2088435968                                   ;64.10
        push      911                                           ;64.10
        push      ebx                                           ;64.10
        call      _for_write_seq_lis                            ;64.10
                                ; LOE ebx esi
.B2.16:                         ; Preds .B2.15
        mov       DWORD PTR [64+esp], 0                         ;64.10
        lea       eax, DWORD PTR [64+esp]                       ;64.10
        push      eax                                           ;64.10
        push      OFFSET FLAT: __STRLITPACK_57.0.5              ;64.10
        push      ebx                                           ;64.10
        call      _for_write_seq_lis_xmit                       ;64.10
                                ; LOE esi
.B2.35:                         ; Preds .B2.16
        add       esp, 36                                       ;64.10
                                ; LOE esi
.B2.17:                         ; Preds .B2.35 .B2.14 .B2.1
        mov       ebx, DWORD PTR [8+ebp]                        ;58.12
        test      esi, 1                                        ;69.13
        jne       .B2.25        ; Prob 40%                      ;69.13
                                ; LOE ebx
.B2.18:                         ; Preds .B2.17
        xor       esi, esi                                      ;70.5
        lea       edx, DWORD PTR [116+esp]                      ;70.5
        mov       eax, DWORD PTR [ebx]                          ;70.5
        test      eax, eax                                      ;70.5
        push      44                                            ;70.5
        cmovl     eax, esi                                      ;70.5
        push      eax                                           ;70.5
        push      2                                             ;70.5
        push      edx                                           ;70.5
        call      _for_check_mult_overflow                      ;70.5
                                ; LOE eax ebx esi
.B2.19:                         ; Preds .B2.18
        mov       edx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST+12] ;70.5
        and       eax, 1                                        ;70.5
        and       edx, 1                                        ;70.5
        add       edx, edx                                      ;70.5
        shl       eax, 4                                        ;70.5
        or        edx, 1                                        ;70.5
        or        edx, eax                                      ;70.5
        or        edx, 262144                                   ;70.5
        push      edx                                           ;70.5
        push      OFFSET FLAT: _DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST ;70.5
        push      DWORD PTR [140+esp]                           ;70.5
        call      _for_alloc_allocatable                        ;70.5
                                ; LOE eax ebx esi
.B2.37:                         ; Preds .B2.19
        add       esp, 28                                       ;70.5
        mov       edi, eax                                      ;70.5
                                ; LOE ebx esi edi
.B2.20:                         ; Preds .B2.37
        test      edi, edi                                      ;70.5
        je        .B2.23        ; Prob 50%                      ;70.5
                                ; LOE ebx esi edi
.B2.21:                         ; Preds .B2.20
        mov       DWORD PTR [48+esp], 0                         ;71.23
        lea       esi, DWORD PTR [48+esp]                       ;71.23
        mov       DWORD PTR [120+esp], 23                       ;71.23
        lea       eax, DWORD PTR [120+esp]                      ;71.23
        mov       DWORD PTR [124+esp], OFFSET FLAT: __STRLITPACK_44 ;71.23
        push      32                                            ;71.23
        push      eax                                           ;71.23
        push      OFFSET FLAT: __STRLITPACK_46.0.2              ;71.23
        push      -2088435968                                   ;71.23
        push      911                                           ;71.23
        push      esi                                           ;71.23
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_VECTORERROR], edi ;70.5
        call      _for_write_seq_lis                            ;71.23
                                ; LOE ebx esi
.B2.22:                         ; Preds .B2.21
        mov       DWORD PTR [136+esp], 0                        ;71.23
        lea       eax, DWORD PTR [136+esp]                      ;71.23
        push      eax                                           ;71.23
        push      OFFSET FLAT: __STRLITPACK_47.0.2              ;71.23
        push      esi                                           ;71.23
        call      _for_write_seq_lis_xmit                       ;71.23
                                ; LOE ebx
.B2.38:                         ; Preds .B2.22
        add       esp, 36                                       ;71.23
        jmp       .B2.25        ; Prob 100%                     ;71.23
                                ; LOE ebx
.B2.23:                         ; Preds .B2.20
        mov       eax, 44                                       ;70.5
        mov       edx, 1                                        ;70.5
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST+12], 133 ;70.5
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST+4], eax ;70.5
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST+16], edx ;70.5
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST+8], esi ;70.5
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST+32], edx ;70.5
        lea       edx, DWORD PTR [44+esp]                       ;70.5
        mov       ecx, DWORD PTR [ebx]                          ;70.5
        test      ecx, ecx                                      ;70.5
        push      eax                                           ;70.5
        cmovge    esi, ecx                                      ;70.5
        push      esi                                           ;70.5
        push      2                                             ;70.5
        push      edx                                           ;70.5
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST+24], esi ;70.5
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST+28], eax ;70.5
        call      _for_check_mult_overflow                      ;70.5
                                ; LOE ebx edi
.B2.39:                         ; Preds .B2.23
        add       esp, 16                                       ;70.5
                                ; LOE ebx edi
.B2.24:                         ; Preds .B2.39
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_VECTORERROR], edi ;70.5
                                ; LOE ebx
.B2.25:                         ; Preds .B2.38 .B2.17 .B2.24
        mov       esi, DWORD PTR [ebx]                          ;75.3
        test      esi, esi                                      ;75.3
        jg        .B2.27        ; Prob 2%                       ;75.3
                                ; LOE esi
.B2.26:                         ; Preds .B2.28 .B2.25
        add       esp, 148                                      ;82.1
        pop       ebx                                           ;82.1
        pop       edi                                           ;82.1
        pop       esi                                           ;82.1
        mov       esp, ebp                                      ;82.1
        pop       ebp                                           ;82.1
        ret                                                     ;82.1
                                ; LOE
.B2.27:                         ; Preds .B2.25                  ; Infreq
        imul      edx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST+32], -44 ;
        xor       ebx, ebx                                      ;
        add       edx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST] ;
        xor       ecx, ecx                                      ;
        xor       eax, eax                                      ;
                                ; LOE eax edx ecx ebx esi
.B2.28:                         ; Preds .B2.28 .B2.27           ; Infreq
        inc       ebx                                           ;75.3
        mov       DWORD PTR [80+eax+edx], ecx                   ;76.4
        mov       DWORD PTR [84+eax+edx], ecx                   ;76.4
        add       eax, 44                                       ;75.3
        cmp       ebx, esi                                      ;75.3
        jb        .B2.28        ; Prob 99%                      ;75.3
        jmp       .B2.26        ; Prob 100%                     ;75.3
        ALIGN     16
                                ; LOE eax edx ecx ebx esi
; mark_end;
_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKATT_2DSETUP ENDP
_TEXT	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
__STRLITPACK_46.0.2	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_47.0.2	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_LINK_VEH_LIST_MODULE_mp_LINKATT_2DSETUP
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_LINK_VEH_LIST_MODULE_mp_LINKTEMP_2DSETUP
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_LINK_VEH_LIST_MODULE_mp_LINKTEMP_2DSETUP
_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKTEMP_2DSETUP	PROC NEAR 
; parameter 1: 8 + ebp
.B3.1:                          ; Preds .B3.0
        push      ebp                                           ;85.12
        mov       ebp, esp                                      ;85.12
        and       esp, -16                                      ;85.12
        push      esi                                           ;85.12
        push      edi                                           ;85.12
        push      ebx                                           ;85.12
        sub       esp, 148                                      ;85.12
        mov       edi, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP+12] ;90.3
        test      edi, 1                                        ;90.7
        je        .B3.17        ; Prob 60%                      ;90.7
                                ; LOE edi
.B3.2:                          ; Preds .B3.1
        mov       eax, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKATT_ARRAYSIZE] ;91.10
        test      eax, eax                                      ;91.10
        jle       .B3.13        ; Prob 2%                       ;91.10
                                ; LOE eax edi
.B3.3:                          ; Preds .B3.2
        imul      ebx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP+32], -40 ;
        mov       esi, 1                                        ;
        add       ebx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP] ;
        mov       DWORD PTR [4+esp], eax                        ;
        mov       DWORD PTR [esp], edi                          ;
                                ; LOE ebx esi
.B3.4:                          ; Preds .B3.11 .B3.3
        lea       edi, DWORD PTR [esi+esi*4]                    ;91.10
        cmp       DWORD PTR [36+ebx+edi*8], 0                   ;91.10
        jle       .B3.11        ; Prob 79%                      ;91.10
                                ; LOE ebx esi edi
.B3.5:                          ; Preds .B3.4
        mov       eax, DWORD PTR [12+ebx+edi*8]                 ;91.10
        mov       edx, eax                                      ;91.10
        shr       edx, 1                                        ;91.10
        and       eax, 1                                        ;91.10
        and       edx, 1                                        ;91.10
        add       eax, eax                                      ;91.10
        shl       edx, 2                                        ;91.10
        or        edx, 1                                        ;91.10
        or        edx, eax                                      ;91.10
        or        edx, 262144                                   ;91.10
        push      edx                                           ;91.10
        push      DWORD PTR [ebx+edi*8]                         ;91.10
        call      _for_dealloc_allocatable                      ;91.10
                                ; LOE eax ebx esi edi
.B3.36:                         ; Preds .B3.5
        add       esp, 8                                        ;91.10
                                ; LOE eax ebx esi edi
.B3.6:                          ; Preds .B3.36
        and       DWORD PTR [12+ebx+edi*8], -2                  ;91.10
        mov       DWORD PTR [ebx+edi*8], 0                      ;91.10
        test      eax, eax                                      ;91.10
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_VECTORERROR], eax ;91.10
        je        .B3.10        ; Prob 50%                      ;91.10
                                ; LOE ebx esi edi
.B3.7:                          ; Preds .B3.6
        mov       DWORD PTR [80+esp], 0                         ;91.10
        lea       edx, DWORD PTR [80+esp]                       ;91.10
        mov       DWORD PTR [128+esp], 28                       ;91.10
        lea       eax, DWORD PTR [128+esp]                      ;91.10
        mov       DWORD PTR [132+esp], OFFSET FLAT: __STRLITPACK_28 ;91.10
        push      32                                            ;91.10
        push      eax                                           ;91.10
        push      OFFSET FLAT: __STRLITPACK_63.0.8              ;91.10
        push      -2088435968                                   ;91.10
        push      911                                           ;91.10
        push      edx                                           ;91.10
        call      _for_write_seq_lis                            ;91.10
                                ; LOE ebx esi edi
.B3.8:                          ; Preds .B3.7
        mov       DWORD PTR [160+esp], esi                      ;91.10
        lea       eax, DWORD PTR [160+esp]                      ;91.10
        push      eax                                           ;91.10
        push      OFFSET FLAT: __STRLITPACK_64.0.8              ;91.10
        lea       edx, DWORD PTR [112+esp]                      ;91.10
        push      edx                                           ;91.10
        call      _for_write_seq_lis_xmit                       ;91.10
                                ; LOE ebx esi edi
.B3.9:                          ; Preds .B3.8
        mov       DWORD PTR [180+esp], 0                        ;91.10
        lea       eax, DWORD PTR [180+esp]                      ;91.10
        push      eax                                           ;91.10
        push      OFFSET FLAT: __STRLITPACK_65.0.8              ;91.10
        lea       edx, DWORD PTR [124+esp]                      ;91.10
        push      edx                                           ;91.10
        call      _for_write_seq_lis_xmit                       ;91.10
                                ; LOE ebx esi edi
.B3.37:                         ; Preds .B3.9
        add       esp, 48                                       ;91.10
                                ; LOE ebx esi edi
.B3.10:                         ; Preds .B3.37 .B3.6
        mov       DWORD PTR [36+ebx+edi*8], 0                   ;91.10
                                ; LOE ebx esi
.B3.11:                         ; Preds .B3.4 .B3.10
        inc       esi                                           ;91.10
        cmp       esi, DWORD PTR [4+esp]                        ;91.10
        jle       .B3.4         ; Prob 82%                      ;91.10
                                ; LOE ebx esi
.B3.12:                         ; Preds .B3.11
        mov       edi, DWORD PTR [esp]                          ;
                                ; LOE edi
.B3.13:                         ; Preds .B3.12 .B3.2
        mov       edx, edi                                      ;91.10
        mov       eax, edi                                      ;91.10
        shr       edx, 1                                        ;91.10
        and       eax, 1                                        ;91.10
        and       edx, 1                                        ;91.10
        add       eax, eax                                      ;91.10
        shl       edx, 2                                        ;91.10
        or        edx, 1                                        ;91.10
        or        edx, eax                                      ;91.10
        or        edx, 262144                                   ;91.10
        push      edx                                           ;91.10
        push      DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP] ;91.10
        call      _for_dealloc_allocatable                      ;91.10
                                ; LOE eax edi
.B3.38:                         ; Preds .B3.13
        add       esp, 8                                        ;91.10
                                ; LOE eax edi
.B3.14:                         ; Preds .B3.38
        and       edi, -2                                       ;91.10
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP], 0 ;91.10
        test      eax, eax                                      ;91.10
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP+12], edi ;91.10
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_VECTORERROR], eax ;91.10
        je        .B3.17        ; Prob 50%                      ;91.10
                                ; LOE edi
.B3.15:                         ; Preds .B3.14
        mov       DWORD PTR [esp], 0                            ;91.10
        lea       ebx, DWORD PTR [esp]                          ;91.10
        mov       DWORD PTR [32+esp], 19                        ;91.10
        lea       eax, DWORD PTR [32+esp]                       ;91.10
        mov       DWORD PTR [36+esp], OFFSET FLAT: __STRLITPACK_32 ;91.10
        push      32                                            ;91.10
        push      eax                                           ;91.10
        push      OFFSET FLAT: __STRLITPACK_58.0.6              ;91.10
        push      -2088435968                                   ;91.10
        push      911                                           ;91.10
        push      ebx                                           ;91.10
        call      _for_write_seq_lis                            ;91.10
                                ; LOE ebx edi
.B3.16:                         ; Preds .B3.15
        mov       DWORD PTR [64+esp], 0                         ;91.10
        lea       eax, DWORD PTR [64+esp]                       ;91.10
        push      eax                                           ;91.10
        push      OFFSET FLAT: __STRLITPACK_59.0.6              ;91.10
        push      ebx                                           ;91.10
        call      _for_write_seq_lis_xmit                       ;91.10
                                ; LOE edi
.B3.39:                         ; Preds .B3.16
        add       esp, 36                                       ;91.10
                                ; LOE edi
.B3.17:                         ; Preds .B3.39 .B3.14 .B3.1
        test      edi, 1                                        ;96.13
        jne       .B3.25        ; Prob 40%                      ;96.13
                                ; LOE
.B3.18:                         ; Preds .B3.17
        mov       ebx, DWORD PTR [8+ebp]                        ;85.12
        xor       esi, esi                                      ;97.5
        lea       edx, DWORD PTR [116+esp]                      ;97.5
        push      40                                            ;97.5
        mov       eax, DWORD PTR [ebx]                          ;97.5
        test      eax, eax                                      ;97.5
        cmovl     eax, esi                                      ;97.5
        push      eax                                           ;97.5
        push      2                                             ;97.5
        push      edx                                           ;97.5
        call      _for_check_mult_overflow                      ;97.5
                                ; LOE eax ebx esi
.B3.19:                         ; Preds .B3.18
        mov       edx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP+12] ;97.5
        and       eax, 1                                        ;97.5
        and       edx, 1                                        ;97.5
        add       edx, edx                                      ;97.5
        shl       eax, 4                                        ;97.5
        or        edx, 1                                        ;97.5
        or        edx, eax                                      ;97.5
        or        edx, 262144                                   ;97.5
        push      edx                                           ;97.5
        push      OFFSET FLAT: _DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP ;97.5
        push      DWORD PTR [140+esp]                           ;97.5
        call      _for_alloc_allocatable                        ;97.5
                                ; LOE eax ebx esi
.B3.41:                         ; Preds .B3.19
        add       esp, 28                                       ;97.5
        mov       edi, eax                                      ;97.5
                                ; LOE ebx esi edi
.B3.20:                         ; Preds .B3.41
        test      edi, edi                                      ;97.5
        je        .B3.23        ; Prob 50%                      ;97.5
                                ; LOE ebx esi edi
.B3.21:                         ; Preds .B3.20
        mov       DWORD PTR [48+esp], 0                         ;98.23
        lea       ebx, DWORD PTR [48+esp]                       ;98.23
        mov       DWORD PTR [120+esp], 17                       ;98.23
        lea       eax, DWORD PTR [120+esp]                      ;98.23
        mov       DWORD PTR [124+esp], OFFSET FLAT: __STRLITPACK_42 ;98.23
        push      32                                            ;98.23
        push      eax                                           ;98.23
        push      OFFSET FLAT: __STRLITPACK_48.0.3              ;98.23
        push      -2088435968                                   ;98.23
        push      911                                           ;98.23
        push      ebx                                           ;98.23
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_VECTORERROR], edi ;97.5
        call      _for_write_seq_lis                            ;98.23
                                ; LOE ebx
.B3.22:                         ; Preds .B3.21
        mov       DWORD PTR [136+esp], 0                        ;98.23
        lea       eax, DWORD PTR [136+esp]                      ;98.23
        push      eax                                           ;98.23
        push      OFFSET FLAT: __STRLITPACK_49.0.3              ;98.23
        push      ebx                                           ;98.23
        call      _for_write_seq_lis_xmit                       ;98.23
                                ; LOE
.B3.42:                         ; Preds .B3.22
        add       esp, 36                                       ;98.23
        jmp       .B3.25        ; Prob 100%                     ;98.23
                                ; LOE
.B3.23:                         ; Preds .B3.20
        mov       ecx, DWORD PTR [ebx]                          ;97.5
        test      ecx, ecx                                      ;97.5
        lea       ebx, DWORD PTR [44+esp]                       ;97.5
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP+8], esi ;97.5
        cmovge    esi, ecx                                      ;97.5
        mov       eax, 40                                       ;97.5
        push      eax                                           ;97.5
        push      esi                                           ;97.5
        push      2                                             ;97.5
        push      ebx                                           ;97.5
        mov       edx, 1                                        ;97.5
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP+12], 133 ;97.5
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP+4], eax ;97.5
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP+16], edx ;97.5
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP+32], edx ;97.5
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP+24], esi ;97.5
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP+28], eax ;97.5
        call      _for_check_mult_overflow                      ;97.5
                                ; LOE edi
.B3.43:                         ; Preds .B3.23
        add       esp, 16                                       ;97.5
                                ; LOE edi
.B3.24:                         ; Preds .B3.43
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_VECTORERROR], edi ;97.5
                                ; LOE
.B3.25:                         ; Preds .B3.42 .B3.17 .B3.24
        mov       ecx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP+24] ;101.3
        test      ecx, ecx                                      ;101.3
        mov       eax, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP+32] ;101.3
        jle       .B3.32        ; Prob 50%                      ;101.3
                                ; LOE eax ecx
.B3.26:                         ; Preds .B3.25
        mov       esi, ecx                                      ;101.3
        shr       esi, 31                                       ;101.3
        add       esi, ecx                                      ;101.3
        sar       esi, 1                                        ;101.3
        mov       edi, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP] ;101.3
        test      esi, esi                                      ;101.3
        jbe       .B3.33        ; Prob 10%                      ;101.3
                                ; LOE eax ecx esi edi
.B3.27:                         ; Preds .B3.26
        xor       ebx, ebx                                      ;
        mov       DWORD PTR [140+esp], eax                      ;
        xor       edx, edx                                      ;
                                ; LOE edx ecx ebx esi edi
.B3.28:                         ; Preds .B3.28 .B3.27
        lea       eax, DWORD PTR [ebx+ebx*4]                    ;101.3
        inc       ebx                                           ;101.3
        shl       eax, 4                                        ;101.3
        cmp       ebx, esi                                      ;101.3
        mov       DWORD PTR [36+edi+eax], edx                   ;101.3
        mov       DWORD PTR [76+edi+eax], edx                   ;101.3
        jb        .B3.28        ; Prob 63%                      ;101.3
                                ; LOE edx ecx ebx esi edi
.B3.29:                         ; Preds .B3.28
        mov       eax, DWORD PTR [140+esp]                      ;
        lea       ebx, DWORD PTR [1+ebx+ebx]                    ;101.3
                                ; LOE eax ecx ebx edi
.B3.30:                         ; Preds .B3.29 .B3.33
        lea       edx, DWORD PTR [-1+ebx]                       ;101.3
        cmp       ecx, edx                                      ;101.3
        jbe       .B3.32        ; Prob 10%                      ;101.3
                                ; LOE eax ebx edi
.B3.31:                         ; Preds .B3.30
        mov       edx, eax                                      ;101.3
        add       ebx, eax                                      ;101.3
        neg       edx                                           ;101.3
        add       edx, ebx                                      ;101.3
        lea       eax, DWORD PTR [edx+edx*4]                    ;101.3
        mov       DWORD PTR [-4+edi+eax*8], 0                   ;101.3
                                ; LOE
.B3.32:                         ; Preds .B3.30 .B3.25 .B3.31
        add       esp, 148                                      ;104.1
        pop       ebx                                           ;104.1
        pop       edi                                           ;104.1
        pop       esi                                           ;104.1
        mov       esp, ebp                                      ;104.1
        pop       ebp                                           ;104.1
        ret                                                     ;104.1
                                ; LOE
.B3.33:                         ; Preds .B3.26                  ; Infreq
        mov       ebx, 1                                        ;
        jmp       .B3.30        ; Prob 100%                     ;
        ALIGN     16
                                ; LOE eax ecx ebx edi
; mark_end;
_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKTEMP_2DSETUP ENDP
_TEXT	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_48.0.3	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_49.0.3	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_LINK_VEH_LIST_MODULE_mp_LINKTEMP_2DSETUP
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_LINK_VEH_LIST_MODULE_mp_LINKATT_SETUP
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_LINK_VEH_LIST_MODULE_mp_LINKATT_SETUP
_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKATT_SETUP	PROC NEAR 
; parameter 1: 8 + ebx
; parameter 2: 12 + ebx
.B4.1:                          ; Preds .B4.0
        push      ebx                                           ;107.12
        mov       ebx, esp                                      ;107.12
        and       esp, -16                                      ;107.12
        push      ebp                                           ;107.12
        push      ebp                                           ;107.12
        mov       ebp, DWORD PTR [4+ebx]                        ;107.12
        mov       DWORD PTR [4+esp], ebp                        ;107.12
        mov       ebp, esp                                      ;107.12
        sub       esp, 136                                      ;107.12
        mov       eax, DWORD PTR [8+ebx]                        ;107.12
        mov       ecx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST] ;115.4
        mov       DWORD PTR [-36+ebp], edi                      ;107.12
        mov       edx, DWORD PTR [eax]                          ;115.8
        sub       edx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST+32] ;115.30
        imul      eax, edx, 44                                  ;115.30
        mov       edi, DWORD PTR [36+ecx+eax]                   ;115.8
        test      edi, edi                                      ;115.30
        mov       DWORD PTR [-40+ebp], esi                      ;107.12
        jg        .B4.3         ; Prob 21%                      ;115.30
                                ; LOE edi
.B4.2:                          ; Preds .B4.1
        xor       edi, edi                                      ;112.2
        jmp       .B4.38        ; Prob 100%                     ;112.2
                                ; LOE edi
.B4.3:                          ; Preds .B4.1
        mov       eax, 0                                        ;117.3
        mov       edx, edi                                      ;117.3
        push      4                                             ;117.3
        cmovle    edx, eax                                      ;117.3
        lea       ecx, DWORD PTR [-108+ebp]                     ;117.3
        push      edx                                           ;117.3
        push      2                                             ;117.3
        push      ecx                                           ;117.3
        mov       DWORD PTR [-132+ebp], edx                     ;117.3
        call      _for_check_mult_overflow                      ;117.3
                                ; LOE eax edi
.B4.73:                         ; Preds .B4.3
        add       esp, 16                                       ;117.3
                                ; LOE eax edi
.B4.4:                          ; Preds .B4.73
        mov       edx, DWORD PTR [8+ebx]                        ;117.3
        and       eax, 1                                        ;117.3
        mov       ecx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP+32] ;117.3
        neg       ecx                                           ;117.3
        add       ecx, DWORD PTR [edx]                          ;117.3
        mov       esi, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP] ;117.3
        shl       eax, 4                                        ;117.3
        or        eax, 262145                                   ;117.3
        lea       edx, DWORD PTR [ecx+ecx*4]                    ;117.3
        push      eax                                           ;117.3
        lea       ecx, DWORD PTR [esi+edx*8]                    ;117.3
        push      ecx                                           ;117.3
        push      DWORD PTR [-108+ebp]                          ;117.3
        call      _for_allocate                                 ;117.3
                                ; LOE eax edi
.B4.74:                         ; Preds .B4.4
        add       esp, 12                                       ;117.3
        mov       esi, eax                                      ;117.3
                                ; LOE esi edi
.B4.5:                          ; Preds .B4.74
        test      esi, esi                                      ;117.3
        je        .B4.8         ; Prob 50%                      ;117.3
                                ; LOE esi edi
.B4.6:                          ; Preds .B4.5
        push      32                                            ;118.24
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_VECTORERROR], esi ;117.3
        lea       eax, DWORD PTR [-104+ebp]                     ;118.24
        push      eax                                           ;118.24
        push      OFFSET FLAT: __STRLITPACK_50.0.4              ;118.24
        push      -2088435968                                   ;118.24
        push      911                                           ;118.24
        mov       DWORD PTR [-88+ebp], 0                        ;118.24
        lea       esi, DWORD PTR [-88+ebp]                      ;118.24
        push      esi                                           ;118.24
        mov       DWORD PTR [-104+ebp], 25                      ;118.24
        mov       DWORD PTR [-100+ebp], OFFSET FLAT: __STRLITPACK_40 ;118.24
        call      _for_write_seq_lis                            ;118.24
                                ; LOE esi edi
.B4.75:                         ; Preds .B4.6
        add       esp, 24                                       ;118.24
                                ; LOE esi edi
.B4.7:                          ; Preds .B4.75
        mov       DWORD PTR [-112+ebp], 0                       ;118.24
        lea       eax, DWORD PTR [-112+ebp]                     ;118.24
        push      eax                                           ;118.24
        push      OFFSET FLAT: __STRLITPACK_51.0.4              ;118.24
        push      esi                                           ;118.24
        call      _for_write_seq_lis_xmit                       ;118.24
                                ; LOE edi
.B4.76:                         ; Preds .B4.7
        add       esp, 12                                       ;118.24
        jmp       .B4.10        ; Prob 100%                     ;118.24
                                ; LOE edi
.B4.8:                          ; Preds .B4.5
        mov       ecx, DWORD PTR [8+ebx]                        ;117.12
        mov       edx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP] ;117.3
        mov       DWORD PTR [-136+ebp], esi                     ;
        mov       eax, DWORD PTR [ecx]                          ;117.12
        mov       ecx, 4                                        ;117.3
        sub       eax, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP+32] ;117.3
        push      ecx                                           ;117.3
        lea       esi, DWORD PTR [eax+eax*4]                    ;117.3
        mov       eax, 1                                        ;117.3
        mov       DWORD PTR [16+edx+esi*8], eax                 ;117.3
        mov       DWORD PTR [32+edx+esi*8], eax                 ;117.3
        mov       eax, DWORD PTR [-132+ebp]                     ;117.3
        push      eax                                           ;117.3
        mov       DWORD PTR [12+edx+esi*8], 5                   ;117.3
        mov       DWORD PTR [4+edx+esi*8], ecx                  ;117.3
        mov       DWORD PTR [8+edx+esi*8], 0                    ;117.3
        mov       DWORD PTR [24+edx+esi*8], eax                 ;117.3
        mov       DWORD PTR [28+edx+esi*8], ecx                 ;117.3
        lea       edx, DWORD PTR [-116+ebp]                     ;117.3
        push      2                                             ;117.3
        push      edx                                           ;117.3
        mov       esi, DWORD PTR [-136+ebp]                     ;117.3
        call      _for_check_mult_overflow                      ;117.3
                                ; LOE esi edi
.B4.77:                         ; Preds .B4.8
        add       esp, 16                                       ;117.3
                                ; LOE esi edi
.B4.9:                          ; Preds .B4.77
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_VECTORERROR], esi ;117.3
                                ; LOE edi
.B4.10:                         ; Preds .B4.76 .B4.9
        mov       edx, DWORD PTR [8+ebx]                        ;119.3
        test      edi, edi                                      ;122.6
        mov       ecx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP+32] ;119.3
        mov       esi, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP] ;119.3
        mov       edx, DWORD PTR [edx]                          ;119.3
        mov       DWORD PTR [-52+ebp], esi                      ;119.3
        lea       ecx, DWORD PTR [ecx+ecx*4]                    ;119.3
        mov       DWORD PTR [-92+ebp], esp                      ;122.6
        lea       eax, DWORD PTR [edx+edx*4]                    ;119.3
        lea       esi, DWORD PTR [esi+eax*8]                    ;119.3
        mov       eax, 0                                        ;122.6
        cmovg     eax, edi                                      ;122.6
        shl       ecx, 3                                        ;119.3
        sub       esi, ecx                                      ;119.3
        shl       eax, 2                                        ;122.6
        mov       DWORD PTR [-56+ebp], ecx                      ;119.3
        mov       DWORD PTR [36+esi], edi                       ;119.3
        call      __alloca_probe                                ;122.6
        and       esp, -16                                      ;122.6
        mov       eax, esp                                      ;122.6
                                ; LOE eax edx edi
.B4.78:                         ; Preds .B4.10
        mov       DWORD PTR [-48+ebp], eax                      ;122.6
        imul      eax, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST+32], 44 ;
        mov       esi, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST] ;125.6
        test      edi, edi                                      ;122.6
        mov       DWORD PTR [-96+ebp], eax                      ;
        jle       .B4.33        ; Prob 10%                      ;122.6
                                ; LOE edx esi edi
.B4.11:                         ; Preds .B4.78
        imul      eax, edx, 44                                  ;122.6
        add       eax, esi                                      ;122.6
        sub       eax, DWORD PTR [-96+ebp]                      ;122.6
        mov       DWORD PTR [-132+ebp], eax                     ;122.6
        mov       ecx, DWORD PTR [28+eax]                       ;122.6
        cmp       ecx, 4                                        ;122.6
        mov       DWORD PTR [-128+ebp], ecx                     ;122.6
        jne       .B4.20        ; Prob 50%                      ;122.6
                                ; LOE eax edx esi edi al ah
.B4.12:                         ; Preds .B4.11
        cmp       edi, 4                                        ;122.6
        jl        .B4.69        ; Prob 10%                      ;122.6
                                ; LOE eax edx esi edi al ah
.B4.13:                         ; Preds .B4.12
        mov       ecx, eax                                      ;122.6
        mov       eax, edi                                      ;122.6
        and       eax, -4                                       ;122.6
        mov       DWORD PTR [-128+ebp], eax                     ;122.6
        mov       eax, DWORD PTR [32+ecx]                       ;122.6
        shl       eax, 2                                        ;
        neg       eax                                           ;
        add       eax, DWORD PTR [ecx]                          ;
        mov       DWORD PTR [-124+ebp], 0                       ;
        mov       DWORD PTR [-120+ebp], eax                     ;
        mov       DWORD PTR [-136+ebp], edx                     ;
        mov       DWORD PTR [-20+ebp], edi                      ;
        mov       eax, DWORD PTR [-128+ebp]                     ;
        mov       edx, DWORD PTR [-120+ebp]                     ;
        mov       ecx, DWORD PTR [-124+ebp]                     ;
        mov       edi, DWORD PTR [-48+ebp]                      ;
                                ; LOE eax edx ecx esi edi
.B4.14:                         ; Preds .B4.14 .B4.13
        movdqu    xmm0, XMMWORD PTR [4+edx+ecx*4]               ;122.6
        movdqa    XMMWORD PTR [edi+ecx*4], xmm0                 ;122.6
        add       ecx, 4                                        ;122.6
        cmp       ecx, eax                                      ;122.6
        jb        .B4.14        ; Prob 82%                      ;122.6
                                ; LOE eax edx ecx esi edi
.B4.15:                         ; Preds .B4.14
        mov       DWORD PTR [-128+ebp], eax                     ;
        mov       edx, DWORD PTR [-136+ebp]                     ;
        mov       edi, DWORD PTR [-20+ebp]                      ;
                                ; LOE edx esi edi
.B4.16:                         ; Preds .B4.15 .B4.69
        cmp       edi, DWORD PTR [-128+ebp]                     ;122.6
        jbe       .B4.64        ; Prob 0%                       ;122.6
                                ; LOE edx esi edi
.B4.17:                         ; Preds .B4.16
        mov       eax, DWORD PTR [-132+ebp]                     ;122.6
        mov       DWORD PTR [-120+ebp], esi                     ;
        mov       DWORD PTR [-136+ebp], edx                     ;
        mov       ecx, DWORD PTR [32+eax]                       ;122.6
        shl       ecx, 2                                        ;
        neg       ecx                                           ;
        add       ecx, DWORD PTR [eax]                          ;
        mov       edx, ecx                                      ;
        mov       ecx, DWORD PTR [-128+ebp]                     ;
        mov       esi, DWORD PTR [-48+ebp]                      ;
                                ; LOE edx ecx esi edi
.B4.18:                         ; Preds .B4.18 .B4.17
        mov       eax, DWORD PTR [4+edx+ecx*4]                  ;122.6
        mov       DWORD PTR [esi+ecx*4], eax                    ;122.6
        inc       ecx                                           ;122.6
        cmp       ecx, edi                                      ;122.6
        jb        .B4.18        ; Prob 82%                      ;122.6
                                ; LOE edx ecx esi edi
.B4.19:                         ; Preds .B4.18
        mov       eax, edi                                      ;
        shr       eax, 31                                       ;
        add       eax, edi                                      ;
        sar       eax, 1                                        ;
        mov       esi, DWORD PTR [-120+ebp]                     ;
        mov       edx, DWORD PTR [-136+ebp]                     ;
        mov       DWORD PTR [-44+ebp], eax                      ;
        jmp       .B4.26        ; Prob 100%                     ;
                                ; LOE edx esi edi
.B4.20:                         ; Preds .B4.11
        mov       eax, edi                                      ;122.6
        shr       eax, 31                                       ;122.6
        add       eax, edi                                      ;122.6
        sar       eax, 1                                        ;122.6
        mov       DWORD PTR [-44+ebp], eax                      ;122.6
        test      eax, eax                                      ;122.6
        jbe       .B4.70        ; Prob 0%                       ;122.6
                                ; LOE edx esi edi
.B4.21:                         ; Preds .B4.20
        xor       ecx, ecx                                      ;
        mov       DWORD PTR [-124+ebp], ecx                     ;
        mov       ecx, DWORD PTR [-132+ebp]                     ;122.6
        mov       DWORD PTR [-120+ebp], esi                     ;
        mov       DWORD PTR [-136+ebp], edx                     ;
        mov       eax, DWORD PTR [32+ecx]                       ;122.6
        imul      eax, DWORD PTR [-128+ebp]                     ;
        mov       ecx, DWORD PTR [ecx]                          ;122.6
        sub       ecx, eax                                      ;
        mov       eax, DWORD PTR [-124+ebp]                     ;
        mov       esi, ecx                                      ;
        mov       edx, DWORD PTR [-128+ebp]                     ;
        mov       ecx, DWORD PTR [-48+ebp]                      ;
        mov       DWORD PTR [-20+ebp], edi                      ;
                                ; LOE eax edx ecx esi
.B4.22:                         ; Preds .B4.22 .B4.21
        lea       edi, DWORD PTR [1+eax+eax]                    ;122.6
        imul      edi, edx                                      ;122.6
        mov       edi, DWORD PTR [esi+edi]                      ;122.6
        mov       DWORD PTR [ecx+eax*8], edi                    ;122.6
        lea       edi, DWORD PTR [2+eax+eax]                    ;122.6
        imul      edi, edx                                      ;122.6
        mov       edi, DWORD PTR [esi+edi]                      ;122.6
        mov       DWORD PTR [4+ecx+eax*8], edi                  ;122.6
        inc       eax                                           ;122.6
        cmp       eax, DWORD PTR [-44+ebp]                      ;122.6
        jb        .B4.22        ; Prob 64%                      ;122.6
                                ; LOE eax edx ecx esi
.B4.23:                         ; Preds .B4.22
        mov       esi, DWORD PTR [-120+ebp]                     ;
        lea       ecx, DWORD PTR [1+eax+eax]                    ;122.6
        mov       edx, DWORD PTR [-136+ebp]                     ;
        mov       edi, DWORD PTR [-20+ebp]                      ;
                                ; LOE edx ecx esi edi
.B4.24:                         ; Preds .B4.23 .B4.70
        lea       eax, DWORD PTR [-1+ecx]                       ;122.6
        cmp       edi, eax                                      ;122.6
        jbe       .B4.26        ; Prob 0%                       ;122.6
                                ; LOE edx ecx esi edi
.B4.25:                         ; Preds .B4.24
        mov       DWORD PTR [-120+ebp], esi                     ;
        mov       esi, DWORD PTR [-132+ebp]                     ;122.6
        mov       eax, DWORD PTR [32+esi]                       ;122.6
        neg       eax                                           ;122.6
        add       eax, ecx                                      ;122.6
        imul      eax, DWORD PTR [-128+ebp]                     ;122.6
        mov       esi, DWORD PTR [esi]                          ;122.6
        mov       eax, DWORD PTR [esi+eax]                      ;122.6
        mov       esi, DWORD PTR [-48+ebp]                      ;122.6
        mov       DWORD PTR [-4+esi+ecx*4], eax                 ;122.6
        mov       esi, DWORD PTR [-120+ebp]                     ;122.6
                                ; LOE edx esi edi
.B4.26:                         ; Preds .B4.19 .B4.64 .B4.25 .B4.24
        cmp       DWORD PTR [-44+ebp], 0                        ;122.6
        jbe       .B4.68        ; Prob 11%                      ;122.6
                                ; LOE edx esi edi
.B4.27:                         ; Preds .B4.26
        mov       edx, DWORD PTR [-52+ebp]                      ;
        xor       eax, eax                                      ;
        sub       edx, DWORD PTR [-56+ebp]                      ;
        mov       DWORD PTR [-120+ebp], esi                     ;
        mov       DWORD PTR [-136+ebp], edx                     ;
        mov       DWORD PTR [-20+ebp], edi                      ;
        ALIGN     16
                                ; LOE eax
.B4.28:                         ; Preds .B4.28 .B4.27
        mov       edi, DWORD PTR [8+ebx]                        ;122.6
        lea       esi, DWORD PTR [1+eax+eax]                    ;122.6
        mov       ecx, DWORD PTR [-136+ebp]                     ;122.6
        mov       edx, DWORD PTR [edi]                          ;122.6
        lea       edx, DWORD PTR [edx+edx*4]                    ;122.6
        sub       esi, DWORD PTR [32+ecx+edx*8]                 ;122.6
        imul      esi, DWORD PTR [28+ecx+edx*8]                 ;122.6
        mov       ecx, DWORD PTR [ecx+edx*8]                    ;122.6
        mov       edx, DWORD PTR [-48+ebp]                      ;122.6
        mov       edx, DWORD PTR [edx+eax*8]                    ;122.6
        mov       DWORD PTR [ecx+esi], edx                      ;122.6
        lea       edx, DWORD PTR [2+eax+eax]                    ;122.6
        mov       esi, DWORD PTR [edi]                          ;122.6
        mov       edi, DWORD PTR [-136+ebp]                     ;122.6
        lea       ecx, DWORD PTR [esi+esi*4]                    ;122.6
        mov       esi, DWORD PTR [-48+ebp]                      ;122.6
        sub       edx, DWORD PTR [32+edi+ecx*8]                 ;122.6
        imul      edx, DWORD PTR [28+edi+ecx*8]                 ;122.6
        mov       ecx, DWORD PTR [edi+ecx*8]                    ;122.6
        mov       edi, DWORD PTR [4+esi+eax*8]                  ;122.6
        inc       eax                                           ;122.6
        cmp       eax, DWORD PTR [-44+ebp]                      ;122.6
        mov       DWORD PTR [ecx+edx], edi                      ;122.6
        jb        .B4.28        ; Prob 64%                      ;122.6
                                ; LOE eax
.B4.29:                         ; Preds .B4.28
        mov       edx, DWORD PTR [8+ebx]                        ;122.6
        lea       eax, DWORD PTR [1+eax+eax]                    ;122.6
        mov       esi, DWORD PTR [-120+ebp]                     ;
        mov       edi, DWORD PTR [-20+ebp]                      ;
        mov       edx, DWORD PTR [edx]                          ;122.6
                                ; LOE eax edx esi edi
.B4.30:                         ; Preds .B4.29 .B4.68
        lea       ecx, DWORD PTR [-1+eax]                       ;122.6
        cmp       edi, ecx                                      ;122.6
        jbe       .B4.33        ; Prob 11%                      ;122.6
                                ; LOE eax edx esi edi
.B4.31:                         ; Preds .B4.30
        mov       ecx, DWORD PTR [-52+ebp]                      ;122.6
        lea       edx, DWORD PTR [edx+edx*4]                    ;122.6
        sub       ecx, DWORD PTR [-56+ebp]                      ;122.6
        mov       DWORD PTR [-120+ebp], esi                     ;
        mov       esi, DWORD PTR [32+ecx+edx*8]                 ;122.6
        neg       esi                                           ;122.6
        add       esi, eax                                      ;122.6
        imul      esi, DWORD PTR [28+ecx+edx*8]                 ;122.6
        mov       edx, DWORD PTR [ecx+edx*8]                    ;122.6
        mov       ecx, DWORD PTR [-48+ebp]                      ;122.6
        mov       eax, DWORD PTR [-4+ecx+eax*4]                 ;122.6
        mov       DWORD PTR [edx+esi], eax                      ;122.6
        mov       eax, DWORD PTR [8+ebx]                        ;125.6
        mov       esi, DWORD PTR [-120+ebp]                     ;125.6
        mov       edx, DWORD PTR [eax]                          ;125.6
                                ; LOE edx esi edi
.B4.33:                         ; Preds .B4.30 .B4.31 .B4.78
        mov       eax, DWORD PTR [-92+ebp]                      ;122.6
        mov       esp, eax                                      ;122.6
                                ; LOE edx esi edi
.B4.79:                         ; Preds .B4.33
        imul      eax, edx, 44                                  ;125.10
        sub       esi, DWORD PTR [-96+ebp]                      ;125.10
        mov       DWORD PTR [-136+ebp], eax                     ;125.10
        mov       edx, DWORD PTR [12+eax+esi]                   ;125.6
        test      dl, 1                                         ;125.10
        mov       DWORD PTR [-132+ebp], edx                     ;125.6
        je        .B4.38        ; Prob 60%                      ;125.10
                                ; LOE edx esi edi dl dh
.B4.34:                         ; Preds .B4.79
        mov       eax, edx                                      ;126.4
        mov       edx, eax                                      ;126.4
        shr       edx, 1                                        ;126.4
        and       eax, 1                                        ;126.4
        and       edx, 1                                        ;126.4
        add       eax, eax                                      ;126.4
        shl       edx, 2                                        ;126.4
        or        edx, 1                                        ;126.4
        or        edx, eax                                      ;126.4
        mov       ecx, DWORD PTR [-136+ebp]                     ;126.4
        or        edx, 262144                                   ;126.4
        push      edx                                           ;126.4
        push      DWORD PTR [ecx+esi]                           ;126.4
        call      _for_dealloc_allocatable                      ;126.4
                                ; LOE eax esi edi
.B4.80:                         ; Preds .B4.34
        add       esp, 8                                        ;126.4
                                ; LOE eax esi edi
.B4.35:                         ; Preds .B4.80
        mov       ecx, DWORD PTR [-136+ebp]                     ;126.4
        mov       edx, DWORD PTR [-132+ebp]                     ;126.4
        and       edx, -2                                       ;126.4
        mov       DWORD PTR [ecx+esi], 0                        ;126.4
        test      eax, eax                                      ;127.19
        mov       DWORD PTR [12+ecx+esi], edx                   ;126.4
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_VECTORERROR], eax ;126.4
        je        .B4.38        ; Prob 50%                      ;127.19
                                ; LOE edi
.B4.36:                         ; Preds .B4.35
        push      32                                            ;127.25
        mov       DWORD PTR [-88+ebp], 0                        ;127.25
        lea       eax, DWORD PTR [-136+ebp]                     ;127.25
        push      eax                                           ;127.25
        push      OFFSET FLAT: __STRLITPACK_52.0.4              ;127.25
        push      -2088435968                                   ;127.25
        push      911                                           ;127.25
        mov       DWORD PTR [-136+ebp], 39                      ;127.25
        lea       esi, DWORD PTR [-88+ebp]                      ;127.25
        push      esi                                           ;127.25
        mov       DWORD PTR [-132+ebp], OFFSET FLAT: __STRLITPACK_38 ;127.25
        call      _for_write_seq_lis                            ;127.25
                                ; LOE esi edi
.B4.81:                         ; Preds .B4.36
        add       esp, 24                                       ;127.25
                                ; LOE esi edi
.B4.37:                         ; Preds .B4.81
        mov       DWORD PTR [-128+ebp], 0                       ;127.25
        lea       eax, DWORD PTR [-128+ebp]                     ;127.25
        push      eax                                           ;127.25
        push      OFFSET FLAT: __STRLITPACK_53.0.4              ;127.25
        push      esi                                           ;127.25
        call      _for_write_seq_lis_xmit                       ;127.25
                                ; LOE edi
.B4.82:                         ; Preds .B4.37
        add       esp, 12                                       ;127.25
                                ; LOE edi
.B4.38:                         ; Preds .B4.82 .B4.35 .B4.79 .B4.2
        mov       eax, DWORD PTR [12+ebx]                       ;107.12
        xor       edx, edx                                      ;132.4
        push      4                                             ;132.4
        lea       esi, DWORD PTR [-48+ebp]                      ;132.4
        mov       ecx, DWORD PTR [eax]                          ;132.4
        test      ecx, ecx                                      ;132.4
        cmovl     ecx, edx                                      ;132.4
        push      ecx                                           ;132.4
        push      2                                             ;132.4
        push      esi                                           ;132.4
        call      _for_check_mult_overflow                      ;132.4
                                ; LOE eax edi
.B4.83:                         ; Preds .B4.38
        add       esp, 16                                       ;132.4
                                ; LOE eax edi
.B4.39:                         ; Preds .B4.83
        mov       edx, DWORD PTR [8+ebx]                        ;132.4
        and       eax, 1                                        ;132.4
        mov       ecx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST+32] ;132.4
        neg       ecx                                           ;132.4
        add       ecx, DWORD PTR [edx]                          ;132.4
        imul      esi, ecx, 44                                  ;132.4
        shl       eax, 4                                        ;132.4
        or        eax, 262145                                   ;132.4
        add       esi, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST] ;132.4
        push      eax                                           ;132.4
        push      esi                                           ;132.4
        push      DWORD PTR [-48+ebp]                           ;132.4
        call      _for_allocate                                 ;132.4
                                ; LOE eax edi
.B4.84:                         ; Preds .B4.39
        add       esp, 12                                       ;132.4
        mov       esi, eax                                      ;132.4
                                ; LOE esi edi
.B4.40:                         ; Preds .B4.84
        test      esi, esi                                      ;132.4
        jne       .B4.43        ; Prob 50%                      ;132.4
                                ; LOE esi edi
.B4.41:                         ; Preds .B4.40
        mov       DWORD PTR [-124+ebp], esi                     ;
        mov       esi, DWORD PTR [8+ebx]                        ;132.13
        mov       ecx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST] ;132.4
        mov       DWORD PTR [-20+ebp], edi                      ;
        xor       edi, edi                                      ;132.4
        mov       eax, DWORD PTR [esi]                          ;132.13
        mov       esi, 4                                        ;132.4
        sub       eax, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST+32] ;132.4
        imul      edx, eax, 44                                  ;132.4
        mov       eax, 1                                        ;132.4
        mov       DWORD PTR [16+ecx+edx], eax                   ;132.4
        mov       DWORD PTR [32+ecx+edx], eax                   ;132.4
        mov       eax, DWORD PTR [12+ebx]                       ;132.4
        push      esi                                           ;132.4
        mov       DWORD PTR [12+ecx+edx], 5                     ;132.4
        mov       eax, DWORD PTR [eax]                          ;132.4
        test      eax, eax                                      ;132.4
        mov       DWORD PTR [4+ecx+edx], esi                    ;132.4
        cmovl     eax, edi                                      ;132.4
        push      eax                                           ;132.4
        mov       DWORD PTR [8+ecx+edx], edi                    ;132.4
        mov       DWORD PTR [24+ecx+edx], eax                   ;132.4
        mov       DWORD PTR [28+ecx+edx], esi                   ;132.4
        lea       edx, DWORD PTR [-120+ebp]                     ;132.4
        push      2                                             ;132.4
        push      edx                                           ;132.4
        mov       esi, DWORD PTR [-124+ebp]                     ;132.4
        mov       edi, DWORD PTR [-20+ebp]                      ;132.4
        call      _for_check_mult_overflow                      ;132.4
                                ; LOE esi edi
.B4.85:                         ; Preds .B4.41
        add       esp, 16                                       ;132.4
                                ; LOE esi edi
.B4.42:                         ; Preds .B4.85
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_VECTORERROR], esi ;132.4
        jmp       .B4.45        ; Prob 100%                     ;132.4
                                ; LOE edi
.B4.43:                         ; Preds .B4.40
        push      32                                            ;133.23
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_VECTORERROR], esi ;132.4
        lea       eax, DWORD PTR [-56+ebp]                      ;133.23
        push      eax                                           ;133.23
        push      OFFSET FLAT: __STRLITPACK_54.0.4              ;133.23
        push      -2088435968                                   ;133.23
        push      911                                           ;133.23
        mov       DWORD PTR [-88+ebp], 0                        ;133.23
        lea       esi, DWORD PTR [-88+ebp]                      ;133.23
        push      esi                                           ;133.23
        mov       DWORD PTR [-56+ebp], 33                       ;133.23
        mov       DWORD PTR [-52+ebp], OFFSET FLAT: __STRLITPACK_36 ;133.23
        call      _for_write_seq_lis                            ;133.23
                                ; LOE esi edi
.B4.86:                         ; Preds .B4.43
        add       esp, 24                                       ;133.23
                                ; LOE esi edi
.B4.44:                         ; Preds .B4.86
        mov       DWORD PTR [-96+ebp], 0                        ;133.23
        lea       eax, DWORD PTR [-96+ebp]                      ;133.23
        push      eax                                           ;133.23
        push      OFFSET FLAT: __STRLITPACK_55.0.4              ;133.23
        push      esi                                           ;133.23
        call      _for_write_seq_lis_xmit                       ;133.23
                                ; LOE edi
.B4.87:                         ; Preds .B4.44
        add       esp, 12                                       ;133.23
                                ; LOE edi
.B4.45:                         ; Preds .B4.87 .B4.42
        mov       eax, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST] ;138.30
        test      edi, edi                                      ;136.15
        mov       esi, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST+32] ;138.30
        jle       .B4.67        ; Prob 16%                      ;136.15
                                ; LOE eax esi edi
.B4.46:                         ; Preds .B4.45
        imul      esi, esi, -44                                 ;
        mov       ecx, edi                                      ;138.30
        shr       ecx, 31                                       ;138.30
        add       eax, esi                                      ;
        add       ecx, edi                                      ;138.30
        mov       edx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP] ;138.6
        sar       ecx, 1                                        ;138.30
        mov       DWORD PTR [-92+ebp], edx                      ;138.6
        test      ecx, ecx                                      ;138.30
        mov       edx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP+32] ;138.6
        jbe       .B4.66        ; Prob 0%                       ;138.30
                                ; LOE eax edx ecx edi
.B4.47:                         ; Preds .B4.46
        xor       esi, esi                                      ;
        mov       DWORD PTR [-32+ebp], esi                      ;
        imul      esi, edx, -40                                 ;
        add       esi, DWORD PTR [-92+ebp]                      ;
        mov       DWORD PTR [-28+ebp], esi                      ;
        mov       DWORD PTR [-16+ebp], eax                      ;
        mov       DWORD PTR [-44+ebp], ecx                      ;
        mov       DWORD PTR [-124+ebp], edx                     ;
        mov       DWORD PTR [-20+ebp], edi                      ;
                                ; LOE
.B4.48:                         ; Preds .B4.48 .B4.47
        mov       esi, DWORD PTR [8+ebx]                        ;138.30
        mov       edx, DWORD PTR [-32+ebp]                      ;138.6
        mov       eax, DWORD PTR [esi]                          ;138.30
        mov       esi, DWORD PTR [-28+ebp]                      ;138.30
        lea       ecx, DWORD PTR [1+edx+edx]                    ;138.6
        imul      edi, eax, 44                                  ;138.6
        lea       edx, DWORD PTR [eax+eax*4]                    ;138.6
        mov       eax, DWORD PTR [32+esi+edx*8]                 ;138.30
        neg       eax                                           ;138.6
        add       eax, ecx                                      ;138.6
        imul      eax, DWORD PTR [28+esi+edx*8]                 ;138.6
        mov       edx, DWORD PTR [esi+edx*8]                    ;138.30
        mov       esi, DWORD PTR [-16+ebp]                      ;138.6
        mov       eax, DWORD PTR [edx+eax]                      ;138.6
        sub       ecx, DWORD PTR [32+edi+esi]                   ;138.6
        imul      ecx, DWORD PTR [28+edi+esi]                   ;138.6
        mov       edi, DWORD PTR [edi+esi]                      ;138.6
        mov       DWORD PTR [edi+ecx], eax                      ;138.6
        mov       ecx, DWORD PTR [8+ebx]                        ;138.30
        mov       edi, DWORD PTR [-32+ebp]                      ;138.6
        mov       edx, DWORD PTR [ecx]                          ;138.30
        imul      ecx, edx, 44                                  ;138.6
        lea       eax, DWORD PTR [2+edi+edi]                    ;138.6
        mov       edi, DWORD PTR [-28+ebp]                      ;138.30
        lea       edx, DWORD PTR [edx+edx*4]                    ;138.6
        mov       DWORD PTR [-24+ebp], ecx                      ;138.6
        mov       ecx, DWORD PTR [32+edi+edx*8]                 ;138.30
        neg       ecx                                           ;138.6
        add       ecx, eax                                      ;138.6
        imul      ecx, DWORD PTR [28+edi+edx*8]                 ;138.6
        mov       edi, DWORD PTR [edi+edx*8]                    ;138.30
        mov       edx, DWORD PTR [-24+ebp]                      ;138.6
        sub       eax, DWORD PTR [32+edx+esi]                   ;138.6
        imul      eax, DWORD PTR [28+edx+esi]                   ;138.6
        mov       edx, DWORD PTR [edx+esi]                      ;138.6
        mov       esi, DWORD PTR [edi+ecx]                      ;138.6
        mov       DWORD PTR [edx+eax], esi                      ;138.6
        mov       eax, DWORD PTR [-32+ebp]                      ;138.30
        inc       eax                                           ;138.30
        mov       DWORD PTR [-32+ebp], eax                      ;138.30
        cmp       eax, DWORD PTR [-44+ebp]                      ;138.30
        jb        .B4.48        ; Prob 64%                      ;138.30
                                ; LOE eax al ah
.B4.49:                         ; Preds .B4.48
        mov       ecx, eax                                      ;138.30
        mov       eax, DWORD PTR [-16+ebp]                      ;
        mov       edx, DWORD PTR [-124+ebp]                     ;
        mov       edi, DWORD PTR [-20+ebp]                      ;
        lea       ecx, DWORD PTR [1+ecx+ecx]                    ;138.30
                                ; LOE eax edx ecx edi
.B4.50:                         ; Preds .B4.49 .B4.66
        lea       esi, DWORD PTR [-1+ecx]                       ;138.30
        cmp       edi, esi                                      ;138.30
        jbe       .B4.52        ; Prob 0%                       ;138.30
                                ; LOE eax edx ecx edi
.B4.51:                         ; Preds .B4.50
        mov       esi, DWORD PTR [8+ebx]                        ;138.6
        neg       edx                                           ;138.6
        mov       DWORD PTR [-20+ebp], edi                      ;
        add       edx, DWORD PTR [esi]                          ;138.6
        imul      edi, DWORD PTR [esi], 44                      ;138.6
        mov       DWORD PTR [-124+ebp], edi                     ;138.6
        lea       esi, DWORD PTR [edx+edx*4]                    ;138.6
        mov       edx, DWORD PTR [-92+ebp]                      ;138.30
        mov       edi, DWORD PTR [32+edx+esi*8]                 ;138.30
        neg       edi                                           ;138.6
        add       edi, ecx                                      ;138.6
        imul      edi, DWORD PTR [28+edx+esi*8]                 ;138.6
        mov       esi, DWORD PTR [edx+esi*8]                    ;138.30
        mov       edx, DWORD PTR [-124+ebp]                     ;138.6
        mov       edi, DWORD PTR [esi+edi]                      ;138.6
        sub       ecx, DWORD PTR [32+edx+eax]                   ;138.6
        imul      ecx, DWORD PTR [28+edx+eax]                   ;138.6
        mov       edx, DWORD PTR [edx+eax]                      ;138.6
        mov       DWORD PTR [edx+ecx], edi                      ;138.6
        mov       edi, DWORD PTR [-20+ebp]                      ;138.6
                                ; LOE eax edi
.B4.52:                         ; Preds .B4.51 .B4.50 .B4.67
        mov       ecx, DWORD PTR [12+ebx]                       ;143.4
        lea       edx, DWORD PTR [1+edi]                        ;143.4
        mov       DWORD PTR [-92+ebp], edx                      ;143.4
        mov       ecx, DWORD PTR [ecx]                          ;143.4
        cmp       ecx, edi                                      ;143.4
        jle       .B4.60        ; Prob 50%                      ;143.4
                                ; LOE eax ecx edi
.B4.53:                         ; Preds .B4.52
        mov       edx, ecx                                      ;143.4
        sub       edx, DWORD PTR [-92+ebp]                      ;143.4
        inc       edx                                           ;143.4
        mov       esi, edx                                      ;143.4
        shr       esi, 31                                       ;143.4
        add       esi, edx                                      ;143.4
        sar       esi, 1                                        ;143.4
        mov       DWORD PTR [-44+ebp], esi                      ;143.4
        test      esi, esi                                      ;143.4
        jbe       .B4.65        ; Prob 10%                      ;143.4
                                ; LOE eax ecx edi
.B4.54:                         ; Preds .B4.53
        mov       DWORD PTR [-16+ebp], eax                      ;
        xor       edx, edx                                      ;
        mov       DWORD PTR [-124+ebp], ecx                     ;
        mov       DWORD PTR [-20+ebp], edi                      ;
        ALIGN     16
                                ; LOE edx
.B4.55:                         ; Preds .B4.55 .B4.54
        mov       eax, DWORD PTR [8+ebx]                        ;144.7
        mov       ecx, DWORD PTR [-20+ebp]                      ;144.7
        mov       DWORD PTR [-32+ebp], edx                      ;
        imul      edi, DWORD PTR [eax], 44                      ;144.7
        lea       ecx, DWORD PTR [1+ecx+edx*2]                  ;144.7
        mov       edx, DWORD PTR [-16+ebp]                      ;144.7
        mov       esi, DWORD PTR [32+edi+edx]                   ;144.7
        neg       esi                                           ;144.7
        add       esi, ecx                                      ;144.7
        inc       ecx                                           ;144.7
        imul      esi, DWORD PTR [28+edi+edx]                   ;144.7
        mov       edi, DWORD PTR [edi+edx]                      ;144.7
        xor       edx, edx                                      ;144.7
        mov       DWORD PTR [edi+esi], edx                      ;144.7
        imul      eax, DWORD PTR [eax], 44                      ;144.7
        mov       esi, DWORD PTR [-16+ebp]                      ;144.7
        sub       ecx, DWORD PTR [32+eax+esi]                   ;144.7
        imul      ecx, DWORD PTR [28+eax+esi]                   ;144.7
        mov       eax, DWORD PTR [eax+esi]                      ;144.7
        mov       DWORD PTR [eax+ecx], edx                      ;144.7
        mov       edx, DWORD PTR [-32+ebp]                      ;143.4
        inc       edx                                           ;143.4
        cmp       edx, DWORD PTR [-44+ebp]                      ;143.4
        jb        .B4.55        ; Prob 63%                      ;143.4
                                ; LOE edx esi
.B4.56:                         ; Preds .B4.55
        mov       eax, esi                                      ;
        lea       edx, DWORD PTR [1+edx+edx]                    ;143.4
        mov       ecx, DWORD PTR [-124+ebp]                     ;
        mov       edi, DWORD PTR [-20+ebp]                      ;
                                ; LOE eax edx ecx edi
.B4.57:                         ; Preds .B4.56 .B4.65
        sub       ecx, DWORD PTR [-92+ebp]                      ;143.4
        lea       esi, DWORD PTR [-1+edx]                       ;143.4
        inc       ecx                                           ;143.4
        cmp       ecx, esi                                      ;143.4
        jbe       .B4.59        ; Prob 10%                      ;143.4
                                ; LOE eax edx edi
.B4.58:                         ; Preds .B4.57
        mov       ecx, DWORD PTR [8+ebx]                        ;144.7
        imul      esi, DWORD PTR [ecx], 44                      ;144.7
        lea       ecx, DWORD PTR [edx+edi]                      ;144.7
        sub       ecx, DWORD PTR [32+esi+eax]                   ;144.7
        imul      ecx, DWORD PTR [28+esi+eax]                   ;144.7
        mov       edx, DWORD PTR [esi+eax]                      ;144.7
        mov       DWORD PTR [edx+ecx], 0                        ;144.7
                                ; LOE eax edi
.B4.59:                         ; Preds .B4.57 .B4.58
        mov       edx, DWORD PTR [12+ebx]                       ;147.4
        mov       ecx, DWORD PTR [edx]                          ;147.4
                                ; LOE eax ecx edi
.B4.60:                         ; Preds .B4.59 .B4.52
        mov       edx, DWORD PTR [8+ebx]                        ;147.4
        mov       edx, DWORD PTR [edx]                          ;147.4
        imul      esi, edx, 44                                  ;147.4
        mov       DWORD PTR [36+esi+eax], ecx                   ;147.4
        test      edi, edi                                      ;149.15
        mov       DWORD PTR [40+esi+eax], edi                   ;148.4
        jle       .B4.63        ; Prob 79%                      ;149.15
                                ; LOE edx
.B4.61:                         ; Preds .B4.60
        mov       esi, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP+32] ;150.7
        sub       edx, esi                                      ;150.7
        mov       ecx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP] ;150.7
        lea       edi, DWORD PTR [edx+edx*4]                    ;150.7
        mov       esi, DWORD PTR [12+ecx+edi*8]                 ;150.18
        mov       edx, esi                                      ;150.7
        shr       edx, 1                                        ;150.7
        mov       eax, esi                                      ;150.7
        and       edx, 1                                        ;150.7
        and       eax, 1                                        ;150.7
        shl       edx, 2                                        ;150.7
        add       eax, eax                                      ;150.7
        or        edx, eax                                      ;150.7
        or        edx, 262144                                   ;150.7
        push      edx                                           ;150.7
        push      DWORD PTR [ecx+edi*8]                         ;150.7
        mov       DWORD PTR [-124+ebp], ecx                     ;150.7
        call      _for_dealloc_allocatable                      ;150.7
                                ; LOE esi edi
.B4.88:                         ; Preds .B4.61
        mov       ecx, DWORD PTR [-124+ebp]                     ;
        add       esp, 8                                        ;150.7
                                ; LOE ecx esi edi cl ch
.B4.62:                         ; Preds .B4.88
        and       esi, -2                                       ;150.7
        mov       DWORD PTR [ecx+edi*8], 0                      ;150.7
        mov       DWORD PTR [12+ecx+edi*8], esi                 ;150.7
                                ; LOE
.B4.63:                         ; Preds .B4.60 .B4.62
        mov       esi, DWORD PTR [-40+ebp]                      ;154.1
        mov       edi, DWORD PTR [-36+ebp]                      ;154.1
        mov       esp, ebp                                      ;154.1
        pop       ebp                                           ;154.1
        mov       esp, ebx                                      ;154.1
        pop       ebx                                           ;154.1
        ret                                                     ;154.1
                                ; LOE
.B4.64:                         ; Preds .B4.16                  ; Infreq
        mov       eax, edi                                      ;
        shr       eax, 31                                       ;
        add       eax, edi                                      ;
        sar       eax, 1                                        ;
        mov       DWORD PTR [-44+ebp], eax                      ;
        jmp       .B4.26        ; Prob 100%                     ;
                                ; LOE edx esi edi
.B4.65:                         ; Preds .B4.53                  ; Infreq
        mov       edx, 1                                        ;
        jmp       .B4.57        ; Prob 100%                     ;
                                ; LOE eax edx ecx edi
.B4.66:                         ; Preds .B4.46                  ; Infreq
        mov       ecx, 1                                        ;
        jmp       .B4.50        ; Prob 100%                     ;
                                ; LOE eax edx ecx edi
.B4.67:                         ; Preds .B4.45                  ; Infreq
        imul      edx, esi, -44                                 ;
        add       eax, edx                                      ;
        jmp       .B4.52        ; Prob 100%                     ;
                                ; LOE eax edi
.B4.68:                         ; Preds .B4.26                  ; Infreq
        mov       eax, 1                                        ;
        jmp       .B4.30        ; Prob 100%                     ;
                                ; LOE eax edx esi edi
.B4.69:                         ; Preds .B4.12                  ; Infreq
        xor       eax, eax                                      ;122.6
        mov       DWORD PTR [-128+ebp], eax                     ;122.6
        jmp       .B4.16        ; Prob 100%                     ;122.6
                                ; LOE edx esi edi
.B4.70:                         ; Preds .B4.20                  ; Infreq
        mov       ecx, 1                                        ;
        jmp       .B4.24        ; Prob 100%                     ;
        ALIGN     16
                                ; LOE edx ecx esi edi
; mark_end;
_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKATT_SETUP ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_LINK_VEH_LIST_MODULE_mp_LINKATT_SETUP
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_LINK_VEH_LIST_MODULE_mp_LINKATT_2DREMOVE
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_LINK_VEH_LIST_MODULE_mp_LINKATT_2DREMOVE
_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKATT_2DREMOVE	PROC NEAR 
.B5.1:                          ; Preds .B5.0
        push      ebp                                           ;157.12
        mov       ebp, esp                                      ;157.12
        and       esp, -16                                      ;157.12
        push      esi                                           ;157.12
        sub       esp, 108                                      ;157.12
        mov       ecx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKATT_ARRAYSIZE] ;161.3
        test      ecx, ecx                                      ;161.3
        jle       .B5.12        ; Prob 2%                       ;161.3
                                ; LOE ecx ebx edi
.B5.2:                          ; Preds .B5.1
        imul      eax, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST+32], -44 ;
        mov       edx, 1                                        ;
        add       eax, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST] ;
        mov       esi, 44                                       ;
        mov       DWORD PTR [8+esp], ecx                        ;
        mov       DWORD PTR [4+esp], edi                        ;
        mov       edi, edx                                      ;
        mov       DWORD PTR [esp], ebx                          ;
        mov       ebx, eax                                      ;
                                ; LOE ebx esi edi
.B5.3:                          ; Preds .B5.10 .B5.2
        cmp       DWORD PTR [36+esi+ebx], 0                     ;162.10
        jle       .B5.10        ; Prob 79%                      ;162.10
                                ; LOE ebx esi edi
.B5.4:                          ; Preds .B5.3
        mov       ecx, DWORD PTR [12+esi+ebx]                   ;162.10
        mov       eax, ecx                                      ;162.10
        shr       eax, 1                                        ;162.10
        and       ecx, 1                                        ;162.10
        and       eax, 1                                        ;162.10
        add       ecx, ecx                                      ;162.10
        shl       eax, 2                                        ;162.10
        or        eax, 1                                        ;162.10
        or        eax, ecx                                      ;162.10
        or        eax, 262144                                   ;162.10
        push      eax                                           ;162.10
        push      DWORD PTR [esi+ebx]                           ;162.10
        call      _for_dealloc_allocatable                      ;162.10
                                ; LOE eax ebx esi edi
.B5.20:                         ; Preds .B5.4
        add       esp, 8                                        ;162.10
                                ; LOE eax ebx esi edi
.B5.5:                          ; Preds .B5.20
        and       DWORD PTR [12+esi+ebx], -2                    ;162.10
        mov       DWORD PTR [esi+ebx], 0                        ;162.10
        test      eax, eax                                      ;162.10
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_VECTORERROR], eax ;162.10
        je        .B5.9         ; Prob 50%                      ;162.10
                                ; LOE ebx esi edi
.B5.6:                          ; Preds .B5.5
        mov       DWORD PTR [48+esp], 0                         ;162.10
        lea       eax, DWORD PTR [48+esp]                       ;162.10
        mov       DWORD PTR [80+esp], 36                        ;162.10
        lea       ecx, DWORD PTR [80+esp]                       ;162.10
        mov       DWORD PTR [84+esp], OFFSET FLAT: __STRLITPACK_30 ;162.10
        push      32                                            ;162.10
        push      ecx                                           ;162.10
        push      OFFSET FLAT: __STRLITPACK_60.0.7              ;162.10
        push      -2088435968                                   ;162.10
        push      911                                           ;162.10
        push      eax                                           ;162.10
        call      _for_write_seq_lis                            ;162.10
                                ; LOE ebx esi edi
.B5.7:                          ; Preds .B5.6
        mov       DWORD PTR [112+esp], edi                      ;162.10
        lea       ecx, DWORD PTR [112+esp]                      ;162.10
        push      ecx                                           ;162.10
        push      OFFSET FLAT: __STRLITPACK_61.0.7              ;162.10
        lea       ecx, DWORD PTR [80+esp]                       ;162.10
        push      ecx                                           ;162.10
        call      _for_write_seq_lis_xmit                       ;162.10
                                ; LOE ebx esi edi
.B5.8:                          ; Preds .B5.7
        mov       DWORD PTR [132+esp], 0                        ;162.10
        lea       ecx, DWORD PTR [132+esp]                      ;162.10
        push      ecx                                           ;162.10
        push      OFFSET FLAT: __STRLITPACK_62.0.7              ;162.10
        lea       ecx, DWORD PTR [92+esp]                       ;162.10
        push      ecx                                           ;162.10
        call      _for_write_seq_lis_xmit                       ;162.10
                                ; LOE ebx esi edi
.B5.21:                         ; Preds .B5.8
        add       esp, 48                                       ;162.10
                                ; LOE ebx esi edi
.B5.9:                          ; Preds .B5.21 .B5.5
        xor       ecx, ecx                                      ;162.10
        mov       DWORD PTR [36+esi+ebx], ecx                   ;162.10
        mov       DWORD PTR [40+esi+ebx], ecx                   ;162.10
                                ; LOE ebx esi edi
.B5.10:                         ; Preds .B5.3 .B5.9
        inc       edi                                           ;163.3
        add       esi, 44                                       ;163.3
        cmp       edi, DWORD PTR [8+esp]                        ;163.3
        jle       .B5.3         ; Prob 82%                      ;163.3
                                ; LOE ebx esi edi
.B5.11:                         ; Preds .B5.10
        mov       edi, DWORD PTR [4+esp]                        ;
        mov       ebx, DWORD PTR [esp]                          ;
                                ; LOE ebx edi
.B5.12:                         ; Preds .B5.11 .B5.1
        mov       esi, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST+12] ;166.3
        test      esi, 1                                        ;166.7
        je        .B5.17        ; Prob 60%                      ;166.7
                                ; LOE ebx esi edi
.B5.13:                         ; Preds .B5.12
        mov       edx, esi                                      ;167.5
        mov       eax, esi                                      ;167.5
        shr       edx, 1                                        ;167.5
        and       eax, 1                                        ;167.5
        and       edx, 1                                        ;167.5
        add       eax, eax                                      ;167.5
        shl       edx, 2                                        ;167.5
        or        edx, 1                                        ;167.5
        or        edx, eax                                      ;167.5
        or        edx, 262144                                   ;167.5
        push      edx                                           ;167.5
        push      DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST] ;167.5
        call      _for_dealloc_allocatable                      ;167.5
                                ; LOE eax ebx esi edi
.B5.22:                         ; Preds .B5.13
        add       esp, 8                                        ;167.5
                                ; LOE eax ebx esi edi
.B5.14:                         ; Preds .B5.22
        and       esi, -2                                       ;167.5
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST], 0 ;167.5
        test      eax, eax                                      ;168.17
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST+12], esi ;167.5
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_VECTORERROR], eax ;167.5
        je        .B5.17        ; Prob 50%                      ;168.17
                                ; LOE ebx edi
.B5.15:                         ; Preds .B5.14
        mov       DWORD PTR [esp], 0                            ;168.23
        lea       esi, DWORD PTR [esp]                          ;168.23
        mov       DWORD PTR [32+esp], 14                        ;168.23
        lea       eax, DWORD PTR [32+esp]                       ;168.23
        mov       DWORD PTR [36+esp], OFFSET FLAT: __STRLITPACK_34 ;168.23
        push      32                                            ;168.23
        push      eax                                           ;168.23
        push      OFFSET FLAT: __STRLITPACK_56.0.5              ;168.23
        push      -2088435968                                   ;168.23
        push      911                                           ;168.23
        push      esi                                           ;168.23
        call      _for_write_seq_lis                            ;168.23
                                ; LOE ebx esi edi
.B5.16:                         ; Preds .B5.15
        mov       DWORD PTR [64+esp], 0                         ;168.23
        lea       eax, DWORD PTR [64+esp]                       ;168.23
        push      eax                                           ;168.23
        push      OFFSET FLAT: __STRLITPACK_57.0.5              ;168.23
        push      esi                                           ;168.23
        call      _for_write_seq_lis_xmit                       ;168.23
                                ; LOE ebx edi
.B5.23:                         ; Preds .B5.16
        add       esp, 36                                       ;168.23
                                ; LOE ebx edi
.B5.17:                         ; Preds .B5.23 .B5.14 .B5.12
        add       esp, 108                                      ;172.1
        pop       esi                                           ;172.1
        mov       esp, ebp                                      ;172.1
        pop       ebp                                           ;172.1
        ret                                                     ;172.1
        ALIGN     16
                                ; LOE
; mark_end;
_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKATT_2DREMOVE ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_LINK_VEH_LIST_MODULE_mp_LINKATT_2DREMOVE
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_LINK_VEH_LIST_MODULE_mp_LINKTEMP_2DREMOVE
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_LINK_VEH_LIST_MODULE_mp_LINKTEMP_2DREMOVE
_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKTEMP_2DREMOVE	PROC NEAR 
.B6.1:                          ; Preds .B6.0
        push      ebp                                           ;174.12
        mov       ebp, esp                                      ;174.12
        and       esp, -16                                      ;174.12
        push      esi                                           ;174.12
        sub       esp, 108                                      ;174.12
        mov       edx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKATT_ARRAYSIZE] ;178.3
        test      edx, edx                                      ;178.3
        jle       .B6.12        ; Prob 2%                       ;178.3
                                ; LOE edx ebx edi
.B6.2:                          ; Preds .B6.1
        imul      esi, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP+32], -40 ;
        mov       eax, 1                                        ;
        add       esi, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP] ;
        mov       DWORD PTR [8+esp], edx                        ;
        mov       DWORD PTR [4+esp], edi                        ;
        mov       DWORD PTR [esp], ebx                          ;
        mov       ebx, eax                                      ;
                                ; LOE ebx esi
.B6.3:                          ; Preds .B6.10 .B6.2
        lea       edi, DWORD PTR [ebx+ebx*4]                    ;179.10
        cmp       DWORD PTR [36+esi+edi*8], 0                   ;179.10
        jle       .B6.10        ; Prob 79%                      ;179.10
                                ; LOE ebx esi edi
.B6.4:                          ; Preds .B6.3
        mov       edx, DWORD PTR [12+esi+edi*8]                 ;179.10
        mov       ecx, edx                                      ;179.10
        shr       ecx, 1                                        ;179.10
        and       edx, 1                                        ;179.10
        and       ecx, 1                                        ;179.10
        add       edx, edx                                      ;179.10
        shl       ecx, 2                                        ;179.10
        or        ecx, 1                                        ;179.10
        or        ecx, edx                                      ;179.10
        or        ecx, 262144                                   ;179.10
        push      ecx                                           ;179.10
        push      DWORD PTR [esi+edi*8]                         ;179.10
        call      _for_dealloc_allocatable                      ;179.10
                                ; LOE eax ebx esi edi
.B6.20:                         ; Preds .B6.4
        add       esp, 8                                        ;179.10
                                ; LOE eax ebx esi edi
.B6.5:                          ; Preds .B6.20
        and       DWORD PTR [12+esi+edi*8], -2                  ;179.10
        mov       DWORD PTR [esi+edi*8], 0                      ;179.10
        test      eax, eax                                      ;179.10
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_VECTORERROR], eax ;179.10
        je        .B6.9         ; Prob 50%                      ;179.10
                                ; LOE ebx esi edi
.B6.6:                          ; Preds .B6.5
        mov       DWORD PTR [48+esp], 0                         ;179.10
        lea       ecx, DWORD PTR [48+esp]                       ;179.10
        mov       DWORD PTR [80+esp], 28                        ;179.10
        lea       edx, DWORD PTR [80+esp]                       ;179.10
        mov       DWORD PTR [84+esp], OFFSET FLAT: __STRLITPACK_28 ;179.10
        push      32                                            ;179.10
        push      edx                                           ;179.10
        push      OFFSET FLAT: __STRLITPACK_63.0.8              ;179.10
        push      -2088435968                                   ;179.10
        push      911                                           ;179.10
        push      ecx                                           ;179.10
        call      _for_write_seq_lis                            ;179.10
                                ; LOE ebx esi edi
.B6.7:                          ; Preds .B6.6
        mov       DWORD PTR [112+esp], ebx                      ;179.10
        lea       edx, DWORD PTR [112+esp]                      ;179.10
        push      edx                                           ;179.10
        push      OFFSET FLAT: __STRLITPACK_64.0.8              ;179.10
        lea       ecx, DWORD PTR [80+esp]                       ;179.10
        push      ecx                                           ;179.10
        call      _for_write_seq_lis_xmit                       ;179.10
                                ; LOE ebx esi edi
.B6.8:                          ; Preds .B6.7
        mov       DWORD PTR [132+esp], 0                        ;179.10
        lea       edx, DWORD PTR [132+esp]                      ;179.10
        push      edx                                           ;179.10
        push      OFFSET FLAT: __STRLITPACK_65.0.8              ;179.10
        lea       ecx, DWORD PTR [92+esp]                       ;179.10
        push      ecx                                           ;179.10
        call      _for_write_seq_lis_xmit                       ;179.10
                                ; LOE ebx esi edi
.B6.21:                         ; Preds .B6.8
        add       esp, 48                                       ;179.10
                                ; LOE ebx esi edi
.B6.9:                          ; Preds .B6.21 .B6.5
        mov       DWORD PTR [36+esi+edi*8], 0                   ;179.10
                                ; LOE ebx esi
.B6.10:                         ; Preds .B6.3 .B6.9
        inc       ebx                                           ;180.3
        cmp       ebx, DWORD PTR [8+esp]                        ;180.3
        jle       .B6.3         ; Prob 82%                      ;180.3
                                ; LOE ebx esi
.B6.11:                         ; Preds .B6.10
        mov       edi, DWORD PTR [4+esp]                        ;
        mov       ebx, DWORD PTR [esp]                          ;
                                ; LOE ebx edi
.B6.12:                         ; Preds .B6.11 .B6.1
        mov       esi, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP+12] ;183.3
        test      esi, 1                                        ;183.7
        je        .B6.17        ; Prob 60%                      ;183.7
                                ; LOE ebx esi edi
.B6.13:                         ; Preds .B6.12
        mov       edx, esi                                      ;184.5
        mov       eax, esi                                      ;184.5
        shr       edx, 1                                        ;184.5
        and       eax, 1                                        ;184.5
        and       edx, 1                                        ;184.5
        add       eax, eax                                      ;184.5
        shl       edx, 2                                        ;184.5
        or        edx, 1                                        ;184.5
        or        edx, eax                                      ;184.5
        or        edx, 262144                                   ;184.5
        push      edx                                           ;184.5
        push      DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP] ;184.5
        call      _for_dealloc_allocatable                      ;184.5
                                ; LOE eax ebx esi edi
.B6.22:                         ; Preds .B6.13
        add       esp, 8                                        ;184.5
                                ; LOE eax ebx esi edi
.B6.14:                         ; Preds .B6.22
        and       esi, -2                                       ;184.5
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP], 0 ;184.5
        test      eax, eax                                      ;185.17
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP+12], esi ;184.5
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_VECTORERROR], eax ;184.5
        je        .B6.17        ; Prob 50%                      ;185.17
                                ; LOE ebx edi
.B6.15:                         ; Preds .B6.14
        mov       DWORD PTR [esp], 0                            ;185.23
        lea       esi, DWORD PTR [esp]                          ;185.23
        mov       DWORD PTR [32+esp], 19                        ;185.23
        lea       eax, DWORD PTR [32+esp]                       ;185.23
        mov       DWORD PTR [36+esp], OFFSET FLAT: __STRLITPACK_32 ;185.23
        push      32                                            ;185.23
        push      eax                                           ;185.23
        push      OFFSET FLAT: __STRLITPACK_58.0.6              ;185.23
        push      -2088435968                                   ;185.23
        push      911                                           ;185.23
        push      esi                                           ;185.23
        call      _for_write_seq_lis                            ;185.23
                                ; LOE ebx esi edi
.B6.16:                         ; Preds .B6.15
        mov       DWORD PTR [64+esp], 0                         ;185.23
        lea       eax, DWORD PTR [64+esp]                       ;185.23
        push      eax                                           ;185.23
        push      OFFSET FLAT: __STRLITPACK_59.0.6              ;185.23
        push      esi                                           ;185.23
        call      _for_write_seq_lis_xmit                       ;185.23
                                ; LOE ebx edi
.B6.23:                         ; Preds .B6.16
        add       esp, 36                                       ;185.23
                                ; LOE ebx edi
.B6.17:                         ; Preds .B6.23 .B6.14 .B6.12
        add       esp, 108                                      ;188.1
        pop       esi                                           ;188.1
        mov       esp, ebp                                      ;188.1
        pop       ebp                                           ;188.1
        ret                                                     ;188.1
        ALIGN     16
                                ; LOE
; mark_end;
_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKTEMP_2DREMOVE ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_LINK_VEH_LIST_MODULE_mp_LINKTEMP_2DREMOVE
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_LINK_VEH_LIST_MODULE_mp_LINKATT_REMOVE
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_LINK_VEH_LIST_MODULE_mp_LINKATT_REMOVE
_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKATT_REMOVE	PROC NEAR 
; parameter 1: 8 + ebp
.B7.1:                          ; Preds .B7.0
        push      ebp                                           ;191.12
        mov       ebp, esp                                      ;191.12
        and       esp, -16                                      ;191.12
        push      esi                                           ;191.12
        push      edi                                           ;191.12
        push      ebx                                           ;191.12
        sub       esp, 52                                       ;191.12
        mov       eax, DWORD PTR [8+ebp]                        ;191.12
        imul      edi, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST+32], -44 ;194.29
        mov       esi, DWORD PTR [eax]                          ;194.7
        imul      ebx, esi, 44                                  ;194.7
        add       edi, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST] ;194.29
        cmp       DWORD PTR [36+ebx+edi], 0                     ;194.29
        jle       .B7.8         ; Prob 79%                      ;194.29
                                ; LOE ebx esi edi
.B7.2:                          ; Preds .B7.1
        mov       eax, DWORD PTR [12+ebx+edi]                   ;195.16
        mov       edx, eax                                      ;195.5
        shr       edx, 1                                        ;195.5
        and       edx, 1                                        ;195.5
        mov       DWORD PTR [esp], eax                          ;195.16
        and       eax, 1                                        ;195.5
        shl       edx, 2                                        ;195.5
        add       eax, eax                                      ;195.5
        or        edx, 1                                        ;195.5
        or        edx, eax                                      ;195.5
        or        edx, 262144                                   ;195.5
        push      edx                                           ;195.5
        push      DWORD PTR [edi+ebx]                           ;195.5
        call      _for_dealloc_allocatable                      ;195.5
                                ; LOE eax ebx esi edi
.B7.11:                         ; Preds .B7.2
        add       esp, 8                                        ;195.5
                                ; LOE eax ebx esi edi
.B7.3:                          ; Preds .B7.11
        mov       edx, DWORD PTR [esp]                          ;195.5
        and       edx, -2                                       ;195.5
        mov       DWORD PTR [edi+ebx], 0                        ;195.5
        test      eax, eax                                      ;196.17
        mov       DWORD PTR [12+ebx+edi], edx                   ;195.5
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_VECTORERROR], eax ;195.5
        je        .B7.7         ; Prob 50%                      ;196.17
                                ; LOE ebx esi edi
.B7.4:                          ; Preds .B7.3
        mov       DWORD PTR [esp], 0                            ;196.23
        lea       eax, DWORD PTR [esp]                          ;196.23
        mov       DWORD PTR [32+esp], 36                        ;196.23
        lea       edx, DWORD PTR [32+esp]                       ;196.23
        mov       DWORD PTR [36+esp], OFFSET FLAT: __STRLITPACK_30 ;196.23
        push      32                                            ;196.23
        push      edx                                           ;196.23
        push      OFFSET FLAT: __STRLITPACK_60.0.7              ;196.23
        push      -2088435968                                   ;196.23
        push      911                                           ;196.23
        push      eax                                           ;196.23
        call      _for_write_seq_lis                            ;196.23
                                ; LOE ebx esi edi
.B7.5:                          ; Preds .B7.4
        mov       DWORD PTR [64+esp], esi                       ;196.23
        lea       edx, DWORD PTR [64+esp]                       ;196.23
        lea       eax, DWORD PTR [24+esp]                       ;
        push      edx                                           ;196.23
        push      OFFSET FLAT: __STRLITPACK_61.0.7              ;196.23
        push      eax                                           ;196.23
        call      _for_write_seq_lis_xmit                       ;196.23
                                ; LOE ebx edi
.B7.6:                          ; Preds .B7.5
        mov       DWORD PTR [84+esp], 0                         ;196.23
        lea       edx, DWORD PTR [84+esp]                       ;196.23
        lea       eax, DWORD PTR [36+esp]                       ;
        push      edx                                           ;196.23
        push      OFFSET FLAT: __STRLITPACK_62.0.7              ;196.23
        push      eax                                           ;196.23
        call      _for_write_seq_lis_xmit                       ;196.23
                                ; LOE ebx edi
.B7.12:                         ; Preds .B7.6
        add       esp, 48                                       ;196.23
                                ; LOE ebx edi
.B7.7:                          ; Preds .B7.12 .B7.3
        xor       eax, eax                                      ;197.5
        mov       DWORD PTR [36+ebx+edi], eax                   ;197.5
        mov       DWORD PTR [40+ebx+edi], eax                   ;198.5
                                ; LOE
.B7.8:                          ; Preds .B7.1 .B7.7
        add       esp, 52                                       ;201.1
        pop       ebx                                           ;201.1
        pop       edi                                           ;201.1
        pop       esi                                           ;201.1
        mov       esp, ebp                                      ;201.1
        pop       ebp                                           ;201.1
        ret                                                     ;201.1
        ALIGN     16
                                ; LOE
; mark_end;
_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKATT_REMOVE ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_LINK_VEH_LIST_MODULE_mp_LINKATT_REMOVE
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_LINK_VEH_LIST_MODULE_mp_LINKTEMP_REMOVE
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_LINK_VEH_LIST_MODULE_mp_LINKTEMP_REMOVE
_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKTEMP_REMOVE	PROC NEAR 
; parameter 1: 8 + ebp
.B8.1:                          ; Preds .B8.0
        push      ebp                                           ;204.12
        mov       ebp, esp                                      ;204.12
        and       esp, -16                                      ;204.12
        push      esi                                           ;204.12
        push      edi                                           ;204.12
        push      ebx                                           ;204.12
        sub       esp, 52                                       ;204.12
        imul      edi, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP+32], -40 ;207.23
        mov       eax, DWORD PTR [8+ebp]                        ;204.12
        add       edi, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP] ;207.23
        mov       esi, DWORD PTR [eax]                          ;207.7
        lea       ebx, DWORD PTR [esi+esi*4]                    ;207.7
        cmp       DWORD PTR [36+edi+ebx*8], 0                   ;207.23
        jle       .B8.8         ; Prob 79%                      ;207.23
                                ; LOE ebx esi edi
.B8.2:                          ; Preds .B8.1
        mov       eax, DWORD PTR [12+edi+ebx*8]                 ;208.16
        mov       edx, eax                                      ;208.5
        shr       edx, 1                                        ;208.5
        and       edx, 1                                        ;208.5
        mov       DWORD PTR [esp], eax                          ;208.16
        and       eax, 1                                        ;208.5
        shl       edx, 2                                        ;208.5
        add       eax, eax                                      ;208.5
        or        edx, 1                                        ;208.5
        or        edx, eax                                      ;208.5
        or        edx, 262144                                   ;208.5
        push      edx                                           ;208.5
        push      DWORD PTR [edi+ebx*8]                         ;208.5
        call      _for_dealloc_allocatable                      ;208.5
                                ; LOE eax ebx esi edi
.B8.11:                         ; Preds .B8.2
        add       esp, 8                                        ;208.5
                                ; LOE eax ebx esi edi
.B8.3:                          ; Preds .B8.11
        mov       edx, DWORD PTR [esp]                          ;208.5
        and       edx, -2                                       ;208.5
        mov       DWORD PTR [edi+ebx*8], 0                      ;208.5
        test      eax, eax                                      ;209.17
        mov       DWORD PTR [12+edi+ebx*8], edx                 ;208.5
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_VECTORERROR], eax ;208.5
        je        .B8.7         ; Prob 50%                      ;209.17
                                ; LOE ebx esi edi
.B8.4:                          ; Preds .B8.3
        mov       DWORD PTR [esp], 0                            ;209.23
        lea       eax, DWORD PTR [esp]                          ;209.23
        mov       DWORD PTR [32+esp], 28                        ;209.23
        lea       edx, DWORD PTR [32+esp]                       ;209.23
        mov       DWORD PTR [36+esp], OFFSET FLAT: __STRLITPACK_28 ;209.23
        push      32                                            ;209.23
        push      edx                                           ;209.23
        push      OFFSET FLAT: __STRLITPACK_63.0.8              ;209.23
        push      -2088435968                                   ;209.23
        push      911                                           ;209.23
        push      eax                                           ;209.23
        call      _for_write_seq_lis                            ;209.23
                                ; LOE ebx esi edi
.B8.5:                          ; Preds .B8.4
        mov       DWORD PTR [64+esp], esi                       ;209.23
        lea       edx, DWORD PTR [64+esp]                       ;209.23
        lea       eax, DWORD PTR [24+esp]                       ;
        push      edx                                           ;209.23
        push      OFFSET FLAT: __STRLITPACK_64.0.8              ;209.23
        push      eax                                           ;209.23
        call      _for_write_seq_lis_xmit                       ;209.23
                                ; LOE ebx edi
.B8.6:                          ; Preds .B8.5
        mov       DWORD PTR [84+esp], 0                         ;209.23
        lea       edx, DWORD PTR [84+esp]                       ;209.23
        lea       eax, DWORD PTR [36+esp]                       ;
        push      edx                                           ;209.23
        push      OFFSET FLAT: __STRLITPACK_65.0.8              ;209.23
        push      eax                                           ;209.23
        call      _for_write_seq_lis_xmit                       ;209.23
                                ; LOE ebx edi
.B8.12:                         ; Preds .B8.6
        add       esp, 48                                       ;209.23
                                ; LOE ebx edi
.B8.7:                          ; Preds .B8.12 .B8.3
        mov       DWORD PTR [36+edi+ebx*8], 0                   ;210.5
                                ; LOE
.B8.8:                          ; Preds .B8.1 .B8.7
        add       esp, 52                                       ;214.1
        pop       ebx                                           ;214.1
        pop       edi                                           ;214.1
        pop       esi                                           ;214.1
        mov       esp, ebp                                      ;214.1
        pop       ebp                                           ;214.1
        ret                                                     ;214.1
        ALIGN     16
                                ; LOE
; mark_end;
_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKTEMP_REMOVE ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_LINK_VEH_LIST_MODULE_mp_LINKTEMP_REMOVE
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST_INSERT
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST_INSERT
_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST_INSERT	PROC NEAR 
; parameter 1: 8 + ebx
; parameter 2: 12 + ebx
; parameter 3: 16 + ebx
.B9.1:                          ; Preds .B9.0
        push      ebx                                           ;218.12
        mov       ebx, esp                                      ;218.12
        and       esp, -16                                      ;218.12
        push      ebp                                           ;218.12
        push      ebp                                           ;218.12
        mov       ebp, DWORD PTR [4+ebx]                        ;218.12
        mov       DWORD PTR [4+esp], ebp                        ;218.12
        mov       ebp, esp                                      ;218.12
        sub       esp, 152                                      ;218.12
        mov       eax, DWORD PTR [8+ebx]                        ;218.12
        mov       DWORD PTR [-44+ebp], esi                      ;218.12
        mov       esi, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST+32] ;224.6
        imul      ecx, DWORD PTR [eax], 44                      ;224.35
        imul      eax, esi, -44                                 ;224.35
        mov       edx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST] ;224.6
        add       eax, edx                                      ;224.35
        mov       DWORD PTR [-40+ebp], edi                      ;218.12
        mov       DWORD PTR [-32+ebp], edx                      ;224.6
        mov       DWORD PTR [-36+ebp], ecx                      ;224.35
        mov       edi, DWORD PTR [36+ecx+eax]                   ;224.37
        cmp       edi, DWORD PTR [40+ecx+eax]                   ;224.35
        jg        .B9.64        ; Prob 50%                      ;224.35
                                ; LOE esi edi
.B9.2:                          ; Preds .B9.1
        mov       eax, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_H_INCREASESIZE] ;225.15
        add       eax, edi                                      ;225.5
        mov       DWORD PTR [-52+ebp], eax                      ;225.5
        test      edi, edi                                      ;226.13
        mov       DWORD PTR [-56+ebp], esp                      ;226.13
        jg        .B9.4         ; Prob 21%                      ;226.13
                                ; LOE edi esp
.B9.3:                          ; Preds .B9.2
        xor       edi, edi                                      ;226.13
        jmp       .B9.39        ; Prob 100%                     ;226.13
                                ; LOE edi
.B9.4:                          ; Preds .B9.2
        mov       eax, 0                                        ;226.13
        mov       edx, edi                                      ;226.13
        push      4                                             ;226.13
        cmovle    edx, eax                                      ;226.13
        lea       ecx, DWORD PTR [-132+ebp]                     ;226.13
        push      edx                                           ;226.13
        push      2                                             ;226.13
        push      ecx                                           ;226.13
        mov       DWORD PTR [-148+ebp], edx                     ;226.13
        call      _for_check_mult_overflow                      ;226.13
                                ; LOE eax edi
.B9.94:                         ; Preds .B9.4
        add       esp, 16                                       ;226.13
                                ; LOE eax edi
.B9.5:                          ; Preds .B9.94
        mov       edx, DWORD PTR [8+ebx]                        ;226.13
        and       eax, 1                                        ;226.13
        mov       ecx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP+32] ;226.13
        neg       ecx                                           ;226.13
        add       ecx, DWORD PTR [edx]                          ;226.13
        mov       esi, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP] ;226.13
        shl       eax, 4                                        ;226.13
        or        eax, 262145                                   ;226.13
        lea       edx, DWORD PTR [ecx+ecx*4]                    ;226.13
        push      eax                                           ;226.13
        lea       ecx, DWORD PTR [esi+edx*8]                    ;226.13
        push      ecx                                           ;226.13
        push      DWORD PTR [-132+ebp]                          ;226.13
        call      _for_allocate                                 ;226.13
                                ; LOE eax edi
.B9.95:                         ; Preds .B9.5
        add       esp, 12                                       ;226.13
        mov       esi, eax                                      ;226.13
                                ; LOE esi edi
.B9.6:                          ; Preds .B9.95
        test      esi, esi                                      ;226.13
        je        .B9.9         ; Prob 50%                      ;226.13
                                ; LOE esi edi
.B9.7:                          ; Preds .B9.6
        push      32                                            ;226.13
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_VECTORERROR], esi ;226.13
        lea       eax, DWORD PTR [-128+ebp]                     ;226.13
        push      eax                                           ;226.13
        push      OFFSET FLAT: __STRLITPACK_50.0.4              ;226.13
        push      -2088435968                                   ;226.13
        push      911                                           ;226.13
        mov       DWORD PTR [-120+ebp], 0                       ;226.13
        lea       edx, DWORD PTR [-120+ebp]                     ;226.13
        push      edx                                           ;226.13
        mov       DWORD PTR [-128+ebp], 25                      ;226.13
        mov       DWORD PTR [-124+ebp], OFFSET FLAT: __STRLITPACK_40 ;226.13
        call      _for_write_seq_lis                            ;226.13
                                ; LOE edi
.B9.96:                         ; Preds .B9.7
        add       esp, 24                                       ;226.13
                                ; LOE edi
.B9.8:                          ; Preds .B9.96
        mov       DWORD PTR [-136+ebp], 0                       ;226.13
        lea       eax, DWORD PTR [-136+ebp]                     ;226.13
        push      eax                                           ;226.13
        push      OFFSET FLAT: __STRLITPACK_51.0.4              ;226.13
        lea       edx, DWORD PTR [-120+ebp]                     ;226.13
        push      edx                                           ;226.13
        call      _for_write_seq_lis_xmit                       ;226.13
                                ; LOE edi
.B9.97:                         ; Preds .B9.8
        add       esp, 12                                       ;226.13
        jmp       .B9.11        ; Prob 100%                     ;226.13
                                ; LOE edi
.B9.9:                          ; Preds .B9.6
        mov       ecx, DWORD PTR [8+ebx]                        ;226.13
        mov       edx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP] ;226.13
        mov       DWORD PTR [-152+ebp], esi                     ;
        mov       eax, DWORD PTR [ecx]                          ;226.13
        mov       ecx, 4                                        ;226.13
        sub       eax, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP+32] ;226.13
        push      ecx                                           ;226.13
        lea       esi, DWORD PTR [eax+eax*4]                    ;226.13
        mov       eax, 1                                        ;226.13
        mov       DWORD PTR [16+edx+esi*8], eax                 ;226.13
        mov       DWORD PTR [32+edx+esi*8], eax                 ;226.13
        mov       eax, DWORD PTR [-148+ebp]                     ;226.13
        push      eax                                           ;226.13
        mov       DWORD PTR [12+edx+esi*8], 5                   ;226.13
        mov       DWORD PTR [4+edx+esi*8], ecx                  ;226.13
        mov       DWORD PTR [8+edx+esi*8], 0                    ;226.13
        mov       DWORD PTR [24+edx+esi*8], eax                 ;226.13
        mov       DWORD PTR [28+edx+esi*8], ecx                 ;226.13
        lea       edx, DWORD PTR [-140+ebp]                     ;226.13
        push      2                                             ;226.13
        push      edx                                           ;226.13
        mov       esi, DWORD PTR [-152+ebp]                     ;226.13
        call      _for_check_mult_overflow                      ;226.13
                                ; LOE esi edi
.B9.98:                         ; Preds .B9.9
        add       esp, 16                                       ;226.13
                                ; LOE esi edi
.B9.10:                         ; Preds .B9.98
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_VECTORERROR], esi ;226.13
                                ; LOE edi
.B9.11:                         ; Preds .B9.97 .B9.10
        mov       ecx, DWORD PTR [8+ebx]                        ;226.13
        test      edi, edi                                      ;226.13
        mov       edx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP+32] ;226.13
        mov       esi, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP] ;226.13
        mov       ecx, DWORD PTR [ecx]                          ;226.13
        mov       DWORD PTR [-72+ebp], esi                      ;226.13
        lea       edx, DWORD PTR [edx+edx*4]                    ;226.13
        mov       DWORD PTR [-80+ebp], esp                      ;226.13
        lea       eax, DWORD PTR [ecx+ecx*4]                    ;226.13
        lea       esi, DWORD PTR [esi+eax*8]                    ;226.13
        mov       eax, 0                                        ;226.13
        cmovg     eax, edi                                      ;226.13
        shl       edx, 3                                        ;226.13
        sub       esi, edx                                      ;226.13
        shl       eax, 2                                        ;226.13
        mov       DWORD PTR [-76+ebp], edx                      ;226.13
        mov       DWORD PTR [36+esi], edi                       ;226.13
        call      __alloca_probe                                ;226.13
        and       esp, -16                                      ;226.13
        mov       eax, esp                                      ;226.13
                                ; LOE eax ecx edi
.B9.99:                         ; Preds .B9.11
        imul      edx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST+32], 44 ;
        mov       DWORD PTR [-68+ebp], eax                      ;226.13
        test      edi, edi                                      ;226.13
        mov       eax, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST] ;226.13
        mov       DWORD PTR [-88+ebp], eax                      ;226.13
        mov       DWORD PTR [-84+ebp], edx                      ;
        jle       .B9.34        ; Prob 10%                      ;226.13
                                ; LOE ecx edi
.B9.12:                         ; Preds .B9.99
        imul      eax, ecx, 44                                  ;226.13
        add       eax, DWORD PTR [-88+ebp]                      ;226.13
        sub       eax, DWORD PTR [-84+ebp]                      ;226.13
        mov       DWORD PTR [-152+ebp], eax                     ;226.13
        mov       edx, DWORD PTR [28+eax]                       ;226.13
        cmp       edx, 4                                        ;226.13
        mov       DWORD PTR [-144+ebp], edx                     ;226.13
        jne       .B9.21        ; Prob 50%                      ;226.13
                                ; LOE eax ecx edi al ah
.B9.13:                         ; Preds .B9.12
        cmp       edi, 4                                        ;226.13
        jl        .B9.87        ; Prob 10%                      ;226.13
                                ; LOE eax ecx edi al ah
.B9.14:                         ; Preds .B9.13
        mov       esi, eax                                      ;226.13
        xor       eax, eax                                      ;
        mov       DWORD PTR [-148+ebp], eax                     ;
        mov       edx, edi                                      ;226.13
        mov       DWORD PTR [-16+ebp], edi                      ;
        and       edx, -4                                       ;226.13
        mov       eax, DWORD PTR [32+esi]                       ;226.13
        shl       eax, 2                                        ;
        neg       eax                                           ;
        add       eax, DWORD PTR [esi]                          ;
        mov       esi, DWORD PTR [-148+ebp]                     ;
        mov       edi, DWORD PTR [-68+ebp]                      ;
                                ; LOE eax edx ecx esi edi
.B9.15:                         ; Preds .B9.15 .B9.14
        movdqu    xmm0, XMMWORD PTR [4+eax+esi*4]               ;226.13
        movdqa    XMMWORD PTR [edi+esi*4], xmm0                 ;226.13
        add       esi, 4                                        ;226.13
        cmp       esi, edx                                      ;226.13
        jb        .B9.15        ; Prob 82%                      ;226.13
                                ; LOE eax edx ecx esi edi
.B9.16:                         ; Preds .B9.15
        mov       edi, DWORD PTR [-16+ebp]                      ;
                                ; LOE edx ecx edi
.B9.17:                         ; Preds .B9.16 .B9.87
        cmp       edx, edi                                      ;226.13
        jae       .B9.82        ; Prob 0%                       ;226.13
                                ; LOE edx ecx edi
.B9.18:                         ; Preds .B9.17
        mov       esi, DWORD PTR [-152+ebp]                     ;226.13
        mov       DWORD PTR [-148+ebp], ecx                     ;
        mov       eax, DWORD PTR [32+esi]                       ;226.13
        shl       eax, 2                                        ;
        neg       eax                                           ;
        add       eax, DWORD PTR [esi]                          ;
        mov       esi, DWORD PTR [-68+ebp]                      ;
                                ; LOE eax edx esi edi
.B9.19:                         ; Preds .B9.19 .B9.18
        mov       ecx, DWORD PTR [4+eax+edx*4]                  ;226.13
        mov       DWORD PTR [esi+edx*4], ecx                    ;226.13
        inc       edx                                           ;226.13
        cmp       edx, edi                                      ;226.13
        jb        .B9.19        ; Prob 82%                      ;226.13
                                ; LOE eax edx esi edi
.B9.20:                         ; Preds .B9.19
        mov       eax, edi                                      ;
        shr       eax, 31                                       ;
        add       eax, edi                                      ;
        sar       eax, 1                                        ;
        mov       ecx, DWORD PTR [-148+ebp]                     ;
        mov       DWORD PTR [-64+ebp], eax                      ;
        jmp       .B9.27        ; Prob 100%                     ;
                                ; LOE ecx edi
.B9.21:                         ; Preds .B9.12
        mov       eax, edi                                      ;226.13
        shr       eax, 31                                       ;226.13
        add       eax, edi                                      ;226.13
        sar       eax, 1                                        ;226.13
        mov       DWORD PTR [-64+ebp], eax                      ;226.13
        test      eax, eax                                      ;226.13
        jbe       .B9.88        ; Prob 0%                       ;226.13
                                ; LOE ecx edi
.B9.22:                         ; Preds .B9.21
        mov       eax, DWORD PTR [-152+ebp]                     ;226.13
        xor       edx, edx                                      ;
        mov       DWORD PTR [-148+ebp], ecx                     ;
        mov       DWORD PTR [-16+ebp], edi                      ;
        mov       esi, DWORD PTR [32+eax]                       ;226.13
        imul      esi, DWORD PTR [-144+ebp]                     ;
        mov       eax, DWORD PTR [eax]                          ;226.13
        mov       ecx, DWORD PTR [-144+ebp]                     ;
        sub       eax, esi                                      ;
        mov       esi, DWORD PTR [-68+ebp]                      ;
                                ; LOE eax edx ecx esi
.B9.23:                         ; Preds .B9.23 .B9.22
        lea       edi, DWORD PTR [1+edx+edx]                    ;226.13
        imul      edi, ecx                                      ;226.13
        mov       edi, DWORD PTR [eax+edi]                      ;226.13
        mov       DWORD PTR [esi+edx*8], edi                    ;226.13
        lea       edi, DWORD PTR [2+edx+edx]                    ;226.13
        imul      edi, ecx                                      ;226.13
        mov       edi, DWORD PTR [eax+edi]                      ;226.13
        mov       DWORD PTR [4+esi+edx*8], edi                  ;226.13
        inc       edx                                           ;226.13
        cmp       edx, DWORD PTR [-64+ebp]                      ;226.13
        jb        .B9.23        ; Prob 64%                      ;226.13
                                ; LOE eax edx ecx esi
.B9.24:                         ; Preds .B9.23
        mov       ecx, DWORD PTR [-148+ebp]                     ;
        lea       edx, DWORD PTR [1+edx+edx]                    ;226.13
        mov       edi, DWORD PTR [-16+ebp]                      ;
                                ; LOE edx ecx edi
.B9.25:                         ; Preds .B9.24 .B9.88
        lea       eax, DWORD PTR [-1+edx]                       ;226.13
        cmp       edi, eax                                      ;226.13
        jbe       .B9.27        ; Prob 0%                       ;226.13
                                ; LOE edx ecx edi
.B9.26:                         ; Preds .B9.25
        mov       esi, DWORD PTR [-152+ebp]                     ;226.13
        mov       eax, DWORD PTR [32+esi]                       ;226.13
        neg       eax                                           ;226.13
        add       eax, edx                                      ;226.13
        imul      eax, DWORD PTR [-144+ebp]                     ;226.13
        mov       esi, DWORD PTR [esi]                          ;226.13
        mov       eax, DWORD PTR [esi+eax]                      ;226.13
        mov       esi, DWORD PTR [-68+ebp]                      ;226.13
        mov       DWORD PTR [-4+esi+edx*4], eax                 ;226.13
                                ; LOE ecx edi
.B9.27:                         ; Preds .B9.20 .B9.82 .B9.26 .B9.25
        cmp       DWORD PTR [-64+ebp], 0                        ;226.13
        jbe       .B9.86        ; Prob 11%                      ;226.13
                                ; LOE ecx edi
.B9.28:                         ; Preds .B9.27
        mov       edx, DWORD PTR [-72+ebp]                      ;
        xor       eax, eax                                      ;
        sub       edx, DWORD PTR [-76+ebp]                      ;
        mov       DWORD PTR [-152+ebp], edx                     ;
        mov       DWORD PTR [-16+ebp], edi                      ;
        ALIGN     16
                                ; LOE eax
.B9.29:                         ; Preds .B9.29 .B9.28
        mov       edi, DWORD PTR [8+ebx]                        ;226.13
        lea       esi, DWORD PTR [1+eax+eax]                    ;226.13
        mov       ecx, DWORD PTR [-152+ebp]                     ;226.13
        mov       edx, DWORD PTR [edi]                          ;226.13
        lea       edx, DWORD PTR [edx+edx*4]                    ;226.13
        sub       esi, DWORD PTR [32+ecx+edx*8]                 ;226.13
        imul      esi, DWORD PTR [28+ecx+edx*8]                 ;226.13
        mov       ecx, DWORD PTR [ecx+edx*8]                    ;226.13
        mov       edx, DWORD PTR [-68+ebp]                      ;226.13
        mov       edx, DWORD PTR [edx+eax*8]                    ;226.13
        mov       DWORD PTR [ecx+esi], edx                      ;226.13
        lea       edx, DWORD PTR [2+eax+eax]                    ;226.13
        mov       esi, DWORD PTR [edi]                          ;226.13
        mov       edi, DWORD PTR [-152+ebp]                     ;226.13
        lea       ecx, DWORD PTR [esi+esi*4]                    ;226.13
        mov       esi, DWORD PTR [-68+ebp]                      ;226.13
        sub       edx, DWORD PTR [32+edi+ecx*8]                 ;226.13
        imul      edx, DWORD PTR [28+edi+ecx*8]                 ;226.13
        mov       ecx, DWORD PTR [edi+ecx*8]                    ;226.13
        mov       edi, DWORD PTR [4+esi+eax*8]                  ;226.13
        inc       eax                                           ;226.13
        cmp       eax, DWORD PTR [-64+ebp]                      ;226.13
        mov       DWORD PTR [ecx+edx], edi                      ;226.13
        jb        .B9.29        ; Prob 64%                      ;226.13
                                ; LOE eax
.B9.30:                         ; Preds .B9.29
        mov       edi, DWORD PTR [-16+ebp]                      ;
        lea       esi, DWORD PTR [1+eax+eax]                    ;226.13
        mov       eax, DWORD PTR [8+ebx]                        ;226.13
        mov       ecx, DWORD PTR [eax]                          ;226.13
                                ; LOE ecx esi edi
.B9.31:                         ; Preds .B9.30 .B9.86
        lea       eax, DWORD PTR [-1+esi]                       ;226.13
        cmp       edi, eax                                      ;226.13
        jbe       .B9.34        ; Prob 11%                      ;226.13
                                ; LOE ecx esi edi
.B9.32:                         ; Preds .B9.31
        lea       eax, DWORD PTR [ecx+ecx*4]                    ;226.13
        mov       ecx, DWORD PTR [-72+ebp]                      ;226.13
        sub       ecx, DWORD PTR [-76+ebp]                      ;226.13
        mov       edx, DWORD PTR [32+ecx+eax*8]                 ;226.13
        neg       edx                                           ;226.13
        add       edx, esi                                      ;226.13
        imul      edx, DWORD PTR [28+ecx+eax*8]                 ;226.13
        mov       eax, DWORD PTR [ecx+eax*8]                    ;226.13
        mov       ecx, DWORD PTR [-68+ebp]                      ;226.13
        mov       esi, DWORD PTR [-4+ecx+esi*4]                 ;226.13
        mov       DWORD PTR [eax+edx], esi                      ;226.13
        mov       eax, DWORD PTR [8+ebx]                        ;226.13
        mov       ecx, DWORD PTR [eax]                          ;226.13
                                ; LOE ecx edi
.B9.34:                         ; Preds .B9.31 .B9.32 .B9.99
        mov       eax, DWORD PTR [-80+ebp]                      ;226.13
        mov       esp, eax                                      ;226.13
                                ; LOE ecx edi
.B9.100:                        ; Preds .B9.34
        imul      ecx, ecx, 44                                  ;226.13
        mov       eax, DWORD PTR [-88+ebp]                      ;226.13
        sub       eax, DWORD PTR [-84+ebp]                      ;226.13
        mov       DWORD PTR [-88+ebp], eax                      ;226.13
        mov       DWORD PTR [-152+ebp], ecx                     ;226.13
        mov       edx, DWORD PTR [12+ecx+eax]                   ;226.13
        test      dl, 1                                         ;226.13
        mov       DWORD PTR [-148+ebp], edx                     ;226.13
        je        .B9.39        ; Prob 60%                      ;226.13
                                ; LOE edx ecx edi dl cl dh ch
.B9.35:                         ; Preds .B9.100
        mov       eax, edx                                      ;226.13
        mov       edx, eax                                      ;226.13
        shr       edx, 1                                        ;226.13
        and       eax, 1                                        ;226.13
        and       edx, 1                                        ;226.13
        add       eax, eax                                      ;226.13
        shl       edx, 2                                        ;226.13
        or        edx, 1                                        ;226.13
        or        edx, eax                                      ;226.13
        or        edx, 262144                                   ;226.13
        mov       esi, DWORD PTR [-88+ebp]                      ;226.13
        push      edx                                           ;226.13
        push      DWORD PTR [ecx+esi]                           ;226.13
        call      _for_dealloc_allocatable                      ;226.13
                                ; LOE eax esi edi
.B9.101:                        ; Preds .B9.35
        add       esp, 8                                        ;226.13
                                ; LOE eax esi edi
.B9.36:                         ; Preds .B9.101
        mov       ecx, DWORD PTR [-152+ebp]                     ;226.13
        mov       edx, DWORD PTR [-148+ebp]                     ;226.13
        and       edx, -2                                       ;226.13
        mov       DWORD PTR [ecx+esi], 0                        ;226.13
        test      eax, eax                                      ;226.13
        mov       DWORD PTR [12+ecx+esi], edx                   ;226.13
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_VECTORERROR], eax ;226.13
        je        .B9.39        ; Prob 50%                      ;226.13
                                ; LOE edi
.B9.37:                         ; Preds .B9.36
        push      32                                            ;226.13
        mov       DWORD PTR [-120+ebp], 0                       ;226.13
        lea       eax, DWORD PTR [-152+ebp]                     ;226.13
        push      eax                                           ;226.13
        push      OFFSET FLAT: __STRLITPACK_52.0.4              ;226.13
        push      -2088435968                                   ;226.13
        push      911                                           ;226.13
        mov       DWORD PTR [-152+ebp], 39                      ;226.13
        lea       edx, DWORD PTR [-120+ebp]                     ;226.13
        push      edx                                           ;226.13
        mov       DWORD PTR [-148+ebp], OFFSET FLAT: __STRLITPACK_38 ;226.13
        call      _for_write_seq_lis                            ;226.13
                                ; LOE edi
.B9.102:                        ; Preds .B9.37
        add       esp, 24                                       ;226.13
                                ; LOE edi
.B9.38:                         ; Preds .B9.102
        mov       DWORD PTR [-144+ebp], 0                       ;226.13
        lea       eax, DWORD PTR [-144+ebp]                     ;226.13
        push      eax                                           ;226.13
        push      OFFSET FLAT: __STRLITPACK_53.0.4              ;226.13
        lea       edx, DWORD PTR [-120+ebp]                     ;226.13
        push      edx                                           ;226.13
        call      _for_write_seq_lis_xmit                       ;226.13
                                ; LOE edi
.B9.103:                        ; Preds .B9.38
        add       esp, 12                                       ;226.13
                                ; LOE edi
.B9.39:                         ; Preds .B9.103 .B9.36 .B9.100 .B9.3
        mov       edx, DWORD PTR [-52+ebp]                      ;226.13
        xor       eax, eax                                      ;226.13
        test      edx, edx                                      ;226.13
        lea       ecx, DWORD PTR [-68+ebp]                      ;226.13
        push      4                                             ;226.13
        cmovg     eax, edx                                      ;226.13
        push      eax                                           ;226.13
        push      2                                             ;226.13
        push      ecx                                           ;226.13
        mov       DWORD PTR [-84+ebp], eax                      ;226.13
        call      _for_check_mult_overflow                      ;226.13
                                ; LOE eax edi
.B9.104:                        ; Preds .B9.39
        add       esp, 16                                       ;226.13
                                ; LOE eax edi
.B9.40:                         ; Preds .B9.104
        mov       edx, DWORD PTR [8+ebx]                        ;226.13
        and       eax, 1                                        ;226.13
        mov       ecx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST+32] ;226.13
        neg       ecx                                           ;226.13
        add       ecx, DWORD PTR [edx]                          ;226.13
        imul      esi, ecx, 44                                  ;226.13
        shl       eax, 4                                        ;226.13
        or        eax, 262145                                   ;226.13
        add       esi, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST] ;226.13
        push      eax                                           ;226.13
        push      esi                                           ;226.13
        push      DWORD PTR [-68+ebp]                           ;226.13
        call      _for_allocate                                 ;226.13
                                ; LOE eax edi
.B9.105:                        ; Preds .B9.40
        add       esp, 12                                       ;226.13
        mov       esi, eax                                      ;226.13
                                ; LOE esi edi
.B9.41:                         ; Preds .B9.105
        test      esi, esi                                      ;226.13
        je        .B9.44        ; Prob 50%                      ;226.13
                                ; LOE esi edi
.B9.42:                         ; Preds .B9.41
        push      32                                            ;226.13
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_VECTORERROR], esi ;226.13
        lea       eax, DWORD PTR [-64+ebp]                      ;226.13
        push      eax                                           ;226.13
        push      OFFSET FLAT: __STRLITPACK_54.0.4              ;226.13
        push      -2088435968                                   ;226.13
        push      911                                           ;226.13
        mov       DWORD PTR [-120+ebp], 0                       ;226.13
        lea       edx, DWORD PTR [-120+ebp]                     ;226.13
        push      edx                                           ;226.13
        mov       DWORD PTR [-64+ebp], 33                       ;226.13
        mov       DWORD PTR [-60+ebp], OFFSET FLAT: __STRLITPACK_36 ;226.13
        call      _for_write_seq_lis                            ;226.13
                                ; LOE edi
.B9.106:                        ; Preds .B9.42
        add       esp, 24                                       ;226.13
                                ; LOE edi
.B9.43:                         ; Preds .B9.106
        mov       DWORD PTR [-72+ebp], 0                        ;226.13
        lea       eax, DWORD PTR [-72+ebp]                      ;226.13
        push      eax                                           ;226.13
        push      OFFSET FLAT: __STRLITPACK_55.0.4              ;226.13
        lea       edx, DWORD PTR [-120+ebp]                     ;226.13
        push      edx                                           ;226.13
        call      _for_write_seq_lis_xmit                       ;226.13
                                ; LOE edi
.B9.107:                        ; Preds .B9.43
        add       esp, 12                                       ;226.13
        jmp       .B9.46        ; Prob 100%                     ;226.13
                                ; LOE edi
.B9.44:                         ; Preds .B9.41
        mov       ecx, DWORD PTR [8+ebx]                        ;226.13
        mov       DWORD PTR [-88+ebp], esi                      ;
        mov       edx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST] ;226.13
        mov       eax, DWORD PTR [ecx]                          ;226.13
        mov       ecx, 4                                        ;226.13
        sub       eax, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST+32] ;226.13
        imul      esi, eax, 44                                  ;226.13
        mov       eax, 1                                        ;226.13
        push      ecx                                           ;226.13
        mov       DWORD PTR [16+edx+esi], eax                   ;226.13
        mov       DWORD PTR [32+edx+esi], eax                   ;226.13
        mov       eax, DWORD PTR [-84+ebp]                      ;226.13
        push      eax                                           ;226.13
        mov       DWORD PTR [12+edx+esi], 5                     ;226.13
        mov       DWORD PTR [4+edx+esi], ecx                    ;226.13
        mov       DWORD PTR [8+edx+esi], 0                      ;226.13
        mov       DWORD PTR [24+edx+esi], eax                   ;226.13
        mov       DWORD PTR [28+edx+esi], ecx                   ;226.13
        lea       edx, DWORD PTR [-76+ebp]                      ;226.13
        push      2                                             ;226.13
        push      edx                                           ;226.13
        mov       esi, DWORD PTR [-88+ebp]                      ;226.13
        call      _for_check_mult_overflow                      ;226.13
                                ; LOE esi edi
.B9.108:                        ; Preds .B9.44
        add       esp, 16                                       ;226.13
                                ; LOE esi edi
.B9.45:                         ; Preds .B9.108
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_VECTORERROR], esi ;226.13
                                ; LOE edi
.B9.46:                         ; Preds .B9.107 .B9.45
        mov       eax, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST] ;226.13
        test      edi, edi                                      ;226.13
        mov       esi, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST+32] ;226.13
        mov       DWORD PTR [-32+ebp], eax                      ;226.13
        jle       .B9.85        ; Prob 16%                      ;226.13
                                ; LOE esi edi
.B9.47:                         ; Preds .B9.46
        imul      ecx, esi, -44                                 ;
        mov       edx, edi                                      ;226.13
        shr       edx, 31                                       ;226.13
        add       edx, edi                                      ;226.13
        mov       eax, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP] ;226.13
        sar       edx, 1                                        ;226.13
        add       ecx, DWORD PTR [-32+ebp]                      ;
        mov       DWORD PTR [-84+ebp], eax                      ;226.13
        test      edx, edx                                      ;226.13
        mov       eax, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP+32] ;226.13
        mov       DWORD PTR [-12+ebp], ecx                      ;
        jbe       .B9.84        ; Prob 0%                       ;226.13
                                ; LOE eax edx esi edi
.B9.48:                         ; Preds .B9.47
        xor       ecx, ecx                                      ;
        mov       DWORD PTR [-28+ebp], ecx                      ;
        imul      ecx, eax, -40                                 ;
        add       ecx, DWORD PTR [-84+ebp]                      ;
        mov       DWORD PTR [-24+ebp], ecx                      ;
        mov       DWORD PTR [-48+ebp], edx                      ;
        mov       DWORD PTR [-88+ebp], eax                      ;
        mov       DWORD PTR [-16+ebp], edi                      ;
        mov       DWORD PTR [-80+ebp], esi                      ;
                                ; LOE
.B9.49:                         ; Preds .B9.49 .B9.48
        mov       esi, DWORD PTR [8+ebx]                        ;226.13
        mov       edx, DWORD PTR [-28+ebp]                      ;226.13
        mov       eax, DWORD PTR [esi]                          ;226.13
        mov       esi, DWORD PTR [-24+ebp]                      ;226.13
        lea       ecx, DWORD PTR [1+edx+edx]                    ;226.13
        imul      edi, eax, 44                                  ;226.13
        lea       edx, DWORD PTR [eax+eax*4]                    ;226.13
        mov       eax, DWORD PTR [32+esi+edx*8]                 ;226.13
        neg       eax                                           ;226.13
        add       eax, ecx                                      ;226.13
        imul      eax, DWORD PTR [28+esi+edx*8]                 ;226.13
        mov       edx, DWORD PTR [esi+edx*8]                    ;226.13
        mov       esi, DWORD PTR [-12+ebp]                      ;226.13
        mov       eax, DWORD PTR [edx+eax]                      ;226.13
        sub       ecx, DWORD PTR [32+edi+esi]                   ;226.13
        imul      ecx, DWORD PTR [28+edi+esi]                   ;226.13
        mov       edi, DWORD PTR [edi+esi]                      ;226.13
        mov       DWORD PTR [edi+ecx], eax                      ;226.13
        mov       ecx, DWORD PTR [8+ebx]                        ;226.13
        mov       edi, DWORD PTR [-28+ebp]                      ;226.13
        mov       edx, DWORD PTR [ecx]                          ;226.13
        imul      ecx, edx, 44                                  ;226.13
        lea       eax, DWORD PTR [2+edi+edi]                    ;226.13
        mov       edi, DWORD PTR [-24+ebp]                      ;226.13
        lea       edx, DWORD PTR [edx+edx*4]                    ;226.13
        mov       DWORD PTR [-20+ebp], ecx                      ;226.13
        mov       ecx, DWORD PTR [32+edi+edx*8]                 ;226.13
        neg       ecx                                           ;226.13
        add       ecx, eax                                      ;226.13
        imul      ecx, DWORD PTR [28+edi+edx*8]                 ;226.13
        mov       edi, DWORD PTR [edi+edx*8]                    ;226.13
        mov       edx, DWORD PTR [-20+ebp]                      ;226.13
        sub       eax, DWORD PTR [32+edx+esi]                   ;226.13
        imul      eax, DWORD PTR [28+edx+esi]                   ;226.13
        mov       edx, DWORD PTR [edx+esi]                      ;226.13
        mov       esi, DWORD PTR [edi+ecx]                      ;226.13
        mov       DWORD PTR [edx+eax], esi                      ;226.13
        mov       eax, DWORD PTR [-28+ebp]                      ;226.13
        inc       eax                                           ;226.13
        mov       DWORD PTR [-28+ebp], eax                      ;226.13
        cmp       eax, DWORD PTR [-48+ebp]                      ;226.13
        jb        .B9.49        ; Prob 64%                      ;226.13
                                ; LOE eax al ah
.B9.50:                         ; Preds .B9.49
        mov       edx, eax                                      ;226.13
        mov       eax, DWORD PTR [-88+ebp]                      ;
        mov       edi, DWORD PTR [-16+ebp]                      ;
        mov       esi, DWORD PTR [-80+ebp]                      ;
        lea       edx, DWORD PTR [1+edx+edx]                    ;226.13
                                ; LOE eax edx esi edi
.B9.51:                         ; Preds .B9.50 .B9.84
        lea       ecx, DWORD PTR [-1+edx]                       ;226.13
        cmp       edi, ecx                                      ;226.13
        jbe       .B9.53        ; Prob 0%                       ;226.13
                                ; LOE eax edx esi edi
.B9.52:                         ; Preds .B9.51
        mov       DWORD PTR [-16+ebp], edi                      ;
        neg       eax                                           ;226.13
        mov       edi, DWORD PTR [8+ebx]                        ;226.13
        mov       DWORD PTR [-80+ebp], esi                      ;
        add       eax, DWORD PTR [edi]                          ;226.13
        imul      esi, DWORD PTR [edi], 44                      ;226.13
        lea       edi, DWORD PTR [eax+eax*4]                    ;226.13
        mov       eax, DWORD PTR [-84+ebp]                      ;226.13
        mov       ecx, DWORD PTR [32+eax+edi*8]                 ;226.13
        neg       ecx                                           ;226.13
        add       ecx, edx                                      ;226.13
        imul      ecx, DWORD PTR [28+eax+edi*8]                 ;226.13
        mov       eax, DWORD PTR [eax+edi*8]                    ;226.13
        mov       edi, DWORD PTR [-12+ebp]                      ;226.13
        mov       ecx, DWORD PTR [eax+ecx]                      ;226.13
        sub       edx, DWORD PTR [32+esi+edi]                   ;226.13
        imul      edx, DWORD PTR [28+esi+edi]                   ;226.13
        mov       esi, DWORD PTR [esi+edi]                      ;226.13
        mov       edi, DWORD PTR [-16+ebp]                      ;226.13
        mov       DWORD PTR [esi+edx], ecx                      ;226.13
        mov       esi, DWORD PTR [-80+ebp]                      ;226.13
                                ; LOE esi edi
.B9.53:                         ; Preds .B9.52 .B9.51 .B9.85
        cmp       edi, DWORD PTR [-52+ebp]                      ;226.13
        lea       eax, DWORD PTR [1+edi]                        ;226.13
        jge       .B9.60        ; Prob 50%                      ;226.13
                                ; LOE eax esi edi
.B9.54:                         ; Preds .B9.53
        neg       eax                                           ;226.13
        add       eax, DWORD PTR [-52+ebp]                      ;226.13
        inc       eax                                           ;226.13
        mov       edx, eax                                      ;226.13
        shr       edx, 31                                       ;226.13
        add       edx, eax                                      ;226.13
        sar       edx, 1                                        ;226.13
        test      edx, edx                                      ;226.13
        jbe       .B9.83        ; Prob 10%                      ;226.13
                                ; LOE eax edx esi edi
.B9.55:                         ; Preds .B9.54
        mov       DWORD PTR [-84+ebp], edx                      ;
        xor       ecx, ecx                                      ;
        mov       DWORD PTR [-88+ebp], eax                      ;
        mov       DWORD PTR [-16+ebp], edi                      ;
        mov       DWORD PTR [-80+ebp], esi                      ;
        ALIGN     16
                                ; LOE ecx
.B9.56:                         ; Preds .B9.56 .B9.55
        mov       edi, DWORD PTR [-16+ebp]                      ;226.13
        mov       DWORD PTR [-48+ebp], ecx                      ;
        lea       edx, DWORD PTR [1+edi+ecx*2]                  ;226.13
        mov       ecx, DWORD PTR [8+ebx]                        ;226.13
        mov       edi, DWORD PTR [-12+ebp]                      ;226.13
        imul      eax, DWORD PTR [ecx], 44                      ;226.13
        mov       esi, DWORD PTR [32+eax+edi]                   ;226.13
        neg       esi                                           ;226.13
        add       esi, edx                                      ;226.13
        inc       edx                                           ;226.13
        imul      esi, DWORD PTR [28+eax+edi]                   ;226.13
        mov       edi, DWORD PTR [eax+edi]                      ;226.13
        xor       eax, eax                                      ;226.13
        mov       DWORD PTR [edi+esi], eax                      ;226.13
        imul      ecx, DWORD PTR [ecx], 44                      ;226.13
        mov       esi, DWORD PTR [-12+ebp]                      ;226.13
        sub       edx, DWORD PTR [32+ecx+esi]                   ;226.13
        imul      edx, DWORD PTR [28+ecx+esi]                   ;226.13
        mov       ecx, DWORD PTR [ecx+esi]                      ;226.13
        mov       DWORD PTR [ecx+edx], eax                      ;226.13
        mov       ecx, DWORD PTR [-48+ebp]                      ;226.13
        inc       ecx                                           ;226.13
        cmp       ecx, DWORD PTR [-84+ebp]                      ;226.13
        jb        .B9.56        ; Prob 63%                      ;226.13
                                ; LOE ecx
.B9.57:                         ; Preds .B9.56
        mov       eax, DWORD PTR [-88+ebp]                      ;
        lea       ecx, DWORD PTR [1+ecx+ecx]                    ;226.13
        mov       edi, DWORD PTR [-16+ebp]                      ;
        mov       esi, DWORD PTR [-80+ebp]                      ;
                                ; LOE eax ecx esi edi
.B9.58:                         ; Preds .B9.57 .B9.83
        lea       edx, DWORD PTR [-1+ecx]                       ;226.13
        cmp       eax, edx                                      ;226.13
        jbe       .B9.60        ; Prob 10%                      ;226.13
                                ; LOE ecx esi edi
.B9.59:                         ; Preds .B9.58
        mov       eax, DWORD PTR [8+ebx]                        ;226.13
        imul      edx, DWORD PTR [eax], 44                      ;226.13
        lea       eax, DWORD PTR [ecx+edi]                      ;226.13
        mov       ecx, DWORD PTR [-12+ebp]                      ;226.13
        sub       eax, DWORD PTR [32+edx+ecx]                   ;226.13
        imul      eax, DWORD PTR [28+edx+ecx]                   ;226.13
        mov       edx, DWORD PTR [edx+ecx]                      ;226.13
        mov       DWORD PTR [edx+eax], 0                        ;226.13
                                ; LOE esi edi
.B9.60:                         ; Preds .B9.53 .B9.58 .B9.59
        mov       eax, DWORD PTR [8+ebx]                        ;226.13
        mov       ecx, DWORD PTR [-52+ebp]                      ;226.13
        mov       edx, DWORD PTR [eax]                          ;226.13
        imul      eax, edx, 44                                  ;226.13
        mov       DWORD PTR [-48+ebp], edx                      ;226.13
        test      edi, edi                                      ;226.13
        mov       edx, DWORD PTR [-12+ebp]                      ;226.13
        mov       DWORD PTR [-36+ebp], eax                      ;226.13
        mov       DWORD PTR [36+eax+edx], ecx                   ;226.13
        mov       DWORD PTR [40+eax+edx], edi                   ;226.13
        jle       .B9.63        ; Prob 79%                      ;226.13
                                ; LOE esi
.B9.61:                         ; Preds .B9.60
        mov       edi, DWORD PTR [-48+ebp]                      ;226.13
        sub       edi, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP+32] ;226.13
        mov       edx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP] ;226.13
        mov       DWORD PTR [-84+ebp], edx                      ;226.13
        lea       ecx, DWORD PTR [edi+edi*4]                    ;226.13
        mov       DWORD PTR [-80+ebp], ecx                      ;226.13
        mov       edi, DWORD PTR [12+edx+ecx*8]                 ;226.13
        mov       eax, edi                                      ;226.13
        shr       eax, 1                                        ;226.13
        mov       DWORD PTR [-88+ebp], edi                      ;226.13
        and       eax, 1                                        ;226.13
        and       edi, 1                                        ;226.13
        shl       eax, 2                                        ;226.13
        add       edi, edi                                      ;226.13
        or        eax, edi                                      ;226.13
        or        eax, 262144                                   ;226.13
        push      eax                                           ;226.13
        push      DWORD PTR [edx+ecx*8]                         ;226.13
        call      _for_dealloc_allocatable                      ;226.13
                                ; LOE esi
.B9.109:                        ; Preds .B9.61
        add       esp, 8                                        ;226.13
                                ; LOE esi
.B9.62:                         ; Preds .B9.109
        mov       edx, DWORD PTR [-84+ebp]                      ;226.13
        mov       ecx, DWORD PTR [-80+ebp]                      ;226.13
        mov       eax, DWORD PTR [-88+ebp]                      ;226.13
        and       eax, -2                                       ;226.13
        mov       DWORD PTR [edx+ecx*8], 0                      ;226.13
        mov       DWORD PTR [12+edx+ecx*8], eax                 ;226.13
                                ; LOE esi
.B9.63:                         ; Preds .B9.60 .B9.62
        mov       eax, DWORD PTR [-56+ebp]                      ;226.13
        mov       esp, eax                                      ;226.13
                                ; LOE esi
.B9.64:                         ; Preds .B9.63 .B9.1
        imul      edx, esi, -44                                 ;228.6
        mov       eax, DWORD PTR [-32+ebp]                      ;228.6
        add       eax, edx                                      ;228.6
        mov       esi, DWORD PTR [-36+ebp]                      ;228.6
        mov       DWORD PTR [-32+ebp], eax                      ;228.6
        mov       edx, DWORD PTR [40+eax+esi]                   ;228.6
        mov       ecx, DWORD PTR [32+eax+esi]                   ;231.8
        mov       eax, DWORD PTR [28+eax+esi]                   ;231.8
        imul      ecx, eax                                      ;
        mov       DWORD PTR [-48+ebp], edx                      ;228.6
        test      edx, edx                                      ;230.12
        mov       DWORD PTR [-52+ebp], ecx                      ;
        jle       .B9.91        ; Prob 16%                      ;230.12
                                ; LOE eax edx esi dl dh
.B9.65:                         ; Preds .B9.64
        mov       edi, DWORD PTR [16+ebx]                       ;218.12
        mov       esi, eax                                      ;
        neg       esi                                           ;
        mov       DWORD PTR [-28+ebp], esi                      ;
        movss     xmm0, DWORD PTR [edi]                         ;233.8
        mov       esi, DWORD PTR [-36+ebp]                      ;233.8
        mov       edi, DWORD PTR [-32+ebp]                      ;233.8
        mov       ecx, edx                                      ;
        imul      ecx, eax                                      ;
        mov       edi, DWORD PTR [edi+esi]                      ;233.8
        mov       DWORD PTR [-84+ebp], ecx                      ;
        mov       DWORD PTR [-56+ebp], edi                      ;233.8
        mov       esi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;233.8
        add       ecx, edi                                      ;233.8
        sub       ecx, DWORD PTR [-52+ebp]                      ;233.8
        shl       esi, 8                                        ;233.8
        mov       DWORD PTR [-80+ebp], esi                      ;233.8
        mov       edi, DWORD PTR [ecx]                          ;233.27
        shl       edi, 8                                        ;233.27
        mov       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;233.8
        add       edi, ecx                                      ;233.8
        sub       edi, esi                                      ;233.8
        mov       esi, DWORD PTR [-56+ebp]                      ;233.24
        movss     xmm1, DWORD PTR [240+edi]                     ;233.27
        comiss    xmm1, xmm0                                    ;233.24
        jb        .B9.70        ; Prob 10%                      ;233.24
                                ; LOE eax edx ecx esi dl dh xmm0
.B9.66:                         ; Preds .B9.65
        mov       edi, esi                                      ;
        sub       edi, DWORD PTR [-52+ebp]                      ;
        mov       DWORD PTR [-56+ebp], esi                      ;
        sub       ecx, DWORD PTR [-80+ebp]                      ;
        mov       esi, edi                                      ;
        mov       edi, DWORD PTR [-84+ebp]                      ;
        mov       DWORD PTR [-88+ebp], eax                      ;
                                ; LOE edx ecx esi edi xmm0
.B9.67:                         ; Preds .B9.68 .B9.66
        dec       edx                                           ;234.10
        add       edi, DWORD PTR [-28+ebp]                      ;234.10
        test      edx, edx                                      ;235.16
        jle       .B9.69        ; Prob 20%                      ;235.16
                                ; LOE edx ecx esi edi xmm0
.B9.68:                         ; Preds .B9.67
        mov       eax, DWORD PTR [edi+esi]                      ;233.27
        shl       eax, 8                                        ;233.27
        movss     xmm1, DWORD PTR [240+eax+ecx]                 ;233.27
        comiss    xmm1, xmm0                                    ;233.24
        jae       .B9.67        ; Prob 82%                      ;233.24
                                ; LOE edx ecx esi edi xmm0
.B9.69:                         ; Preds .B9.67 .B9.68
        mov       esi, DWORD PTR [-56+ebp]                      ;
        mov       eax, DWORD PTR [-88+ebp]                      ;
                                ; LOE eax edx esi
.B9.70:                         ; Preds .B9.69 .B9.65
        mov       edi, DWORD PTR [-48+ebp]                      ;238.39
        lea       ecx, DWORD PTR [1+edx]                        ;237.8
        mov       DWORD PTR [-84+ebp], ecx                      ;237.8
        lea       edi, DWORD PTR [1+edi]                        ;238.39
        cmp       ecx, edi                                      ;238.14
        jne       .B9.72        ; Prob 50%                      ;238.14
                                ; LOE eax edx esi
.B9.71:                         ; Preds .B9.70
        imul      eax, DWORD PTR [-84+ebp]                      ;228.6
        mov       edx, DWORD PTR [12+ebx]                       ;239.10
        sub       esi, DWORD PTR [-52+ebp]                      ;239.10
        mov       ecx, DWORD PTR [edx]                          ;239.10
        mov       DWORD PTR [eax+esi], ecx                      ;239.10
        jmp       .B9.81        ; Prob 100%                     ;239.10
                                ; LOE
.B9.72:                         ; Preds .B9.70
        mov       ecx, DWORD PTR [-48+ebp]                      ;241.10
        cmp       ecx, DWORD PTR [-84+ebp]                      ;241.10
        jl        .B9.80        ; Prob 50%                      ;241.10
                                ; LOE eax edx esi
.B9.73:                         ; Preds .B9.72
        neg       edx                                           ;241.10
        add       edx, DWORD PTR [-48+ebp]                      ;241.10
        mov       esi, edx                                      ;241.10
        shr       esi, 31                                       ;241.10
        add       esi, edx                                      ;241.10
        sar       esi, 1                                        ;241.10
        test      esi, esi                                      ;241.10
        jbe       .B9.89        ; Prob 11%                      ;241.10
                                ; LOE edx esi
.B9.74:                         ; Preds .B9.73
        xor       ecx, ecx                                      ;
        mov       DWORD PTR [-80+ebp], esi                      ;
        xor       eax, eax                                      ;
        mov       DWORD PTR [-88+ebp], edx                      ;
        ALIGN     16
                                ; LOE eax ecx
.B9.75:                         ; Preds .B9.75 .B9.74
        mov       esi, DWORD PTR [8+ebx]                        ;243.13
        mov       DWORD PTR [-56+ebp], ecx                      ;
        imul      edi, DWORD PTR [esi], 44                      ;243.13
        mov       esi, DWORD PTR [-32+ebp]                      ;243.40
        mov       ecx, DWORD PTR [32+edi+esi]                   ;243.40
        mov       edx, DWORD PTR [28+edi+esi]                   ;243.40
        imul      ecx, edx                                      ;243.13
        mov       edi, DWORD PTR [edi+esi]                      ;243.40
        mov       esi, DWORD PTR [-48+ebp]                      ;243.13
        sub       edi, ecx                                      ;243.13
        mov       DWORD PTR [-36+ebp], edi                      ;243.13
        mov       edi, edx                                      ;243.13
        lea       ecx, DWORD PTR [esi+eax]                      ;243.13
        mov       DWORD PTR [-28+ebp], ecx                      ;243.13
        imul      edi, ecx                                      ;243.13
        lea       ecx, DWORD PTR [1+esi+eax]                    ;243.13
        imul      edx, ecx                                      ;243.13
        mov       ecx, DWORD PTR [-36+ebp]                      ;243.13
        mov       edi, DWORD PTR [ecx+edi]                      ;243.13
        mov       DWORD PTR [ecx+edx], edi                      ;243.13
        mov       edx, DWORD PTR [8+ebx]                        ;243.13
        mov       edi, DWORD PTR [-32+ebp]                      ;243.40
        imul      edx, DWORD PTR [edx], 44                      ;243.13
        mov       esi, DWORD PTR [32+edx+edi]                   ;243.40
        mov       ecx, DWORD PTR [28+edx+edi]                   ;243.40
        imul      esi, ecx                                      ;243.13
        mov       edx, DWORD PTR [edx+edi]                      ;243.40
        sub       edx, esi                                      ;243.13
        mov       esi, DWORD PTR [-48+ebp]                      ;243.13
        lea       edi, DWORD PTR [-1+esi+eax]                   ;243.13
        add       eax, -2                                       ;241.10
        imul      edi, ecx                                      ;243.13
        imul      ecx, DWORD PTR [-28+ebp]                      ;243.13
        mov       esi, DWORD PTR [edx+edi]                      ;243.13
        mov       DWORD PTR [edx+ecx], esi                      ;243.13
        mov       ecx, DWORD PTR [-56+ebp]                      ;241.10
        inc       ecx                                           ;241.10
        cmp       ecx, DWORD PTR [-80+ebp]                      ;241.10
        jb        .B9.75        ; Prob 64%                      ;241.10
                                ; LOE eax ecx
.B9.76:                         ; Preds .B9.75
        mov       edx, DWORD PTR [-88+ebp]                      ;
        lea       eax, DWORD PTR [1+ecx+ecx]                    ;241.10
                                ; LOE eax edx
.B9.77:                         ; Preds .B9.76 .B9.89
        lea       ecx, DWORD PTR [-1+eax]                       ;241.10
        cmp       edx, ecx                                      ;241.10
        jbe       .B9.79        ; Prob 11%                      ;241.10
                                ; LOE eax
.B9.78:                         ; Preds .B9.77
        mov       edi, DWORD PTR [8+ebx]                        ;243.13
        mov       ecx, DWORD PTR [-32+ebp]                      ;243.40
        imul      edx, DWORD PTR [edi], 44                      ;243.13
        mov       edi, DWORD PTR [-48+ebp]                      ;243.13
        sub       edi, eax                                      ;243.13
        mov       esi, DWORD PTR [32+edx+ecx]                   ;243.40
        imul      esi, DWORD PTR [28+edx+ecx]                   ;243.13
        lea       eax, DWORD PTR [1+edi]                        ;243.13
        add       edi, 2                                        ;243.13
        imul      eax, DWORD PTR [28+edx+ecx]                   ;243.13
        imul      edi, DWORD PTR [28+edx+ecx]                   ;243.13
        add       eax, DWORD PTR [edx+ecx]                      ;243.13
        add       edi, DWORD PTR [edx+ecx]                      ;243.13
        sub       eax, esi                                      ;243.13
        sub       edi, esi                                      ;243.13
        mov       eax, DWORD PTR [eax]                          ;243.13
        mov       DWORD PTR [edi], eax                          ;243.13
                                ; LOE
.B9.79:                         ; Preds .B9.77 .B9.78
        mov       eax, DWORD PTR [8+ebx]                        ;
        mov       ecx, DWORD PTR [-32+ebp]                      ;245.10
        imul      esi, DWORD PTR [eax], 44                      ;
        mov       edx, DWORD PTR [32+ecx+esi]                   ;245.10
        mov       eax, DWORD PTR [28+ecx+esi]                   ;245.10
        imul      edx, eax                                      ;
        mov       esi, DWORD PTR [ecx+esi]                      ;245.10
        mov       DWORD PTR [-52+ebp], edx                      ;
                                ; LOE eax esi
.B9.80:                         ; Preds .B9.79 .B9.72
        imul      eax, DWORD PTR [-84+ebp]                      ;228.6
        add       esi, eax                                      ;245.10
        mov       eax, DWORD PTR [12+ebx]                       ;245.10
        sub       esi, DWORD PTR [-52+ebp]                      ;245.10
        mov       edx, DWORD PTR [eax]                          ;245.10
        mov       DWORD PTR [esi], edx                          ;245.10
                                ; LOE
.B9.81:                         ; Preds .B9.91 .B9.71 .B9.80
        mov       eax, DWORD PTR [8+ebx]                        ;248.6
        mov       ecx, DWORD PTR [-32+ebp]                      ;248.6
        mov       esi, DWORD PTR [-44+ebp]                      ;251.1
        imul      edx, DWORD PTR [eax], 44                      ;248.6
        mov       edi, DWORD PTR [-40+ebp]                      ;251.1
        inc       DWORD PTR [40+edx+ecx]                        ;248.6
        mov       esp, ebp                                      ;251.1
        pop       ebp                                           ;251.1
        mov       esp, ebx                                      ;251.1
        pop       ebx                                           ;251.1
        ret                                                     ;251.1
                                ; LOE
.B9.82:                         ; Preds .B9.17                  ; Infreq
        mov       eax, edi                                      ;
        shr       eax, 31                                       ;
        add       eax, edi                                      ;
        sar       eax, 1                                        ;
        mov       DWORD PTR [-64+ebp], eax                      ;
        jmp       .B9.27        ; Prob 100%                     ;
                                ; LOE ecx edi
.B9.83:                         ; Preds .B9.54                  ; Infreq
        mov       ecx, 1                                        ;
        jmp       .B9.58        ; Prob 100%                     ;
                                ; LOE eax ecx esi edi
.B9.84:                         ; Preds .B9.47                  ; Infreq
        mov       edx, 1                                        ;
        jmp       .B9.51        ; Prob 100%                     ;
                                ; LOE eax edx esi edi
.B9.85:                         ; Preds .B9.46                  ; Infreq
        imul      eax, esi, -44                                 ;
        add       eax, DWORD PTR [-32+ebp]                      ;
        mov       DWORD PTR [-12+ebp], eax                      ;
        jmp       .B9.53        ; Prob 100%                     ;
                                ; LOE esi edi
.B9.86:                         ; Preds .B9.27                  ; Infreq
        mov       esi, 1                                        ;
        jmp       .B9.31        ; Prob 100%                     ;
                                ; LOE ecx esi edi
.B9.87:                         ; Preds .B9.13                  ; Infreq
        xor       edx, edx                                      ;226.13
        jmp       .B9.17        ; Prob 100%                     ;226.13
                                ; LOE edx ecx edi
.B9.88:                         ; Preds .B9.21                  ; Infreq
        mov       edx, 1                                        ;
        jmp       .B9.25        ; Prob 100%                     ;
                                ; LOE edx ecx edi
.B9.89:                         ; Preds .B9.73                  ; Infreq
        mov       eax, 1                                        ;
        jmp       .B9.77        ; Prob 100%                     ;
                                ; LOE eax edx
.B9.91:                         ; Preds .B9.64                  ; Infreq
        inc       edx                                           ;231.8
        imul      eax, edx                                      ;231.8
        mov       ecx, DWORD PTR [-32+ebp]                      ;231.8
        mov       edi, DWORD PTR [12+ebx]                       ;231.8
        add       eax, DWORD PTR [ecx+esi]                      ;231.8
        sub       eax, DWORD PTR [-52+ebp]                      ;231.8
        mov       edx, DWORD PTR [edi]                          ;231.8
        mov       DWORD PTR [eax], edx                          ;231.8
        jmp       .B9.81        ; Prob 100%                     ;231.8
        ALIGN     16
                                ; LOE
; mark_end;
_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST_INSERT ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST_INSERT
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST_REMOVE
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST_REMOVE
_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST_REMOVE	PROC NEAR 
; parameter 1: 8 + ebp
; parameter 2: 12 + ebp
.B10.1:                         ; Preds .B10.0
        push      ebp                                           ;254.12
        mov       ebp, esp                                      ;254.12
        and       esp, -16                                      ;254.12
        push      esi                                           ;254.12
        push      edi                                           ;254.12
        push      ebx                                           ;254.12
        sub       esp, 52                                       ;254.12
        mov       eax, DWORD PTR [8+ebp]                        ;254.12
        imul      ecx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST+32], -44 ;259.3
        mov       edx, DWORD PTR [eax]                          ;259.13
        imul      ebx, edx, 44                                  ;259.3
        add       ecx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST] ;259.3
        mov       DWORD PTR [12+esp], ecx                       ;259.3
        mov       DWORD PTR [44+esp], edx                       ;259.13
        mov       ecx, DWORD PTR [40+ebx+ecx]                   ;259.3
        test      ecx, ecx                                      ;259.3
        jle       .B10.6        ; Prob 2%                       ;259.3
                                ; LOE ecx ebx
.B10.2:                         ; Preds .B10.1
        mov       eax, DWORD PTR [12+ebp]                       ;254.12
        mov       edi, DWORD PTR [12+esp]                       ;260.9
        mov       edx, DWORD PTR [eax]                          ;260.9
        mov       eax, 1                                        ;
        mov       DWORD PTR [esp], edx                          ;260.9
        mov       edx, DWORD PTR [28+ebx+edi]                   ;260.9
        mov       esi, DWORD PTR [32+ebx+edi]                   ;260.9
        imul      esi, edx                                      ;
        mov       ebx, DWORD PTR [ebx+edi]                      ;260.9
        mov       DWORD PTR [8+esp], esi                        ;
        mov       DWORD PTR [4+esp], ebx                        ;260.9
        sub       ebx, DWORD PTR [8+esp]                        ;
        mov       esi, edx                                      ;
        mov       edi, DWORD PTR [esp]                          ;
                                ; LOE eax edx ecx ebx esi edi
.B10.3:                         ; Preds .B10.4 .B10.2
        cmp       edi, DWORD PTR [esi+ebx]                      ;260.33
        je        .B10.10       ; Prob 20%                      ;260.33
                                ; LOE eax edx ecx ebx esi edi
.B10.4:                         ; Preds .B10.3
        inc       eax                                           ;267.3
        add       esi, edx                                      ;267.3
        cmp       eax, ecx                                      ;267.3
        jle       .B10.3        ; Prob 82%                      ;267.3
                                ; LOE eax edx ecx ebx esi edi
.B10.6:                         ; Preds .B10.4 .B10.1
        mov       DWORD PTR [esp], 0                            ;272.4
        lea       ebx, DWORD PTR [esp]                          ;272.4
        mov       DWORD PTR [32+esp], 28                        ;272.4
        lea       eax, DWORD PTR [32+esp]                       ;272.4
        mov       DWORD PTR [36+esp], OFFSET FLAT: __STRLITPACK_26 ;272.4
        push      32                                            ;272.4
        push      eax                                           ;272.4
        push      OFFSET FLAT: __STRLITPACK_66.0.10             ;272.4
        push      -2088435968                                   ;272.4
        push      911                                           ;272.4
        push      ebx                                           ;272.4
        call      _for_write_seq_lis                            ;272.4
                                ; LOE ebx
.B10.7:                         ; Preds .B10.6
        mov       eax, DWORD PTR [68+esp]                       ;272.4
        lea       edx, DWORD PTR [64+esp]                       ;272.4
        mov       DWORD PTR [64+esp], eax                       ;272.4
        push      edx                                           ;272.4
        push      OFFSET FLAT: __STRLITPACK_67.0.10             ;272.4
        push      ebx                                           ;272.4
        call      _for_write_seq_lis_xmit                       ;272.4
                                ; LOE ebx
.B10.8:                         ; Preds .B10.7
        mov       DWORD PTR [84+esp], 0                         ;272.4
        lea       eax, DWORD PTR [84+esp]                       ;272.4
        push      eax                                           ;272.4
        push      OFFSET FLAT: __STRLITPACK_68.0.10             ;272.4
        push      ebx                                           ;272.4
        call      _for_write_seq_lis_xmit                       ;272.4
                                ; LOE
.B10.9:                         ; Preds .B10.8
        add       esp, 100                                      ;274.1
        pop       ebx                                           ;274.1
        pop       edi                                           ;274.1
        pop       esi                                           ;274.1
        mov       esp, ebp                                      ;274.1
        pop       ebp                                           ;274.1
        ret                                                     ;274.1
                                ; LOE
.B10.10:                        ; Preds .B10.3                  ; Infreq
        lea       ebx, DWORD PTR [-1+ecx]                       ;262.8
        cmp       ebx, eax                                      ;262.8
        jl        .B10.18       ; Prob 50%                      ;262.8
                                ; LOE eax edx ecx
.B10.11:                        ; Preds .B10.10                 ; Infreq
        mov       edx, eax                                      ;262.8
        neg       edx                                           ;262.8
        add       ecx, edx                                      ;262.8
        mov       edx, ecx                                      ;262.8
        shr       edx, 31                                       ;262.8
        add       edx, ecx                                      ;262.8
        sar       edx, 1                                        ;262.8
        test      edx, edx                                      ;262.8
        jbe       .B10.19       ; Prob 10%                      ;262.8
                                ; LOE eax edx ecx
.B10.12:                        ; Preds .B10.11                 ; Infreq
        mov       DWORD PTR [16+esp], edx                       ;
        xor       esi, esi                                      ;
        mov       DWORD PTR [esp], ecx                          ;
        mov       DWORD PTR [20+esp], eax                       ;
        ALIGN     16
                                ; LOE esi
.B10.13:                        ; Preds .B10.13 .B10.12         ; Infreq
        mov       ecx, DWORD PTR [8+ebp]                        ;263.10
        mov       eax, DWORD PTR [12+esp]                       ;263.36
        mov       DWORD PTR [24+esp], esi                       ;
        imul      edi, DWORD PTR [ecx], 44                      ;263.10
        mov       ebx, DWORD PTR [32+edi+eax]                   ;263.36
        mov       edx, DWORD PTR [28+edi+eax]                   ;263.36
        imul      ebx, edx                                      ;263.10
        mov       eax, DWORD PTR [edi+eax]                      ;263.36
        mov       edi, edx                                      ;263.36
        sub       eax, ebx                                      ;263.10
        mov       ebx, DWORD PTR [20+esp]                       ;263.36
        lea       ebx, DWORD PTR [ebx+esi*2]                    ;263.36
        imul      edx, ebx                                      ;263.10
        lea       esi, DWORD PTR [1+ebx]                        ;263.36
        imul      edi, esi                                      ;263.36
        mov       edi, DWORD PTR [eax+edi]                      ;263.10
        add       ebx, 2                                        ;263.36
        mov       DWORD PTR [eax+edx], edi                      ;263.10
        imul      ecx, DWORD PTR [ecx], 44                      ;263.10
        mov       eax, DWORD PTR [12+esp]                       ;263.36
        mov       edi, DWORD PTR [32+ecx+eax]                   ;263.36
        mov       edx, DWORD PTR [28+ecx+eax]                   ;263.36
        imul      edi, edx                                      ;263.10
        imul      ebx, edx                                      ;263.36
        imul      edx, esi                                      ;263.10
        mov       ecx, DWORD PTR [ecx+eax]                      ;263.36
        sub       ecx, edi                                      ;263.10
        mov       esi, DWORD PTR [ecx+ebx]                      ;263.10
        mov       DWORD PTR [ecx+edx], esi                      ;263.10
        mov       esi, DWORD PTR [24+esp]                       ;262.8
        inc       esi                                           ;262.8
        cmp       esi, DWORD PTR [16+esp]                       ;262.8
        jb        .B10.13       ; Prob 63%                      ;262.8
                                ; LOE esi
.B10.14:                        ; Preds .B10.13                 ; Infreq
        mov       ecx, DWORD PTR [esp]                          ;
        lea       edi, DWORD PTR [1+esi+esi]                    ;262.8
        mov       eax, DWORD PTR [20+esp]                       ;
                                ; LOE eax ecx edi
.B10.15:                        ; Preds .B10.14 .B10.19         ; Infreq
        lea       edx, DWORD PTR [-1+edi]                       ;262.8
        cmp       ecx, edx                                      ;262.8
        jbe       .B10.17       ; Prob 10%                      ;262.8
                                ; LOE eax edi
.B10.16:                        ; Preds .B10.15                 ; Infreq
        mov       edx, DWORD PTR [8+ebp]                        ;263.10
        lea       esi, DWORD PTR [eax+edi]                      ;263.10
        mov       ecx, DWORD PTR [12+esp]                       ;263.36
        lea       edi, DWORD PTR [-1+eax+edi]                   ;263.10
        imul      edx, DWORD PTR [edx], 44                      ;263.10
        mov       ebx, DWORD PTR [32+edx+ecx]                   ;263.36
        imul      esi, DWORD PTR [28+edx+ecx]                   ;263.36
        imul      ebx, DWORD PTR [28+edx+ecx]                   ;263.10
        imul      edi, DWORD PTR [28+edx+ecx]                   ;263.10
        add       esi, DWORD PTR [edx+ecx]                      ;263.10
        add       edi, DWORD PTR [edx+ecx]                      ;263.10
        sub       esi, ebx                                      ;263.10
        sub       edi, ebx                                      ;263.10
        mov       eax, DWORD PTR [esi]                          ;263.10
        mov       DWORD PTR [edi], eax                          ;263.10
                                ; LOE
.B10.17:                        ; Preds .B10.15 .B10.16         ; Infreq
        mov       edx, DWORD PTR [8+ebp]                        ;
        mov       esi, DWORD PTR [12+esp]                       ;269.4
        imul      ebx, DWORD PTR [edx], 44                      ;
        mov       edx, DWORD PTR [28+ebx+esi]                   ;269.4
        mov       eax, DWORD PTR [32+ebx+esi]                   ;269.4
        imul      eax, edx                                      ;
        mov       ecx, DWORD PTR [ebx+esi]                      ;269.4
        mov       DWORD PTR [4+esp], ecx                        ;269.4
        mov       DWORD PTR [8+esp], eax                        ;
        mov       ecx, DWORD PTR [40+ebx+esi]                   ;269.4
                                ; LOE edx ecx
.B10.18:                        ; Preds .B10.17 .B10.10         ; Infreq
        imul      edx, ecx                                      ;269.4
        mov       eax, DWORD PTR [4+esp]                        ;269.4
        add       eax, edx                                      ;269.4
        sub       eax, DWORD PTR [8+esp]                        ;269.4
        mov       edx, DWORD PTR [8+ebp]                        ;270.4
        mov       ebx, DWORD PTR [12+esp]                       ;270.4
        mov       DWORD PTR [eax], 0                            ;269.4
        imul      ecx, DWORD PTR [edx], 44                      ;270.4
        dec       DWORD PTR [40+ecx+ebx]                        ;270.4
        add       esp, 52                                       ;270.4
        pop       ebx                                           ;270.4
        pop       edi                                           ;270.4
        pop       esi                                           ;270.4
        mov       esp, ebp                                      ;270.4
        pop       ebp                                           ;270.4
        ret                                                     ;270.4
                                ; LOE
.B10.19:                        ; Preds .B10.11                 ; Infreq
        mov       edi, 1                                        ;
        jmp       .B10.15       ; Prob 100%                     ;
        ALIGN     16
                                ; LOE eax ecx edi
; mark_end;
_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST_REMOVE ENDP
_TEXT	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_66.0.10	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_67.0.10	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_68.0.10	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST_REMOVE
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_LINK_VEH_LIST_MODULE_mp_ENQATT_2DSETUP
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_LINK_VEH_LIST_MODULE_mp_ENQATT_2DSETUP
_DYNUST_LINK_VEH_LIST_MODULE_mp_ENQATT_2DSETUP	PROC NEAR 
; parameter 1: 8 + ebp
.B11.1:                         ; Preds .B11.0
        push      ebp                                           ;277.12
        mov       ebp, esp                                      ;277.12
        and       esp, -16                                      ;277.12
        push      esi                                           ;277.12
        push      edi                                           ;277.12
        push      ebx                                           ;277.12
        sub       esp, 148                                      ;277.12
        mov       esi, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_ENTQUEVEHLIST+12] ;282.3
        test      esi, 1                                        ;282.7
        je        .B11.13       ; Prob 60%                      ;282.7
                                ; LOE esi
.B11.2:                         ; Preds .B11.1
        mov       edx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_ENQATT_ARRAYSIZE] ;283.10
        test      edx, edx                                      ;283.10
        jle       .B11.10       ; Prob 2%                       ;283.10
                                ; LOE edx esi
.B11.3:                         ; Preds .B11.2
        imul      edi, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_ENTQUEVEHLIST+32], -44 ;
        mov       eax, 1                                        ;
        add       edi, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_ENTQUEVEHLIST] ;
        mov       ebx, 44                                       ;
        mov       DWORD PTR [4+esp], edx                        ;
        mov       DWORD PTR [esp], esi                          ;
        mov       esi, eax                                      ;
                                ; LOE ebx esi edi
.B11.4:                         ; Preds .B11.8 .B11.3
        cmp       DWORD PTR [36+ebx+edi], 0                     ;283.10
        jle       .B11.8        ; Prob 79%                      ;283.10
                                ; LOE ebx esi edi
.B11.5:                         ; Preds .B11.4
        mov       edx, DWORD PTR [12+ebx+edi]                   ;283.10
        mov       ecx, edx                                      ;283.10
        shr       ecx, 1                                        ;283.10
        and       edx, 1                                        ;283.10
        and       ecx, 1                                        ;283.10
        add       edx, edx                                      ;283.10
        shl       ecx, 2                                        ;283.10
        or        ecx, 1                                        ;283.10
        or        ecx, edx                                      ;283.10
        or        ecx, 262144                                   ;283.10
        push      ecx                                           ;283.10
        push      DWORD PTR [ebx+edi]                           ;283.10
        call      _for_dealloc_allocatable                      ;283.10
                                ; LOE eax ebx esi edi
.B11.33:                        ; Preds .B11.5
        add       esp, 8                                        ;283.10
                                ; LOE eax ebx esi edi
.B11.6:                         ; Preds .B11.33
        and       DWORD PTR [12+ebx+edi], -2                    ;283.10
        mov       DWORD PTR [ebx+edi], 0                        ;283.10
        test      eax, eax                                      ;283.10
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_VECTORERROR], eax ;283.10
        jne       .B11.25       ; Prob 5%                       ;283.10
                                ; LOE ebx esi edi
.B11.7:                         ; Preds .B11.6 .B11.40
        xor       edx, edx                                      ;283.10
        mov       DWORD PTR [36+ebx+edi], edx                   ;283.10
        mov       DWORD PTR [40+ebx+edi], edx                   ;283.10
                                ; LOE ebx esi edi
.B11.8:                         ; Preds .B11.4 .B11.7
        inc       esi                                           ;283.10
        add       ebx, 44                                       ;283.10
        cmp       esi, DWORD PTR [4+esp]                        ;283.10
        jle       .B11.4        ; Prob 82%                      ;283.10
                                ; LOE ebx esi edi
.B11.9:                         ; Preds .B11.8
        mov       esi, DWORD PTR [esp]                          ;
                                ; LOE esi
.B11.10:                        ; Preds .B11.9 .B11.2
        mov       edx, esi                                      ;283.10
        mov       eax, esi                                      ;283.10
        shr       edx, 1                                        ;283.10
        and       eax, 1                                        ;283.10
        and       edx, 1                                        ;283.10
        add       eax, eax                                      ;283.10
        shl       edx, 2                                        ;283.10
        or        edx, 1                                        ;283.10
        or        edx, eax                                      ;283.10
        or        edx, 262144                                   ;283.10
        push      edx                                           ;283.10
        push      DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_ENTQUEVEHLIST] ;283.10
        call      _for_dealloc_allocatable                      ;283.10
                                ; LOE eax esi
.B11.34:                        ; Preds .B11.10
        add       esp, 8                                        ;283.10
                                ; LOE eax esi
.B11.11:                        ; Preds .B11.34
        and       esi, -2                                       ;283.10
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_ENTQUEVEHLIST], 0 ;283.10
        test      eax, eax                                      ;283.10
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_ENTQUEVEHLIST+12], esi ;283.10
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_VECTORERROR], eax ;283.10
        jne       .B11.23       ; Prob 5%                       ;283.10
                                ; LOE esi
.B11.12:                        ; Preds .B11.39 .B11.11
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_ENQATT_ARRAYSIZE], 0 ;283.10
                                ; LOE esi
.B11.13:                        ; Preds .B11.12 .B11.1
        mov       ebx, DWORD PTR [8+ebp]                        ;277.12
        test      esi, 1                                        ;288.13
        jne       .B11.21       ; Prob 40%                      ;288.13
                                ; LOE ebx
.B11.14:                        ; Preds .B11.13
        xor       edi, edi                                      ;289.5
        lea       edx, DWORD PTR [124+esp]                      ;289.5
        mov       eax, DWORD PTR [ebx]                          ;289.5
        test      eax, eax                                      ;289.5
        push      44                                            ;289.5
        cmovl     eax, edi                                      ;289.5
        push      eax                                           ;289.5
        push      2                                             ;289.5
        push      edx                                           ;289.5
        call      _for_check_mult_overflow                      ;289.5
                                ; LOE eax ebx edi
.B11.15:                        ; Preds .B11.14
        mov       edx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_ENTQUEVEHLIST+12] ;289.5
        and       eax, 1                                        ;289.5
        and       edx, 1                                        ;289.5
        add       edx, edx                                      ;289.5
        shl       eax, 4                                        ;289.5
        or        edx, 1                                        ;289.5
        or        edx, eax                                      ;289.5
        or        edx, 262144                                   ;289.5
        push      edx                                           ;289.5
        push      OFFSET FLAT: _DYNUST_LINK_VEH_LIST_MODULE_mp_ENTQUEVEHLIST ;289.5
        push      DWORD PTR [148+esp]                           ;289.5
        call      _for_alloc_allocatable                        ;289.5
                                ; LOE eax ebx edi
.B11.36:                        ; Preds .B11.15
        add       esp, 28                                       ;289.5
        mov       esi, eax                                      ;289.5
                                ; LOE ebx esi edi
.B11.16:                        ; Preds .B11.36
        test      esi, esi                                      ;289.5
        je        .B11.19       ; Prob 50%                      ;289.5
                                ; LOE ebx esi edi
.B11.17:                        ; Preds .B11.16
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_VECTORERROR], esi ;289.5
        lea       esi, DWORD PTR [80+esp]                       ;290.23
        mov       DWORD PTR [80+esp], 0                         ;290.23
        lea       eax, DWORD PTR [128+esp]                      ;290.23
        mov       DWORD PTR [128+esp], 25                       ;290.23
        mov       DWORD PTR [132+esp], OFFSET FLAT: __STRLITPACK_24 ;290.23
        push      32                                            ;290.23
        push      eax                                           ;290.23
        push      OFFSET FLAT: __STRLITPACK_69.0.11             ;290.23
        push      -2088435968                                   ;290.23
        push      911                                           ;290.23
        push      esi                                           ;290.23
        call      _for_write_seq_lis                            ;290.23
                                ; LOE ebx esi
.B11.18:                        ; Preds .B11.17
        mov       DWORD PTR [144+esp], 0                        ;290.23
        lea       eax, DWORD PTR [144+esp]                      ;290.23
        push      eax                                           ;290.23
        push      OFFSET FLAT: __STRLITPACK_70.0.11             ;290.23
        push      esi                                           ;290.23
        call      _for_write_seq_lis_xmit                       ;290.23
                                ; LOE ebx
.B11.37:                        ; Preds .B11.18
        add       esp, 36                                       ;290.23
        jmp       .B11.21       ; Prob 100%                     ;290.23
                                ; LOE ebx
.B11.19:                        ; Preds .B11.16
        mov       eax, 44                                       ;289.5
        mov       edx, 1                                        ;289.5
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_ENTQUEVEHLIST+12], 133 ;289.5
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_ENTQUEVEHLIST+4], eax ;289.5
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_ENTQUEVEHLIST+16], edx ;289.5
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_ENTQUEVEHLIST+8], edi ;289.5
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_ENTQUEVEHLIST+32], edx ;289.5
        lea       edx, DWORD PTR [116+esp]                      ;289.5
        mov       ecx, DWORD PTR [ebx]                          ;289.5
        test      ecx, ecx                                      ;289.5
        push      eax                                           ;289.5
        cmovge    edi, ecx                                      ;289.5
        push      edi                                           ;289.5
        push      2                                             ;289.5
        push      edx                                           ;289.5
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_ENTQUEVEHLIST+24], edi ;289.5
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_ENTQUEVEHLIST+28], eax ;289.5
        call      _for_check_mult_overflow                      ;289.5
                                ; LOE ebx esi
.B11.38:                        ; Preds .B11.19
        add       esp, 16                                       ;289.5
                                ; LOE ebx esi
.B11.20:                        ; Preds .B11.38
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_VECTORERROR], esi ;289.5
                                ; LOE ebx
.B11.21:                        ; Preds .B11.37 .B11.13 .B11.20
        mov       edi, DWORD PTR [ebx]                          ;294.3
        test      edi, edi                                      ;294.3
        jg        .B11.28       ; Prob 2%                       ;294.3
                                ; LOE ebx edi
.B11.22:                        ; Preds .B11.30 .B11.21
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_ENQATT_ARRAYSIZE], edi ;299.3
        add       esp, 148                                      ;301.1
        pop       ebx                                           ;301.1
        pop       edi                                           ;301.1
        pop       esi                                           ;301.1
        mov       esp, ebp                                      ;301.1
        pop       ebp                                           ;301.1
        ret                                                     ;301.1
                                ; LOE
.B11.23:                        ; Preds .B11.11                 ; Infreq
        mov       DWORD PTR [esp], 0                            ;283.10
        lea       edx, DWORD PTR [esp]                          ;283.10
        mov       DWORD PTR [32+esp], 14                        ;283.10
        lea       eax, DWORD PTR [32+esp]                       ;283.10
        mov       DWORD PTR [36+esp], OFFSET FLAT: __STRLITPACK_16 ;283.10
        push      32                                            ;283.10
        push      eax                                           ;283.10
        push      OFFSET FLAT: __STRLITPACK_77.0.13             ;283.10
        push      -2088435968                                   ;283.10
        push      911                                           ;283.10
        push      edx                                           ;283.10
        call      _for_write_seq_lis                            ;283.10
                                ; LOE esi
.B11.24:                        ; Preds .B11.23                 ; Infreq
        push      32                                            ;283.10
        xor       eax, eax                                      ;283.10
        push      eax                                           ;283.10
        push      eax                                           ;283.10
        push      -2088435968                                   ;283.10
        push      eax                                           ;283.10
        push      OFFSET FLAT: __STRLITPACK_78                  ;283.10
        call      _for_stop_core                                ;283.10
                                ; LOE esi
.B11.39:                        ; Preds .B11.24                 ; Infreq
        add       esp, 48                                       ;283.10
        jmp       .B11.12       ; Prob 100%                     ;283.10
                                ; LOE esi
.B11.25:                        ; Preds .B11.6                  ; Infreq
        mov       DWORD PTR [48+esp], 0                         ;283.10
        lea       ecx, DWORD PTR [48+esp]                       ;283.10
        mov       DWORD PTR [40+esp], 36                        ;283.10
        lea       edx, DWORD PTR [40+esp]                       ;283.10
        mov       DWORD PTR [44+esp], OFFSET FLAT: __STRLITPACK_14 ;283.10
        push      32                                            ;283.10
        push      edx                                           ;283.10
        push      OFFSET FLAT: __STRLITPACK_79.0.14             ;283.10
        push      -2088435968                                   ;283.10
        push      911                                           ;283.10
        push      ecx                                           ;283.10
        call      _for_write_seq_lis                            ;283.10
                                ; LOE ebx esi edi
.B11.26:                        ; Preds .B11.25                 ; Infreq
        mov       DWORD PTR [72+esp], 0                         ;283.10
        lea       edx, DWORD PTR [136+esp]                      ;283.10
        mov       DWORD PTR [136+esp], esi                      ;283.10
        push      32                                            ;283.10
        push      edx                                           ;283.10
        push      OFFSET FLAT: __STRLITPACK_80.0.14             ;283.10
        push      -2088435968                                   ;283.10
        push      911                                           ;283.10
        lea       ecx, DWORD PTR [92+esp]                       ;283.10
        push      ecx                                           ;283.10
        call      _for_write_seq_lis                            ;283.10
                                ; LOE ebx esi edi
.B11.27:                        ; Preds .B11.26                 ; Infreq
        push      32                                            ;283.10
        xor       edx, edx                                      ;283.10
        push      edx                                           ;283.10
        push      edx                                           ;283.10
        push      -2088435968                                   ;283.10
        push      edx                                           ;283.10
        push      OFFSET FLAT: __STRLITPACK_81                  ;283.10
        call      _for_stop_core                                ;283.10
                                ; LOE ebx esi edi
.B11.40:                        ; Preds .B11.27                 ; Infreq
        add       esp, 72                                       ;283.10
        jmp       .B11.7        ; Prob 100%                     ;283.10
                                ; LOE ebx esi edi
.B11.28:                        ; Preds .B11.21                 ; Infreq
        imul      edx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_ENTQUEVEHLIST+32], -44 ;
        xor       esi, esi                                      ;
        add       edx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_ENTQUEVEHLIST] ;
        xor       ecx, ecx                                      ;
        xor       eax, eax                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B11.29:                        ; Preds .B11.29 .B11.28         ; Infreq
        inc       esi                                           ;294.3
        mov       DWORD PTR [80+eax+edx], ecx                   ;295.4
        mov       DWORD PTR [84+eax+edx], ecx                   ;295.4
        add       eax, 44                                       ;294.3
        cmp       esi, edi                                      ;294.3
        jb        .B11.29       ; Prob 99%                      ;294.3
                                ; LOE eax edx ecx ebx esi edi
.B11.30:                        ; Preds .B11.29                 ; Infreq
        mov       edi, DWORD PTR [ebx]                          ;299.3
        jmp       .B11.22       ; Prob 100%                     ;299.3
        ALIGN     16
                                ; LOE edi
; mark_end;
_DYNUST_LINK_VEH_LIST_MODULE_mp_ENQATT_2DSETUP ENDP
_TEXT	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_69.0.11	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_70.0.11	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_LINK_VEH_LIST_MODULE_mp_ENQATT_2DSETUP
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_LINK_VEH_LIST_MODULE_mp_ENQATT_SETUP
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_LINK_VEH_LIST_MODULE_mp_ENQATT_SETUP
_DYNUST_LINK_VEH_LIST_MODULE_mp_ENQATT_SETUP	PROC NEAR 
; parameter 1: 8 + ebp
; parameter 2: 12 + ebp
.B12.1:                         ; Preds .B12.0
        push      ebp                                           ;305.12
        mov       ebp, esp                                      ;305.12
        and       esp, -16                                      ;305.12
        push      esi                                           ;305.12
        push      edi                                           ;305.12
        push      ebx                                           ;305.12
        sub       esp, 100                                      ;305.12
        mov       eax, DWORD PTR [8+ebp]                        ;305.12
        mov       ecx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_ENTQUEVEHLIST] ;313.4
        mov       edx, DWORD PTR [eax]                          ;313.8
        sub       edx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_ENTQUEVEHLIST+32] ;313.32
        imul      eax, edx, 44                                  ;313.32
        mov       esi, DWORD PTR [36+ecx+eax]                   ;313.8
        test      esi, esi                                      ;313.32
        jg        .B12.3        ; Prob 21%                      ;313.32
                                ; LOE esi
.B12.2:                         ; Preds .B12.1
        xor       esi, esi                                      ;310.2
        jmp       .B12.22       ; Prob 100%                     ;310.2
                                ; LOE esi
.B12.3:                         ; Preds .B12.1
        mov       eax, 0                                        ;315.6
        mov       edx, esi                                      ;315.6
        cmovl     edx, eax                                      ;315.6
        lea       ecx, DWORD PTR [12+esp]                       ;315.6
        push      4                                             ;315.6
        push      edx                                           ;315.6
        push      2                                             ;315.6
        push      ecx                                           ;315.6
        call      _for_check_mult_overflow                      ;315.6
                                ; LOE eax esi
.B12.4:                         ; Preds .B12.3
        mov       edx, DWORD PTR [8+ebp]                        ;315.6
        and       eax, 1                                        ;315.6
        mov       ecx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP+32] ;315.6
        neg       ecx                                           ;315.6
        add       ecx, DWORD PTR [edx]                          ;315.6
        mov       ebx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP] ;315.6
        shl       eax, 4                                        ;315.6
        or        eax, 262145                                   ;315.6
        lea       edi, DWORD PTR [ecx+ecx*4]                    ;315.6
        push      eax                                           ;315.6
        lea       edx, DWORD PTR [ebx+edi*8]                    ;315.6
        push      edx                                           ;315.6
        push      DWORD PTR [36+esp]                            ;315.6
        call      _for_allocate                                 ;315.6
                                ; LOE eax esi
.B12.57:                        ; Preds .B12.4
        add       esp, 28                                       ;315.6
        mov       ebx, eax                                      ;315.6
                                ; LOE ebx esi
.B12.5:                         ; Preds .B12.57
        test      ebx, ebx                                      ;315.6
        je        .B12.8        ; Prob 50%                      ;315.6
                                ; LOE ebx esi
.B12.6:                         ; Preds .B12.5
        mov       DWORD PTR [32+esp], 0                         ;317.5
        lea       edx, DWORD PTR [32+esp]                       ;317.5
        mov       DWORD PTR [16+esp], 25                        ;317.5
        lea       eax, DWORD PTR [16+esp]                       ;317.5
        mov       DWORD PTR [20+esp], OFFSET FLAT: __STRLITPACK_22 ;317.5
        push      32                                            ;317.5
        push      eax                                           ;317.5
        push      OFFSET FLAT: __STRLITPACK_71.0.12             ;317.5
        push      -2088435968                                   ;317.5
        push      911                                           ;317.5
        push      edx                                           ;317.5
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_VECTORERROR], ebx ;315.6
        call      _for_write_seq_lis                            ;317.5
                                ; LOE esi
.B12.7:                         ; Preds .B12.6
        push      32                                            ;318.5
        xor       eax, eax                                      ;318.5
        push      eax                                           ;318.5
        push      eax                                           ;318.5
        push      -2088435968                                   ;318.5
        push      eax                                           ;318.5
        push      OFFSET FLAT: __STRLITPACK_72                  ;318.5
        call      _for_stop_core                                ;318.5
                                ; LOE esi
.B12.58:                        ; Preds .B12.7
        add       esp, 48                                       ;318.5
        jmp       .B12.10       ; Prob 100%                     ;318.5
                                ; LOE esi
.B12.8:                         ; Preds .B12.5
        mov       edi, DWORD PTR [8+ebp]                        ;315.15
        xor       eax, eax                                      ;315.6
        mov       DWORD PTR [esp], ebx                          ;
        mov       ebx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP+32] ;315.6
        neg       ebx                                           ;315.6
        mov       DWORD PTR [96+esp], esi                       ;
        mov       esi, DWORD PTR [edi]                          ;315.15
        add       ebx, esi                                      ;315.6
        mov       ecx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP] ;315.6
        mov       edi, 1                                        ;315.6
        lea       edx, DWORD PTR [ebx+ebx*4]                    ;315.6
        mov       ebx, 4                                        ;315.6
        mov       DWORD PTR [16+ecx+edx*8], edi                 ;315.6
        mov       DWORD PTR [32+ecx+edx*8], edi                 ;315.6
        mov       edi, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_ENTQUEVEHLIST+32] ;315.6
        neg       edi                                           ;315.6
        add       edi, esi                                      ;315.6
        imul      edi, edi, 44                                  ;315.6
        mov       esi, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_ENTQUEVEHLIST] ;315.6
        mov       DWORD PTR [8+ecx+edx*8], eax                  ;315.6
        mov       DWORD PTR [4+ecx+edx*8], ebx                  ;315.6
        mov       esi, DWORD PTR [36+esi+edi]                   ;315.6
        test      esi, esi                                      ;315.6
        mov       DWORD PTR [28+ecx+edx*8], ebx                 ;315.6
        cmovl     esi, eax                                      ;315.6
        lea       eax, DWORD PTR [8+esp]                        ;315.6
        push      ebx                                           ;315.6
        push      esi                                           ;315.6
        push      2                                             ;315.6
        push      eax                                           ;315.6
        mov       DWORD PTR [24+ecx+edx*8], esi                 ;315.6
        mov       DWORD PTR [12+ecx+edx*8], 5                   ;315.6
        mov       ebx, DWORD PTR [16+esp]                       ;315.6
        mov       esi, DWORD PTR [112+esp]                      ;315.6
        call      _for_check_mult_overflow                      ;315.6
                                ; LOE ebx esi bl bh
.B12.59:                        ; Preds .B12.8
        add       esp, 16                                       ;315.6
                                ; LOE ebx esi bl bh
.B12.9:                         ; Preds .B12.59
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_VECTORERROR], ebx ;315.6
                                ; LOE esi
.B12.10:                        ; Preds .B12.58 .B12.9
        mov       eax, DWORD PTR [8+ebp]                        ;322.17
        imul      ecx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_ENTQUEVEHLIST+32], 44 ;322.6
        mov       eax, DWORD PTR [eax]                          ;322.17
        imul      edx, eax, 44                                  ;322.6
        mov       ebx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_ENTQUEVEHLIST] ;322.6
        add       edx, ebx                                      ;322.6
        sub       edx, ecx                                      ;322.6
        mov       edi, DWORD PTR [36+edx]                       ;322.6
        test      edi, edi                                      ;322.6
        mov       DWORD PTR [24+esp], edi                       ;322.6
        jle       .B12.18       ; Prob 50%                      ;322.6
                                ; LOE eax ecx ebx esi
.B12.11:                        ; Preds .B12.10
        mov       edi, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP+32] ;323.23
        sub       ebx, ecx                                      ;
        mov       edx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP] ;323.23
        mov       DWORD PTR [4+esp], edi                        ;323.23
        mov       edi, DWORD PTR [24+esp]                       ;322.6
        mov       DWORD PTR [esp], edx                          ;323.23
        mov       edx, edi                                      ;322.6
        shr       edx, 31                                       ;322.6
        add       edx, edi                                      ;322.6
        sar       edx, 1                                        ;322.6
        test      edx, edx                                      ;322.6
        jbe       .B12.48       ; Prob 3%                       ;322.6
                                ; LOE eax edx ebx esi
.B12.12:                        ; Preds .B12.11
        imul      ecx, DWORD PTR [4+esp], -40                   ;
        xor       edi, edi                                      ;
        add       ecx, DWORD PTR [esp]                          ;
        mov       DWORD PTR [64+esp], ebx                       ;
        mov       DWORD PTR [68+esp], ecx                       ;
        mov       DWORD PTR [28+esp], edx                       ;
        mov       DWORD PTR [96+esp], esi                       ;
                                ; LOE eax edi
.B12.13:                        ; Preds .B12.67 .B12.12
        imul      esi, eax, 44                                  ;323.5
        lea       edx, DWORD PTR [eax+eax*4]                    ;323.5
        mov       ebx, DWORD PTR [64+esp]                       ;323.23
        lea       ecx, DWORD PTR [1+edi+edi]                    ;323.5
        mov       DWORD PTR [72+esp], edi                       ;
        mov       eax, DWORD PTR [32+esi+ebx]                   ;323.23
        neg       eax                                           ;323.5
        add       eax, ecx                                      ;323.5
        imul      eax, DWORD PTR [28+esi+ebx]                   ;323.5
        mov       ebx, DWORD PTR [esi+ebx]                      ;323.23
        mov       esi, DWORD PTR [68+esp]                       ;323.5
        mov       eax, DWORD PTR [ebx+eax]                      ;323.5
        sub       ecx, DWORD PTR [32+esi+edx*8]                 ;323.5
        imul      ecx, DWORD PTR [28+esi+edx*8]                 ;323.5
        mov       edx, DWORD PTR [esi+edx*8]                    ;323.5
        mov       DWORD PTR [edx+ecx], eax                      ;323.5
        lea       eax, DWORD PTR [2+edi+edi]                    ;323.5
        mov       ecx, DWORD PTR [8+ebp]                        ;323.23
        mov       edx, DWORD PTR [ecx]                          ;323.23
        imul      ebx, edx, 44                                  ;323.5
        lea       ecx, DWORD PTR [edx+edx*4]                    ;323.5
        mov       edx, DWORD PTR [64+esp]                       ;323.23
        mov       edi, DWORD PTR [32+ebx+edx]                   ;323.23
        neg       edi                                           ;323.5
        add       edi, eax                                      ;323.5
        imul      edi, DWORD PTR [28+ebx+edx]                   ;323.5
        sub       eax, DWORD PTR [32+esi+ecx*8]                 ;323.5
        imul      eax, DWORD PTR [28+esi+ecx*8]                 ;323.5
        mov       ebx, DWORD PTR [ebx+edx]                      ;323.23
        mov       esi, DWORD PTR [esi+ecx*8]                    ;323.5
        mov       edi, DWORD PTR [ebx+edi]                      ;323.5
        mov       DWORD PTR [esi+eax], edi                      ;323.5
        mov       edi, DWORD PTR [72+esp]                       ;322.6
        inc       edi                                           ;322.6
        cmp       edi, DWORD PTR [28+esp]                       ;322.6
        jae       .B12.14       ; Prob 36%                      ;322.6
                                ; LOE edx edi dl dh
.B12.67:                        ; Preds .B12.13
        mov       eax, DWORD PTR [8+ebp]                        ;313.8
        mov       eax, DWORD PTR [eax]                          ;313.8
        jmp       .B12.13       ; Prob 100%                     ;313.8
                                ; LOE eax edi
.B12.14:                        ; Preds .B12.13
        mov       eax, DWORD PTR [8+ebp]                        ;323.23
        lea       ecx, DWORD PTR [1+edi+edi]                    ;322.6
        mov       ebx, edx                                      ;
        mov       esi, DWORD PTR [96+esp]                       ;
        mov       eax, DWORD PTR [eax]                          ;323.23
                                ; LOE eax ecx ebx esi
.B12.15:                        ; Preds .B12.14 .B12.48
        lea       edx, DWORD PTR [-1+ecx]                       ;322.6
        cmp       edx, DWORD PTR [24+esp]                       ;322.6
        jae       .B12.19       ; Prob 3%                       ;322.6
                                ; LOE eax ecx ebx esi
.B12.16:                        ; Preds .B12.15
        mov       edi, DWORD PTR [4+esp]                        ;323.5
        neg       edi                                           ;323.5
        imul      edx, eax, 44                                  ;323.5
        add       edi, eax                                      ;323.5
        mov       DWORD PTR [64+esp], ebx                       ;
        lea       eax, DWORD PTR [edi+edi*4]                    ;323.5
        mov       edi, DWORD PTR [32+edx+ebx]                   ;323.23
        neg       edi                                           ;323.5
        add       edi, ecx                                      ;323.5
        imul      edi, DWORD PTR [28+edx+ebx]                   ;323.5
        mov       ebx, DWORD PTR [edx+ebx]                      ;323.23
        mov       edx, DWORD PTR [esp]                          ;323.5
        mov       ebx, DWORD PTR [ebx+edi]                      ;323.5
        sub       ecx, DWORD PTR [32+edx+eax*8]                 ;323.5
        imul      ecx, DWORD PTR [28+edx+eax*8]                 ;323.5
        mov       eax, DWORD PTR [edx+eax*8]                    ;323.5
        mov       DWORD PTR [eax+ecx], ebx                      ;323.5
        mov       ecx, DWORD PTR [8+ebp]                        ;327.6
        mov       ebx, DWORD PTR [64+esp]                       ;327.6
        mov       eax, DWORD PTR [ecx]                          ;327.6
        jmp       .B12.19       ; Prob 100%                     ;327.6
                                ; LOE eax ebx esi
.B12.18:                        ; Preds .B12.10
        sub       ebx, ecx                                      ;
                                ; LOE eax ebx esi
.B12.19:                        ; Preds .B12.15 .B12.16 .B12.18
        imul      eax, eax, 44                                  ;327.10
        mov       edi, DWORD PTR [12+eax+ebx]                   ;327.6
        test      edi, 1                                        ;327.10
        mov       DWORD PTR [esp], eax                          ;327.10
        je        .B12.22       ; Prob 60%                      ;327.10
                                ; LOE ebx esi edi
.B12.20:                        ; Preds .B12.19
        mov       edx, edi                                      ;328.4
        mov       eax, edi                                      ;328.4
        shr       edx, 1                                        ;328.4
        and       eax, 1                                        ;328.4
        and       edx, 1                                        ;328.4
        add       eax, eax                                      ;328.4
        shl       edx, 2                                        ;328.4
        or        edx, 1                                        ;328.4
        or        edx, eax                                      ;328.4
        or        edx, 262144                                   ;328.4
        push      edx                                           ;328.4
        mov       ecx, DWORD PTR [4+esp]                        ;328.4
        push      DWORD PTR [ecx+ebx]                           ;328.4
        call      _for_dealloc_allocatable                      ;328.4
                                ; LOE eax ebx esi edi
.B12.60:                        ; Preds .B12.20
        add       esp, 8                                        ;328.4
                                ; LOE eax ebx esi edi
.B12.21:                        ; Preds .B12.60
        mov       edx, DWORD PTR [esp]                          ;328.4
        and       edi, -2                                       ;328.4
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_VECTORERROR], eax ;328.4
        test      eax, eax                                      ;329.19
        mov       DWORD PTR [edx+ebx], 0                        ;328.4
        mov       DWORD PTR [12+edx+ebx], edi                   ;328.4
        jne       .B12.49       ; Prob 5%                       ;329.19
                                ; LOE esi
.B12.22:                        ; Preds .B12.66 .B12.21 .B12.19 .B12.2
        mov       eax, DWORD PTR [12+ebp]                       ;305.12
        xor       edx, edx                                      ;337.4
        lea       ebx, DWORD PTR [76+esp]                       ;337.4
        push      4                                             ;337.4
        mov       ecx, DWORD PTR [eax]                          ;337.4
        test      ecx, ecx                                      ;337.4
        cmovl     ecx, edx                                      ;337.4
        push      ecx                                           ;337.4
        push      2                                             ;337.4
        push      ebx                                           ;337.4
        call      _for_check_mult_overflow                      ;337.4
                                ; LOE eax esi
.B12.23:                        ; Preds .B12.22
        mov       edx, DWORD PTR [8+ebp]                        ;337.4
        and       eax, 1                                        ;337.4
        mov       ecx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_ENTQUEVEHLIST+32] ;337.4
        neg       ecx                                           ;337.4
        add       ecx, DWORD PTR [edx]                          ;337.4
        imul      ebx, ecx, 44                                  ;337.4
        shl       eax, 4                                        ;337.4
        or        eax, 262145                                   ;337.4
        add       ebx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_ENTQUEVEHLIST] ;337.4
        push      eax                                           ;337.4
        push      ebx                                           ;337.4
        push      DWORD PTR [100+esp]                           ;337.4
        call      _for_allocate                                 ;337.4
                                ; LOE eax esi
.B12.62:                        ; Preds .B12.23
        add       esp, 28                                       ;337.4
        mov       ebx, eax                                      ;337.4
                                ; LOE ebx esi
.B12.24:                        ; Preds .B12.62
        test      ebx, ebx                                      ;337.4
        jne       .B12.27       ; Prob 50%                      ;337.4
                                ; LOE ebx esi
.B12.25:                        ; Preds .B12.24
        mov       DWORD PTR [28+esp], ebx                       ;
        xor       edi, edi                                      ;337.4
        mov       ebx, DWORD PTR [8+ebp]                        ;337.13
        mov       ecx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_ENTQUEVEHLIST] ;337.4
        mov       eax, DWORD PTR [ebx]                          ;337.13
        mov       ebx, 4                                        ;337.4
        sub       eax, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_ENTQUEVEHLIST+32] ;337.4
        imul      edx, eax, 44                                  ;337.4
        mov       eax, 1                                        ;337.4
        mov       DWORD PTR [16+ecx+edx], eax                   ;337.4
        mov       DWORD PTR [32+ecx+edx], eax                   ;337.4
        mov       eax, DWORD PTR [12+ebp]                       ;337.4
        mov       DWORD PTR [12+ecx+edx], 5                     ;337.4
        mov       DWORD PTR [4+ecx+edx], ebx                    ;337.4
        mov       eax, DWORD PTR [eax]                          ;337.4
        test      eax, eax                                      ;337.4
        mov       DWORD PTR [8+ecx+edx], edi                    ;337.4
        cmovl     eax, edi                                      ;337.4
        mov       DWORD PTR [24+ecx+edx], eax                   ;337.4
        mov       DWORD PTR [28+ecx+edx], ebx                   ;337.4
        lea       edx, DWORD PTR [24+esp]                       ;337.4
        push      ebx                                           ;337.4
        push      eax                                           ;337.4
        push      2                                             ;337.4
        push      edx                                           ;337.4
        mov       ebx, DWORD PTR [44+esp]                       ;337.4
        call      _for_check_mult_overflow                      ;337.4
                                ; LOE ebx esi bl bh
.B12.63:                        ; Preds .B12.25
        add       esp, 16                                       ;337.4
                                ; LOE ebx esi bl bh
.B12.26:                        ; Preds .B12.63
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_VECTORERROR], ebx ;337.4
        jmp       .B12.29       ; Prob 100%                     ;337.4
                                ; LOE esi
.B12.27:                        ; Preds .B12.24
        mov       DWORD PTR [32+esp], 0                         ;339.7
        lea       edx, DWORD PTR [32+esp]                       ;339.7
        mov       DWORD PTR [64+esp], 35                        ;339.7
        lea       eax, DWORD PTR [64+esp]                       ;339.7
        mov       DWORD PTR [68+esp], OFFSET FLAT: __STRLITPACK_18 ;339.7
        push      32                                            ;339.7
        push      eax                                           ;339.7
        push      OFFSET FLAT: __STRLITPACK_75.0.12             ;339.7
        push      -2088435968                                   ;339.7
        push      911                                           ;339.7
        push      edx                                           ;339.7
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_VECTORERROR], ebx ;337.4
        call      _for_write_seq_lis                            ;339.7
                                ; LOE esi
.B12.28:                        ; Preds .B12.27
        push      32                                            ;340.4
        xor       eax, eax                                      ;340.4
        push      eax                                           ;340.4
        push      eax                                           ;340.4
        push      -2088435968                                   ;340.4
        push      eax                                           ;340.4
        push      OFFSET FLAT: __STRLITPACK_76                  ;340.4
        call      _for_stop_core                                ;340.4
                                ; LOE esi
.B12.64:                        ; Preds .B12.28
        add       esp, 48                                       ;340.4
                                ; LOE esi
.B12.29:                        ; Preds .B12.64 .B12.26
        mov       eax, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_ENTQUEVEHLIST] ;346.32
        test      esi, esi                                      ;344.15
        mov       edi, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_ENTQUEVEHLIST+32] ;346.32
        jle       .B12.53       ; Prob 16%                      ;344.15
                                ; LOE eax esi edi
.B12.30:                        ; Preds .B12.29
        imul      edi, edi, -44                                 ;
        mov       ebx, esi                                      ;346.32
        shr       ebx, 31                                       ;346.32
        add       eax, edi                                      ;
        add       ebx, esi                                      ;346.32
        sar       ebx, 1                                        ;346.32
        mov       ecx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP] ;346.6
        test      ebx, ebx                                      ;346.32
        mov       edx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP+32] ;346.6
        jbe       .B12.52       ; Prob 0%                       ;346.32
                                ; LOE eax edx ecx ebx esi
.B12.31:                        ; Preds .B12.30
        xor       edi, edi                                      ;
        mov       DWORD PTR [92+esp], edi                       ;
        imul      edi, edx, -40                                 ;
        mov       DWORD PTR [28+esp], edx                       ;
        add       edi, ecx                                      ;
        mov       DWORD PTR [88+esp], edi                       ;
        mov       DWORD PTR [84+esp], eax                       ;
        mov       DWORD PTR [80+esp], ebx                       ;
        mov       DWORD PTR [72+esp], ecx                       ;
        mov       DWORD PTR [96+esp], esi                       ;
        mov       edx, DWORD PTR [92+esp]                       ;
                                ; LOE edx
.B12.32:                        ; Preds .B12.32 .B12.31
        mov       eax, DWORD PTR [8+ebp]                        ;346.32
        lea       ebx, DWORD PTR [1+edx+edx]                    ;346.6
        mov       ecx, DWORD PTR [88+esp]                       ;346.32
        mov       DWORD PTR [92+esp], edx                       ;
        mov       esi, DWORD PTR [eax]                          ;346.32
        imul      edi, esi, 44                                  ;346.6
        lea       esi, DWORD PTR [esi+esi*4]                    ;346.6
        mov       edx, DWORD PTR [32+ecx+esi*8]                 ;346.32
        neg       edx                                           ;346.6
        add       edx, ebx                                      ;346.6
        imul      edx, DWORD PTR [28+ecx+esi*8]                 ;346.6
        mov       ecx, DWORD PTR [ecx+esi*8]                    ;346.32
        mov       esi, DWORD PTR [84+esp]                       ;346.6
        mov       edx, DWORD PTR [ecx+edx]                      ;346.6
        sub       ebx, DWORD PTR [32+edi+esi]                   ;346.6
        imul      ebx, DWORD PTR [28+edi+esi]                   ;346.6
        mov       edi, DWORD PTR [edi+esi]                      ;346.6
        mov       DWORD PTR [edi+ebx], edx                      ;346.6
        mov       edi, DWORD PTR [eax]                          ;346.32
        mov       edx, DWORD PTR [88+esp]                       ;346.32
        mov       eax, DWORD PTR [92+esp]                       ;346.6
        imul      ebx, edi, 44                                  ;346.6
        lea       ecx, DWORD PTR [edi+edi*4]                    ;346.6
        mov       edi, DWORD PTR [32+edx+ecx*8]                 ;346.32
        lea       eax, DWORD PTR [2+eax+eax]                    ;346.6
        neg       edi                                           ;346.6
        add       edi, eax                                      ;346.6
        imul      edi, DWORD PTR [28+edx+ecx*8]                 ;346.6
        sub       eax, DWORD PTR [32+ebx+esi]                   ;346.6
        imul      eax, DWORD PTR [28+ebx+esi]                   ;346.6
        mov       edx, DWORD PTR [edx+ecx*8]                    ;346.32
        mov       esi, DWORD PTR [ebx+esi]                      ;346.6
        mov       ebx, DWORD PTR [edx+edi]                      ;346.6
        mov       edx, DWORD PTR [92+esp]                       ;346.32
        inc       edx                                           ;346.32
        mov       DWORD PTR [esi+eax], ebx                      ;346.6
        cmp       edx, DWORD PTR [80+esp]                       ;346.32
        jb        .B12.32       ; Prob 64%                      ;346.32
                                ; LOE edx
.B12.33:                        ; Preds .B12.32
        mov       DWORD PTR [92+esp], edx                       ;
        mov       ebx, edx                                      ;346.32
        mov       eax, DWORD PTR [84+esp]                       ;
        mov       edx, DWORD PTR [28+esp]                       ;
        mov       ecx, DWORD PTR [72+esp]                       ;
        lea       ebx, DWORD PTR [1+ebx+ebx]                    ;346.32
        mov       esi, DWORD PTR [96+esp]                       ;
                                ; LOE eax edx ecx ebx esi
.B12.34:                        ; Preds .B12.33 .B12.52
        lea       edi, DWORD PTR [-1+ebx]                       ;346.32
        cmp       esi, edi                                      ;346.32
        jbe       .B12.36       ; Prob 0%                       ;346.32
                                ; LOE eax edx ecx ebx esi
.B12.35:                        ; Preds .B12.34
        mov       edi, DWORD PTR [8+ebp]                        ;346.6
        neg       edx                                           ;346.6
        mov       DWORD PTR [96+esp], esi                       ;
        add       edx, DWORD PTR [edi]                          ;346.6
        imul      esi, DWORD PTR [edi], 44                      ;346.6
        lea       edi, DWORD PTR [edx+edx*4]                    ;346.6
        mov       edx, DWORD PTR [32+ecx+edi*8]                 ;346.32
        neg       edx                                           ;346.6
        add       edx, ebx                                      ;346.6
        imul      edx, DWORD PTR [28+ecx+edi*8]                 ;346.6
        sub       ebx, DWORD PTR [32+esi+eax]                   ;346.6
        imul      ebx, DWORD PTR [28+esi+eax]                   ;346.6
        mov       ecx, DWORD PTR [ecx+edi*8]                    ;346.32
        mov       esi, DWORD PTR [esi+eax]                      ;346.6
        mov       edx, DWORD PTR [ecx+edx]                      ;346.6
        mov       DWORD PTR [esi+ebx], edx                      ;346.6
        mov       esi, DWORD PTR [96+esp]                       ;346.6
                                ; LOE eax esi
.B12.36:                        ; Preds .B12.35 .B12.34 .B12.53
        mov       edx, DWORD PTR [12+ebp]                       ;351.4
        mov       ebx, DWORD PTR [edx]                          ;351.4
        lea       edx, DWORD PTR [1+esi]                        ;351.4
        cmp       ebx, esi                                      ;351.4
        jle       .B12.44       ; Prob 50%                      ;351.4
                                ; LOE eax edx ebx esi
.B12.37:                        ; Preds .B12.36
        mov       ecx, ebx                                      ;351.4
        sub       ecx, edx                                      ;351.4
        inc       ecx                                           ;351.4
        mov       edi, ecx                                      ;351.4
        shr       edi, 31                                       ;351.4
        add       edi, ecx                                      ;351.4
        sar       edi, 1                                        ;351.4
        test      edi, edi                                      ;351.4
        jbe       .B12.51       ; Prob 10%                      ;351.4
                                ; LOE eax edx ebx esi edi
.B12.38:                        ; Preds .B12.37
        mov       DWORD PTR [72+esp], edx                       ;
        xor       ecx, ecx                                      ;
        mov       DWORD PTR [80+esp], edi                       ;
        mov       DWORD PTR [28+esp], ebx                       ;
        mov       DWORD PTR [96+esp], esi                       ;
        mov       edx, DWORD PTR [8+ebp]                        ;
                                ; LOE eax edx ecx
.B12.39:                        ; Preds .B12.39 .B12.38
        imul      edi, DWORD PTR [edx], 44                      ;352.7
        mov       ebx, DWORD PTR [96+esp]                       ;352.7
        mov       esi, DWORD PTR [32+edi+eax]                   ;352.7
        neg       esi                                           ;352.7
        mov       DWORD PTR [84+esp], ecx                       ;
        lea       ebx, DWORD PTR [1+ebx+ecx*2]                  ;352.7
        add       esi, ebx                                      ;352.7
        xor       ecx, ecx                                      ;352.7
        imul      esi, DWORD PTR [28+edi+eax]                   ;352.7
        inc       ebx                                           ;352.7
        mov       edi, DWORD PTR [edi+eax]                      ;352.7
        mov       DWORD PTR [edi+esi], ecx                      ;352.7
        imul      esi, DWORD PTR [edx], 44                      ;352.7
        sub       ebx, DWORD PTR [32+esi+eax]                   ;352.7
        imul      ebx, DWORD PTR [28+esi+eax]                   ;352.7
        mov       esi, DWORD PTR [esi+eax]                      ;352.7
        mov       DWORD PTR [esi+ebx], ecx                      ;352.7
        mov       ecx, DWORD PTR [84+esp]                       ;351.4
        inc       ecx                                           ;351.4
        cmp       ecx, DWORD PTR [80+esp]                       ;351.4
        jb        .B12.39       ; Prob 63%                      ;351.4
                                ; LOE eax edx ecx
.B12.40:                        ; Preds .B12.39
        mov       edx, DWORD PTR [72+esp]                       ;
        lea       ecx, DWORD PTR [1+ecx+ecx]                    ;351.4
        mov       ebx, DWORD PTR [28+esp]                       ;
        mov       esi, DWORD PTR [96+esp]                       ;
                                ; LOE eax edx ecx ebx esi
.B12.41:                        ; Preds .B12.40 .B12.51
        sub       ebx, edx                                      ;351.4
        lea       edx, DWORD PTR [-1+ecx]                       ;351.4
        inc       ebx                                           ;351.4
        cmp       ebx, edx                                      ;351.4
        jbe       .B12.43       ; Prob 10%                      ;351.4
                                ; LOE eax ecx esi
.B12.42:                        ; Preds .B12.41
        mov       edx, DWORD PTR [8+ebp]                        ;352.7
        lea       edi, DWORD PTR [ecx+esi]                      ;352.7
        imul      ebx, DWORD PTR [edx], 44                      ;352.7
        sub       edi, DWORD PTR [32+ebx+eax]                   ;352.7
        imul      edi, DWORD PTR [28+ebx+eax]                   ;352.7
        mov       ecx, DWORD PTR [ebx+eax]                      ;352.7
        mov       DWORD PTR [ecx+edi], 0                        ;352.7
                                ; LOE eax esi
.B12.43:                        ; Preds .B12.41 .B12.42
        mov       edx, DWORD PTR [12+ebp]                       ;355.4
        mov       ebx, DWORD PTR [edx]                          ;355.4
                                ; LOE eax ebx esi
.B12.44:                        ; Preds .B12.43 .B12.36
        mov       edx, DWORD PTR [8+ebp]                        ;355.4
        mov       edx, DWORD PTR [edx]                          ;355.4
        imul      ecx, edx, 44                                  ;355.4
        mov       DWORD PTR [36+ecx+eax], ebx                   ;355.4
        test      esi, esi                                      ;357.15
        mov       DWORD PTR [40+ecx+eax], esi                   ;356.4
        jle       .B12.47       ; Prob 79%                      ;357.15
                                ; LOE edx
.B12.45:                        ; Preds .B12.44
        mov       ebx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP+32] ;358.6
        sub       edx, ebx                                      ;358.6
        mov       esi, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP] ;358.6
        lea       edi, DWORD PTR [edx+edx*4]                    ;358.6
        mov       ebx, DWORD PTR [12+esi+edi*8]                 ;358.17
        mov       edx, ebx                                      ;358.6
        shr       edx, 1                                        ;358.6
        mov       eax, ebx                                      ;358.6
        and       edx, 1                                        ;358.6
        and       eax, 1                                        ;358.6
        shl       edx, 2                                        ;358.6
        add       eax, eax                                      ;358.6
        or        edx, eax                                      ;358.6
        or        edx, 262144                                   ;358.6
        push      edx                                           ;358.6
        push      DWORD PTR [esi+edi*8]                         ;358.6
        call      _for_dealloc_allocatable                      ;358.6
                                ; LOE ebx esi edi
.B12.65:                        ; Preds .B12.45
        add       esp, 8                                        ;358.6
                                ; LOE ebx esi edi
.B12.46:                        ; Preds .B12.65
        and       ebx, -2                                       ;358.6
        mov       DWORD PTR [esi+edi*8], 0                      ;358.6
        mov       DWORD PTR [12+esi+edi*8], ebx                 ;358.6
                                ; LOE
.B12.47:                        ; Preds .B12.44 .B12.46
        add       esp, 100                                      ;361.1
        pop       ebx                                           ;361.1
        pop       edi                                           ;361.1
        pop       esi                                           ;361.1
        mov       esp, ebp                                      ;361.1
        pop       ebp                                           ;361.1
        ret                                                     ;361.1
                                ; LOE
.B12.48:                        ; Preds .B12.11                 ; Infreq
        mov       ecx, 1                                        ;
        jmp       .B12.15       ; Prob 100%                     ;
                                ; LOE eax ecx ebx esi
.B12.49:                        ; Preds .B12.21                 ; Infreq
        mov       DWORD PTR [32+esp], 0                         ;330.6
        lea       edx, DWORD PTR [32+esp]                       ;330.6
        mov       DWORD PTR [esp], 39                           ;330.6
        lea       eax, DWORD PTR [esp]                          ;330.6
        mov       DWORD PTR [4+esp], OFFSET FLAT: __STRLITPACK_20 ;330.6
        push      32                                            ;330.6
        push      eax                                           ;330.6
        push      OFFSET FLAT: __STRLITPACK_73.0.12             ;330.6
        push      -2088435968                                   ;330.6
        push      911                                           ;330.6
        push      edx                                           ;330.6
        call      _for_write_seq_lis                            ;330.6
                                ; LOE esi
.B12.50:                        ; Preds .B12.49                 ; Infreq
        push      32                                            ;331.6
        xor       eax, eax                                      ;331.6
        push      eax                                           ;331.6
        push      eax                                           ;331.6
        push      -2088435968                                   ;331.6
        push      eax                                           ;331.6
        push      OFFSET FLAT: __STRLITPACK_74                  ;331.6
        call      _for_stop_core                                ;331.6
                                ; LOE esi
.B12.66:                        ; Preds .B12.50                 ; Infreq
        add       esp, 48                                       ;331.6
        jmp       .B12.22       ; Prob 100%                     ;331.6
                                ; LOE esi
.B12.51:                        ; Preds .B12.37                 ; Infreq
        mov       ecx, 1                                        ;
        jmp       .B12.41       ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B12.52:                        ; Preds .B12.30                 ; Infreq
        mov       ebx, 1                                        ;
        jmp       .B12.34       ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B12.53:                        ; Preds .B12.29                 ; Infreq
        imul      edx, edi, -44                                 ;
        add       eax, edx                                      ;
        jmp       .B12.36       ; Prob 100%                     ;
        ALIGN     16
                                ; LOE eax esi
; mark_end;
_DYNUST_LINK_VEH_LIST_MODULE_mp_ENQATT_SETUP ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_LINK_VEH_LIST_MODULE_mp_ENQATT_SETUP
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_LINK_VEH_LIST_MODULE_mp_ENQATT_2DREMOVE
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_LINK_VEH_LIST_MODULE_mp_ENQATT_2DREMOVE
_DYNUST_LINK_VEH_LIST_MODULE_mp_ENQATT_2DREMOVE	PROC NEAR 
.B13.1:                         ; Preds .B13.0
        push      ebp                                           ;365.12
        mov       ebp, esp                                      ;365.12
        and       esp, -16                                      ;365.12
        push      esi                                           ;365.12
        sub       esp, 92                                       ;365.12
        mov       ecx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_ENQATT_ARRAYSIZE] ;369.3
        test      ecx, ecx                                      ;369.3
        jle       .B13.9        ; Prob 2%                       ;369.3
                                ; LOE ecx ebx edi
.B13.2:                         ; Preds .B13.1
        imul      eax, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_ENTQUEVEHLIST+32], -44 ;
        mov       edx, 1                                        ;
        add       eax, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_ENTQUEVEHLIST] ;
        mov       esi, 44                                       ;
        mov       DWORD PTR [8+esp], ecx                        ;
        mov       DWORD PTR [4+esp], edi                        ;
        mov       edi, edx                                      ;
        mov       DWORD PTR [esp], ebx                          ;
        mov       ebx, eax                                      ;
                                ; LOE ebx esi edi
.B13.3:                         ; Preds .B13.7 .B13.2
        cmp       DWORD PTR [36+esi+ebx], 0                     ;370.10
        jle       .B13.7        ; Prob 79%                      ;370.10
                                ; LOE ebx esi edi
.B13.4:                         ; Preds .B13.3
        mov       ecx, DWORD PTR [12+esi+ebx]                   ;370.10
        mov       eax, ecx                                      ;370.10
        shr       eax, 1                                        ;370.10
        and       ecx, 1                                        ;370.10
        and       eax, 1                                        ;370.10
        add       ecx, ecx                                      ;370.10
        shl       eax, 2                                        ;370.10
        or        eax, 1                                        ;370.10
        or        eax, ecx                                      ;370.10
        or        eax, 262144                                   ;370.10
        push      eax                                           ;370.10
        push      DWORD PTR [esi+ebx]                           ;370.10
        call      _for_dealloc_allocatable                      ;370.10
                                ; LOE eax ebx esi edi
.B13.20:                        ; Preds .B13.4
        add       esp, 8                                        ;370.10
                                ; LOE eax ebx esi edi
.B13.5:                         ; Preds .B13.20
        and       DWORD PTR [12+esi+ebx], -2                    ;370.10
        mov       DWORD PTR [esi+ebx], 0                        ;370.10
        test      eax, eax                                      ;370.10
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_VECTORERROR], eax ;370.10
        jne       .B13.15       ; Prob 5%                       ;370.10
                                ; LOE ebx esi edi
.B13.6:                         ; Preds .B13.5 .B13.23
        xor       ecx, ecx                                      ;370.10
        mov       DWORD PTR [36+esi+ebx], ecx                   ;370.10
        mov       DWORD PTR [40+esi+ebx], ecx                   ;370.10
                                ; LOE ebx esi edi
.B13.7:                         ; Preds .B13.3 .B13.6
        inc       edi                                           ;371.3
        add       esi, 44                                       ;371.3
        cmp       edi, DWORD PTR [8+esp]                        ;371.3
        jle       .B13.3        ; Prob 82%                      ;371.3
                                ; LOE ebx esi edi
.B13.8:                         ; Preds .B13.7
        mov       edi, DWORD PTR [4+esp]                        ;
        mov       ebx, DWORD PTR [esp]                          ;
                                ; LOE ebx edi
.B13.9:                         ; Preds .B13.8 .B13.1
        mov       esi, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_ENTQUEVEHLIST+12] ;374.3
        test      esi, 1                                        ;374.7
        je        .B13.12       ; Prob 60%                      ;374.7
                                ; LOE ebx esi edi
.B13.10:                        ; Preds .B13.9
        mov       edx, esi                                      ;375.5
        mov       eax, esi                                      ;375.5
        shr       edx, 1                                        ;375.5
        and       eax, 1                                        ;375.5
        and       edx, 1                                        ;375.5
        add       eax, eax                                      ;375.5
        shl       edx, 2                                        ;375.5
        or        edx, 1                                        ;375.5
        or        edx, eax                                      ;375.5
        or        edx, 262144                                   ;375.5
        push      edx                                           ;375.5
        push      DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_ENTQUEVEHLIST] ;375.5
        call      _for_dealloc_allocatable                      ;375.5
                                ; LOE eax ebx esi edi
.B13.21:                        ; Preds .B13.10
        add       esp, 8                                        ;375.5
                                ; LOE eax ebx esi edi
.B13.11:                        ; Preds .B13.21
        and       esi, -2                                       ;375.5
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_ENTQUEVEHLIST], 0 ;375.5
        test      eax, eax                                      ;376.17
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_ENTQUEVEHLIST+12], esi ;375.5
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_VECTORERROR], eax ;375.5
        jne       .B13.13       ; Prob 5%                       ;376.17
                                ; LOE ebx edi
.B13.12:                        ; Preds .B13.22 .B13.11 .B13.9
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_ENQATT_ARRAYSIZE], 0 ;381.3
        add       esp, 92                                       ;383.1
        pop       esi                                           ;383.1
        mov       esp, ebp                                      ;383.1
        pop       ebp                                           ;383.1
        ret                                                     ;383.1
                                ; LOE
.B13.13:                        ; Preds .B13.11                 ; Infreq
        mov       DWORD PTR [esp], 0                            ;377.5
        lea       edx, DWORD PTR [esp]                          ;377.5
        mov       DWORD PTR [32+esp], 14                        ;377.5
        lea       eax, DWORD PTR [32+esp]                       ;377.5
        mov       DWORD PTR [36+esp], OFFSET FLAT: __STRLITPACK_16 ;377.5
        push      32                                            ;377.5
        push      eax                                           ;377.5
        push      OFFSET FLAT: __STRLITPACK_77.0.13             ;377.5
        push      -2088435968                                   ;377.5
        push      911                                           ;377.5
        push      edx                                           ;377.5
        call      _for_write_seq_lis                            ;377.5
                                ; LOE ebx edi
.B13.14:                        ; Preds .B13.13                 ; Infreq
        push      32                                            ;378.5
        xor       eax, eax                                      ;378.5
        push      eax                                           ;378.5
        push      eax                                           ;378.5
        push      -2088435968                                   ;378.5
        push      eax                                           ;378.5
        push      OFFSET FLAT: __STRLITPACK_78                  ;378.5
        call      _for_stop_core                                ;378.5
                                ; LOE ebx edi
.B13.22:                        ; Preds .B13.14                 ; Infreq
        add       esp, 48                                       ;378.5
        jmp       .B13.12       ; Prob 100%                     ;378.5
                                ; LOE ebx edi
.B13.15:                        ; Preds .B13.5                  ; Infreq
        mov       DWORD PTR [48+esp], 0                         ;370.10
        lea       eax, DWORD PTR [48+esp]                       ;370.10
        mov       DWORD PTR [40+esp], 36                        ;370.10
        lea       ecx, DWORD PTR [40+esp]                       ;370.10
        mov       DWORD PTR [44+esp], OFFSET FLAT: __STRLITPACK_14 ;370.10
        push      32                                            ;370.10
        push      ecx                                           ;370.10
        push      OFFSET FLAT: __STRLITPACK_79.0.14             ;370.10
        push      -2088435968                                   ;370.10
        push      911                                           ;370.10
        push      eax                                           ;370.10
        call      _for_write_seq_lis                            ;370.10
                                ; LOE ebx esi edi
.B13.16:                        ; Preds .B13.15                 ; Infreq
        mov       DWORD PTR [72+esp], 0                         ;370.10
        lea       ecx, DWORD PTR [104+esp]                      ;370.10
        mov       DWORD PTR [104+esp], edi                      ;370.10
        push      32                                            ;370.10
        push      ecx                                           ;370.10
        push      OFFSET FLAT: __STRLITPACK_80.0.14             ;370.10
        push      -2088435968                                   ;370.10
        push      911                                           ;370.10
        lea       ecx, DWORD PTR [92+esp]                       ;370.10
        push      ecx                                           ;370.10
        call      _for_write_seq_lis                            ;370.10
                                ; LOE ebx esi edi
.B13.17:                        ; Preds .B13.16                 ; Infreq
        push      32                                            ;370.10
        xor       ecx, ecx                                      ;370.10
        push      ecx                                           ;370.10
        push      ecx                                           ;370.10
        push      -2088435968                                   ;370.10
        push      ecx                                           ;370.10
        push      OFFSET FLAT: __STRLITPACK_81                  ;370.10
        call      _for_stop_core                                ;370.10
                                ; LOE ebx esi edi
.B13.23:                        ; Preds .B13.17                 ; Infreq
        add       esp, 72                                       ;370.10
        jmp       .B13.6        ; Prob 100%                     ;370.10
        ALIGN     16
                                ; LOE ebx esi edi
; mark_end;
_DYNUST_LINK_VEH_LIST_MODULE_mp_ENQATT_2DREMOVE ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_LINK_VEH_LIST_MODULE_mp_ENQATT_2DREMOVE
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_LINK_VEH_LIST_MODULE_mp_ENQATT_REMOVE
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_LINK_VEH_LIST_MODULE_mp_ENQATT_REMOVE
_DYNUST_LINK_VEH_LIST_MODULE_mp_ENQATT_REMOVE	PROC NEAR 
; parameter 1: 8 + ebp
.B14.1:                         ; Preds .B14.0
        push      ebp                                           ;386.12
        mov       ebp, esp                                      ;386.12
        and       esp, -16                                      ;386.12
        push      esi                                           ;386.12
        push      edi                                           ;386.12
        push      ebx                                           ;386.12
        sub       esp, 52                                       ;386.12
        mov       eax, DWORD PTR [8+ebp]                        ;386.12
        imul      edi, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_ENTQUEVEHLIST+32], -44 ;389.31
        mov       esi, DWORD PTR [eax]                          ;389.7
        imul      ebx, esi, 44                                  ;389.7
        add       edi, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_ENTQUEVEHLIST] ;389.31
        cmp       DWORD PTR [36+ebx+edi], 0                     ;389.31
        jle       .B14.5        ; Prob 79%                      ;389.31
                                ; LOE ebx esi edi
.B14.2:                         ; Preds .B14.1
        mov       eax, DWORD PTR [12+ebx+edi]                   ;390.16
        mov       edx, eax                                      ;390.5
        shr       edx, 1                                        ;390.5
        and       edx, 1                                        ;390.5
        mov       DWORD PTR [esp], eax                          ;390.16
        and       eax, 1                                        ;390.5
        shl       edx, 2                                        ;390.5
        add       eax, eax                                      ;390.5
        or        edx, 1                                        ;390.5
        or        edx, eax                                      ;390.5
        or        edx, 262144                                   ;390.5
        push      edx                                           ;390.5
        push      DWORD PTR [edi+ebx]                           ;390.5
        call      _for_dealloc_allocatable                      ;390.5
                                ; LOE eax ebx esi edi
.B14.11:                        ; Preds .B14.2
        add       esp, 8                                        ;390.5
                                ; LOE eax ebx esi edi
.B14.3:                         ; Preds .B14.11
        mov       edx, DWORD PTR [esp]                          ;390.5
        and       edx, -2                                       ;390.5
        mov       DWORD PTR [edi+ebx], 0                        ;390.5
        test      eax, eax                                      ;391.17
        mov       DWORD PTR [12+ebx+edi], edx                   ;390.5
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_VECTORERROR], eax ;390.5
        jne       .B14.6        ; Prob 5%                       ;391.17
                                ; LOE ebx esi edi
.B14.13:                        ; Preds .B14.3
        xor       esi, esi                                      ;396.5
                                ; LOE ebx esi edi
.B14.4:                         ; Preds .B14.12 .B14.13
        mov       DWORD PTR [36+ebx+edi], esi                   ;396.5
        mov       DWORD PTR [40+ebx+edi], esi                   ;397.5
                                ; LOE
.B14.5:                         ; Preds .B14.1 .B14.4
        add       esp, 52                                       ;400.1
        pop       ebx                                           ;400.1
        pop       edi                                           ;400.1
        pop       esi                                           ;400.1
        mov       esp, ebp                                      ;400.1
        pop       ebp                                           ;400.1
        ret                                                     ;400.1
                                ; LOE
.B14.6:                         ; Preds .B14.3                  ; Infreq
        mov       DWORD PTR [esp], 0                            ;392.4
        lea       edx, DWORD PTR [esp]                          ;392.4
        mov       DWORD PTR [32+esp], 36                        ;392.4
        lea       eax, DWORD PTR [32+esp]                       ;392.4
        mov       DWORD PTR [36+esp], OFFSET FLAT: __STRLITPACK_14 ;392.4
        push      32                                            ;392.4
        push      eax                                           ;392.4
        push      OFFSET FLAT: __STRLITPACK_79.0.14             ;392.4
        push      -2088435968                                   ;392.4
        push      911                                           ;392.4
        push      edx                                           ;392.4
        call      _for_write_seq_lis                            ;392.4
                                ; LOE ebx esi edi
.B14.7:                         ; Preds .B14.6                  ; Infreq
        mov       DWORD PTR [24+esp], 0                         ;393.4
        lea       eax, DWORD PTR [64+esp]                       ;393.4
        mov       DWORD PTR [64+esp], esi                       ;393.4
        push      32                                            ;393.4
        push      eax                                           ;393.4
        push      OFFSET FLAT: __STRLITPACK_80.0.14             ;393.4
        push      -2088435968                                   ;393.4
        push      911                                           ;393.4
        lea       edx, DWORD PTR [44+esp]                       ;393.4
        push      edx                                           ;393.4
        call      _for_write_seq_lis                            ;393.4
                                ; LOE ebx edi
.B14.8:                         ; Preds .B14.7                  ; Infreq
        push      32                                            ;394.4
        xor       esi, esi                                      ;394.4
        push      esi                                           ;394.4
        push      esi                                           ;394.4
        push      -2088435968                                   ;394.4
        push      esi                                           ;394.4
        push      OFFSET FLAT: __STRLITPACK_81                  ;394.4
        call      _for_stop_core                                ;394.4
                                ; LOE ebx esi edi
.B14.12:                        ; Preds .B14.8                  ; Infreq
        add       esp, 72                                       ;394.4
        jmp       .B14.4        ; Prob 100%                     ;394.4
        ALIGN     16
                                ; LOE ebx esi edi
; mark_end;
_DYNUST_LINK_VEH_LIST_MODULE_mp_ENQATT_REMOVE ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_LINK_VEH_LIST_MODULE_mp_ENQATT_REMOVE
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_LINK_VEH_LIST_MODULE_mp_ENTRYQUEUE_INSERT
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_LINK_VEH_LIST_MODULE_mp_ENTRYQUEUE_INSERT
_DYNUST_LINK_VEH_LIST_MODULE_mp_ENTRYQUEUE_INSERT	PROC NEAR 
; parameter 1: 8 + ebp
; parameter 2: 12 + ebp
.B15.1:                         ; Preds .B15.0
        push      ebp                                           ;405.12
        mov       ebp, esp                                      ;405.12
        and       esp, -16                                      ;405.12
        push      esi                                           ;405.12
        push      edi                                           ;405.12
        push      ebx                                           ;405.12
        sub       esp, 116                                      ;405.12
        mov       eax, DWORD PTR [8+ebp]                        ;405.12
        mov       edi, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_ENTQUEVEHLIST+32] ;409.6
        imul      ecx, edi, 44                                  ;409.35
        imul      esi, DWORD PTR [eax], 44                      ;409.35
        mov       edx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_ENTQUEVEHLIST] ;409.6
        neg       ecx                                           ;409.35
        mov       DWORD PTR [88+esp], edx                       ;409.6
        lea       eax, DWORD PTR [edx+esi]                      ;409.35
        mov       ebx, DWORD PTR [36+ecx+eax]                   ;409.38
        cmp       ebx, DWORD PTR [40+ecx+eax]                   ;409.35
        jne       .B15.47       ; Prob 50%                      ;409.35
                                ; LOE ebx esi edi
.B15.2:                         ; Preds .B15.1
        mov       eax, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_H_INCREASESIZE] ;410.15
        add       eax, ebx                                      ;410.5
        mov       DWORD PTR [80+esp], eax                       ;410.5
        test      ebx, ebx                                      ;411.13
        jg        .B15.4        ; Prob 21%                      ;411.13
                                ; LOE ebx
.B15.3:                         ; Preds .B15.2
        xor       ebx, ebx                                      ;411.13
        jmp       .B15.23       ; Prob 100%                     ;411.13
                                ; LOE ebx
.B15.4:                         ; Preds .B15.2
        mov       eax, 0                                        ;411.13
        mov       edx, ebx                                      ;411.13
        cmovl     edx, eax                                      ;411.13
        lea       ecx, DWORD PTR [12+esp]                       ;411.13
        push      4                                             ;411.13
        push      edx                                           ;411.13
        push      2                                             ;411.13
        push      ecx                                           ;411.13
        call      _for_check_mult_overflow                      ;411.13
                                ; LOE eax ebx
.B15.5:                         ; Preds .B15.4
        mov       edx, DWORD PTR [8+ebp]                        ;411.13
        and       eax, 1                                        ;411.13
        mov       ecx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP+32] ;411.13
        neg       ecx                                           ;411.13
        add       ecx, DWORD PTR [edx]                          ;411.13
        mov       esi, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP] ;411.13
        shl       eax, 4                                        ;411.13
        or        eax, 262145                                   ;411.13
        lea       edi, DWORD PTR [ecx+ecx*4]                    ;411.13
        push      eax                                           ;411.13
        lea       edx, DWORD PTR [esi+edi*8]                    ;411.13
        push      edx                                           ;411.13
        push      DWORD PTR [36+esp]                            ;411.13
        call      _for_allocate                                 ;411.13
                                ; LOE eax ebx
.B15.57:                        ; Preds .B15.5
        add       esp, 28                                       ;411.13
        mov       esi, eax                                      ;411.13
                                ; LOE ebx esi
.B15.6:                         ; Preds .B15.57
        test      esi, esi                                      ;411.13
        je        .B15.9        ; Prob 50%                      ;411.13
                                ; LOE ebx esi
.B15.7:                         ; Preds .B15.6
        mov       DWORD PTR [32+esp], 0                         ;411.13
        lea       edx, DWORD PTR [32+esp]                       ;411.13
        mov       DWORD PTR [16+esp], 25                        ;411.13
        lea       eax, DWORD PTR [16+esp]                       ;411.13
        mov       DWORD PTR [20+esp], OFFSET FLAT: __STRLITPACK_22 ;411.13
        push      32                                            ;411.13
        push      eax                                           ;411.13
        push      OFFSET FLAT: __STRLITPACK_71.0.12             ;411.13
        push      -2088435968                                   ;411.13
        push      911                                           ;411.13
        push      edx                                           ;411.13
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_VECTORERROR], esi ;411.13
        call      _for_write_seq_lis                            ;411.13
                                ; LOE ebx
.B15.8:                         ; Preds .B15.7
        push      32                                            ;411.13
        xor       eax, eax                                      ;411.13
        push      eax                                           ;411.13
        push      eax                                           ;411.13
        push      -2088435968                                   ;411.13
        push      eax                                           ;411.13
        push      OFFSET FLAT: __STRLITPACK_72                  ;411.13
        call      _for_stop_core                                ;411.13
                                ; LOE ebx
.B15.58:                        ; Preds .B15.8
        add       esp, 48                                       ;411.13
        jmp       .B15.11       ; Prob 100%                     ;411.13
                                ; LOE ebx
.B15.9:                         ; Preds .B15.6
        mov       edi, DWORD PTR [8+ebp]                        ;411.13
        xor       eax, eax                                      ;411.13
        mov       DWORD PTR [108+esp], ebx                      ;
        mov       ebx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP+32] ;411.13
        neg       ebx                                           ;411.13
        mov       DWORD PTR [esp], esi                          ;
        mov       esi, DWORD PTR [edi]                          ;411.13
        add       ebx, esi                                      ;411.13
        mov       ecx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP] ;411.13
        mov       edi, 1                                        ;411.13
        lea       edx, DWORD PTR [ebx+ebx*4]                    ;411.13
        mov       ebx, 4                                        ;411.13
        mov       DWORD PTR [16+ecx+edx*8], edi                 ;411.13
        mov       DWORD PTR [32+ecx+edx*8], edi                 ;411.13
        mov       edi, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_ENTQUEVEHLIST+32] ;411.13
        neg       edi                                           ;411.13
        add       edi, esi                                      ;411.13
        imul      edi, edi, 44                                  ;411.13
        mov       esi, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_ENTQUEVEHLIST] ;411.13
        mov       DWORD PTR [8+ecx+edx*8], eax                  ;411.13
        mov       DWORD PTR [4+ecx+edx*8], ebx                  ;411.13
        mov       esi, DWORD PTR [36+esi+edi]                   ;411.13
        test      esi, esi                                      ;411.13
        mov       DWORD PTR [28+ecx+edx*8], ebx                 ;411.13
        cmovl     esi, eax                                      ;411.13
        lea       eax, DWORD PTR [8+esp]                        ;411.13
        push      ebx                                           ;411.13
        push      esi                                           ;411.13
        push      2                                             ;411.13
        push      eax                                           ;411.13
        mov       DWORD PTR [24+ecx+edx*8], esi                 ;411.13
        mov       DWORD PTR [12+ecx+edx*8], 5                   ;411.13
        mov       esi, DWORD PTR [16+esp]                       ;411.13
        mov       ebx, DWORD PTR [124+esp]                      ;411.13
        call      _for_check_mult_overflow                      ;411.13
                                ; LOE ebx esi bl bh
.B15.59:                        ; Preds .B15.9
        add       esp, 16                                       ;411.13
                                ; LOE ebx esi bl bh
.B15.10:                        ; Preds .B15.59
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_VECTORERROR], esi ;411.13
                                ; LOE ebx
.B15.11:                        ; Preds .B15.58 .B15.10
        mov       eax, DWORD PTR [8+ebp]                        ;411.13
        imul      edx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_ENTQUEVEHLIST+32], 44 ;411.13
        mov       eax, DWORD PTR [eax]                          ;411.13
        imul      ecx, eax, 44                                  ;411.13
        mov       esi, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_ENTQUEVEHLIST] ;411.13
        add       ecx, esi                                      ;411.13
        sub       ecx, edx                                      ;411.13
        mov       DWORD PTR [esp], edx                          ;411.13
        mov       edx, DWORD PTR [36+ecx]                       ;411.13
        test      edx, edx                                      ;411.13
        jle       .B15.19       ; Prob 50%                      ;411.13
                                ; LOE eax edx ebx esi
.B15.12:                        ; Preds .B15.11
        mov       ecx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP] ;411.13
        mov       DWORD PTR [4+esp], ecx                        ;411.13
        mov       ecx, edx                                      ;411.13
        shr       ecx, 31                                       ;411.13
        add       ecx, edx                                      ;411.13
        sar       ecx, 1                                        ;411.13
        mov       edi, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP+32] ;411.13
        sub       esi, DWORD PTR [esp]                          ;
        mov       DWORD PTR [24+esp], edi                       ;411.13
        test      ecx, ecx                                      ;411.13
        mov       DWORD PTR [28+esp], ecx                       ;411.13
        jbe       .B15.48       ; Prob 3%                       ;411.13
                                ; LOE eax edx ebx esi
.B15.13:                        ; Preds .B15.12
        imul      ecx, DWORD PTR [24+esp], -40                  ;
        xor       edi, edi                                      ;
        add       ecx, DWORD PTR [4+esp]                        ;
        mov       DWORD PTR [64+esp], ecx                       ;
        mov       DWORD PTR [68+esp], esi                       ;
        mov       DWORD PTR [esp], edx                          ;
        mov       DWORD PTR [108+esp], ebx                      ;
                                ; LOE eax edi
.B15.14:                        ; Preds .B15.67 .B15.13
        imul      esi, eax, 44                                  ;411.13
        lea       edx, DWORD PTR [eax+eax*4]                    ;411.13
        mov       ebx, DWORD PTR [68+esp]                       ;411.13
        lea       ecx, DWORD PTR [1+edi+edi]                    ;411.13
        mov       DWORD PTR [72+esp], edi                       ;
        mov       eax, DWORD PTR [32+esi+ebx]                   ;411.13
        neg       eax                                           ;411.13
        add       eax, ecx                                      ;411.13
        imul      eax, DWORD PTR [28+esi+ebx]                   ;411.13
        mov       ebx, DWORD PTR [esi+ebx]                      ;411.13
        mov       esi, DWORD PTR [64+esp]                       ;411.13
        mov       eax, DWORD PTR [ebx+eax]                      ;411.13
        sub       ecx, DWORD PTR [32+esi+edx*8]                 ;411.13
        imul      ecx, DWORD PTR [28+esi+edx*8]                 ;411.13
        mov       edx, DWORD PTR [esi+edx*8]                    ;411.13
        mov       DWORD PTR [edx+ecx], eax                      ;411.13
        lea       eax, DWORD PTR [2+edi+edi]                    ;411.13
        mov       ecx, DWORD PTR [8+ebp]                        ;411.13
        mov       edx, DWORD PTR [ecx]                          ;411.13
        imul      ebx, edx, 44                                  ;411.13
        lea       ecx, DWORD PTR [edx+edx*4]                    ;411.13
        mov       edx, DWORD PTR [68+esp]                       ;411.13
        mov       edi, DWORD PTR [32+ebx+edx]                   ;411.13
        neg       edi                                           ;411.13
        add       edi, eax                                      ;411.13
        imul      edi, DWORD PTR [28+ebx+edx]                   ;411.13
        sub       eax, DWORD PTR [32+esi+ecx*8]                 ;411.13
        imul      eax, DWORD PTR [28+esi+ecx*8]                 ;411.13
        mov       ebx, DWORD PTR [ebx+edx]                      ;411.13
        mov       esi, DWORD PTR [esi+ecx*8]                    ;411.13
        mov       edi, DWORD PTR [ebx+edi]                      ;411.13
        mov       DWORD PTR [esi+eax], edi                      ;411.13
        mov       edi, DWORD PTR [72+esp]                       ;411.13
        inc       edi                                           ;411.13
        cmp       edi, DWORD PTR [28+esp]                       ;411.13
        jae       .B15.15       ; Prob 36%                      ;411.13
                                ; LOE edx edi dl dh
.B15.67:                        ; Preds .B15.14
        mov       eax, DWORD PTR [8+ebp]                        ;411.13
        mov       eax, DWORD PTR [eax]                          ;411.13
        jmp       .B15.14       ; Prob 100%                     ;411.13
                                ; LOE eax edi
.B15.15:                        ; Preds .B15.14
        mov       eax, DWORD PTR [8+ebp]                        ;411.13
        lea       ecx, DWORD PTR [1+edi+edi]                    ;411.13
        mov       esi, edx                                      ;
        mov       edx, DWORD PTR [esp]                          ;
        mov       ebx, DWORD PTR [108+esp]                      ;
        mov       eax, DWORD PTR [eax]                          ;411.13
                                ; LOE eax edx ecx ebx esi
.B15.16:                        ; Preds .B15.15 .B15.48
        lea       edi, DWORD PTR [-1+ecx]                       ;411.13
        cmp       edx, edi                                      ;411.13
        jbe       .B15.20       ; Prob 3%                       ;411.13
                                ; LOE eax ecx ebx esi
.B15.17:                        ; Preds .B15.16
        mov       edi, DWORD PTR [24+esp]                       ;411.13
        neg       edi                                           ;411.13
        imul      edx, eax, 44                                  ;411.13
        add       edi, eax                                      ;411.13
        mov       DWORD PTR [68+esp], esi                       ;
        lea       eax, DWORD PTR [edi+edi*4]                    ;411.13
        mov       edi, DWORD PTR [32+edx+esi]                   ;411.13
        neg       edi                                           ;411.13
        add       edi, ecx                                      ;411.13
        imul      edi, DWORD PTR [28+edx+esi]                   ;411.13
        mov       esi, DWORD PTR [edx+esi]                      ;411.13
        mov       edx, DWORD PTR [4+esp]                        ;411.13
        mov       esi, DWORD PTR [esi+edi]                      ;411.13
        sub       ecx, DWORD PTR [32+edx+eax*8]                 ;411.13
        imul      ecx, DWORD PTR [28+edx+eax*8]                 ;411.13
        mov       eax, DWORD PTR [edx+eax*8]                    ;411.13
        mov       DWORD PTR [eax+ecx], esi                      ;411.13
        mov       ecx, DWORD PTR [8+ebp]                        ;411.13
        mov       esi, DWORD PTR [68+esp]                       ;411.13
        mov       eax, DWORD PTR [ecx]                          ;411.13
        jmp       .B15.20       ; Prob 100%                     ;411.13
                                ; LOE eax ebx esi
.B15.19:                        ; Preds .B15.11
        sub       esi, DWORD PTR [esp]                          ;
                                ; LOE eax ebx esi
.B15.20:                        ; Preds .B15.16 .B15.17 .B15.19
        imul      eax, eax, 44                                  ;411.13
        mov       edx, DWORD PTR [12+eax+esi]                   ;411.13
        test      dl, 1                                         ;411.13
        mov       DWORD PTR [esp], eax                          ;411.13
        mov       DWORD PTR [4+esp], edx                        ;411.13
        je        .B15.23       ; Prob 60%                      ;411.13
                                ; LOE edx ebx esi dl dh
.B15.21:                        ; Preds .B15.20
        mov       eax, edx                                      ;411.13
        mov       edx, eax                                      ;411.13
        shr       edx, 1                                        ;411.13
        and       eax, 1                                        ;411.13
        and       edx, 1                                        ;411.13
        add       eax, eax                                      ;411.13
        shl       edx, 2                                        ;411.13
        or        edx, 1                                        ;411.13
        or        edx, eax                                      ;411.13
        or        edx, 262144                                   ;411.13
        push      edx                                           ;411.13
        mov       ecx, DWORD PTR [4+esp]                        ;411.13
        push      DWORD PTR [ecx+esi]                           ;411.13
        call      _for_dealloc_allocatable                      ;411.13
                                ; LOE eax ebx esi
.B15.60:                        ; Preds .B15.21
        add       esp, 8                                        ;411.13
                                ; LOE eax ebx esi
.B15.22:                        ; Preds .B15.60
        mov       ecx, DWORD PTR [esp]                          ;411.13
        mov       edx, DWORD PTR [4+esp]                        ;411.13
        and       edx, -2                                       ;411.13
        mov       DWORD PTR [ecx+esi], 0                        ;411.13
        test      eax, eax                                      ;411.13
        mov       DWORD PTR [12+ecx+esi], edx                   ;411.13
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_VECTORERROR], eax ;411.13
        jne       .B15.49       ; Prob 5%                       ;411.13
                                ; LOE ebx
.B15.23:                        ; Preds .B15.66 .B15.22 .B15.20 .B15.3
        mov       eax, DWORD PTR [80+esp]                       ;411.13
        xor       esi, esi                                      ;411.13
        test      eax, eax                                      ;411.13
        lea       edx, DWORD PTR [84+esp]                       ;411.13
        push      4                                             ;411.13
        cmovg     esi, eax                                      ;411.13
        push      esi                                           ;411.13
        push      2                                             ;411.13
        push      edx                                           ;411.13
        call      _for_check_mult_overflow                      ;411.13
                                ; LOE eax ebx esi
.B15.24:                        ; Preds .B15.23
        mov       edx, DWORD PTR [8+ebp]                        ;411.13
        and       eax, 1                                        ;411.13
        mov       ecx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_ENTQUEVEHLIST+32] ;411.13
        neg       ecx                                           ;411.13
        add       ecx, DWORD PTR [edx]                          ;411.13
        imul      edi, ecx, 44                                  ;411.13
        shl       eax, 4                                        ;411.13
        or        eax, 262145                                   ;411.13
        add       edi, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_ENTQUEVEHLIST] ;411.13
        push      eax                                           ;411.13
        push      edi                                           ;411.13
        push      DWORD PTR [108+esp]                           ;411.13
        call      _for_allocate                                 ;411.13
                                ; LOE eax ebx esi
.B15.62:                        ; Preds .B15.24
        add       esp, 28                                       ;411.13
        mov       edi, eax                                      ;411.13
                                ; LOE ebx esi edi
.B15.25:                        ; Preds .B15.62
        test      edi, edi                                      ;411.13
        je        .B15.28       ; Prob 50%                      ;411.13
                                ; LOE ebx esi edi
.B15.26:                        ; Preds .B15.25
        mov       DWORD PTR [32+esp], 0                         ;411.13
        lea       edx, DWORD PTR [32+esp]                       ;411.13
        mov       DWORD PTR [72+esp], 35                        ;411.13
        lea       eax, DWORD PTR [72+esp]                       ;411.13
        mov       DWORD PTR [76+esp], OFFSET FLAT: __STRLITPACK_18 ;411.13
        push      32                                            ;411.13
        push      eax                                           ;411.13
        push      OFFSET FLAT: __STRLITPACK_75.0.12             ;411.13
        push      -2088435968                                   ;411.13
        push      911                                           ;411.13
        push      edx                                           ;411.13
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_VECTORERROR], edi ;411.13
        call      _for_write_seq_lis                            ;411.13
                                ; LOE ebx
.B15.27:                        ; Preds .B15.26
        push      32                                            ;411.13
        xor       eax, eax                                      ;411.13
        push      eax                                           ;411.13
        push      eax                                           ;411.13
        push      -2088435968                                   ;411.13
        push      eax                                           ;411.13
        push      OFFSET FLAT: __STRLITPACK_76                  ;411.13
        call      _for_stop_core                                ;411.13
                                ; LOE ebx
.B15.63:                        ; Preds .B15.27
        add       esp, 48                                       ;411.13
        jmp       .B15.30       ; Prob 100%                     ;411.13
                                ; LOE ebx
.B15.28:                        ; Preds .B15.25
        mov       edx, DWORD PTR [8+ebp]                        ;411.13
        mov       eax, 4                                        ;411.13
        mov       DWORD PTR [24+esp], edi                       ;
        mov       edi, 1                                        ;411.13
        mov       ecx, DWORD PTR [edx]                          ;411.13
        sub       ecx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_ENTQUEVEHLIST+32] ;411.13
        imul      ecx, ecx, 44                                  ;411.13
        mov       edx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_ENTQUEVEHLIST] ;411.13
        mov       DWORD PTR [16+edx+ecx], edi                   ;411.13
        mov       DWORD PTR [32+edx+ecx], edi                   ;411.13
        lea       edi, DWORD PTR [64+esp]                       ;411.13
        push      eax                                           ;411.13
        push      esi                                           ;411.13
        push      2                                             ;411.13
        push      edi                                           ;411.13
        mov       DWORD PTR [12+edx+ecx], 5                     ;411.13
        mov       DWORD PTR [4+edx+ecx], eax                    ;411.13
        mov       DWORD PTR [8+edx+ecx], 0                      ;411.13
        mov       DWORD PTR [24+edx+ecx], esi                   ;411.13
        mov       DWORD PTR [28+edx+ecx], eax                   ;411.13
        mov       edi, DWORD PTR [40+esp]                       ;411.13
        call      _for_check_mult_overflow                      ;411.13
                                ; LOE ebx edi
.B15.64:                        ; Preds .B15.28
        add       esp, 16                                       ;411.13
                                ; LOE ebx edi
.B15.29:                        ; Preds .B15.64
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_VECTORERROR], edi ;411.13
                                ; LOE ebx
.B15.30:                        ; Preds .B15.63 .B15.29
        mov       eax, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_ENTQUEVEHLIST] ;411.13
        test      ebx, ebx                                      ;411.13
        mov       edi, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_ENTQUEVEHLIST+32] ;411.13
        mov       DWORD PTR [88+esp], eax                       ;411.13
        jle       .B15.53       ; Prob 16%                      ;411.13
                                ; LOE ebx edi
.B15.31:                        ; Preds .B15.30
        mov       eax, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP] ;411.13
        mov       ecx, ebx                                      ;411.13
        mov       DWORD PTR [28+esp], eax                       ;411.13
        imul      eax, edi, -44                                 ;
        shr       ecx, 31                                       ;411.13
        add       ecx, ebx                                      ;411.13
        sar       ecx, 1                                        ;411.13
        add       eax, DWORD PTR [88+esp]                       ;
        mov       edx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP+32] ;411.13
        test      ecx, ecx                                      ;411.13
        mov       DWORD PTR [100+esp], ecx                      ;411.13
        jbe       .B15.52       ; Prob 0%                       ;411.13
                                ; LOE eax edx ebx edi
.B15.32:                        ; Preds .B15.31
        imul      esi, edx, -40                                 ;
        xor       ecx, ecx                                      ;
        add       esi, DWORD PTR [28+esp]                       ;
        mov       DWORD PTR [96+esp], esi                       ;
        mov       DWORD PTR [92+esp], eax                       ;
        mov       DWORD PTR [24+esp], edx                       ;
        mov       DWORD PTR [108+esp], ebx                      ;
        mov       DWORD PTR [68+esp], edi                       ;
                                ; LOE ecx
.B15.33:                        ; Preds .B15.33 .B15.32
        mov       ebx, DWORD PTR [8+ebp]                        ;411.13
        mov       edx, DWORD PTR [96+esp]                       ;411.13
        mov       DWORD PTR [104+esp], ecx                      ;
        lea       ecx, DWORD PTR [1+ecx+ecx]                    ;411.13
        mov       esi, DWORD PTR [ebx]                          ;411.13
        imul      edi, esi, 44                                  ;411.13
        lea       esi, DWORD PTR [esi+esi*4]                    ;411.13
        mov       eax, DWORD PTR [32+edx+esi*8]                 ;411.13
        neg       eax                                           ;411.13
        add       eax, ecx                                      ;411.13
        imul      eax, DWORD PTR [28+edx+esi*8]                 ;411.13
        mov       edx, DWORD PTR [edx+esi*8]                    ;411.13
        mov       esi, DWORD PTR [92+esp]                       ;411.13
        mov       eax, DWORD PTR [edx+eax]                      ;411.13
        sub       ecx, DWORD PTR [32+edi+esi]                   ;411.13
        imul      ecx, DWORD PTR [28+edi+esi]                   ;411.13
        mov       edi, DWORD PTR [edi+esi]                      ;411.13
        mov       DWORD PTR [edi+ecx], eax                      ;411.13
        mov       edi, DWORD PTR [ebx]                          ;411.13
        mov       eax, DWORD PTR [96+esp]                       ;411.13
        mov       ecx, DWORD PTR [104+esp]                      ;411.13
        imul      ebx, edi, 44                                  ;411.13
        lea       edx, DWORD PTR [edi+edi*4]                    ;411.13
        mov       edi, DWORD PTR [32+eax+edx*8]                 ;411.13
        lea       ecx, DWORD PTR [2+ecx+ecx]                    ;411.13
        neg       edi                                           ;411.13
        add       edi, ecx                                      ;411.13
        imul      edi, DWORD PTR [28+eax+edx*8]                 ;411.13
        sub       ecx, DWORD PTR [32+ebx+esi]                   ;411.13
        imul      ecx, DWORD PTR [28+ebx+esi]                   ;411.13
        mov       eax, DWORD PTR [eax+edx*8]                    ;411.13
        mov       esi, DWORD PTR [ebx+esi]                      ;411.13
        mov       ebx, DWORD PTR [eax+edi]                      ;411.13
        mov       DWORD PTR [esi+ecx], ebx                      ;411.13
        mov       ecx, DWORD PTR [104+esp]                      ;411.13
        inc       ecx                                           ;411.13
        cmp       ecx, DWORD PTR [100+esp]                      ;411.13
        jb        .B15.33       ; Prob 64%                      ;411.13
                                ; LOE ecx
.B15.34:                        ; Preds .B15.33
        mov       eax, DWORD PTR [92+esp]                       ;
        lea       ecx, DWORD PTR [1+ecx+ecx]                    ;411.13
        mov       edx, DWORD PTR [24+esp]                       ;
        mov       ebx, DWORD PTR [108+esp]                      ;
        mov       edi, DWORD PTR [68+esp]                       ;
                                ; LOE eax edx ecx ebx edi
.B15.35:                        ; Preds .B15.34 .B15.52
        lea       esi, DWORD PTR [-1+ecx]                       ;411.13
        cmp       ebx, esi                                      ;411.13
        jbe       .B15.37       ; Prob 0%                       ;411.13
                                ; LOE eax edx ecx ebx edi
.B15.36:                        ; Preds .B15.35
        mov       esi, DWORD PTR [8+ebp]                        ;411.13
        neg       edx                                           ;411.13
        mov       DWORD PTR [68+esp], edi                       ;
        mov       edi, DWORD PTR [28+esp]                       ;411.13
        add       edx, DWORD PTR [esi]                          ;411.13
        mov       DWORD PTR [108+esp], ebx                      ;
        imul      ebx, DWORD PTR [esi], 44                      ;411.13
        lea       esi, DWORD PTR [edx+edx*4]                    ;411.13
        mov       edx, DWORD PTR [32+edi+esi*8]                 ;411.13
        neg       edx                                           ;411.13
        add       edx, ecx                                      ;411.13
        imul      edx, DWORD PTR [28+edi+esi*8]                 ;411.13
        sub       ecx, DWORD PTR [32+ebx+eax]                   ;411.13
        imul      ecx, DWORD PTR [28+ebx+eax]                   ;411.13
        mov       edi, DWORD PTR [edi+esi*8]                    ;411.13
        mov       ebx, DWORD PTR [ebx+eax]                      ;411.13
        mov       edx, DWORD PTR [edi+edx]                      ;411.13
        mov       DWORD PTR [ebx+ecx], edx                      ;411.13
        mov       ebx, DWORD PTR [108+esp]                      ;411.13
        mov       edi, DWORD PTR [68+esp]                       ;411.13
                                ; LOE eax ebx edi
.B15.37:                        ; Preds .B15.36 .B15.35 .B15.53
        cmp       ebx, DWORD PTR [80+esp]                       ;411.13
        lea       edx, DWORD PTR [1+ebx]                        ;411.13
        jge       .B15.44       ; Prob 50%                      ;411.13
                                ; LOE eax edx ebx edi
.B15.38:                        ; Preds .B15.37
        neg       edx                                           ;411.13
        add       edx, DWORD PTR [80+esp]                       ;411.13
        inc       edx                                           ;411.13
        mov       esi, edx                                      ;411.13
        shr       esi, 31                                       ;411.13
        add       esi, edx                                      ;411.13
        sar       esi, 1                                        ;411.13
        test      esi, esi                                      ;411.13
        jbe       .B15.51       ; Prob 10%                      ;411.13
                                ; LOE eax edx ebx esi edi
.B15.39:                        ; Preds .B15.38
        mov       DWORD PTR [24+esp], edx                       ;
        xor       ecx, ecx                                      ;
        mov       DWORD PTR [28+esp], esi                       ;
        mov       DWORD PTR [108+esp], ebx                      ;
        mov       DWORD PTR [68+esp], edi                       ;
        mov       edx, DWORD PTR [8+ebp]                        ;
                                ; LOE eax edx ecx
.B15.40:                        ; Preds .B15.40 .B15.39
        imul      edi, DWORD PTR [edx], 44                      ;411.13
        mov       ebx, DWORD PTR [108+esp]                      ;411.13
        mov       esi, DWORD PTR [32+edi+eax]                   ;411.13
        neg       esi                                           ;411.13
        mov       DWORD PTR [92+esp], ecx                       ;
        lea       ebx, DWORD PTR [1+ebx+ecx*2]                  ;411.13
        add       esi, ebx                                      ;411.13
        xor       ecx, ecx                                      ;411.13
        imul      esi, DWORD PTR [28+edi+eax]                   ;411.13
        inc       ebx                                           ;411.13
        mov       edi, DWORD PTR [edi+eax]                      ;411.13
        mov       DWORD PTR [edi+esi], ecx                      ;411.13
        imul      esi, DWORD PTR [edx], 44                      ;411.13
        sub       ebx, DWORD PTR [32+esi+eax]                   ;411.13
        imul      ebx, DWORD PTR [28+esi+eax]                   ;411.13
        mov       esi, DWORD PTR [esi+eax]                      ;411.13
        mov       DWORD PTR [esi+ebx], ecx                      ;411.13
        mov       ecx, DWORD PTR [92+esp]                       ;411.13
        inc       ecx                                           ;411.13
        cmp       ecx, DWORD PTR [28+esp]                       ;411.13
        jb        .B15.40       ; Prob 63%                      ;411.13
                                ; LOE eax edx ecx
.B15.41:                        ; Preds .B15.40
        mov       edx, DWORD PTR [24+esp]                       ;
        lea       ecx, DWORD PTR [1+ecx+ecx]                    ;411.13
        mov       ebx, DWORD PTR [108+esp]                      ;
        mov       edi, DWORD PTR [68+esp]                       ;
                                ; LOE eax edx ecx ebx edi
.B15.42:                        ; Preds .B15.41 .B15.51
        lea       esi, DWORD PTR [-1+ecx]                       ;411.13
        cmp       edx, esi                                      ;411.13
        jbe       .B15.44       ; Prob 10%                      ;411.13
                                ; LOE eax ecx ebx edi
.B15.43:                        ; Preds .B15.42
        mov       edx, DWORD PTR [8+ebp]                        ;411.13
        imul      esi, DWORD PTR [edx], 44                      ;411.13
        lea       edx, DWORD PTR [ecx+ebx]                      ;411.13
        sub       edx, DWORD PTR [32+esi+eax]                   ;411.13
        imul      edx, DWORD PTR [28+esi+eax]                   ;411.13
        mov       ecx, DWORD PTR [esi+eax]                      ;411.13
        mov       DWORD PTR [ecx+edx], 0                        ;411.13
                                ; LOE eax ebx edi
.B15.44:                        ; Preds .B15.42 .B15.37 .B15.43
        mov       esi, DWORD PTR [8+ebp]                        ;411.13
        mov       edx, DWORD PTR [80+esp]                       ;411.13
        mov       ecx, DWORD PTR [esi]                          ;411.13
        imul      esi, ecx, 44                                  ;411.13
        mov       DWORD PTR [36+eax+esi], edx                   ;411.13
        test      ebx, ebx                                      ;411.13
        mov       DWORD PTR [40+eax+esi], ebx                   ;411.13
        jle       .B15.47       ; Prob 79%                      ;411.13
                                ; LOE ecx esi edi
.B15.45:                        ; Preds .B15.44
        sub       ecx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP+32] ;411.13
        mov       edx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP] ;411.13
        mov       DWORD PTR [28+esp], edx                       ;411.13
        lea       ebx, DWORD PTR [ecx+ecx*4]                    ;411.13
        mov       ecx, DWORD PTR [12+edx+ebx*8]                 ;411.13
        mov       eax, ecx                                      ;411.13
        shr       eax, 1                                        ;411.13
        mov       DWORD PTR [24+esp], ecx                       ;411.13
        and       eax, 1                                        ;411.13
        and       ecx, 1                                        ;411.13
        shl       eax, 2                                        ;411.13
        add       ecx, ecx                                      ;411.13
        or        eax, ecx                                      ;411.13
        or        eax, 262144                                   ;411.13
        push      eax                                           ;411.13
        push      DWORD PTR [edx+ebx*8]                         ;411.13
        call      _for_dealloc_allocatable                      ;411.13
                                ; LOE ebx esi edi
.B15.65:                        ; Preds .B15.45
        add       esp, 8                                        ;411.13
                                ; LOE ebx esi edi
.B15.46:                        ; Preds .B15.65
        mov       edx, DWORD PTR [28+esp]                       ;411.13
        mov       eax, DWORD PTR [24+esp]                       ;411.13
        and       eax, -2                                       ;411.13
        mov       DWORD PTR [edx+ebx*8], 0                      ;411.13
        mov       DWORD PTR [12+edx+ebx*8], eax                 ;411.13
                                ; LOE esi edi
.B15.47:                        ; Preds .B15.44 .B15.46 .B15.1
        imul      eax, edi, -44                                 ;413.6
        add       esi, DWORD PTR [88+esp]                       ;413.6
        mov       edx, DWORD PTR [12+ebp]                       ;405.12
        mov       edi, DWORD PTR [40+eax+esi]                   ;413.33
        inc       edi                                           ;413.6
        mov       DWORD PTR [40+eax+esi], edi                   ;413.6
        sub       edi, DWORD PTR [32+eax+esi]                   ;414.6
        imul      edi, DWORD PTR [28+eax+esi]                   ;414.6
        mov       ebx, DWORD PTR [eax+esi]                      ;414.6
        mov       ecx, DWORD PTR [edx]                          ;414.6
        mov       DWORD PTR [ebx+edi], ecx                      ;414.6
        add       esp, 116                                      ;416.1
        pop       ebx                                           ;416.1
        pop       edi                                           ;416.1
        pop       esi                                           ;416.1
        mov       esp, ebp                                      ;416.1
        pop       ebp                                           ;416.1
        ret                                                     ;416.1
                                ; LOE
.B15.48:                        ; Preds .B15.12                 ; Infreq
        mov       ecx, 1                                        ;
        jmp       .B15.16       ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B15.49:                        ; Preds .B15.22                 ; Infreq
        mov       DWORD PTR [32+esp], 0                         ;411.13
        lea       edx, DWORD PTR [32+esp]                       ;411.13
        mov       DWORD PTR [esp], 39                           ;411.13
        lea       eax, DWORD PTR [esp]                          ;411.13
        mov       DWORD PTR [4+esp], OFFSET FLAT: __STRLITPACK_20 ;411.13
        push      32                                            ;411.13
        push      eax                                           ;411.13
        push      OFFSET FLAT: __STRLITPACK_73.0.12             ;411.13
        push      -2088435968                                   ;411.13
        push      911                                           ;411.13
        push      edx                                           ;411.13
        call      _for_write_seq_lis                            ;411.13
                                ; LOE ebx
.B15.50:                        ; Preds .B15.49                 ; Infreq
        push      32                                            ;411.13
        xor       eax, eax                                      ;411.13
        push      eax                                           ;411.13
        push      eax                                           ;411.13
        push      -2088435968                                   ;411.13
        push      eax                                           ;411.13
        push      OFFSET FLAT: __STRLITPACK_74                  ;411.13
        call      _for_stop_core                                ;411.13
                                ; LOE ebx
.B15.66:                        ; Preds .B15.50                 ; Infreq
        add       esp, 48                                       ;411.13
        jmp       .B15.23       ; Prob 100%                     ;411.13
                                ; LOE ebx
.B15.51:                        ; Preds .B15.38                 ; Infreq
        mov       ecx, 1                                        ;
        jmp       .B15.42       ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx edi
.B15.52:                        ; Preds .B15.31                 ; Infreq
        mov       ecx, 1                                        ;
        jmp       .B15.35       ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx edi
.B15.53:                        ; Preds .B15.30                 ; Infreq
        imul      eax, edi, -44                                 ;
        add       eax, DWORD PTR [88+esp]                       ;
        jmp       .B15.37       ; Prob 100%                     ;
        ALIGN     16
                                ; LOE eax ebx edi
; mark_end;
_DYNUST_LINK_VEH_LIST_MODULE_mp_ENTRYQUEUE_INSERT ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_LINK_VEH_LIST_MODULE_mp_ENTRYQUEUE_INSERT
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_LINK_VEH_LIST_MODULE_mp_ENTRYQUEUE_REMOVE
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_LINK_VEH_LIST_MODULE_mp_ENTRYQUEUE_REMOVE
_DYNUST_LINK_VEH_LIST_MODULE_mp_ENTRYQUEUE_REMOVE	PROC NEAR 
; parameter 1: 44 + esp
; parameter 2: 48 + esp
.B16.1:                         ; Preds .B16.0
        push      esi                                           ;420.12
        push      edi                                           ;420.12
        push      ebx                                           ;420.12
        push      ebp                                           ;420.12
        sub       esp, 24                                       ;420.12
        imul      ecx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_ENTQUEVEHLIST+32], 44 ;425.32
        mov       eax, DWORD PTR [48+esp]                       ;420.12
        mov       edi, ecx                                      ;425.32
        mov       edx, DWORD PTR [44+esp]                       ;420.12
        neg       edi                                           ;425.32
        mov       ebp, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_ENTQUEVEHLIST] ;425.4
        mov       ebx, DWORD PTR [eax]                          ;425.7
        mov       esi, ebx                                      ;425.7
        imul      eax, DWORD PTR [edx], 44                      ;425.32
        mov       DWORD PTR [esp], edi                          ;425.32
        lea       edx, DWORD PTR [ebp+eax]                      ;425.32
        mov       edi, DWORD PTR [40+edi+edx]                   ;425.7
        cmp       edi, ebx                                      ;425.32
        jne       .B16.6        ; Prob 50%                      ;425.32
                                ; LOE eax edx ecx ebx ebp esi edi
.B16.2:                         ; Preds .B16.1
        mov       esi, ecx                                      ;427.7
        neg       esi                                           ;427.7
        mov       ebx, DWORD PTR [esp]                          ;426.7
        mov       DWORD PTR [40+ebx+edx], 0                     ;426.7
        mov       ebx, DWORD PTR [32+esi+edx]                   ;427.7
        cmp       DWORD PTR [24+esi+edx], 0                     ;427.7
        jle       .B16.24       ; Prob 10%                      ;427.7
                                ; LOE eax ecx ebx ebp
.B16.3:                         ; Preds .B16.2
        sub       ebp, ecx                                      ;
        mov       esi, 1                                        ;
        mov       edx, DWORD PTR [44+esp]                       ;
        lea       edi, DWORD PTR [eax+ebp]                      ;
                                ; LOE eax edx ebx ebp esi edi
.B16.4:                         ; Preds .B16.4 .B16.3
        mov       ecx, DWORD PTR [32+edi]                       ;427.7
        inc       esi                                           ;427.7
        neg       ecx                                           ;427.7
        add       ecx, ebx                                      ;427.7
        inc       ebx                                           ;427.7
        imul      ecx, DWORD PTR [28+edi]                       ;427.7
        mov       eax, DWORD PTR [ebp+eax]                      ;427.7
        mov       DWORD PTR [eax+ecx], 0                        ;427.7
        imul      eax, DWORD PTR [edx], 44                      ;427.7
        lea       edi, DWORD PTR [ebp+eax]                      ;427.7
        cmp       esi, DWORD PTR [24+edi]                       ;427.7
        jle       .B16.4        ; Prob 82%                      ;427.7
        jmp       .B16.24       ; Prob 100%                     ;427.7
                                ; LOE eax edx ebx ebp esi edi
.B16.6:                         ; Preds .B16.1
        jle       .B16.14       ; Prob 50%                      ;429.8
                                ; LOE eax ecx ebx ebp esi edi
.B16.7:                         ; Preds .B16.6
        sub       edi, ebx                                      ;429.8
        sub       ebp, ecx                                      ;
        mov       edx, edi                                      ;429.8
        shr       edx, 31                                       ;429.8
        add       edx, edi                                      ;429.8
        sar       edx, 1                                        ;429.8
        test      edx, edx                                      ;429.8
        jbe       .B16.25       ; Prob 10%                      ;429.8
                                ; LOE edx ebx ebp esi edi
.B16.8:                         ; Preds .B16.7
        mov       DWORD PTR [12+esp], esi                       ;
        xor       eax, eax                                      ;
        mov       DWORD PTR [8+esp], edx                        ;
        mov       DWORD PTR [esp], edi                          ;
        mov       DWORD PTR [4+esp], ebx                        ;
                                ; LOE eax ebp
.B16.9:                         ; Preds .B16.29 .B16.8
        mov       ecx, DWORD PTR [4+esp]                        ;430.10
        lea       ebx, DWORD PTR [eax+eax]                      ;430.10
        mov       DWORD PTR [16+esp], eax                       ;
        lea       edi, DWORD PTR [ecx+eax*2]                    ;430.10
        mov       eax, DWORD PTR [44+esp]                       ;430.10
        mov       DWORD PTR [20+esp], edi                       ;430.10
        lea       edi, DWORD PTR [1+edi]                        ;430.10
        imul      eax, DWORD PTR [eax], 44                      ;430.10
        mov       esi, DWORD PTR [32+eax+ebp]                   ;430.45
        imul      esi, DWORD PTR [28+eax+ebp]                   ;430.10
        imul      edi, DWORD PTR [28+eax+ebp]                   ;430.10
        mov       edx, DWORD PTR [eax+ebp]                      ;430.45
        sub       edx, esi                                      ;430.10
        mov       esi, DWORD PTR [12+esp]                       ;430.10
        neg       esi                                           ;430.10
        add       esi, ebx                                      ;430.10
        mov       edi, DWORD PTR [edx+edi]                      ;430.10
        lea       esi, DWORD PTR [1+esi+ecx]                    ;430.10
        imul      esi, DWORD PTR [28+eax+ebp]                   ;430.10
        mov       DWORD PTR [edx+esi], edi                      ;430.10
        mov       edx, DWORD PTR [44+esp]                       ;430.10
        mov       esi, DWORD PTR [20+esp]                       ;430.10
        add       esi, 2                                        ;430.10
        imul      edx, DWORD PTR [edx], 44                      ;430.10
        mov       eax, DWORD PTR [32+edx+ebp]                   ;430.45
        imul      eax, DWORD PTR [28+edx+ebp]                   ;430.10
        imul      esi, DWORD PTR [28+edx+ebp]                   ;430.10
        mov       edi, DWORD PTR [edx+ebp]                      ;430.45
        sub       edi, eax                                      ;430.10
        mov       eax, DWORD PTR [48+esp]                       ;430.10
        sub       ebx, DWORD PTR [eax]                          ;430.10
        mov       eax, DWORD PTR [16+esp]                       ;429.8
        inc       eax                                           ;429.8
        lea       ebx, DWORD PTR [2+ebx+ecx]                    ;430.10
        mov       ecx, DWORD PTR [edi+esi]                      ;430.10
        imul      ebx, DWORD PTR [28+edx+ebp]                   ;430.10
        mov       DWORD PTR [edi+ebx], ecx                      ;430.10
        cmp       eax, DWORD PTR [8+esp]                        ;429.8
        jae       .B16.10       ; Prob 37%                      ;429.8
                                ; LOE eax ebp
.B16.29:                        ; Preds .B16.9
        mov       edx, DWORD PTR [48+esp]                       ;425.7
        mov       ecx, DWORD PTR [edx]                          ;425.7
        mov       DWORD PTR [12+esp], ecx                       ;425.7
        jmp       .B16.9        ; Prob 100%                     ;425.7
                                ; LOE eax ebp
.B16.10:                        ; Preds .B16.9
        mov       edx, DWORD PTR [48+esp]                       ;429.8
        lea       eax, DWORD PTR [1+eax+eax]                    ;429.8
        mov       edi, DWORD PTR [esp]                          ;
        mov       ebx, DWORD PTR [4+esp]                        ;
        mov       esi, DWORD PTR [edx]                          ;429.8
                                ; LOE eax ebx ebp esi edi
.B16.11:                        ; Preds .B16.10 .B16.25
        lea       edx, DWORD PTR [-1+eax]                       ;429.8
        cmp       edi, edx                                      ;429.8
        jbe       .B16.13       ; Prob 10%                      ;429.8
                                ; LOE eax ebx ebp esi
.B16.12:                        ; Preds .B16.11
        mov       edx, DWORD PTR [44+esp]                       ;430.10
        imul      ecx, DWORD PTR [edx], 44                      ;430.10
        lea       edx, DWORD PTR [eax+ebx]                      ;430.10
        sub       eax, esi                                      ;430.10
        mov       edi, DWORD PTR [32+ecx+ebp]                   ;430.45
        add       eax, ebx                                      ;430.10
        imul      edx, DWORD PTR [28+ecx+ebp]                   ;430.10
        imul      edi, DWORD PTR [28+ecx+ebp]                   ;430.10
        imul      eax, DWORD PTR [28+ecx+ebp]                   ;430.10
        add       edx, DWORD PTR [ecx+ebp]                      ;430.10
        add       eax, DWORD PTR [ecx+ebp]                      ;430.10
        sub       edx, edi                                      ;430.10
        sub       eax, edi                                      ;430.10
        mov       ebx, DWORD PTR [edx]                          ;430.10
        mov       DWORD PTR [eax], ebx                          ;430.10
        mov       eax, DWORD PTR [48+esp]                       ;430.10
        mov       esi, DWORD PTR [eax]                          ;430.10
                                ; LOE ebp esi
.B16.13:                        ; Preds .B16.11 .B16.12
        mov       eax, DWORD PTR [44+esp]                       ;
        mov       ebx, esi                                      ;432.5
        imul      eax, DWORD PTR [eax], 44                      ;
        jmp       .B16.15       ; Prob 100%                     ;
                                ; LOE eax ebx ebp
.B16.14:                        ; Preds .B16.6
        sub       ebp, ecx                                      ;
                                ; LOE eax ebx ebp
.B16.15:                        ; Preds .B16.13 .B16.14
        add       eax, ebp                                      ;432.5
        mov       ecx, DWORD PTR [40+eax]                       ;432.5
        sub       ecx, ebx                                      ;432.5
        test      ebx, ebx                                      ;432.5
        jle       .B16.23       ; Prob 50%                      ;432.5
                                ; LOE eax ecx ebx ebp
.B16.16:                        ; Preds .B16.15
        mov       esi, ebx                                      ;432.5
        shr       esi, 31                                       ;432.5
        add       esi, ebx                                      ;432.5
        sar       esi, 1                                        ;432.5
        test      esi, esi                                      ;432.5
        jbe       .B16.26       ; Prob 10%                      ;432.5
                                ; LOE ecx ebx ebp esi
.B16.17:                        ; Preds .B16.16
        xor       edx, edx                                      ;
        mov       DWORD PTR [esp], esi                          ;
        xor       eax, eax                                      ;
        mov       DWORD PTR [8+esp], ecx                        ;
        mov       DWORD PTR [4+esp], ebx                        ;
        mov       esi, DWORD PTR [44+esp]                       ;
                                ; LOE eax edx ebp esi
.B16.18:                        ; Preds .B16.18 .B16.17
        imul      edi, DWORD PTR [esi], 44                      ;432.5
        mov       ecx, DWORD PTR [8+esp]                        ;432.5
        mov       ebx, DWORD PTR [32+edi+ebp]                   ;432.5
        neg       ebx                                           ;432.5
        lea       ecx, DWORD PTR [1+ecx+edx*2]                  ;432.5
        inc       edx                                           ;432.5
        add       ebx, ecx                                      ;432.5
        inc       ecx                                           ;432.5
        imul      ebx, DWORD PTR [28+edi+ebp]                   ;432.5
        mov       edi, DWORD PTR [edi+ebp]                      ;432.5
        mov       DWORD PTR [edi+ebx], eax                      ;432.5
        imul      ebx, DWORD PTR [esi], 44                      ;432.5
        sub       ecx, DWORD PTR [32+ebx+ebp]                   ;432.5
        imul      ecx, DWORD PTR [28+ebx+ebp]                   ;432.5
        mov       ebx, DWORD PTR [ebx+ebp]                      ;432.5
        cmp       edx, DWORD PTR [esp]                          ;432.5
        mov       DWORD PTR [ebx+ecx], eax                      ;432.5
        jb        .B16.18       ; Prob 63%                      ;432.5
                                ; LOE eax edx ebp esi
.B16.19:                        ; Preds .B16.18
        mov       ecx, DWORD PTR [8+esp]                        ;
        lea       esi, DWORD PTR [1+edx+edx]                    ;432.5
        mov       ebx, DWORD PTR [4+esp]                        ;
                                ; LOE ecx ebx ebp esi
.B16.20:                        ; Preds .B16.19 .B16.26
        lea       eax, DWORD PTR [-1+esi]                       ;432.5
        cmp       ebx, eax                                      ;432.5
        jbe       .B16.22       ; Prob 10%                      ;432.5
                                ; LOE ecx ebp esi
.B16.21:                        ; Preds .B16.20
        mov       eax, DWORD PTR [44+esp]                       ;432.5
        lea       ebx, DWORD PTR [esi+ecx]                      ;432.5
        imul      edx, DWORD PTR [eax], 44                      ;432.5
        sub       ebx, DWORD PTR [32+edx+ebp]                   ;432.5
        imul      ebx, DWORD PTR [28+edx+ebp]                   ;432.5
        mov       ecx, DWORD PTR [edx+ebp]                      ;432.5
        mov       DWORD PTR [ecx+ebx], 0                        ;432.5
                                ; LOE ebp
.B16.22:                        ; Preds .B16.20 .B16.21
        mov       eax, DWORD PTR [44+esp]                       ;
        mov       edx, DWORD PTR [48+esp]                       ;433.32
        imul      eax, DWORD PTR [eax], 44                      ;
        add       eax, ebp                                      ;
        mov       ecx, DWORD PTR [40+eax]                       ;433.32
        sub       ecx, DWORD PTR [edx]                          ;433.32
                                ; LOE eax ecx
.B16.23:                        ; Preds .B16.22 .B16.15
        mov       DWORD PTR [40+eax], ecx                       ;433.5
                                ; LOE
.B16.24:                        ; Preds .B16.4 .B16.2 .B16.23
        add       esp, 24                                       ;435.1
        pop       ebp                                           ;435.1
        pop       ebx                                           ;435.1
        pop       edi                                           ;435.1
        pop       esi                                           ;435.1
        ret                                                     ;435.1
                                ; LOE
.B16.25:                        ; Preds .B16.7                  ; Infreq
        mov       eax, 1                                        ;
        jmp       .B16.11       ; Prob 100%                     ;
                                ; LOE eax ebx ebp esi edi
.B16.26:                        ; Preds .B16.16                 ; Infreq
        mov       esi, 1                                        ;
        jmp       .B16.20       ; Prob 100%                     ;
        ALIGN     16
                                ; LOE ecx ebx ebp esi
; mark_end;
_DYNUST_LINK_VEH_LIST_MODULE_mp_ENTRYQUEUE_REMOVE ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_LINK_VEH_LIST_MODULE_mp_ENTRYQUEUE_REMOVE
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_LINK_VEH_LIST_MODULE_mp_ENTRYQUEUE_INSFRONT
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_LINK_VEH_LIST_MODULE_mp_ENTRYQUEUE_INSFRONT
_DYNUST_LINK_VEH_LIST_MODULE_mp_ENTRYQUEUE_INSFRONT	PROC NEAR 
; parameter 1: 28 + esp
; parameter 2: 32 + esp
.B17.1:                         ; Preds .B17.0
        push      esi                                           ;438.12
        push      edi                                           ;438.12
        push      ebx                                           ;438.12
        push      ebp                                           ;438.12
        sub       esp, 8                                        ;438.12
        imul      edx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_ENTQUEVEHLIST+32], 44 ;443.3
        mov       ebx, DWORD PTR [28+esp]                       ;438.12
        mov       ebp, edx                                      ;443.3
        mov       ecx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_ENTQUEVEHLIST] ;443.3
        neg       ebp                                           ;443.3
        imul      eax, DWORD PTR [ebx], 44                      ;443.3
        lea       esi, DWORD PTR [ecx+eax]                      ;443.3
        mov       ebp, DWORD PTR [36+ebp+esi]                   ;443.13
        dec       ebp                                           ;443.3
        test      ebp, ebp                                      ;443.3
        jle       .B17.9        ; Prob 50%                      ;443.3
                                ; LOE eax edx ecx ebx ebp
.B17.2:                         ; Preds .B17.1
        mov       eax, ebp                                      ;443.3
        sub       ecx, edx                                      ;
        shr       eax, 31                                       ;443.3
        add       eax, ebp                                      ;443.3
        sar       eax, 1                                        ;443.3
        test      eax, eax                                      ;443.3
        jbe       .B17.11       ; Prob 3%                       ;443.3
                                ; LOE eax ecx ebx ebp
.B17.3:                         ; Preds .B17.2
        mov       DWORD PTR [4+esp], eax                        ;
        xor       edx, edx                                      ;
        mov       DWORD PTR [esp], ebp                          ;
                                ; LOE edx ecx ebx
.B17.4:                         ; Preds .B17.4 .B17.3
        imul      eax, DWORD PTR [ebx], 44                      ;444.6
        mov       edi, DWORD PTR [32+eax+ecx]                   ;444.34
        mov       ebp, DWORD PTR [28+eax+ecx]                   ;444.34
        imul      edi, ebp                                      ;444.6
        mov       esi, DWORD PTR [eax+ecx]                      ;444.34
        lea       eax, DWORD PTR [2+edx+edx]                    ;444.6
        sub       esi, edi                                      ;444.6
        lea       edi, DWORD PTR [1+edx+edx]                    ;444.6
        imul      edi, ebp                                      ;444.6
        imul      ebp, eax                                      ;444.6
        mov       edi, DWORD PTR [esi+edi]                      ;444.6
        mov       DWORD PTR [esi+ebp], edi                      ;444.6
        imul      ebp, DWORD PTR [ebx], 44                      ;444.6
        mov       edi, DWORD PTR [32+ebp+ecx]                   ;444.34
        mov       esi, DWORD PTR [28+ebp+ecx]                   ;444.34
        imul      edi, esi                                      ;444.6
        imul      eax, esi                                      ;444.6
        mov       ebp, DWORD PTR [ebp+ecx]                      ;444.34
        sub       ebp, edi                                      ;444.6
        lea       edi, DWORD PTR [3+edx+edx]                    ;444.6
        imul      esi, edi                                      ;444.6
        inc       edx                                           ;443.3
        mov       eax, DWORD PTR [ebp+eax]                      ;444.6
        mov       DWORD PTR [ebp+esi], eax                      ;444.6
        cmp       edx, DWORD PTR [4+esp]                        ;443.3
        jb        .B17.4        ; Prob 63%                      ;443.3
                                ; LOE edx ecx ebx
.B17.5:                         ; Preds .B17.4
        mov       ebp, DWORD PTR [esp]                          ;
        lea       eax, DWORD PTR [1+edx+edx]                    ;443.3
                                ; LOE eax ecx ebx ebp
.B17.6:                         ; Preds .B17.5 .B17.11
        lea       edx, DWORD PTR [-1+eax]                       ;443.3
        cmp       ebp, edx                                      ;443.3
        jbe       .B17.8        ; Prob 3%                       ;443.3
                                ; LOE eax ecx ebx
.B17.7:                         ; Preds .B17.6
        imul      ebp, DWORD PTR [ebx], 44                      ;444.6
        mov       edi, eax                                      ;444.6
        mov       edx, DWORD PTR [28+ebp+ecx]                   ;444.34
        inc       eax                                           ;444.6
        imul      edi, edx                                      ;444.6
        imul      eax, edx                                      ;444.6
        mov       esi, DWORD PTR [32+ebp+ecx]                   ;444.34
        imul      esi, edx                                      ;444.6
        add       edi, DWORD PTR [ebp+ecx]                      ;444.6
        add       eax, DWORD PTR [ebp+ecx]                      ;444.6
        sub       edi, esi                                      ;444.6
        sub       eax, esi                                      ;444.6
        mov       edx, DWORD PTR [edi]                          ;444.6
        mov       DWORD PTR [eax], edx                          ;444.6
                                ; LOE ecx ebx
.B17.8:                         ; Preds .B17.6 .B17.7
        imul      eax, DWORD PTR [ebx], 44                      ;
        jmp       .B17.10       ; Prob 100%                     ;
                                ; LOE eax ecx
.B17.9:                         ; Preds .B17.1
        sub       ecx, edx                                      ;
                                ; LOE eax ecx
.B17.10:                        ; Preds .B17.8 .B17.9
        mov       ebx, DWORD PTR [32+esp]                       ;438.12
        mov       ebp, DWORD PTR [28+ecx+eax]                   ;446.3
        mov       edx, DWORD PTR [32+ecx+eax]                   ;446.3
        imul      edx, ebp                                      ;446.3
        mov       ecx, DWORD PTR [ecx+eax]                      ;446.3
        sub       ecx, edx                                      ;446.3
        mov       eax, DWORD PTR [ebx]                          ;446.3
        mov       DWORD PTR [ebp+ecx], eax                      ;446.3
        add       esp, 8                                        ;447.1
        pop       ebp                                           ;447.1
        pop       ebx                                           ;447.1
        pop       edi                                           ;447.1
        pop       esi                                           ;447.1
        ret                                                     ;447.1
                                ; LOE
.B17.11:                        ; Preds .B17.2                  ; Infreq
        mov       eax, 1                                        ;
        jmp       .B17.6        ; Prob 100%                     ;
        ALIGN     16
                                ; LOE eax ecx ebx ebp
; mark_end;
_DYNUST_LINK_VEH_LIST_MODULE_mp_ENTRYQUEUE_INSFRONT ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_LINK_VEH_LIST_MODULE_mp_ENTRYQUEUE_INSFRONT
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_LINK_VEH_LIST_MODULE_mp_TCHAINATT_2DSETUP
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_LINK_VEH_LIST_MODULE_mp_TCHAINATT_2DSETUP
_DYNUST_LINK_VEH_LIST_MODULE_mp_TCHAINATT_2DSETUP	PROC NEAR 
; parameter 1: 8 + ebp
.B18.1:                         ; Preds .B18.0
        push      ebp                                           ;452.12
        mov       ebp, esp                                      ;452.12
        and       esp, -16                                      ;452.12
        push      esi                                           ;452.12
        push      edi                                           ;452.12
        push      ebx                                           ;452.12
        sub       esp, 132                                      ;452.12
        mov       esi, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TRIPCHAINLIST+12] ;457.3
        test      esi, 1                                        ;457.7
        je        .B18.13       ; Prob 60%                      ;457.7
                                ; LOE esi
.B18.2:                         ; Preds .B18.1
        mov       eax, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TCHAINATT_ARRAYSIZE] ;458.10
        test      eax, eax                                      ;458.10
        jle       .B18.10       ; Prob 2%                       ;458.10
                                ; LOE eax esi
.B18.3:                         ; Preds .B18.2
        imul      ebx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TRIPCHAINLIST+32], -40 ;
        mov       edi, 1                                        ;
        add       ebx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TRIPCHAINLIST] ;
        mov       DWORD PTR [4+esp], eax                        ;
        mov       DWORD PTR [esp], esi                          ;
                                ; LOE ebx edi
.B18.4:                         ; Preds .B18.8 .B18.3
        lea       esi, DWORD PTR [edi+edi*4]                    ;458.10
        movsx     eax, WORD PTR [36+ebx+esi*8]                  ;458.10
        test      eax, eax                                      ;458.10
        jle       .B18.8        ; Prob 79%                      ;458.10
                                ; LOE ebx esi edi
.B18.5:                         ; Preds .B18.4
        mov       eax, DWORD PTR [12+ebx+esi*8]                 ;458.10
        mov       edx, eax                                      ;458.10
        shr       edx, 1                                        ;458.10
        and       eax, 1                                        ;458.10
        and       edx, 1                                        ;458.10
        add       eax, eax                                      ;458.10
        shl       edx, 2                                        ;458.10
        or        edx, 1                                        ;458.10
        or        edx, eax                                      ;458.10
        or        edx, 262144                                   ;458.10
        push      edx                                           ;458.10
        push      DWORD PTR [ebx+esi*8]                         ;458.10
        call      _for_dealloc_allocatable                      ;458.10
                                ; LOE eax ebx esi edi
.B18.33:                        ; Preds .B18.5
        add       esp, 8                                        ;458.10
                                ; LOE eax ebx esi edi
.B18.6:                         ; Preds .B18.33
        and       DWORD PTR [12+ebx+esi*8], -2                  ;458.10
        mov       DWORD PTR [ebx+esi*8], 0                      ;458.10
        test      eax, eax                                      ;458.10
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_VECTORERROR], eax ;458.10
        jne       .B18.25       ; Prob 5%                       ;458.10
                                ; LOE ebx esi edi
.B18.7:                         ; Preds .B18.6 .B18.40
        xor       eax, eax                                      ;458.10
        mov       WORD PTR [36+ebx+esi*8], ax                   ;458.10
        mov       WORD PTR [38+ebx+esi*8], ax                   ;458.10
                                ; LOE ebx edi
.B18.8:                         ; Preds .B18.4 .B18.7
        inc       edi                                           ;458.10
        cmp       edi, DWORD PTR [4+esp]                        ;458.10
        jle       .B18.4        ; Prob 82%                      ;458.10
                                ; LOE ebx edi
.B18.9:                         ; Preds .B18.8
        mov       esi, DWORD PTR [esp]                          ;
                                ; LOE esi
.B18.10:                        ; Preds .B18.9 .B18.2
        mov       edx, esi                                      ;458.10
        mov       eax, esi                                      ;458.10
        shr       edx, 1                                        ;458.10
        and       eax, 1                                        ;458.10
        and       edx, 1                                        ;458.10
        add       eax, eax                                      ;458.10
        shl       edx, 2                                        ;458.10
        or        edx, 1                                        ;458.10
        or        edx, eax                                      ;458.10
        or        edx, 262144                                   ;458.10
        push      edx                                           ;458.10
        push      DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TRIPCHAINLIST] ;458.10
        call      _for_dealloc_allocatable                      ;458.10
                                ; LOE eax esi
.B18.34:                        ; Preds .B18.10
        add       esp, 8                                        ;458.10
                                ; LOE eax esi
.B18.11:                        ; Preds .B18.34
        and       esi, -2                                       ;458.10
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TRIPCHAINLIST], 0 ;458.10
        test      eax, eax                                      ;458.10
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TRIPCHAINLIST+12], esi ;458.10
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_VECTORERROR], eax ;458.10
        jne       .B18.23       ; Prob 5%                       ;458.10
                                ; LOE esi
.B18.12:                        ; Preds .B18.39 .B18.11
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TCHAINATT_ARRAYSIZE], 0 ;458.10
                                ; LOE esi
.B18.13:                        ; Preds .B18.12 .B18.1
        mov       ebx, DWORD PTR [8+ebp]                        ;452.12
        test      esi, 1                                        ;463.13
        jne       .B18.21       ; Prob 40%                      ;463.13
                                ; LOE ebx
.B18.14:                        ; Preds .B18.13
        xor       edi, edi                                      ;464.5
        lea       edx, DWORD PTR [128+esp]                      ;464.5
        mov       eax, DWORD PTR [ebx]                          ;464.5
        test      eax, eax                                      ;464.5
        push      40                                            ;464.5
        cmovl     eax, edi                                      ;464.5
        push      eax                                           ;464.5
        push      2                                             ;464.5
        push      edx                                           ;464.5
        call      _for_check_mult_overflow                      ;464.5
                                ; LOE eax ebx edi
.B18.15:                        ; Preds .B18.14
        mov       edx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TRIPCHAINLIST+12] ;464.5
        and       eax, 1                                        ;464.5
        and       edx, 1                                        ;464.5
        add       edx, edx                                      ;464.5
        shl       eax, 4                                        ;464.5
        or        edx, 1                                        ;464.5
        or        edx, eax                                      ;464.5
        or        edx, 262144                                   ;464.5
        push      edx                                           ;464.5
        push      OFFSET FLAT: _DYNUST_LINK_VEH_LIST_MODULE_mp_TRIPCHAINLIST ;464.5
        push      DWORD PTR [152+esp]                           ;464.5
        call      _for_alloc_allocatable                        ;464.5
                                ; LOE eax ebx edi
.B18.36:                        ; Preds .B18.15
        add       esp, 28                                       ;464.5
        mov       esi, eax                                      ;464.5
                                ; LOE ebx esi edi
.B18.16:                        ; Preds .B18.36
        test      esi, esi                                      ;464.5
        je        .B18.19       ; Prob 50%                      ;464.5
                                ; LOE ebx esi edi
.B18.17:                        ; Preds .B18.16
        mov       DWORD PTR [80+esp], 0                         ;466.5
        lea       edx, DWORD PTR [80+esp]                       ;466.5
        mov       DWORD PTR [120+esp], 25                       ;466.5
        lea       eax, DWORD PTR [120+esp]                      ;466.5
        mov       DWORD PTR [124+esp], OFFSET FLAT: __STRLITPACK_12 ;466.5
        push      32                                            ;466.5
        push      eax                                           ;466.5
        push      OFFSET FLAT: __STRLITPACK_82.0.18             ;466.5
        push      -2088435968                                   ;466.5
        push      911                                           ;466.5
        push      edx                                           ;466.5
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_VECTORERROR], esi ;464.5
        call      _for_write_seq_lis                            ;466.5
                                ; LOE ebx edi
.B18.18:                        ; Preds .B18.17
        push      32                                            ;467.4
        push      edi                                           ;467.4
        push      edi                                           ;467.4
        push      -2088435968                                   ;467.4
        push      edi                                           ;467.4
        push      OFFSET FLAT: __STRLITPACK_83                  ;467.4
        call      _for_stop_core                                ;467.4
                                ; LOE ebx
.B18.37:                        ; Preds .B18.18
        add       esp, 48                                       ;467.4
        jmp       .B18.21       ; Prob 100%                     ;467.4
                                ; LOE ebx
.B18.19:                        ; Preds .B18.16
        mov       eax, 40                                       ;464.5
        mov       edx, 1                                        ;464.5
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TRIPCHAINLIST+12], 133 ;464.5
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TRIPCHAINLIST+4], eax ;464.5
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TRIPCHAINLIST+16], edx ;464.5
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TRIPCHAINLIST+8], edi ;464.5
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TRIPCHAINLIST+32], edx ;464.5
        lea       edx, DWORD PTR [116+esp]                      ;464.5
        mov       ecx, DWORD PTR [ebx]                          ;464.5
        test      ecx, ecx                                      ;464.5
        push      eax                                           ;464.5
        cmovge    edi, ecx                                      ;464.5
        push      edi                                           ;464.5
        push      2                                             ;464.5
        push      edx                                           ;464.5
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TRIPCHAINLIST+24], edi ;464.5
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TRIPCHAINLIST+28], eax ;464.5
        call      _for_check_mult_overflow                      ;464.5
                                ; LOE ebx esi
.B18.38:                        ; Preds .B18.19
        add       esp, 16                                       ;464.5
                                ; LOE ebx esi
.B18.20:                        ; Preds .B18.38
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_VECTORERROR], esi ;464.5
                                ; LOE ebx
.B18.21:                        ; Preds .B18.37 .B18.13 .B18.20
        mov       esi, DWORD PTR [ebx]                          ;472.3
        test      esi, esi                                      ;472.3
        jg        .B18.28       ; Prob 2%                       ;472.3
                                ; LOE ebx esi
.B18.22:                        ; Preds .B18.30 .B18.21
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TCHAINATT_ARRAYSIZE], esi ;477.3
        add       esp, 132                                      ;479.1
        pop       ebx                                           ;479.1
        pop       edi                                           ;479.1
        pop       esi                                           ;479.1
        mov       esp, ebp                                      ;479.1
        pop       ebp                                           ;479.1
        ret                                                     ;479.1
                                ; LOE
.B18.23:                        ; Preds .B18.11                 ; Infreq
        mov       DWORD PTR [esp], 0                            ;458.10
        lea       edx, DWORD PTR [esp]                          ;458.10
        mov       DWORD PTR [32+esp], 14                        ;458.10
        lea       eax, DWORD PTR [32+esp]                       ;458.10
        mov       DWORD PTR [36+esp], OFFSET FLAT: __STRLITPACK_2 ;458.10
        push      32                                            ;458.10
        push      eax                                           ;458.10
        push      OFFSET FLAT: __STRLITPACK_92.0.22             ;458.10
        push      -2088435968                                   ;458.10
        push      911                                           ;458.10
        push      edx                                           ;458.10
        call      _for_write_seq_lis                            ;458.10
                                ; LOE esi
.B18.24:                        ; Preds .B18.23                 ; Infreq
        push      32                                            ;458.10
        xor       eax, eax                                      ;458.10
        push      eax                                           ;458.10
        push      eax                                           ;458.10
        push      -2088435968                                   ;458.10
        push      eax                                           ;458.10
        push      OFFSET FLAT: __STRLITPACK_93                  ;458.10
        call      _for_stop_core                                ;458.10
                                ; LOE esi
.B18.39:                        ; Preds .B18.24                 ; Infreq
        add       esp, 48                                       ;458.10
        jmp       .B18.12       ; Prob 100%                     ;458.10
                                ; LOE esi
.B18.25:                        ; Preds .B18.6                  ; Infreq
        mov       DWORD PTR [48+esp], 0                         ;458.10
        lea       edx, DWORD PTR [48+esp]                       ;458.10
        mov       DWORD PTR [40+esp], 36                        ;458.10
        lea       eax, DWORD PTR [40+esp]                       ;458.10
        mov       DWORD PTR [44+esp], OFFSET FLAT: __STRLITPACK_0 ;458.10
        push      32                                            ;458.10
        push      eax                                           ;458.10
        push      OFFSET FLAT: __STRLITPACK_94.0.23             ;458.10
        push      -2088435968                                   ;458.10
        push      911                                           ;458.10
        push      edx                                           ;458.10
        call      _for_write_seq_lis                            ;458.10
                                ; LOE ebx esi edi
.B18.26:                        ; Preds .B18.25                 ; Infreq
        mov       DWORD PTR [72+esp], 0                         ;458.10
        lea       eax, DWORD PTR [136+esp]                      ;458.10
        mov       DWORD PTR [136+esp], edi                      ;458.10
        push      32                                            ;458.10
        push      eax                                           ;458.10
        push      OFFSET FLAT: __STRLITPACK_95.0.23             ;458.10
        push      -2088435968                                   ;458.10
        push      911                                           ;458.10
        lea       edx, DWORD PTR [92+esp]                       ;458.10
        push      edx                                           ;458.10
        call      _for_write_seq_lis                            ;458.10
                                ; LOE ebx esi edi
.B18.27:                        ; Preds .B18.26                 ; Infreq
        push      32                                            ;458.10
        xor       eax, eax                                      ;458.10
        push      eax                                           ;458.10
        push      eax                                           ;458.10
        push      -2088435968                                   ;458.10
        push      eax                                           ;458.10
        push      OFFSET FLAT: __STRLITPACK_96                  ;458.10
        call      _for_stop_core                                ;458.10
                                ; LOE ebx esi edi
.B18.40:                        ; Preds .B18.27                 ; Infreq
        add       esp, 72                                       ;458.10
        jmp       .B18.7        ; Prob 100%                     ;458.10
                                ; LOE ebx esi edi
.B18.28:                        ; Preds .B18.21                 ; Infreq
        imul      eax, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TRIPCHAINLIST+32], -40 ;
        xor       ecx, ecx                                      ;
        add       eax, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TRIPCHAINLIST] ;
        xor       edx, edx                                      ;
                                ; LOE eax edx ecx ebx esi
.B18.29:                        ; Preds .B18.29 .B18.28         ; Infreq
        lea       edi, DWORD PTR [ecx+ecx*4]                    ;473.4
        inc       ecx                                           ;472.3
        mov       WORD PTR [76+eax+edi*8], dx                   ;473.4
        cmp       ecx, esi                                      ;472.3
        mov       WORD PTR [78+eax+edi*8], dx                   ;473.4
        jb        .B18.29       ; Prob 99%                      ;472.3
                                ; LOE eax edx ecx ebx esi
.B18.30:                        ; Preds .B18.29                 ; Infreq
        mov       esi, DWORD PTR [ebx]                          ;477.3
        jmp       .B18.22       ; Prob 100%                     ;477.3
        ALIGN     16
                                ; LOE esi
; mark_end;
_DYNUST_LINK_VEH_LIST_MODULE_mp_TCHAINATT_2DSETUP ENDP
_TEXT	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_82.0.18	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_LINK_VEH_LIST_MODULE_mp_TCHAINATT_2DSETUP
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_LINK_VEH_LIST_MODULE_mp_TCHAINATT_SETUP
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_LINK_VEH_LIST_MODULE_mp_TCHAINATT_SETUP
_DYNUST_LINK_VEH_LIST_MODULE_mp_TCHAINATT_SETUP	PROC NEAR 
; parameter 1: 8 + ebp
; parameter 2: 12 + ebp
.B19.1:                         ; Preds .B19.0
        push      ebp                                           ;482.12
        mov       ebp, esp                                      ;482.12
        and       esp, -16                                      ;482.12
        push      esi                                           ;482.12
        push      edi                                           ;482.12
        push      ebx                                           ;482.12
        sub       esp, 100                                      ;482.12
        mov       eax, DWORD PTR [8+ebp]                        ;482.12
        mov       ecx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TRIPCHAINLIST] ;490.4
        mov       edx, DWORD PTR [eax]                          ;490.8
        sub       edx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TRIPCHAINLIST+32] ;490.32
        lea       eax, DWORD PTR [edx+edx*4]                    ;490.32
        movsx     esi, WORD PTR [36+ecx+eax*8]                  ;490.8
        test      esi, esi                                      ;490.32
        jg        .B19.3        ; Prob 21%                      ;490.32
                                ; LOE esi
.B19.2:                         ; Preds .B19.1
        xor       esi, esi                                      ;487.2
        jmp       .B19.22       ; Prob 100%                     ;487.2
                                ; LOE esi
.B19.3:                         ; Preds .B19.1
        mov       eax, 0                                        ;492.6
        mov       edx, esi                                      ;492.6
        cmovl     edx, eax                                      ;492.6
        lea       ecx, DWORD PTR [12+esp]                       ;492.6
        push      4                                             ;492.6
        push      edx                                           ;492.6
        push      2                                             ;492.6
        push      ecx                                           ;492.6
        call      _for_check_mult_overflow                      ;492.6
                                ; LOE eax esi
.B19.4:                         ; Preds .B19.3
        mov       edx, DWORD PTR [8+ebp]                        ;492.6
        and       eax, 1                                        ;492.6
        mov       ecx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP+32] ;492.6
        neg       ecx                                           ;492.6
        add       ecx, DWORD PTR [edx]                          ;492.6
        mov       ebx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP] ;492.6
        shl       eax, 4                                        ;492.6
        or        eax, 262145                                   ;492.6
        lea       edi, DWORD PTR [ecx+ecx*4]                    ;492.6
        push      eax                                           ;492.6
        lea       edx, DWORD PTR [ebx+edi*8]                    ;492.6
        push      edx                                           ;492.6
        push      DWORD PTR [36+esp]                            ;492.6
        call      _for_allocate                                 ;492.6
                                ; LOE eax esi
.B19.57:                        ; Preds .B19.4
        add       esp, 28                                       ;492.6
        mov       ebx, eax                                      ;492.6
                                ; LOE ebx esi
.B19.5:                         ; Preds .B19.57
        test      ebx, ebx                                      ;492.6
        je        .B19.8        ; Prob 50%                      ;492.6
                                ; LOE ebx esi
.B19.6:                         ; Preds .B19.5
        mov       DWORD PTR [32+esp], 0                         ;494.5
        lea       edx, DWORD PTR [32+esp]                       ;494.5
        mov       DWORD PTR [16+esp], 25                        ;494.5
        lea       eax, DWORD PTR [16+esp]                       ;494.5
        mov       DWORD PTR [20+esp], OFFSET FLAT: __STRLITPACK_10 ;494.5
        push      32                                            ;494.5
        push      eax                                           ;494.5
        push      OFFSET FLAT: __STRLITPACK_84.0.19             ;494.5
        push      -2088435968                                   ;494.5
        push      911                                           ;494.5
        push      edx                                           ;494.5
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_VECTORERROR], ebx ;492.6
        call      _for_write_seq_lis                            ;494.5
                                ; LOE esi
.B19.7:                         ; Preds .B19.6
        push      32                                            ;495.5
        xor       eax, eax                                      ;495.5
        push      eax                                           ;495.5
        push      eax                                           ;495.5
        push      -2088435968                                   ;495.5
        push      eax                                           ;495.5
        push      OFFSET FLAT: __STRLITPACK_85                  ;495.5
        call      _for_stop_core                                ;495.5
                                ; LOE esi
.B19.58:                        ; Preds .B19.7
        add       esp, 48                                       ;495.5
        jmp       .B19.10       ; Prob 100%                     ;495.5
                                ; LOE esi
.B19.8:                         ; Preds .B19.5
        mov       edi, DWORD PTR [8+ebp]                        ;492.15
        mov       DWORD PTR [esp], ebx                          ;
        imul      ecx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP+32], -40 ;492.6
        mov       ebx, DWORD PTR [edi]                          ;492.15
        mov       edi, 1                                        ;492.6
        mov       eax, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP] ;492.6
        mov       DWORD PTR [96+esp], esi                       ;
        lea       esi, DWORD PTR [ebx+ebx*4]                    ;492.6
        mov       ebx, 4                                        ;492.6
        lea       edx, DWORD PTR [eax+esi*8]                    ;492.6
        xor       eax, eax                                      ;492.6
        mov       DWORD PTR [16+ecx+edx], edi                   ;492.6
        mov       DWORD PTR [32+ecx+edx], edi                   ;492.6
        imul      edi, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TRIPCHAINLIST+32], -40 ;492.6
        add       edi, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TRIPCHAINLIST] ;492.6
        mov       DWORD PTR [12+ecx+edx], 5                     ;492.6
        mov       DWORD PTR [4+ecx+edx], ebx                    ;492.6
        movsx     esi, WORD PTR [36+edi+esi*8]                  ;492.6
        test      esi, esi                                      ;492.6
        mov       DWORD PTR [8+ecx+edx], eax                    ;492.6
        cmovl     esi, eax                                      ;492.6
        mov       DWORD PTR [24+ecx+edx], esi                   ;492.6
        mov       DWORD PTR [28+ecx+edx], ebx                   ;492.6
        lea       edx, DWORD PTR [8+esp]                        ;492.6
        push      ebx                                           ;492.6
        push      esi                                           ;492.6
        push      2                                             ;492.6
        push      edx                                           ;492.6
        mov       ebx, DWORD PTR [16+esp]                       ;492.6
        mov       esi, DWORD PTR [112+esp]                      ;492.6
        call      _for_check_mult_overflow                      ;492.6
                                ; LOE ebx esi bl bh
.B19.59:                        ; Preds .B19.8
        add       esp, 16                                       ;492.6
                                ; LOE ebx esi bl bh
.B19.9:                         ; Preds .B19.59
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_VECTORERROR], ebx ;492.6
                                ; LOE esi
.B19.10:                        ; Preds .B19.58 .B19.9
        mov       eax, DWORD PTR [8+ebp]                        ;499.17
        mov       edx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TRIPCHAINLIST+32] ;499.6
        mov       ebx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TRIPCHAINLIST] ;499.6
        mov       eax, DWORD PTR [eax]                          ;499.17
        lea       edx, DWORD PTR [edx+edx*4]                    ;499.6
        shl       edx, 3                                        ;499.6
        lea       ecx, DWORD PTR [eax+eax*4]                    ;499.6
        lea       edi, DWORD PTR [ebx+ecx*8]                    ;499.6
        sub       edi, edx                                      ;499.6
        movsx     ecx, WORD PTR [36+edi]                        ;499.17
        test      ecx, ecx                                      ;499.6
        mov       DWORD PTR [24+esp], ecx                       ;499.17
        jle       .B19.18       ; Prob 50%                      ;499.6
                                ; LOE eax edx ebx esi
.B19.11:                        ; Preds .B19.10
        mov       ecx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP+32] ;500.23
        sub       ebx, edx                                      ;
        mov       edi, DWORD PTR [24+esp]                       ;499.6
        mov       DWORD PTR [4+esp], ecx                        ;500.23
        mov       ecx, edi                                      ;499.6
        shr       ecx, 31                                       ;499.6
        add       ecx, edi                                      ;499.6
        sar       ecx, 1                                        ;499.6
        mov       DWORD PTR [64+esp], ecx                       ;499.6
        test      ecx, ecx                                      ;499.6
        mov       ecx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP] ;499.6
        jbe       .B19.48       ; Prob 3%                       ;499.6
                                ; LOE eax ecx ebx esi
.B19.12:                        ; Preds .B19.11
        imul      edi, DWORD PTR [4+esp], -40                   ;
        xor       edx, edx                                      ;
        add       edi, ecx                                      ;
        mov       DWORD PTR [28+esp], edi                       ;
        mov       DWORD PTR [68+esp], ebx                       ;
        mov       DWORD PTR [esp], ecx                          ;
        mov       DWORD PTR [96+esp], esi                       ;
                                ; LOE eax edx
.B19.13:                        ; Preds .B19.67 .B19.12
        mov       esi, DWORD PTR [68+esp]                       ;500.23
        lea       edi, DWORD PTR [eax+eax*4]                    ;500.5
        lea       ebx, DWORD PTR [1+edx+edx]                    ;500.5
        mov       eax, DWORD PTR [32+esi+edi*8]                 ;500.23
        neg       eax                                           ;500.5
        add       eax, ebx                                      ;500.5
        imul      eax, DWORD PTR [28+esi+edi*8]                 ;500.5
        mov       ecx, DWORD PTR [esi+edi*8]                    ;500.23
        mov       esi, DWORD PTR [28+esp]                       ;500.5
        mov       ecx, DWORD PTR [ecx+eax]                      ;500.5
        sub       ebx, DWORD PTR [32+esi+edi*8]                 ;500.5
        imul      ebx, DWORD PTR [28+esi+edi*8]                 ;500.5
        mov       edi, DWORD PTR [esi+edi*8]                    ;500.5
        mov       eax, DWORD PTR [8+ebp]                        ;500.23
        mov       DWORD PTR [edi+ebx], ecx                      ;500.5
        lea       ecx, DWORD PTR [2+edx+edx]                    ;500.5
        mov       ebx, DWORD PTR [eax]                          ;500.23
        inc       edx                                           ;499.6
        mov       edi, DWORD PTR [68+esp]                       ;500.23
        lea       ebx, DWORD PTR [ebx+ebx*4]                    ;500.5
        mov       eax, DWORD PTR [32+edi+ebx*8]                 ;500.23
        neg       eax                                           ;500.5
        add       eax, ecx                                      ;500.5
        imul      eax, DWORD PTR [28+edi+ebx*8]                 ;500.5
        sub       ecx, DWORD PTR [32+esi+ebx*8]                 ;500.5
        imul      ecx, DWORD PTR [28+esi+ebx*8]                 ;500.5
        mov       edi, DWORD PTR [edi+ebx*8]                    ;500.23
        mov       ebx, DWORD PTR [esi+ebx*8]                    ;500.5
        cmp       edx, DWORD PTR [64+esp]                       ;499.6
        mov       esi, DWORD PTR [edi+eax]                      ;500.5
        mov       DWORD PTR [ebx+ecx], esi                      ;500.5
        jae       .B19.14       ; Prob 36%                      ;499.6
                                ; LOE edx
.B19.67:                        ; Preds .B19.13
        mov       eax, DWORD PTR [8+ebp]                        ;490.8
        mov       eax, DWORD PTR [eax]                          ;490.8
        jmp       .B19.13       ; Prob 100%                     ;490.8
                                ; LOE eax edx
.B19.14:                        ; Preds .B19.13
        mov       eax, DWORD PTR [8+ebp]                        ;500.23
        lea       edx, DWORD PTR [1+edx+edx]                    ;499.6
        mov       ebx, DWORD PTR [68+esp]                       ;
        mov       ecx, DWORD PTR [esp]                          ;
        mov       esi, DWORD PTR [96+esp]                       ;
        mov       eax, DWORD PTR [eax]                          ;500.23
                                ; LOE eax edx ecx ebx esi
.B19.15:                        ; Preds .B19.14 .B19.48
        lea       edi, DWORD PTR [-1+edx]                       ;499.6
        cmp       edi, DWORD PTR [24+esp]                       ;499.6
        jae       .B19.19       ; Prob 3%                       ;499.6
                                ; LOE eax edx ecx ebx esi
.B19.16:                        ; Preds .B19.15
        imul      edi, DWORD PTR [4+esp], -40                   ;500.5
        lea       eax, DWORD PTR [eax+eax*4]                    ;500.5
        add       ecx, edi                                      ;500.5
        mov       edi, DWORD PTR [32+ebx+eax*8]                 ;500.23
        neg       edi                                           ;500.5
        add       edi, edx                                      ;500.5
        imul      edi, DWORD PTR [28+ebx+eax*8]                 ;500.5
        sub       edx, DWORD PTR [32+ecx+eax*8]                 ;500.5
        imul      edx, DWORD PTR [28+ecx+eax*8]                 ;500.5
        mov       DWORD PTR [68+esp], ebx                       ;
        mov       ebx, DWORD PTR [ebx+eax*8]                    ;500.23
        mov       eax, DWORD PTR [ecx+eax*8]                    ;500.5
        mov       ecx, DWORD PTR [ebx+edi]                      ;500.5
        mov       DWORD PTR [eax+edx], ecx                      ;500.5
        mov       edx, DWORD PTR [8+ebp]                        ;504.6
        mov       ebx, DWORD PTR [68+esp]                       ;504.6
        mov       eax, DWORD PTR [edx]                          ;504.6
        jmp       .B19.19       ; Prob 100%                     ;504.6
                                ; LOE eax ebx esi
.B19.18:                        ; Preds .B19.10
        sub       ebx, edx                                      ;
                                ; LOE eax ebx esi
.B19.19:                        ; Preds .B19.15 .B19.16 .B19.18
        lea       eax, DWORD PTR [eax+eax*4]                    ;504.10
        mov       DWORD PTR [esp], eax                          ;504.10
        mov       edi, DWORD PTR [12+ebx+eax*8]                 ;504.6
        test      edi, 1                                        ;504.10
        je        .B19.22       ; Prob 60%                      ;504.10
                                ; LOE ebx esi edi
.B19.20:                        ; Preds .B19.19
        mov       edx, edi                                      ;505.4
        mov       eax, edi                                      ;505.4
        shr       edx, 1                                        ;505.4
        and       eax, 1                                        ;505.4
        and       edx, 1                                        ;505.4
        add       eax, eax                                      ;505.4
        shl       edx, 2                                        ;505.4
        or        edx, 1                                        ;505.4
        or        edx, eax                                      ;505.4
        or        edx, 262144                                   ;505.4
        push      edx                                           ;505.4
        mov       ecx, DWORD PTR [4+esp]                        ;505.4
        push      DWORD PTR [ebx+ecx*8]                         ;505.4
        call      _for_dealloc_allocatable                      ;505.4
                                ; LOE eax ebx esi edi
.B19.60:                        ; Preds .B19.20
        add       esp, 8                                        ;505.4
                                ; LOE eax ebx esi edi
.B19.21:                        ; Preds .B19.60
        mov       edx, DWORD PTR [esp]                          ;505.4
        and       edi, -2                                       ;505.4
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_VECTORERROR], eax ;505.4
        test      eax, eax                                      ;506.19
        mov       DWORD PTR [ebx+edx*8], 0                      ;505.4
        mov       DWORD PTR [12+ebx+edx*8], edi                 ;505.4
        jne       .B19.49       ; Prob 5%                       ;506.19
                                ; LOE esi
.B19.22:                        ; Preds .B19.66 .B19.21 .B19.19 .B19.2
        mov       eax, DWORD PTR [12+ebp]                       ;482.12
        xor       edx, edx                                      ;514.4
        lea       ebx, DWORD PTR [76+esp]                       ;514.4
        push      4                                             ;514.4
        mov       ecx, DWORD PTR [eax]                          ;514.4
        test      ecx, ecx                                      ;514.4
        cmovl     ecx, edx                                      ;514.4
        push      ecx                                           ;514.4
        push      2                                             ;514.4
        push      ebx                                           ;514.4
        call      _for_check_mult_overflow                      ;514.4
                                ; LOE eax esi
.B19.23:                        ; Preds .B19.22
        mov       edx, DWORD PTR [8+ebp]                        ;514.4
        and       eax, 1                                        ;514.4
        mov       ecx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TRIPCHAINLIST+32] ;514.4
        neg       ecx                                           ;514.4
        add       ecx, DWORD PTR [edx]                          ;514.4
        mov       ebx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TRIPCHAINLIST] ;514.4
        shl       eax, 4                                        ;514.4
        or        eax, 262145                                   ;514.4
        lea       edi, DWORD PTR [ecx+ecx*4]                    ;514.4
        push      eax                                           ;514.4
        lea       edx, DWORD PTR [ebx+edi*8]                    ;514.4
        push      edx                                           ;514.4
        push      DWORD PTR [100+esp]                           ;514.4
        call      _for_allocate                                 ;514.4
                                ; LOE eax esi
.B19.62:                        ; Preds .B19.23
        add       esp, 28                                       ;514.4
        mov       ebx, eax                                      ;514.4
                                ; LOE ebx esi
.B19.24:                        ; Preds .B19.62
        test      ebx, ebx                                      ;514.4
        jne       .B19.27       ; Prob 50%                      ;514.4
                                ; LOE ebx esi
.B19.25:                        ; Preds .B19.24
        mov       DWORD PTR [28+esp], ebx                       ;
        xor       edi, edi                                      ;514.4
        mov       ebx, DWORD PTR [8+ebp]                        ;514.13
        mov       ecx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TRIPCHAINLIST] ;514.4
        mov       eax, DWORD PTR [ebx]                          ;514.13
        mov       ebx, 4                                        ;514.4
        sub       eax, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TRIPCHAINLIST+32] ;514.4
        lea       edx, DWORD PTR [eax+eax*4]                    ;514.4
        mov       eax, 1                                        ;514.4
        mov       DWORD PTR [16+ecx+edx*8], eax                 ;514.4
        mov       DWORD PTR [32+ecx+edx*8], eax                 ;514.4
        mov       eax, DWORD PTR [12+ebp]                       ;514.4
        mov       DWORD PTR [12+ecx+edx*8], 5                   ;514.4
        mov       DWORD PTR [4+ecx+edx*8], ebx                  ;514.4
        mov       eax, DWORD PTR [eax]                          ;514.4
        test      eax, eax                                      ;514.4
        mov       DWORD PTR [8+ecx+edx*8], edi                  ;514.4
        cmovl     eax, edi                                      ;514.4
        mov       DWORD PTR [24+ecx+edx*8], eax                 ;514.4
        mov       DWORD PTR [28+ecx+edx*8], ebx                 ;514.4
        lea       edx, DWORD PTR [24+esp]                       ;514.4
        push      ebx                                           ;514.4
        push      eax                                           ;514.4
        push      2                                             ;514.4
        push      edx                                           ;514.4
        mov       ebx, DWORD PTR [44+esp]                       ;514.4
        call      _for_check_mult_overflow                      ;514.4
                                ; LOE ebx esi bl bh
.B19.63:                        ; Preds .B19.25
        add       esp, 16                                       ;514.4
                                ; LOE ebx esi bl bh
.B19.26:                        ; Preds .B19.63
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_VECTORERROR], ebx ;514.4
        jmp       .B19.29       ; Prob 100%                     ;514.4
                                ; LOE esi
.B19.27:                        ; Preds .B19.24
        mov       DWORD PTR [32+esp], 0                         ;516.7
        lea       edx, DWORD PTR [32+esp]                       ;516.7
        mov       DWORD PTR [64+esp], 35                        ;516.7
        lea       eax, DWORD PTR [64+esp]                       ;516.7
        mov       DWORD PTR [68+esp], OFFSET FLAT: __STRLITPACK_6 ;516.7
        push      32                                            ;516.7
        push      eax                                           ;516.7
        push      OFFSET FLAT: __STRLITPACK_88.0.19             ;516.7
        push      -2088435968                                   ;516.7
        push      911                                           ;516.7
        push      edx                                           ;516.7
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_VECTORERROR], ebx ;514.4
        call      _for_write_seq_lis                            ;516.7
                                ; LOE esi
.B19.28:                        ; Preds .B19.27
        push      32                                            ;517.4
        xor       eax, eax                                      ;517.4
        push      eax                                           ;517.4
        push      eax                                           ;517.4
        push      -2088435968                                   ;517.4
        push      eax                                           ;517.4
        push      OFFSET FLAT: __STRLITPACK_89                  ;517.4
        call      _for_stop_core                                ;517.4
                                ; LOE esi
.B19.64:                        ; Preds .B19.28
        add       esp, 48                                       ;517.4
                                ; LOE esi
.B19.29:                        ; Preds .B19.64 .B19.26
        mov       edi, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TRIPCHAINLIST] ;523.32
        test      esi, esi                                      ;521.15
        mov       ebx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TRIPCHAINLIST+32] ;523.32
        jle       .B19.53       ; Prob 16%                      ;521.15
                                ; LOE ebx esi edi
.B19.30:                        ; Preds .B19.29
        imul      ebx, ebx, -40                                 ;
        mov       ecx, esi                                      ;523.32
        shr       ecx, 31                                       ;523.32
        add       edi, ebx                                      ;
        add       ecx, esi                                      ;523.32
        sar       ecx, 1                                        ;523.32
        mov       edx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP] ;523.6
        test      ecx, ecx                                      ;523.32
        mov       eax, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP+32] ;523.6
        jbe       .B19.52       ; Prob 0%                       ;523.32
                                ; LOE eax edx ecx esi edi
.B19.31:                        ; Preds .B19.30
        xor       ebx, ebx                                      ;
        mov       DWORD PTR [92+esp], ebx                       ;
        imul      ebx, eax, -40                                 ;
        mov       DWORD PTR [28+esp], eax                       ;
        add       ebx, edx                                      ;
        mov       DWORD PTR [88+esp], ebx                       ;
        mov       DWORD PTR [84+esp], edi                       ;
        mov       DWORD PTR [80+esp], ecx                       ;
        mov       DWORD PTR [72+esp], edx                       ;
        mov       DWORD PTR [96+esp], esi                       ;
        mov       eax, DWORD PTR [92+esp]                       ;
                                ; LOE eax
.B19.32:                        ; Preds .B19.32 .B19.31
        mov       ebx, DWORD PTR [8+ebp]                        ;523.32
        lea       ecx, DWORD PTR [1+eax+eax]                    ;523.6
        mov       edx, DWORD PTR [88+esp]                       ;523.32
        mov       DWORD PTR [92+esp], eax                       ;
        mov       esi, DWORD PTR [ebx]                          ;523.32
        lea       edi, DWORD PTR [esi+esi*4]                    ;523.6
        mov       esi, DWORD PTR [84+esp]                       ;523.6
        mov       eax, DWORD PTR [32+edx+edi*8]                 ;523.32
        neg       eax                                           ;523.6
        add       eax, ecx                                      ;523.6
        imul      eax, DWORD PTR [28+edx+edi*8]                 ;523.6
        sub       ecx, DWORD PTR [32+esi+edi*8]                 ;523.6
        imul      ecx, DWORD PTR [28+esi+edi*8]                 ;523.6
        mov       edx, DWORD PTR [edx+edi*8]                    ;523.32
        mov       edi, DWORD PTR [esi+edi*8]                    ;523.6
        mov       eax, DWORD PTR [edx+eax]                      ;523.6
        mov       DWORD PTR [edi+ecx], eax                      ;523.6
        mov       ebx, DWORD PTR [ebx]                          ;523.32
        mov       edi, DWORD PTR [88+esp]                       ;523.32
        mov       eax, DWORD PTR [92+esp]                       ;523.6
        lea       ecx, DWORD PTR [ebx+ebx*4]                    ;523.6
        mov       ebx, DWORD PTR [32+edi+ecx*8]                 ;523.32
        neg       ebx                                           ;523.6
        lea       edx, DWORD PTR [2+eax+eax]                    ;523.6
        add       ebx, edx                                      ;523.6
        inc       eax                                           ;523.32
        imul      ebx, DWORD PTR [28+edi+ecx*8]                 ;523.6
        sub       edx, DWORD PTR [32+esi+ecx*8]                 ;523.6
        imul      edx, DWORD PTR [28+esi+ecx*8]                 ;523.6
        mov       edi, DWORD PTR [edi+ecx*8]                    ;523.32
        mov       esi, DWORD PTR [esi+ecx*8]                    ;523.6
        cmp       eax, DWORD PTR [80+esp]                       ;523.32
        mov       ecx, DWORD PTR [edi+ebx]                      ;523.6
        mov       DWORD PTR [esi+edx], ecx                      ;523.6
        jb        .B19.32       ; Prob 64%                      ;523.32
                                ; LOE eax
.B19.33:                        ; Preds .B19.32
        mov       DWORD PTR [92+esp], eax                       ;
        mov       ecx, eax                                      ;523.32
        mov       edi, DWORD PTR [84+esp]                       ;
        mov       eax, DWORD PTR [28+esp]                       ;
        mov       edx, DWORD PTR [72+esp]                       ;
        lea       ebx, DWORD PTR [1+ecx+ecx]                    ;523.32
        mov       esi, DWORD PTR [96+esp]                       ;
                                ; LOE eax edx ebx esi edi
.B19.34:                        ; Preds .B19.33 .B19.52
        lea       ecx, DWORD PTR [-1+ebx]                       ;523.32
        cmp       esi, ecx                                      ;523.32
        jbe       .B19.36       ; Prob 0%                       ;523.32
                                ; LOE eax edx ebx esi edi
.B19.35:                        ; Preds .B19.34
        imul      eax, eax, -40                                 ;523.6
        mov       ecx, DWORD PTR [8+ebp]                        ;523.32
        add       edx, eax                                      ;523.6
        mov       ecx, DWORD PTR [ecx]                          ;523.32
        lea       ecx, DWORD PTR [ecx+ecx*4]                    ;523.6
        mov       eax, DWORD PTR [32+edx+ecx*8]                 ;523.32
        neg       eax                                           ;523.6
        add       eax, ebx                                      ;523.6
        imul      eax, DWORD PTR [28+edx+ecx*8]                 ;523.6
        sub       ebx, DWORD PTR [32+edi+ecx*8]                 ;523.6
        imul      ebx, DWORD PTR [28+edi+ecx*8]                 ;523.6
        mov       edx, DWORD PTR [edx+ecx*8]                    ;523.32
        mov       ecx, DWORD PTR [edi+ecx*8]                    ;523.6
        mov       edx, DWORD PTR [edx+eax]                      ;523.6
        mov       DWORD PTR [ecx+ebx], edx                      ;523.6
                                ; LOE esi edi
.B19.36:                        ; Preds .B19.35 .B19.34 .B19.53
        mov       eax, DWORD PTR [12+ebp]                       ;528.4
        mov       ecx, DWORD PTR [eax]                          ;528.4
        lea       eax, DWORD PTR [1+esi]                        ;528.4
        cmp       ecx, esi                                      ;528.4
        jle       .B19.44       ; Prob 50%                      ;528.4
                                ; LOE eax ecx esi edi
.B19.37:                        ; Preds .B19.36
        mov       edx, ecx                                      ;528.4
        sub       edx, eax                                      ;528.4
        inc       edx                                           ;528.4
        mov       ebx, edx                                      ;528.4
        shr       ebx, 31                                       ;528.4
        add       ebx, edx                                      ;528.4
        sar       ebx, 1                                        ;528.4
        test      ebx, ebx                                      ;528.4
        jbe       .B19.51       ; Prob 10%                      ;528.4
                                ; LOE eax ecx ebx esi edi
.B19.38:                        ; Preds .B19.37
        mov       DWORD PTR [72+esp], eax                       ;
        xor       edx, edx                                      ;
        mov       DWORD PTR [80+esp], ebx                       ;
        mov       DWORD PTR [28+esp], ecx                       ;
        mov       DWORD PTR [96+esp], esi                       ;
        mov       eax, DWORD PTR [8+ebp]                        ;
                                ; LOE eax edx edi
.B19.39:                        ; Preds .B19.39 .B19.38
        mov       ecx, DWORD PTR [eax]                          ;529.7
        mov       ebx, DWORD PTR [96+esp]                       ;529.7
        mov       DWORD PTR [84+esp], edx                       ;
        lea       esi, DWORD PTR [ecx+ecx*4]                    ;529.7
        lea       ecx, DWORD PTR [1+ebx+edx*2]                  ;529.7
        mov       ebx, DWORD PTR [32+edi+esi*8]                 ;529.7
        neg       ebx                                           ;529.7
        xor       edx, edx                                      ;529.7
        add       ebx, ecx                                      ;529.7
        inc       ecx                                           ;529.7
        imul      ebx, DWORD PTR [28+edi+esi*8]                 ;529.7
        mov       esi, DWORD PTR [edi+esi*8]                    ;529.7
        mov       DWORD PTR [esi+ebx], edx                      ;529.7
        mov       ebx, DWORD PTR [eax]                          ;529.7
        lea       esi, DWORD PTR [ebx+ebx*4]                    ;529.7
        sub       ecx, DWORD PTR [32+edi+esi*8]                 ;529.7
        imul      ecx, DWORD PTR [28+edi+esi*8]                 ;529.7
        mov       ebx, DWORD PTR [edi+esi*8]                    ;529.7
        mov       DWORD PTR [ebx+ecx], edx                      ;529.7
        mov       edx, DWORD PTR [84+esp]                       ;528.4
        inc       edx                                           ;528.4
        cmp       edx, DWORD PTR [80+esp]                       ;528.4
        jb        .B19.39       ; Prob 63%                      ;528.4
                                ; LOE eax edx edi
.B19.40:                        ; Preds .B19.39
        mov       eax, DWORD PTR [72+esp]                       ;
        lea       edx, DWORD PTR [1+edx+edx]                    ;528.4
        mov       ecx, DWORD PTR [28+esp]                       ;
        mov       esi, DWORD PTR [96+esp]                       ;
                                ; LOE eax edx ecx esi edi
.B19.41:                        ; Preds .B19.40 .B19.51
        sub       ecx, eax                                      ;528.4
        lea       eax, DWORD PTR [-1+edx]                       ;528.4
        inc       ecx                                           ;528.4
        cmp       ecx, eax                                      ;528.4
        jbe       .B19.43       ; Prob 10%                      ;528.4
                                ; LOE edx esi edi
.B19.42:                        ; Preds .B19.41
        mov       eax, DWORD PTR [8+ebp]                        ;529.7
        mov       ecx, DWORD PTR [eax]                          ;529.7
        lea       eax, DWORD PTR [edx+esi]                      ;529.7
        lea       ebx, DWORD PTR [ecx+ecx*4]                    ;529.7
        sub       eax, DWORD PTR [32+edi+ebx*8]                 ;529.7
        imul      eax, DWORD PTR [28+edi+ebx*8]                 ;529.7
        mov       edx, DWORD PTR [edi+ebx*8]                    ;529.7
        mov       DWORD PTR [edx+eax], 0                        ;529.7
                                ; LOE esi edi
.B19.43:                        ; Preds .B19.41 .B19.42
        mov       eax, DWORD PTR [12+ebp]                       ;532.30
        mov       ecx, DWORD PTR [eax]                          ;532.30
                                ; LOE ecx esi edi
.B19.44:                        ; Preds .B19.43 .B19.36
        mov       eax, DWORD PTR [8+ebp]                        ;532.4
        test      esi, esi                                      ;534.15
        mov       edx, DWORD PTR [eax]                          ;532.4
        lea       eax, DWORD PTR [edx+edx*4]                    ;532.4
        mov       WORD PTR [36+edi+eax*8], cx                   ;532.4
        mov       WORD PTR [38+edi+eax*8], si                   ;533.4
        jle       .B19.47       ; Prob 79%                      ;534.15
                                ; LOE eax
.B19.45:                        ; Preds .B19.44
        imul      esi, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP+32], -40 ;535.6
        mov       ebx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP] ;535.6
        lea       edi, DWORD PTR [ebx+eax*8]                    ;535.6
        mov       ebx, DWORD PTR [12+esi+edi]                   ;535.17
        mov       edx, ebx                                      ;535.6
        shr       edx, 1                                        ;535.6
        mov       eax, ebx                                      ;535.6
        and       edx, 1                                        ;535.6
        and       eax, 1                                        ;535.6
        shl       edx, 2                                        ;535.6
        add       eax, eax                                      ;535.6
        or        edx, eax                                      ;535.6
        or        edx, 262144                                   ;535.6
        push      edx                                           ;535.6
        push      DWORD PTR [esi+edi]                           ;535.6
        call      _for_dealloc_allocatable                      ;535.6
                                ; LOE ebx esi edi
.B19.65:                        ; Preds .B19.45
        add       esp, 8                                        ;535.6
                                ; LOE ebx esi edi
.B19.46:                        ; Preds .B19.65
        and       ebx, -2                                       ;535.6
        mov       DWORD PTR [esi+edi], 0                        ;535.6
        mov       DWORD PTR [12+esi+edi], ebx                   ;535.6
                                ; LOE
.B19.47:                        ; Preds .B19.44 .B19.46
        add       esp, 100                                      ;538.1
        pop       ebx                                           ;538.1
        pop       edi                                           ;538.1
        pop       esi                                           ;538.1
        mov       esp, ebp                                      ;538.1
        pop       ebp                                           ;538.1
        ret                                                     ;538.1
                                ; LOE
.B19.48:                        ; Preds .B19.11                 ; Infreq
        mov       edx, 1                                        ;
        jmp       .B19.15       ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B19.49:                        ; Preds .B19.21                 ; Infreq
        mov       DWORD PTR [32+esp], 0                         ;507.6
        lea       edx, DWORD PTR [32+esp]                       ;507.6
        mov       DWORD PTR [esp], 37                           ;507.6
        lea       eax, DWORD PTR [esp]                          ;507.6
        mov       DWORD PTR [4+esp], OFFSET FLAT: __STRLITPACK_8 ;507.6
        push      32                                            ;507.6
        push      eax                                           ;507.6
        push      OFFSET FLAT: __STRLITPACK_86.0.19             ;507.6
        push      -2088435968                                   ;507.6
        push      911                                           ;507.6
        push      edx                                           ;507.6
        call      _for_write_seq_lis                            ;507.6
                                ; LOE esi
.B19.50:                        ; Preds .B19.49                 ; Infreq
        push      32                                            ;508.6
        xor       eax, eax                                      ;508.6
        push      eax                                           ;508.6
        push      eax                                           ;508.6
        push      -2088435968                                   ;508.6
        push      eax                                           ;508.6
        push      OFFSET FLAT: __STRLITPACK_87                  ;508.6
        call      _for_stop_core                                ;508.6
                                ; LOE esi
.B19.66:                        ; Preds .B19.50                 ; Infreq
        add       esp, 48                                       ;508.6
        jmp       .B19.22       ; Prob 100%                     ;508.6
                                ; LOE esi
.B19.51:                        ; Preds .B19.37                 ; Infreq
        mov       edx, 1                                        ;
        jmp       .B19.41       ; Prob 100%                     ;
                                ; LOE eax edx ecx esi edi
.B19.52:                        ; Preds .B19.30                 ; Infreq
        mov       ebx, 1                                        ;
        jmp       .B19.34       ; Prob 100%                     ;
                                ; LOE eax edx ebx esi edi
.B19.53:                        ; Preds .B19.29                 ; Infreq
        imul      eax, ebx, -40                                 ;
        add       edi, eax                                      ;
        jmp       .B19.36       ; Prob 100%                     ;
        ALIGN     16
                                ; LOE esi edi
; mark_end;
_DYNUST_LINK_VEH_LIST_MODULE_mp_TCHAINATT_SETUP ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_LINK_VEH_LIST_MODULE_mp_TCHAINATT_SETUP
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_LINK_VEH_LIST_MODULE_mp_TRIPCHAIN_INSERT
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_LINK_VEH_LIST_MODULE_mp_TRIPCHAIN_INSERT
_DYNUST_LINK_VEH_LIST_MODULE_mp_TRIPCHAIN_INSERT	PROC NEAR 
; parameter 1: 8 + ebp
; parameter 2: 12 + ebp
.B20.1:                         ; Preds .B20.0
        push      ebp                                           ;542.12
        mov       ebp, esp                                      ;542.12
        and       esp, -16                                      ;542.12
        push      esi                                           ;542.12
        push      edi                                           ;542.12
        push      ebx                                           ;542.12
        sub       esp, 116                                      ;542.12
        mov       edx, DWORD PTR [8+ebp]                        ;542.12
        mov       edi, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TRIPCHAINLIST] ;545.6
        mov       eax, DWORD PTR [edx]                          ;545.10
        mov       edx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TRIPCHAINLIST+32] ;545.6
        lea       ecx, DWORD PTR [eax+eax*4]                    ;545.35
        lea       eax, DWORD PTR [edx+edx*4]                    ;545.35
        shl       eax, 3                                        ;545.35
        lea       ebx, DWORD PTR [ecx*8]                        ;545.35
        neg       eax                                           ;545.35
        lea       ecx, DWORD PTR [edi+ecx*8]                    ;545.35
        movsx     esi, WORD PTR [36+eax+ecx]                    ;545.35
        movsx     eax, WORD PTR [38+eax+ecx]                    ;545.35
        cmp       eax, esi                                      ;545.35
        jne       .B20.47       ; Prob 50%                      ;545.35
                                ; LOE edx ebx esi edi
.B20.2:                         ; Preds .B20.1
        mov       eax, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_H_INCREASESIZE] ;546.15
        add       eax, esi                                      ;546.5
        mov       DWORD PTR [88+esp], eax                       ;546.5
        test      esi, esi                                      ;547.13
        jg        .B20.4        ; Prob 21%                      ;547.13
                                ; LOE esi
.B20.3:                         ; Preds .B20.2
        xor       esi, esi                                      ;547.13
        jmp       .B20.23       ; Prob 100%                     ;547.13
                                ; LOE esi
.B20.4:                         ; Preds .B20.2
        mov       eax, 0                                        ;547.13
        mov       edx, esi                                      ;547.13
        cmovl     edx, eax                                      ;547.13
        lea       ecx, DWORD PTR [12+esp]                       ;547.13
        push      4                                             ;547.13
        push      edx                                           ;547.13
        push      2                                             ;547.13
        push      ecx                                           ;547.13
        call      _for_check_mult_overflow                      ;547.13
                                ; LOE eax esi
.B20.5:                         ; Preds .B20.4
        mov       edx, DWORD PTR [8+ebp]                        ;547.13
        and       eax, 1                                        ;547.13
        mov       ecx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP+32] ;547.13
        neg       ecx                                           ;547.13
        add       ecx, DWORD PTR [edx]                          ;547.13
        mov       ebx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP] ;547.13
        shl       eax, 4                                        ;547.13
        or        eax, 262145                                   ;547.13
        lea       edi, DWORD PTR [ecx+ecx*4]                    ;547.13
        push      eax                                           ;547.13
        lea       edx, DWORD PTR [ebx+edi*8]                    ;547.13
        push      edx                                           ;547.13
        push      DWORD PTR [36+esp]                            ;547.13
        call      _for_allocate                                 ;547.13
                                ; LOE eax esi
.B20.57:                        ; Preds .B20.5
        add       esp, 28                                       ;547.13
        mov       ebx, eax                                      ;547.13
                                ; LOE ebx esi
.B20.6:                         ; Preds .B20.57
        test      ebx, ebx                                      ;547.13
        je        .B20.9        ; Prob 50%                      ;547.13
                                ; LOE ebx esi
.B20.7:                         ; Preds .B20.6
        mov       DWORD PTR [32+esp], 0                         ;547.13
        lea       edx, DWORD PTR [32+esp]                       ;547.13
        mov       DWORD PTR [16+esp], 25                        ;547.13
        lea       eax, DWORD PTR [16+esp]                       ;547.13
        mov       DWORD PTR [20+esp], OFFSET FLAT: __STRLITPACK_10 ;547.13
        push      32                                            ;547.13
        push      eax                                           ;547.13
        push      OFFSET FLAT: __STRLITPACK_84.0.19             ;547.13
        push      -2088435968                                   ;547.13
        push      911                                           ;547.13
        push      edx                                           ;547.13
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_VECTORERROR], ebx ;547.13
        call      _for_write_seq_lis                            ;547.13
                                ; LOE esi
.B20.8:                         ; Preds .B20.7
        push      32                                            ;547.13
        xor       eax, eax                                      ;547.13
        push      eax                                           ;547.13
        push      eax                                           ;547.13
        push      -2088435968                                   ;547.13
        push      eax                                           ;547.13
        push      OFFSET FLAT: __STRLITPACK_85                  ;547.13
        call      _for_stop_core                                ;547.13
                                ; LOE esi
.B20.58:                        ; Preds .B20.8
        add       esp, 48                                       ;547.13
        jmp       .B20.11       ; Prob 100%                     ;547.13
                                ; LOE esi
.B20.9:                         ; Preds .B20.6
        mov       edi, DWORD PTR [8+ebp]                        ;547.13
        mov       DWORD PTR [esp], ebx                          ;
        imul      ecx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP+32], -40 ;547.13
        mov       ebx, DWORD PTR [edi]                          ;547.13
        mov       edi, 1                                        ;547.13
        mov       eax, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP] ;547.13
        mov       DWORD PTR [108+esp], esi                      ;
        lea       esi, DWORD PTR [ebx+ebx*4]                    ;547.13
        mov       ebx, 4                                        ;547.13
        lea       edx, DWORD PTR [eax+esi*8]                    ;547.13
        xor       eax, eax                                      ;547.13
        mov       DWORD PTR [16+ecx+edx], edi                   ;547.13
        mov       DWORD PTR [32+ecx+edx], edi                   ;547.13
        imul      edi, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TRIPCHAINLIST+32], -40 ;547.13
        add       edi, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TRIPCHAINLIST] ;547.13
        mov       DWORD PTR [12+ecx+edx], 5                     ;547.13
        mov       DWORD PTR [4+ecx+edx], ebx                    ;547.13
        movsx     esi, WORD PTR [36+edi+esi*8]                  ;547.13
        test      esi, esi                                      ;547.13
        mov       DWORD PTR [8+ecx+edx], eax                    ;547.13
        cmovl     esi, eax                                      ;547.13
        mov       DWORD PTR [24+ecx+edx], esi                   ;547.13
        mov       DWORD PTR [28+ecx+edx], ebx                   ;547.13
        lea       edx, DWORD PTR [8+esp]                        ;547.13
        push      ebx                                           ;547.13
        push      esi                                           ;547.13
        push      2                                             ;547.13
        push      edx                                           ;547.13
        mov       ebx, DWORD PTR [16+esp]                       ;547.13
        mov       esi, DWORD PTR [124+esp]                      ;547.13
        call      _for_check_mult_overflow                      ;547.13
                                ; LOE ebx esi bl bh
.B20.59:                        ; Preds .B20.9
        add       esp, 16                                       ;547.13
                                ; LOE ebx esi bl bh
.B20.10:                        ; Preds .B20.59
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_VECTORERROR], ebx ;547.13
                                ; LOE esi
.B20.11:                        ; Preds .B20.58 .B20.10
        mov       eax, DWORD PTR [8+ebp]                        ;547.13
        mov       edx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TRIPCHAINLIST+32] ;547.13
        mov       ebx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TRIPCHAINLIST] ;547.13
        mov       eax, DWORD PTR [eax]                          ;547.13
        lea       edx, DWORD PTR [edx+edx*4]                    ;547.13
        shl       edx, 3                                        ;547.13
        lea       ecx, DWORD PTR [eax+eax*4]                    ;547.13
        lea       edi, DWORD PTR [ebx+ecx*8]                    ;547.13
        sub       edi, edx                                      ;547.13
        movsx     ecx, WORD PTR [36+edi]                        ;547.13
        test      ecx, ecx                                      ;547.13
        mov       DWORD PTR [24+esp], ecx                       ;547.13
        jle       .B20.19       ; Prob 50%                      ;547.13
                                ; LOE eax edx ebx esi
.B20.12:                        ; Preds .B20.11
        mov       ecx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP+32] ;547.13
        sub       ebx, edx                                      ;
        mov       edi, DWORD PTR [24+esp]                       ;547.13
        mov       DWORD PTR [4+esp], ecx                        ;547.13
        mov       ecx, edi                                      ;547.13
        shr       ecx, 31                                       ;547.13
        add       ecx, edi                                      ;547.13
        sar       ecx, 1                                        ;547.13
        mov       DWORD PTR [64+esp], ecx                       ;547.13
        test      ecx, ecx                                      ;547.13
        mov       ecx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP] ;547.13
        jbe       .B20.48       ; Prob 3%                       ;547.13
                                ; LOE eax ecx ebx esi
.B20.13:                        ; Preds .B20.12
        imul      edi, DWORD PTR [4+esp], -40                   ;
        xor       edx, edx                                      ;
        add       edi, ecx                                      ;
        mov       DWORD PTR [28+esp], edi                       ;
        mov       DWORD PTR [68+esp], ebx                       ;
        mov       DWORD PTR [esp], ecx                          ;
        mov       DWORD PTR [108+esp], esi                      ;
                                ; LOE eax edx
.B20.14:                        ; Preds .B20.67 .B20.13
        mov       esi, DWORD PTR [68+esp]                       ;547.13
        lea       edi, DWORD PTR [eax+eax*4]                    ;547.13
        lea       ebx, DWORD PTR [1+edx+edx]                    ;547.13
        mov       eax, DWORD PTR [32+esi+edi*8]                 ;547.13
        neg       eax                                           ;547.13
        add       eax, ebx                                      ;547.13
        imul      eax, DWORD PTR [28+esi+edi*8]                 ;547.13
        mov       ecx, DWORD PTR [esi+edi*8]                    ;547.13
        mov       esi, DWORD PTR [28+esp]                       ;547.13
        mov       ecx, DWORD PTR [ecx+eax]                      ;547.13
        sub       ebx, DWORD PTR [32+esi+edi*8]                 ;547.13
        imul      ebx, DWORD PTR [28+esi+edi*8]                 ;547.13
        mov       edi, DWORD PTR [esi+edi*8]                    ;547.13
        mov       eax, DWORD PTR [8+ebp]                        ;547.13
        mov       DWORD PTR [edi+ebx], ecx                      ;547.13
        lea       ecx, DWORD PTR [2+edx+edx]                    ;547.13
        mov       ebx, DWORD PTR [eax]                          ;547.13
        inc       edx                                           ;547.13
        mov       edi, DWORD PTR [68+esp]                       ;547.13
        lea       ebx, DWORD PTR [ebx+ebx*4]                    ;547.13
        mov       eax, DWORD PTR [32+edi+ebx*8]                 ;547.13
        neg       eax                                           ;547.13
        add       eax, ecx                                      ;547.13
        imul      eax, DWORD PTR [28+edi+ebx*8]                 ;547.13
        sub       ecx, DWORD PTR [32+esi+ebx*8]                 ;547.13
        imul      ecx, DWORD PTR [28+esi+ebx*8]                 ;547.13
        mov       edi, DWORD PTR [edi+ebx*8]                    ;547.13
        mov       ebx, DWORD PTR [esi+ebx*8]                    ;547.13
        cmp       edx, DWORD PTR [64+esp]                       ;547.13
        mov       esi, DWORD PTR [edi+eax]                      ;547.13
        mov       DWORD PTR [ebx+ecx], esi                      ;547.13
        jae       .B20.15       ; Prob 36%                      ;547.13
                                ; LOE edx
.B20.67:                        ; Preds .B20.14
        mov       eax, DWORD PTR [8+ebp]                        ;545.10
        mov       eax, DWORD PTR [eax]                          ;545.10
        jmp       .B20.14       ; Prob 100%                     ;545.10
                                ; LOE eax edx
.B20.15:                        ; Preds .B20.14
        mov       eax, DWORD PTR [8+ebp]                        ;547.13
        lea       edx, DWORD PTR [1+edx+edx]                    ;547.13
        mov       ebx, DWORD PTR [68+esp]                       ;
        mov       ecx, DWORD PTR [esp]                          ;
        mov       esi, DWORD PTR [108+esp]                      ;
        mov       eax, DWORD PTR [eax]                          ;547.13
                                ; LOE eax edx ecx ebx esi
.B20.16:                        ; Preds .B20.15 .B20.48
        lea       edi, DWORD PTR [-1+edx]                       ;547.13
        cmp       edi, DWORD PTR [24+esp]                       ;547.13
        jae       .B20.20       ; Prob 3%                       ;547.13
                                ; LOE eax edx ecx ebx esi
.B20.17:                        ; Preds .B20.16
        imul      edi, DWORD PTR [4+esp], -40                   ;547.13
        lea       eax, DWORD PTR [eax+eax*4]                    ;547.13
        add       ecx, edi                                      ;547.13
        mov       edi, DWORD PTR [32+ebx+eax*8]                 ;547.13
        neg       edi                                           ;547.13
        add       edi, edx                                      ;547.13
        imul      edi, DWORD PTR [28+ebx+eax*8]                 ;547.13
        sub       edx, DWORD PTR [32+ecx+eax*8]                 ;547.13
        imul      edx, DWORD PTR [28+ecx+eax*8]                 ;547.13
        mov       DWORD PTR [68+esp], ebx                       ;
        mov       ebx, DWORD PTR [ebx+eax*8]                    ;547.13
        mov       eax, DWORD PTR [ecx+eax*8]                    ;547.13
        mov       ecx, DWORD PTR [ebx+edi]                      ;547.13
        mov       DWORD PTR [eax+edx], ecx                      ;547.13
        mov       edx, DWORD PTR [8+ebp]                        ;547.13
        mov       ebx, DWORD PTR [68+esp]                       ;547.13
        mov       eax, DWORD PTR [edx]                          ;547.13
        jmp       .B20.20       ; Prob 100%                     ;547.13
                                ; LOE eax ebx esi
.B20.19:                        ; Preds .B20.11
        sub       ebx, edx                                      ;
                                ; LOE eax ebx esi
.B20.20:                        ; Preds .B20.16 .B20.17 .B20.19
        lea       eax, DWORD PTR [eax+eax*4]                    ;547.13
        mov       DWORD PTR [esp], eax                          ;547.13
        mov       edx, DWORD PTR [12+ebx+eax*8]                 ;547.13
        test      dl, 1                                         ;547.13
        mov       DWORD PTR [4+esp], edx                        ;547.13
        je        .B20.23       ; Prob 60%                      ;547.13
                                ; LOE edx ebx esi dl dh
.B20.21:                        ; Preds .B20.20
        mov       eax, edx                                      ;547.13
        mov       edx, eax                                      ;547.13
        shr       edx, 1                                        ;547.13
        and       eax, 1                                        ;547.13
        and       edx, 1                                        ;547.13
        add       eax, eax                                      ;547.13
        shl       edx, 2                                        ;547.13
        or        edx, 1                                        ;547.13
        or        edx, eax                                      ;547.13
        or        edx, 262144                                   ;547.13
        push      edx                                           ;547.13
        mov       ecx, DWORD PTR [4+esp]                        ;547.13
        push      DWORD PTR [ebx+ecx*8]                         ;547.13
        call      _for_dealloc_allocatable                      ;547.13
                                ; LOE eax ebx esi
.B20.60:                        ; Preds .B20.21
        add       esp, 8                                        ;547.13
                                ; LOE eax ebx esi
.B20.22:                        ; Preds .B20.60
        mov       ecx, DWORD PTR [esp]                          ;547.13
        mov       edx, DWORD PTR [4+esp]                        ;547.13
        and       edx, -2                                       ;547.13
        mov       DWORD PTR [ebx+ecx*8], 0                      ;547.13
        test      eax, eax                                      ;547.13
        mov       DWORD PTR [12+ebx+ecx*8], edx                 ;547.13
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_VECTORERROR], eax ;547.13
        jne       .B20.49       ; Prob 5%                       ;547.13
                                ; LOE esi
.B20.23:                        ; Preds .B20.66 .B20.22 .B20.20 .B20.3
        mov       eax, DWORD PTR [88+esp]                       ;547.13
        xor       ebx, ebx                                      ;547.13
        test      eax, eax                                      ;547.13
        lea       edx, DWORD PTR [92+esp]                       ;547.13
        push      4                                             ;547.13
        cmovg     ebx, eax                                      ;547.13
        push      ebx                                           ;547.13
        push      2                                             ;547.13
        push      edx                                           ;547.13
        call      _for_check_mult_overflow                      ;547.13
                                ; LOE eax ebx esi
.B20.24:                        ; Preds .B20.23
        mov       edx, DWORD PTR [8+ebp]                        ;547.13
        and       eax, 1                                        ;547.13
        mov       ecx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TRIPCHAINLIST+32] ;547.13
        neg       ecx                                           ;547.13
        add       ecx, DWORD PTR [edx]                          ;547.13
        mov       edi, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TRIPCHAINLIST] ;547.13
        shl       eax, 4                                        ;547.13
        or        eax, 262145                                   ;547.13
        lea       edx, DWORD PTR [ecx+ecx*4]                    ;547.13
        push      eax                                           ;547.13
        lea       ecx, DWORD PTR [edi+edx*8]                    ;547.13
        push      ecx                                           ;547.13
        push      DWORD PTR [116+esp]                           ;547.13
        call      _for_allocate                                 ;547.13
                                ; LOE eax ebx esi
.B20.62:                        ; Preds .B20.24
        add       esp, 28                                       ;547.13
        mov       edi, eax                                      ;547.13
                                ; LOE ebx esi edi
.B20.25:                        ; Preds .B20.62
        test      edi, edi                                      ;547.13
        je        .B20.28       ; Prob 50%                      ;547.13
                                ; LOE ebx esi edi
.B20.26:                        ; Preds .B20.25
        mov       DWORD PTR [32+esp], 0                         ;547.13
        lea       edx, DWORD PTR [32+esp]                       ;547.13
        mov       DWORD PTR [80+esp], 35                        ;547.13
        lea       eax, DWORD PTR [80+esp]                       ;547.13
        mov       DWORD PTR [84+esp], OFFSET FLAT: __STRLITPACK_6 ;547.13
        push      32                                            ;547.13
        push      eax                                           ;547.13
        push      OFFSET FLAT: __STRLITPACK_88.0.19             ;547.13
        push      -2088435968                                   ;547.13
        push      911                                           ;547.13
        push      edx                                           ;547.13
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_VECTORERROR], edi ;547.13
        call      _for_write_seq_lis                            ;547.13
                                ; LOE esi
.B20.27:                        ; Preds .B20.26
        push      32                                            ;547.13
        xor       eax, eax                                      ;547.13
        push      eax                                           ;547.13
        push      eax                                           ;547.13
        push      -2088435968                                   ;547.13
        push      eax                                           ;547.13
        push      OFFSET FLAT: __STRLITPACK_89                  ;547.13
        call      _for_stop_core                                ;547.13
                                ; LOE esi
.B20.63:                        ; Preds .B20.27
        add       esp, 48                                       ;547.13
        jmp       .B20.30       ; Prob 100%                     ;547.13
                                ; LOE esi
.B20.28:                        ; Preds .B20.25
        mov       edx, DWORD PTR [8+ebp]                        ;547.13
        mov       eax, 4                                        ;547.13
        mov       DWORD PTR [24+esp], edi                       ;
        mov       edi, 1                                        ;547.13
        mov       ecx, DWORD PTR [edx]                          ;547.13
        sub       ecx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TRIPCHAINLIST+32] ;547.13
        mov       edx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TRIPCHAINLIST] ;547.13
        lea       ecx, DWORD PTR [ecx+ecx*4]                    ;547.13
        mov       DWORD PTR [16+edx+ecx*8], edi                 ;547.13
        mov       DWORD PTR [32+edx+ecx*8], edi                 ;547.13
        lea       edi, DWORD PTR [72+esp]                       ;547.13
        push      eax                                           ;547.13
        push      ebx                                           ;547.13
        push      2                                             ;547.13
        push      edi                                           ;547.13
        mov       DWORD PTR [12+edx+ecx*8], 5                   ;547.13
        mov       DWORD PTR [4+edx+ecx*8], eax                  ;547.13
        mov       DWORD PTR [8+edx+ecx*8], 0                    ;547.13
        mov       DWORD PTR [24+edx+ecx*8], ebx                 ;547.13
        mov       DWORD PTR [28+edx+ecx*8], eax                 ;547.13
        mov       edi, DWORD PTR [40+esp]                       ;547.13
        call      _for_check_mult_overflow                      ;547.13
                                ; LOE esi edi
.B20.64:                        ; Preds .B20.28
        add       esp, 16                                       ;547.13
                                ; LOE esi edi
.B20.29:                        ; Preds .B20.64
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_VECTORERROR], edi ;547.13
                                ; LOE esi
.B20.30:                        ; Preds .B20.63 .B20.29
        mov       edi, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TRIPCHAINLIST] ;547.13
        test      esi, esi                                      ;547.13
        mov       edx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TRIPCHAINLIST+32] ;547.13
        jle       .B20.53       ; Prob 16%                      ;547.13
                                ; LOE edx esi edi
.B20.31:                        ; Preds .B20.30
        mov       eax, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP] ;547.13
        mov       ebx, esi                                      ;547.13
        mov       DWORD PTR [28+esp], eax                       ;547.13
        imul      eax, edx, -40                                 ;
        shr       ebx, 31                                       ;547.13
        add       eax, edi                                      ;
        add       ebx, esi                                      ;547.13
        sar       ebx, 1                                        ;547.13
        mov       ecx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP+32] ;547.13
        test      ebx, ebx                                      ;547.13
        mov       DWORD PTR [24+esp], ecx                       ;547.13
        mov       DWORD PTR [100+esp], ebx                      ;547.13
        jbe       .B20.52       ; Prob 0%                       ;547.13
                                ; LOE eax edx esi edi
.B20.32:                        ; Preds .B20.31
        imul      ebx, DWORD PTR [24+esp], -40                  ;
        xor       ecx, ecx                                      ;
        add       ebx, DWORD PTR [28+esp]                       ;
        mov       DWORD PTR [96+esp], ebx                       ;
        mov       DWORD PTR [68+esp], eax                       ;
        mov       DWORD PTR [108+esp], esi                      ;
        mov       DWORD PTR [76+esp], edx                       ;
        mov       DWORD PTR [64+esp], edi                       ;
                                ; LOE ecx
.B20.33:                        ; Preds .B20.33 .B20.32
        mov       ebx, DWORD PTR [8+ebp]                        ;547.13
        lea       edx, DWORD PTR [1+ecx+ecx]                    ;547.13
        mov       eax, DWORD PTR [96+esp]                       ;547.13
        mov       DWORD PTR [104+esp], ecx                      ;
        mov       esi, DWORD PTR [ebx]                          ;547.13
        lea       edi, DWORD PTR [esi+esi*4]                    ;547.13
        mov       esi, DWORD PTR [68+esp]                       ;547.13
        mov       ecx, DWORD PTR [32+eax+edi*8]                 ;547.13
        neg       ecx                                           ;547.13
        add       ecx, edx                                      ;547.13
        imul      ecx, DWORD PTR [28+eax+edi*8]                 ;547.13
        sub       edx, DWORD PTR [32+esi+edi*8]                 ;547.13
        imul      edx, DWORD PTR [28+esi+edi*8]                 ;547.13
        mov       eax, DWORD PTR [eax+edi*8]                    ;547.13
        mov       edi, DWORD PTR [esi+edi*8]                    ;547.13
        mov       ecx, DWORD PTR [eax+ecx]                      ;547.13
        mov       DWORD PTR [edi+edx], ecx                      ;547.13
        mov       ebx, DWORD PTR [ebx]                          ;547.13
        mov       edi, DWORD PTR [96+esp]                       ;547.13
        mov       ecx, DWORD PTR [104+esp]                      ;547.13
        lea       edx, DWORD PTR [ebx+ebx*4]                    ;547.13
        mov       ebx, DWORD PTR [32+edi+edx*8]                 ;547.13
        neg       ebx                                           ;547.13
        lea       eax, DWORD PTR [2+ecx+ecx]                    ;547.13
        add       ebx, eax                                      ;547.13
        inc       ecx                                           ;547.13
        imul      ebx, DWORD PTR [28+edi+edx*8]                 ;547.13
        sub       eax, DWORD PTR [32+esi+edx*8]                 ;547.13
        imul      eax, DWORD PTR [28+esi+edx*8]                 ;547.13
        mov       edi, DWORD PTR [edi+edx*8]                    ;547.13
        mov       esi, DWORD PTR [esi+edx*8]                    ;547.13
        cmp       ecx, DWORD PTR [100+esp]                      ;547.13
        mov       edx, DWORD PTR [edi+ebx]                      ;547.13
        mov       DWORD PTR [esi+eax], edx                      ;547.13
        jb        .B20.33       ; Prob 64%                      ;547.13
                                ; LOE ecx
.B20.34:                        ; Preds .B20.33
        mov       eax, DWORD PTR [68+esp]                       ;
        lea       ecx, DWORD PTR [1+ecx+ecx]                    ;547.13
        mov       esi, DWORD PTR [108+esp]                      ;
        mov       edx, DWORD PTR [76+esp]                       ;
        mov       edi, DWORD PTR [64+esp]                       ;
                                ; LOE eax edx ecx esi edi
.B20.35:                        ; Preds .B20.34 .B20.52
        lea       ebx, DWORD PTR [-1+ecx]                       ;547.13
        cmp       esi, ebx                                      ;547.13
        jbe       .B20.37       ; Prob 0%                       ;547.13
                                ; LOE eax edx ecx esi edi
.B20.36:                        ; Preds .B20.35
        mov       ebx, DWORD PTR [8+ebp]                        ;547.13
        mov       DWORD PTR [76+esp], edx                       ;
        mov       DWORD PTR [108+esp], esi                      ;
        mov       edx, DWORD PTR [ebx]                          ;547.13
        imul      ebx, DWORD PTR [24+esp], -40                  ;547.13
        mov       esi, DWORD PTR [28+esp]                       ;547.13
        lea       edx, DWORD PTR [edx+edx*4]                    ;547.13
        add       esi, ebx                                      ;547.13
        mov       ebx, DWORD PTR [32+esi+edx*8]                 ;547.13
        neg       ebx                                           ;547.13
        add       ebx, ecx                                      ;547.13
        imul      ebx, DWORD PTR [28+esi+edx*8]                 ;547.13
        sub       ecx, DWORD PTR [32+eax+edx*8]                 ;547.13
        imul      ecx, DWORD PTR [28+eax+edx*8]                 ;547.13
        mov       esi, DWORD PTR [esi+edx*8]                    ;547.13
        mov       edx, DWORD PTR [eax+edx*8]                    ;547.13
        mov       esi, DWORD PTR [esi+ebx]                      ;547.13
        mov       DWORD PTR [edx+ecx], esi                      ;547.13
        mov       esi, DWORD PTR [108+esp]                      ;547.13
        mov       edx, DWORD PTR [76+esp]                       ;547.13
                                ; LOE eax edx esi edi
.B20.37:                        ; Preds .B20.36 .B20.35 .B20.53
        cmp       esi, DWORD PTR [88+esp]                       ;547.13
        lea       ecx, DWORD PTR [1+esi]                        ;547.13
        mov       DWORD PTR [24+esp], ecx                       ;547.13
        jge       .B20.44       ; Prob 50%                      ;547.13
                                ; LOE eax edx ecx esi edi cl ch
.B20.38:                        ; Preds .B20.37
        neg       ecx                                           ;547.13
        add       ecx, DWORD PTR [88+esp]                       ;547.13
        inc       ecx                                           ;547.13
        mov       ebx, ecx                                      ;547.13
        shr       ebx, 31                                       ;547.13
        add       ebx, ecx                                      ;547.13
        sar       ebx, 1                                        ;547.13
        mov       DWORD PTR [24+esp], ecx                       ;547.13
        test      ebx, ebx                                      ;547.13
        jbe       .B20.51       ; Prob 10%                      ;547.13
                                ; LOE eax edx ebx esi edi
.B20.39:                        ; Preds .B20.38
        mov       DWORD PTR [76+esp], edx                       ;
        xor       ecx, ecx                                      ;
        mov       DWORD PTR [28+esp], ebx                       ;
        mov       DWORD PTR [108+esp], esi                      ;
        mov       DWORD PTR [64+esp], edi                       ;
        mov       edx, DWORD PTR [8+ebp]                        ;
                                ; LOE eax edx ecx
.B20.40:                        ; Preds .B20.40 .B20.39
        mov       ebx, DWORD PTR [edx]                          ;547.13
        mov       esi, DWORD PTR [108+esp]                      ;547.13
        mov       DWORD PTR [68+esp], ecx                       ;
        lea       edi, DWORD PTR [ebx+ebx*4]                    ;547.13
        lea       ebx, DWORD PTR [1+esi+ecx*2]                  ;547.13
        mov       esi, DWORD PTR [32+eax+edi*8]                 ;547.13
        neg       esi                                           ;547.13
        xor       ecx, ecx                                      ;547.13
        add       esi, ebx                                      ;547.13
        inc       ebx                                           ;547.13
        imul      esi, DWORD PTR [28+eax+edi*8]                 ;547.13
        mov       edi, DWORD PTR [eax+edi*8]                    ;547.13
        mov       DWORD PTR [edi+esi], ecx                      ;547.13
        mov       esi, DWORD PTR [edx]                          ;547.13
        lea       edi, DWORD PTR [esi+esi*4]                    ;547.13
        sub       ebx, DWORD PTR [32+eax+edi*8]                 ;547.13
        imul      ebx, DWORD PTR [28+eax+edi*8]                 ;547.13
        mov       esi, DWORD PTR [eax+edi*8]                    ;547.13
        mov       DWORD PTR [esi+ebx], ecx                      ;547.13
        mov       ecx, DWORD PTR [68+esp]                       ;547.13
        inc       ecx                                           ;547.13
        cmp       ecx, DWORD PTR [28+esp]                       ;547.13
        jb        .B20.40       ; Prob 63%                      ;547.13
                                ; LOE eax edx ecx
.B20.41:                        ; Preds .B20.40
        mov       esi, DWORD PTR [108+esp]                      ;
        lea       ebx, DWORD PTR [1+ecx+ecx]                    ;547.13
        mov       edx, DWORD PTR [76+esp]                       ;
        mov       edi, DWORD PTR [64+esp]                       ;
                                ; LOE eax edx ebx esi edi
.B20.42:                        ; Preds .B20.41 .B20.51
        lea       ecx, DWORD PTR [-1+ebx]                       ;547.13
        cmp       ecx, DWORD PTR [24+esp]                       ;547.13
        jae       .B20.44       ; Prob 10%                      ;547.13
                                ; LOE eax edx ebx esi edi
.B20.43:                        ; Preds .B20.42
        mov       ecx, DWORD PTR [8+ebp]                        ;547.13
        add       ebx, esi                                      ;547.13
        mov       ecx, DWORD PTR [ecx]                          ;547.13
        lea       ecx, DWORD PTR [ecx+ecx*4]                    ;547.13
        sub       ebx, DWORD PTR [32+eax+ecx*8]                 ;547.13
        imul      ebx, DWORD PTR [28+eax+ecx*8]                 ;547.13
        mov       ecx, DWORD PTR [eax+ecx*8]                    ;547.13
        mov       DWORD PTR [ecx+ebx], 0                        ;547.13
                                ; LOE eax edx esi edi
.B20.44:                        ; Preds .B20.42 .B20.37 .B20.43
        mov       ebx, DWORD PTR [8+ebp]                        ;547.13
        test      esi, esi                                      ;547.13
        mov       ecx, DWORD PTR [ebx]                          ;547.13
        lea       ebx, DWORD PTR [ecx+ecx*4]                    ;547.13
        mov       DWORD PTR [96+esp], ebx                       ;547.13
        mov       ecx, DWORD PTR [88+esp]                       ;547.13
        lea       ebx, DWORD PTR [ebx*8]                        ;547.13
        mov       WORD PTR [36+eax+ebx], cx                     ;547.13
        mov       WORD PTR [38+eax+ebx], si                     ;547.13
        jle       .B20.47       ; Prob 79%                      ;547.13
                                ; LOE edx ebx edi
.B20.45:                        ; Preds .B20.44
        imul      ecx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP+32], -40 ;547.13
        mov       esi, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP] ;547.13
        mov       eax, DWORD PTR [96+esp]                       ;547.13
        mov       DWORD PTR [68+esp], ebx                       ;
        mov       DWORD PTR [28+esp], ecx                       ;547.13
        lea       ebx, DWORD PTR [esi+eax*8]                    ;547.13
        mov       DWORD PTR [64+esp], ebx                       ;547.13
        mov       esi, DWORD PTR [12+ecx+ebx]                   ;547.13
        mov       eax, esi                                      ;547.13
        shr       eax, 1                                        ;547.13
        mov       DWORD PTR [24+esp], esi                       ;547.13
        and       eax, 1                                        ;547.13
        and       esi, 1                                        ;547.13
        shl       eax, 2                                        ;547.13
        add       esi, esi                                      ;547.13
        or        eax, esi                                      ;547.13
        or        eax, 262144                                   ;547.13
        push      eax                                           ;547.13
        push      DWORD PTR [ecx+ebx]                           ;547.13
        mov       DWORD PTR [84+esp], edx                       ;547.13
        mov       ebx, DWORD PTR [76+esp]                       ;547.13
        call      _for_dealloc_allocatable                      ;547.13
                                ; LOE ebx edi bl bh
.B20.65:                        ; Preds .B20.45
        mov       edx, DWORD PTR [84+esp]                       ;
        add       esp, 8                                        ;547.13
                                ; LOE edx ebx edi dl bl dh bh
.B20.46:                        ; Preds .B20.65
        mov       ecx, DWORD PTR [28+esp]                       ;547.13
        mov       esi, DWORD PTR [64+esp]                       ;547.13
        mov       eax, DWORD PTR [24+esp]                       ;547.13
        and       eax, -2                                       ;547.13
        mov       DWORD PTR [ecx+esi], 0                        ;547.13
        mov       DWORD PTR [12+ecx+esi], eax                   ;547.13
                                ; LOE edx ebx edi
.B20.47:                        ; Preds .B20.44 .B20.46 .B20.1
        imul      ecx, edx, -40                                 ;549.6
        add       ebx, edi                                      ;549.6
        movzx     edx, WORD PTR [38+ecx+ebx]                    ;549.33
        inc       edx                                           ;549.58
        movsx     edi, dx                                       ;550.6
        sub       edi, DWORD PTR [32+ecx+ebx]                   ;550.6
        imul      edi, DWORD PTR [28+ecx+ebx]                   ;550.6
        mov       eax, DWORD PTR [12+ebp]                       ;542.12
        mov       esi, DWORD PTR [ecx+ebx]                      ;550.6
        mov       WORD PTR [38+ecx+ebx], dx                     ;549.6
        mov       ebx, DWORD PTR [eax]                          ;550.6
        mov       DWORD PTR [esi+edi], ebx                      ;550.6
        add       esp, 116                                      ;552.1
        pop       ebx                                           ;552.1
        pop       edi                                           ;552.1
        pop       esi                                           ;552.1
        mov       esp, ebp                                      ;552.1
        pop       ebp                                           ;552.1
        ret                                                     ;552.1
                                ; LOE
.B20.48:                        ; Preds .B20.12                 ; Infreq
        mov       edx, 1                                        ;
        jmp       .B20.16       ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B20.49:                        ; Preds .B20.22                 ; Infreq
        mov       DWORD PTR [32+esp], 0                         ;547.13
        lea       edx, DWORD PTR [32+esp]                       ;547.13
        mov       DWORD PTR [esp], 37                           ;547.13
        lea       eax, DWORD PTR [esp]                          ;547.13
        mov       DWORD PTR [4+esp], OFFSET FLAT: __STRLITPACK_8 ;547.13
        push      32                                            ;547.13
        push      eax                                           ;547.13
        push      OFFSET FLAT: __STRLITPACK_86.0.19             ;547.13
        push      -2088435968                                   ;547.13
        push      911                                           ;547.13
        push      edx                                           ;547.13
        call      _for_write_seq_lis                            ;547.13
                                ; LOE esi
.B20.50:                        ; Preds .B20.49                 ; Infreq
        push      32                                            ;547.13
        xor       eax, eax                                      ;547.13
        push      eax                                           ;547.13
        push      eax                                           ;547.13
        push      -2088435968                                   ;547.13
        push      eax                                           ;547.13
        push      OFFSET FLAT: __STRLITPACK_87                  ;547.13
        call      _for_stop_core                                ;547.13
                                ; LOE esi
.B20.66:                        ; Preds .B20.50                 ; Infreq
        add       esp, 48                                       ;547.13
        jmp       .B20.23       ; Prob 100%                     ;547.13
                                ; LOE esi
.B20.51:                        ; Preds .B20.38                 ; Infreq
        mov       ebx, 1                                        ;
        jmp       .B20.42       ; Prob 100%                     ;
                                ; LOE eax edx ebx esi edi
.B20.52:                        ; Preds .B20.31                 ; Infreq
        mov       ecx, 1                                        ;
        jmp       .B20.35       ; Prob 100%                     ;
                                ; LOE eax edx ecx esi edi
.B20.53:                        ; Preds .B20.30                 ; Infreq
        imul      eax, edx, -40                                 ;
        add       eax, edi                                      ;
        jmp       .B20.37       ; Prob 100%                     ;
        ALIGN     16
                                ; LOE eax edx esi edi
; mark_end;
_DYNUST_LINK_VEH_LIST_MODULE_mp_TRIPCHAIN_INSERT ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_LINK_VEH_LIST_MODULE_mp_TRIPCHAIN_INSERT
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_LINK_VEH_LIST_MODULE_mp_TRIPCHAIN_REMOVE
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_LINK_VEH_LIST_MODULE_mp_TRIPCHAIN_REMOVE
_DYNUST_LINK_VEH_LIST_MODULE_mp_TRIPCHAIN_REMOVE	PROC NEAR 
; parameter 1: 8 + ebp
; parameter 2: 12 + ebp
.B21.1:                         ; Preds .B21.0
        push      ebp                                           ;556.12
        mov       ebp, esp                                      ;556.12
        and       esp, -16                                      ;556.12
        push      esi                                           ;556.12
        push      edi                                           ;556.12
        push      ebx                                           ;556.12
        sub       esp, 52                                       ;556.12
        imul      ebx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TRIPCHAINLIST+32], -40 ;560.3
        mov       edx, DWORD PTR [8+ebp]                        ;556.12
        add       ebx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TRIPCHAINLIST] ;560.3
        mov       edx, DWORD PTR [edx]                          ;560.13
        lea       eax, DWORD PTR [edx+edx*4]                    ;560.3
        mov       DWORD PTR [12+esp], eax                       ;560.3
        movsx     edi, WORD PTR [38+ebx+eax*8]                  ;560.13
        test      edi, edi                                      ;560.3
        jle       .B21.6        ; Prob 2%                       ;560.3
                                ; LOE eax edx ebx edi al ah
.B21.2:                         ; Preds .B21.1
        mov       esi, DWORD PTR [12+ebp]                       ;556.12
        mov       DWORD PTR [16+esp], 1                         ;
        mov       DWORD PTR [8+esp], edx                        ;
        mov       ecx, DWORD PTR [esi]                          ;561.9
        mov       DWORD PTR [esp], ecx                          ;561.9
        mov       ecx, eax                                      ;561.9
        mov       DWORD PTR [20+esp], ebx                       ;
        mov       esi, DWORD PTR [28+ebx+ecx*8]                 ;561.9
        mov       eax, DWORD PTR [32+ebx+ecx*8]                 ;561.9
        imul      eax, esi                                      ;
        mov       ecx, DWORD PTR [ebx+ecx*8]                    ;561.9
        sub       ecx, eax                                      ;
        mov       DWORD PTR [4+esp], ecx                        ;
        mov       ecx, DWORD PTR [16+esp]                       ;
        mov       eax, DWORD PTR [4+esp]                        ;
        mov       edx, esi                                      ;
        mov       ebx, DWORD PTR [esp]                          ;
                                ; LOE eax edx ecx ebx esi edi
.B21.3:                         ; Preds .B21.4 .B21.2
        cmp       ebx, DWORD PTR [edx+eax]                      ;561.35
        je        .B21.9        ; Prob 20%                      ;561.35
                                ; LOE eax edx ecx ebx esi edi
.B21.4:                         ; Preds .B21.3
        inc       ecx                                           ;568.3
        add       edx, esi                                      ;568.3
        cmp       ecx, edi                                      ;568.3
        jle       .B21.3        ; Prob 82%                      ;568.3
                                ; LOE eax edx ecx ebx esi edi
.B21.6:                         ; Preds .B21.4 .B21.1
        mov       DWORD PTR [esp], 0                            ;573.5
        lea       edx, DWORD PTR [esp]                          ;573.5
        mov       DWORD PTR [32+esp], 26                        ;573.5
        lea       eax, DWORD PTR [32+esp]                       ;573.5
        mov       DWORD PTR [36+esp], OFFSET FLAT: __STRLITPACK_4 ;573.5
        push      32                                            ;573.5
        push      eax                                           ;573.5
        push      OFFSET FLAT: __STRLITPACK_90.0.21             ;573.5
        push      -2088435968                                   ;573.5
        push      911                                           ;573.5
        push      edx                                           ;573.5
        call      _for_write_seq_lis                            ;573.5
                                ; LOE
.B21.7:                         ; Preds .B21.6
        push      32                                            ;574.2
        xor       eax, eax                                      ;574.2
        push      eax                                           ;574.2
        push      eax                                           ;574.2
        push      -2088435968                                   ;574.2
        push      eax                                           ;574.2
        push      OFFSET FLAT: __STRLITPACK_91                  ;574.2
        call      _for_stop_core                                ;574.2
                                ; LOE
.B21.8:                         ; Preds .B21.7
        add       esp, 100                                      ;576.1
        pop       ebx                                           ;576.1
        pop       edi                                           ;576.1
        pop       esi                                           ;576.1
        mov       esp, ebp                                      ;576.1
        pop       ebp                                           ;576.1
        ret                                                     ;576.1
                                ; LOE
.B21.9:                         ; Preds .B21.3                  ; Infreq
        mov       eax, DWORD PTR [12+esp]                       ;563.18
        mov       ebx, DWORD PTR [20+esp]                       ;
        mov       edx, DWORD PTR [8+esp]                        ;
        movsx     edi, WORD PTR [36+ebx+eax*8]                  ;563.18
        lea       esi, DWORD PTR [-1+edi]                       ;563.8
        cmp       esi, ecx                                      ;563.8
        jl        .B21.16       ; Prob 50%                      ;563.8
                                ; LOE edx ecx ebx edi dl bl dh bh
.B21.10:                        ; Preds .B21.9                  ; Infreq
        mov       eax, ecx                                      ;563.8
        neg       eax                                           ;563.8
        lea       esi, DWORD PTR [eax+edi]                      ;563.8
        mov       edi, esi                                      ;563.8
        shr       edi, 31                                       ;563.8
        add       edi, esi                                      ;563.8
        sar       edi, 1                                        ;563.8
        test      edi, edi                                      ;563.8
        jbe       .B21.17       ; Prob 10%                      ;563.8
                                ; LOE edx ecx ebx esi edi dl bl dh bh
.B21.11:                        ; Preds .B21.10                 ; Infreq
        mov       DWORD PTR [4+esp], edi                        ;
        xor       eax, eax                                      ;
        mov       DWORD PTR [esp], esi                          ;
        mov       DWORD PTR [16+esp], ecx                       ;
        mov       DWORD PTR [20+esp], ebx                       ;
                                ; LOE eax edx
.B21.12:                        ; Preds .B21.21 .B21.11         ; Infreq
        mov       ecx, DWORD PTR [20+esp]                       ;564.38
        lea       edx, DWORD PTR [edx+edx*4]                    ;564.10
        mov       DWORD PTR [8+esp], eax                        ;
        mov       ebx, DWORD PTR [32+ecx+edx*8]                 ;564.38
        mov       esi, DWORD PTR [28+ecx+edx*8]                 ;564.38
        imul      ebx, esi                                      ;564.10
        mov       edi, DWORD PTR [ecx+edx*8]                    ;564.38
        mov       edx, DWORD PTR [16+esp]                       ;564.38
        sub       edi, ebx                                      ;564.10
        mov       ebx, esi                                      ;564.38
        lea       edx, DWORD PTR [edx+eax*2]                    ;564.38
        imul      esi, edx                                      ;564.10
        lea       eax, DWORD PTR [1+edx]                        ;564.38
        imul      ebx, eax                                      ;564.38
        mov       ebx, DWORD PTR [edi+ebx]                      ;564.10
        add       edx, 2                                        ;564.38
        mov       DWORD PTR [edi+esi], ebx                      ;564.10
        mov       edi, DWORD PTR [8+ebp]                        ;564.38
        mov       esi, DWORD PTR [edi]                          ;564.38
        lea       edi, DWORD PTR [esi+esi*4]                    ;564.10
        mov       ebx, DWORD PTR [32+ecx+edi*8]                 ;564.38
        mov       esi, DWORD PTR [28+ecx+edi*8]                 ;564.38
        imul      ebx, esi                                      ;564.10
        imul      edx, esi                                      ;564.38
        imul      esi, eax                                      ;564.10
        mov       ecx, DWORD PTR [ecx+edi*8]                    ;564.38
        sub       ecx, ebx                                      ;564.10
        mov       eax, DWORD PTR [ecx+edx]                      ;564.10
        mov       DWORD PTR [ecx+esi], eax                      ;564.10
        mov       eax, DWORD PTR [8+esp]                        ;563.8
        inc       eax                                           ;563.8
        cmp       eax, DWORD PTR [4+esp]                        ;563.8
        jae       .B21.13       ; Prob 37%                      ;563.8
                                ; LOE eax
.B21.21:                        ; Preds .B21.12                 ; Infreq
        mov       edx, DWORD PTR [8+ebp]                        ;560.13
        mov       edx, DWORD PTR [edx]                          ;560.13
        jmp       .B21.12       ; Prob 100%                     ;560.13
                                ; LOE eax edx
.B21.13:                        ; Preds .B21.12                 ; Infreq
        mov       edx, DWORD PTR [8+ebp]                        ;563.8
        lea       eax, DWORD PTR [1+eax+eax]                    ;563.8
        mov       esi, DWORD PTR [esp]                          ;
        mov       ecx, DWORD PTR [16+esp]                       ;
        mov       ebx, DWORD PTR [20+esp]                       ;
        mov       edx, DWORD PTR [edx]                          ;563.8
                                ; LOE eax edx ecx ebx esi bl bh
.B21.14:                        ; Preds .B21.13 .B21.17         ; Infreq
        lea       edi, DWORD PTR [-1+eax]                       ;563.8
        cmp       esi, edi                                      ;563.8
        jbe       .B21.16       ; Prob 10%                      ;563.8
                                ; LOE eax edx ecx ebx bl bh
.B21.15:                        ; Preds .B21.14                 ; Infreq
        lea       edx, DWORD PTR [edx+edx*4]                    ;564.10
        mov       esi, DWORD PTR [32+ebx+edx*8]                 ;564.38
        lea       edi, DWORD PTR [ecx+eax]                      ;564.10
        imul      edi, DWORD PTR [28+ebx+edx*8]                 ;564.38
        lea       ecx, DWORD PTR [-1+ecx+eax]                   ;564.10
        imul      esi, DWORD PTR [28+ebx+edx*8]                 ;564.10
        imul      ecx, DWORD PTR [28+ebx+edx*8]                 ;564.10
        add       edi, DWORD PTR [ebx+edx*8]                    ;564.10
        add       ecx, DWORD PTR [ebx+edx*8]                    ;564.10
        sub       edi, esi                                      ;564.10
        sub       ecx, esi                                      ;564.10
        mov       eax, DWORD PTR [edi]                          ;564.10
        mov       DWORD PTR [ecx], eax                          ;564.10
        mov       eax, DWORD PTR [8+ebp]                        ;564.10
        mov       edx, DWORD PTR [eax]                          ;564.10
                                ; LOE edx ebx bl bh
.B21.16:                        ; Preds .B21.14 .B21.9 .B21.15  ; Infreq
        mov       esi, DWORD PTR [8+ebp]                        ;571.32
        lea       eax, DWORD PTR [edx+edx*4]                    ;570.5
        movsx     ecx, WORD PTR [36+ebx+eax*8]                  ;570.5
        sub       ecx, DWORD PTR [32+ebx+eax*8]                 ;570.5
        imul      ecx, DWORD PTR [28+ebx+eax*8]                 ;570.5
        mov       edx, DWORD PTR [ebx+eax*8]                    ;570.5
        mov       DWORD PTR [edx+ecx], 0                        ;570.5
        mov       edi, DWORD PTR [esi]                          ;571.32
        lea       eax, DWORD PTR [edi+edi*4]                    ;571.5
        dec       WORD PTR [38+ebx+eax*8]                       ;571.57
        add       esp, 52                                       ;571.57
        pop       ebx                                           ;571.57
        pop       edi                                           ;571.57
        pop       esi                                           ;571.57
        mov       esp, ebp                                      ;571.57
        pop       ebp                                           ;571.57
        ret                                                     ;571.57
                                ; LOE
.B21.17:                        ; Preds .B21.10                 ; Infreq
        mov       eax, 1                                        ;
        jmp       .B21.14       ; Prob 100%                     ;
        ALIGN     16
                                ; LOE eax edx ecx ebx esi bl bh
; mark_end;
_DYNUST_LINK_VEH_LIST_MODULE_mp_TRIPCHAIN_REMOVE ENDP
_TEXT	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_90.0.21	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_LINK_VEH_LIST_MODULE_mp_TRIPCHAIN_REMOVE
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_LINK_VEH_LIST_MODULE_mp_TCHAINATT_2DREMOVE
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_LINK_VEH_LIST_MODULE_mp_TCHAINATT_2DREMOVE
_DYNUST_LINK_VEH_LIST_MODULE_mp_TCHAINATT_2DREMOVE	PROC NEAR 
.B22.1:                         ; Preds .B22.0
        push      ebp                                           ;579.12
        mov       ebp, esp                                      ;579.12
        and       esp, -16                                      ;579.12
        push      esi                                           ;579.12
        sub       esp, 92                                       ;579.12
        mov       edx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TCHAINATT_ARRAYSIZE] ;583.3
        test      edx, edx                                      ;583.3
        jle       .B22.9        ; Prob 2%                       ;583.3
                                ; LOE edx ebx edi
.B22.2:                         ; Preds .B22.1
        imul      esi, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TRIPCHAINLIST+32], -40 ;
        mov       eax, 1                                        ;
        add       esi, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TRIPCHAINLIST] ;
        mov       DWORD PTR [8+esp], edx                        ;
        mov       DWORD PTR [4+esp], edi                        ;
        mov       DWORD PTR [esp], ebx                          ;
        mov       ebx, eax                                      ;
                                ; LOE ebx esi
.B22.3:                         ; Preds .B22.7 .B22.2
        lea       edi, DWORD PTR [ebx+ebx*4]                    ;584.10
        movsx     edx, WORD PTR [36+esi+edi*8]                  ;584.10
        test      edx, edx                                      ;584.10
        jle       .B22.7        ; Prob 79%                      ;584.10
                                ; LOE ebx esi edi
.B22.4:                         ; Preds .B22.3
        mov       edx, DWORD PTR [12+esi+edi*8]                 ;584.10
        mov       ecx, edx                                      ;584.10
        shr       ecx, 1                                        ;584.10
        and       edx, 1                                        ;584.10
        and       ecx, 1                                        ;584.10
        add       edx, edx                                      ;584.10
        shl       ecx, 2                                        ;584.10
        or        ecx, 1                                        ;584.10
        or        ecx, edx                                      ;584.10
        or        ecx, 262144                                   ;584.10
        push      ecx                                           ;584.10
        push      DWORD PTR [esi+edi*8]                         ;584.10
        call      _for_dealloc_allocatable                      ;584.10
                                ; LOE eax ebx esi edi
.B22.20:                        ; Preds .B22.4
        add       esp, 8                                        ;584.10
                                ; LOE eax ebx esi edi
.B22.5:                         ; Preds .B22.20
        and       DWORD PTR [12+esi+edi*8], -2                  ;584.10
        mov       DWORD PTR [esi+edi*8], 0                      ;584.10
        test      eax, eax                                      ;584.10
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_VECTORERROR], eax ;584.10
        jne       .B22.15       ; Prob 5%                       ;584.10
                                ; LOE ebx esi edi
.B22.6:                         ; Preds .B22.5 .B22.23
        xor       edx, edx                                      ;584.10
        mov       WORD PTR [36+esi+edi*8], dx                   ;584.10
        mov       WORD PTR [38+esi+edi*8], dx                   ;584.10
                                ; LOE ebx esi
.B22.7:                         ; Preds .B22.3 .B22.6
        inc       ebx                                           ;585.3
        cmp       ebx, DWORD PTR [8+esp]                        ;585.3
        jle       .B22.3        ; Prob 82%                      ;585.3
                                ; LOE ebx esi
.B22.8:                         ; Preds .B22.7
        mov       edi, DWORD PTR [4+esp]                        ;
        mov       ebx, DWORD PTR [esp]                          ;
                                ; LOE ebx edi
.B22.9:                         ; Preds .B22.8 .B22.1
        mov       esi, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TRIPCHAINLIST+12] ;588.3
        test      esi, 1                                        ;588.7
        je        .B22.12       ; Prob 60%                      ;588.7
                                ; LOE ebx esi edi
.B22.10:                        ; Preds .B22.9
        mov       edx, esi                                      ;589.5
        mov       eax, esi                                      ;589.5
        shr       edx, 1                                        ;589.5
        and       eax, 1                                        ;589.5
        and       edx, 1                                        ;589.5
        add       eax, eax                                      ;589.5
        shl       edx, 2                                        ;589.5
        or        edx, 1                                        ;589.5
        or        edx, eax                                      ;589.5
        or        edx, 262144                                   ;589.5
        push      edx                                           ;589.5
        push      DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TRIPCHAINLIST] ;589.5
        call      _for_dealloc_allocatable                      ;589.5
                                ; LOE eax ebx esi edi
.B22.21:                        ; Preds .B22.10
        add       esp, 8                                        ;589.5
                                ; LOE eax ebx esi edi
.B22.11:                        ; Preds .B22.21
        and       esi, -2                                       ;589.5
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TRIPCHAINLIST], 0 ;589.5
        test      eax, eax                                      ;590.17
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TRIPCHAINLIST+12], esi ;589.5
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_VECTORERROR], eax ;589.5
        jne       .B22.13       ; Prob 5%                       ;590.17
                                ; LOE ebx edi
.B22.12:                        ; Preds .B22.22 .B22.11 .B22.9
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TCHAINATT_ARRAYSIZE], 0 ;595.3
        add       esp, 92                                       ;597.1
        pop       esi                                           ;597.1
        mov       esp, ebp                                      ;597.1
        pop       ebp                                           ;597.1
        ret                                                     ;597.1
                                ; LOE
.B22.13:                        ; Preds .B22.11                 ; Infreq
        mov       DWORD PTR [esp], 0                            ;591.5
        lea       edx, DWORD PTR [esp]                          ;591.5
        mov       DWORD PTR [32+esp], 14                        ;591.5
        lea       eax, DWORD PTR [32+esp]                       ;591.5
        mov       DWORD PTR [36+esp], OFFSET FLAT: __STRLITPACK_2 ;591.5
        push      32                                            ;591.5
        push      eax                                           ;591.5
        push      OFFSET FLAT: __STRLITPACK_92.0.22             ;591.5
        push      -2088435968                                   ;591.5
        push      911                                           ;591.5
        push      edx                                           ;591.5
        call      _for_write_seq_lis                            ;591.5
                                ; LOE ebx edi
.B22.14:                        ; Preds .B22.13                 ; Infreq
        push      32                                            ;592.5
        xor       eax, eax                                      ;592.5
        push      eax                                           ;592.5
        push      eax                                           ;592.5
        push      -2088435968                                   ;592.5
        push      eax                                           ;592.5
        push      OFFSET FLAT: __STRLITPACK_93                  ;592.5
        call      _for_stop_core                                ;592.5
                                ; LOE ebx edi
.B22.22:                        ; Preds .B22.14                 ; Infreq
        add       esp, 48                                       ;592.5
        jmp       .B22.12       ; Prob 100%                     ;592.5
                                ; LOE ebx edi
.B22.15:                        ; Preds .B22.5                  ; Infreq
        mov       DWORD PTR [48+esp], 0                         ;584.10
        lea       ecx, DWORD PTR [48+esp]                       ;584.10
        mov       DWORD PTR [40+esp], 36                        ;584.10
        lea       edx, DWORD PTR [40+esp]                       ;584.10
        mov       DWORD PTR [44+esp], OFFSET FLAT: __STRLITPACK_0 ;584.10
        push      32                                            ;584.10
        push      edx                                           ;584.10
        push      OFFSET FLAT: __STRLITPACK_94.0.23             ;584.10
        push      -2088435968                                   ;584.10
        push      911                                           ;584.10
        push      ecx                                           ;584.10
        call      _for_write_seq_lis                            ;584.10
                                ; LOE ebx esi edi
.B22.16:                        ; Preds .B22.15                 ; Infreq
        mov       DWORD PTR [72+esp], 0                         ;584.10
        lea       edx, DWORD PTR [104+esp]                      ;584.10
        mov       DWORD PTR [104+esp], ebx                      ;584.10
        push      32                                            ;584.10
        push      edx                                           ;584.10
        push      OFFSET FLAT: __STRLITPACK_95.0.23             ;584.10
        push      -2088435968                                   ;584.10
        push      911                                           ;584.10
        lea       ecx, DWORD PTR [92+esp]                       ;584.10
        push      ecx                                           ;584.10
        call      _for_write_seq_lis                            ;584.10
                                ; LOE ebx esi edi
.B22.17:                        ; Preds .B22.16                 ; Infreq
        push      32                                            ;584.10
        xor       edx, edx                                      ;584.10
        push      edx                                           ;584.10
        push      edx                                           ;584.10
        push      -2088435968                                   ;584.10
        push      edx                                           ;584.10
        push      OFFSET FLAT: __STRLITPACK_96                  ;584.10
        call      _for_stop_core                                ;584.10
                                ; LOE ebx esi edi
.B22.23:                        ; Preds .B22.17                 ; Infreq
        add       esp, 72                                       ;584.10
        jmp       .B22.6        ; Prob 100%                     ;584.10
        ALIGN     16
                                ; LOE ebx esi edi
; mark_end;
_DYNUST_LINK_VEH_LIST_MODULE_mp_TCHAINATT_2DREMOVE ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_LINK_VEH_LIST_MODULE_mp_TCHAINATT_2DREMOVE
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_LINK_VEH_LIST_MODULE_mp_TCHAINATT_REMOVE
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_LINK_VEH_LIST_MODULE_mp_TCHAINATT_REMOVE
_DYNUST_LINK_VEH_LIST_MODULE_mp_TCHAINATT_REMOVE	PROC NEAR 
; parameter 1: 8 + ebp
.B23.1:                         ; Preds .B23.0
        push      ebp                                           ;600.12
        mov       ebp, esp                                      ;600.12
        and       esp, -16                                      ;600.12
        push      esi                                           ;600.12
        push      edi                                           ;600.12
        push      ebx                                           ;600.12
        sub       esp, 52                                       ;600.12
        imul      edi, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TRIPCHAINLIST+32], -40 ;603.31
        mov       eax, DWORD PTR [8+ebp]                        ;600.12
        add       edi, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TRIPCHAINLIST] ;603.31
        mov       esi, DWORD PTR [eax]                          ;603.7
        lea       ebx, DWORD PTR [esi+esi*4]                    ;603.7
        movsx     edx, WORD PTR [36+edi+ebx*8]                  ;603.7
        test      edx, edx                                      ;603.31
        jle       .B23.5        ; Prob 79%                      ;603.31
                                ; LOE ebx esi edi
.B23.2:                         ; Preds .B23.1
        mov       eax, DWORD PTR [12+edi+ebx*8]                 ;604.16
        mov       edx, eax                                      ;604.5
        shr       edx, 1                                        ;604.5
        and       edx, 1                                        ;604.5
        mov       DWORD PTR [esp], eax                          ;604.16
        and       eax, 1                                        ;604.5
        shl       edx, 2                                        ;604.5
        add       eax, eax                                      ;604.5
        or        edx, 1                                        ;604.5
        or        edx, eax                                      ;604.5
        or        edx, 262144                                   ;604.5
        push      edx                                           ;604.5
        push      DWORD PTR [edi+ebx*8]                         ;604.5
        call      _for_dealloc_allocatable                      ;604.5
                                ; LOE eax ebx esi edi
.B23.11:                        ; Preds .B23.2
        add       esp, 8                                        ;604.5
                                ; LOE eax ebx esi edi
.B23.3:                         ; Preds .B23.11
        mov       edx, DWORD PTR [esp]                          ;604.5
        and       edx, -2                                       ;604.5
        mov       DWORD PTR [edi+ebx*8], 0                      ;604.5
        test      eax, eax                                      ;605.17
        mov       DWORD PTR [12+edi+ebx*8], edx                 ;604.5
        mov       DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_VECTORERROR], eax ;604.5
        jne       .B23.6        ; Prob 5%                       ;605.17
                                ; LOE ebx esi edi
.B23.13:                        ; Preds .B23.3
        xor       esi, esi                                      ;610.5
                                ; LOE ebx esi edi
.B23.4:                         ; Preds .B23.12 .B23.13
        mov       WORD PTR [36+edi+ebx*8], si                   ;610.5
        mov       WORD PTR [38+edi+ebx*8], si                   ;611.5
                                ; LOE
.B23.5:                         ; Preds .B23.1 .B23.4
        add       esp, 52                                       ;614.1
        pop       ebx                                           ;614.1
        pop       edi                                           ;614.1
        pop       esi                                           ;614.1
        mov       esp, ebp                                      ;614.1
        pop       ebp                                           ;614.1
        ret                                                     ;614.1
                                ; LOE
.B23.6:                         ; Preds .B23.3                  ; Infreq
        mov       DWORD PTR [esp], 0                            ;606.4
        lea       edx, DWORD PTR [esp]                          ;606.4
        mov       DWORD PTR [32+esp], 36                        ;606.4
        lea       eax, DWORD PTR [32+esp]                       ;606.4
        mov       DWORD PTR [36+esp], OFFSET FLAT: __STRLITPACK_0 ;606.4
        push      32                                            ;606.4
        push      eax                                           ;606.4
        push      OFFSET FLAT: __STRLITPACK_94.0.23             ;606.4
        push      -2088435968                                   ;606.4
        push      911                                           ;606.4
        push      edx                                           ;606.4
        call      _for_write_seq_lis                            ;606.4
                                ; LOE ebx esi edi
.B23.7:                         ; Preds .B23.6                  ; Infreq
        mov       DWORD PTR [24+esp], 0                         ;607.4
        lea       eax, DWORD PTR [64+esp]                       ;607.4
        mov       DWORD PTR [64+esp], esi                       ;607.4
        push      32                                            ;607.4
        push      eax                                           ;607.4
        push      OFFSET FLAT: __STRLITPACK_95.0.23             ;607.4
        push      -2088435968                                   ;607.4
        push      911                                           ;607.4
        lea       edx, DWORD PTR [44+esp]                       ;607.4
        push      edx                                           ;607.4
        call      _for_write_seq_lis                            ;607.4
                                ; LOE ebx edi
.B23.8:                         ; Preds .B23.7                  ; Infreq
        push      32                                            ;608.4
        xor       esi, esi                                      ;608.4
        push      esi                                           ;608.4
        push      esi                                           ;608.4
        push      -2088435968                                   ;608.4
        push      esi                                           ;608.4
        push      OFFSET FLAT: __STRLITPACK_96                  ;608.4
        call      _for_stop_core                                ;608.4
                                ; LOE ebx esi edi
.B23.12:                        ; Preds .B23.8                  ; Infreq
        add       esp, 72                                       ;608.4
        jmp       .B23.4        ; Prob 100%                     ;608.4
        ALIGN     16
                                ; LOE ebx esi edi
; mark_end;
_DYNUST_LINK_VEH_LIST_MODULE_mp_TCHAINATT_REMOVE ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_LINK_VEH_LIST_MODULE_mp_TCHAINATT_REMOVE
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
	PUBLIC _DYNUST_LINK_VEH_LIST_MODULE_mp_H_INCREASESIZE
_DYNUST_LINK_VEH_LIST_MODULE_mp_H_INCREASESIZE	DD	30
	DD 1 DUP (0H)	; pad
	PUBLIC _DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP
_DYNUST_LINK_VEH_LIST_MODULE_mp_TEMPP	DD	0
	DD	0
	DD	0
	DD	128
	DD	1
	DD	0
	DD	0
	DD	0
	DD	0
	DD 1 DUP (0H)	; pad
	PUBLIC _DYNUST_LINK_VEH_LIST_MODULE_mp_TRIPCHAINLIST
_DYNUST_LINK_VEH_LIST_MODULE_mp_TRIPCHAINLIST	DD	0
	DD	0
	DD	0
	DD	128
	DD	1
	DD	0
	DD	0
	DD	0
	DD	0
	DD 1 DUP (0H)	; pad
	PUBLIC _DYNUST_LINK_VEH_LIST_MODULE_mp_ENTQUEVEHLIST
_DYNUST_LINK_VEH_LIST_MODULE_mp_ENTQUEVEHLIST	DD	0
	DD	0
	DD	0
	DD	128
	DD	1
	DD	0
	DD	0
	DD	0
	DD	0
	DD 1 DUP (0H)	; pad
	PUBLIC _DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST
_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST	DD	0
	DD	0
	DD	0
	DD	128
	DD	1
	DD	0
	DD	0
	DD	0
	DD	0
_DATA	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_30	DB	68
	DB	69
	DB	65
	DB	76
	DB	76
	DB	79
	DB	67
	DB	65
	DB	84
	DB	69
	DB	32
	DB	76
	DB	105
	DB	110
	DB	107
	DB	65
	DB	116
	DB	116
	DB	95
	DB	65
	DB	114
	DB	114
	DB	97
	DB	121
	DB	32
	DB	118
	DB	101
	DB	99
	DB	116
	DB	111
	DB	114
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_60.0.7	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_61.0.7	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_62.0.7	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_34	DB	86
	DB	104
	DB	99
	DB	65
	DB	116
	DB	116
	DB	32
	DB	68
	DB	101
	DB	115
	DB	116
	DB	111
	DB	114
	DB	121
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_56.0.5	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_57.0.5	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_44	DB	76
	DB	105
	DB	110
	DB	107
	DB	86
	DB	101
	DB	104
	DB	76
	DB	105
	DB	115
	DB	116
	DB	32
	DB	83
	DB	101
	DB	116
	DB	117
	DB	112
	DB	32
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	0
__STRLITPACK_28	DB	68
	DB	69
	DB	65
	DB	76
	DB	76
	DB	79
	DB	67
	DB	65
	DB	84
	DB	69
	DB	32
	DB	116
	DB	101
	DB	109
	DB	112
	DB	80
	DB	32
	DB	118
	DB	101
	DB	99
	DB	116
	DB	111
	DB	114
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_63.0.8	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_64.0.8	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_65.0.8	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_32	DB	116
	DB	101
	DB	109
	DB	112
	DB	80
	DB	32
	DB	68
	DB	101
	DB	115
	DB	116
	DB	111
	DB	114
	DB	121
	DB	32
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	0
__STRLITPACK_58.0.6	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_59.0.6	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_42	DB	116
	DB	101
	DB	109
	DB	112
	DB	80
	DB	32
	DB	83
	DB	101
	DB	116
	DB	117
	DB	112
	DB	32
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_40	DB	97
	DB	108
	DB	108
	DB	111
	DB	99
	DB	97
	DB	116
	DB	101
	DB	32
	DB	116
	DB	109
	DB	112
	DB	80
	DB	32
	DB	118
	DB	101
	DB	99
	DB	116
	DB	111
	DB	114
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_50.0.4	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_51.0.4	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_38	DB	68
	DB	69
	DB	65
	DB	76
	DB	76
	DB	79
	DB	67
	DB	65
	DB	84
	DB	69
	DB	32
	DB	76
	DB	105
	DB	110
	DB	107
	DB	65
	DB	116
	DB	116
	DB	95
	DB	49
	DB	68
	DB	65
	DB	114
	DB	114
	DB	97
	DB	121
	DB	32
	DB	118
	DB	101
	DB	99
	DB	116
	DB	111
	DB	114
	DB	32
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	0
__STRLITPACK_52.0.4	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_53.0.4	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_36	DB	97
	DB	108
	DB	108
	DB	111
	DB	99
	DB	97
	DB	116
	DB	101
	DB	32
	DB	76
	DB	105
	DB	110
	DB	107
	DB	86
	DB	101
	DB	104
	DB	76
	DB	105
	DB	115
	DB	116
	DB	32
	DB	118
	DB	101
	DB	99
	DB	116
	DB	111
	DB	114
	DB	32
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_54.0.4	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_55.0.4	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_26	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	109
	DB	116
	DB	106
	DB	120
	DB	95
	DB	114
	DB	101
	DB	109
	DB	111
	DB	118
	DB	101
	DB	32
	DB	105
	DB	110
	DB	32
	DB	108
	DB	105
	DB	110
	DB	107
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_14	DB	68
	DB	69
	DB	65
	DB	76
	DB	76
	DB	79
	DB	67
	DB	65
	DB	84
	DB	69
	DB	32
	DB	76
	DB	105
	DB	110
	DB	107
	DB	65
	DB	116
	DB	116
	DB	95
	DB	65
	DB	114
	DB	114
	DB	97
	DB	121
	DB	32
	DB	118
	DB	101
	DB	99
	DB	116
	DB	111
	DB	114
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_79.0.14	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_80.0.14	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_81	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_16	DB	86
	DB	104
	DB	99
	DB	65
	DB	116
	DB	116
	DB	32
	DB	68
	DB	101
	DB	115
	DB	116
	DB	111
	DB	114
	DB	121
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_77.0.13	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_78	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_24	DB	69
	DB	110
	DB	116
	DB	81
	DB	117
	DB	101
	DB	86
	DB	101
	DB	104
	DB	76
	DB	105
	DB	115
	DB	116
	DB	32
	DB	83
	DB	101
	DB	116
	DB	117
	DB	112
	DB	32
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_22	DB	97
	DB	108
	DB	108
	DB	111
	DB	99
	DB	97
	DB	116
	DB	101
	DB	32
	DB	116
	DB	109
	DB	112
	DB	80
	DB	32
	DB	118
	DB	101
	DB	99
	DB	116
	DB	111
	DB	114
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_71.0.12	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_72	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_20	DB	68
	DB	69
	DB	65
	DB	76
	DB	76
	DB	79
	DB	67
	DB	65
	DB	84
	DB	69
	DB	32
	DB	76
	DB	105
	DB	110
	DB	107
	DB	65
	DB	116
	DB	116
	DB	95
	DB	49
	DB	68
	DB	65
	DB	114
	DB	114
	DB	97
	DB	121
	DB	32
	DB	118
	DB	101
	DB	99
	DB	116
	DB	111
	DB	114
	DB	32
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	0
__STRLITPACK_73.0.12	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_74	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_18	DB	97
	DB	108
	DB	108
	DB	111
	DB	99
	DB	97
	DB	116
	DB	101
	DB	32
	DB	69
	DB	110
	DB	116
	DB	81
	DB	117
	DB	101
	DB	86
	DB	101
	DB	104
	DB	76
	DB	105
	DB	115
	DB	116
	DB	32
	DB	118
	DB	101
	DB	99
	DB	116
	DB	111
	DB	114
	DB	32
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	0
__STRLITPACK_75.0.12	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_76	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_0	DB	68
	DB	69
	DB	65
	DB	76
	DB	76
	DB	79
	DB	67
	DB	65
	DB	84
	DB	69
	DB	32
	DB	76
	DB	105
	DB	110
	DB	107
	DB	65
	DB	116
	DB	116
	DB	95
	DB	65
	DB	114
	DB	114
	DB	97
	DB	121
	DB	32
	DB	118
	DB	101
	DB	99
	DB	116
	DB	111
	DB	114
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_94.0.23	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_95.0.23	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_96	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_2	DB	86
	DB	104
	DB	99
	DB	65
	DB	116
	DB	116
	DB	32
	DB	68
	DB	101
	DB	115
	DB	116
	DB	111
	DB	114
	DB	121
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_92.0.22	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_93	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_12	DB	84
	DB	114
	DB	105
	DB	112
	DB	67
	DB	104
	DB	97
	DB	105
	DB	110
	DB	76
	DB	105
	DB	115
	DB	116
	DB	32
	DB	83
	DB	101
	DB	116
	DB	117
	DB	112
	DB	32
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_83	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_10	DB	97
	DB	108
	DB	108
	DB	111
	DB	99
	DB	97
	DB	116
	DB	101
	DB	32
	DB	116
	DB	109
	DB	112
	DB	80
	DB	32
	DB	118
	DB	101
	DB	99
	DB	116
	DB	111
	DB	114
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_84.0.19	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_85	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_8	DB	68
	DB	69
	DB	65
	DB	76
	DB	76
	DB	79
	DB	67
	DB	65
	DB	84
	DB	69
	DB	32
	DB	84
	DB	114
	DB	105
	DB	112
	DB	67
	DB	104
	DB	97
	DB	105
	DB	110
	DB	76
	DB	105
	DB	115
	DB	116
	DB	32
	DB	118
	DB	101
	DB	99
	DB	116
	DB	111
	DB	114
	DB	32
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_86.0.19	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_87	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_6	DB	97
	DB	108
	DB	108
	DB	111
	DB	99
	DB	97
	DB	116
	DB	101
	DB	32
	DB	84
	DB	114
	DB	105
	DB	112
	DB	67
	DB	104
	DB	97
	DB	105
	DB	110
	DB	76
	DB	105
	DB	115
	DB	116
	DB	32
	DB	118
	DB	101
	DB	99
	DB	116
	DB	111
	DB	114
	DB	32
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	0
__STRLITPACK_88.0.19	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_89	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_4	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	101
	DB	110
	DB	116
	DB	114
	DB	121
	DB	113
	DB	117
	DB	101
	DB	117
	DB	101
	DB	95
	DB	114
	DB	101
	DB	109
	DB	111
	DB	118
	DB	101
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_91	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
EXTRN	_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH:BYTE
	COMM _DYNUST_LINK_VEH_LIST_MODULE_mp_VECTORERROR:BYTE:4
	COMM _DYNUST_LINK_VEH_LIST_MODULE_mp_TCHAINATT_ARRAYSIZE:BYTE:4
	COMM _DYNUST_LINK_VEH_LIST_MODULE_mp_ENQATT_ARRAYSIZE:BYTE:4
	COMM _DYNUST_LINK_VEH_LIST_MODULE_mp_LINKATT_ARRAYSIZE:BYTE:4
_DATA	ENDS
EXTRN	_for_stop_core:PROC
EXTRN	_for_dealloc_allocatable:PROC
EXTRN	_for_allocate:PROC
EXTRN	_for_write_seq_lis_xmit:PROC
EXTRN	_for_write_seq_lis:PROC
EXTRN	_for_alloc_allocatable:PROC
EXTRN	_for_check_mult_overflow:PROC
EXTRN	__alloca_probe:PROC
EXTRN	__fltused:BYTE
	INCLUDELIB <ifconsol>
	INCLUDELIB <libifcoremt>
	INCLUDELIB <libifport>
	INCLUDELIB <libmmt>
	INCLUDELIB <LIBCMT>
	INCLUDELIB <libirc>
	INCLUDELIB <svml_dispmt>
	INCLUDELIB <OLDNAMES>
	END
