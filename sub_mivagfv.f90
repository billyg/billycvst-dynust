SUBROUTINE  MIVAGFV(dynust_mode_flag,MaxIntervals)
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~ Copyright (C) 2002-2011 The Arizona Board of Regents on Behalf of The University of Arizona.  ~~!
!~~ Developed by Yi-Chang Chiu, Ph.D. as par to the DynusT project.                               ~~!
!~~ This program is free software: you can redistribute it and/or modify it                       ~~!
!~~ under the terms of the GNU General Public License version 2 as published by                   ~~!
!~~ the Free Software Foundation.                                                                 ~~!
!~~ This program is distributed in the hope that it will be useful,                               ~~!
!~~ but WITHOUT ANY WARRANTY; without even the implied warranty of                                ~~!
!~~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                                          ~~!
!~~ See the GNU General Public License for more details.                                          ~~!
!~~ You should have received a copy of the GNU General Public License                             ~~!
!~~ along with this program (in the file COPYING).                                                ~~!
!~~ IF not, see <http://www.gnu.org/licenses/>.                                                   ~~!
!~~ Please see http://www.dynust.net for contact information and names                            ~~!
!~~ of individual contributors.                                                                   ~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!

    USE DYNUST_MAIN_MODULE
    USE DYNUST_NETWORK_MODULE
    USE DYNUST_VEH_MODULE
    USE DYNUST_VEH_PATH_ATT_MODULE
    USE MEMORY_MODULE
    USE DFPORT
    USE RAND_GEN_INT
    USE IFQWIN
    INTERFACE
      SUBROUTINE printscreen(printstr,intx,inty,dx1,dy1,dx2,dy2,refresh)
       INTEGER intx, inty, refresh,dx1,dy1,dx2,dy2
       CHARACTER (80) printstr
      END SUBROUTINE 
    END INTERFACE  
    INTERFACE
      SUBROUTINE IMSLSort(Ain,ROWS,COLS)     
         INTEGER ROWS,COLS
         INTEGER, INTENT(inout), DIMENSION(ROWS,COLS) :: AIN
      END SUBROUTINE
    END INTERFACE
    INTERFACE
      SUBROUTINE TDSP_MAIN_MIVA(dynust_mode_flag,isnow,IvehType)
        INTEGER dynust_mode_flag,isnow,IVehType
      END SUBROUTINE
    END INTERFACE
	INTERFACE
      SUBROUTINE tdsp_core(dynust_mode_flag,ip)
        INTEGER dynust_mode_flag,ip
  	  END SUBROUTINE
  	END INTERFACE
          	    
    INTEGER dynust_mode_flag,localerror
    LOGICAL::minTimeAlloc =.false.
    CHARACTER(80) printstr1,printstr2
    CHARACTER(80) printstr3,printstr4
	REAL factor,UEcost
    LOGICAL FoundFlag,ifound
    INTEGER EpocDropT,MosCt,MosCa,MosCh,MaxIntervals
	INTEGER t,tmpnodsum,ItoVio,IfromVio,ItimeVio,EpocDrop, EpocDropH
    INTEGER NNewPath, Mos, MostRCount,EpocFind
	INTEGER,ALLOCATABLE::AIN(:,:),AINPtr(:),AIN0(:,:),AIN1(:,:)
	INTEGER,ALLOCATABLE::AINStatus(:)
    INTEGER, ALLOCATABLE::tmpuepath(:), tmpuepath2(:), tmpasspath(:), tmparrayp(:)
	INTEGER,ALLOCATABLE:: vehmap(:)
	INTEGER,ALLOCATABLE:: IDtable(:,:), intoout(:), outtoin(:)  
    REAL, ALLOCATABLE:: gap(:), probtemp(:), assgtemp(:), assged(:)
    INTEGER, ALLOCATABLE::GVs(:,:)
    INTEGER, ALLOCATABLE::SkimData(:,:,:)
    REAL,ALLOCATABLE:: skimtmp(:,:,:)
    REAL, ALLOCATABLE::skimtemp(:,:,:)
	INTEGER vhpcect
    INTEGER AutoCTL, TruckCTL, HOVCTL
    REAL totalgap
    REAL,ALLOCATABLE::tmparrayt(:)
    INTEGER kpsettmpsize
    type mintimedatatype
      INTEGER, pointer::link(:,:)
      INTEGER:: ct
      INTEGER::size
    endtype
    type(mintimedatatype), ALLOCATABLE::minTimeData(:,:)
    
    ALLOCATABLE::mintemp(:,:)
    
    TYPE pathinstruct
      INTEGER, POINTER :: N(:) ! size ASSOCIATED with the path length
    END TYPE pathinstruct

    TYPE GFVKPATH
      INTEGER::ct
      INTEGER::point
      INTEGER::nodesum
      REAL::ttime 
      REAL::gap
      LOGICAL::status
      REAL::assignratio
      INTEGER::psize
      INTEGER, pointer::P(:)
      REAL, pointer::t(:)

    END TYPE GFVKPATH

! --  Define the data structure for storing the attributes of each path in the Grand Path Set
! --  LinkVehList: link list keeps all the vehicles on the link
    TYPE(pathinstruct),ALLOCATABLE::pathin(:)
    INTEGER, ALLOCATABLE::nodesum(:)
    INTEGER, ALLOCATABLE::pathorder(:,:)
    type(GFVKPATH), ALLOCATABLE::kpset(:)
    INTEGER iseecutoff_veh    
    INTEGER::iseed(1) = 0   

!> > > > > > > > > >                < < < < < < < < < < <
!>>>>>>>>>>>>>>>>>>>CODES START HERE<<<<<<<<<<<<<<<<<<<<<
!> > > > > > > > > >                < < < < < < < < < < <
   IF(SkimFlag > 0.AND.iteration == iteMax) THEN
     OPEN(file='skim.dat',unit=3300,status='unknown',iostat=error)
     IF(skimOutInt < 0.1) THEN
       WRITE(911,*) "Skim output switch is turned on but the output interval is zero"
       STOP
     ENDIF
     InPeriod = SimPeriod/skimOutInt
     ALLOCATE(SkimData(noof_master_destinations,InPeriod,2)) ! THE FIRST DIMENSION IS FOR ORIGIN ZONE
     SkimData(:,:,:) = 0
     ALLOCATE(skimtmp(InPeriod,noof_master_destinations,noof_master_destinations))
     skimdata(:,:,:) = 0
   ENDIF

   OPEN(file='ErrorLog.dat',unit=911,status='unknown',iostat=error)
	IF(error /= 0) THEN
      WRITE(911,*) 'Error when opening ErrorLog.dat'
	  STOP
	ENDIF
    ALLOCATE(tmpuepath(maxnu_pa))
    tmpuepath = 0
    ALLOCATE(tmpuepath2(maxnu_pa))
    tmpuepath2 = 0
    ALLOCATE(tmpasspath(10000))
    tmpasspath = 0
    ALLOCATE(tmparrayp(1000000))
    tmparrayp = 0
    ALLOCATE(tmparrayt(1000000))
    tmparrayt = 0
    
    kpsettmpsize = 1000

    totalvhpcect = 0.0
    
    ALLOCATE(kpset(max(iteMax*10,200)))
    kpset(:)%ct = 0
    kpset(:)%point = 0
    kpset(:)%nodesum = 0
    kpset(:)%ttime = 0.0
    kpset(:)%gap = 0.0
    kpset(:)%status = .true.
    kpset(:)%assignratio = 0.0
    kpset(:)%psize = kpsettmpsize
    DO is = 1, max(iteMax*10,200)
      ALLOCATE(kpset(is)%p(kpsettmpsize))
      ALLOCATE(kpset(is)%t(kpsettmpsize))      
      kpset(is)%p(:) = 0
      kpset(is)%t(:) = 0      
    ENDDO

    IF(iteration == 1) THEN
      ALLOCATE(converg(iteMax,EpocNum*iti_nuEP,2)) !1: is actual time, 2, is sp time.
      converg(:,:,:)=0
    ENDIF

    ALLOCATE(minTimedata(noof_master_destinations,iti_nuEP))
    mintimedata(:,:)%size = 0
    mintimedata(:,:)%ct = 0
    
    ALLOCATE(gap(EpocNum*iti_nuEP))
    gap(:) = 0
    	
	OPEN(file = "AltPath.dat", unit = 989, status = "unknown")
	IF(iteration == 1) OPEN(file = "Convergence.dat", unit = 5252, status = "unknown")	
	
    iseed(1) = istrm
    MosCt = 0
    MosCa = 01
    MosCh = 0
    AutoCTL = 0
    TruckCTL = 0
    HOVCTL  =  0
	EpocDrop = 0
    EpocDropT = 0
    EpocDropH = 0   	
! read the vehicle path
    OPEN(file='mtcph.itf',unit=6099,status='unknown',iostat=error)
	IF(error /= 0) THEN
         WRITE(911,*) 'Error when opening mtcph.itf'
	   STOP
	ENDIF
    IF(iteration == 1) numGenVeh=justveh
    vhpcect = 0
!    IF(RunMode /= 1) demandmidcounter = justveh
    demandmidcounter = justveh

! determine factor based on which   it is at. Need to be changed later on
    IF(asgflg == 0) THEN
      factor = min(factorcap,1.0/(mod(iteration,(iteMax+1)/itersub)+1))
    ELSEIF(asgflg == 1) THEN
      factor = min(factorcap,1.0/(mod(iteration,(iteMax+1)/itersub)))
    ELSE
      WRITE(911,*) "error in the last number asgflg in PARAMETER.dat"
      STOP
    ENDIF

! Number of vehicles to be changed with paths    
	NNewPath = nint(float(justveh)*factor) !calculate how many to be assigned with new path

    ItoVio = 0
	IfromVio = 0
	ItimeVio = 0
     
    ALLOCATE(AINPtr(noof_master_destinations),stat=error) ! this array is to keep track which dest has at least one veh so that ksp is called
    IF(error /= 0) THEN
	  WRITE(911,*)"allocate AINPtr error - insufficient memory"
	  STOP
    ENDIF
    AINPtr(:) = 0

    ALLOCATE(pathin(demandmidcounter),stat=error)
    IF(error /= 0) THEN
	  WRITE(911,*)"allocate pathin error - insufficient memory"
	  STOP
    ENDIF

    ALLOCATE(Nodesum(demandmidcounter),stat=error)
    IF(error /= 0) THEN
	  WRITE(911,*)"allocate nodesum error - insufficient memory"
	  STOP
    ENDIF
    nodesum(:) = 0

    DO icheck = 1, demandmidcounter
     READ(989,'(i10,i8,1000i7)') ii0, i0, i1, i2, i3, ic2
     ALLOCATE(pathin(icheck)%N(ic2+2),stat=localerror)
	 pathin(icheck)%N(:) = 0
	ENDDO
	rewind(989)
    
    ALLOCATE(vehmap(maxid),stat=error)
    IF(error /= 0) THEN
	  WRITE(911,*)"allocate vehmap error - insufficient memory"
	  STOP
    ENDIF
    vehmap(:) = 0

    ALLOCATE(IDtable(demandmidcounter,2),stat=error)
    IF(error /= 0) THEN
	  WRITE(911,*)"allocate IDtable error - insufficient memory"
	  STOP
    ENDIF
    IdTable(:,:) = 0

    ALLOCATE(intoout(demandmidcounter),stat=error)
    IF(error /= 0) THEN
	  WRITE(911,*)"allocate intoout error - insufficient memory"
	  STOP
    ENDIF
    intoout(:) = 0

    ALLOCATE(outtoin(demandmidcounter),stat=error)
    IF(error /= 0) THEN
	  WRITE(911,*)"allocate outtoin error - insufficient memory"
	  STOP
    ENDIF
    outtoin(:) = 0

    DO imust = 1, demandmidcounter ! read in the paths, 1: vehid, 2: num of nodes, 3: path nodes
     READ(989,'(i10, i8,1000i7)') ignodesum, pathin(imust)%N(1), i1, i2, i3, pathin(imust)%N(2), (pathin(imust)%N(is),is=3,pathin(imust)%N(2)+2)
     nodesum(imust) = ignodesum
     
     IF(pathin(imust)%N(1) < 1.or.pathin(imust)%N(2) < 1) THEN
        print *, "error in ue reading AltPath.dat"
     ENDIF
	 vehmap(pathin(imust)%N(1)) = imust
	 IdTable(imust,2) = imust
	 IdTable(imust,1) = pathin(imust)%N(1)
	 intoout(imust) = pathin(imust)%N(1)
	 outtoin(pathin(imust)%N(1)) = imust
!   calculate min time for each origin-agg int combination
     ioutime = imust	 
	ENDDO

! sort IdTable
	 CALL IMSLSort(IdTable,demandmidcounter,2)	!SORT IDTABLE BASED ON ACTUAL VEHICLE ID

      EpocPeriod = SimPeriod/EpocNum
      EpocCounter = 1
      EpocFind = 0
      ALLOCATE(Epoc(EpocNum,4)) ! the end of each epoc

	  Epoc(:,:) = 0
      DO iu = 1, justveh
          ijs = iu
		  EpocFind = EpocFind + 1
        IF(m_dynust_last_stand(ijs)%stime > EpocCounter*EpocPeriod-0.09) THEN
!        IF(ttilFstNMove(ijs) > EpocCounter*EpocPeriod-0.09) THEN
          Epoc(EpocCounter,1) = iu-1
          ijs2 = ijs - 1
          Epoc(EpocCounter,2) = nint(m_dynust_last_stand(ijs2)%ttilFstNMove/xminPerSimInt)
          Epoc(EpocCounter,3) = EpocFind-1
          Epoc(EpocCounter,4) = nint((float(iu-1)/justveh)*NNewPath) ! acumulated number of vehicles to be assigned at the end of this epoc
		  EpocCounter = EpocCounter + 1
		  EpocFind = 0
		ENDIF

        IF(m_dynust_last_stand(ijs)%vehtype == 1.and.m_dynust_last_stand(ijs)%vehclass == 3) THEN
          AutoCTL = AutoCTL + 1
        ELSEIF(m_dynust_last_stand(ijs)%vehtype == 2.and.m_dynust_last_stand(ijs)%vehclass == 3) THEN
          TruckCTL = TruckCTL + 1
        ELSEIF(m_dynust_last_stand(ijs)%vehtype == 3.and.m_dynust_last_stand(ijs)%vehclass == 3) THEN
          HOVCTL = HOVCTL + 1
        ENDIF
      ENDDO
           
      DO i = 1, EpocNum
        IF(Epoc(i,1) < 1) THEN
           IF(EpocFind > 0) THEN
              Epoc(i,1) = justveh
			  Epoc(i,2) = m_dynust_last_stand(pathin(justveh)%N(1))%ttilFstNMove
			  Epoc(i,3) = EpocFind
              Epoc(i,4) = NnewPath
			  IF(i < EpocNum) Epoc(min(EpocNum,i+1):EpocNum,:) = 0
			  exit
		   ENDIF
		ENDIF
	  ENDDO

2005 DO 20000 IVehType = 1, 3
AINPtr(:) = 0
IF(IVehType == 2) THEN
  IF(.not.truckok.or.(truckok.and.NTolllink == 0)) go to 20000
ENDIF
IF(IVehType == 3) THEN
  IF(.not.hovok.or.(hovok.and.NTolllink == 0)) go to 20000
ENDIF
  
IF(IVehType == 1) THEN
   printstr1 = "Assigning SOV  "
   CALL printscreen(printstr1,20,425,0,0,800,30,1)
ELSEIF(IVehType == 2) THEN
   printstr1 = "Assigning Truck"
   CALL printscreen(printstr1,20,440,0,0,0,0,0)
ELSE
   printstr1 = "Assigning HOV  "
   CALL printscreen(printstr1,20,455,0,0,0,0,0)
ENDIF

WRITE(5559,*)

6057 DO 10000 isnow = 1, EpocNum

     WRITE(printstr1,'(i2)') iteration
     WRITE(printstr2,'(i2)') isnow

     printstr3 = 'Iteration '//printstr1
     CALL printscreen(printstr3,300,395,20,5,400,80,1)
     printstr4 = ' - '//printstr2
     CALL printscreen(printstr4,392,395,0,0,0,0,0)
     
	ifound = .false.
	DO icheck=1, EpocNullNm
     IF(isnow == EpocNull(icheck)) THEN
	    ifound = .true.
		exit
     ENDIF
	ENDDO
	IF(ifound) THEN
	   go to 10000
	ENDIF

    IF(Epoc(isnow,3) > 0) THEN

    IF(isnow == 1) THEN
	  EpocSize = Epoc(1,1)
	ELSE
	  EpocSize = Epoc(isnow,1)-Epoc(isnow-1,1)
    ENDIF


	ALLOCATE(AIN(EpocSize,4))
    AIN(:,:) = 0
    ALLOCATE(AIN0(EpocSize,4))
    AIN0(:,:) = 0
    ALLOCATE(AIN1(EpocSize,1))
    AIN1(:,:) = 0

    ALLOCATE(AINStatus(EpocSize))
    AINStatus(:) = 0

    DO 1009 j = 1, noof_master_destinations
     DO 1009 m = 1, iti_nuEP
      ALLOCATE(mintimedata(j,m)%link(noofarcs/3,2)) ! 1st: link id, 2nd: travel time
      mintimedata(j,m)%link(:,:) = 0
      mintimedata(j,m)%size = noofarcs/3
      mintimedata(j,m)%ct = 0
1009  CONTINUE 
     minTimeAlloc = .true.

     DO io = 1, EpocSize
	  IF(isnow == 1) THEN
       isd = IdTable(io,2)	  
       ibs = pathin(isd)%N(1)
       AIN(io,1)=MasterDest(m_dynust_last_stand(ibs)%jdest) !destination
       AIN(io,2)=m_dynust_network_arc_nde(m_dynust_last_stand(ibs)%isec)%idnod !origin
       AIN(io,3)=min(iti_nuEP,ifix(m_dynust_last_stand(ibs)%ttilFstNMove/xminPerSimInt/simPerAgg)+1) ! departure time       
       AIN(io,4)=ibs
       AIN0(io,1)=MasterDest(m_dynust_last_stand(ibs)%jdest) !destination
       AIN0(io,3)=m_dynust_last_stand(ibs)%isec !origin
       AIN0(io,2)=max(1,min(iti_nuEP,ifix(m_dynust_last_stand(ibs)%ttilFstNMove/xminPerSimInt/simPerAgg)+1)) ! departure time
       AIN0(io,4)=ibs
       AIN1(io,1) = m_dynust_last_stand(ibs)%atime*AttScale ! arrival time for vehicles
	  ELSE
	   isd = IdTable(io+Epoc(isnow-1,1),2)
	   ibs = pathin(isd)%N(1)
       AIN(io,1)= MasterDest(m_dynust_last_stand(ibs)%jdest)
       AIN(io,2)= m_dynust_network_arc_nde(m_dynust_last_stand(ibs)%isec)%idnod
       AIN(io,3)= min(iti_nuEP,ifix(((max(m_dynust_last_stand(ibs)%stime,m_dynust_last_stand(ibs)%ttilFstNMove)/xminPerSimInt)-Epoc(isnow-1,2))/simPerAgg)+1)
       AIN(io,4)= ibs
       AIN0(io,1)=MasterDest(m_dynust_last_stand(ibs)%jdest) !destination
       AIN0(io,3)=m_dynust_last_stand(ibs)%isec !origin
       AIN0(io,2)=max(1,min(iti_nuEP,ifix(((max(m_dynust_last_stand(ibs)%stime,m_dynust_last_stand(ibs)%ttilFstNMOve)/xminPerSimInt)-Epoc(isnow-1,2))/simPerAgg)+1))
       AIN0(io,4)=ibs
       AIN1(io,1) = m_dynust_last_stand(ibs)%atime*AttScale ! arrival time for vehicles       
	  ENDIF
	ENDDO
   

	CALL IMSLSort(AIN1,EpocSize,1) ! arrival time

	thisone = AIN1(nint(EpocSize*ProjPeriod),1)/AttScale-(isnow-1)*EpocPeriod
	iti_nu = max(nint((EpocPeriod/xminPerSimInt)/simPerAgg),ifix((thisone/xminPerSimInt)/simPerAgg)+1)
	WRITE(5559,'("Ite #:", i3,", Epoch #:", i3,", Epoch Len:", f8.2,", ArrTime:", f8.2,", ProjPerd%:", f5.2,", ProjPerd(min):", f8.2)') iteration, isnow, float(EpocPeriod), thisone, ProjPeriod, (iti_nu*simPerAgg)*xminPerSimInt

    CALL MEMALLOCATE_SP(1)
    callksp1=.true.
	
	CALL TDSP_MAIN_MIVA(3,isnow,IVehType)	
	
	CALL IMSLSort(AIN0,EpocSize,4)
	
    DO io = 1, EpocSize
     iveh = AIN0(io,4)
     icsize = mintimedata(AIN0(io,1),AIN0(io,2))%size
     IF(io == 1) THEN
       mintimedata(AIN0(io,1),AIN0(io,2))%ct = mintimedata(AIN0(io,1),AIN0(io,2))%ct + 1
       IF(mintimedata(AIN0(io,1),AIN0(io,2))%ct > icsize) THEN
        ALLOCATE(mintemp(icsize,2))
        mintemp(1:icsize,:) = mintimedata(AIN0(io,1),AIN0(io,2))%link(1:icsize,:)
        mintimedata(AIN0(io,1),AIN0(io,2))%size = icsize + noofarcs/3
        DEALLOCATE(mintimedata(AIN0(io,1),AIN0(io,2))%link)
        ALLOCATE(mintimedata(AIN0(io,1),AIN0(io,2))%link(mintimedata(AIN0(io,1),AIN0(io,2))%size,2)) ! size has increased
        mintimedata(AIN0(io,1),AIN0(io,2))%link(1:icsize,:) = mintemp(i:icsize,:)
        DEALLOCATE(mintemp)
       ENDIF
       ng = mintimedata(AIN0(io,1),AIN0(io,2))%ct
       mintimedata(AIN0(io,1),AIN0(io,2))%link(ng,1) = AIN0(io,3) ! link #
       IF(m_dynust_last_stand(iveh)%atime > 0) THEN
        minTimedata(AIN0(io,1),AIN0(io,2))%link(ng,2) = (m_dynust_last_stand(iveh)%atime-m_dynust_last_stand(iveh)%ttilFstNMove+m_dynust_last_stand(iveh)%expense)*Attscale ! asg min TT       
       ELSE
        minTimedata(AIN0(io,1),AIN0(io,2))%link(ng,2) = (MaxIntervals*xminPerSimInt-m_dynust_last_stand(iveh)%ttilFstNMove+m_dynust_last_stand(iveh)%expense)*Attscale ! asg min TT             
       ENDIF
     ELSE ! io > 1
     IF(AIN0(io,1) /= AIN0(io-1,1).or.AIN0(io,2) /= AIN0(io-1,2).or.AIN0(io,3) /= AIN0(io-1,3)) THEN
       mintimedata(AIN0(io,1),AIN0(io,2))%ct = mintimedata(AIN0(io,1),AIN0(io,2))%ct + 1
       IF(mintimedata(AIN0(io,1),AIN0(io,2))%ct > icsize) THEN
        ALLOCATE(mintemp(icsize+noofarcs/3,2))
        mintemp(:,:) = 0
        mintemp(1:icsize,:) = mintimedata(AIN0(io,1),AIN0(io,2))%link(1:icsize,:)
        DEALLOCATE(mintimedata(AIN0(io,1),AIN0(io,2))%link)
        ALLOCATE(mintimedata(AIN0(io,1),AIN0(io,2))%link(icsize+noofarcs/3,2))
        mintimedata(AIN0(io,1),AIN0(io,2))%link(:,:) = 0
        mintimedata(AIN0(io,1),AIN0(io,2))%link(1:icsize,:) = mintemp(1:icsize,:)
        mintimedata(AIN0(io,1),AIN0(io,2))%size = icsize + noofarcs/3
        DEALLOCATE(mintemp)
       ENDIF
       ng = mintimedata(AIN0(io,1),AIN0(io,2))%ct

       IF(minTimedata(AIN0(io,1),AIN0(io,2))%link(ng,2) > 0.001.and.ain0(io,1) == 1.and.ain0(io,2) == 11) THEN
         print *,"error"
       ENDIF
       mintimedata(AIN0(io,1),AIN0(io,2))%link(ng,1) = AIN0(io,3)
       IF(m_dynust_last_stand(iveh)%atime > 0) THEN
         minTimedata(AIN0(io,1),AIN0(io,2))%link(ng,2) = (m_dynust_last_stand(iveh)%atime-m_dynust_last_stand(iveh)%ttilFstNMove+m_dynust_last_stand(iveh)%expense)*AttScale ! asg min TT       
       ELSE
         minTimedata(AIN0(io,1),AIN0(io,2))%link(ng,2) = (MaxIntervals*xminPerSimInt-m_dynust_last_stand(iveh)%ttilFstNMove+m_dynust_last_stand(iveh)%expense)*AttScale ! asg min TT       
       ENDIF
     ELSE
       IF(m_dynust_last_stand(iveh)%atime > 0) THEN
        IF(minTimedata(AIN0(io,1),AIN0(io,2))%link(mintimedata(AIN0(io,1),AIN0(io,2))%ct,2) > (m_dynust_last_stand(iveh)%atime-m_dynust_last_stand(iveh)%ttilFstNMove+m_dynust_last_stand(iveh)%expense)*AttScale) THEN 
          minTimedata(AIN0(io,1),AIN0(io,2))%link(mintimedata(AIN0(io,1),AIN0(io,2))%ct,2) =  (m_dynust_last_stand(iveh)%atime-m_dynust_last_stand(iveh)%ttilFstNMove+m_dynust_last_stand(iveh)%expense)*Attscale ! asg min TT       
        ENDIF
       ELSE
        IF(minTimedata(AIN0(io,1),AIN0(io,2))%link(mintimedata(AIN0(io,1),AIN0(io,2))%ct,2) > (MaxIntervals*xminPerSimInt-m_dynust_last_stand(iveh)%ttilFstNMove+m_dynust_last_stand(iveh)%expense)*AttScale) THEN 
          minTimedata(AIN0(io,1),AIN0(io,2))%link(mintimedata(AIN0(io,1),AIN0(io,2))%ct,2) =   (MaxIntervals*xminPerSimInt-m_dynust_last_stand(iveh)%ttilFstNMove+m_dynust_last_stand(iveh)%expense)*Attscale ! asg min TT       
        ENDIF       
       ENDIF

     ENDIF
     ENDIF
	ENDDO 
	
	CALL IMSLSort(AIN,EpocSize,4)
	
    vhpcect = 0
    DO ih = 1, EpocSize
      IF(m_dynust_last_stand(AIN(ih,4))%vehclass == 3) THEN ! UE vehicle
       IF(IVehType == 1) THEN
         IF(m_dynust_last_stand(AIN(ih,4))%vehtype == IVehType.or.(m_dynust_last_stand(AIN(ih,4))%vehtype == 2.and.abs(TollVoTA-TollVoTT) < 0.1).or.(m_dynust_last_stand(AIN(ih,4))%vehtype == 3.and.abs(TollVoTA-TollVoTH) < 0.1)) THEN
           AINPtr(AIN(ih,1))=AINPtr(AIN(ih,1))+1
         ENDIF
       ELSEIF(IVehType == 2) THEN
         IF(m_dynust_last_stand(AIN(ih,4))%vehtype == IVehType.or.(m_dynust_last_stand(AIN(ih,4))%vehtype == 3.and.abs(TollVoTT-TollVoTH) < 0.1)) THEN
           AINPtr(AIN(ih,1))=AINPtr(AIN(ih,1))+1
         ENDIF            
       ELSE
           AINPtr(AIN(ih,1))=AINPtr(AIN(ih,1))+1
       ENDIF
      ENDIF  
    ENDDO

 
5196    MostRCount = 1
        MostRcount1 = 1

! For each destination
    DO 100 j = 1, noof_master_destinations

       WRITE(6666,*) j
       close(6666)
       READ(6666,*) printstr1
       printstr1 = trim(printstr1)
       close(6666)
       result = SETCOLORRGB(Z'a00000')  
       result = RECTANGLE ($GFILLINTERIOR, 220,415,450,500)
       
       printstr1 = 'Now at dest  '//printstr1
       CALL printscreen(printstr1,280,440,10,10,300,10,1)

	   !CALL tdsp_core(3,j) !tdsp_core is either called from DynusT, or UE/SO assignment	


! For each origin
   DO 220 i = 1,noofnodes_org

! For each arrival time
    DO 200 t = 1, iti_nuEP

!   PROCESS SKIM INFO IF NECESSARY         
     IF(IVehType == 1) THEN
       IF(SkimFlag > 0.AND.iteration == iteMax) THEN
        DO imhere = backpointr(i),backpointr(i+1)-1
            icmov = m_dynust_network_arc_nde(imhere)%BackToForLink
            !CALL GetNewUEPath(1,icmov,i,j,t,tmpuepath,tmpnodsum,nnk,FoundFlag,iArrT,UECost,0) ! Get UE Cost Only
            CALL RETRIEVE_VEH_PATH_AStar_MIVA(1,icmov,ipinit,j,UECost,tmpuepath2,nnk) ! pass 1 to select the best path
            IF(UECOST < 10000) THEN
                InTime = ifix(((isnow-1)*iti_nuEP + t-1)*simPerAgg*xminPerSimInt/SkimOutInt)+1
                InZone = m_dynust_network_node_nde(i)%izone
                IF(InZone < 1) THEN ! THIS NODE DOES NOT HAVE ZONE MAPPING IN NETWORK.
                    WRITE(511,*) "ERROR IN NETWROK.DAT"
                    WRITE(511,*) "Each node need to belonw to one zone"
                    WRITE(511,*) "Please update network.dat"
                ELSEIF(UECost < 10000) THEN
                    skimData(InZone,InTime,1) = skimData(InZone,InTime,1) + 1
                    skimData(InZone,InTime,2) = skimData(InZone,InTime,2) + nint(UECost*AttScale)
                ENDIF
            ENDIF
        ENDDO
       ENDIF
     ENDIF

! -- assign paths to vehicles in this i,j,t
iCheckFinalVeh = 0

Select Case (AssMtd)

case (1) ! GFV

IF (MostRCount1 <= EpocSize) THEN !enter path assignment

!~~~~~~~~~~~~~~~~~
         ipass = 0

         DO Mostmp = MostRCount1, EpocSize
           ipick = 0
           igetout = 0
           ifrom = AIN(Mostmp,2) ! origin
           ito =   AIN(Mostmp,1) ! destination
           itime2 = min(iti_nuEP,AIN(Mostmp,3)) ! departure time
           idhere = AIN(Mostmp,4) ! Actual Veh ID
           IF(ito /= j) THEN
             igetout = 1 ! to indicate that gap out from destination loop
             exit
           ELSEIF(ito <= j.and.ifrom /= i) THEN
	         exit
           ELSEIF(ito <= j.and.ifrom <= i.and.itime2 /= t) THEN
		    exit
           ENDIF
         ENDDO
                  
         npathct = 0
         iseebestveh = 0

         
        IF(Mostmp > MostRCount1) THEN ! found at least one vehicle
         
        !CALL GetNewUEPath(AIN(Mostmp-1,4),m_dynust_last_stand(AIN(Mostmp-1,4))%isec,AIN(mostmp-1,2),AIN(Mostmp-1,1), min(iti_nuEP,AIN(Mostmp-1,3)),tmpuepath,tmpnodsum,nnk,FoundFlag,iArrT,UECost,1) ! Get UE Path
         CALL RETRIEVE_VEH_PATH_AStar_MIVA(AIN(Mostmp-1,4),m_dynust_last_stand((AIN(Mostmp-1,4)))%isec,ipinit,j,UECost,tmpuepath2,nnk) ! pass 1 to select the best path
          DO IH2 = 1, NNK
            tmpuepath(ih2) = tmpuepath2(NNK-ih2+1)
          ENDDO
          SNodeSum = sum(tmpuepath(1:nnk))
          iSNodeSum = SNodeSum
          
          ipass = 1
          totalflow = 0.0
20        DO is = MostRCount1, Mostmp-1     ! process paths over vehicles
          ifrom = m_dynust_last_stand(AIN(is,4))%isec           ! generation link
          ito = AIN(is,1)                   ! destination     
          itime2 = min(iti_nuEP,AIN(is,3))  ! departure time 
          iveh = AIN(is,4)                  ! veh ID

          IF(m_dynust_last_stand(iveh)%vehtype == IVehType) THEN ! assign the matching vehicle type only

           i2bfound = 0
           DO iseemove = 1,  minTimedata(ito,itime2)%size
             IF(ifrom == minTimedata(ito,itime2)%link(iseemove,1)) THEN
              ilinkto = iseemove
              i2bfound = 1
              exit
             ENDIF
           ENDDO
           IF(i2bfound == 0) THEN
             WRITE(911,*) "error in finding correct link"
             STOP
           ENDIF
          
          IF(m_dynust_last_stand(iveh)%atime > 0) THEN
            thisnew1 = max(0.0, m_dynust_last_stand(iveh)%atime-max(m_dynust_last_stand(iveh)%stime,m_dynust_last_stand(iveh)%ttilFstNMove)+m_dynust_last_stand(iveh)%expense)
          ELSE
            thisnew1 = max(0.0, MaxIntervals*xminPerSimInt-max(m_dynust_last_stand(iveh)%stime,m_dynust_last_stand(iveh)%ttilFstNMove)+m_dynust_last_stand(iveh)%expense)
          ENDIF
          thisnew2 = minTimedata(ito,itime2)%link(ilinkto,2)/AttScale
          
          IF(m_dynust_last_stand(iveh)%atime > 0.or.(m_dynust_last_stand(iveh)%atime < 0.01.and.m_dynust_last_stand(iveh)%expense > 99)) THEN ! need this condition because some vehicles may not have arrived at the destination so atime = 0
            converg(iteration,(isnow-1)*iti_nuEP+itime2,1) = converg(iteration,(isnow-1)*iti_nuEP+itime2,1) + thisnew1
            converg(iteration,(isnow-1)*iti_nuEP+itime2,2) = converg(iteration,(isnow-1)*iti_nuEP+itime2,2) + thisnew2

            IF(is == MostRCount1) THEN ! IF this is the first vehicle
              npathct = npathct + 1
              Kpset(npathct)%ct = Kpset(npathct)%ct + 1 ! counter
              totalflow = totalflow + 1              
              kpset(npathct)%p(Kpset(npathct)%ct) = AIN(is,4) ! assign the veh id
              kpset(npathct)%t(Kpset(npathct)%ct) = thisnew1 ! record the travel time for this vehicle

              kpset(npathct)%point = AIN(is,4) ! pointer to vehicle for retrieving paths later on
              kpset(npathct)%nodesum = nodesum(outtoin(AIN(is,4))) ! record the path node sum
              kpset(npathct)%ttime = kpset(npathct)%ttime + thisnew1 ! accumulate travel time
              kpset(npathct)%gap  = max(0.0,(kpset(npathct)%ttime - Kpset(npathct)%ct*minTimedata(AIN(is,1),AIN(is,3))%link(ilinkto,2)/AttScale))/(Kpset(npathct)%ct*minTimedata(AIN(is,1),AIN(is,3))%link(ilinkto,2)/AttScale)
            
            ELSE ! IF this is the 1+ vehicle
              iseethis = 0
              DO 30 ig = 1, npathct ! loop over existing paths
                IF(nodesum(outtoin(AIN(is,4))) == kpset(ig)%nodesum) THEN ! found exist path
                   iseethis = 1
                   IF(kpset(ig)%ct == kpset(ig)%psize) THEN ! need to REALlocate the array size 
                     tmparrayp(1:kpset(ig)%ct) = kpset(ig)%p(1:kpset(ig)%ct)
                     tmparrayt(1:kpset(ig)%ct) = kpset(ig)%t(1:kpset(ig)%ct)                     
                     DEALLOCATE(kpset(ig)%p)
                     DEALLOCATE(kpset(ig)%t)
                     kpset(ig)%psize = kpset(ig)%psize + kpsettmpsize                     
                     ALLOCATE(kpset(ig)%p(kpset(ig)%psize))
                     ALLOCATE(kpset(ig)%t(kpset(ig)%psize))
                     kpset(ig)%p(:) = 0
                     kpset(ig)%t(:) = 0
                     kpset(ig)%p(1:kpset(ig)%ct) = tmparrayp(1:kpset(ig)%ct)
                     kpset(ig)%t(1:kpset(ig)%ct) = tmparrayt(1:kpset(ig)%ct)                     
                   ENDIF
                   
                   kpset(ig)%ct = Kpset(ig)%ct + 1 ! counter                   
                   totalflow = totalflow + 1                   
                   kpset(ig)%p(Kpset(ig)%ct) = AIN(is,4)                   
                   toseethistime1 = max(0.0, m_dynust_last_stand(AIN(is,4))%atime-max(m_dynust_last_stand(AIN(is,4))%stime,m_dynust_last_stand(AIN(is,4))%ttilFstNMove)+m_dynust_last_stand(is)%expense)

                   kpset(ig)%t(Kpset(ig)%ct) = toseethistime1
                   kpset(ig)%ttime = kpset(ig)%ttime + toseethistime1 ! accumulate travel time

                   IF(iseebestveh > 0) THEN ! already found the best vehicle
                    toseethistime2 = max(0.0, m_dynust_last_stand(AIN(iseebestveh,4))%atime-max(m_dynust_last_stand(AIN(iseebestveh,4))%stime,m_dynust_last_stand(AIN(iseebestveh,4))%ttilFstNMove)+m_dynust_last_stand(iseebestveh)%expense)
                   ELSE
                    IF(kpset(ig)%nodesum == tmpnodsum) THEN ! the existing path is the shortest
                     iseebestveh = is
                     toseethistime2 = minTimedata(AIN(is,1),AIN(is,3))%link(ilinkto,2)/AttScale
                    ELSE
                     toseethistime2 = minTimedata(AIN(is,1),AIN(is,3))%link(ilinkto,2)/AttScale
                    ENDIF                     
                    kpset(ig)%gap  = max(0.0,(kpset(ig)%ttime - Kpset(ig)%ct*minTimedata(AIN(is,1),AIN(is,3))%link(ilinkto,2)/AttScale))/(Kpset(ig)%ct*minTimedata(AIN(is,1),AIN(is,3))%link(ilinkto,2)/AttScale)
                   ENDIF
                   go to 2100
                ENDIF
30             CONTINUE
              
               2100 CONTINUE
                IF(iseethis == 0) THEN ! found a new path within the existing vehicles
                   npathct = npathct + 1
                   kpset(npathct)%ct = kpset(npathct)%ct + 1 ! counter
                   totalflow = totalflow + 1                   
                   kpset(npathct)%p(kpset(npathct)%ct) = AIN(is,4)                   
                   thisistime = max(0.0, m_dynust_last_stand(AIN(is,4))%atime-max(m_dynust_last_stand(AIN(is,4))%stime,m_dynust_last_stand(AIN(is,4))%ttilFstNMove)+m_dynust_last_stand(is)%expense)                         
                   kpset(npathct)%t(Kpset(npathct)%ct) = thisistime
                   kpset(npathct)%point = AIN(is,4) ! pointer to vehicle for retrieving paths later on
                   kpset(npathct)%nodesum = nodesum(outtoin(AIN(is,4))) ! record the path node sum

                   IF(thisistime < 0) THEN
                      print *, "error in thisistime"
                   ENDIF
                   kpset(npathct)%ttime = kpset(npathct)%ttime + thisistime ! accumulate travel time
!                  kpset(npathct)%gap  = max(0.0,(kpset(npathct)%ttime - Kpset(npathct)%ct*UECost)/(Kpset(npathct)%ct*UECost))
                   kpset(npathct)%gap  = max(0.0,(kpset(npathct)%ttime - Kpset(npathct)%ct*minTimedata(AIN(is,1),AIN(is,3))%link(ilinkto,2)/AttScale))/(Kpset(npathct)%ct*minTimedata(AIN(is,1),AIN(is,3))%link(ilinkto,2)/AttScale)
                ENDIF        
            ENDIF
          ENDIF !atime(AIN(is,4)) > 0
         ENDIF
21       ENDDO ! over all vehicles
         
         IF(npathct > 0) THEN
           icshort = 0
           ttmin = 10000000.0
           ittmin = 0
           isfound = 0
           DO ih = 1, npathct 
             kpset(ih)%ttime = kpset(ih)%ttime/kpset(ih)%ct ! convert to average cost

             IF(kpset(ih)%ttime < ttmin) THEN
                ittmin = ih
                ttmin = kpset(ih)%ttime
             ENDIF

             IF(iSNodeSum == kpset(ih)%NodeSum) icshort = ih ! the SP is is one of the existing one

! The following is the sort vehicles according to the travel time for later assignment purposes
	         ALLOCATE(GVs(kpset(ih)%ct,2))
	         GVs(1:kpset(ih)%ct,2) = kpset(ih)%p(1:kpset(ih)%ct)
	         DO ib = 1, kpset(ih)%ct
	           GVs(ib,1) = nint(kpset(ih)%t(ib)*100.0)
	         ENDDO
	         CALL IMSLSort(GVs,kpset(ih)%ct,2) ! sort vehicles according to the travel time
	         kpset(ih)%p(1:kpset(ih)%ct) = GVs(1:kpset(ih)%ct,2)
             kpset(ih)%t(1:kpset(ih)%ct) = GVs(1:kpset(ih)%ct,1)/100.0
	         DEALLOCATE(GVs)	         
           ENDDO !DO ih = 1, npathct 

           ! define the negative and positive path set           
           
           IF(icshort == 0) THEN ! SP not found in existing set, new SP added to the path set
             npathct = npathct + 1
             kpset(npathct)%ct = 0
             kpset(npathct)%point = 0
             kpset(npathct)%nodesum = iSNodeSum
             kpset(npathct)%ttime = min(UECost,kpset(ittmin)%ttime*0.99) ! Set travel time for the SP !!!!!
             kpset(npathct)%gap = 0
             kpset(npathct)%status = .true.
             kpset(npathct)%p(:) = 0
             kpset(npathct)%t(:) = 0
             DO it2 = 1, npathct
               kpset(it2)%gap = (kpset(it2)%ttime-kpset(npathct)%ttime)/kpset(npathct)%ttime ! reset the min travel time
               converg(iteration,(isnow-1)*iti_nuEP+itime2,1) = converg(iteration,(isnow-1)*iti_nuEP+itime2,1) + kpset(it2)%ttime*kpset(it2)%ct
               converg(iteration,(isnow-1)*iti_nuEP+itime2,2) = converg(iteration,(isnow-1)*iti_nuEP+itime2,2) + kpset(ittmin)%ttime*kpset(it2)%ct
             ENDDO
           ELSE ! SP is one of the existing paths set the SP time be the minimal of the UECost and min existing time
             kpset(icshort)%ttime = kpset(ittmin)%ttime*0.99 ! Set travel time for the SP !!!!!
             DO it2 = 1, npathct
               kpset(it2)%gap = (kpset(it2)%ttime-kpset(icshort)%ttime)/kpset(icshort)%ttime ! reset the min travel time
               converg(iteration,(isnow-1)*iti_nuEP+itime2,1) = converg(iteration,(isnow-1)*iti_nuEP+itime2,1) + kpset(it2)%ttime*kpset(it2)%ct
               converg(iteration,(isnow-1)*iti_nuEP+itime2,2) = converg(iteration,(isnow-1)*iti_nuEP+itime2,2) + kpset(ittmin)%ttime*kpset(it2)%ct
             ENDDO
           ENDIF

! Starting assignment
           IF(npathct > 1) THEN ! IF more than one path exists --- assignement takes place for more than one path
           ALLOCATE(pathorder(npathct,2))

! Sort paths according to the average travel time
           DO ihh = 1, npathct
             pathorder(ihh,1) = nint(kpset(ihh)%ttime*1000.0)
             pathorder(ihh,2) = ihh
           ENDDO
           CALL IMSLSort(pathorder,npathct,2) ! sort paths according to avg travel time

           factor = 0
           DO isee = 1, npathct
             factor = factor + kpset(isee)%gap*kpset(isee)%ct
           ENDDO

           factor = factor/totalflow
           factor = factor**min(1.0,(0.7+0.3*iteration/iteMax)) ! scale up
!          factor = min(1.0,factor)
           factor = min(1.0/((mod(iteration,(iteMax+1)/itersub))),factorcap,factor) ! used prior to Jan 2011
             
           
           IF(factor < 0.or.factor > 1.0) print *, "error in factor"

           
           mose = 0           
           iseecutoff_path = npathct

           rrs = ranxy(9)
           IF (rrs <= (factor*totalflow-ifix(factor*totalflow))) THEN
             itoasign = ifix(factor*totalflow)+1
           ELSE
             itoasign = ifix(factor*totalflow)
           ENDIF
           
           IF(itoasign > 0) THEN ! assign only IF itoasign > 0
           DO iu = npathct, 2, -1
             IF((mose + kpset(pathorder(iu,2))%ct) < itoasign.and.iu > 2) THEN
               mose = mose + kpset(pathorder(iu,2))%ct
               kpset(pathorder(iu,2))%status = .false.
             ELSE
               iseecutoff_path = iu ! this is the cutoff point for the negative set
               iseecutoff_veh = min(kpset(pathorder(iu,2))%ct,itoasign - mose) ! this is the cutoff point for the vehicles in negative set
               kpset(pathorder(iu,2))%status = .false.               
               exit
             ENDIF
           ENDDO
           
           ! negative path set
           totalneggap = 0
           DO icho = npathct, iseecutoff_path, -1 !negative set
             totalneggap = totalneggap + kpset(pathorder(icho,2))%gap
           ENDDO
           DO icho = npathct, iseecutoff_path, -1
             kpset(pathorder(icho,2))%assignratio = -1*kpset(pathorder(icho,2))%gap / totalneggap
           ENDDO
           
           ! positive path set  
           totalposgap = 0
           DO icho = iseecutoff_path-1, 1, -1 !positive set
             totalposgap = totalposgap + ((-1)*(kpset(pathorder(icho,2))%gap-    kpset(pathorder(iseecutoff_path,2))%gap))**scale
           ENDDO
           IF(totalposgap > 0) THEN
             DO icho = iseecutoff_path-1, 1, -1
                 kpset(pathorder(icho,2))%assignratio = ((-1)*(kpset(pathorder(icho,2))%gap - kpset(pathorder(iseecutoff_path,2))%gap))**scale / totalposgap
             ENDDO
           ENDIF

           ! Starting the assignment
           ! Build the assignment cumulative prob function
           IF (iseecutoff_path-1 >= 1) THEN
             ALLOCATE(probtemp(iseecutoff_path-1))
             ALLOCATE(assgtemp(iseecutoff_path-1))
             ALLOCATE(assged(iseecutoff_path-1))
             probtemp(:) = 0
             assgtemp(:) = 0
             assged(:) = 0
             
             
             DO iou = 1, iseecutoff_path-1 ! build the cumulative prob fuction
               IF(iou == 1) THEN
                  probtemp(iou) = kpset(pathorder(iou,2))%assignratio
                  assgtemp(iou) = itoasign*kpset(pathorder(iou,2))%assignratio
               ELSE
                  probtemp(iou) = probtemp(iou-1)+kpset(pathorder(iou,2))%assignratio
                  assgtemp(iou) = itoasign*kpset(pathorder(iou,2))%assignratio
               ENDIF
             ENDDO
             
             probtemp(iseecutoff_path-1) = 1
              
             iassigned = 0
             mcurrentpath = 1
             iknow = 0
             iproc = 0

             DO icho = npathct, iseecutoff_path, -1 ! for all negative set paths
             
              IF(icho > iseecutoff_path) THEN
                i2cling =  kpset(pathorder(icho,2))%ct
              ELSE ! last path
                i2cling = max(1, iseecutoff_veh)
              ENDIF
              i2flor = 1              
              
             
! Start to assignment vehicles from here
              IF(i2cling > 0) THEN ! at least assign one vehicle             
              DO 40 iget = i2cling, i2flor, -1 ! for all vehicles in the negative set paths
                iveh =  kpset(pathorder(icho,2))%p(iget) ! iveh is ext veh #
                IF(iveh < 1) EXIT !!!! NEED TO FURTHER VERIFY THIS CONDITION
                totalvhpcect = totalvhpcect + 1

                r1 = ranxy(17) 
                
                DO ms = 1, iseecutoff_path-1
                  IF(probtemp(ms) >= r1) exit
                ENDDO
                iseethis = pathorder(ms,2)
                
                iassigned = iassigned + 1
                assged(ms) = assged(ms) + 1
                            
                IF(m_dynust_last_stand(iveh)%expense > 99.or.kpset(iseethis)%point == 0) THEN ! the selected path is the newly solved SP so pointe not available
                  tmpasspath(1) = pathin(outtoin(iveh))%N(3) ! assign the first node to the temp array
                  newnnk = nnk + 1 ! including the upsm node
                  tmpasspath(2:nnk+1) = m_dynust_network_node_nde(tmpuepath(1:nnk))%IntoOutNodeNum ! tmpuepath is the temp path from the latest SP
                ELSE
                  newnnk =   pathin(outtoin(kpset(iseethis)%point))%N(2)
                  tmpasspath(1:newnnk) = pathin(outtoin(kpset(iseethis)%point))%N(3:newnnk+3)
                ENDIF

                 
                IF(newnnk > pathin(outtoin(iveh))%N(2)) THEN ! IF the assigned path is longer the existing path
                  isee = pathin(outtoin(iveh))%N(1) ! keep original id
                  isee2 = pathin(outtoin(iveh))%N(3) ! keep origin upstm node
		          DEALLOCATE(pathin(outtoin(iveh))%N)
		          ALLOCATE(pathin(outtoin(iveh))%N(newnnk+3))
                  pathin(outtoin(iveh))%N(1) = isee 
                  pathin(outtoin(iveh))%N(2) = newnnk
		          pathin(outtoin(iveh))%N(3:newnnk+2) = tmpasspath(1:newnnk)
		        ELSE
                  pathin(outtoin(iveh))%N(3:newnnk+2) = 0
		          pathin(outtoin(iveh))%N(2) = newnnk
		          pathin(outtoin(iveh))%N(3:newnnk+2) = tmpasspath(1:newnnk)
                ENDIF
                
                IF(iassigned > itoasign) go to 2690
40              ENDDO  !DO iget = i2cling, i2flor, -1

              ENDIF !IF(i2cling > 0 THEN
             ENDDO !DO icho = npathct, iseecutoff_path, -1

            2690 CONTINUE

           ENDIF ! IF (iseecutoff_path-1 >= 1) THEN
           ENDIF ! itoasign > 0
           ENDIF ! npathct > 1 ! assignment above
           
         ENDIF !IF(npathct > 0) THEN    
       ENDIF ! IF found at least one vehicle
                  

         IF(ALLOCATED(probtemp)) DEALLOCATE(probtemp)
         IF(ALLOCATED(assgtemp)) DEALLOCATE(assgtemp)
         IF(ALLOCATED(assged)) DEALLOCATE(assged)
         IF(ALLOCATED(pathorder)) DEALLOCATE(pathorder)
         MostRCount1 = Mostmp
         IF(npathct > 0) THEN
         kpset(:)%ct = 0
         kpset(:)%point = 0
         kpset(:)%nodesum = 0
         kpset(:)%ttime = 0.0
         kpset(:)%gap = 0.0
         kpset(:)%status = .true.
         kpset(:)%assignratio = 0.0
         ! don't reset psize
         DO is = 1, iteMax
           kpset(is)%p(:) = 0
           kpset(is)%t(:) = 0
         ENDDO
         ENDIF
         
ENDIF !(MostRCount1 <= EpocSize) THEN !enter path assignment

!~~~~~~~~~~~~~~~~~
case (0) !MSA
iCheckFinalVeh = 0

IF (MostRCount <= EpocSize) THEN !enter path assignment
	  DO Mos = MostRCount, EpocSize

       ipick = 0
       ifrom2 = m_dynust_last_stand(AIN(Mos,4))%isec    ! generation link            
       ifrom = AIN(Mos,2)           ! not used any more
       ito =   AIN(Mos,1)           ! destination
       itime2 = AIN(Mos,3)          ! departure time
       IF(itime2 > iti_nuEP) THEN
         exit ! vehicles in demandmidcounter but larger than justveh
       ENDIF

       IF(ito /= j) THEN
          MostRcount = Mos
          iCheckFinalVeh = 1 ! not finished checking
          exit
       ELSEIF(ito <= j.and.ifrom /= i) THEN
          MostRcount = Mos
          iCheckFinalVeh = 1
	      exit
       ELSEIF(ito <= j.and.ifrom <= i.and.itime2 /= t) THEN
          MostRcount = Mos 
          iCheckFinalVeh = 1	      
		  exit
       ENDIF
       
	   IF(ito /= j) THEN
	      ito = j
		  MasterDest(m_dynust_last_stand(AIN(Mos,4))%jdest) = j
		  ItoVio = ItoVio + 1
	   ELSEIF(ifrom /= i) THEN
	      ifrom = i
          m_dynust_last_stand(AIN(Mos,4))%isec=m_dynust_network_arc_nde(BackPointr(ifrom))%BackToForLink
		  IfromVio = IfromVio + 1
	   ELSEIF(itime2 /= t) THEN
	      itime2 = t
		  ItimeVio = ItimeVio + 1
	   ENDIF

909	   MostRcount = Mos
	   AINStatus(mos) = 1
         
	       IF(IVehType == 1) THEN
	         IF(m_dynust_last_stand(AIN(Mos,4))%vehtype == 1) MosCa = MosCa + 1
	       ELSEIF(IVehType == 2) THEN
	         IF(m_dynust_last_stand(AIN(Mos,4))%vehtype == 2) MosCt = MosCt + 1
           ELSE
	         IF(m_dynust_last_stand(AIN(Mos,4))%vehtype == 3) MosCh = MosCh + 1           
	       ENDIF
	    IF(IVehType == 1) THEN
	      IF(EpocDrop > factor*AutoCTL) exit ! Chiu 20077
	    ELSEIF(IVehType == 2) THEN ! truck
	      IF(EpocDropT > factor*TruckCTL) exit ! Chiu 20077
        ELSE
	      IF(EpocDropH > factor*HOVCTL) exit ! Chiu 20077        
	    ENDIF


	    IF((IVehType == 1.and.m_dynust_last_stand(AIN(Mos,4))%vehtype == 1).or.(IVehType == 2.and.m_dynust_last_stand(AIN(Mos,4))%vehtype == 2).or.(IVehType == 3.and.m_dynust_last_stand(AIN(Mos,4))%vehtype == 3))THEN

           IF(IVehType == 1) THEN
             shada = min(1.0,(factor*MosCa/max(1,EpocDrop )))
           ELSEIF(IVehType == 2) THEN
             shada = min(1.0,(factor*MosCt/max(1,EpocDropT)))
           ELSE
             shada = min(1.0,(factor*MosCh/max(1,EpocDropH)))
           ENDIF
 
         !CALL GetNewUEPath(AIN(Mos,4),m_dynust_last_stand((AIN(Mos,4)))%isec,AIN(mos,2),AIN(Mos,1), min(iti_nuEP,AIN(Mos,3)),tmpuepath,tmpnodsum,nnk,FoundFlag,iArrT,UECost,1) ! Get UE Path         
         CALL RETRIEVE_VEH_PATH_AStar_MIVA(AIN(mos,4),m_dynust_last_stand((AIN(mos,4)))%isec,ipinit,j,UECost,tmpuepath2,nnk) ! pass 1 to select the best path
         IF(uecost > 99) THEN
           CONTINUE
         ENDIF
         
         IF(m_dynust_last_stand(AIN(mos,4))%atime > 0) THEN

! ============ 
! find correct link for converg calculation 
           DO iseemove = 1,  minTimedata(ito,itime2)%size
             IF(ifrom2 == minTimedata(ito,itime2)%link(iseemove,1)) THEN
              ilinkto = iseemove
              exit
             ENDIF
           ENDDO

          thisis1 =  max(0.0, m_dynust_last_stand(AIN(mos,4))%atime-max(m_dynust_last_stand(AIN(mos,4))%stime,m_dynust_last_stand(AIN(mos,4))%ttilFstNMove)+m_dynust_last_stand(mos)%expense)
          thisis2 = minTimedata(ito,itime2)%link(ilinkto,2)/AttScale
          IF(thisis1 < thisis2) THEN
            WRITE(511,*)" this time error,i,j,t",i,j,t
          ENDIF
          converg(iteration,(isnow-1)*iti_nuEP+itime2,1) = converg(iteration,(isnow-1)*iti_nuEP+itime2,1) + thisis1
          converg(iteration,(isnow-1)*iti_nuEP+itime2,2) = converg(iteration,(isnow-1)*iti_nuEP+itime2,2) + thisis2
         ENDIF

         r2 = ranxy(18) 

		 IF(r2 <= factor.and.m_dynust_last_stand((AIN(mos,4)))%vehclass == 3) THEN ! assign this vehicle with a new path
		  FoundFlag = .False.
          totalvhpcect = totalvhpcect + 1
          
         !CALL GetNewUEPath(AIN(mos,4),m_dynust_last_stand((AIN(mos,4)))%isec,ifrom,ito,itime2,tmpuepath,tmpnodsum,nnk,FoundFlag,iArrT,UECost,1)
         ipinit = 1
         CALL RETRIEVE_VEH_PATH_AStar_MIVA(AIN(mos,4),m_dynust_last_stand((AIN(mos,4)))%isec,ipinit,j,generalized_cost,tmpuepath,nnk) ! pass 1 to select the best path
         !GETNEWUEPATH(iveh,icu1,i,j,t,tmpuepath,tmpnodsum,nnk,Foundflag,iArrTime,generalized_cost,pathflag) 

         IF(uecost > 99) THEN
           CONTINUE
         ENDIF
         

          IF(.not.FoundFlag) THEN
             WRITE(911,*) 'error did not find new UE path'
			 WRITE(911,*) 'For veh id', AIN(Mos,4)
             STOP
	      ENDIF

!writing out external node number
          IF(nnk > maxnu_pa) THEN
            WRITE(911,*) "Error! Please increase maxnu_pa in PARAMETER.dat"
            STOP
          ENDIF
          DO is = 1, nnk
            tmpuepath(is) = m_dynust_network_node_nde(tmpuepath(is))%IntoOutNodeNum
          ENDDO

         IF((nnk+2) > pathin(vehmap(AIN(Mos,4)))%N(2)) THEN
          isee = pathin(vehmap(AIN(Mos,4)))%N(1)
          isee2 = pathin(vehmap(AIN(Mos,4)))%N(3)
		  DEALLOCATE(pathin(vehmap(AIN(Mos,4)))%N)
		  ALLOCATE(pathin(vehmap(AIN(Mos,4)))%N(nnk+3))
          pathin(vehmap(AIN(Mos,4)))%N(1) = isee 
          pathin(vehmap(AIN(Mos,4)))%N(2) = nnk+1
          pathin(vehmap(AIN(Mos,4)))%N(3) = isee2
		  pathin(vehmap(AIN(Mos,4)))%N(4:nnk+3) = tmpuepath(1:nnk)
		 ELSE
          pathin(vehmap(AIN(Mos,4)))%N(4:pathin(vehmap(AIN(Mos,4)))%N(2)+2) = 0
		  pathin(vehmap(AIN(Mos,4)))%N(2) = nnk+1
		  pathin(vehmap(AIN(Mos,4)))%N(4:nnk+3) = tmpuepath(1:nnk)
         ENDIF

          IF(IVehType == 1) THEN
             Epocdrop = Epocdrop + 1
          ELSEIF(IVehType == 2) THEN
            EpocDropT = EpocDropT + 1
          ELSE
            EpocDropH = EpocDropH + 1
          ENDIF  
          tmpuepath(1:maxnu_pa) = 0

		ENDIF  
		ENDIF

      IF(MostRCount == justveh.and.iCheckFinalVeh == 0) go to 2222 ! last vehicle processed
	  ENDDO ! mos loop

ENDIF ! enter path assignment


END SELECT


200   CONTINUE ! soint loop

2222  CONTINUE


      IF(MostRCount == justveh.and.iCheckFinalVeh == 0) go to 3333 ! last vehicle processed
220   CONTINUE ! origin loop 

3333 CONTINUE

!    PRINT OUT SKIMDATA IF NEEDED
     IF(IVehType == 1) THEN
      IF(SkimFlag > 0.AND.iteration == iteMax) THEN
       DO MT = 1, nint(SimPeriod/EpocNum)/skimOutInt
!        WRITE(3300,'(" TIME = ",f7.1)')  (((isnow-1)*iti_nuEP)*simPerAgg*xminPerSimInt + MT*skimOutInt)
         DO M2T = 1, noof_master_destinations
           IF(skimdata(M2T,MT,1) > 0) THEN
             WRITE(3300,'(F7.1,2I7,F12.2)') (((isnow-1)*iti_nuEP)*simPerAgg*xminPerSimInt + MT*skimOutInt), M2T, J, skimdata(M2T,MT,2)/AttScale/skimdata(M2T,MT,1)
           ELSE ! this OD pair does not existing value, use neighboring zone as the approximation. This needs to be improved later on.
             IF(M2T == 1) THEN
                KS = 1
                DO WHILE (skimdata(KS,MT,1) == 0.AND.KS < noof_master_destinations)
                    KS = KS + 1
                ENDDO
             ELSE
                DO KS = MAX(M2T-10,1),MIN(M2T+10,noof_master_destinations)
                    IF(skimdata(KS,MT,1) > 0) EXIT
                ENDDO
             ENDIF
             IF(KS > 0.AND.KS <= noof_master_destinations.AND.skimdata(KS,MT,1) > 0) THEN
                WRITE(3300,'(F7.1,2I7,F12.2)') (((isnow-1)*iti_nuEP)*simPerAgg*xminPerSimInt + MT*skimOutInt), M2T, J, skimdata(KS,MT,2)/AttScale/skimdata(KS,MT,1)
             ELSE
                WRITE(3300,'(F7.1,2I7,F12.2)') (((isnow-1)*iti_nuEP)*simPerAgg*xminPerSimInt + MT*skimOutInt), M2T, J, 0
             ENDIF
           ENDIF
         ENDDO
       ENDDO
      ENDIF
     ENDIF
100   CONTINUE ! destination loop

1001   FORMAT(2f12.4)
1002   FORMAT(i4,150i7)


      close(9999)
      DEALLOCATE(AIN)
      DEALLOCATE(AIN0)
      DEALLOCATE(AIN1)      
      DEALLOCATE(AINStatus)
   ENDIF

   IF(minTimeAlloc == .true.) THEN
      DO i = 1, noof_master_destinations
      DO j = 1, iti_nuEP
         DEALLOCATE(minTimedata(i,j)%link)
      ENDDO
      ENDDO
      minTimeAlloc = .false.
   ENDIF

   IF(callksp1) THEN
     CALL DEALLOCATE_SP1
     !CALL DEALLOCATE_SP2
     callksp1=.false.
   ENDIF

10000 CONTINUE ! Epoch loop

20000  CONTINUE


! REWRITE SKIM.DAT TO TIME-ORIGIN-DESTINATION HIERARCHICAL FORMAT
    IF(skimFlag > 0.AND.iteration == iteMax) THEN
        REWIND(3300)
        OPEN(FILE = 'skimtmp.dat',unit = 3301, status ='unknown')
        DO M1 = 1, EpocNum
            DO M2 = 1, noof_master_destinations
                DO M3 = 1, nint(SimPeriod/EpocNum)/skimOutInt
                    DO M4 = 1, noof_master_destinations
                        mk = (m1-1)*nint(SimPeriod/EpocNum)/skimOutInt+m3
                        READ(3300,*) s1, i2, i2, skimtmp(mk, m2, m4)
                    ENDDO 
                ENDDO
            ENDDO
        ENDDO
        DO M1 = 1, EpocNum
            DO M3 = 1, nint(SimPeriod/EpocNum)/skimOutInt
                DO M2 = 1, noof_master_destinations
                    DO M4 = 1, noof_master_destinations
                        mk = (m1-1)*nint(SimPeriod/EpocNum)/skimOutInt+m3
                        WRITE(3301,'(F9.1,2I6,F10.2)') (((M1-1)*iti_nuEP)*simPerAgg*xminPerSimInt + M3*skimOutInt),M4,M2,skimtmp(mk,m2,m4)
                    ENDDO 
                ENDDO
            ENDDO
        ENDDO
        CLOSE(3300)
        CLOSE(3301)
        istat = SYSTEM('del .\skim.dat')
        istat = SYSTEM('rename .\skimtmp.dat skim.dat')
   
    ENDIF
      DEALLOCATE(Epoc)

      IF(ItoVio > 0) WRITE(911,*) 'ItoVio   = ', ItoVio
      IF(IfromVio > 0) WRITE(911,*) 'IfromVio = ', IfromVio
      IF(ItimeVio > 0) WRITE(911,*) 'ItimeVio = ', ItimeVio

	  close(989)
	  DO imust = 1, demandmidcounter
	    WRITE(6099,'(i9,1000i7)') pathin(idtable(imust,2))%N(1),pathin(idtable(imust,2))%N(2)-1,(pathin(idtable(imust,2))%N(is),is=4,pathin(idtable(imust,2))%N(2)+2)
	  ENDDO
      close(6099)

! Compute the gap function value
      ttactual = 0
      ttsp = 0
      DO is = 1, iti_nuEP*EpocNum
        IF(converg(iteration,is,2) > 0.0001) THEN
          gap(is) = (converg(iteration,is,1)-converg(iteration,is,2))/converg(iteration,is,2)
          ttactual = ttactual + converg(iteration,is,1)
          ttsp = ttsp + converg(iteration,is,2)
        ELSE
          gap(is) = 0        
        ENDIF
      ENDDO

      totalgap = (ttactual-ttsp)/ttsp

       IF(ttsp < minconvggap) THEN ! record the iteration with min gap value
          minconvggap = ttsp
          minconvgite = iteration
       ENDIF
      IF(iteration > 1) THEN
        IF(totalgap <= no_via/10000.0) THEN
          reach_converg = .true.
        ENDIF
      ENDIF

	  DO is = 1, demandmidcounter
         DEALLOCATE(pathin(is)%N) 
	  ENDDO 
	  DEALLOCATE(pathin)
	  DEALLOCATE(IDTable)
	  DEALLOCATE(VehMap)
	  DEALLOCATE(intoout)
	  DEALLOCATE(outtoin)
	  IF(ALLOCATED(ain))        DEALLOCATE(AIN)
	  IF(ALLOCATED(ain0))       DEALLOCATE(AIN0)
	  IF(ALLOCATED(ain1))       DEALLOCATE(AIN1)
	  IF(ALLOCATED(ainstatus))  DEALLOCATE(AINstatus)
	  DEALLOCATE(AINPtr)
	  DEALLOCATE(tmpuepath)
	  DEALLOCATE(tmpuepath2)
      DEALLOCATE(tmpasspath)
      DEALLOCATE(tmparrayp)
      DEALLOCATE(tmparrayt)

!	  IF(ALLOCATED(converg)) DEALLOCATE(converg)
	  DEALLOCATE(gap)
      DO is = 1, max(iteMax*10,200)
        DEALLOCATE(kpset(is)%p)
        DEALLOCATE(kpset(is)%t)        
      ENDDO 
      DEALLOCATE(kpset)
      DEALLOCATE(minTimedata)
	  IF(ALLOCATED(mintemp))    DEALLOCATE(mintemp)     
	  IF(ALLOCATED(nodesum))    DEALLOCATE(nodesum)
	  IF(ALLOCATED(epoc))       DEALLOCATE(epoc)
      IF(ALLOCATED(probtemp))   DEALLOCATE(probtemp)
      IF(ALLOCATED(assgtemp))   DEALLOCATE(assgtemp)
      IF(ALLOCATED(assged))     DEALLOCATE(assged)
      IF(ALLOCATED(pathorder))  DEALLOCATE(pathorder)
	  IF(ALLOCATED(GVs))        DEALLOCATE(GVs)
	  IF(ALLOCATED(epoc))       DEALLOCATE(epoc)
	  IF(ALLOCATED(skimdata))   DEALLOCATE(skimdata)
      CLOSE(3300)
	  WRITE(180,*) 
      WRITE(180,'(" # of re-assigned vehicles and re-assignment rate = ", 2f10.3 )') totalvhpcect, totalvhpcect/justveh
      WRITE(180,*) 
      
END SUBROUTINE

 