SUBROUTINE SIM_MOVE_VEHICLES(t,l)
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~ Copyright (C) 2002-2011 The Arizona Board of Regents on Behalf of The University of Arizona.  ~~!
!~~ Developed by Yi-Chang Chiu, Ph.D. as par to the DynusT project.                               ~~!
!~~ This program is free software: you can redistribute it and/or modify it                       ~~!
!~~ under the terms of the GNU General Public License version 2 as published by                   ~~!
!~~ the Free Software Foundation.                                                                 ~~!
!~~ This program is distributed in the hope that it will be useful,                               ~~!
!~~ but WITHOUT ANY WARRANTY; without even the implied warranty of                                ~~!
!~~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                                          ~~!
!~~ See the GNU General Public License for more details.                                          ~~!
!~~ You should have received a copy of the GNU General Public License                             ~~!
!~~ along with this program (in the file COPYING).                                                ~~!
!~~ IF not, see <http://www.gnu.org/licenses/>.                                                   ~~!
!~~ Please see http://www.dynust.net for contact information and names                            ~~!
!~~ of individual contributors.                                                                   ~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
USE DYNUST_MAIN_MODULE
USE DYNUST_VEH_MODULE
USE DYNUST_NETWORK_MODULE
USE DYNUST_VEH_PATH_ATT_MODULE
USE DYNUST_SIGNAL_MODULE
USE DYNUST_TRANSLINK_MODULE
USE DYNUST_LINK_VEH_LIST_MODULE
USE DYNUST_AMS_MODULE
USE DYNUST_FUEL_MODULE
USE DYNUST_ROUTE_SWITCH_MODULE
USE VARIABLE_MESSAGE_SIGN_MODULE
USE DFPORT
USE RAND_GEN_INT
USE OMP_LIB


INTEGER::tid
REAL,INTENT(IN):: T
INTEGER,INTENT(IN)::L
LOGICAL:: flag=.False.    
LOGICAL:: ArriveIntDest = .False.
LOGICAL EndID,CallMultM,CapOk
INTEGER Nlnk,CuLnk, ReturnID,aggtime,ls, agstime,NlnkMv
INTEGER VehPID(100000), iab, itt
INTEGER,ALLOCATABLE:: numcarstmp(:,:)
INTERFACE
    SUBROUTINE CAL_ACCUMU_STAT(i,j,t)
         INTEGER i,j
         REAL t
    END SUBROUTINE
END INTERFACE
REAL evalue
 
 
!> > > > > > > > > >                < < < < < < < < < < <
!>>>>>>>>>>>>>>>>>>>CODES START HERE<<<<<<<<<<<<<<<<<<<<<
!> > > > > > > > > >                < < < < < < < < < < <

ALLOCATE(numcarstmp(noofarcs,4))

m_dynust_network_arc_de(:)%UnivLink%DetStatus=.false.

numcarstmp(:,:)=0

! THE FOLLOWING IS TO WRITE VEH POSITION ON LINK
IF(VehJO > 0) THEN
    IF(mod(nint(t/xminPerSimInt),1) == 0) THEN
        IF(VehJO > 0.and.iteMax == 0.or.(iteMax > 0.and.((mod(iteration,VehJO) == 0.and.iteration > 0).or.iteration == iteMax))) THEN
            WRITE(888,'("time=> ",f7.1)') t
        ENDIF
    ENDIF
ENDIF


!$OMP PARALLEL SHARED(numcarstmp) FIRSTPRIVATE(VehJO,xminPerSimInt,iteMax,iteration,tdspstep,JamSwitch,JamSWBar,l,Nlnk,simPerAgg,time_now) PRIVATE(iab,itt,i,j,flag,iveh,xposold,xpos,xposnew,r,nl,imbus,idec,itmp1,itmp2,GuiTotalTime,aggtime,ArriveIntDest,agstime,ReturnID) NUM_THREADS(nThread)
!$OMP DO SCHEDULE(DYNAMIC)
   
DO 7 i=1,noofarcs

    IF(m_dynust_network_arc_nde(i)%link_iden /= 99) THEN

!   THE FOLLOWING IS TO WRITE VEH POSITION ON LINK
    IF(VehJO > 0) THEN
        IF(mod(nint(t/xminPerSimInt),1) == 0) THEN
            IF(LinkVehList(i)%Fid > 0.and.(iteMax == 0.or.(iteMax > 0.and.((mod(iteration,VehJO) == 0.and.iteration > 0).or.iteration == iteMax)))) THEN
                !WRITE(888,'("== ",3i8)') IntoOutNodeNum(iunod(i)),IntoOutNodeNum(idnod(i))
            ENDIF
        ENDIF
    ENDIF


    kj = 0
    iveh = 1
    DO while (LinkVehList(i)%P(iveh) > 0 .and. iveh <= LinkVehList(i)%Psize) ! close at 20
        j=LinkVehList(i)%P(iveh)
        IF(.not.m_dynust_veh(j)%aparindex) THEN ! this vehicle has not arrives at the STOP bar
            VehPID(2*(iveh-1)+1) = nint(m_dynust_veh(j)%position*1000)
            VehPID(2*iveh) = j
			xposold=m_dynust_veh(j)%position
            xpos=m_dynust_veh(j)%position-m_dynust_veh(j)%vehSpeed*xminPerSimInt
                   
            IF(control_array(m_dynust_network_arc_nde(i)%idnod)%controltype == 4.or.control_array(m_dynust_network_arc_nde(i)%idnod)%controltype == 5) THEN
			    IF(xpos <= linkdetlen/5280.0) m_dynust_network_arc_de(i)%UnivLink%DetStatus = .true.
			ENDIF
            
            IF(m_dynust_last_stand(j)%vehclass == 3) THEN
!             update monetary cost for j
              iab = m_dynust_network_arc_nde(i)%FortoBackLink
              itt = min(aggint,ifix((t/xminPerSimInt)/simPerAgg)+1)
              m_dynust_last_stand(j)%expense=m_dynust_last_stand(j)%expense+(m_dynust_veh(j)%vehSpeed*xminPerSimInt)/m_dynust_network_arc_nde(i)%s*m_dynust_network_arc_de(iab)%costexp(itt,m_dynust_last_stand(j)%vehtype)
  			  m_dynust_veh(j)%tocross = m_dynust_veh(j)%position/max(0.0001,m_dynust_veh(j)%vehSpeed) !! be careful for AMS implementation
              m_dynust_veh(j)%tleft = max(0.0,xminPerSimInt - m_dynust_veh(j)%tocross)
            ENDIF

            IF(FuelOut > 0.and.(iteration == iteMax.or.reach_converg == .true.)) THEN
!$OMP CRITICAL
              CALL OutputFuelDemand(i,j,t)
!$OMP END CRITICAL
            ENDIF


!           ENROUTE DIVERSION
            IF((m_dynust_veh_nde(j)%icurrnt > 1.AND.MOD(l-1,tdspstep) == 0).AND.(m_dynust_last_stand(j)%vehclass == 4).AND.t>=InfoStartTime)THEN
                r = ranxy(26)
                IF(r <= m_dynust_veh_nde(j)%compliance) THEN
                    CALL INFO_SWITCH(j,i,m_dynust_veh_nde(j)%icurrnt,l) ! CLASS 4 CHECK IF REROUTE IS NEEDED 
                    CALL RETRIEVE_NEXT_LINK(t,j,i,m_dynust_veh_nde(j)%icurrnt,Nlnk,NlnkMv)        
                    nl = m_dynust_veh(j)%nexlink(1)
	                IF(m_dynust_network_arc_nde(nl)%link_iden == 6) m_dynust_veh(j)%HovFlag = .true.
                ENDIF
            ENDIF
            

            imbus=0
!           IF(m_dynust_last_stand(j)%vehtype == 7) THEN
!               CALL TRANSIT_STOP(i,j,xpos,imbus)
!               IF(xpos > 0.and.imbus == 0) m_dynust_veh(j)%distans=m_dynust_veh(j)%distans+m_dynust_veh(j)%vehSpeed*xminPerSimInt
!               IF(xpos <= 0.and.imbus == 0) m_dynust_veh(j)%distans=m_dynust_veh(j)%distans+m_dynust_veh(j)%position
!               IF(imbus == 1) goto 8
!           ENDIF

            IF(xpos > 0.0) THEN  ! HGF big IF the following for those have not reached STOP bar
                IF(m_dynust_veh(j)%vehSpeed <= VKFunc(m_dynust_network_arc_de(i)%FlowModelnum)%V02/60.0) THEN
                  m_dynust_veh(j)%ttSTOP = m_dynust_veh(j)%ttSTOP + xminPerSimInt
                ENDIF
                m_dynust_veh(j)%position=xpos
!               m_dynust_veh(j)%distans = m_dynust_veh(j)%distans+m_dynust_veh(j)%vehSpeed*xminPerSimInt !!!uytr
                                
                IF(m_dynust_veh(j)%position > m_dynust_network_arc_nde(i)%s) THEN
                    m_dynust_veh(j)%position = m_dynust_network_arc_nde(i)%s
                ENDIF
        
                m_dynust_veh(j)%ttilnow = m_dynust_veh(j)%ttilnow + xminPerSimInt

                GuiTotalTime=GuiTotalTime+xminPerSimInt

                IF(m_dynust_network_arc_de(i)%link_detector > 0) THEN !DET
                    xposold=xposold*5280
                    xposnew=xpos*5280

                    idec=m_dynust_network_arc_de(i)%link_detector
                    IF(idec > 0) THEN
                        itmp1=detector(idec,4)
                        itmp2=detector(idec,5)
                    ENDIF

                    IF(xposold > itmp1.and.xposnew < itmp2) THEN
                        occup(idec)=occup(idec)+detector_length(idec)
                    ELSEIF(xposold < itmp1.and.xposnew > itmp2) THEN
                        occup(idec)=occup(idec)+vehicle_length
                    ELSEIF(xposold > itmp1.and.xposnew > itmp2.and.xposnew < itmp1) THEN
                        occup(idec)=occup(idec)+(itmp1-xposnew)
                    ELSEIF(xposold < itmp1.and.xposold > itmp2.and.xposnew < itmp2) THEN
                        occup(idec)=occup(idec)+(xposold-itmp2)
                    ENDIF
                ENDIF !DET


            ELSE ! HGF VEHICLES HAVE REACHED THE END OF THE LINK
                IF(m_dynust_veh(j)%vehSpeed <= VKFunc(m_dynust_network_arc_de(i)%FlowModelnum)%V02/60.0) THEN 
                   m_dynust_veh(j)%ttSTOP = m_dynust_veh(j)%ttSTOP + m_dynust_veh(j)%tocross ! cumulate stop time
                ENDIF
                m_dynust_veh(j)%apar = t
                m_dynust_veh(j)%aparindex=.true.

                aggtime = max(1,ifix((m_dynust_veh(j)%inpar/xminPerSimInt)/simPerAgg)+1)              
         
                IF(t < m_dynust_veh(j)%inpar)THEN
                    !WRITE(911,*) "error in vehicle moving"
                    !WRITE(911,*) "t less than inpar"
                    STOP
                ENDIF
		        IF(iteMax == 0.and.mod(l,simPerAgg) == 0) THEN
		            m_dynust_network_arc_de(i)%link_time(aggtime)=0
                    m_dynust_network_arc_de(i)%link_count(aggtime)=0
		        ELSE
		            m_dynust_network_arc_de(i)%link_time(aggtime)=m_dynust_network_arc_de(i)%link_time(aggtime) + max(0.0,t-m_dynust_veh(j)%inpar)*AttScale
                    m_dynust_network_arc_de(i)%link_count(aggtime)=m_dynust_network_arc_de(i)%link_count(aggtime)+1
                ENDIF
		
		        m_dynust_veh(j)%inpar = 0.0

                m_dynust_veh(j)%ttilnow = m_dynust_veh(j)%ttilnow + m_dynust_veh(j)%tocross
                IF(m_dynust_veh_nde(j)%icurrnt == 1) m_dynust_last_stand(j)%ttilFstNMove = t + m_dynust_veh(j)%tocross

                GuiTotalTime=GuiTotalTime+m_dynust_veh(j)%tocross


! THE FOLLOWING BLOCK IS CONSIDERING TRIP CHAINS
                IF(m_dynust_veh(j)%NoOfIntDst > 1.and.m_dynust_veh(j)%DestVisit < m_dynust_veh(j)%NoOfIntDst) THEN 

                    ArriveIntDest = .False.
                    IF(iteration == 0) THEN
		                IF(m_dynust_network_arc_nde(nl)%idnod == destination(MasterDest(m_dynust_veh_nde(j)%IntDestZone(m_dynust_veh(j)%DestVisit)))) ArriveIntDest=.True.
                    ELSE
			            IF(m_dynust_last_stand(j)%vehclass == 2.or.m_dynust_last_stand(j)%vehclass == 3) THEN
		                    IF(m_dynust_network_arc_nde(nl)%idnod == destination(MasterDest(m_dynust_veh_nde(j)%IntDestZone(m_dynust_veh(j)%DestVisit)))) ArriveIntDest=.True.
			            ELSE
			                IF(m_dynust_veh_nde(j)%icurrnt == m_dynust_veh(j)%IntDestPath(m_dynust_veh(j)%DestVisit)) ArriveIntDest = .True.
			            ENDIF
                    ENDIF
	                IF(ArriveIntDest) THEN
		                CALL TripChain_Insert(i,j) ! Insert the vehicle to list that carrys vehicles exit the network for activity
	                    CALL linkVehList_remove(i,j) ! remove this vehicle out of the current link

                        m_dynust_network_arc_de(i)%npar=m_dynust_network_arc_de(i)%npar-1
                        IF(m_dynust_last_stand(j)%vehtype == 2.or.m_dynust_last_stand(j)%vehtype == 5.or.m_dynust_last_stand(j)%vehtype == 7) THEN
			                m_dynust_network_arc_de(i)%nTruck=m_dynust_network_arc_de(i)%nTruck-1
                        ENDIF
	                    m_dynust_network_arc_de(i)%volume=m_dynust_network_arc_de(i)%volume-1
			            IF(m_dynust_network_arc_de(i)%volume < 0) THEN
                            !WRITE(911,*)'Negative volume on link',i
	                        !WRITE(911,*)'Please contact developers'
!                           STOP
			            ENDIF
                        IF(m_dynust_network_arc_de(i)%pcetotal < 0) THEN
                            !WRITE(911,*) "pcetotal negative"
                            !WRITE(911,*) "On link ", IntoOutNodeNum(iunod(i)),IntoOutNodeNum(idnod(i))
!                           STOP
                        ENDIF
	                    m_dynust_veh(j)%RemainDwell=m_dynust_veh(j)%IntDestDwell(m_dynust_veh(j)%DestVisit)/60.0-m_dynust_veh(j)%tleft ! start counting the remaining dewell time for the current activity
	                    m_dynust_veh(j)%IntDestPath(m_dynust_veh(j)%DestVisit) = m_dynust_veh_nde(j)%icurrnt ! record the number of nodes on the path 
                        m_dynust_veh(j)%tocross=0.0
                        m_dynust_veh(j)%tleft=0.0
	                    go to 8
                    ENDIF
                ENDIF
                               
!               THIS VEHICLE HAS REACHED TEH DESTINATION 
                IF(m_dynust_veh_nde(j)%icurrnt >= vehatt_P_Size(j)-1) THEN
                    m_dynust_veh(j)%notin=1
                    IF(m_dynust_veh(j)%itag == 0) THEN
                        numcarstmp(i,1)=numcarstmp(i,1)+1
                        numcarstmp(i,2)=numcarstmp(i,2)-1
                    ELSEIF(m_dynust_veh(j)%itag == 1) THEN
                        numcarstmp(i,3) = numcarstmp(i,3)+1
                        numcarstmp(i,4) = numcarstmp(i,4)-1
                    ENDIF
                    m_dynust_veh(j)%itag=2

!$OMP CRITICAL
                    CALL CAL_ACCUMU_STAT(i,j,t) ! get the stats for this vehicle
!$OMP END CRITICAL

                    CALL linkVehList_remove(i,j) ! take this vehicle out of the network

                    agstime = ifix((l-1)*xminPerSimInt)+1
                    m_dynust_network_arc_de(i)%AccuVol(agstime)=m_dynust_network_arc_de(i)%AccuVol(agstime) + 1
                    IF(m_dynust_last_stand(j)%vehtype == 2) m_dynust_network_arc_de(i)%AccuVolT(agstime)=m_dynust_network_arc_de(i)%AccuVolT(agstime) + 1
                    IF(m_dynust_last_stand(j)%vehtype == 3) m_dynust_network_arc_de(i)%AccuVolH(agstime)=m_dynust_network_arc_de(i)%AccuVolH(agstime) + 1

                    CALL vehatt_A_Remove(j) ! need to DEALLOCATE A first
                    CALL vehatt_P_Remove(j)  ! 05082006
                    IF(m_dynust_veh(j)%qflag) THEN
                        IF(m_dynust_network_arc_de(i)%vehicle_queue > 0.and..not.m_dynust_network_arc_nde(i)%link_ams) THEN
                            m_dynust_network_arc_de(i)%vehicle_queue=m_dynust_network_arc_de(i)%vehicle_queue-1
                        ENDIF
                        m_dynust_veh(j)%qflag=.false.
                    ENDIF
		            m_dynust_network_arc_de(i)%npar=m_dynust_network_arc_de(i)%npar-1
                    IF(m_dynust_last_stand(j)%vehtype == 2.or.m_dynust_last_stand(j)%vehtype == 5.or.m_dynust_last_stand(j)%vehtype == 7) THEN
		                m_dynust_network_arc_de(i)%nTruck=m_dynust_network_arc_de(i)%nTruck-1
                    ENDIF
	                m_dynust_network_arc_de(i)%volume=m_dynust_network_arc_de(i)%volume-1
!			        IF(m_dynust_network_arc_de(i)%volume < 0) THEN
                        !WRITE(911,*)'Negative volume on link',i
	                    !WRITE(911,*)'Please contact developers'
!			        ENDIF

!                    IF(m_dynust_network_arc_de(i)%pcetotal < 0) THEN
                        !WRITE(911,*) "pcetotal negative"
                        !WRITE(911,*) "On link ", IntoOutNodeNum(iunod(i)),IntoOutNodeNum(idnod(i))
!                    ENDIF
	                m_dynust_veh(j)%position = 0.0
		            m_dynust_veh(j)%IntDestPath(m_dynust_veh(j)%DestVisit) = m_dynust_veh_nde(j)%icurrnt
	                goto 8
                ELSE ! THE VEHICLE HAS REACHED THE DOWNSTREAM NOE BUT NOT DESTINATION
                
!     CHECK IF THE CUMULATIVE DELAY EXCEEDS THE TOLERABLE DELAY
!$OMP CRITICAL
                    IF(ReadHistArr > 0.AND.RunMode == 2.AND.m_dynust_veh_nde(j)%icurrnt > 5.AND.m_dynust_last_stand(j)%vehclass /= 4.AND.m_dynust_last_stand(j)%vehtype /= 6) THEN
                       evalue = vehatt_value(j,m_dynust_veh_nde(j)%icurrnt,4) ! INFLATED BY ATTSCALE
                       m_dynust_veh(j)%currentdelay = (t-m_dynust_last_stand(j)%stime)*AttScale - evalue
                       IF(evalue > 0.AND.(m_dynust_veh(j)%currentdelay > m_dynust_veh(j)%delaytole).AND.m_dynust_veh(j)%DivStatus == 0.AND.m_dynust_network_arc_de(i)%LinkStatus%inci /=2) THEN ! CHECK EVALUE > 0 BECAUSE WHEN VEHICLE IS STILL IN THE NETWORK AT TIME OF SIMULATION TERMINATION, THE EVALUE = 0 FROM THE BASELINE CASE, IN THIS CASE, DON'T ACTIVATE THE RULE
                          CallMultM = .false.
                          r4 = ranxy(11)
                          IF(NoofBuses > 0.AND.r4 <= TransitEnrouteSwitchFlag) THEN
                             !CALL CHECK_IF_MULTM(distscale,j,i,m_dynust_veh_nde(j)%icurrnt,l,CallMultM,LowestR,LowestS,LowestOnNode,LowestOffNode)
                             !IF(CallMultM) then
                             !  CALL DELAY_SWITCH_MULT(j,i,m_dynust_veh_nde(j)%icurrnt,l,LowestR,LowestS,LowestOnNode,LowestOffNode,CapOk) ! MULTI MODAL REROUTE IS NEEDED 
                             !ELSE
                             !  CALL DELAY_SWITCH_AUTO(j,i,m_dynust_veh_nde(j)%icurrnt,l) ! AUTO REROUTE IS NEEDED
                             !ENDIF
                          ELSE
                             !CALL DELAY_SWITCH_AUTO(j,i,m_dynust_veh_nde(j)%icurrnt,l) ! AUTO REROUTE IS NEEDED 
                          ENDIF
                          CALL RETRIEVE_NEXT_LINK(t,j,i,m_dynust_veh_nde(j)%icurrnt,Nlnk,NlnkMv)        
                          nl = m_dynust_veh(j)%nexlink(1)
                          IF(m_dynust_network_arc_nde(nl)%link_iden == 6) m_dynust_veh(j)%HovFlag = .true.
                       ENDIF
                    ENDIF
!$OMP END CRITICAL

! CHECK VMS CASE
                    IF(vms_num > 0) THEN
                        IF(nodeWVMS(m_dynust_network_arc_nde(i)%idnod) > 0) THEN ! IF downstream node is a VMS node
                            IF(t >= vms_start(nodeWVMS(m_dynust_network_arc_nde(i)%idnod)).and.t <= vms_end(nodeWVMS(m_dynust_network_arc_nde(i)%idnod))) THEN
                                Select Case (vmstype(nodeWVMS(m_dynust_network_arc_nde(i)%idnod)))
	                            Case (1) ! speed advisory
	    
	                            Case (2) ! mandatory detoure
	                                CALL vms_detour(t,i,j,nodeWVMS(m_dynust_network_arc_nde(i)%idnod))
	                            Case (3)! congestion warning
	                                CALL vms_diversion(t,i,j,nodeWVMS(m_dynust_network_arc_nde(i)%idnod))
	                            Case (5)! multi-choice detour
	                                CALL vms_multi(t,i,j,nodeWVMS(m_dynust_network_arc_nde(i)%idnod))
	                            end select
                            ENDIF
                        ENDIF
                    ENDIF


                    CALL RETRIEVE_NEXT_LINK(t,j,i,m_dynust_veh_nde(j)%icurrnt,Nlnk,NlnkMV)        
                    nl = m_dynust_veh(j)%nexlink(1)
                    m_dynust_veh(j)%aparindex=.true.

	                IF(m_dynust_network_arc_nde(nl)%link_iden == 6) m_dynust_veh(j)%HovFlag = .true.
143                 FORMAT('jpath: ',10i5)


!$OMP CRITICAL
                    CALL TranLink_Scan(t,nl,time_now-(xminPerSimInt-m_dynust_veh(j)%tocross)*60,j,ReturnID) ! find correct position in terms arrival time at the node
                    IF(ReturnID <= 0) THEN
                        flag=.True.
                        goto 778
                    ENDIF
                    CALL TranLink_Insert(nl,ReturnID,1,j)
                    CALL TranLink_Insert(nl,ReturnID,2,i)
                    CALL TranLink_Insert(nl,ReturnID,3,nint(1000*(time_now-(xminPerSimInt-m_dynust_veh(j)%tocross)*60))) ! record the time this vehicle arrive
                    CALL TranLink_AvSizeIncre(nl)
                    m_dynust_network_arc_de(nl)%inoutbuff%NVehIn=m_dynust_network_arc_de(nl)%inoutbuff%NVehIn+1
                    m_dynust_network_arc_de(i)%inoutbuff%NVehOut=m_dynust_network_arc_de(i)%inoutbuff%NVehOut+1
778                 flag=flag
!$OMP END CRITICAL
                    IF(flag) THEN
                        flag=.False.
                        goto 8
                    ENDIF                
                ENDIF
            ENDIF ! HGF ENDIF 

8           CONTINUE 

        ELSE ! not aparindex

            m_dynust_veh(j)%tocross = 0
            m_dynust_veh(j)%tleft = max(0.0,xminPerSimInt - m_dynust_veh(j)%tocross)
        ENDIF ! .not.aparindex(j) 
  
        iveh = iveh + 1
        IF(iveh > LinkVehList(i)%Psize) exit 
        IF(iveh > 100000) THEN
            !WRITE(911,*) "error in iveh in vehicle moving exceeding VehPID array size"
            STOP      
        ENDIF
      
20 ENDDO ! endof of DO iveh on link i
   
IF(VehJO > 0) THEN
    IF(mod(nint(t/xminPerSimInt),1) == 0) THEN
        IF(LinkVehList(i)%Fid > 0.and.(iteMax == 0.or.(iteMax > 0.and.((mod(iteration,VehJO) == 0.and.iteration > 0).or.iteration == iteMax)))) THEN
            !WRITE(888,'(i5, 100000(i6,i6))') iveh-1, (VehPID(js),js = 1, 2*(iveh-1))
        ENDIF
    ENDIF
ENDIF

ENDIF ! LINK_IDEN IS NOT 99 - CENTROID CONNECTOR
7 CONTINUE

!$OMP END DO NOWAIT
!$OMP END PARALLEL

nout_nontag = nout_nontag + sum(numcarstmp(:,1))
numNtag = numNtag + sum(numcarstmp(:,2))
nout_tag = nout_tag + sum(numcarstmp(:,3))
Numcars = numcars + sum(numcarstmp(:,4))

DEALLOCATE(numcarstmp)


!WRITE(9876,'(200F6.2)') t, (m_dynust_veh(ijt)%distans,ijt = 1,justveh,60)



END SUBROUTINE

