; mark_description "Intel(R) Visual Fortran Compiler XE for applications running on IA-32, Version 13.1.1.171 Build 20130313";
; mark_description "-S";
	.686P
 	.387
	OPTION DOTNAME
	ASSUME	CS:FLAT,DS:FLAT,SS:FLAT
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
TXTST0:
; -- Begin  _NEXTA_STAT
; mark_begin;
IF @Version GE 800
  .MMX
ELSEIF @Version GE 612
  .MMX
  MMWORD TEXTEQU <QWORD>
ENDIF
IF @Version GE 800
  .XMM
ELSEIF @Version GE 614
  .XMM
  XMMWORD TEXTEQU <OWORD>
ENDIF
       ALIGN     16
	PUBLIC _NEXTA_STAT
_NEXTA_STAT	PROC NEAR 
; parameter 1: 8 + ebp
.B1.1:                          ; Preds .B1.0
        push      ebp                                           ;1.18
        mov       ebp, esp                                      ;1.18
        and       esp, -16                                      ;1.18
        push      esi                                           ;1.18
        push      edi                                           ;1.18
        push      ebx                                           ;1.18
        sub       esp, 132                                      ;1.18
        mov       esi, DWORD PTR [8+ebp]                        ;1.18
        mov       ebx, DWORD PTR [esi]                          ;32.7
        lea       eax, DWORD PTR [-1+ebx]                       ;32.12
        cmp       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_INT_D] ;32.16
        jne       .B1.5         ; Prob 50%                      ;32.16
                                ; LOE ebx esi
.B1.2:                          ; Preds .B1.1
        xor       eax, eax                                      ;33.16
        lea       edx, DWORD PTR [16+esp]                       ;33.16
        mov       DWORD PTR [16+esp], eax                       ;33.16
        push      32                                            ;33.16
        push      eax                                           ;33.16
        push      OFFSET FLAT: __STRLITPACK_38.0.1              ;33.16
        push      -2088435968                                   ;33.16
        push      800                                           ;33.16
        push      edx                                           ;33.16
        call      _for_close                                    ;33.16
                                ; LOE esi
.B1.3:                          ; Preds .B1.2
        mov       DWORD PTR [40+esp], 0                         ;34.10
        lea       eax, DWORD PTR [24+esp]                       ;34.10
        mov       DWORD PTR [24+esp], 8                         ;34.10
        mov       DWORD PTR [28+esp], OFFSET FLAT: __STRLITPACK_37 ;34.10
        mov       DWORD PTR [32+esp], 7                         ;34.10
        mov       DWORD PTR [36+esp], OFFSET FLAT: __STRLITPACK_36 ;34.10
        push      32                                            ;34.10
        push      eax                                           ;34.10
        push      OFFSET FLAT: __STRLITPACK_39.0.1              ;34.10
        push      -2088435968                                   ;34.10
        push      800                                           ;34.10
        lea       edx, DWORD PTR [60+esp]                       ;34.10
        push      edx                                           ;34.10
        call      _for_open                                     ;34.10
                                ; LOE esi
.B1.38:                         ; Preds .B1.3
        add       esp, 48                                       ;34.10
                                ; LOE esi
.B1.4:                          ; Preds .B1.38
        mov       ebx, DWORD PTR [esi]                          ;38.59
                                ; LOE ebx
.B1.5:                          ; Preds .B1.1 .B1.4
        xor       eax, eax                                      ;37.7
        mov       DWORD PTR [16+esp], eax                       ;37.7
        push      32                                            ;37.7
        push      OFFSET FLAT: NEXTA_STAT$format_pack.0.1+80    ;37.7
        push      eax                                           ;37.7
        push      OFFSET FLAT: __STRLITPACK_40.0.1              ;37.7
        push      -2088435968                                   ;37.7
        push      800                                           ;37.7
        lea       edx, DWORD PTR [40+esp]                       ;37.7
        push      edx                                           ;37.7
        call      _for_write_seq_fmt                            ;37.7
                                ; LOE ebx
.B1.6:                          ; Preds .B1.5
        cvtsi2ss  xmm0, ebx                                     ;38.59
        mulss     xmm0, DWORD PTR [_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT] ;38.7
        lea       eax, DWORD PTR [116+esp]                      ;38.7
        mov       DWORD PTR [44+esp], 0                         ;38.7
        movss     DWORD PTR [116+esp], xmm0                     ;38.7
        push      32                                            ;38.7
        push      OFFSET FLAT: NEXTA_STAT$format_pack.0.1+124   ;38.7
        push      eax                                           ;38.7
        push      OFFSET FLAT: __STRLITPACK_41.0.1              ;38.7
        push      -2088435968                                   ;38.7
        push      800                                           ;38.7
        lea       edx, DWORD PTR [68+esp]                       ;38.7
        push      edx                                           ;38.7
        call      _for_write_seq_fmt                            ;38.7
                                ; LOE
.B1.7:                          ; Preds .B1.6
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_JUSTVEH] ;39.7
        lea       eax, DWORD PTR [152+esp]                      ;39.7
        mov       DWORD PTR [72+esp], 0                         ;39.7
        mov       DWORD PTR [152+esp], edi                      ;39.7
        push      32                                            ;39.7
        push      OFFSET FLAT: NEXTA_STAT$format_pack.0.1+176   ;39.7
        push      eax                                           ;39.7
        push      OFFSET FLAT: __STRLITPACK_42.0.1              ;39.7
        push      -2088435968                                   ;39.7
        push      800                                           ;39.7
        lea       edx, DWORD PTR [96+esp]                       ;39.7
        push      edx                                           ;39.7
        call      _for_write_seq_fmt                            ;39.7
                                ; LOE edi
.B1.8:                          ; Preds .B1.7
        mov       ebx, edi                                      ;41.7
        lea       eax, DWORD PTR [188+esp]                      ;41.7
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NUMCARS] ;41.7
        sub       ebx, esi                                      ;41.7
        mov       DWORD PTR [100+esp], 0                        ;41.7
        mov       DWORD PTR [188+esp], ebx                      ;41.7
        push      32                                            ;41.7
        push      OFFSET FLAT: NEXTA_STAT$format_pack.0.1+228   ;41.7
        push      eax                                           ;41.7
        push      OFFSET FLAT: __STRLITPACK_43.0.1              ;41.7
        push      -2088435968                                   ;41.7
        push      800                                           ;41.7
        lea       edx, DWORD PTR [124+esp]                      ;41.7
        push      edx                                           ;41.7
        call      _for_write_seq_fmt                            ;41.7
                                ; LOE ebx esi edi
.B1.39:                         ; Preds .B1.8
        add       esp, 112                                      ;41.7
                                ; LOE ebx esi edi
.B1.9:                          ; Preds .B1.39
        mov       DWORD PTR [16+esp], 0                         ;43.7
        lea       eax, DWORD PTR [112+esp]                      ;43.7
        mov       DWORD PTR [112+esp], esi                      ;43.7
        push      32                                            ;43.7
        push      OFFSET FLAT: NEXTA_STAT$format_pack.0.1+280   ;43.7
        push      eax                                           ;43.7
        push      OFFSET FLAT: __STRLITPACK_44.0.1              ;43.7
        push      -2088435968                                   ;43.7
        push      800                                           ;43.7
        lea       edx, DWORD PTR [40+esp]                       ;43.7
        push      edx                                           ;43.7
        call      _for_write_seq_fmt                            ;43.7
                                ; LOE ebx edi
.B1.40:                         ; Preds .B1.9
        add       esp, 28                                       ;43.7
                                ; LOE ebx edi
.B1.10:                         ; Preds .B1.40
        test      edi, edi                                      ;47.18
        pxor      xmm0, xmm0                                    ;51.8
        movss     DWORD PTR [56+esp], xmm0                      ;51.8
        jle       .B1.12        ; Prob 16%                      ;47.18
                                ; LOE ebx edi
.B1.11:                         ; Preds .B1.10
        cvtsi2ss  xmm0, edi                                     ;47.38
        movss     xmm1, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRIPTIME1] ;47.23
        divss     xmm1, xmm0                                    ;47.23
        jmp       .B1.13        ; Prob 100%                     ;47.23
                                ; LOE ebx xmm1
.B1.12:                         ; Preds .B1.10
        movss     xmm1, DWORD PTR [56+esp]                      ;45.7
                                ; LOE ebx xmm1
.B1.13:                         ; Preds .B1.11 .B1.12
        mov       DWORD PTR [16+esp], 0                         ;48.7
        lea       eax, DWORD PTR [120+esp]                      ;48.7
        movss     DWORD PTR [120+esp], xmm1                     ;48.7
        push      32                                            ;48.7
        push      OFFSET FLAT: NEXTA_STAT$format_pack.0.1+332   ;48.7
        push      eax                                           ;48.7
        push      OFFSET FLAT: __STRLITPACK_45.0.1              ;48.7
        push      -2088435968                                   ;48.7
        push      800                                           ;48.7
        lea       edx, DWORD PTR [40+esp]                       ;48.7
        push      edx                                           ;48.7
        call      _for_write_seq_fmt                            ;48.7
                                ; LOE ebx
.B1.41:                         ; Preds .B1.13
        add       esp, 28                                       ;48.7
                                ; LOE ebx
.B1.14:                         ; Preds .B1.41
        test      ebx, ebx                                      ;50.28
        jle       .B1.35        ; Prob 16%                      ;50.28
                                ; LOE ebx
.B1.15:                         ; Preds .B1.14
        cvtsi2ss  xmm0, ebx                                     ;53.26
        movss     xmm1, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRIPTIME1] ;53.8
        lea       eax, DWORD PTR [64+esp]                       ;54.7
        divss     xmm1, xmm0                                    ;53.8
        mov       DWORD PTR [16+esp], 0                         ;54.7
        movss     DWORD PTR [64+esp], xmm1                      ;54.7
        push      32                                            ;54.7
        push      OFFSET FLAT: NEXTA_STAT$format_pack.0.1+436   ;54.7
        push      eax                                           ;54.7
        push      OFFSET FLAT: __STRLITPACK_47.0.1              ;54.7
        push      -2088435968                                   ;54.7
        push      800                                           ;54.7
        lea       edx, DWORD PTR [40+esp]                       ;54.7
        push      edx                                           ;54.7
        call      _for_write_seq_fmt                            ;54.7
                                ; LOE
.B1.42:                         ; Preds .B1.35 .B1.15
        add       esp, 28                                       ;54.7
                                ; LOE
.B1.16:                         ; Preds .B1.42
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFARCS] ;57.7
        xor       ebx, ebx                                      ;
        mov       DWORD PTR [60+esp], eax                       ;57.7
        xor       esi, esi                                      ;
        pxor      xmm0, xmm0                                    ;
        test      eax, eax                                      ;57.7
        movss     DWORD PTR [52+esp], xmm0                      ;
        movss     xmm0, DWORD PTR [56+esp]                      ;
        jle       .B1.28        ; Prob 2%                       ;57.7
                                ; LOE ebx esi xmm0
.B1.17:                         ; Preds .B1.16
        imul      edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32], -152 ;
        mov       ecx, 1                                        ;
        movss     xmm2, DWORD PTR [52+esp]                      ;
        movss     xmm3, DWORD PTR [56+esp]                      ;
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;58.4
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;59.11
        lea       eax, DWORD PTR [152+eax+edi]                  ;
        imul      edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32], -900 ;
        lea       edx, DWORD PTR [900+edx+edi]                  ;
                                ; LOE eax edx ecx ebx esi xmm0 xmm2 xmm3
.B1.18:                         ; Preds .B1.26 .B1.17
        movsx     edi, WORD PTR [148+eax]                       ;58.8
        cmp       edi, 99                                       ;58.47
        jge       .B1.26        ; Prob 50%                      ;58.47
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm2 xmm3
.B1.19:                         ; Preds .B1.18
        movss     xmm1, DWORD PTR [652+edx]                     ;59.31
        cmp       edi, 1                                        ;60.53
        addss     xmm3, xmm1                                    ;59.11
        je        .B1.25        ; Prob 16%                      ;60.53
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1 xmm2 xmm3
.B1.20:                         ; Preds .B1.19
        cmp       edi, 2                                        ;60.100
        je        .B1.25        ; Prob 16%                      ;60.100
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1 xmm2 xmm3
.B1.21:                         ; Preds .B1.20
        cmp       edi, 8                                        ;60.147
        je        .B1.25        ; Prob 16%                      ;60.147
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1 xmm2 xmm3
.B1.22:                         ; Preds .B1.21
        cmp       edi, 9                                        ;60.194
        je        .B1.25        ; Prob 16%                      ;60.194
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1 xmm2 xmm3
.B1.23:                         ; Preds .B1.22
        cmp       edi, 10                                       ;60.241
        je        .B1.25        ; Prob 16%                      ;60.241
                                ; LOE eax edx ecx ebx esi xmm0 xmm1 xmm2 xmm3
.B1.24:                         ; Preds .B1.23
        inc       esi                                           ;65.13
        addss     xmm2, xmm1                                    ;64.13
        jmp       .B1.26        ; Prob 100%                     ;64.13
                                ; LOE eax edx ecx ebx esi xmm0 xmm2 xmm3
.B1.25:                         ; Preds .B1.21 .B1.22 .B1.23 .B1.19 .B1.20
                                ;      
        inc       ebx                                           ;62.13
        addss     xmm0, xmm1                                    ;61.13
                                ; LOE eax edx ecx ebx esi xmm0 xmm2 xmm3
.B1.26:                         ; Preds .B1.25 .B1.24 .B1.18
        inc       ecx                                           ;68.7
        add       eax, 152                                      ;68.7
        add       edx, 900                                      ;68.7
        cmp       ecx, DWORD PTR [60+esp]                       ;68.7
        jle       .B1.18        ; Prob 82%                      ;68.7
                                ; LOE eax edx ecx ebx esi xmm0 xmm2 xmm3
.B1.27:                         ; Preds .B1.26
        movss     DWORD PTR [52+esp], xmm2                      ;
        movss     DWORD PTR [56+esp], xmm3                      ;
                                ; LOE ebx esi xmm0
.B1.28:                         ; Preds .B1.16 .B1.27
        cvtsi2ss  xmm1, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFARCS_ORG] ;70.28
        movss     xmm2, DWORD PTR [56+esp]                      ;70.27
        lea       eax, DWORD PTR [128+esp]                      ;72.7
        divss     xmm2, xmm1                                    ;70.27
        mulss     xmm2, DWORD PTR [_2il0floatpacket.1]          ;70.7
        mov       DWORD PTR [16+esp], 0                         ;72.7
        movss     DWORD PTR [128+esp], xmm2                     ;72.7
        push      32                                            ;72.7
        push      OFFSET FLAT: NEXTA_STAT$format_pack.0.1+488   ;72.7
        push      eax                                           ;72.7
        push      OFFSET FLAT: __STRLITPACK_48.0.1              ;72.7
        push      -2088435968                                   ;72.7
        push      800                                           ;72.7
        lea       edx, DWORD PTR [40+esp]                       ;72.7
        push      edx                                           ;72.7
        movss     DWORD PTR [88+esp], xmm0                      ;72.7
        call      _for_write_seq_fmt                            ;72.7
                                ; LOE ebx esi
.B1.43:                         ; Preds .B1.28
        movss     xmm0, DWORD PTR [88+esp]                      ;
        add       esp, 28                                       ;72.7
                                ; LOE ebx esi xmm0
.B1.29:                         ; Preds .B1.43
        mov       DWORD PTR [16+esp], 0                         ;77.9
        test      ebx, ebx                                      ;75.18
        jle       .B1.34        ; Prob 16%                      ;75.18
                                ; LOE ebx esi xmm0
.B1.30:                         ; Preds .B1.29
        cvtsi2ss  xmm1, ebx                                     ;76.36
        divss     xmm0, xmm1                                    ;76.35
        mulss     xmm0, DWORD PTR [_2il0floatpacket.1]          ;76.9
        lea       eax, DWORD PTR [72+esp]                       ;77.9
        movss     DWORD PTR [72+esp], xmm0                      ;77.9
        push      32                                            ;77.9
        push      OFFSET FLAT: NEXTA_STAT$format_pack.0.1+540   ;77.9
        push      eax                                           ;77.9
        push      OFFSET FLAT: __STRLITPACK_49.0.1              ;77.9
        push      -2088435968                                   ;77.9
        push      800                                           ;77.9
        lea       edx, DWORD PTR [40+esp]                       ;77.9
        push      edx                                           ;77.9
        call      _for_write_seq_fmt                            ;77.9
                                ; LOE esi
.B1.44:                         ; Preds .B1.34 .B1.30
        add       esp, 28                                       ;77.9
                                ; LOE esi
.B1.31:                         ; Preds .B1.44
        test      esi, esi                                      ;82.17
        jle       .B1.33        ; Prob 16%                      ;82.17
                                ; LOE esi
.B1.32:                         ; Preds .B1.31
        cvtsi2ss  xmm0, esi                                     ;83.29
        movss     xmm2, DWORD PTR [52+esp]                      ;83.28
        lea       eax, DWORD PTR [80+esp]                       ;84.9
        divss     xmm2, xmm0                                    ;83.28
        movss     xmm1, DWORD PTR [_2il0floatpacket.1]          ;83.4
        mulss     xmm1, xmm2                                    ;83.4
        mov       DWORD PTR [16+esp], 0                         ;84.9
        movss     DWORD PTR [80+esp], xmm1                      ;84.9
        push      32                                            ;84.9
        push      OFFSET FLAT: NEXTA_STAT$format_pack.0.1+644   ;84.9
        push      eax                                           ;84.9
        push      OFFSET FLAT: __STRLITPACK_51.0.1              ;84.9
        push      -2088435968                                   ;84.9
        push      800                                           ;84.9
        lea       edx, DWORD PTR [40+esp]                       ;84.9
        push      edx                                           ;84.9
        call      _for_write_seq_fmt                            ;84.9
                                ; LOE
.B1.45:                         ; Preds .B1.32
        add       esp, 28                                       ;84.9
                                ; LOE
.B1.33:                         ; Preds .B1.45 .B1.31
        add       esp, 132                                      ;94.1
        pop       ebx                                           ;94.1
        pop       edi                                           ;94.1
        pop       esi                                           ;94.1
        mov       esp, ebp                                      ;94.1
        pop       ebp                                           ;94.1
        ret                                                     ;94.1
                                ; LOE
.B1.34:                         ; Preds .B1.29                  ; Infreq
        mov       DWORD PTR [56+esp], 1065353216                ;79.6
        lea       eax, DWORD PTR [56+esp]                       ;79.6
        push      32                                            ;79.6
        push      OFFSET FLAT: NEXTA_STAT$format_pack.0.1+592   ;79.6
        push      eax                                           ;79.6
        push      OFFSET FLAT: __STRLITPACK_50.0.1              ;79.6
        push      -2088435968                                   ;79.6
        push      800                                           ;79.6
        lea       edx, DWORD PTR [40+esp]                       ;79.6
        push      edx                                           ;79.6
        call      _for_write_seq_fmt                            ;79.6
        jmp       .B1.44        ; Prob 100%                     ;79.6
                                ; LOE esi
.B1.35:                         ; Preds .B1.14                  ; Infreq
        mov       DWORD PTR [16+esp], 0                         ;51.8
        lea       eax, DWORD PTR [48+esp]                       ;51.8
        mov       DWORD PTR [48+esp], 0                         ;51.8
        push      32                                            ;51.8
        push      OFFSET FLAT: NEXTA_STAT$format_pack.0.1+384   ;51.8
        push      eax                                           ;51.8
        push      OFFSET FLAT: __STRLITPACK_46.0.1              ;51.8
        push      -2088435968                                   ;51.8
        push      800                                           ;51.8
        lea       edx, DWORD PTR [40+esp]                       ;51.8
        push      edx                                           ;51.8
        call      _for_write_seq_fmt                            ;51.8
        jmp       .B1.42        ; Prob 100%                     ;51.8
        ALIGN     16
                                ; LOE
; mark_end;
_NEXTA_STAT ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
NEXTA_STAT$format_pack.0.1	DB	54
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	1
	DB	1
	DB	0
	DB	0
	DB	0
	DB	10
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	1
	DB	1
	DB	0
	DB	0
	DB	0
	DB	10
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	10
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	10
	DB	0
	DB	0
	DB	0
	DB	5
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	30
	DB	0
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	28
	DB	0
	DB	67
	DB	117
	DB	114
	DB	114
	DB	101
	DB	110
	DB	116
	DB	32
	DB	84
	DB	105
	DB	109
	DB	101
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	58
	DB	33
	DB	0
	DB	0
	DB	1
	DB	1
	DB	0
	DB	0
	DB	0
	DB	10
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	28
	DB	0
	DB	84
	DB	111
	DB	116
	DB	97
	DB	108
	DB	32
	DB	35
	DB	32
	DB	111
	DB	102
	DB	32
	DB	71
	DB	101
	DB	110
	DB	32
	DB	86
	DB	101
	DB	104
	DB	115
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	58
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	10
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	28
	DB	0
	DB	84
	DB	111
	DB	116
	DB	97
	DB	108
	DB	32
	DB	35
	DB	32
	DB	111
	DB	102
	DB	32
	DB	79
	DB	117
	DB	116
	DB	32
	DB	86
	DB	101
	DB	104
	DB	115
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	58
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	10
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	28
	DB	0
	DB	84
	DB	111
	DB	116
	DB	97
	DB	108
	DB	32
	DB	35
	DB	32
	DB	111
	DB	102
	DB	32
	DB	73
	DB	110
	DB	32
	DB	86
	DB	101
	DB	104
	DB	115
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	58
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	10
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	28
	DB	0
	DB	65
	DB	118
	DB	103
	DB	32
	DB	84
	DB	114
	DB	97
	DB	118
	DB	101
	DB	108
	DB	32
	DB	84
	DB	105
	DB	109
	DB	101
	DB	32
	DB	65
	DB	108
	DB	108
	DB	32
	DB	86
	DB	101
	DB	104
	DB	115
	DB	32
	DB	32
	DB	32
	DB	58
	DB	33
	DB	0
	DB	0
	DB	1
	DB	1
	DB	0
	DB	0
	DB	0
	DB	10
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	28
	DB	0
	DB	65
	DB	118
	DB	103
	DB	32
	DB	84
	DB	114
	DB	97
	DB	118
	DB	101
	DB	108
	DB	32
	DB	84
	DB	105
	DB	109
	DB	101
	DB	32
	DB	102
	DB	111
	DB	114
	DB	32
	DB	111
	DB	117
	DB	116
	DB	32
	DB	86
	DB	101
	DB	104
	DB	58
	DB	33
	DB	0
	DB	0
	DB	1
	DB	1
	DB	0
	DB	0
	DB	0
	DB	10
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	28
	DB	0
	DB	65
	DB	118
	DB	103
	DB	32
	DB	84
	DB	114
	DB	97
	DB	118
	DB	101
	DB	108
	DB	32
	DB	84
	DB	105
	DB	109
	DB	101
	DB	32
	DB	102
	DB	111
	DB	114
	DB	32
	DB	111
	DB	117
	DB	116
	DB	32
	DB	86
	DB	101
	DB	104
	DB	58
	DB	33
	DB	0
	DB	0
	DB	1
	DB	1
	DB	0
	DB	0
	DB	0
	DB	10
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	28
	DB	0
	DB	65
	DB	118
	DB	103
	DB	32
	DB	115
	DB	112
	DB	101
	DB	101
	DB	100
	DB	32
	DB	102
	DB	111
	DB	114
	DB	32
	DB	97
	DB	108
	DB	108
	DB	32
	DB	108
	DB	105
	DB	110
	DB	107
	DB	115
	DB	32
	DB	32
	DB	32
	DB	32
	DB	58
	DB	33
	DB	0
	DB	0
	DB	1
	DB	1
	DB	0
	DB	0
	DB	0
	DB	10
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	28
	DB	0
	DB	65
	DB	118
	DB	103
	DB	32
	DB	115
	DB	112
	DB	101
	DB	101
	DB	100
	DB	32
	DB	102
	DB	111
	DB	114
	DB	32
	DB	102
	DB	114
	DB	101
	DB	101
	DB	119
	DB	97
	DB	121
	DB	115
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	58
	DB	33
	DB	0
	DB	0
	DB	1
	DB	1
	DB	0
	DB	0
	DB	0
	DB	10
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	28
	DB	0
	DB	65
	DB	118
	DB	103
	DB	32
	DB	115
	DB	112
	DB	101
	DB	101
	DB	100
	DB	32
	DB	102
	DB	111
	DB	114
	DB	32
	DB	102
	DB	114
	DB	101
	DB	101
	DB	119
	DB	97
	DB	121
	DB	115
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	58
	DB	33
	DB	0
	DB	0
	DB	1
	DB	1
	DB	0
	DB	0
	DB	0
	DB	10
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	28
	DB	0
	DB	65
	DB	118
	DB	103
	DB	32
	DB	115
	DB	112
	DB	101
	DB	101
	DB	100
	DB	32
	DB	102
	DB	111
	DB	114
	DB	32
	DB	97
	DB	114
	DB	116
	DB	101
	DB	114
	DB	105
	DB	97
	DB	108
	DB	115
	DB	32
	DB	32
	DB	32
	DB	32
	DB	58
	DB	33
	DB	0
	DB	0
	DB	1
	DB	1
	DB	0
	DB	0
	DB	0
	DB	10
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
_DATA	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
__STRLITPACK_38.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_39.0.1	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	26
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_40.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_41.0.1	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_42.0.1	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_43.0.1	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_44.0.1	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_45.0.1	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_47.0.1	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_46.0.1	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_48.0.1	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_50.0.1	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_49.0.1	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_51.0.1	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _NEXTA_STAT
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_37	DB	102
	DB	111
	DB	114
	DB	116
	DB	46
	DB	56
	DB	48
	DB	48
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_36	DB	117
	DB	110
	DB	107
	DB	110
	DB	111
	DB	119
	DB	110
	DB	0
_2il0floatpacket.1	DD	042700000H
_2il0floatpacket.2	DD	03f800000H
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
EXTRN	_DYNUST_MAIN_MODULE_mp_NOOFARCS_ORG:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NOOFARCS:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TRIPTIME1:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NUMCARS:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_JUSTVEH:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_INT_D:BYTE
_DATA	ENDS
EXTRN	_for_write_seq_fmt:PROC
EXTRN	_for_open:PROC
EXTRN	_for_close:PROC
EXTRN	__fltused:BYTE
	INCLUDELIB <ifconsol>
	INCLUDELIB <libifcoremt>
	INCLUDELIB <libifport>
	INCLUDELIB <libmmt>
	INCLUDELIB <LIBCMT>
	INCLUDELIB <libirc>
	INCLUDELIB <svml_dispmt>
	INCLUDELIB <OLDNAMES>
	END
