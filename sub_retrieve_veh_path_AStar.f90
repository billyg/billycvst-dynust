SUBROUTINE RETRIEVE_VEH_PATH_AStar(j,i,iselect,CurNode,jdst,itrace,SeeNodeSum)
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~ Copyright (C) 2002-2011 The Arizona Board of Regents on Behalf of The University of Arizona.  ~~!
!~~ Developed by Yi-Chang Chiu, Ph.D. as par to the DynusT project.                               ~~!
!~~ This program is free software: you can redistribute it and/or modify it                       ~~!
!~~ under the terms of the GNU General Public License version 2 as published by                   ~~!
!~~ the Free Software Foundation.                                                                 ~~!
!~~ This program is distributed in the hope that it will be useful,                               ~~!
!~~ but WITHOUT ANY WARRANTY; without even the implied warranty of                                ~~!
!~~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                                          ~~!
!~~ See the GNU General Public License for more details.                                          ~~!
!~~ You should have received a copy of the GNU General Public License                             ~~!
!~~ along with this program (in the file COPYING).                                                ~~!
!~~ IF not, see <http://www.gnu.org/licenses/>.                                                   ~~!
!~~ Please see http://www.dynust.net for contact information and names                            ~~!
!~~ of individual contributors.                                                                   ~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
      USE DYNUST_MAIN_MODULE
      USE DYNUST_VEH_MODULE
      USE DYNUST_NETWORK_MODULE
      USE DYNUST_VEH_PATH_ATT_MODULE
      USE DFPORT
      USE RAND_GEN_INT
      USE DYNUST_TDSP_ASTAR_MODULE
            
      INTEGER pathttmp(maxnu_pa)
      INTEGER Index1D, ifg2,itrace
      INTEGER, INTENT(IN) ::J,I,ISELECT, jdst,CurNode
      INTEGER(2) CurN
     
      REAL evalue, TimeIndex 
      INTEGER SeeNodeSum, Size, In_Dest
      INTEGER :: LinkID, Counter, Arg_Origin  

!> > > > > > > > > >                < < < < < < < < < < <
!>>>>>>>>>>>>>>>>>>>CODES START HERE<<<<<<<<<<<<<<<<<<<<<
!> > > > > > > > > >                < < < < < < < < < < <

      PathTTmp = 0
      TimIdex = FLOOR(time_now/60.0) + 1 ! convert to min
      !In_Dest  = m_dynust_last_stand(j)%jdestNode
      In_Dest = destination(m_dynust_last_stand(j)%jdest)
      Size = nint(SimPeriod)
      
      CALL Create_AStar_Scan_Eligible_Set( Size )
      CALL Create_AStar_Fixed_Label_Set
      CALL Create_Labels (Noofarcs)

      CALL AStar_Initialization( i, In_Dest, TimIdex, 0, noofnodes_org ) 
      CALL AStar_Get_Path( i, In_Dest, LinkID, TimIdex, 0, 0 )
      CALL RetrieveTDSP( i, LinkID, pathttmp, Counter )

      CALL Destroy_AStar_Scan_Eligible_Set( Size )
      CALL Destroy_AStar_Fixed_Label_Set
      CALL Destroy_Labels ()

      nnk = Counter
      Index1D = nnk
      IF(pathttmp(1) > 900000) THEN
        print *,"found centroid"
      ENDIF
      IF(vehatt_Array(j)%PSize < CurNode+nnk-1) THEN
       isizetmp = max(vehatt_Array(j)%PSize+v_IncreaseSize,CurNode+nnk-1)
       CALL vehatt_P_Setup(j,isizetmp)
       CALL vehatt_A_Setup(j,isizetmp) ! initial setup
       vehatt_Array(j)%PSize = isizetmp ! update the PSize
      ENDIF 

      DO is = 1, nnk
        evalue = float(pathttmp(nnk-is+1))
        CurN = CurNode-1+is
	    CALL vehatt_Insert(j,CurN,1,evalue)
	  ENDDO
	  
	  CALL vehatt_Active_Size(j,CurNode-1+nnk)
	  
	  IF(itrace == 1.and.CurNode+nnk <= vehatt_Array(j)%PSize) THEN
	    CALL vehatt_Clear(j,CurNode+nnk)
      ENDIF


END SUBROUTINE


