; mark_description "Intel(R) Visual Fortran Compiler XE for applications running on IA-32, Version 13.1.1.171 Build 20130313";
; mark_description "-S";
	.686P
 	.387
	OPTION DOTNAME
	ASSUME	CS:FLAT,DS:FLAT,SS:FLAT
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
TXTST0:
; -- Begin  _DYNUST_CONGESTIONPRICING_MODULE$
; mark_begin;
IF @Version GE 800
  .MMX
ELSEIF @Version GE 612
  .MMX
  MMWORD TEXTEQU <QWORD>
ENDIF
IF @Version GE 800
  .XMM
ELSEIF @Version GE 614
  .XMM
  XMMWORD TEXTEQU <OWORD>
ENDIF
       ALIGN     16
	PUBLIC _DYNUST_CONGESTIONPRICING_MODULE$
_DYNUST_CONGESTIONPRICING_MODULE$	PROC NEAR 
.B1.1:                          ; Preds .B1.0
        ret                                                     ;1.8
        ALIGN     16
                                ; LOE
; mark_end;
_DYNUST_CONGESTIONPRICING_MODULE$ ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_CONGESTIONPRICING_MODULE$
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_CONGESTIONPRICING_MODULE_mp_READCONGPRICINGCONFIG
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_CONGESTIONPRICING_MODULE_mp_READCONGPRICINGCONFIG
_DYNUST_CONGESTIONPRICING_MODULE_mp_READCONGPRICINGCONFIG	PROC NEAR 
.B2.1:                          ; Preds .B2.0
        push      ebx                                           ;43.12
        mov       ebx, esp                                      ;43.12
        and       esp, -16                                      ;43.12
        push      ebp                                           ;43.12
        push      ebp                                           ;43.12
        mov       ebp, DWORD PTR [4+ebx]                        ;43.12
        mov       DWORD PTR [4+esp], ebp                        ;43.12
        mov       ebp, esp                                      ;43.12
        sub       esp, 456                                      ;43.12
        lea       eax, DWORD PTR [-408+ebp]                     ;55.13
        push      32                                            ;55.13
        push      eax                                           ;55.13
        push      OFFSET FLAT: __STRLITPACK_21.0.2              ;55.13
        push      -2088435968                                   ;55.13
        push      -1                                            ;55.13
        mov       DWORD PTR [-304+ebp], edi                     ;43.12
        lea       edx, DWORD PTR [-440+ebp]                     ;55.13
        push      edx                                           ;55.13
        mov       DWORD PTR [-316+ebp], esi                     ;43.12
        mov       DWORD PTR [-440+ebp], 0                       ;55.13
        mov       DWORD PTR [-408+ebp], 27                      ;55.13
        mov       DWORD PTR [-404+ebp], OFFSET FLAT: __STRLITPACK_20 ;55.13
        mov       DWORD PTR [-400+ebp], OFFSET FLAT: _DYNUST_CONGESTIONPRICING_MODULE_mp_CONGEXT ;55.13
        call      _for_inquire                                  ;55.13
                                ; LOE
.B2.254:                        ; Preds .B2.1
        add       esp, 24                                       ;55.13
                                ; LOE
.B2.2:                          ; Preds .B2.254
        test      BYTE PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGEXT], 1 ;56.7
        je        .B2.222       ; Prob 60%                      ;56.7
                                ; LOE
.B2.3:                          ; Preds .B2.2
        push      32                                            ;57.6
        mov       DWORD PTR [-440+ebp], 0                       ;57.6
        lea       eax, DWORD PTR [-456+ebp]                     ;57.6
        push      eax                                           ;57.6
        push      OFFSET FLAT: __STRLITPACK_22.0.2              ;57.6
        push      -2088435968                                   ;57.6
        push      7777                                          ;57.6
        mov       DWORD PTR [-456+ebp], 27                      ;57.6
        lea       edx, DWORD PTR [-440+ebp]                     ;57.6
        push      edx                                           ;57.6
        mov       DWORD PTR [-452+ebp], OFFSET FLAT: __STRLITPACK_19 ;57.6
        mov       DWORD PTR [-448+ebp], 7                       ;57.6
        mov       DWORD PTR [-444+ebp], OFFSET FLAT: __STRLITPACK_18 ;57.6
        call      _for_open                                     ;57.6
                                ; LOE
.B2.255:                        ; Preds .B2.3
        add       esp, 24                                       ;57.6
                                ; LOE
.B2.4:                          ; Preds .B2.255
        push      32                                            ;58.6
        mov       DWORD PTR [-440+ebp], 0                       ;58.6
        lea       eax, DWORD PTR [-376+ebp]                     ;58.6
        push      eax                                           ;58.6
        push      OFFSET FLAT: __STRLITPACK_23.0.2              ;58.6
        push      -2088435968                                   ;58.6
        push      7777                                          ;58.6
        mov       DWORD PTR [-376+ebp], OFFSET FLAT: _DYNUST_CONGESTIONPRICING_MODULE_mp_NSEG ;58.6
        lea       edx, DWORD PTR [-440+ebp]                     ;58.6
        push      edx                                           ;58.6
        call      _for_read_seq_lis                             ;58.6
                                ; LOE
.B2.256:                        ; Preds .B2.4
        add       esp, 24                                       ;58.6
                                ; LOE
.B2.5:                          ; Preds .B2.256
        mov       esi, DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_NSEG] ;59.6
        mov       ecx, 224                                      ;59.6
        xor       edx, edx                                      ;59.6
        test      esi, esi                                      ;59.6
        push      ecx                                           ;59.6
        cmovle    esi, edx                                      ;59.6
        lea       edi, DWORD PTR [-388+ebp]                     ;59.6
        push      esi                                           ;59.6
        push      2                                             ;59.6
        push      edi                                           ;59.6
        mov       eax, 1                                        ;59.6
        mov       DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP+12], 133 ;59.6
        mov       DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP+4], ecx ;59.6
        mov       DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP+16], eax ;59.6
        mov       DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP+8], edx ;59.6
        mov       DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP+32], eax ;59.6
        mov       DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP+24], esi ;59.6
        mov       DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP+28], ecx ;59.6
        call      _for_check_mult_overflow                      ;59.6
                                ; LOE eax
.B2.257:                        ; Preds .B2.5
        add       esp, 16                                       ;59.6
                                ; LOE eax
.B2.6:                          ; Preds .B2.257
        mov       edx, DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP+12] ;59.6
        and       eax, 1                                        ;59.6
        and       edx, 1                                        ;59.6
        shl       eax, 4                                        ;59.6
        add       edx, edx                                      ;59.6
        or        edx, eax                                      ;59.6
        or        edx, 262144                                   ;59.6
        push      edx                                           ;59.6
        push      OFFSET FLAT: _DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP ;59.6
        push      DWORD PTR [-388+ebp]                          ;59.6
        call      _for_alloc_allocatable                        ;59.6
                                ; LOE
.B2.258:                        ; Preds .B2.6
        add       esp, 12                                       ;59.6
                                ; LOE
.B2.7:                          ; Preds .B2.258
        mov       edx, DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP+24] ;60.6
        test      edx, edx                                      ;60.6
        mov       ecx, DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP+32] ;60.6
        jle       .B2.26        ; Prob 50%                      ;60.6
                                ; LOE edx ecx
.B2.8:                          ; Preds .B2.7
        mov       esi, edx                                      ;60.6
        shr       esi, 31                                       ;60.6
        add       esi, edx                                      ;60.6
        sar       esi, 1                                        ;60.6
        mov       eax, DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP] ;60.6
        test      esi, esi                                      ;60.6
        mov       DWORD PTR [-384+ebp], esi                     ;60.6
        jbe       .B2.225       ; Prob 10%                      ;60.6
                                ; LOE eax edx ecx
.B2.9:                          ; Preds .B2.8
        xor       edi, edi                                      ;
        mov       DWORD PTR [-396+ebp], edx                     ;
        xor       esi, esi                                      ;
        mov       DWORD PTR [-392+ebp], ecx                     ;
        xor       ecx, ecx                                      ;
        mov       edx, DWORD PTR [-384+ebp]                     ;
                                ; LOE eax edx ecx esi edi
.B2.10:                         ; Preds .B2.10 .B2.9
        inc       edi                                           ;60.6
        mov       DWORD PTR [esi+eax], ecx                      ;60.6
        mov       DWORD PTR [224+esi+eax], ecx                  ;60.6
        add       esi, 448                                      ;60.6
        cmp       edi, edx                                      ;60.6
        jb        .B2.10        ; Prob 64%                      ;60.6
                                ; LOE eax edx ecx esi edi
.B2.11:                         ; Preds .B2.10
        mov       edx, DWORD PTR [-396+ebp]                     ;
        lea       edi, DWORD PTR [1+edi+edi]                    ;60.6
        mov       ecx, DWORD PTR [-392+ebp]                     ;
                                ; LOE eax edx ecx edi
.B2.12:                         ; Preds .B2.11 .B2.225
        lea       esi, DWORD PTR [-1+edi]                       ;60.6
        cmp       edx, esi                                      ;60.6
        jbe       .B2.14        ; Prob 10%                      ;60.6
                                ; LOE eax edx ecx edi
.B2.13:                         ; Preds .B2.12
        mov       esi, ecx                                      ;60.6
        add       edi, ecx                                      ;60.6
        neg       esi                                           ;60.6
        add       esi, edi                                      ;60.6
        lea       edi, DWORD PTR [esi*8]                        ;60.6
        sub       edi, esi                                      ;60.6
        shl       edi, 5                                        ;60.6
        mov       DWORD PTR [-224+eax+edi], 0                   ;60.6
                                ; LOE eax edx ecx
.B2.14:                         ; Preds .B2.12 .B2.13
        cmp       DWORD PTR [-384+ebp], 0                       ;61.6
        jbe       .B2.224       ; Prob 10%                      ;61.6
                                ; LOE eax edx ecx
.B2.15:                         ; Preds .B2.14
        xor       edi, edi                                      ;
        mov       DWORD PTR [-396+ebp], edx                     ;
        xor       esi, esi                                      ;
        mov       DWORD PTR [-392+ebp], ecx                     ;
        xor       ecx, ecx                                      ;
        mov       edx, DWORD PTR [-384+ebp]                     ;
                                ; LOE eax edx ecx esi edi
.B2.16:                         ; Preds .B2.16 .B2.15
        inc       edi                                           ;61.6
        mov       DWORD PTR [4+esi+eax], ecx                    ;61.6
        mov       DWORD PTR [228+esi+eax], ecx                  ;61.6
        add       esi, 448                                      ;61.6
        cmp       edi, edx                                      ;61.6
        jb        .B2.16        ; Prob 64%                      ;61.6
                                ; LOE eax edx ecx esi edi
.B2.17:                         ; Preds .B2.16
        mov       edx, DWORD PTR [-396+ebp]                     ;
        lea       edi, DWORD PTR [1+edi+edi]                    ;61.6
        mov       ecx, DWORD PTR [-392+ebp]                     ;
                                ; LOE eax edx ecx edi
.B2.18:                         ; Preds .B2.17 .B2.224
        lea       esi, DWORD PTR [-1+edi]                       ;61.6
        cmp       edx, esi                                      ;61.6
        jbe       .B2.20        ; Prob 10%                      ;61.6
                                ; LOE eax edx ecx edi
.B2.19:                         ; Preds .B2.18
        mov       esi, ecx                                      ;61.6
        add       edi, ecx                                      ;61.6
        neg       esi                                           ;61.6
        add       esi, edi                                      ;61.6
        lea       edi, DWORD PTR [esi*8]                        ;61.6
        sub       edi, esi                                      ;61.6
        shl       edi, 5                                        ;61.6
        mov       DWORD PTR [-220+eax+edi], 0                   ;61.6
                                ; LOE eax edx ecx
.B2.20:                         ; Preds .B2.18 .B2.19
        cmp       DWORD PTR [-384+ebp], 0                       ;62.6
        jbe       .B2.223       ; Prob 10%                      ;62.6
                                ; LOE eax edx ecx
.B2.21:                         ; Preds .B2.20
        xor       edi, edi                                      ;
        mov       DWORD PTR [-396+ebp], edx                     ;
        xor       esi, esi                                      ;
        mov       DWORD PTR [-392+ebp], ecx                     ;
        xor       ecx, ecx                                      ;
        mov       edx, DWORD PTR [-384+ebp]                     ;
                                ; LOE eax edx ecx esi edi
.B2.22:                         ; Preds .B2.22 .B2.21
        inc       edi                                           ;62.6
        mov       DWORD PTR [200+esi+eax], ecx                  ;62.6
        mov       DWORD PTR [424+esi+eax], ecx                  ;62.6
        add       esi, 448                                      ;62.6
        cmp       edi, edx                                      ;62.6
        jb        .B2.22        ; Prob 64%                      ;62.6
                                ; LOE eax edx ecx esi edi
.B2.23:                         ; Preds .B2.22
        mov       edx, DWORD PTR [-396+ebp]                     ;
        lea       esi, DWORD PTR [1+edi+edi]                    ;62.6
        mov       ecx, DWORD PTR [-392+ebp]                     ;
                                ; LOE eax edx ecx esi
.B2.24:                         ; Preds .B2.23 .B2.223
        lea       edi, DWORD PTR [-1+esi]                       ;62.6
        cmp       edx, edi                                      ;62.6
        jbe       .B2.26        ; Prob 10%                      ;62.6
                                ; LOE eax ecx esi
.B2.25:                         ; Preds .B2.24
        mov       edx, ecx                                      ;62.6
        add       esi, ecx                                      ;62.6
        neg       edx                                           ;62.6
        add       edx, esi                                      ;62.6
        lea       esi, DWORD PTR [edx*8]                        ;62.6
        sub       esi, edx                                      ;62.6
        shl       esi, 5                                        ;62.6
        mov       DWORD PTR [-24+eax+esi], 0                    ;62.6
                                ; LOE ecx
.B2.26:                         ; Preds .B2.24 .B2.7 .B2.25
        mov       esi, DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_NSEG] ;63.6
        test      esi, esi                                      ;63.6
        jle       .B2.89        ; Prob 3%                       ;63.6
                                ; LOE ecx esi
.B2.27:                         ; Preds .B2.26
        xor       edx, edx                                      ;
        lea       eax, DWORD PTR [ecx*8]                        ;
        sub       eax, ecx                                      ;
        shl       eax, 5                                        ;
        mov       ecx, DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP] ;64.8
        mov       DWORD PTR [-340+ebp], edx                     ;
        mov       DWORD PTR [-328+ebp], eax                     ;
        mov       DWORD PTR [-336+ebp], edx                     ;
        mov       DWORD PTR [-332+ebp], esi                     ;
                                ; LOE ecx
.B2.28:                         ; Preds .B2.87 .B2.27
        sub       ecx, DWORD PTR [-328+ebp]                     ;64.8
        lea       esi, DWORD PTR [-440+ebp]                     ;64.8
        mov       eax, DWORD PTR [-340+ebp]                     ;64.8
        push      32                                            ;64.8
        mov       DWORD PTR [-440+ebp], 0                       ;64.8
        lea       edx, DWORD PTR [444+eax+ecx]                  ;64.8
        mov       DWORD PTR [-112+ebp], edx                     ;64.8
        lea       ecx, DWORD PTR [-112+ebp]                     ;64.8
        push      ecx                                           ;64.8
        push      OFFSET FLAT: __STRLITPACK_24.0.2              ;64.8
        push      -2088435968                                   ;64.8
        push      7777                                          ;64.8
        push      esi                                           ;64.8
        call      _for_read_seq_lis                             ;64.8
                                ; LOE
.B2.259:                        ; Preds .B2.28
        add       esp, 24                                       ;64.8
                                ; LOE
.B2.29:                         ; Preds .B2.259
        imul      edx, DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP+32], -224 ;64.8
        lea       esi, DWORD PTR [-104+ebp]                     ;64.8
        mov       eax, DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP] ;64.8
        lea       edi, DWORD PTR [-440+ebp]                     ;64.8
        push      esi                                           ;64.8
        push      OFFSET FLAT: __STRLITPACK_25.0.2              ;64.8
        push      edi                                           ;64.8
        lea       ecx, DWORD PTR [424+eax+edx]                  ;64.8
        add       ecx, DWORD PTR [-340+ebp]                     ;64.8
        mov       DWORD PTR [-104+ebp], ecx                     ;64.8
        call      _for_read_seq_lis_xmit                        ;64.8
                                ; LOE
.B2.260:                        ; Preds .B2.29
        add       esp, 12                                       ;64.8
                                ; LOE
.B2.30:                         ; Preds .B2.260
        imul      edx, DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP+32], -224 ;64.8
        lea       esi, DWORD PTR [-96+ebp]                      ;64.8
        mov       eax, DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP] ;64.8
        lea       edi, DWORD PTR [-440+ebp]                     ;64.8
        push      esi                                           ;64.8
        push      OFFSET FLAT: __STRLITPACK_26.0.2              ;64.8
        push      edi                                           ;64.8
        lea       ecx, DWORD PTR [428+eax+edx]                  ;64.8
        add       ecx, DWORD PTR [-340+ebp]                     ;64.8
        mov       DWORD PTR [-96+ebp], ecx                      ;64.8
        call      _for_read_seq_lis_xmit                        ;64.8
                                ; LOE
.B2.261:                        ; Preds .B2.30
        add       esp, 12                                       ;64.8
                                ; LOE
.B2.31:                         ; Preds .B2.261
        imul      edx, DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP+32], -224 ;64.8
        lea       esi, DWORD PTR [-88+ebp]                      ;64.8
        mov       eax, DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP] ;64.8
        lea       edi, DWORD PTR [-440+ebp]                     ;64.8
        push      esi                                           ;64.8
        push      OFFSET FLAT: __STRLITPACK_27.0.2              ;64.8
        push      edi                                           ;64.8
        lea       ecx, DWORD PTR [432+eax+edx]                  ;64.8
        add       ecx, DWORD PTR [-340+ebp]                     ;64.8
        mov       DWORD PTR [-88+ebp], ecx                      ;64.8
        call      _for_read_seq_lis_xmit                        ;64.8
                                ; LOE
.B2.262:                        ; Preds .B2.31
        add       esp, 12                                       ;64.8
                                ; LOE
.B2.32:                         ; Preds .B2.262
        imul      edx, DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP+32], -224 ;64.8
        lea       esi, DWORD PTR [-80+ebp]                      ;64.8
        mov       eax, DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP] ;64.8
        lea       edi, DWORD PTR [-440+ebp]                     ;64.8
        push      esi                                           ;64.8
        push      OFFSET FLAT: __STRLITPACK_28.0.2              ;64.8
        push      edi                                           ;64.8
        lea       ecx, DWORD PTR [436+eax+edx]                  ;64.8
        add       ecx, DWORD PTR [-340+ebp]                     ;64.8
        mov       DWORD PTR [-80+ebp], ecx                      ;64.8
        call      _for_read_seq_lis_xmit                        ;64.8
                                ; LOE
.B2.263:                        ; Preds .B2.32
        add       esp, 12                                       ;64.8
                                ; LOE
.B2.33:                         ; Preds .B2.263
        imul      edx, DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP+32], -224 ;64.8
        lea       esi, DWORD PTR [-72+ebp]                      ;64.8
        mov       eax, DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP] ;64.8
        lea       edi, DWORD PTR [-440+ebp]                     ;64.8
        push      esi                                           ;64.8
        push      OFFSET FLAT: __STRLITPACK_29.0.2              ;64.8
        push      edi                                           ;64.8
        lea       ecx, DWORD PTR [440+eax+edx]                  ;64.8
        add       ecx, DWORD PTR [-340+ebp]                     ;64.8
        mov       DWORD PTR [-72+ebp], ecx                      ;64.8
        call      _for_read_seq_lis_xmit                        ;64.8
                                ; LOE
.B2.264:                        ; Preds .B2.33
        add       esp, 12                                       ;64.8
                                ; LOE
.B2.34:                         ; Preds .B2.264
        push      32                                            ;66.8
        mov       DWORD PTR [-440+ebp], 0                       ;66.8
        lea       edx, DWORD PTR [-64+ebp]                      ;66.8
        push      edx                                           ;66.8
        push      OFFSET FLAT: __STRLITPACK_30.0.2              ;66.8
        push      -2088435968                                   ;66.8
        push      7777                                          ;66.8
        lea       ecx, DWORD PTR [-440+ebp]                     ;66.8
        push      ecx                                           ;66.8
        lea       eax, DWORD PTR [-204+ebp]                     ;66.8
        mov       DWORD PTR [-64+ebp], eax                      ;66.8
        call      _for_read_seq_lis                             ;66.8
                                ; LOE
.B2.265:                        ; Preds .B2.34
        add       esp, 24                                       ;66.8
                                ; LOE
.B2.35:                         ; Preds .B2.265
        imul      edx, DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP+32], -224 ;66.8
        lea       esi, DWORD PTR [-56+ebp]                      ;66.8
        mov       eax, DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP] ;66.8
        lea       edi, DWORD PTR [-440+ebp]                     ;66.8
        push      esi                                           ;66.8
        push      OFFSET FLAT: __STRLITPACK_31.0.2              ;66.8
        push      edi                                           ;66.8
        lea       ecx, DWORD PTR [224+eax+edx]                  ;66.8
        add       ecx, DWORD PTR [-340+ebp]                     ;66.8
        mov       DWORD PTR [-56+ebp], ecx                      ;66.8
        call      _for_read_seq_lis_xmit                        ;66.8
                                ; LOE
.B2.266:                        ; Preds .B2.35
        add       esp, 12                                       ;66.8
                                ; LOE
.B2.36:                         ; Preds .B2.266
        imul      edx, DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP+32], -224 ;67.8
        mov       edi, 1                                        ;67.8
        mov       ecx, DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP] ;67.8
        mov       eax, 4                                        ;67.8
        add       edx, DWORD PTR [-340+ebp]                     ;67.8
        xor       esi, esi                                      ;67.8
        push      eax                                           ;67.8
        mov       DWORD PTR [248+ecx+edx], edi                  ;67.8
        mov       DWORD PTR [264+ecx+edx], edi                  ;67.8
        mov       edi, DWORD PTR [224+ecx+edx]                  ;67.8
        test      edi, edi                                      ;67.8
        mov       DWORD PTR [236+ecx+edx], eax                  ;67.8
        cmovl     edi, esi                                      ;67.8
        push      edi                                           ;67.8
        mov       DWORD PTR [260+ecx+edx], eax                  ;67.8
        lea       eax, DWORD PTR [-196+ebp]                     ;67.8
        push      2                                             ;67.8
        push      eax                                           ;67.8
        mov       DWORD PTR [244+ecx+edx], 5                    ;67.8
        mov       DWORD PTR [240+ecx+edx], esi                  ;67.8
        mov       DWORD PTR [256+ecx+edx], edi                  ;67.8
        call      _for_check_mult_overflow                      ;67.8
                                ; LOE eax
.B2.267:                        ; Preds .B2.36
        add       esp, 16                                       ;67.8
                                ; LOE eax
.B2.37:                         ; Preds .B2.267
        imul      ecx, DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP+32], -224 ;67.8
        and       eax, 1                                        ;67.8
        mov       edx, DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP] ;67.8
        shl       eax, 4                                        ;67.8
        or        eax, 262144                                   ;67.8
        push      eax                                           ;67.8
        lea       esi, DWORD PTR [232+edx+ecx]                  ;67.8
        add       esi, DWORD PTR [-340+ebp]                     ;67.8
        push      esi                                           ;67.8
        push      DWORD PTR [-196+ebp]                          ;67.8
        call      _for_allocate                                 ;67.8
                                ; LOE
.B2.268:                        ; Preds .B2.37
        add       esp, 12                                       ;67.8
                                ; LOE
.B2.38:                         ; Preds .B2.268
        imul      ecx, DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP+32], -224 ;68.8
        mov       esi, DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP] ;68.8
        add       ecx, DWORD PTR [-340+ebp]                     ;68.8
        mov       eax, DWORD PTR [264+ecx+esi]                  ;68.8
        mov       edx, DWORD PTR [256+ecx+esi]                  ;68.8
        test      edx, edx                                      ;68.8
        mov       DWORD PTR [-320+ebp], eax                     ;68.8
        jle       .B2.45        ; Prob 50%                      ;68.8
                                ; LOE edx ecx esi
.B2.39:                         ; Preds .B2.38
        mov       eax, edx                                      ;68.8
        shr       eax, 31                                       ;68.8
        add       eax, edx                                      ;68.8
        sar       eax, 1                                        ;68.8
        mov       DWORD PTR [-272+ebp], eax                     ;68.8
        test      eax, eax                                      ;68.8
        jbe       .B2.226       ; Prob 10%                      ;68.8
                                ; LOE edx ecx esi
.B2.40:                         ; Preds .B2.39
        mov       DWORD PTR [-396+ebp], edx                     ;
        mov       eax, DWORD PTR [260+ecx+esi]                  ;68.8
        mov       edx, DWORD PTR [-320+ebp]                     ;
        imul      edx, eax                                      ;
        mov       edi, DWORD PTR [232+ecx+esi]                  ;68.8
        sub       edi, edx                                      ;
        mov       DWORD PTR [-260+ebp], 0                       ;
        mov       DWORD PTR [-288+ebp], eax                     ;68.8
        mov       DWORD PTR [-384+ebp], ecx                     ;
        mov       eax, edi                                      ;
        mov       ecx, DWORD PTR [-260+ebp]                     ;
        mov       DWORD PTR [-392+ebp], esi                     ;
                                ; LOE eax ecx
.B2.41:                         ; Preds .B2.41 .B2.40
        mov       edx, DWORD PTR [-320+ebp]                     ;68.8
        xor       edi, edi                                      ;68.8
        mov       esi, DWORD PTR [-288+ebp]                     ;68.8
        mov       DWORD PTR [-260+ebp], ecx                     ;
        lea       edx, DWORD PTR [edx+ecx*2]                    ;68.8
        mov       ecx, esi                                      ;68.8
        imul      ecx, edx                                      ;68.8
        inc       edx                                           ;68.8
        imul      edx, esi                                      ;68.8
        mov       DWORD PTR [eax+ecx], edi                      ;68.8
        mov       ecx, DWORD PTR [-260+ebp]                     ;68.8
        inc       ecx                                           ;68.8
        mov       DWORD PTR [eax+edx], edi                      ;68.8
        cmp       ecx, DWORD PTR [-272+ebp]                     ;68.8
        jb        .B2.41        ; Prob 64%                      ;68.8
                                ; LOE eax ecx
.B2.42:                         ; Preds .B2.41
        mov       DWORD PTR [-260+ebp], ecx                     ;
        mov       eax, ecx                                      ;68.8
        mov       edx, DWORD PTR [-396+ebp]                     ;
        mov       esi, DWORD PTR [-392+ebp]                     ;
        mov       ecx, DWORD PTR [-384+ebp]                     ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;68.8
                                ; LOE eax edx ecx esi
.B2.43:                         ; Preds .B2.42 .B2.226
        lea       edi, DWORD PTR [-1+eax]                       ;68.8
        cmp       edx, edi                                      ;68.8
        jbe       .B2.45        ; Prob 10%                      ;68.8
                                ; LOE eax ecx esi
.B2.44:                         ; Preds .B2.43
        mov       edx, DWORD PTR [-320+ebp]                     ;68.8
        lea       eax, DWORD PTR [-1+eax+edx]                   ;68.8
        sub       eax, edx                                      ;68.8
        imul      eax, DWORD PTR [260+ecx+esi]                  ;68.8
        mov       ecx, DWORD PTR [232+ecx+esi]                  ;68.8
        mov       DWORD PTR [ecx+eax], 0                        ;68.8
                                ; LOE
.B2.45:                         ; Preds .B2.38 .B2.43 .B2.44
        push      32                                            ;69.8
        mov       DWORD PTR [-440+ebp], 0                       ;69.8
        lea       edx, DWORD PTR [-48+ebp]                      ;69.8
        push      edx                                           ;69.8
        push      OFFSET FLAT: __STRLITPACK_32.0.2              ;69.8
        push      -2088435968                                   ;69.8
        push      7777                                          ;69.8
        lea       ecx, DWORD PTR [-440+ebp]                     ;69.8
        push      ecx                                           ;69.8
        lea       eax, DWORD PTR [-204+ebp]                     ;69.8
        mov       DWORD PTR [-48+ebp], eax                      ;69.8
        call      _for_read_seq_lis                             ;69.8
                                ; LOE
.B2.269:                        ; Preds .B2.45
        add       esp, 24                                       ;69.8
                                ; LOE
.B2.46:                         ; Preds .B2.269
        imul      edx, DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP+32], -224 ;69.8
        lea       esi, DWORD PTR [-40+ebp]                      ;69.8
        mov       eax, DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP] ;69.8
        lea       edi, DWORD PTR [-440+ebp]                     ;69.8
        push      esi                                           ;69.8
        push      OFFSET FLAT: __STRLITPACK_33.0.2              ;69.8
        push      edi                                           ;69.8
        lea       ecx, DWORD PTR [228+eax+edx]                  ;69.8
        add       ecx, DWORD PTR [-340+ebp]                     ;69.8
        mov       DWORD PTR [-40+ebp], ecx                      ;69.8
        call      _for_read_seq_lis_xmit                        ;69.8
                                ; LOE
.B2.270:                        ; Preds .B2.46
        add       esp, 12                                       ;69.8
                                ; LOE
.B2.47:                         ; Preds .B2.270
        imul      edx, DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP+32], -224 ;70.8
        mov       edi, 1                                        ;70.8
        mov       ecx, DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP] ;70.8
        mov       eax, 4                                        ;70.8
        add       edx, DWORD PTR [-340+ebp]                     ;70.8
        xor       esi, esi                                      ;70.8
        push      eax                                           ;70.8
        mov       DWORD PTR [284+ecx+edx], edi                  ;70.8
        mov       DWORD PTR [300+ecx+edx], edi                  ;70.8
        mov       edi, DWORD PTR [228+ecx+edx]                  ;70.8
        test      edi, edi                                      ;70.8
        mov       DWORD PTR [272+ecx+edx], eax                  ;70.8
        cmovl     edi, esi                                      ;70.8
        push      edi                                           ;70.8
        mov       DWORD PTR [296+ecx+edx], eax                  ;70.8
        lea       eax, DWORD PTR [-188+ebp]                     ;70.8
        push      2                                             ;70.8
        push      eax                                           ;70.8
        mov       DWORD PTR [280+ecx+edx], 5                    ;70.8
        mov       DWORD PTR [276+ecx+edx], esi                  ;70.8
        mov       DWORD PTR [292+ecx+edx], edi                  ;70.8
        call      _for_check_mult_overflow                      ;70.8
                                ; LOE eax
.B2.271:                        ; Preds .B2.47
        add       esp, 16                                       ;70.8
                                ; LOE eax
.B2.48:                         ; Preds .B2.271
        imul      ecx, DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP+32], -224 ;70.8
        and       eax, 1                                        ;70.8
        mov       edx, DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP] ;70.8
        shl       eax, 4                                        ;70.8
        or        eax, 262144                                   ;70.8
        push      eax                                           ;70.8
        lea       esi, DWORD PTR [268+edx+ecx]                  ;70.8
        add       esi, DWORD PTR [-340+ebp]                     ;70.8
        push      esi                                           ;70.8
        push      DWORD PTR [-188+ebp]                          ;70.8
        call      _for_allocate                                 ;70.8
                                ; LOE
.B2.272:                        ; Preds .B2.48
        add       esp, 12                                       ;70.8
                                ; LOE
.B2.49:                         ; Preds .B2.272
        imul      eax, DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP+32], -224 ;71.8
        mov       edx, DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP] ;71.8
        add       eax, DWORD PTR [-340+ebp]                     ;71.8
        mov       ecx, DWORD PTR [300+eax+edx]                  ;71.8
        mov       esi, DWORD PTR [292+eax+edx]                  ;71.8
        test      esi, esi                                      ;71.8
        mov       DWORD PTR [-312+ebp], ecx                     ;71.8
        jle       .B2.56        ; Prob 50%                      ;71.8
                                ; LOE eax edx esi
.B2.50:                         ; Preds .B2.49
        mov       ecx, esi                                      ;71.8
        shr       ecx, 31                                       ;71.8
        add       ecx, esi                                      ;71.8
        sar       ecx, 1                                        ;71.8
        mov       DWORD PTR [-280+ebp], ecx                     ;71.8
        test      ecx, ecx                                      ;71.8
        jbe       .B2.227       ; Prob 10%                      ;71.8
                                ; LOE eax edx esi
.B2.51:                         ; Preds .B2.50
        mov       DWORD PTR [-380+ebp], esi                     ;
        mov       ecx, DWORD PTR [296+eax+edx]                  ;71.8
        mov       esi, DWORD PTR [-312+ebp]                     ;
        imul      esi, ecx                                      ;
        mov       edi, DWORD PTR [268+eax+edx]                  ;71.8
        sub       edi, esi                                      ;
        mov       DWORD PTR [-256+ebp], 0                       ;
        mov       DWORD PTR [-284+ebp], ecx                     ;71.8
        mov       DWORD PTR [-368+ebp], eax                     ;
        mov       eax, edi                                      ;
        mov       ecx, DWORD PTR [-256+ebp]                     ;
        mov       DWORD PTR [-372+ebp], edx                     ;
                                ; LOE eax ecx
.B2.52:                         ; Preds .B2.52 .B2.51
        mov       edx, DWORD PTR [-312+ebp]                     ;71.8
        xor       edi, edi                                      ;71.8
        mov       esi, DWORD PTR [-284+ebp]                     ;71.8
        mov       DWORD PTR [-256+ebp], ecx                     ;
        lea       edx, DWORD PTR [edx+ecx*2]                    ;71.8
        mov       ecx, esi                                      ;71.8
        imul      ecx, edx                                      ;71.8
        inc       edx                                           ;71.8
        imul      edx, esi                                      ;71.8
        mov       DWORD PTR [eax+ecx], edi                      ;71.8
        mov       ecx, DWORD PTR [-256+ebp]                     ;71.8
        inc       ecx                                           ;71.8
        mov       DWORD PTR [eax+edx], edi                      ;71.8
        cmp       ecx, DWORD PTR [-280+ebp]                     ;71.8
        jb        .B2.52        ; Prob 64%                      ;71.8
                                ; LOE eax ecx
.B2.53:                         ; Preds .B2.52
        mov       DWORD PTR [-256+ebp], ecx                     ;
        mov       esi, DWORD PTR [-380+ebp]                     ;
        mov       edx, DWORD PTR [-372+ebp]                     ;
        mov       eax, DWORD PTR [-368+ebp]                     ;
        lea       ecx, DWORD PTR [1+ecx+ecx]                    ;71.8
                                ; LOE eax edx ecx esi
.B2.54:                         ; Preds .B2.53 .B2.227
        lea       edi, DWORD PTR [-1+ecx]                       ;71.8
        cmp       esi, edi                                      ;71.8
        jbe       .B2.56        ; Prob 10%                      ;71.8
                                ; LOE eax edx ecx
.B2.55:                         ; Preds .B2.54
        mov       esi, DWORD PTR [-312+ebp]                     ;71.8
        lea       edi, DWORD PTR [-1+ecx+esi]                   ;71.8
        mov       ecx, DWORD PTR [268+eax+edx]                  ;71.8
        sub       edi, esi                                      ;71.8
        imul      edi, DWORD PTR [296+eax+edx]                  ;71.8
        mov       DWORD PTR [ecx+edi], 0                        ;71.8
                                ; LOE eax edx
.B2.56:                         ; Preds .B2.49 .B2.54 .B2.55
        mov       ecx, 1                                        ;72.8
        xor       edi, edi                                      ;72.8
        mov       DWORD PTR [336+eax+edx], ecx                  ;72.8
        mov       esi, 2                                        ;72.8
        mov       DWORD PTR [348+eax+edx], ecx                  ;72.8
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_AGGINT] ;72.8
        test      ecx, ecx                                      ;72.8
        mov       DWORD PTR [312+eax+edx], edi                  ;72.8
        cmovle    ecx, edi                                      ;72.8
        mov       edi, 8                                        ;72.8
        push      edi                                           ;72.8
        push      ecx                                           ;72.8
        mov       DWORD PTR [316+eax+edx], 5                    ;72.8
        mov       DWORD PTR [308+eax+edx], 4                    ;72.8
        mov       DWORD PTR [320+eax+edx], esi                  ;72.8
        mov       DWORD PTR [328+eax+edx], esi                  ;72.8
        mov       DWORD PTR [340+eax+edx], ecx                  ;72.8
        mov       DWORD PTR [332+eax+edx], 4                    ;72.8
        mov       DWORD PTR [344+eax+edx], edi                  ;72.8
        lea       eax, DWORD PTR [-180+ebp]                     ;72.8
        push      esi                                           ;72.8
        push      eax                                           ;72.8
        call      _for_check_mult_overflow                      ;72.8
                                ; LOE eax
.B2.273:                        ; Preds .B2.56
        add       esp, 16                                       ;72.8
                                ; LOE eax
.B2.57:                         ; Preds .B2.273
        imul      ecx, DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP+32], -224 ;72.8
        and       eax, 1                                        ;72.8
        mov       edx, DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP] ;72.8
        shl       eax, 4                                        ;72.8
        or        eax, 262144                                   ;72.8
        push      eax                                           ;72.8
        lea       esi, DWORD PTR [304+edx+ecx]                  ;72.8
        add       esi, DWORD PTR [-340+ebp]                     ;72.8
        push      esi                                           ;72.8
        push      DWORD PTR [-180+ebp]                          ;72.8
        call      _for_allocate                                 ;72.8
                                ; LOE
.B2.274:                        ; Preds .B2.57
        add       esp, 12                                       ;72.8
                                ; LOE
.B2.58:                         ; Preds .B2.274
        imul      eax, DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP+32], -224 ;73.8
        mov       edx, DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP] ;73.8
        add       eax, DWORD PTR [-340+ebp]                     ;73.8
        mov       ecx, DWORD PTR [340+eax+edx]                  ;73.8
        test      ecx, ecx                                      ;73.8
        mov       esi, DWORD PTR [348+eax+edx]                  ;73.8
        mov       DWORD PTR [-220+ebp], ecx                     ;73.8
        jle       .B2.69        ; Prob 10%                      ;73.8
                                ; LOE eax edx esi
.B2.59:                         ; Preds .B2.58
        mov       edi, DWORD PTR [336+eax+edx]                  ;73.8
        mov       ecx, DWORD PTR [328+eax+edx]                  ;73.8
        mov       DWORD PTR [-212+ebp], edi                     ;73.8
        mov       edi, ecx                                      ;
        shr       edi, 31                                       ;
        add       edi, ecx                                      ;
        sar       edi, 1                                        ;
        mov       DWORD PTR [-228+ebp], 0                       ;
        mov       DWORD PTR [-208+ebp], edi                     ;
        mov       DWORD PTR [-224+ebp], esi                     ;
        mov       DWORD PTR [-232+ebp], ecx                     ;
        mov       DWORD PTR [-240+ebp], esi                     ;
        mov       DWORD PTR [-244+ebp], edx                     ;
        mov       DWORD PTR [-248+ebp], eax                     ;
                                ; LOE
.B2.60:                         ; Preds .B2.67 .B2.59
        cmp       DWORD PTR [-232+ebp], 0                       ;73.8
        jle       .B2.67        ; Prob 50%                      ;73.8
                                ; LOE
.B2.61:                         ; Preds .B2.60
        cmp       DWORD PTR [-208+ebp], 0                       ;73.8
        jbe       .B2.228       ; Prob 10%                      ;73.8
                                ; LOE
.B2.62:                         ; Preds .B2.61
        mov       esi, DWORD PTR [-248+ebp]                     ;73.8
        xor       ecx, ecx                                      ;
        mov       edi, DWORD PTR [-244+ebp]                     ;73.8
        mov       DWORD PTR [-200+ebp], ecx                     ;
        mov       ecx, DWORD PTR [-240+ebp]                     ;
        neg       ecx                                           ;
        add       ecx, DWORD PTR [-224+ebp]                     ;
        mov       eax, DWORD PTR [304+esi+edi]                  ;73.8
        mov       edx, DWORD PTR [332+esi+edi]                  ;73.8
        imul      ecx, DWORD PTR [344+esi+edi]                  ;
        mov       esi, DWORD PTR [-212+ebp]                     ;
        imul      esi, edx                                      ;
        sub       eax, esi                                      ;
        mov       DWORD PTR [-216+ebp], edx                     ;73.8
        add       ecx, eax                                      ;
        mov       eax, DWORD PTR [-200+ebp]                     ;
                                ; LOE eax ecx
.B2.63:                         ; Preds .B2.63 .B2.62
        mov       edx, DWORD PTR [-212+ebp]                     ;73.8
        xor       edi, edi                                      ;73.8
        mov       esi, DWORD PTR [-216+ebp]                     ;73.8
        mov       DWORD PTR [-200+ebp], eax                     ;
        lea       edx, DWORD PTR [edx+eax*2]                    ;73.8
        mov       eax, esi                                      ;73.8
        imul      eax, edx                                      ;73.8
        inc       edx                                           ;73.8
        imul      edx, esi                                      ;73.8
        mov       DWORD PTR [ecx+eax], edi                      ;73.8
        mov       eax, DWORD PTR [-200+ebp]                     ;73.8
        inc       eax                                           ;73.8
        mov       DWORD PTR [ecx+edx], edi                      ;73.8
        cmp       eax, DWORD PTR [-208+ebp]                     ;73.8
        jb        .B2.63        ; Prob 63%                      ;73.8
                                ; LOE eax ecx
.B2.64:                         ; Preds .B2.63
        lea       esi, DWORD PTR [1+eax+eax]                    ;73.8
                                ; LOE esi
.B2.65:                         ; Preds .B2.64 .B2.228
        lea       eax, DWORD PTR [-1+esi]                       ;73.8
        cmp       eax, DWORD PTR [-232+ebp]                     ;73.8
        jae       .B2.67        ; Prob 10%                      ;73.8
                                ; LOE esi
.B2.66:                         ; Preds .B2.65
        mov       ecx, DWORD PTR [-248+ebp]                     ;73.8
        mov       edx, DWORD PTR [-244+ebp]                     ;73.8
        mov       edi, DWORD PTR [-224+ebp]                     ;73.8
        sub       edi, DWORD PTR [-240+ebp]                     ;73.8
        imul      edi, DWORD PTR [344+ecx+edx]                  ;73.8
        mov       DWORD PTR [-236+ebp], edi                     ;73.8
        mov       edi, DWORD PTR [-212+ebp]                     ;73.8
        mov       eax, edi                                      ;73.8
        neg       eax                                           ;73.8
        lea       esi, DWORD PTR [-1+esi+edi]                   ;73.8
        add       eax, esi                                      ;73.8
        imul      eax, DWORD PTR [332+ecx+edx]                  ;73.8
        add       eax, DWORD PTR [304+ecx+edx]                  ;73.8
        mov       edx, DWORD PTR [-236+ebp]                     ;73.8
        mov       DWORD PTR [eax+edx], 0                        ;73.8
                                ; LOE
.B2.67:                         ; Preds .B2.60 .B2.65 .B2.66
        mov       eax, DWORD PTR [-228+ebp]                     ;73.8
        inc       eax                                           ;73.8
        inc       DWORD PTR [-224+ebp]                          ;73.8
        mov       DWORD PTR [-228+ebp], eax                     ;73.8
        cmp       eax, DWORD PTR [-220+ebp]                     ;73.8
        jb        .B2.60        ; Prob 82%                      ;73.8
                                ; LOE
.B2.68:                         ; Preds .B2.67
        mov       edx, DWORD PTR [-244+ebp]                     ;
        mov       eax, DWORD PTR [-248+ebp]                     ;
                                ; LOE eax edx
.B2.69:                         ; Preds .B2.58 .B2.68
        mov       edi, 1                                        ;74.8
        mov       ecx, 4                                        ;74.8
        mov       DWORD PTR [368+eax+edx], edi                  ;74.8
        xor       esi, esi                                      ;74.8
        mov       DWORD PTR [384+eax+edx], edi                  ;74.8
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_AGGINT] ;74.8
        test      edi, edi                                      ;74.8
        push      ecx                                           ;74.8
        cmovle    edi, esi                                      ;74.8
        push      edi                                           ;74.8
        mov       DWORD PTR [364+eax+edx], 5                    ;74.8
        mov       DWORD PTR [356+eax+edx], ecx                  ;74.8
        mov       DWORD PTR [360+eax+edx], esi                  ;74.8
        mov       DWORD PTR [376+eax+edx], edi                  ;74.8
        mov       DWORD PTR [380+eax+edx], ecx                  ;74.8
        lea       eax, DWORD PTR [-172+ebp]                     ;74.8
        push      2                                             ;74.8
        push      eax                                           ;74.8
        call      _for_check_mult_overflow                      ;74.8
                                ; LOE eax
.B2.275:                        ; Preds .B2.69
        add       esp, 16                                       ;74.8
                                ; LOE eax
.B2.70:                         ; Preds .B2.275
        imul      ecx, DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP+32], -224 ;74.8
        and       eax, 1                                        ;74.8
        mov       edx, DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP] ;74.8
        shl       eax, 4                                        ;74.8
        or        eax, 262144                                   ;74.8
        push      eax                                           ;74.8
        lea       esi, DWORD PTR [352+edx+ecx]                  ;74.8
        add       esi, DWORD PTR [-340+ebp]                     ;74.8
        push      esi                                           ;74.8
        push      DWORD PTR [-172+ebp]                          ;74.8
        call      _for_allocate                                 ;74.8
                                ; LOE
.B2.276:                        ; Preds .B2.70
        add       esp, 12                                       ;74.8
                                ; LOE
.B2.71:                         ; Preds .B2.276
        imul      edx, DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP+32], -224 ;75.8
        mov       eax, DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP] ;75.8
        add       edx, DWORD PTR [-340+ebp]                     ;75.8
        mov       ecx, DWORD PTR [384+edx+eax]                  ;75.8
        mov       esi, DWORD PTR [376+edx+eax]                  ;75.8
        test      esi, esi                                      ;75.8
        mov       DWORD PTR [-308+ebp], ecx                     ;75.8
        jle       .B2.78        ; Prob 50%                      ;75.8
                                ; LOE eax edx esi
.B2.72:                         ; Preds .B2.71
        mov       ecx, esi                                      ;75.8
        shr       ecx, 31                                       ;75.8
        add       ecx, esi                                      ;75.8
        sar       ecx, 1                                        ;75.8
        mov       DWORD PTR [-276+ebp], ecx                     ;75.8
        test      ecx, ecx                                      ;75.8
        jbe       .B2.229       ; Prob 10%                      ;75.8
                                ; LOE eax edx esi
.B2.73:                         ; Preds .B2.72
        mov       DWORD PTR [-364+ebp], esi                     ;
        mov       ecx, DWORD PTR [380+edx+eax]                  ;75.8
        mov       esi, DWORD PTR [-308+ebp]                     ;
        imul      esi, ecx                                      ;
        mov       edi, DWORD PTR [352+edx+eax]                  ;75.8
        sub       edi, esi                                      ;
        mov       DWORD PTR [-264+ebp], 0                       ;
        mov       DWORD PTR [-292+ebp], ecx                     ;75.8
        mov       DWORD PTR [-360+ebp], eax                     ;
        mov       eax, edi                                      ;
        mov       ecx, DWORD PTR [-264+ebp]                     ;
        mov       DWORD PTR [-356+ebp], edx                     ;
                                ; LOE eax ecx
.B2.74:                         ; Preds .B2.74 .B2.73
        mov       edx, DWORD PTR [-308+ebp]                     ;75.8
        xor       edi, edi                                      ;75.8
        mov       esi, DWORD PTR [-292+ebp]                     ;75.8
        mov       DWORD PTR [-264+ebp], ecx                     ;
        lea       edx, DWORD PTR [edx+ecx*2]                    ;75.8
        mov       ecx, esi                                      ;75.8
        imul      ecx, edx                                      ;75.8
        inc       edx                                           ;75.8
        imul      edx, esi                                      ;75.8
        mov       DWORD PTR [eax+ecx], edi                      ;75.8
        mov       ecx, DWORD PTR [-264+ebp]                     ;75.8
        inc       ecx                                           ;75.8
        mov       DWORD PTR [eax+edx], edi                      ;75.8
        cmp       ecx, DWORD PTR [-276+ebp]                     ;75.8
        jb        .B2.74        ; Prob 64%                      ;75.8
                                ; LOE eax ecx
.B2.75:                         ; Preds .B2.74
        mov       DWORD PTR [-264+ebp], ecx                     ;
        mov       esi, DWORD PTR [-364+ebp]                     ;
        mov       eax, DWORD PTR [-360+ebp]                     ;
        mov       edx, DWORD PTR [-356+ebp]                     ;
        lea       ecx, DWORD PTR [1+ecx+ecx]                    ;75.8
                                ; LOE eax edx ecx esi
.B2.76:                         ; Preds .B2.75 .B2.229
        lea       edi, DWORD PTR [-1+ecx]                       ;75.8
        cmp       esi, edi                                      ;75.8
        jbe       .B2.78        ; Prob 10%                      ;75.8
                                ; LOE eax edx ecx
.B2.77:                         ; Preds .B2.76
        mov       esi, DWORD PTR [-308+ebp]                     ;75.8
        lea       edi, DWORD PTR [-1+ecx+esi]                   ;75.8
        mov       ecx, DWORD PTR [352+edx+eax]                  ;75.8
        sub       edi, esi                                      ;75.8
        imul      edi, DWORD PTR [380+edx+eax]                  ;75.8
        mov       DWORD PTR [ecx+edi], 0                        ;75.8
                                ; LOE eax edx
.B2.78:                         ; Preds .B2.71 .B2.76 .B2.77
        mov       edi, 1                                        ;76.8
        mov       ecx, 4                                        ;76.8
        mov       DWORD PTR [404+edx+eax], edi                  ;76.8
        xor       esi, esi                                      ;76.8
        mov       DWORD PTR [420+edx+eax], edi                  ;76.8
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_AGGINT] ;76.8
        test      edi, edi                                      ;76.8
        push      ecx                                           ;76.8
        cmovle    edi, esi                                      ;76.8
        push      edi                                           ;76.8
        mov       DWORD PTR [400+edx+eax], 5                    ;76.8
        mov       DWORD PTR [392+edx+eax], ecx                  ;76.8
        mov       DWORD PTR [396+edx+eax], esi                  ;76.8
        mov       DWORD PTR [412+edx+eax], edi                  ;76.8
        mov       DWORD PTR [416+edx+eax], ecx                  ;76.8
        lea       eax, DWORD PTR [-164+ebp]                     ;76.8
        push      2                                             ;76.8
        push      eax                                           ;76.8
        call      _for_check_mult_overflow                      ;76.8
                                ; LOE eax
.B2.277:                        ; Preds .B2.78
        add       esp, 16                                       ;76.8
                                ; LOE eax
.B2.79:                         ; Preds .B2.277
        imul      ecx, DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP+32], -224 ;76.8
        and       eax, 1                                        ;76.8
        mov       edx, DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP] ;76.8
        shl       eax, 4                                        ;76.8
        or        eax, 262144                                   ;76.8
        push      eax                                           ;76.8
        lea       esi, DWORD PTR [388+edx+ecx]                  ;76.8
        add       esi, DWORD PTR [-340+ebp]                     ;76.8
        push      esi                                           ;76.8
        push      DWORD PTR [-164+ebp]                          ;76.8
        call      _for_allocate                                 ;76.8
                                ; LOE
.B2.278:                        ; Preds .B2.79
        add       esp, 12                                       ;76.8
                                ; LOE
.B2.80:                         ; Preds .B2.278
        mov       eax, DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP+32] ;77.8
        mov       ecx, DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP] ;77.8
        lea       edx, DWORD PTR [eax*8]                        ;77.8
        sub       edx, eax                                      ;77.8
        shl       edx, 5                                        ;77.8
        mov       eax, DWORD PTR [-340+ebp]                     ;77.8
        sub       eax, edx                                      ;77.8
        mov       DWORD PTR [-328+ebp], edx                     ;77.8
        mov       esi, DWORD PTR [420+eax+ecx]                  ;77.8
        mov       edi, DWORD PTR [412+eax+ecx]                  ;77.8
        test      edi, edi                                      ;77.8
        mov       DWORD PTR [-300+ebp], esi                     ;77.8
        mov       DWORD PTR [-324+ebp], edi                     ;77.8
        jle       .B2.87        ; Prob 50%                      ;77.8
                                ; LOE eax ecx edi
.B2.81:                         ; Preds .B2.80
        mov       edx, edi                                      ;77.8
        mov       esi, edx                                      ;77.8
        shr       esi, 31                                       ;77.8
        add       esi, edx                                      ;77.8
        sar       esi, 1                                        ;77.8
        mov       DWORD PTR [-268+ebp], esi                     ;77.8
        test      esi, esi                                      ;77.8
        jbe       .B2.230       ; Prob 10%                      ;77.8
                                ; LOE eax ecx
.B2.82:                         ; Preds .B2.81
        mov       esi, DWORD PTR [416+eax+ecx]                  ;77.8
        xor       edx, edx                                      ;
        mov       edi, DWORD PTR [-300+ebp]                     ;
        imul      edi, esi                                      ;
        mov       DWORD PTR [-252+ebp], edx                     ;
        mov       edx, DWORD PTR [388+eax+ecx]                  ;77.8
        sub       edx, edi                                      ;
        mov       DWORD PTR [-344+ebp], edx                     ;
        mov       DWORD PTR [-348+ebp], eax                     ;
        mov       DWORD PTR [-296+ebp], esi                     ;77.8
        mov       edx, DWORD PTR [-252+ebp]                     ;
        mov       eax, DWORD PTR [-344+ebp]                     ;
        mov       DWORD PTR [-352+ebp], ecx                     ;
                                ; LOE eax edx
.B2.83:                         ; Preds .B2.83 .B2.82
        mov       ecx, DWORD PTR [-300+ebp]                     ;77.8
        xor       edi, edi                                      ;77.8
        mov       esi, DWORD PTR [-296+ebp]                     ;77.8
        mov       DWORD PTR [-252+ebp], edx                     ;
        lea       ecx, DWORD PTR [ecx+edx*2]                    ;77.8
        mov       edx, esi                                      ;77.8
        imul      edx, ecx                                      ;77.8
        inc       ecx                                           ;77.8
        imul      ecx, esi                                      ;77.8
        mov       DWORD PTR [eax+edx], edi                      ;77.8
        mov       edx, DWORD PTR [-252+ebp]                     ;77.8
        inc       edx                                           ;77.8
        mov       DWORD PTR [eax+ecx], edi                      ;77.8
        cmp       edx, DWORD PTR [-268+ebp]                     ;77.8
        jb        .B2.83        ; Prob 64%                      ;77.8
                                ; LOE eax edx
.B2.84:                         ; Preds .B2.83
        mov       eax, DWORD PTR [-348+ebp]                     ;
        lea       edx, DWORD PTR [1+edx+edx]                    ;77.8
        mov       ecx, DWORD PTR [-352+ebp]                     ;
                                ; LOE eax edx ecx
.B2.85:                         ; Preds .B2.84 .B2.230
        lea       esi, DWORD PTR [-1+edx]                       ;77.8
        cmp       esi, DWORD PTR [-324+ebp]                     ;77.8
        jae       .B2.87        ; Prob 10%                      ;77.8
                                ; LOE eax edx ecx
.B2.86:                         ; Preds .B2.85
        mov       esi, DWORD PTR [-300+ebp]                     ;77.8
        lea       edx, DWORD PTR [-1+edx+esi]                   ;77.8
        sub       edx, esi                                      ;77.8
        imul      edx, DWORD PTR [416+eax+ecx]                  ;77.8
        mov       eax, DWORD PTR [388+eax+ecx]                  ;77.8
        mov       DWORD PTR [eax+edx], 0                        ;77.8
                                ; LOE ecx
.B2.87:                         ; Preds .B2.80 .B2.85 .B2.86
        mov       edx, DWORD PTR [-336+ebp]                     ;63.6
        inc       edx                                           ;63.6
        mov       eax, DWORD PTR [-340+ebp]                     ;63.6
        add       eax, 224                                      ;63.6
        mov       DWORD PTR [-340+ebp], eax                     ;63.6
        mov       DWORD PTR [-336+ebp], edx                     ;63.6
        cmp       edx, DWORD PTR [-332+ebp]                     ;63.6
        jb        .B2.28        ; Prob 82%                      ;63.6
                                ; LOE ecx
.B2.89:                         ; Preds .B2.87 .B2.26
        push      32                                            ;80.6
        push      -2088435968                                   ;80.6
        push      7777                                          ;80.6
        mov       DWORD PTR [-440+ebp], 0                       ;80.6
        lea       eax, DWORD PTR [-440+ebp]                     ;80.6
        push      eax                                           ;80.6
        call      _for_rewind                                   ;80.6
                                ; LOE
.B2.279:                        ; Preds .B2.89
        add       esp, 16                                       ;80.6
                                ; LOE
.B2.90:                         ; Preds .B2.279
        push      32                                            ;81.6
        xor       eax, eax                                      ;81.6
        lea       edx, DWORD PTR [-440+ebp]                     ;81.6
        push      eax                                           ;81.6
        push      OFFSET FLAT: __STRLITPACK_34.0.2              ;81.6
        push      -2088435968                                   ;81.6
        push      7777                                          ;81.6
        push      edx                                           ;81.6
        mov       DWORD PTR [-440+ebp], eax                     ;81.6
        call      _for_read_seq_lis                             ;81.6
                                ; LOE
.B2.280:                        ; Preds .B2.90
        add       esp, 24                                       ;81.6
                                ; LOE
.B2.91:                         ; Preds .B2.280
        mov       esi, DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_NSEG] ;83.6
        xor       ecx, ecx                                      ;
        test      esi, esi                                      ;83.6
        jle       .B2.220       ; Prob 2%                       ;83.6
                                ; LOE ecx esi
.B2.92:                         ; Preds .B2.91
        mov       edx, 1                                        ;
        xor       eax, eax                                      ;
        mov       edi, 224                                      ;
        mov       DWORD PTR [-240+ebp], eax                     ;
        mov       DWORD PTR [-228+ebp], ecx                     ;
        mov       DWORD PTR [-300+ebp], eax                     ;
        mov       DWORD PTR [-84+ebp], edi                      ;
        mov       DWORD PTR [-140+ebp], edx                     ;
        mov       DWORD PTR [-136+ebp], esi                     ;
                                ; LOE
.B2.93:                         ; Preds .B2.218 .B2.92
        push      32                                            ;84.8
        xor       eax, eax                                      ;84.8
        lea       edx, DWORD PTR [-440+ebp]                     ;84.8
        push      eax                                           ;84.8
        push      OFFSET FLAT: __STRLITPACK_35.0.2              ;84.8
        push      -2088435968                                   ;84.8
        push      7777                                          ;84.8
        push      edx                                           ;84.8
        mov       DWORD PTR [-440+ebp], eax                     ;84.8
        call      _for_read_seq_lis                             ;84.8
                                ; LOE
.B2.281:                        ; Preds .B2.93
        add       esp, 24                                       ;84.8
                                ; LOE
.B2.94:                         ; Preds .B2.281
        mov       DWORD PTR [-144+ebp], esp                     ;85.8
        lea       edx, DWORD PTR [-32+ebp]                      ;85.8
        push      32                                            ;85.8
        push      edx                                           ;85.8
        push      OFFSET FLAT: __STRLITPACK_36.0.2              ;85.8
        push      -2088435968                                   ;85.8
        push      7777                                          ;85.8
        mov       DWORD PTR [-440+ebp], 0                       ;85.8
        lea       ecx, DWORD PTR [-440+ebp]                     ;85.8
        push      ecx                                           ;85.8
        lea       eax, DWORD PTR [-156+ebp]                     ;85.8
        mov       DWORD PTR [-32+ebp], eax                      ;85.8
        call      _for_read_seq_lis                             ;85.8
                                ; LOE
.B2.282:                        ; Preds .B2.94
        add       esp, 24                                       ;85.8
                                ; LOE
.B2.95:                         ; Preds .B2.282
        lea       edx, DWORD PTR [-24+ebp]                      ;85.8
        push      edx                                           ;85.8
        push      OFFSET FLAT: __STRLITPACK_37.0.2              ;85.8
        lea       ecx, DWORD PTR [-440+ebp]                     ;85.8
        push      ecx                                           ;85.8
        lea       eax, DWORD PTR [-152+ebp]                     ;85.8
        mov       DWORD PTR [-24+ebp], eax                      ;85.8
        call      _for_read_seq_lis_xmit                        ;85.8
                                ; LOE
.B2.283:                        ; Preds .B2.95
        add       esp, 12                                       ;85.8
                                ; LOE
.B2.96:                         ; Preds .B2.283
        imul      edx, DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP+32], -224 ;85.8
        xor       edi, edi                                      ;85.8
        mov       ecx, DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP] ;85.8
        add       edx, DWORD PTR [-84+ebp]                      ;85.8
        mov       esi, DWORD PTR [ecx+edx]                      ;85.8
        test      esi, esi                                      ;85.8
        mov       eax, DWORD PTR [36+ecx+edx]                   ;85.8
        cmovge    edi, esi                                      ;85.8
        shl       edi, 2                                        ;85.8
        mov       DWORD PTR [-236+ebp], eax                     ;85.8
        mov       DWORD PTR [-148+ebp], edi                     ;85.8
        cmp       eax, DWORD PTR [12+ecx+edx]                   ;85.8
        je        .B2.115       ; Prob 50%                      ;85.8
                                ; LOE edx ecx esi edi
.B2.97:                         ; Preds .B2.96
        mov       eax, edi                                      ;85.8
        call      __alloca_probe                                ;85.8
        and       esp, -16                                      ;85.8
        mov       eax, esp                                      ;85.8
                                ; LOE eax edx ecx esi
.B2.284:                        ; Preds .B2.97
        mov       DWORD PTR [-240+ebp], eax                     ;85.8
        cmp       DWORD PTR [-236+ebp], 4                       ;85.8
        jne       .B2.107       ; Prob 50%                      ;85.8
                                ; LOE edx ecx esi
.B2.98:                         ; Preds .B2.284
        test      esi, esi                                      ;85.8
        jle       .B2.114       ; Prob 50%                      ;85.8
                                ; LOE edx ecx esi
.B2.99:                         ; Preds .B2.98
        cmp       esi, 4                                        ;85.8
        jl        .B2.231       ; Prob 10%                      ;85.8
                                ; LOE edx ecx esi
.B2.100:                        ; Preds .B2.99
        xor       edi, edi                                      ;
        mov       eax, esi                                      ;85.8
        mov       DWORD PTR [-332+ebp], edi                     ;
        and       eax, -4                                       ;85.8
        mov       edi, DWORD PTR [40+ecx+edx]                   ;85.8
        shl       edi, 2                                        ;
        neg       edi                                           ;
        add       edi, DWORD PTR [8+ecx+edx]                    ;
        mov       DWORD PTR [-396+ebp], edx                     ;
        mov       DWORD PTR [-392+ebp], ecx                     ;
        mov       edx, DWORD PTR [-332+ebp]                     ;
        mov       ecx, DWORD PTR [-240+ebp]                     ;
                                ; LOE eax edx ecx esi edi
.B2.101:                        ; Preds .B2.101 .B2.100
        movdqu    xmm0, XMMWORD PTR [4+edi+edx*4]               ;85.33
        movdqa    XMMWORD PTR [ecx+edx*4], xmm0                 ;85.33
        add       edx, 4                                        ;85.8
        cmp       edx, eax                                      ;85.8
        jb        .B2.101       ; Prob 82%                      ;85.8
                                ; LOE eax edx ecx esi edi
.B2.102:                        ; Preds .B2.101
        mov       edx, DWORD PTR [-396+ebp]                     ;
        mov       ecx, DWORD PTR [-392+ebp]                     ;
                                ; LOE eax edx ecx esi
.B2.103:                        ; Preds .B2.102 .B2.231
        cmp       eax, esi                                      ;85.8
        jae       .B2.114       ; Prob 10%                      ;85.8
                                ; LOE eax edx ecx esi
.B2.104:                        ; Preds .B2.103
        mov       edi, DWORD PTR [40+ecx+edx]                   ;85.8
        shl       edi, 2                                        ;
        neg       edi                                           ;
        add       edi, DWORD PTR [8+ecx+edx]                    ;
        mov       ecx, DWORD PTR [-240+ebp]                     ;
                                ; LOE eax ecx esi edi
.B2.105:                        ; Preds .B2.105 .B2.104
        mov       edx, DWORD PTR [4+edi+eax*4]                  ;85.33
        mov       DWORD PTR [ecx+eax*4], edx                    ;85.33
        inc       eax                                           ;85.8
        cmp       eax, esi                                      ;85.8
        jb        .B2.105       ; Prob 82%                      ;85.8
        jmp       .B2.114       ; Prob 100%                     ;85.8
                                ; LOE eax ecx esi edi
.B2.107:                        ; Preds .B2.284
        test      esi, esi                                      ;85.8
        jle       .B2.114       ; Prob 50%                      ;85.8
                                ; LOE edx ecx esi
.B2.108:                        ; Preds .B2.107
        mov       eax, esi                                      ;85.8
        shr       eax, 31                                       ;85.8
        add       eax, esi                                      ;85.8
        sar       eax, 1                                        ;85.8
        mov       DWORD PTR [-248+ebp], eax                     ;85.8
        test      eax, eax                                      ;85.8
        jbe       .B2.232       ; Prob 10%                      ;85.8
                                ; LOE edx ecx esi
.B2.109:                        ; Preds .B2.108
        mov       edi, DWORD PTR [40+ecx+edx]                   ;85.8
        xor       eax, eax                                      ;
        imul      edi, DWORD PTR [-236+ebp]                     ;
        mov       DWORD PTR [-324+ebp], eax                     ;
        mov       eax, DWORD PTR [8+ecx+edx]                    ;85.8
        sub       eax, edi                                      ;
        mov       DWORD PTR [-384+ebp], esi                     ;
        mov       DWORD PTR [-396+ebp], edx                     ;
        mov       DWORD PTR [-392+ebp], ecx                     ;
        mov       edx, eax                                      ;
        mov       esi, DWORD PTR [-324+ebp]                     ;
        mov       eax, DWORD PTR [-240+ebp]                     ;
                                ; LOE eax edx esi
.B2.110:                        ; Preds .B2.110 .B2.109
        mov       ecx, DWORD PTR [-236+ebp]                     ;85.8
        lea       edi, DWORD PTR [1+esi+esi]                    ;85.8
        imul      edi, ecx                                      ;85.8
        mov       edi, DWORD PTR [edx+edi]                      ;85.33
        mov       DWORD PTR [eax+esi*8], edi                    ;85.33
        lea       edi, DWORD PTR [2+esi+esi]                    ;85.8
        imul      edi, ecx                                      ;85.8
        mov       ecx, DWORD PTR [edx+edi]                      ;85.33
        mov       DWORD PTR [4+eax+esi*8], ecx                  ;85.33
        inc       esi                                           ;85.8
        cmp       esi, DWORD PTR [-248+ebp]                     ;85.8
        jb        .B2.110       ; Prob 64%                      ;85.8
                                ; LOE eax edx esi
.B2.111:                        ; Preds .B2.110
        mov       DWORD PTR [-324+ebp], esi                     ;
        mov       eax, esi                                      ;85.8
        mov       esi, DWORD PTR [-384+ebp]                     ;
        mov       edx, DWORD PTR [-396+ebp]                     ;
        mov       ecx, DWORD PTR [-392+ebp]                     ;
        lea       edi, DWORD PTR [1+eax+eax]                    ;85.8
                                ; LOE edx ecx esi edi
.B2.112:                        ; Preds .B2.111 .B2.232
        lea       eax, DWORD PTR [-1+edi]                       ;85.8
        cmp       esi, eax                                      ;85.8
        jbe       .B2.114       ; Prob 10%                      ;85.8
                                ; LOE edx ecx esi edi
.B2.113:                        ; Preds .B2.112
        mov       eax, DWORD PTR [40+ecx+edx]                   ;85.8
        neg       eax                                           ;85.8
        add       eax, edi                                      ;85.8
        imul      eax, DWORD PTR [-236+ebp]                     ;85.8
        mov       edx, DWORD PTR [8+ecx+edx]                    ;85.8
        mov       ecx, DWORD PTR [edx+eax]                      ;85.33
        mov       eax, DWORD PTR [-240+ebp]                     ;85.33
        mov       DWORD PTR [-4+eax+edi*4], ecx                 ;85.33
                                ; LOE esi
.B2.114:                        ; Preds .B2.105 .B2.103 .B2.112 .B2.98 .B2.107
                                ;       .B2.113
        mov       eax, DWORD PTR [-240+ebp]                     ;85.8
        mov       edi, 1                                        ;
        jmp       .B2.116       ; Prob 100%                     ;
                                ; LOE eax esi edi
.B2.115:                        ; Preds .B2.96
        mov       eax, DWORD PTR [40+ecx+edx]                   ;85.8
        xor       edi, edi                                      ;
        neg       eax                                           ;85.8
        inc       eax                                           ;85.8
        imul      eax, DWORD PTR [-236+ebp]                     ;85.8
        add       eax, DWORD PTR [8+ecx+edx]                    ;85.8
                                ; LOE eax esi edi
.B2.116:                        ; Preds .B2.115 .B2.114
        mov       DWORD PTR [-276+ebp], eax                     ;85.8
        lea       eax, DWORD PTR [-280+ebp]                     ;85.8
        push      eax                                           ;85.8
        push      OFFSET FLAT: __STRLITPACK_38.0.2              ;85.8
        mov       edx, DWORD PTR [-148+ebp]                     ;85.8
        lea       ecx, DWORD PTR [-440+ebp]                     ;85.8
        push      ecx                                           ;85.8
        mov       DWORD PTR [-280+ebp], edx                     ;85.8
        call      _for_read_seq_lis_xmit                        ;85.8
                                ; LOE esi edi
.B2.285:                        ; Preds .B2.116
        add       esp, 12                                       ;85.8
                                ; LOE esi edi
.B2.117:                        ; Preds .B2.285
        test      edi, edi                                      ;85.8
        je        .B2.146       ; Prob 50%                      ;85.8
                                ; LOE esi
.B2.118:                        ; Preds .B2.117
        test      esi, esi                                      ;85.33
        jle       .B2.146       ; Prob 10%                      ;85.33
                                ; LOE esi
.B2.119:                        ; Preds .B2.118
        imul      edx, DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP+32], -224 ;85.33
        mov       eax, DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP] ;85.33
        add       edx, DWORD PTR [-84+ebp]                      ;85.33
        mov       DWORD PTR [-368+ebp], eax                     ;85.33
        mov       ecx, DWORD PTR [36+eax+edx]                   ;85.33
        cmp       ecx, 4                                        ;85.33
        mov       DWORD PTR [-292+ebp], ecx                     ;85.33
        jne       .B2.140       ; Prob 50%                      ;85.33
                                ; LOE eax edx esi al ah
.B2.120:                        ; Preds .B2.119
        cmp       esi, 4                                        ;85.33
        jl        .B2.233       ; Prob 10%                      ;85.33
                                ; LOE eax edx esi al ah
.B2.121:                        ; Preds .B2.120
        mov       ecx, eax                                      ;85.33
        mov       eax, DWORD PTR [40+ecx+edx]                   ;85.33
        shl       eax, 2                                        ;85.33
        neg       eax                                           ;85.33
        mov       ecx, DWORD PTR [8+ecx+edx]                    ;85.33
        lea       edi, DWORD PTR [ecx+eax]                      ;85.33
        mov       DWORD PTR [-340+ebp], edi                     ;85.33
        lea       edi, DWORD PTR [4+ecx+eax]                    ;85.33
        and       edi, 15                                       ;85.33
        je        .B2.124       ; Prob 50%                      ;85.33
                                ; LOE edx esi edi
.B2.122:                        ; Preds .B2.121
        test      edi, 3                                        ;85.33
        jne       .B2.233       ; Prob 10%                      ;85.33
                                ; LOE edx esi edi
.B2.123:                        ; Preds .B2.122
        neg       edi                                           ;85.33
        add       edi, 16                                       ;85.33
        shr       edi, 2                                        ;85.33
                                ; LOE edx esi edi
.B2.124:                        ; Preds .B2.123 .B2.121
        lea       eax, DWORD PTR [4+edi]                        ;85.33
        cmp       esi, eax                                      ;85.33
        jl        .B2.233       ; Prob 10%                      ;85.33
                                ; LOE edx esi edi
.B2.125:                        ; Preds .B2.124
        mov       eax, esi                                      ;85.33
        sub       eax, edi                                      ;85.33
        and       eax, 3                                        ;85.33
        neg       eax                                           ;85.33
        add       eax, esi                                      ;85.33
        test      edi, edi                                      ;85.33
        jbe       .B2.129       ; Prob 0%                       ;85.33
                                ; LOE eax edx esi edi
.B2.126:                        ; Preds .B2.125
        mov       DWORD PTR [-372+ebp], edx                     ;
        xor       ecx, ecx                                      ;
        mov       DWORD PTR [-384+ebp], esi                     ;
        mov       DWORD PTR [-380+ebp], eax                     ;
        mov       edx, DWORD PTR [-340+ebp]                     ;
        mov       esi, DWORD PTR [-240+ebp]                     ;
                                ; LOE edx ecx esi edi
.B2.127:                        ; Preds .B2.127 .B2.126
        mov       eax, DWORD PTR [esi+ecx*4]                    ;85.33
        mov       DWORD PTR [4+edx+ecx*4], eax                  ;85.33
        inc       ecx                                           ;85.33
        cmp       ecx, edi                                      ;85.33
        jb        .B2.127       ; Prob 82%                      ;85.33
                                ; LOE edx ecx esi edi
.B2.128:                        ; Preds .B2.127
        mov       eax, DWORD PTR [-380+ebp]                     ;
        mov       edx, DWORD PTR [-372+ebp]                     ;
        mov       esi, DWORD PTR [-384+ebp]                     ;
                                ; LOE eax edx esi edi
.B2.129:                        ; Preds .B2.125 .B2.128
        mov       ecx, DWORD PTR [-240+ebp]                     ;85.33
        lea       ecx, DWORD PTR [ecx+edi*4]                    ;85.33
        test      cl, 15                                        ;85.33
        je        .B2.133       ; Prob 60%                      ;85.33
                                ; LOE eax edx esi edi
.B2.130:                        ; Preds .B2.129
        mov       DWORD PTR [-384+ebp], esi                     ;
        mov       ecx, DWORD PTR [-340+ebp]                     ;
        mov       esi, DWORD PTR [-240+ebp]                     ;
                                ; LOE eax edx ecx esi edi
.B2.131:                        ; Preds .B2.131 .B2.130
        movdqu    xmm0, XMMWORD PTR [esi+edi*4]                 ;85.33
        movdqa    XMMWORD PTR [4+ecx+edi*4], xmm0               ;85.33
        add       edi, 4                                        ;85.33
        cmp       edi, eax                                      ;85.33
        jb        .B2.131       ; Prob 82%                      ;85.33
        jmp       .B2.135       ; Prob 100%                     ;85.33
                                ; LOE eax edx ecx esi edi
.B2.133:                        ; Preds .B2.129
        mov       DWORD PTR [-384+ebp], esi                     ;
        mov       ecx, DWORD PTR [-340+ebp]                     ;
        mov       esi, DWORD PTR [-240+ebp]                     ;
                                ; LOE eax edx ecx esi edi
.B2.134:                        ; Preds .B2.134 .B2.133
        movdqa    xmm0, XMMWORD PTR [esi+edi*4]                 ;85.33
        movdqa    XMMWORD PTR [4+ecx+edi*4], xmm0               ;85.33
        add       edi, 4                                        ;85.33
        cmp       edi, eax                                      ;85.33
        jb        .B2.134       ; Prob 82%                      ;85.33
                                ; LOE eax edx ecx esi edi
.B2.135:                        ; Preds .B2.131 .B2.134
        mov       esi, DWORD PTR [-384+ebp]                     ;
                                ; LOE eax edx esi
.B2.136:                        ; Preds .B2.135 .B2.233
        cmp       eax, esi                                      ;85.33
        jae       .B2.146       ; Prob 0%                       ;85.33
                                ; LOE eax edx esi
.B2.137:                        ; Preds .B2.136
        mov       edi, DWORD PTR [-368+ebp]                     ;85.33
        mov       ecx, DWORD PTR [40+edi+edx]                   ;85.33
        shl       ecx, 2                                        ;
        neg       ecx                                           ;
        add       ecx, DWORD PTR [8+edi+edx]                    ;
        mov       edi, DWORD PTR [-240+ebp]                     ;
                                ; LOE eax ecx esi edi
.B2.138:                        ; Preds .B2.138 .B2.137
        mov       edx, DWORD PTR [edi+eax*4]                    ;85.33
        mov       DWORD PTR [4+ecx+eax*4], edx                  ;85.33
        inc       eax                                           ;85.33
        cmp       eax, esi                                      ;85.33
        jb        .B2.138       ; Prob 82%                      ;85.33
        jmp       .B2.146       ; Prob 100%                     ;85.33
                                ; LOE eax ecx esi edi
.B2.140:                        ; Preds .B2.119
        mov       eax, esi                                      ;85.33
        shr       eax, 31                                       ;85.33
        add       eax, esi                                      ;85.33
        sar       eax, 1                                        ;85.33
        mov       DWORD PTR [-256+ebp], eax                     ;85.33
        test      eax, eax                                      ;85.33
        jbe       .B2.251       ; Prob 0%                       ;85.33
                                ; LOE edx esi
.B2.141:                        ; Preds .B2.140
        mov       edi, DWORD PTR [-368+ebp]                     ;85.33
        xor       eax, eax                                      ;
        mov       DWORD PTR [-372+ebp], edx                     ;
        mov       DWORD PTR [-384+ebp], esi                     ;
        mov       ecx, DWORD PTR [40+edi+edx]                   ;85.33
        imul      ecx, DWORD PTR [-292+ebp]                     ;
        mov       edi, DWORD PTR [8+edi+edx]                    ;85.33
        sub       edi, ecx                                      ;
        mov       DWORD PTR [-284+ebp], edi                     ;
                                ; LOE eax
.B2.142:                        ; Preds .B2.142 .B2.141
        mov       esi, DWORD PTR [-240+ebp]                     ;85.33
        lea       ecx, DWORD PTR [1+eax+eax]                    ;85.33
        mov       edi, DWORD PTR [-292+ebp]                     ;85.33
        imul      ecx, edi                                      ;85.33
        mov       edx, DWORD PTR [esi+eax*8]                    ;85.33
        mov       esi, DWORD PTR [-284+ebp]                     ;85.33
        mov       DWORD PTR [esi+ecx], edx                      ;85.33
        lea       edx, DWORD PTR [2+eax+eax]                    ;85.33
        imul      edx, edi                                      ;85.33
        mov       ecx, DWORD PTR [-240+ebp]                     ;85.33
        mov       edi, DWORD PTR [4+ecx+eax*8]                  ;85.33
        inc       eax                                           ;85.33
        mov       DWORD PTR [esi+edx], edi                      ;85.33
        cmp       eax, DWORD PTR [-256+ebp]                     ;85.33
        jb        .B2.142       ; Prob 64%                      ;85.33
                                ; LOE eax
.B2.143:                        ; Preds .B2.142
        mov       edx, DWORD PTR [-372+ebp]                     ;
        lea       ecx, DWORD PTR [1+eax+eax]                    ;85.33
        mov       esi, DWORD PTR [-384+ebp]                     ;
                                ; LOE edx ecx esi
.B2.144:                        ; Preds .B2.143 .B2.251
        lea       eax, DWORD PTR [-1+ecx]                       ;85.33
        cmp       esi, eax                                      ;85.33
        jbe       .B2.146       ; Prob 0%                       ;85.33
                                ; LOE edx ecx
.B2.145:                        ; Preds .B2.144
        mov       esi, DWORD PTR [-368+ebp]                     ;85.33
        mov       eax, DWORD PTR [40+esi+edx]                   ;85.33
        neg       eax                                           ;85.33
        add       eax, ecx                                      ;85.33
        imul      eax, DWORD PTR [-292+ebp]                     ;85.33
        mov       edi, DWORD PTR [8+esi+edx]                    ;85.33
        mov       edx, DWORD PTR [-240+ebp]                     ;85.33
        mov       ecx, DWORD PTR [-4+edx+ecx*4]                 ;85.33
        mov       DWORD PTR [edi+eax], ecx                      ;85.33
                                ; LOE
.B2.146:                        ; Preds .B2.138 .B2.144 .B2.117 .B2.118 .B2.136
                                ;       .B2.145
        push      0                                             ;85.8
        push      OFFSET FLAT: __STRLITPACK_39.0.2              ;85.8
        lea       eax, DWORD PTR [-440+ebp]                     ;85.8
        push      eax                                           ;85.8
        call      _for_read_seq_lis_xmit                        ;85.8
                                ; LOE
.B2.286:                        ; Preds .B2.146
        add       esp, 12                                       ;85.8
                                ; LOE
.B2.147:                        ; Preds .B2.286
        mov       eax, DWORD PTR [-144+ebp]                     ;85.8
        mov       esp, eax                                      ;85.8
                                ; LOE
.B2.288:                        ; Preds .B2.147
        mov       DWORD PTR [-128+ebp], esp                     ;86.8
        lea       edx, DWORD PTR [-16+ebp]                      ;86.8
        push      32                                            ;86.8
        push      edx                                           ;86.8
        push      OFFSET FLAT: __STRLITPACK_40.0.2              ;86.8
        push      -2088435968                                   ;86.8
        push      7777                                          ;86.8
        mov       DWORD PTR [-440+ebp], 0                       ;86.8
        lea       ecx, DWORD PTR [-440+ebp]                     ;86.8
        push      ecx                                           ;86.8
        lea       eax, DWORD PTR [-156+ebp]                     ;86.8
        mov       DWORD PTR [-16+ebp], eax                      ;86.8
        call      _for_read_seq_lis                             ;86.8
                                ; LOE
.B2.287:                        ; Preds .B2.288
        add       esp, 24                                       ;86.8
                                ; LOE
.B2.148:                        ; Preds .B2.287
        lea       edx, DWORD PTR [-8+ebp]                       ;86.8
        push      edx                                           ;86.8
        push      OFFSET FLAT: __STRLITPACK_41.0.2              ;86.8
        lea       ecx, DWORD PTR [-440+ebp]                     ;86.8
        push      ecx                                           ;86.8
        lea       eax, DWORD PTR [-152+ebp]                     ;86.8
        mov       DWORD PTR [-8+ebp], eax                       ;86.8
        call      _for_read_seq_lis_xmit                        ;86.8
                                ; LOE
.B2.289:                        ; Preds .B2.148
        add       esp, 12                                       ;86.8
                                ; LOE
.B2.149:                        ; Preds .B2.289
        imul      edx, DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP+32], -224 ;86.8
        xor       edi, edi                                      ;86.8
        mov       ecx, DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP] ;86.8
        add       edx, DWORD PTR [-84+ebp]                      ;86.8
        mov       esi, DWORD PTR [4+ecx+edx]                    ;86.8
        test      esi, esi                                      ;86.8
        mov       eax, DWORD PTR [72+ecx+edx]                   ;86.8
        cmovge    edi, esi                                      ;86.8
        shl       edi, 2                                        ;86.8
        mov       DWORD PTR [-232+ebp], eax                     ;86.8
        mov       DWORD PTR [-132+ebp], edi                     ;86.8
        cmp       eax, DWORD PTR [48+ecx+edx]                   ;86.8
        je        .B2.168       ; Prob 50%                      ;86.8
                                ; LOE edx ecx esi edi
.B2.150:                        ; Preds .B2.149
        mov       eax, edi                                      ;86.8
        call      __alloca_probe                                ;86.8
        and       esp, -16                                      ;86.8
        mov       eax, esp                                      ;86.8
                                ; LOE eax edx ecx esi
.B2.290:                        ; Preds .B2.150
        mov       DWORD PTR [-300+ebp], eax                     ;86.8
        cmp       DWORD PTR [-232+ebp], 4                       ;86.8
        jne       .B2.160       ; Prob 50%                      ;86.8
                                ; LOE edx ecx esi
.B2.151:                        ; Preds .B2.290
        test      esi, esi                                      ;86.8
        jle       .B2.167       ; Prob 50%                      ;86.8
                                ; LOE edx ecx esi
.B2.152:                        ; Preds .B2.151
        cmp       esi, 4                                        ;86.8
        jl        .B2.236       ; Prob 10%                      ;86.8
                                ; LOE edx ecx esi
.B2.153:                        ; Preds .B2.152
        xor       edi, edi                                      ;
        mov       eax, esi                                      ;86.8
        mov       DWORD PTR [-328+ebp], edi                     ;
        and       eax, -4                                       ;86.8
        mov       edi, DWORD PTR [76+ecx+edx]                   ;86.8
        shl       edi, 2                                        ;
        neg       edi                                           ;
        add       edi, DWORD PTR [44+ecx+edx]                   ;
        mov       DWORD PTR [-364+ebp], ecx                     ;
        mov       DWORD PTR [-360+ebp], edx                     ;
        mov       edx, DWORD PTR [-328+ebp]                     ;
        mov       ecx, DWORD PTR [-300+ebp]                     ;
                                ; LOE eax edx ecx esi edi
.B2.154:                        ; Preds .B2.154 .B2.153
        movdqu    xmm0, XMMWORD PTR [4+edi+edx*4]               ;86.33
        movdqa    XMMWORD PTR [ecx+edx*4], xmm0                 ;86.33
        add       edx, 4                                        ;86.8
        cmp       edx, eax                                      ;86.8
        jb        .B2.154       ; Prob 82%                      ;86.8
                                ; LOE eax edx ecx esi edi
.B2.155:                        ; Preds .B2.154
        mov       ecx, DWORD PTR [-364+ebp]                     ;
        mov       edx, DWORD PTR [-360+ebp]                     ;
                                ; LOE eax edx ecx esi
.B2.156:                        ; Preds .B2.155 .B2.236
        cmp       eax, esi                                      ;86.8
        jae       .B2.167       ; Prob 10%                      ;86.8
                                ; LOE eax edx ecx esi
.B2.157:                        ; Preds .B2.156
        mov       edi, DWORD PTR [76+ecx+edx]                   ;86.8
        shl       edi, 2                                        ;
        neg       edi                                           ;
        add       edi, DWORD PTR [44+ecx+edx]                   ;
        mov       ecx, DWORD PTR [-300+ebp]                     ;
                                ; LOE eax ecx esi edi
.B2.158:                        ; Preds .B2.158 .B2.157
        mov       edx, DWORD PTR [4+edi+eax*4]                  ;86.33
        mov       DWORD PTR [ecx+eax*4], edx                    ;86.33
        inc       eax                                           ;86.8
        cmp       eax, esi                                      ;86.8
        jb        .B2.158       ; Prob 82%                      ;86.8
        jmp       .B2.167       ; Prob 100%                     ;86.8
                                ; LOE eax ecx esi edi
.B2.160:                        ; Preds .B2.290
        test      esi, esi                                      ;86.8
        jle       .B2.167       ; Prob 50%                      ;86.8
                                ; LOE edx ecx esi
.B2.161:                        ; Preds .B2.160
        mov       eax, esi                                      ;86.8
        shr       eax, 31                                       ;86.8
        add       eax, esi                                      ;86.8
        sar       eax, 1                                        ;86.8
        mov       DWORD PTR [-244+ebp], eax                     ;86.8
        test      eax, eax                                      ;86.8
        jbe       .B2.237       ; Prob 10%                      ;86.8
                                ; LOE edx ecx esi
.B2.162:                        ; Preds .B2.161
        mov       edi, DWORD PTR [76+ecx+edx]                   ;86.8
        xor       eax, eax                                      ;
        imul      edi, DWORD PTR [-232+ebp]                     ;
        mov       DWORD PTR [-320+ebp], eax                     ;
        mov       eax, DWORD PTR [44+ecx+edx]                   ;86.8
        sub       eax, edi                                      ;
        mov       DWORD PTR [-356+ebp], esi                     ;
        mov       DWORD PTR [-360+ebp], edx                     ;
        mov       DWORD PTR [-364+ebp], ecx                     ;
        mov       edx, eax                                      ;
        mov       esi, DWORD PTR [-320+ebp]                     ;
        mov       eax, DWORD PTR [-300+ebp]                     ;
                                ; LOE eax edx esi
.B2.163:                        ; Preds .B2.163 .B2.162
        mov       ecx, DWORD PTR [-232+ebp]                     ;86.8
        lea       edi, DWORD PTR [1+esi+esi]                    ;86.8
        imul      edi, ecx                                      ;86.8
        mov       edi, DWORD PTR [edx+edi]                      ;86.33
        mov       DWORD PTR [eax+esi*8], edi                    ;86.33
        lea       edi, DWORD PTR [2+esi+esi]                    ;86.8
        imul      edi, ecx                                      ;86.8
        mov       ecx, DWORD PTR [edx+edi]                      ;86.33
        mov       DWORD PTR [4+eax+esi*8], ecx                  ;86.33
        inc       esi                                           ;86.8
        cmp       esi, DWORD PTR [-244+ebp]                     ;86.8
        jb        .B2.163       ; Prob 64%                      ;86.8
                                ; LOE eax edx esi
.B2.164:                        ; Preds .B2.163
        mov       DWORD PTR [-320+ebp], esi                     ;
        mov       eax, esi                                      ;86.8
        mov       esi, DWORD PTR [-356+ebp]                     ;
        mov       ecx, DWORD PTR [-364+ebp]                     ;
        mov       edx, DWORD PTR [-360+ebp]                     ;
        lea       edi, DWORD PTR [1+eax+eax]                    ;86.8
                                ; LOE edx ecx esi edi
.B2.165:                        ; Preds .B2.164 .B2.237
        lea       eax, DWORD PTR [-1+edi]                       ;86.8
        cmp       esi, eax                                      ;86.8
        jbe       .B2.167       ; Prob 10%                      ;86.8
                                ; LOE edx ecx esi edi
.B2.166:                        ; Preds .B2.165
        mov       eax, DWORD PTR [76+ecx+edx]                   ;86.8
        neg       eax                                           ;86.8
        add       eax, edi                                      ;86.8
        imul      eax, DWORD PTR [-232+ebp]                     ;86.8
        mov       edx, DWORD PTR [44+ecx+edx]                   ;86.8
        mov       ecx, DWORD PTR [edx+eax]                      ;86.33
        mov       eax, DWORD PTR [-300+ebp]                     ;86.33
        mov       DWORD PTR [-4+eax+edi*4], ecx                 ;86.33
                                ; LOE esi
.B2.167:                        ; Preds .B2.158 .B2.151 .B2.160 .B2.156 .B2.165
                                ;       .B2.166
        mov       eax, DWORD PTR [-300+ebp]                     ;86.8
        mov       edi, 1                                        ;
        jmp       .B2.169       ; Prob 100%                     ;
                                ; LOE eax esi edi
.B2.168:                        ; Preds .B2.149
        mov       eax, DWORD PTR [76+ecx+edx]                   ;86.8
        xor       edi, edi                                      ;
        neg       eax                                           ;86.8
        inc       eax                                           ;86.8
        imul      eax, DWORD PTR [-232+ebp]                     ;86.8
        add       eax, DWORD PTR [44+ecx+edx]                   ;86.8
                                ; LOE eax esi edi
.B2.169:                        ; Preds .B2.168 .B2.167
        mov       DWORD PTR [-268+ebp], eax                     ;86.8
        lea       eax, DWORD PTR [-272+ebp]                     ;86.8
        push      eax                                           ;86.8
        push      OFFSET FLAT: __STRLITPACK_42.0.2              ;86.8
        mov       edx, DWORD PTR [-132+ebp]                     ;86.8
        lea       ecx, DWORD PTR [-440+ebp]                     ;86.8
        push      ecx                                           ;86.8
        mov       DWORD PTR [-272+ebp], edx                     ;86.8
        call      _for_read_seq_lis_xmit                        ;86.8
                                ; LOE esi edi
.B2.291:                        ; Preds .B2.169
        add       esp, 12                                       ;86.8
                                ; LOE esi edi
.B2.170:                        ; Preds .B2.291
        test      edi, edi                                      ;86.8
        je        .B2.199       ; Prob 50%                      ;86.8
                                ; LOE esi
.B2.171:                        ; Preds .B2.170
        test      esi, esi                                      ;86.33
        jle       .B2.199       ; Prob 10%                      ;86.33
                                ; LOE esi
.B2.172:                        ; Preds .B2.171
        imul      edx, DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP+32], -224 ;86.33
        mov       eax, DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP] ;86.33
        add       edx, DWORD PTR [-84+ebp]                      ;86.33
        mov       DWORD PTR [-344+ebp], eax                     ;86.33
        mov       ecx, DWORD PTR [72+eax+edx]                   ;86.33
        cmp       ecx, 4                                        ;86.33
        mov       DWORD PTR [-264+ebp], ecx                     ;86.33
        jne       .B2.193       ; Prob 50%                      ;86.33
                                ; LOE eax edx esi al ah
.B2.173:                        ; Preds .B2.172
        cmp       esi, 4                                        ;86.33
        jl        .B2.238       ; Prob 10%                      ;86.33
                                ; LOE eax edx esi al ah
.B2.174:                        ; Preds .B2.173
        mov       ecx, eax                                      ;86.33
        mov       eax, DWORD PTR [76+ecx+edx]                   ;86.33
        shl       eax, 2                                        ;86.33
        neg       eax                                           ;86.33
        mov       ecx, DWORD PTR [44+ecx+edx]                   ;86.33
        lea       edi, DWORD PTR [ecx+eax]                      ;86.33
        mov       DWORD PTR [-336+ebp], edi                     ;86.33
        lea       edi, DWORD PTR [4+ecx+eax]                    ;86.33
        and       edi, 15                                       ;86.33
        je        .B2.177       ; Prob 50%                      ;86.33
                                ; LOE edx esi edi
.B2.175:                        ; Preds .B2.174
        test      edi, 3                                        ;86.33
        jne       .B2.238       ; Prob 10%                      ;86.33
                                ; LOE edx esi edi
.B2.176:                        ; Preds .B2.175
        neg       edi                                           ;86.33
        add       edi, 16                                       ;86.33
        shr       edi, 2                                        ;86.33
                                ; LOE edx esi edi
.B2.177:                        ; Preds .B2.176 .B2.174
        lea       eax, DWORD PTR [4+edi]                        ;86.33
        cmp       esi, eax                                      ;86.33
        jl        .B2.238       ; Prob 10%                      ;86.33
                                ; LOE edx esi edi
.B2.178:                        ; Preds .B2.177
        mov       eax, esi                                      ;86.33
        sub       eax, edi                                      ;86.33
        and       eax, 3                                        ;86.33
        neg       eax                                           ;86.33
        add       eax, esi                                      ;86.33
        test      edi, edi                                      ;86.33
        jbe       .B2.182       ; Prob 0%                       ;86.33
                                ; LOE eax edx esi edi
.B2.179:                        ; Preds .B2.178
        mov       DWORD PTR [-348+ebp], edx                     ;
        xor       ecx, ecx                                      ;
        mov       DWORD PTR [-356+ebp], esi                     ;
        mov       DWORD PTR [-352+ebp], eax                     ;
        mov       edx, DWORD PTR [-336+ebp]                     ;
        mov       esi, DWORD PTR [-300+ebp]                     ;
                                ; LOE edx ecx esi edi
.B2.180:                        ; Preds .B2.180 .B2.179
        mov       eax, DWORD PTR [esi+ecx*4]                    ;86.33
        mov       DWORD PTR [4+edx+ecx*4], eax                  ;86.33
        inc       ecx                                           ;86.33
        cmp       ecx, edi                                      ;86.33
        jb        .B2.180       ; Prob 82%                      ;86.33
                                ; LOE edx ecx esi edi
.B2.181:                        ; Preds .B2.180
        mov       eax, DWORD PTR [-352+ebp]                     ;
        mov       edx, DWORD PTR [-348+ebp]                     ;
        mov       esi, DWORD PTR [-356+ebp]                     ;
                                ; LOE eax edx esi edi
.B2.182:                        ; Preds .B2.178 .B2.181
        mov       ecx, DWORD PTR [-300+ebp]                     ;86.33
        lea       ecx, DWORD PTR [ecx+edi*4]                    ;86.33
        test      cl, 15                                        ;86.33
        je        .B2.186       ; Prob 60%                      ;86.33
                                ; LOE eax edx esi edi
.B2.183:                        ; Preds .B2.182
        mov       DWORD PTR [-356+ebp], esi                     ;
        mov       ecx, DWORD PTR [-336+ebp]                     ;
        mov       esi, DWORD PTR [-300+ebp]                     ;
                                ; LOE eax edx ecx esi edi
.B2.184:                        ; Preds .B2.184 .B2.183
        movdqu    xmm0, XMMWORD PTR [esi+edi*4]                 ;86.33
        movdqa    XMMWORD PTR [4+ecx+edi*4], xmm0               ;86.33
        add       edi, 4                                        ;86.33
        cmp       edi, eax                                      ;86.33
        jb        .B2.184       ; Prob 82%                      ;86.33
        jmp       .B2.188       ; Prob 100%                     ;86.33
                                ; LOE eax edx ecx esi edi
.B2.186:                        ; Preds .B2.182
        mov       DWORD PTR [-356+ebp], esi                     ;
        mov       ecx, DWORD PTR [-336+ebp]                     ;
        mov       esi, DWORD PTR [-300+ebp]                     ;
                                ; LOE eax edx ecx esi edi
.B2.187:                        ; Preds .B2.187 .B2.186
        movdqa    xmm0, XMMWORD PTR [esi+edi*4]                 ;86.33
        movdqa    XMMWORD PTR [4+ecx+edi*4], xmm0               ;86.33
        add       edi, 4                                        ;86.33
        cmp       edi, eax                                      ;86.33
        jb        .B2.187       ; Prob 82%                      ;86.33
                                ; LOE eax edx ecx esi edi
.B2.188:                        ; Preds .B2.184 .B2.187
        mov       esi, DWORD PTR [-356+ebp]                     ;
                                ; LOE eax edx esi
.B2.189:                        ; Preds .B2.188 .B2.238
        cmp       eax, esi                                      ;86.33
        jae       .B2.199       ; Prob 0%                       ;86.33
                                ; LOE eax edx esi
.B2.190:                        ; Preds .B2.189
        mov       edi, DWORD PTR [-344+ebp]                     ;86.33
        mov       ecx, DWORD PTR [76+edi+edx]                   ;86.33
        shl       ecx, 2                                        ;
        neg       ecx                                           ;
        add       ecx, DWORD PTR [44+edi+edx]                   ;
        mov       edi, DWORD PTR [-300+ebp]                     ;
                                ; LOE eax ecx esi edi
.B2.191:                        ; Preds .B2.191 .B2.190
        mov       edx, DWORD PTR [edi+eax*4]                    ;86.33
        mov       DWORD PTR [4+ecx+eax*4], edx                  ;86.33
        inc       eax                                           ;86.33
        cmp       eax, esi                                      ;86.33
        jb        .B2.191       ; Prob 82%                      ;86.33
        jmp       .B2.199       ; Prob 100%                     ;86.33
                                ; LOE eax ecx esi edi
.B2.193:                        ; Preds .B2.172
        mov       eax, esi                                      ;86.33
        shr       eax, 31                                       ;86.33
        add       eax, esi                                      ;86.33
        sar       eax, 1                                        ;86.33
        mov       DWORD PTR [-252+ebp], eax                     ;86.33
        test      eax, eax                                      ;86.33
        jbe       .B2.250       ; Prob 0%                       ;86.33
                                ; LOE edx esi
.B2.194:                        ; Preds .B2.193
        mov       edi, DWORD PTR [-344+ebp]                     ;86.33
        xor       eax, eax                                      ;
        mov       DWORD PTR [-348+ebp], edx                     ;
        mov       DWORD PTR [-356+ebp], esi                     ;
        mov       ecx, DWORD PTR [76+edi+edx]                   ;86.33
        imul      ecx, DWORD PTR [-264+ebp]                     ;
        mov       edi, DWORD PTR [44+edi+edx]                   ;86.33
        sub       edi, ecx                                      ;
        mov       DWORD PTR [-260+ebp], edi                     ;
                                ; LOE eax
.B2.195:                        ; Preds .B2.195 .B2.194
        mov       esi, DWORD PTR [-300+ebp]                     ;86.33
        lea       ecx, DWORD PTR [1+eax+eax]                    ;86.33
        mov       edi, DWORD PTR [-264+ebp]                     ;86.33
        imul      ecx, edi                                      ;86.33
        mov       edx, DWORD PTR [esi+eax*8]                    ;86.33
        mov       esi, DWORD PTR [-260+ebp]                     ;86.33
        mov       DWORD PTR [esi+ecx], edx                      ;86.33
        lea       edx, DWORD PTR [2+eax+eax]                    ;86.33
        imul      edx, edi                                      ;86.33
        mov       ecx, DWORD PTR [-300+ebp]                     ;86.33
        mov       edi, DWORD PTR [4+ecx+eax*8]                  ;86.33
        inc       eax                                           ;86.33
        mov       DWORD PTR [esi+edx], edi                      ;86.33
        cmp       eax, DWORD PTR [-252+ebp]                     ;86.33
        jb        .B2.195       ; Prob 64%                      ;86.33
                                ; LOE eax
.B2.196:                        ; Preds .B2.195
        mov       edx, DWORD PTR [-348+ebp]                     ;
        lea       ecx, DWORD PTR [1+eax+eax]                    ;86.33
        mov       esi, DWORD PTR [-356+ebp]                     ;
                                ; LOE edx ecx esi
.B2.197:                        ; Preds .B2.196 .B2.250
        lea       eax, DWORD PTR [-1+ecx]                       ;86.33
        cmp       esi, eax                                      ;86.33
        jbe       .B2.199       ; Prob 0%                       ;86.33
                                ; LOE edx ecx
.B2.198:                        ; Preds .B2.197
        mov       esi, DWORD PTR [-344+ebp]                     ;86.33
        mov       eax, DWORD PTR [76+esi+edx]                   ;86.33
        neg       eax                                           ;86.33
        add       eax, ecx                                      ;86.33
        imul      eax, DWORD PTR [-264+ebp]                     ;86.33
        mov       edi, DWORD PTR [44+esi+edx]                   ;86.33
        mov       edx, DWORD PTR [-300+ebp]                     ;86.33
        mov       ecx, DWORD PTR [-4+edx+ecx*4]                 ;86.33
        mov       DWORD PTR [edi+eax], ecx                      ;86.33
                                ; LOE
.B2.199:                        ; Preds .B2.191 .B2.170 .B2.171 .B2.189 .B2.197
                                ;       .B2.198
        push      0                                             ;86.8
        push      OFFSET FLAT: __STRLITPACK_43.0.2              ;86.8
        lea       eax, DWORD PTR [-440+ebp]                     ;86.8
        push      eax                                           ;86.8
        call      _for_read_seq_lis_xmit                        ;86.8
                                ; LOE
.B2.292:                        ; Preds .B2.199
        add       esp, 12                                       ;86.8
                                ; LOE
.B2.200:                        ; Preds .B2.292
        mov       eax, DWORD PTR [-128+ebp]                     ;86.8
        mov       esp, eax                                      ;86.8
                                ; LOE
.B2.293:                        ; Preds .B2.200
        mov       ecx, DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP+32] ;87.8
        mov       esi, DWORD PTR [-84+ebp]                      ;87.28
        mov       edi, DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP] ;87.8
        mov       DWORD PTR [-108+ebp], ecx                     ;87.8
        lea       eax, DWORD PTR [ecx*8]                        ;87.28
        sub       eax, ecx                                      ;87.28
        shl       eax, 5                                        ;87.28
        sub       esi, eax                                      ;87.28
        mov       DWORD PTR [-60+ebp], eax                      ;87.28
        mov       DWORD PTR [-124+ebp], esi                     ;87.28
        mov       DWORD PTR [-68+ebp], edi                      ;87.8
        mov       eax, DWORD PTR [76+edi+esi]                   ;87.31
        mov       edx, DWORD PTR [44+edi+esi]                   ;87.31
        mov       DWORD PTR [-116+ebp], eax                     ;87.31
        mov       eax, DWORD PTR [36+edi+esi]                   ;87.12
        mov       DWORD PTR [-120+ebp], edx                     ;87.31
        mov       edx, DWORD PTR [40+edi+esi]                   ;87.12
        imul      edx, eax                                      ;87.28
        mov       ecx, DWORD PTR [72+edi+esi]                   ;87.31
        mov       esi, DWORD PTR [8+edi+esi]                    ;87.12
        sub       esi, edx                                      ;87.28
        mov       edx, DWORD PTR [-120+ebp]                     ;87.28
        mov       edi, DWORD PTR [eax+esi]                      ;87.12
        mov       eax, DWORD PTR [-116+ebp]                     ;87.28
        imul      eax, ecx                                      ;87.28
        sub       edx, eax                                      ;87.28
        cmp       edi, DWORD PTR [ecx+edx]                      ;87.28
        je        .B2.204       ; Prob 50%                      ;87.28
                                ; LOE
.B2.201:                        ; Preds .B2.293
        mov       eax, 32                                       ;88.11
        lea       edx, DWORD PTR [-312+ebp]                     ;88.11
        push      eax                                           ;88.11
        push      edx                                           ;88.11
        push      OFFSET FLAT: __STRLITPACK_44.0.2              ;88.11
        push      -2088435968                                   ;88.11
        push      911                                           ;88.11
        mov       DWORD PTR [-440+ebp], 0                       ;88.11
        lea       ecx, DWORD PTR [-440+ebp]                     ;88.11
        push      ecx                                           ;88.11
        mov       DWORD PTR [-312+ebp], eax                     ;88.11
        mov       DWORD PTR [-308+ebp], OFFSET FLAT: __STRLITPACK_16 ;88.11
        call      _for_write_seq_lis                            ;88.11
                                ; LOE
.B2.294:                        ; Preds .B2.201
        add       esp, 24                                       ;88.11
                                ; LOE
.B2.202:                        ; Preds .B2.294
        mov       eax, DWORD PTR [-140+ebp]                     ;88.11
        lea       edx, DWORD PTR [-296+ebp]                     ;88.11
        push      edx                                           ;88.11
        push      OFFSET FLAT: __STRLITPACK_45.0.2              ;88.11
        mov       DWORD PTR [-296+ebp], eax                     ;88.11
        lea       ecx, DWORD PTR [-440+ebp]                     ;88.11
        push      ecx                                           ;88.11
        call      _for_write_seq_lis_xmit                       ;88.11
                                ; LOE
.B2.295:                        ; Preds .B2.202
        add       esp, 12                                       ;88.11
                                ; LOE
.B2.203:                        ; Preds .B2.295
        mov       DWORD PTR [-288+ebp], 0                       ;88.11
        lea       eax, DWORD PTR [-288+ebp]                     ;88.11
        push      eax                                           ;88.11
        push      OFFSET FLAT: __STRLITPACK_46.0.2              ;88.11
        lea       edx, DWORD PTR [-440+ebp]                     ;88.11
        push      edx                                           ;88.11
        call      _for_write_seq_lis_xmit                       ;88.11
                                ; LOE
.B2.296:                        ; Preds .B2.203
        add       esp, 12                                       ;88.11
                                ; LOE
.B2.204:                        ; Preds .B2.293 .B2.296
        mov       edx, DWORD PTR [-124+ebp]                     ;91.18
        mov       eax, DWORD PTR [-68+ebp]                      ;91.18
        mov       eax, DWORD PTR [eax+edx]                      ;91.18
        dec       eax                                           ;91.8
        test      eax, eax                                      ;91.8
        jle       .B2.211       ; Prob 2%                       ;91.8
                                ; LOE eax
.B2.205:                        ; Preds .B2.204
        mov       DWORD PTR [-76+ebp], eax                      ;
        mov       esi, 1                                        ;
                                ; LOE esi
.B2.206:                        ; Preds .B2.209 .B2.205
        mov       edx, DWORD PTR [-68+ebp]                      ;92.17
        sub       edx, DWORD PTR [-60+ebp]                      ;92.17
        mov       ecx, DWORD PTR [-84+ebp]                      ;92.17
        mov       DWORD PTR [-36+ebp], esi                      ;
        push      OFFSET FLAT: __NLITPACK_0.0.2                 ;92.17
        mov       eax, DWORD PTR [36+ecx+edx]                   ;92.17
        mov       edi, DWORD PTR [40+ecx+edx]                   ;92.17
        imul      edi, eax                                      ;92.17
        imul      esi, eax                                      ;92.17
        mov       edx, DWORD PTR [8+ecx+edx]                    ;92.17
        sub       edx, edi                                      ;92.17
        add       eax, edx                                      ;92.17
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM+32] ;92.34
        shl       ecx, 2                                        ;92.17
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM] ;92.17
        sub       edi, ecx                                      ;92.17
        mov       eax, DWORD PTR [esi+eax]                      ;92.17
        mov       ecx, DWORD PTR [esi+edx]                      ;92.17
        lea       esi, DWORD PTR [edi+eax*4]                    ;92.17
        push      esi                                           ;92.17
        mov       esi, DWORD PTR [-36+ebp]                      ;92.17
        lea       ecx, DWORD PTR [edi+ecx*4]                    ;92.17
        push      ecx                                           ;92.17
        call      _DYNUST_NETWORK_MODULE_mp_GETFLINKFROMNODE    ;92.17
                                ; LOE eax esi
.B2.297:                        ; Preds .B2.206
        add       esp, 12                                       ;92.17
                                ; LOE eax esi
.B2.207:                        ; Preds .B2.297
        test      eax, eax                                      ;93.18
        jle       .B2.246       ; Prob 16%                      ;93.18
                                ; LOE eax esi
.B2.208:                        ; Preds .B2.207
        mov       ecx, DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP+32] ;97.12
        mov       DWORD PTR [-108+ebp], ecx                     ;97.12
        mov       edx, DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP] ;97.12
        mov       DWORD PTR [-68+ebp], edx                      ;97.12
        lea       edi, DWORD PTR [ecx*8]                        ;97.12
        sub       edi, ecx                                      ;97.12
        shl       edi, 5                                        ;97.12
        mov       ecx, DWORD PTR [-84+ebp]                      ;97.12
        sub       ecx, edi                                      ;97.12
        mov       DWORD PTR [-60+ebp], edi                      ;97.12
        mov       edi, DWORD PTR [8+edx+ecx]                    ;97.12
        mov       DWORD PTR [-100+ebp], edi                     ;97.12
        mov       edi, DWORD PTR [40+edx+ecx]                   ;97.12
        neg       edi                                           ;97.12
        add       edi, esi                                      ;97.12
        inc       esi                                           ;99.8
        imul      edi, DWORD PTR [36+edx+ecx]                   ;97.12
        mov       edx, DWORD PTR [-100+ebp]                     ;97.12
        mov       DWORD PTR [edx+edi], eax                      ;97.12
                                ; LOE esi
.B2.209:                        ; Preds .B2.249 .B2.208
        cmp       esi, DWORD PTR [-76+ebp]                      ;99.8
        jle       .B2.206       ; Prob 82%                      ;99.8
                                ; LOE esi
.B2.211:                        ; Preds .B2.209 .B2.204
        imul      eax, DWORD PTR [-108+ebp], -224               ;100.8
        add       eax, DWORD PTR [-68+ebp]                      ;100.8
        mov       edx, DWORD PTR [-84+ebp]                      ;100.18
        mov       edx, DWORD PTR [4+eax+edx]                    ;100.18
        dec       edx                                           ;100.8
        test      edx, edx                                      ;100.8
        jle       .B2.218       ; Prob 2%                       ;100.8
                                ; LOE edx
.B2.212:                        ; Preds .B2.211
        mov       eax, 1                                        ;
        mov       DWORD PTR [-44+ebp], eax                      ;
        mov       DWORD PTR [-52+ebp], edx                      ;
                                ; LOE
.B2.213:                        ; Preds .B2.216 .B2.212
        mov       ecx, DWORD PTR [-68+ebp]                      ;101.17
        sub       ecx, DWORD PTR [-60+ebp]                      ;101.17
        mov       edi, DWORD PTR [-84+ebp]                      ;101.17
        mov       edx, DWORD PTR [-44+ebp]                      ;101.17
        push      OFFSET FLAT: __NLITPACK_0.0.2                 ;101.17
        mov       eax, DWORD PTR [72+edi+ecx]                   ;101.17
        mov       esi, DWORD PTR [76+edi+ecx]                   ;101.17
        imul      esi, eax                                      ;101.17
        imul      edx, eax                                      ;101.17
        mov       ecx, DWORD PTR [44+edi+ecx]                   ;101.17
        sub       ecx, esi                                      ;101.17
        add       eax, ecx                                      ;101.17
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM+32] ;101.34
        shl       esi, 2                                        ;101.17
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM] ;101.17
        sub       edi, esi                                      ;101.17
        mov       eax, DWORD PTR [edx+eax]                      ;101.17
        mov       esi, DWORD PTR [edx+ecx]                      ;101.17
        lea       edx, DWORD PTR [edi+eax*4]                    ;101.17
        push      edx                                           ;101.17
        lea       esi, DWORD PTR [edi+esi*4]                    ;101.17
        push      esi                                           ;101.17
        call      _DYNUST_NETWORK_MODULE_mp_GETFLINKFROMNODE    ;101.17
                                ; LOE eax
.B2.298:                        ; Preds .B2.213
        add       esp, 12                                       ;101.17
                                ; LOE eax
.B2.214:                        ; Preds .B2.298
        test      eax, eax                                      ;102.18
        jle       .B2.242       ; Prob 16%                      ;102.18
                                ; LOE eax
.B2.215:                        ; Preds .B2.214
        mov       edx, DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP+32] ;106.12
        mov       esi, DWORD PTR [-84+ebp]                      ;106.12
        mov       edi, DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP] ;106.12
        mov       DWORD PTR [-68+ebp], edi                      ;106.12
        lea       ecx, DWORD PTR [edx*8]                        ;106.12
        sub       ecx, edx                                      ;106.12
        shl       ecx, 5                                        ;106.12
        sub       esi, ecx                                      ;106.12
        mov       DWORD PTR [-60+ebp], ecx                      ;106.12
        mov       edx, DWORD PTR [44+edi+esi]                   ;106.12
        mov       ecx, DWORD PTR [76+edi+esi]                   ;106.12
        neg       ecx                                           ;106.12
        mov       DWORD PTR [-92+ebp], edx                      ;106.12
        mov       edx, DWORD PTR [-44+ebp]                      ;106.12
        add       ecx, edx                                      ;106.12
        imul      ecx, DWORD PTR [72+edi+esi]                   ;106.12
        inc       edx                                           ;108.8
        mov       esi, DWORD PTR [-92+ebp]                      ;106.12
        mov       DWORD PTR [-44+ebp], edx                      ;108.8
        mov       DWORD PTR [esi+ecx], eax                      ;106.12
                                ; LOE
.B2.216:                        ; Preds .B2.245 .B2.215
        mov       eax, DWORD PTR [-44+ebp]                      ;108.8
        cmp       eax, DWORD PTR [-52+ebp]                      ;108.8
        jle       .B2.213       ; Prob 82%                      ;108.8
                                ; LOE
.B2.218:                        ; Preds .B2.216 .B2.211
        mov       edx, DWORD PTR [-140+ebp]                     ;109.6
        inc       edx                                           ;109.6
        mov       eax, DWORD PTR [-84+ebp]                      ;109.6
        add       eax, 224                                      ;109.6
        mov       DWORD PTR [-84+ebp], eax                      ;109.6
        mov       DWORD PTR [-140+ebp], edx                     ;109.6
        cmp       edx, DWORD PTR [-136+ebp]                     ;109.6
        jle       .B2.93        ; Prob 82%                      ;109.6
                                ; LOE
.B2.219:                        ; Preds .B2.218
        mov       ecx, DWORD PTR [-228+ebp]                     ;
                                ; LOE ecx
.B2.220:                        ; Preds .B2.91 .B2.219
        test      cl, 1                                         ;110.9
        jne       .B2.241       ; Prob 3%                       ;110.9
                                ; LOE
.B2.221:                        ; Preds .B2.220 .B2.300
        push      32                                            ;111.12
        xor       eax, eax                                      ;111.12
        lea       edx, DWORD PTR [-440+ebp]                     ;111.12
        push      eax                                           ;111.12
        push      OFFSET FLAT: __STRLITPACK_54.0.2              ;111.12
        push      -2088435968                                   ;111.12
        push      7777                                          ;111.12
        push      edx                                           ;111.12
        mov       DWORD PTR [-440+ebp], eax                     ;111.12
        call      _for_close                                    ;111.12
                                ; LOE
.B2.299:                        ; Preds .B2.221
        add       esp, 24                                       ;111.12
                                ; LOE
.B2.222:                        ; Preds .B2.2 .B2.299
        mov       esi, DWORD PTR [-316+ebp]                     ;114.1
        mov       edi, DWORD PTR [-304+ebp]                     ;114.1
        mov       esp, ebp                                      ;114.1
        pop       ebp                                           ;114.1
        mov       esp, ebx                                      ;114.1
        pop       ebx                                           ;114.1
        ret                                                     ;114.1
                                ; LOE
.B2.223:                        ; Preds .B2.20                  ; Infreq
        mov       esi, 1                                        ;
        jmp       .B2.24        ; Prob 100%                     ;
                                ; LOE eax edx ecx esi
.B2.224:                        ; Preds .B2.14                  ; Infreq
        mov       edi, 1                                        ;
        jmp       .B2.18        ; Prob 100%                     ;
                                ; LOE eax edx ecx edi
.B2.225:                        ; Preds .B2.8                   ; Infreq
        mov       edi, 1                                        ;
        jmp       .B2.12        ; Prob 100%                     ;
                                ; LOE eax edx ecx edi
.B2.226:                        ; Preds .B2.39                  ; Infreq
        mov       eax, 1                                        ;
        jmp       .B2.43        ; Prob 100%                     ;
                                ; LOE eax edx ecx esi
.B2.227:                        ; Preds .B2.50                  ; Infreq
        mov       ecx, 1                                        ;
        jmp       .B2.54        ; Prob 100%                     ;
                                ; LOE eax edx ecx esi
.B2.228:                        ; Preds .B2.61                  ; Infreq
        mov       esi, 1                                        ;
        jmp       .B2.65        ; Prob 100%                     ;
                                ; LOE esi
.B2.229:                        ; Preds .B2.72                  ; Infreq
        mov       ecx, 1                                        ;
        jmp       .B2.76        ; Prob 100%                     ;
                                ; LOE eax edx ecx esi
.B2.230:                        ; Preds .B2.81                  ; Infreq
        mov       edx, 1                                        ;
        jmp       .B2.85        ; Prob 100%                     ;
                                ; LOE eax edx ecx
.B2.231:                        ; Preds .B2.99                  ; Infreq
        xor       eax, eax                                      ;85.8
        jmp       .B2.103       ; Prob 100%                     ;85.8
                                ; LOE eax edx ecx esi
.B2.232:                        ; Preds .B2.108                 ; Infreq
        mov       edi, 1                                        ;
        jmp       .B2.112       ; Prob 100%                     ;
                                ; LOE edx ecx esi edi
.B2.233:                        ; Preds .B2.120 .B2.124 .B2.122 ; Infreq
        xor       eax, eax                                      ;85.33
        jmp       .B2.136       ; Prob 100%                     ;85.33
                                ; LOE eax edx esi
.B2.236:                        ; Preds .B2.152                 ; Infreq
        xor       eax, eax                                      ;86.8
        jmp       .B2.156       ; Prob 100%                     ;86.8
                                ; LOE eax edx ecx esi
.B2.237:                        ; Preds .B2.161                 ; Infreq
        mov       edi, 1                                        ;
        jmp       .B2.165       ; Prob 100%                     ;
                                ; LOE edx ecx esi edi
.B2.238:                        ; Preds .B2.173 .B2.177 .B2.175 ; Infreq
        xor       eax, eax                                      ;86.33
        jmp       .B2.189       ; Prob 100%                     ;86.33
                                ; LOE eax edx esi
.B2.241:                        ; Preds .B2.220                 ; Infreq
        push      32                                            ;110.20
        xor       eax, eax                                      ;110.20
        push      eax                                           ;110.20
        push      eax                                           ;110.20
        push      -2088435968                                   ;110.20
        push      eax                                           ;110.20
        push      OFFSET FLAT: __STRLITPACK_53                  ;110.20
        call      _for_stop_core                                ;110.20
                                ; LOE
.B2.300:                        ; Preds .B2.241                 ; Infreq
        add       esp, 24                                       ;110.20
        jmp       .B2.221       ; Prob 100%                     ;110.20
                                ; LOE
.B2.242:                        ; Preds .B2.214                 ; Infreq
        push      32                                            ;103.12
        push      OFFSET FLAT: DYNUST_CONGESTIONPRICING_MODULE_mp_READCONGPRICINGCONFIG$format_pack.0.2+104 ;103.12
        mov       eax, DWORD PTR [-140+ebp]                     ;103.12
        lea       edx, DWORD PTR [-192+ebp]                     ;103.12
        push      edx                                           ;103.12
        push      OFFSET FLAT: __STRLITPACK_50.0.2              ;103.12
        push      -2088435968                                   ;103.12
        push      911                                           ;103.12
        mov       DWORD PTR [-440+ebp], 0                       ;103.12
        lea       ecx, DWORD PTR [-440+ebp]                     ;103.12
        push      ecx                                           ;103.12
        mov       DWORD PTR [-192+ebp], eax                     ;103.12
        call      _for_write_seq_fmt                            ;103.12
                                ; LOE
.B2.301:                        ; Preds .B2.242                 ; Infreq
        add       esp, 28                                       ;103.12
                                ; LOE
.B2.243:                        ; Preds .B2.301                 ; Infreq
        mov       esi, DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP+32] ;103.12
        mov       edi, DWORD PTR [-84+ebp]                      ;103.12
        mov       edx, DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP] ;103.12
        mov       DWORD PTR [-68+ebp], edx                      ;103.12
        lea       ecx, DWORD PTR [esi*8]                        ;103.12
        sub       ecx, esi                                      ;103.12
        shl       ecx, 5                                        ;103.12
        sub       edi, ecx                                      ;103.12
        mov       DWORD PTR [-60+ebp], ecx                      ;103.12
        mov       ecx, DWORD PTR [-44+ebp]                      ;103.12
        mov       esi, DWORD PTR [72+edx+edi]                   ;103.94
        imul      ecx, esi                                      ;103.12
        mov       eax, DWORD PTR [44+edx+edi]                   ;103.94
        add       ecx, eax                                      ;103.12
        mov       edx, DWORD PTR [76+edx+edi]                   ;103.94
        lea       edi, DWORD PTR [-176+ebp]                     ;103.12
        imul      edx, esi                                      ;103.12
        sub       ecx, edx                                      ;103.12
        mov       DWORD PTR [-208+ebp], eax                     ;103.94
        push      edi                                           ;103.12
        push      OFFSET FLAT: __STRLITPACK_51.0.2              ;103.12
        mov       eax, DWORD PTR [ecx]                          ;103.12
        mov       DWORD PTR [-176+ebp], eax                     ;103.12
        lea       eax, DWORD PTR [-440+ebp]                     ;103.12
        push      eax                                           ;103.12
        mov       DWORD PTR [-212+ebp], edx                     ;103.12
        call      _for_write_seq_fmt_xmit                       ;103.12
                                ; LOE esi
.B2.302:                        ; Preds .B2.243                 ; Infreq
        add       esp, 12                                       ;103.12
                                ; LOE esi
.B2.244:                        ; Preds .B2.302                 ; Infreq
        mov       eax, DWORD PTR [-44+ebp]                      ;103.109
        lea       edi, DWORD PTR [-440+ebp]                     ;103.12
        inc       eax                                           ;103.109
        imul      esi, eax                                      ;103.109
        mov       edx, DWORD PTR [-208+ebp]                     ;103.12
        add       edx, esi                                      ;103.12
        lea       esi, DWORD PTR [-160+ebp]                     ;103.12
        sub       edx, DWORD PTR [-212+ebp]                     ;103.12
        push      esi                                           ;103.12
        push      OFFSET FLAT: __STRLITPACK_52.0.2              ;103.12
        mov       ecx, DWORD PTR [edx]                          ;103.12
        push      edi                                           ;103.12
        mov       DWORD PTR [-44+ebp], eax                      ;103.109
        mov       DWORD PTR [-160+ebp], ecx                     ;103.12
        call      _for_write_seq_fmt_xmit                       ;103.12
                                ; LOE
.B2.303:                        ; Preds .B2.244                 ; Infreq
        add       esp, 12                                       ;103.12
                                ; LOE
.B2.245:                        ; Preds .B2.303                 ; Infreq
        mov       DWORD PTR [-228+ebp], -1                      ;104.12
        jmp       .B2.216       ; Prob 100%                     ;104.12
                                ; LOE
.B2.246:                        ; Preds .B2.207                 ; Infreq
        push      32                                            ;94.12
        push      OFFSET FLAT: DYNUST_CONGESTIONPRICING_MODULE_mp_READCONGPRICINGCONFIG$format_pack.0.2 ;94.12
        mov       eax, DWORD PTR [-140+ebp]                     ;94.12
        lea       edx, DWORD PTR [-200+ebp]                     ;94.12
        push      edx                                           ;94.12
        push      OFFSET FLAT: __STRLITPACK_47.0.2              ;94.12
        push      -2088435968                                   ;94.12
        push      911                                           ;94.12
        mov       DWORD PTR [-440+ebp], 0                       ;94.12
        lea       ecx, DWORD PTR [-440+ebp]                     ;94.12
        push      ecx                                           ;94.12
        mov       DWORD PTR [-200+ebp], eax                     ;94.12
        call      _for_write_seq_fmt                            ;94.12
                                ; LOE esi
.B2.304:                        ; Preds .B2.246                 ; Infreq
        add       esp, 28                                       ;94.12
                                ; LOE esi
.B2.247:                        ; Preds .B2.304                 ; Infreq
        mov       eax, DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP+32] ;94.12
        mov       ecx, DWORD PTR [-84+ebp]                      ;94.12
        mov       edi, DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP] ;94.12
        mov       DWORD PTR [-108+ebp], eax                     ;94.12
        lea       edx, DWORD PTR [eax*8]                        ;94.12
        sub       edx, eax                                      ;94.12
        shl       edx, 5                                        ;94.12
        sub       ecx, edx                                      ;94.12
        mov       DWORD PTR [-60+ebp], edx                      ;94.12
        mov       DWORD PTR [-68+ebp], edi                      ;94.12
        mov       edx, DWORD PTR [36+edi+ecx]                   ;94.94
        mov       eax, DWORD PTR [8+edi+ecx]                    ;94.94
        mov       ecx, DWORD PTR [40+edi+ecx]                   ;94.94
        mov       edi, esi                                      ;94.12
        imul      edi, edx                                      ;94.12
        imul      ecx, edx                                      ;94.12
        add       edi, eax                                      ;94.12
        sub       edi, ecx                                      ;94.12
        mov       DWORD PTR [-216+ebp], edx                     ;94.94
        lea       edx, DWORD PTR [-184+ebp]                     ;94.12
        push      edx                                           ;94.12
        mov       DWORD PTR [-220+ebp], eax                     ;94.94
        mov       DWORD PTR [-224+ebp], ecx                     ;94.12
        lea       ecx, DWORD PTR [-440+ebp]                     ;94.12
        push      OFFSET FLAT: __STRLITPACK_48.0.2              ;94.12
        mov       eax, DWORD PTR [edi]                          ;94.12
        push      ecx                                           ;94.12
        mov       DWORD PTR [-184+ebp], eax                     ;94.12
        call      _for_write_seq_fmt_xmit                       ;94.12
                                ; LOE esi
.B2.305:                        ; Preds .B2.247                 ; Infreq
        add       esp, 12                                       ;94.12
                                ; LOE esi
.B2.248:                        ; Preds .B2.305                 ; Infreq
        inc       esi                                           ;94.110
        lea       edi, DWORD PTR [-168+ebp]                     ;94.12
        mov       eax, DWORD PTR [-216+ebp]                     ;94.110
        imul      eax, esi                                      ;94.110
        mov       edx, DWORD PTR [-220+ebp]                     ;94.12
        add       edx, eax                                      ;94.12
        lea       eax, DWORD PTR [-440+ebp]                     ;94.12
        sub       edx, DWORD PTR [-224+ebp]                     ;94.12
        push      edi                                           ;94.12
        push      OFFSET FLAT: __STRLITPACK_49.0.2              ;94.12
        mov       ecx, DWORD PTR [edx]                          ;94.12
        push      eax                                           ;94.12
        mov       DWORD PTR [-168+ebp], ecx                     ;94.12
        call      _for_write_seq_fmt_xmit                       ;94.12
                                ; LOE esi
.B2.306:                        ; Preds .B2.248                 ; Infreq
        add       esp, 12                                       ;94.12
                                ; LOE esi
.B2.249:                        ; Preds .B2.306                 ; Infreq
        mov       DWORD PTR [-228+ebp], -1                      ;95.12
        jmp       .B2.209       ; Prob 100%                     ;95.12
                                ; LOE esi
.B2.250:                        ; Preds .B2.193                 ; Infreq
        mov       ecx, 1                                        ;
        jmp       .B2.197       ; Prob 100%                     ;
                                ; LOE edx ecx esi
.B2.251:                        ; Preds .B2.140                 ; Infreq
        mov       ecx, 1                                        ;
        jmp       .B2.144       ; Prob 100%                     ;
        ALIGN     16
                                ; LOE edx ecx esi
; mark_end;
_DYNUST_CONGESTIONPRICING_MODULE_mp_READCONGPRICINGCONFIG ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
DYNUST_CONGESTIONPRICING_MODULE_mp_READCONGPRICINGCONFIG$format_pack.0.2	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	34
	DB	0
	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	67
	DB	111
	DB	110
	DB	103
	DB	32
	DB	80
	DB	114
	DB	105
	DB	99
	DB	101
	DB	32
	DB	72
	DB	79
	DB	84
	DB	32
	DB	83
	DB	101
	DB	103
	DB	109
	DB	101
	DB	110
	DB	116
	DB	32
	DB	35
	DB	32
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	4
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	6
	DB	0
	DB	32
	DB	110
	DB	111
	DB	100
	DB	101
	DB	32
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	4
	DB	0
	DB	32
	DB	116
	DB	111
	DB	32
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	34
	DB	0
	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	67
	DB	111
	DB	110
	DB	103
	DB	32
	DB	80
	DB	114
	DB	105
	DB	99
	DB	101
	DB	32
	DB	71
	DB	80
	DB	76
	DB	32
	DB	83
	DB	101
	DB	103
	DB	109
	DB	101
	DB	110
	DB	116
	DB	32
	DB	35
	DB	32
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	4
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	6
	DB	0
	DB	32
	DB	110
	DB	111
	DB	100
	DB	101
	DB	32
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	4
	DB	0
	DB	32
	DB	116
	DB	111
	DB	32
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
_DATA	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
__STRLITPACK_21.0.2	DB	56
	DB	4
	DB	13
	DB	0
	DB	16
	DB	3
	DB	27
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_22.0.2	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	26
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_23.0.2	DB	9
	DB	3
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_24.0.2	DB	9
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_25.0.2	DB	26
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_26.0.2	DB	26
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_27.0.2	DB	26
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_28.0.2	DB	26
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_29.0.2	DB	26
	DB	3
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_30.0.2	DB	9
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_31.0.2	DB	9
	DB	3
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_32.0.2	DB	9
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_33.0.2	DB	9
	DB	3
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_34.0.2	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_35.0.2	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_36.0.2	DB	9
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_37.0.2	DB	9
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_38.0.2	DB	9
	DB	5
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_39.0.2	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_40.0.2	DB	9
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_41.0.2	DB	9
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_42.0.2	DB	9
	DB	5
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_43.0.2	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_44.0.2	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_45.0.2	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_46.0.2	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__NLITPACK_0.0.2	DD	1
__STRLITPACK_47.0.2	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_48.0.2	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_49.0.2	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_50.0.2	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_51.0.2	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_52.0.2	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_54.0.2	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_CONGESTIONPRICING_MODULE_mp_READCONGPRICINGCONFIG
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_CONGESTIONPRICING_MODULE_mp_PRICECAL
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_CONGESTIONPRICING_MODULE_mp_PRICECAL
_DYNUST_CONGESTIONPRICING_MODULE_mp_PRICECAL	PROC NEAR 
.B3.1:                          ; Preds .B3.0
        push      esi                                           ;117.12
        push      edi                                           ;117.12
        push      ebx                                           ;117.12
        push      ebp                                           ;117.12
        sub       esp, 216                                      ;117.12
        mov       edx, DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_NSEG] ;126.1
        mov       DWORD PTR [144+esp], edx                      ;126.1
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NTOLLLINK] ;153.6
        mov       ebp, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELTIME+44] ;129.23
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELTIME+40] ;129.23
        imul      ebp, esi                                      ;
        movss     xmm6, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TOLLVOTT] ;142.34
        movss     xmm5, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TOLLVOTA] ;136.32
        movss     xmm4, DWORD PTR [_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT] ;153.6
        movss     xmm0, DWORD PTR [_2il0floatpacket.14]         ;128.41
        mov       DWORD PTR [28+esp], edx                       ;153.6
        imul      edx, DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP+32], -224 ;
        movss     xmm2, DWORD PTR [_2il0floatpacket.15]         ;153.6
        movss     xmm1, DWORD PTR [_2il0floatpacket.16]         ;153.6
        movss     DWORD PTR [32+esp], xmm5                      ;153.6
        movss     DWORD PTR [4+esp], xmm6                       ;153.6
        mov       edi, DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP] ;153.6
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_AGGINT] ;153.6
        mov       ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELTIME] ;129.23
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELTIME+32] ;129.23
        sub       ebx, ebp                                      ;
        shl       ecx, 2                                        ;
        mov       DWORD PTR [176+esp], eax                      ;153.6
        lea       eax, DWORD PTR [edi+edx]                      ;
        mov       DWORD PTR [44+esp], eax                       ;
        sub       ebx, ecx                                      ;
        imul      ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TOLLLINK+32], -52 ;
        imul      eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32], -152 ;
        cvtsi2ss  xmm3, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIMPERAGG] ;153.6
        add       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TOLLLINK] ;
        mov       DWORD PTR [148+esp], 1                        ;
        add       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;
        mov       DWORD PTR [172+esp], ecx                      ;153.6
        mov       DWORD PTR [8+esp], edx                        ;153.6
        mov       DWORD PTR [120+esp], esi                      ;153.6
        mov       DWORD PTR [124+esp], ebx                      ;153.6
        mov       DWORD PTR [128+esp], eax                      ;153.6
        mov       DWORD PTR [12+esp], edi                       ;153.6
        mov       esi, DWORD PTR [148+esp]                      ;153.6
        mov       ebp, DWORD PTR [28+esp]                       ;153.6
        mov       edx, DWORD PTR [176+esp]                      ;153.6
        mov       ecx, DWORD PTR [144+esp]                      ;153.6
                                ; LOE edx ecx ebp esi xmm1 xmm2 xmm3 xmm4
.B3.2:                          ; Preds .B3.68 .B3.1
        test      ecx, ecx                                      ;126.1
        jle       .B3.21        ; Prob 2%                       ;126.1
                                ; LOE edx ecx ebp esi xmm1 xmm2 xmm3 xmm4
.B3.3:                          ; Preds .B3.2
        mov       eax, 1                                        ;
        mov       ebx, 224                                      ;
        test      edx, edx                                      ;127.2
        jle       .B3.21        ; Prob 2%                       ;127.2
                                ; LOE eax edx ecx ebx ebp esi xmm1 xmm2 xmm3 xmm4
.B3.4:                          ; Preds .B3.3
        mov       DWORD PTR [148+esp], esi                      ;
        movss     DWORD PTR [48+esp], xmm3                      ;
        movss     DWORD PTR [52+esp], xmm4                      ;
        mov       esi, DWORD PTR [44+esp]                       ;
                                ; LOE eax ebx esi
.B3.5:                          ; Preds .B3.19 .B3.4
        movss     xmm1, DWORD PTR [200+ebx+esi]                 ;130.45
        mov       ebp, 1                                        ;
        movss     xmm0, DWORD PTR [_2il0floatpacket.17]         ;130.44
        divss     xmm0, xmm1                                    ;130.44
        mov       ecx, DWORD PTR [192+ebx+esi]                  ;129.4
        mov       edi, DWORD PTR [196+ebx+esi]                  ;129.4
        imul      edi, ecx                                      ;
        movss     xmm2, DWORD PTR [212+ebx+esi]                 ;133.54
        movss     DWORD PTR [76+esp], xmm0                      ;130.44
        movss     DWORD PTR [80+esp], xmm1                      ;130.44
        movss     DWORD PTR [84+esp], xmm2                      ;130.44
        mov       edx, DWORD PTR [164+ebx+esi]                  ;129.4
        sub       edx, edi                                      ;
        mov       edi, DWORD PTR [4+ebx+esi]                    ;129.23
        dec       edi                                           ;129.23
        mov       DWORD PTR [64+esp], edx                       ;
        mov       DWORD PTR [116+esp], edi                      ;129.23
        mov       edi, DWORD PTR [156+ebx+esi]                  ;128.4
        mov       edx, DWORD PTR [160+ebx+esi]                  ;128.4
        imul      edx, edi                                      ;
        mov       DWORD PTR [20+esp], edi                       ;128.4
        mov       edi, DWORD PTR [128+ebx+esi]                  ;128.4
        sub       edi, edx                                      ;
        mov       edx, DWORD PTR [ebx+esi]                      ;128.23
        dec       edx                                           ;128.23
        mov       DWORD PTR [60+esp], edi                       ;
        mov       DWORD PTR [108+esp], edx                      ;130.44
        mov       DWORD PTR [68+esp], ecx                       ;130.44
        mov       DWORD PTR [24+esp], eax                       ;130.44
                                ; LOE ebx ebp esi
.B3.6:                          ; Preds .B3.18 .B3.5
        mov       ecx, ebp                                      ;128.23
        pxor      xmm1, xmm1                                    ;
        pxor      xmm0, xmm0                                    ;
        cmp       DWORD PTR [108+esp], 0                        ;128.23
        jg        .B3.76        ; Prob 15%                      ;128.23
                                ; LOE ecx ebx ebp esi xmm0 xmm1
.B3.7:                          ; Preds .B3.6 .B3.78
        divss     xmm0, xmm1                                    ;128.23
        mov       edx, DWORD PTR [20+esp]                       ;128.4
        pxor      xmm1, xmm1                                    ;
        imul      edx, ebp                                      ;128.4
        mulss     xmm0, DWORD PTR [_2il0floatpacket.14]         ;128.4
        mov       eax, DWORD PTR [60+esp]                       ;128.4
        mov       DWORD PTR [72+esp], edx                       ;128.4
        mov       DWORD PTR [56+esp], ebp                       ;129.23
        movss     DWORD PTR [eax+edx], xmm0                     ;128.4
        pxor      xmm0, xmm0                                    ;
        cmp       DWORD PTR [116+esp], 0                        ;129.23
        jle       .B3.11        ; Prob 0%                       ;129.23
                                ; LOE ebx ebp esi xmm0 xmm1
.B3.8:                          ; Preds .B3.7
        mov       ecx, DWORD PTR [72+ebx+esi]                   ;129.23
        xor       edx, edx                                      ;
        mov       eax, DWORD PTR [76+ebx+esi]                   ;129.23
        imul      eax, ecx                                      ;
        movss     xmm3, DWORD PTR [48+esp]                      ;
        movss     xmm5, DWORD PTR [52+esp]                      ;
        movss     xmm4, DWORD PTR [_2il0floatpacket.15]         ;
        mov       edi, DWORD PTR [44+ebx+esi]                   ;129.23
        sub       edi, eax                                      ;
        xor       eax, eax                                      ;
        mov       DWORD PTR [36+esp], ebp                       ;
        mov       DWORD PTR [136+esp], edi                      ;
        mov       DWORD PTR [132+esp], ecx                      ;
        mov       DWORD PTR [40+esp], ebx                       ;
        mov       esi, DWORD PTR [56+esp]                       ;
        mov       ebp, DWORD PTR [176+esp]                      ;
        ALIGN     16
                                ; LOE eax edx ebp esi xmm0 xmm1 xmm3 xmm4 xmm5
.B3.9:                          ; Preds .B3.9 .B3.8
        add       eax, DWORD PTR [132+esp]                      ;129.23
        pxor      xmm2, xmm2                                    ;129.23
        mov       edi, DWORD PTR [136+esp]                      ;129.23
        inc       edx                                           ;129.23
        mov       ebx, DWORD PTR [eax+edi]                      ;129.23
        imul      ecx, ebx, 152                                 ;129.23
        mov       edi, DWORD PTR [128+esp]                      ;129.23
        addss     xmm0, DWORD PTR [20+ecx+edi]                  ;129.23
        mov       ecx, DWORD PTR [120+esp]                      ;129.23
        imul      ecx, esi                                      ;129.23
        mov       edi, DWORD PTR [124+esp]                      ;129.23
        lea       ebx, DWORD PTR [edi+ebx*4]                    ;129.23
        cvtsi2ss  xmm2, DWORD PTR [ebx+ecx]                     ;129.23
        divss     xmm2, xmm4                                    ;129.23
        addss     xmm1, xmm2                                    ;129.23
        divss     xmm2, xmm5                                    ;129.23
        divss     xmm2, xmm3                                    ;129.23
        cvttss2si ecx, xmm2                                     ;129.23
        add       esi, ecx                                      ;129.23
        cmp       esi, ebp                                      ;129.23
        jl        L4            ; Prob 50%                      ;129.23
        mov       esi, ebp                                      ;129.23
L4:                                                             ;
        cmp       edx, DWORD PTR [116+esp]                      ;129.23
        jb        .B3.9         ; Prob 82%                      ;129.23
                                ; LOE eax edx ebp esi xmm0 xmm1 xmm3 xmm4 xmm5
.B3.10:                         ; Preds .B3.9
        mov       ebp, DWORD PTR [36+esp]                       ;
        mov       ebx, DWORD PTR [40+esp]                       ;
        mov       esi, DWORD PTR [44+esp]                       ;
                                ; LOE ebx ebp esi xmm0 xmm1
.B3.11:                         ; Preds .B3.7 .B3.10
        divss     xmm0, xmm1                                    ;129.23
        mulss     xmm0, DWORD PTR [_2il0floatpacket.14]         ;129.4
        pxor      xmm3, xmm3                                    ;130.4
        movss     xmm2, DWORD PTR [_2il0floatpacket.17]         ;130.24
        divss     xmm2, xmm0                                    ;130.24
        mov       edx, DWORD PTR [68+esp]                       ;129.4
        imul      edx, ebp                                      ;129.4
        subss     xmm2, DWORD PTR [76+esp]                      ;130.40
        mov       eax, DWORD PTR [64+esp]                       ;129.4
        maxss     xmm3, xmm2                                    ;130.4
        mov       ecx, DWORD PTR [60+esp]                       ;133.52
        mov       edi, DWORD PTR [72+esp]                       ;133.52
        movss     DWORD PTR [eax+edx], xmm0                     ;129.4
        movss     xmm1, DWORD PTR [84+esp]                      ;133.52
        movss     xmm0, DWORD PTR [ecx+edi]                     ;133.52
        divss     xmm0, DWORD PTR [80+esp]                      ;133.52
        movss     DWORD PTR [88+esp], xmm3                      ;130.4
        call      ___libm_sse2_powf                             ;133.52
                                ; LOE ebx ebp esi xmm0
.B3.81:                         ; Preds .B3.11
        cmp       DWORD PTR [148+esp], 1                        ;134.4
        jne       .B3.15        ; Prob 67%                      ;134.4
                                ; LOE ebx ebp esi xmm0
.B3.12:                         ; Preds .B3.81
        movss     xmm1, DWORD PTR [32+esp]                      ;136.58
        mov       edi, ebp                                      ;136.6
        divss     xmm1, xmm0                                    ;136.58
        divss     xmm1, DWORD PTR [84+esp]                      ;136.64
        movss     xmm3, DWORD PTR [216+ebx+esi]                 ;136.85
        mov       edx, DWORD PTR [112+ebx+esi]                  ;136.6
        mov       eax, DWORD PTR [120+ebx+esi]                  ;136.6
        movss     xmm2, DWORD PTR [204+ebx+esi]                 ;136.32
        imul      edi, eax                                      ;136.6
        addss     xmm3, DWORD PTR [88+esp]                      ;136.84
        imul      eax, DWORD PTR [124+ebx+esi]                  ;136.6
        imul      edx, DWORD PTR [108+ebx+esi]                  ;136.6
        movaps    xmm0, xmm2                                    ;136.6
        mulss     xmm3, xmm1                                    ;136.77
        mov       ecx, DWORD PTR [80+ebx+esi]                   ;136.6
        maxss     xmm0, xmm3                                    ;136.6
        sub       edx, DWORD PTR [108+ebx+esi]                  ;136.6
        sub       ecx, eax                                      ;136.6
        sub       ecx, edx                                      ;136.6
        comiss    xmm0, xmm2                                    ;137.29
        jbe       .B3.14        ; Prob 50%                      ;137.29
                                ; LOE ecx ebx ebp esi edi xmm0 xmm3
.B3.13:                         ; Preds .B3.12
        movss     xmm0, DWORD PTR [208+ebx+esi]                 ;138.34
        minss     xmm0, xmm3                                    ;138.8
        movss     DWORD PTR [ecx+edi], xmm0                     ;138.8
        jmp       .B3.18        ; Prob 100%                     ;138.8
                                ; LOE ebx ebp esi
.B3.14:                         ; Preds .B3.12
        movss     DWORD PTR [ecx+edi], xmm0                     ;138.8
        jmp       .B3.18        ; Prob 100%                     ;138.8
                                ; LOE ebx ebp esi
.B3.15:                         ; Preds .B3.81
        cmp       DWORD PTR [148+esp], 2                        ;134.4
        jne       .B3.18        ; Prob 50%                      ;134.4
                                ; LOE ebx ebp esi xmm0
.B3.16:                         ; Preds .B3.15
        mov       ecx, DWORD PTR [108+ebx+esi]                  ;142.8
        mov       edx, DWORD PTR [120+ebx+esi]                  ;142.8
        cmp       DWORD PTR [220+ebx+esi], 3                    ;141.23
        je        .B3.70        ; Prob 16%                      ;141.23
                                ; LOE edx ecx ebx ebp esi xmm0
.B3.17:                         ; Preds .B3.16
        mov       eax, ebp                                      ;147.8
        sub       eax, DWORD PTR [124+ebx+esi]                  ;147.8
        imul      eax, edx                                      ;147.8
        mov       edx, DWORD PTR [112+ebx+esi]                  ;147.8
        neg       edx                                           ;147.8
        add       edx, 2                                        ;147.8
        imul      edx, ecx                                      ;147.8
        add       edx, DWORD PTR [80+ebx+esi]                   ;147.8
        mov       DWORD PTR [edx+eax], 1120403456               ;147.8
                                ; LOE ebx ebp esi
.B3.18:                         ; Preds .B3.15 .B3.13 .B3.14 .B3.71 .B3.72
                                ;       .B3.17
        inc       ebp                                           ;150.2
        cmp       ebp, DWORD PTR [176+esp]                      ;150.2
        jle       .B3.6         ; Prob 82%                      ;150.2
                                ; LOE ebx ebp esi
.B3.19:                         ; Preds .B3.18
        mov       eax, DWORD PTR [24+esp]                       ;
        add       ebx, 224                                      ;151.1
        inc       eax                                           ;151.1
        cmp       eax, DWORD PTR [144+esp]                      ;151.1
        jle       .B3.5         ; Prob 82%                      ;151.1
                                ; LOE eax ebx esi
.B3.20:                         ; Preds .B3.19
        movss     xmm1, DWORD PTR [_2il0floatpacket.16]         ;
        movss     xmm3, DWORD PTR [48+esp]                      ;
        movss     xmm4, DWORD PTR [52+esp]                      ;
        mov       esi, DWORD PTR [148+esp]                      ;
        mov       ebp, DWORD PTR [28+esp]                       ;
        mov       edx, DWORD PTR [176+esp]                      ;
        mov       ecx, DWORD PTR [144+esp]                      ;
        movss     xmm2, DWORD PTR [_2il0floatpacket.15]         ;
                                ; LOE edx ecx ebp esi xmm1 xmm2 xmm3 xmm4
.B3.21:                         ; Preds .B3.3 .B3.20 .B3.2
        test      ebp, ebp                                      ;153.6
        jle       .B3.68        ; Prob 2%                       ;153.6
                                ; LOE edx ecx ebp esi xmm1 xmm2 xmm3 xmm4
.B3.22:                         ; Preds .B3.21
        mov       DWORD PTR [148+esp], esi                      ;
        mov       ebx, 1                                        ;
        mov       eax, 52                                       ;
                                ; LOE eax ebx xmm1 xmm2 xmm3 xmm4
.B3.23:                         ; Preds .B3.66 .B3.22
        mov       ecx, DWORD PTR [172+esp]                      ;153.6
        mov       ebp, DWORD PTR [12+eax+ecx]                   ;153.6
        test      ebp, ebp                                      ;153.6
        mov       edx, DWORD PTR [8+eax+ecx]                    ;153.6
        jle       .B3.66        ; Prob 2%                       ;153.6
                                ; LOE eax edx ebx ebp xmm1 xmm2 xmm3 xmm4
.B3.24:                         ; Preds .B3.23
        imul      ecx, edx, 152                                 ;153.6
        mov       esi, 1                                        ;
        mov       DWORD PTR [112+esp], ecx                      ;153.6
        cmp       DWORD PTR [144+esp], 0                        ;153.6
        jle       .B3.66        ; Prob 2%                       ;153.6
                                ; LOE eax edx ebx ebp esi xmm1 xmm2 xmm3 xmm4
.B3.25:                         ; Preds .B3.24
        mov       edi, DWORD PTR [8+esp]                        ;
        mov       ecx, DWORD PTR [12+esp]                       ;
        mov       DWORD PTR [212+esp], edx                      ;
        mov       DWORD PTR [192+esp], eax                      ;
        mov       DWORD PTR [16+esp], ebx                       ;
        lea       ecx, DWORD PTR [224+ecx+edi]                  ;
                                ; LOE ecx ebp esi xmm1 xmm2 xmm3 xmm4
.B3.26:                         ; Preds .B3.64 .B3.25
        mov       DWORD PTR [96+esp], ecx                       ;
        lea       ebx, DWORD PTR [esi+esi*2]                    ;
        mov       DWORD PTR [188+esp], ebx                      ;
        mov       edx, 1                                        ;
        mov       DWORD PTR [100+esp], esi                      ;
        mov       eax, ecx                                      ;
        mov       DWORD PTR [104+esp], ebp                      ;
                                ; LOE eax edx xmm1 xmm2 xmm3 xmm4
.B3.27:                         ; Preds .B3.63 .B3.26
        mov       ecx, DWORD PTR [eax]                          ;153.6
        test      ecx, ecx                                      ;153.6
        jle       .B3.63        ; Prob 3%                       ;153.6
                                ; LOE eax edx ecx xmm1 xmm2 xmm3 xmm4
.B3.28:                         ; Preds .B3.27
        mov       esi, DWORD PTR [36+eax]                       ;153.6
        xor       ebp, ebp                                      ;
        mov       ebx, DWORD PTR [40+eax]                       ;153.6
        imul      ebx, esi                                      ;
        mov       edi, DWORD PTR [8+eax]                        ;153.6
        sub       edi, ebx                                      ;
        xor       ebx, ebx                                      ;
        mov       DWORD PTR [200+esp], edi                      ;
        mov       DWORD PTR [204+esp], esi                      ;
        mov       DWORD PTR [208+esp], ecx                      ;
        mov       DWORD PTR [140+esp], edx                      ;
                                ; LOE eax ebx ebp xmm1 xmm2 xmm3 xmm4
.B3.29:                         ; Preds .B3.61 .B3.28
        add       ebx, DWORD PTR [204+esp]                      ;153.6
        inc       ebp                                           ;153.6
        mov       ecx, DWORD PTR [200+esp]                      ;153.6
        mov       edx, DWORD PTR [212+esp]                      ;153.6
        cmp       edx, DWORD PTR [ebx+ecx]                      ;153.6
        jne       .B3.61        ; Prob 50%                      ;153.6
                                ; LOE eax ebx ebp xmm1 xmm2 xmm3 xmm4
.B3.30:                         ; Preds .B3.29
        mov       edx, DWORD PTR [192+esp]                      ;153.6
        mov       edi, DWORD PTR [172+esp]                      ;153.6
        mov       ecx, DWORD PTR [176+esp]                      ;153.6
        imul      esi, DWORD PTR [48+edx+edi], -24              ;153.6
        add       esi, DWORD PTR [16+edx+edi]                   ;153.6
        mov       edi, DWORD PTR [188+esp]                      ;153.6
        mov       DWORD PTR [184+esp], esi                      ;153.6
        movss     xmm5, DWORD PTR [4+esi+edi*8]                 ;153.6
        movss     xmm0, DWORD PTR [esi+edi*8]                   ;153.6
        subss     xmm5, xmm1                                    ;153.6
        divss     xmm0, xmm4                                    ;153.6
        divss     xmm5, xmm4                                    ;153.6
        divss     xmm0, xmm3                                    ;153.6
        divss     xmm5, xmm3                                    ;153.6
        cvttss2si edx, xmm0                                     ;153.6
        cvttss2si esi, xmm5                                     ;153.6
        inc       edx                                           ;153.6
        inc       esi                                           ;153.6
        cmp       edx, ecx                                      ;153.6
        cmovge    edx, ecx                                      ;153.6
        cmp       esi, ecx                                      ;153.6
        cmovge    esi, ecx                                      ;153.6
        sub       esi, edx                                      ;153.6
        lea       ecx, DWORD PTR [1+esi]                        ;153.6
        mov       DWORD PTR [180+esp], ecx                      ;153.6
        js        .B3.51        ; Prob 50%                      ;153.6
                                ; LOE eax edx ecx ebx ebp esi cl ch xmm1 xmm2 xmm3 xmm4
.B3.31:                         ; Preds .B3.30
        mov       edi, ecx                                      ;153.6
        sar       edi, 2                                        ;153.6
        shr       edi, 29                                       ;153.6
        pxor      xmm0, xmm0                                    ;
        add       edi, ecx                                      ;153.6
        sar       edi, 3                                        ;153.6
        mov       DWORD PTR [196+esp], edi                      ;153.6
        test      edi, edi                                      ;153.6
        jbe       .B3.73        ; Prob 11%                      ;153.6
                                ; LOE eax edx ebx ebp esi xmm0 xmm1 xmm2 xmm3 xmm4
.B3.32:                         ; Preds .B3.31
        mov       ecx, DWORD PTR [148+esp]                      ;
        mov       DWORD PTR [152+esp], esi                      ;
        pxor      xmm6, xmm6                                    ;
        mov       edi, DWORD PTR [120+eax]                      ;153.6
        pxor      xmm5, xmm5                                    ;
        mov       esi, DWORD PTR [124+eax]                      ;153.6
        sub       ecx, DWORD PTR [112+eax]                      ;
        imul      esi, edi                                      ;
        imul      ecx, DWORD PTR [108+eax]                      ;
        mov       DWORD PTR [esp], edi                          ;153.6
        mov       edi, DWORD PTR [80+eax]                       ;153.6
        sub       edi, esi                                      ;
        add       ecx, edi                                      ;
        mov       DWORD PTR [168+esp], 0                        ;
        mov       DWORD PTR [156+esp], ebx                      ;
        mov       DWORD PTR [164+esp], eax                      ;
        mov       DWORD PTR [160+esp], ebp                      ;
        mov       esi, ecx                                      ;
        mov       ecx, DWORD PTR [esp]                          ;
        mov       ebx, DWORD PTR [168+esp]                      ;
        mov       eax, DWORD PTR [196+esp]                      ;
        ALIGN     16
                                ; LOE eax edx ecx ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5 xmm6
.B3.33:                         ; Preds .B3.33 .B3.32
        mov       edi, ecx                                      ;153.6
        lea       ebp, DWORD PTR [edx+ebx*8]                    ;153.6
        imul      edi, ebp                                      ;153.6
        inc       ebx                                           ;153.6
        addss     xmm0, DWORD PTR [esi+edi]                     ;153.6
        lea       edi, DWORD PTR [1+ebp]                        ;153.6
        imul      edi, ecx                                      ;153.6
        addss     xmm6, DWORD PTR [esi+edi]                     ;153.6
        lea       edi, DWORD PTR [2+ebp]                        ;153.6
        imul      edi, ecx                                      ;153.6
        addss     xmm5, DWORD PTR [esi+edi]                     ;153.6
        lea       edi, DWORD PTR [3+ebp]                        ;153.6
        imul      edi, ecx                                      ;153.6
        addss     xmm6, DWORD PTR [esi+edi]                     ;153.6
        lea       edi, DWORD PTR [4+ebp]                        ;153.6
        imul      edi, ecx                                      ;153.6
        addss     xmm5, DWORD PTR [esi+edi]                     ;153.6
        lea       edi, DWORD PTR [5+ebp]                        ;153.6
        imul      edi, ecx                                      ;153.6
        addss     xmm6, DWORD PTR [esi+edi]                     ;153.6
        lea       edi, DWORD PTR [6+ebp]                        ;153.6
        add       ebp, 7                                        ;153.6
        imul      edi, ecx                                      ;153.6
        imul      ebp, ecx                                      ;153.6
        addss     xmm5, DWORD PTR [esi+edi]                     ;153.6
        addss     xmm6, DWORD PTR [esi+ebp]                     ;153.6
        cmp       ebx, eax                                      ;153.6
        jb        .B3.33        ; Prob 99%                      ;153.6
                                ; LOE eax edx ecx ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5 xmm6
.B3.34:                         ; Preds .B3.33
        mov       DWORD PTR [168+esp], ebx                      ;
        addss     xmm0, xmm6                                    ;153.6
        mov       ecx, ebx                                      ;153.6
        addss     xmm0, xmm5                                    ;153.6
        mov       esi, DWORD PTR [152+esp]                      ;
        lea       ecx, DWORD PTR [1+ecx*8]                      ;153.6
        mov       ebx, DWORD PTR [156+esp]                      ;
        mov       ebp, DWORD PTR [160+esp]                      ;
        mov       eax, DWORD PTR [164+esp]                      ;
                                ; LOE eax edx ecx ebx ebp esi xmm0 xmm1 xmm2 xmm3 xmm4
.B3.35:                         ; Preds .B3.34 .B3.73
        cmp       ecx, DWORD PTR [180+esp]                      ;153.6
        ja        .B3.52        ; Prob 50%                      ;153.6
                                ; LOE eax edx ecx ebx ebp esi xmm0 xmm1 xmm2 xmm3 xmm4
.B3.36:                         ; Preds .B3.35
        sub       esi, ecx                                      ;153.6
        jmp       DWORD PTR [..1..TPKT.15_0+4+esi*4]            ;153.6
                                ; LOE eax edx ecx ebx ebp xmm0 xmm1 xmm2 xmm3 xmm4
..1.15_0.TAG.6::
.B3.38:                         ; Preds .B3.36
        mov       esi, DWORD PTR [124+eax]                      ;153.6
        lea       edi, DWORD PTR [5+edx+ecx]                    ;153.6
        neg       esi                                           ;153.6
        add       esi, edi                                      ;153.6
        mov       edi, DWORD PTR [148+esp]                      ;153.6
        sub       edi, DWORD PTR [112+eax]                      ;153.6
        imul      edi, DWORD PTR [108+eax]                      ;153.6
        imul      esi, DWORD PTR [120+eax]                      ;153.6
        add       edi, DWORD PTR [80+eax]                       ;153.6
        addss     xmm0, DWORD PTR [edi+esi]                     ;153.6
                                ; LOE eax edx ecx ebx ebp xmm0 xmm1 xmm2 xmm3 xmm4
..1.15_0.TAG.5::
.B3.40:                         ; Preds .B3.36 .B3.38
        mov       esi, DWORD PTR [124+eax]                      ;153.6
        lea       edi, DWORD PTR [4+edx+ecx]                    ;153.6
        neg       esi                                           ;153.6
        add       esi, edi                                      ;153.6
        mov       edi, DWORD PTR [148+esp]                      ;153.6
        sub       edi, DWORD PTR [112+eax]                      ;153.6
        imul      edi, DWORD PTR [108+eax]                      ;153.6
        imul      esi, DWORD PTR [120+eax]                      ;153.6
        add       edi, DWORD PTR [80+eax]                       ;153.6
        addss     xmm0, DWORD PTR [edi+esi]                     ;153.6
                                ; LOE eax edx ecx ebx ebp xmm0 xmm1 xmm2 xmm3 xmm4
..1.15_0.TAG.4::
.B3.42:                         ; Preds .B3.36 .B3.40
        mov       esi, DWORD PTR [124+eax]                      ;153.6
        lea       edi, DWORD PTR [3+edx+ecx]                    ;153.6
        neg       esi                                           ;153.6
        add       esi, edi                                      ;153.6
        mov       edi, DWORD PTR [148+esp]                      ;153.6
        sub       edi, DWORD PTR [112+eax]                      ;153.6
        imul      edi, DWORD PTR [108+eax]                      ;153.6
        imul      esi, DWORD PTR [120+eax]                      ;153.6
        add       edi, DWORD PTR [80+eax]                       ;153.6
        addss     xmm0, DWORD PTR [edi+esi]                     ;153.6
                                ; LOE eax edx ecx ebx ebp xmm0 xmm1 xmm2 xmm3 xmm4
..1.15_0.TAG.3::
.B3.44:                         ; Preds .B3.36 .B3.42
        mov       esi, DWORD PTR [124+eax]                      ;153.6
        lea       edi, DWORD PTR [2+edx+ecx]                    ;153.6
        neg       esi                                           ;153.6
        add       esi, edi                                      ;153.6
        mov       edi, DWORD PTR [148+esp]                      ;153.6
        sub       edi, DWORD PTR [112+eax]                      ;153.6
        imul      edi, DWORD PTR [108+eax]                      ;153.6
        imul      esi, DWORD PTR [120+eax]                      ;153.6
        add       edi, DWORD PTR [80+eax]                       ;153.6
        addss     xmm0, DWORD PTR [edi+esi]                     ;153.6
                                ; LOE eax edx ecx ebx ebp xmm0 xmm1 xmm2 xmm3 xmm4
..1.15_0.TAG.2::
.B3.46:                         ; Preds .B3.36 .B3.44
        mov       esi, DWORD PTR [124+eax]                      ;153.6
        lea       edi, DWORD PTR [1+edx+ecx]                    ;153.6
        neg       esi                                           ;153.6
        add       esi, edi                                      ;153.6
        mov       edi, DWORD PTR [148+esp]                      ;153.6
        sub       edi, DWORD PTR [112+eax]                      ;153.6
        imul      edi, DWORD PTR [108+eax]                      ;153.6
        imul      esi, DWORD PTR [120+eax]                      ;153.6
        add       edi, DWORD PTR [80+eax]                       ;153.6
        addss     xmm0, DWORD PTR [edi+esi]                     ;153.6
                                ; LOE eax edx ecx ebx ebp xmm0 xmm1 xmm2 xmm3 xmm4
..1.15_0.TAG.1::
.B3.48:                         ; Preds .B3.36 .B3.46
        mov       esi, DWORD PTR [124+eax]                      ;153.6
        lea       edi, DWORD PTR [edx+ecx]                      ;153.6
        neg       esi                                           ;153.6
        add       esi, edi                                      ;153.6
        mov       edi, DWORD PTR [148+esp]                      ;153.6
        sub       edi, DWORD PTR [112+eax]                      ;153.6
        imul      edi, DWORD PTR [108+eax]                      ;153.6
        imul      esi, DWORD PTR [120+eax]                      ;153.6
        add       edi, DWORD PTR [80+eax]                       ;153.6
        addss     xmm0, DWORD PTR [edi+esi]                     ;153.6
                                ; LOE eax edx ecx ebx ebp xmm0 xmm1 xmm2 xmm3 xmm4
..1.15_0.TAG.0::
.B3.50:                         ; Preds .B3.36 .B3.48
        mov       esi, DWORD PTR [124+eax]                      ;153.6
        lea       edx, DWORD PTR [-1+edx+ecx]                   ;153.6
        mov       ecx, DWORD PTR [148+esp]                      ;153.6
        neg       esi                                           ;153.6
        sub       ecx, DWORD PTR [112+eax]                      ;153.6
        add       esi, edx                                      ;153.6
        imul      ecx, DWORD PTR [108+eax]                      ;153.6
        imul      esi, DWORD PTR [120+eax]                      ;153.6
        add       ecx, DWORD PTR [80+eax]                       ;153.6
        addss     xmm0, DWORD PTR [ecx+esi]                     ;153.6
        jmp       .B3.52        ; Prob 100%                     ;153.6
                                ; LOE eax ebx ebp xmm0 xmm1 xmm2 xmm3 xmm4
.B3.51:                         ; Preds .B3.30
        pxor      xmm0, xmm0                                    ;
                                ; LOE eax ebx ebp xmm0 xmm1 xmm2 xmm3 xmm4
.B3.52:                         ; Preds .B3.50 .B3.35 .B3.51
        cvtsi2ss  xmm5, DWORD PTR [180+esp]                     ;153.6
        mov       ecx, DWORD PTR [188+esp]                      ;153.6
        mov       edx, DWORD PTR [184+esp]                      ;153.6
        divss     xmm0, xmm5                                    ;153.6
        cmp       DWORD PTR [20+edx+ecx*8], 0                   ;153.6
        jne       .B3.57        ; Prob 50%                      ;153.6
                                ; LOE eax edx ecx ebx ebp dl cl dh ch xmm0 xmm1 xmm2 xmm3 xmm4
.B3.53:                         ; Preds .B3.52
        cmp       DWORD PTR [148+esp], 1                        ;153.6
        je        .B3.74        ; Prob 16%                      ;153.6
                                ; LOE eax edx ecx ebx ebp dl cl dh ch xmm0 xmm1 xmm2 xmm3 xmm4
.B3.54:                         ; Preds .B3.53
        comiss    xmm2, DWORD PTR [12+edx+ecx*8]                ;153.6
        jbe       .B3.61        ; Prob 50%                      ;153.6
                                ; LOE eax edx ecx ebx ebp dl cl dh ch xmm0 xmm1 xmm2 xmm3 xmm4
.B3.55:                         ; Preds .B3.54
        movss     xmm5, DWORD PTR [204+eax]                     ;153.6
        maxss     xmm5, xmm0                                    ;153.6
        comiss    xmm2, xmm5                                    ;153.6
        movss     DWORD PTR [12+edx+ecx*8], xmm5                ;153.6
        jbe       .B3.61        ; Prob 50%                      ;153.6
                                ; LOE eax edx ecx ebx ebp dl cl dh ch xmm0 xmm1 xmm2 xmm3 xmm4
.B3.56:                         ; Preds .B3.55
        movss     xmm5, DWORD PTR [208+eax]                     ;153.6
        minss     xmm5, xmm0                                    ;153.6
        movss     DWORD PTR [12+edx+ecx*8], xmm5                ;153.6
        jmp       .B3.61        ; Prob 100%                     ;153.6
                                ; LOE eax ebx ebp xmm1 xmm2 xmm3 xmm4
.B3.57:                         ; Preds .B3.52
        cmp       DWORD PTR [148+esp], 1                        ;153.6
        je        .B3.75        ; Prob 16%                      ;153.6
                                ; LOE eax edx ecx ebx ebp dl cl dh ch xmm0 xmm1 xmm2 xmm3 xmm4
.B3.58:                         ; Preds .B3.57
        comiss    xmm2, DWORD PTR [12+edx+ecx*8]                ;153.6
        jbe       .B3.61        ; Prob 50%                      ;153.6
                                ; LOE eax ebx ebp xmm0 xmm1 xmm2 xmm3 xmm4
.B3.59:                         ; Preds .B3.58
        movss     xmm5, DWORD PTR [204+eax]                     ;153.6
        mov       ecx, DWORD PTR [128+esp]                      ;153.6
        maxss     xmm5, xmm0                                    ;153.6
        mov       edx, DWORD PTR [112+esp]                      ;153.6
        mov       edi, DWORD PTR [188+esp]                      ;153.6
        mov       esi, DWORD PTR [184+esp]                      ;153.6
        mulss     xmm5, DWORD PTR [20+edx+ecx]                  ;153.6
        movss     DWORD PTR [12+esi+edi*8], xmm5                ;153.6
        comiss    xmm2, xmm5                                    ;153.6
        jbe       .B3.61        ; Prob 50%                      ;153.6
                                ; LOE eax edx ecx ebx ebp esi edi dl cl dh ch xmm0 xmm1 xmm2 xmm3 xmm4
.B3.60:                         ; Preds .B3.59
        movss     xmm5, DWORD PTR [208+eax]                     ;153.6
        minss     xmm5, xmm0                                    ;153.6
        mulss     xmm5, DWORD PTR [20+edx+ecx]                  ;153.6
        movss     DWORD PTR [12+esi+edi*8], xmm5                ;153.6
                                ; LOE eax ebx ebp xmm1 xmm2 xmm3 xmm4
.B3.61:                         ; Preds .B3.54 .B3.58 .B3.29 .B3.59 .B3.55
                                ;       .B3.74 .B3.56 .B3.75 .B3.60
        cmp       ebp, DWORD PTR [208+esp]                      ;153.6
        jb        .B3.29        ; Prob 82%                      ;153.6
                                ; LOE eax ebx ebp xmm1 xmm2 xmm3 xmm4
.B3.62:                         ; Preds .B3.61
        mov       edx, DWORD PTR [140+esp]                      ;
                                ; LOE eax edx xmm1 xmm2 xmm3 xmm4
.B3.63:                         ; Preds .B3.62 .B3.27
        inc       edx                                           ;153.6
        add       eax, 224                                      ;153.6
        cmp       edx, DWORD PTR [144+esp]                      ;153.6
        jle       .B3.27        ; Prob 82%                      ;153.6
                                ; LOE eax edx xmm1 xmm2 xmm3 xmm4
.B3.64:                         ; Preds .B3.63
        mov       esi, DWORD PTR [100+esp]                      ;
        inc       esi                                           ;153.6
        mov       ebp, DWORD PTR [104+esp]                      ;
        cmp       esi, ebp                                      ;153.6
        mov       ecx, DWORD PTR [96+esp]                       ;
        jle       .B3.26        ; Prob 82%                      ;153.6
                                ; LOE ecx ebp esi xmm1 xmm2 xmm3 xmm4
.B3.65:                         ; Preds .B3.64
        mov       eax, DWORD PTR [192+esp]                      ;
        mov       ebx, DWORD PTR [16+esp]                       ;
                                ; LOE eax ebx xmm1 xmm2 xmm3 xmm4
.B3.66:                         ; Preds .B3.24 .B3.65 .B3.23
        inc       ebx                                           ;153.6
        add       eax, 52                                       ;153.6
        cmp       ebx, DWORD PTR [28+esp]                       ;153.6
        jle       .B3.23        ; Prob 82%                      ;153.6
                                ; LOE eax ebx xmm1 xmm2 xmm3 xmm4
.B3.67:                         ; Preds .B3.66
        mov       esi, DWORD PTR [148+esp]                      ;
        mov       ebp, DWORD PTR [28+esp]                       ;
        mov       edx, DWORD PTR [176+esp]                      ;
        mov       ecx, DWORD PTR [144+esp]                      ;
                                ; LOE edx ecx ebp esi xmm1 xmm2 xmm3 xmm4
.B3.68:                         ; Preds .B3.67 .B3.21
        inc       esi                                           ;155.1
        cmp       esi, 2                                        ;155.1
        jle       .B3.2         ; Prob 50%                      ;155.1
                                ; LOE edx ecx ebp esi xmm1 xmm2 xmm3 xmm4
.B3.69:                         ; Preds .B3.68
        add       esp, 216                                      ;157.1
        pop       ebp                                           ;157.1
        pop       ebx                                           ;157.1
        pop       edi                                           ;157.1
        pop       esi                                           ;157.1
        ret                                                     ;157.1
                                ; LOE
.B3.70:                         ; Preds .B3.16                  ; Infreq
        movss     xmm1, DWORD PTR [4+esp]                       ;142.60
        mov       edi, ebp                                      ;142.8
        divss     xmm1, xmm0                                    ;142.60
        divss     xmm1, DWORD PTR [84+esp]                      ;142.66
        movss     xmm2, DWORD PTR [216+ebx+esi]                 ;142.87
        mov       eax, DWORD PTR [112+ebx+esi]                  ;142.8
        neg       eax                                           ;142.8
        addss     xmm2, DWORD PTR [88+esp]                      ;142.86
        imul      edi, edx                                      ;142.8
        imul      edx, DWORD PTR [124+ebx+esi]                  ;142.8
        movss     xmm3, DWORD PTR [204+ebx+esi]                 ;142.34
        movaps    xmm0, xmm3                                    ;142.8
        mulss     xmm1, xmm2                                    ;142.79
        add       eax, 2                                        ;142.8
        imul      eax, ecx                                      ;142.8
        maxss     xmm0, xmm1                                    ;142.8
        mov       ecx, DWORD PTR [80+ebx+esi]                   ;142.8
        sub       ecx, edx                                      ;142.8
        add       eax, ecx                                      ;142.8
        comiss    xmm0, xmm3                                    ;143.31
        jbe       .B3.72        ; Prob 50%                      ;143.31
                                ; LOE eax ebx ebp esi edi xmm0 xmm1
.B3.71:                         ; Preds .B3.70                  ; Infreq
        movss     xmm0, DWORD PTR [208+ebx+esi]                 ;144.36
        mov       edx, edi                                      ;144.10
        minss     xmm0, xmm1                                    ;144.10
        movss     DWORD PTR [eax+edx], xmm0                     ;144.10
        jmp       .B3.18        ; Prob 100%                     ;144.10
                                ; LOE ebx ebp esi
.B3.72:                         ; Preds .B3.70                  ; Infreq
        mov       edx, edi                                      ;144.10
        movss     DWORD PTR [eax+edx], xmm0                     ;144.10
        jmp       .B3.18        ; Prob 100%                     ;144.10
                                ; LOE ebx ebp esi
.B3.73:                         ; Preds .B3.31                  ; Infreq
        mov       ecx, 1                                        ;
        jmp       .B3.35        ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx ebp esi xmm0 xmm1 xmm2 xmm3 xmm4
.B3.74:                         ; Preds .B3.53                  ; Infreq
        movss     xmm5, DWORD PTR [208+eax]                     ;153.6
        minss     xmm5, xmm0                                    ;153.6
        movss     DWORD PTR [8+edx+ecx*8], xmm5                 ;153.6
        jmp       .B3.61        ; Prob 100%                     ;153.6
                                ; LOE eax ebx ebp xmm1 xmm2 xmm3 xmm4
.B3.75:                         ; Preds .B3.57                  ; Infreq
        movss     xmm5, DWORD PTR [208+eax]                     ;153.6
        mov       ecx, DWORD PTR [128+esp]                      ;153.6
        minss     xmm5, xmm0                                    ;153.6
        mov       edx, DWORD PTR [112+esp]                      ;153.6
        mov       edi, DWORD PTR [188+esp]                      ;153.6
        mov       esi, DWORD PTR [184+esp]                      ;153.6
        mulss     xmm5, DWORD PTR [20+edx+ecx]                  ;153.6
        movss     DWORD PTR [8+esi+edi*8], xmm5                 ;153.6
        jmp       .B3.61        ; Prob 100%                     ;153.6
                                ; LOE eax ebx ebp xmm1 xmm2 xmm3 xmm4
.B3.76:                         ; Preds .B3.6                   ; Infreq
        mov       edi, DWORD PTR [36+ebx+esi]                   ;128.23
        xor       edx, edx                                      ;
        mov       eax, DWORD PTR [40+ebx+esi]                   ;128.23
        imul      eax, edi                                      ;
        movss     xmm3, DWORD PTR [48+esp]                      ;
        movss     xmm5, DWORD PTR [52+esp]                      ;
        movss     xmm4, DWORD PTR [_2il0floatpacket.15]         ;
        mov       DWORD PTR [92+esp], edi                       ;128.23
        mov       edi, DWORD PTR [8+ebx+esi]                    ;128.23
        sub       edi, eax                                      ;
        xor       eax, eax                                      ;
        mov       DWORD PTR [36+esp], ebp                       ;
        mov       DWORD PTR [40+esp], ebx                       ;
        mov       esi, edi                                      ;
        ALIGN     16
                                ; LOE eax edx ecx esi xmm0 xmm1 xmm3 xmm4 xmm5
.B3.77:                         ; Preds .B3.77 .B3.76           ; Infreq
        add       eax, DWORD PTR [92+esp]                       ;128.23
        pxor      xmm2, xmm2                                    ;128.23
        mov       ebx, DWORD PTR [128+esp]                      ;128.23
        inc       edx                                           ;128.23
        mov       ebp, DWORD PTR [eax+esi]                      ;128.23
        imul      edi, ebp, 152                                 ;128.23
        addss     xmm0, DWORD PTR [20+edi+ebx]                  ;128.23
        mov       ebx, DWORD PTR [120+esp]                      ;128.23
        imul      ebx, ecx                                      ;128.23
        mov       edi, DWORD PTR [124+esp]                      ;128.23
        lea       ebp, DWORD PTR [edi+ebp*4]                    ;128.23
        cvtsi2ss  xmm2, DWORD PTR [ebp+ebx]                     ;128.23
        divss     xmm2, xmm4                                    ;128.23
        mov       ebp, DWORD PTR [176+esp]                      ;128.23
        addss     xmm1, xmm2                                    ;128.23
        divss     xmm2, xmm5                                    ;128.23
        divss     xmm2, xmm3                                    ;128.23
        cvttss2si ebx, xmm2                                     ;128.23
        add       ecx, ebx                                      ;128.23
        cmp       ecx, ebp                                      ;128.23
        jl        L5            ; Prob 50%                      ;128.23
        mov       ecx, ebp                                      ;128.23
L5:                                                             ;
        cmp       edx, DWORD PTR [108+esp]                      ;128.23
        jb        .B3.77        ; Prob 82%                      ;128.23
                                ; LOE eax edx ecx esi xmm0 xmm1 xmm3 xmm4 xmm5
.B3.78:                         ; Preds .B3.77                  ; Infreq
        mov       ebp, DWORD PTR [36+esp]                       ;
        mov       ebx, DWORD PTR [40+esp]                       ;
        mov       esi, DWORD PTR [44+esp]                       ;
        jmp       .B3.7         ; Prob 100%                     ;
        ALIGN     16
                                ; LOE ebx ebp esi xmm0 xmm1
; mark_end;
_DYNUST_CONGESTIONPRICING_MODULE_mp_PRICECAL ENDP
_TEXT	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DB 3 DUP ( 0H)	; pad
..1..TPKT.15_0	DD	OFFSET FLAT: ..1.15_0.TAG.0
	DD	OFFSET FLAT: ..1.15_0.TAG.1
	DD	OFFSET FLAT: ..1.15_0.TAG.2
	DD	OFFSET FLAT: ..1.15_0.TAG.3
	DD	OFFSET FLAT: ..1.15_0.TAG.4
	DD	OFFSET FLAT: ..1.15_0.TAG.5
	DD	OFFSET FLAT: ..1.15_0.TAG.6
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_CONGESTIONPRICING_MODULE_mp_PRICECAL
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_CONGESTIONPRICING_MODULE_mp_CALAVGSPEED
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_CONGESTIONPRICING_MODULE_mp_CALAVGSPEED
_DYNUST_CONGESTIONPRICING_MODULE_mp_CALAVGSPEED	PROC NEAR 
; parameter 1: 56 + esp
; parameter 2: 60 + esp
; parameter 3: 64 + esp
.B4.1:                          ; Preds .B4.0
        push      esi                                           ;160.15
        push      edi                                           ;160.15
        push      ebx                                           ;160.15
        push      ebp                                           ;160.15
        sub       esp, 36                                       ;160.15
        mov       edx, DWORD PTR [64+esp]                       ;160.15
        mov       ecx, DWORD PTR [56+esp]                       ;160.15
        mov       ebx, DWORD PTR [60+esp]                       ;160.15
        mov       eax, DWORD PTR [edx]                          ;172.1
        mov       DWORD PTR [32+esp], eax                       ;172.1
        mov       eax, DWORD PTR [ecx]                          ;174.13
        sub       eax, DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP+32] ;
        lea       edx, DWORD PTR [eax*8]                        ;
        sub       edx, eax                                      ;
        shl       edx, 5                                        ;
        mov       eax, DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP] ;174.3
        cmp       DWORD PTR [ebx], 1                            ;173.10
        je        .B4.8         ; Prob 16%                      ;173.10
                                ; LOE eax edx
.B4.2:                          ; Preds .B4.1
        mov       ecx, DWORD PTR [4+eax+edx]                    ;183.13
        pxor      xmm4, xmm4                                    ;169.1
        dec       ecx                                           ;183.3
        movaps    xmm0, xmm4                                    ;
        mov       DWORD PTR [24+esp], ecx                       ;183.3
        test      ecx, ecx                                      ;183.3
        jle       .B4.6         ; Prob 2%                       ;183.3
                                ; LOE eax edx xmm0 xmm4
.B4.3:                          ; Preds .B4.2
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELTIME+44] ;185.21
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELTIME+40] ;185.21
        imul      esi, ecx                                      ;
        movss     xmm3, DWORD PTR [_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT] ;186.73
        movss     xmm2, DWORD PTR [_2il0floatpacket.21]         ;185.56
        cvtsi2ss  xmm1, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIMPERAGG] ;186.98
        mov       ebp, DWORD PTR [_DYNUST_MAIN_MODULE_mp_AGGINT] ;186.7
        mov       DWORD PTR [28+esp], ecx                       ;185.21
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELTIME] ;185.7
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELTIME+32] ;
        sub       edi, esi                                      ;
        shl       ecx, 2                                        ;
        mov       DWORD PTR [esp], ebp                          ;186.7
        sub       edi, ecx                                      ;
        mov       ebp, DWORD PTR [44+eax+edx]                   ;185.7
        mov       ebx, DWORD PTR [76+eax+edx]                   ;185.7
        mov       edx, DWORD PTR [72+eax+edx]                   ;185.7
        mov       eax, edx                                      ;
        imul      ebx, edx                                      ;
        imul      ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32], -152 ;
        sub       ebp, ebx                                      ;
        add       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;
        mov       DWORD PTR [4+esp], 1                          ;
        mov       DWORD PTR [12+esp], ebp                       ;
        mov       DWORD PTR [20+esp], ecx                       ;
        mov       DWORD PTR [16+esp], edi                       ;
        mov       DWORD PTR [8+esp], edx                        ;
        mov       esi, DWORD PTR [4+esp]                        ;
        mov       ebp, DWORD PTR [esp]                          ;
        mov       ebx, DWORD PTR [32+esp]                       ;
        ALIGN     16
                                ; LOE eax ebx ebp esi xmm0 xmm1 xmm2 xmm3 xmm4
.B4.4:                          ; Preds .B4.4 .B4.3
        mov       edi, DWORD PTR [12+esp]                       ;185.21
        pxor      xmm5, xmm5                                    ;185.21
        mov       ecx, DWORD PTR [28+esp]                       ;185.7
        inc       esi                                           ;188.3
        imul      ecx, ebx                                      ;185.7
        mov       edx, DWORD PTR [eax+edi]                      ;185.21
        mov       edi, DWORD PTR [16+esp]                       ;185.7
        add       eax, DWORD PTR [8+esp]                        ;188.3
        lea       edi, DWORD PTR [edi+edx*4]                    ;185.7
        cvtsi2ss  xmm5, DWORD PTR [edi+ecx]                     ;185.21
        divss     xmm5, xmm2                                    ;185.56
        addss     xmm4, xmm5                                    ;185.7
        divss     xmm5, xmm3                                    ;186.83
        divss     xmm5, xmm1                                    ;186.32
        cvttss2si ecx, xmm5                                     ;186.32
        add       ebx, ecx                                      ;186.30
        imul      ecx, edx, 152                                 ;187.21
        cmp       ebx, ebp                                      ;186.7
        jl        L7            ; Prob 50%                      ;186.7
        mov       ebx, ebp                                      ;186.7
L7:                                                             ;
        mov       edx, DWORD PTR [20+esp]                       ;187.7
        cmp       esi, DWORD PTR [24+esp]                       ;188.3
        addss     xmm0, DWORD PTR [20+edx+ecx]                  ;187.7
        jle       .B4.4         ; Prob 82%                      ;188.3
                                ; LOE eax ebx ebp esi xmm0 xmm1 xmm2 xmm3 xmm4
.B4.6:                          ; Preds .B4.8 .B4.10 .B4.4 .B4.2
        divss     xmm0, xmm4                                    ;189.3
                                ; LOE xmm0
.B4.7:                          ; Preds .B4.6
        movss     DWORD PTR [esp], xmm0                         ;192.1
        fld       DWORD PTR [esp]                               ;192.1
        add       esp, 36                                       ;192.1
        pop       ebp                                           ;192.1
        pop       ebx                                           ;192.1
        pop       edi                                           ;192.1
        pop       esi                                           ;192.1
        ret                                                     ;192.1
                                ; LOE
.B4.8:                          ; Preds .B4.1                   ; Infreq
        mov       ecx, DWORD PTR [eax+edx]                      ;174.13
        pxor      xmm4, xmm4                                    ;169.1
        dec       ecx                                           ;174.3
        movaps    xmm0, xmm4                                    ;
        mov       DWORD PTR [24+esp], ecx                       ;174.3
        test      ecx, ecx                                      ;174.3
        jle       .B4.6         ; Prob 2%                       ;174.3
                                ; LOE eax edx xmm0 xmm4
.B4.9:                          ; Preds .B4.8                   ; Infreq
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELTIME+44] ;176.21
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELTIME+40] ;176.21
        imul      esi, ecx                                      ;
        movss     xmm3, DWORD PTR [_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT] ;177.74
        movss     xmm2, DWORD PTR [_2il0floatpacket.21]         ;176.57
        cvtsi2ss  xmm1, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIMPERAGG] ;177.99
        mov       ebp, DWORD PTR [_DYNUST_MAIN_MODULE_mp_AGGINT] ;177.7
        mov       DWORD PTR [28+esp], ecx                       ;176.21
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELTIME] ;176.7
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELTIME+32] ;
        sub       edi, esi                                      ;
        shl       ecx, 2                                        ;
        mov       DWORD PTR [4+esp], ebp                        ;177.7
        sub       edi, ecx                                      ;
        mov       ebp, DWORD PTR [8+eax+edx]                    ;176.7
        mov       ebx, DWORD PTR [40+eax+edx]                   ;176.7
        mov       edx, DWORD PTR [36+eax+edx]                   ;176.7
        mov       eax, edx                                      ;
        imul      ebx, edx                                      ;
        imul      ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32], -152 ;
        sub       ebp, ebx                                      ;
        add       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;
        mov       DWORD PTR [esp], 1                            ;
        mov       DWORD PTR [12+esp], ebp                       ;
        mov       DWORD PTR [20+esp], ecx                       ;
        mov       DWORD PTR [16+esp], edi                       ;
        mov       DWORD PTR [8+esp], edx                        ;
        mov       esi, DWORD PTR [esp]                          ;
        mov       ebp, DWORD PTR [4+esp]                        ;
        mov       ebx, DWORD PTR [32+esp]                       ;
        ALIGN     16
                                ; LOE eax ebx ebp esi xmm0 xmm1 xmm2 xmm3 xmm4
.B4.10:                         ; Preds .B4.10 .B4.9            ; Infreq
        mov       edi, DWORD PTR [12+esp]                       ;176.21
        mov       ecx, ebx                                      ;176.7
        imul      ecx, DWORD PTR [28+esp]                       ;176.7
        pxor      xmm5, xmm5                                    ;176.21
        mov       edx, DWORD PTR [eax+edi]                      ;176.21
        inc       esi                                           ;179.3
        mov       edi, DWORD PTR [16+esp]                       ;176.7
        add       eax, DWORD PTR [8+esp]                        ;179.3
        lea       edi, DWORD PTR [edi+edx*4]                    ;176.7
        cvtsi2ss  xmm5, DWORD PTR [edi+ecx]                     ;176.21
        divss     xmm5, xmm2                                    ;176.57
        addss     xmm4, xmm5                                    ;176.7
        divss     xmm5, xmm3                                    ;177.84
        divss     xmm5, xmm1                                    ;177.32
        cvttss2si ecx, xmm5                                     ;177.32
        add       ebx, ecx                                      ;177.30
        imul      ecx, edx, 152                                 ;178.21
        cmp       ebx, ebp                                      ;177.7
        jl        L8            ; Prob 50%                      ;177.7
        mov       ebx, ebp                                      ;177.7
L8:                                                             ;
        mov       edx, DWORD PTR [20+esp]                       ;178.7
        cmp       esi, DWORD PTR [24+esp]                       ;179.3
        addss     xmm0, DWORD PTR [20+edx+ecx]                  ;178.7
        jle       .B4.10        ; Prob 82%                      ;179.3
        jmp       .B4.6         ; Prob 100%                     ;179.3
        ALIGN     16
                                ; LOE eax ebx ebp esi xmm0 xmm1 xmm2 xmm3 xmm4
; mark_end;
_DYNUST_CONGESTIONPRICING_MODULE_mp_CALAVGSPEED ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_CONGESTIONPRICING_MODULE_mp_CALAVGSPEED
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_CONGESTIONPRICING_MODULE_mp_PRICEUPDATE
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_CONGESTIONPRICING_MODULE_mp_PRICEUPDATE
_DYNUST_CONGESTIONPRICING_MODULE_mp_PRICEUPDATE	PROC NEAR 
; parameter 1: 140 + esp
.B5.1:                          ; Preds .B5.0
        push      esi                                           ;195.12
        push      edi                                           ;195.12
        push      ebx                                           ;195.12
        push      ebp                                           ;195.12
        sub       esp, 120                                      ;195.12
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NTOLLLINK] ;205.4
        test      eax, eax                                      ;205.4
        jle       .B5.47        ; Prob 2%                       ;205.4
                                ; LOE eax
.B5.2:                          ; Preds .B5.1
        mov       edx, DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_NSEG] ;208.7
        mov       esi, 52                                       ;
        mov       DWORD PTR [52+esp], edx                       ;208.7
        imul      edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32], -152 ;
        imul      ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TOLLLINK+32], -52 ;
        imul      ecx, DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP+32], -224 ;
        movss     xmm2, DWORD PTR [_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT] ;211.42
        movss     xmm3, DWORD PTR [_2il0floatpacket.23]         ;212.73
        movss     xmm1, DWORD PTR [_2il0floatpacket.24]         ;220.55
        cvtsi2ss  xmm4, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIMPERAGG] ;211.88
        add       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;
        mov       edi, DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP] ;209.10
        mov       ebp, DWORD PTR [_DYNUST_MAIN_MODULE_mp_AGGINT] ;211.18
        mov       DWORD PTR [16+esp], 1                         ;
        add       ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TOLLLINK] ;
        mov       DWORD PTR [44+esp], edx                       ;
        mov       DWORD PTR [12+esp], ecx                       ;
        mov       DWORD PTR [96+esp], ebx                       ;
        mov       DWORD PTR [100+esp], ebp                      ;
        mov       DWORD PTR [20+esp], edi                       ;
        mov       DWORD PTR [24+esp], eax                       ;
        mov       edx, DWORD PTR [16+esp]                       ;
                                ; LOE edx esi xmm1 xmm2 xmm3 xmm4
.B5.3:                          ; Preds .B5.45 .B5.2
        mov       ecx, DWORD PTR [96+esp]                       ;206.4
        mov       ebp, DWORD PTR [12+esi+ecx]                   ;207.7
        test      ebp, ebp                                      ;207.7
        mov       eax, DWORD PTR [8+esi+ecx]                    ;206.4
        jle       .B5.45        ; Prob 2%                       ;207.7
                                ; LOE eax edx ebp esi xmm1 xmm2 xmm3 xmm4
.B5.4:                          ; Preds .B5.3
        imul      ecx, eax, 152                                 ;206.4
        mov       ebx, 1                                        ;
        mov       DWORD PTR [40+esp], ecx                       ;206.4
        cmp       DWORD PTR [52+esp], 0                         ;208.7
        jle       .B5.45        ; Prob 2%                       ;208.7
                                ; LOE eax edx ebx ebp esi xmm1 xmm2 xmm3 xmm4
.B5.5:                          ; Preds .B5.4
        mov       edi, DWORD PTR [12+esp]                       ;
        mov       ecx, DWORD PTR [20+esp]                       ;
        mov       DWORD PTR [116+esp], eax                      ;
        mov       DWORD PTR [92+esp], esi                       ;
        mov       DWORD PTR [16+esp], edx                       ;
        lea       ecx, DWORD PTR [224+ecx+edi]                  ;
                                ; LOE ecx ebx ebp xmm1 xmm2 xmm3 xmm4
.B5.6:                          ; Preds .B5.43 .B5.5
        mov       DWORD PTR [28+esp], ecx                       ;
        lea       esi, DWORD PTR [ebx+ebx*2]                    ;
        mov       DWORD PTR [88+esp], esi                       ;
        mov       edx, 1                                        ;
        mov       DWORD PTR [32+esp], ebx                       ;
        mov       eax, ecx                                      ;
        mov       DWORD PTR [36+esp], ebp                       ;
                                ; LOE eax edx xmm1 xmm2 xmm3 xmm4
.B5.7:                          ; Preds .B5.42 .B5.6
        mov       ecx, DWORD PTR [eax]                          ;209.10
        test      ecx, ecx                                      ;209.10
        jle       .B5.42        ; Prob 3%                       ;209.10
                                ; LOE eax edx ecx xmm1 xmm2 xmm3 xmm4
.B5.8:                          ; Preds .B5.7
        mov       esi, DWORD PTR [36+eax]                       ;210.16
        xor       ebp, ebp                                      ;
        mov       ebx, DWORD PTR [40+eax]                       ;210.16
        imul      ebx, esi                                      ;
        mov       edi, DWORD PTR [8+eax]                        ;210.16
        sub       edi, ebx                                      ;
        xor       ebx, ebx                                      ;
        mov       DWORD PTR [104+esp], edi                      ;
        mov       DWORD PTR [108+esp], esi                      ;
        mov       DWORD PTR [112+esp], ecx                      ;
        mov       DWORD PTR [48+esp], edx                       ;
                                ; LOE eax ebx ebp xmm1 xmm2 xmm3 xmm4
.B5.9:                          ; Preds .B5.40 .B5.8
        add       ebx, DWORD PTR [108+esp]                      ;210.32
        inc       ebp                                           ;210.32
        mov       ecx, DWORD PTR [104+esp]                      ;210.32
        mov       edx, DWORD PTR [116+esp]                      ;210.32
        cmp       edx, DWORD PTR [ebx+ecx]                      ;210.32
        jne       .B5.40        ; Prob 50%                      ;210.32
                                ; LOE eax ebx ebp xmm1 xmm2 xmm3 xmm4
.B5.10:                         ; Preds .B5.9
        mov       ecx, DWORD PTR [92+esp]                       ;211.18
        mov       edx, DWORD PTR [96+esp]                       ;211.18
        mov       esi, DWORD PTR [100+esp]                      ;211.18
        imul      edi, DWORD PTR [48+ecx+edx], -24              ;211.18
        add       edi, DWORD PTR [16+ecx+edx]                   ;211.18
        mov       edx, DWORD PTR [88+esp]                       ;211.42
        mov       DWORD PTR [84+esp], edi                       ;211.18
        movss     xmm5, DWORD PTR [4+edi+edx*8]                 ;212.43
        movss     xmm0, DWORD PTR [edi+edx*8]                   ;211.42
        subss     xmm5, xmm3                                    ;212.73
        divss     xmm0, xmm2                                    ;211.72
        divss     xmm5, xmm2                                    ;212.78
        divss     xmm0, xmm4                                    ;211.36
        divss     xmm5, xmm4                                    ;212.36
        cvttss2si ecx, xmm0                                     ;211.36
        cvttss2si edx, xmm5                                     ;212.36
        inc       ecx                                           ;211.98
        inc       edx                                           ;212.104
        cmp       ecx, esi                                      ;211.18
        cmovge    ecx, esi                                      ;211.18
        cmp       edx, esi                                      ;212.18
        cmovge    edx, esi                                      ;212.18
        sub       edx, ecx                                      ;213.29
        mov       esi, DWORD PTR [140+esp]                      ;216.20
        mov       DWORD PTR [76+esp], edx                       ;213.29
        mov       edi, DWORD PTR [esi]                          ;216.20
        lea       edx, DWORD PTR [1+edx]                        ;213.42
        mov       DWORD PTR [80+esp], edi                       ;216.20
        test      edx, edx                                      ;213.29
        mov       DWORD PTR [72+esp], edx                       ;213.42
        jle       .B5.51        ; Prob 10%                      ;213.29
                                ; LOE eax edx ecx ebx ebp dl dh xmm1 xmm2 xmm3 xmm4
.B5.11:                         ; Preds .B5.10
        mov       esi, edx                                      ;213.29
        mov       edx, esi                                      ;213.29
        sar       edx, 2                                        ;213.29
        shr       edx, 29                                       ;213.29
        pxor      xmm0, xmm0                                    ;
        add       edx, esi                                      ;213.29
        sar       edx, 3                                        ;213.29
        test      edx, edx                                      ;213.29
        jbe       .B5.50        ; Prob 0%                       ;213.29
                                ; LOE eax edx ecx ebx ebp xmm0 xmm1 xmm2 xmm3 xmm4
.B5.12:                         ; Preds .B5.11
        mov       DWORD PTR [8+esp], edx                        ;
        mov       edx, DWORD PTR [112+eax]                      ;213.29
        pxor      xmm6, xmm6                                    ;
        neg       edx                                           ;
        pxor      xmm5, xmm5                                    ;
        mov       edi, DWORD PTR [120+eax]                      ;213.29
        mov       esi, DWORD PTR [124+eax]                      ;213.29
        add       edx, DWORD PTR [80+esp]                       ;
        imul      esi, edi                                      ;
        imul      edx, DWORD PTR [108+eax]                      ;
        mov       DWORD PTR [esp], edi                          ;213.29
        mov       edi, DWORD PTR [80+eax]                       ;213.29
        sub       edi, esi                                      ;
        add       edx, edi                                      ;
        mov       DWORD PTR [68+esp], 0                         ;
        mov       DWORD PTR [4+esp], edx                        ;
        mov       DWORD PTR [56+esp], ebx                       ;
        mov       DWORD PTR [64+esp], eax                       ;
        mov       edx, DWORD PTR [8+esp]                        ;
        mov       esi, DWORD PTR [4+esp]                        ;
        mov       eax, DWORD PTR [esp]                          ;
        mov       ebx, DWORD PTR [68+esp]                       ;
        mov       DWORD PTR [60+esp], ebp                       ;
        ALIGN     16
                                ; LOE eax edx ecx ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5 xmm6
.B5.13:                         ; Preds .B5.13 .B5.12
        mov       edi, eax                                      ;213.29
        lea       ebp, DWORD PTR [ecx+ebx*8]                    ;213.29
        imul      edi, ebp                                      ;213.29
        inc       ebx                                           ;213.29
        addss     xmm0, DWORD PTR [esi+edi]                     ;213.29
        lea       edi, DWORD PTR [1+ebp]                        ;213.29
        imul      edi, eax                                      ;213.29
        addss     xmm6, DWORD PTR [esi+edi]                     ;213.29
        lea       edi, DWORD PTR [2+ebp]                        ;213.29
        imul      edi, eax                                      ;213.29
        addss     xmm5, DWORD PTR [esi+edi]                     ;213.29
        lea       edi, DWORD PTR [3+ebp]                        ;213.29
        imul      edi, eax                                      ;213.29
        addss     xmm6, DWORD PTR [esi+edi]                     ;213.29
        lea       edi, DWORD PTR [4+ebp]                        ;213.29
        imul      edi, eax                                      ;213.29
        addss     xmm5, DWORD PTR [esi+edi]                     ;213.29
        lea       edi, DWORD PTR [5+ebp]                        ;213.29
        imul      edi, eax                                      ;213.29
        addss     xmm6, DWORD PTR [esi+edi]                     ;213.29
        lea       edi, DWORD PTR [6+ebp]                        ;213.29
        add       ebp, 7                                        ;213.29
        imul      edi, eax                                      ;213.29
        imul      ebp, eax                                      ;213.29
        addss     xmm5, DWORD PTR [esi+edi]                     ;213.29
        addss     xmm6, DWORD PTR [esi+ebp]                     ;213.29
        cmp       ebx, edx                                      ;213.29
        jb        .B5.13        ; Prob 99%                      ;213.29
                                ; LOE eax edx ecx ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5 xmm6
.B5.14:                         ; Preds .B5.13
        mov       DWORD PTR [68+esp], ebx                       ;
        addss     xmm0, xmm6                                    ;213.29
        mov       edx, ebx                                      ;213.29
        addss     xmm0, xmm5                                    ;213.29
        mov       ebx, DWORD PTR [56+esp]                       ;
        lea       edx, DWORD PTR [1+edx*8]                      ;213.29
        mov       ebp, DWORD PTR [60+esp]                       ;
        mov       eax, DWORD PTR [64+esp]                       ;
                                ; LOE eax edx ecx ebx ebp xmm0 xmm1 xmm2 xmm3 xmm4
.B5.15:                         ; Preds .B5.14 .B5.50
        cmp       edx, DWORD PTR [72+esp]                       ;213.29
        ja        .B5.31        ; Prob 50%                      ;213.29
                                ; LOE eax edx ecx ebx ebp xmm0 xmm1 xmm2 xmm3 xmm4
.B5.16:                         ; Preds .B5.15
        mov       esi, DWORD PTR [76+esp]                       ;213.29
        sub       esi, edx                                      ;213.29
        jmp       DWORD PTR [..1..TPKT.17_0+4+esi*4]            ;213.29
                                ; LOE eax edx ecx ebx ebp xmm0 xmm1 xmm2 xmm3 xmm4
..1.17_0.TAG.6::
.B5.18:                         ; Preds .B5.16
        mov       esi, DWORD PTR [124+eax]                      ;213.29
        lea       edi, DWORD PTR [5+ecx+edx]                    ;213.29
        neg       esi                                           ;213.29
        add       esi, edi                                      ;213.29
        mov       edi, DWORD PTR [112+eax]                      ;213.29
        neg       edi                                           ;213.29
        add       edi, DWORD PTR [80+esp]                       ;213.29
        imul      edi, DWORD PTR [108+eax]                      ;213.29
        imul      esi, DWORD PTR [120+eax]                      ;213.29
        add       edi, DWORD PTR [80+eax]                       ;213.29
        addss     xmm0, DWORD PTR [edi+esi]                     ;213.29
                                ; LOE eax edx ecx ebx ebp xmm0 xmm1 xmm2 xmm3 xmm4
..1.17_0.TAG.5::
.B5.20:                         ; Preds .B5.16 .B5.18
        mov       esi, DWORD PTR [124+eax]                      ;213.29
        lea       edi, DWORD PTR [4+ecx+edx]                    ;213.29
        neg       esi                                           ;213.29
        add       esi, edi                                      ;213.29
        mov       edi, DWORD PTR [112+eax]                      ;213.29
        neg       edi                                           ;213.29
        add       edi, DWORD PTR [80+esp]                       ;213.29
        imul      edi, DWORD PTR [108+eax]                      ;213.29
        imul      esi, DWORD PTR [120+eax]                      ;213.29
        add       edi, DWORD PTR [80+eax]                       ;213.29
        addss     xmm0, DWORD PTR [edi+esi]                     ;213.29
                                ; LOE eax edx ecx ebx ebp xmm0 xmm1 xmm2 xmm3 xmm4
..1.17_0.TAG.4::
.B5.22:                         ; Preds .B5.16 .B5.20
        mov       esi, DWORD PTR [124+eax]                      ;213.29
        lea       edi, DWORD PTR [3+ecx+edx]                    ;213.29
        neg       esi                                           ;213.29
        add       esi, edi                                      ;213.29
        mov       edi, DWORD PTR [112+eax]                      ;213.29
        neg       edi                                           ;213.29
        add       edi, DWORD PTR [80+esp]                       ;213.29
        imul      edi, DWORD PTR [108+eax]                      ;213.29
        imul      esi, DWORD PTR [120+eax]                      ;213.29
        add       edi, DWORD PTR [80+eax]                       ;213.29
        addss     xmm0, DWORD PTR [edi+esi]                     ;213.29
                                ; LOE eax edx ecx ebx ebp xmm0 xmm1 xmm2 xmm3 xmm4
..1.17_0.TAG.3::
.B5.24:                         ; Preds .B5.16 .B5.22
        mov       esi, DWORD PTR [124+eax]                      ;213.29
        lea       edi, DWORD PTR [2+ecx+edx]                    ;213.29
        neg       esi                                           ;213.29
        add       esi, edi                                      ;213.29
        mov       edi, DWORD PTR [112+eax]                      ;213.29
        neg       edi                                           ;213.29
        add       edi, DWORD PTR [80+esp]                       ;213.29
        imul      edi, DWORD PTR [108+eax]                      ;213.29
        imul      esi, DWORD PTR [120+eax]                      ;213.29
        add       edi, DWORD PTR [80+eax]                       ;213.29
        addss     xmm0, DWORD PTR [edi+esi]                     ;213.29
                                ; LOE eax edx ecx ebx ebp xmm0 xmm1 xmm2 xmm3 xmm4
..1.17_0.TAG.2::
.B5.26:                         ; Preds .B5.16 .B5.24
        mov       esi, DWORD PTR [124+eax]                      ;213.29
        lea       edi, DWORD PTR [1+ecx+edx]                    ;213.29
        neg       esi                                           ;213.29
        add       esi, edi                                      ;213.29
        mov       edi, DWORD PTR [112+eax]                      ;213.29
        neg       edi                                           ;213.29
        add       edi, DWORD PTR [80+esp]                       ;213.29
        imul      edi, DWORD PTR [108+eax]                      ;213.29
        imul      esi, DWORD PTR [120+eax]                      ;213.29
        add       edi, DWORD PTR [80+eax]                       ;213.29
        addss     xmm0, DWORD PTR [edi+esi]                     ;213.29
                                ; LOE eax edx ecx ebx ebp xmm0 xmm1 xmm2 xmm3 xmm4
..1.17_0.TAG.1::
.B5.28:                         ; Preds .B5.16 .B5.26
        mov       esi, DWORD PTR [124+eax]                      ;213.29
        lea       edi, DWORD PTR [ecx+edx]                      ;213.29
        neg       esi                                           ;213.29
        add       esi, edi                                      ;213.29
        mov       edi, DWORD PTR [112+eax]                      ;213.29
        neg       edi                                           ;213.29
        add       edi, DWORD PTR [80+esp]                       ;213.29
        imul      edi, DWORD PTR [108+eax]                      ;213.29
        imul      esi, DWORD PTR [120+eax]                      ;213.29
        add       edi, DWORD PTR [80+eax]                       ;213.29
        addss     xmm0, DWORD PTR [edi+esi]                     ;213.29
                                ; LOE eax edx ecx ebx ebp xmm0 xmm1 xmm2 xmm3 xmm4
..1.17_0.TAG.0::
.B5.30:                         ; Preds .B5.16 .B5.28
        mov       esi, DWORD PTR [124+eax]                      ;213.29
        lea       edx, DWORD PTR [-1+ecx+edx]                   ;213.29
        mov       ecx, DWORD PTR [112+eax]                      ;213.29
        neg       esi                                           ;213.29
        neg       ecx                                           ;213.29
        add       esi, edx                                      ;213.29
        add       ecx, DWORD PTR [80+esp]                       ;213.29
        imul      ecx, DWORD PTR [108+eax]                      ;213.29
        imul      esi, DWORD PTR [120+eax]                      ;213.29
        add       ecx, DWORD PTR [80+eax]                       ;213.29
        addss     xmm0, DWORD PTR [ecx+esi]                     ;213.29
                                ; LOE eax ebx ebp xmm0 xmm1 xmm2 xmm3 xmm4
.B5.31:                         ; Preds .B5.51 .B5.30 .B5.15
        cvtsi2ss  xmm5, DWORD PTR [72+esp]                      ;213.68
        mov       ecx, DWORD PTR [88+esp]                       ;215.53
        mov       edx, DWORD PTR [84+esp]                       ;215.53
        divss     xmm0, xmm5                                    ;213.18
        cmp       DWORD PTR [20+edx+ecx*8], 0                   ;215.53
        jne       .B5.36        ; Prob 50%                      ;215.53
                                ; LOE eax edx ecx ebx ebp dl cl dh ch xmm0 xmm1 xmm2 xmm3 xmm4
.B5.32:                         ; Preds .B5.31
        cmp       DWORD PTR [80+esp], 1                         ;216.32
        je        .B5.48        ; Prob 16%                      ;216.32
                                ; LOE eax edx ecx ebx ebp dl cl dh ch xmm0 xmm1 xmm2 xmm3 xmm4
.B5.33:                         ; Preds .B5.32
        comiss    xmm1, DWORD PTR [12+edx+ecx*8]                ;220.55
        jbe       .B5.40        ; Prob 50%                      ;220.55
                                ; LOE eax edx ecx ebx ebp dl cl dh ch xmm0 xmm1 xmm2 xmm3 xmm4
.B5.34:                         ; Preds .B5.33
        movss     xmm5, DWORD PTR [204+eax]                     ;220.97
        maxss     xmm5, xmm0                                    ;220.62
        comiss    xmm1, xmm5                                    ;221.55
        movss     DWORD PTR [12+edx+ecx*8], xmm5                ;220.62
        jbe       .B5.40        ; Prob 50%                      ;221.55
                                ; LOE eax edx ecx ebx ebp dl cl dh ch xmm0 xmm1 xmm2 xmm3 xmm4
.B5.35:                         ; Preds .B5.34
        movss     xmm5, DWORD PTR [208+eax]                     ;221.97
        minss     xmm5, xmm0                                    ;221.62
        movss     DWORD PTR [12+edx+ecx*8], xmm5                ;221.62
        jmp       .B5.40        ; Prob 100%                     ;221.62
                                ; LOE eax ebx ebp xmm1 xmm2 xmm3 xmm4
.B5.36:                         ; Preds .B5.31
        cmp       DWORD PTR [80+esp], 1                         ;224.32
        je        .B5.49        ; Prob 16%                      ;224.32
                                ; LOE eax edx ecx ebx ebp dl cl dh ch xmm0 xmm1 xmm2 xmm3 xmm4
.B5.37:                         ; Preds .B5.36
        comiss    xmm1, DWORD PTR [12+edx+ecx*8]                ;228.54
        jbe       .B5.40        ; Prob 50%                      ;228.54
                                ; LOE eax ebx ebp xmm0 xmm1 xmm2 xmm3 xmm4
.B5.38:                         ; Preds .B5.37
        movss     xmm5, DWORD PTR [204+eax]                     ;228.96
        mov       edx, DWORD PTR [40+esp]                       ;228.61
        maxss     xmm5, xmm0                                    ;228.61
        mov       ecx, DWORD PTR [44+esp]                       ;228.61
        mov       edi, DWORD PTR [88+esp]                       ;228.61
        mov       esi, DWORD PTR [84+esp]                       ;228.61
        mulss     xmm5, DWORD PTR [20+edx+ecx]                  ;228.61
        movss     DWORD PTR [12+esi+edi*8], xmm5                ;228.61
        comiss    xmm1, xmm5                                    ;229.54
        jbe       .B5.40        ; Prob 50%                      ;229.54
                                ; LOE eax edx ecx ebx ebp esi edi dl cl dh ch xmm0 xmm1 xmm2 xmm3 xmm4
.B5.39:                         ; Preds .B5.38
        movss     xmm5, DWORD PTR [208+eax]                     ;229.96
        minss     xmm5, xmm0                                    ;229.61
        mulss     xmm5, DWORD PTR [20+edx+ecx]                  ;229.61
        movss     DWORD PTR [12+esi+edi*8], xmm5                ;229.61
                                ; LOE eax ebx ebp xmm1 xmm2 xmm3 xmm4
.B5.40:                         ; Preds .B5.33 .B5.37 .B5.9 .B5.38 .B5.34
                                ;       .B5.48 .B5.35 .B5.49 .B5.39
        cmp       ebp, DWORD PTR [112+esp]                      ;209.10
        jb        .B5.9         ; Prob 82%                      ;209.10
                                ; LOE eax ebx ebp xmm1 xmm2 xmm3 xmm4
.B5.41:                         ; Preds .B5.40
        mov       edx, DWORD PTR [48+esp]                       ;
                                ; LOE eax edx xmm1 xmm2 xmm3 xmm4
.B5.42:                         ; Preds .B5.41 .B5.7
        inc       edx                                           ;234.7
        add       eax, 224                                      ;234.7
        cmp       edx, DWORD PTR [52+esp]                       ;234.7
        jle       .B5.7         ; Prob 82%                      ;234.7
                                ; LOE eax edx xmm1 xmm2 xmm3 xmm4
.B5.43:                         ; Preds .B5.42
        mov       ebx, DWORD PTR [32+esp]                       ;
        inc       ebx                                           ;207.7
        mov       ebp, DWORD PTR [36+esp]                       ;
        cmp       ebx, ebp                                      ;207.7
        mov       ecx, DWORD PTR [28+esp]                       ;
        jle       .B5.6         ; Prob 82%                      ;207.7
                                ; LOE ecx ebx ebp xmm1 xmm2 xmm3 xmm4
.B5.44:                         ; Preds .B5.43
        mov       esi, DWORD PTR [92+esp]                       ;
        mov       edx, DWORD PTR [16+esp]                       ;
                                ; LOE edx esi xmm1 xmm2 xmm3 xmm4
.B5.45:                         ; Preds .B5.4 .B5.44 .B5.3
        inc       edx                                           ;205.4
        add       esi, 52                                       ;205.4
        cmp       edx, DWORD PTR [24+esp]                       ;205.4
        jle       .B5.3         ; Prob 82%                      ;205.4
                                ; LOE edx esi xmm1 xmm2 xmm3 xmm4
.B5.47:                         ; Preds .B5.45 .B5.1
        add       esp, 120                                      ;241.1
        pop       ebp                                           ;241.1
        pop       ebx                                           ;241.1
        pop       edi                                           ;241.1
        pop       esi                                           ;241.1
        ret                                                     ;241.1
                                ; LOE
.B5.48:                         ; Preds .B5.32                  ; Infreq
        movss     xmm5, DWORD PTR [208+eax]                     ;218.58
        minss     xmm5, xmm0                                    ;218.23
        movss     DWORD PTR [8+edx+ecx*8], xmm5                 ;218.23
        jmp       .B5.40        ; Prob 100%                     ;218.23
                                ; LOE eax ebx ebp xmm1 xmm2 xmm3 xmm4
.B5.49:                         ; Preds .B5.36                  ; Infreq
        movss     xmm5, DWORD PTR [208+eax]                     ;226.57
        mov       edx, DWORD PTR [40+esp]                       ;226.22
        minss     xmm5, xmm0                                    ;226.22
        mov       ecx, DWORD PTR [44+esp]                       ;226.22
        mov       edi, DWORD PTR [88+esp]                       ;226.22
        mov       esi, DWORD PTR [84+esp]                       ;226.22
        mulss     xmm5, DWORD PTR [20+edx+ecx]                  ;226.22
        movss     DWORD PTR [8+esi+edi*8], xmm5                 ;226.22
        jmp       .B5.40        ; Prob 100%                     ;226.22
                                ; LOE eax ebx ebp xmm1 xmm2 xmm3 xmm4
.B5.50:                         ; Preds .B5.11                  ; Infreq
        mov       edx, 1                                        ;
        jmp       .B5.15        ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx ebp xmm0 xmm1 xmm2 xmm3 xmm4
.B5.51:                         ; Preds .B5.10                  ; Infreq
        pxor      xmm0, xmm0                                    ;
        jmp       .B5.31        ; Prob 100%                     ;
        ALIGN     16
                                ; LOE eax ebx ebp xmm0 xmm1 xmm2 xmm3 xmm4
; mark_end;
_DYNUST_CONGESTIONPRICING_MODULE_mp_PRICEUPDATE ENDP
_TEXT	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
..1..TPKT.17_0	DD	OFFSET FLAT: ..1.17_0.TAG.0
	DD	OFFSET FLAT: ..1.17_0.TAG.1
	DD	OFFSET FLAT: ..1.17_0.TAG.2
	DD	OFFSET FLAT: ..1.17_0.TAG.3
	DD	OFFSET FLAT: ..1.17_0.TAG.4
	DD	OFFSET FLAT: ..1.17_0.TAG.5
	DD	OFFSET FLAT: ..1.17_0.TAG.6
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_CONGESTIONPRICING_MODULE_mp_PRICEUPDATE
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_CONGESTIONPRICING_MODULE_mp_PRICEOUTPUT
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_CONGESTIONPRICING_MODULE_mp_PRICEOUTPUT
_DYNUST_CONGESTIONPRICING_MODULE_mp_PRICEOUTPUT	PROC NEAR 
.B6.1:                          ; Preds .B6.0
        push      ebp                                           ;244.12
        mov       ebp, esp                                      ;244.12
        and       esp, -16                                      ;244.12
        push      esi                                           ;244.12
        push      edi                                           ;244.12
        push      ebx                                           ;244.12
        sub       esp, 180                                      ;244.12
        mov       DWORD PTR [64+esp], 0                         ;251.1
        lea       ebx, DWORD PTR [64+esp]                       ;251.1
        mov       DWORD PTR [esp], 14                           ;251.1
        lea       eax, DWORD PTR [esp]                          ;251.1
        mov       DWORD PTR [4+esp], OFFSET FLAT: __STRLITPACK_11 ;251.1
        mov       DWORD PTR [8+esp], 7                          ;251.1
        mov       DWORD PTR [12+esp], OFFSET FLAT: __STRLITPACK_10 ;251.1
        push      32                                            ;251.1
        push      eax                                           ;251.1
        push      OFFSET FLAT: __STRLITPACK_55.0.6              ;251.1
        push      -2088435968                                   ;251.1
        push      7778                                          ;251.1
        push      ebx                                           ;251.1
        call      _for_open                                     ;251.1
                                ; LOE ebx
.B6.2:                          ; Preds .B6.1
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TOLLVOTA] ;253.2
        lea       edx, DWORD PTR [48+esp]                       ;253.2
        mov       DWORD PTR [88+esp], 0                         ;253.2
        mov       DWORD PTR [48+esp], eax                       ;253.2
        push      32                                            ;253.2
        push      OFFSET FLAT: DYNUST_CONGESTIONPRICING_MODULE_mp_PRICEOUTPUT$format_pack.0.6+56 ;253.2
        push      edx                                           ;253.2
        push      OFFSET FLAT: __STRLITPACK_56.0.6              ;253.2
        push      -2088435968                                   ;253.2
        push      7778                                          ;253.2
        push      ebx                                           ;253.2
        call      _for_write_seq_fmt                            ;253.2
                                ; LOE ebx
.B6.3:                          ; Preds .B6.2
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TOLLVOTH] ;253.2
        lea       edx, DWORD PTR [84+esp]                       ;253.2
        mov       DWORD PTR [84+esp], eax                       ;253.2
        push      edx                                           ;253.2
        push      OFFSET FLAT: __STRLITPACK_57.0.6              ;253.2
        push      ebx                                           ;253.2
        call      _for_write_seq_fmt_xmit                       ;253.2
                                ; LOE ebx
.B6.4:                          ; Preds .B6.3
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TOLLVOTT] ;253.2
        lea       edx, DWORD PTR [104+esp]                      ;253.2
        mov       DWORD PTR [104+esp], eax                      ;253.2
        push      edx                                           ;253.2
        push      OFFSET FLAT: __STRLITPACK_58.0.6              ;253.2
        push      ebx                                           ;253.2
        call      _for_write_seq_fmt_xmit                       ;253.2
                                ; LOE ebx
.B6.5:                          ; Preds .B6.4
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELTIMEWEIGHT] ;254.2
        lea       edx, DWORD PTR [124+esp]                      ;254.2
        mov       DWORD PTR [140+esp], 0                        ;254.2
        mov       DWORD PTR [124+esp], eax                      ;254.2
        push      32                                            ;254.2
        push      OFFSET FLAT: DYNUST_CONGESTIONPRICING_MODULE_mp_PRICEOUTPUT$format_pack.0.6+76 ;254.2
        push      edx                                           ;254.2
        push      OFFSET FLAT: __STRLITPACK_59.0.6              ;254.2
        push      -2088435968                                   ;254.2
        push      7778                                          ;254.2
        push      ebx                                           ;254.2
        call      _for_write_seq_fmt                            ;254.2
                                ; LOE ebx
.B6.6:                          ; Preds .B6.5
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NTOLLLINKTMP] ;254.2
        lea       edx, DWORD PTR [160+esp]                      ;254.2
        mov       DWORD PTR [160+esp], eax                      ;254.2
        push      edx                                           ;254.2
        push      OFFSET FLAT: __STRLITPACK_60.0.6              ;254.2
        push      ebx                                           ;254.2
        call      _for_write_seq_fmt_xmit                       ;254.2
                                ; LOE ebx
.B6.37:                         ; Preds .B6.6
        add       esp, 116                                      ;254.2
                                ; LOE ebx
.B6.7:                          ; Preds .B6.37
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NTOLLLINK] ;255.5
        test      edi, edi                                      ;255.5
        jle       .B6.23        ; Prob 2%                       ;255.5
                                ; LOE ebx edi
.B6.8:                          ; Preds .B6.7
        imul      esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE+32], -44 ;
        mov       edx, 1                                        ;
        imul      ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TOLLLINK+32], -52 ;
        add       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE] ;
        mov       eax, 52                                       ;
        add       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TOLLLINK] ;
        mov       DWORD PTR [16+esp], esi                       ;
        mov       DWORD PTR [60+esp], edi                       ;
                                ; LOE eax edx ecx
.B6.9:                          ; Preds .B6.21 .B6.8
        mov       edi, DWORD PTR [12+eax+ecx]                   ;257.4
        test      edi, edi                                      ;257.4
        jle       .B6.21        ; Prob 2%                       ;257.4
                                ; LOE eax edx ecx edi
.B6.10:                         ; Preds .B6.9
        imul      esi, DWORD PTR [48+eax+ecx], -24              ;
        add       esi, DWORD PTR [16+eax+ecx]                   ;
        mov       DWORD PTR [52+esp], esi                       ;
        imul      esi, DWORD PTR [eax+ecx], 44                  ;258.25
        mov       ebx, DWORD PTR [16+esp]                       ;258.9
        mov       DWORD PTR [44+esp], 1                         ;
        mov       DWORD PTR [104+esp], edi                      ;258.9
        mov       esi, DWORD PTR [36+esi+ebx]                   ;258.9
        mov       DWORD PTR [96+esp], esi                       ;258.9
        imul      esi, DWORD PTR [4+eax+ecx], 44                ;258.85
        mov       ebx, DWORD PTR [36+esi+ebx]                   ;258.9
        mov       DWORD PTR [100+esp], ebx                      ;258.9
        mov       esi, DWORD PTR [52+esp]                       ;258.9
        mov       ebx, DWORD PTR [44+esp]                       ;258.9
        mov       DWORD PTR [36+esp], eax                       ;258.9
        mov       DWORD PTR [28+esp], ecx                       ;258.9
        mov       DWORD PTR [20+esp], edx                       ;258.9
                                ; LOE ebx esi
.B6.11:                         ; Preds .B6.19 .B6.10
        mov       eax, DWORD PTR [96+esp]                       ;258.9
        mov       DWORD PTR [64+esp], 0                         ;258.9
        mov       DWORD PTR [112+esp], eax                      ;258.9
        push      32                                            ;258.9
        push      OFFSET FLAT: DYNUST_CONGESTIONPRICING_MODULE_mp_PRICEOUTPUT$format_pack.0.6 ;258.9
        lea       edx, DWORD PTR [120+esp]                      ;258.9
        push      edx                                           ;258.9
        push      OFFSET FLAT: __STRLITPACK_61.0.6              ;258.9
        push      -2088435968                                   ;258.9
        push      7778                                          ;258.9
        lea       ecx, DWORD PTR [88+esp]                       ;258.9
        push      ecx                                           ;258.9
        call      _for_write_seq_fmt                            ;258.9
                                ; LOE ebx esi
.B6.12:                         ; Preds .B6.11
        mov       eax, DWORD PTR [128+esp]                      ;258.9
        lea       edx, DWORD PTR [148+esp]                      ;258.9
        mov       DWORD PTR [148+esp], eax                      ;258.9
        push      edx                                           ;258.9
        push      OFFSET FLAT: __STRLITPACK_62.0.6              ;258.9
        lea       ecx, DWORD PTR [100+esp]                      ;258.9
        push      ecx                                           ;258.9
        call      _for_write_seq_fmt_xmit                       ;258.9
                                ; LOE ebx esi
.B6.13:                         ; Preds .B6.12
        lea       edi, DWORD PTR [ebx+ebx*2]                    ;258.9
        mov       eax, DWORD PTR [esi+edi*8]                    ;258.9
        lea       edx, DWORD PTR [168+esp]                      ;258.9
        mov       DWORD PTR [168+esp], eax                      ;258.9
        push      edx                                           ;258.9
        push      OFFSET FLAT: __STRLITPACK_63.0.6              ;258.9
        lea       ecx, DWORD PTR [112+esp]                      ;258.9
        push      ecx                                           ;258.9
        call      _for_write_seq_fmt_xmit                       ;258.9
                                ; LOE ebx esi edi
.B6.14:                         ; Preds .B6.13
        mov       eax, DWORD PTR [4+esi+edi*8]                  ;258.9
        lea       edx, DWORD PTR [188+esp]                      ;258.9
        mov       DWORD PTR [188+esp], eax                      ;258.9
        push      edx                                           ;258.9
        push      OFFSET FLAT: __STRLITPACK_64.0.6              ;258.9
        lea       ecx, DWORD PTR [124+esp]                      ;258.9
        push      ecx                                           ;258.9
        call      _for_write_seq_fmt_xmit                       ;258.9
                                ; LOE ebx esi edi
.B6.15:                         ; Preds .B6.14
        mov       eax, DWORD PTR [20+esi+edi*8]                 ;258.9
        lea       edx, DWORD PTR [208+esp]                      ;258.9
        mov       DWORD PTR [208+esp], eax                      ;258.9
        push      edx                                           ;258.9
        push      OFFSET FLAT: __STRLITPACK_65.0.6              ;258.9
        lea       ecx, DWORD PTR [136+esp]                      ;258.9
        push      ecx                                           ;258.9
        call      _for_write_seq_fmt_xmit                       ;258.9
                                ; LOE ebx esi edi
.B6.16:                         ; Preds .B6.15
        mov       eax, DWORD PTR [8+esi+edi*8]                  ;258.9
        lea       edx, DWORD PTR [228+esp]                      ;258.9
        mov       DWORD PTR [228+esp], eax                      ;258.9
        push      edx                                           ;258.9
        push      OFFSET FLAT: __STRLITPACK_66.0.6              ;258.9
        lea       ecx, DWORD PTR [148+esp]                      ;258.9
        push      ecx                                           ;258.9
        call      _for_write_seq_fmt_xmit                       ;258.9
                                ; LOE ebx esi edi
.B6.17:                         ; Preds .B6.16
        mov       eax, DWORD PTR [16+esi+edi*8]                 ;258.9
        lea       edx, DWORD PTR [248+esp]                      ;258.9
        mov       DWORD PTR [248+esp], eax                      ;258.9
        push      edx                                           ;258.9
        push      OFFSET FLAT: __STRLITPACK_67.0.6              ;258.9
        lea       ecx, DWORD PTR [160+esp]                      ;258.9
        push      ecx                                           ;258.9
        call      _for_write_seq_fmt_xmit                       ;258.9
                                ; LOE ebx esi edi
.B6.18:                         ; Preds .B6.17
        mov       eax, DWORD PTR [12+esi+edi*8]                 ;258.9
        lea       edx, DWORD PTR [268+esp]                      ;258.9
        mov       DWORD PTR [268+esp], eax                      ;258.9
        push      edx                                           ;258.9
        push      OFFSET FLAT: __STRLITPACK_68.0.6              ;258.9
        lea       ecx, DWORD PTR [172+esp]                      ;258.9
        push      ecx                                           ;258.9
        call      _for_write_seq_fmt_xmit                       ;258.9
                                ; LOE ebx esi
.B6.38:                         ; Preds .B6.18
        add       esp, 112                                      ;258.9
                                ; LOE ebx esi
.B6.19:                         ; Preds .B6.38
        inc       ebx                                           ;260.4
        cmp       ebx, DWORD PTR [104+esp]                      ;260.4
        jle       .B6.11        ; Prob 82%                      ;260.4
                                ; LOE ebx esi
.B6.20:                         ; Preds .B6.19
        mov       eax, DWORD PTR [36+esp]                       ;
        mov       ecx, DWORD PTR [28+esp]                       ;
        mov       edx, DWORD PTR [20+esp]                       ;
                                ; LOE eax edx ecx
.B6.21:                         ; Preds .B6.20 .B6.9
        inc       edx                                           ;255.5
        add       eax, 52                                       ;255.5
        cmp       edx, DWORD PTR [60+esp]                       ;255.5
        jle       .B6.9         ; Prob 82%                      ;255.5
                                ; LOE eax edx ecx
.B6.22:                         ; Preds .B6.21
        lea       ebx, DWORD PTR [64+esp]                       ;
                                ; LOE ebx
.B6.23:                         ; Preds .B6.22 .B6.7
        xor       eax, eax                                      ;263.7
        mov       DWORD PTR [64+esp], eax                       ;263.7
        push      32                                            ;263.7
        push      eax                                           ;263.7
        push      OFFSET FLAT: __STRLITPACK_69.0.6              ;263.7
        push      -2088435968                                   ;263.7
        push      7778                                          ;263.7
        push      ebx                                           ;263.7
        call      _for_close                                    ;263.7
                                ; LOE
.B6.39:                         ; Preds .B6.23
        add       esp, 24                                       ;263.7
                                ; LOE
.B6.24:                         ; Preds .B6.39
        mov       edx, DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_NSEG] ;266.1
        test      edx, edx                                      ;266.1
        mov       eax, DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP] ;273.1
        jle       .B6.33        ; Prob 28%                      ;266.1
                                ; LOE eax edx
.B6.25:                         ; Preds .B6.24
        imul      edi, DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP+32], -224 ;
        mov       esi, 1                                        ;
        mov       DWORD PTR [16+esp], eax                       ;
        add       edi, eax                                      ;
        mov       DWORD PTR [20+esp], edx                       ;
        mov       ebx, 224                                      ;
                                ; LOE ebx esi edi
.B6.26:                         ; Preds .B6.31 .B6.25
        mov       eax, DWORD PTR [20+ebx+edi]                   ;267.14
        mov       edx, eax                                      ;267.3
        shr       edx, 1                                        ;267.3
        and       eax, 1                                        ;267.3
        and       edx, 1                                        ;267.3
        add       eax, eax                                      ;267.3
        shl       edx, 2                                        ;267.3
        or        edx, eax                                      ;267.3
        or        edx, 262144                                   ;267.3
        push      edx                                           ;267.3
        push      DWORD PTR [8+ebx+edi]                         ;267.3
        call      _for_dealloc_allocatable                      ;267.3
                                ; LOE ebx esi edi
.B6.27:                         ; Preds .B6.26
        mov       eax, DWORD PTR [56+ebx+edi]                   ;268.14
        mov       edx, eax                                      ;268.3
        shr       edx, 1                                        ;268.3
        and       eax, 1                                        ;268.3
        and       edx, 1                                        ;268.3
        add       eax, eax                                      ;268.3
        shl       edx, 2                                        ;268.3
        or        edx, eax                                      ;268.3
        or        edx, 262144                                   ;268.3
        push      edx                                           ;268.3
        push      DWORD PTR [44+ebx+edi]                        ;268.3
        mov       DWORD PTR [8+ebx+edi], 0                      ;267.3
        and       DWORD PTR [20+ebx+edi], -2                    ;267.3
        call      _for_dealloc_allocatable                      ;268.3
                                ; LOE ebx esi edi
.B6.28:                         ; Preds .B6.27
        mov       eax, DWORD PTR [92+ebx+edi]                   ;269.14
        mov       edx, eax                                      ;269.3
        shr       edx, 1                                        ;269.3
        and       eax, 1                                        ;269.3
        and       edx, 1                                        ;269.3
        add       eax, eax                                      ;269.3
        shl       edx, 2                                        ;269.3
        or        edx, eax                                      ;269.3
        or        edx, 262144                                   ;269.3
        push      edx                                           ;269.3
        push      DWORD PTR [80+ebx+edi]                        ;269.3
        mov       DWORD PTR [44+ebx+edi], 0                     ;268.3
        and       DWORD PTR [56+ebx+edi], -2                    ;268.3
        call      _for_dealloc_allocatable                      ;269.3
                                ; LOE ebx esi edi
.B6.29:                         ; Preds .B6.28
        mov       eax, DWORD PTR [140+ebx+edi]                  ;270.14
        mov       edx, eax                                      ;270.3
        shr       edx, 1                                        ;270.3
        and       eax, 1                                        ;270.3
        and       edx, 1                                        ;270.3
        add       eax, eax                                      ;270.3
        shl       edx, 2                                        ;270.3
        or        edx, eax                                      ;270.3
        or        edx, 262144                                   ;270.3
        push      edx                                           ;270.3
        push      DWORD PTR [128+ebx+edi]                       ;270.3
        mov       DWORD PTR [80+ebx+edi], 0                     ;269.3
        and       DWORD PTR [92+ebx+edi], -2                    ;269.3
        call      _for_dealloc_allocatable                      ;270.3
                                ; LOE ebx esi edi
.B6.30:                         ; Preds .B6.29
        mov       eax, DWORD PTR [176+ebx+edi]                  ;271.14
        mov       edx, eax                                      ;271.3
        shr       edx, 1                                        ;271.3
        and       eax, 1                                        ;271.3
        and       edx, 1                                        ;271.3
        add       eax, eax                                      ;271.3
        shl       edx, 2                                        ;271.3
        or        edx, eax                                      ;271.3
        or        edx, 262144                                   ;271.3
        push      edx                                           ;271.3
        push      DWORD PTR [164+ebx+edi]                       ;271.3
        mov       DWORD PTR [128+ebx+edi], 0                    ;270.3
        and       DWORD PTR [140+ebx+edi], -2                   ;270.3
        call      _for_dealloc_allocatable                      ;271.3
                                ; LOE ebx esi edi
.B6.40:                         ; Preds .B6.30
        add       esp, 40                                       ;271.3
                                ; LOE ebx esi edi
.B6.31:                         ; Preds .B6.40
        inc       esi                                           ;272.1
        mov       DWORD PTR [164+ebx+edi], 0                    ;271.3
        and       DWORD PTR [176+ebx+edi], -2                   ;271.3
        add       ebx, 224                                      ;272.1
        cmp       esi, DWORD PTR [20+esp]                       ;272.1
        jle       .B6.26        ; Prob 82%                      ;272.1
                                ; LOE ebx esi edi
.B6.32:                         ; Preds .B6.31
        mov       eax, DWORD PTR [16+esp]                       ;
                                ; LOE eax
.B6.33:                         ; Preds .B6.32 .B6.24
        mov       ebx, DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP+12] ;273.1
        mov       ecx, ebx                                      ;273.1
        shr       ecx, 1                                        ;273.1
        mov       edx, ebx                                      ;273.1
        and       ecx, 1                                        ;273.1
        and       edx, 1                                        ;273.1
        shl       ecx, 2                                        ;273.1
        add       edx, edx                                      ;273.1
        or        ecx, edx                                      ;273.1
        or        ecx, 262144                                   ;273.1
        push      ecx                                           ;273.1
        push      eax                                           ;273.1
        call      _for_dealloc_allocatable                      ;273.1
                                ; LOE ebx
.B6.34:                         ; Preds .B6.33
        and       ebx, -2                                       ;273.1
        mov       DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP], 0 ;273.1
        mov       DWORD PTR [_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP+12], ebx ;273.1
        add       esp, 188                                      ;275.1
        pop       ebx                                           ;275.1
        pop       edi                                           ;275.1
        pop       esi                                           ;275.1
        mov       esp, ebp                                      ;275.1
        pop       ebp                                           ;275.1
        ret                                                     ;275.1
        ALIGN     16
                                ; LOE
; mark_end;
_DYNUST_CONGESTIONPRICING_MODULE_mp_PRICEOUTPUT ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
	DD 4 DUP (0H)	; pad
DYNUST_CONGESTIONPRICING_MODULE_mp_PRICEOUTPUT$format_pack.0.6	DB	54
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	2
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	1
	DB	2
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	5
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	3
	DB	3
	DB	0
	DB	0
	DB	0
	DB	10
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	3
	DB	3
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	3
	DB	1
	DB	0
	DB	0
	DB	0
	DB	5
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	6
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
_DATA	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
__STRLITPACK_55.0.6	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	26
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_56.0.6	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_57.0.6	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_58.0.6	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_59.0.6	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_60.0.6	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_61.0.6	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_62.0.6	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_63.0.6	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_64.0.6	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_65.0.6	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_66.0.6	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_67.0.6	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_68.0.6	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_69.0.6	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_CONGESTIONPRICING_MODULE_mp_PRICEOUTPUT
_BSS	SEGMENT  DWORD PUBLIC FLAT  'BSS'
	ALIGN 004H
	PUBLIC _DYNUST_CONGESTIONPRICING_MODULE_mp_NSEG
_DYNUST_CONGESTIONPRICING_MODULE_mp_NSEG	DD 1 DUP (0H)	; pad
	PUBLIC _DYNUST_CONGESTIONPRICING_MODULE_mp_CONGEXT
_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGEXT	DD 1 DUP (0H)	; pad
_BSS	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
	DD 1 DUP (0H)	; pad
	PUBLIC _DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP
_DYNUST_CONGESTIONPRICING_MODULE_mp_CONGP	DD	0
	DD	0
	DD	0
	DD	128
	DD	1
	DD	0
	DD	0
	DD	0
	DD	0
_DATA	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_20	DB	67
	DB	111
	DB	110
	DB	103
	DB	101
	DB	115
	DB	116
	DB	105
	DB	111
	DB	110
	DB	80
	DB	114
	DB	105
	DB	99
	DB	105
	DB	110
	DB	103
	DB	67
	DB	111
	DB	110
	DB	102
	DB	105
	DB	103
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
__STRLITPACK_19	DB	67
	DB	111
	DB	110
	DB	103
	DB	101
	DB	115
	DB	116
	DB	105
	DB	111
	DB	110
	DB	80
	DB	114
	DB	105
	DB	99
	DB	105
	DB	110
	DB	103
	DB	67
	DB	111
	DB	110
	DB	102
	DB	105
	DB	103
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
__STRLITPACK_18	DB	117
	DB	110
	DB	107
	DB	110
	DB	111
	DB	119
	DB	110
	DB	0
__STRLITPACK_16	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	72
	DB	79
	DB	84
	DB	47
	DB	71
	DB	80
	DB	32
	DB	112
	DB	97
	DB	105
	DB	114
	DB	44
	DB	32
	DB	102
	DB	111
	DB	114
	DB	32
	DB	112
	DB	97
	DB	105
	DB	114
	DB	32
	DB	32
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_53	DB	0
	DB 3 DUP ( 0H)	; pad
_2il0floatpacket.14	DD	042700000H
_2il0floatpacket.15	DD	042c80000H
_2il0floatpacket.16	DD	03dcccccdH
_2il0floatpacket.17	DD	03f800000H
_2il0floatpacket.21	DD	042c80000H
_2il0floatpacket.23	DD	03dcccccdH
_2il0floatpacket.24	DD	042c80000H
__STRLITPACK_11	DB	84
	DB	111
	DB	108
	DB	108
	DB	95
	DB	67
	DB	111
	DB	110
	DB	103
	DB	80
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_10	DB	117
	DB	110
	DB	107
	DB	110
	DB	111
	DB	119
	DB	110
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
EXTRN	_DYNUST_MAIN_MODULE_mp_NTOLLLINKTMP:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TRAVELTIMEWEIGHT:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TOLLVOTH:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TOLLLINK:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NTOLLLINK:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_SIMPERAGG:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TRAVELTIME:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TOLLVOTT:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TOLLVOTA:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_AGGINT:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM:BYTE
_DATA	ENDS
EXTRN	_for_dealloc_allocatable:PROC
EXTRN	_for_close:PROC
EXTRN	_for_stop_core:PROC
EXTRN	_for_write_seq_fmt_xmit:PROC
EXTRN	_for_write_seq_fmt:PROC
EXTRN	_for_write_seq_lis_xmit:PROC
EXTRN	_for_write_seq_lis:PROC
EXTRN	_for_rewind:PROC
EXTRN	_for_allocate:PROC
EXTRN	_for_read_seq_lis_xmit:PROC
EXTRN	_for_alloc_allocatable:PROC
EXTRN	_for_check_mult_overflow:PROC
EXTRN	_for_read_seq_lis:PROC
EXTRN	_for_open:PROC
EXTRN	_for_inquire:PROC
EXTRN	_DYNUST_NETWORK_MODULE_mp_GETFLINKFROMNODE:PROC
EXTRN	___libm_sse2_powf:PROC
EXTRN	__alloca_probe:PROC
EXTRN	__fltused:BYTE
	INCLUDELIB <ifconsol>
	INCLUDELIB <libifcoremt>
	INCLUDELIB <libifport>
	INCLUDELIB <libmmt>
	INCLUDELIB <LIBCMT>
	INCLUDELIB <libirc>
	INCLUDELIB <svml_dispmt>
	INCLUDELIB <OLDNAMES>
	END
