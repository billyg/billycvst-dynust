; mark_description "Intel(R) Visual Fortran Compiler XE for applications running on IA-32, Version 13.1.1.171 Build 20130313";
; mark_description "-S";
	.686P
 	.387
	OPTION DOTNAME
	ASSUME	CS:FLAT,DS:FLAT,SS:FLAT
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
TXTST0:
; -- Begin  _WRITE_VEHICLES
; mark_begin;
IF @Version GE 800
  .MMX
ELSEIF @Version GE 612
  .MMX
  MMWORD TEXTEQU <QWORD>
ENDIF
IF @Version GE 800
  .XMM
ELSEIF @Version GE 614
  .XMM
  XMMWORD TEXTEQU <OWORD>
ENDIF
       ALIGN     16
	PUBLIC _WRITE_VEHICLES
_WRITE_VEHICLES	PROC NEAR 
; parameter 1: 8 + ebp
; parameter 2: 12 + ebp
; parameter 3: 16 + ebp
; parameter 4: 20 + ebp
; parameter 5: 24 + ebp
; parameter 6: 28 + ebp
.B1.1:                          ; Preds .B1.0
        push      ebp                                           ;1.16
        mov       ebp, esp                                      ;1.16
        and       esp, -16                                      ;1.16
        push      esi                                           ;1.16
        push      edi                                           ;1.16
        push      ebx                                           ;1.16
        sub       esp, 340                                      ;1.16
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_FUELOUT], 0 ;39.20
        jle       .B1.21        ; Prob 16%                      ;39.20
                                ; LOE
.B1.2:                          ; Preds .B1.1
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ITERATION] ;39.20
        cmp       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ITEMAX] ;39.39
        jne       .B1.20        ; Prob 50%                      ;39.39
                                ; LOE
.B1.3:                          ; Preds .B1.20 .B1.2
        mov       edi, DWORD PTR [16+ebp]                       ;41.26
        lea       edx, DWORD PTR [32+esp]                       ;41.11
        mov       ebx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE+32] ;41.11
        shl       ebx, 6                                        ;41.11
        mov       edi, DWORD PTR [edi]                          ;41.26
        neg       ebx                                           ;41.11
        mov       DWORD PTR [28+esp], edi                       ;41.26
        shl       edi, 6                                        ;41.11
        add       ebx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE] ;41.11
        mov       DWORD PTR [256+esp], 0                        ;41.11
        mov       ecx, DWORD PTR [20+ebp]                       ;41.11
        mov       eax, DWORD PTR [4+edi+ebx]                    ;41.11
        lea       ebx, DWORD PTR [256+esp]                      ;41.11
        mov       DWORD PTR [32+esp], eax                       ;41.11
        push      32                                            ;41.11
        push      OFFSET FLAT: WRITE_VEHICLES$format_pack.0.1+72 ;41.11
        push      edx                                           ;41.11
        push      OFFSET FLAT: __STRLITPACK_0.0.1               ;41.11
        push      -2088435968                                   ;41.11
        push      DWORD PTR [ecx]                               ;41.11
        push      ebx                                           ;41.11
        call      _for_write_seq_fmt                            ;41.11
                                ; LOE ebx edi
.B1.4:                          ; Preds .B1.3
        mov       DWORD PTR [52+esp], edi                       ;
        mov       edi, DWORD PTR [56+esp]                       ;41.11
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;41.11
        shl       edi, 5                                        ;41.11
        shl       eax, 5                                        ;41.11
        mov       esi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;41.11
        neg       eax                                           ;41.11
        add       esi, edi                                      ;41.11
        mov       DWORD PTR [28+esp], edi                       ;41.11
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;41.11
        neg       edi                                           ;41.11
        add       edi, DWORD PTR [28+eax+esi]                   ;41.11
        imul      edi, edi, 152                                 ;41.11
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;41.11
        mov       DWORD PTR [48+esp], edx                       ;41.11
        mov       DWORD PTR [32+esp], eax                       ;41.11
        imul      edx, DWORD PTR [28+edx+edi], 44               ;41.50
        imul      eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE+32], 44 ;41.11
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE] ;41.11
        add       edx, ecx                                      ;41.11
        sub       edx, eax                                      ;41.11
        mov       DWORD PTR [40+esp], ecx                       ;41.11
        mov       DWORD PTR [36+esp], eax                       ;41.11
        lea       eax, DWORD PTR [68+esp]                       ;41.11
        mov       DWORD PTR [44+esp], edi                       ;41.11
        mov       ecx, DWORD PTR [36+edx]                       ;41.11
        mov       DWORD PTR [68+esp], ecx                       ;41.11
        push      eax                                           ;41.11
        push      OFFSET FLAT: __STRLITPACK_1.0.1               ;41.11
        push      ebx                                           ;41.11
        mov       edi, DWORD PTR [64+esp]                       ;41.11
        call      _for_write_seq_fmt_xmit                       ;41.11
                                ; LOE ebx esi edi
.B1.5:                          ; Preds .B1.4
        mov       edx, DWORD PTR [56+esp]                       ;41.153
        mov       eax, DWORD PTR [60+esp]                       ;41.153
        imul      ecx, DWORD PTR [24+eax+edx], 44               ;41.153
        mov       eax, DWORD PTR [52+esp]                       ;41.11
        add       eax, ecx                                      ;41.11
        lea       ecx, DWORD PTR [64+esp]                       ;41.11
        sub       eax, DWORD PTR [48+esp]                       ;41.11
        mov       edx, DWORD PTR [36+eax]                       ;41.11
        mov       DWORD PTR [64+esp], edx                       ;41.11
        push      ecx                                           ;41.11
        push      OFFSET FLAT: __STRLITPACK_2.0.1               ;41.11
        push      ebx                                           ;41.11
        call      _for_write_seq_fmt_xmit                       ;41.11
                                ; LOE ebx esi edi
.B1.6:                          ; Preds .B1.5
        mov       eax, DWORD PTR [56+esp]                       ;41.11
        lea       ecx, DWORD PTR [68+esp]                       ;41.11
        mov       edx, DWORD PTR [12+eax+esi]                   ;41.11
        mov       DWORD PTR [68+esp], edx                       ;41.11
        push      ecx                                           ;41.11
        push      OFFSET FLAT: __STRLITPACK_3.0.1               ;41.11
        push      ebx                                           ;41.11
        call      _for_write_seq_fmt_xmit                       ;41.11
                                ; LOE ebx esi edi
.B1.7:                          ; Preds .B1.6
        mov       eax, DWORD PTR [68+esp]                       ;41.11
        lea       ecx, DWORD PTR [112+esp]                      ;41.11
        movzx     edx, BYTE PTR [4+eax+esi]                     ;41.11
        mov       BYTE PTR [112+esp], dl                        ;41.11
        push      ecx                                           ;41.11
        push      OFFSET FLAT: __STRLITPACK_4.0.1               ;41.11
        push      ebx                                           ;41.11
        call      _for_write_seq_fmt_xmit                       ;41.11
                                ; LOE ebx esi edi
.B1.8:                          ; Preds .B1.7
        mov       eax, DWORD PTR [80+esp]                       ;41.11
        lea       ecx, DWORD PTR [132+esp]                      ;41.11
        movzx     edx, BYTE PTR [5+eax+esi]                     ;41.11
        mov       BYTE PTR [132+esp], dl                        ;41.11
        push      ecx                                           ;41.11
        push      OFFSET FLAT: __STRLITPACK_5.0.1               ;41.11
        push      ebx                                           ;41.11
        call      _for_write_seq_fmt_xmit                       ;41.11
                                ; LOE ebx edi
.B1.9:                          ; Preds .B1.8
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;41.11
        lea       ecx, DWORD PTR [152+esp]                      ;41.11
        shl       eax, 8                                        ;41.11
        mov       esi, DWORD PTR [116+esp]                      ;41.11
        neg       eax                                           ;41.11
        shl       esi, 8                                        ;41.11
        add       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;41.11
        movzx     edx, BYTE PTR [236+esi+eax]                   ;41.11
        mov       BYTE PTR [152+esp], dl                        ;41.11
        push      ecx                                           ;41.11
        push      OFFSET FLAT: __STRLITPACK_6.0.1               ;41.11
        push      ebx                                           ;41.11
        call      _for_write_seq_fmt_xmit                       ;41.11
                                ; LOE ebx esi edi
.B1.10:                         ; Preds .B1.9
        push      DWORD PTR [16+ebp]                            ;41.372
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_P_SIZE  ;41.372
                                ; LOE eax ebx esi edi
.B1.11:                         ; Preds .B1.10
        mov       DWORD PTR [176+esp], eax                      ;41.11
        lea       eax, DWORD PTR [176+esp]                      ;41.11
        push      eax                                           ;41.11
        push      OFFSET FLAT: __STRLITPACK_7.0.1               ;41.11
        push      ebx                                           ;41.11
        call      _for_write_seq_fmt_xmit                       ;41.11
                                ; LOE ebx esi edi
.B1.67:                         ; Preds .B1.11
        add       esp, 116                                      ;41.11
                                ; LOE ebx esi edi
.B1.12:                         ; Preds .B1.67
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;41.11
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;41.11
        add       esi, eax                                      ;41.11
        mov       DWORD PTR [12+esp], edx                       ;41.11
        shl       edx, 8                                        ;41.11
        sub       esi, edx                                      ;41.11
        mov       DWORD PTR [20+esp], eax                       ;41.11
        lea       eax, DWORD PTR [80+esp]                       ;41.11
        movzx     ecx, BYTE PTR [160+esi]                       ;41.11
        mov       BYTE PTR [80+esp], cl                         ;41.11
        push      eax                                           ;41.11
        push      OFFSET FLAT: __STRLITPACK_8.0.1               ;41.11
        push      ebx                                           ;41.11
        call      _for_write_seq_fmt_xmit                       ;41.11
                                ; LOE ebx esi edi
.B1.13:                         ; Preds .B1.12
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE+32] ;41.11
        lea       ecx, DWORD PTR [100+esp]                      ;41.11
        shl       eax, 6                                        ;41.11
        neg       eax                                           ;41.11
        add       edi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE] ;41.11
        mov       DWORD PTR [20+esp], eax                       ;41.11
        movzx     edx, BYTE PTR [56+eax+edi]                    ;41.11
        mov       BYTE PTR [100+esp], dl                        ;41.11
        push      ecx                                           ;41.11
        push      OFFSET FLAT: __STRLITPACK_9.0.1               ;41.11
        push      ebx                                           ;41.11
        call      _for_write_seq_fmt_xmit                       ;41.11
                                ; LOE ebx esi edi
.B1.14:                         ; Preds .B1.13
        mov       eax, DWORD PTR [32+esp]                       ;41.11
        lea       ecx, DWORD PTR [120+esp]                      ;41.11
        mov       edx, DWORD PTR [60+eax+edi]                   ;41.11
        mov       DWORD PTR [120+esp], edx                      ;41.11
        push      ecx                                           ;41.11
        push      OFFSET FLAT: __STRLITPACK_10.0.1              ;41.11
        push      ebx                                           ;41.11
        call      _for_write_seq_fmt_xmit                       ;41.11
                                ; LOE ebx esi edi
.B1.15:                         ; Preds .B1.14
        mov       eax, DWORD PTR [44+esp]                       ;41.11
        lea       ecx, DWORD PTR [140+esp]                      ;41.11
        mov       edx, DWORD PTR [52+eax+edi]                   ;41.11
        mov       DWORD PTR [140+esp], edx                      ;41.11
        push      ecx                                           ;41.11
        push      OFFSET FLAT: __STRLITPACK_11.0.1              ;41.11
        push      ebx                                           ;41.11
        call      _for_write_seq_fmt_xmit                       ;41.11
                                ; LOE ebx esi edi
.B1.16:                         ; Preds .B1.15
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;41.11
        shl       eax, 5                                        ;41.11
        mov       edx, DWORD PTR [48+esp]                       ;41.11
        neg       eax                                           ;41.11
        add       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;41.11
        mov       DWORD PTR [52+esp], eax                       ;41.11
        mov       DWORD PTR [48+esp], edx                       ;41.11
        movzx     ecx, WORD PTR [22+eax+edx]                    ;41.11
        lea       eax, DWORD PTR [160+esp]                      ;41.11
        mov       WORD PTR [160+esp], cx                        ;41.11
        push      eax                                           ;41.11
        push      OFFSET FLAT: __STRLITPACK_12.0.1              ;41.11
        push      ebx                                           ;41.11
        call      _for_write_seq_fmt_xmit                       ;41.11
                                ; LOE ebx esi edi
.B1.17:                         ; Preds .B1.16
        movzx     eax, BYTE PTR [24+esi]                        ;41.11
        lea       edx, DWORD PTR [180+esp]                      ;41.11
        mov       BYTE PTR [180+esp], al                        ;41.11
        push      edx                                           ;41.11
        push      OFFSET FLAT: __STRLITPACK_13.0.1              ;41.11
        push      ebx                                           ;41.11
        call      _for_write_seq_fmt_xmit                       ;41.11
                                ; LOE ebx edi
.B1.18:                         ; Preds .B1.17
        mov       edx, DWORD PTR [76+esp]                       ;41.11
        mov       ecx, DWORD PTR [72+esp]                       ;41.11
        mov       eax, DWORD PTR [80+esp]                       ;4194305.557
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;4194305.557
        neg       esi                                           ;41.11
        add       esi, DWORD PTR [28+edx+ecx]                   ;41.11
        lea       edx, DWORD PTR [200+esp]                      ;41.11
        movss     xmm0, DWORD PTR [8+eax+edi]                   ;4194305.557
        imul      eax, esi, 152                                 ;41.11
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;4194305.557
        divss     xmm0, DWORD PTR [20+edi+eax]                  ;41.11
        movss     DWORD PTR [200+esp], xmm0                     ;41.11
        push      edx                                           ;41.11
        push      OFFSET FLAT: __STRLITPACK_14.0.1              ;41.11
        push      ebx                                           ;41.11
        call      _for_write_seq_fmt_xmit                       ;41.11
                                ; LOE ebx
.B1.19:                         ; Preds .B1.18
        mov       edx, DWORD PTR [_DYNUST_FUEL_MODULE_mp_VEHFUEL+32] ;41.11
        neg       edx                                           ;41.11
        add       edx, DWORD PTR [112+esp]                      ;41.11
        mov       ecx, DWORD PTR [_DYNUST_FUEL_MODULE_mp_VEHFUEL] ;41.11
        lea       eax, DWORD PTR [edx*8]                        ;41.11
        lea       esi, DWORD PTR [eax+edx*4]                    ;41.11
        mov       edi, DWORD PTR [ecx+esi]                      ;41.11
        lea       eax, DWORD PTR [92+esp]                       ;41.11
        mov       DWORD PTR [92+esp], edi                       ;41.11
        push      eax                                           ;41.11
        push      OFFSET FLAT: __STRLITPACK_15.0.1              ;41.11
        push      ebx                                           ;41.11
        call      _for_write_seq_fmt_xmit                       ;41.11
                                ; LOE ebx
.B1.68:                         ; Preds .B1.19
        add       esp, 96                                       ;41.11
        jmp       .B1.37        ; Prob 100%                     ;41.11
                                ; LOE ebx
.B1.20:                         ; Preds .B1.2
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_REACH_CONVERG], -1 ;39.66
        je        .B1.3         ; Prob 16%                      ;39.66
                                ; LOE
.B1.21:                         ; Preds .B1.1 .B1.20
        mov       esi, DWORD PTR [16+ebp]                       ;44.26
        lea       edx, DWORD PTR [136+esp]                      ;44.11
        mov       ebx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE+32] ;44.11
        shl       ebx, 6                                        ;44.11
        mov       esi, DWORD PTR [esi]                          ;44.26
        neg       ebx                                           ;44.11
        mov       DWORD PTR [28+esp], esi                       ;44.26
        shl       esi, 6                                        ;44.11
        add       ebx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE] ;44.11
        mov       DWORD PTR [256+esp], 0                        ;44.11
        mov       ecx, DWORD PTR [20+ebp]                       ;44.11
        mov       eax, DWORD PTR [4+esi+ebx]                    ;44.11
        lea       ebx, DWORD PTR [256+esp]                      ;44.11
        mov       DWORD PTR [136+esp], eax                      ;44.11
        push      32                                            ;44.11
        push      OFFSET FLAT: WRITE_VEHICLES$format_pack.0.1+72 ;44.11
        push      edx                                           ;44.11
        push      OFFSET FLAT: __STRLITPACK_16.0.1              ;44.11
        push      -2088435968                                   ;44.11
        push      DWORD PTR [ecx]                               ;44.11
        push      ebx                                           ;44.11
        call      _for_write_seq_fmt                            ;44.11
                                ; LOE ebx esi
.B1.22:                         ; Preds .B1.21
        mov       edi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;44.11
        mov       ecx, DWORD PTR [56+esp]                       ;44.11
        shl       ecx, 5                                        ;44.11
        shl       edi, 5                                        ;44.11
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;44.11
        neg       edi                                           ;44.11
        add       eax, ecx                                      ;44.11
        mov       DWORD PTR [28+esp], ecx                       ;44.11
        mov       DWORD PTR [32+esp], edi                       ;44.11
        mov       DWORD PTR [52+esp], eax                       ;44.11
        mov       ecx, DWORD PTR [28+edi+eax]                   ;44.11
        sub       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;44.11
        imul      ecx, ecx, 152                                 ;44.11
        imul      eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE+32], 44 ;44.11
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;44.50
        mov       DWORD PTR [48+esp], edi                       ;44.50
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE] ;44.11
        imul      edi, DWORD PTR [28+edi+ecx], 44               ;44.50
        add       edi, edx                                      ;44.11
        sub       edi, eax                                      ;44.11
        mov       DWORD PTR [40+esp], edx                       ;44.11
        mov       DWORD PTR [36+esp], eax                       ;44.11
        lea       eax, DWORD PTR [172+esp]                      ;44.11
        mov       DWORD PTR [44+esp], ecx                       ;44.11
        mov       edx, DWORD PTR [36+edi]                       ;44.11
        mov       DWORD PTR [172+esp], edx                      ;44.11
        push      eax                                           ;44.11
        push      OFFSET FLAT: __STRLITPACK_17.0.1              ;44.11
        push      ebx                                           ;44.11
        mov       edi, DWORD PTR [64+esp]                       ;44.11
        call      _for_write_seq_fmt_xmit                       ;44.11
                                ; LOE ebx esi edi
.B1.23:                         ; Preds .B1.22
        mov       edx, DWORD PTR [56+esp]                       ;44.153
        mov       eax, DWORD PTR [60+esp]                       ;44.153
        imul      ecx, DWORD PTR [24+eax+edx], 44               ;44.153
        mov       eax, DWORD PTR [52+esp]                       ;44.11
        add       eax, ecx                                      ;44.11
        lea       ecx, DWORD PTR [192+esp]                      ;44.11
        sub       eax, DWORD PTR [48+esp]                       ;44.11
        mov       edx, DWORD PTR [36+eax]                       ;44.11
        mov       DWORD PTR [192+esp], edx                      ;44.11
        push      ecx                                           ;44.11
        push      OFFSET FLAT: __STRLITPACK_18.0.1              ;44.11
        push      ebx                                           ;44.11
        call      _for_write_seq_fmt_xmit                       ;44.11
                                ; LOE ebx esi edi
.B1.24:                         ; Preds .B1.23
        mov       eax, DWORD PTR [56+esp]                       ;44.11
        lea       ecx, DWORD PTR [212+esp]                      ;44.11
        mov       edx, DWORD PTR [12+eax+edi]                   ;44.11
        mov       DWORD PTR [212+esp], edx                      ;44.11
        push      ecx                                           ;44.11
        push      OFFSET FLAT: __STRLITPACK_19.0.1              ;44.11
        push      ebx                                           ;44.11
        call      _for_write_seq_fmt_xmit                       ;44.11
                                ; LOE ebx esi edi
.B1.25:                         ; Preds .B1.24
        mov       eax, DWORD PTR [68+esp]                       ;44.11
        lea       ecx, DWORD PTR [232+esp]                      ;44.11
        movzx     edx, BYTE PTR [4+eax+edi]                     ;44.11
        mov       BYTE PTR [232+esp], dl                        ;44.11
        push      ecx                                           ;44.11
        push      OFFSET FLAT: __STRLITPACK_20.0.1              ;44.11
        push      ebx                                           ;44.11
        call      _for_write_seq_fmt_xmit                       ;44.11
                                ; LOE ebx esi edi
.B1.26:                         ; Preds .B1.25
        mov       eax, DWORD PTR [80+esp]                       ;44.11
        lea       ecx, DWORD PTR [252+esp]                      ;44.11
        movzx     edx, BYTE PTR [5+eax+edi]                     ;44.11
        mov       BYTE PTR [252+esp], dl                        ;44.11
        push      ecx                                           ;44.11
        push      OFFSET FLAT: __STRLITPACK_21.0.1              ;44.11
        push      ebx                                           ;44.11
        call      _for_write_seq_fmt_xmit                       ;44.11
                                ; LOE ebx esi
.B1.27:                         ; Preds .B1.26
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;44.11
        lea       ecx, DWORD PTR [272+esp]                      ;44.11
        shl       eax, 8                                        ;44.11
        mov       edi, DWORD PTR [116+esp]                      ;44.11
        neg       eax                                           ;44.11
        shl       edi, 8                                        ;44.11
        add       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;44.11
        movzx     edx, BYTE PTR [236+edi+eax]                   ;44.11
        mov       BYTE PTR [272+esp], dl                        ;44.11
        push      ecx                                           ;44.11
        push      OFFSET FLAT: __STRLITPACK_22.0.1              ;44.11
        push      ebx                                           ;44.11
        call      _for_write_seq_fmt_xmit                       ;44.11
                                ; LOE ebx esi edi
.B1.28:                         ; Preds .B1.27
        push      DWORD PTR [16+ebp]                            ;44.372
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_P_SIZE  ;44.372
                                ; LOE eax ebx esi edi
.B1.29:                         ; Preds .B1.28
        mov       DWORD PTR [296+esp], eax                      ;44.11
        lea       eax, DWORD PTR [296+esp]                      ;44.11
        push      eax                                           ;44.11
        push      OFFSET FLAT: __STRLITPACK_23.0.1              ;44.11
        push      ebx                                           ;44.11
        call      _for_write_seq_fmt_xmit                       ;44.11
                                ; LOE ebx esi edi
.B1.70:                         ; Preds .B1.29
        add       esp, 116                                      ;44.11
                                ; LOE ebx esi edi
.B1.30:                         ; Preds .B1.70
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;44.11
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;44.11
        add       edi, eax                                      ;44.11
        mov       DWORD PTR [12+esp], edx                       ;44.11
        shl       edx, 8                                        ;44.11
        sub       edi, edx                                      ;44.11
        mov       DWORD PTR [20+esp], eax                       ;44.11
        lea       eax, DWORD PTR [200+esp]                      ;44.11
        movzx     ecx, BYTE PTR [160+edi]                       ;44.11
        mov       BYTE PTR [200+esp], cl                        ;44.11
        push      eax                                           ;44.11
        push      OFFSET FLAT: __STRLITPACK_24.0.1              ;44.11
        push      ebx                                           ;44.11
        call      _for_write_seq_fmt_xmit                       ;44.11
                                ; LOE ebx esi edi
.B1.31:                         ; Preds .B1.30
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE+32] ;44.11
        lea       ecx, DWORD PTR [220+esp]                      ;44.11
        shl       eax, 6                                        ;44.11
        neg       eax                                           ;44.11
        add       esi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE] ;44.11
        mov       DWORD PTR [20+esp], eax                       ;44.11
        movzx     edx, BYTE PTR [56+eax+esi]                    ;44.11
        mov       BYTE PTR [220+esp], dl                        ;44.11
        push      ecx                                           ;44.11
        push      OFFSET FLAT: __STRLITPACK_25.0.1              ;44.11
        push      ebx                                           ;44.11
        call      _for_write_seq_fmt_xmit                       ;44.11
                                ; LOE ebx esi edi
.B1.32:                         ; Preds .B1.31
        mov       eax, DWORD PTR [32+esp]                       ;44.11
        lea       ecx, DWORD PTR [240+esp]                      ;44.11
        mov       edx, DWORD PTR [60+eax+esi]                   ;44.11
        mov       DWORD PTR [240+esp], edx                      ;44.11
        push      ecx                                           ;44.11
        push      OFFSET FLAT: __STRLITPACK_26.0.1              ;44.11
        push      ebx                                           ;44.11
        call      _for_write_seq_fmt_xmit                       ;44.11
                                ; LOE ebx esi edi
.B1.33:                         ; Preds .B1.32
        mov       eax, DWORD PTR [44+esp]                       ;44.11
        lea       ecx, DWORD PTR [260+esp]                      ;44.11
        mov       edx, DWORD PTR [52+eax+esi]                   ;44.11
        mov       DWORD PTR [260+esp], edx                      ;44.11
        push      ecx                                           ;44.11
        push      OFFSET FLAT: __STRLITPACK_27.0.1              ;44.11
        push      ebx                                           ;44.11
        call      _for_write_seq_fmt_xmit                       ;44.11
                                ; LOE ebx esi edi
.B1.34:                         ; Preds .B1.33
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;44.11
        shl       eax, 5                                        ;44.11
        mov       edx, DWORD PTR [48+esp]                       ;44.11
        neg       eax                                           ;44.11
        add       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;44.11
        mov       DWORD PTR [52+esp], eax                       ;44.11
        mov       DWORD PTR [48+esp], edx                       ;44.11
        movzx     ecx, WORD PTR [22+eax+edx]                    ;44.11
        lea       eax, DWORD PTR [280+esp]                      ;44.11
        mov       WORD PTR [280+esp], cx                        ;44.11
        push      eax                                           ;44.11
        push      OFFSET FLAT: __STRLITPACK_28.0.1              ;44.11
        push      ebx                                           ;44.11
        call      _for_write_seq_fmt_xmit                       ;44.11
                                ; LOE ebx esi edi
.B1.35:                         ; Preds .B1.34
        movzx     eax, BYTE PTR [24+edi]                        ;44.11
        lea       edx, DWORD PTR [300+esp]                      ;44.11
        mov       BYTE PTR [300+esp], al                        ;44.11
        push      edx                                           ;44.11
        push      OFFSET FLAT: __STRLITPACK_29.0.1              ;44.11
        push      ebx                                           ;44.11
        call      _for_write_seq_fmt_xmit                       ;44.11
                                ; LOE ebx esi
.B1.36:                         ; Preds .B1.35
        mov       eax, DWORD PTR [80+esp]                       ;4194305.557
        mov       edx, DWORD PTR [76+esp]                       ;44.11
        mov       ecx, DWORD PTR [72+esp]                       ;44.11
        movss     xmm0, DWORD PTR [8+eax+esi]                   ;4194305.557
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;4194305.557
        neg       esi                                           ;44.11
        add       esi, DWORD PTR [28+edx+ecx]                   ;44.11
        lea       edx, DWORD PTR [320+esp]                      ;44.11
        imul      eax, esi, 152                                 ;44.11
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;4194305.557
        divss     xmm0, DWORD PTR [20+edi+eax]                  ;44.11
        movss     DWORD PTR [320+esp], xmm0                     ;44.11
        push      edx                                           ;44.11
        push      OFFSET FLAT: __STRLITPACK_30.0.1              ;44.11
        push      ebx                                           ;44.11
        call      _for_write_seq_fmt_xmit                       ;44.11
                                ; LOE ebx
.B1.71:                         ; Preds .B1.36
        add       esp, 84                                       ;44.11
                                ; LOE ebx
.B1.37:                         ; Preds .B1.68 .B1.71
        mov       eax, DWORD PTR [28+esp]                       ;46.7
        mov       edx, DWORD PTR [12+esp]                       ;46.7
        shl       eax, 8                                        ;46.7
        shl       edx, 8                                        ;46.7
        mov       ecx, DWORD PTR [20+esp]                       ;46.7
        neg       edx                                           ;46.7
        add       ecx, eax                                      ;46.7
        add       ecx, edx                                      ;46.7
        movsx     ecx, BYTE PTR [160+ecx]                       ;46.17
        test      ecx, ecx                                      ;46.7
        jle       .B1.48        ; Prob 2%                       ;46.7
                                ; LOE eax ecx ebx
.B1.38:                         ; Preds .B1.37
        mov       DWORD PTR [12+esp], ecx                       ;49.143
        mov       edi, 1                                        ;
        mov       DWORD PTR [esp], eax                          ;49.143
        mov       esi, DWORD PTR [16+ebp]                       ;49.143
                                ; LOE ebx esi edi
.B1.39:                         ; Preds .B1.46 .B1.38
        push      esi                                           ;48.11
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_P_SIZE  ;48.11
                                ; LOE eax ebx esi edi
.B1.40:                         ; Preds .B1.39
        dec       eax                                           ;48.14
        mov       DWORD PTR [48+esp], eax                       ;48.14
        push      OFFSET FLAT: __NLITPACK_0.0.1                 ;48.11
        lea       eax, DWORD PTR [52+esp]                       ;48.11
        push      eax                                           ;48.11
        push      esi                                           ;48.11
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_VALUE   ;48.11
                                ; LOE ebx esi edi f1
.B1.73:                         ; Preds .B1.40
        fstp      DWORD PTR [328+esp]                           ;48.11
        movss     xmm3, DWORD PTR [328+esp]                     ;48.11
        add       esp, 16                                       ;48.11
                                ; LOE ebx esi edi xmm3
.B1.41:                         ; Preds .B1.73
        movss     xmm0, DWORD PTR [_2il0floatpacket.7]          ;48.11
        andps     xmm0, xmm3                                    ;48.11
        pxor      xmm3, xmm0                                    ;48.11
        movss     xmm1, DWORD PTR [_2il0floatpacket.8]          ;48.11
        movaps    xmm2, xmm3                                    ;48.11
        movaps    xmm7, xmm3                                    ;48.11
        cmpltss   xmm2, xmm1                                    ;48.11
        andps     xmm1, xmm2                                    ;48.11
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE] ;48.11
        addss     xmm7, xmm1                                    ;48.11
        subss     xmm7, xmm1                                    ;48.11
        movaps    xmm6, xmm7                                    ;48.11
        subss     xmm6, xmm3                                    ;48.11
        movss     xmm3, DWORD PTR [_2il0floatpacket.9]          ;48.11
        movaps    xmm5, xmm3                                    ;48.11
        movaps    xmm4, xmm6                                    ;48.11
        addss     xmm5, xmm3                                    ;48.11
        cmpnless  xmm4, xmm3                                    ;48.11
        cmpless   xmm6, DWORD PTR [_2il0floatpacket.10]         ;48.11
        andps     xmm4, xmm5                                    ;48.11
        andps     xmm6, xmm5                                    ;48.11
        subss     xmm7, xmm4                                    ;48.11
        addss     xmm7, xmm6                                    ;48.11
        orps      xmm7, xmm0                                    ;48.11
        cvtss2si  eax, xmm7                                     ;48.14
        sub       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE+32] ;48.94
        imul      ecx, eax, 84                                  ;48.94
        mov       eax, DWORD PTR [72+edx+ecx]                   ;48.14
        add       eax, eax                                      ;48.94
        neg       eax                                           ;48.94
        add       eax, DWORD PTR [40+edx+ecx]                   ;48.94
        movsx     edx, WORD PTR [4+eax]                         ;48.14
        test      edx, edx                                      ;48.94
        jle       .B1.60        ; Prob 16%                      ;48.94
                                ; LOE ebx esi edi
.B1.42:                         ; Preds .B1.41
        push      esi                                           ;49.13
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_P_SIZE  ;49.13
                                ; LOE eax ebx esi edi
.B1.43:                         ; Preds .B1.42
        dec       eax                                           ;49.28
        mov       DWORD PTR [40+esp], eax                       ;49.28
        lea       eax, DWORD PTR [40+esp]                       ;49.13
        push      OFFSET FLAT: __NLITPACK_0.0.1                 ;49.13
        push      eax                                           ;49.13
        push      esi                                           ;49.13
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_VALUE   ;49.13
                                ; LOE ebx esi edi f1
.B1.75:                         ; Preds .B1.43
        fstp      DWORD PTR [328+esp]                           ;49.13
        movss     xmm3, DWORD PTR [328+esp]                     ;49.13
                                ; LOE ebx esi edi xmm3
.B1.44:                         ; Preds .B1.75
        movss     xmm0, DWORD PTR [_2il0floatpacket.7]          ;49.13
        andps     xmm0, xmm3                                    ;49.13
        pxor      xmm3, xmm0                                    ;49.13
        movss     xmm1, DWORD PTR [_2il0floatpacket.8]          ;49.13
        movaps    xmm2, xmm3                                    ;49.13
        movaps    xmm7, xmm3                                    ;49.13
        cmpltss   xmm2, xmm1                                    ;49.13
        andps     xmm1, xmm2                                    ;49.13
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE] ;49.13
        addss     xmm7, xmm1                                    ;49.13
        mov       DWORD PTR [272+esp], 0                        ;49.13
        subss     xmm7, xmm1                                    ;49.13
        movaps    xmm6, xmm7                                    ;49.13
        subss     xmm6, xmm3                                    ;49.13
        movss     xmm3, DWORD PTR [_2il0floatpacket.9]          ;49.13
        movaps    xmm5, xmm3                                    ;49.13
        movaps    xmm4, xmm6                                    ;49.13
        addss     xmm5, xmm3                                    ;49.13
        cmpnless  xmm4, xmm3                                    ;49.13
        cmpless   xmm6, DWORD PTR [_2il0floatpacket.10]         ;49.13
        andps     xmm4, xmm5                                    ;49.13
        andps     xmm6, xmm5                                    ;49.13
        subss     xmm7, xmm4                                    ;49.13
        addss     xmm7, xmm6                                    ;49.13
        orps      xmm7, xmm0                                    ;49.13
        cvtss2si  eax, xmm7                                     ;49.28
        sub       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE+32] ;49.13
        imul      ecx, eax, 84                                  ;49.13
        mov       eax, DWORD PTR [72+edx+ecx]                   ;49.28
        add       eax, eax                                      ;49.13
        neg       eax                                           ;49.13
        add       eax, DWORD PTR [40+edx+ecx]                   ;49.13
        lea       ecx, DWORD PTR [336+esp]                      ;49.13
        movzx     edx, WORD PTR [4+eax]                         ;49.13
        mov       WORD PTR [336+esp], dx                        ;49.13
        push      32                                            ;49.13
        push      OFFSET FLAT: WRITE_VEHICLES$format_pack.0.1+40 ;49.13
        push      ecx                                           ;49.13
        mov       eax, DWORD PTR [20+ebp]                       ;49.13
        push      OFFSET FLAT: __STRLITPACK_31.0.1              ;49.13
        push      -2088435968                                   ;49.13
        push      DWORD PTR [eax]                               ;49.13
        push      ebx                                           ;49.13
        call      _for_write_seq_fmt                            ;49.13
                                ; LOE ebx esi edi
.B1.45:                         ; Preds .B1.44
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;49.13
        mov       eax, edi                                      ;49.13
        shl       edx, 8                                        ;49.13
        mov       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;49.13
        neg       edx                                           ;49.13
        add       ecx, DWORD PTR [44+esp]                       ;49.13
        sub       eax, DWORD PTR [232+edx+ecx]                  ;49.13
        mov       edx, DWORD PTR [200+edx+ecx]                  ;49.108
        lea       ecx, DWORD PTR [372+esp]                      ;49.13
        movsx     eax, WORD PTR [edx+eax*2]                     ;49.108
        cvtsi2ss  xmm0, eax                                     ;49.108
        divss     xmm0, DWORD PTR [_2il0floatpacket.6]          ;49.13
        movss     DWORD PTR [372+esp], xmm0                     ;49.13
        push      ecx                                           ;49.13
        push      OFFSET FLAT: __STRLITPACK_32.0.1              ;49.13
        push      ebx                                           ;49.13
        call      _for_write_seq_fmt_xmit                       ;49.13
                                ; LOE ebx esi edi
.B1.76:                         ; Preds .B1.63 .B1.45
        add       esp, 56                                       ;49.13
                                ; LOE ebx esi edi
.B1.46:                         ; Preds .B1.76
        inc       edi                                           ;53.9
        cmp       edi, DWORD PTR [12+esp]                       ;53.9
        jle       .B1.39        ; Prob 82%                      ;53.9
                                ; LOE ebx esi edi
.B1.48:                         ; Preds .B1.46 .B1.37
        mov       eax, DWORD PTR [24+ebp]                       ;1.16
        cmp       DWORD PTR [eax], 1                            ;54.17
        je        .B1.50        ; Prob 16%                      ;54.17
                                ; LOE ebx
.B1.49:                         ; Preds .B1.48
        add       esp, 340                                      ;62.5
        pop       ebx                                           ;62.5
        pop       edi                                           ;62.5
        pop       esi                                           ;62.5
        mov       esp, ebp                                      ;62.5
        pop       ebp                                           ;62.5
        ret                                                     ;62.5
                                ; LOE
.B1.50:                         ; Preds .B1.48                  ; Infreq
        mov       esi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;54.23
        neg       esi                                           ;54.23
        add       esi, DWORD PTR [28+esp]                       ;54.23
        shl       esi, 5                                        ;54.23
        mov       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;54.23
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;54.23
        neg       edi                                           ;54.23
        add       edi, DWORD PTR [28+ecx+esi]                   ;54.23
        imul      esi, edi, 152                                 ;54.23
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;54.23
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE+32] ;54.23
        neg       edx                                           ;54.23
        add       edx, DWORD PTR [28+ecx+esi]                   ;54.23
        lea       ecx, DWORD PTR [esp]                          ;54.23
        imul      edi, edx, 44                                  ;54.23
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE] ;54.23
        mov       DWORD PTR [256+esp], 0                        ;54.23
        mov       eax, DWORD PTR [28+ebp]                       ;1.16
        mov       edx, DWORD PTR [36+edx+edi]                   ;54.23
        mov       DWORD PTR [esp], edx                          ;54.23
        push      32                                            ;54.23
        push      OFFSET FLAT: WRITE_VEHICLES$format_pack.0.1+20 ;54.23
        push      ecx                                           ;54.23
        push      OFFSET FLAT: __STRLITPACK_35.0.1              ;54.23
        push      -2088435968                                   ;54.23
        push      DWORD PTR [eax]                               ;54.23
        push      ebx                                           ;54.23
        call      _for_write_seq_fmt                            ;54.23
                                ; LOE ebx
.B1.51:                         ; Preds .B1.50                  ; Infreq
        push      0                                             ;54.23
        push      OFFSET FLAT: __STRLITPACK_36.0.1              ;54.23
        push      ebx                                           ;54.23
        call      _for_write_seq_fmt_xmit                       ;54.23
                                ; LOE ebx
.B1.52:                         ; Preds .B1.51                  ; Infreq
        push      DWORD PTR [16+ebp]                            ;54.217
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_P_SIZE  ;54.217
                                ; LOE eax ebx
.B1.77:                         ; Preds .B1.52                  ; Infreq
        add       esp, 44                                       ;54.217
                                ; LOE eax ebx
.B1.53:                         ; Preds .B1.77                  ; Infreq
        dec       eax                                           ;54.23
        mov       DWORD PTR [12+esp], 1                         ;54.23
        test      eax, eax                                      ;54.23
        jle       .B1.59        ; Prob 2%                       ;54.23
                                ; LOE eax ebx
.B1.54:                         ; Preds .B1.53                  ; Infreq
        mov       DWORD PTR [20+esp], eax                       ;54.23
        lea       esi, DWORD PTR [12+esp]                       ;54.23
        mov       edi, DWORD PTR [16+ebp]                       ;54.23
                                ; LOE ebx esi edi
.B1.55:                         ; Preds .B1.57 .B1.54           ; Infreq
        push      OFFSET FLAT: __NLITPACK_0.0.1                 ;54.23
        push      esi                                           ;54.23
        push      edi                                           ;54.23
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_VALUE   ;54.23
                                ; LOE ebx esi edi f1
.B1.78:                         ; Preds .B1.55                  ; Infreq
        fstp      DWORD PTR [324+esp]                           ;54.23
        movss     xmm3, DWORD PTR [324+esp]                     ;54.23
                                ; LOE ebx esi edi xmm3
.B1.56:                         ; Preds .B1.78                  ; Infreq
        movss     xmm0, DWORD PTR [_2il0floatpacket.7]          ;54.23
        andps     xmm0, xmm3                                    ;54.23
        pxor      xmm3, xmm0                                    ;54.23
        movss     xmm1, DWORD PTR [_2il0floatpacket.8]          ;54.23
        movaps    xmm2, xmm3                                    ;54.23
        movaps    xmm7, xmm3                                    ;54.23
        cmpltss   xmm2, xmm1                                    ;54.23
        andps     xmm1, xmm2                                    ;54.23
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE+32] ;54.23
        addss     xmm7, xmm1                                    ;54.23
        neg       edx                                           ;54.23
        subss     xmm7, xmm1                                    ;54.23
        movaps    xmm6, xmm7                                    ;54.23
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE] ;54.23
        subss     xmm6, xmm3                                    ;54.23
        movss     xmm3, DWORD PTR [_2il0floatpacket.9]          ;54.23
        movaps    xmm4, xmm6                                    ;54.23
        movaps    xmm5, xmm3                                    ;54.23
        cmpnless  xmm4, xmm3                                    ;54.23
        addss     xmm5, xmm3                                    ;54.23
        cmpless   xmm6, DWORD PTR [_2il0floatpacket.10]         ;54.23
        andps     xmm4, xmm5                                    ;54.23
        andps     xmm6, xmm5                                    ;54.23
        subss     xmm7, xmm4                                    ;54.23
        addss     xmm7, xmm6                                    ;54.23
        orps      xmm7, xmm0                                    ;54.23
        cvtss2si  eax, xmm7                                     ;54.142
        add       edx, eax                                      ;54.23
        imul      eax, edx, 44                                  ;54.23
        mov       edx, DWORD PTR [36+ecx+eax]                   ;54.23
        lea       ecx, DWORD PTR [316+esp]                      ;54.23
        mov       DWORD PTR [316+esp], edx                      ;54.23
        push      ecx                                           ;54.23
        push      OFFSET FLAT: __STRLITPACK_37.0.1              ;54.23
        push      ebx                                           ;54.23
        call      _for_write_seq_fmt_xmit                       ;54.23
                                ; LOE ebx esi edi
.B1.79:                         ; Preds .B1.56                  ; Infreq
        add       esp, 24                                       ;54.23
                                ; LOE ebx esi edi
.B1.57:                         ; Preds .B1.79                  ; Infreq
        mov       eax, DWORD PTR [12+esp]                       ;54.23
        inc       eax                                           ;54.23
        mov       DWORD PTR [12+esp], eax                       ;54.23
        cmp       eax, DWORD PTR [20+esp]                       ;54.23
        jle       .B1.55        ; Prob 82%                      ;54.23
                                ; LOE ebx esi edi
.B1.59:                         ; Preds .B1.57 .B1.53           ; Infreq
        push      0                                             ;54.23
        push      OFFSET FLAT: __STRLITPACK_38.0.1              ;54.23
        push      ebx                                           ;54.23
        call      _for_write_seq_fmt_xmit                       ;54.23
                                ; LOE
.B1.80:                         ; Preds .B1.59                  ; Infreq
        add       esp, 352                                      ;54.23
        pop       ebx                                           ;54.23
        pop       edi                                           ;54.23
        pop       esi                                           ;54.23
        mov       esp, ebp                                      ;54.23
        pop       ebp                                           ;54.23
        ret                                                     ;54.23
                                ; LOE
.B1.60:                         ; Preds .B1.41                  ; Infreq
        push      esi                                           ;51.13
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_P_SIZE  ;51.13
                                ; LOE eax ebx esi edi
.B1.61:                         ; Preds .B1.60                  ; Infreq
        dec       eax                                           ;51.28
        mov       DWORD PTR [8+esp], eax                        ;51.28
        lea       eax, DWORD PTR [8+esp]                        ;51.13
        push      OFFSET FLAT: __NLITPACK_0.0.1                 ;51.13
        push      eax                                           ;51.13
        push      esi                                           ;51.13
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_VALUE   ;51.13
                                ; LOE ebx esi edi f1
.B1.82:                         ; Preds .B1.61                  ; Infreq
        fstp      DWORD PTR [328+esp]                           ;51.13
        movss     xmm3, DWORD PTR [328+esp]                     ;51.13
                                ; LOE ebx esi edi xmm3
.B1.62:                         ; Preds .B1.82                  ; Infreq
        movss     xmm0, DWORD PTR [_2il0floatpacket.7]          ;51.13
        andps     xmm0, xmm3                                    ;51.13
        pxor      xmm3, xmm0                                    ;51.13
        movss     xmm1, DWORD PTR [_2il0floatpacket.8]          ;51.13
        movaps    xmm2, xmm3                                    ;51.13
        movaps    xmm7, xmm3                                    ;51.13
        cmpltss   xmm2, xmm1                                    ;51.13
        andps     xmm1, xmm2                                    ;51.13
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE+32] ;51.13
        addss     xmm7, xmm1                                    ;51.13
        neg       edx                                           ;51.13
        subss     xmm7, xmm1                                    ;51.13
        movaps    xmm6, xmm7                                    ;51.13
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE] ;51.13
        subss     xmm6, xmm3                                    ;51.13
        movss     xmm3, DWORD PTR [_2il0floatpacket.9]          ;51.13
        movaps    xmm4, xmm6                                    ;51.13
        movaps    xmm5, xmm3                                    ;51.13
        cmpnless  xmm4, xmm3                                    ;51.13
        addss     xmm5, xmm3                                    ;51.13
        cmpless   xmm6, DWORD PTR [_2il0floatpacket.10]         ;51.13
        andps     xmm4, xmm5                                    ;51.13
        andps     xmm6, xmm5                                    ;51.13
        mov       DWORD PTR [272+esp], 0                        ;51.13
        subss     xmm7, xmm4                                    ;51.13
        addss     xmm7, xmm6                                    ;51.13
        orps      xmm7, xmm0                                    ;51.13
        cvtss2si  eax, xmm7                                     ;51.28
        add       edx, eax                                      ;51.13
        imul      eax, edx, 44                                  ;51.13
        movzx     edx, WORD PTR [40+ecx+eax]                    ;51.13
        lea       ecx, DWORD PTR [304+esp]                      ;51.13
        mov       WORD PTR [304+esp], dx                        ;51.13
        push      32                                            ;51.13
        push      OFFSET FLAT: WRITE_VEHICLES$format_pack.0.1+40 ;51.13
        push      ecx                                           ;51.13
        mov       eax, DWORD PTR [20+ebp]                       ;51.13
        push      OFFSET FLAT: __STRLITPACK_33.0.1              ;51.13
        push      -2088435968                                   ;51.13
        push      DWORD PTR [eax]                               ;51.13
        push      ebx                                           ;51.13
        call      _for_write_seq_fmt                            ;51.13
                                ; LOE ebx esi edi
.B1.63:                         ; Preds .B1.62                  ; Infreq
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;51.13
        mov       eax, edi                                      ;51.13
        shl       edx, 8                                        ;51.13
        mov       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;51.13
        neg       edx                                           ;51.13
        add       ecx, DWORD PTR [44+esp]                       ;51.13
        sub       eax, DWORD PTR [232+edx+ecx]                  ;51.13
        mov       edx, DWORD PTR [200+edx+ecx]                  ;51.106
        lea       ecx, DWORD PTR [340+esp]                      ;51.13
        movsx     eax, WORD PTR [edx+eax*2]                     ;51.106
        cvtsi2ss  xmm0, eax                                     ;51.106
        divss     xmm0, DWORD PTR [_2il0floatpacket.6]          ;51.13
        movss     DWORD PTR [340+esp], xmm0                     ;51.13
        push      ecx                                           ;51.13
        push      OFFSET FLAT: __STRLITPACK_34.0.1              ;51.13
        push      ebx                                           ;51.13
        call      _for_write_seq_fmt_xmit                       ;51.13
        jmp       .B1.76        ; Prob 100%                     ;51.13
        ALIGN     16
                                ; LOE ebx esi edi
; mark_end;
_WRITE_VEHICLES ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
WRITE_VEHICLES$format_pack.0.1	DB	54
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	6
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	-24
	DB	3
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	12
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	2
	DB	1
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	9
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	2
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	2
	DB	1
	DB	0
	DB	0
	DB	0
	DB	8
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	6
	DB	0
	DB	0
	DB	0
	DB	6
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	4
	DB	2
	DB	0
	DB	0
	DB	0
	DB	8
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	2
	DB	0
	DB	0
	DB	0
	DB	5
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	8
	DB	1
	DB	0
	DB	0
	DB	0
	DB	12
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	1
	DB	1
	DB	0
	DB	0
	DB	0
	DB	6
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
_DATA	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
__STRLITPACK_0.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_1.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_2.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_3.0.1	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_4.0.1	DB	5
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_5.0.1	DB	5
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_6.0.1	DB	5
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_7.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_8.0.1	DB	5
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_9.0.1	DB	5
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_10.0.1	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_11.0.1	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_12.0.1	DB	7
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_13.0.1	DB	5
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_14.0.1	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_15.0.1	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_16.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_17.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_18.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_19.0.1	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_20.0.1	DB	5
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_21.0.1	DB	5
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_22.0.1	DB	5
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_23.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_24.0.1	DB	5
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_25.0.1	DB	5
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_26.0.1	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_27.0.1	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_28.0.1	DB	7
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_29.0.1	DB	5
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_30.0.1	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__NLITPACK_0.0.1	DD	1
__STRLITPACK_33.0.1	DB	7
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_34.0.1	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_31.0.1	DB	7
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_32.0.1	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_35.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_36.0.1	DB	2
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_37.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_38.0.1	DB	1
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _WRITE_VEHICLES
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _RESORT_OUTPUT_VEH
; mark_begin;
       ALIGN     16
	PUBLIC _RESORT_OUTPUT_VEH
_RESORT_OUTPUT_VEH	PROC NEAR 
.B2.1:                          ; Preds .B2.0
        push      ebp                                           ;65.12
        mov       ebp, esp                                      ;65.12
        and       esp, -16                                      ;65.12
        push      esi                                           ;65.12
        push      edi                                           ;65.12
        push      ebx                                           ;65.12
        sub       esp, 1476                                     ;65.12
        xor       ebx, ebx                                      ;79.30
        mov       eax, 128                                      ;79.30
        mov       esi, 1                                        ;79.30
        mov       DWORD PTR [1240+esp], ebx                     ;79.30
        lea       edx, DWORD PTR [1280+esp]                     ;84.7
        mov       DWORD PTR [1244+esp], ebx                     ;79.30
        mov       DWORD PTR [1248+esp], ebx                     ;79.30
        mov       DWORD PTR [1252+esp], eax                     ;79.30
        mov       DWORD PTR [1256+esp], esi                     ;79.30
        mov       DWORD PTR [1260+esp], ebx                     ;79.30
        mov       DWORD PTR [1264+esp], ebx                     ;79.30
        mov       DWORD PTR [1268+esp], ebx                     ;79.30
        mov       DWORD PTR [1272+esp], ebx                     ;79.30
        mov       DWORD PTR [184+esp], ebx                      ;74.37
        mov       DWORD PTR [188+esp], ebx                      ;74.37
        mov       DWORD PTR [192+esp], ebx                      ;74.37
        mov       DWORD PTR [196+esp], eax                      ;74.37
        mov       DWORD PTR [200+esp], 2                        ;74.37
        mov       DWORD PTR [204+esp], ebx                      ;74.37
        mov       DWORD PTR [208+esp], ebx                      ;74.37
        mov       DWORD PTR [212+esp], ebx                      ;74.37
        mov       DWORD PTR [216+esp], ebx                      ;74.37
        mov       DWORD PTR [220+esp], ebx                      ;74.37
        mov       DWORD PTR [224+esp], ebx                      ;74.37
        mov       DWORD PTR [228+esp], ebx                      ;74.37
        mov       DWORD PTR [240+esp], ebx                      ;73.59
        mov       DWORD PTR [244+esp], ebx                      ;73.59
        mov       DWORD PTR [248+esp], ebx                      ;73.59
        mov       DWORD PTR [252+esp], eax                      ;73.59
        mov       DWORD PTR [256+esp], esi                      ;73.59
        mov       DWORD PTR [260+esp], ebx                      ;73.59
        mov       DWORD PTR [264+esp], ebx                      ;73.59
        mov       DWORD PTR [268+esp], ebx                      ;73.59
        mov       DWORD PTR [272+esp], ebx                      ;73.59
        mov       DWORD PTR [280+esp], ebx                      ;73.50
        mov       DWORD PTR [284+esp], ebx                      ;73.50
        mov       DWORD PTR [288+esp], ebx                      ;73.50
        mov       DWORD PTR [292+esp], eax                      ;73.50
        mov       DWORD PTR [296+esp], esi                      ;73.50
        mov       DWORD PTR [300+esp], ebx                      ;73.50
        mov       DWORD PTR [304+esp], ebx                      ;73.50
        mov       DWORD PTR [308+esp], ebx                      ;73.50
        mov       DWORD PTR [312+esp], ebx                      ;73.50
        mov       DWORD PTR [320+esp], ebx                      ;73.41
        mov       DWORD PTR [324+esp], ebx                      ;73.41
        mov       DWORD PTR [328+esp], ebx                      ;73.41
        mov       DWORD PTR [332+esp], eax                      ;73.41
        mov       DWORD PTR [336+esp], esi                      ;73.41
        mov       DWORD PTR [340+esp], ebx                      ;73.41
        mov       DWORD PTR [344+esp], ebx                      ;73.41
        mov       DWORD PTR [348+esp], ebx                      ;73.41
        mov       DWORD PTR [352+esp], ebx                      ;73.41
        mov       DWORD PTR [760+esp], ebx                      ;73.32
        mov       DWORD PTR [764+esp], ebx                      ;73.32
        mov       DWORD PTR [768+esp], ebx                      ;73.32
        mov       DWORD PTR [772+esp], eax                      ;73.32
        mov       DWORD PTR [776+esp], esi                      ;73.32
        mov       DWORD PTR [780+esp], ebx                      ;73.32
        mov       DWORD PTR [784+esp], ebx                      ;73.32
        mov       DWORD PTR [788+esp], ebx                      ;73.32
        mov       DWORD PTR [792+esp], ebx                      ;73.32
        mov       DWORD PTR [800+esp], ebx                      ;72.134
        mov       DWORD PTR [804+esp], ebx                      ;72.134
        mov       DWORD PTR [808+esp], ebx                      ;72.134
        mov       DWORD PTR [812+esp], eax                      ;72.134
        mov       DWORD PTR [816+esp], esi                      ;72.134
        mov       DWORD PTR [820+esp], ebx                      ;72.134
        mov       DWORD PTR [824+esp], ebx                      ;72.134
        mov       DWORD PTR [828+esp], ebx                      ;72.134
        mov       DWORD PTR [832+esp], ebx                      ;72.134
        mov       DWORD PTR [840+esp], ebx                      ;72.123
        mov       DWORD PTR [844+esp], ebx                      ;72.123
        mov       DWORD PTR [848+esp], ebx                      ;72.123
        mov       DWORD PTR [852+esp], eax                      ;72.123
        mov       DWORD PTR [856+esp], esi                      ;72.123
        mov       DWORD PTR [860+esp], ebx                      ;72.123
        mov       DWORD PTR [864+esp], ebx                      ;72.123
        mov       DWORD PTR [868+esp], ebx                      ;72.123
        mov       DWORD PTR [872+esp], ebx                      ;72.123
        mov       DWORD PTR [360+esp], ebx                      ;72.115
        mov       DWORD PTR [364+esp], ebx                      ;72.115
        mov       DWORD PTR [368+esp], ebx                      ;72.115
        mov       DWORD PTR [372+esp], eax                      ;72.115
        mov       DWORD PTR [376+esp], esi                      ;72.115
        mov       DWORD PTR [380+esp], ebx                      ;72.115
        mov       DWORD PTR [384+esp], ebx                      ;72.115
        mov       DWORD PTR [388+esp], ebx                      ;72.115
        mov       DWORD PTR [392+esp], ebx                      ;72.115
        mov       DWORD PTR [400+esp], ebx                      ;72.109
        mov       DWORD PTR [404+esp], ebx                      ;72.109
        mov       DWORD PTR [408+esp], ebx                      ;72.109
        mov       DWORD PTR [412+esp], eax                      ;72.109
        mov       DWORD PTR [416+esp], esi                      ;72.109
        mov       DWORD PTR [420+esp], ebx                      ;72.109
        mov       DWORD PTR [424+esp], ebx                      ;72.109
        mov       DWORD PTR [428+esp], ebx                      ;72.109
        mov       DWORD PTR [432+esp], ebx                      ;72.109
        mov       DWORD PTR [440+esp], ebx                      ;72.102
        mov       DWORD PTR [444+esp], ebx                      ;72.102
        mov       DWORD PTR [448+esp], ebx                      ;72.102
        mov       DWORD PTR [452+esp], eax                      ;72.102
        mov       DWORD PTR [456+esp], esi                      ;72.102
        mov       DWORD PTR [460+esp], ebx                      ;72.102
        mov       DWORD PTR [464+esp], ebx                      ;72.102
        mov       DWORD PTR [468+esp], ebx                      ;72.102
        mov       DWORD PTR [472+esp], ebx                      ;72.102
        mov       DWORD PTR [480+esp], ebx                      ;72.93
        mov       DWORD PTR [484+esp], ebx                      ;72.93
        mov       DWORD PTR [488+esp], ebx                      ;72.93
        mov       DWORD PTR [492+esp], eax                      ;72.93
        mov       DWORD PTR [496+esp], esi                      ;72.93
        mov       DWORD PTR [500+esp], ebx                      ;72.93
        mov       DWORD PTR [504+esp], ebx                      ;72.93
        mov       DWORD PTR [508+esp], ebx                      ;72.93
        mov       DWORD PTR [512+esp], ebx                      ;72.93
        mov       DWORD PTR [520+esp], ebx                      ;72.84
        mov       DWORD PTR [524+esp], ebx                      ;72.84
        mov       DWORD PTR [528+esp], ebx                      ;72.84
        mov       DWORD PTR [532+esp], eax                      ;72.84
        mov       DWORD PTR [536+esp], esi                      ;72.84
        mov       DWORD PTR [540+esp], ebx                      ;72.84
        mov       DWORD PTR [544+esp], ebx                      ;72.84
        mov       DWORD PTR [548+esp], ebx                      ;72.84
        mov       DWORD PTR [552+esp], ebx                      ;72.84
        mov       DWORD PTR [880+esp], ebx                      ;72.78
        mov       DWORD PTR [884+esp], ebx                      ;72.78
        mov       DWORD PTR [888+esp], ebx                      ;72.78
        mov       DWORD PTR [892+esp], eax                      ;72.78
        mov       DWORD PTR [896+esp], esi                      ;72.78
        mov       DWORD PTR [900+esp], ebx                      ;72.78
        mov       DWORD PTR [904+esp], ebx                      ;72.78
        mov       DWORD PTR [908+esp], ebx                      ;72.78
        mov       DWORD PTR [912+esp], ebx                      ;72.78
        mov       DWORD PTR [560+esp], ebx                      ;72.70
        mov       DWORD PTR [564+esp], ebx                      ;72.70
        mov       DWORD PTR [568+esp], ebx                      ;72.70
        mov       DWORD PTR [572+esp], eax                      ;72.70
        mov       DWORD PTR [576+esp], esi                      ;72.70
        mov       DWORD PTR [580+esp], ebx                      ;72.70
        mov       DWORD PTR [584+esp], ebx                      ;72.70
        mov       DWORD PTR [588+esp], ebx                      ;72.70
        mov       DWORD PTR [592+esp], ebx                      ;72.70
        mov       DWORD PTR [600+esp], ebx                      ;72.61
        mov       DWORD PTR [604+esp], ebx                      ;72.61
        mov       DWORD PTR [608+esp], ebx                      ;72.61
        mov       DWORD PTR [612+esp], eax                      ;72.61
        mov       DWORD PTR [616+esp], esi                      ;72.61
        mov       DWORD PTR [620+esp], ebx                      ;72.61
        mov       DWORD PTR [624+esp], ebx                      ;72.61
        mov       DWORD PTR [628+esp], ebx                      ;72.61
        mov       DWORD PTR [632+esp], ebx                      ;72.61
        mov       DWORD PTR [640+esp], ebx                      ;72.53
        mov       DWORD PTR [644+esp], ebx                      ;72.53
        mov       DWORD PTR [648+esp], ebx                      ;72.53
        mov       DWORD PTR [652+esp], eax                      ;72.53
        mov       DWORD PTR [656+esp], esi                      ;72.53
        mov       DWORD PTR [660+esp], ebx                      ;72.53
        mov       DWORD PTR [664+esp], ebx                      ;72.53
        mov       DWORD PTR [668+esp], ebx                      ;72.53
        mov       DWORD PTR [672+esp], ebx                      ;72.53
        mov       DWORD PTR [680+esp], ebx                      ;72.47
        mov       DWORD PTR [684+esp], ebx                      ;72.47
        mov       DWORD PTR [688+esp], ebx                      ;72.47
        mov       DWORD PTR [692+esp], eax                      ;72.47
        mov       DWORD PTR [696+esp], esi                      ;72.47
        mov       DWORD PTR [700+esp], ebx                      ;72.47
        mov       DWORD PTR [704+esp], ebx                      ;72.47
        mov       DWORD PTR [708+esp], ebx                      ;72.47
        mov       DWORD PTR [712+esp], ebx                      ;72.47
        mov       DWORD PTR [720+esp], ebx                      ;72.41
        mov       DWORD PTR [724+esp], ebx                      ;72.41
        mov       DWORD PTR [728+esp], ebx                      ;72.41
        mov       DWORD PTR [732+esp], eax                      ;72.41
        mov       DWORD PTR [736+esp], esi                      ;72.41
        mov       DWORD PTR [740+esp], ebx                      ;72.41
        mov       DWORD PTR [744+esp], ebx                      ;72.41
        mov       DWORD PTR [748+esp], ebx                      ;72.41
        mov       DWORD PTR [752+esp], ebx                      ;72.41
        mov       DWORD PTR [920+esp], ebx                      ;72.35
        mov       DWORD PTR [924+esp], ebx                      ;72.35
        mov       DWORD PTR [928+esp], ebx                      ;72.35
        mov       DWORD PTR [932+esp], eax                      ;72.35
        mov       DWORD PTR [936+esp], esi                      ;72.35
        mov       DWORD PTR [940+esp], ebx                      ;72.35
        mov       DWORD PTR [944+esp], ebx                      ;72.35
        mov       DWORD PTR [948+esp], ebx                      ;72.35
        mov       DWORD PTR [952+esp], ebx                      ;72.35
        mov       DWORD PTR [1280+esp], ebx                     ;84.7
        push      32                                            ;84.7
        push      ebx                                           ;84.7
        push      OFFSET FLAT: __STRLITPACK_55.0.2              ;84.7
        push      -2088435968                                   ;84.7
        push      97                                            ;84.7
        push      edx                                           ;84.7
        call      _for_close                                    ;84.7
                                ; LOE ebx esi
.B2.2:                          ; Preds .B2.1
        mov       DWORD PTR [1304+esp], ebx                     ;85.7
        push      32                                            ;85.7
        push      ebx                                           ;85.7
        push      OFFSET FLAT: __STRLITPACK_56.0.2              ;85.7
        push      -2088435968                                   ;85.7
        push      98                                            ;85.7
        lea       eax, DWORD PTR [1324+esp]                     ;85.7
        push      eax                                           ;85.7
        call      _for_close                                    ;85.7
                                ; LOE ebx esi
.B2.3:                          ; Preds .B2.2
        mov       DWORD PTR [1328+esp], 0                       ;86.1
        lea       eax, DWORD PTR [48+esp]                       ;86.1
        mov       DWORD PTR [48+esp], 18                        ;86.1
        mov       DWORD PTR [52+esp], OFFSET FLAT: __STRLITPACK_54 ;86.1
        mov       DWORD PTR [56+esp], 3                         ;86.1
        mov       DWORD PTR [60+esp], OFFSET FLAT: __STRLITPACK_53 ;86.1
        push      32                                            ;86.1
        push      eax                                           ;86.1
        push      OFFSET FLAT: __STRLITPACK_57.0.2              ;86.1
        push      -2088435968                                   ;86.1
        push      97                                            ;86.1
        lea       edx, DWORD PTR [1348+esp]                     ;86.1
        push      edx                                           ;86.1
        call      _for_open                                     ;86.1
                                ; LOE ebx esi
.B2.4:                          ; Preds .B2.3
        mov       DWORD PTR [1352+esp], 0                       ;87.1
        lea       eax, DWORD PTR [88+esp]                       ;87.1
        mov       DWORD PTR [88+esp], 15                        ;87.1
        mov       DWORD PTR [92+esp], OFFSET FLAT: __STRLITPACK_52 ;87.1
        mov       DWORD PTR [96+esp], 3                         ;87.1
        mov       DWORD PTR [100+esp], OFFSET FLAT: __STRLITPACK_51 ;87.1
        push      32                                            ;87.1
        push      eax                                           ;87.1
        push      OFFSET FLAT: __STRLITPACK_58.0.2              ;87.1
        push      -2088435968                                   ;87.1
        push      98                                            ;87.1
        lea       edx, DWORD PTR [1372+esp]                     ;87.1
        push      edx                                           ;87.1
        call      _for_open                                     ;87.1
                                ; LOE ebx esi
.B2.5:                          ; Preds .B2.4
        mov       DWORD PTR [1376+esp], 0                       ;88.1
        lea       eax, DWORD PTR [128+esp]                      ;88.1
        mov       DWORD PTR [128+esp], 21                       ;88.1
        mov       DWORD PTR [132+esp], OFFSET FLAT: __STRLITPACK_50 ;88.1
        mov       DWORD PTR [136+esp], 7                        ;88.1
        mov       DWORD PTR [140+esp], OFFSET FLAT: __STRLITPACK_49 ;88.1
        push      32                                            ;88.1
        push      eax                                           ;88.1
        push      OFFSET FLAT: __STRLITPACK_59.0.2              ;88.1
        push      -2088435968                                   ;88.1
        push      997                                           ;88.1
        lea       edx, DWORD PTR [1396+esp]                     ;88.1
        push      edx                                           ;88.1
        call      _for_open                                     ;88.1
                                ; LOE ebx esi
.B2.239:                        ; Preds .B2.5
        add       esp, 120                                      ;88.1
                                ; LOE ebx esi
.B2.6:                          ; Preds .B2.239
        mov       DWORD PTR [1280+esp], 0                       ;89.1
        lea       eax, DWORD PTR [48+esp]                       ;89.1
        mov       DWORD PTR [48+esp], 18                        ;89.1
        mov       DWORD PTR [52+esp], OFFSET FLAT: __STRLITPACK_48 ;89.1
        mov       DWORD PTR [56+esp], 7                         ;89.1
        mov       DWORD PTR [60+esp], OFFSET FLAT: __STRLITPACK_47 ;89.1
        push      32                                            ;89.1
        push      eax                                           ;89.1
        push      OFFSET FLAT: __STRLITPACK_60.0.2              ;89.1
        push      -2088435968                                   ;89.1
        push      998                                           ;89.1
        lea       edx, DWORD PTR [1300+esp]                     ;89.1
        push      edx                                           ;89.1
        call      _for_open                                     ;89.1
                                ; LOE ebx esi
.B2.7:                          ; Preds .B2.6
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TOTALVEH] ;95.3
        lea       edx, DWORD PTR [1104+esp]                     ;95.3
        mov       DWORD PTR [1304+esp], 0                       ;95.3
        mov       DWORD PTR [88+esp], eax                       ;95.3
        mov       DWORD PTR [1104+esp], eax                     ;95.3
        push      32                                            ;95.3
        push      edx                                           ;95.3
        push      OFFSET FLAT: __STRLITPACK_61.0.2              ;95.3
        push      -2088435968                                   ;95.3
        push      997                                           ;95.3
        lea       ecx, DWORD PTR [1324+esp]                     ;95.3
        push      ecx                                           ;95.3
        call      _for_write_seq_lis                            ;95.3
                                ; LOE ebx esi
.B2.8:                          ; Preds .B2.7
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFSTOPS] ;95.3
        lea       edx, DWORD PTR [1136+esp]                     ;95.3
        mov       DWORD PTR [1136+esp], eax                     ;95.3
        push      edx                                           ;95.3
        push      OFFSET FLAT: __STRLITPACK_62.0.2              ;95.3
        lea       ecx, DWORD PTR [1336+esp]                     ;95.3
        push      ecx                                           ;95.3
        call      _for_write_seq_lis_xmit                       ;95.3
                                ; LOE ebx esi
.B2.9:                          ; Preds .B2.8
        mov       DWORD PTR [292+esp], 44                       ;95.3
        lea       eax, DWORD PTR [292+esp]                      ;95.3
        mov       DWORD PTR [296+esp], OFFSET FLAT: __STRLITPACK_45 ;95.3
        push      eax                                           ;95.3
        push      OFFSET FLAT: __STRLITPACK_63.0.2              ;95.3
        lea       edx, DWORD PTR [1348+esp]                     ;95.3
        push      edx                                           ;95.3
        call      _for_write_seq_lis_xmit                       ;95.3
                                ; LOE ebx esi
.B2.10:                         ; Preds .B2.9
        mov       DWORD PTR [1352+esp], ebx                     ;96.3
        push      32                                            ;96.3
        push      OFFSET FLAT: RESORT_OUTPUT_VEH$format_pack.0.2+156 ;96.3
        push      ebx                                           ;96.3
        push      OFFSET FLAT: __STRLITPACK_64.0.2              ;96.3
        push      -2088435968                                   ;96.3
        push      997                                           ;96.3
        lea       eax, DWORD PTR [1376+esp]                     ;96.3
        push      eax                                           ;96.3
        call      _for_write_seq_fmt                            ;96.3
                                ; LOE ebx esi
.B2.11:                         ; Preds .B2.10
        mov       eax, DWORD PTR [164+esp]                      ;98.3
        test      eax, eax                                      ;98.3
        cmovle    eax, ebx                                      ;98.3
        mov       edi, 4                                        ;98.3
        mov       edx, 2                                        ;98.3
        mov       DWORD PTR [296+esp], 133                      ;98.3
        mov       DWORD PTR [288+esp], edi                      ;98.3
        mov       DWORD PTR [300+esp], edx                      ;98.3
        mov       DWORD PTR [292+esp], ebx                      ;98.3
        lea       ecx, DWORD PTR [eax*4]                        ;98.3
        mov       DWORD PTR [316+esp], esi                      ;98.3
        mov       DWORD PTR [308+esp], eax                      ;98.3
        mov       DWORD PTR [328+esp], esi                      ;98.3
        mov       DWORD PTR [320+esp], edx                      ;98.3
        mov       DWORD PTR [312+esp], edi                      ;98.3
        mov       DWORD PTR [324+esp], ecx                      ;98.3
        lea       ecx, DWORD PTR [416+esp]                      ;98.3
        push      8                                             ;98.3
        push      eax                                           ;98.3
        push      edx                                           ;98.3
        push      ecx                                           ;98.3
        call      _for_check_mult_overflow                      ;98.3
                                ; LOE eax ebx esi edi
.B2.240:                        ; Preds .B2.11
        add       esp, 116                                      ;98.3
                                ; LOE eax ebx esi edi
.B2.12:                         ; Preds .B2.240
        mov       edx, DWORD PTR [196+esp]                      ;98.3
        and       eax, 1                                        ;98.3
        and       edx, 1                                        ;98.3
        lea       ecx, DWORD PTR [184+esp]                      ;98.3
        shl       eax, 4                                        ;98.3
        add       edx, edx                                      ;98.3
        or        edx, eax                                      ;98.3
        or        edx, 262144                                   ;98.3
        push      edx                                           ;98.3
        push      ecx                                           ;98.3
        push      DWORD PTR [324+esp]                           ;98.3
        call      _for_alloc_allocatable                        ;98.3
                                ; LOE ebx esi edi
.B2.13:                         ; Preds .B2.12
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TOTALVEH] ;99.3
        test      eax, eax                                      ;99.3
        mov       DWORD PTR [944+esp], 133                      ;99.3
        lea       edx, DWORD PTR [368+esp]                      ;99.3
        cmovle    eax, ebx                                      ;99.3
        mov       DWORD PTR [936+esp], edi                      ;99.3
        mov       DWORD PTR [948+esp], esi                      ;99.3
        mov       DWORD PTR [940+esp], ebx                      ;99.3
        mov       DWORD PTR [964+esp], esi                      ;99.3
        mov       DWORD PTR [956+esp], eax                      ;99.3
        mov       DWORD PTR [960+esp], edi                      ;99.3
        push      edi                                           ;99.3
        push      eax                                           ;99.3
        push      2                                             ;99.3
        push      edx                                           ;99.3
        call      _for_check_mult_overflow                      ;99.3
                                ; LOE eax ebx esi edi
.B2.14:                         ; Preds .B2.13
        mov       edx, DWORD PTR [960+esp]                      ;99.3
        and       eax, 1                                        ;99.3
        and       edx, 1                                        ;99.3
        lea       ecx, DWORD PTR [948+esp]                      ;99.3
        shl       eax, 4                                        ;99.3
        add       edx, edx                                      ;99.3
        or        edx, eax                                      ;99.3
        or        edx, 262144                                   ;99.3
        push      edx                                           ;99.3
        push      ecx                                           ;99.3
        push      DWORD PTR [392+esp]                           ;99.3
        call      _for_alloc_allocatable                        ;99.3
                                ; LOE ebx esi edi
.B2.15:                         ; Preds .B2.14
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TOTALVEH] ;100.3
        test      eax, eax                                      ;100.3
        mov       DWORD PTR [772+esp], 133                      ;100.3
        lea       edx, DWORD PTR [436+esp]                      ;100.3
        cmovle    eax, ebx                                      ;100.3
        mov       DWORD PTR [764+esp], edi                      ;100.3
        mov       DWORD PTR [776+esp], esi                      ;100.3
        mov       DWORD PTR [768+esp], ebx                      ;100.3
        mov       DWORD PTR [792+esp], esi                      ;100.3
        mov       DWORD PTR [784+esp], eax                      ;100.3
        mov       DWORD PTR [788+esp], edi                      ;100.3
        push      edi                                           ;100.3
        push      eax                                           ;100.3
        push      2                                             ;100.3
        push      edx                                           ;100.3
        call      _for_check_mult_overflow                      ;100.3
                                ; LOE eax ebx esi edi
.B2.16:                         ; Preds .B2.15
        mov       edx, DWORD PTR [788+esp]                      ;100.3
        and       eax, 1                                        ;100.3
        and       edx, 1                                        ;100.3
        lea       ecx, DWORD PTR [776+esp]                      ;100.3
        shl       eax, 4                                        ;100.3
        add       edx, edx                                      ;100.3
        or        edx, eax                                      ;100.3
        or        edx, 262144                                   ;100.3
        push      edx                                           ;100.3
        push      ecx                                           ;100.3
        push      DWORD PTR [460+esp]                           ;100.3
        call      _for_alloc_allocatable                        ;100.3
                                ; LOE ebx esi edi
.B2.17:                         ; Preds .B2.16
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TOTALVEH] ;101.3
        test      eax, eax                                      ;101.3
        mov       DWORD PTR [760+esp], 133                      ;101.3
        lea       edx, DWORD PTR [504+esp]                      ;101.3
        cmovle    eax, ebx                                      ;101.3
        mov       DWORD PTR [752+esp], edi                      ;101.3
        mov       DWORD PTR [764+esp], esi                      ;101.3
        mov       DWORD PTR [756+esp], ebx                      ;101.3
        mov       DWORD PTR [780+esp], esi                      ;101.3
        mov       DWORD PTR [772+esp], eax                      ;101.3
        mov       DWORD PTR [776+esp], edi                      ;101.3
        push      edi                                           ;101.3
        push      eax                                           ;101.3
        push      2                                             ;101.3
        push      edx                                           ;101.3
        call      _for_check_mult_overflow                      ;101.3
                                ; LOE eax ebx esi edi
.B2.18:                         ; Preds .B2.17
        mov       edx, DWORD PTR [776+esp]                      ;101.3
        and       eax, 1                                        ;101.3
        and       edx, 1                                        ;101.3
        lea       ecx, DWORD PTR [764+esp]                      ;101.3
        shl       eax, 4                                        ;101.3
        add       edx, edx                                      ;101.3
        or        edx, eax                                      ;101.3
        or        edx, 262144                                   ;101.3
        push      edx                                           ;101.3
        push      ecx                                           ;101.3
        push      DWORD PTR [528+esp]                           ;101.3
        call      _for_alloc_allocatable                        ;101.3
                                ; LOE ebx esi edi
.B2.19:                         ; Preds .B2.18
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TOTALVEH] ;102.3
        test      eax, eax                                      ;102.3
        mov       DWORD PTR [748+esp], 133                      ;102.3
        lea       edx, DWORD PTR [572+esp]                      ;102.3
        cmovle    eax, ebx                                      ;102.3
        mov       DWORD PTR [740+esp], edi                      ;102.3
        mov       DWORD PTR [752+esp], esi                      ;102.3
        mov       DWORD PTR [744+esp], ebx                      ;102.3
        mov       DWORD PTR [768+esp], esi                      ;102.3
        mov       DWORD PTR [760+esp], eax                      ;102.3
        mov       DWORD PTR [764+esp], edi                      ;102.3
        push      edi                                           ;102.3
        push      eax                                           ;102.3
        push      2                                             ;102.3
        push      edx                                           ;102.3
        call      _for_check_mult_overflow                      ;102.3
                                ; LOE eax ebx esi edi
.B2.20:                         ; Preds .B2.19
        mov       edx, DWORD PTR [764+esp]                      ;102.3
        and       eax, 1                                        ;102.3
        and       edx, 1                                        ;102.3
        lea       ecx, DWORD PTR [752+esp]                      ;102.3
        shl       eax, 4                                        ;102.3
        add       edx, edx                                      ;102.3
        or        edx, eax                                      ;102.3
        or        edx, 262144                                   ;102.3
        push      edx                                           ;102.3
        push      ecx                                           ;102.3
        push      DWORD PTR [596+esp]                           ;102.3
        call      _for_alloc_allocatable                        ;102.3
                                ; LOE ebx esi edi
.B2.245:                        ; Preds .B2.20
        add       esp, 124                                      ;102.3
                                ; LOE ebx esi edi
.B2.21:                         ; Preds .B2.245
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TOTALVEH] ;103.3
        test      eax, eax                                      ;103.3
        mov       DWORD PTR [612+esp], 133                      ;103.3
        lea       edx, DWORD PTR [516+esp]                      ;103.3
        cmovle    eax, ebx                                      ;103.3
        mov       DWORD PTR [604+esp], edi                      ;103.3
        mov       DWORD PTR [616+esp], esi                      ;103.3
        mov       DWORD PTR [608+esp], ebx                      ;103.3
        mov       DWORD PTR [632+esp], esi                      ;103.3
        mov       DWORD PTR [624+esp], eax                      ;103.3
        mov       DWORD PTR [628+esp], edi                      ;103.3
        push      edi                                           ;103.3
        push      eax                                           ;103.3
        push      2                                             ;103.3
        push      edx                                           ;103.3
        call      _for_check_mult_overflow                      ;103.3
                                ; LOE eax ebx esi edi
.B2.22:                         ; Preds .B2.21
        mov       edx, DWORD PTR [628+esp]                      ;103.3
        and       eax, 1                                        ;103.3
        and       edx, 1                                        ;103.3
        lea       ecx, DWORD PTR [616+esp]                      ;103.3
        shl       eax, 4                                        ;103.3
        add       edx, edx                                      ;103.3
        or        edx, eax                                      ;103.3
        or        edx, 262144                                   ;103.3
        push      edx                                           ;103.3
        push      ecx                                           ;103.3
        push      DWORD PTR [540+esp]                           ;103.3
        call      _for_alloc_allocatable                        ;103.3
                                ; LOE ebx esi edi
.B2.23:                         ; Preds .B2.22
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TOTALVEH] ;104.3
        test      eax, eax                                      ;104.3
        mov       DWORD PTR [600+esp], 133                      ;104.3
        lea       edx, DWORD PTR [584+esp]                      ;104.3
        cmovle    eax, ebx                                      ;104.3
        mov       DWORD PTR [592+esp], edi                      ;104.3
        mov       DWORD PTR [604+esp], esi                      ;104.3
        mov       DWORD PTR [596+esp], ebx                      ;104.3
        mov       DWORD PTR [620+esp], esi                      ;104.3
        mov       DWORD PTR [612+esp], eax                      ;104.3
        mov       DWORD PTR [616+esp], edi                      ;104.3
        push      edi                                           ;104.3
        push      eax                                           ;104.3
        push      2                                             ;104.3
        push      edx                                           ;104.3
        call      _for_check_mult_overflow                      ;104.3
                                ; LOE eax ebx esi edi
.B2.24:                         ; Preds .B2.23
        mov       edx, DWORD PTR [616+esp]                      ;104.3
        and       eax, 1                                        ;104.3
        and       edx, 1                                        ;104.3
        lea       ecx, DWORD PTR [604+esp]                      ;104.3
        shl       eax, 4                                        ;104.3
        add       edx, edx                                      ;104.3
        or        edx, eax                                      ;104.3
        or        edx, 262144                                   ;104.3
        push      edx                                           ;104.3
        push      ecx                                           ;104.3
        push      DWORD PTR [608+esp]                           ;104.3
        call      _for_alloc_allocatable                        ;104.3
                                ; LOE ebx esi edi
.B2.25:                         ; Preds .B2.24
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TOTALVEH] ;105.3
        test      eax, eax                                      ;105.3
        mov       DWORD PTR [948+esp], 133                      ;105.3
        lea       edx, DWORD PTR [652+esp]                      ;105.3
        cmovle    eax, ebx                                      ;105.3
        mov       DWORD PTR [940+esp], edi                      ;105.3
        mov       DWORD PTR [952+esp], esi                      ;105.3
        mov       DWORD PTR [944+esp], ebx                      ;105.3
        mov       DWORD PTR [968+esp], esi                      ;105.3
        mov       DWORD PTR [960+esp], eax                      ;105.3
        mov       DWORD PTR [964+esp], edi                      ;105.3
        push      edi                                           ;105.3
        push      eax                                           ;105.3
        push      2                                             ;105.3
        push      edx                                           ;105.3
        call      _for_check_mult_overflow                      ;105.3
                                ; LOE eax ebx esi edi
.B2.26:                         ; Preds .B2.25
        mov       edx, DWORD PTR [964+esp]                      ;105.3
        and       eax, 1                                        ;105.3
        and       edx, 1                                        ;105.3
        lea       ecx, DWORD PTR [952+esp]                      ;105.3
        shl       eax, 4                                        ;105.3
        add       edx, edx                                      ;105.3
        or        edx, eax                                      ;105.3
        or        edx, 262144                                   ;105.3
        push      edx                                           ;105.3
        push      ecx                                           ;105.3
        push      DWORD PTR [676+esp]                           ;105.3
        call      _for_alloc_allocatable                        ;105.3
                                ; LOE ebx esi edi
.B2.27:                         ; Preds .B2.26
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TOTALVEH] ;106.3
        test      eax, eax                                      ;106.3
        mov       DWORD PTR [616+esp], 133                      ;106.3
        lea       edx, DWORD PTR [720+esp]                      ;106.3
        cmovle    eax, ebx                                      ;106.3
        mov       DWORD PTR [608+esp], edi                      ;106.3
        mov       DWORD PTR [620+esp], esi                      ;106.3
        mov       DWORD PTR [612+esp], ebx                      ;106.3
        mov       DWORD PTR [636+esp], esi                      ;106.3
        mov       DWORD PTR [628+esp], eax                      ;106.3
        mov       DWORD PTR [632+esp], edi                      ;106.3
        push      edi                                           ;106.3
        push      eax                                           ;106.3
        push      2                                             ;106.3
        push      edx                                           ;106.3
        call      _for_check_mult_overflow                      ;106.3
                                ; LOE eax ebx esi edi
.B2.28:                         ; Preds .B2.27
        mov       edx, DWORD PTR [632+esp]                      ;106.3
        and       eax, 1                                        ;106.3
        and       edx, 1                                        ;106.3
        lea       ecx, DWORD PTR [620+esp]                      ;106.3
        shl       eax, 4                                        ;106.3
        add       edx, edx                                      ;106.3
        or        edx, eax                                      ;106.3
        or        edx, 262144                                   ;106.3
        push      edx                                           ;106.3
        push      ecx                                           ;106.3
        push      DWORD PTR [744+esp]                           ;106.3
        call      _for_alloc_allocatable                        ;106.3
                                ; LOE ebx esi edi
.B2.250:                        ; Preds .B2.28
        add       esp, 112                                      ;106.3
                                ; LOE ebx esi edi
.B2.29:                         ; Preds .B2.250
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TOTALVEH] ;107.3
        test      eax, eax                                      ;107.3
        mov       DWORD PTR [492+esp], 133                      ;107.3
        lea       edx, DWORD PTR [676+esp]                      ;107.3
        cmovle    eax, ebx                                      ;107.3
        mov       DWORD PTR [484+esp], edi                      ;107.3
        mov       DWORD PTR [496+esp], esi                      ;107.3
        mov       DWORD PTR [488+esp], ebx                      ;107.3
        mov       DWORD PTR [512+esp], esi                      ;107.3
        mov       DWORD PTR [504+esp], eax                      ;107.3
        mov       DWORD PTR [508+esp], edi                      ;107.3
        push      edi                                           ;107.3
        push      eax                                           ;107.3
        push      2                                             ;107.3
        push      edx                                           ;107.3
        call      _for_check_mult_overflow                      ;107.3
                                ; LOE eax ebx esi edi
.B2.30:                         ; Preds .B2.29
        mov       edx, DWORD PTR [508+esp]                      ;107.3
        and       eax, 1                                        ;107.3
        and       edx, 1                                        ;107.3
        lea       ecx, DWORD PTR [496+esp]                      ;107.3
        shl       eax, 4                                        ;107.3
        add       edx, edx                                      ;107.3
        or        edx, eax                                      ;107.3
        or        edx, 262144                                   ;107.3
        push      edx                                           ;107.3
        push      ecx                                           ;107.3
        push      DWORD PTR [700+esp]                           ;107.3
        call      _for_alloc_allocatable                        ;107.3
                                ; LOE ebx esi edi
.B2.31:                         ; Preds .B2.30
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TOTALVEH] ;108.3
        test      eax, eax                                      ;108.3
        mov       DWORD PTR [480+esp], 133                      ;108.3
        lea       edx, DWORD PTR [744+esp]                      ;108.3
        cmovle    eax, ebx                                      ;108.3
        mov       DWORD PTR [472+esp], edi                      ;108.3
        mov       DWORD PTR [484+esp], esi                      ;108.3
        mov       DWORD PTR [476+esp], ebx                      ;108.3
        mov       DWORD PTR [500+esp], esi                      ;108.3
        mov       DWORD PTR [492+esp], eax                      ;108.3
        mov       DWORD PTR [496+esp], edi                      ;108.3
        push      edi                                           ;108.3
        push      eax                                           ;108.3
        push      2                                             ;108.3
        push      edx                                           ;108.3
        call      _for_check_mult_overflow                      ;108.3
                                ; LOE eax ebx esi edi
.B2.32:                         ; Preds .B2.31
        mov       edx, DWORD PTR [496+esp]                      ;108.3
        and       eax, 1                                        ;108.3
        and       edx, 1                                        ;108.3
        lea       ecx, DWORD PTR [484+esp]                      ;108.3
        shl       eax, 4                                        ;108.3
        add       edx, edx                                      ;108.3
        or        edx, eax                                      ;108.3
        or        edx, 262144                                   ;108.3
        push      edx                                           ;108.3
        push      ecx                                           ;108.3
        push      DWORD PTR [768+esp]                           ;108.3
        call      _for_alloc_allocatable                        ;108.3
                                ; LOE ebx esi edi
.B2.33:                         ; Preds .B2.32
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TOTALVEH] ;109.3
        test      eax, eax                                      ;109.3
        mov       DWORD PTR [468+esp], 133                      ;109.3
        lea       edx, DWORD PTR [812+esp]                      ;109.3
        cmovle    eax, ebx                                      ;109.3
        mov       DWORD PTR [460+esp], edi                      ;109.3
        mov       DWORD PTR [472+esp], esi                      ;109.3
        mov       DWORD PTR [464+esp], ebx                      ;109.3
        mov       DWORD PTR [488+esp], esi                      ;109.3
        mov       DWORD PTR [480+esp], eax                      ;109.3
        mov       DWORD PTR [484+esp], edi                      ;109.3
        push      edi                                           ;109.3
        push      eax                                           ;109.3
        push      2                                             ;109.3
        push      edx                                           ;109.3
        call      _for_check_mult_overflow                      ;109.3
                                ; LOE eax ebx esi edi
.B2.34:                         ; Preds .B2.33
        mov       edx, DWORD PTR [484+esp]                      ;109.3
        and       eax, 1                                        ;109.3
        and       edx, 1                                        ;109.3
        lea       ecx, DWORD PTR [472+esp]                      ;109.3
        shl       eax, 4                                        ;109.3
        add       edx, edx                                      ;109.3
        or        edx, eax                                      ;109.3
        or        edx, 262144                                   ;109.3
        push      edx                                           ;109.3
        push      ecx                                           ;109.3
        push      DWORD PTR [836+esp]                           ;109.3
        call      _for_alloc_allocatable                        ;109.3
                                ; LOE ebx esi edi
.B2.35:                         ; Preds .B2.34
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TOTALVEH] ;110.3
        test      eax, eax                                      ;110.3
        mov       DWORD PTR [456+esp], 133                      ;110.3
        lea       edx, DWORD PTR [880+esp]                      ;110.3
        cmovle    eax, ebx                                      ;110.3
        mov       DWORD PTR [448+esp], edi                      ;110.3
        mov       DWORD PTR [460+esp], esi                      ;110.3
        mov       DWORD PTR [452+esp], ebx                      ;110.3
        mov       DWORD PTR [476+esp], esi                      ;110.3
        mov       DWORD PTR [468+esp], eax                      ;110.3
        mov       DWORD PTR [472+esp], edi                      ;110.3
        push      edi                                           ;110.3
        push      eax                                           ;110.3
        push      2                                             ;110.3
        push      edx                                           ;110.3
        call      _for_check_mult_overflow                      ;110.3
                                ; LOE eax ebx esi edi
.B2.36:                         ; Preds .B2.35
        mov       edx, DWORD PTR [472+esp]                      ;110.3
        and       eax, 1                                        ;110.3
        and       edx, 1                                        ;110.3
        lea       ecx, DWORD PTR [460+esp]                      ;110.3
        shl       eax, 4                                        ;110.3
        add       edx, edx                                      ;110.3
        or        edx, eax                                      ;110.3
        or        edx, 262144                                   ;110.3
        push      edx                                           ;110.3
        push      ecx                                           ;110.3
        push      DWORD PTR [904+esp]                           ;110.3
        call      _for_alloc_allocatable                        ;110.3
                                ; LOE ebx esi edi
.B2.255:                        ; Preds .B2.36
        add       esp, 112                                      ;110.3
                                ; LOE ebx esi edi
.B2.37:                         ; Preds .B2.255
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TOTALVEH] ;111.3
        test      eax, eax                                      ;111.3
        mov       DWORD PTR [772+esp], 133                      ;111.3
        lea       edx, DWORD PTR [836+esp]                      ;111.3
        cmovle    eax, ebx                                      ;111.3
        mov       DWORD PTR [764+esp], edi                      ;111.3
        mov       DWORD PTR [776+esp], esi                      ;111.3
        mov       DWORD PTR [768+esp], ebx                      ;111.3
        mov       DWORD PTR [792+esp], esi                      ;111.3
        mov       DWORD PTR [784+esp], eax                      ;111.3
        mov       DWORD PTR [788+esp], edi                      ;111.3
        push      edi                                           ;111.3
        push      eax                                           ;111.3
        push      2                                             ;111.3
        push      edx                                           ;111.3
        call      _for_check_mult_overflow                      ;111.3
                                ; LOE eax ebx esi edi
.B2.38:                         ; Preds .B2.37
        mov       edx, DWORD PTR [788+esp]                      ;111.3
        and       eax, 1                                        ;111.3
        and       edx, 1                                        ;111.3
        lea       ecx, DWORD PTR [776+esp]                      ;111.3
        shl       eax, 4                                        ;111.3
        add       edx, edx                                      ;111.3
        or        edx, eax                                      ;111.3
        or        edx, 262144                                   ;111.3
        push      edx                                           ;111.3
        push      ecx                                           ;111.3
        push      DWORD PTR [860+esp]                           ;111.3
        call      _for_alloc_allocatable                        ;111.3
                                ; LOE ebx esi edi
.B2.39:                         ; Preds .B2.38
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TOTALVEH] ;112.3
        test      eax, eax                                      ;112.3
        mov       DWORD PTR [360+esp], 133                      ;112.3
        lea       edx, DWORD PTR [904+esp]                      ;112.3
        cmovle    eax, ebx                                      ;112.3
        mov       DWORD PTR [352+esp], edi                      ;112.3
        mov       DWORD PTR [364+esp], esi                      ;112.3
        mov       DWORD PTR [356+esp], ebx                      ;112.3
        mov       DWORD PTR [380+esp], esi                      ;112.3
        mov       DWORD PTR [372+esp], eax                      ;112.3
        mov       DWORD PTR [376+esp], edi                      ;112.3
        push      edi                                           ;112.3
        push      eax                                           ;112.3
        push      2                                             ;112.3
        push      edx                                           ;112.3
        call      _for_check_mult_overflow                      ;112.3
                                ; LOE eax ebx esi edi
.B2.40:                         ; Preds .B2.39
        mov       edx, DWORD PTR [376+esp]                      ;112.3
        and       eax, 1                                        ;112.3
        and       edx, 1                                        ;112.3
        lea       ecx, DWORD PTR [364+esp]                      ;112.3
        shl       eax, 4                                        ;112.3
        add       edx, edx                                      ;112.3
        or        edx, eax                                      ;112.3
        or        edx, 262144                                   ;112.3
        push      edx                                           ;112.3
        push      ecx                                           ;112.3
        push      DWORD PTR [928+esp]                           ;112.3
        call      _for_alloc_allocatable                        ;112.3
                                ; LOE ebx esi edi
.B2.41:                         ; Preds .B2.40
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TOTALVEH] ;113.3
        test      eax, eax                                      ;113.3
        mov       DWORD PTR [348+esp], 133                      ;113.3
        lea       edx, DWORD PTR [972+esp]                      ;113.3
        cmovle    eax, ebx                                      ;113.3
        mov       DWORD PTR [340+esp], edi                      ;113.3
        mov       DWORD PTR [352+esp], esi                      ;113.3
        mov       DWORD PTR [344+esp], ebx                      ;113.3
        mov       DWORD PTR [368+esp], esi                      ;113.3
        mov       DWORD PTR [360+esp], eax                      ;113.3
        mov       DWORD PTR [364+esp], edi                      ;113.3
        push      edi                                           ;113.3
        push      eax                                           ;113.3
        push      2                                             ;113.3
        push      edx                                           ;113.3
        call      _for_check_mult_overflow                      ;113.3
                                ; LOE eax ebx esi edi
.B2.42:                         ; Preds .B2.41
        mov       edx, DWORD PTR [364+esp]                      ;113.3
        and       eax, 1                                        ;113.3
        and       edx, 1                                        ;113.3
        lea       ecx, DWORD PTR [352+esp]                      ;113.3
        shl       eax, 4                                        ;113.3
        add       edx, edx                                      ;113.3
        or        edx, eax                                      ;113.3
        or        edx, 262144                                   ;113.3
        push      edx                                           ;113.3
        push      ecx                                           ;113.3
        push      DWORD PTR [996+esp]                           ;113.3
        call      _for_alloc_allocatable                        ;113.3
                                ; LOE ebx esi edi
.B2.43:                         ; Preds .B2.42
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TOTALVEH] ;114.3
        test      eax, eax                                      ;114.3
        mov       DWORD PTR [336+esp], 133                      ;114.3
        lea       edx, DWORD PTR [1040+esp]                     ;114.3
        cmovle    eax, ebx                                      ;114.3
        mov       DWORD PTR [328+esp], edi                      ;114.3
        mov       DWORD PTR [340+esp], esi                      ;114.3
        mov       DWORD PTR [332+esp], ebx                      ;114.3
        mov       DWORD PTR [356+esp], esi                      ;114.3
        mov       DWORD PTR [348+esp], eax                      ;114.3
        mov       DWORD PTR [352+esp], edi                      ;114.3
        push      edi                                           ;114.3
        push      eax                                           ;114.3
        push      2                                             ;114.3
        push      edx                                           ;114.3
        call      _for_check_mult_overflow                      ;114.3
                                ; LOE eax ebx esi edi
.B2.44:                         ; Preds .B2.43
        mov       edx, DWORD PTR [352+esp]                      ;114.3
        and       eax, 1                                        ;114.3
        and       edx, 1                                        ;114.3
        lea       ecx, DWORD PTR [340+esp]                      ;114.3
        shl       eax, 4                                        ;114.3
        add       edx, edx                                      ;114.3
        or        edx, eax                                      ;114.3
        or        edx, 262144                                   ;114.3
        push      edx                                           ;114.3
        push      ecx                                           ;114.3
        push      DWORD PTR [1064+esp]                          ;114.3
        call      _for_alloc_allocatable                        ;114.3
                                ; LOE ebx esi edi
.B2.260:                        ; Preds .B2.44
        add       esp, 112                                      ;114.3
                                ; LOE ebx esi edi
.B2.45:                         ; Preds .B2.260
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TOTALVEH] ;115.3
        test      eax, eax                                      ;115.3
        mov       DWORD PTR [852+esp], 133                      ;115.3
        lea       edx, DWORD PTR [964+esp]                      ;115.3
        cmovle    eax, ebx                                      ;115.3
        mov       DWORD PTR [844+esp], edi                      ;115.3
        mov       DWORD PTR [856+esp], esi                      ;115.3
        mov       DWORD PTR [848+esp], ebx                      ;115.3
        mov       DWORD PTR [872+esp], esi                      ;115.3
        mov       DWORD PTR [864+esp], eax                      ;115.3
        mov       DWORD PTR [868+esp], edi                      ;115.3
        push      edi                                           ;115.3
        push      eax                                           ;115.3
        push      2                                             ;115.3
        push      edx                                           ;115.3
        call      _for_check_mult_overflow                      ;115.3
                                ; LOE eax ebx esi edi
.B2.46:                         ; Preds .B2.45
        mov       edx, DWORD PTR [868+esp]                      ;115.3
        and       eax, 1                                        ;115.3
        and       edx, 1                                        ;115.3
        lea       ecx, DWORD PTR [856+esp]                      ;115.3
        shl       eax, 4                                        ;115.3
        add       edx, edx                                      ;115.3
        or        edx, eax                                      ;115.3
        or        edx, 262144                                   ;115.3
        push      edx                                           ;115.3
        push      ecx                                           ;115.3
        push      DWORD PTR [988+esp]                           ;115.3
        call      _for_alloc_allocatable                        ;115.3
                                ; LOE ebx esi edi
.B2.47:                         ; Preds .B2.46
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TOTALVEH] ;116.3
        test      eax, eax                                      ;116.3
        mov       DWORD PTR [840+esp], 133                      ;116.3
        lea       edx, DWORD PTR [1000+esp]                     ;116.3
        cmovle    eax, ebx                                      ;116.3
        mov       DWORD PTR [832+esp], edi                      ;116.3
        mov       DWORD PTR [844+esp], esi                      ;116.3
        mov       DWORD PTR [836+esp], ebx                      ;116.3
        mov       DWORD PTR [860+esp], esi                      ;116.3
        mov       DWORD PTR [852+esp], eax                      ;116.3
        mov       DWORD PTR [856+esp], edi                      ;116.3
        push      edi                                           ;116.3
        push      eax                                           ;116.3
        push      2                                             ;116.3
        push      edx                                           ;116.3
        call      _for_check_mult_overflow                      ;116.3
                                ; LOE eax ebx esi edi
.B2.48:                         ; Preds .B2.47
        mov       edx, DWORD PTR [856+esp]                      ;116.3
        and       eax, 1                                        ;116.3
        and       edx, 1                                        ;116.3
        lea       ecx, DWORD PTR [844+esp]                      ;116.3
        shl       eax, 4                                        ;116.3
        add       edx, edx                                      ;116.3
        or        edx, eax                                      ;116.3
        or        edx, 262144                                   ;116.3
        push      edx                                           ;116.3
        push      ecx                                           ;116.3
        push      DWORD PTR [1024+esp]                          ;116.3
        call      _for_alloc_allocatable                        ;116.3
                                ; LOE ebx esi edi
.B2.49:                         ; Preds .B2.48
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TOTALVEH] ;117.3
        test      edx, edx                                      ;117.3
        lea       ecx, DWORD PTR [1036+esp]                     ;117.3
        cmovle    edx, ebx                                      ;117.3
        mov       eax, 36                                       ;117.3
        mov       DWORD PTR [1308+esp], 133                     ;117.3
        mov       DWORD PTR [1300+esp], eax                     ;117.3
        mov       DWORD PTR [1312+esp], esi                     ;117.3
        mov       DWORD PTR [1304+esp], ebx                     ;117.3
        mov       DWORD PTR [1328+esp], esi                     ;117.3
        mov       DWORD PTR [1320+esp], edx                     ;117.3
        mov       DWORD PTR [1324+esp], eax                     ;117.3
        push      eax                                           ;117.3
        push      edx                                           ;117.3
        push      2                                             ;117.3
        push      ecx                                           ;117.3
        call      _for_check_mult_overflow                      ;117.3
                                ; LOE eax ebx esi edi
.B2.50:                         ; Preds .B2.49
        mov       edx, DWORD PTR [1324+esp]                     ;117.3
        and       eax, 1                                        ;117.3
        and       edx, 1                                        ;117.3
        lea       ecx, DWORD PTR [1312+esp]                     ;117.3
        shl       eax, 4                                        ;117.3
        add       edx, edx                                      ;117.3
        or        edx, eax                                      ;117.3
        or        edx, 262144                                   ;117.3
        push      edx                                           ;117.3
        push      ecx                                           ;117.3
        push      DWORD PTR [1060+esp]                          ;117.3
        call      _for_alloc_allocatable                        ;117.3
                                ; LOE ebx esi edi
.B2.264:                        ; Preds .B2.50
        add       esp, 84                                       ;117.3
                                ; LOE ebx esi edi
.B2.51:                         ; Preds .B2.264
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TOTALVEH] ;119.1
        test      ecx, ecx                                      ;119.1
        jle       .B2.83        ; Prob 2%                       ;119.1
                                ; LOE ecx ebx esi edi
.B2.52:                         ; Preds .B2.51
        mov       eax, esi                                      ;
        lea       edi, DWORD PTR [1280+esp]                     ;128.31
        mov       DWORD PTR [276+esp], ecx                      ;128.31
        mov       ebx, eax                                      ;128.31
                                ; LOE ebx edi
.B2.53:                         ; Preds .B2.81 .B2.52
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_FUELOUT], 0 ;120.14
        jle       .B2.222       ; Prob 16%                      ;120.14
                                ; LOE ebx edi
.B2.54:                         ; Preds .B2.53
        mov       edx, DWORD PTR [952+esp]                      ;121.19
        lea       esi, DWORD PTR [ebx*4]                        ;121.5
        shl       edx, 2                                        ;121.5
        mov       ecx, DWORD PTR [920+esp]                      ;121.5
        sub       ecx, edx                                      ;121.5
        mov       DWORD PTR [1280+esp], 0                       ;121.5
        mov       DWORD PTR [1096+esp], 4                       ;121.5
        lea       edx, DWORD PTR [ecx+ebx*4]                    ;121.5
        mov       DWORD PTR [1100+esp], edx                     ;121.5
        lea       edx, DWORD PTR [1096+esp]                     ;121.5
        push      32                                            ;121.5
        push      OFFSET FLAT: RESORT_OUTPUT_VEH$format_pack.0.2+52 ;121.5
        push      edx                                           ;121.5
        push      OFFSET FLAT: __STRLITPACK_65.0.2              ;121.5
        push      -2088435968                                   ;121.5
        push      97                                            ;121.5
        push      edi                                           ;121.5
        call      _for_read_seq_fmt                             ;121.5
                                ; LOE ebx esi edi
.B2.55:                         ; Preds .B2.54
        mov       edx, DWORD PTR [780+esp]                      ;121.29
        shl       edx, 2                                        ;121.5
        mov       ecx, DWORD PTR [748+esp]                      ;121.5
        sub       ecx, edx                                      ;121.5
        mov       DWORD PTR [1132+esp], 4                       ;121.5
        lea       edx, DWORD PTR [ecx+ebx*4]                    ;121.5
        mov       DWORD PTR [1136+esp], edx                     ;121.5
        lea       edx, DWORD PTR [1132+esp]                     ;121.5
        push      edx                                           ;121.5
        push      OFFSET FLAT: __STRLITPACK_66.0.2              ;121.5
        push      edi                                           ;121.5
        call      _for_read_seq_fmt_xmit                        ;121.5
                                ; LOE ebx esi edi
.B2.56:                         ; Preds .B2.55
        mov       edx, DWORD PTR [752+esp]                      ;121.39
        shl       edx, 2                                        ;121.5
        mov       ecx, DWORD PTR [720+esp]                      ;121.5
        sub       ecx, edx                                      ;121.5
        mov       DWORD PTR [1152+esp], 4                       ;121.5
        lea       edx, DWORD PTR [ecx+ebx*4]                    ;121.5
        mov       DWORD PTR [1156+esp], edx                     ;121.5
        lea       edx, DWORD PTR [1152+esp]                     ;121.5
        push      edx                                           ;121.5
        push      OFFSET FLAT: __STRLITPACK_67.0.2              ;121.5
        push      edi                                           ;121.5
        call      _for_read_seq_fmt_xmit                        ;121.5
                                ; LOE ebx esi edi
.B2.57:                         ; Preds .B2.56
        mov       edx, DWORD PTR [844+esp]                      ;121.49
        shl       edx, 2                                        ;121.5
        mov       ecx, DWORD PTR [812+esp]                      ;121.5
        sub       ecx, edx                                      ;121.5
        mov       DWORD PTR [1172+esp], 4                       ;121.5
        lea       edx, DWORD PTR [ecx+ebx*4]                    ;121.5
        mov       DWORD PTR [1176+esp], edx                     ;121.5
        lea       edx, DWORD PTR [1172+esp]                     ;121.5
        push      edx                                           ;121.5
        push      OFFSET FLAT: __STRLITPACK_68.0.2              ;121.5
        push      edi                                           ;121.5
        call      _for_read_seq_fmt_xmit                        ;121.5
                                ; LOE ebx esi edi
.B2.58:                         ; Preds .B2.57
        mov       edx, DWORD PTR [736+esp]                      ;121.62
        shl       edx, 2                                        ;121.5
        mov       ecx, DWORD PTR [704+esp]                      ;121.5
        sub       ecx, edx                                      ;121.5
        mov       DWORD PTR [1192+esp], 4                       ;121.5
        lea       edx, DWORD PTR [ecx+ebx*4]                    ;121.5
        mov       DWORD PTR [1196+esp], edx                     ;121.5
        lea       edx, DWORD PTR [1192+esp]                     ;121.5
        push      edx                                           ;121.5
        push      OFFSET FLAT: __STRLITPACK_69.0.2              ;121.5
        push      edi                                           ;121.5
        call      _for_read_seq_fmt_xmit                        ;121.5
                                ; LOE ebx esi edi
.B2.59:                         ; Preds .B2.58
        mov       edx, DWORD PTR [708+esp]                      ;121.74
        shl       edx, 2                                        ;121.5
        mov       ecx, DWORD PTR [676+esp]                      ;121.5
        sub       ecx, edx                                      ;121.5
        mov       DWORD PTR [1212+esp], 4                       ;121.5
        lea       edx, DWORD PTR [ecx+ebx*4]                    ;121.5
        mov       DWORD PTR [1216+esp], edx                     ;121.5
        lea       edx, DWORD PTR [1212+esp]                     ;121.5
        push      edx                                           ;121.5
        push      OFFSET FLAT: __STRLITPACK_70.0.2              ;121.5
        push      edi                                           ;121.5
        call      _for_read_seq_fmt_xmit                        ;121.5
                                ; LOE ebx esi edi
.B2.60:                         ; Preds .B2.59
        mov       edx, DWORD PTR [680+esp]                      ;121.87
        shl       edx, 2                                        ;121.5
        mov       ecx, DWORD PTR [648+esp]                      ;121.5
        sub       ecx, edx                                      ;121.5
        mov       DWORD PTR [1232+esp], 4                       ;121.5
        lea       edx, DWORD PTR [ecx+ebx*4]                    ;121.5
        mov       DWORD PTR [1236+esp], edx                     ;121.5
        lea       edx, DWORD PTR [1232+esp]                     ;121.5
        push      edx                                           ;121.5
        push      OFFSET FLAT: __STRLITPACK_71.0.2              ;121.5
        push      edi                                           ;121.5
        call      _for_read_seq_fmt_xmit                        ;121.5
                                ; LOE ebx esi edi
.B2.61:                         ; Preds .B2.60
        mov       edx, DWORD PTR [1012+esp]                     ;121.99
        shl       edx, 2                                        ;121.5
        mov       ecx, DWORD PTR [980+esp]                      ;121.5
        sub       ecx, edx                                      ;121.5
        mov       DWORD PTR [1252+esp], 4                       ;121.5
        lea       edx, DWORD PTR [ecx+ebx*4]                    ;121.5
        mov       DWORD PTR [1256+esp], edx                     ;121.5
        lea       edx, DWORD PTR [1252+esp]                     ;121.5
        push      edx                                           ;121.5
        push      OFFSET FLAT: __STRLITPACK_72.0.2              ;121.5
        push      edi                                           ;121.5
        call      _for_read_seq_fmt_xmit                        ;121.5
                                ; LOE ebx esi edi
.B2.62:                         ; Preds .B2.61
        mov       edx, DWORD PTR [664+esp]                      ;121.109
        shl       edx, 2                                        ;121.5
        mov       ecx, DWORD PTR [632+esp]                      ;121.5
        sub       ecx, edx                                      ;121.5
        mov       DWORD PTR [1272+esp], 4                       ;121.5
        lea       edx, DWORD PTR [ecx+ebx*4]                    ;121.5
        mov       DWORD PTR [1276+esp], edx                     ;121.5
        lea       edx, DWORD PTR [1272+esp]                     ;121.5
        push      edx                                           ;121.5
        push      OFFSET FLAT: __STRLITPACK_73.0.2              ;121.5
        push      edi                                           ;121.5
        call      _for_read_seq_fmt_xmit                        ;121.5
                                ; LOE ebx esi edi
.B2.265:                        ; Preds .B2.62
        add       esp, 124                                      ;121.5
                                ; LOE ebx esi edi
.B2.63:                         ; Preds .B2.265
        mov       edx, DWORD PTR [512+esp]                      ;121.122
        shl       edx, 2                                        ;121.5
        mov       ecx, DWORD PTR [480+esp]                      ;121.5
        sub       ecx, edx                                      ;121.5
        mov       DWORD PTR [1168+esp], 4                       ;121.5
        lea       edx, DWORD PTR [ecx+ebx*4]                    ;121.5
        mov       DWORD PTR [1172+esp], edx                     ;121.5
        lea       edx, DWORD PTR [1168+esp]                     ;121.5
        push      edx                                           ;121.5
        push      OFFSET FLAT: __STRLITPACK_74.0.2              ;121.5
        push      edi                                           ;121.5
        call      _for_read_seq_fmt_xmit                        ;121.5
                                ; LOE ebx esi edi
.B2.64:                         ; Preds .B2.63
        mov       edx, DWORD PTR [364+esp]                      ;121.135
        shl       edx, 2                                        ;121.5
        mov       ecx, DWORD PTR [332+esp]                      ;121.5
        sub       ecx, edx                                      ;121.5
        mov       DWORD PTR [1188+esp], 4                       ;121.5
        lea       edx, DWORD PTR [ecx+ebx*4]                    ;121.5
        mov       DWORD PTR [1192+esp], edx                     ;121.5
        lea       edx, DWORD PTR [1188+esp]                     ;121.5
        push      edx                                           ;121.5
        push      OFFSET FLAT: __STRLITPACK_75.0.2              ;121.5
        push      edi                                           ;121.5
        call      _for_read_seq_fmt_xmit                        ;121.5
                                ; LOE ebx esi edi
.B2.65:                         ; Preds .B2.64
        mov       edx, DWORD PTR [336+esp]                      ;121.148
        shl       edx, 2                                        ;121.5
        mov       ecx, DWORD PTR [304+esp]                      ;121.5
        sub       ecx, edx                                      ;121.5
        mov       DWORD PTR [1208+esp], 4                       ;121.5
        lea       edx, DWORD PTR [ecx+ebx*4]                    ;121.5
        mov       DWORD PTR [1212+esp], edx                     ;121.5
        lea       edx, DWORD PTR [1208+esp]                     ;121.5
        push      edx                                           ;121.5
        push      OFFSET FLAT: __STRLITPACK_76.0.2              ;121.5
        push      edi                                           ;121.5
        call      _for_read_seq_fmt_xmit                        ;121.5
                                ; LOE ebx esi edi
.B2.66:                         ; Preds .B2.65
        mov       edx, DWORD PTR [508+esp]                      ;121.161
        shl       edx, 2                                        ;121.5
        mov       ecx, DWORD PTR [476+esp]                      ;121.5
        sub       ecx, edx                                      ;121.5
        mov       DWORD PTR [1228+esp], 4                       ;121.5
        lea       edx, DWORD PTR [ecx+ebx*4]                    ;121.5
        mov       DWORD PTR [1232+esp], edx                     ;121.5
        lea       edx, DWORD PTR [1228+esp]                     ;121.5
        push      edx                                           ;121.5
        push      OFFSET FLAT: __STRLITPACK_77.0.2              ;121.5
        push      edi                                           ;121.5
        call      _for_read_seq_fmt_xmit                        ;121.5
                                ; LOE ebx esi edi
.B2.67:                         ; Preds .B2.66
        mov       edx, DWORD PTR [480+esp]                      ;121.172
        shl       edx, 2                                        ;121.5
        mov       ecx, DWORD PTR [448+esp]                      ;121.5
        sub       ecx, edx                                      ;121.5
        mov       DWORD PTR [1248+esp], 4                       ;121.5
        lea       edx, DWORD PTR [ecx+ebx*4]                    ;121.5
        mov       DWORD PTR [1252+esp], edx                     ;121.5
        lea       edx, DWORD PTR [1248+esp]                     ;121.5
        push      edx                                           ;121.5
        push      OFFSET FLAT: __STRLITPACK_78.0.2              ;121.5
        push      edi                                           ;121.5
        call      _for_read_seq_fmt_xmit                        ;121.5
                                ; LOE ebx esi edi
.B2.68:                         ; Preds .B2.67
        mov       edx, DWORD PTR [332+esp]                      ;121.182
        shl       edx, 2                                        ;121.5
        mov       ecx, DWORD PTR [300+esp]                      ;121.5
        sub       ecx, edx                                      ;121.5
        mov       DWORD PTR [1268+esp], 4                       ;121.5
        lea       edx, DWORD PTR [ecx+ebx*4]                    ;121.5
        mov       DWORD PTR [1272+esp], edx                     ;121.5
        lea       edx, DWORD PTR [1268+esp]                     ;121.5
        push      edx                                           ;121.5
        push      OFFSET FLAT: __STRLITPACK_79.0.2              ;121.5
        push      edi                                           ;121.5
        call      _for_read_seq_fmt_xmit                        ;121.5
                                ; LOE ebx esi edi
.B2.69:                         ; Preds .B2.68
        mov       edx, DWORD PTR [464+esp]                      ;121.195
        shl       edx, 2                                        ;121.5
        mov       ecx, DWORD PTR [432+esp]                      ;121.5
        sub       ecx, edx                                      ;121.5
        mov       DWORD PTR [1288+esp], 4                       ;121.5
        lea       edx, DWORD PTR [ecx+ebx*4]                    ;121.5
        mov       DWORD PTR [1292+esp], edx                     ;121.5
        lea       edx, DWORD PTR [1288+esp]                     ;121.5
        push      edx                                           ;121.5
        push      OFFSET FLAT: __STRLITPACK_80.0.2              ;121.5
        push      edi                                           ;121.5
        call      _for_read_seq_fmt_xmit                        ;121.5
                                ; LOE ebx esi edi
.B2.266:                        ; Preds .B2.69
        add       esp, 84                                       ;121.5
                                ; LOE ebx esi edi
.B2.70:                         ; Preds .B2.266 .B2.285
        mov       edx, DWORD PTR [872+esp]                      ;125.17
        shl       edx, 2                                        ;125.3
        mov       ecx, DWORD PTR [840+esp]                      ;125.3
        sub       ecx, edx                                      ;125.3
        add       ecx, esi                                      ;125.3
        mov       DWORD PTR [1280+esp], 0                       ;125.3
        mov       DWORD PTR [1224+esp], 4                       ;125.3
        mov       DWORD PTR [1228+esp], ecx                     ;125.3
        push      32                                            ;125.3
        push      OFFSET FLAT: RESORT_OUTPUT_VEH$format_pack.0.2+20 ;125.3
        lea       edx, DWORD PTR [1232+esp]                     ;125.3
        push      edx                                           ;125.3
        push      OFFSET FLAT: __STRLITPACK_96.0.2              ;125.3
        push      -2088435968                                   ;125.3
        push      97                                            ;125.3
        push      edi                                           ;125.3
        call      _for_read_seq_fmt                             ;125.3
                                ; LOE ebx esi edi
.B2.71:                         ; Preds .B2.70
        mov       edx, DWORD PTR [860+esp]                      ;125.32
        shl       edx, 2                                        ;125.3
        mov       ecx, DWORD PTR [828+esp]                      ;125.3
        sub       ecx, edx                                      ;125.3
        lea       edx, DWORD PTR [1260+esp]                     ;125.3
        add       ecx, esi                                      ;125.3
        mov       DWORD PTR [1260+esp], 4                       ;125.3
        mov       DWORD PTR [1264+esp], ecx                     ;125.3
        push      edx                                           ;125.3
        push      OFFSET FLAT: __STRLITPACK_97.0.2              ;125.3
        push      edi                                           ;125.3
        call      _for_read_seq_fmt_xmit                        ;125.3
                                ; LOE ebx esi edi
.B2.72:                         ; Preds .B2.71
        mov       eax, DWORD PTR [264+esp]                      ;127.3
        mov       ecx, DWORD PTR [992+esp]                      ;127.14
        neg       ecx                                           ;127.3
        mov       DWORD PTR [1008+esp], ebx                     ;
        add       ecx, ebx                                      ;127.3
        mov       edi, DWORD PTR [268+esp]                      ;127.3
        lea       ebx, DWORD PTR [eax+eax]                      ;127.3
        mov       edx, DWORD PTR [256+esp]                      ;127.3
        neg       ebx                                           ;127.3
        imul      edi, eax                                      ;127.3
        movss     xmm0, DWORD PTR [_2il0floatpacket.75]         ;128.14
        movss     xmm1, DWORD PTR [_2il0floatpacket.76]         ;128.14
        movss     xmm4, DWORD PTR [_2il0floatpacket.77]         ;128.14
        movaps    xmm6, xmm4                                    ;128.14
        shl       edx, 2                                        ;127.3
        addss     xmm6, xmm4                                    ;128.14
        add       ebx, edx                                      ;127.3
        sub       edx, eax                                      ;128.3
        add       esi, DWORD PTR [224+esp]                      ;127.3
        neg       ebx                                           ;127.3
        add       ebx, esi                                      ;127.3
        sub       esi, edx                                      ;128.3
        mov       DWORD PTR [1016+esp], edi                     ;127.3
        sub       ebx, edi                                      ;127.3
        mov       edi, DWORD PTR [960+esp]                      ;127.3
        mov       edx, 4                                        ;129.3
        sub       esi, DWORD PTR [1016+esp]                     ;128.3
        mov       ecx, DWORD PTR [edi+ecx*4]                    ;127.3
        mov       DWORD PTR [ebx], ecx                          ;127.3
        mov       ecx, DWORD PTR [832+esp]                      ;128.14
        mov       ebx, DWORD PTR [1008+esp]                     ;128.14
        neg       ecx                                           ;128.14
        add       ecx, ebx                                      ;128.14
        mov       edi, DWORD PTR [800+esp]                      ;128.14
        movss     xmm3, DWORD PTR [edi+ecx*4]                   ;128.14
        xor       ecx, ecx                                      ;129.3
        mulss     xmm3, DWORD PTR [_2il0floatpacket.74]         ;128.14
        andps     xmm0, xmm3                                    ;128.14
        pxor      xmm3, xmm0                                    ;128.14
        movaps    xmm2, xmm3                                    ;128.14
        cmpltss   xmm2, xmm1                                    ;128.14
        andps     xmm1, xmm2                                    ;128.14
        movaps    xmm2, xmm3                                    ;128.14
        addss     xmm2, xmm1                                    ;128.14
        subss     xmm2, xmm1                                    ;128.14
        movaps    xmm7, xmm2                                    ;128.14
        subss     xmm7, xmm3                                    ;128.14
        movaps    xmm5, xmm7                                    ;128.14
        cmpless   xmm7, DWORD PTR [_2il0floatpacket.78]         ;128.14
        cmpnless  xmm5, xmm4                                    ;128.14
        andps     xmm5, xmm6                                    ;128.14
        andps     xmm7, xmm6                                    ;128.14
        subss     xmm2, xmm5                                    ;128.14
        addss     xmm2, xmm7                                    ;128.14
        orps      xmm2, xmm0                                    ;128.14
        cvtss2si  edi, xmm2                                     ;128.3
        mov       DWORD PTR [esi], edi                          ;128.3
        lea       esi, DWORD PTR [ebx*4]                        ;129.3
        mov       edi, DWORD PTR [1280+esp]                     ;129.3
        lea       eax, DWORD PTR [esi+esi*8]                    ;129.3
        mov       DWORD PTR [1000+esp], eax                     ;129.3
        add       edi, eax                                      ;129.3
        imul      eax, DWORD PTR [1312+esp], -36                ;129.3
        mov       esi, 1                                        ;129.3
        mov       DWORD PTR [16+eax+edi], esi                   ;129.3
        mov       DWORD PTR [32+eax+edi], esi                   ;129.3
        mov       esi, DWORD PTR [952+esp]                      ;129.32
        neg       esi                                           ;129.3
        add       esi, ebx                                      ;129.3
        mov       ebx, DWORD PTR [920+esp]                      ;129.3
        push      edx                                           ;129.3
        mov       DWORD PTR [4+eax+edi], edx                    ;129.3
        mov       esi, DWORD PTR [ebx+esi*4]                    ;129.3
        test      esi, esi                                      ;129.3
        mov       DWORD PTR [28+eax+edi], edx                   ;129.3
        cmovl     esi, ecx                                      ;129.3
        push      esi                                           ;129.3
        push      2                                             ;129.3
        mov       DWORD PTR [12+eax+edi], 5                     ;129.3
        lea       edx, DWORD PTR [1368+esp]                     ;129.3
        push      edx                                           ;129.3
        mov       DWORD PTR [8+eax+edi], ecx                    ;129.3
        mov       DWORD PTR [24+eax+edi], esi                   ;129.3
        lea       edi, DWORD PTR [1336+esp]                     ;129.3
        mov       ebx, DWORD PTR [1024+esp]                     ;129.3
        call      _for_check_mult_overflow                      ;129.3
                                ; LOE eax ebx edi bl bh
.B2.73:                         ; Preds .B2.72
        imul      edx, DWORD PTR [1328+esp], -36                ;129.3
        and       eax, 1                                        ;129.3
        shl       eax, 4                                        ;129.3
        add       edx, DWORD PTR [1296+esp]                     ;129.3
        or        eax, 262144                                   ;129.3
        add       edx, DWORD PTR [1016+esp]                     ;129.3
        push      eax                                           ;129.3
        push      edx                                           ;129.3
        push      DWORD PTR [1380+esp]                          ;129.3
        call      _for_allocate                                 ;129.3
                                ; LOE ebx edi bl bh
.B2.74:                         ; Preds .B2.73
        xor       edx, edx                                      ;130.3
        mov       DWORD PTR [1348+esp], edx                     ;130.3
        push      32                                            ;130.3
        push      OFFSET FLAT: RESORT_OUTPUT_VEH$format_pack.0.2 ;130.3
        push      edx                                           ;130.3
        push      OFFSET FLAT: __STRLITPACK_98.0.2              ;130.3
        push      -2088435968                                   ;130.3
        push      98                                            ;130.3
        push      edi                                           ;130.3
        call      _for_read_seq_fmt                             ;130.3
                                ; LOE ebx edi bl bh
.B2.268:                        ; Preds .B2.74
        add       esp, 96                                       ;130.3
                                ; LOE ebx edi bl bh
.B2.75:                         ; Preds .B2.268
        mov       ecx, ebx                                      ;130.3
        sub       ecx, DWORD PTR [912+esp]                      ;130.3
        mov       edx, DWORD PTR [880+esp]                      ;130.3
        mov       ecx, DWORD PTR [edx+ecx*4]                    ;130.3
        test      ecx, ecx                                      ;130.3
        jle       .B2.80        ; Prob 2%                       ;130.3
                                ; LOE ecx ebx edi bl bh
.B2.76:                         ; Preds .B2.75
        mov       DWORD PTR [968+esp], ebx                      ;
        mov       esi, 1                                        ;
        mov       DWORD PTR [984+esp], ecx                      ;
        mov       ebx, DWORD PTR [960+esp]                      ;
                                ; LOE ebx esi edi
.B2.77:                         ; Preds .B2.78 .B2.76
        imul      eax, DWORD PTR [1272+esp], -36                ;130.3
        lea       ecx, DWORD PTR [1456+esp]                     ;130.3
        add       eax, DWORD PTR [1240+esp]                     ;130.3
        mov       DWORD PTR [1456+esp], 4                       ;130.3
        mov       edx, DWORD PTR [32+ebx+eax]                   ;130.17
        neg       edx                                           ;130.3
        add       edx, esi                                      ;130.3
        imul      edx, DWORD PTR [28+ebx+eax]                   ;130.3
        add       edx, DWORD PTR [ebx+eax]                      ;130.3
        mov       DWORD PTR [1460+esp], edx                     ;130.3
        push      ecx                                           ;130.3
        push      OFFSET FLAT: __STRLITPACK_99.0.2              ;130.3
        push      edi                                           ;130.3
        call      _for_read_seq_fmt_xmit                        ;130.3
                                ; LOE ebx esi edi
.B2.269:                        ; Preds .B2.77
        add       esp, 12                                       ;130.3
                                ; LOE ebx esi edi
.B2.78:                         ; Preds .B2.269
        inc       esi                                           ;130.3
        cmp       esi, DWORD PTR [984+esp]                      ;130.3
        jle       .B2.77        ; Prob 82%                      ;130.3
                                ; LOE ebx esi edi
.B2.79:                         ; Preds .B2.78
        mov       ebx, DWORD PTR [968+esp]                      ;
                                ; LOE ebx edi bl bh
.B2.80:                         ; Preds .B2.79 .B2.75
        push      0                                             ;130.3
        push      OFFSET FLAT: __STRLITPACK_100.0.2             ;130.3
        push      edi                                           ;130.3
        call      _for_read_seq_fmt_xmit                        ;130.3
                                ; LOE ebx edi bl bh
.B2.270:                        ; Preds .B2.80
        add       esp, 12                                       ;130.3
                                ; LOE ebx edi bl bh
.B2.81:                         ; Preds .B2.270
        inc       ebx                                           ;131.1
        cmp       ebx, DWORD PTR [276+esp]                      ;131.1
        jle       .B2.53        ; Prob 82%                      ;131.1
                                ; LOE ebx edi
.B2.82:                         ; Preds .B2.81
        mov       esi, 1                                        ;
        xor       ebx, ebx                                      ;
                                ; LOE ebx esi
.B2.83:                         ; Preds .B2.82 .B2.51
        push      OFFSET FLAT: __NLITPACK_1.0.2                 ;133.6
        push      OFFSET FLAT: _DYNUST_MAIN_MODULE_mp_TOTALVEH  ;133.6
        push      DWORD PTR [192+esp]                           ;133.6
        call      _IMSLSORT                                     ;133.6
                                ; LOE ebx esi
.B2.271:                        ; Preds .B2.83
        add       esp, 12                                       ;133.6
                                ; LOE ebx esi
.B2.84:                         ; Preds .B2.271
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TOTALVEH] ;134.1
        test      eax, eax                                      ;134.1
        mov       DWORD PTR [1348+esp], eax                     ;134.1
        jle       .B2.114       ; Prob 2%                       ;134.1
                                ; LOE ebx esi
.B2.85:                         ; Preds .B2.84
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_FUELOUT] ;136.3
        lea       ebx, DWORD PTR [1280+esp]                     ;
        mov       DWORD PTR [1332+esp], edi                     ;136.3
        imul      edi, DWORD PTR [1272+esp], -36                ;
        mov       edx, DWORD PTR [216+esp]                      ;135.8
        add       edi, DWORD PTR [1240+esp]                     ;
        shl       edx, 2                                        ;
        mov       eax, DWORD PTR [184+esp]                      ;135.3
        mov       DWORD PTR [1276+esp], edi                     ;
        sub       eax, edx                                      ;
        mov       edx, DWORD PTR [752+esp]                      ;
        mov       edi, DWORD PTR [792+esp]                      ;
        shl       edx, 2                                        ;
        shl       edi, 2                                        ;
        neg       edx                                           ;
        neg       edi                                           ;
        add       edx, DWORD PTR [720+esp]                      ;
        add       edi, DWORD PTR [760+esp]                      ;
        mov       DWORD PTR [1020+esp], edx                     ;
        mov       DWORD PTR [1036+esp], edi                     ;
        mov       edx, DWORD PTR [632+esp]                      ;
        mov       edi, DWORD PTR [912+esp]                      ;
        shl       edx, 2                                        ;
        shl       edi, 2                                        ;
        neg       edx                                           ;
        mov       ecx, DWORD PTR [228+esp]                      ;135.8
        neg       edi                                           ;
        neg       ecx                                           ;
        add       ecx, 2                                        ;
        add       edx, DWORD PTR [600+esp]                      ;
        add       edi, DWORD PTR [880+esp]                      ;
        imul      ecx, DWORD PTR [224+esp]                      ;
        mov       DWORD PTR [1028+esp], edx                     ;
        add       ecx, eax                                      ;
        mov       DWORD PTR [1076+esp], edi                     ;
        mov       edx, DWORD PTR [552+esp]                      ;
        mov       edi, DWORD PTR [512+esp]                      ;
        shl       edx, 2                                        ;
        shl       edi, 2                                        ;
        neg       edx                                           ;
        neg       edi                                           ;
        add       edx, DWORD PTR [520+esp]                      ;
        add       edi, DWORD PTR [480+esp]                      ;
        mov       DWORD PTR [1052+esp], edx                     ;
        mov       DWORD PTR [1092+esp], edi                     ;
        mov       eax, DWORD PTR [952+esp]                      ;
        mov       edx, DWORD PTR [352+esp]                      ;
        mov       edi, DWORD PTR [312+esp]                      ;
        shl       eax, 2                                        ;
        shl       edx, 2                                        ;
        neg       eax                                           ;
        shl       edi, 2                                        ;
        neg       edx                                           ;
        neg       edi                                           ;
        add       eax, DWORD PTR [920+esp]                      ;
        add       edx, DWORD PTR [320+esp]                      ;
        add       edi, DWORD PTR [280+esp]                      ;
        mov       DWORD PTR [1364+esp], eax                     ;
        mov       DWORD PTR [1004+esp], edx                     ;
        mov       DWORD PTR [1044+esp], edi                     ;
        mov       eax, DWORD PTR [712+esp]                      ;
        mov       edx, DWORD PTR [472+esp]                      ;
        mov       edi, DWORD PTR [432+esp]                      ;
        shl       eax, 2                                        ;
        shl       edx, 2                                        ;
        neg       eax                                           ;
        shl       edi, 2                                        ;
        neg       edx                                           ;
        neg       edi                                           ;
        add       eax, DWORD PTR [680+esp]                      ;
        add       edx, DWORD PTR [440+esp]                      ;
        add       edi, DWORD PTR [400+esp]                      ;
        mov       DWORD PTR [1084+esp], eax                     ;
        mov       DWORD PTR [988+esp], edx                      ;
        mov       DWORD PTR [996+esp], edi                      ;
        mov       eax, DWORD PTR [672+esp]                      ;
        mov       edx, DWORD PTR [272+esp]                      ;
        mov       edi, DWORD PTR [872+esp]                      ;
        shl       eax, 2                                        ;
        shl       edx, 2                                        ;
        neg       eax                                           ;
        shl       edi, 2                                        ;
        neg       edx                                           ;
        neg       edi                                           ;
        add       eax, DWORD PTR [640+esp]                      ;
        add       edx, DWORD PTR [240+esp]                      ;
        add       edi, DWORD PTR [840+esp]                      ;
        mov       DWORD PTR [1068+esp], eax                     ;
        mov       DWORD PTR [1012+esp], edx                     ;
        mov       DWORD PTR [1372+esp], edi                     ;
        mov       eax, DWORD PTR [592+esp]                      ;
        mov       edx, DWORD PTR [832+esp]                      ;
        mov       edi, DWORD PTR [392+esp]                      ;
        shl       eax, 2                                        ;
        shl       edx, 2                                        ;
        neg       eax                                           ;
        shl       edi, 2                                        ;
        neg       edx                                           ;
        neg       edi                                           ;
        add       eax, DWORD PTR [560+esp]                      ;
        add       edx, DWORD PTR [800+esp]                      ;
        add       edi, DWORD PTR [360+esp]                      ;
        mov       DWORD PTR [1340+esp], esi                     ;
        mov       DWORD PTR [1356+esp], edx                     ;
        mov       DWORD PTR [1060+esp], edi                     ;
        lea       edi, DWORD PTR [1440+esp]                     ;
        mov       DWORD PTR [276+esp], eax                      ;
        mov       DWORD PTR [1324+esp], ecx                     ;
                                ; LOE ebx
.B2.86:                         ; Preds .B2.112 .B2.85
        mov       esi, DWORD PTR [1324+esp]                     ;135.3
        mov       eax, DWORD PTR [1340+esp]                     ;135.3
        mov       edx, DWORD PTR [1364+esp]                     ;137.5
        mov       DWORD PTR [1280+esp], 0                       ;137.5
        mov       esi, DWORD PTR [esi+eax*4]                    ;135.3
        cmp       DWORD PTR [1332+esp], 0                       ;136.14
        mov       eax, DWORD PTR [edx+esi*4]                    ;137.5
        jle       .B2.207       ; Prob 16%                      ;136.14
                                ; LOE eax ebx esi
.B2.87:                         ; Preds .B2.86
        mov       DWORD PTR [1312+esp], eax                     ;137.5
        lea       eax, DWORD PTR [1312+esp]                     ;137.5
        push      32                                            ;137.5
        push      OFFSET FLAT: RESORT_OUTPUT_VEH$format_pack.0.2+52 ;137.5
        push      eax                                           ;137.5
        push      OFFSET FLAT: __STRLITPACK_101.0.2             ;137.5
        push      -2088435968                                   ;137.5
        push      997                                           ;137.5
        push      ebx                                           ;137.5
        call      _for_write_seq_fmt                            ;137.5
                                ; LOE ebx esi
.B2.88:                         ; Preds .B2.87
        mov       eax, DWORD PTR [1048+esp]                     ;137.5
        lea       ecx, DWORD PTR [1348+esp]                     ;137.5
        mov       edx, DWORD PTR [eax+esi*4]                    ;137.5
        mov       DWORD PTR [1348+esp], edx                     ;137.5
        push      ecx                                           ;137.5
        push      OFFSET FLAT: __STRLITPACK_102.0.2             ;137.5
        push      ebx                                           ;137.5
        call      _for_write_seq_fmt_xmit                       ;137.5
                                ; LOE ebx esi
.B2.89:                         ; Preds .B2.88
        mov       eax, DWORD PTR [1124+esp]                     ;137.5
        lea       ecx, DWORD PTR [1368+esp]                     ;137.5
        mov       edx, DWORD PTR [eax+esi*4]                    ;137.5
        mov       DWORD PTR [1368+esp], edx                     ;137.5
        push      ecx                                           ;137.5
        push      OFFSET FLAT: __STRLITPACK_103.0.2             ;137.5
        push      ebx                                           ;137.5
        call      _for_write_seq_fmt_xmit                       ;137.5
                                ; LOE ebx esi
.B2.90:                         ; Preds .B2.89
        mov       eax, DWORD PTR [1088+esp]                     ;137.5
        lea       ecx, DWORD PTR [1388+esp]                     ;137.5
        mov       edx, DWORD PTR [eax+esi*4]                    ;137.5
        mov       DWORD PTR [1388+esp], edx                     ;137.5
        push      ecx                                           ;137.5
        push      OFFSET FLAT: __STRLITPACK_104.0.2             ;137.5
        push      ebx                                           ;137.5
        call      _for_write_seq_fmt_xmit                       ;137.5
                                ; LOE ebx esi
.B2.91:                         ; Preds .B2.90
        mov       eax, DWORD PTR [1132+esp]                     ;137.5
        lea       ecx, DWORD PTR [1408+esp]                     ;137.5
        mov       edx, DWORD PTR [eax+esi*4]                    ;137.5
        mov       DWORD PTR [1408+esp], edx                     ;137.5
        push      ecx                                           ;137.5
        push      OFFSET FLAT: __STRLITPACK_105.0.2             ;137.5
        push      ebx                                           ;137.5
        call      _for_write_seq_fmt_xmit                       ;137.5
                                ; LOE ebx esi
.B2.92:                         ; Preds .B2.91
        mov       eax, DWORD PTR [1104+esp]                     ;137.5
        lea       ecx, DWORD PTR [1428+esp]                     ;137.5
        mov       edx, DWORD PTR [eax+esi*4]                    ;137.5
        mov       DWORD PTR [1428+esp], edx                     ;137.5
        push      ecx                                           ;137.5
        push      OFFSET FLAT: __STRLITPACK_106.0.2             ;137.5
        push      ebx                                           ;137.5
        call      _for_write_seq_fmt_xmit                       ;137.5
                                ; LOE ebx esi
.B2.93:                         ; Preds .B2.92
        mov       eax, DWORD PTR [364+esp]                      ;137.5
        lea       ecx, DWORD PTR [1448+esp]                     ;137.5
        mov       edx, DWORD PTR [eax+esi*4]                    ;137.5
        mov       DWORD PTR [1448+esp], edx                     ;137.5
        push      ecx                                           ;137.5
        push      OFFSET FLAT: __STRLITPACK_107.0.2             ;137.5
        push      ebx                                           ;137.5
        call      _for_write_seq_fmt_xmit                       ;137.5
                                ; LOE ebx esi
.B2.94:                         ; Preds .B2.93
        mov       eax, DWORD PTR [1176+esp]                     ;137.5
        lea       ecx, DWORD PTR [1468+esp]                     ;137.5
        mov       edx, DWORD PTR [eax+esi*4]                    ;137.5
        mov       DWORD PTR [1488+esp], edx                     ;137.5
        mov       DWORD PTR [1468+esp], edx                     ;137.5
        push      ecx                                           ;137.5
        push      OFFSET FLAT: __STRLITPACK_108.0.2             ;137.5
        push      ebx                                           ;137.5
        call      _for_write_seq_fmt_xmit                       ;137.5
                                ; LOE ebx esi
.B2.95:                         ; Preds .B2.94
        mov       eax, DWORD PTR [1164+esp]                     ;137.5
        lea       ecx, DWORD PTR [1488+esp]                     ;137.5
        mov       edx, DWORD PTR [eax+esi*4]                    ;137.5
        mov       DWORD PTR [1488+esp], edx                     ;137.5
        push      ecx                                           ;137.5
        push      OFFSET FLAT: __STRLITPACK_109.0.2             ;137.5
        push      ebx                                           ;137.5
        call      _for_write_seq_fmt_xmit                       ;137.5
                                ; LOE ebx esi
.B2.272:                        ; Preds .B2.95
        add       esp, 124                                      ;137.5
                                ; LOE ebx esi
.B2.96:                         ; Preds .B2.272
        mov       eax, DWORD PTR [1092+esp]                     ;137.5
        lea       ecx, DWORD PTR [1384+esp]                     ;137.5
        mov       edx, DWORD PTR [eax+esi*4]                    ;137.5
        mov       DWORD PTR [1384+esp], edx                     ;137.5
        push      ecx                                           ;137.5
        push      OFFSET FLAT: __STRLITPACK_110.0.2             ;137.5
        push      ebx                                           ;137.5
        call      _for_write_seq_fmt_xmit                       ;137.5
                                ; LOE ebx esi
.B2.97:                         ; Preds .B2.96
        mov       eax, DWORD PTR [1016+esp]                     ;137.5
        lea       ecx, DWORD PTR [1404+esp]                     ;137.5
        mov       edx, DWORD PTR [eax+esi*4]                    ;137.5
        mov       DWORD PTR [1404+esp], edx                     ;137.5
        push      ecx                                           ;137.5
        push      OFFSET FLAT: __STRLITPACK_111.0.2             ;137.5
        push      ebx                                           ;137.5
        call      _for_write_seq_fmt_xmit                       ;137.5
                                ; LOE ebx esi
.B2.98:                         ; Preds .B2.97
        mov       eax, DWORD PTR [1068+esp]                     ;137.5
        lea       ecx, DWORD PTR [1424+esp]                     ;137.5
        mov       edx, DWORD PTR [eax+esi*4]                    ;137.5
        mov       DWORD PTR [1424+esp], edx                     ;137.5
        push      ecx                                           ;137.5
        push      OFFSET FLAT: __STRLITPACK_112.0.2             ;137.5
        push      ebx                                           ;137.5
        call      _for_write_seq_fmt_xmit                       ;137.5
                                ; LOE ebx esi
.B2.99:                         ; Preds .B2.98
        mov       eax, DWORD PTR [1024+esp]                     ;137.5
        lea       ecx, DWORD PTR [1444+esp]                     ;137.5
        mov       edx, DWORD PTR [eax+esi*4]                    ;137.5
        mov       DWORD PTR [1444+esp], edx                     ;137.5
        push      ecx                                           ;137.5
        push      OFFSET FLAT: __STRLITPACK_113.0.2             ;137.5
        push      ebx                                           ;137.5
        call      _for_write_seq_fmt_xmit                       ;137.5
                                ; LOE ebx esi
.B2.100:                        ; Preds .B2.99
        mov       eax, DWORD PTR [1044+esp]                     ;137.5
        lea       ecx, DWORD PTR [1464+esp]                     ;137.5
        mov       edx, DWORD PTR [eax+esi*4]                    ;137.5
        mov       DWORD PTR [1464+esp], edx                     ;137.5
        push      ecx                                           ;137.5
        push      OFFSET FLAT: __STRLITPACK_114.0.2             ;137.5
        push      ebx                                           ;137.5
        call      _for_write_seq_fmt_xmit                       ;137.5
                                ; LOE ebx esi
.B2.101:                        ; Preds .B2.100
        mov       eax, DWORD PTR [1072+esp]                     ;137.5
        lea       ecx, DWORD PTR [1484+esp]                     ;137.5
        mov       edx, DWORD PTR [eax+esi*4]                    ;137.5
        mov       DWORD PTR [1484+esp], edx                     ;137.5
        push      ecx                                           ;137.5
        push      OFFSET FLAT: __STRLITPACK_115.0.2             ;137.5
        push      ebx                                           ;137.5
        call      _for_write_seq_fmt_xmit                       ;137.5
                                ; LOE ebx esi
.B2.102:                        ; Preds .B2.101
        mov       eax, DWORD PTR [1132+esp]                     ;137.5
        lea       ecx, DWORD PTR [1504+esp]                     ;137.5
        mov       edx, DWORD PTR [eax+esi*4]                    ;137.5
        mov       DWORD PTR [1504+esp], edx                     ;137.5
        push      ecx                                           ;137.5
        push      OFFSET FLAT: __STRLITPACK_116.0.2             ;137.5
        push      ebx                                           ;137.5
        call      _for_write_seq_fmt_xmit                       ;137.5
                                ; LOE ebx esi
.B2.273:                        ; Preds .B2.102
        add       esp, 84                                       ;137.5
                                ; LOE ebx esi
.B2.103:                        ; Preds .B2.273 .B2.283
        mov       eax, DWORD PTR [1372+esp]                     ;141.3
        mov       DWORD PTR [1280+esp], 0                       ;141.3
        mov       edx, DWORD PTR [eax+esi*4]                    ;141.3
        mov       DWORD PTR [1440+esp], edx                     ;141.3
        push      32                                            ;141.3
        push      OFFSET FLAT: RESORT_OUTPUT_VEH$format_pack.0.2+20 ;141.3
        lea       ecx, DWORD PTR [1448+esp]                     ;141.3
        push      ecx                                           ;141.3
        push      OFFSET FLAT: __STRLITPACK_132.0.2             ;141.3
        push      -2088435968                                   ;141.3
        push      997                                           ;141.3
        push      ebx                                           ;141.3
        call      _for_write_seq_fmt                            ;141.3
                                ; LOE ebx esi
.B2.104:                        ; Preds .B2.103
        mov       eax, DWORD PTR [1384+esp]                     ;141.3
        lea       ecx, DWORD PTR [1476+esp]                     ;141.3
        mov       edx, DWORD PTR [eax+esi*4]                    ;141.3
        mov       DWORD PTR [1476+esp], edx                     ;141.3
        push      ecx                                           ;141.3
        push      OFFSET FLAT: __STRLITPACK_133.0.2             ;141.3
        push      ebx                                           ;141.3
        call      _for_write_seq_fmt_xmit                       ;141.3
                                ; LOE ebx esi
.B2.105:                        ; Preds .B2.104
        xor       eax, eax                                      ;142.3
        mov       DWORD PTR [1320+esp], eax                     ;142.3
        push      32                                            ;142.3
        push      OFFSET FLAT: RESORT_OUTPUT_VEH$format_pack.0.2 ;142.3
        push      eax                                           ;142.3
        push      OFFSET FLAT: __STRLITPACK_134.0.2             ;142.3
        push      -2088435968                                   ;142.3
        push      998                                           ;142.3
        push      ebx                                           ;142.3
        call      _for_write_seq_fmt                            ;142.3
                                ; LOE ebx esi
.B2.274:                        ; Preds .B2.105
        add       esp, 68                                       ;142.3
                                ; LOE ebx esi
.B2.106:                        ; Preds .B2.274
        cmp       DWORD PTR [1388+esp], 0                       ;142.3
        jle       .B2.111       ; Prob 2%                       ;142.3
                                ; LOE ebx esi
.B2.107:                        ; Preds .B2.106
        shl       esi, 2                                        ;119.1
        mov       edx, 1                                        ;
        mov       ebx, edx                                      ;
        lea       ecx, DWORD PTR [esi+esi*8]                    ;119.1
        mov       esi, DWORD PTR [1276+esp]                     ;142.19
        mov       edi, DWORD PTR [ecx+esi]                      ;142.19
        mov       eax, DWORD PTR [32+ecx+esi]                   ;142.19
        mov       esi, DWORD PTR [28+ecx+esi]                   ;142.19
        imul      eax, esi                                      ;
        mov       DWORD PTR [1380+esp], esi                     ;142.19
        sub       edi, eax                                      ;
                                ; LOE ebx esi edi
.B2.108:                        ; Preds .B2.109 .B2.107
        mov       eax, DWORD PTR [esi+edi]                      ;142.3
        lea       ecx, DWORD PTR [1464+esp]                     ;142.3
        mov       DWORD PTR [1464+esp], eax                     ;142.3
        push      ecx                                           ;142.3
        push      OFFSET FLAT: __STRLITPACK_135.0.2             ;142.3
        lea       eax, DWORD PTR [1288+esp]                     ;142.3
        push      eax                                           ;142.3
        call      _for_write_seq_fmt_xmit                       ;142.3
                                ; LOE ebx esi edi
.B2.275:                        ; Preds .B2.108
        add       esp, 12                                       ;142.3
                                ; LOE ebx esi edi
.B2.109:                        ; Preds .B2.275
        inc       ebx                                           ;142.3
        add       esi, DWORD PTR [1380+esp]                     ;142.3
        cmp       ebx, DWORD PTR [1388+esp]                     ;142.3
        jle       .B2.108       ; Prob 82%                      ;142.3
                                ; LOE ebx esi edi
.B2.110:                        ; Preds .B2.109
        lea       ebx, DWORD PTR [1280+esp]                     ;
                                ; LOE ebx
.B2.111:                        ; Preds .B2.110 .B2.106
        push      0                                             ;142.3
        push      OFFSET FLAT: __STRLITPACK_136.0.2             ;142.3
        push      ebx                                           ;142.3
        call      _for_write_seq_fmt_xmit                       ;142.3
                                ; LOE ebx
.B2.276:                        ; Preds .B2.111
        add       esp, 12                                       ;142.3
                                ; LOE ebx
.B2.112:                        ; Preds .B2.276
        mov       eax, DWORD PTR [1340+esp]                     ;143.1
        inc       eax                                           ;143.1
        mov       DWORD PTR [1340+esp], eax                     ;143.1
        cmp       eax, DWORD PTR [1348+esp]                     ;143.1
        jle       .B2.86        ; Prob 82%                      ;143.1
                                ; LOE ebx
.B2.113:                        ; Preds .B2.112
        mov       esi, 1                                        ;
        xor       ebx, ebx                                      ;
                                ; LOE ebx esi
.B2.114:                        ; Preds .B2.113 .B2.84
        mov       DWORD PTR [1280+esp], ebx                     ;145.7
        push      32                                            ;145.7
        push      ebx                                           ;145.7
        push      OFFSET FLAT: __STRLITPACK_137.0.2             ;145.7
        push      -2088435968                                   ;145.7
        push      97                                            ;145.7
        lea       eax, DWORD PTR [1300+esp]                     ;145.7
        push      eax                                           ;145.7
        call      _for_close                                    ;145.7
                                ; LOE ebx esi
.B2.115:                        ; Preds .B2.114
        mov       DWORD PTR [1304+esp], ebx                     ;146.7
        push      32                                            ;146.7
        push      ebx                                           ;146.7
        push      OFFSET FLAT: __STRLITPACK_138.0.2             ;146.7
        push      -2088435968                                   ;146.7
        push      98                                            ;146.7
        lea       eax, DWORD PTR [1324+esp]                     ;146.7
        push      eax                                           ;146.7
        call      _for_close                                    ;146.7
                                ; LOE ebx esi
.B2.116:                        ; Preds .B2.115
        mov       DWORD PTR [1328+esp], ebx                     ;147.7
        push      32                                            ;147.7
        push      ebx                                           ;147.7
        push      OFFSET FLAT: __STRLITPACK_139.0.2             ;147.7
        push      -2088435968                                   ;147.7
        push      997                                           ;147.7
        lea       eax, DWORD PTR [1348+esp]                     ;147.7
        push      eax                                           ;147.7
        call      _for_close                                    ;147.7
                                ; LOE ebx esi
.B2.117:                        ; Preds .B2.116
        mov       DWORD PTR [1352+esp], ebx                     ;148.7
        push      32                                            ;148.7
        push      ebx                                           ;148.7
        push      OFFSET FLAT: __STRLITPACK_140.0.2             ;148.7
        push      -2088435968                                   ;148.7
        push      998                                           ;148.7
        lea       eax, DWORD PTR [1372+esp]                     ;148.7
        push      eax                                           ;148.7
        call      _for_close                                    ;148.7
                                ; LOE esi
.B2.118:                        ; Preds .B2.117
        push      40                                            ;151.6
        push      OFFSET FLAT: __STRLITPACK_42                  ;151.6
        call      _SYSTEM                                       ;151.6
                                ; LOE esi
.B2.119:                        ; Preds .B2.118
        push      49                                            ;152.6
        push      OFFSET FLAT: __STRLITPACK_41                  ;152.6
        call      _SYSTEM                                       ;152.6
                                ; LOE esi
.B2.120:                        ; Preds .B2.119
        push      43                                            ;153.6
        push      OFFSET FLAT: __STRLITPACK_40                  ;153.6
        call      _SYSTEM                                       ;153.6
                                ; LOE esi
.B2.277:                        ; Preds .B2.120
        add       esp, 120                                      ;153.6
                                ; LOE esi
.B2.121:                        ; Preds .B2.277
        mov       ebx, DWORD PTR [196+esp]                      ;159.3
        mov       edx, ebx                                      ;159.3
        shr       edx, 1                                        ;159.3
        mov       eax, ebx                                      ;159.3
        and       edx, 1                                        ;159.3
        and       eax, 1                                        ;159.3
        shl       edx, 2                                        ;159.3
        add       eax, eax                                      ;159.3
        or        edx, eax                                      ;159.3
        or        edx, 262144                                   ;159.3
        push      edx                                           ;159.3
        push      DWORD PTR [188+esp]                           ;159.3
        call      _for_dealloc_allocatable                      ;159.3
                                ; LOE ebx esi
.B2.122:                        ; Preds .B2.121
        and       ebx, -2                                       ;159.3
        mov       DWORD PTR [204+esp], ebx                      ;159.3
        mov       ebx, DWORD PTR [940+esp]                      ;160.3
        mov       edx, ebx                                      ;160.3
        shr       edx, 1                                        ;160.3
        mov       eax, ebx                                      ;160.3
        and       edx, 1                                        ;160.3
        and       eax, 1                                        ;160.3
        shl       edx, 2                                        ;160.3
        add       eax, eax                                      ;160.3
        or        edx, eax                                      ;160.3
        or        edx, 262144                                   ;160.3
        mov       DWORD PTR [192+esp], 0                        ;159.3
        push      edx                                           ;160.3
        push      DWORD PTR [932+esp]                           ;160.3
        call      _for_dealloc_allocatable                      ;160.3
                                ; LOE ebx esi
.B2.123:                        ; Preds .B2.122
        and       ebx, -2                                       ;160.3
        mov       DWORD PTR [948+esp], ebx                      ;160.3
        mov       ebx, DWORD PTR [748+esp]                      ;161.3
        mov       edx, ebx                                      ;161.3
        shr       edx, 1                                        ;161.3
        mov       eax, ebx                                      ;161.3
        and       edx, 1                                        ;161.3
        and       eax, 1                                        ;161.3
        shl       edx, 2                                        ;161.3
        add       eax, eax                                      ;161.3
        or        edx, eax                                      ;161.3
        or        edx, 262144                                   ;161.3
        mov       DWORD PTR [936+esp], 0                        ;160.3
        push      edx                                           ;161.3
        push      DWORD PTR [740+esp]                           ;161.3
        call      _for_dealloc_allocatable                      ;161.3
                                ; LOE ebx esi
.B2.124:                        ; Preds .B2.123
        and       ebx, -2                                       ;161.3
        mov       DWORD PTR [756+esp], ebx                      ;161.3
        mov       ebx, DWORD PTR [716+esp]                      ;162.3
        mov       edx, ebx                                      ;162.3
        shr       edx, 1                                        ;162.3
        mov       eax, ebx                                      ;162.3
        and       edx, 1                                        ;162.3
        and       eax, 1                                        ;162.3
        shl       edx, 2                                        ;162.3
        add       eax, eax                                      ;162.3
        or        edx, eax                                      ;162.3
        or        edx, 262144                                   ;162.3
        mov       DWORD PTR [744+esp], 0                        ;161.3
        push      edx                                           ;162.3
        push      DWORD PTR [708+esp]                           ;162.3
        call      _for_dealloc_allocatable                      ;162.3
                                ; LOE ebx esi
.B2.125:                        ; Preds .B2.124
        and       ebx, -2                                       ;162.3
        mov       DWORD PTR [724+esp], ebx                      ;162.3
        mov       ebx, DWORD PTR [684+esp]                      ;163.3
        mov       edx, ebx                                      ;163.3
        shr       edx, 1                                        ;163.3
        mov       eax, ebx                                      ;163.3
        and       edx, 1                                        ;163.3
        and       eax, 1                                        ;163.3
        shl       edx, 2                                        ;163.3
        add       eax, eax                                      ;163.3
        or        edx, eax                                      ;163.3
        or        edx, 262144                                   ;163.3
        mov       DWORD PTR [712+esp], 0                        ;162.3
        push      edx                                           ;163.3
        push      DWORD PTR [676+esp]                           ;163.3
        call      _for_dealloc_allocatable                      ;163.3
                                ; LOE ebx esi
.B2.126:                        ; Preds .B2.125
        and       ebx, -2                                       ;163.3
        mov       DWORD PTR [692+esp], ebx                      ;163.3
        mov       ebx, DWORD PTR [652+esp]                      ;164.3
        mov       edx, ebx                                      ;164.3
        shr       edx, 1                                        ;164.3
        mov       eax, ebx                                      ;164.3
        and       edx, 1                                        ;164.3
        and       eax, 1                                        ;164.3
        shl       edx, 2                                        ;164.3
        add       eax, eax                                      ;164.3
        or        edx, eax                                      ;164.3
        or        edx, 262144                                   ;164.3
        mov       DWORD PTR [680+esp], 0                        ;163.3
        push      edx                                           ;164.3
        push      DWORD PTR [644+esp]                           ;164.3
        call      _for_dealloc_allocatable                      ;164.3
                                ; LOE ebx esi
.B2.127:                        ; Preds .B2.126
        and       ebx, -2                                       ;164.3
        mov       DWORD PTR [660+esp], ebx                      ;164.3
        mov       ebx, DWORD PTR [620+esp]                      ;165.3
        mov       edx, ebx                                      ;165.3
        shr       edx, 1                                        ;165.3
        mov       eax, ebx                                      ;165.3
        and       edx, 1                                        ;165.3
        and       eax, 1                                        ;165.3
        shl       edx, 2                                        ;165.3
        add       eax, eax                                      ;165.3
        or        edx, eax                                      ;165.3
        or        edx, 262144                                   ;165.3
        mov       DWORD PTR [648+esp], 0                        ;164.3
        push      edx                                           ;165.3
        push      DWORD PTR [612+esp]                           ;165.3
        call      _for_dealloc_allocatable                      ;165.3
                                ; LOE ebx esi
.B2.128:                        ; Preds .B2.127
        and       ebx, -2                                       ;165.3
        mov       DWORD PTR [628+esp], ebx                      ;165.3
        mov       ebx, DWORD PTR [948+esp]                      ;166.3
        mov       edx, ebx                                      ;166.3
        shr       edx, 1                                        ;166.3
        mov       eax, ebx                                      ;166.3
        and       edx, 1                                        ;166.3
        and       eax, 1                                        ;166.3
        shl       edx, 2                                        ;166.3
        add       eax, eax                                      ;166.3
        or        edx, eax                                      ;166.3
        or        edx, 262144                                   ;166.3
        mov       DWORD PTR [616+esp], 0                        ;165.3
        push      edx                                           ;166.3
        push      DWORD PTR [940+esp]                           ;166.3
        call      _for_dealloc_allocatable                      ;166.3
                                ; LOE ebx esi
.B2.129:                        ; Preds .B2.128
        and       ebx, -2                                       ;166.3
        mov       DWORD PTR [956+esp], ebx                      ;166.3
        mov       ebx, DWORD PTR [596+esp]                      ;167.3
        mov       edx, ebx                                      ;167.3
        shr       edx, 1                                        ;167.3
        mov       eax, ebx                                      ;167.3
        and       edx, 1                                        ;167.3
        and       eax, 1                                        ;167.3
        shl       edx, 2                                        ;167.3
        add       eax, eax                                      ;167.3
        or        edx, eax                                      ;167.3
        or        edx, 262144                                   ;167.3
        mov       DWORD PTR [944+esp], 0                        ;166.3
        push      edx                                           ;167.3
        push      DWORD PTR [588+esp]                           ;167.3
        call      _for_dealloc_allocatable                      ;167.3
                                ; LOE ebx esi
.B2.130:                        ; Preds .B2.129
        and       ebx, -2                                       ;167.3
        mov       DWORD PTR [604+esp], ebx                      ;167.3
        mov       ebx, DWORD PTR [564+esp]                      ;168.3
        mov       edx, ebx                                      ;168.3
        shr       edx, 1                                        ;168.3
        mov       eax, ebx                                      ;168.3
        and       edx, 1                                        ;168.3
        and       eax, 1                                        ;168.3
        shl       edx, 2                                        ;168.3
        add       eax, eax                                      ;168.3
        or        edx, eax                                      ;168.3
        or        edx, 262144                                   ;168.3
        mov       DWORD PTR [592+esp], 0                        ;167.3
        push      edx                                           ;168.3
        push      DWORD PTR [556+esp]                           ;168.3
        call      _for_dealloc_allocatable                      ;168.3
                                ; LOE ebx esi
.B2.131:                        ; Preds .B2.130
        and       ebx, -2                                       ;168.3
        mov       DWORD PTR [572+esp], ebx                      ;168.3
        mov       ebx, DWORD PTR [532+esp]                      ;169.3
        mov       edx, ebx                                      ;169.3
        shr       edx, 1                                        ;169.3
        mov       eax, ebx                                      ;169.3
        and       edx, 1                                        ;169.3
        and       eax, 1                                        ;169.3
        shl       edx, 2                                        ;169.3
        add       eax, eax                                      ;169.3
        or        edx, eax                                      ;169.3
        or        edx, 262144                                   ;169.3
        mov       DWORD PTR [560+esp], 0                        ;168.3
        push      edx                                           ;169.3
        push      DWORD PTR [524+esp]                           ;169.3
        call      _for_dealloc_allocatable                      ;169.3
                                ; LOE ebx esi
.B2.132:                        ; Preds .B2.131
        and       ebx, -2                                       ;169.3
        mov       DWORD PTR [540+esp], ebx                      ;169.3
        mov       ebx, DWORD PTR [500+esp]                      ;170.3
        mov       edx, ebx                                      ;170.3
        shr       edx, 1                                        ;170.3
        mov       eax, ebx                                      ;170.3
        and       edx, 1                                        ;170.3
        and       eax, 1                                        ;170.3
        shl       edx, 2                                        ;170.3
        add       eax, eax                                      ;170.3
        or        edx, eax                                      ;170.3
        or        edx, 262144                                   ;170.3
        mov       DWORD PTR [528+esp], 0                        ;169.3
        push      edx                                           ;170.3
        push      DWORD PTR [492+esp]                           ;170.3
        call      _for_dealloc_allocatable                      ;170.3
                                ; LOE ebx esi
.B2.133:                        ; Preds .B2.132
        and       ebx, -2                                       ;170.3
        mov       DWORD PTR [508+esp], ebx                      ;170.3
        mov       ebx, DWORD PTR [468+esp]                      ;171.3
        mov       edx, ebx                                      ;171.3
        shr       edx, 1                                        ;171.3
        mov       eax, ebx                                      ;171.3
        and       edx, 1                                        ;171.3
        and       eax, 1                                        ;171.3
        shl       edx, 2                                        ;171.3
        add       eax, eax                                      ;171.3
        or        edx, eax                                      ;171.3
        or        edx, 262144                                   ;171.3
        mov       DWORD PTR [496+esp], 0                        ;170.3
        push      edx                                           ;171.3
        push      DWORD PTR [460+esp]                           ;171.3
        call      _for_dealloc_allocatable                      ;171.3
                                ; LOE ebx esi
.B2.134:                        ; Preds .B2.133
        and       ebx, -2                                       ;171.3
        mov       DWORD PTR [476+esp], ebx                      ;171.3
        mov       ebx, DWORD PTR [876+esp]                      ;172.3
        mov       edx, ebx                                      ;172.3
        shr       edx, 1                                        ;172.3
        mov       eax, ebx                                      ;172.3
        and       edx, 1                                        ;172.3
        and       eax, 1                                        ;172.3
        shl       edx, 2                                        ;172.3
        add       eax, eax                                      ;172.3
        or        edx, eax                                      ;172.3
        or        edx, 262144                                   ;172.3
        mov       DWORD PTR [464+esp], 0                        ;171.3
        push      edx                                           ;172.3
        push      DWORD PTR [868+esp]                           ;172.3
        call      _for_dealloc_allocatable                      ;172.3
                                ; LOE ebx esi
.B2.135:                        ; Preds .B2.134
        and       ebx, -2                                       ;172.3
        mov       DWORD PTR [884+esp], ebx                      ;172.3
        mov       ebx, DWORD PTR [444+esp]                      ;173.3
        mov       edx, ebx                                      ;173.3
        shr       edx, 1                                        ;173.3
        mov       eax, ebx                                      ;173.3
        and       edx, 1                                        ;173.3
        and       eax, 1                                        ;173.3
        shl       edx, 2                                        ;173.3
        add       eax, eax                                      ;173.3
        or        edx, eax                                      ;173.3
        or        edx, 262144                                   ;173.3
        mov       DWORD PTR [872+esp], 0                        ;172.3
        push      edx                                           ;173.3
        push      DWORD PTR [436+esp]                           ;173.3
        call      _for_dealloc_allocatable                      ;173.3
                                ; LOE ebx esi
.B2.278:                        ; Preds .B2.135
        add       esp, 120                                      ;173.3
                                ; LOE ebx esi
.B2.136:                        ; Preds .B2.278
        and       ebx, -2                                       ;173.3
        mov       DWORD PTR [332+esp], ebx                      ;173.3
        mov       ebx, DWORD PTR [292+esp]                      ;174.3
        mov       edx, ebx                                      ;174.3
        shr       edx, 1                                        ;174.3
        mov       eax, ebx                                      ;174.3
        and       edx, 1                                        ;174.3
        and       eax, 1                                        ;174.3
        shl       edx, 2                                        ;174.3
        add       eax, eax                                      ;174.3
        or        edx, eax                                      ;174.3
        or        edx, 262144                                   ;174.3
        mov       DWORD PTR [320+esp], 0                        ;173.3
        push      edx                                           ;174.3
        push      DWORD PTR [284+esp]                           ;174.3
        call      _for_dealloc_allocatable                      ;174.3
                                ; LOE ebx esi
.B2.137:                        ; Preds .B2.136
        and       ebx, -2                                       ;174.3
        mov       DWORD PTR [300+esp], ebx                      ;174.3
        mov       ebx, DWORD PTR [260+esp]                      ;175.3
        mov       edx, ebx                                      ;175.3
        shr       edx, 1                                        ;175.3
        mov       eax, ebx                                      ;175.3
        and       edx, 1                                        ;175.3
        and       eax, 1                                        ;175.3
        shl       edx, 2                                        ;175.3
        add       eax, eax                                      ;175.3
        or        edx, eax                                      ;175.3
        or        edx, 262144                                   ;175.3
        mov       DWORD PTR [288+esp], 0                        ;174.3
        push      edx                                           ;175.3
        push      DWORD PTR [252+esp]                           ;175.3
        call      _for_dealloc_allocatable                      ;175.3
                                ; LOE ebx esi
.B2.138:                        ; Preds .B2.137
        and       ebx, -2                                       ;175.3
        mov       DWORD PTR [268+esp], ebx                      ;175.3
        mov       ebx, DWORD PTR [868+esp]                      ;176.3
        mov       edx, ebx                                      ;176.3
        shr       edx, 1                                        ;176.3
        mov       eax, ebx                                      ;176.3
        and       edx, 1                                        ;176.3
        and       eax, 1                                        ;176.3
        shl       edx, 2                                        ;176.3
        add       eax, eax                                      ;176.3
        or        edx, eax                                      ;176.3
        or        edx, 262144                                   ;176.3
        mov       DWORD PTR [256+esp], 0                        ;175.3
        push      edx                                           ;176.3
        push      DWORD PTR [860+esp]                           ;176.3
        call      _for_dealloc_allocatable                      ;176.3
                                ; LOE ebx esi
.B2.139:                        ; Preds .B2.138
        and       ebx, -2                                       ;176.3
        mov       DWORD PTR [876+esp], ebx                      ;176.3
        mov       ebx, DWORD PTR [836+esp]                      ;177.3
        mov       edx, ebx                                      ;177.3
        shr       edx, 1                                        ;177.3
        mov       eax, ebx                                      ;177.3
        and       edx, 1                                        ;177.3
        and       eax, 1                                        ;177.3
        shl       edx, 2                                        ;177.3
        add       eax, eax                                      ;177.3
        or        edx, eax                                      ;177.3
        or        edx, 262144                                   ;177.3
        mov       DWORD PTR [864+esp], 0                        ;176.3
        push      edx                                           ;177.3
        push      DWORD PTR [828+esp]                           ;177.3
        call      _for_dealloc_allocatable                      ;177.3
                                ; LOE ebx esi
.B2.279:                        ; Preds .B2.139
        add       esp, 32                                       ;177.3
                                ; LOE ebx esi
.B2.140:                        ; Preds .B2.279
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TOTALVEH] ;178.3
        and       ebx, -2                                       ;177.3
        mov       DWORD PTR [800+esp], 0                        ;177.3
        test      edx, edx                                      ;178.3
        mov       DWORD PTR [812+esp], ebx                      ;177.3
        mov       eax, DWORD PTR [1240+esp]                     ;181.3
        jle       .B2.145       ; Prob 28%                      ;178.3
                                ; LOE eax edx esi
.B2.141:                        ; Preds .B2.140
        imul      ebx, DWORD PTR [1272+esp], -36                ;
        mov       DWORD PTR [276+esp], eax                      ;
        add       ebx, eax                                      ;
        mov       DWORD PTR [988+esp], edx                      ;
                                ; LOE ebx esi
.B2.142:                        ; Preds .B2.143 .B2.141
        lea       edi, DWORD PTR [esi*4]                        ;179.5
        lea       edi, DWORD PTR [edi+edi*8]                    ;179.5
        mov       eax, DWORD PTR [12+edi+ebx]                   ;179.16
        mov       edx, eax                                      ;179.5
        shr       edx, 1                                        ;179.5
        and       eax, 1                                        ;179.5
        and       edx, 1                                        ;179.5
        add       eax, eax                                      ;179.5
        shl       edx, 2                                        ;179.5
        or        edx, eax                                      ;179.5
        or        edx, 262144                                   ;179.5
        push      edx                                           ;179.5
        push      DWORD PTR [edi+ebx]                           ;179.5
        call      _for_dealloc_allocatable                      ;179.5
                                ; LOE ebx esi edi
.B2.280:                        ; Preds .B2.142
        add       esp, 8                                        ;179.5
                                ; LOE ebx esi edi
.B2.143:                        ; Preds .B2.280
        inc       esi                                           ;180.3
        and       DWORD PTR [12+ebx+edi], -2                    ;179.5
        mov       DWORD PTR [ebx+edi], 0                        ;179.5
        cmp       esi, DWORD PTR [988+esp]                      ;180.3
        jle       .B2.142       ; Prob 82%                      ;180.3
                                ; LOE ebx esi
.B2.144:                        ; Preds .B2.143
        mov       eax, DWORD PTR [276+esp]                      ;
                                ; LOE eax
.B2.145:                        ; Preds .B2.144 .B2.140
        mov       ebx, DWORD PTR [1252+esp]                     ;181.3
        mov       ecx, ebx                                      ;181.3
        shr       ecx, 1                                        ;181.3
        mov       edx, ebx                                      ;181.3
        and       ecx, 1                                        ;181.3
        and       edx, 1                                        ;181.3
        shl       ecx, 2                                        ;181.3
        add       edx, edx                                      ;181.3
        or        ecx, edx                                      ;181.3
        or        ecx, 262144                                   ;181.3
        push      ecx                                           ;181.3
        push      eax                                           ;181.3
        call      _for_dealloc_allocatable                      ;181.3
                                ; LOE ebx
.B2.281:                        ; Preds .B2.145
        add       esp, 8                                        ;181.3
                                ; LOE ebx
.B2.146:                        ; Preds .B2.281
        and       ebx, -2                                       ;181.3
        mov       DWORD PTR [1240+esp], 0                       ;181.3
        mov       DWORD PTR [1252+esp], ebx                     ;181.3
                                ; LOE
.B2.166:                        ; Preds .B2.146
        add       esp, 1476                                     ;182.1
        pop       ebx                                           ;182.1
        pop       edi                                           ;182.1
        pop       esi                                           ;182.1
        mov       esp, ebp                                      ;182.1
        pop       ebp                                           ;182.1
        ret                                                     ;182.1
                                ; LOE
.B2.207:                        ; Preds .B2.86                  ; Infreq
        mov       DWORD PTR [960+esp], eax                      ;139.5
        lea       eax, DWORD PTR [960+esp]                      ;139.5
        push      32                                            ;139.5
        push      OFFSET FLAT: RESORT_OUTPUT_VEH$format_pack.0.2+52 ;139.5
        push      eax                                           ;139.5
        push      OFFSET FLAT: __STRLITPACK_117.0.2             ;139.5
        push      -2088435968                                   ;139.5
        push      997                                           ;139.5
        push      ebx                                           ;139.5
        call      _for_write_seq_fmt                            ;139.5
                                ; LOE ebx esi
.B2.208:                        ; Preds .B2.207                 ; Infreq
        mov       eax, DWORD PTR [1048+esp]                     ;139.5
        lea       ecx, DWORD PTR [996+esp]                      ;139.5
        mov       edx, DWORD PTR [eax+esi*4]                    ;139.5
        mov       DWORD PTR [996+esp], edx                      ;139.5
        push      ecx                                           ;139.5
        push      OFFSET FLAT: __STRLITPACK_118.0.2             ;139.5
        push      ebx                                           ;139.5
        call      _for_write_seq_fmt_xmit                       ;139.5
                                ; LOE ebx esi
.B2.209:                        ; Preds .B2.208                 ; Infreq
        mov       eax, DWORD PTR [1124+esp]                     ;139.5
        lea       ecx, DWORD PTR [1016+esp]                     ;139.5
        mov       edx, DWORD PTR [eax+esi*4]                    ;139.5
        mov       DWORD PTR [1016+esp], edx                     ;139.5
        push      ecx                                           ;139.5
        push      OFFSET FLAT: __STRLITPACK_119.0.2             ;139.5
        push      ebx                                           ;139.5
        call      _for_write_seq_fmt_xmit                       ;139.5
                                ; LOE ebx esi
.B2.210:                        ; Preds .B2.209                 ; Infreq
        mov       eax, DWORD PTR [1088+esp]                     ;139.5
        lea       ecx, DWORD PTR [1036+esp]                     ;139.5
        mov       edx, DWORD PTR [eax+esi*4]                    ;139.5
        mov       DWORD PTR [1036+esp], edx                     ;139.5
        push      ecx                                           ;139.5
        push      OFFSET FLAT: __STRLITPACK_120.0.2             ;139.5
        push      ebx                                           ;139.5
        call      _for_write_seq_fmt_xmit                       ;139.5
                                ; LOE ebx esi
.B2.211:                        ; Preds .B2.210                 ; Infreq
        mov       eax, DWORD PTR [1132+esp]                     ;139.5
        lea       ecx, DWORD PTR [1056+esp]                     ;139.5
        mov       edx, DWORD PTR [eax+esi*4]                    ;139.5
        mov       DWORD PTR [1056+esp], edx                     ;139.5
        push      ecx                                           ;139.5
        push      OFFSET FLAT: __STRLITPACK_121.0.2             ;139.5
        push      ebx                                           ;139.5
        call      _for_write_seq_fmt_xmit                       ;139.5
                                ; LOE ebx esi
.B2.212:                        ; Preds .B2.211                 ; Infreq
        mov       eax, DWORD PTR [1104+esp]                     ;139.5
        lea       ecx, DWORD PTR [1076+esp]                     ;139.5
        mov       edx, DWORD PTR [eax+esi*4]                    ;139.5
        mov       DWORD PTR [1076+esp], edx                     ;139.5
        push      ecx                                           ;139.5
        push      OFFSET FLAT: __STRLITPACK_122.0.2             ;139.5
        push      ebx                                           ;139.5
        call      _for_write_seq_fmt_xmit                       ;139.5
                                ; LOE ebx esi
.B2.213:                        ; Preds .B2.212                 ; Infreq
        mov       eax, DWORD PTR [364+esp]                      ;139.5
        lea       ecx, DWORD PTR [1096+esp]                     ;139.5
        mov       edx, DWORD PTR [eax+esi*4]                    ;139.5
        mov       DWORD PTR [1096+esp], edx                     ;139.5
        push      ecx                                           ;139.5
        push      OFFSET FLAT: __STRLITPACK_123.0.2             ;139.5
        push      ebx                                           ;139.5
        call      _for_write_seq_fmt_xmit                       ;139.5
                                ; LOE ebx esi
.B2.214:                        ; Preds .B2.213                 ; Infreq
        mov       eax, DWORD PTR [1176+esp]                     ;139.5
        lea       ecx, DWORD PTR [1116+esp]                     ;139.5
        mov       edx, DWORD PTR [eax+esi*4]                    ;139.5
        mov       DWORD PTR [1488+esp], edx                     ;139.5
        mov       DWORD PTR [1116+esp], edx                     ;139.5
        push      ecx                                           ;139.5
        push      OFFSET FLAT: __STRLITPACK_124.0.2             ;139.5
        push      ebx                                           ;139.5
        call      _for_write_seq_fmt_xmit                       ;139.5
                                ; LOE ebx esi
.B2.215:                        ; Preds .B2.214                 ; Infreq
        mov       eax, DWORD PTR [1164+esp]                     ;139.5
        lea       ecx, DWORD PTR [1136+esp]                     ;139.5
        mov       edx, DWORD PTR [eax+esi*4]                    ;139.5
        mov       DWORD PTR [1136+esp], edx                     ;139.5
        push      ecx                                           ;139.5
        push      OFFSET FLAT: __STRLITPACK_125.0.2             ;139.5
        push      ebx                                           ;139.5
        call      _for_write_seq_fmt_xmit                       ;139.5
                                ; LOE ebx esi
.B2.282:                        ; Preds .B2.215                 ; Infreq
        add       esp, 124                                      ;139.5
                                ; LOE ebx esi
.B2.216:                        ; Preds .B2.282                 ; Infreq
        mov       eax, DWORD PTR [1092+esp]                     ;139.5
        lea       ecx, DWORD PTR [1032+esp]                     ;139.5
        mov       edx, DWORD PTR [eax+esi*4]                    ;139.5
        mov       DWORD PTR [1032+esp], edx                     ;139.5
        push      ecx                                           ;139.5
        push      OFFSET FLAT: __STRLITPACK_126.0.2             ;139.5
        push      ebx                                           ;139.5
        call      _for_write_seq_fmt_xmit                       ;139.5
                                ; LOE ebx esi
.B2.217:                        ; Preds .B2.216                 ; Infreq
        mov       eax, DWORD PTR [1016+esp]                     ;139.5
        lea       ecx, DWORD PTR [1052+esp]                     ;139.5
        mov       edx, DWORD PTR [eax+esi*4]                    ;139.5
        mov       DWORD PTR [1052+esp], edx                     ;139.5
        push      ecx                                           ;139.5
        push      OFFSET FLAT: __STRLITPACK_127.0.2             ;139.5
        push      ebx                                           ;139.5
        call      _for_write_seq_fmt_xmit                       ;139.5
                                ; LOE ebx esi
.B2.218:                        ; Preds .B2.217                 ; Infreq
        mov       eax, DWORD PTR [1068+esp]                     ;139.5
        lea       ecx, DWORD PTR [1072+esp]                     ;139.5
        mov       edx, DWORD PTR [eax+esi*4]                    ;139.5
        mov       DWORD PTR [1072+esp], edx                     ;139.5
        push      ecx                                           ;139.5
        push      OFFSET FLAT: __STRLITPACK_128.0.2             ;139.5
        push      ebx                                           ;139.5
        call      _for_write_seq_fmt_xmit                       ;139.5
                                ; LOE ebx esi
.B2.219:                        ; Preds .B2.218                 ; Infreq
        mov       eax, DWORD PTR [1024+esp]                     ;139.5
        lea       ecx, DWORD PTR [1092+esp]                     ;139.5
        mov       edx, DWORD PTR [eax+esi*4]                    ;139.5
        mov       DWORD PTR [1092+esp], edx                     ;139.5
        push      ecx                                           ;139.5
        push      OFFSET FLAT: __STRLITPACK_129.0.2             ;139.5
        push      ebx                                           ;139.5
        call      _for_write_seq_fmt_xmit                       ;139.5
                                ; LOE ebx esi
.B2.220:                        ; Preds .B2.219                 ; Infreq
        mov       eax, DWORD PTR [1044+esp]                     ;139.5
        lea       ecx, DWORD PTR [1112+esp]                     ;139.5
        mov       edx, DWORD PTR [eax+esi*4]                    ;139.5
        mov       DWORD PTR [1112+esp], edx                     ;139.5
        push      ecx                                           ;139.5
        push      OFFSET FLAT: __STRLITPACK_130.0.2             ;139.5
        push      ebx                                           ;139.5
        call      _for_write_seq_fmt_xmit                       ;139.5
                                ; LOE ebx esi
.B2.221:                        ; Preds .B2.220                 ; Infreq
        mov       eax, DWORD PTR [1072+esp]                     ;139.5
        lea       ecx, DWORD PTR [1132+esp]                     ;139.5
        mov       edx, DWORD PTR [eax+esi*4]                    ;139.5
        mov       DWORD PTR [1132+esp], edx                     ;139.5
        push      ecx                                           ;139.5
        push      OFFSET FLAT: __STRLITPACK_131.0.2             ;139.5
        push      ebx                                           ;139.5
        call      _for_write_seq_fmt_xmit                       ;139.5
                                ; LOE ebx esi
.B2.283:                        ; Preds .B2.221                 ; Infreq
        add       esp, 72                                       ;139.5
        jmp       .B2.103       ; Prob 100%                     ;139.5
                                ; LOE ebx esi
.B2.222:                        ; Preds .B2.53                  ; Infreq
        mov       edx, DWORD PTR [952+esp]                      ;123.19
        lea       esi, DWORD PTR [ebx*4]                        ;123.5
        shl       edx, 2                                        ;123.5
        mov       ecx, DWORD PTR [920+esp]                      ;123.5
        sub       ecx, edx                                      ;123.5
        mov       DWORD PTR [1280+esp], 0                       ;123.5
        mov       DWORD PTR [64+esp], 4                         ;123.5
        lea       edx, DWORD PTR [ecx+ebx*4]                    ;123.5
        mov       DWORD PTR [68+esp], edx                       ;123.5
        lea       edx, DWORD PTR [64+esp]                       ;123.5
        push      32                                            ;123.5
        push      OFFSET FLAT: RESORT_OUTPUT_VEH$format_pack.0.2+52 ;123.5
        push      edx                                           ;123.5
        push      OFFSET FLAT: __STRLITPACK_81.0.2              ;123.5
        push      -2088435968                                   ;123.5
        push      97                                            ;123.5
        push      edi                                           ;123.5
        call      _for_read_seq_fmt                             ;123.5
                                ; LOE ebx esi edi
.B2.223:                        ; Preds .B2.222                 ; Infreq
        mov       edx, DWORD PTR [780+esp]                      ;123.29
        shl       edx, 2                                        ;123.5
        mov       ecx, DWORD PTR [748+esp]                      ;123.5
        sub       ecx, edx                                      ;123.5
        mov       DWORD PTR [100+esp], 4                        ;123.5
        lea       edx, DWORD PTR [ecx+ebx*4]                    ;123.5
        mov       DWORD PTR [104+esp], edx                      ;123.5
        lea       edx, DWORD PTR [100+esp]                      ;123.5
        push      edx                                           ;123.5
        push      OFFSET FLAT: __STRLITPACK_82.0.2              ;123.5
        push      edi                                           ;123.5
        call      _for_read_seq_fmt_xmit                        ;123.5
                                ; LOE ebx esi edi
.B2.224:                        ; Preds .B2.223                 ; Infreq
        mov       edx, DWORD PTR [752+esp]                      ;123.39
        shl       edx, 2                                        ;123.5
        mov       ecx, DWORD PTR [720+esp]                      ;123.5
        sub       ecx, edx                                      ;123.5
        mov       DWORD PTR [120+esp], 4                        ;123.5
        lea       edx, DWORD PTR [ecx+ebx*4]                    ;123.5
        mov       DWORD PTR [124+esp], edx                      ;123.5
        lea       edx, DWORD PTR [120+esp]                      ;123.5
        push      edx                                           ;123.5
        push      OFFSET FLAT: __STRLITPACK_83.0.2              ;123.5
        push      edi                                           ;123.5
        call      _for_read_seq_fmt_xmit                        ;123.5
                                ; LOE ebx esi edi
.B2.225:                        ; Preds .B2.224                 ; Infreq
        mov       edx, DWORD PTR [844+esp]                      ;123.49
        shl       edx, 2                                        ;123.5
        mov       ecx, DWORD PTR [812+esp]                      ;123.5
        sub       ecx, edx                                      ;123.5
        mov       DWORD PTR [140+esp], 4                        ;123.5
        lea       edx, DWORD PTR [ecx+ebx*4]                    ;123.5
        mov       DWORD PTR [144+esp], edx                      ;123.5
        lea       edx, DWORD PTR [140+esp]                      ;123.5
        push      edx                                           ;123.5
        push      OFFSET FLAT: __STRLITPACK_84.0.2              ;123.5
        push      edi                                           ;123.5
        call      _for_read_seq_fmt_xmit                        ;123.5
                                ; LOE ebx esi edi
.B2.226:                        ; Preds .B2.225                 ; Infreq
        mov       edx, DWORD PTR [736+esp]                      ;123.62
        shl       edx, 2                                        ;123.5
        mov       ecx, DWORD PTR [704+esp]                      ;123.5
        sub       ecx, edx                                      ;123.5
        mov       DWORD PTR [160+esp], 4                        ;123.5
        lea       edx, DWORD PTR [ecx+ebx*4]                    ;123.5
        mov       DWORD PTR [164+esp], edx                      ;123.5
        lea       edx, DWORD PTR [160+esp]                      ;123.5
        push      edx                                           ;123.5
        push      OFFSET FLAT: __STRLITPACK_85.0.2              ;123.5
        push      edi                                           ;123.5
        call      _for_read_seq_fmt_xmit                        ;123.5
                                ; LOE ebx esi edi
.B2.227:                        ; Preds .B2.226                 ; Infreq
        mov       edx, DWORD PTR [708+esp]                      ;123.74
        shl       edx, 2                                        ;123.5
        mov       ecx, DWORD PTR [676+esp]                      ;123.5
        sub       ecx, edx                                      ;123.5
        mov       DWORD PTR [180+esp], 4                        ;123.5
        lea       edx, DWORD PTR [ecx+ebx*4]                    ;123.5
        mov       DWORD PTR [184+esp], edx                      ;123.5
        lea       edx, DWORD PTR [180+esp]                      ;123.5
        push      edx                                           ;123.5
        push      OFFSET FLAT: __STRLITPACK_86.0.2              ;123.5
        push      edi                                           ;123.5
        call      _for_read_seq_fmt_xmit                        ;123.5
                                ; LOE ebx esi edi
.B2.228:                        ; Preds .B2.227                 ; Infreq
        mov       edx, DWORD PTR [680+esp]                      ;123.87
        shl       edx, 2                                        ;123.5
        mov       ecx, DWORD PTR [648+esp]                      ;123.5
        sub       ecx, edx                                      ;123.5
        mov       DWORD PTR [200+esp], 4                        ;123.5
        lea       edx, DWORD PTR [ecx+ebx*4]                    ;123.5
        mov       DWORD PTR [204+esp], edx                      ;123.5
        lea       edx, DWORD PTR [200+esp]                      ;123.5
        push      edx                                           ;123.5
        push      OFFSET FLAT: __STRLITPACK_87.0.2              ;123.5
        push      edi                                           ;123.5
        call      _for_read_seq_fmt_xmit                        ;123.5
                                ; LOE ebx esi edi
.B2.229:                        ; Preds .B2.228                 ; Infreq
        mov       edx, DWORD PTR [1012+esp]                     ;123.99
        shl       edx, 2                                        ;123.5
        mov       ecx, DWORD PTR [980+esp]                      ;123.5
        sub       ecx, edx                                      ;123.5
        mov       DWORD PTR [220+esp], 4                        ;123.5
        lea       edx, DWORD PTR [ecx+ebx*4]                    ;123.5
        mov       DWORD PTR [224+esp], edx                      ;123.5
        lea       edx, DWORD PTR [220+esp]                      ;123.5
        push      edx                                           ;123.5
        push      OFFSET FLAT: __STRLITPACK_88.0.2              ;123.5
        push      edi                                           ;123.5
        call      _for_read_seq_fmt_xmit                        ;123.5
                                ; LOE ebx esi edi
.B2.230:                        ; Preds .B2.229                 ; Infreq
        mov       edx, DWORD PTR [664+esp]                      ;123.109
        shl       edx, 2                                        ;123.5
        mov       ecx, DWORD PTR [632+esp]                      ;123.5
        sub       ecx, edx                                      ;123.5
        mov       DWORD PTR [240+esp], 4                        ;123.5
        lea       edx, DWORD PTR [ecx+ebx*4]                    ;123.5
        mov       DWORD PTR [244+esp], edx                      ;123.5
        lea       edx, DWORD PTR [240+esp]                      ;123.5
        push      edx                                           ;123.5
        push      OFFSET FLAT: __STRLITPACK_89.0.2              ;123.5
        push      edi                                           ;123.5
        call      _for_read_seq_fmt_xmit                        ;123.5
                                ; LOE ebx esi edi
.B2.284:                        ; Preds .B2.230                 ; Infreq
        add       esp, 124                                      ;123.5
                                ; LOE ebx esi edi
.B2.231:                        ; Preds .B2.284                 ; Infreq
        mov       edx, DWORD PTR [512+esp]                      ;123.122
        shl       edx, 2                                        ;123.5
        mov       ecx, DWORD PTR [480+esp]                      ;123.5
        sub       ecx, edx                                      ;123.5
        mov       DWORD PTR [136+esp], 4                        ;123.5
        lea       edx, DWORD PTR [ecx+ebx*4]                    ;123.5
        mov       DWORD PTR [140+esp], edx                      ;123.5
        lea       edx, DWORD PTR [136+esp]                      ;123.5
        push      edx                                           ;123.5
        push      OFFSET FLAT: __STRLITPACK_90.0.2              ;123.5
        push      edi                                           ;123.5
        call      _for_read_seq_fmt_xmit                        ;123.5
                                ; LOE ebx esi edi
.B2.232:                        ; Preds .B2.231                 ; Infreq
        mov       edx, DWORD PTR [364+esp]                      ;123.135
        shl       edx, 2                                        ;123.5
        mov       ecx, DWORD PTR [332+esp]                      ;123.5
        sub       ecx, edx                                      ;123.5
        mov       DWORD PTR [156+esp], 4                        ;123.5
        lea       edx, DWORD PTR [ecx+ebx*4]                    ;123.5
        mov       DWORD PTR [160+esp], edx                      ;123.5
        lea       edx, DWORD PTR [156+esp]                      ;123.5
        push      edx                                           ;123.5
        push      OFFSET FLAT: __STRLITPACK_91.0.2              ;123.5
        push      edi                                           ;123.5
        call      _for_read_seq_fmt_xmit                        ;123.5
                                ; LOE ebx esi edi
.B2.233:                        ; Preds .B2.232                 ; Infreq
        mov       edx, DWORD PTR [336+esp]                      ;123.148
        shl       edx, 2                                        ;123.5
        mov       ecx, DWORD PTR [304+esp]                      ;123.5
        sub       ecx, edx                                      ;123.5
        mov       DWORD PTR [176+esp], 4                        ;123.5
        lea       edx, DWORD PTR [ecx+ebx*4]                    ;123.5
        mov       DWORD PTR [180+esp], edx                      ;123.5
        lea       edx, DWORD PTR [176+esp]                      ;123.5
        push      edx                                           ;123.5
        push      OFFSET FLAT: __STRLITPACK_92.0.2              ;123.5
        push      edi                                           ;123.5
        call      _for_read_seq_fmt_xmit                        ;123.5
                                ; LOE ebx esi edi
.B2.234:                        ; Preds .B2.233                 ; Infreq
        mov       edx, DWORD PTR [508+esp]                      ;123.161
        shl       edx, 2                                        ;123.5
        mov       ecx, DWORD PTR [476+esp]                      ;123.5
        sub       ecx, edx                                      ;123.5
        mov       DWORD PTR [196+esp], 4                        ;123.5
        lea       edx, DWORD PTR [ecx+ebx*4]                    ;123.5
        mov       DWORD PTR [200+esp], edx                      ;123.5
        lea       edx, DWORD PTR [196+esp]                      ;123.5
        push      edx                                           ;123.5
        push      OFFSET FLAT: __STRLITPACK_93.0.2              ;123.5
        push      edi                                           ;123.5
        call      _for_read_seq_fmt_xmit                        ;123.5
                                ; LOE ebx esi edi
.B2.235:                        ; Preds .B2.234                 ; Infreq
        mov       edx, DWORD PTR [480+esp]                      ;123.172
        shl       edx, 2                                        ;123.5
        mov       ecx, DWORD PTR [448+esp]                      ;123.5
        sub       ecx, edx                                      ;123.5
        mov       DWORD PTR [216+esp], 4                        ;123.5
        lea       edx, DWORD PTR [ecx+ebx*4]                    ;123.5
        mov       DWORD PTR [220+esp], edx                      ;123.5
        lea       edx, DWORD PTR [216+esp]                      ;123.5
        push      edx                                           ;123.5
        push      OFFSET FLAT: __STRLITPACK_94.0.2              ;123.5
        push      edi                                           ;123.5
        call      _for_read_seq_fmt_xmit                        ;123.5
                                ; LOE ebx esi edi
.B2.236:                        ; Preds .B2.235                 ; Infreq
        mov       edx, DWORD PTR [332+esp]                      ;123.182
        shl       edx, 2                                        ;123.5
        mov       ecx, DWORD PTR [300+esp]                      ;123.5
        sub       ecx, edx                                      ;123.5
        mov       DWORD PTR [236+esp], 4                        ;123.5
        lea       edx, DWORD PTR [ecx+ebx*4]                    ;123.5
        mov       DWORD PTR [240+esp], edx                      ;123.5
        lea       edx, DWORD PTR [236+esp]                      ;123.5
        push      edx                                           ;123.5
        push      OFFSET FLAT: __STRLITPACK_95.0.2              ;123.5
        push      edi                                           ;123.5
        call      _for_read_seq_fmt_xmit                        ;123.5
                                ; LOE ebx esi edi
.B2.285:                        ; Preds .B2.236                 ; Infreq
        add       esp, 72                                       ;123.5
        jmp       .B2.70        ; Prob 100%                     ;123.5
        ALIGN     16
                                ; LOE ebx esi edi
; mark_end;
_RESORT_OUTPUT_VEH ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
	DD 4 DUP (0H)	; pad
RESORT_OUTPUT_VEH$format_pack.0.2	DB	54
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	-24
	DB	3
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	12
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	2
	DB	1
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	9
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	2
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	2
	DB	1
	DB	0
	DB	0
	DB	0
	DB	8
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	6
	DB	0
	DB	0
	DB	0
	DB	6
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	4
	DB	2
	DB	0
	DB	0
	DB	0
	DB	8
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	2
	DB	0
	DB	0
	DB	0
	DB	5
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	8
	DB	1
	DB	0
	DB	0
	DB	0
	DB	12
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	1
	DB	1
	DB	0
	DB	0
	DB	0
	DB	6
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	101
	DB	0
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	35
	DB	32
	DB	32
	DB	32
	DB	117
	DB	115
	DB	101
	DB	99
	DB	32
	DB	32
	DB	32
	DB	100
	DB	115
	DB	101
	DB	99
	DB	32
	DB	32
	DB	32
	DB	115
	DB	116
	DB	105
	DB	109
	DB	101
	DB	32
	DB	118
	DB	101
	DB	104
	DB	99
	DB	108
	DB	115
	DB	32
	DB	118
	DB	101
	DB	104
	DB	116
	DB	121
	DB	112
	DB	101
	DB	32
	DB	105
	DB	111
	DB	99
	DB	32
	DB	35
	DB	79
	DB	78
	DB	111
	DB	100
	DB	101
	DB	32
	DB	35
	DB	73
	DB	110
	DB	116
	DB	68
	DB	101
	DB	32
	DB	105
	DB	110
	DB	102
	DB	111
	DB	32
	DB	114
	DB	105
	DB	98
	DB	102
	DB	32
	DB	32
	DB	32
	DB	32
	DB	99
	DB	111
	DB	109
	DB	112
	DB	32
	DB	32
	DB	32
	DB	105
	DB	122
	DB	111
	DB	110
	DB	101
	DB	32
	DB	69
	DB	118
	DB	97
	DB	99
	DB	32
	DB	73
	DB	110
	DB	105
	DB	116
	DB	80
	DB	111
	DB	115
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
_DATA	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_55.0.2	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_56.0.2	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_57.0.2	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	26
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_58.0.2	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	26
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_59.0.2	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	26
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_60.0.2	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	26
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_61.0.2	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_62.0.2	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_63.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_64.0.2	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_81.0.2	DB	9
	DB	5
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_82.0.2	DB	9
	DB	5
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_83.0.2	DB	9
	DB	5
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_84.0.2	DB	26
	DB	5
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_85.0.2	DB	9
	DB	5
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_86.0.2	DB	9
	DB	5
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_87.0.2	DB	9
	DB	5
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_88.0.2	DB	9
	DB	5
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_89.0.2	DB	9
	DB	5
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_90.0.2	DB	9
	DB	5
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_91.0.2	DB	26
	DB	5
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_92.0.2	DB	26
	DB	5
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_93.0.2	DB	9
	DB	5
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_94.0.2	DB	9
	DB	5
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_95.0.2	DB	26
	DB	5
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_65.0.2	DB	9
	DB	5
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_66.0.2	DB	9
	DB	5
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_67.0.2	DB	9
	DB	5
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_68.0.2	DB	26
	DB	5
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_69.0.2	DB	9
	DB	5
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_70.0.2	DB	9
	DB	5
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_71.0.2	DB	9
	DB	5
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_72.0.2	DB	9
	DB	5
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_73.0.2	DB	9
	DB	5
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_74.0.2	DB	9
	DB	5
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_75.0.2	DB	26
	DB	5
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_76.0.2	DB	26
	DB	5
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_77.0.2	DB	9
	DB	5
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_78.0.2	DB	9
	DB	5
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_79.0.2	DB	26
	DB	5
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_80.0.2	DB	9
	DB	5
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_96.0.2	DB	9
	DB	5
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_97.0.2	DB	9
	DB	5
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_98.0.2	DB	2
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_99.0.2	DB	9
	DB	5
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_100.0.2	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__NLITPACK_1.0.2	DD	2
__STRLITPACK_117.0.2	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_118.0.2	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_119.0.2	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_120.0.2	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_121.0.2	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_122.0.2	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_123.0.2	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_124.0.2	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_125.0.2	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_126.0.2	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_127.0.2	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_128.0.2	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_129.0.2	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_130.0.2	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_131.0.2	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_101.0.2	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_102.0.2	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_103.0.2	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_104.0.2	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_105.0.2	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_106.0.2	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_107.0.2	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_108.0.2	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_109.0.2	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_110.0.2	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_111.0.2	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_112.0.2	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_113.0.2	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_114.0.2	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_115.0.2	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_116.0.2	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_132.0.2	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_133.0.2	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_134.0.2	DB	2
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_135.0.2	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_136.0.2	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_137.0.2	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_138.0.2	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_139.0.2	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_140.0.2	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _RESORT_OUTPUT_VEH
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _WRITEUETRAVELTIME
; mark_begin;
       ALIGN     16
	PUBLIC _WRITEUETRAVELTIME
_WRITEUETRAVELTIME	PROC NEAR 
.B3.1:                          ; Preds .B3.0
        push      ebp                                           ;185.12
        mov       ebp, esp                                      ;185.12
        and       esp, -16                                      ;185.12
        push      edi                                           ;185.12
        push      ebx                                           ;185.12
        sub       esp, 168                                      ;185.12
        mov       DWORD PTR [32+esp], 0                         ;190.3
        lea       edi, DWORD PTR [32+esp]                       ;190.3
        mov       DWORD PTR [64+esp], 16                        ;190.3
        lea       eax, DWORD PTR [64+esp]                       ;190.3
        mov       DWORD PTR [68+esp], OFFSET FLAT: __STRLITPACK_154 ;190.3
        mov       DWORD PTR [72+esp], 7                         ;190.3
        mov       DWORD PTR [76+esp], OFFSET FLAT: __STRLITPACK_153 ;190.3
        mov       DWORD PTR [80+esp], 11                        ;190.3
        mov       DWORD PTR [84+esp], OFFSET FLAT: __STRLITPACK_152 ;190.3
        push      32                                            ;190.3
        push      eax                                           ;190.3
        push      OFFSET FLAT: __STRLITPACK_155.0.3             ;190.3
        push      -2088435965                                   ;190.3
        push      2000                                          ;190.3
        push      edi                                           ;190.3
        call      _for_open                                     ;190.3
                                ; LOE eax esi edi
.B3.24:                         ; Preds .B3.1
        add       esp, 24                                       ;190.3
                                ; LOE eax esi edi
.B3.2:                          ; Preds .B3.24
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;190.3
        test      eax, eax                                      ;191.12
        jne       .B3.20        ; Prob 5%                       ;191.12
                                ; LOE esi edi
.B3.32:                         ; Preds .B3.2
        xor       ebx, ebx                                      ;212.9
                                ; LOE ebx esi edi
.B3.3:                          ; Preds .B3.31 .B3.32
        mov       DWORD PTR [32+esp], 0                         ;196.3
        lea       eax, DWORD PTR [88+esp]                       ;196.3
        mov       DWORD PTR [88+esp], 13                        ;196.3
        mov       DWORD PTR [92+esp], OFFSET FLAT: __STRLITPACK_149 ;196.3
        mov       DWORD PTR [96+esp], 7                         ;196.3
        mov       DWORD PTR [100+esp], OFFSET FLAT: __STRLITPACK_148 ;196.3
        mov       DWORD PTR [104+esp], 11                       ;196.3
        mov       DWORD PTR [108+esp], OFFSET FLAT: __STRLITPACK_147 ;196.3
        push      32                                            ;196.3
        push      eax                                           ;196.3
        push      OFFSET FLAT: __STRLITPACK_158.0.3             ;196.3
        push      -2088435965                                   ;196.3
        push      2001                                          ;196.3
        push      edi                                           ;196.3
        call      _for_open                                     ;196.3
                                ; LOE eax ebx esi edi
.B3.25:                         ; Preds .B3.3
        add       esp, 24                                       ;196.3
                                ; LOE eax ebx esi edi
.B3.4:                          ; Preds .B3.25
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;196.3
        test      eax, eax                                      ;197.12
        jne       .B3.18        ; Prob 5%                       ;197.12
                                ; LOE ebx esi edi
.B3.5:                          ; Preds .B3.30 .B3.4
        mov       DWORD PTR [32+esp], 0                         ;202.3
        lea       eax, DWORD PTR [112+esp]                      ;202.3
        mov       DWORD PTR [112+esp], 15                       ;202.3
        mov       DWORD PTR [116+esp], OFFSET FLAT: __STRLITPACK_144 ;202.3
        mov       DWORD PTR [120+esp], 7                        ;202.3
        mov       DWORD PTR [124+esp], OFFSET FLAT: __STRLITPACK_143 ;202.3
        push      32                                            ;202.3
        push      eax                                           ;202.3
        push      OFFSET FLAT: __STRLITPACK_161.0.3             ;202.3
        push      -2088435965                                   ;202.3
        push      2002                                          ;202.3
        push      edi                                           ;202.3
        call      _for_open                                     ;202.3
                                ; LOE eax ebx esi edi
.B3.26:                         ; Preds .B3.5
        add       esp, 24                                       ;202.3
                                ; LOE eax ebx esi edi
.B3.6:                          ; Preds .B3.26
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;202.3
        test      eax, eax                                      ;203.12
        jne       .B3.16        ; Prob 5%                       ;203.12
                                ; LOE ebx esi edi
.B3.7:                          ; Preds .B3.29 .B3.6
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELTIME+24] ;208.3
        lea       ecx, DWORD PTR [24+esp]                       ;208.3
        shl       eax, 2                                        ;208.3
        imul      eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELTIME+36] ;208.3
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELTIME] ;208.3
        mov       DWORD PTR [32+esp], 0                         ;208.3
        mov       DWORD PTR [24+esp], eax                       ;208.3
        mov       DWORD PTR [28+esp], edx                       ;208.3
        push      32                                            ;208.3
        push      ecx                                           ;208.3
        push      OFFSET FLAT: __STRLITPACK_164.0.3             ;208.3
        push      -2088435968                                   ;208.3
        push      2000                                          ;208.3
        push      edi                                           ;208.3
        call      _for_write_seq                                ;208.3
                                ; LOE ebx esi edi
.B3.8:                          ; Preds .B3.7
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELPENALTY+36] ;209.3
        imul      eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELPENALTY+48] ;209.3
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELPENALTY+24] ;209.3
        shl       edx, 2                                        ;209.3
        imul      edx, eax                                      ;209.3
        lea       eax, DWORD PTR [152+esp]                      ;209.3
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELPENALTY] ;209.3
        mov       DWORD PTR [56+esp], 0                         ;209.3
        mov       DWORD PTR [152+esp], edx                      ;209.3
        mov       DWORD PTR [156+esp], ecx                      ;209.3
        push      32                                            ;209.3
        push      eax                                           ;209.3
        push      OFFSET FLAT: __STRLITPACK_165.0.3             ;209.3
        push      -2088435968                                   ;209.3
        push      2001                                          ;209.3
        push      edi                                           ;209.3
        call      _for_write_seq                                ;209.3
                                ; LOE ebx esi edi
.B3.9:                          ; Preds .B3.8
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFARCS] ;210.3
        lea       edx, DWORD PTR [184+esp]                      ;210.3
        mov       DWORD PTR [80+esp], 0                         ;210.3
        mov       DWORD PTR [184+esp], eax                      ;210.3
        push      32                                            ;210.3
        push      edx                                           ;210.3
        push      OFFSET FLAT: __STRLITPACK_166.0.3             ;210.3
        push      -2088435968                                   ;210.3
        push      2002                                          ;210.3
        push      edi                                           ;210.3
        call      _for_write_seq_lis                            ;210.3
                                ; LOE ebx esi edi
.B3.10:                         ; Preds .B3.9
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_AGGINT] ;210.3
        lea       edx, DWORD PTR [216+esp]                      ;210.3
        mov       DWORD PTR [216+esp], eax                      ;210.3
        push      edx                                           ;210.3
        push      OFFSET FLAT: __STRLITPACK_167.0.3             ;210.3
        push      edi                                           ;210.3
        call      _for_write_seq_lis_xmit                       ;210.3
                                ; LOE ebx esi edi
.B3.11:                         ; Preds .B3.10
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_MAXMOVE] ;210.3
        lea       edx, DWORD PTR [236+esp]                      ;210.3
        mov       DWORD PTR [236+esp], eax                      ;210.3
        push      edx                                           ;210.3
        push      OFFSET FLAT: __STRLITPACK_168.0.3             ;210.3
        push      edi                                           ;210.3
        call      _for_write_seq_lis_xmit                       ;210.3
                                ; LOE ebx esi edi
.B3.12:                         ; Preds .B3.11
        mov       DWORD PTR [128+esp], ebx                      ;212.9
        push      32                                            ;212.9
        push      ebx                                           ;212.9
        push      OFFSET FLAT: __STRLITPACK_169.0.3             ;212.9
        push      -2088435968                                   ;212.9
        push      2000                                          ;212.9
        push      edi                                           ;212.9
        call      _for_close                                    ;212.9
                                ; LOE ebx esi edi
.B3.27:                         ; Preds .B3.12
        add       esp, 120                                      ;212.9
                                ; LOE ebx esi edi
.B3.13:                         ; Preds .B3.27
        mov       DWORD PTR [32+esp], ebx                       ;213.9
        push      32                                            ;213.9
        push      ebx                                           ;213.9
        push      OFFSET FLAT: __STRLITPACK_170.0.3             ;213.9
        push      -2088435968                                   ;213.9
        push      2001                                          ;213.9
        push      edi                                           ;213.9
        call      _for_close                                    ;213.9
                                ; LOE ebx esi edi
.B3.14:                         ; Preds .B3.13
        mov       DWORD PTR [56+esp], ebx                       ;214.9
        push      32                                            ;214.9
        push      ebx                                           ;214.9
        push      OFFSET FLAT: __STRLITPACK_171.0.3             ;214.9
        push      -2088435968                                   ;214.9
        push      2002                                          ;214.9
        push      edi                                           ;214.9
        call      _for_close                                    ;214.9
                                ; LOE esi
.B3.15:                         ; Preds .B3.14
        add       esp, 216                                      ;215.1
        pop       ebx                                           ;215.1
        pop       edi                                           ;215.1
        mov       esp, ebp                                      ;215.1
        pop       ebp                                           ;215.1
        ret                                                     ;215.1
                                ; LOE
.B3.16:                         ; Preds .B3.6                   ; Infreq
        mov       DWORD PTR [32+esp], 0                         ;204.5
        lea       eax, DWORD PTR [16+esp]                       ;204.5
        mov       DWORD PTR [16+esp], 34                        ;204.5
        mov       DWORD PTR [20+esp], OFFSET FLAT: __STRLITPACK_141 ;204.5
        push      32                                            ;204.5
        push      eax                                           ;204.5
        push      OFFSET FLAT: __STRLITPACK_162.0.3             ;204.5
        push      -2088435968                                   ;204.5
        push      911                                           ;204.5
        push      edi                                           ;204.5
        call      _for_write_seq_lis                            ;204.5
                                ; LOE ebx esi edi
.B3.17:                         ; Preds .B3.16                  ; Infreq
        push      32                                            ;205.5
        push      ebx                                           ;205.5
        push      ebx                                           ;205.5
        push      -2088435968                                   ;205.5
        push      ebx                                           ;205.5
        push      OFFSET FLAT: __STRLITPACK_163                 ;205.5
        call      _for_stop_core                                ;205.5
                                ; LOE ebx esi edi
.B3.29:                         ; Preds .B3.17                  ; Infreq
        add       esp, 48                                       ;205.5
        jmp       .B3.7         ; Prob 100%                     ;205.5
                                ; LOE ebx esi edi
.B3.18:                         ; Preds .B3.4                   ; Infreq
        mov       eax, 32                                       ;198.5
        lea       edx, DWORD PTR [8+esp]                        ;198.5
        mov       DWORD PTR [32+esp], 0                         ;198.5
        mov       DWORD PTR [8+esp], eax                        ;198.5
        mov       DWORD PTR [12+esp], OFFSET FLAT: __STRLITPACK_145 ;198.5
        push      eax                                           ;198.5
        push      edx                                           ;198.5
        push      OFFSET FLAT: __STRLITPACK_159.0.3             ;198.5
        push      -2088435968                                   ;198.5
        push      911                                           ;198.5
        push      edi                                           ;198.5
        call      _for_write_seq_lis                            ;198.5
                                ; LOE ebx esi edi
.B3.19:                         ; Preds .B3.18                  ; Infreq
        push      32                                            ;199.5
        push      ebx                                           ;199.5
        push      ebx                                           ;199.5
        push      -2088435968                                   ;199.5
        push      ebx                                           ;199.5
        push      OFFSET FLAT: __STRLITPACK_160                 ;199.5
        call      _for_stop_core                                ;199.5
                                ; LOE ebx esi edi
.B3.30:                         ; Preds .B3.19                  ; Infreq
        add       esp, 48                                       ;199.5
        jmp       .B3.5         ; Prob 100%                     ;199.5
                                ; LOE ebx esi edi
.B3.20:                         ; Preds .B3.2                   ; Infreq
        mov       DWORD PTR [32+esp], 0                         ;192.5
        lea       eax, DWORD PTR [esp]                          ;192.5
        mov       DWORD PTR [esp], 35                           ;192.5
        mov       DWORD PTR [4+esp], OFFSET FLAT: __STRLITPACK_150 ;192.5
        push      32                                            ;192.5
        push      eax                                           ;192.5
        push      OFFSET FLAT: __STRLITPACK_156.0.3             ;192.5
        push      -2088435968                                   ;192.5
        push      911                                           ;192.5
        push      edi                                           ;192.5
        call      _for_write_seq_lis                            ;192.5
                                ; LOE esi edi
.B3.21:                         ; Preds .B3.20                  ; Infreq
        push      32                                            ;193.5
        xor       ebx, ebx                                      ;193.5
        push      ebx                                           ;193.5
        push      ebx                                           ;193.5
        push      -2088435968                                   ;193.5
        push      ebx                                           ;193.5
        push      OFFSET FLAT: __STRLITPACK_157                 ;193.5
        call      _for_stop_core                                ;193.5
                                ; LOE ebx esi edi
.B3.31:                         ; Preds .B3.21                  ; Infreq
        add       esp, 48                                       ;193.5
        jmp       .B3.3         ; Prob 100%                     ;193.5
        ALIGN     16
                                ; LOE ebx esi edi
; mark_end;
_WRITEUETRAVELTIME ENDP
_TEXT	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_155.0.3	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	26
	DB	0
	DB	56
	DB	4
	DB	15
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_156.0.3	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_158.0.3	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	26
	DB	0
	DB	56
	DB	4
	DB	15
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_159.0.3	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_161.0.3	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	26
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_162.0.3	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_164.0.3	DB	9
	DB	5
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_165.0.3	DB	26
	DB	5
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_166.0.3	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_167.0.3	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_168.0.3	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_169.0.3	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_170.0.3	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_171.0.3	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _WRITEUETRAVELTIME
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _READUETRAVELTIME
; mark_begin;
       ALIGN     16
	PUBLIC _READUETRAVELTIME
_READUETRAVELTIME	PROC NEAR 
.B4.1:                          ; Preds .B4.0
        push      ebp                                           ;218.12
        mov       ebp, esp                                      ;218.12
        and       esp, -16                                      ;218.12
        push      esi                                           ;218.12
        push      edi                                           ;218.12
        push      ebx                                           ;218.12
        sub       esp, 260                                      ;218.12
        mov       DWORD PTR [64+esp], 0                         ;225.12
        lea       ebx, DWORD PTR [172+esp]                      ;225.12
        mov       DWORD PTR [160+esp], 16                       ;225.12
        lea       edx, DWORD PTR [64+esp]                       ;225.12
        mov       DWORD PTR [164+esp], OFFSET FLAT: __STRLITPACK_194 ;225.12
        lea       eax, DWORD PTR [160+esp]                      ;225.12
        mov       DWORD PTR [168+esp], ebx                      ;225.12
        push      32                                            ;225.12
        push      eax                                           ;225.12
        push      OFFSET FLAT: __STRLITPACK_195.0.4             ;225.12
        push      -2088435968                                   ;225.12
        push      -1                                            ;225.12
        push      edx                                           ;225.12
        call      _for_inquire                                  ;225.12
                                ; LOE ebx
.B4.76:                         ; Preds .B4.1
        add       esp, 24                                       ;225.12
                                ; LOE ebx
.B4.2:                          ; Preds .B4.76
        test      BYTE PTR [172+esp], 1                         ;226.6
        je        .B4.72        ; Prob 8%                       ;226.6
                                ; LOE ebx
.B4.3:                          ; Preds .B4.2
        mov       DWORD PTR [64+esp], 0                         ;227.3
        lea       eax, DWORD PTR [96+esp]                       ;227.3
        mov       DWORD PTR [96+esp], 16                        ;227.3
        mov       DWORD PTR [100+esp], OFFSET FLAT: __STRLITPACK_193 ;227.3
        mov       DWORD PTR [104+esp], 7                        ;227.3
        mov       DWORD PTR [108+esp], OFFSET FLAT: __STRLITPACK_192 ;227.3
        mov       DWORD PTR [112+esp], 11                       ;227.3
        mov       DWORD PTR [116+esp], OFFSET FLAT: __STRLITPACK_191 ;227.3
        push      32                                            ;227.3
        push      eax                                           ;227.3
        push      OFFSET FLAT: __STRLITPACK_196.0.4             ;227.3
        push      -2088435965                                   ;227.3
        push      2000                                          ;227.3
        lea       edx, DWORD PTR [84+esp]                       ;227.3
        push      edx                                           ;227.3
        call      _for_open                                     ;227.3
                                ; LOE eax ebx
.B4.77:                         ; Preds .B4.3
        add       esp, 24                                       ;227.3
                                ; LOE eax ebx
.B4.4:                          ; Preds .B4.77
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;227.3
        test      eax, eax                                      ;228.12
        jne       .B4.70        ; Prob 5%                       ;228.12
                                ; LOE ebx
.B4.5:                          ; Preds .B4.4 .B4.93 .B4.94
        mov       DWORD PTR [64+esp], 0                         ;237.12
        lea       eax, DWORD PTR [176+esp]                      ;237.12
        mov       DWORD PTR [176+esp], 13                       ;237.12
        mov       DWORD PTR [180+esp], OFFSET FLAT: __STRLITPACK_186 ;237.12
        mov       DWORD PTR [184+esp], ebx                      ;237.12
        push      32                                            ;237.12
        push      eax                                           ;237.12
        push      OFFSET FLAT: __STRLITPACK_201.0.4             ;237.12
        push      -2088435968                                   ;237.12
        push      -1                                            ;237.12
        lea       edx, DWORD PTR [84+esp]                       ;237.12
        push      edx                                           ;237.12
        call      _for_inquire                                  ;237.12
                                ; LOE ebx
.B4.78:                         ; Preds .B4.5
        add       esp, 24                                       ;237.12
                                ; LOE ebx
.B4.6:                          ; Preds .B4.78
        test      BYTE PTR [172+esp], 1                         ;238.6
        je        .B4.68        ; Prob 8%                       ;238.6
                                ; LOE ebx
.B4.7:                          ; Preds .B4.6
        mov       DWORD PTR [64+esp], 0                         ;239.3
        lea       eax, DWORD PTR [120+esp]                      ;239.3
        mov       DWORD PTR [120+esp], 13                       ;239.3
        mov       DWORD PTR [124+esp], OFFSET FLAT: __STRLITPACK_185 ;239.3
        mov       DWORD PTR [128+esp], 7                        ;239.3
        mov       DWORD PTR [132+esp], OFFSET FLAT: __STRLITPACK_184 ;239.3
        mov       DWORD PTR [136+esp], 11                       ;239.3
        mov       DWORD PTR [140+esp], OFFSET FLAT: __STRLITPACK_183 ;239.3
        push      32                                            ;239.3
        push      eax                                           ;239.3
        push      OFFSET FLAT: __STRLITPACK_202.0.4             ;239.3
        push      -2088435965                                   ;239.3
        push      2001                                          ;239.3
        lea       edx, DWORD PTR [84+esp]                       ;239.3
        push      edx                                           ;239.3
        call      _for_open                                     ;239.3
                                ; LOE eax ebx
.B4.79:                         ; Preds .B4.7
        add       esp, 24                                       ;239.3
                                ; LOE eax ebx
.B4.8:                          ; Preds .B4.79
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;239.3
        test      eax, eax                                      ;240.12
        jne       .B4.66        ; Prob 5%                       ;240.12
                                ; LOE ebx
.B4.9:                          ; Preds .B4.91 .B4.92 .B4.8
        mov       DWORD PTR [64+esp], 0                         ;249.12
        lea       eax, DWORD PTR [192+esp]                      ;249.12
        mov       DWORD PTR [192+esp], 15                       ;249.12
        mov       DWORD PTR [196+esp], OFFSET FLAT: __STRLITPACK_178 ;249.12
        mov       DWORD PTR [200+esp], ebx                      ;249.12
        push      32                                            ;249.12
        push      eax                                           ;249.12
        push      OFFSET FLAT: __STRLITPACK_207.0.4             ;249.12
        push      -2088435968                                   ;249.12
        push      -1                                            ;249.12
        lea       edx, DWORD PTR [84+esp]                       ;249.12
        push      edx                                           ;249.12
        call      _for_inquire                                  ;249.12
                                ; LOE
.B4.80:                         ; Preds .B4.9
        add       esp, 24                                       ;249.12
                                ; LOE
.B4.10:                         ; Preds .B4.80
        test      BYTE PTR [172+esp], 1                         ;250.6
        je        .B4.64        ; Prob 8%                       ;250.6
                                ; LOE
.B4.11:                         ; Preds .B4.10
        mov       DWORD PTR [64+esp], 0                         ;251.3
        lea       eax, DWORD PTR [144+esp]                      ;251.3
        mov       DWORD PTR [144+esp], 15                       ;251.3
        mov       DWORD PTR [148+esp], OFFSET FLAT: __STRLITPACK_177 ;251.3
        mov       DWORD PTR [152+esp], 7                        ;251.3
        mov       DWORD PTR [156+esp], OFFSET FLAT: __STRLITPACK_176 ;251.3
        push      32                                            ;251.3
        push      eax                                           ;251.3
        push      OFFSET FLAT: __STRLITPACK_208.0.4             ;251.3
        push      -2088435965                                   ;251.3
        push      2002                                          ;251.3
        lea       edx, DWORD PTR [84+esp]                       ;251.3
        push      edx                                           ;251.3
        call      _for_open                                     ;251.3
                                ; LOE eax
.B4.81:                         ; Preds .B4.11
        add       esp, 24                                       ;251.3
                                ; LOE eax
.B4.12:                         ; Preds .B4.81
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;251.3
        test      eax, eax                                      ;252.12
        jne       .B4.62        ; Prob 5%                       ;252.12
                                ; LOE
.B4.13:                         ; Preds .B4.89 .B4.90 .B4.12
        mov       DWORD PTR [64+esp], 0                         ;261.3
        lea       eax, DWORD PTR [188+esp]                      ;261.3
        mov       DWORD PTR [216+esp], eax                      ;261.3
        lea       edx, DWORD PTR [216+esp]                      ;261.3
        push      32                                            ;261.3
        push      edx                                           ;261.3
        push      OFFSET FLAT: __STRLITPACK_213.0.4             ;261.3
        push      -2088435968                                   ;261.3
        push      2002                                          ;261.3
        lea       ecx, DWORD PTR [84+esp]                       ;261.3
        push      ecx                                           ;261.3
        call      _for_read_seq_lis                             ;261.3
                                ; LOE
.B4.14:                         ; Preds .B4.13
        mov       DWORD PTR [248+esp], OFFSET FLAT: _DYNUST_MAIN_MODULE_mp_ITI_NUTMP ;261.3
        lea       eax, DWORD PTR [248+esp]                      ;261.3
        push      eax                                           ;261.3
        push      OFFSET FLAT: __STRLITPACK_214.0.4             ;261.3
        lea       edx, DWORD PTR [96+esp]                       ;261.3
        push      edx                                           ;261.3
        call      _for_read_seq_lis_xmit                        ;261.3
                                ; LOE
.B4.15:                         ; Preds .B4.14
        lea       edx, DWORD PTR [268+esp]                      ;261.3
        lea       eax, DWORD PTR [240+esp]                      ;261.3
        mov       DWORD PTR [268+esp], eax                      ;261.3
        push      edx                                           ;261.3
        push      OFFSET FLAT: __STRLITPACK_215.0.4             ;261.3
        lea       ecx, DWORD PTR [108+esp]                      ;261.3
        push      ecx                                           ;261.3
        call      _for_read_seq_lis_xmit                        ;261.3
                                ; LOE
.B4.16:                         ; Preds .B4.15
        mov       edx, DWORD PTR [236+esp]                      ;263.3
        xor       ebx, ebx                                      ;263.3
        test      edx, edx                                      ;263.3
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ITI_NUTMP] ;263.3
        cmovle    edx, ebx                                      ;263.3
        mov       eax, 4                                        ;263.3
        mov       ecx, 1                                        ;263.3
        test      edi, edi                                      ;263.3
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_UETRAVELTIME+32], ecx ;263.3
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_UETRAVELTIME+44], ecx ;263.3
        cmovle    edi, ebx                                      ;263.3
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_UETRAVELTIME+12], 133 ;263.3
        lea       ecx, DWORD PTR [256+esp]                      ;263.3
        push      eax                                           ;263.3
        push      edi                                           ;263.3
        push      edx                                           ;263.3
        push      3                                             ;263.3
        push      ecx                                           ;263.3
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_UETRAVELTIME+4], eax ;263.3
        lea       esi, DWORD PTR [edx*4]                        ;263.3
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_UETRAVELTIME+16], 2 ;263.3
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_UETRAVELTIME+8], ebx ;263.3
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_UETRAVELTIME+24], edx ;263.3
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_UETRAVELTIME+36], edi ;263.3
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_UETRAVELTIME+28], eax ;263.3
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_UETRAVELTIME+40], esi ;263.3
        call      _for_check_mult_overflow                      ;263.3
                                ; LOE eax
.B4.17:                         ; Preds .B4.16
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_UETRAVELTIME+12] ;263.3
        and       eax, 1                                        ;263.3
        and       edx, 1                                        ;263.3
        shl       eax, 4                                        ;263.3
        add       edx, edx                                      ;263.3
        or        edx, eax                                      ;263.3
        or        edx, 262144                                   ;263.3
        push      edx                                           ;263.3
        push      OFFSET FLAT: _DYNUST_MAIN_MODULE_mp_UETRAVELTIME ;263.3
        push      DWORD PTR [284+esp]                           ;263.3
        call      _for_alloc_allocatable                        ;263.3
                                ; LOE
.B4.83:                         ; Preds .B4.17
        add       esp, 80                                       ;263.3
                                ; LOE
.B4.18:                         ; Preds .B4.83
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_UETRAVELTIME+36] ;264.3
        test      ecx, ecx                                      ;264.3
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_UETRAVELTIME+44] ;264.3
        jle       .B4.20        ; Prob 10%                      ;264.3
                                ; LOE ecx edi
.B4.19:                         ; Preds .B4.18
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_UETRAVELTIME+40] ;264.3
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_UETRAVELTIME+24] ;264.3
        test      edx, edx                                      ;264.3
        mov       DWORD PTR [240+esp], eax                      ;264.3
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_UETRAVELTIME+32] ;264.3
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_UETRAVELTIME] ;264.3
        jg        .B4.32        ; Prob 50%                      ;264.3
                                ; LOE eax edx ecx esi edi
.B4.20:                         ; Preds .B4.47 .B4.35 .B4.18 .B4.19
        mov       ecx, DWORD PTR [188+esp]                      ;266.3
        xor       edi, edi                                      ;266.3
        test      ecx, ecx                                      ;266.3
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ITI_NUTMP] ;266.3
        cmovle    ecx, edi                                      ;266.3
        mov       ebx, 4                                        ;266.3
        test      edx, edx                                      ;266.3
        mov       eax, DWORD PTR [204+esp]                      ;266.3
        cmovle    edx, edi                                      ;266.3
        mov       esi, 1                                        ;266.3
        test      eax, eax                                      ;266.3
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_UETRAVELPENALTY+8], edi ;266.3
        cmovle    eax, edi                                      ;266.3
        lea       edi, DWORD PTR [212+esp]                      ;266.3
        push      ebx                                           ;266.3
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_UETRAVELPENALTY+32], esi ;266.3
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_UETRAVELPENALTY+44], esi ;266.3
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_UETRAVELPENALTY+56], esi ;266.3
        lea       esi, DWORD PTR [ecx*4]                        ;266.3
        push      eax                                           ;266.3
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_UETRAVELPENALTY+40], esi ;266.3
        imul      esi, edx                                      ;266.3
        push      edx                                           ;266.3
        push      ecx                                           ;266.3
        push      ebx                                           ;266.3
        push      edi                                           ;266.3
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_UETRAVELPENALTY+12], 133 ;266.3
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_UETRAVELPENALTY+4], ebx ;266.3
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_UETRAVELPENALTY+16], 3 ;266.3
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_UETRAVELPENALTY+24], ecx ;266.3
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_UETRAVELPENALTY+36], edx ;266.3
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_UETRAVELPENALTY+48], eax ;266.3
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_UETRAVELPENALTY+28], ebx ;266.3
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_UETRAVELPENALTY+52], esi ;266.3
        call      _for_check_mult_overflow                      ;266.3
                                ; LOE eax
.B4.21:                         ; Preds .B4.20
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_UETRAVELPENALTY+12] ;266.3
        and       eax, 1                                        ;266.3
        and       edx, 1                                        ;266.3
        shl       eax, 4                                        ;266.3
        add       edx, edx                                      ;266.3
        or        edx, eax                                      ;266.3
        or        edx, 262144                                   ;266.3
        push      edx                                           ;266.3
        push      OFFSET FLAT: _DYNUST_MAIN_MODULE_mp_UETRAVELPENALTY ;266.3
        push      DWORD PTR [244+esp]                           ;266.3
        call      _for_alloc_allocatable                        ;266.3
                                ; LOE
.B4.85:                         ; Preds .B4.21
        add       esp, 36                                       ;266.3
                                ; LOE
.B4.22:                         ; Preds .B4.85
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_UETRAVELPENALTY+48] ;267.3
        test      ecx, ecx                                      ;267.3
        mov       ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_UETRAVELPENALTY+56] ;267.3
        jle       .B4.25        ; Prob 10%                      ;267.3
                                ; LOE ecx ebx
.B4.23:                         ; Preds .B4.22
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_UETRAVELPENALTY+40] ;267.3
        mov       DWORD PTR [240+esp], edi                      ;267.3
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_UETRAVELPENALTY+52] ;267.3
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_UETRAVELPENALTY+44] ;267.3
        mov       DWORD PTR [244+esp], edi                      ;267.3
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_UETRAVELPENALTY] ;267.3
        mov       DWORD PTR [4+esp], eax                        ;267.3
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_UETRAVELPENALTY+36] ;267.3
        test      eax, eax                                      ;267.3
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_UETRAVELPENALTY+32] ;267.3
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_UETRAVELPENALTY+24] ;267.3
        mov       DWORD PTR [248+esp], edi                      ;267.3
        mov       DWORD PTR [252+esp], 0                        ;
        jle       .B4.25        ; Prob 10%                      ;267.3
                                ; LOE eax edx ecx ebx esi
.B4.24:                         ; Preds .B4.23
        test      esi, esi                                      ;267.3
        jg        .B4.28        ; Prob 50%                      ;267.3
                                ; LOE eax edx ecx ebx esi
.B4.25:                         ; Preds .B4.37 .B4.24 .B4.23 .B4.22
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_UETRAVELTIME+24] ;269.3
        lea       ecx, DWORD PTR [esp]                          ;269.3
        shl       eax, 2                                        ;269.3
        imul      eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_UETRAVELTIME+36] ;269.3
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_UETRAVELTIME] ;269.3
        mov       DWORD PTR [64+esp], 0                         ;269.3
        mov       DWORD PTR [esp], eax                          ;269.3
        mov       DWORD PTR [4+esp], edx                        ;269.3
        push      32                                            ;269.3
        push      ecx                                           ;269.3
        push      OFFSET FLAT: __STRLITPACK_216.0.4             ;269.3
        push      -2088435968                                   ;269.3
        push      2000                                          ;269.3
        lea       ebx, DWORD PTR [84+esp]                       ;269.3
        push      ebx                                           ;269.3
        call      _for_read_seq                                 ;269.3
                                ; LOE
.B4.26:                         ; Preds .B4.25
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_UETRAVELPENALTY+36] ;270.3
        lea       ebx, DWORD PTR [80+esp]                       ;270.3
        imul      eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_UETRAVELPENALTY+48] ;270.3
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_UETRAVELPENALTY+24] ;270.3
        shl       edx, 2                                        ;270.3
        imul      edx, eax                                      ;270.3
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_UETRAVELPENALTY] ;270.3
        mov       DWORD PTR [88+esp], 0                         ;270.3
        mov       DWORD PTR [80+esp], edx                       ;270.3
        mov       DWORD PTR [84+esp], ecx                       ;270.3
        push      32                                            ;270.3
        push      ebx                                           ;270.3
        push      OFFSET FLAT: __STRLITPACK_217.0.4             ;270.3
        push      -2088435968                                   ;270.3
        push      2001                                          ;270.3
        lea       esi, DWORD PTR [108+esp]                      ;270.3
        push      esi                                           ;270.3
        call      _for_read_seq                                 ;270.3
                                ; LOE
.B4.27:                         ; Preds .B4.26
        add       esp, 308                                      ;272.1
        pop       ebx                                           ;272.1
        pop       edi                                           ;272.1
        pop       esi                                           ;272.1
        mov       esp, ebp                                      ;272.1
        pop       ebp                                           ;272.1
        ret                                                     ;272.1
                                ; LOE
.B4.28:                         ; Preds .B4.24
        mov       edi, DWORD PTR [4+esp]                        ;
        imul      edi, DWORD PTR [240+esp]                      ;
        pxor      xmm0, xmm0                                    ;267.3
        imul      ebx, DWORD PTR [244+esp]                      ;
        mov       DWORD PTR [220+esp], eax                      ;
        mov       eax, esi                                      ;267.3
        shl       edx, 2                                        ;
        and       eax, -8                                       ;267.3
        mov       DWORD PTR [228+esp], eax                      ;267.3
        mov       eax, DWORD PTR [248+esp]                      ;
        sub       eax, edx                                      ;
        sub       edx, edi                                      ;
        add       eax, edx                                      ;
        sub       edi, ebx                                      ;
        add       eax, edi                                      ;
        lea       edx, DWORD PTR [esi*4]                        ;
        mov       DWORD PTR [esp], 0                            ;
        add       eax, ebx                                      ;
        mov       DWORD PTR [236+esp], eax                      ;
        mov       eax, DWORD PTR [220+esp]                      ;
        mov       ebx, DWORD PTR [esp]                          ;
        mov       DWORD PTR [60+esp], edx                       ;
        mov       DWORD PTR [56+esp], ecx                       ;
                                ; LOE ebx esi
.B4.29:                         ; Preds .B4.38 .B4.31 .B4.59 .B4.28
        cmp       esi, 24                                       ;267.3
        jle       .B4.52        ; Prob 0%                       ;267.3
                                ; LOE ebx esi
.B4.30:                         ; Preds .B4.29
        mov       eax, DWORD PTR [240+esp]                      ;267.3
        imul      eax, ebx                                      ;267.3
        mov       edx, DWORD PTR [244+esp]                      ;267.3
        imul      edx, DWORD PTR [252+esp]                      ;267.3
        add       eax, DWORD PTR [248+esp]                      ;267.3
        push      DWORD PTR [60+esp]                            ;267.3
        push      0                                             ;267.3
        add       edx, eax                                      ;267.3
        push      edx                                           ;267.3
        call      __intel_fast_memset                           ;267.3
                                ; LOE ebx esi
.B4.87:                         ; Preds .B4.30
        add       esp, 12                                       ;267.3
                                ; LOE ebx esi
.B4.31:                         ; Preds .B4.56 .B4.87
        inc       ebx                                           ;267.3
        cmp       ebx, DWORD PTR [220+esp]                      ;267.3
        jae       .B4.37        ; Prob 18%                      ;267.3
        jmp       .B4.29        ; Prob 100%                     ;267.3
                                ; LOE ebx esi
.B4.32:                         ; Preds .B4.19
        imul      edi, DWORD PTR [240+esp]                      ;
        mov       ebx, edx                                      ;264.3
        pxor      xmm0, xmm0                                    ;264.3
        and       ebx, -8                                       ;264.3
        mov       DWORD PTR [228+esp], ebx                      ;264.3
        mov       ebx, eax                                      ;
        shl       esi, 2                                        ;
        sub       ebx, esi                                      ;
        sub       esi, edi                                      ;
        mov       DWORD PTR [220+esp], ecx                      ;
        xor       ecx, ecx                                      ;
        add       ebx, esi                                      ;
        lea       esi, DWORD PTR [edx*4]                        ;
        mov       DWORD PTR [56+esp], ecx                       ;
        add       ebx, edi                                      ;
        mov       DWORD PTR [4+esp], eax                        ;
        mov       DWORD PTR [esp], ecx                          ;
        mov       DWORD PTR [60+esp], esi                       ;
        mov       DWORD PTR [212+esp], eax                      ;
        mov       DWORD PTR [236+esp], ebx                      ;
        mov       ecx, DWORD PTR [220+esp]                      ;
        mov       esi, DWORD PTR [esp]                          ;
        mov       eax, DWORD PTR [4+esp]                        ;
        mov       edi, DWORD PTR [56+esp]                       ;
        mov       DWORD PTR [252+esp], edx                      ;
                                ; LOE eax ebx esi edi
.B4.33:                         ; Preds .B4.35 .B4.47 .B4.32
        cmp       DWORD PTR [252+esp], 24                       ;264.3
        jle       .B4.40        ; Prob 0%                       ;264.3
                                ; LOE eax ebx esi edi
.B4.34:                         ; Preds .B4.33
        push      DWORD PTR [60+esp]                            ;264.3
        push      0                                             ;264.3
        push      ebx                                           ;264.3
        mov       DWORD PTR [16+esp], eax                       ;264.3
        call      __intel_fast_memset                           ;264.3
                                ; LOE ebx esi edi
.B4.88:                         ; Preds .B4.34
        mov       eax, DWORD PTR [16+esp]                       ;
        add       esp, 12                                       ;264.3
                                ; LOE eax ebx esi edi al ah
.B4.35:                         ; Preds .B4.44 .B4.88
        inc       edi                                           ;264.3
        mov       edx, DWORD PTR [240+esp]                      ;264.3
        add       eax, edx                                      ;264.3
        add       ebx, edx                                      ;264.3
        add       esi, edx                                      ;264.3
        cmp       edi, DWORD PTR [220+esp]                      ;264.3
        jb        .B4.33        ; Prob 82%                      ;264.3
        jmp       .B4.20        ; Prob 100%                     ;264.3
                                ; LOE eax ebx esi edi
.B4.37:                         ; Preds .B4.31 .B4.59           ; Infreq
        mov       eax, DWORD PTR [252+esp]                      ;267.3
        inc       eax                                           ;267.3
        mov       DWORD PTR [252+esp], eax                      ;267.3
        cmp       eax, DWORD PTR [56+esp]                       ;267.3
        jae       .B4.25        ; Prob 18%                      ;267.3
                                ; LOE esi
.B4.38:                         ; Preds .B4.37                  ; Infreq
        xor       ebx, ebx                                      ;
        jmp       .B4.29        ; Prob 100%                     ;
                                ; LOE ebx esi
.B4.40:                         ; Preds .B4.33                  ; Infreq
        cmp       DWORD PTR [252+esp], 8                        ;264.3
        jl        .B4.51        ; Prob 10%                      ;264.3
                                ; LOE eax ebx esi edi
.B4.41:                         ; Preds .B4.40                  ; Infreq
        xor       ecx, ecx                                      ;264.3
        mov       DWORD PTR [248+esp], ecx                      ;264.3
        pxor      xmm0, xmm0                                    ;
        mov       ecx, DWORD PTR [236+esp]                      ;
        mov       edx, DWORD PTR [228+esp]                      ;264.3
        mov       DWORD PTR [esp], ebx                          ;
        mov       DWORD PTR [4+esp], eax                        ;
        add       ecx, esi                                      ;
        mov       DWORD PTR [244+esp], ecx                      ;
        mov       ecx, DWORD PTR [212+esp]                      ;
        mov       ebx, DWORD PTR [248+esp]                      ;
        mov       DWORD PTR [56+esp], edi                       ;
        mov       edi, edx                                      ;
        add       ecx, esi                                      ;
        mov       eax, ecx                                      ;
        mov       ecx, DWORD PTR [244+esp]                      ;
                                ; LOE eax edx ecx ebx esi edi xmm0
.B4.42:                         ; Preds .B4.42 .B4.41           ; Infreq
        movups    XMMWORD PTR [eax+ebx*4], xmm0                 ;264.3
        movups    XMMWORD PTR [16+ecx+ebx*4], xmm0              ;264.3
        add       ebx, 8                                        ;264.3
        cmp       ebx, edi                                      ;264.3
        jb        .B4.42        ; Prob 82%                      ;264.3
                                ; LOE eax edx ecx ebx esi edi xmm0
.B4.43:                         ; Preds .B4.42                  ; Infreq
        mov       ebx, DWORD PTR [esp]                          ;
        mov       eax, DWORD PTR [4+esp]                        ;
        mov       edi, DWORD PTR [56+esp]                       ;
                                ; LOE eax edx ebx esi edi
.B4.44:                         ; Preds .B4.43 .B4.51           ; Infreq
        cmp       edx, DWORD PTR [252+esp]                      ;264.3
        jae       .B4.35        ; Prob 10%                      ;264.3
                                ; LOE eax edx ebx esi edi
.B4.45:                         ; Preds .B4.44                  ; Infreq
        mov       DWORD PTR [56+esp], edi                       ;
        xor       ecx, ecx                                      ;
        mov       edi, DWORD PTR [252+esp]                      ;
                                ; LOE eax edx ecx ebx esi edi
.B4.46:                         ; Preds .B4.46 .B4.45           ; Infreq
        mov       DWORD PTR [eax+edx*4], ecx                    ;264.3
        inc       edx                                           ;264.3
        cmp       edx, edi                                      ;264.3
        jb        .B4.46        ; Prob 82%                      ;264.3
                                ; LOE eax edx ecx ebx esi edi
.B4.47:                         ; Preds .B4.46                  ; Infreq
        mov       edi, DWORD PTR [56+esp]                       ;
        inc       edi                                           ;264.3
        mov       edx, DWORD PTR [240+esp]                      ;264.3
        add       eax, edx                                      ;264.3
        add       ebx, edx                                      ;264.3
        add       esi, edx                                      ;264.3
        cmp       edi, DWORD PTR [220+esp]                      ;264.3
        jb        .B4.33        ; Prob 82%                      ;264.3
        jmp       .B4.20        ; Prob 100%                     ;264.3
                                ; LOE eax ebx esi edi
.B4.51:                         ; Preds .B4.40                  ; Infreq
        xor       edx, edx                                      ;264.3
        jmp       .B4.44        ; Prob 100%                     ;264.3
                                ; LOE eax edx ebx esi edi
.B4.52:                         ; Preds .B4.29                  ; Infreq
        cmp       esi, 8                                        ;267.3
        jl        .B4.61        ; Prob 10%                      ;267.3
                                ; LOE ebx esi
.B4.53:                         ; Preds .B4.52                  ; Infreq
        xor       edx, edx                                      ;267.3
        mov       DWORD PTR [4+esp], edx                        ;267.3
        pxor      xmm0, xmm0                                    ;
        mov       edx, DWORD PTR [244+esp]                      ;
        imul      edx, DWORD PTR [252+esp]                      ;
        mov       eax, DWORD PTR [240+esp]                      ;
        imul      eax, ebx                                      ;
        mov       edi, DWORD PTR [248+esp]                      ;
        mov       ecx, DWORD PTR [228+esp]                      ;267.3
        mov       DWORD PTR [esp], esi                          ;
        mov       esi, ecx                                      ;
        add       edi, edx                                      ;
        add       edx, DWORD PTR [236+esp]                      ;
        add       edi, eax                                      ;
        add       edx, eax                                      ;
        mov       eax, DWORD PTR [4+esp]                        ;
                                ; LOE eax edx ecx ebx esi edi xmm0
.B4.54:                         ; Preds .B4.54 .B4.53           ; Infreq
        movups    XMMWORD PTR [edx+eax*4], xmm0                 ;267.3
        movups    XMMWORD PTR [16+edi+eax*4], xmm0              ;267.3
        add       eax, 8                                        ;267.3
        cmp       eax, esi                                      ;267.3
        jb        .B4.54        ; Prob 82%                      ;267.3
                                ; LOE eax edx ecx ebx esi edi xmm0
.B4.55:                         ; Preds .B4.54                  ; Infreq
        mov       esi, DWORD PTR [esp]                          ;
                                ; LOE ecx ebx esi
.B4.56:                         ; Preds .B4.55 .B4.61           ; Infreq
        cmp       ecx, esi                                      ;267.3
        jae       .B4.31        ; Prob 10%                      ;267.3
                                ; LOE ecx ebx esi
.B4.57:                         ; Preds .B4.56                  ; Infreq
        mov       eax, DWORD PTR [244+esp]                      ;
        imul      eax, DWORD PTR [252+esp]                      ;
        mov       edx, DWORD PTR [240+esp]                      ;
        imul      edx, ebx                                      ;
        add       eax, DWORD PTR [236+esp]                      ;
        add       eax, edx                                      ;
        xor       edx, edx                                      ;
                                ; LOE eax edx ecx ebx esi
.B4.58:                         ; Preds .B4.58 .B4.57           ; Infreq
        mov       DWORD PTR [eax+ecx*4], edx                    ;267.3
        inc       ecx                                           ;267.3
        cmp       ecx, esi                                      ;267.3
        jb        .B4.58        ; Prob 82%                      ;267.3
                                ; LOE eax edx ecx ebx esi
.B4.59:                         ; Preds .B4.58                  ; Infreq
        inc       ebx                                           ;267.3
        cmp       ebx, DWORD PTR [220+esp]                      ;267.3
        jae       .B4.37        ; Prob 18%                      ;267.3
        jmp       .B4.29        ; Prob 100%                     ;267.3
                                ; LOE ebx esi
.B4.61:                         ; Preds .B4.52                  ; Infreq
        xor       ecx, ecx                                      ;267.3
        jmp       .B4.56        ; Prob 100%                     ;267.3
                                ; LOE ecx ebx esi
.B4.62:                         ; Preds .B4.12                  ; Infreq
        mov       DWORD PTR [64+esp], 0                         ;253.5
        lea       eax, DWORD PTR [24+esp]                       ;253.5
        mov       DWORD PTR [24+esp], 34                        ;253.5
        mov       DWORD PTR [28+esp], OFFSET FLAT: __STRLITPACK_174 ;253.5
        push      32                                            ;253.5
        push      eax                                           ;253.5
        push      OFFSET FLAT: __STRLITPACK_209.0.4             ;253.5
        push      -2088435968                                   ;253.5
        push      911                                           ;253.5
        lea       edx, DWORD PTR [84+esp]                       ;253.5
        push      edx                                           ;253.5
        call      _for_write_seq_lis                            ;253.5
                                ; LOE
.B4.63:                         ; Preds .B4.62                  ; Infreq
        push      32                                            ;254.5
        xor       eax, eax                                      ;254.5
        push      eax                                           ;254.5
        push      eax                                           ;254.5
        push      -2088435968                                   ;254.5
        push      eax                                           ;254.5
        push      OFFSET FLAT: __STRLITPACK_210                 ;254.5
        call      _for_stop_core                                ;254.5
                                ; LOE
.B4.89:                         ; Preds .B4.63                  ; Infreq
        add       esp, 48                                       ;254.5
        jmp       .B4.13        ; Prob 100%                     ;254.5
                                ; LOE
.B4.64:                         ; Preds .B4.10                  ; Infreq
        mov       DWORD PTR [64+esp], 0                         ;257.5
        lea       eax, DWORD PTR [48+esp]                       ;257.5
        mov       DWORD PTR [48+esp], 41                        ;257.5
        mov       DWORD PTR [52+esp], OFFSET FLAT: __STRLITPACK_172 ;257.5
        push      32                                            ;257.5
        push      eax                                           ;257.5
        push      OFFSET FLAT: __STRLITPACK_211.0.4             ;257.5
        push      -2088435968                                   ;257.5
        push      911                                           ;257.5
        lea       edx, DWORD PTR [84+esp]                       ;257.5
        push      edx                                           ;257.5
        call      _for_write_seq_lis                            ;257.5
                                ; LOE
.B4.65:                         ; Preds .B4.64                  ; Infreq
        push      32                                            ;258.5
        xor       eax, eax                                      ;258.5
        push      eax                                           ;258.5
        push      eax                                           ;258.5
        push      -2088435968                                   ;258.5
        push      eax                                           ;258.5
        push      OFFSET FLAT: __STRLITPACK_212                 ;258.5
        call      _for_stop_core                                ;258.5
                                ; LOE
.B4.90:                         ; Preds .B4.65                  ; Infreq
        add       esp, 48                                       ;258.5
        jmp       .B4.13        ; Prob 100%                     ;258.5
                                ; LOE
.B4.66:                         ; Preds .B4.8                   ; Infreq
        mov       eax, 32                                       ;241.5
        lea       edx, DWORD PTR [16+esp]                       ;241.5
        mov       DWORD PTR [64+esp], 0                         ;241.5
        mov       DWORD PTR [16+esp], eax                       ;241.5
        mov       DWORD PTR [20+esp], OFFSET FLAT: __STRLITPACK_181 ;241.5
        push      eax                                           ;241.5
        push      edx                                           ;241.5
        push      OFFSET FLAT: __STRLITPACK_203.0.4             ;241.5
        push      -2088435968                                   ;241.5
        push      911                                           ;241.5
        lea       ecx, DWORD PTR [84+esp]                       ;241.5
        push      ecx                                           ;241.5
        call      _for_write_seq_lis                            ;241.5
                                ; LOE ebx
.B4.67:                         ; Preds .B4.66                  ; Infreq
        push      32                                            ;242.5
        xor       eax, eax                                      ;242.5
        push      eax                                           ;242.5
        push      eax                                           ;242.5
        push      -2088435968                                   ;242.5
        push      eax                                           ;242.5
        push      OFFSET FLAT: __STRLITPACK_204                 ;242.5
        call      _for_stop_core                                ;242.5
                                ; LOE ebx
.B4.91:                         ; Preds .B4.67                  ; Infreq
        add       esp, 48                                       ;242.5
        jmp       .B4.9         ; Prob 100%                     ;242.5
                                ; LOE ebx
.B4.68:                         ; Preds .B4.6                   ; Infreq
        mov       DWORD PTR [64+esp], 0                         ;245.5
        lea       eax, DWORD PTR [40+esp]                       ;245.5
        mov       DWORD PTR [40+esp], 43                        ;245.5
        mov       DWORD PTR [44+esp], OFFSET FLAT: __STRLITPACK_179 ;245.5
        push      32                                            ;245.5
        push      eax                                           ;245.5
        push      OFFSET FLAT: __STRLITPACK_205.0.4             ;245.5
        push      -2088435968                                   ;245.5
        push      911                                           ;245.5
        lea       edx, DWORD PTR [84+esp]                       ;245.5
        push      edx                                           ;245.5
        call      _for_write_seq_lis                            ;245.5
                                ; LOE ebx
.B4.69:                         ; Preds .B4.68                  ; Infreq
        push      32                                            ;246.5
        xor       eax, eax                                      ;246.5
        push      eax                                           ;246.5
        push      eax                                           ;246.5
        push      -2088435968                                   ;246.5
        push      eax                                           ;246.5
        push      OFFSET FLAT: __STRLITPACK_206                 ;246.5
        call      _for_stop_core                                ;246.5
                                ; LOE ebx
.B4.92:                         ; Preds .B4.69                  ; Infreq
        add       esp, 48                                       ;246.5
        jmp       .B4.9         ; Prob 100%                     ;246.5
                                ; LOE ebx
.B4.70:                         ; Preds .B4.4                   ; Infreq
        mov       DWORD PTR [64+esp], 0                         ;229.5
        lea       eax, DWORD PTR [8+esp]                        ;229.5
        mov       DWORD PTR [8+esp], 35                         ;229.5
        mov       DWORD PTR [12+esp], OFFSET FLAT: __STRLITPACK_189 ;229.5
        push      32                                            ;229.5
        push      eax                                           ;229.5
        push      OFFSET FLAT: __STRLITPACK_197.0.4             ;229.5
        push      -2088435968                                   ;229.5
        push      911                                           ;229.5
        lea       edx, DWORD PTR [84+esp]                       ;229.5
        push      edx                                           ;229.5
        call      _for_write_seq_lis                            ;229.5
                                ; LOE ebx
.B4.71:                         ; Preds .B4.70                  ; Infreq
        push      32                                            ;230.5
        xor       eax, eax                                      ;230.5
        push      eax                                           ;230.5
        push      eax                                           ;230.5
        push      -2088435968                                   ;230.5
        push      eax                                           ;230.5
        push      OFFSET FLAT: __STRLITPACK_198                 ;230.5
        call      _for_stop_core                                ;230.5
                                ; LOE ebx
.B4.93:                         ; Preds .B4.71                  ; Infreq
        add       esp, 48                                       ;230.5
        jmp       .B4.5         ; Prob 100%                     ;230.5
                                ; LOE ebx
.B4.72:                         ; Preds .B4.2                   ; Infreq
        mov       DWORD PTR [64+esp], 0                         ;233.5
        lea       eax, DWORD PTR [32+esp]                       ;233.5
        mov       DWORD PTR [32+esp], 46                        ;233.5
        mov       DWORD PTR [36+esp], OFFSET FLAT: __STRLITPACK_187 ;233.5
        push      32                                            ;233.5
        push      eax                                           ;233.5
        push      OFFSET FLAT: __STRLITPACK_199.0.4             ;233.5
        push      -2088435968                                   ;233.5
        push      911                                           ;233.5
        lea       edx, DWORD PTR [84+esp]                       ;233.5
        push      edx                                           ;233.5
        call      _for_write_seq_lis                            ;233.5
                                ; LOE ebx
.B4.73:                         ; Preds .B4.72                  ; Infreq
        push      32                                            ;234.5
        xor       eax, eax                                      ;234.5
        push      eax                                           ;234.5
        push      eax                                           ;234.5
        push      -2088435968                                   ;234.5
        push      eax                                           ;234.5
        push      OFFSET FLAT: __STRLITPACK_200                 ;234.5
        call      _for_stop_core                                ;234.5
                                ; LOE ebx
.B4.94:                         ; Preds .B4.73                  ; Infreq
        add       esp, 48                                       ;234.5
        jmp       .B4.5         ; Prob 100%                     ;234.5
        ALIGN     16
                                ; LOE ebx
; mark_end;
_READUETRAVELTIME ENDP
_TEXT	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_195.0.4	DB	56
	DB	4
	DB	13
	DB	0
	DB	16
	DB	3
	DB	27
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_199.0.4	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_196.0.4	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	26
	DB	0
	DB	56
	DB	4
	DB	15
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_197.0.4	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_201.0.4	DB	56
	DB	4
	DB	13
	DB	0
	DB	16
	DB	3
	DB	27
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_205.0.4	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_202.0.4	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	26
	DB	0
	DB	56
	DB	4
	DB	15
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_203.0.4	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_207.0.4	DB	56
	DB	4
	DB	13
	DB	0
	DB	16
	DB	3
	DB	27
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_211.0.4	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_208.0.4	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	26
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_209.0.4	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_213.0.4	DB	9
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_214.0.4	DB	9
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_215.0.4	DB	9
	DB	3
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_216.0.4	DB	26
	DB	5
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_217.0.4	DB	26
	DB	5
	DB	1
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _READUETRAVELTIME
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _RANXY
; mark_begin;
       ALIGN     16
	PUBLIC _RANXY
_RANXY	PROC NEAR 
; parameter 1: 4 + esp
.B5.1:                          ; Preds .B5.0
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_RANCT] ;278.2
        cmp       eax, 1000000                                  ;282.4
        jl        L6            ; Prob 50%                      ;282.4
        mov       eax, 0                                        ;282.4
L6:                                                             ;
        inc       eax                                           ;284.2
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_RANCT], eax ;284.2
        fld       DWORD PTR [_DYNUST_MAIN_MODULE_mp_RANX-4+eax*4] ;287.2
        ret                                                     ;287.2
        ALIGN     16
                                ; LOE
; mark_end;
_RANXY ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _RANXY
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DB 3 DUP ( 0H)	; pad
_2il0floatpacket.6	DD	042700000H
_2il0floatpacket.7	DD	080000000H
_2il0floatpacket.8	DD	04b000000H
_2il0floatpacket.9	DD	03f000000H
_2il0floatpacket.10	DD	0bf000000H
__STRLITPACK_54	DB	111
	DB	117
	DB	116
	DB	112
	DB	117
	DB	116
	DB	95
	DB	118
	DB	101
	DB	104
	DB	105
	DB	99
	DB	108
	DB	101
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_53	DB	111
	DB	108
	DB	100
	DB	0
__STRLITPACK_52	DB	111
	DB	117
	DB	116
	DB	112
	DB	117
	DB	116
	DB	95
	DB	112
	DB	97
	DB	116
	DB	104
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
__STRLITPACK_51	DB	111
	DB	108
	DB	100
	DB	0
__STRLITPACK_50	DB	111
	DB	117
	DB	116
	DB	112
	DB	117
	DB	116
	DB	95
	DB	118
	DB	101
	DB	104
	DB	105
	DB	99
	DB	108
	DB	101
	DB	95
	DB	102
	DB	110
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_49	DB	117
	DB	110
	DB	107
	DB	110
	DB	111
	DB	119
	DB	110
	DB	0
__STRLITPACK_48	DB	111
	DB	117
	DB	116
	DB	112
	DB	117
	DB	116
	DB	95
	DB	112
	DB	97
	DB	116
	DB	104
	DB	95
	DB	102
	DB	110
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_47	DB	117
	DB	110
	DB	107
	DB	110
	DB	111
	DB	119
	DB	110
	DB	0
__STRLITPACK_45	DB	32
	DB	32
	DB	32
	DB	35
	DB	32
	DB	111
	DB	102
	DB	32
	DB	118
	DB	101
	DB	104
	DB	105
	DB	99
	DB	108
	DB	101
	DB	115
	DB	32
	DB	105
	DB	110
	DB	32
	DB	116
	DB	104
	DB	101
	DB	32
	DB	102
	DB	105
	DB	108
	DB	101
	DB	44
	DB	32
	DB	77
	DB	97
	DB	120
	DB	32
	DB	35
	DB	32
	DB	111
	DB	102
	DB	32
	DB	83
	DB	84
	DB	79
	DB	80
	DB	115
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_42	DB	100
	DB	101
	DB	108
	DB	32
	DB	46
	DB	92
	DB	111
	DB	117
	DB	116
	DB	112
	DB	117
	DB	116
	DB	95
	DB	118
	DB	101
	DB	104
	DB	105
	DB	99
	DB	108
	DB	101
	DB	46
	DB	100
	DB	97
	DB	116
	DB	32
	DB	111
	DB	117
	DB	116
	DB	112
	DB	117
	DB	116
	DB	95
	DB	112
	DB	97
	DB	116
	DB	104
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_41	DB	114
	DB	101
	DB	110
	DB	97
	DB	109
	DB	101
	DB	32
	DB	46
	DB	92
	DB	111
	DB	117
	DB	116
	DB	112
	DB	117
	DB	116
	DB	95
	DB	118
	DB	101
	DB	104
	DB	105
	DB	99
	DB	108
	DB	101
	DB	95
	DB	102
	DB	110
	DB	46
	DB	100
	DB	97
	DB	116
	DB	32
	DB	111
	DB	117
	DB	116
	DB	112
	DB	117
	DB	116
	DB	95
	DB	118
	DB	101
	DB	104
	DB	105
	DB	99
	DB	108
	DB	101
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_40	DB	114
	DB	101
	DB	110
	DB	97
	DB	109
	DB	101
	DB	32
	DB	46
	DB	92
	DB	111
	DB	117
	DB	116
	DB	112
	DB	117
	DB	116
	DB	95
	DB	112
	DB	97
	DB	116
	DB	104
	DB	95
	DB	102
	DB	110
	DB	46
	DB	100
	DB	97
	DB	116
	DB	32
	DB	111
	DB	117
	DB	116
	DB	112
	DB	117
	DB	116
	DB	95
	DB	112
	DB	97
	DB	116
	DB	104
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
_2il0floatpacket.74	DD	041200000H
_2il0floatpacket.75	DD	080000000H
_2il0floatpacket.76	DD	04b000000H
_2il0floatpacket.77	DD	03f000000H
_2il0floatpacket.78	DD	0bf000000H
__STRLITPACK_154	DB	85
	DB	69
	DB	84
	DB	114
	DB	97
	DB	118
	DB	101
	DB	108
	DB	84
	DB	105
	DB	109
	DB	101
	DB	46
	DB	98
	DB	105
	DB	112
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_153	DB	117
	DB	110
	DB	107
	DB	110
	DB	111
	DB	119
	DB	110
	DB	0
__STRLITPACK_152	DB	117
	DB	110
	DB	102
	DB	111
	DB	114
	DB	109
	DB	97
	DB	116
	DB	116
	DB	101
	DB	100
	DB	0
__STRLITPACK_150	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	119
	DB	104
	DB	101
	DB	110
	DB	32
	DB	111
	DB	112
	DB	101
	DB	110
	DB	105
	DB	110
	DB	103
	DB	32
	DB	85
	DB	69
	DB	84
	DB	114
	DB	97
	DB	118
	DB	101
	DB	108
	DB	84
	DB	105
	DB	109
	DB	101
	DB	46
	DB	98
	DB	105
	DB	112
	DB	0
__STRLITPACK_157	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_149	DB	85
	DB	69
	DB	80
	DB	101
	DB	110
	DB	97
	DB	108
	DB	116
	DB	121
	DB	46
	DB	98
	DB	105
	DB	112
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_148	DB	117
	DB	110
	DB	107
	DB	110
	DB	111
	DB	119
	DB	110
	DB	0
__STRLITPACK_147	DB	117
	DB	110
	DB	102
	DB	111
	DB	114
	DB	109
	DB	97
	DB	116
	DB	116
	DB	101
	DB	100
	DB	0
__STRLITPACK_145	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	119
	DB	104
	DB	101
	DB	110
	DB	32
	DB	111
	DB	112
	DB	101
	DB	110
	DB	105
	DB	110
	DB	103
	DB	32
	DB	85
	DB	69
	DB	80
	DB	101
	DB	110
	DB	97
	DB	108
	DB	116
	DB	121
	DB	46
	DB	98
	DB	105
	DB	112
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_160	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_144	DB	85
	DB	69
	DB	73
	DB	110
	DB	100
	DB	101
	DB	120
	DB	70
	DB	105
	DB	108
	DB	101
	DB	46
	DB	105
	DB	110
	DB	100
	DB	0
__STRLITPACK_143	DB	117
	DB	110
	DB	107
	DB	110
	DB	111
	DB	119
	DB	110
	DB	0
__STRLITPACK_141	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	119
	DB	104
	DB	101
	DB	110
	DB	32
	DB	111
	DB	112
	DB	101
	DB	110
	DB	105
	DB	110
	DB	103
	DB	32
	DB	85
	DB	69
	DB	73
	DB	110
	DB	100
	DB	101
	DB	120
	DB	70
	DB	105
	DB	108
	DB	101
	DB	46
	DB	105
	DB	110
	DB	100
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_163	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_194	DB	85
	DB	69
	DB	84
	DB	114
	DB	97
	DB	118
	DB	101
	DB	108
	DB	84
	DB	105
	DB	109
	DB	101
	DB	46
	DB	98
	DB	105
	DB	112
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_187	DB	85
	DB	69
	DB	84
	DB	114
	DB	97
	DB	118
	DB	101
	DB	108
	DB	84
	DB	105
	DB	109
	DB	101
	DB	46
	DB	98
	DB	105
	DB	112
	DB	32
	DB	100
	DB	111
	DB	101
	DB	115
	DB	32
	DB	110
	DB	111
	DB	116
	DB	32
	DB	101
	DB	120
	DB	105
	DB	115
	DB	116
	DB	32
	DB	119
	DB	104
	DB	101
	DB	110
	DB	32
	DB	114
	DB	101
	DB	113
	DB	117
	DB	101
	DB	115
	DB	116
	DB	101
	DB	100
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_200	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_193	DB	85
	DB	69
	DB	84
	DB	114
	DB	97
	DB	118
	DB	101
	DB	108
	DB	84
	DB	105
	DB	109
	DB	101
	DB	46
	DB	98
	DB	105
	DB	112
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_192	DB	117
	DB	110
	DB	107
	DB	110
	DB	111
	DB	119
	DB	110
	DB	0
__STRLITPACK_191	DB	117
	DB	110
	DB	102
	DB	111
	DB	114
	DB	109
	DB	97
	DB	116
	DB	116
	DB	101
	DB	100
	DB	0
__STRLITPACK_189	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	119
	DB	104
	DB	101
	DB	110
	DB	32
	DB	111
	DB	112
	DB	101
	DB	110
	DB	105
	DB	110
	DB	103
	DB	32
	DB	85
	DB	69
	DB	84
	DB	114
	DB	97
	DB	118
	DB	101
	DB	108
	DB	84
	DB	105
	DB	109
	DB	101
	DB	46
	DB	98
	DB	105
	DB	112
	DB	0
__STRLITPACK_198	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_186	DB	85
	DB	69
	DB	80
	DB	101
	DB	110
	DB	97
	DB	108
	DB	116
	DB	121
	DB	46
	DB	98
	DB	105
	DB	112
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_179	DB	85
	DB	69
	DB	80
	DB	101
	DB	110
	DB	97
	DB	108
	DB	116
	DB	121
	DB	46
	DB	98
	DB	105
	DB	112
	DB	32
	DB	100
	DB	111
	DB	101
	DB	115
	DB	32
	DB	110
	DB	111
	DB	116
	DB	32
	DB	101
	DB	120
	DB	105
	DB	115
	DB	116
	DB	32
	DB	119
	DB	104
	DB	101
	DB	110
	DB	32
	DB	114
	DB	101
	DB	113
	DB	117
	DB	101
	DB	115
	DB	116
	DB	101
	DB	100
	DB	0
__STRLITPACK_206	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_185	DB	85
	DB	69
	DB	80
	DB	101
	DB	110
	DB	97
	DB	108
	DB	116
	DB	121
	DB	46
	DB	98
	DB	105
	DB	112
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_184	DB	117
	DB	110
	DB	107
	DB	110
	DB	111
	DB	119
	DB	110
	DB	0
__STRLITPACK_183	DB	117
	DB	110
	DB	102
	DB	111
	DB	114
	DB	109
	DB	97
	DB	116
	DB	116
	DB	101
	DB	100
	DB	0
__STRLITPACK_181	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	119
	DB	104
	DB	101
	DB	110
	DB	32
	DB	111
	DB	112
	DB	101
	DB	110
	DB	105
	DB	110
	DB	103
	DB	32
	DB	85
	DB	69
	DB	80
	DB	101
	DB	110
	DB	97
	DB	108
	DB	116
	DB	121
	DB	46
	DB	98
	DB	105
	DB	112
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_204	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_178	DB	85
	DB	69
	DB	73
	DB	110
	DB	100
	DB	101
	DB	120
	DB	70
	DB	105
	DB	108
	DB	101
	DB	46
	DB	105
	DB	110
	DB	100
	DB	0
__STRLITPACK_172	DB	85
	DB	69
	DB	73
	DB	110
	DB	100
	DB	101
	DB	120
	DB	70
	DB	105
	DB	108
	DB	101
	DB	32
	DB	100
	DB	111
	DB	101
	DB	115
	DB	32
	DB	110
	DB	111
	DB	116
	DB	32
	DB	101
	DB	120
	DB	105
	DB	115
	DB	116
	DB	32
	DB	119
	DB	104
	DB	101
	DB	110
	DB	32
	DB	114
	DB	101
	DB	113
	DB	117
	DB	101
	DB	115
	DB	116
	DB	101
	DB	100
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_212	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_177	DB	85
	DB	69
	DB	73
	DB	110
	DB	100
	DB	101
	DB	120
	DB	70
	DB	105
	DB	108
	DB	101
	DB	46
	DB	105
	DB	110
	DB	100
	DB	0
__STRLITPACK_176	DB	117
	DB	110
	DB	107
	DB	110
	DB	111
	DB	119
	DB	110
	DB	0
__STRLITPACK_174	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	119
	DB	104
	DB	101
	DB	110
	DB	32
	DB	111
	DB	112
	DB	101
	DB	110
	DB	105
	DB	110
	DB	103
	DB	32
	DB	85
	DB	69
	DB	73
	DB	110
	DB	100
	DB	101
	DB	120
	DB	70
	DB	105
	DB	108
	DB	101
	DB	46
	DB	105
	DB	110
	DB	100
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_210	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE:BYTE
EXTRN	_DYNUST_FUEL_MODULE_mp_VEHFUEL:BYTE
EXTRN	_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH:BYTE
EXTRN	_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE:BYTE
EXTRN	_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_REACH_CONVERG:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ITEMAX:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ITERATION:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_FUELOUT:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NOOFSTOPS:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TOTALVEH:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_MAXMOVE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_AGGINT:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NOOFARCS:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TRAVELPENALTY:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TRAVELTIME:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ERROR:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_UETRAVELPENALTY:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_UETRAVELTIME:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ITI_NUTMP:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_RANX:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_RANCT:BYTE
_DATA	ENDS
EXTRN	_for_read_seq:PROC
EXTRN	_for_read_seq_lis_xmit:PROC
EXTRN	_for_read_seq_lis:PROC
EXTRN	_for_inquire:PROC
EXTRN	_for_write_seq:PROC
EXTRN	_for_stop_core:PROC
EXTRN	_for_dealloc_allocatable:PROC
EXTRN	_for_allocate:PROC
EXTRN	_for_read_seq_fmt_xmit:PROC
EXTRN	_for_read_seq_fmt:PROC
EXTRN	_for_alloc_allocatable:PROC
EXTRN	_for_check_mult_overflow:PROC
EXTRN	_for_write_seq_lis_xmit:PROC
EXTRN	_for_write_seq_lis:PROC
EXTRN	_for_open:PROC
EXTRN	_for_close:PROC
EXTRN	_IMSLSORT:PROC
EXTRN	_SYSTEM:PROC
EXTRN	_for_write_seq_fmt_xmit:PROC
EXTRN	_for_write_seq_fmt:PROC
EXTRN	_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_P_SIZE:PROC
EXTRN	_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_VALUE:PROC
EXTRN	__intel_fast_memset:PROC
EXTRN	__fltused:BYTE
	INCLUDELIB <ifconsol>
	INCLUDELIB <libifcoremt>
	INCLUDELIB <libifport>
	INCLUDELIB <libmmt>
	INCLUDELIB <LIBCMT>
	INCLUDELIB <libirc>
	INCLUDELIB <svml_dispmt>
	INCLUDELIB <OLDNAMES>
	END
