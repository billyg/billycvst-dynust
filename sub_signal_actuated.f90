      SUBROUTINE SIGNAL_ACTUATED(IntoOutNodeNumber,t)
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~ Copyright (C) 2002-2011 The Arizona Board of Regents on Behalf of The University of Arizona.  ~~!
!~~ Developed by Yi-Chang Chiu, Ph.D. as par to the DynusT project.                               ~~!
!~~ This program is free software: you can redistribute it and/or modify it                       ~~!
!~~ under the terms of the GNU General Public License version 2 as published by                   ~~!
!~~ the Free Software Foundation.                                                                 ~~!
!~~ This program is distributed in the hope that it will be useful,                               ~~!
!~~ but WITHOUT ANY WARRANTY; without even the implied warranty of                                ~~!
!~~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                                          ~~!
!~~ See the GNU General Public License for more details.                                          ~~!
!~~ You should have received a copy of the GNU General Public License                             ~~!
!~~ along with this program (in the file COPYING).                                                ~~!
!~~ IF not, see <http://www.gnu.org/licenses/>.                                                   ~~!
!~~ Please see http://www.dynust.net for contact information and names                            ~~!
!~~ of individual contributors.                                                                   ~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!

      USE DYNUST_MAIN_MODULE 
      USE DYNUST_NETWORK_MODULE
      USE DYNUST_SIGNAL_MODULE

	  INTEGER(1) GreenTmp
	  REAL diffas,t
	  INTEGER CurrentActPhase,currentStatus,IntroGreen,MinG,MaxG,NIBLink,AssignG
	  LOGICAL time_needed
	  INTEGER imovex
!> > > > > > > > > >                < < < < < < < < < < <
!>>>>>>>>>>>>>>>>>>>CODES START HERE<<<<<<<<<<<<<<<<<<<<<
!> > > > > > > > > >                < < < < < < < < < < <

! this is a simplifed actuated control that has some assumptions and restrictions that can be 
! later relaxed. 
!     1: GreenExt green extension which is fixed at 6 sec, this is tied in with xminPerSimInt. 
!     2: minG needs to be at least xminPerSimInt otherwise many other more complex situations need to be considered
      
      GreenExt = nint(60*xminPerSimInt) ! greenext is set to be the sim interval. Will relax in the future

! --  t_beging is the start of the current simulation interval
! --  since xminPerSimInt in in minutes, it should be multiplied by 60 to
! --   convert it into seconds.
! --
      t_begin=time_now-xminPerSimInt*60
! --
! --  t_end is the end of the current simulation interval.
! --
      t_end=time_now
      imovex = 0
! initialize green primarily in consideration of RT allowed during red
      DO iact = 1, control_array(IntoOutNodeNumber)%NPhase
        CurrentStatus = GetSignalPar(IntoOutNodeNumber,iact,1)  ! status of each phase
        DO iff = 1, GetSignalPar(IntoOutNodeNumber,iact,5)      ! no of inbound approaches in this phase
          InlinkNu = control_array(IntoOutNodeNumber)%F(iact)%Mov(iff)%InlinkNu
          DO ih = 1, m_dynust_network_arc_nde(InlinkNu)%llink%no
             IF(m_dynust_network_arc_nde(InlinkNu)%llink%p(ih) == m_dynust_network_arc_de(inlinkNu)%RightLane) THEN
               imovex = ih
               exit
             ENDIF
          ENDDO
          IF(CurrentStatus > 0.and.imovex > 0) THEN ! for active phase
            m_dynust_network_arcmove_de(InlinkNu,imovex)%green = 60*xminPerSimInt  ! always allow righ-turn during green
          ELSE ! not active phase set green only for the RT allowed phases
            ObLNu = control_array(IntoOutNodeNumber)%F(iact)%Mov(iff)%Nobappch
            DO nos = 1, ObLNu
             MovNu = control_array(IntoOutNodeNumber)%F(iact)%Mov(iff)%obappnu(nos)
               IF(MovNu == imovex) THEN
                  m_dynust_network_arcmove_de(InlinkNu,MovNu)%green = 60*xminPerSimInt ! give green time to RT allowed right-turn movement\
               ENDIF
            ENDDO
          ENDIF
        ENDDO
      ENDDO

      CurrentActPhase = control_array(IntoOutNodeNumber)%ActivePhase

      CurrentStatus = GetSignalPar(IntoOutNodeNumber,CurrentActPhase,1)
      IntoGreen     = GetSignalPar(IntoOutNodeNumber,CurrentActPhase,2)
      MinG          = max(nint(60*xminPerSimInt),GetSignalPar(IntoOutNodeNumber,CurrentActPhase,3)) ! minG needs to be at least the length of sim int - need to relax in the future
      MaxG          = GetSignalPar(IntoOutNodeNumber,CurrentActPhase,4)
      IF(MaxG < 0) THEN
        WRITE(911,*) "error in Max Green specification"
        WRITE(911,*) "check node number", m_dynust_network_node_nde(IntoOutNodeNumber)%IntoOutNodeNum
        STOP
      ENDIF
      NIBLink       = GetSignalPar(IntoOutNodeNumber,CurrentActPhase,5)
      
      time_needed = FindTimeNeeded(IntoOutNodeNumber,currentactphase) ! find IF extension is needed due to vehicle actuation

	  Select Case (CurrentStatus)
	  Case (1) ! IntoGreen of the current phase is within minG
		IF(IntoGreen+GreenExt <= MinG) THEN ! the end of the extension will not go beyond the minG - simply update
              CALL AssignGreen(IntoOutNodeNumber,CurrentActPhase,NIBlink,GreenExt)  !update Green into this phase
              control_array(IntoOutNodeNumber)%F(CurrentActPhase)%UsedG = control_array(IntoOutNodeNumber)%F(CurrentActPhase)%UsedG + GreenExt !assign green to movements			
		ELSEIF(IntoGreen+GreenExt > MinG.and.IntoGreen+GreenExt <= MaxG.and.time_needed) THEN ! the end IF sim falls in between mnG and maxG
            CALL AssignGreen(IntoOutNodeNumber,CurrentActPhase,NIBlink,GreenExt)
			control_array(IntoOutNodeNumber)%F(CurrentActPhase)%Status = 2 ! update status to 2
			control_array(IntoOutNodeNumber)%F(CurrentActPhase)%UsedG = control_array(IntoOutNodeNumber)%F(CurrentActPhase)%UsedG + GreenExt ! update intogreen
		ELSE ! need to gap out
            GreenTmp = min(nint(60*xminPerSimInt),MinG - IntoGreen) ! gap out 
            IF(GreenTmp < 0.or.greentmp > nint(60*xminPerSimInt)) THEN
               print *, "error in Greentmp"
            ENDIF
            CALL AssignGreen(IntoOutNodeNumber,CurrentActPhase,NIBlink,GreenTmp)  !update Green into this phase 
            control_array(IntoOutNodeNumber)%F(CurrentActPhase)%UsedG = 0 ! reset intogreen
			control_array(IntoOutNodeNumber)%F(CurrentActPhase)%Status = 0

			IF(CurrentActPhase == control_array(IntoOutNodeNumber)%NPhase) THEN ! update phase and update info for this new active phase
               control_array(IntoOutNodeNumber)%ActivePhase = 1
			ELSE
               control_array(IntoOutNodeNumber)%ActivePhase = control_array(IntoOutNodeNumber)%ActivePhase + 1
			ENDIF

			CurrentActPhase = control_array(IntoOutNodeNumber)%ActivePhase
            ! find minG for the to be activated phase

            GreenTmp = min(nint(60*xminPerSimInt),IntoGreen+GreenExt-MinG) ! this minG is from the previoius phase

            control_array(IntoOutNodeNumber)%F(CurrentActPhase)%UsedG = GreenTmp
            NIBLink = GetSignalPar(IntoOutNodeNumber,CurrentActPhase,5)

            minG = max(nint(60*xminPerSimInt),GetSignalPar(IntoOutNodeNumber,CurrentActPhase,3)) ! minG needs to be at least the length of sim int - need to relax in the future			  
			IF(GreenTmp < minG) THEN ! check in the initial green ALLOCATED to the next phase is within minG or not
			  control_array(IntoOutNodeNumber)%F(control_array(IntoOutNodeNumber)%ActivePhase)%Status = 1
			ELSE   
			   control_array(IntoOutNodeNumber)%F(control_array(IntoOutNodeNumber)%ActivePhase)%Status = 2
			ENDIF
			  
			CALL AssignGreen(IntoOutNodeNumber,CurrentActPhase,NIBlink,GreenTmp)  !update Green into this phase
		 ENDIF

		
	  Case (2) ! the IntroGreen at the current phase is btw minG and maxG
        IF(time_needed) THEN ! not gap out
		  IF(IntoGreen+GreenExt < MaxG) THEN
            CALL AssignGreen(IntoOutNodeNumber,CurrentActPhase,NIBlink,GreenExt)  !update Into Green
			control_array(IntoOutNodeNumber)%F(CurrentActPhase)%UsedG = control_array(IntoOutNodeNumber)%F(CurrentActPhase)%UsedG + GreenExt ! update IntoGreen
			 ! 
          ELSE ! need to Maxout
		      GreenTmp = MaxG - IntoGreen! update green for current phase
              IF(GreenTmp < 0.or.greentmp > 60*xminPerSimInt) THEN
                 print *, "error in Greentmp"
              ENDIF
              CALL AssignGreen(IntoOutNodeNumber,CurrentActPhase,NIBlink,GreenTmp)  !update Into Green
			  control_array(IntoOutNodeNumber)%F(CurrentActPhase)%UsedG = 0 ! reset intogreen
			  control_array(IntoOutNodeNumber)%F(CurrentActPhase)%Status = 1
			 ! update phase
			  IF(CurrentActPhase == control_array(IntoOutNodeNumber)%NPhase) THEN
                 control_array(IntoOutNodeNumber)%ActivePhase = 1
			  ELSE
                 control_array(IntoOutNodeNumber)%ActivePhase = control_array(IntoOutNodeNumber)%ActivePhase + 1
			  ENDIF
              
			  GreenTmp = min(nint(60*xminPerSimInt),IntoGreen+GreenExt-MaxG) ! update green for next phase
              IF(GreenTmp < 0.or.greentmp > nint(60*xminPerSimInt)) THEN
                print *, "error in Greentmp"
              ENDIF

              MinG = max(nint(60*xminPerSimInt),GetSignalPar(IntoOutNodeNumber,CurrentActPhase,3)) ! minG needs to be at least the length of sim int - need to relax in the future				  
			  
			  IF(GreenTmp < MinG) THEN
			    control_array(IntoOutNodeNumber)%F(control_array(IntoOutNodeNumber)%ActivePhase)%Status = 1
			  ELSE
			    control_array(IntoOutNodeNumber)%F(control_array(IntoOutNodeNumber)%ActivePhase)%Status = 2			  
			  ENDIF
			  
			  CurrentActPhase = control_array(IntoOutNodeNumber)%ActivePhase

              control_array(IntoOutNodeNumber)%F(CurrentActPhase)%UsedG = GreenTmp
              NIBLink = GetSignalPar(IntoOutNodeNumber,CurrentActPhase,5)
			  CALL AssignGreen(IntoOutNodeNumber,CurrentActPhase,NIBlink,GreenTmp)  !update Green into this phase
		  ENDIF
		ELSE ! gap out
			 ! update phase
              control_array(IntoOutNodeNumber)%F(CurrentActPhase)%UsedG = 0 ! reset intogreen
			  control_array(IntoOutNodeNumber)%F(CurrentActPhase)%Status = 0
!			  GreenTmp = max(0,minG-intoGreen)
              GreenTmp = 0
			  CALL AssignGreen(IntoOutNodeNumber,CurrentActPhase,NIBlink,GreenTmp)  !update Green into this phase
			  			  
			  IF(CurrentActPhase == control_array(IntoOutNodeNumber)%NPhase) THEN
                 control_array(IntoOutNodeNumber)%ActivePhase = 1
			  ELSE
                 control_array(IntoOutNodeNumber)%ActivePhase = control_array(IntoOutNodeNumber)%ActivePhase + 1
			  ENDIF

              GreenTmp = GreenExt ! use the minG from the current expired phase			  
              CurrentActPhase = control_array(IntoOutNodeNumber)%ActivePhase
              MinG = max(nint(60*xminPerSimInt),GetSignalPar(IntoOutNodeNumber,CurrentActPhase,3)) ! minG needs to be at least the length of sim int - need to relax in the future	
!			  GreenTmp = min(GreenExt,MinG) ! update green for next phase

              control_array(IntoOutNodeNumber)%F(CurrentActPhase)%UsedG = control_array(IntoOutNodeNumber)%F(CurrentActPhase)%UsedG + GreenTmp
              NIBLink = GetSignalPar(IntoOutNodeNumber,CurrentActPhase,5)

			  IF(control_array(IntoOutNodeNumber)%F(CurrentActPhase)%UsedG < minG) THEN
			    control_array(IntoOutNodeNumber)%F(control_array(IntoOutNodeNumber)%ActivePhase)%Status = 1
			  ELSE
			    control_array(IntoOutNodeNumber)%F(control_array(IntoOutNodeNumber)%ActivePhase)%Status = 2			  
			  ENDIF

			  CALL AssignGreen(IntoOutNodeNumber,CurrentActPhase,NIBlink,GreenTmp)  !update Green into this phase
		ENDIF

	  End Select
	  
END SUBROUTINE