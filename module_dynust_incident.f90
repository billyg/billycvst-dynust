MODULE INCIDENT_MODULE
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~ Copyright (C) 2002-2011 The Arizona Board of Regents on Behalf of The University of Arizona.  ~~!
!~~ Developed by Yi-Chang Chiu, Ph.D. as par to the DynusT project.                               ~~!
!~~ This program is free software: you can redistribute it and/or modify it                       ~~!
!~~ under the terms of the GNU General Public License version 2 as published by                   ~~!
!~~ the Free Software Foundation.                                                                 ~~!
!~~ This program is distributed in the hope that it will be useful,                               ~~!
!~~ but WITHOUT ANY WARRANTY; without even the implied warranty of                                ~~!
!~~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                                          ~~!
!~~ See the GNU General Public License for more details.                                          ~~!
!~~ You should have received a copy of the GNU General Public License                             ~~!
!~~ along with this program (in the file COPYING).                                                ~~!
!~~ IF not, see <http://www.gnu.org/licenses/>.                                                   ~~!
!~~ Please see http://www.dynust.net for contact information and names                            ~~!
!~~ of individual contributors.                                                                   ~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
  USE DYNUST_MAIN_MODULE
  USE DYNUST_NETWORK_MODULE
  
  CONTAINS

SUBROUTINE INCI_ACTIVATE(i,tt) 
     INTEGER aggtime

     seve=inci(i,3)      
     ilink=incil(i)
     aggtime = max(1,ifix((tt/xminPerSimInt)/simPerAgg)+1)              

     IF(IncClsFlag == 0) THEN !IncClsFlag = 0 means incident.dat involves capacity reduction
       m_dynust_network_arc_de(ilink)%xl=m_dynust_network_arc_de(ilink)%nlanes*m_dynust_network_arc_nde(ilink)%s*(1-seve)
       IF(seve > 0.999) THEN 
         m_dynust_network_arc_de(ilink)%MaxFlowRate= 0
	   ELSE
        IF(.not.incistartflag(i))  m_dynust_network_arc_de(ilink)%MaxFlowRate=m_dynust_network_arc_de(ilink)%MaxFlowRate*(1-seve)
	   ENDIF
	 ENDIF
     m_dynust_network_arc_de(ilink)%LinkStatus%inci = 2 ! during incident
     m_dynust_network_arc_de(ilink)%LinkStatus%incisev = seve
END SUBROUTINE


!~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>
SUBROUTINE INCI_ADD(i)
   USE DYNUST_MAIN_MODULE

      listtotal=listtotal+1
      incilist(listtotal)=i
      ilink=incil(i)
      seve=inci(i,3)
END SUBROUTINE

!~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>
SUBROUTINE INCI_CAPACITY_RESTORE(ilink,seve)
      m_dynust_network_arc_de(ilink)%MaxFlowRate=m_dynust_network_arc_de(ilink)%MaxFlowRateOrig
	  m_dynust_network_arc_de(ilink)%xl=m_dynust_network_arc_de(ilink)%nlanes*m_dynust_network_arc_nde(ilink)%s
	  m_dynust_network_arc_de(ilink)%LinkStatus%inci = 3 ! after incident
	  m_dynust_network_arc_de(ilink)%LinkStatus%incisev = 0
      ia1=m_dynust_network_arc_nde(ilink)%ForToBackLink
	  IF(iteration == 0) cost(ia1,:)=0.0 
END SUBROUTINE

!~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>     
SUBROUTINE INCI_SCAN(tt)
      USE DYNUST_MAIN_MODULE
      DO i = 1, inci_num
         IF(tt >= inci(i,1).and.tt < inci(i,2)) THEN
            CALL INCI_ACTIVATE(i,tt)
            incistartflag(i) = .True.
         ENDIF
      ENDDO
      DO i=1,inci_num
        IF(tt >= inci(i,2)) THEN
	    IF(incistartflag(i)) THEN
            ilink=incil(i)
            CALL INCI_CAPACITY_RESTORE(ilink,inci(i,3))
	   	    incistartflag(i) = .False.
	    ENDIF
        ENDIF 
      end do
END SUBROUTINE

END MODULE