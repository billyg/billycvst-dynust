; mark_description "Intel(R) Visual Fortran Compiler XE for applications running on IA-32, Version 13.1.1.171 Build 20130313";
; mark_description "-S";
	.686P
 	.387
	OPTION DOTNAME
	ASSUME	CS:FLAT,DS:FLAT,SS:FLAT
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
TXTST0:
; -- Begin  _WRITE_PRINT_TRAJECTORY
; mark_begin;
IF @Version GE 800
  .MMX
ELSEIF @Version GE 612
  .MMX
  MMWORD TEXTEQU <QWORD>
ENDIF
IF @Version GE 800
  .XMM
ELSEIF @Version GE 614
  .XMM
  XMMWORD TEXTEQU <OWORD>
ENDIF
       ALIGN     16
	PUBLIC _WRITE_PRINT_TRAJECTORY
_WRITE_PRINT_TRAJECTORY	PROC NEAR 
; parameter 1: 8 + ebp
; parameter 2: 12 + ebp
; parameter 3: 16 + ebp
; parameter 4: 20 + ebp
.B1.1:                          ; Preds .B1.0
        push      ebp                                           ;1.12
        mov       ebp, esp                                      ;1.12
        and       esp, -16                                      ;1.12
        push      esi                                           ;1.12
        push      edi                                           ;1.12
        push      ebx                                           ;1.12
        sub       esp, 180                                      ;1.12
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_I18], 0     ;33.11
        jle       .B1.58        ; Prob 16%                      ;33.11
                                ; LOE
.B1.2:                          ; Preds .B1.1 .B1.58
        mov       eax, DWORD PTR [12+ebp]                       ;1.12
        cmp       DWORD PTR [eax], 1                            ;37.16
        je        .B1.55        ; Prob 16%                      ;37.16
                                ; LOE
.B1.3:                          ; Preds .B1.2
        movss     xmm0, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TIME_NOW] ;43.6
        divss     xmm0, DWORD PTR [_2il0floatpacket.4]          ;43.19
        mov       edi, DWORD PTR [8+ebp]                        ;43.25
        mov       ebx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE+32] ;44.16
        neg       ebx                                           ;44.3
        mov       edi, DWORD PTR [edi]                          ;43.25
        mov       edx, edi                                      ;43.6
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;43.25
        sub       edx, eax                                      ;43.6
        add       ebx, edi                                      ;44.3
        shl       edx, 5                                        ;43.6
        shl       ebx, 6                                        ;44.3
        mov       esi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;43.19
        mov       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE] ;44.3
        mov       DWORD PTR [4+esp], eax                        ;43.25
        subss     xmm0, DWORD PTR [12+esi+edx]                  ;43.6
        movsx     eax, WORD PTR [12+ecx+ebx]                    ;44.16
        movss     DWORD PTR [8+esp], xmm0                       ;43.6
        mov       DWORD PTR [esp], eax                          ;44.16
        lea       edx, DWORD PTR [-1+eax]                       ;45.3
        mov       DWORD PTR [60+esp], edx                       ;45.3
                                ; LOE esi edi
.B1.4:                          ; Preds .B1.57 .B1.3
        mov       DWORD PTR [16+esp], 0                         ;49.6
        lea       ebx, DWORD PTR [16+esp]                       ;49.6
        mov       DWORD PTR [56+esp], edi                       ;49.6
        lea       eax, DWORD PTR [56+esp]                       ;49.6
        push      32                                            ;49.6
        push      OFFSET FLAT: WRITE_PRINT_TRAJECTORY$format_pack.0.1+40 ;49.6
        push      eax                                           ;49.6
        mov       edx, DWORD PTR [16+ebp]                       ;1.12
        push      OFFSET FLAT: __STRLITPACK_13.0.1              ;49.6
        push      -2088435968                                   ;49.6
        push      DWORD PTR [edx]                               ;49.6
        push      ebx                                           ;49.6
        call      _for_write_seq_fmt                            ;49.6
                                ; LOE ebx esi edi
.B1.5:                          ; Preds .B1.4
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;49.26
        neg       edx                                           ;49.6
        add       edx, edi                                      ;49.6
        shl       edx, 8                                        ;49.6
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;49.6
        movzx     ecx, BYTE PTR [237+eax+edx]                   ;49.6
        lea       eax, DWORD PTR [92+esp]                       ;49.6
        mov       BYTE PTR [92+esp], cl                         ;49.6
        push      eax                                           ;49.6
        push      OFFSET FLAT: __STRLITPACK_14.0.1              ;49.6
        push      ebx                                           ;49.6
        call      _for_write_seq_fmt_xmit                       ;49.6
                                ; LOE ebx esi edi
.B1.6:                          ; Preds .B1.5
        sub       edi, DWORD PTR [44+esp]                       ;49.6
        lea       edx, DWORD PTR [112+esp]                      ;49.6
        shl       edi, 5                                        ;49.6
        movzx     eax, WORD PTR [22+esi+edi]                    ;49.6
        mov       WORD PTR [112+esp], ax                        ;49.6
        push      edx                                           ;49.6
        push      OFFSET FLAT: __STRLITPACK_15.0.1              ;49.6
        push      ebx                                           ;49.6
        call      _for_write_seq_fmt_xmit                       ;49.6
                                ; LOE ebx esi edi
.B1.7:                          ; Preds .B1.6
        movzx     eax, WORD PTR [20+esi+edi]                    ;49.6
        lea       edx, DWORD PTR [132+esp]                      ;49.6
        mov       WORD PTR [132+esp], ax                        ;49.6
        push      edx                                           ;49.6
        push      OFFSET FLAT: __STRLITPACK_16.0.1              ;49.6
        push      ebx                                           ;49.6
        call      _for_write_seq_fmt_xmit                       ;49.6
                                ; LOE ebx esi edi
.B1.8:                          ; Preds .B1.7
        movzx     eax, BYTE PTR [4+esi+edi]                     ;49.6
        lea       edx, DWORD PTR [152+esp]                      ;49.6
        mov       BYTE PTR [152+esp], al                        ;49.6
        push      edx                                           ;49.6
        push      OFFSET FLAT: __STRLITPACK_17.0.1              ;49.6
        push      ebx                                           ;49.6
        call      _for_write_seq_fmt_xmit                       ;49.6
                                ; LOE ebx esi edi
.B1.9:                          ; Preds .B1.8
        mov       esi, DWORD PTR [28+esi+edi]                   ;49.6
        sub       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;49.6
        imul      esi, esi, 152                                 ;49.6
        imul      ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE+32], 44 ;49.6
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;49.6
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE] ;49.6
        mov       DWORD PTR [80+esp], ecx                       ;49.6
        imul      edi, DWORD PTR [28+edx+esi], 44               ;49.137
        add       edi, eax                                      ;49.6
        sub       edi, ecx                                      ;49.6
        mov       ecx, DWORD PTR [36+edi]                       ;49.6
        lea       edi, DWORD PTR [172+esp]                      ;49.6
        mov       DWORD PTR [172+esp], ecx                      ;49.6
        push      edi                                           ;49.6
        push      OFFSET FLAT: __STRLITPACK_18.0.1              ;49.6
        push      ebx                                           ;49.6
        mov       DWORD PTR [100+esp], edx                      ;49.6
        mov       DWORD PTR [136+esp], eax                      ;49.6
        call      _for_write_seq_fmt_xmit                       ;49.6
                                ; LOE ebx esi
.B1.10:                         ; Preds .B1.9
        mov       edx, DWORD PTR [100+esp]                      ;
        lea       ecx, DWORD PTR [192+esp]                      ;49.6
        imul      edx, DWORD PTR [24+edx+esi], 44               ;49.239
        mov       eax, DWORD PTR [136+esp]                      ;
        add       eax, edx                                      ;49.6
        sub       eax, DWORD PTR [92+esp]                       ;49.6
        mov       eax, DWORD PTR [36+eax]                       ;49.6
        mov       DWORD PTR [192+esp], eax                      ;49.6
        push      ecx                                           ;49.6
        push      OFFSET FLAT: __STRLITPACK_19.0.1              ;49.6
        push      ebx                                           ;49.6
        call      _for_write_seq_fmt_xmit                       ;49.6
                                ; LOE ebx
.B1.11:                         ; Preds .B1.10
        push      DWORD PTR [8+ebp]                             ;49.6
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_P_SIZE  ;49.6
                                ; LOE eax ebx
.B1.12:                         ; Preds .B1.11
        dec       eax                                           ;49.341
        mov       DWORD PTR [108+esp], eax                      ;49.341
        lea       eax, DWORD PTR [108+esp]                      ;49.6
        push      OFFSET FLAT: __NLITPACK_0.0.1                 ;49.6
        push      eax                                           ;49.6
        push      DWORD PTR [8+ebp]                             ;49.6
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_VALUE   ;49.6
                                ; LOE ebx f1
.B1.62:                         ; Preds .B1.12
        fstp      DWORD PTR [260+esp]                           ;49.6
        movss     xmm3, DWORD PTR [260+esp]                     ;49.6
        add       esp, 116                                      ;49.6
                                ; LOE ebx xmm3
.B1.13:                         ; Preds .B1.62
        movss     xmm0, DWORD PTR [_2il0floatpacket.6]          ;49.6
        andps     xmm0, xmm3                                    ;49.6
        pxor      xmm3, xmm0                                    ;49.6
        movss     xmm1, DWORD PTR [_2il0floatpacket.7]          ;49.6
        movaps    xmm2, xmm3                                    ;49.6
        movaps    xmm7, xmm3                                    ;49.6
        cmpltss   xmm2, xmm1                                    ;49.6
        andps     xmm1, xmm2                                    ;49.6
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE+32] ;49.341
        addss     xmm7, xmm1                                    ;49.6
        neg       edx                                           ;49.6
        subss     xmm7, xmm1                                    ;49.6
        movaps    xmm6, xmm7                                    ;49.6
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE] ;49.6
        subss     xmm6, xmm3                                    ;49.6
        movss     xmm3, DWORD PTR [_2il0floatpacket.8]          ;49.6
        movaps    xmm4, xmm6                                    ;49.6
        movaps    xmm5, xmm3                                    ;49.6
        cmpnless  xmm4, xmm3                                    ;49.6
        addss     xmm5, xmm3                                    ;49.6
        cmpless   xmm6, DWORD PTR [_2il0floatpacket.9]          ;49.6
        andps     xmm4, xmm5                                    ;49.6
        andps     xmm6, xmm5                                    ;49.6
        subss     xmm7, xmm4                                    ;49.6
        addss     xmm7, xmm6                                    ;49.6
        orps      xmm7, xmm0                                    ;49.6
        cvtss2si  eax, xmm7                                     ;49.341
        add       edx, eax                                      ;49.6
        lea       eax, DWORD PTR [48+esp]                       ;49.6
        imul      esi, edx, 44                                  ;49.6
        mov       edi, DWORD PTR [36+ecx+esi]                   ;49.6
        mov       DWORD PTR [48+esp], edi                       ;49.6
        push      eax                                           ;49.6
        push      OFFSET FLAT: __STRLITPACK_20.0.1              ;49.6
        push      ebx                                           ;49.6
        call      _for_write_seq_fmt_xmit                       ;49.6
                                ; LOE ebx
.B1.14:                         ; Preds .B1.13
        mov       esi, DWORD PTR [8+ebp]                        ;49.426
        lea       ecx, DWORD PTR [124+esp]                      ;49.6
        mov       edi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;49.6
        mov       eax, DWORD PTR [esi]                          ;49.426
        mov       esi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;49.426
        neg       esi                                           ;49.6
        add       esi, eax                                      ;49.6
        shl       esi, 5                                        ;49.6
        mov       DWORD PTR [24+esp], eax                       ;49.426
        mov       edx, DWORD PTR [12+edi+esi]                   ;49.6
        mov       DWORD PTR [124+esp], edx                      ;49.6
        push      ecx                                           ;49.6
        push      OFFSET FLAT: __STRLITPACK_21.0.1              ;49.6
        push      ebx                                           ;49.6
        call      _for_write_seq_fmt_xmit                       ;49.6
                                ; LOE ebx esi edi
.B1.15:                         ; Preds .B1.14
        movss     xmm0, DWORD PTR [32+esp]                      ;49.6
        lea       eax, DWORD PTR [144+esp]                      ;49.6
        movss     DWORD PTR [144+esp], xmm0                     ;49.6
        push      eax                                           ;49.6
        push      OFFSET FLAT: __STRLITPACK_22.0.1              ;49.6
        push      ebx                                           ;49.6
        call      _for_write_seq_fmt_xmit                       ;49.6
                                ; LOE ebx esi edi
.B1.16:                         ; Preds .B1.15
        mov       eax, DWORD PTR [36+esp]                       ;49.6
        lea       edx, DWORD PTR [44+esp]                       ;49.6
        mov       DWORD PTR [44+esp], eax                       ;49.6
        push      edx                                           ;49.6
        push      OFFSET FLAT: __STRLITPACK_23.0.1              ;49.6
        push      ebx                                           ;49.6
        call      _for_write_seq_fmt_xmit                       ;49.6
                                ; LOE ebx esi edi
.B1.17:                         ; Preds .B1.16
        movzx     eax, BYTE PTR [5+edi+esi]                     ;49.6
        lea       edx, DWORD PTR [176+esp]                      ;49.6
        mov       BYTE PTR [176+esp], al                        ;49.6
        push      edx                                           ;49.6
        push      OFFSET FLAT: __STRLITPACK_24.0.1              ;49.6
        push      ebx                                           ;49.6
        call      _for_write_seq_fmt_xmit                       ;49.6
                                ; LOE ebx
.B1.18:                         ; Preds .B1.17
        mov       edi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;49.502
        lea       edx, DWORD PTR [196+esp]                      ;49.6
        neg       edi                                           ;49.6
        add       edi, DWORD PTR [72+esp]                       ;49.6
        shl       edi, 8                                        ;49.6
        mov       esi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;49.6
        movzx     eax, BYTE PTR [236+esi+edi]                   ;49.6
        mov       BYTE PTR [196+esp], al                        ;49.6
        push      edx                                           ;49.6
        push      OFFSET FLAT: __STRLITPACK_25.0.1              ;49.6
        push      ebx                                           ;49.6
        call      _for_write_seq_fmt_xmit                       ;49.6
                                ; LOE ebx
.B1.19:                         ; Preds .B1.18
        xor       eax, eax                                      ;50.6
        mov       DWORD PTR [88+esp], eax                       ;50.6
        push      32                                            ;50.6
        push      OFFSET FLAT: WRITE_PRINT_TRAJECTORY$format_pack.0.1+20 ;50.6
        push      eax                                           ;50.6
        mov       esi, DWORD PTR [16+ebp]                       ;50.6
        push      OFFSET FLAT: __STRLITPACK_26.0.1              ;50.6
        push      -2088435968                                   ;50.6
        mov       esi, DWORD PTR [esi]                          ;50.6
        push      esi                                           ;50.6
        push      ebx                                           ;50.6
        call      _for_write_seq_fmt                            ;50.6
                                ; LOE ebx esi
.B1.63:                         ; Preds .B1.19
        add       esp, 100                                      ;50.6
                                ; LOE ebx esi
.B1.20:                         ; Preds .B1.63
        mov       DWORD PTR [76+esp], 1                         ;50.6
        cmp       DWORD PTR [esp], 0                            ;50.6
        jle       .B1.26        ; Prob 2%                       ;50.6
                                ; LOE ebx esi
.B1.21:                         ; Preds .B1.20
        mov       ebx, DWORD PTR [esp]                          ;50.6
        lea       esi, DWORD PTR [76+esp]                       ;50.6
        mov       edi, DWORD PTR [8+ebp]                        ;50.6
                                ; LOE ebx esi edi
.B1.22:                         ; Preds .B1.21 .B1.24
        push      OFFSET FLAT: __NLITPACK_0.0.1                 ;50.6
        push      esi                                           ;50.6
        push      edi                                           ;50.6
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_VALUE   ;50.6
                                ; LOE ebx esi edi f1
.B1.64:                         ; Preds .B1.22
        fstp      DWORD PTR [156+esp]                           ;50.6
        movss     xmm3, DWORD PTR [156+esp]                     ;50.6
                                ; LOE ebx esi edi xmm3
.B1.23:                         ; Preds .B1.64
        movss     xmm0, DWORD PTR [_2il0floatpacket.6]          ;50.6
        andps     xmm0, xmm3                                    ;50.6
        pxor      xmm3, xmm0                                    ;50.6
        movss     xmm1, DWORD PTR [_2il0floatpacket.7]          ;50.6
        movaps    xmm2, xmm3                                    ;50.6
        movaps    xmm7, xmm3                                    ;50.6
        cmpltss   xmm2, xmm1                                    ;50.6
        andps     xmm1, xmm2                                    ;50.6
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE+32] ;50.25
        addss     xmm7, xmm1                                    ;50.6
        neg       edx                                           ;50.6
        subss     xmm7, xmm1                                    ;50.6
        movaps    xmm6, xmm7                                    ;50.6
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE] ;50.6
        subss     xmm6, xmm3                                    ;50.6
        movss     xmm3, DWORD PTR [_2il0floatpacket.8]          ;50.6
        movaps    xmm4, xmm6                                    ;50.6
        movaps    xmm5, xmm3                                    ;50.6
        cmpnless  xmm4, xmm3                                    ;50.6
        addss     xmm5, xmm3                                    ;50.6
        cmpless   xmm6, DWORD PTR [_2il0floatpacket.9]          ;50.6
        andps     xmm4, xmm5                                    ;50.6
        andps     xmm6, xmm5                                    ;50.6
        subss     xmm7, xmm4                                    ;50.6
        addss     xmm7, xmm6                                    ;50.6
        orps      xmm7, xmm0                                    ;50.6
        cvtss2si  eax, xmm7                                     ;50.25
        add       edx, eax                                      ;50.6
        imul      eax, edx, 44                                  ;50.6
        mov       edx, DWORD PTR [36+ecx+eax]                   ;50.6
        lea       ecx, DWORD PTR [188+esp]                      ;50.6
        mov       DWORD PTR [188+esp], edx                      ;50.6
        push      ecx                                           ;50.6
        push      OFFSET FLAT: __STRLITPACK_27.0.1              ;50.6
        lea       eax, DWORD PTR [36+esp]                       ;50.6
        push      eax                                           ;50.6
        call      _for_write_seq_fmt_xmit                       ;50.6
                                ; LOE ebx esi edi
.B1.65:                         ; Preds .B1.23
        add       esp, 24                                       ;50.6
                                ; LOE ebx esi edi
.B1.24:                         ; Preds .B1.65
        mov       eax, DWORD PTR [76+esp]                       ;50.6
        inc       eax                                           ;50.6
        mov       DWORD PTR [76+esp], eax                       ;50.6
        cmp       eax, ebx                                      ;50.6
        jle       .B1.22        ; Prob 82%                      ;50.6
                                ; LOE ebx esi edi
.B1.25:                         ; Preds .B1.24
        mov       eax, DWORD PTR [16+ebp]                       ;52.9
        lea       ebx, DWORD PTR [16+esp]                       ;
        mov       esi, DWORD PTR [eax]                          ;52.9
                                ; LOE ebx esi
.B1.26:                         ; Preds .B1.25 .B1.20
        push      0                                             ;50.6
        push      OFFSET FLAT: __STRLITPACK_28.0.1              ;50.6
        push      ebx                                           ;50.6
        call      _for_write_seq_fmt_xmit                       ;50.6
                                ; LOE ebx esi
.B1.66:                         ; Preds .B1.26
        add       esp, 12                                       ;50.6
                                ; LOE ebx esi
.B1.27:                         ; Preds .B1.66
        mov       DWORD PTR [16+esp], 0                         ;52.9
        cmp       DWORD PTR [60+esp], 0                         ;51.20
        jle       .B1.52        ; Prob 16%                      ;51.20
                                ; LOE ebx esi
.B1.28:                         ; Preds .B1.27
        push      32                                            ;52.9
        push      OFFSET FLAT: WRITE_PRINT_TRAJECTORY$format_pack.0.1 ;52.9
        push      0                                             ;52.9
        push      OFFSET FLAT: __STRLITPACK_29.0.1              ;52.9
        push      -2088435968                                   ;52.9
        push      esi                                           ;52.9
        push      ebx                                           ;52.9
        call      _for_write_seq_fmt                            ;52.9
                                ; LOE ebx
.B1.67:                         ; Preds .B1.28
        add       esp, 28                                       ;52.9
                                ; LOE ebx
.B1.29:                         ; Preds .B1.67
        mov       DWORD PTR [68+esp], 1                         ;52.9
        lea       edi, DWORD PTR [160+esp]                      ;52.49
        mov       esi, DWORD PTR [8+ebp]                        ;52.49
                                ; LOE ebx esi edi
.B1.30:                         ; Preds .B1.32 .B1.29
        push      OFFSET FLAT: __NLITPACK_1.0.1                 ;52.29
        lea       eax, DWORD PTR [72+esp]                       ;52.29
        push      eax                                           ;52.29
        push      esi                                           ;52.29
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_VALUE   ;52.29
                                ; LOE ebx esi edi f1
.B1.68:                         ; Preds .B1.30
        fstp      DWORD PTR [156+esp]                           ;52.29
        movss     xmm0, DWORD PTR [156+esp]                     ;52.29
                                ; LOE ebx esi edi xmm0
.B1.31:                         ; Preds .B1.68
        divss     xmm0, DWORD PTR [_2il0floatpacket.5]          ;52.9
        movss     DWORD PTR [172+esp], xmm0                     ;52.9
        push      edi                                           ;52.9
        push      OFFSET FLAT: __STRLITPACK_30.0.1              ;52.9
        push      ebx                                           ;52.9
        call      _for_write_seq_fmt_xmit                       ;52.9
                                ; LOE ebx esi edi
.B1.69:                         ; Preds .B1.31
        add       esp, 24                                       ;52.9
                                ; LOE ebx esi edi
.B1.32:                         ; Preds .B1.69
        mov       eax, DWORD PTR [68+esp]                       ;52.9
        inc       eax                                           ;52.9
        mov       DWORD PTR [68+esp], eax                       ;52.9
        cmp       eax, DWORD PTR [60+esp]                       ;52.9
        jle       .B1.30        ; Prob 82%                      ;52.9
                                ; LOE ebx esi edi
.B1.33:                         ; Preds .B1.32
        push      0                                             ;52.9
        push      OFFSET FLAT: __STRLITPACK_31.0.1              ;52.9
        push      ebx                                           ;52.9
        call      _for_write_seq_fmt_xmit                       ;52.9
                                ; LOE ebx
.B1.34:                         ; Preds .B1.33
        push      OFFSET FLAT: __NLITPACK_1.0.1                 ;57.22
        push      OFFSET FLAT: __NLITPACK_0.0.1                 ;57.22
        push      DWORD PTR [8+ebp]                             ;57.22
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_VALUE   ;57.22
                                ; LOE ebx f1
.B1.70:                         ; Preds .B1.34
        fstp      DWORD PTR [168+esp]                           ;57.22
        movss     xmm0, DWORD PTR [168+esp]                     ;57.22
                                ; LOE ebx xmm0
.B1.35:                         ; Preds .B1.70
        divss     xmm0, DWORD PTR [_2il0floatpacket.5]          ;57.9
        mov       DWORD PTR [40+esp], 0                         ;58.9
        lea       eax, DWORD PTR [24+esp]                       ;58.9
        movss     DWORD PTR [24+esp], xmm0                      ;58.9
        push      32                                            ;58.9
        push      OFFSET FLAT: WRITE_PRINT_TRAJECTORY$format_pack.0.1 ;58.9
        push      eax                                           ;58.9
        mov       esi, DWORD PTR [16+ebp]                       ;58.9
        push      OFFSET FLAT: __STRLITPACK_33.0.1              ;58.9
        push      -2088435968                                   ;58.9
        mov       esi, DWORD PTR [esi]                          ;58.9
        push      esi                                           ;58.9
        push      ebx                                           ;58.9
        call      _for_write_seq_fmt                            ;58.9
                                ; LOE ebx esi
.B1.36:                         ; Preds .B1.35
        push      0                                             ;58.9
        push      OFFSET FLAT: __STRLITPACK_34.0.1              ;58.9
        push      ebx                                           ;58.9
        call      _for_write_seq_fmt_xmit                       ;58.9
                                ; LOE ebx esi
.B1.71:                         ; Preds .B1.36
        add       esp, 64                                       ;58.9
                                ; LOE ebx esi
.B1.37:                         ; Preds .B1.71
        mov       DWORD PTR [68+esp], 2                         ;58.9
        cmp       DWORD PTR [60+esp], 2                         ;58.9
        jl        .B1.44        ; Prob 10%                      ;58.9
                                ; LOE ebx esi
.B1.38:                         ; Preds .B1.37
        mov       esi, DWORD PTR [8+ebp]                        ;58.69
        lea       edi, DWORD PTR [68+esp]                       ;58.69
                                ; LOE ebx esi edi
.B1.39:                         ; Preds .B1.38 .B1.42
        push      OFFSET FLAT: __NLITPACK_1.0.1                 ;58.39
        push      edi                                           ;58.39
        push      esi                                           ;58.39
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_VALUE   ;58.39
                                ; LOE ebx esi edi f1
.B1.72:                         ; Preds .B1.39
        fstp      DWORD PTR [64+esp]                            ;58.39
                                ; LOE ebx esi edi
.B1.40:                         ; Preds .B1.72
        mov       eax, DWORD PTR [80+esp]                       ;58.69
        dec       eax                                           ;58.69
        mov       DWORD PTR [24+esp], eax                       ;58.69
        push      OFFSET FLAT: __NLITPACK_1.0.1                 ;58.69
        lea       edx, DWORD PTR [28+esp]                       ;58.69
        push      edx                                           ;58.69
        push      esi                                           ;58.69
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_VALUE   ;58.69
                                ; LOE ebx esi edi f1
.B1.73:                         ; Preds .B1.40
        fstp      DWORD PTR [168+esp]                           ;58.69
        movss     xmm2, DWORD PTR [168+esp]                     ;58.69
                                ; LOE ebx esi edi xmm2
.B1.41:                         ; Preds .B1.73
        movss     xmm0, DWORD PTR [_2il0floatpacket.5]          ;58.59
        lea       eax, DWORD PTR [176+esp]                      ;58.9
        movss     xmm1, DWORD PTR [76+esp]                      ;58.59
        divss     xmm1, xmm0                                    ;58.59
        divss     xmm2, xmm0                                    ;58.91
        subss     xmm1, xmm2                                    ;58.9
        movss     DWORD PTR [176+esp], xmm1                     ;58.9
        push      eax                                           ;58.9
        push      OFFSET FLAT: __STRLITPACK_35.0.1              ;58.9
        push      ebx                                           ;58.9
        call      _for_write_seq_fmt_xmit                       ;58.9
                                ; LOE ebx esi edi
.B1.74:                         ; Preds .B1.41
        add       esp, 36                                       ;58.9
                                ; LOE ebx esi edi
.B1.42:                         ; Preds .B1.74
        mov       eax, DWORD PTR [68+esp]                       ;58.9
        inc       eax                                           ;58.9
        mov       DWORD PTR [68+esp], eax                       ;58.9
        cmp       eax, DWORD PTR [60+esp]                       ;58.9
        jle       .B1.39        ; Prob 82%                      ;58.9
                                ; LOE ebx esi edi
.B1.43:                         ; Preds .B1.42
        mov       eax, DWORD PTR [16+ebp]                       ;63.9
        mov       esi, DWORD PTR [eax]                          ;63.9
                                ; LOE ebx esi
.B1.44:                         ; Preds .B1.43 .B1.37
        push      0                                             ;58.9
        push      OFFSET FLAT: __STRLITPACK_36.0.1              ;58.9
        push      ebx                                           ;58.9
        call      _for_write_seq_fmt_xmit                       ;58.9
                                ; LOE ebx esi
.B1.45:                         ; Preds .B1.44
        xor       eax, eax                                      ;63.9
        mov       DWORD PTR [28+esp], eax                       ;63.9
        push      32                                            ;63.9
        push      OFFSET FLAT: WRITE_PRINT_TRAJECTORY$format_pack.0.1 ;63.9
        push      eax                                           ;63.9
        push      OFFSET FLAT: __STRLITPACK_38.0.1              ;63.9
        push      -2088435968                                   ;63.9
        push      esi                                           ;63.9
        push      ebx                                           ;63.9
        call      _for_write_seq_fmt                            ;63.9
                                ; LOE ebx
.B1.75:                         ; Preds .B1.45
        add       esp, 40                                       ;63.9
                                ; LOE ebx
.B1.46:                         ; Preds .B1.75
        mov       DWORD PTR [68+esp], 1                         ;63.9
        mov       esi, DWORD PTR [60+esp]                       ;63.9
        mov       edi, DWORD PTR [8+ebp]                        ;63.9
                                ; LOE ebx esi edi
.B1.47:                         ; Preds .B1.49 .B1.46
        push      OFFSET FLAT: __NLITPACK_2.0.1                 ;63.29
        lea       eax, DWORD PTR [72+esp]                       ;63.29
        push      eax                                           ;63.29
        push      edi                                           ;63.29
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_VALUE   ;63.29
                                ; LOE ebx esi edi f1
.B1.76:                         ; Preds .B1.47
        fstp      DWORD PTR [156+esp]                           ;63.29
        movss     xmm0, DWORD PTR [156+esp]                     ;63.29
                                ; LOE ebx esi edi xmm0
.B1.48:                         ; Preds .B1.76
        divss     xmm0, DWORD PTR [_2il0floatpacket.5]          ;63.9
        movss     DWORD PTR [180+esp], xmm0                     ;63.9
        lea       eax, DWORD PTR [180+esp]                      ;63.9
        push      eax                                           ;63.9
        push      OFFSET FLAT: __STRLITPACK_39.0.1              ;63.9
        push      ebx                                           ;63.9
        call      _for_write_seq_fmt_xmit                       ;63.9
                                ; LOE ebx esi edi
.B1.77:                         ; Preds .B1.48
        add       esp, 24                                       ;63.9
                                ; LOE ebx esi edi
.B1.49:                         ; Preds .B1.77
        mov       eax, DWORD PTR [68+esp]                       ;63.9
        inc       eax                                           ;63.9
        mov       DWORD PTR [68+esp], eax                       ;63.9
        cmp       eax, esi                                      ;63.9
        jle       .B1.47        ; Prob 82%                      ;63.9
                                ; LOE ebx esi edi
.B1.50:                         ; Preds .B1.49
        push      0                                             ;63.9
        push      OFFSET FLAT: __STRLITPACK_40.0.1              ;63.9
        push      ebx                                           ;63.9
        call      _for_write_seq_fmt_xmit                       ;63.9
                                ; LOE
.B1.78:                         ; Preds .B1.50
        add       esp, 12                                       ;63.9
                                ; LOE
.B1.51:                         ; Preds .B1.78 .B1.58
        add       esp, 180                                      ;98.1
        pop       ebx                                           ;98.1
        pop       edi                                           ;98.1
        pop       esi                                           ;98.1
        mov       esp, ebp                                      ;98.1
        pop       ebp                                           ;98.1
        ret                                                     ;98.1
                                ; LOE
.B1.52:                         ; Preds .B1.27                  ; Infreq
        push      32                                            ;54.9
        push      OFFSET FLAT: WRITE_PRINT_TRAJECTORY$format_pack.0.1 ;54.9
        push      0                                             ;54.9
        push      OFFSET FLAT: __STRLITPACK_32.0.1              ;54.9
        push      -2088435968                                   ;54.9
        push      esi                                           ;54.9
        push      ebx                                           ;54.9
        call      _for_write_seq_fmt                            ;54.9
                                ; LOE ebx esi
.B1.53:                         ; Preds .B1.52                  ; Infreq
        xor       eax, eax                                      ;60.9
        mov       DWORD PTR [44+esp], eax                       ;60.9
        push      32                                            ;60.9
        push      OFFSET FLAT: WRITE_PRINT_TRAJECTORY$format_pack.0.1 ;60.9
        push      eax                                           ;60.9
        push      OFFSET FLAT: __STRLITPACK_37.0.1              ;60.9
        push      -2088435968                                   ;60.9
        push      esi                                           ;60.9
        push      ebx                                           ;60.9
        call      _for_write_seq_fmt                            ;60.9
                                ; LOE ebx esi
.B1.54:                         ; Preds .B1.53                  ; Infreq
        xor       eax, eax                                      ;65.9
        mov       DWORD PTR [72+esp], eax                       ;65.9
        push      32                                            ;65.9
        push      OFFSET FLAT: WRITE_PRINT_TRAJECTORY$format_pack.0.1 ;65.9
        push      eax                                           ;65.9
        push      OFFSET FLAT: __STRLITPACK_41.0.1              ;65.9
        push      -2088435968                                   ;65.9
        push      esi                                           ;65.9
        push      ebx                                           ;65.9
        call      _for_write_seq_fmt                            ;65.9
                                ; LOE
.B1.79:                         ; Preds .B1.54                  ; Infreq
        add       esp, 264                                      ;65.9
        pop       ebx                                           ;65.9
        pop       edi                                           ;65.9
        pop       esi                                           ;65.9
        mov       esp, ebp                                      ;65.9
        pop       ebp                                           ;65.9
        ret                                                     ;65.9
                                ; LOE
.B1.55:                         ; Preds .B1.2                   ; Infreq
        mov       ecx, DWORD PTR [8+ebp]                        ;39.6
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;39.11
        neg       edx                                           ;39.6
        add       edx, DWORD PTR [ecx]                          ;39.6
        shl       edx, 8                                        ;39.6
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;39.6
        movss     xmm0, DWORD PTR [88+eax+edx]                  ;39.6
        movss     DWORD PTR [8+esp], xmm0                       ;39.6
        push      ecx                                           ;40.19
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_P_SIZE  ;40.19
                                ; LOE eax
.B1.80:                         ; Preds .B1.55                  ; Infreq
        mov       DWORD PTR [4+esp], eax                        ;40.19
                                ; LOE
.B1.56:                         ; Preds .B1.80                  ; Infreq
        dec       DWORD PTR [4+esp]                             ;40.6
        push      DWORD PTR [8+ebp]                             ;41.16
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_P_SIZE  ;41.16
                                ; LOE eax
.B1.81:                         ; Preds .B1.56                  ; Infreq
        add       esp, 8                                        ;41.16
        mov       DWORD PTR [60+esp], eax                       ;41.16
                                ; LOE
.B1.57:                         ; Preds .B1.81                  ; Infreq
        mov       edi, DWORD PTR [8+ebp]                        ;49.6
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;49.47
        mov       esi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;49.6
        dec       DWORD PTR [60+esp]                            ;41.3
        mov       edi, DWORD PTR [edi]                          ;49.6
        mov       DWORD PTR [4+esp], eax                        ;49.47
        jmp       .B1.4         ; Prob 100%                     ;49.47
                                ; LOE esi edi
.B1.58:                         ; Preds .B1.1                   ; Infreq
        test      BYTE PTR [_DYNUST_MAIN_MODULE_mp_REACH_CONVERG], 1 ;33.18
        jne       .B1.2         ; Prob 40%                      ;33.18
        jmp       .B1.51        ; Prob 100%                     ;33.18
        ALIGN     16
                                ; LOE
; mark_end;
_WRITE_PRINT_TRAJECTORY ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
WRITE_PRINT_TRAJECTORY$format_pack.0.1	DB	54
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	2
	DB	50
	DB	0
	DB	0
	DB	0
	DB	8
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	50
	DB	0
	DB	0
	DB	0
	DB	8
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	5
	DB	0
	DB	86
	DB	101
	DB	104
	DB	32
	DB	35
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	9
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	5
	DB	0
	DB	32
	DB	84
	DB	97
	DB	103
	DB	61
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	2
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	7
	DB	0
	DB	32
	DB	79
	DB	114
	DB	105
	DB	103
	DB	90
	DB	61
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	5
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	7
	DB	0
	DB	32
	DB	68
	DB	101
	DB	115
	DB	116
	DB	90
	DB	61
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	5
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	7
	DB	0
	DB	32
	DB	67
	DB	108
	DB	97
	DB	115
	DB	115
	DB	61
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	2
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	7
	DB	0
	DB	32
	DB	85
	DB	115
	DB	116
	DB	109
	DB	78
	DB	61
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	7
	DB	0
	DB	32
	DB	68
	DB	111
	DB	119
	DB	110
	DB	78
	DB	61
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	7
	DB	0
	DB	32
	DB	68
	DB	101
	DB	115
	DB	116
	DB	78
	DB	61
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	7
	DB	0
	DB	32
	DB	83
	DB	84
	DB	105
	DB	109
	DB	101
	DB	61
	DB	0
	DB	33
	DB	0
	DB	0
	DB	2
	DB	1
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	19
	DB	0
	DB	32
	DB	84
	DB	111
	DB	116
	DB	97
	DB	108
	DB	32
	DB	84
	DB	114
	DB	97
	DB	118
	DB	101
	DB	108
	DB	32
	DB	84
	DB	105
	DB	109
	DB	101
	DB	61
	DB	0
	DB	33
	DB	0
	DB	0
	DB	2
	DB	1
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	12
	DB	0
	DB	32
	DB	35
	DB	32
	DB	111
	DB	102
	DB	32
	DB	78
	DB	111
	DB	100
	DB	101
	DB	115
	DB	61
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	4
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	8
	DB	0
	DB	32
	DB	86
	DB	101
	DB	104
	DB	84
	DB	121
	DB	112
	DB	101
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	2
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	4
	DB	0
	DB	32
	DB	76
	DB	79
	DB	79
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	2
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
_DATA	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
__STRLITPACK_13.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_14.0.1	DB	5
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_15.0.1	DB	7
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_16.0.1	DB	7
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_17.0.1	DB	5
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_18.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_19.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__NLITPACK_0.0.1	DD	1
__STRLITPACK_20.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_21.0.1	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_22.0.1	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_23.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_24.0.1	DB	5
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_25.0.1	DB	5
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_26.0.1	DB	2
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_27.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_28.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_32.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_29.0.1	DB	2
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__NLITPACK_1.0.1	DD	3
__STRLITPACK_30.0.1	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_31.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_37.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_33.0.1	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_34.0.1	DB	2
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_35.0.1	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_36.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_41.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_38.0.1	DB	2
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__NLITPACK_2.0.1	DD	2
__STRLITPACK_39.0.1	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_40.0.1	DB	1
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _WRITE_PRINT_TRAJECTORY
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _PRINTICM
; mark_begin;
       ALIGN     16
	PUBLIC _PRINTICM
_PRINTICM	PROC NEAR 
; parameter 1: 8 + ebp
; parameter 2: 12 + ebp
.B2.1:                          ; Preds .B2.0
        push      ebp                                           ;101.12
        mov       ebp, esp                                      ;101.12
        and       esp, -16                                      ;101.12
        push      esi                                           ;101.12
        push      edi                                           ;101.12
        push      ebx                                           ;101.12
        sub       esp, 132                                      ;101.12
        mov       DWORD PTR [80+esp], 0                         ;111.1
        lea       ebx, DWORD PTR [80+esp]                       ;111.1
        mov       DWORD PTR [112+esp], 8                        ;111.1
        lea       eax, DWORD PTR [112+esp]                      ;111.1
        mov       DWORD PTR [116+esp], OFFSET FLAT: __STRLITPACK_46 ;111.1
        push      32                                            ;111.1
        push      eax                                           ;111.1
        mov       edx, DWORD PTR [8+ebp]                        ;101.12
        push      OFFSET FLAT: __STRLITPACK_48.0.2              ;111.1
        push      -2088435968                                   ;111.1
        push      DWORD PTR [edx]                               ;111.1
        push      ebx                                           ;111.1
        call      _for_write_seq_lis                            ;111.1
                                ; LOE ebx
.B2.2:                          ; Preds .B2.1
        mov       eax, DWORD PTR [12+ebp]                       ;101.12
        lea       edx, DWORD PTR [144+esp]                      ;111.1
        cvtsi2ss  xmm0, DWORD PTR [eax]                         ;111.28
        mulss     xmm0, DWORD PTR [_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT] ;111.1
        movss     DWORD PTR [144+esp], xmm0                     ;111.1
        push      edx                                           ;111.1
        push      OFFSET FLAT: __STRLITPACK_49.0.2              ;111.1
        push      ebx                                           ;111.1
        call      _for_write_seq_lis_xmit                       ;111.1
                                ; LOE ebx
.B2.27:                         ; Preds .B2.2
        add       esp, 36                                       ;111.1
                                ; LOE ebx
.B2.3:                          ; Preds .B2.27
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_JUSTVEH] ;112.5
        mov       edx, 1                                        ;112.5
        test      esi, esi                                      ;112.5
        jle       .B2.11        ; Prob 2%                       ;112.5
                                ; LOE edx ebx esi
.B2.4:                          ; Preds .B2.3
        mov       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;115.9
        mov       edi, 1                                        ;
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;115.12
        mov       DWORD PTR [28+esp], eax                       ;
        mov       DWORD PTR [36+esp], ecx                       ;
        mov       DWORD PTR [52+esp], esi                       ;
        mov       ebx, DWORD PTR [8+ebp]                        ;
                                ; LOE edx ebx edi
.B2.5:                          ; Preds .B2.9 .B2.4
        mov       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;113.13
        neg       ecx                                           ;113.8
        add       ecx, edi                                      ;113.8
        shl       ecx, 8                                        ;113.8
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;113.5
        test      BYTE PTR [96+eax+ecx], 1                      ;113.13
        jne       .B2.9         ; Prob 50%                      ;113.13
                                ; LOE edx ebx edi
.B2.6:                          ; Preds .B2.5
        mov       eax, DWORD PTR [ebx]                          ;114.6
        cmp       eax, 6059                                     ;114.15
        je        .B2.18        ; Prob 16%                      ;114.15
                                ; LOE eax edx ebx edi
.B2.7:                          ; Preds .B2.6
        mov       DWORD PTR [44+esp], edx                       ;112.5
                                ; LOE eax ebx edi
.B2.8:                          ; Preds .B2.24 .B2.7
        cmp       eax, 6060                                     ;117.15
        je        .B2.12        ; Prob 16%                      ;117.15
                                ; LOE eax ebx edi
.B2.9:                          ; Preds .B2.18 .B2.29 .B2.12 .B2.8 .B2.5
                                ;      
        inc       edi                                           ;122.4
        mov       edx, edi                                      ;122.4
        cmp       edi, DWORD PTR [52+esp]                       ;122.4
        jle       .B2.5         ; Prob 82%                      ;122.4
                                ; LOE edx ebx edi
.B2.11:                         ; Preds .B2.9 .B2.3
        add       esp, 132                                      ;124.1
        pop       ebx                                           ;124.1
        pop       edi                                           ;124.1
        pop       esi                                           ;124.1
        mov       esp, ebp                                      ;124.1
        pop       ebp                                           ;124.1
        ret                                                     ;124.1
                                ; LOE
.B2.12:                         ; Preds .B2.8                   ; Infreq
        mov       esi, edi                                      ;118.43
        sub       esi, DWORD PTR [28+esp]                       ;118.43
        shl       esi, 5                                        ;118.43
        mov       edx, DWORD PTR [36+esp]                       ;118.43
        cmp       BYTE PTR [5+edx+esi], 3                       ;118.43
        jne       .B2.9         ; Prob 84%                      ;118.43
                                ; LOE eax ebx esi edi
.B2.13:                         ; Preds .B2.12                  ; Infreq
        mov       DWORD PTR [80+esp], 0                         ;118.49
        lea       edx, DWORD PTR [32+esp]                       ;118.49
        mov       DWORD PTR [32+esp], edi                       ;118.49
        push      32                                            ;118.49
        push      OFFSET FLAT: PRINTICM$format_pack.0.2+20      ;118.49
        push      edx                                           ;118.49
        push      OFFSET FLAT: __STRLITPACK_54.0.2              ;118.49
        push      -2088435968                                   ;118.49
        push      eax                                           ;118.49
        lea       eax, DWORD PTR [104+esp]                      ;118.49
        push      eax                                           ;118.49
        call      _for_write_seq_fmt                            ;118.49
                                ; LOE ebx esi edi
.B2.14:                         ; Preds .B2.13                  ; Infreq
        mov       eax, DWORD PTR [64+esp]                       ;118.49
        lea       ecx, DWORD PTR [76+esp]                       ;118.49
        movzx     edx, WORD PTR [22+eax+esi]                    ;118.49
        mov       WORD PTR [76+esp], dx                         ;118.49
        push      ecx                                           ;118.49
        push      OFFSET FLAT: __STRLITPACK_55.0.2              ;118.49
        lea       esi, DWORD PTR [116+esp]                      ;118.49
        push      esi                                           ;118.49
        call      _for_write_seq_fmt_xmit                       ;118.49
                                ; LOE ebx edi
.B2.15:                         ; Preds .B2.14                  ; Infreq
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE+32] ;118.49
        lea       esi, DWORD PTR [84+esp]                       ;118.49
        neg       eax                                           ;118.49
        lea       ecx, DWORD PTR [52+esp]                       ;118.49
        add       eax, edi                                      ;118.49
        shl       eax, 6                                        ;118.49
        mov       edi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE] ;118.49
        movsx     edx, WORD PTR [12+edi+eax]                    ;118.49
        mov       DWORD PTR [52+esp], edx                       ;118.101
        push      OFFSET FLAT: __NLITPACK_3.0.2                 ;118.49
        push      ecx                                           ;118.49
        push      esi                                           ;118.49
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_VALUE   ;118.49
                                ; LOE ebx f1
.B2.28:                         ; Preds .B2.15                  ; Infreq
        fstp      DWORD PTR [52+esp]                            ;118.49
        movss     xmm3, DWORD PTR [52+esp]                      ;118.49
                                ; LOE ebx xmm3
.B2.16:                         ; Preds .B2.28                  ; Infreq
        movss     xmm0, DWORD PTR [_2il0floatpacket.12]         ;118.49
        lea       edi, DWORD PTR [116+esp]                      ;118.49
        andps     xmm0, xmm3                                    ;118.49
        pxor      xmm3, xmm0                                    ;118.49
        movss     xmm1, DWORD PTR [_2il0floatpacket.13]         ;118.49
        movaps    xmm2, xmm3                                    ;118.49
        movaps    xmm7, xmm3                                    ;118.49
        cmpltss   xmm2, xmm1                                    ;118.49
        andps     xmm1, xmm2                                    ;118.49
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE] ;118.49
        addss     xmm7, xmm1                                    ;118.49
        subss     xmm7, xmm1                                    ;118.49
        movaps    xmm6, xmm7                                    ;118.49
        subss     xmm6, xmm3                                    ;118.49
        movss     xmm3, DWORD PTR [_2il0floatpacket.14]         ;118.49
        movaps    xmm5, xmm3                                    ;118.49
        movaps    xmm4, xmm6                                    ;118.49
        addss     xmm5, xmm3                                    ;118.49
        cmpnless  xmm4, xmm3                                    ;118.49
        cmpless   xmm6, DWORD PTR [_2il0floatpacket.15]         ;118.49
        andps     xmm4, xmm5                                    ;118.49
        andps     xmm6, xmm5                                    ;118.49
        subss     xmm7, xmm4                                    ;118.49
        addss     xmm7, xmm6                                    ;118.49
        orps      xmm7, xmm0                                    ;118.49
        cvtss2si  eax, xmm7                                     ;118.101
        sub       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE+32] ;118.49
        imul      ecx, eax, 44                                  ;118.49
        movzx     esi, WORD PTR [40+edx+ecx]                    ;118.49
        mov       WORD PTR [116+esp], si                        ;118.49
        push      edi                                           ;118.49
        push      OFFSET FLAT: __STRLITPACK_56.0.2              ;118.49
        lea       eax, DWORD PTR [140+esp]                      ;118.49
        push      eax                                           ;118.49
        call      _for_write_seq_fmt_xmit                       ;118.49
                                ; LOE ebx
.B2.17:                         ; Preds .B2.16                  ; Infreq
        mov       edi, DWORD PTR [108+esp]                      ;118.186
        mov       ecx, edi                                      ;118.49
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;118.186
        sub       ecx, eax                                      ;118.49
        shl       ecx, 5                                        ;118.49
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;118.49
        mov       DWORD PTR [92+esp], eax                       ;118.186
        lea       eax, DWORD PTR [136+esp]                      ;118.49
        mov       DWORD PTR [100+esp], edx                      ;118.49
        movzx     esi, WORD PTR [20+edx+ecx]                    ;118.49
        mov       WORD PTR [136+esp], si                        ;118.49
        push      eax                                           ;118.49
        push      OFFSET FLAT: __STRLITPACK_57.0.2              ;118.49
        lea       edx, DWORD PTR [152+esp]                      ;118.49
        push      edx                                           ;118.49
        call      _for_write_seq_fmt_xmit                       ;118.49
                                ; LOE ebx edi
.B2.29:                         ; Preds .B2.17                  ; Infreq
        add       esp, 76                                       ;118.49
        jmp       .B2.9         ; Prob 100%                     ;118.49
                                ; LOE ebx edi
.B2.18:                         ; Preds .B2.6                   ; Infreq
        mov       esi, edi                                      ;115.43
        sub       esi, DWORD PTR [28+esp]                       ;115.43
        shl       esi, 5                                        ;115.43
        mov       ecx, DWORD PTR [36+esp]                       ;115.43
        mov       DWORD PTR [20+esp], esi                       ;115.43
        cmp       BYTE PTR [5+ecx+esi], 1                       ;115.43
        jne       .B2.9         ; Prob 84%                      ;115.43
                                ; LOE eax edx ebx edi
.B2.19:                         ; Preds .B2.18                  ; Infreq
        mov       DWORD PTR [44+esp], edx                       ;112.5
        lea       edx, DWORD PTR [16+esp]                       ;115.49
        mov       DWORD PTR [80+esp], 0                         ;115.49
        mov       DWORD PTR [16+esp], edi                       ;115.49
        push      32                                            ;115.49
        push      OFFSET FLAT: PRINTICM$format_pack.0.2         ;115.49
        push      edx                                           ;115.49
        push      OFFSET FLAT: __STRLITPACK_50.0.2              ;115.49
        push      -2088435968                                   ;115.49
        push      eax                                           ;115.49
        lea       eax, DWORD PTR [104+esp]                      ;115.49
        push      eax                                           ;115.49
        call      _for_write_seq_fmt                            ;115.49
                                ; LOE ebx edi
.B2.20:                         ; Preds .B2.19                  ; Infreq
        mov       eax, DWORD PTR [64+esp]                       ;115.49
        lea       esi, DWORD PTR [52+esp]                       ;115.49
        mov       edx, DWORD PTR [48+esp]                       ;115.49
        movzx     ecx, WORD PTR [22+eax+edx]                    ;115.49
        mov       WORD PTR [52+esp], cx                         ;115.49
        push      esi                                           ;115.49
        push      OFFSET FLAT: __STRLITPACK_51.0.2              ;115.49
        lea       eax, DWORD PTR [116+esp]                      ;115.49
        push      eax                                           ;115.49
        call      _for_write_seq_fmt_xmit                       ;115.49
                                ; LOE ebx edi
.B2.21:                         ; Preds .B2.20                  ; Infreq
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE+32] ;115.49
        lea       esi, DWORD PTR [84+esp]                       ;115.49
        neg       eax                                           ;115.49
        lea       ecx, DWORD PTR [48+esp]                       ;115.49
        add       eax, edi                                      ;115.49
        shl       eax, 6                                        ;115.49
        mov       edi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE] ;115.49
        movsx     edx, WORD PTR [12+edi+eax]                    ;115.49
        mov       DWORD PTR [48+esp], edx                       ;115.101
        push      OFFSET FLAT: __NLITPACK_3.0.2                 ;115.49
        push      ecx                                           ;115.49
        push      esi                                           ;115.49
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_VALUE   ;115.49
                                ; LOE ebx f1
.B2.30:                         ; Preds .B2.21                  ; Infreq
        fstp      DWORD PTR [52+esp]                            ;115.49
        movss     xmm3, DWORD PTR [52+esp]                      ;115.49
                                ; LOE ebx xmm3
.B2.22:                         ; Preds .B2.30                  ; Infreq
        movss     xmm0, DWORD PTR [_2il0floatpacket.12]         ;115.49
        lea       edi, DWORD PTR [92+esp]                       ;115.49
        andps     xmm0, xmm3                                    ;115.49
        pxor      xmm3, xmm0                                    ;115.49
        movss     xmm1, DWORD PTR [_2il0floatpacket.13]         ;115.49
        movaps    xmm2, xmm3                                    ;115.49
        movaps    xmm7, xmm3                                    ;115.49
        cmpltss   xmm2, xmm1                                    ;115.49
        andps     xmm1, xmm2                                    ;115.49
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE] ;115.49
        addss     xmm7, xmm1                                    ;115.49
        subss     xmm7, xmm1                                    ;115.49
        movaps    xmm6, xmm7                                    ;115.49
        subss     xmm6, xmm3                                    ;115.49
        movss     xmm3, DWORD PTR [_2il0floatpacket.14]         ;115.49
        movaps    xmm5, xmm3                                    ;115.49
        movaps    xmm4, xmm6                                    ;115.49
        addss     xmm5, xmm3                                    ;115.49
        cmpnless  xmm4, xmm3                                    ;115.49
        cmpless   xmm6, DWORD PTR [_2il0floatpacket.15]         ;115.49
        andps     xmm4, xmm5                                    ;115.49
        andps     xmm6, xmm5                                    ;115.49
        subss     xmm7, xmm4                                    ;115.49
        addss     xmm7, xmm6                                    ;115.49
        orps      xmm7, xmm0                                    ;115.49
        cvtss2si  eax, xmm7                                     ;115.101
        sub       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE+32] ;115.49
        imul      ecx, eax, 44                                  ;115.49
        movzx     esi, WORD PTR [40+edx+ecx]                    ;115.49
        mov       WORD PTR [92+esp], si                         ;115.49
        push      edi                                           ;115.49
        push      OFFSET FLAT: __STRLITPACK_52.0.2              ;115.49
        lea       eax, DWORD PTR [140+esp]                      ;115.49
        push      eax                                           ;115.49
        call      _for_write_seq_fmt_xmit                       ;115.49
                                ; LOE ebx
.B2.23:                         ; Preds .B2.22                  ; Infreq
        mov       edi, DWORD PTR [108+esp]                      ;115.186
        mov       ecx, edi                                      ;115.49
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;115.186
        sub       ecx, eax                                      ;115.49
        shl       ecx, 5                                        ;115.49
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;115.49
        mov       DWORD PTR [92+esp], eax                       ;115.186
        lea       eax, DWORD PTR [120+esp]                      ;115.49
        mov       DWORD PTR [100+esp], edx                      ;115.49
        movzx     esi, WORD PTR [20+edx+ecx]                    ;115.49
        mov       WORD PTR [120+esp], si                        ;115.49
        push      eax                                           ;115.49
        push      OFFSET FLAT: __STRLITPACK_53.0.2              ;115.49
        lea       edx, DWORD PTR [152+esp]                      ;115.49
        push      edx                                           ;115.49
        call      _for_write_seq_fmt_xmit                       ;115.49
                                ; LOE ebx edi
.B2.31:                         ; Preds .B2.23                  ; Infreq
        add       esp, 76                                       ;115.49
                                ; LOE ebx edi
.B2.24:                         ; Preds .B2.31                  ; Infreq
        mov       eax, DWORD PTR [ebx]                          ;117.6
        jmp       .B2.8         ; Prob 100%                     ;117.6
        ALIGN     16
                                ; LOE eax ebx edi
; mark_end;
_PRINTICM ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
PRINTICM$format_pack.0.2	DB	54
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	4
	DB	0
	DB	0
	DB	0
	DB	8
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	4
	DB	0
	DB	0
	DB	0
	DB	8
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
_DATA	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_48.0.2	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_49.0.2	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_50.0.2	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_51.0.2	DB	7
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__NLITPACK_3.0.2	DD	1
__STRLITPACK_52.0.2	DB	7
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_53.0.2	DB	7
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_54.0.2	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_55.0.2	DB	7
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_56.0.2	DB	7
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_57.0.2	DB	7
	DB	1
	DB	1
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _PRINTICM
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _PRINTALT
; mark_begin;
       ALIGN     16
	PUBLIC _PRINTALT
_PRINTALT	PROC NEAR 
; parameter 1: 8 + ebp
; parameter 2: 12 + ebp
.B3.1:                          ; Preds .B3.0
        push      ebp                                           ;126.12
        mov       ebp, esp                                      ;126.12
        and       esp, -16                                      ;126.12
        push      esi                                           ;126.12
        push      edi                                           ;126.12
        push      ebx                                           ;126.12
        sub       esp, 148                                      ;126.12
        mov       esi, DWORD PTR [8+ebp]                        ;126.12
        push      esi                                           ;137.15
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_P_SIZE  ;137.15
                                ; LOE eax esi
.B3.48:                         ; Preds .B3.1
        add       esp, 4                                        ;137.15
        mov       edi, eax                                      ;137.15
                                ; LOE esi edi
.B3.2:                          ; Preds .B3.48
        mov       DWORD PTR [60+esp], 1                         ;137.4
        test      edi, edi                                      ;137.4
        pxor      xmm1, xmm1                                    ;
        jle       .B3.7         ; Prob 2%                       ;137.4
                                ; LOE esi edi xmm1
.B3.3:                          ; Preds .B3.2
        lea       ebx, DWORD PTR [60+esp]                       ;138.32
                                ; LOE ebx esi edi xmm1
.B3.4:                          ; Preds .B3.3 .B3.5
        push      OFFSET FLAT: __NLITPACK_4.0.3                 ;138.32
        push      ebx                                           ;138.32
        push      esi                                           ;138.32
        movss     DWORD PTR [12+esp], xmm1                      ;138.32
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_VALUE   ;138.32
                                ; LOE ebx esi edi f1
.B3.49:                         ; Preds .B3.4
        fstp      DWORD PTR [20+esp]                            ;138.32
        movss     xmm1, DWORD PTR [12+esp]                      ;
        movss     xmm5, DWORD PTR [20+esp]                      ;138.32
        add       esp, 12                                       ;138.32
                                ; LOE ebx esi edi xmm1 xmm5
.B3.5:                          ; Preds .B3.49
        movss     xmm2, DWORD PTR [_2il0floatpacket.19]         ;138.27
        andps     xmm2, xmm5                                    ;138.27
        pxor      xmm5, xmm2                                    ;138.27
        movss     xmm3, DWORD PTR [_2il0floatpacket.20]         ;138.27
        movaps    xmm4, xmm5                                    ;138.27
        movaps    xmm0, xmm5                                    ;138.27
        cmpltss   xmm4, xmm3                                    ;138.27
        andps     xmm3, xmm4                                    ;138.27
        mov       edx, DWORD PTR [60+esp]                       ;139.4
        addss     xmm0, xmm3                                    ;138.27
        inc       edx                                           ;139.4
        subss     xmm0, xmm3                                    ;138.27
        movaps    xmm3, xmm0                                    ;138.27
        cmp       edx, edi                                      ;139.4
        mov       DWORD PTR [60+esp], edx                       ;139.4
        subss     xmm3, xmm5                                    ;138.27
        movss     xmm5, DWORD PTR [_2il0floatpacket.21]         ;138.27
        movaps    xmm6, xmm3                                    ;138.27
        movaps    xmm7, xmm5                                    ;138.27
        cmpnless  xmm6, xmm5                                    ;138.27
        addss     xmm7, xmm5                                    ;138.27
        cmpless   xmm3, DWORD PTR [_2il0floatpacket.22]         ;138.27
        andps     xmm6, xmm7                                    ;138.27
        andps     xmm3, xmm7                                    ;138.27
        subss     xmm0, xmm6                                    ;138.27
        addss     xmm0, xmm3                                    ;138.27
        orps      xmm0, xmm2                                    ;138.27
        cvtss2si  eax, xmm0                                     ;138.27
        cvtsi2ss  xmm0, eax                                     ;138.27
        addss     xmm1, xmm0                                    ;138.7
        jle       .B3.4         ; Prob 82%                      ;139.4
                                ; LOE ebx esi edi xmm1
.B3.7:                          ; Preds .B3.5 .B3.2
        cvttss2si ebx, xmm1                                     ;140.4
        mov       DWORD PTR [16+esp], 0                         ;143.4
        lea       eax, DWORD PTR [80+esp]                       ;143.4
        mov       DWORD PTR [80+esp], ebx                       ;143.4
        lea       ebx, DWORD PTR [16+esp]                       ;143.4
        push      32                                            ;143.4
        push      OFFSET FLAT: PRINTALT$format_pack.0.3         ;143.4
        push      eax                                           ;143.4
        push      OFFSET FLAT: __STRLITPACK_66.0.3              ;143.4
        push      -2088435968                                   ;143.4
        push      989                                           ;143.4
        push      ebx                                           ;143.4
        call      _for_write_seq_fmt                            ;143.4
                                ; LOE ebx esi
.B3.8:                          ; Preds .B3.7
        mov       edi, DWORD PTR [esi]                          ;143.4
        lea       eax, DWORD PTR [116+esp]                      ;143.4
        mov       DWORD PTR [116+esp], edi                      ;143.4
        push      eax                                           ;143.4
        push      OFFSET FLAT: __STRLITPACK_67.0.3              ;143.4
        push      ebx                                           ;143.4
        call      _for_write_seq_fmt_xmit                       ;143.4
                                ; LOE ebx esi edi
.B3.9:                          ; Preds .B3.8
        sub       edi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;143.4
        lea       ecx, DWORD PTR [136+esp]                      ;143.4
        shl       edi, 5                                        ;143.4
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;143.4
        mov       DWORD PTR [40+esp], eax                       ;143.4
        movzx     edx, BYTE PTR [5+eax+edi]                     ;143.4
        mov       BYTE PTR [136+esp], dl                        ;143.4
        push      ecx                                           ;143.4
        push      OFFSET FLAT: __STRLITPACK_68.0.3              ;143.4
        push      ebx                                           ;143.4
        call      _for_write_seq_fmt_xmit                       ;143.4
                                ; LOE ebx esi edi
.B3.10:                         ; Preds .B3.9
        mov       eax, DWORD PTR [52+esp]                       ;143.4
        lea       ecx, DWORD PTR [156+esp]                      ;143.4
        movzx     edx, WORD PTR [22+eax+edi]                    ;143.4
        mov       WORD PTR [156+esp], dx                        ;143.4
        push      ecx                                           ;143.4
        push      OFFSET FLAT: __STRLITPACK_69.0.3              ;143.4
        push      ebx                                           ;143.4
        call      _for_write_seq_fmt_xmit                       ;143.4
                                ; LOE ebx esi edi
.B3.11:                         ; Preds .B3.10
        mov       eax, DWORD PTR [64+esp]                       ;143.4
        lea       ecx, DWORD PTR [176+esp]                      ;143.4
        movzx     edx, WORD PTR [20+eax+edi]                    ;143.4
        mov       WORD PTR [176+esp], dx                        ;143.4
        push      ecx                                           ;143.4
        push      OFFSET FLAT: __STRLITPACK_70.0.3              ;143.4
        push      ebx                                           ;143.4
        call      _for_write_seq_fmt_xmit                       ;143.4
                                ; LOE ebx esi
.B3.12:                         ; Preds .B3.11
        push      esi                                           ;143.137
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_P_SIZE  ;143.137
                                ; LOE eax ebx esi
.B3.13:                         ; Preds .B3.12
        inc       eax                                           ;143.4
        mov       DWORD PTR [200+esp], eax                      ;143.4
        lea       eax, DWORD PTR [200+esp]                      ;143.4
        push      eax                                           ;143.4
        push      OFFSET FLAT: __STRLITPACK_71.0.3              ;143.4
        push      ebx                                           ;143.4
        call      _for_write_seq_fmt_xmit                       ;143.4
                                ; LOE ebx esi
.B3.14:                         ; Preds .B3.13
        mov       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;143.4
        neg       ecx                                           ;143.4
        add       ecx, DWORD PTR [esi]                          ;143.4
        shl       ecx, 5                                        ;143.4
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;143.4
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;143.4
        neg       edi                                           ;143.4
        add       edi, DWORD PTR [28+edx+ecx]                   ;143.4
        imul      ecx, edi, 152                                 ;143.4
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;143.4
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE+32] ;143.157
        neg       eax                                           ;143.4
        add       eax, DWORD PTR [28+edx+ecx]                   ;143.4
        lea       edx, DWORD PTR [220+esp]                      ;143.4
        imul      edi, eax, 44                                  ;143.4
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE] ;143.4
        mov       eax, DWORD PTR [36+eax+edi]                   ;143.4
        mov       DWORD PTR [220+esp], eax                      ;143.4
        push      edx                                           ;143.4
        push      OFFSET FLAT: __STRLITPACK_72.0.3              ;143.4
        push      ebx                                           ;143.4
        call      _for_write_seq_fmt_xmit                       ;143.4
                                ; LOE ebx esi
.B3.15:                         ; Preds .B3.14
        push      0                                             ;143.4
        push      OFFSET FLAT: __STRLITPACK_73.0.3              ;143.4
        push      ebx                                           ;143.4
        call      _for_write_seq_fmt_xmit                       ;143.4
                                ; LOE ebx esi
.B3.16:                         ; Preds .B3.15
        push      esi                                           ;143.335
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_P_SIZE  ;143.335
                                ; LOE eax ebx esi
.B3.51:                         ; Preds .B3.16
        add       esp, 120                                      ;143.335
        mov       edi, eax                                      ;143.335
                                ; LOE ebx esi edi
.B3.17:                         ; Preds .B3.51
        mov       DWORD PTR [68+esp], 1                         ;143.4
        test      edi, edi                                      ;143.4
        jle       .B3.23        ; Prob 2%                       ;143.4
                                ; LOE ebx esi edi
.B3.19:                         ; Preds .B3.17 .B3.21
        push      OFFSET FLAT: __NLITPACK_4.0.3                 ;143.4
        lea       eax, DWORD PTR [72+esp]                       ;143.4
        push      eax                                           ;143.4
        push      esi                                           ;143.4
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_VALUE   ;143.4
                                ; LOE ebx esi edi f1
.B3.52:                         ; Preds .B3.19
        fstp      DWORD PTR [20+esp]                            ;143.4
        movss     xmm3, DWORD PTR [20+esp]                      ;143.4
                                ; LOE ebx esi edi xmm3
.B3.20:                         ; Preds .B3.52
        movss     xmm0, DWORD PTR [_2il0floatpacket.19]         ;143.4
        andps     xmm0, xmm3                                    ;143.4
        pxor      xmm3, xmm0                                    ;143.4
        movss     xmm1, DWORD PTR [_2il0floatpacket.20]         ;143.4
        movaps    xmm2, xmm3                                    ;143.4
        movaps    xmm7, xmm3                                    ;143.4
        cmpltss   xmm2, xmm1                                    ;143.4
        andps     xmm1, xmm2                                    ;143.4
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE+32] ;143.261
        addss     xmm7, xmm1                                    ;143.4
        neg       edx                                           ;143.4
        subss     xmm7, xmm1                                    ;143.4
        movaps    xmm6, xmm7                                    ;143.4
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE] ;143.4
        subss     xmm6, xmm3                                    ;143.4
        movss     xmm3, DWORD PTR [_2il0floatpacket.21]         ;143.4
        movaps    xmm4, xmm6                                    ;143.4
        movaps    xmm5, xmm3                                    ;143.4
        cmpnless  xmm4, xmm3                                    ;143.4
        addss     xmm5, xmm3                                    ;143.4
        cmpless   xmm6, DWORD PTR [_2il0floatpacket.22]         ;143.4
        andps     xmm4, xmm5                                    ;143.4
        andps     xmm6, xmm5                                    ;143.4
        subss     xmm7, xmm4                                    ;143.4
        addss     xmm7, xmm6                                    ;143.4
        orps      xmm7, xmm0                                    ;143.4
        cvtss2si  eax, xmm7                                     ;143.261
        add       edx, eax                                      ;143.4
        imul      eax, edx, 44                                  ;143.4
        mov       edx, DWORD PTR [36+ecx+eax]                   ;143.4
        lea       ecx, DWORD PTR [156+esp]                      ;143.4
        mov       DWORD PTR [156+esp], edx                      ;143.4
        push      ecx                                           ;143.4
        push      OFFSET FLAT: __STRLITPACK_74.0.3              ;143.4
        push      ebx                                           ;143.4
        call      _for_write_seq_fmt_xmit                       ;143.4
                                ; LOE ebx esi edi
.B3.53:                         ; Preds .B3.20
        add       esp, 24                                       ;143.4
                                ; LOE ebx esi edi
.B3.21:                         ; Preds .B3.53
        mov       eax, DWORD PTR [68+esp]                       ;143.4
        inc       eax                                           ;143.4
        mov       DWORD PTR [68+esp], eax                       ;143.4
        cmp       eax, edi                                      ;143.4
        jle       .B3.19        ; Prob 82%                      ;143.4
                                ; LOE ebx esi edi
.B3.23:                         ; Preds .B3.21 .B3.17
        push      0                                             ;143.4
        push      OFFSET FLAT: __STRLITPACK_75.0.3              ;143.4
        push      ebx                                           ;143.4
        call      _for_write_seq_fmt_xmit                       ;143.4
                                ; LOE ebx esi
.B3.54:                         ; Preds .B3.23
        add       esp, 12                                       ;143.4
                                ; LOE ebx esi
.B3.24:                         ; Preds .B3.54
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAJALT], 0 ;145.15
        jle       .B3.43        ; Prob 16%                      ;145.15
                                ; LOE ebx esi
.B3.25:                         ; Preds .B3.24
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;146.30
        lea       edi, DWORD PTR [48+esp]                       ;146.6
        neg       edx                                           ;146.6
        add       edx, DWORD PTR [esi]                          ;146.6
        shl       edx, 5                                        ;146.6
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;146.6
        mov       DWORD PTR [16+esp], 0                         ;146.6
        mov       ecx, DWORD PTR [12+eax+edx]                   ;146.6
        mov       DWORD PTR [48+esp], ecx                       ;146.6
        push      32                                            ;146.6
        push      OFFSET FLAT: PRINTALT$format_pack.0.3+44      ;146.6
        push      edi                                           ;146.6
        push      OFFSET FLAT: __STRLITPACK_76.0.3              ;146.6
        push      -2088435968                                   ;146.6
        push      988                                           ;146.6
        push      ebx                                           ;146.6
        call      _for_write_seq_fmt                            ;146.6
                                ; LOE ebx esi
.B3.26:                         ; Preds .B3.25
        push      0                                             ;146.6
        push      OFFSET FLAT: __STRLITPACK_77.0.3              ;146.6
        push      ebx                                           ;146.6
        call      _for_write_seq_fmt_xmit                       ;146.6
                                ; LOE ebx esi
.B3.27:                         ; Preds .B3.26
        push      esi                                           ;146.97
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_P_SIZE  ;146.97
                                ; LOE eax ebx esi
.B3.55:                         ; Preds .B3.27
        add       esp, 44                                       ;146.97
        mov       edi, eax                                      ;146.97
                                ; LOE ebx esi edi
.B3.28:                         ; Preds .B3.55
        dec       edi                                           ;146.6
        mov       DWORD PTR [52+esp], 1                         ;146.6
        test      edi, edi                                      ;146.6
        jle       .B3.34        ; Prob 2%                       ;146.6
                                ; LOE ebx esi edi
.B3.29:                         ; Preds .B3.28
        lea       edx, DWORD PTR [52+esp]                       ;146.81
                                ; LOE ebx esi edi
.B3.30:                         ; Preds .B3.32 .B3.29
        push      OFFSET FLAT: __NLITPACK_5.0.3                 ;146.61
        lea       eax, DWORD PTR [56+esp]                       ;146.61
        push      eax                                           ;146.61
        push      esi                                           ;146.61
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_VALUE   ;146.61
                                ; LOE ebx esi edi f1
.B3.56:                         ; Preds .B3.30
        fstp      DWORD PTR [20+esp]                            ;146.61
        movss     xmm0, DWORD PTR [20+esp]                      ;146.61
                                ; LOE ebx esi edi xmm0
.B3.31:                         ; Preds .B3.56
        divss     xmm0, DWORD PTR [_2il0floatpacket.18]         ;146.6
        movss     DWORD PTR [148+esp], xmm0                     ;146.6
        lea       eax, DWORD PTR [148+esp]                      ;146.6
        push      eax                                           ;146.6
        push      OFFSET FLAT: __STRLITPACK_78.0.3              ;146.6
        push      ebx                                           ;146.6
        call      _for_write_seq_fmt_xmit                       ;146.6
                                ; LOE ebx esi edi
.B3.57:                         ; Preds .B3.31
        add       esp, 24                                       ;146.6
                                ; LOE ebx esi edi
.B3.32:                         ; Preds .B3.57
        mov       eax, DWORD PTR [52+esp]                       ;146.6
        inc       eax                                           ;146.6
        mov       DWORD PTR [52+esp], eax                       ;146.6
        cmp       eax, edi                                      ;146.6
        jle       .B3.30        ; Prob 82%                      ;146.6
                                ; LOE ebx esi edi
.B3.34:                         ; Preds .B3.32 .B3.28
        push      0                                             ;146.6
        push      OFFSET FLAT: __STRLITPACK_79.0.3              ;146.6
        push      ebx                                           ;146.6
        call      _for_write_seq_fmt_xmit                       ;146.6
                                ; LOE ebx esi
.B3.58:                         ; Preds .B3.34
        add       esp, 12                                       ;146.6
                                ; LOE ebx esi
.B3.35:                         ; Preds .B3.58
        mov       eax, DWORD PTR [esi]                          ;148.9
        mov       esi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;148.9
        pxor      xmm0, xmm0                                    ;148.38
        neg       esi                                           ;148.38
        mov       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;149.79
        add       esi, eax                                      ;148.38
        neg       ecx                                           ;149.79
        shl       esi, 5                                        ;148.38
        add       ecx, eax                                      ;149.79
        mov       edi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;148.6
        shl       ecx, 8                                        ;149.79
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;149.49
        movss     xmm1, DWORD PTR [16+edi+esi]                  ;148.9
        movss     xmm3, DWORD PTR [12+edi+esi]                  ;149.50
        movss     xmm2, DWORD PTR [88+edx+ecx]                  ;149.79
        comiss    xmm1, xmm0                                    ;148.38
        jbe       .B3.37        ; Prob 50%                      ;148.38
                                ; LOE eax ebx esi edi xmm1 xmm2 xmm3
.B3.36:                         ; Preds .B3.35
        subss     xmm1, xmm2                                    ;149.78
        pxor      xmm0, xmm0                                    ;149.9
        subss     xmm1, xmm3                                    ;149.49
        maxss     xmm0, xmm1                                    ;149.9
        movss     DWORD PTR [12+esp], xmm0                      ;149.9
        jmp       .B3.38        ; Prob 100%                     ;149.9
                                ; LOE eax ebx esi edi
.B3.37:                         ; Preds .B3.35
        mov       edx, DWORD PTR [12+ebp]                       ;126.12
        pxor      xmm1, xmm1                                    ;151.9
        movss     xmm0, DWORD PTR [edx]                         ;151.9
        subss     xmm0, xmm2                                    ;151.56
        subss     xmm0, xmm3                                    ;151.27
        maxss     xmm1, xmm0                                    ;151.9
        movss     DWORD PTR [12+esp], xmm1                      ;151.9
                                ; LOE eax ebx esi edi
.B3.38:                         ; Preds .B3.36 .B3.37
        pxor      xmm0, xmm0                                    ;153.13
        comiss    xmm0, DWORD PTR [12+esp]                      ;153.13
        ja        .B3.44        ; Prob 5%                       ;153.13
                                ; LOE eax ebx esi edi
.B3.39:                         ; Preds .B3.60 .B3.38
        mov       DWORD PTR [16+esp], 0                         ;157.6
        mov       DWORD PTR [8+esp], eax                        ;157.6
        lea       eax, DWORD PTR [8+esp]                        ;157.6
        push      32                                            ;157.6
        push      OFFSET FLAT: PRINTALT$format_pack.0.3+64      ;157.6
        push      eax                                           ;157.6
        push      OFFSET FLAT: __STRLITPACK_82.0.3              ;157.6
        push      -2088435968                                   ;157.6
        push      990                                           ;157.6
        push      ebx                                           ;157.6
        call      _for_write_seq_fmt                            ;157.6
                                ; LOE ebx esi edi
.B3.40:                         ; Preds .B3.39
        movzx     eax, WORD PTR [22+edi+esi]                    ;157.6
        lea       edx, DWORD PTR [84+esp]                       ;157.6
        mov       WORD PTR [84+esp], ax                         ;157.6
        push      edx                                           ;157.6
        push      OFFSET FLAT: __STRLITPACK_83.0.3              ;157.6
        push      ebx                                           ;157.6
        call      _for_write_seq_fmt_xmit                       ;157.6
                                ; LOE ebx esi edi
.B3.41:                         ; Preds .B3.40
        movzx     eax, WORD PTR [20+edi+esi]                    ;157.6
        lea       edx, DWORD PTR [104+esp]                      ;157.6
        mov       WORD PTR [104+esp], ax                        ;157.6
        push      edx                                           ;157.6
        push      OFFSET FLAT: __STRLITPACK_84.0.3              ;157.6
        push      ebx                                           ;157.6
        call      _for_write_seq_fmt_xmit                       ;157.6
                                ; LOE ebx
.B3.42:                         ; Preds .B3.41
        movss     xmm0, DWORD PTR [64+esp]                      ;157.6
        lea       eax, DWORD PTR [124+esp]                      ;157.6
        movss     DWORD PTR [124+esp], xmm0                     ;157.6
        push      eax                                           ;157.6
        push      OFFSET FLAT: __STRLITPACK_85.0.3              ;157.6
        push      ebx                                           ;157.6
        call      _for_write_seq_fmt_xmit                       ;157.6
                                ; LOE
.B3.59:                         ; Preds .B3.42
        add       esp, 64                                       ;157.6
                                ; LOE
.B3.43:                         ; Preds .B3.59 .B3.24
        add       esp, 148                                      ;160.1
        pop       ebx                                           ;160.1
        pop       edi                                           ;160.1
        pop       esi                                           ;160.1
        mov       esp, ebp                                      ;160.1
        pop       ebp                                           ;160.1
        ret                                                     ;160.1
                                ; LOE
.B3.44:                         ; Preds .B3.38                  ; Infreq
        mov       DWORD PTR [16+esp], 0                         ;154.8
        lea       edx, DWORD PTR [esp]                          ;154.8
        mov       DWORD PTR [esp], 34                           ;154.8
        mov       DWORD PTR [4+esp], OFFSET FLAT: __STRLITPACK_60 ;154.8
        push      32                                            ;154.8
        push      edx                                           ;154.8
        push      OFFSET FLAT: __STRLITPACK_80.0.3              ;154.8
        push      -2088435968                                   ;154.8
        push      911                                           ;154.8
        push      ebx                                           ;154.8
        mov       DWORD PTR [32+esp], eax                       ;154.8
        call      _for_write_seq_lis                            ;154.8
                                ; LOE ebx esi edi
.B3.45:                         ; Preds .B3.44                  ; Infreq
        xor       edx, edx                                      ;155.8
        push      32                                            ;155.8
        push      edx                                           ;155.8
        push      edx                                           ;155.8
        push      -2088435968                                   ;155.8
        push      edx                                           ;155.8
        push      OFFSET FLAT: __STRLITPACK_81                  ;155.8
        call      _for_stop_core                                ;155.8
                                ; LOE ebx esi edi
.B3.60:                         ; Preds .B3.45                  ; Infreq
        mov       eax, DWORD PTR [56+esp]                       ;
        add       esp, 48                                       ;155.8
        jmp       .B3.39        ; Prob 100%                     ;155.8
        ALIGN     16
                                ; LOE eax ebx esi edi
; mark_end;
_PRINTALT ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
	DD 1 DUP (0H)	; pad
PRINTALT$format_pack.0.3	DB	54
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	10
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	8
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	-24
	DB	3
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	2
	DB	-24
	DB	3
	DB	0
	DB	0
	DB	8
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	3
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	2
	DB	1
	DB	0
	DB	0
	DB	0
	DB	8
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
_DATA	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DB 3 DUP ( 0H)	; pad
__NLITPACK_4.0.3	DD	1
__STRLITPACK_66.0.3	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_67.0.3	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_68.0.3	DB	5
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_69.0.3	DB	7
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_70.0.3	DB	7
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_71.0.3	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_72.0.3	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_73.0.3	DB	2
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_74.0.3	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_75.0.3	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_76.0.3	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_77.0.3	DB	2
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__NLITPACK_5.0.3	DD	3
__STRLITPACK_78.0.3	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_79.0.3	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_80.0.3	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_82.0.3	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_83.0.3	DB	7
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_84.0.3	DB	7
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_85.0.3	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _PRINTALT
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DB 3 DUP ( 0H)	; pad
_2il0floatpacket.4	DD	042700000H
_2il0floatpacket.5	DD	042c80000H
_2il0floatpacket.6	DD	080000000H
_2il0floatpacket.7	DD	04b000000H
_2il0floatpacket.8	DD	03f000000H
_2il0floatpacket.9	DD	0bf000000H
__STRLITPACK_46	DB	84
	DB	105
	DB	109
	DB	101
	DB	32
	DB	61
	DB	61
	DB	62
	DB	0
	DB 3 DUP ( 0H)	; pad
_2il0floatpacket.12	DD	080000000H
_2il0floatpacket.13	DD	04b000000H
_2il0floatpacket.14	DD	03f000000H
_2il0floatpacket.15	DD	0bf000000H
__STRLITPACK_60	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	112
	DB	114
	DB	105
	DB	110
	DB	116
	DB	105
	DB	110
	DB	103
	DB	32
	DB	101
	DB	110
	DB	116
	DB	114
	DB	121
	DB	32
	DB	113
	DB	117
	DB	101
	DB	117
	DB	101
	DB	32
	DB	116
	DB	105
	DB	109
	DB	101
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_81	DB	0
	DB 3 DUP ( 0H)	; pad
_2il0floatpacket.18	DD	042c80000H
_2il0floatpacket.19	DD	080000000H
_2il0floatpacket.20	DD	04b000000H
_2il0floatpacket.21	DD	03f000000H
_2il0floatpacket.22	DD	0bf000000H
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE:BYTE
EXTRN	_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TIME_NOW:BYTE
EXTRN	_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH:BYTE
EXTRN	_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_REACH_CONVERG:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_I18:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_JUSTVEH:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TRAJALT:BYTE
_DATA	ENDS
EXTRN	_for_stop_core:PROC
EXTRN	_for_write_seq_lis_xmit:PROC
EXTRN	_for_write_seq_lis:PROC
EXTRN	_for_write_seq_fmt_xmit:PROC
EXTRN	_for_write_seq_fmt:PROC
EXTRN	_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_P_SIZE:PROC
EXTRN	_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_VALUE:PROC
EXTRN	__fltused:BYTE
	INCLUDELIB <ifconsol>
	INCLUDELIB <libifcoremt>
	INCLUDELIB <libifport>
	INCLUDELIB <libmmt>
	INCLUDELIB <LIBCMT>
	INCLUDELIB <libirc>
	INCLUDELIB <svml_dispmt>
	INCLUDELIB <OLDNAMES>
	END
