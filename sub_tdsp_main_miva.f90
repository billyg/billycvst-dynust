      SUBROUTINE TDSP_MAIN_MIVA(dynust_mode_flag,isnow,IvehType)
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~ Copyright (C) 2002-2011 The Arizona Board of Regents on Behalf of The University of Arizona.  ~~!
!~~ Developed by Yi-Chang Chiu, Ph.D. as par to the DynusT project.                               ~~!
!~~ This program is free software: you can redistribute it and/or modify it                       ~~!
!~~ under the terms of the GNU General Public License version 2 as published by                   ~~!
!~~ the Free Software Foundation.                                                                 ~~!
!~~ This program is distributed in the hope that it will be useful,                               ~~!
!~~ but WITHOUT ANY WARRANTY; without even the implied warranty of                                ~~!
!~~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                                          ~~!
!~~ See the GNU General Public License for more details.                                          ~~!
!~~ You should have received a copy of the GNU General Public License                             ~~!
!~~ along with this program (in the file COPYING).                                                ~~!
!~~ IF not, see <http://www.gnu.org/licenses/>.                                                   ~~!
!~~ Please see http://www.dynust.net for contact information and names                            ~~!
!~~ of individual contributors.                                                                   ~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
    USE DYNUST_MAIN_MODULE
    USE DYNUST_NETWORK_MODULE
    USE DYNUST_TDSP_ASTAR_MODULE
    
	INTEGER dynust_mode_flag,iloop,isnow,IVehType,iti_new
!> > > > > > > > > >                < < < < < < < < < < <
!>>>>>>>>>>>>>>>>>>>CODES START HERE<<<<<<<<<<<<<<<<<<<<<
!> > > > > > > > > >                < < < < < < < < < < <

    CALL Toll_Link_Pricing(IVehType,dynust_mode_flag,isnow,1)

	IF(dynust_mode_flag == 0) THEN  !dynust
      kpaths=kay
	  DO ilink=1,noofarcs
         DO itt=1,Iti_nu
            TTime(ilink,itt)=m_dynust_network_arc_nde(ilink)%TTimeOfBackLink
         ENDDO
	  ENDDO
 
    ELSE

      kpaths = kay	  
	  DO ilink=1,noofarcs
	     iti_new = iti_nuEP
         DO itt= (isnow-1)*iti_new+1, min(aggint,(isnow-1)*iti_new + iti_nu)
		  itt2 = itt-(isnow-1)*iti_new
	      IF(m_dynust_network_arc_nde(ilink)%link_iden == 1) THEN
           TTime(m_dynust_network_arc_nde(ilink)%ForToBackLink,itt2)=TravelTime(ilink,itt)/AttScale*(1.0-TravelTimeWeight/100.0)
	      ELSE
	       TTime(m_dynust_network_arc_nde(ilink)%ForToBackLink,itt2)=TravelTime(ilink,itt)/AttScale
	      ENDIF
         ENDDO
      ENDDO
    ENDIF


      IF(dynust_mode_flag == 0) THEN
         DO itt=1,Iti_nu
	         TTPenalty(:,itt,:)=m_dynust_network_arcmove_nde(:,:)%penalty
         ENDDO
    	ELSE
         iti_new = iti_nuEP
         DO itt= (isnow-1)*Iti_new+1, min(aggint,(isnow-1)*iti_new + iti_nu)
		   itt2 = itt-(isnow-1)*iti_new
           TTPenalty(:,itt2,:)=TravelPenalty(:,itt,:)
         ENDDO
	  ENDIF 


       DO ilink=1,noofarcs
        DO itt=1,Iti_nu
         DO movee=1,MaxMove
	    IF(dynust_mode_flag == 2) THEN ! SO case
          ELSE ! UE or static case
           TTmarginal(itt,ilink,movee)=TTime(ilink,itt)+TTPenalty(ilink,itt,movee)
	    ENDIF
          ENDDO
        ENDDO
       ENDDO

      CALL Toll_Link_Pricing(IVehType,dynust_mode_flag,isnow,1)

END SUBROUTINE
         

