; mark_description "Intel(R) Visual Fortran Compiler XE for applications running on IA-32, Version 13.1.1.171 Build 20130313";
; mark_description "-S";
	.686P
 	.387
	OPTION DOTNAME
	ASSUME	CS:FLAT,DS:FLAT,SS:FLAT
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
TXTST0:
; -- Begin  _CAL_LINK_DISCHARGE_RATE
; mark_begin;
IF @Version GE 800
  .MMX
ELSEIF @Version GE 612
  .MMX
  MMWORD TEXTEQU <QWORD>
ENDIF
IF @Version GE 800
  .XMM
ELSEIF @Version GE 614
  .XMM
  XMMWORD TEXTEQU <OWORD>
ENDIF
       ALIGN     16
	PUBLIC _CAL_LINK_DISCHARGE_RATE
_CAL_LINK_DISCHARGE_RATE	PROC NEAR 
; parameter 1: 8 + ebp
.B1.1:                          ; Preds .B1.0
        push      ebp                                           ;1.12
        mov       ebp, esp                                      ;1.12
        and       esp, -16                                      ;1.12
        push      edi                                           ;1.12
        push      ebx                                           ;1.12
        sub       esp, 504                                      ;1.12
        mov       ebx, 1                                        ;42.10
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFARCS] ;42.10
        test      edi, edi                                      ;42.10
        jle       .B1.11        ; Prob 2%                       ;42.10
                                ; LOE ebx esi edi
.B1.2:                          ; Preds .B1.1
        movdqa    xmm1, XMMWORD PTR [_2il0floatpacket.5]        ;176.34
        xor       ecx, ecx                                      ;
        mov       eax, DWORD PTR [_CAL_LINK_DISCHARGE_RATE$INDCAP.0.1] ;263.8
        psrlq     xmm1, 32                                      ;176.34
        movss     xmm6, DWORD PTR [_2il0floatpacket.1]          ;114.252
        mov       edx, 1                                        ;
        movss     xmm5, DWORD PTR [_2il0floatpacket.2]          ;111.189
        movss     xmm4, DWORD PTR [_2il0floatpacket.3]          ;194.103
        movaps    xmm3, XMMWORD PTR [_2il0floatpacket.4]        ;176.96
        movdqa    xmm2, XMMWORD PTR [_2il0floatpacket.6]        ;176.34
        movss     xmm0, DWORD PTR [_2il0floatpacket.7]          ;176.96
        movdqa    XMMWORD PTR [208+esp], xmm1                   ;176.96
        mov       DWORD PTR [380+esp], eax                      ;176.96
        mov       DWORD PTR [384+esp], ecx                      ;176.96
        mov       DWORD PTR [488+esp], ebx                      ;176.96
        mov       DWORD PTR [484+esp], edi                      ;176.96
        mov       DWORD PTR [388+esp], esi                      ;176.96
                                ; LOE edx
.B1.3:                          ; Preds .B1.9 .B1.2
        mov       eax, edx                                      ;45.44
        sub       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;45.44
        imul      ecx, eax, 152                                 ;45.44
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;45.3
        movsx     ebx, WORD PTR [148+eax+ecx]                   ;45.6
        cmp       ebx, 99                                       ;45.44
        jge       .B1.9         ; Prob 50%                      ;45.44
                                ; LOE eax edx ecx
.B1.4:                          ; Preds .B1.3
        mov       ebx, DWORD PTR [24+eax+ecx]                   ;48.10
        sub       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE+32] ;48.79
        imul      edi, ebx, 84                                  ;48.79
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE] ;48.7
        mov       ebx, DWORD PTR [32+esi+edi]                   ;48.10
        shl       ebx, 2                                        ;48.79
        neg       ebx                                           ;48.79
        add       ebx, DWORD PTR [esi+edi]                      ;48.79
        mov       ebx, DWORD PTR [8+ebx]                        ;48.10
        mov       esi, ebx                                      ;48.79
        and       esi, -2                                       ;48.79
        cmp       esi, 4                                        ;48.79
        je        .B1.161       ; Prob 16%                      ;48.79
                                ; LOE eax edx ecx ebx
.B1.5:                          ; Preds .B1.4
        cmp       ebx, 1                                        ;88.79
        je        .B1.150       ; Prob 5%                       ;88.79
                                ; LOE eax edx ecx ebx
.B1.6:                          ; Preds .B1.5
        cmp       ebx, 3                                        ;105.79
        je        .B1.138       ; Prob 16%                      ;105.79
                                ; LOE eax edx ecx ebx
.B1.7:                          ; Preds .B1.6
        cmp       ebx, 6                                        ;127.79
        je        .B1.75        ; Prob 16%                      ;127.79
                                ; LOE eax edx ecx ebx
.B1.8:                          ; Preds .B1.7
        cmp       ebx, 2                                        ;207.79
        je        .B1.12        ; Prob 16%                      ;207.79
                                ; LOE eax edx ecx
.B1.9:                          ; Preds .B1.178 .B1.169 .B1.159 .B1.147 .B1.134
                                ;       .B1.119 .B1.70 .B1.69 .B1.67 .B1.54
                                ;       .B1.57 .B1.8 .B1.3
        inc       edx                                           ;42.10
        mov       DWORD PTR [488+esp], edx                      ;42.10
        cmp       edx, DWORD PTR [484+esp]                      ;42.10
        jle       .B1.3         ; Prob 82%                      ;42.10
                                ; LOE edx
.B1.10:                         ; Preds .B1.9
        mov       esi, DWORD PTR [388+esp]                      ;
                                ; LOE esi
.B1.11:                         ; Preds .B1.10 .B1.1
        add       esp, 504                                      ;290.1
        pop       ebx                                           ;290.1
        pop       edi                                           ;290.1
        mov       esp, ebp                                      ;290.1
        pop       ebp                                           ;290.1
        ret                                                     ;290.1
                                ; LOE
.B1.12:                         ; Preds .B1.8                   ; Infreq
        mov       esi, DWORD PTR [100+eax+ecx]                  ;210.13
        mov       ebx, DWORD PTR [104+eax+ecx]                  ;210.13
        imul      ebx, esi                                      ;210.52
        mov       eax, DWORD PTR [72+eax+ecx]                   ;210.13
        sub       eax, ebx                                      ;210.52
        mov       edx, DWORD PTR [488+esp]                      ;42.10
        mov       DWORD PTR [460+esp], edx                      ;42.10
        cmp       DWORD PTR [esi+eax], 0                        ;210.52
        jle       .B1.74        ; Prob 16%                      ;210.52
                                ; LOE
.B1.13:                         ; Preds .B1.12                  ; Infreq
        lea       eax, DWORD PTR [464+esp]                      ;211.17
        lea       ecx, DWORD PTR [460+esp]                      ;211.17
        lea       edx, DWORD PTR [404+esp]                      ;211.17
        push      eax                                           ;211.17
        push      OFFSET FLAT: __NLITPACK_2.0.1                 ;211.17
        push      DWORD PTR [8+ebp]                             ;211.17
        push      edx                                           ;211.17
        push      ecx                                           ;211.17
        call      _SIGNAL_PERMISSIVE_LEFT_RATE                  ;211.17
                                ; LOE
.B1.184:                        ; Preds .B1.74 .B1.13           ; Infreq
        add       esp, 20                                       ;211.17
                                ; LOE
.B1.14:                         ; Preds .B1.184                 ; Infreq
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;233.12
        neg       ecx                                           ;
        mov       edx, DWORD PTR [460+esp]                      ;233.19
        add       ecx, edx                                      ;
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGNDATA+32] ;225.5
        imul      edi, ecx, 152                                 ;
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;233.12
        lea       ecx, DWORD PTR [eax*8]                        ;
        mov       ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGNCOUNT] ;217.10
        lea       eax, DWORD PTR [ecx+eax*4]                    ;
        mov       DWORD PTR [192+esp], esi                      ;233.12
        test      ebx, ebx                                      ;217.10
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGNDATA] ;225.5
        mov       DWORD PTR [156+esp], edi                      ;
        jle       .B1.19        ; Prob 2%                       ;217.10
                                ; LOE eax edx ebx esi edi
.B1.15:                         ; Preds .B1.14                  ; Infreq
        mov       ecx, DWORD PTR [192+esp]                      ;218.15
        mov       DWORD PTR [152+esp], 1                        ;
        mov       DWORD PTR [144+esp], esi                      ;
        mov       ecx, DWORD PTR [24+ecx+edi]                   ;218.15
        mov       DWORD PTR [148+esp], ecx                      ;218.15
        mov       ecx, esi                                      ;
        mov       DWORD PTR [468+esp], edx                      ;
        sub       ecx, eax                                      ;
        mov       edx, DWORD PTR [148+esp]                      ;
        mov       esi, DWORD PTR [152+esp]                      ;
                                ; LOE eax edx ecx ebx esi
.B1.16:                         ; Preds .B1.17 .B1.15           ; Infreq
        lea       edi, DWORD PTR [esi*8]                        ;218.49
        lea       edi, DWORD PTR [edi+esi*4]                    ;218.49
        cmp       edx, DWORD PTR [ecx+edi]                      ;218.49
        je        .B1.73        ; Prob 20%                      ;218.49
                                ; LOE eax edx ecx ebx esi
.B1.17:                         ; Preds .B1.16                  ; Infreq
        inc       esi                                           ;222.5
        cmp       esi, ebx                                      ;222.5
        jle       .B1.16        ; Prob 82%                      ;222.5
                                ; LOE eax edx ecx ebx esi
.B1.18:                         ; Preds .B1.17                  ; Infreq
        mov       esi, DWORD PTR [144+esp]                      ;
        mov       edx, DWORD PTR [468+esp]                      ;
                                ; LOE eax edx esi
.B1.19:                         ; Preds .B1.14 .B1.73 .B1.18    ; Infreq
        mov       ebx, DWORD PTR [384+esp]                      ;225.5
        lea       ecx, DWORD PTR [ebx*8]                        ;225.5
        lea       edi, DWORD PTR [ecx+ebx*4]                    ;225.5
        add       edi, esi                                      ;225.5
        sub       edi, eax                                      ;225.5
        mov       esi, DWORD PTR [4+edi]                        ;225.5
        test      esi, esi                                      ;225.5
        jle       .B1.72        ; Prob 2%                       ;225.5
                                ; LOE edx ebx esi bl bh
.B1.20:                         ; Preds .B1.19                  ; Infreq
        mov       eax, ebx                                      ;
        mov       ecx, 1                                        ;
        sub       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGNAPPRH+32] ;
        shl       eax, 5                                        ;
        add       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGNAPPRH] ;
                                ; LOE eax edx ecx esi
.B1.21:                         ; Preds .B1.22 .B1.20           ; Infreq
        cmp       edx, DWORD PTR [-4+eax+ecx*4]                 ;226.12
        je        .B1.63        ; Prob 20%                      ;226.12
                                ; LOE eax edx ecx esi
.B1.22:                         ; Preds .B1.21                  ; Infreq
        inc       ecx                                           ;230.10
        cmp       ecx, esi                                      ;230.10
        jle       .B1.21        ; Prob 82%                      ;230.10
                                ; LOE eax edx ecx esi
.B1.23:                         ; Preds .B1.22                  ; Infreq
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGNAPPRH] ;252.7
        cmp       esi, 4                                        ;251.6
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;253.12
        mov       DWORD PTR [60+esp], ecx                       ;252.7
        mov       ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGNAPPRH+32] ;252.7
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;253.12
        mov       DWORD PTR [40+esp], eax                       ;253.12
        jl        .B1.55        ; Prob 10%                      ;251.6
                                ; LOE edx ecx ebx esi
.B1.24:                         ; Preds .B1.23                  ; Infreq
        mov       edi, DWORD PTR [384+esp]                      ;251.6
        mov       eax, ebx                                      ;251.6
        shl       edi, 5                                        ;251.6
        mov       DWORD PTR [52+esp], ecx                       ;
        mov       ecx, DWORD PTR [60+esp]                       ;251.6
        shl       eax, 5                                        ;251.6
        mov       DWORD PTR [68+esp], edi                       ;251.6
        mov       DWORD PTR [64+esp], eax                       ;251.6
        add       ecx, edi                                      ;251.6
        sub       ecx, eax                                      ;251.6
        and       ecx, 15                                       ;251.6
        mov       DWORD PTR [48+esp], ecx                       ;251.6
        mov       ecx, DWORD PTR [52+esp]                       ;251.6
        je        .B1.27        ; Prob 50%                      ;251.6
                                ; LOE edx ecx ebx esi cl ch
.B1.25:                         ; Preds .B1.24                  ; Infreq
        test      BYTE PTR [48+esp], 3                          ;251.6
        jne       .B1.55        ; Prob 10%                      ;251.6
                                ; LOE edx ecx ebx esi cl ch
.B1.26:                         ; Preds .B1.25                  ; Infreq
        mov       eax, DWORD PTR [48+esp]                       ;251.6
        neg       eax                                           ;251.6
        add       eax, 16                                       ;251.6
        shr       eax, 2                                        ;251.6
        mov       DWORD PTR [48+esp], eax                       ;251.6
                                ; LOE edx ecx ebx esi cl ch
.B1.27:                         ; Preds .B1.26 .B1.24           ; Infreq
        mov       eax, DWORD PTR [48+esp]                       ;251.6
        lea       edi, DWORD PTR [4+eax]                        ;251.6
        cmp       esi, edi                                      ;251.6
        jl        .B1.55        ; Prob 10%                      ;251.6
                                ; LOE eax edx ecx ebx esi al cl ah ch
.B1.28:                         ; Preds .B1.27                  ; Infreq
        mov       edi, esi                                      ;251.6
        sub       edi, eax                                      ;251.6
        and       edi, 3                                        ;251.6
        pxor      xmm0, xmm0                                    ;
        neg       edi                                           ;251.6
        add       edi, esi                                      ;251.6
        mov       DWORD PTR [56+esp], edi                       ;251.6
        mov       edi, DWORD PTR [68+esp]                       ;
        add       edi, DWORD PTR [60+esp]                       ;
        sub       edi, DWORD PTR [64+esp]                       ;
        mov       DWORD PTR [68+esp], edi                       ;
        test      eax, eax                                      ;251.6
        mov       eax, DWORD PTR [56+esp]                       ;251.6
        jbe       .B1.32        ; Prob 3%                       ;251.6
                                ; LOE eax edx ecx ebx esi al cl ah ch xmm0
.B1.29:                         ; Preds .B1.28                  ; Infreq
        imul      edi, DWORD PTR [40+esp], -900                 ;
        movss     xmm2, DWORD PTR [_2il0floatpacket.7]          ;
        add       edi, ecx                                      ;
        mov       DWORD PTR [44+esp], 0                         ;
        mov       DWORD PTR [32+esp], ebx                       ;
        mov       DWORD PTR [36+esp], esi                       ;
        mov       DWORD PTR [468+esp], edx                      ;
        mov       DWORD PTR [56+esp], eax                       ;
        mov       edx, edi                                      ;
        mov       ebx, DWORD PTR [44+esp]                       ;
        mov       esi, DWORD PTR [68+esp]                       ;
        mov       edi, DWORD PTR [48+esp]                       ;
                                ; LOE edx ecx ebx esi edi xmm0 xmm2
.B1.30:                         ; Preds .B1.30 .B1.29           ; Infreq
        imul      eax, DWORD PTR [esi+ebx*4], 900               ;253.12
        inc       ebx                                           ;251.6
        movss     xmm1, DWORD PTR [648+eax+edx]                 ;253.34
        cmp       ebx, edi                                      ;251.6
        mulss     xmm1, xmm2                                    ;253.64
        mulss     xmm1, DWORD PTR [652+eax+edx]                 ;253.96
        addss     xmm0, xmm1                                    ;253.12
        jb        .B1.30        ; Prob 82%                      ;251.6
                                ; LOE edx ecx ebx esi edi xmm0 xmm2
.B1.31:                         ; Preds .B1.30                  ; Infreq
        mov       DWORD PTR [48+esp], edi                       ;
        mov       ebx, DWORD PTR [32+esp]                       ;
        mov       eax, DWORD PTR [56+esp]                       ;
        mov       esi, DWORD PTR [36+esp]                       ;
        mov       edx, DWORD PTR [468+esp]                      ;
                                ; LOE eax edx ecx ebx esi al ah xmm0
.B1.32:                         ; Preds .B1.28 .B1.31           ; Infreq
        mov       DWORD PTR [32+esp], ebx                       ;
        lea       ebx, DWORD PTR [648+ecx]                      ;253.34
        mov       DWORD PTR [36+esp], esi                       ;253.65
        lea       edi, DWORD PTR [652+ecx]                      ;253.65
        pxor      xmm3, xmm3                                    ;251.6
        movss     xmm3, xmm0                                    ;251.6
        movd      xmm1, ebx                                     ;253.34
        movd      xmm2, edi                                     ;253.65
        pshufd    xmm4, xmm1, 0                                 ;253.34
        movd      xmm0, DWORD PTR [40+esp]                      ;253.34
        pshufd    xmm1, xmm2, 0                                 ;253.65
        movdqa    XMMWORD PTR [128+esp], xmm4                   ;253.65
        mov       DWORD PTR [468+esp], edx                      ;253.65
        pshufd    xmm0, xmm0, 0                                 ;253.34
        mov       ebx, DWORD PTR [32+esp]                       ;253.65
        mov       esi, DWORD PTR [68+esp]                       ;253.65
        mov       edx, DWORD PTR [48+esp]                       ;253.65
        movdqa    XMMWORD PTR [112+esp], xmm1                   ;253.65
        movdqa    xmm4, XMMWORD PTR [_2il0floatpacket.6]        ;253.65
        movaps    xmm5, XMMWORD PTR [_2il0floatpacket.4]        ;253.65
                                ; LOE eax edx ecx ebx esi xmm0 xmm3 xmm4 xmm5
.B1.33:                         ; Preds .B1.33 .B1.32           ; Infreq
        movdqa    xmm1, XMMWORD PTR [esi+edx*4]                 ;253.34
        add       edx, 4                                        ;251.6
        movdqa    xmm2, XMMWORD PTR [_2il0floatpacket.5]        ;253.34
        psubd     xmm1, xmm0                                    ;253.34
        pmuludq   xmm2, xmm1                                    ;253.34
        psrlq     xmm1, 32                                      ;253.34
        pmuludq   xmm1, XMMWORD PTR [208+esp]                   ;253.34
        pand      xmm2, xmm4                                    ;253.34
        psllq     xmm1, 32                                      ;253.34
        movdqa    xmm7, XMMWORD PTR [128+esp]                   ;253.34
        por       xmm2, xmm1                                    ;253.34
        paddd     xmm7, xmm2                                    ;253.34
        cmp       edx, eax                                      ;251.6
        movd      edi, xmm7                                     ;253.34
        pshuflw   xmm6, xmm7, 238                               ;253.34
        punpckhqdq xmm7, xmm7                                   ;253.34
        paddd     xmm2, XMMWORD PTR [112+esp]                   ;253.65
        movd      xmm1, DWORD PTR [edi]                         ;253.34
        movd      edi, xmm6                                     ;253.34
        movd      xmm6, DWORD PTR [edi]                         ;253.34
        movd      edi, xmm7                                     ;253.34
        pshuflw   xmm7, xmm7, 238                               ;253.34
        punpcklqdq xmm1, xmm6                                   ;253.34
        movd      xmm6, DWORD PTR [edi]                         ;253.34
        movd      edi, xmm7                                     ;253.34
        movd      xmm7, DWORD PTR [edi]                         ;253.34
        movd      edi, xmm2                                     ;253.65
        punpcklqdq xmm6, xmm7                                   ;253.34
        shufps    xmm1, xmm6, 136                               ;253.34
        pshuflw   xmm6, xmm2, 238                               ;253.65
        movd      xmm7, DWORD PTR [edi]                         ;253.65
        movd      edi, xmm6                                     ;253.65
        punpckhqdq xmm2, xmm2                                   ;253.65
        mulps     xmm1, xmm5                                    ;253.64
        movd      xmm6, DWORD PTR [edi]                         ;253.65
        movd      edi, xmm2                                     ;253.65
        pshuflw   xmm2, xmm2, 238                               ;253.65
        punpcklqdq xmm7, xmm6                                   ;253.65
        movd      xmm6, DWORD PTR [edi]                         ;253.65
        movd      edi, xmm2                                     ;253.65
        movd      xmm2, DWORD PTR [edi]                         ;253.65
        punpcklqdq xmm6, xmm2                                   ;253.65
        shufps    xmm7, xmm6, 136                               ;253.65
        mulps     xmm1, xmm7                                    ;253.96
        addps     xmm3, xmm1                                    ;253.12
        jb        .B1.33        ; Prob 82%                      ;251.6
                                ; LOE eax edx ecx ebx esi xmm0 xmm3 xmm4 xmm5
.B1.34:                         ; Preds .B1.33                  ; Infreq
        movaps    xmm0, xmm3                                    ;251.6
        movhlps   xmm0, xmm3                                    ;251.6
        mov       esi, DWORD PTR [36+esp]                       ;
        addps     xmm3, xmm0                                    ;251.6
        movaps    xmm1, xmm3                                    ;251.6
        shufps    xmm1, xmm3, 245                               ;251.6
        mov       edx, DWORD PTR [468+esp]                      ;
        addss     xmm3, xmm1                                    ;251.6
                                ; LOE eax edx ecx ebx esi xmm3
.B1.35:                         ; Preds .B1.34 .B1.55           ; Infreq
        cmp       eax, esi                                      ;251.6
        jae       .B1.39        ; Prob 3%                       ;251.6
                                ; LOE eax edx ecx ebx esi xmm3
.B1.36:                         ; Preds .B1.35                  ; Infreq
        imul      edi, DWORD PTR [40+esp], -900                 ;
        neg       ebx                                           ;
        movss     xmm1, DWORD PTR [_2il0floatpacket.7]          ;
        add       ebx, DWORD PTR [384+esp]                      ;
        add       ecx, edi                                      ;
        shl       ebx, 5                                        ;
        add       ebx, DWORD PTR [60+esp]                       ;
                                ; LOE eax edx ecx ebx esi xmm1 xmm3
.B1.37:                         ; Preds .B1.37 .B1.36           ; Infreq
        imul      edi, DWORD PTR [ebx+eax*4], 900               ;253.12
        inc       eax                                           ;251.6
        movss     xmm0, DWORD PTR [648+edi+ecx]                 ;253.34
        cmp       eax, esi                                      ;251.6
        mulss     xmm0, xmm1                                    ;253.64
        mulss     xmm0, DWORD PTR [652+edi+ecx]                 ;253.96
        addss     xmm3, xmm0                                    ;253.12
        jb        .B1.37        ; Prob 82%                      ;251.6
                                ; LOE eax edx ecx ebx esi xmm1 xmm3
.B1.39:                         ; Preds .B1.37 .B1.72 .B1.35    ; Infreq
        cvtsi2ss  xmm0, esi                                     ;255.32
        mov       ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_LEVEL2N] ;256.10
        test      ebx, ebx                                      ;256.10
        divss     xmm3, xmm0                                    ;255.10
        jle       .B1.44        ; Prob 2%                       ;256.10
                                ; LOE edx ebx xmm3
.B1.40:                         ; Preds .B1.39                  ; Infreq
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_YIELDCAPIND+32] ;257.12
        mov       ecx, 1                                        ;
        shl       eax, 2                                        ;
        neg       eax                                           ;
        add       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_YIELDCAPIND] ;
                                ; LOE eax edx ecx ebx xmm3
.B1.41:                         ; Preds .B1.42 .B1.40           ; Infreq
        cvtsi2ss  xmm0, DWORD PTR [eax+ecx*4]                   ;257.28
        comiss    xmm0, xmm3                                    ;257.25
        jae       .B1.60        ; Prob 20%                      ;257.25
                                ; LOE eax edx ecx ebx xmm3
.B1.42:                         ; Preds .B1.41                  ; Infreq
        inc       ecx                                           ;261.5
        cmp       ecx, ebx                                      ;261.5
        jle       .B1.41        ; Prob 82%                      ;261.5
                                ; LOE eax edx ecx ebx xmm3
.B1.44:                         ; Preds .B1.42 .B1.60 .B1.39    ; Infreq
        cmp       DWORD PTR [380+esp], 0                        ;263.18
        jle       .B1.58        ; Prob 16%                      ;263.18
                                ; LOE edx ebx xmm3
.B1.45:                         ; Preds .B1.59 .B1.58 .B1.44    ; Infreq
        mov       eax, DWORD PTR [192+esp]                      ;265.12
        mov       ebx, 1                                        ;265.5
        mov       ecx, DWORD PTR [156+esp]                      ;265.12
        movsx     ecx, BYTE PTR [32+eax+ecx]                    ;265.12
        test      ecx, ecx                                      ;265.5
        jle       .B1.57        ; Prob 2%                       ;265.5
                                ; LOE edx ecx ebx
.B1.46:                         ; Preds .B1.45                  ; Infreq
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+44] ;267.55
        mov       DWORD PTR [320+esp], esi                      ;267.55
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE] ;267.12
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+40] ;267.55
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+32] ;267.12
        mov       DWORD PTR [316+esp], eax                      ;267.12
        mov       eax, 1                                        ;
        mov       DWORD PTR [276+esp], esi                      ;
        mov       DWORD PTR [280+esp], edi                      ;
        mov       DWORD PTR [284+esp], ebx                      ;
        mov       DWORD PTR [272+esp], ecx                      ;
        mov       DWORD PTR [468+esp], edx                      ;
        movss     xmm0, DWORD PTR [_2il0floatpacket.3]          ;
        movss     xmm1, DWORD PTR [_2il0floatpacket.2]          ;
        movss     xmm2, DWORD PTR [_2il0floatpacket.1]          ;
                                ; LOE eax xmm0 xmm1 xmm2
.B1.47:                         ; Preds .B1.53 .B1.46           ; Infreq
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_YIELDCAP+32] ;267.93
        shl       esi, 2                                        ;
        mov       edx, DWORD PTR [468+esp]                      ;
        mov       DWORD PTR [296+esp], esi                      ;
        mov       esi, edx                                      ;
        sub       esi, DWORD PTR [276+esp]                      ;
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_YIELDCAP+40] ;267.128
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_YIELDCAP+44] ;267.128
        mov       DWORD PTR [308+esp], ecx                      ;267.128
        imul      ebx, ecx                                      ;
        lea       ecx, DWORD PTR [esi*4]                        ;
        shl       esi, 5                                        ;
        sub       esi, ecx                                      ;
        mov       ecx, eax                                      ;
        sub       ecx, DWORD PTR [320+esp]                      ;
        imul      ecx, DWORD PTR [280+esp]                      ;
        add       ecx, DWORD PTR [316+esp]                      ;
        mov       DWORD PTR [292+esp], esi                      ;
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;267.55
        mov       esi, DWORD PTR [4+ecx+esi]                    ;267.55
        mov       DWORD PTR [300+esp], esi                      ;267.55
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;
        neg       esi                                           ;
        add       esi, edx                                      ;
        imul      esi, esi, 900                                 ;
        mov       DWORD PTR [312+esp], edi                      ;267.55
        movsx     edi, BYTE PTR [668+edi+esi]                   ;267.94
        mov       DWORD PTR [304+esp], esi                      ;
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+44] ;266.52
        neg       esi                                           ;266.52
        mov       DWORD PTR [288+esp], edi                      ;267.94
        add       esi, eax                                      ;266.52
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+32] ;266.52
        neg       edi                                           ;266.52
        add       edi, edx                                      ;266.52
        imul      esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+40] ;266.52
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE] ;266.52
        lea       edx, DWORD PTR [5+edx+edi*8]                  ;266.52
        cmp       BYTE PTR [edx+esi], 1                         ;266.52
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_YIELDCAP] ;266.52
        je        .B1.56        ; Prob 16%                      ;266.52
                                ; LOE eax edx ecx ebx xmm0 xmm1 xmm2
.B1.48:                         ; Preds .B1.47                  ; Infreq
        mov       edi, DWORD PTR [380+esp]                      ;270.12
        mov       DWORD PTR [268+esp], eax                      ;
        mov       eax, DWORD PTR [296+esp]                      ;270.12
        lea       esi, DWORD PTR [edx+edi*4]                    ;270.12
        mov       edi, DWORD PTR [308+esp]                      ;270.129
        lea       edx, DWORD PTR [edi+edi]                      ;270.129
        neg       edx                                           ;270.12
        lea       edi, DWORD PTR [edi+edi*2]                    ;270.148
        add       edx, ebx                                      ;270.12
        neg       edi                                           ;270.148
        neg       edx                                           ;270.12
        add       ebx, edi                                      ;270.12
        add       edx, esi                                      ;270.12
        sub       esi, ebx                                      ;270.12
        sub       edx, eax                                      ;270.12
        sub       esi, eax                                      ;270.12
        mov       eax, DWORD PTR [300+esp]                      ;270.93
        mov       ebx, DWORD PTR [edx]                          ;270.129
        add       ebx, DWORD PTR [esi]                          ;270.147
        imul      eax, ebx                                      ;270.93
        imul      eax, DWORD PTR [288+esp]                      ;270.127
        cvtsi2ss  xmm4, eax                                     ;270.127
        mulss     xmm4, xmm2                                    ;270.167
        divss     xmm4, xmm1                                    ;270.12
        mov       edx, DWORD PTR [292+esp]                      ;270.12
        mov       eax, DWORD PTR [268+esp]                      ;270.12
        movss     DWORD PTR [20+ecx+edx], xmm4                  ;270.12
                                ; LOE eax xmm0 xmm1 xmm2 xmm4
.B1.49:                         ; Preds .B1.56 .B1.48           ; Infreq
        cvttss2si edx, xmm4                                     ;272.55
        cvtsi2ss  xmm3, edx                                     ;272.55
        subss     xmm4, xmm3                                    ;272.54
        comiss    xmm4, xmm0                                    ;272.103
        jbe       .B1.53        ; Prob 78%                      ;272.103
                                ; LOE eax xmm0 xmm1 xmm2
.B1.50:                         ; Preds .B1.49                  ; Infreq
        mov       eax, DWORD PTR [284+esp]                      ;265.5
        mov       DWORD PTR [464+esp], eax                      ;265.5
        push      OFFSET FLAT: __NLITPACK_10.0.1                ;273.19
        call      _RANXY                                        ;273.19
                                ; LOE f1
.B1.185:                        ; Preds .B1.50                  ; Infreq
        fstp      DWORD PTR [396+esp]                           ;273.19
        movss     xmm2, DWORD PTR [_2il0floatpacket.1]          ;
        movss     xmm1, DWORD PTR [_2il0floatpacket.2]          ;
        movss     xmm0, DWORD PTR [_2il0floatpacket.3]          ;
        movss     xmm5, DWORD PTR [396+esp]                     ;273.19
        add       esp, 4                                        ;273.19
                                ; LOE xmm0 xmm1 xmm2 xmm5
.B1.51:                         ; Preds .B1.185                 ; Infreq
        mov       esi, DWORD PTR [460+esp]                      ;275.24
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+32] ;275.14
        mov       DWORD PTR [468+esp], esi                      ;275.24
        sub       esi, edi                                      ;275.20
        mov       DWORD PTR [276+esp], edi                      ;275.14
        mov       eax, DWORD PTR [464+esp]                      ;275.24
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+44] ;275.24
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+40] ;275.24
        lea       edi, DWORD PTR [esi*4]                        ;275.20
        shl       esi, 5                                        ;275.20
        sub       esi, edi                                      ;275.20
        mov       edi, eax                                      ;275.20
        sub       edi, edx                                      ;275.20
        imul      edi, ebx                                      ;275.20
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE] ;275.14
        add       edi, ecx                                      ;275.20
        mov       DWORD PTR [320+esp], edx                      ;275.24
        mov       DWORD PTR [316+esp], ecx                      ;275.14
        mov       DWORD PTR [280+esp], ebx                      ;275.24
        movss     xmm4, DWORD PTR [20+edi+esi]                  ;275.66
        cvttss2si edx, xmm4                                     ;275.66
        cvtsi2ss  xmm3, edx                                     ;275.66
        subss     xmm4, xmm3                                    ;275.65
        comiss    xmm4, xmm5                                    ;275.20
        jb        .B1.53        ; Prob 50%                      ;275.20
                                ; LOE eax edx esi edi xmm0 xmm1 xmm2
.B1.52:                         ; Preds .B1.51                  ; Infreq
        inc       edx                                           ;276.96
        cvtsi2ss  xmm3, edx                                     ;276.7
        movss     DWORD PTR [20+edi+esi], xmm3                  ;276.7
                                ; LOE eax xmm0 xmm1 xmm2
.B1.53:                         ; Preds .B1.52 .B1.51 .B1.49    ; Infreq
        inc       eax                                           ;280.10
        mov       DWORD PTR [284+esp], eax                      ;280.10
        cmp       eax, DWORD PTR [272+esp]                      ;280.10
        jle       .B1.47        ; Prob 82%                      ;280.10
                                ; LOE eax al ah xmm0 xmm1 xmm2
.B1.54:                         ; Preds .B1.53                  ; Infreq
        mov       edx, DWORD PTR [468+esp]                      ;
        mov       DWORD PTR [464+esp], eax                      ;265.5
        jmp       .B1.9         ; Prob 100%                     ;265.5
                                ; LOE edx
.B1.55:                         ; Preds .B1.23 .B1.27 .B1.25    ; Infreq
        xor       eax, eax                                      ;
        pxor      xmm3, xmm3                                    ;
        jmp       .B1.35        ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi xmm3
.B1.56:                         ; Preds .B1.47                  ; Infreq
        sub       edx, ebx                                      ;267.12
        mov       ebx, DWORD PTR [308+esp]                      ;267.12
        sub       ebx, DWORD PTR [296+esp]                      ;267.12
        add       edx, ebx                                      ;267.12
        mov       esi, DWORD PTR [380+esp]                      ;267.128
        mov       edi, DWORD PTR [292+esp]                      ;267.12
        mov       edx, DWORD PTR [edx+esi*4]                    ;267.128
        imul      edx, DWORD PTR [300+esp]                      ;267.93
        imul      edx, DWORD PTR [288+esp]                      ;267.127
        cvtsi2ss  xmm4, edx                                     ;267.127
        divss     xmm4, xmm1                                    ;267.12
        movss     DWORD PTR [20+ecx+edi], xmm4                  ;267.12
        mov       edx, DWORD PTR [304+esp]                      ;268.12
        mov       ecx, DWORD PTR [312+esp]                      ;268.12
        movss     DWORD PTR [416+ecx+edx], xmm4                 ;268.12
        jmp       .B1.49        ; Prob 100%                     ;268.12
                                ; LOE eax xmm0 xmm1 xmm2 xmm4
.B1.57:                         ; Preds .B1.138 .B1.127 .B1.110 .B1.45 ; Infreq
        mov       DWORD PTR [464+esp], 1                        ;265.5
        jmp       .B1.9         ; Prob 100%                     ;265.5
                                ; LOE edx
.B1.58:                         ; Preds .B1.44                  ; Infreq
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_YIELDCAPIND+32] ;263.18
        neg       ecx                                           ;263.21
        add       ecx, ebx                                      ;263.21
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_YIELDCAPIND] ;263.18
        cvtsi2ss  xmm0, DWORD PTR [eax+ecx*4]                   ;263.38
        comiss    xmm3, xmm0                                    ;263.36
        jbe       .B1.45        ; Prob 50%                      ;263.36
                                ; LOE edx ebx
.B1.59:                         ; Preds .B1.58                  ; Infreq
        mov       DWORD PTR [380+esp], ebx                      ;263.60
        mov       DWORD PTR [_CAL_LINK_DISCHARGE_RATE$INDCAP.0.1], ebx ;263.60
        jmp       .B1.45        ; Prob 100%                     ;263.60
                                ; LOE edx
.B1.60:                         ; Preds .B1.41                  ; Infreq
        mov       DWORD PTR [380+esp], ecx                      ;258.9
        mov       DWORD PTR [_CAL_LINK_DISCHARGE_RATE$INDCAP.0.1], ecx ;258.9
        jmp       .B1.44        ; Prob 100%                     ;258.9
                                ; LOE edx ebx xmm3
.B1.63:                         ; Preds .B1.21                  ; Infreq
        mov       ebx, DWORD PTR [192+esp]                      ;233.19
        mov       ecx, DWORD PTR [156+esp]                      ;233.19
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE] ;237.12
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+40] ;237.15
        movsx     edi, BYTE PTR [32+ebx+ecx]                    ;233.19
        test      edi, edi                                      ;233.12
        mov       DWORD PTR [20+esp], edi                       ;233.19
        mov       DWORD PTR [16+esp], eax                       ;237.12
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+44] ;237.15
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+32] ;237.12
        mov       DWORD PTR [24+esp], esi                       ;237.15
        jle       .B1.71        ; Prob 2%                       ;233.12
                                ; LOE edx ecx ebx
.B1.64:                         ; Preds .B1.63                  ; Infreq
        mov       edi, edx                                      ;
        lea       esi, DWORD PTR [edx*4]                        ;
        shl       edi, 5                                        ;
        sub       edi, esi                                      ;
        lea       esi, DWORD PTR [ecx*4]                        ;
        shl       ecx, 5                                        ;
        sub       ecx, esi                                      ;
        mov       esi, DWORD PTR [24+esp]                       ;
        imul      ebx, esi                                      ;
        sub       esi, ecx                                      ;
        movss     xmm0, DWORD PTR [404+esp]                     ;234.57
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;235.56
        mov       DWORD PTR [8+esp], eax                        ;235.56
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;235.56
        add       edi, DWORD PTR [16+esp]                       ;
        mov       DWORD PTR [4+esp], eax                        ;235.56
        xor       eax, eax                                      ;234.57
        mov       DWORD PTR [12+esp], edi                       ;
        sub       edi, ebx                                      ;
        add       edi, esi                                      ;
        mov       DWORD PTR [esp], ecx                          ;
        mov       DWORD PTR [468+esp], edx                      ;
        mov       edx, eax                                      ;
        mov       ecx, DWORD PTR [24+esp]                       ;
        mov       esi, DWORD PTR [20+esp]                       ;
                                ; LOE eax edx ecx ebx esi edi xmm0
.B1.65:                         ; Preds .B1.65 .B1.64           ; Infreq
        pxor      xmm1, xmm1                                    ;234.57
        inc       eax                                           ;234.57
        cvtsi2ss  xmm1, DWORD PTR [4+edx+edi]                   ;234.57
        mulss     xmm1, xmm0                                    ;234.15
        movss     DWORD PTR [20+edx+edi], xmm1                  ;234.15
        add       edx, ecx                                      ;234.57
        cmp       eax, esi                                      ;234.57
        jb        .B1.65        ; Prob 82%                      ;234.57
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1
.B1.66:                         ; Preds .B1.65                  ; Infreq
        mov       esi, DWORD PTR [4+esp]                        ;235.15
        inc       eax                                           ;236.12
        neg       esi                                           ;235.15
        mov       edx, DWORD PTR [468+esp]                      ;
        add       esi, edx                                      ;235.15
        imul      esi, esi, 900                                 ;235.15
        mov       edi, DWORD PTR [8+esp]                        ;235.15
        mov       ecx, DWORD PTR [esp]                          ;
        movss     DWORD PTR [416+edi+esi], xmm1                 ;235.15
        mov       edi, DWORD PTR [20+esp]                       ;236.12
        inc       edi                                           ;236.12
        mov       DWORD PTR [464+esp], edi                      ;236.12
                                ; LOE eax edx ecx ebx
.B1.67:                         ; Preds .B1.71 .B1.66           ; Infreq
        mov       esi, DWORD PTR [24+esp]                       ;237.105
        imul      esi, eax                                      ;237.105
        sub       esi, ebx                                      ;237.105
        mov       eax, DWORD PTR [12+esp]                       ;237.105
        add       eax, esi                                      ;237.105
        sub       eax, ecx                                      ;237.105
        movss     xmm1, DWORD PTR [20+eax]                      ;237.57
        cvttss2si ecx, xmm1                                     ;237.57
        cvtsi2ss  xmm0, ecx                                     ;237.57
        subss     xmm1, xmm0                                    ;237.56
        comiss    xmm1, DWORD PTR [_2il0floatpacket.3]          ;237.105
        jbe       .B1.9         ; Prob 78%                      ;237.105
                                ; LOE edx
.B1.68:                         ; Preds .B1.67                  ; Infreq
        push      OFFSET FLAT: __NLITPACK_9.0.1                 ;239.19
        call      _RANXY                                        ;239.19
                                ; LOE f1
.B1.186:                        ; Preds .B1.68                  ; Infreq
        fstp      DWORD PTR [396+esp]                           ;239.19
        movss     xmm2, DWORD PTR [396+esp]                     ;239.19
        add       esp, 4                                        ;239.19
                                ; LOE xmm2
.B1.69:                         ; Preds .B1.186                 ; Infreq
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+32] ;241.14
        mov       ecx, DWORD PTR [464+esp]                      ;241.24
        neg       ebx                                           ;241.20
        mov       edx, DWORD PTR [460+esp]                      ;241.24
        add       ebx, edx                                      ;241.20
        sub       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+44] ;241.20
        imul      ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+40] ;241.20
        add       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE] ;241.20
        lea       eax, DWORD PTR [ebx*4]                        ;241.20
        shl       ebx, 5                                        ;241.20
        sub       ebx, eax                                      ;241.20
        movss     xmm1, DWORD PTR [20+ecx+ebx]                  ;241.24
        cvttss2si eax, xmm1                                     ;241.66
        cvtsi2ss  xmm0, eax                                     ;241.66
        subss     xmm1, xmm0                                    ;241.65
        comiss    xmm1, xmm2                                    ;241.20
        jb        .B1.9         ; Prob 50%                      ;241.20
                                ; LOE eax edx ecx ebx
.B1.70:                         ; Preds .B1.69                  ; Infreq
        inc       eax                                           ;242.96
        cvtsi2ss  xmm0, eax                                     ;242.7
        movss     DWORD PTR [20+ecx+ebx], xmm0                  ;242.7
        jmp       .B1.9         ; Prob 100%                     ;242.7
                                ; LOE edx
.B1.71:                         ; Preds .B1.63                  ; Infreq
        mov       edi, edx                                      ;
        lea       esi, DWORD PTR [edx*4]                        ;
        shl       edi, 5                                        ;
        mov       eax, 1                                        ;
        sub       edi, esi                                      ;
        lea       esi, DWORD PTR [ecx*4]                        ;
        add       edi, DWORD PTR [16+esp]                       ;
        shl       ecx, 5                                        ;
        imul      ebx, DWORD PTR [24+esp]                       ;
        sub       ecx, esi                                      ;
        mov       DWORD PTR [464+esp], 1                        ;265.5
        mov       DWORD PTR [12+esp], edi                       ;
        jmp       .B1.67        ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx
.B1.72:                         ; Preds .B1.19                  ; Infreq
        pxor      xmm3, xmm3                                    ;
        jmp       .B1.39        ; Prob 100%                     ;
                                ; LOE edx esi xmm3
.B1.73:                         ; Preds .B1.16                  ; Infreq
        mov       DWORD PTR [152+esp], esi                      ;
        mov       ecx, esi                                      ;219.14
        mov       esi, DWORD PTR [144+esp]                      ;
        mov       edx, DWORD PTR [468+esp]                      ;
        mov       DWORD PTR [384+esp], ecx                      ;219.14
        jmp       .B1.19        ; Prob 100%                     ;219.14
                                ; LOE eax edx esi
.B1.74:                         ; Preds .B1.12                  ; Infreq
        lea       edx, DWORD PTR [460+esp]                      ;213.17
        lea       eax, DWORD PTR [404+esp]                      ;213.17
        push      OFFSET FLAT: __NLITPACK_1.0.1                 ;213.17
        push      OFFSET FLAT: __NLITPACK_3.0.1                 ;213.17
        push      DWORD PTR [8+ebp]                             ;213.17
        push      eax                                           ;213.17
        push      edx                                           ;213.17
        call      _SIGNAL_PERMISSIVE_LEFT_RATE                  ;213.17
        jmp       .B1.184       ; Prob 100%                     ;213.17
                                ; LOE
.B1.75:                         ; Preds .B1.7                   ; Infreq
        mov       esi, DWORD PTR [100+eax+ecx]                  ;130.13
        mov       ebx, DWORD PTR [104+eax+ecx]                  ;130.13
        imul      ebx, esi                                      ;130.52
        mov       eax, DWORD PTR [72+eax+ecx]                   ;130.13
        sub       eax, ebx                                      ;130.52
        mov       edx, DWORD PTR [488+esp]                      ;42.10
        mov       DWORD PTR [460+esp], edx                      ;42.10
        cmp       DWORD PTR [esi+eax], 0                        ;130.52
        jle       .B1.137       ; Prob 16%                      ;130.52
                                ; LOE
.B1.76:                         ; Preds .B1.75                  ; Infreq
        lea       eax, DWORD PTR [464+esp]                      ;131.17
        lea       ecx, DWORD PTR [460+esp]                      ;131.17
        lea       edx, DWORD PTR [404+esp]                      ;131.17
        push      eax                                           ;131.17
        push      OFFSET FLAT: __NLITPACK_2.0.1                 ;131.17
        push      DWORD PTR [8+ebp]                             ;131.17
        push      edx                                           ;131.17
        push      ecx                                           ;131.17
        call      _SIGNAL_PERMISSIVE_LEFT_RATE                  ;131.17
                                ; LOE
.B1.188:                        ; Preds .B1.137 .B1.76          ; Infreq
        add       esp, 20                                       ;131.17
                                ; LOE
.B1.77:                         ; Preds .B1.188                 ; Infreq
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;152.12
        neg       ecx                                           ;
        mov       edx, DWORD PTR [460+esp]                      ;152.19
        add       ecx, edx                                      ;
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGNDATA+32] ;144.5
        imul      edi, ecx, 152                                 ;
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;152.12
        lea       ecx, DWORD PTR [eax*8]                        ;
        mov       ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGNCOUNT] ;136.10
        lea       eax, DWORD PTR [ecx+eax*4]                    ;
        mov       DWORD PTR [228+esp], esi                      ;152.12
        test      ebx, ebx                                      ;136.10
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGNDATA] ;144.5
        mov       DWORD PTR [224+esp], edi                      ;
        jle       .B1.82        ; Prob 2%                       ;136.10
                                ; LOE eax edx ebx esi edi
.B1.78:                         ; Preds .B1.77                  ; Infreq
        mov       ecx, DWORD PTR [228+esp]                      ;137.15
        mov       DWORD PTR [204+esp], 1                        ;
        mov       DWORD PTR [196+esp], esi                      ;
        mov       ecx, DWORD PTR [24+ecx+edi]                   ;137.15
        mov       DWORD PTR [200+esp], ecx                      ;137.15
        mov       ecx, esi                                      ;
        mov       DWORD PTR [468+esp], edx                      ;
        sub       ecx, eax                                      ;
        mov       edx, DWORD PTR [200+esp]                      ;
        mov       esi, DWORD PTR [204+esp]                      ;
                                ; LOE eax edx ecx ebx esi
.B1.79:                         ; Preds .B1.80 .B1.78           ; Infreq
        lea       edi, DWORD PTR [esi*8]                        ;137.49
        lea       edi, DWORD PTR [edi+esi*4]                    ;137.49
        cmp       edx, DWORD PTR [ecx+edi]                      ;137.49
        je        .B1.136       ; Prob 20%                      ;137.49
                                ; LOE eax edx ecx ebx esi
.B1.80:                         ; Preds .B1.79                  ; Infreq
        inc       esi                                           ;141.5
        cmp       esi, ebx                                      ;141.5
        jle       .B1.79        ; Prob 82%                      ;141.5
                                ; LOE eax edx ecx ebx esi
.B1.81:                         ; Preds .B1.80                  ; Infreq
        mov       esi, DWORD PTR [196+esp]                      ;
        mov       edx, DWORD PTR [468+esp]                      ;
                                ; LOE eax edx esi
.B1.82:                         ; Preds .B1.77 .B1.136 .B1.81   ; Infreq
        mov       ebx, DWORD PTR [384+esp]                      ;144.5
        lea       ecx, DWORD PTR [ebx*8]                        ;144.5
        lea       edi, DWORD PTR [ecx+ebx*4]                    ;144.5
        add       edi, esi                                      ;144.5
        sub       edi, eax                                      ;144.5
        mov       esi, DWORD PTR [4+edi]                        ;144.5
        test      esi, esi                                      ;144.5
        jle       .B1.87        ; Prob 2%                       ;144.5
                                ; LOE edx ebx esi bl bh
.B1.83:                         ; Preds .B1.82                  ; Infreq
        mov       eax, ebx                                      ;
        mov       ecx, 1                                        ;
        sub       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGNAPPRH+32] ;
        shl       eax, 5                                        ;
        add       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGNAPPRH] ;
                                ; LOE eax edx ecx esi
.B1.84:                         ; Preds .B1.85 .B1.83           ; Infreq
        cmp       edx, DWORD PTR [-4+eax+ecx*4]                 ;145.12
        je        .B1.127       ; Prob 20%                      ;145.12
                                ; LOE eax edx ecx esi
.B1.85:                         ; Preds .B1.84                  ; Infreq
        inc       ecx                                           ;149.10
        cmp       ecx, esi                                      ;149.10
        jle       .B1.84        ; Prob 82%                      ;149.10
                                ; LOE eax edx ecx esi
.B1.86:                         ; Preds .B1.85                  ; Infreq
        test      esi, esi                                      ;144.5
                                ; LOE edx esi
.B1.87:                         ; Preds .B1.86 .B1.82           ; Infreq
        mov       DWORD PTR [_CAL_LINK_DISCHARGE_RATE$INDCAP.0.1], 0 ;173.5
        jle       .B1.126       ; Prob 0%                       ;174.6
                                ; LOE edx esi
.B1.88:                         ; Preds .B1.87                  ; Infreq
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGNAPPRH] ;175.7
        cmp       esi, 4                                        ;174.6
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;176.12
        mov       DWORD PTR [100+esp], ecx                      ;175.7
        mov       ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIGNAPPRH+32] ;175.7
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;176.12
        mov       DWORD PTR [80+esp], eax                       ;176.12
        jl        .B1.120       ; Prob 10%                      ;174.6
                                ; LOE edx ecx ebx esi
.B1.89:                         ; Preds .B1.88                  ; Infreq
        mov       edi, DWORD PTR [384+esp]                      ;174.6
        mov       eax, ebx                                      ;174.6
        shl       edi, 5                                        ;174.6
        mov       DWORD PTR [92+esp], ecx                       ;
        mov       ecx, DWORD PTR [100+esp]                      ;174.6
        shl       eax, 5                                        ;174.6
        mov       DWORD PTR [108+esp], edi                      ;174.6
        mov       DWORD PTR [104+esp], eax                      ;174.6
        add       ecx, edi                                      ;174.6
        sub       ecx, eax                                      ;174.6
        and       ecx, 15                                       ;174.6
        mov       DWORD PTR [88+esp], ecx                       ;174.6
        mov       ecx, DWORD PTR [92+esp]                       ;174.6
        je        .B1.92        ; Prob 50%                      ;174.6
                                ; LOE edx ecx ebx esi cl ch
.B1.90:                         ; Preds .B1.89                  ; Infreq
        test      BYTE PTR [88+esp], 3                          ;174.6
        jne       .B1.120       ; Prob 10%                      ;174.6
                                ; LOE edx ecx ebx esi cl ch
.B1.91:                         ; Preds .B1.90                  ; Infreq
        mov       eax, DWORD PTR [88+esp]                       ;174.6
        neg       eax                                           ;174.6
        add       eax, 16                                       ;174.6
        shr       eax, 2                                        ;174.6
        mov       DWORD PTR [88+esp], eax                       ;174.6
                                ; LOE edx ecx ebx esi cl ch
.B1.92:                         ; Preds .B1.91 .B1.89           ; Infreq
        mov       eax, DWORD PTR [88+esp]                       ;174.6
        lea       edi, DWORD PTR [4+eax]                        ;174.6
        cmp       esi, edi                                      ;174.6
        jl        .B1.120       ; Prob 10%                      ;174.6
                                ; LOE eax edx ecx ebx esi al cl ah ch
.B1.93:                         ; Preds .B1.92                  ; Infreq
        mov       edi, esi                                      ;174.6
        sub       edi, eax                                      ;174.6
        and       edi, 3                                        ;174.6
        pxor      xmm0, xmm0                                    ;
        neg       edi                                           ;174.6
        add       edi, esi                                      ;174.6
        mov       DWORD PTR [96+esp], edi                       ;174.6
        mov       edi, DWORD PTR [108+esp]                      ;
        add       edi, DWORD PTR [100+esp]                      ;
        sub       edi, DWORD PTR [104+esp]                      ;
        mov       DWORD PTR [108+esp], edi                      ;
        test      eax, eax                                      ;174.6
        mov       eax, DWORD PTR [96+esp]                       ;174.6
        jbe       .B1.97        ; Prob 3%                       ;174.6
                                ; LOE eax edx ecx ebx esi al cl ah ch xmm0
.B1.94:                         ; Preds .B1.93                  ; Infreq
        imul      edi, DWORD PTR [80+esp], -900                 ;
        movss     xmm2, DWORD PTR [_2il0floatpacket.7]          ;
        add       edi, ecx                                      ;
        mov       DWORD PTR [84+esp], 0                         ;
        mov       DWORD PTR [72+esp], ebx                       ;
        mov       DWORD PTR [76+esp], esi                       ;
        mov       DWORD PTR [468+esp], edx                      ;
        mov       DWORD PTR [96+esp], eax                       ;
        mov       edx, edi                                      ;
        mov       ebx, DWORD PTR [84+esp]                       ;
        mov       esi, DWORD PTR [108+esp]                      ;
        mov       edi, DWORD PTR [88+esp]                       ;
                                ; LOE edx ecx ebx esi edi xmm0 xmm2
.B1.95:                         ; Preds .B1.95 .B1.94           ; Infreq
        imul      eax, DWORD PTR [esi+ebx*4], 900               ;252.7
        inc       ebx                                           ;174.6
        movss     xmm1, DWORD PTR [648+eax+edx]                 ;176.34
        cmp       ebx, edi                                      ;174.6
        mulss     xmm1, xmm2                                    ;176.64
        mulss     xmm1, DWORD PTR [652+eax+edx]                 ;176.96
        addss     xmm0, xmm1                                    ;176.12
        jb        .B1.95        ; Prob 82%                      ;174.6
                                ; LOE edx ecx ebx esi edi xmm0 xmm2
.B1.96:                         ; Preds .B1.95                  ; Infreq
        mov       DWORD PTR [88+esp], edi                       ;
        mov       ebx, DWORD PTR [72+esp]                       ;
        mov       eax, DWORD PTR [96+esp]                       ;
        mov       esi, DWORD PTR [76+esp]                       ;
        mov       edx, DWORD PTR [468+esp]                      ;
                                ; LOE eax edx ecx ebx esi al ah xmm0
.B1.97:                         ; Preds .B1.93 .B1.96           ; Infreq
        mov       DWORD PTR [72+esp], ebx                       ;
        lea       ebx, DWORD PTR [648+ecx]                      ;176.34
        mov       DWORD PTR [76+esp], esi                       ;176.65
        lea       edi, DWORD PTR [652+ecx]                      ;176.65
        pxor      xmm3, xmm3                                    ;250.5
        movss     xmm3, xmm0                                    ;250.5
        movd      xmm1, ebx                                     ;176.34
        movd      xmm2, edi                                     ;176.65
        pshufd    xmm4, xmm1, 0                                 ;176.34
        movd      xmm0, DWORD PTR [80+esp]                      ;176.34
        pshufd    xmm1, xmm2, 0                                 ;176.65
        movdqa    XMMWORD PTR [176+esp], xmm4                   ;176.65
        mov       DWORD PTR [468+esp], edx                      ;176.65
        pshufd    xmm0, xmm0, 0                                 ;176.34
        mov       ebx, DWORD PTR [72+esp]                       ;176.65
        mov       esi, DWORD PTR [108+esp]                      ;176.65
        mov       edx, DWORD PTR [88+esp]                       ;176.65
        movdqa    XMMWORD PTR [160+esp], xmm1                   ;176.65
        movdqa    xmm4, XMMWORD PTR [_2il0floatpacket.6]        ;176.65
        movaps    xmm5, XMMWORD PTR [_2il0floatpacket.4]        ;176.65
                                ; LOE eax edx ecx ebx esi xmm0 xmm3 xmm4 xmm5
.B1.98:                         ; Preds .B1.98 .B1.97           ; Infreq
        movdqa    xmm1, XMMWORD PTR [esi+edx*4]                 ;176.34
        add       edx, 4                                        ;174.6
        movdqa    xmm2, XMMWORD PTR [_2il0floatpacket.5]        ;176.34
        psubd     xmm1, xmm0                                    ;176.34
        pmuludq   xmm2, xmm1                                    ;176.34
        psrlq     xmm1, 32                                      ;176.34
        pmuludq   xmm1, XMMWORD PTR [208+esp]                   ;176.34
        pand      xmm2, xmm4                                    ;176.34
        psllq     xmm1, 32                                      ;176.34
        movdqa    xmm7, XMMWORD PTR [176+esp]                   ;176.34
        por       xmm2, xmm1                                    ;176.34
        paddd     xmm7, xmm2                                    ;176.34
        cmp       edx, eax                                      ;174.6
        movd      edi, xmm7                                     ;176.34
        pshuflw   xmm6, xmm7, 238                               ;176.34
        punpckhqdq xmm7, xmm7                                   ;176.34
        paddd     xmm2, XMMWORD PTR [160+esp]                   ;176.65
        movd      xmm1, DWORD PTR [edi]                         ;176.34
        movd      edi, xmm6                                     ;176.34
        movd      xmm6, DWORD PTR [edi]                         ;176.34
        movd      edi, xmm7                                     ;176.34
        pshuflw   xmm7, xmm7, 238                               ;176.34
        punpcklqdq xmm1, xmm6                                   ;176.34
        movd      xmm6, DWORD PTR [edi]                         ;176.34
        movd      edi, xmm7                                     ;176.34
        movd      xmm7, DWORD PTR [edi]                         ;176.34
        movd      edi, xmm2                                     ;176.65
        punpcklqdq xmm6, xmm7                                   ;176.34
        shufps    xmm1, xmm6, 136                               ;176.34
        pshuflw   xmm6, xmm2, 238                               ;176.65
        movd      xmm7, DWORD PTR [edi]                         ;176.65
        movd      edi, xmm6                                     ;176.65
        punpckhqdq xmm2, xmm2                                   ;176.65
        mulps     xmm1, xmm5                                    ;176.64
        movd      xmm6, DWORD PTR [edi]                         ;176.65
        movd      edi, xmm2                                     ;176.65
        pshuflw   xmm2, xmm2, 238                               ;176.65
        punpcklqdq xmm7, xmm6                                   ;176.65
        movd      xmm6, DWORD PTR [edi]                         ;176.65
        movd      edi, xmm2                                     ;176.65
        movd      xmm2, DWORD PTR [edi]                         ;176.65
        punpcklqdq xmm6, xmm2                                   ;176.65
        shufps    xmm7, xmm6, 136                               ;176.65
        mulps     xmm1, xmm7                                    ;176.96
        addps     xmm3, xmm1                                    ;176.12
        jb        .B1.98        ; Prob 82%                      ;174.6
                                ; LOE eax edx ecx ebx esi xmm0 xmm3 xmm4 xmm5
.B1.99:                         ; Preds .B1.98                  ; Infreq
        movaps    xmm0, xmm3                                    ;250.5
        movhlps   xmm0, xmm3                                    ;250.5
        mov       esi, DWORD PTR [76+esp]                       ;
        addps     xmm3, xmm0                                    ;250.5
        movaps    xmm1, xmm3                                    ;250.5
        shufps    xmm1, xmm3, 245                               ;250.5
        mov       edx, DWORD PTR [468+esp]                      ;
        addss     xmm3, xmm1                                    ;250.5
                                ; LOE eax edx ecx ebx esi xmm3
.B1.100:                        ; Preds .B1.99 .B1.120          ; Infreq
        cmp       eax, esi                                      ;174.6
        jae       .B1.104       ; Prob 3%                       ;174.6
                                ; LOE eax edx ecx ebx esi xmm3
.B1.101:                        ; Preds .B1.100                 ; Infreq
        imul      edi, DWORD PTR [80+esp], -900                 ;
        neg       ebx                                           ;
        movss     xmm1, DWORD PTR [_2il0floatpacket.7]          ;
        add       ebx, DWORD PTR [384+esp]                      ;
        add       ecx, edi                                      ;
        shl       ebx, 5                                        ;
        add       ebx, DWORD PTR [100+esp]                      ;
                                ; LOE eax edx ecx ebx esi xmm1 xmm3
.B1.102:                        ; Preds .B1.102 .B1.101         ; Infreq
        imul      edi, DWORD PTR [ebx+eax*4], 900               ;252.7
        inc       eax                                           ;174.6
        movss     xmm0, DWORD PTR [648+edi+ecx]                 ;176.34
        cmp       eax, esi                                      ;174.6
        mulss     xmm0, xmm1                                    ;176.64
        mulss     xmm0, DWORD PTR [652+edi+ecx]                 ;176.96
        addss     xmm3, xmm0                                    ;176.12
        jb        .B1.102       ; Prob 82%                      ;174.6
                                ; LOE eax edx ecx ebx esi xmm1 xmm3
.B1.104:                        ; Preds .B1.102 .B1.126 .B1.100 ; Infreq
        cvtsi2ss  xmm0, esi                                     ;178.32
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_LEVEL2N] ;179.10
        test      ecx, ecx                                      ;179.10
        divss     xmm3, xmm0                                    ;178.10
        jle       .B1.109       ; Prob 2%                       ;179.10
                                ; LOE edx ecx xmm3
.B1.105:                        ; Preds .B1.104                 ; Infreq
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_STOPCAP2WIND+32] ;180.12
        shl       eax, 2                                        ;
        mov       DWORD PTR [380+esp], 1                        ;
        neg       eax                                           ;
        add       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_STOPCAP2WIND] ;
        mov       ebx, DWORD PTR [380+esp]                      ;
                                ; LOE eax edx ecx ebx xmm3
.B1.106:                        ; Preds .B1.107 .B1.105         ; Infreq
        cvtsi2ss  xmm0, DWORD PTR [eax+ebx*4]                   ;180.28
        comiss    xmm0, xmm3                                    ;180.25
        jae       .B1.123       ; Prob 20%                      ;180.25
                                ; LOE eax edx ecx ebx xmm3
.B1.107:                        ; Preds .B1.106                 ; Infreq
        inc       ebx                                           ;184.5
        cmp       ebx, ecx                                      ;184.5
        jle       .B1.106       ; Prob 82%                      ;184.5
                                ; LOE eax edx ecx ebx xmm3
.B1.109:                        ; Preds .B1.107 .B1.123 .B1.104 ; Infreq
        mov       DWORD PTR [380+esp], ecx                      ;185.23
        mov       DWORD PTR [_CAL_LINK_DISCHARGE_RATE$INDCAP.0.1], ecx ;185.23
                                ; LOE edx
.B1.110:                        ; Preds .B1.109 .B1.123         ; Infreq
        mov       eax, DWORD PTR [228+esp]                      ;187.12
        mov       edi, 1                                        ;187.5
        mov       ecx, DWORD PTR [224+esp]                      ;187.12
        movsx     esi, BYTE PTR [32+eax+ecx]                    ;187.12
        test      esi, esi                                      ;187.5
        jle       .B1.57        ; Prob 2%                       ;187.5
                                ; LOE edx esi edi
.B1.111:                        ; Preds .B1.110                 ; Infreq
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE] ;189.12
        mov       DWORD PTR [328+esp], ecx                      ;189.12
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+44] ;189.55
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+40] ;189.55
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+32] ;189.12
        mov       DWORD PTR [332+esp], eax                      ;189.12
        mov       eax, 1                                        ;
        mov       DWORD PTR [340+esp], ebx                      ;
        mov       DWORD PTR [344+esp], ecx                      ;
        mov       DWORD PTR [352+esp], edi                      ;
        mov       DWORD PTR [336+esp], esi                      ;
        mov       DWORD PTR [468+esp], edx                      ;
        movss     xmm0, DWORD PTR [_2il0floatpacket.3]          ;
        movss     xmm1, DWORD PTR [_2il0floatpacket.2]          ;
        movss     xmm2, DWORD PTR [_2il0floatpacket.1]          ;
                                ; LOE eax xmm0 xmm1 xmm2
.B1.112:                        ; Preds .B1.118 .B1.111         ; Infreq
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;189.55
        neg       esi                                           ;
        mov       edx, DWORD PTR [468+esp]                      ;
        add       esi, edx                                      ;
        imul      esi, esi, 900                                 ;
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;189.55
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_STOPCAP2W+40] ;189.128
        mov       DWORD PTR [356+esp], ebx                      ;189.55
        mov       DWORD PTR [360+esp], esi                      ;
        movsx     ebx, BYTE PTR [668+ebx+esi]                   ;189.94
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_STOPCAP2W+44] ;189.128
        mov       DWORD PTR [376+esp], edi                      ;189.128
        imul      esi, edi                                      ;
        mov       edi, edx                                      ;
        sub       edi, DWORD PTR [332+esp]                      ;
        mov       DWORD PTR [348+esp], ebx                      ;189.94
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_STOPCAP2W+32] ;189.93
        shl       ebx, 2                                        ;
        mov       DWORD PTR [364+esp], ebx                      ;
        lea       ebx, DWORD PTR [edi*4]                        ;
        shl       edi, 5                                        ;
        sub       edi, ebx                                      ;
        mov       ebx, eax                                      ;
        sub       ebx, DWORD PTR [344+esp]                      ;
        imul      ebx, DWORD PTR [340+esp]                      ;
        add       ebx, DWORD PTR [328+esp]                      ;
        mov       DWORD PTR [368+esp], edi                      ;
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+44] ;188.13
        mov       edi, DWORD PTR [4+ebx+edi]                    ;189.55
        neg       ecx                                           ;188.52
        mov       DWORD PTR [372+esp], edi                      ;189.55
        add       ecx, eax                                      ;188.52
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+32] ;188.52
        neg       edi                                           ;188.52
        add       edi, edx                                      ;188.52
        imul      ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+40] ;188.52
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE] ;188.52
        lea       edx, DWORD PTR [5+edx+edi*8]                  ;188.52
        cmp       BYTE PTR [edx+ecx], 1                         ;188.52
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_STOPCAP2W] ;188.52
        je        .B1.121       ; Prob 16%                      ;188.52
                                ; LOE eax edx ebx esi xmm0 xmm1 xmm2
.B1.113:                        ; Preds .B1.112                 ; Infreq
        mov       edi, DWORD PTR [380+esp]                      ;192.12
        mov       DWORD PTR [324+esp], eax                      ;
        mov       eax, DWORD PTR [364+esp]                      ;192.12
        lea       ecx, DWORD PTR [edx+edi*4]                    ;192.12
        mov       edi, DWORD PTR [376+esp]                      ;192.129
        lea       edx, DWORD PTR [edi+edi]                      ;192.129
        neg       edx                                           ;192.12
        lea       edi, DWORD PTR [edi+edi*2]                    ;192.149
        add       edx, esi                                      ;192.12
        neg       edi                                           ;192.149
        neg       edx                                           ;192.12
        add       esi, edi                                      ;192.12
        add       edx, ecx                                      ;192.12
        sub       ecx, esi                                      ;192.12
        sub       edx, eax                                      ;192.12
        sub       ecx, eax                                      ;192.12
        mov       eax, DWORD PTR [372+esp]                      ;192.93
        mov       esi, DWORD PTR [edx]                          ;192.129
        add       esi, DWORD PTR [ecx]                          ;192.148
        imul      eax, esi                                      ;192.93
        imul      eax, DWORD PTR [348+esp]                      ;192.127
        cvtsi2ss  xmm4, eax                                     ;192.127
        mulss     xmm4, xmm2                                    ;192.169
        divss     xmm4, xmm1                                    ;192.12
        mov       edx, DWORD PTR [368+esp]                      ;192.12
        mov       eax, DWORD PTR [324+esp]                      ;192.12
        movss     DWORD PTR [20+ebx+edx], xmm4                  ;192.12
                                ; LOE eax xmm0 xmm1 xmm2 xmm4
.B1.114:                        ; Preds .B1.121 .B1.113         ; Infreq
        cvttss2si edx, xmm4                                     ;194.55
        cvtsi2ss  xmm3, edx                                     ;194.55
        subss     xmm4, xmm3                                    ;194.54
        comiss    xmm4, xmm0                                    ;194.103
        jbe       .B1.118       ; Prob 78%                      ;194.103
                                ; LOE eax xmm0 xmm1 xmm2
.B1.115:                        ; Preds .B1.114                 ; Infreq
        mov       eax, DWORD PTR [352+esp]                      ;265.5
        mov       DWORD PTR [464+esp], eax                      ;265.5
        push      OFFSET FLAT: __NLITPACK_8.0.1                 ;195.20
        call      _RANXY                                        ;195.20
                                ; LOE f1
.B1.189:                        ; Preds .B1.115                 ; Infreq
        fstp      DWORD PTR [396+esp]                           ;195.20
        movss     xmm2, DWORD PTR [_2il0floatpacket.1]          ;
        movss     xmm1, DWORD PTR [_2il0floatpacket.2]          ;
        movss     xmm0, DWORD PTR [_2il0floatpacket.3]          ;
        movss     xmm5, DWORD PTR [396+esp]                     ;195.20
        add       esp, 4                                        ;195.20
                                ; LOE xmm0 xmm1 xmm2 xmm5
.B1.116:                        ; Preds .B1.189                 ; Infreq
        mov       esi, DWORD PTR [460+esp]                      ;196.24
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+32] ;196.14
        mov       DWORD PTR [468+esp], esi                      ;196.24
        sub       esi, edi                                      ;196.20
        mov       DWORD PTR [332+esp], edi                      ;196.14
        mov       eax, DWORD PTR [464+esp]                      ;196.24
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+44] ;196.24
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+40] ;196.24
        lea       edi, DWORD PTR [esi*4]                        ;196.20
        shl       esi, 5                                        ;196.20
        sub       esi, edi                                      ;196.20
        mov       edi, eax                                      ;196.20
        sub       edi, edx                                      ;196.20
        imul      edi, ebx                                      ;196.20
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE] ;196.14
        add       edi, ecx                                      ;196.20
        mov       DWORD PTR [344+esp], edx                      ;196.24
        mov       DWORD PTR [328+esp], ecx                      ;196.14
        mov       DWORD PTR [340+esp], ebx                      ;196.24
        movss     xmm4, DWORD PTR [20+edi+esi]                  ;196.66
        cvttss2si edx, xmm4                                     ;196.66
        cvtsi2ss  xmm3, edx                                     ;196.66
        subss     xmm4, xmm3                                    ;196.65
        comiss    xmm4, xmm5                                    ;196.20
        jb        .B1.118       ; Prob 50%                      ;196.20
                                ; LOE eax edx esi edi xmm0 xmm1 xmm2
.B1.117:                        ; Preds .B1.116                 ; Infreq
        inc       edx                                           ;197.96
        cvtsi2ss  xmm3, edx                                     ;197.7
        movss     DWORD PTR [20+edi+esi], xmm3                  ;197.7
                                ; LOE eax xmm0 xmm1 xmm2
.B1.118:                        ; Preds .B1.117 .B1.116 .B1.114 ; Infreq
        inc       eax                                           ;201.10
        mov       DWORD PTR [352+esp], eax                      ;201.10
        cmp       eax, DWORD PTR [336+esp]                      ;201.10
        jle       .B1.112       ; Prob 82%                      ;201.10
                                ; LOE eax al ah xmm0 xmm1 xmm2
.B1.119:                        ; Preds .B1.118                 ; Infreq
        mov       edx, DWORD PTR [468+esp]                      ;
        mov       DWORD PTR [464+esp], eax                      ;265.5
        jmp       .B1.9         ; Prob 100%                     ;265.5
                                ; LOE edx
.B1.120:                        ; Preds .B1.88 .B1.92 .B1.90    ; Infreq
        xor       eax, eax                                      ;
        pxor      xmm3, xmm3                                    ;
        jmp       .B1.100       ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi xmm3
.B1.121:                        ; Preds .B1.112                 ; Infreq
        mov       ecx, DWORD PTR [376+esp]                      ;189.12
        sub       edx, esi                                      ;189.12
        sub       ecx, DWORD PTR [364+esp]                      ;189.12
        add       edx, ecx                                      ;189.12
        mov       esi, DWORD PTR [380+esp]                      ;189.128
        mov       edi, DWORD PTR [368+esp]                      ;189.12
        mov       edx, DWORD PTR [edx+esi*4]                    ;189.128
        imul      edx, DWORD PTR [372+esp]                      ;189.93
        imul      edx, DWORD PTR [348+esp]                      ;189.127
        cvtsi2ss  xmm4, edx                                     ;189.127
        divss     xmm4, xmm1                                    ;189.12
        movss     DWORD PTR [20+ebx+edi], xmm4                  ;189.12
        mov       ebx, DWORD PTR [356+esp]                      ;190.12
        mov       edx, DWORD PTR [360+esp]                      ;190.12
        movss     DWORD PTR [416+ebx+edx], xmm4                 ;190.12
        jmp       .B1.114       ; Prob 100%                     ;190.12
                                ; LOE eax xmm0 xmm1 xmm2 xmm4
.B1.123:                        ; Preds .B1.106                 ; Infreq
        mov       DWORD PTR [380+esp], ebx                      ;
        mov       eax, ebx                                      ;181.9
        test      eax, eax                                      ;185.18
        mov       DWORD PTR [_CAL_LINK_DISCHARGE_RATE$INDCAP.0.1], eax ;181.9
        jle       .B1.109       ; Prob 23%                      ;185.18
        jmp       .B1.110       ; Prob 100%                     ;185.18
                                ; LOE edx ecx
.B1.126:                        ; Preds .B1.87                  ; Infreq
        pxor      xmm3, xmm3                                    ;
        jmp       .B1.104       ; Prob 100%                     ;
                                ; LOE edx esi xmm3
.B1.127:                        ; Preds .B1.84                  ; Infreq
        mov       eax, DWORD PTR [228+esp]                      ;152.19
        mov       ecx, DWORD PTR [224+esp]                      ;152.19
        movsx     edi, BYTE PTR [32+eax+ecx]                    ;152.19
        mov       eax, 1                                        ;152.12
        test      edi, edi                                      ;152.12
        jle       .B1.57        ; Prob 2%                       ;152.12
                                ; LOE eax edx edi
.B1.128:                        ; Preds .B1.127                 ; Infreq
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE] ;153.15
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+40] ;153.57
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+32] ;153.15
        mov       DWORD PTR [256+esp], ebx                      ;153.15
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+44] ;153.57
        mov       DWORD PTR [264+esp], ecx                      ;153.15
        mov       ecx, 1                                        ;
        mov       DWORD PTR [252+esp], esi                      ;
        mov       DWORD PTR [248+esp], edi                      ;
        movss     xmm3, DWORD PTR [_2il0floatpacket.3]          ;
                                ; LOE eax edx ecx ebx xmm3
.B1.129:                        ; Preds .B1.133 .B1.128         ; Infreq
        mov       esi, edx                                      ;153.15
        sub       esi, DWORD PTR [264+esp]                      ;153.15
        lea       edi, DWORD PTR [esi*4]                        ;153.15
        shl       esi, 5                                        ;153.15
        sub       esi, edi                                      ;153.15
        mov       edi, ecx                                      ;153.15
        sub       edi, ebx                                      ;153.15
        imul      edi, DWORD PTR [252+esp]                      ;153.15
        add       edi, DWORD PTR [256+esp]                      ;153.15
        cvtsi2ss  xmm1, DWORD PTR [4+edi+esi]                   ;153.57
        mulss     xmm1, DWORD PTR [404+esp]                     ;153.15
        movss     DWORD PTR [20+edi+esi], xmm1                  ;153.15
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;154.56
        neg       esi                                           ;154.15
        add       esi, edx                                      ;154.15
        imul      edi, esi, 900                                 ;154.15
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;154.56
        movss     DWORD PTR [416+esi+edi], xmm1                 ;154.15
        cvttss2si esi, xmm1                                     ;156.57
        cvtsi2ss  xmm0, esi                                     ;156.57
        subss     xmm1, xmm0                                    ;156.56
        comiss    xmm1, xmm3                                    ;156.105
        jbe       .B1.133       ; Prob 78%                      ;156.105
                                ; LOE eax edx ecx ebx xmm3
.B1.130:                        ; Preds .B1.129                 ; Infreq
        mov       DWORD PTR [464+esp], eax                      ;265.5
        push      OFFSET FLAT: __NLITPACK_7.0.1                 ;158.19
        call      _RANXY                                        ;158.19
                                ; LOE f1
.B1.190:                        ; Preds .B1.130                 ; Infreq
        fstp      DWORD PTR [396+esp]                           ;158.19
        movss     xmm3, DWORD PTR [_2il0floatpacket.3]          ;
        movss     xmm2, DWORD PTR [396+esp]                     ;158.19
        add       esp, 4                                        ;158.19
                                ; LOE xmm2 xmm3
.B1.131:                        ; Preds .B1.190                 ; Infreq
        mov       edx, DWORD PTR [460+esp]                      ;160.24
        mov       edi, edx                                      ;160.20
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+32] ;160.14
        sub       edi, eax                                      ;160.20
        mov       DWORD PTR [264+esp], eax                      ;160.14
        mov       ecx, DWORD PTR [464+esp]                      ;160.24
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+40] ;160.24
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE] ;160.14
        lea       eax, DWORD PTR [edi*4]                        ;160.20
        shl       edi, 5                                        ;160.20
        sub       edi, eax                                      ;160.20
        mov       eax, ecx                                      ;160.20
        sub       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+44] ;160.20
        imul      eax, esi                                      ;160.20
        add       eax, ebx                                      ;160.20
        mov       DWORD PTR [252+esp], esi                      ;160.24
        mov       DWORD PTR [256+esp], ebx                      ;160.14
        movss     xmm1, DWORD PTR [20+eax+edi]                  ;160.66
        cvttss2si esi, xmm1                                     ;160.66
        cvtsi2ss  xmm0, esi                                     ;160.66
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+44] ;160.20
        subss     xmm1, xmm0                                    ;160.65
        comiss    xmm1, xmm2                                    ;160.20
        jb        .B1.133       ; Prob 50%                      ;160.20
                                ; LOE eax edx ecx ebx esi edi xmm3
.B1.132:                        ; Preds .B1.131                 ; Infreq
        inc       esi                                           ;161.96
        cvtsi2ss  xmm0, esi                                     ;161.7
        mov       esi, edi                                      ;161.7
        movss     DWORD PTR [20+eax+esi], xmm0                  ;161.7
                                ; LOE edx ecx ebx xmm3
.B1.133:                        ; Preds .B1.132 .B1.131 .B1.129 ; Infreq
        inc       ecx                                           ;165.12
        mov       eax, ecx                                      ;165.12
        cmp       ecx, DWORD PTR [248+esp]                      ;165.12
        jle       .B1.129       ; Prob 82%                      ;165.12
                                ; LOE eax edx ecx ebx xmm3
.B1.134:                        ; Preds .B1.133                 ; Infreq
        mov       DWORD PTR [464+esp], ecx                      ;265.5
        jmp       .B1.9         ; Prob 100%                     ;265.5
                                ; LOE edx
.B1.136:                        ; Preds .B1.79                  ; Infreq
        mov       DWORD PTR [204+esp], esi                      ;
        mov       ecx, esi                                      ;138.14
        mov       esi, DWORD PTR [196+esp]                      ;
        mov       edx, DWORD PTR [468+esp]                      ;
        mov       DWORD PTR [384+esp], ecx                      ;138.14
        jmp       .B1.82        ; Prob 100%                     ;138.14
                                ; LOE eax edx esi
.B1.137:                        ; Preds .B1.75                  ; Infreq
        lea       edx, DWORD PTR [460+esp]                      ;133.17
        lea       eax, DWORD PTR [404+esp]                      ;133.17
        push      OFFSET FLAT: __NLITPACK_1.0.1                 ;133.17
        push      OFFSET FLAT: __NLITPACK_3.0.1                 ;133.17
        push      DWORD PTR [8+ebp]                             ;133.17
        push      eax                                           ;133.17
        push      edx                                           ;133.17
        call      _SIGNAL_PERMISSIVE_LEFT_RATE                  ;133.17
        jmp       .B1.188       ; Prob 100%                     ;133.17
                                ; LOE
.B1.138:                        ; Preds .B1.6                   ; Infreq
        movsx     eax, BYTE PTR [32+eax+ecx]                    ;108.17
        test      eax, eax                                      ;108.10
        mov       DWORD PTR [452+esp], eax                      ;108.17
        mov       DWORD PTR [448+esp], 1                        ;108.10
        jle       .B1.57        ; Prob 2%                       ;108.10
                                ; LOE edx
.B1.139:                        ; Preds .B1.138                 ; Infreq
        mov       eax, DWORD PTR [488+esp]                      ;42.10
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE] ;111.13
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+44] ;111.55
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+40] ;111.55
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+32] ;111.13
        mov       DWORD PTR [460+esp], eax                      ;42.10
        mov       eax, 1                                        ;
        mov       DWORD PTR [408+esp], ecx                      ;
        mov       DWORD PTR [412+esp], ebx                      ;
        mov       DWORD PTR [416+esp], esi                      ;
        mov       DWORD PTR [420+esp], edi                      ;
        mov       DWORD PTR [468+esp], edx                      ;
        movss     xmm0, DWORD PTR [_2il0floatpacket.3]          ;
        movss     xmm1, DWORD PTR [_2il0floatpacket.2]          ;
        movss     xmm2, DWORD PTR [_2il0floatpacket.1]          ;
                                ; LOE eax xmm0 xmm1 xmm2
.B1.140:                        ; Preds .B1.146 .B1.139         ; Infreq
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;111.55
        neg       ecx                                           ;
        mov       edx, DWORD PTR [468+esp]                      ;
        add       ecx, edx                                      ;
        imul      esi, ecx, 900                                 ;
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;111.55
        mov       DWORD PTR [428+esp], esi                      ;
        mov       DWORD PTR [424+esp], ecx                      ;111.55
        mov       ebx, DWORD PTR [424+ecx+esi]                  ;111.93
        inc       ebx                                           ;111.93
        cmp       ebx, 4                                        ;111.129
        movsx     edi, BYTE PTR [668+ecx+esi]                   ;111.94
        jl        L2            ; Prob 50%                      ;111.129
        mov       ebx, 4                                        ;111.129
L2:                                                             ;
        mov       DWORD PTR [432+esp], ebx                      ;111.129
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_STOPCAP4W+44] ;111.129
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_STOPCAP4W+40] ;111.129
        imul      esi, ebx                                      ;
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_STOPCAP4W] ;111.93
        sub       ecx, esi                                      ;
        mov       esi, edx                                      ;
        sub       esi, DWORD PTR [408+esp]                      ;
        mov       DWORD PTR [440+esp], ebx                      ;111.129
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_STOPCAP4W+32] ;111.93
        shl       ebx, 2                                        ;
        mov       DWORD PTR [436+esp], ebx                      ;
        lea       ebx, DWORD PTR [esi*4]                        ;
        shl       esi, 5                                        ;
        sub       esi, ebx                                      ;
        mov       ebx, eax                                      ;
        sub       ebx, DWORD PTR [416+esp]                      ;
        imul      ebx, DWORD PTR [412+esp]                      ;
        add       ebx, DWORD PTR [420+esp]                      ;
        mov       DWORD PTR [444+esp], esi                      ;
        imul      edi, DWORD PTR [4+ebx+esi]                    ;111.93
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+44] ;110.53
        neg       esi                                           ;110.53
        mov       DWORD PTR [456+esp], edi                      ;111.93
        add       esi, eax                                      ;110.53
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+32] ;110.53
        neg       edi                                           ;110.53
        add       edi, edx                                      ;110.53
        imul      esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+40] ;110.53
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE] ;110.53
        lea       edx, DWORD PTR [5+edx+edi*8]                  ;110.53
        cmp       BYTE PTR [edx+esi], 1                         ;110.53
        mov       esi, DWORD PTR [456+esp]                      ;110.53
        je        .B1.148       ; Prob 16%                      ;110.53
                                ; LOE eax ecx ebx esi xmm0 xmm1 xmm2
.B1.141:                        ; Preds .B1.140                 ; Infreq
        mov       edi, DWORD PTR [440+esp]                      ;114.130
        mov       DWORD PTR [400+esp], eax                      ;
        mov       edx, DWORD PTR [436+esp]                      ;114.13
        lea       eax, DWORD PTR [edi+edi]                      ;114.130
        sub       eax, edx                                      ;114.13
        lea       edi, DWORD PTR [edi+edi*2]                    ;114.191
        add       eax, ecx                                      ;114.13
        sub       edi, edx                                      ;114.13
        mov       edx, DWORD PTR [432+esp]                      ;114.130
        add       ecx, edi                                      ;114.13
        mov       eax, DWORD PTR [eax+edx*4]                    ;114.130
        add       eax, DWORD PTR [ecx+edx*4]                    ;114.190
        imul      esi, eax                                      ;114.128
        cvtsi2ss  xmm4, esi                                     ;114.128
        mulss     xmm4, xmm2                                    ;114.252
        divss     xmm4, xmm1                                    ;114.13
        mov       ecx, DWORD PTR [444+esp]                      ;114.13
        mov       eax, DWORD PTR [400+esp]                      ;114.13
        movss     DWORD PTR [20+ebx+ecx], xmm4                  ;114.13
                                ; LOE eax xmm0 xmm1 xmm2 xmm4
.B1.142:                        ; Preds .B1.148 .B1.141         ; Infreq
        cvttss2si edx, xmm4                                     ;116.55
        cvtsi2ss  xmm3, edx                                     ;116.55
        subss     xmm4, xmm3                                    ;116.54
        comiss    xmm4, xmm0                                    ;116.103
        jbe       .B1.146       ; Prob 78%                      ;116.103
                                ; LOE eax xmm0 xmm1 xmm2
.B1.143:                        ; Preds .B1.142                 ; Infreq
        mov       eax, DWORD PTR [448+esp]                      ;265.5
        mov       DWORD PTR [464+esp], eax                      ;265.5
        push      OFFSET FLAT: __NLITPACK_6.0.1                 ;117.19
        call      _RANXY                                        ;117.19
                                ; LOE f1
.B1.192:                        ; Preds .B1.143                 ; Infreq
        fstp      DWORD PTR [396+esp]                           ;117.19
        movss     xmm2, DWORD PTR [_2il0floatpacket.1]          ;
        movss     xmm1, DWORD PTR [_2il0floatpacket.2]          ;
        movss     xmm0, DWORD PTR [_2il0floatpacket.3]          ;
        movss     xmm5, DWORD PTR [396+esp]                     ;117.19
        add       esp, 4                                        ;117.19
                                ; LOE xmm0 xmm1 xmm2 xmm5
.B1.144:                        ; Preds .B1.192                 ; Infreq
        mov       esi, DWORD PTR [460+esp]                      ;118.24
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+32] ;118.14
        mov       DWORD PTR [468+esp], esi                      ;118.24
        sub       esi, edi                                      ;118.20
        mov       DWORD PTR [408+esp], edi                      ;118.14
        mov       eax, DWORD PTR [464+esp]                      ;118.24
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+44] ;118.24
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+40] ;118.24
        lea       edi, DWORD PTR [esi*4]                        ;118.20
        shl       esi, 5                                        ;118.20
        sub       esi, edi                                      ;118.20
        mov       edi, eax                                      ;118.20
        sub       edi, edx                                      ;118.20
        imul      edi, ebx                                      ;118.20
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE] ;118.14
        add       edi, ecx                                      ;118.20
        mov       DWORD PTR [416+esp], edx                      ;118.24
        mov       DWORD PTR [420+esp], ecx                      ;118.14
        mov       DWORD PTR [412+esp], ebx                      ;118.24
        movss     xmm4, DWORD PTR [20+edi+esi]                  ;118.66
        cvttss2si edx, xmm4                                     ;118.66
        cvtsi2ss  xmm3, edx                                     ;118.66
        subss     xmm4, xmm3                                    ;118.65
        comiss    xmm4, xmm5                                    ;118.20
        jb        .B1.146       ; Prob 50%                      ;118.20
                                ; LOE eax edx esi edi xmm0 xmm1 xmm2
.B1.145:                        ; Preds .B1.144                 ; Infreq
        inc       edx                                           ;119.96
        cvtsi2ss  xmm3, edx                                     ;119.7
        movss     DWORD PTR [20+edi+esi], xmm3                  ;119.7
                                ; LOE eax xmm0 xmm1 xmm2
.B1.146:                        ; Preds .B1.145 .B1.144 .B1.142 ; Infreq
        inc       eax                                           ;123.10
        mov       DWORD PTR [448+esp], eax                      ;123.10
        cmp       eax, DWORD PTR [452+esp]                      ;123.10
        jle       .B1.140       ; Prob 82%                      ;123.10
                                ; LOE eax al ah xmm0 xmm1 xmm2
.B1.147:                        ; Preds .B1.146                 ; Infreq
        mov       edx, DWORD PTR [468+esp]                      ;
        mov       DWORD PTR [464+esp], eax                      ;265.5
        jmp       .B1.9         ; Prob 100%                     ;265.5
                                ; LOE edx
.B1.148:                        ; Preds .B1.140                 ; Infreq
        mov       edx, DWORD PTR [440+esp]                      ;111.13
        sub       edx, DWORD PTR [436+esp]                      ;111.13
        add       ecx, edx                                      ;111.13
        cvtsi2ss  xmm4, esi                                     ;111.93
        mov       esi, DWORD PTR [432+esp]                      ;111.129
        mov       edi, DWORD PTR [428+esp]                      ;112.13
        cvtsi2ss  xmm3, DWORD PTR [ecx+esi*4]                   ;111.129
        divss     xmm3, xmm1                                    ;111.189
        mulss     xmm4, xmm3                                    ;111.13
        mov       ecx, DWORD PTR [444+esp]                      ;111.13
        movss     DWORD PTR [20+ebx+ecx], xmm4                  ;111.13
        mov       ebx, DWORD PTR [424+esp]                      ;112.13
        movss     DWORD PTR [416+ebx+edi], xmm4                 ;112.13
        jmp       .B1.142       ; Prob 100%                     ;112.13
                                ; LOE eax xmm0 xmm1 xmm2 xmm4
.B1.150:                        ; Preds .B1.5                   ; Infreq
        mov       eax, DWORD PTR [488+esp]                      ;42.10
        mov       edx, OFFSET FLAT: __NLITPACK_1.0.1            ;90.14
        mov       DWORD PTR [460+esp], eax                      ;42.10
        lea       ebx, DWORD PTR [460+esp]                      ;90.14
        lea       ecx, DWORD PTR [404+esp]                      ;90.14
        push      edx                                           ;90.14
        push      edx                                           ;90.14
        push      DWORD PTR [8+ebp]                             ;90.14
        push      ecx                                           ;90.14
        push      ebx                                           ;90.14
        call      _SIGNAL_PERMISSIVE_LEFT_RATE                  ;90.14
                                ; LOE
.B1.193:                        ; Preds .B1.150                 ; Infreq
        add       esp, 20                                       ;90.14
                                ; LOE
.B1.151:                        ; Preds .B1.193                 ; Infreq
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;92.10
        neg       eax                                           ;92.10
        mov       edx, DWORD PTR [460+esp]                      ;92.17
        add       eax, edx                                      ;92.10
        imul      ebx, eax, 152                                 ;92.10
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;92.10
        movsx     eax, BYTE PTR [32+ecx+ebx]                    ;92.17
        mov       ebx, 1                                        ;92.10
        test      eax, eax                                      ;92.10
        jle       .B1.160       ; Prob 2%                       ;92.10
                                ; LOE eax edx ebx
.B1.152:                        ; Preds .B1.151                 ; Infreq
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+44] ;93.54
        mov       DWORD PTR [28+esp], esi                       ;93.54
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+32] ;93.12
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE] ;93.12
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+40] ;93.54
        mov       DWORD PTR [240+esp], esi                      ;
        mov       DWORD PTR [260+esp], ecx                      ;93.12
        mov       ecx, 1                                        ;
        mov       DWORD PTR [244+esp], edi                      ;
        mov       DWORD PTR [236+esp], eax                      ;
        movss     xmm3, DWORD PTR [_2il0floatpacket.3]          ;
        mov       esi, DWORD PTR [28+esp]                       ;
                                ; LOE edx ecx ebx esi xmm3
.B1.153:                        ; Preds .B1.157 .B1.152         ; Infreq
        mov       eax, edx                                      ;93.12
        sub       eax, DWORD PTR [240+esp]                      ;93.12
        lea       edi, DWORD PTR [eax*4]                        ;93.12
        shl       eax, 5                                        ;93.12
        sub       eax, edi                                      ;93.12
        mov       edi, ecx                                      ;93.12
        sub       edi, esi                                      ;93.12
        imul      edi, DWORD PTR [244+esp]                      ;93.12
        add       edi, DWORD PTR [260+esp]                      ;93.12
        cvtsi2ss  xmm1, DWORD PTR [4+edi+eax]                   ;93.54
        mulss     xmm1, DWORD PTR [404+esp]                     ;93.12
        movss     DWORD PTR [20+edi+eax], xmm1                  ;93.12
        cvttss2si eax, xmm1                                     ;95.55
        cvtsi2ss  xmm0, eax                                     ;95.55
        subss     xmm1, xmm0                                    ;95.54
        comiss    xmm1, xmm3                                    ;95.103
        jbe       .B1.157       ; Prob 78%                      ;95.103
                                ; LOE edx ecx ebx esi xmm3
.B1.154:                        ; Preds .B1.153                 ; Infreq
        mov       DWORD PTR [464+esp], ebx                      ;265.5
        push      OFFSET FLAT: __NLITPACK_5.0.1                 ;96.16
        call      _RANXY                                        ;96.16
                                ; LOE f1
.B1.194:                        ; Preds .B1.154                 ; Infreq
        fstp      DWORD PTR [396+esp]                           ;96.16
        movss     xmm3, DWORD PTR [_2il0floatpacket.3]          ;
        movss     xmm2, DWORD PTR [396+esp]                     ;96.16
        add       esp, 4                                        ;96.16
                                ; LOE xmm2 xmm3
.B1.155:                        ; Preds .B1.194                 ; Infreq
        mov       edx, DWORD PTR [460+esp]                      ;98.21
        mov       edi, edx                                      ;98.17
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+32] ;98.11
        sub       edi, eax                                      ;98.17
        mov       DWORD PTR [240+esp], eax                      ;98.11
        mov       ecx, DWORD PTR [464+esp]                      ;98.21
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+40] ;98.21
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE] ;98.11
        lea       eax, DWORD PTR [edi*4]                        ;98.17
        shl       edi, 5                                        ;98.17
        sub       edi, eax                                      ;98.17
        mov       eax, ecx                                      ;98.17
        sub       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+44] ;98.17
        imul      eax, esi                                      ;98.17
        add       eax, ebx                                      ;98.17
        mov       DWORD PTR [260+esp], ebx                      ;98.11
        mov       DWORD PTR [244+esp], esi                      ;98.21
        movss     xmm1, DWORD PTR [20+eax+edi]                  ;98.63
        cvttss2si ebx, xmm1                                     ;98.63
        cvtsi2ss  xmm0, ebx                                     ;98.63
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+44] ;98.17
        subss     xmm1, xmm0                                    ;98.62
        comiss    xmm1, xmm2                                    ;98.17
        jb        .B1.157       ; Prob 50%                      ;98.17
                                ; LOE eax edx ecx ebx esi edi xmm3
.B1.156:                        ; Preds .B1.155                 ; Infreq
        inc       ebx                                           ;98.202
        cvtsi2ss  xmm0, ebx                                     ;98.113
        mov       ebx, edi                                      ;98.113
        movss     DWORD PTR [20+eax+ebx], xmm0                  ;98.113
                                ; LOE edx ecx esi xmm3
.B1.157:                        ; Preds .B1.156 .B1.155 .B1.153 ; Infreq
        inc       ecx                                           ;100.9
        mov       ebx, ecx                                      ;100.9
        cmp       ecx, DWORD PTR [236+esp]                      ;100.9
        jle       .B1.153       ; Prob 82%                      ;100.9
                                ; LOE edx ecx ebx esi xmm3
.B1.158:                        ; Preds .B1.157                 ; Infreq
        mov       DWORD PTR [464+esp], ecx                      ;265.5
                                ; LOE edx
.B1.159:                        ; Preds .B1.158 .B1.160         ; Infreq
        mov       eax, edx                                      ;101.10
        sub       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;101.10
        imul      esi, eax, 900                                 ;101.10
        movss     xmm1, DWORD PTR [_2il0floatpacket.8]          ;101.10
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;101.10
        movsx     ecx, BYTE PTR [668+ebx+esi]                   ;101.90
        cvtsi2ss  xmm0, ecx                                     ;101.90
        movss     xmm2, DWORD PTR [440+ebx+esi]                 ;101.51
        divss     xmm2, xmm0                                    ;101.89
        mulss     xmm2, xmm1                                    ;101.10
        movss     DWORD PTR [416+ebx+esi], xmm2                 ;101.10
        jmp       .B1.9         ; Prob 100%                     ;101.10
                                ; LOE edx
.B1.160:                        ; Preds .B1.151                 ; Infreq
        mov       DWORD PTR [464+esp], 1                        ;265.5
        jmp       .B1.159       ; Prob 100%                     ;265.5
                                ; LOE edx
.B1.161:                        ; Preds .B1.4                   ; Infreq
        mov       ecx, DWORD PTR [488+esp]                      ;42.10
        sub       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;51.4
        mov       DWORD PTR [460+esp], ecx                      ;42.10
        imul      ecx, edx, 900                                 ;51.4
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;51.4
        mov       eax, DWORD PTR [388+ebx+ecx]                  ;51.8
        shl       eax, 2                                        ;51.4
        neg       eax                                           ;51.4
        mov       edx, DWORD PTR [356+ebx+ecx]                  ;51.8
        mov       esi, DWORD PTR [8+eax+edx]                    ;51.4
        test      esi, esi                                      ;55.16
        mov       eax, DWORD PTR [4+eax+edx]                    ;52.4
        jle       .B1.166       ; Prob 16%                      ;55.16
                                ; LOE eax ecx ebx esi
.B1.162:                        ; Preds .B1.161                 ; Infreq
        test      eax, eax                                      ;55.28
        jle       .B1.166       ; Prob 16%                      ;55.28
                                ; LOE eax ecx ebx esi
.B1.163:                        ; Preds .B1.162                 ; Infreq
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+44] ;57.9
        neg       edx                                           ;57.52
        add       edx, eax                                      ;57.52
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+32] ;57.6
        neg       eax                                           ;57.52
        add       eax, esi                                      ;57.52
        imul      edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+40] ;57.52
        lea       edi, DWORD PTR [eax*4]                        ;57.52
        shl       eax, 5                                        ;57.52
        sub       eax, edi                                      ;57.52
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE] ;57.6
        lea       eax, DWORD PTR [4+eax+edi]                    ;57.52
        cmp       DWORD PTR [eax+edx], 0                        ;57.52
        jle       .B1.165       ; Prob 16%                      ;57.52
                                ; LOE ecx ebx esi
.B1.164:                        ; Preds .B1.163                 ; Infreq
        mov       DWORD PTR [392+ebx+ecx], esi                  ;58.12
        jmp       .B1.166       ; Prob 100%                     ;58.12
                                ; LOE
.B1.165:                        ; Preds .B1.163                 ; Infreq
        mov       DWORD PTR [392+ebx+ecx], 0                    ;58.12
                                ; LOE
.B1.166:                        ; Preds .B1.162 .B1.161 .B1.164 .B1.165 ; Infreq
        lea       esi, DWORD PTR [460+esp]                      ;63.15
        lea       ebx, DWORD PTR [404+esp]                      ;63.15
        push      OFFSET FLAT: __NLITPACK_1.0.1                 ;63.15
        push      OFFSET FLAT: __NLITPACK_0.0.1                 ;63.15
        push      DWORD PTR [8+ebp]                             ;63.15
        push      ebx                                           ;63.15
        push      esi                                           ;63.15
        call      _SIGNAL_PERMISSIVE_LEFT_RATE                  ;63.15
                                ; LOE ebx esi
.B1.195:                        ; Preds .B1.166                 ; Infreq
        add       esp, 20                                       ;63.15
                                ; LOE ebx esi
.B1.167:                        ; Preds .B1.195                 ; Infreq
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;65.10
        neg       ecx                                           ;65.52
        add       ecx, DWORD PTR [460+esp]                      ;65.52
        imul      ecx, ecx, 152                                 ;65.52
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;65.10
        mov       eax, DWORD PTR [100+edi+ecx]                  ;65.13
        mov       edx, DWORD PTR [104+edi+ecx]                  ;65.13
        imul      edx, eax                                      ;65.52
        mov       edi, DWORD PTR [72+edi+ecx]                   ;65.13
        sub       edi, edx                                      ;65.52
        cmp       DWORD PTR [eax+edi], 0                        ;65.52
        jle       .B1.181       ; Prob 16%                      ;65.52
                                ; LOE ebx esi
.B1.168:                        ; Preds .B1.167                 ; Infreq
        lea       eax, DWORD PTR [464+esp]                      ;66.17
        push      eax                                           ;66.17
        push      OFFSET FLAT: __NLITPACK_2.0.1                 ;66.17
        push      DWORD PTR [8+ebp]                             ;66.17
        push      ebx                                           ;66.17
        push      esi                                           ;66.17
        call      _SIGNAL_PERMISSIVE_LEFT_RATE                  ;66.17
                                ; LOE
.B1.196:                        ; Preds .B1.181 .B1.168         ; Infreq
        add       esp, 20                                       ;66.17
                                ; LOE
.B1.169:                        ; Preds .B1.196                 ; Infreq
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;71.10
        neg       eax                                           ;71.10
        mov       edx, DWORD PTR [460+esp]                      ;71.17
        add       eax, edx                                      ;71.10
        imul      ebx, eax, 152                                 ;71.10
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;71.10
        mov       DWORD PTR [464+esp], 1                        ;71.10
        movsx     eax, BYTE PTR [32+ecx+ebx]                    ;71.17
        test      eax, eax                                      ;71.10
        jle       .B1.9         ; Prob 2%                       ;71.10
                                ; LOE eax edx
.B1.170:                        ; Preds .B1.169                 ; Infreq
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+32] ;74.15
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE] ;74.15
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+40] ;74.57
        mov       DWORD PTR [232+esp], ebx                      ;74.15
        mov       ebx, 1                                        ;
        mov       DWORD PTR [472+esp], eax                      ;
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+44] ;74.57
        mov       DWORD PTR [480+esp], edi                      ;
        mov       DWORD PTR [476+esp], ecx                      ;
        mov       DWORD PTR [468+esp], edx                      ;
        mov       eax, DWORD PTR [232+esp]                      ;
                                ; LOE eax ebx esi
.B1.171:                        ; Preds .B1.177 .B1.170         ; Infreq
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+44] ;73.16
        neg       ecx                                           ;73.55
        add       ecx, ebx                                      ;73.55
        imul      ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+40] ;73.55
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE] ;73.13
        mov       edx, DWORD PTR [468+esp]                      ;73.55
        sub       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+32] ;73.55
        lea       ecx, DWORD PTR [5+ecx+edi]                    ;73.55
        movsx     ecx, BYTE PTR [ecx+edx*8]                     ;73.16
        cmp       ecx, 1                                        ;73.55
        je        .B1.179       ; Prob 16%                      ;73.55
                                ; LOE eax ecx ebx esi
.B1.172:                        ; Preds .B1.171                 ; Infreq
        neg       esi                                           ;
        neg       eax                                           ;
        add       esi, ebx                                      ;
        add       eax, DWORD PTR [468+esp]                      ;
        imul      esi, DWORD PTR [480+esp]                      ;
        add       esi, DWORD PTR [476+esp]                      ;
        lea       edx, DWORD PTR [eax*4]                        ;
        shl       eax, 5                                        ;
        sub       eax, edx                                      ;
        cmp       ecx, 3                                        ;75.59
        cvtsi2ss  xmm1, DWORD PTR [4+esi+eax]                   ;76.57
        je        .B1.180       ; Prob 16%                      ;75.59
                                ; LOE eax esi xmm1
.B1.173:                        ; Preds .B1.172                 ; Infreq
        movss     xmm0, DWORD PTR [404+esp]                     ;78.57
        mulss     xmm0, xmm1                                    ;78.15
        movss     DWORD PTR [20+esi+eax], xmm0                  ;78.15
                                ; LOE
.B1.174:                        ; Preds .B1.179 .B1.180 .B1.173 ; Infreq
        push      OFFSET FLAT: __NLITPACK_4.0.1                 ;80.19
        call      _RANXY                                        ;80.19
                                ; LOE f1
.B1.197:                        ; Preds .B1.174                 ; Infreq
        fstp      DWORD PTR [396+esp]                           ;80.19
        movss     xmm2, DWORD PTR [396+esp]                     ;80.19
        add       esp, 4                                        ;80.19
                                ; LOE xmm2
.B1.175:                        ; Preds .B1.197                 ; Infreq
        mov       edx, DWORD PTR [460+esp]                      ;81.24
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+32] ;81.14
        mov       DWORD PTR [468+esp], edx                      ;81.24
        sub       edx, eax                                      ;81.20
        mov       ebx, DWORD PTR [464+esp]                      ;81.24
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+40] ;81.24
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE] ;81.14
        mov       DWORD PTR [480+esp], edi                      ;81.24
        lea       ecx, DWORD PTR [edx*4]                        ;81.20
        shl       edx, 5                                        ;81.20
        sub       edx, ecx                                      ;81.20
        mov       ecx, ebx                                      ;81.20
        sub       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+44] ;81.20
        imul      ecx, edi                                      ;81.20
        add       ecx, esi                                      ;81.20
        mov       DWORD PTR [476+esp], esi                      ;81.14
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+44] ;81.20
        movss     xmm1, DWORD PTR [20+ecx+edx]                  ;81.66
        cvttss2si edi, xmm1                                     ;81.66
        cvtsi2ss  xmm0, edi                                     ;81.66
        subss     xmm1, xmm0                                    ;81.65
        comiss    xmm1, xmm2                                    ;81.20
        jb        .B1.177       ; Prob 50%                      ;81.20
                                ; LOE eax edx ecx ebx esi edi
.B1.176:                        ; Preds .B1.175                 ; Infreq
        inc       edi                                           ;82.100
        cvtsi2ss  xmm0, edi                                     ;82.11
        movss     DWORD PTR [20+ecx+edx], xmm0                  ;82.11
                                ; LOE eax ebx esi
.B1.177:                        ; Preds .B1.175 .B1.176         ; Infreq
        inc       ebx                                           ;84.4
        mov       DWORD PTR [464+esp], ebx                      ;84.4
        cmp       ebx, DWORD PTR [472+esp]                      ;84.4
        jle       .B1.171       ; Prob 82%                      ;84.4
                                ; LOE eax ebx esi
.B1.178:                        ; Preds .B1.177                 ; Infreq
        mov       edx, DWORD PTR [468+esp]                      ;
        jmp       .B1.9         ; Prob 100%                     ;
                                ; LOE edx
.B1.179:                        ; Preds .B1.171                 ; Infreq
        neg       eax                                           ;74.15
        neg       esi                                           ;74.15
        mov       edi, DWORD PTR [468+esp]                      ;74.15
        add       eax, edi                                      ;74.15
        add       esi, ebx                                      ;74.15
        imul      esi, DWORD PTR [480+esp]                      ;74.15
        add       esi, DWORD PTR [476+esp]                      ;74.15
        lea       ecx, DWORD PTR [eax*4]                        ;74.15
        shl       eax, 5                                        ;74.15
        sub       eax, ecx                                      ;74.15
        sub       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;74.15
        imul      ebx, edi, 900                                 ;74.15
        cvtsi2ss  xmm1, DWORD PTR [4+esi+eax]                   ;74.57
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;74.57
        movsx     ecx, BYTE PTR [44+edx+ebx]                    ;74.137
        test      ecx, ecx                                      ;74.15
        mulss     xmm1, DWORD PTR [416+edx+ebx]                 ;74.95
        mov       edx, 1                                        ;74.15
        cmovle    ecx, edx                                      ;74.15
        cvtsi2ss  xmm0, ecx                                     ;74.137
        mulss     xmm1, xmm0                                    ;74.15
        movss     DWORD PTR [20+esi+eax], xmm1                  ;74.15
        jmp       .B1.174       ; Prob 100%                     ;74.15
                                ; LOE
.B1.180:                        ; Preds .B1.172                 ; Infreq
        mov       edx, DWORD PTR [468+esp]                      ;76.15
        mov       edi, 1                                        ;76.15
        sub       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;76.15
        imul      ebx, edx, 900                                 ;76.15
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;76.57
        movsx     edx, BYTE PTR [45+ecx+ebx]                    ;76.138
        test      edx, edx                                      ;76.15
        mulss     xmm1, DWORD PTR [420+ecx+ebx]                 ;76.95
        cmovle    edx, edi                                      ;76.15
        cvtsi2ss  xmm0, edx                                     ;76.138
        mulss     xmm1, xmm0                                    ;76.15
        movss     DWORD PTR [20+esi+eax], xmm1                  ;76.15
        jmp       .B1.174       ; Prob 100%                     ;76.15
                                ; LOE
.B1.181:                        ; Preds .B1.167                 ; Infreq
        push      OFFSET FLAT: __NLITPACK_1.0.1                 ;68.17
        push      OFFSET FLAT: __NLITPACK_3.0.1                 ;68.17
        push      DWORD PTR [8+ebp]                             ;68.17
        push      ebx                                           ;68.17
        push      esi                                           ;68.17
        call      _SIGNAL_PERMISSIVE_LEFT_RATE                  ;68.17
        jmp       .B1.196       ; Prob 100%                     ;68.17
        ALIGN     16
                                ; LOE
; mark_end;
_CAL_LINK_DISCHARGE_RATE ENDP
_TEXT	ENDS
_BSS	SEGMENT  DWORD PUBLIC FLAT  'BSS'
	ALIGN 004H
_CAL_LINK_DISCHARGE_RATE$INDCAP.0.1	DD 1 DUP (0H)	; pad
_BSS	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
__NLITPACK_1.0.1	DD	0
__NLITPACK_3.0.1	DD	4
__NLITPACK_2.0.1	DD	1
__NLITPACK_10.0.1	DD	11
__NLITPACK_9.0.1	DD	10
__NLITPACK_8.0.1	DD	9
__NLITPACK_7.0.1	DD	8
__NLITPACK_6.0.1	DD	7
__NLITPACK_5.0.1	DD	6
__NLITPACK_0.0.1	DD	2
__NLITPACK_4.0.1	DD	5
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _CAL_LINK_DISCHARGE_RATE
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DD 1 DUP (0H)	; pad
_2il0floatpacket.4	DD	042700000H,042700000H,042700000H,042700000H
_2il0floatpacket.5	DD	000000384H,000000384H,000000384H,000000384H
_2il0floatpacket.6	DD	0ffffffffH,000000000H,0ffffffffH,000000000H
_2il0floatpacket.1	DD	03f000000H
_2il0floatpacket.2	DD	045610000H
_2il0floatpacket.3	DD	038d1b717H
_2il0floatpacket.7	DD	042700000H
_2il0floatpacket.8	DD	040c00000H
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
EXTRN	_DYNUST_NETWORK_MODULE_mp_YIELDCAP:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_YIELDCAPIND:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_STOPCAP2W:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_STOPCAP2WIND:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_LEVEL2N:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_SIGNAPPRH:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_SIGNDATA:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_SIGNCOUNT:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_STOPCAP4W:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NOOFARCS:BYTE
_DATA	ENDS
EXTRN	_SIGNAL_PERMISSIVE_LEFT_RATE:PROC
EXTRN	_RANXY:PROC
EXTRN	__fltused:BYTE
	INCLUDELIB <ifconsol>
	INCLUDELIB <libifcoremt>
	INCLUDELIB <libifport>
	INCLUDELIB <libmmt>
	INCLUDELIB <LIBCMT>
	INCLUDELIB <libirc>
	INCLUDELIB <svml_dispmt>
	INCLUDELIB <OLDNAMES>
	END
