; mark_description "Intel(R) Visual Fortran Compiler XE for applications running on IA-32, Version 13.1.1.171 Build 20130313";
; mark_description "-S";
	.686P
 	.387
	OPTION DOTNAME
	ASSUME	CS:FLAT,DS:FLAT,SS:FLAT
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
TXTST0:
; -- Begin  _DYNUST_TRANSIT_MODULE$
; mark_begin;
IF @Version GE 800
  .MMX
ELSEIF @Version GE 612
  .MMX
  MMWORD TEXTEQU <QWORD>
ENDIF
IF @Version GE 800
  .XMM
ELSEIF @Version GE 614
  .XMM
  XMMWORD TEXTEQU <OWORD>
ENDIF
       ALIGN     16
	PUBLIC _DYNUST_TRANSIT_MODULE$
_DYNUST_TRANSIT_MODULE$	PROC NEAR 
.B1.1:                          ; Preds .B1.0
        ret                                                     ;1.8
        ALIGN     16
                                ; LOE
; mark_end;
_DYNUST_TRANSIT_MODULE$ ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_TRANSIT_MODULE$
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_TRANSIT_MODULE_mp_INPUTTRANSIT
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_TRANSIT_MODULE_mp_INPUTTRANSIT
_DYNUST_TRANSIT_MODULE_mp_INPUTTRANSIT	PROC NEAR 
.B2.1:                          ; Preds .B2.0
        push      ebp                                           ;77.12
        mov       ebp, esp                                      ;77.12
        and       esp, -16                                      ;77.12
        push      esi                                           ;77.12
        push      edi                                           ;77.12
        push      ebx                                           ;77.12
        sub       esp, 868                                      ;77.12
        mov       DWORD PTR [592+esp], 0                        ;84.10
        lea       eax, DWORD PTR [264+esp]                      ;84.10
        mov       DWORD PTR [144+esp], 24                       ;84.10
        lea       ecx, DWORD PTR [592+esp]                      ;84.10
        mov       DWORD PTR [148+esp], OFFSET FLAT: __STRLITPACK_69 ;84.10
        lea       edx, DWORD PTR [144+esp]                      ;84.10
        mov       DWORD PTR [152+esp], eax                      ;84.10
        push      32                                            ;84.10
        push      edx                                           ;84.10
        push      OFFSET FLAT: __STRLITPACK_70.0.2              ;84.10
        push      -2088435968                                   ;84.10
        push      -1                                            ;84.10
        push      ecx                                           ;84.10
        call      _for_inquire                                  ;84.10
                                ; LOE
.B2.274:                        ; Preds .B2.1
        add       esp, 24                                       ;84.10
                                ; LOE
.B2.2:                          ; Preds .B2.274
        test      BYTE PTR [264+esp], 1                         ;86.4
        je        .B2.219       ; Prob 60%                      ;86.4
                                ; LOE
.B2.3:                          ; Preds .B2.2
        xor       eax, eax                                      ;87.7
        mov       DWORD PTR [592+esp], eax                      ;87.7
        push      32                                            ;87.7
        push      eax                                           ;87.7
        push      OFFSET FLAT: __STRLITPACK_71.0.2              ;87.7
        push      -2088435968                                   ;87.7
        push      1                                             ;87.7
        lea       edx, DWORD PTR [612+esp]                      ;87.7
        push      edx                                           ;87.7
        call      _for_close                                    ;87.7
                                ; LOE
.B2.4:                          ; Preds .B2.3
        mov       DWORD PTR [360+esp], 0                        ;88.6
        lea       esi, DWORD PTR [360+esp]                      ;88.6
        mov       DWORD PTR [88+esp], 24                        ;88.6
        lea       eax, DWORD PTR [88+esp]                       ;88.6
        mov       DWORD PTR [92+esp], OFFSET FLAT: __STRLITPACK_16 ;88.6
        mov       DWORD PTR [96+esp], 7                         ;88.6
        mov       DWORD PTR [100+esp], OFFSET FLAT: __STRLITPACK_15 ;88.6
        push      32                                            ;88.6
        push      eax                                           ;88.6
        push      OFFSET FLAT: __STRLITPACK_145.0.7             ;88.6
        push      -2088435968                                   ;88.6
        push      1                                             ;88.6
        push      esi                                           ;88.6
        call      _for_open                                     ;88.6
                                ; LOE esi
.B2.275:                        ; Preds .B2.4
        add       esp, 48                                       ;88.6
                                ; LOE esi
.B2.5:                          ; Preds .B2.275
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], 0   ;88.6
        jne       .B2.270       ; Prob 5%                       ;88.6
                                ; LOE esi
.B2.6:                          ; Preds .B2.334 .B2.5
        mov       edi, 1                                        ;88.6
        xor       ebx, ebx                                      ;88.6
                                ; LOE ebx esi edi
.B2.7:                          ; Preds .B2.8 .B2.6
        mov       DWORD PTR [336+esp], ebx                      ;88.6
        push      32                                            ;88.6
        push      ebx                                           ;88.6
        push      OFFSET FLAT: __STRLITPACK_148.0.7             ;88.6
        push      -2088435968                                   ;88.6
        push      1                                             ;88.6
        push      esi                                           ;88.6
        call      _for_read_seq_lis                             ;88.6
                                ; LOE ebx esi edi
.B2.276:                        ; Preds .B2.7
        add       esp, 24                                       ;88.6
                                ; LOE ebx esi edi
.B2.8:                          ; Preds .B2.276
        inc       edi                                           ;88.6
        cmp       edi, 14                                       ;88.6
        jle       .B2.7         ; Prob 92%                      ;88.6
                                ; LOE ebx esi edi
.B2.9:                          ; Preds .B2.8
        mov       DWORD PTR [336+esp], 0                        ;88.6
        lea       eax, DWORD PTR [160+esp]                      ;88.6
        mov       DWORD PTR [160+esp], OFFSET FLAT: _DYNUST_TRANSIT_MODULE_mp_SIMSTARTTIME ;88.6
        push      32                                            ;88.6
        push      eax                                           ;88.6
        push      OFFSET FLAT: __STRLITPACK_149.0.7             ;88.6
        push      -2088435968                                   ;88.6
        push      1                                             ;88.6
        push      esi                                           ;88.6
        call      _for_read_seq_lis                             ;88.6
                                ; LOE esi
.B2.10:                         ; Preds .B2.9
        xor       eax, eax                                      ;88.6
        lea       edx, DWORD PTR [120+esp]                      ;88.6
        mov       DWORD PTR [120+esp], eax                      ;88.6
        push      eax                                           ;88.6
        push      1                                             ;88.6
        push      edx                                           ;88.6
        call      _for_eof                                      ;88.6
                                ; LOE eax esi
.B2.277:                        ; Preds .B2.10
        add       esp, 36                                       ;88.6
        mov       edx, eax                                      ;88.6
                                ; LOE edx esi
.B2.11:                         ; Preds .B2.277
        xor       eax, eax                                      ;
        xor       edi, edi                                      ;88.6
        test      dl, 1                                         ;88.6
        jne       .B2.30        ; Prob 6%                       ;88.6
                                ; LOE esi edi
.B2.12:                         ; Preds .B2.11
        xor       ebx, ebx                                      ;88.6
        lea       ecx, DWORD PTR [396+esp]                      ;88.6
                                ; LOE ebx esi edi
.B2.13:                         ; Preds .B2.12 .B2.28
        mov       DWORD PTR [336+esp], ebx                      ;88.6
        inc       edi                                           ;88.6
        push      32                                            ;88.6
        push      ebx                                           ;88.6
        push      OFFSET FLAT: __STRLITPACK_150.0.7             ;88.6
        push      -2088435968                                   ;88.6
        push      1                                             ;88.6
        push      esi                                           ;88.6
        call      _for_read_seq_lis                             ;88.6
                                ; LOE ebx esi edi
.B2.14:                         ; Preds .B2.13
        mov       DWORD PTR [360+esp], ebx                      ;88.6
        push      32                                            ;88.6
        push      ebx                                           ;88.6
        push      OFFSET FLAT: __STRLITPACK_151.0.7             ;88.6
        push      -2088435968                                   ;88.6
        push      1                                             ;88.6
        push      esi                                           ;88.6
        call      _for_read_seq_lis                             ;88.6
                                ; LOE ebx esi edi
.B2.15:                         ; Preds .B2.14
        mov       DWORD PTR [384+esp], ebx                      ;88.6
        push      32                                            ;88.6
        push      ebx                                           ;88.6
        push      OFFSET FLAT: __STRLITPACK_152.0.7             ;88.6
        push      -2088435968                                   ;88.6
        push      1                                             ;88.6
        push      esi                                           ;88.6
        call      _for_read_seq_lis                             ;88.6
                                ; LOE ebx esi edi
.B2.16:                         ; Preds .B2.15
        mov       DWORD PTR [408+esp], ebx                      ;88.6
        push      32                                            ;88.6
        push      ebx                                           ;88.6
        push      OFFSET FLAT: __STRLITPACK_153.0.7             ;88.6
        push      -2088435968                                   ;88.6
        push      1                                             ;88.6
        push      esi                                           ;88.6
        call      _for_read_seq_lis                             ;88.6
                                ; LOE ebx esi edi
.B2.17:                         ; Preds .B2.16
        mov       DWORD PTR [432+esp], 0                        ;88.6
        lea       eax, DWORD PTR [484+esp]                      ;88.6
        mov       DWORD PTR [512+esp], eax                      ;88.6
        push      32                                            ;88.6
        lea       edx, DWORD PTR [516+esp]                      ;88.6
        push      edx                                           ;88.6
        push      OFFSET FLAT: __STRLITPACK_154.0.7             ;88.6
        push      -2088435968                                   ;88.6
        push      1                                             ;88.6
        push      esi                                           ;88.6
        call      _for_read_seq_lis                             ;88.6
                                ; LOE ebx esi edi
.B2.278:                        ; Preds .B2.17
        add       esp, 120                                      ;88.6
                                ; LOE ebx esi edi
.B2.18:                         ; Preds .B2.278
        lea       edx, DWORD PTR [424+esp]                      ;88.6
        lea       eax, DWORD PTR [396+esp]                      ;88.6
        mov       DWORD PTR [424+esp], eax                      ;88.6
        push      edx                                           ;88.6
        push      OFFSET FLAT: __STRLITPACK_155.0.7             ;88.6
        push      esi                                           ;88.6
        call      _for_read_seq_lis_xmit                        ;88.6
                                ; LOE ebx esi edi
.B2.279:                        ; Preds .B2.18
        add       esp, 12                                       ;88.6
                                ; LOE ebx esi edi
.B2.19:                         ; Preds .B2.279
        mov       edx, DWORD PTR [388+esp]                      ;88.6
        test      edx, edx                                      ;88.6
        jle       .B2.27        ; Prob 2%                       ;88.6
                                ; LOE edx ebx esi edi
.B2.20:                         ; Preds .B2.19
        mov       eax, 1                                        ;
        mov       DWORD PTR [12+esp], edx                       ;
        mov       DWORD PTR [8+esp], edi                        ;
        mov       edi, eax                                      ;
                                ; LOE ebx esi edi
.B2.21:                         ; Preds .B2.25 .B2.20
        mov       DWORD PTR [336+esp], ebx                      ;88.6
        push      32                                            ;88.6
        push      ebx                                           ;88.6
        push      OFFSET FLAT: __STRLITPACK_156.0.7             ;88.6
        push      -2088435968                                   ;88.6
        push      1                                             ;88.6
        push      esi                                           ;88.6
        call      _for_read_seq_lis                             ;88.6
                                ; LOE ebx esi edi
.B2.22:                         ; Preds .B2.21
        mov       DWORD PTR [360+esp], ebx                      ;88.6
        push      32                                            ;88.6
        push      ebx                                           ;88.6
        push      OFFSET FLAT: __STRLITPACK_157.0.7             ;88.6
        push      -2088435968                                   ;88.6
        push      1                                             ;88.6
        push      esi                                           ;88.6
        call      _for_read_seq_lis                             ;88.6
                                ; LOE ebx esi edi
.B2.23:                         ; Preds .B2.22
        mov       DWORD PTR [384+esp], ebx                      ;88.6
        push      32                                            ;88.6
        push      ebx                                           ;88.6
        push      OFFSET FLAT: __STRLITPACK_158.0.7             ;88.6
        push      -2088435968                                   ;88.6
        push      1                                             ;88.6
        push      esi                                           ;88.6
        call      _for_read_seq_lis                             ;88.6
                                ; LOE ebx esi edi
.B2.24:                         ; Preds .B2.23
        mov       DWORD PTR [408+esp], ebx                      ;88.6
        push      32                                            ;88.6
        push      ebx                                           ;88.6
        push      OFFSET FLAT: __STRLITPACK_159.0.7             ;88.6
        push      -2088435968                                   ;88.6
        push      1                                             ;88.6
        push      esi                                           ;88.6
        call      _for_read_seq_lis                             ;88.6
                                ; LOE ebx esi edi
.B2.280:                        ; Preds .B2.24
        add       esp, 96                                       ;88.6
                                ; LOE ebx esi edi
.B2.25:                         ; Preds .B2.280
        inc       edi                                           ;88.6
        cmp       edi, DWORD PTR [12+esp]                       ;88.6
        jle       .B2.21        ; Prob 82%                      ;88.6
                                ; LOE ebx esi edi
.B2.26:                         ; Preds .B2.25
        mov       edi, DWORD PTR [8+esp]                        ;
                                ; LOE ebx esi edi
.B2.27:                         ; Preds .B2.19 .B2.26
        mov       DWORD PTR [96+esp], ebx                       ;88.6
        push      ebx                                           ;88.6
        push      1                                             ;88.6
        lea       eax, DWORD PTR [104+esp]                      ;88.6
        push      eax                                           ;88.6
        call      _for_eof                                      ;88.6
                                ; LOE eax ebx esi edi
.B2.281:                        ; Preds .B2.27
        add       esp, 12                                       ;88.6
                                ; LOE eax ebx esi edi
.B2.28:                         ; Preds .B2.281
        test      al, 1                                         ;88.6
        je        .B2.13        ; Prob 82%                      ;88.6
                                ; LOE ebx esi edi
.B2.30:                         ; Preds .B2.28 .B2.11
        xor       eax, eax                                      ;88.6
        mov       DWORD PTR [336+esp], eax                      ;88.6
        push      32                                            ;88.6
        push      eax                                           ;88.6
        push      OFFSET FLAT: __STRLITPACK_160.0.7             ;88.6
        push      -2088435968                                   ;88.6
        push      1                                             ;88.6
        push      esi                                           ;88.6
        mov       DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_NUMROUTES], edi ;88.6
        call      _for_close                                    ;88.6
                                ; LOE
.B2.31:                         ; Preds .B2.30
        mov       ecx, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_NUMROUTES] ;89.1
        xor       eax, eax                                      ;89.1
        test      ecx, ecx                                      ;89.1
        cmovle    ecx, eax                                      ;89.1
        mov       edx, 176                                      ;89.1
        lea       ebx, DWORD PTR [84+esp]                       ;89.1
        push      edx                                           ;89.1
        push      ecx                                           ;89.1
        push      2                                             ;89.1
        push      ebx                                           ;89.1
        mov       esi, 1                                        ;89.1
        mov       DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE+12], 133 ;89.1
        mov       DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE+4], edx ;89.1
        mov       DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE+16], esi ;89.1
        mov       DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE+8], eax ;89.1
        mov       DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE+32], esi ;89.1
        mov       DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE+24], ecx ;89.1
        mov       DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE+28], edx ;89.1
        call      _for_check_mult_overflow                      ;89.1
                                ; LOE eax
.B2.32:                         ; Preds .B2.31
        mov       edx, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE+12] ;89.1
        and       eax, 1                                        ;89.1
        and       edx, 1                                        ;89.1
        shl       eax, 4                                        ;89.1
        add       edx, edx                                      ;89.1
        or        edx, eax                                      ;89.1
        or        edx, 262144                                   ;89.1
        push      edx                                           ;89.1
        push      OFFSET FLAT: _DYNUST_TRANSIT_MODULE_mp_M_ROUTE ;89.1
        push      DWORD PTR [108+esp]                           ;89.1
        call      _for_alloc_allocatable                        ;89.1
                                ; LOE
.B2.33:                         ; Preds .B2.32
        mov       ecx, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_NUMROUTES] ;90.1
        xor       eax, eax                                      ;90.1
        test      ecx, ecx                                      ;90.1
        cmovle    ecx, eax                                      ;90.1
        mov       edx, 44                                       ;90.1
        lea       ebx, DWORD PTR [208+esp]                      ;90.1
        push      edx                                           ;90.1
        push      ecx                                           ;90.1
        push      2                                             ;90.1
        push      ebx                                           ;90.1
        mov       esi, 1                                        ;90.1
        mov       DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_SCHEDULE+12], 133 ;90.1
        mov       DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_SCHEDULE+4], edx ;90.1
        mov       DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_SCHEDULE+16], esi ;90.1
        mov       DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_SCHEDULE+8], eax ;90.1
        mov       DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_SCHEDULE+32], esi ;90.1
        mov       DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_SCHEDULE+24], ecx ;90.1
        mov       DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_SCHEDULE+28], edx ;90.1
        call      _for_check_mult_overflow                      ;90.1
                                ; LOE eax
.B2.34:                         ; Preds .B2.33
        mov       edx, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_SCHEDULE+12] ;90.1
        and       eax, 1                                        ;90.1
        and       edx, 1                                        ;90.1
        shl       eax, 4                                        ;90.1
        add       edx, edx                                      ;90.1
        or        edx, eax                                      ;90.1
        or        edx, 262144                                   ;90.1
        push      edx                                           ;90.1
        push      OFFSET FLAT: _DYNUST_TRANSIT_MODULE_mp_M_SCHEDULE ;90.1
        push      DWORD PTR [232+esp]                           ;90.1
        call      _for_alloc_allocatable                        ;90.1
                                ; LOE
.B2.284:                        ; Preds .B2.34
        add       esp, 80                                       ;90.1
                                ; LOE
.B2.35:                         ; Preds .B2.284
        xor       ecx, ecx                                      ;93.1
        movdqa    xmm0, XMMWORD PTR [_2il0floatpacket.2]        ;93.1
        xor       edx, edx                                      ;
        xor       eax, eax                                      ;
                                ; LOE eax edx ecx xmm0
.B2.36:                         ; Preds .B2.40 .B2.35
        mov       ebx, eax                                      ;93.1
                                ; LOE eax edx ecx ebx xmm0
.B2.37:                         ; Preds .B2.37 .B2.36
        movdqa    XMMWORD PTR [_DYNUST_TRANSIT_MODULE_mp_TRANSITREPOS+edx+ebx*4], xmm0 ;93.1
        movdqa    XMMWORD PTR [_DYNUST_TRANSIT_MODULE_mp_TRANSITREPOS+16+edx+ebx*4], xmm0 ;93.1
        movdqa    XMMWORD PTR [_DYNUST_TRANSIT_MODULE_mp_TRANSITREPOS+32+edx+ebx*4], xmm0 ;93.1
        movdqa    XMMWORD PTR [_DYNUST_TRANSIT_MODULE_mp_TRANSITREPOS+48+edx+ebx*4], xmm0 ;93.1
        add       ebx, 16                                       ;93.1
        cmp       ebx, 992                                      ;93.1
        jb        .B2.37        ; Prob 99%                      ;93.1
                                ; LOE eax edx ecx ebx xmm0
.B2.38:                         ; Preds .B2.37
        mov       ebx, eax                                      ;93.1
                                ; LOE eax edx ecx ebx xmm0
.B2.39:                         ; Preds .B2.39 .B2.38
        movdqa    XMMWORD PTR [_DYNUST_TRANSIT_MODULE_mp_TRANSITREPOS+3968+edx+ebx*4], xmm0 ;93.1
        add       ebx, 4                                        ;93.1
        cmp       ebx, 8                                        ;93.1
        jb        .B2.39        ; Prob 99%                      ;93.1
                                ; LOE eax edx ecx ebx xmm0
.B2.40:                         ; Preds .B2.39
        inc       ecx                                           ;93.1
        add       edx, 4000                                     ;93.1
        cmp       ecx, 5                                        ;93.1
        jb        .B2.36        ; Prob 80%                      ;93.1
                                ; LOE eax edx ecx xmm0
.B2.41:                         ; Preds .B2.40
        xor       eax, eax                                      ;94.1
        lea       edx, DWORD PTR [80+esp]                       ;97.1
        mov       DWORD PTR [592+esp], eax                      ;97.1
        mov       DWORD PTR [80+esp], 24                        ;97.1
        mov       DWORD PTR [84+esp], OFFSET FLAT: __STRLITPACK_68 ;97.1
        mov       DWORD PTR [88+esp], 7                         ;97.1
        mov       DWORD PTR [92+esp], OFFSET FLAT: __STRLITPACK_67 ;97.1
        push      32                                            ;97.1
        push      edx                                           ;97.1
        push      OFFSET FLAT: __STRLITPACK_72.0.2              ;97.1
        push      -2088435968                                   ;97.1
        push      1                                             ;97.1
        mov       DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_TRANSITREPOSPNTR], eax ;94.1
        lea       ecx, DWORD PTR [612+esp]                      ;97.1
        push      ecx                                           ;97.1
        call      _for_open                                     ;97.1
                                ; LOE
.B2.285:                        ; Preds .B2.41
        add       esp, 24                                       ;97.1
                                ; LOE
.B2.42:                         ; Preds .B2.285
        mov       esi, 1                                        ;98.1
        xor       ebx, ebx                                      ;98.1
        lea       edi, DWORD PTR [592+esp]                      ;98.1
                                ; LOE ebx esi edi
.B2.43:                         ; Preds .B2.44 .B2.42
        mov       DWORD PTR [592+esp], ebx                      ;99.3
        push      32                                            ;99.3
        push      ebx                                           ;99.3
        push      OFFSET FLAT: __STRLITPACK_73.0.2              ;99.3
        push      -2088435968                                   ;99.3
        push      1                                             ;99.3
        push      edi                                           ;99.3
        call      _for_read_seq_lis                             ;99.3
                                ; LOE ebx esi edi
.B2.286:                        ; Preds .B2.43
        add       esp, 24                                       ;99.3
                                ; LOE ebx esi edi
.B2.44:                         ; Preds .B2.286
        inc       esi                                           ;100.1
        cmp       esi, 14                                       ;100.1
        jle       .B2.43        ; Prob 92%                      ;100.1
                                ; LOE ebx esi edi
.B2.45:                         ; Preds .B2.44
        mov       DWORD PTR [592+esp], 0                        ;101.1
        lea       eax, DWORD PTR [168+esp]                      ;101.1
        mov       DWORD PTR [168+esp], OFFSET FLAT: _DYNUST_TRANSIT_MODULE_mp_SIMSTARTTIME ;101.1
        push      32                                            ;101.1
        push      eax                                           ;101.1
        push      OFFSET FLAT: __STRLITPACK_74.0.2              ;101.1
        push      -2088435968                                   ;101.1
        push      1                                             ;101.1
        lea       edx, DWORD PTR [612+esp]                      ;101.1
        push      edx                                           ;101.1
        call      _for_read_seq_lis                             ;101.1
                                ; LOE
.B2.287:                        ; Preds .B2.45
        add       esp, 24                                       ;101.1
                                ; LOE
.B2.46:                         ; Preds .B2.287
        mov       esi, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_NUMROUTES] ;104.4
        mov       ecx, 1                                        ;104.4
        mov       DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_INPUTTRANSIT$I.0.2], 1 ;104.4
        xor       ebx, ebx                                      ;
        test      esi, esi                                      ;104.4
        jle       .B2.269       ; Prob 2%                       ;104.4
                                ; LOE ecx ebx esi
.B2.47:                         ; Preds .B2.46
        mov       edx, 1                                        ;
        mov       eax, 176                                      ;
        mov       edi, 44                                       ;
        movss     xmm1, DWORD PTR [_2il0floatpacket.3]          ;214.177
        pxor      xmm0, xmm0                                    ;115.14
        mov       DWORD PTR [668+esp], ebx                      ;
        mov       DWORD PTR [676+esp], edi                      ;
        mov       DWORD PTR [680+esp], eax                      ;
        mov       DWORD PTR [460+esp], edx                      ;
        mov       DWORD PTR [468+esp], ecx                      ;
        mov       DWORD PTR [464+esp], esi                      ;
                                ; LOE
.B2.48:                         ; Preds .B2.214 .B2.47
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_LOGOUT], 0  ;105.13
        jle       .B2.50        ; Prob 16%                      ;105.13
                                ; LOE
.B2.49:                         ; Preds .B2.48
        mov       eax, DWORD PTR [460+esp]                      ;105.18
        lea       edx, DWORD PTR [368+esp]                      ;105.18
        mov       DWORD PTR [592+esp], 0                        ;105.18
        mov       DWORD PTR [368+esp], eax                      ;105.18
        push      32                                            ;105.18
        push      OFFSET FLAT: DYNUST_TRANSIT_MODULE_mp_INPUTTRANSIT$format_pack.0.2 ;105.18
        push      edx                                           ;105.18
        push      OFFSET FLAT: __STRLITPACK_75.0.2              ;105.18
        push      -2088435968                                   ;105.18
        push      711                                           ;105.18
        lea       ecx, DWORD PTR [616+esp]                      ;105.18
        push      ecx                                           ;105.18
        call      _for_write_seq_fmt                            ;105.18
                                ; LOE
.B2.288:                        ; Preds .B2.49
        add       esp, 28                                       ;105.18
                                ; LOE
.B2.50:                         ; Preds .B2.288 .B2.48
        imul      esi, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE+32], 176 ;106.3
        imul      ebx, DWORD PTR [468+esp], 176                 ;107.13
        mov       edi, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE] ;106.3
        mov       edx, edi                                      ;106.3
        sub       edx, esi                                      ;106.3
        add       edi, ebx                                      ;107.3
        mov       ecx, DWORD PTR [680+esp]                      ;106.3
        sub       edi, esi                                      ;107.3
        mov       eax, DWORD PTR [460+esp]                      ;106.3
        mov       DWORD PTR [592+esp], 0                        ;107.3
        mov       DWORD PTR [312+esp], 6                        ;107.3
        mov       DWORD PTR [672+esp], ebx                      ;107.13
        mov       DWORD PTR [316+esp], edi                      ;107.3
        push      32                                            ;107.3
        mov       DWORD PTR [8+edx+ecx], eax                    ;106.3
        lea       eax, DWORD PTR [316+esp]                      ;107.3
        push      eax                                           ;107.3
        push      OFFSET FLAT: __STRLITPACK_76.0.2              ;107.3
        push      -2088435968                                   ;107.3
        push      1                                             ;107.3
        lea       edx, DWORD PTR [612+esp]                      ;107.3
        push      edx                                           ;107.3
        call      _for_read_seq_lis                             ;107.3
                                ; LOE
.B2.51:                         ; Preds .B2.50
        imul      eax, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE+32], 176 ;107.3
        lea       ecx, DWORD PTR [528+esp]                      ;107.3
        mov       edx, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE] ;107.3
        add       eax, -12                                      ;107.3
        add       edx, DWORD PTR [696+esp]                      ;107.3
        sub       edx, eax                                      ;107.3
        mov       DWORD PTR [528+esp], edx                      ;107.3
        push      ecx                                           ;107.3
        push      OFFSET FLAT: __STRLITPACK_77.0.2              ;107.3
        lea       ebx, DWORD PTR [624+esp]                      ;107.3
        push      ebx                                           ;107.3
        call      _for_read_seq_lis_xmit                        ;107.3
                                ; LOE
.B2.52:                         ; Preds .B2.51
        imul      eax, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE+32], 176 ;107.3
        lea       ecx, DWORD PTR [548+esp]                      ;107.3
        mov       edx, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE] ;107.3
        add       eax, -16                                      ;107.3
        add       edx, DWORD PTR [708+esp]                      ;107.3
        sub       edx, eax                                      ;107.3
        mov       DWORD PTR [548+esp], edx                      ;107.3
        push      ecx                                           ;107.3
        push      OFFSET FLAT: __STRLITPACK_78.0.2              ;107.3
        lea       ebx, DWORD PTR [636+esp]                      ;107.3
        push      ebx                                           ;107.3
        call      _for_read_seq_lis_xmit                        ;107.3
                                ; LOE
.B2.53:                         ; Preds .B2.52
        imul      ebx, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE+32], -176 ;108.3
        mov       esi, 4                                        ;108.3
        mov       ecx, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE] ;108.3
        xor       eax, eax                                      ;108.3
        add       ebx, DWORD PTR [728+esp]                      ;108.3
        mov       edx, 1                                        ;108.3
        push      esi                                           ;108.3
        mov       edi, DWORD PTR [16+ecx+ebx]                   ;108.3
        test      edi, edi                                      ;108.3
        mov       DWORD PTR [28+ecx+ebx], eax                   ;108.3
        cmovl     edi, eax                                      ;108.3
        push      edi                                           ;108.3
        push      2                                             ;108.3
        mov       DWORD PTR [32+ecx+ebx], 5                     ;108.3
        lea       eax, DWORD PTR [492+esp]                      ;108.3
        push      eax                                           ;108.3
        mov       DWORD PTR [24+ecx+ebx], esi                   ;108.3
        mov       DWORD PTR [36+ecx+ebx], edx                   ;108.3
        mov       DWORD PTR [52+ecx+ebx], edx                   ;108.3
        mov       DWORD PTR [44+ecx+ebx], edi                   ;108.3
        mov       DWORD PTR [48+ecx+ebx], esi                   ;108.3
        call      _for_check_mult_overflow                      ;108.3
                                ; LOE eax
.B2.54:                         ; Preds .B2.53
        imul      edx, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE+32], 176 ;108.3
        and       eax, 1                                        ;108.3
        mov       ecx, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE] ;108.3
        add       edx, -20                                      ;108.3
        shl       eax, 4                                        ;108.3
        add       ecx, DWORD PTR [736+esp]                      ;108.3
        or        eax, 262144                                   ;108.3
        push      eax                                           ;108.3
        sub       ecx, edx                                      ;108.3
        push      ecx                                           ;108.3
        push      DWORD PTR [504+esp]                           ;108.3
        call      _for_allocate                                 ;108.3
                                ; LOE
.B2.55:                         ; Preds .B2.54
        imul      ecx, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE+32], -176 ;109.3
        mov       ebx, 1                                        ;109.3
        mov       edx, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE] ;109.3
        xor       eax, eax                                      ;109.3
        add       ecx, DWORD PTR [756+esp]                      ;109.3
        push      ebx                                           ;109.3
        mov       esi, DWORD PTR [16+edx+ecx]                   ;109.3
        test      esi, esi                                      ;109.3
        mov       DWORD PTR [68+edx+ecx], 5                     ;109.3
        cmovl     esi, eax                                      ;109.3
        push      esi                                           ;109.3
        push      2                                             ;109.3
        mov       DWORD PTR [60+edx+ecx], ebx                   ;109.3
        lea       edi, DWORD PTR [524+esp]                      ;109.3
        push      edi                                           ;109.3
        mov       DWORD PTR [72+edx+ecx], ebx                   ;109.3
        mov       DWORD PTR [64+edx+ecx], eax                   ;109.3
        mov       DWORD PTR [88+edx+ecx], ebx                   ;109.3
        mov       DWORD PTR [80+edx+ecx], esi                   ;109.3
        mov       DWORD PTR [84+edx+ecx], ebx                   ;109.3
        call      _for_check_mult_overflow                      ;109.3
                                ; LOE eax
.B2.56:                         ; Preds .B2.55
        imul      edx, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE+32], 176 ;109.3
        and       eax, 1                                        ;109.3
        mov       ecx, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE] ;109.3
        add       edx, -56                                      ;109.3
        shl       eax, 4                                        ;109.3
        add       ecx, DWORD PTR [764+esp]                      ;109.3
        or        eax, 262144                                   ;109.3
        push      eax                                           ;109.3
        sub       ecx, edx                                      ;109.3
        push      ecx                                           ;109.3
        push      DWORD PTR [536+esp]                           ;109.3
        call      _for_allocate                                 ;109.3
                                ; LOE
.B2.57:                         ; Preds .B2.56
        imul      ebx, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE+32], -176 ;110.3
        mov       edi, 2                                        ;110.3
        mov       ecx, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE] ;110.3
        xor       eax, eax                                      ;110.3
        add       ebx, DWORD PTR [784+esp]                      ;110.3
        mov       edx, 1                                        ;110.3
        push      edi                                           ;110.3
        mov       esi, DWORD PTR [16+ecx+ebx]                   ;110.3
        test      esi, esi                                      ;110.3
        mov       DWORD PTR [100+ecx+ebx], eax                  ;110.3
        cmovl     esi, eax                                      ;110.3
        push      esi                                           ;110.3
        push      edi                                           ;110.3
        mov       DWORD PTR [104+ecx+ebx], 5                    ;110.3
        lea       eax, DWORD PTR [556+esp]                      ;110.3
        push      eax                                           ;110.3
        mov       DWORD PTR [96+ecx+ebx], edi                   ;110.3
        mov       DWORD PTR [108+ecx+ebx], edx                  ;110.3
        mov       DWORD PTR [124+ecx+ebx], edx                  ;110.3
        mov       DWORD PTR [116+ecx+ebx], esi                  ;110.3
        mov       DWORD PTR [120+ecx+ebx], edi                  ;110.3
        call      _for_check_mult_overflow                      ;110.3
                                ; LOE eax
.B2.291:                        ; Preds .B2.57
        add       esp, 120                                      ;110.3
                                ; LOE eax
.B2.58:                         ; Preds .B2.291
        imul      edx, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE+32], 176 ;110.3
        and       eax, 1                                        ;110.3
        mov       ecx, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE] ;110.3
        add       edx, -92                                      ;110.3
        shl       eax, 4                                        ;110.3
        add       ecx, DWORD PTR [672+esp]                      ;110.3
        or        eax, 262144                                   ;110.3
        push      eax                                           ;110.3
        sub       ecx, edx                                      ;110.3
        push      ecx                                           ;110.3
        push      DWORD PTR [448+esp]                           ;110.3
        call      _for_allocate                                 ;110.3
                                ; LOE
.B2.59:                         ; Preds .B2.58
        imul      ecx, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE+32], -176 ;111.3
        mov       ebx, 1                                        ;111.3
        mov       edx, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE] ;111.3
        xor       eax, eax                                      ;111.3
        add       ecx, DWORD PTR [692+esp]                      ;111.3
        push      ebx                                           ;111.3
        mov       esi, DWORD PTR [16+edx+ecx]                   ;111.3
        test      esi, esi                                      ;111.3
        mov       DWORD PTR [140+edx+ecx], 5                    ;111.3
        cmovl     esi, eax                                      ;111.3
        push      esi                                           ;111.3
        push      2                                             ;111.3
        mov       DWORD PTR [132+edx+ecx], ebx                  ;111.3
        lea       edi, DWORD PTR [468+esp]                      ;111.3
        push      edi                                           ;111.3
        mov       DWORD PTR [144+edx+ecx], ebx                  ;111.3
        mov       DWORD PTR [136+edx+ecx], eax                  ;111.3
        mov       DWORD PTR [160+edx+ecx], ebx                  ;111.3
        mov       DWORD PTR [152+edx+ecx], esi                  ;111.3
        mov       DWORD PTR [156+edx+ecx], ebx                  ;111.3
        call      _for_check_mult_overflow                      ;111.3
                                ; LOE eax
.B2.60:                         ; Preds .B2.59
        imul      edx, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE+32], 176 ;111.3
        and       eax, 1                                        ;111.3
        mov       ecx, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE] ;111.3
        add       edx, -128                                     ;111.3
        shl       eax, 4                                        ;111.3
        add       ecx, DWORD PTR [700+esp]                      ;111.3
        or        eax, 262144                                   ;111.3
        push      eax                                           ;111.3
        sub       ecx, edx                                      ;111.3
        push      ecx                                           ;111.3
        push      DWORD PTR [480+esp]                           ;111.3
        call      _for_allocate                                 ;111.3
                                ; LOE
.B2.61:                         ; Preds .B2.60
        xor       eax, eax                                      ;112.3
        mov       DWORD PTR [632+esp], eax                      ;112.3
        push      32                                            ;112.3
        push      eax                                           ;112.3
        push      OFFSET FLAT: __STRLITPACK_79.0.2              ;112.3
        push      -2088435968                                   ;112.3
        push      1                                             ;112.3
        lea       edx, DWORD PTR [652+esp]                      ;112.3
        push      edx                                           ;112.3
        call      _for_read_seq_lis                             ;112.3
                                ; LOE
.B2.293:                        ; Preds .B2.61
        add       esp, 64                                       ;112.3
                                ; LOE
.B2.62:                         ; Preds .B2.293
        imul      eax, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE+32], -176 ;112.3
        add       eax, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE] ;112.3
        mov       edx, DWORD PTR [680+esp]                      ;112.3
        mov       edi, DWORD PTR [16+eax+edx]                   ;112.3
        test      edi, edi                                      ;112.3
        jle       .B2.67        ; Prob 2%                       ;112.3
                                ; LOE edi
.B2.63:                         ; Preds .B2.62
        mov       ebx, 1                                        ;
        lea       esi, DWORD PTR [592+esp]                      ;
                                ; LOE ebx esi edi
.B2.64:                         ; Preds .B2.65 .B2.63
        imul      eax, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE+32], 176 ;112.3
        mov       edx, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE] ;112.3
        neg       eax                                           ;112.3
        add       edx, DWORD PTR [672+esp]                      ;112.3
        mov       DWORD PTR [568+esp], 4                        ;112.3
        mov       ecx, DWORD PTR [52+eax+edx]                   ;112.14
        neg       ecx                                           ;112.3
        add       ecx, ebx                                      ;112.3
        imul      ecx, DWORD PTR [48+eax+edx]                   ;112.3
        add       ecx, DWORD PTR [20+eax+edx]                   ;112.3
        lea       eax, DWORD PTR [568+esp]                      ;112.3
        mov       DWORD PTR [572+esp], ecx                      ;112.3
        push      eax                                           ;112.3
        push      OFFSET FLAT: __STRLITPACK_80.0.2              ;112.3
        push      esi                                           ;112.3
        call      _for_read_seq_lis_xmit                        ;112.3
                                ; LOE ebx esi edi
.B2.294:                        ; Preds .B2.64
        add       esp, 12                                       ;112.3
                                ; LOE ebx esi edi
.B2.65:                         ; Preds .B2.294
        inc       ebx                                           ;112.3
        cmp       ebx, edi                                      ;112.3
        jle       .B2.64        ; Prob 82%                      ;112.3
                                ; LOE ebx esi edi
.B2.67:                         ; Preds .B2.65 .B2.62
        push      0                                             ;112.3
        push      OFFSET FLAT: __STRLITPACK_81.0.2              ;112.3
        lea       eax, DWORD PTR [600+esp]                      ;112.3
        push      eax                                           ;112.3
        call      _for_read_seq_lis_xmit                        ;112.3
                                ; LOE
.B2.68:                         ; Preds .B2.67
        xor       eax, eax                                      ;113.3
        mov       DWORD PTR [604+esp], eax                      ;113.3
        push      32                                            ;113.3
        push      eax                                           ;113.3
        push      OFFSET FLAT: __STRLITPACK_82.0.2              ;113.3
        push      -2088435968                                   ;113.3
        push      1                                             ;113.3
        lea       edx, DWORD PTR [624+esp]                      ;113.3
        push      edx                                           ;113.3
        call      _for_read_seq_lis                             ;113.3
                                ; LOE
.B2.295:                        ; Preds .B2.68
        add       esp, 36                                       ;113.3
                                ; LOE
.B2.69:                         ; Preds .B2.295
        imul      eax, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE+32], -176 ;113.3
        add       eax, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE] ;113.3
        mov       edx, DWORD PTR [680+esp]                      ;113.3
        mov       edi, DWORD PTR [16+eax+edx]                   ;113.3
        test      edi, edi                                      ;113.3
        jle       .B2.74        ; Prob 2%                       ;113.3
                                ; LOE edi
.B2.70:                         ; Preds .B2.69
        mov       ebx, 1                                        ;
        lea       esi, DWORD PTR [592+esp]                      ;
                                ; LOE ebx esi edi
.B2.71:                         ; Preds .B2.72 .B2.70
        imul      eax, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE+32], 176 ;113.3
        mov       edx, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE] ;113.3
        neg       eax                                           ;113.3
        add       edx, DWORD PTR [672+esp]                      ;113.3
        mov       DWORD PTR [576+esp], 1                        ;113.3
        mov       ecx, DWORD PTR [88+eax+edx]                   ;113.14
        neg       ecx                                           ;113.3
        add       ecx, ebx                                      ;113.3
        imul      ecx, DWORD PTR [84+eax+edx]                   ;113.3
        add       ecx, DWORD PTR [56+eax+edx]                   ;113.3
        lea       eax, DWORD PTR [576+esp]                      ;113.3
        mov       DWORD PTR [580+esp], ecx                      ;113.3
        push      eax                                           ;113.3
        push      OFFSET FLAT: __STRLITPACK_83.0.2              ;113.3
        push      esi                                           ;113.3
        call      _for_read_seq_lis_xmit                        ;113.3
                                ; LOE ebx esi edi
.B2.296:                        ; Preds .B2.71
        add       esp, 12                                       ;113.3
                                ; LOE ebx esi edi
.B2.72:                         ; Preds .B2.296
        inc       ebx                                           ;113.3
        cmp       ebx, edi                                      ;113.3
        jle       .B2.71        ; Prob 82%                      ;113.3
                                ; LOE ebx esi edi
.B2.74:                         ; Preds .B2.72 .B2.69
        push      0                                             ;113.3
        push      OFFSET FLAT: __STRLITPACK_84.0.2              ;113.3
        lea       eax, DWORD PTR [600+esp]                      ;113.3
        push      eax                                           ;113.3
        call      _for_read_seq_lis_xmit                        ;113.3
                                ; LOE
.B2.75:                         ; Preds .B2.74
        xor       eax, eax                                      ;114.3
        mov       DWORD PTR [604+esp], eax                      ;114.3
        push      32                                            ;114.3
        push      eax                                           ;114.3
        push      OFFSET FLAT: __STRLITPACK_85.0.2              ;114.3
        push      -2088435968                                   ;114.3
        push      1                                             ;114.3
        lea       edx, DWORD PTR [624+esp]                      ;114.3
        push      edx                                           ;114.3
        call      _for_read_seq_lis                             ;114.3
                                ; LOE
.B2.297:                        ; Preds .B2.75
        add       esp, 36                                       ;114.3
                                ; LOE
.B2.76:                         ; Preds .B2.297
        imul      eax, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE+32], -176 ;114.3
        add       eax, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE] ;114.3
        mov       edx, DWORD PTR [680+esp]                      ;114.3
        mov       edi, DWORD PTR [16+eax+edx]                   ;114.3
        test      edi, edi                                      ;114.3
        jle       .B2.81        ; Prob 2%                       ;114.3
                                ; LOE edi
.B2.77:                         ; Preds .B2.76
        mov       esi, DWORD PTR [672+esp]                      ;
        mov       ebx, 1                                        ;
                                ; LOE ebx esi edi
.B2.78:                         ; Preds .B2.79 .B2.77
        imul      eax, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE+32], 176 ;114.3
        mov       edx, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE] ;114.3
        neg       eax                                           ;114.3
        add       edx, esi                                      ;114.3
        mov       DWORD PTR [584+esp], 2                        ;114.3
        mov       ecx, DWORD PTR [124+eax+edx]                  ;114.14
        neg       ecx                                           ;114.3
        add       ecx, ebx                                      ;114.3
        imul      ecx, DWORD PTR [120+eax+edx]                  ;114.3
        add       ecx, DWORD PTR [92+eax+edx]                   ;114.3
        lea       eax, DWORD PTR [584+esp]                      ;114.3
        mov       DWORD PTR [588+esp], ecx                      ;114.3
        push      eax                                           ;114.3
        push      OFFSET FLAT: __STRLITPACK_86.0.2              ;114.3
        lea       edx, DWORD PTR [600+esp]                      ;114.3
        push      edx                                           ;114.3
        call      _for_read_seq_lis_xmit                        ;114.3
                                ; LOE ebx esi edi
.B2.298:                        ; Preds .B2.78
        add       esp, 12                                       ;114.3
                                ; LOE ebx esi edi
.B2.79:                         ; Preds .B2.298
        inc       ebx                                           ;114.3
        cmp       ebx, edi                                      ;114.3
        jle       .B2.78        ; Prob 82%                      ;114.3
                                ; LOE ebx esi edi
.B2.81:                         ; Preds .B2.79 .B2.76
        push      0                                             ;114.3
        push      OFFSET FLAT: __STRLITPACK_87.0.2              ;114.3
        lea       eax, DWORD PTR [600+esp]                      ;114.3
        push      eax                                           ;114.3
        call      _for_read_seq_lis_xmit                        ;114.3
                                ; LOE
.B2.299:                        ; Preds .B2.81
        add       esp, 12                                       ;114.3
                                ; LOE
.B2.82:                         ; Preds .B2.299
        imul      eax, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE+32], 176 ;115.14
        mov       edx, DWORD PTR [680+esp]                      ;115.14
        sub       edx, eax                                      ;115.14
        mov       ecx, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE] ;115.14
        mov       DWORD PTR [332+esp], eax                      ;115.14
        mov       ebx, DWORD PTR [160+edx+ecx]                  ;115.14
        mov       esi, DWORD PTR [156+edx+ecx]                  ;115.14
        cmp       esi, 1                                        ;115.14
        mov       eax, DWORD PTR [152+edx+ecx]                  ;115.14
        mov       DWORD PTR [500+esp], ebx                      ;115.14
        mov       DWORD PTR [164+esp], esi                      ;115.14
        jne       .B2.97        ; Prob 50%                      ;115.14
                                ; LOE eax edx ecx
.B2.83:                         ; Preds .B2.82
        test      eax, eax                                      ;115.14
        jle       .B2.104       ; Prob 50%                      ;115.14
                                ; LOE eax edx ecx
.B2.84:                         ; Preds .B2.83
        cmp       eax, 16                                       ;115.14
        jl        .B2.226       ; Prob 10%                      ;115.14
                                ; LOE eax edx ecx
.B2.85:                         ; Preds .B2.84
        mov       edi, DWORD PTR [128+edx+ecx]                  ;115.14
        mov       DWORD PTR [172+esp], edi                      ;115.14
        and       edi, 15                                       ;115.14
        mov       ebx, edi                                      ;115.14
        neg       ebx                                           ;115.14
        add       ebx, 16                                       ;115.14
        test      edi, edi                                      ;115.14
        cmovne    edi, ebx                                      ;115.14
        lea       esi, DWORD PTR [16+edi]                       ;115.14
        cmp       eax, esi                                      ;115.14
        jl        .B2.226       ; Prob 10%                      ;115.14
                                ; LOE eax edx ecx edi
.B2.86:                         ; Preds .B2.85
        mov       esi, eax                                      ;115.14
        sub       esi, edi                                      ;115.14
        and       esi, 15                                       ;115.14
        neg       esi                                           ;115.14
        add       esi, eax                                      ;115.14
        test      edi, edi                                      ;115.14
        jbe       .B2.90        ; Prob 10%                      ;115.14
                                ; LOE eax edx ecx esi edi
.B2.87:                         ; Preds .B2.86
        mov       DWORD PTR [644+esp], ecx                      ;
        xor       ebx, ebx                                      ;
        mov       ecx, DWORD PTR [172+esp]                      ;
                                ; LOE eax edx ecx ebx esi edi
.B2.88:                         ; Preds .B2.88 .B2.87
        mov       BYTE PTR [ebx+ecx], 0                         ;115.14
        inc       ebx                                           ;115.14
        cmp       ebx, edi                                      ;115.14
        jb        .B2.88        ; Prob 82%                      ;115.14
                                ; LOE eax edx ecx ebx esi edi
.B2.89:                         ; Preds .B2.88
        mov       ecx, DWORD PTR [644+esp]                      ;
                                ; LOE eax edx ecx esi edi
.B2.90:                         ; Preds .B2.89 .B2.86
        mov       ebx, DWORD PTR [172+esp]                      ;
        pxor      xmm0, xmm0                                    ;
                                ; LOE eax edx ecx ebx esi edi xmm0
.B2.91:                         ; Preds .B2.91 .B2.90
        movdqa    XMMWORD PTR [edi+ebx], xmm0                   ;115.14
        add       edi, 16                                       ;115.14
        cmp       edi, esi                                      ;115.14
        jb        .B2.91        ; Prob 82%                      ;115.14
                                ; LOE eax edx ecx ebx esi edi xmm0
.B2.93:                         ; Preds .B2.91 .B2.226
        cmp       esi, eax                                      ;115.14
        jae       .B2.104       ; Prob 10%                      ;115.14
                                ; LOE eax edx ecx esi
.B2.94:                         ; Preds .B2.93
        mov       ebx, DWORD PTR [128+edx+ecx]                  ;115.14
                                ; LOE eax edx ecx ebx esi
.B2.95:                         ; Preds .B2.95 .B2.94
        mov       BYTE PTR [esi+ebx], 0                         ;115.14
        inc       esi                                           ;115.14
        cmp       esi, eax                                      ;115.14
        jb        .B2.95        ; Prob 82%                      ;115.14
        jmp       .B2.104       ; Prob 100%                     ;115.14
                                ; LOE eax edx ecx ebx esi
.B2.97:                         ; Preds .B2.82
        test      eax, eax                                      ;115.14
        jle       .B2.104       ; Prob 50%                      ;115.14
                                ; LOE eax edx ecx
.B2.98:                         ; Preds .B2.97
        mov       ebx, eax                                      ;115.14
        shr       ebx, 31                                       ;115.14
        add       ebx, eax                                      ;115.14
        sar       ebx, 1                                        ;115.14
        mov       DWORD PTR [508+esp], ebx                      ;115.14
        test      ebx, ebx                                      ;115.14
        jbe       .B2.228       ; Prob 10%                      ;115.14
                                ; LOE eax edx ecx
.B2.99:                         ; Preds .B2.98
        mov       esi, DWORD PTR [500+esp]                      ;
        xor       ebx, ebx                                      ;
        imul      esi, DWORD PTR [164+esp]                      ;
        mov       edi, DWORD PTR [128+edx+ecx]                  ;115.14
        sub       edi, esi                                      ;
        mov       DWORD PTR [188+esp], eax                      ;
        mov       DWORD PTR [648+esp], edx                      ;
        mov       DWORD PTR [644+esp], ecx                      ;
        mov       eax, edi                                      ;
        mov       edx, DWORD PTR [164+esp]                      ;
                                ; LOE eax edx ebx
.B2.100:                        ; Preds .B2.100 .B2.99
        mov       esi, DWORD PTR [500+esp]                      ;115.14
        mov       edi, edx                                      ;115.14
        xor       ecx, ecx                                      ;115.14
        lea       esi, DWORD PTR [esi+ebx*2]                    ;115.14
        inc       ebx                                           ;115.14
        imul      edi, esi                                      ;115.14
        inc       esi                                           ;115.14
        imul      esi, edx                                      ;115.14
        mov       BYTE PTR [eax+edi], cl                        ;115.14
        mov       BYTE PTR [eax+esi], cl                        ;115.14
        cmp       ebx, DWORD PTR [508+esp]                      ;115.14
        jb        .B2.100       ; Prob 64%                      ;115.14
                                ; LOE eax edx ebx
.B2.101:                        ; Preds .B2.100
        mov       eax, DWORD PTR [188+esp]                      ;
        lea       ebx, DWORD PTR [1+ebx+ebx]                    ;115.14
        mov       edx, DWORD PTR [648+esp]                      ;
        mov       ecx, DWORD PTR [644+esp]                      ;
                                ; LOE eax edx ecx ebx
.B2.102:                        ; Preds .B2.101 .B2.228
        lea       esi, DWORD PTR [-1+ebx]                       ;115.14
        cmp       eax, esi                                      ;115.14
        jbe       .B2.104       ; Prob 10%                      ;115.14
                                ; LOE edx ecx ebx
.B2.103:                        ; Preds .B2.102
        mov       eax, DWORD PTR [500+esp]                      ;115.14
        lea       esi, DWORD PTR [-1+ebx+eax]                   ;115.14
        mov       ebx, DWORD PTR [128+edx+ecx]                  ;115.14
        sub       esi, eax                                      ;115.14
        imul      esi, DWORD PTR [164+esp]                      ;115.14
        mov       BYTE PTR [ebx+esi], 0                         ;115.14
                                ; LOE edx ecx
.B2.104:                        ; Preds .B2.95 .B2.93 .B2.83 .B2.103 .B2.102
                                ;       .B2.97
        xor       eax, eax                                      ;
        mov       DWORD PTR [488+esp], eax                      ;
        mov       edi, DWORD PTR [52+edx+ecx]                   ;132.6
        mov       eax, DWORD PTR [48+edx+ecx]                   ;132.6
        imul      edi, eax                                      ;
        mov       ebx, DWORD PTR [20+edx+ecx]                   ;132.6
        mov       esi, DWORD PTR [16+edx+ecx]                   ;117.3
        sub       ebx, edi                                      ;
        test      esi, esi                                      ;117.3
        jle       .B2.111       ; Prob 2%                       ;117.3
                                ; LOE eax edx ecx ebx esi
.B2.105:                        ; Preds .B2.104
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM+32] ;118.5
        shl       edi, 2                                        ;
        neg       edi                                           ;
        add       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM] ;
        mov       DWORD PTR [696+esp], edi                      ;
        imul      edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE+32], -84 ;
        add       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE] ;
        mov       DWORD PTR [412+esp], 1                        ;
        mov       DWORD PTR [564+esp], edi                      ;
        mov       DWORD PTR [704+esp], eax                      ;
        mov       DWORD PTR [700+esp], esi                      ;
        mov       DWORD PTR [648+esp], edx                      ;
        mov       DWORD PTR [644+esp], ecx                      ;
        mov       edi, DWORD PTR [412+esp]                      ;
                                ; LOE ebx edi
.B2.106:                        ; Preds .B2.109 .B2.105
        mov       ecx, DWORD PTR [704+esp]                      ;118.44
        imul      ecx, edi                                      ;118.44
        mov       eax, DWORD PTR [696+esp]                      ;118.8
        mov       esi, DWORD PTR [ebx+ecx]                      ;118.8
        mov       edx, DWORD PTR [eax+esi*4]                    ;118.8
        test      edx, edx                                      ;118.44
        jle       .B2.263       ; Prob 16%                      ;118.44
                                ; LOE edx ecx ebx esi edi
.B2.107:                        ; Preds .B2.106
        mov       DWORD PTR [ebx+ecx], edx                      ;119.7
        mov       eax, edi                                      ;120.35
        mov       ecx, DWORD PTR [648+esp]                      ;120.35
        mov       esi, DWORD PTR [644+esp]                      ;120.35
        sub       eax, DWORD PTR [88+ecx+esi]                   ;120.35
        imul      eax, DWORD PTR [84+ecx+esi]                   ;120.35
        mov       ecx, DWORD PTR [56+ecx+esi]                   ;120.10
        cmp       BYTE PTR [ecx+eax], 0                         ;120.33
        jle       .B2.109       ; Prob 16%                      ;120.33
                                ; LOE edx ebx edi
.B2.108:                        ; Preds .B2.107
        imul      ecx, edx, 84                                  ;120.40
        mov       eax, DWORD PTR [564+esp]                      ;120.40
        movsx     esi, WORD PTR [36+eax+ecx]                    ;120.40
        test      esi, esi                                      ;120.89
        jle       .B2.258       ; Prob 16%                      ;120.89
                                ; LOE edx ebx edi
.B2.109:                        ; Preds .B2.262 .B2.108 .B2.107 .B2.268
        inc       edi                                           ;131.3
        cmp       edi, DWORD PTR [700+esp]                      ;131.3
        jle       .B2.106       ; Prob 82%                      ;131.3
                                ; LOE ebx edi
.B2.110:                        ; Preds .B2.109
        mov       eax, DWORD PTR [704+esp]                      ;
                                ; LOE eax ebx
.B2.111:                        ; Preds .B2.104 .B2.110
        mov       esi, DWORD PTR [ebx+eax]                      ;132.6
        test      esi, esi                                      ;132.24
        mov       ebx, DWORD PTR [ebx+eax*2]                    ;138.24
        jle       .B2.255       ; Prob 16%                      ;132.24
                                ; LOE ebx esi
.B2.112:                        ; Preds .B2.111
        test      ebx, ebx                                      ;132.49
        jle       .B2.255       ; Prob 16%                      ;132.49
                                ; LOE ebx esi
.B2.113:                        ; Preds .B2.112 .B2.331
        test      BYTE PTR [488+esp], 1                         ;136.6
        jne       .B2.254       ; Prob 3%                       ;136.6
                                ; LOE ebx esi
.B2.114:                        ; Preds .B2.330 .B2.113
        mov       DWORD PTR [448+esp], esi                      ;138.24
        mov       DWORD PTR [452+esp], ebx                      ;138.24
        push      OFFSET FLAT: __NLITPACK_0.0.2                 ;138.24
        lea       eax, DWORD PTR [456+esp]                      ;138.24
        push      eax                                           ;138.24
        lea       edx, DWORD PTR [456+esp]                      ;138.24
        push      edx                                           ;138.24
        call      _DYNUST_NETWORK_MODULE_mp_GETFLINKFROMNODE    ;138.24
                                ; LOE eax
.B2.300:                        ; Preds .B2.114
        add       esp, 12                                       ;138.24
                                ; LOE eax
.B2.115:                        ; Preds .B2.300
        mov       ebx, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE+32] ;138.24
        imul      edx, ebx, 176                                 ;138.3
        mov       DWORD PTR [472+esp], ebx                      ;138.24
        mov       ebx, DWORD PTR [680+esp]                      ;138.3
        sub       ebx, edx                                      ;138.3
        mov       esi, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE] ;138.24
        mov       DWORD PTR [476+esp], edx                      ;138.3
        mov       edi, DWORD PTR [48+ebx+esi]                   ;141.3
        mov       DWORD PTR [164+ebx+esi], eax                  ;138.3
        mov       eax, DWORD PTR [52+ebx+esi]                   ;141.3
        imul      eax, edi                                      ;141.61
        mov       edx, DWORD PTR [20+ebx+esi]                   ;141.3
        mov       ecx, edx                                      ;141.61
        sub       ecx, eax                                      ;141.61
        mov       DWORD PTR [692+esp], edi                      ;141.3
        mov       DWORD PTR [480+esp], ecx                      ;141.61
        mov       DWORD PTR [496+esp], eax                      ;141.61
        imul      edi, DWORD PTR [ecx+edi], 84                  ;141.6
        imul      ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE+32], -84 ;141.61
        add       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE] ;141.61
        mov       DWORD PTR [484+esp], ecx                      ;141.61
        mov       eax, DWORD PTR [72+ecx+edi]                   ;141.6
        add       eax, eax                                      ;141.61
        neg       eax                                           ;141.61
        mov       edi, DWORD PTR [40+ecx+edi]                   ;141.6
        movsx     ecx, WORD PTR [2+eax+edi]                     ;141.6
        test      ecx, ecx                                      ;141.61
        jle       .B2.248       ; Prob 16%                      ;141.61
                                ; LOE eax edx ebx esi edi
.B2.116:                        ; Preds .B2.115
        movsx     edi, WORD PTR [4+eax+edi]                     ;142.24
        mov       DWORD PTR [172+ebx+esi], edi                  ;142.5
        mov       edi, DWORD PTR [16+ebx+esi]                   ;153.3
        mov       eax, DWORD PTR [692+esp]                      ;153.3
        imul      eax, edi                                      ;153.3
        add       edx, eax                                      ;153.6
        sub       edx, DWORD PTR [496+esp]                      ;153.6
        mov       edx, DWORD PTR [edx]                          ;153.6
                                ; LOE edx ebx esi edi
.B2.117:                        ; Preds .B2.116 .B2.249 .B2.329
        imul      ecx, edx, 84                                  ;153.6
        mov       eax, DWORD PTR [484+esp]                      ;153.6
        movsx     eax, WORD PTR [36+eax+ecx]                    ;153.6
        test      eax, eax                                      ;153.72
        jle       .B2.242       ; Prob 16%                      ;153.72
                                ; LOE eax edx ebx esi edi
.B2.118:                        ; Preds .B2.242 .B2.117
        mov       DWORD PTR [168+ebx+esi], eax                  ;154.5
                                ; LOE esi edi
.B2.119:                        ; Preds .B2.328 .B2.118
        test      edi, edi                                      ;164.3
        jle       .B2.124       ; Prob 2%                       ;164.3
                                ; LOE esi edi
.B2.120:                        ; Preds .B2.119
        mov       eax, 1                                        ;
        mov       DWORD PTR [684+esp], edi                      ;
        mov       ebx, DWORD PTR [692+esp]                      ;
        mov       edi, DWORD PTR [480+esp]                      ;
        mov       DWORD PTR [524+esp], esi                      ;
        mov       esi, eax                                      ;
                                ; LOE ebx esi edi
.B2.121:                        ; Preds .B2.122 .B2.120
        mov       eax, DWORD PTR [ebx+edi]                      ;165.8
        test      eax, eax                                      ;165.30
        jle       .B2.239       ; Prob 16%                      ;165.30
                                ; LOE eax ebx esi edi
.B2.122:                        ; Preds .B2.121 .B2.241
        inc       esi                                           ;170.3
        add       ebx, DWORD PTR [692+esp]                      ;170.3
        cmp       esi, DWORD PTR [684+esp]                      ;170.3
        jle       .B2.121       ; Prob 82%                      ;170.3
                                ; LOE ebx esi edi
.B2.123:                        ; Preds .B2.122
        mov       edi, DWORD PTR [684+esp]                      ;
        mov       esi, DWORD PTR [524+esp]                      ;
                                ; LOE esi edi
.B2.124:                        ; Preds .B2.119 .B2.123
        dec       edi                                           ;172.3
        test      edi, edi                                      ;172.3
        jle       .B2.131       ; Prob 2%                       ;172.3
                                ; LOE esi edi
.B2.125:                        ; Preds .B2.124
        mov       DWORD PTR [684+esp], edi                      ;
        mov       eax, DWORD PTR [472+esp]                      ;
        mov       ebx, 1                                        ;
                                ; LOE eax ebx esi
.B2.126:                        ; Preds .B2.129 .B2.125
        imul      edi, eax, -176                                ;173.10
        mov       ecx, ebx                                      ;173.10
        add       edi, DWORD PTR [680+esp]                      ;173.10
        mov       eax, DWORD PTR [20+esi+edi]                   ;173.28
        mov       edx, DWORD PTR [52+esi+edi]                   ;173.28
        mov       esi, DWORD PTR [48+esi+edi]                   ;173.28
        imul      ecx, esi                                      ;173.10
        imul      edx, esi                                      ;173.10
        add       ecx, eax                                      ;173.10
        sub       ecx, edx                                      ;173.10
        mov       edi, DWORD PTR [ecx]                          ;173.10
        mov       DWORD PTR [660+esp], edi                      ;173.10
        lea       edi, DWORD PTR [1+ebx]                        ;173.51
        imul      esi, edi                                      ;173.51
        add       eax, esi                                      ;173.10
        sub       eax, edx                                      ;173.10
        mov       eax, DWORD PTR [eax]                          ;173.10
        mov       DWORD PTR [664+esp], eax                      ;173.10
        push      OFFSET FLAT: __NLITPACK_0.0.2                 ;173.10
        lea       edx, DWORD PTR [668+esp]                      ;173.10
        push      edx                                           ;173.10
        lea       ecx, DWORD PTR [668+esp]                      ;173.10
        push      ecx                                           ;173.10
        call      _DYNUST_NETWORK_MODULE_mp_GETFLINKFROMNODE    ;173.10
                                ; LOE eax ebx edi
.B2.301:                        ; Preds .B2.126
        add       esp, 12                                       ;173.10
                                ; LOE eax ebx edi
.B2.127:                        ; Preds .B2.301
        test      eax, eax                                      ;174.11
        jle       .B2.234       ; Prob 16%                      ;174.11
                                ; LOE ebx edi
.B2.128:                        ; Preds .B2.127
        mov       eax, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE+32] ;183.3
        mov       esi, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE] ;183.3
                                ; LOE eax esi edi
.B2.129:                        ; Preds .B2.238 .B2.128
        mov       ebx, edi                                      ;180.3
        cmp       edi, DWORD PTR [684+esp]                      ;180.3
        jle       .B2.126       ; Prob 82%                      ;180.3
                                ; LOE eax ebx esi
.B2.130:                        ; Preds .B2.129
        mov       DWORD PTR [472+esp], eax                      ;
                                ; LOE esi
.B2.131:                        ; Preds .B2.130 .B2.124
        imul      eax, DWORD PTR [472+esp], -176                ;183.3
        add       eax, DWORD PTR [680+esp]                      ;183.3
        mov       ecx, DWORD PTR [16+esi+eax]                   ;183.3
        test      ecx, ecx                                      ;183.3
        jle       .B2.143       ; Prob 50%                      ;183.3
                                ; LOE eax ecx esi
.B2.132:                        ; Preds .B2.131
        mov       edx, ecx                                      ;183.3
        xor       ebx, ebx                                      ;
        shr       edx, 31                                       ;183.3
        add       edx, ecx                                      ;183.3
        sar       edx, 1                                        ;183.3
        mov       DWORD PTR [556+esp], edx                      ;183.3
        test      edx, edx                                      ;183.3
        jbe       .B2.229       ; Prob 3%                       ;183.3
                                ; LOE eax ecx ebx esi
.B2.133:                        ; Preds .B2.132
        mov       DWORD PTR [428+esp], ebx                      ;
        mov       ebx, DWORD PTR [88+esi+eax]                   ;184.8
        mov       edx, DWORD PTR [84+esi+eax]                   ;184.8
        imul      ebx, edx                                      ;
        mov       DWORD PTR [420+esp], 0                        ;
        mov       edi, DWORD PTR [56+esi+eax]                   ;184.8
        mov       DWORD PTR [404+esp], ecx                      ;
        sub       edi, ebx                                      ;
        mov       DWORD PTR [548+esp], edx                      ;184.8
        mov       DWORD PTR [552+esp], edi                      ;
        mov       ebx, DWORD PTR [428+esp]                      ;
        mov       ecx, DWORD PTR [420+esp]                      ;
        mov       DWORD PTR [524+esp], esi                      ;
                                ; LOE eax ecx ebx
.B2.134:                        ; Preds .B2.138 .B2.133
        mov       edi, DWORD PTR [548+esp]                      ;184.31
        lea       edx, DWORD PTR [1+ecx+ecx]                    ;184.31
        imul      edi, edx                                      ;184.31
        mov       esi, DWORD PTR [552+esp]                      ;184.31
        cmp       BYTE PTR [esi+edi], 0                         ;184.31
        jle       .B2.136       ; Prob 16%                      ;184.31
                                ; LOE eax edx ecx ebx
.B2.135:                        ; Preds .B2.134
        mov       esi, DWORD PTR [524+esp]                      ;186.7
        inc       ebx                                           ;185.7
        sub       edx, DWORD PTR [160+esi+eax]                  ;186.7
        imul      edx, DWORD PTR [156+esi+eax]                  ;186.7
        mov       edi, DWORD PTR [128+esi+eax]                  ;186.7
        mov       BYTE PTR [edi+edx], bl                        ;186.7
                                ; LOE eax ecx ebx
.B2.136:                        ; Preds .B2.135 .B2.134
        mov       edi, DWORD PTR [548+esp]                      ;184.31
        lea       edx, DWORD PTR [2+ecx+ecx]                    ;184.31
        imul      edi, edx                                      ;184.31
        mov       esi, DWORD PTR [552+esp]                      ;184.31
        cmp       BYTE PTR [esi+edi], 0                         ;184.31
        jle       .B2.138       ; Prob 16%                      ;184.31
                                ; LOE eax edx ecx ebx
.B2.137:                        ; Preds .B2.136
        mov       esi, DWORD PTR [524+esp]                      ;186.7
        inc       ebx                                           ;185.7
        sub       edx, DWORD PTR [160+esi+eax]                  ;186.7
        imul      edx, DWORD PTR [156+esi+eax]                  ;186.7
        mov       edi, DWORD PTR [128+esi+eax]                  ;186.7
        mov       BYTE PTR [edi+edx], bl                        ;186.7
                                ; LOE eax ecx ebx
.B2.138:                        ; Preds .B2.137 .B2.136
        inc       ecx                                           ;183.3
        cmp       ecx, DWORD PTR [556+esp]                      ;183.3
        jb        .B2.134       ; Prob 64%                      ;183.3
                                ; LOE eax ecx ebx
.B2.139:                        ; Preds .B2.138
        mov       DWORD PTR [420+esp], ecx                      ;
        mov       edx, ecx                                      ;183.3
        mov       ecx, DWORD PTR [404+esp]                      ;
        mov       esi, DWORD PTR [524+esp]                      ;
        lea       edx, DWORD PTR [1+edx+edx]                    ;183.3
                                ; LOE eax edx ecx ebx esi
.B2.140:                        ; Preds .B2.139 .B2.229
        lea       edi, DWORD PTR [-1+edx]                       ;183.3
        cmp       ecx, edi                                      ;183.3
        jbe       .B2.144       ; Prob 3%                       ;183.3
                                ; LOE eax edx ebx esi
.B2.141:                        ; Preds .B2.140
        mov       edi, edx                                      ;184.31
        sub       edi, DWORD PTR [88+esi+eax]                   ;184.31
        imul      edi, DWORD PTR [84+esi+eax]                   ;184.31
        mov       ecx, DWORD PTR [56+esi+eax]                   ;184.8
        cmp       BYTE PTR [ecx+edi], 0                         ;184.31
        jle       .B2.144       ; Prob 16%                      ;184.31
                                ; LOE eax edx ebx esi
.B2.142:                        ; Preds .B2.141
        sub       edx, DWORD PTR [160+esi+eax]                  ;186.7
        inc       ebx                                           ;185.7
        imul      edx, DWORD PTR [156+esi+eax]                  ;186.7
        mov       eax, DWORD PTR [128+esi+eax]                  ;186.7
        mov       BYTE PTR [eax+edx], bl                        ;186.7
        jmp       .B2.144       ; Prob 100%                     ;186.7
                                ; LOE ebx
.B2.143:                        ; Preds .B2.131
        xor       ebx, ebx                                      ;
                                ; LOE ebx
.B2.144:                        ; Preds .B2.142 .B2.141 .B2.140 .B2.143
        imul      eax, DWORD PTR [468+esp], 44                  ;190.13
        imul      edx, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_SCHEDULE+32], -44 ;190.3
        mov       ecx, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_SCHEDULE] ;190.3
        add       ecx, eax                                      ;190.3
        add       ecx, edx                                      ;190.3
        mov       DWORD PTR [592+esp], 0                        ;190.3
        mov       DWORD PTR [756+esp], eax                      ;190.13
        mov       DWORD PTR [520+esp], ecx                      ;190.3
        push      32                                            ;190.3
        lea       esi, DWORD PTR [524+esp]                      ;190.3
        push      esi                                           ;190.3
        push      OFFSET FLAT: __STRLITPACK_115.0.2             ;190.3
        push      -2088435968                                   ;190.3
        push      1                                             ;190.3
        lea       edi, DWORD PTR [612+esp]                      ;190.3
        push      edi                                           ;190.3
        call      _for_read_seq_lis                             ;190.3
                                ; LOE ebx
.B2.145:                        ; Preds .B2.144
        imul      eax, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_SCHEDULE+32], 44 ;190.3
        lea       ecx, DWORD PTR [552+esp]                      ;190.3
        mov       edx, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_SCHEDULE] ;190.3
        add       eax, -4                                       ;190.3
        add       edx, DWORD PTR [780+esp]                      ;190.3
        sub       edx, eax                                      ;190.3
        mov       DWORD PTR [552+esp], edx                      ;190.3
        push      ecx                                           ;190.3
        push      OFFSET FLAT: __STRLITPACK_116.0.2             ;190.3
        lea       esi, DWORD PTR [624+esp]                      ;190.3
        push      esi                                           ;190.3
        call      _for_read_seq_lis_xmit                        ;190.3
                                ; LOE ebx
.B2.302:                        ; Preds .B2.145
        add       esp, 36                                       ;190.3
                                ; LOE ebx
.B2.146:                        ; Preds .B2.302
        imul      esi, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_SCHEDULE+32], -44 ;191.28
        mov       edi, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_SCHEDULE] ;191.3
        add       esi, DWORD PTR [676+esp]                      ;191.28
        mov       eax, DWORD PTR [4+esi+edi]                    ;191.6
        cmp       eax, ebx                                      ;191.28
        mov       DWORD PTR [492+esp], eax                      ;191.6
        je        .B2.159       ; Prob 50%                      ;191.28
                                ; LOE ebx esi edi
.B2.147:                        ; Preds .B2.146
        mov       eax, DWORD PTR [460+esp]                      ;192.5
        lea       edx, DWORD PTR [288+esp]                      ;192.5
        mov       DWORD PTR [592+esp], 0                        ;192.5
        mov       DWORD PTR [288+esp], eax                      ;192.5
        push      32                                            ;192.5
        push      OFFSET FLAT: DYNUST_TRANSIT_MODULE_mp_INPUTTRANSIT$format_pack.0.2+408 ;192.5
        push      edx                                           ;192.5
        push      OFFSET FLAT: __STRLITPACK_117.0.2             ;192.5
        push      -2088435968                                   ;192.5
        push      911                                           ;192.5
        lea       ecx, DWORD PTR [616+esp]                      ;192.5
        push      ecx                                           ;192.5
        call      _for_write_seq_fmt                            ;192.5
                                ; LOE ebx esi edi
.B2.148:                        ; Preds .B2.147
        imul      eax, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE+32], 176 ;193.5
        mov       ecx, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE] ;193.5
        mov       edx, DWORD PTR [700+esp]                      ;193.5
        mov       DWORD PTR [620+esp], 0                        ;193.5
        mov       DWORD PTR [204+esp], 6                        ;193.5
        mov       DWORD PTR [564+esp], eax                      ;193.5
        add       edx, ecx                                      ;193.5
        sub       edx, eax                                      ;193.5
        lea       eax, DWORD PTR [204+esp]                      ;193.5
        mov       DWORD PTR [560+esp], ecx                      ;193.5
        mov       DWORD PTR [208+esp], edx                      ;193.5
        push      32                                            ;193.5
        push      OFFSET FLAT: DYNUST_TRANSIT_MODULE_mp_INPUTTRANSIT$format_pack.0.2+460 ;193.5
        push      eax                                           ;193.5
        push      OFFSET FLAT: __STRLITPACK_118.0.2             ;193.5
        push      -2088435968                                   ;193.5
        push      911                                           ;193.5
        lea       ecx, DWORD PTR [644+esp]                      ;193.5
        push      ecx                                           ;193.5
        call      _for_write_seq_fmt                            ;193.5
                                ; LOE ebx esi edi
.B2.149:                        ; Preds .B2.148
        mov       DWORD PTR [648+esp], 0                        ;194.5
        lea       eax, DWORD PTR [352+esp]                      ;194.5
        mov       DWORD PTR [352+esp], ebx                      ;194.5
        push      32                                            ;194.5
        push      OFFSET FLAT: DYNUST_TRANSIT_MODULE_mp_INPUTTRANSIT$format_pack.0.2+488 ;194.5
        push      eax                                           ;194.5
        push      OFFSET FLAT: __STRLITPACK_119.0.2             ;194.5
        push      -2088435968                                   ;194.5
        push      911                                           ;194.5
        lea       edx, DWORD PTR [672+esp]                      ;194.5
        push      edx                                           ;194.5
        call      _for_write_seq_fmt                            ;194.5
                                ; LOE esi edi
.B2.150:                        ; Preds .B2.149
        mov       eax, DWORD PTR [576+esp]                      ;195.5
        lea       edx, DWORD PTR [388+esp]                      ;195.5
        mov       DWORD PTR [676+esp], 0                        ;195.5
        mov       DWORD PTR [388+esp], eax                      ;195.5
        push      32                                            ;195.5
        push      OFFSET FLAT: DYNUST_TRANSIT_MODULE_mp_INPUTTRANSIT$format_pack.0.2+548 ;195.5
        push      edx                                           ;195.5
        push      OFFSET FLAT: __STRLITPACK_120.0.2             ;195.5
        push      -2088435968                                   ;195.5
        push      911                                           ;195.5
        lea       ecx, DWORD PTR [700+esp]                      ;195.5
        push      ecx                                           ;195.5
        call      _for_write_seq_fmt                            ;195.5
                                ; LOE esi edi
.B2.303:                        ; Preds .B2.150
        add       esp, 112                                      ;195.5
                                ; LOE esi edi
.B2.151:                        ; Preds .B2.303
        xor       eax, eax                                      ;196.5
        mov       DWORD PTR [592+esp], eax                      ;196.5
        push      32                                            ;196.5
        push      OFFSET FLAT: DYNUST_TRANSIT_MODULE_mp_INPUTTRANSIT$format_pack.0.2+608 ;196.5
        push      eax                                           ;196.5
        push      OFFSET FLAT: __STRLITPACK_121.0.2             ;196.5
        push      -2088435968                                   ;196.5
        push      911                                           ;196.5
        lea       edx, DWORD PTR [616+esp]                      ;196.5
        push      edx                                           ;196.5
        call      _for_write_seq_fmt                            ;196.5
                                ; LOE esi edi
.B2.304:                        ; Preds .B2.151
        add       esp, 28                                       ;196.5
                                ; LOE esi edi
.B2.152:                        ; Preds .B2.304
        mov       eax, DWORD PTR [536+esp]                      ;197.5
        neg       eax                                           ;197.5
        add       eax, DWORD PTR [680+esp]                      ;197.5
        mov       edx, DWORD PTR [532+esp]                      ;197.5
        mov       DWORD PTR [536+esp], eax                      ;197.5
        mov       ecx, DWORD PTR [16+eax+edx]                   ;197.5
        test      ecx, ecx                                      ;197.5
        mov       DWORD PTR [560+esp], ecx                      ;197.5
        jle       .B2.158       ; Prob 2%                       ;197.5
                                ; LOE eax edx esi edi al dl ah dh
.B2.153:                        ; Preds .B2.152
        mov       ecx, edx                                      ;198.10
        mov       edx, eax                                      ;198.10
        mov       DWORD PTR [280+esp], 1                        ;
        mov       DWORD PTR [268+esp], esi                      ;
        mov       ebx, DWORD PTR [56+edx+ecx]                   ;198.10
        mov       eax, DWORD PTR [88+edx+ecx]                   ;198.10
        mov       edx, DWORD PTR [84+edx+ecx]                   ;198.10
        imul      eax, edx                                      ;
        sub       ebx, eax                                      ;
        imul      eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE+32], -44 ;
        add       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE] ;
        mov       DWORD PTR [276+esp], edx                      ;
        mov       DWORD PTR [272+esp], edi                      ;
        mov       DWORD PTR [516+esp], eax                      ;
        mov       DWORD PTR [540+esp], edx                      ;
        mov       esi, DWORD PTR [276+esp]                      ;
        mov       edi, DWORD PTR [280+esp]                      ;
                                ; LOE ebx esi edi
.B2.154:                        ; Preds .B2.156 .B2.153
        cmp       BYTE PTR [esi+ebx], 0                         ;198.36
        jle       .B2.156       ; Prob 16%                      ;198.36
                                ; LOE ebx esi edi
.B2.155:                        ; Preds .B2.154
        mov       edx, DWORD PTR [536+esp]                      ;198.41
        mov       eax, edi                                      ;198.41
        mov       ecx, DWORD PTR [532+esp]                      ;198.41
        mov       DWORD PTR [592+esp], 0                        ;198.41
        sub       eax, DWORD PTR [52+edx+ecx]                   ;198.41
        imul      eax, DWORD PTR [48+edx+ecx]                   ;198.41
        mov       edx, DWORD PTR [20+edx+ecx]                   ;198.41
        imul      ecx, DWORD PTR [edx+eax], 44                  ;198.54
        lea       edx, DWORD PTR [544+esp]                      ;198.41
        mov       eax, DWORD PTR [516+esp]                      ;198.41
        mov       eax, DWORD PTR [36+eax+ecx]                   ;198.41
        mov       DWORD PTR [544+esp], eax                      ;198.41
        push      32                                            ;198.41
        push      edx                                           ;198.41
        push      OFFSET FLAT: __STRLITPACK_122.0.2             ;198.41
        push      -2088435968                                   ;198.41
        push      911                                           ;198.41
        lea       ecx, DWORD PTR [612+esp]                      ;198.41
        push      ecx                                           ;198.41
        call      _for_write_seq_lis                            ;198.41
                                ; LOE ebx esi edi
.B2.305:                        ; Preds .B2.155
        add       esp, 24                                       ;198.41
                                ; LOE ebx esi edi
.B2.156:                        ; Preds .B2.305 .B2.154
        inc       edi                                           ;199.5
        add       esi, DWORD PTR [540+esp]                      ;199.5
        cmp       edi, DWORD PTR [560+esp]                      ;199.5
        jle       .B2.154       ; Prob 82%                      ;199.5
                                ; LOE ebx esi edi
.B2.157:                        ; Preds .B2.156
        mov       esi, DWORD PTR [268+esp]                      ;
        mov       edi, DWORD PTR [272+esp]                      ;
                                ; LOE esi edi
.B2.158:                        ; Preds .B2.157 .B2.152
        mov       DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_ROUTEERROR], -1 ;200.5
                                ; LOE esi edi
.B2.159:                        ; Preds .B2.146 .B2.158
        mov       ebx, DWORD PTR [esi+edi]                      ;203.3
        mov       ecx, 112                                      ;203.3
        xor       eax, eax                                      ;203.3
        test      ebx, ebx                                      ;203.3
        push      ecx                                           ;203.3
        cmovl     ebx, eax                                      ;203.3
        mov       edx, 1                                        ;203.3
        push      ebx                                           ;203.3
        push      2                                             ;203.3
        mov       DWORD PTR [20+esi+edi], 5                     ;203.3
        mov       DWORD PTR [12+esi+edi], ecx                   ;203.3
        mov       DWORD PTR [24+esi+edi], edx                   ;203.3
        mov       DWORD PTR [16+esi+edi], eax                   ;203.3
        mov       DWORD PTR [40+esi+edi], edx                   ;203.3
        mov       DWORD PTR [32+esi+edi], ebx                   ;203.3
        mov       DWORD PTR [36+esi+edi], ecx                   ;203.3
        lea       esi, DWORD PTR [468+esp]                      ;203.3
        push      esi                                           ;203.3
        call      _for_check_mult_overflow                      ;203.3
                                ; LOE eax
.B2.160:                        ; Preds .B2.159
        imul      edx, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_SCHEDULE+32], 44 ;203.3
        and       eax, 1                                        ;203.3
        mov       ecx, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_SCHEDULE] ;203.3
        add       edx, -8                                       ;203.3
        shl       eax, 4                                        ;203.3
        add       ecx, DWORD PTR [772+esp]                      ;203.3
        or        eax, 262144                                   ;203.3
        push      eax                                           ;203.3
        sub       ecx, edx                                      ;203.3
        push      ecx                                           ;203.3
        push      DWORD PTR [480+esp]                           ;203.3
        call      _for_allocate                                 ;203.3
                                ; LOE
.B2.307:                        ; Preds .B2.160
        add       esp, 28                                       ;203.3
                                ; LOE
.B2.161:                        ; Preds .B2.307
        mov       ecx, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_SCHEDULE+32] ;204.3
        imul      edx, ecx, -44                                 ;204.3
        mov       eax, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_SCHEDULE] ;204.3
        add       edx, DWORD PTR [676+esp]                      ;204.3
        mov       edx, DWORD PTR [edx+eax]                      ;204.3
        test      edx, edx                                      ;204.3
        jle       .B2.171       ; Prob 28%                      ;204.3
                                ; LOE eax edx ecx
.B2.162:                        ; Preds .B2.161
        mov       DWORD PTR [636+esp], edx                      ;
        mov       ebx, 1                                        ;
                                ; LOE eax ebx
.B2.163:                        ; Preds .B2.335 .B2.162
        imul      esi, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_SCHEDULE+32], -44 ;205.5
        xor       edx, edx                                      ;205.5
        add       esi, DWORD PTR [676+esp]                      ;205.5
        mov       edi, 1                                        ;205.5
        mov       DWORD PTR [640+esp], ebx                      ;
        sub       ebx, DWORD PTR [40+eax+esi]                   ;205.5
        mov       ecx, DWORD PTR [8+eax+esi]                    ;205.14
        imul      ebx, DWORD PTR [36+eax+esi]                   ;205.5
        mov       esi, DWORD PTR [4+eax+esi]                    ;205.5
        mov       eax, 4                                        ;205.5
        test      esi, esi                                      ;205.5
        push      eax                                           ;205.5
        cmovl     esi, edx                                      ;205.5
        push      esi                                           ;205.5
        push      2                                             ;205.5
        mov       DWORD PTR [28+ecx+ebx], eax                   ;205.5
        lea       eax, DWORD PTR [636+esp]                      ;205.5
        push      eax                                           ;205.5
        mov       DWORD PTR [12+ecx+ebx], 5                     ;205.5
        mov       DWORD PTR [4+ecx+ebx], 4                      ;205.5
        mov       DWORD PTR [16+ecx+ebx], edi                   ;205.5
        mov       DWORD PTR [8+ecx+ebx], edx                    ;205.5
        mov       DWORD PTR [32+ecx+ebx], edi                   ;205.5
        mov       DWORD PTR [24+ecx+ebx], esi                   ;205.5
        mov       ebx, DWORD PTR [656+esp]                      ;205.5
        call      _for_check_mult_overflow                      ;205.5
                                ; LOE eax ebx bl bh
.B2.164:                        ; Preds .B2.163
        imul      edx, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_SCHEDULE+32], 44 ;205.5
        and       eax, 1                                        ;205.5
        mov       ecx, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_SCHEDULE] ;205.5
        neg       edx                                           ;205.5
        add       ecx, DWORD PTR [772+esp]                      ;205.5
        shl       eax, 4                                        ;205.5
        or        eax, 262144                                   ;205.5
        mov       esi, DWORD PTR [40+edx+ecx]                   ;205.5
        neg       esi                                           ;205.5
        add       esi, ebx                                      ;205.5
        imul      esi, DWORD PTR [36+edx+ecx]                   ;205.5
        add       esi, DWORD PTR [8+edx+ecx]                    ;205.5
        push      eax                                           ;205.5
        push      esi                                           ;205.5
        push      DWORD PTR [648+esp]                           ;205.5
        call      _for_allocate                                 ;205.5
                                ; LOE ebx bl bh
.B2.165:                        ; Preds .B2.164
        imul      ecx, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_SCHEDULE+32], -44 ;206.5
        xor       eax, eax                                      ;206.5
        mov       esi, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_SCHEDULE] ;206.5
        mov       edi, 1                                        ;206.5
        add       ecx, DWORD PTR [704+esp]                      ;206.5
        mov       DWORD PTR [668+esp], ebx                      ;
        sub       ebx, DWORD PTR [40+esi+ecx]                   ;206.5
        imul      ebx, DWORD PTR [36+esi+ecx]                   ;206.5
        mov       edx, DWORD PTR [8+esi+ecx]                    ;206.14
        mov       ecx, DWORD PTR [4+esi+ecx]                    ;206.5
        test      ecx, ecx                                      ;206.5
        mov       DWORD PTR [44+edx+ebx], eax                   ;206.5
        cmovl     ecx, eax                                      ;206.5
        mov       eax, 4                                        ;206.5
        push      eax                                           ;206.5
        push      ecx                                           ;206.5
        push      2                                             ;206.5
        mov       DWORD PTR [48+edx+ebx], 5                     ;206.5
        mov       DWORD PTR [40+edx+ebx], 4                     ;206.5
        mov       DWORD PTR [52+edx+ebx], edi                   ;206.5
        mov       DWORD PTR [68+edx+ebx], edi                   ;206.5
        mov       DWORD PTR [60+edx+ebx], ecx                   ;206.5
        mov       DWORD PTR [64+edx+ebx], eax                   ;206.5
        lea       edx, DWORD PTR [668+esp]                      ;206.5
        push      edx                                           ;206.5
        mov       ebx, DWORD PTR [684+esp]                      ;206.5
        call      _for_check_mult_overflow                      ;206.5
                                ; LOE eax ebx bl bh
.B2.166:                        ; Preds .B2.165
        imul      edx, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_SCHEDULE+32], 44 ;206.5
        and       eax, 1                                        ;206.5
        mov       ecx, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_SCHEDULE] ;206.5
        neg       edx                                           ;206.5
        add       ecx, DWORD PTR [800+esp]                      ;206.5
        shl       eax, 4                                        ;206.5
        or        eax, 262144                                   ;206.5
        mov       esi, DWORD PTR [40+edx+ecx]                   ;206.5
        neg       esi                                           ;206.5
        add       esi, ebx                                      ;206.5
        imul      esi, DWORD PTR [36+edx+ecx]                   ;206.5
        mov       edi, DWORD PTR [8+edx+ecx]                    ;206.5
        push      eax                                           ;206.5
        lea       edx, DWORD PTR [36+esi+edi]                   ;206.5
        push      edx                                           ;206.5
        push      DWORD PTR [680+esp]                           ;206.5
        call      _for_allocate                                 ;206.5
                                ; LOE ebx bl bh
.B2.167:                        ; Preds .B2.166
        imul      ecx, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_SCHEDULE+32], -44 ;207.5
        xor       eax, eax                                      ;207.5
        mov       esi, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_SCHEDULE] ;207.5
        mov       edi, 1                                        ;207.5
        add       ecx, DWORD PTR [732+esp]                      ;207.5
        mov       DWORD PTR [696+esp], ebx                      ;
        sub       ebx, DWORD PTR [40+esi+ecx]                   ;207.5
        imul      ebx, DWORD PTR [36+esi+ecx]                   ;207.5
        mov       edx, DWORD PTR [8+esi+ecx]                    ;207.14
        mov       ecx, DWORD PTR [4+esi+ecx]                    ;207.5
        test      ecx, ecx                                      ;207.5
        mov       DWORD PTR [80+edx+ebx], eax                   ;207.5
        cmovl     ecx, eax                                      ;207.5
        mov       eax, 4                                        ;207.5
        push      eax                                           ;207.5
        push      ecx                                           ;207.5
        push      2                                             ;207.5
        mov       DWORD PTR [84+edx+ebx], 5                     ;207.5
        mov       DWORD PTR [76+edx+ebx], 4                     ;207.5
        mov       DWORD PTR [88+edx+ebx], edi                   ;207.5
        mov       DWORD PTR [104+edx+ebx], edi                  ;207.5
        mov       DWORD PTR [96+edx+ebx], ecx                   ;207.5
        mov       DWORD PTR [100+edx+ebx], eax                  ;207.5
        lea       edx, DWORD PTR [700+esp]                      ;207.5
        push      edx                                           ;207.5
        mov       ebx, DWORD PTR [712+esp]                      ;207.5
        call      _for_check_mult_overflow                      ;207.5
                                ; LOE eax ebx bl bh
.B2.168:                        ; Preds .B2.167
        imul      edx, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_SCHEDULE+32], 44 ;207.5
        and       eax, 1                                        ;207.5
        mov       ecx, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_SCHEDULE] ;207.5
        neg       edx                                           ;207.5
        add       ecx, DWORD PTR [828+esp]                      ;207.5
        shl       eax, 4                                        ;207.5
        or        eax, 262144                                   ;207.5
        mov       esi, DWORD PTR [40+edx+ecx]                   ;207.5
        neg       esi                                           ;207.5
        add       esi, ebx                                      ;207.5
        imul      esi, DWORD PTR [36+edx+ecx]                   ;207.5
        mov       edi, DWORD PTR [8+edx+ecx]                    ;207.5
        push      eax                                           ;207.5
        lea       edx, DWORD PTR [72+esi+edi]                   ;207.5
        push      edx                                           ;207.5
        push      DWORD PTR [712+esp]                           ;207.5
        call      _for_allocate                                 ;207.5
                                ; LOE ebx bl bh
.B2.311:                        ; Preds .B2.168
        add       esp, 84                                       ;207.5
                                ; LOE ebx bl bh
.B2.169:                        ; Preds .B2.311
        inc       ebx                                           ;208.3
        cmp       ebx, DWORD PTR [636+esp]                      ;208.3
        jg        .B2.170       ; Prob 18%                      ;208.3
                                ; LOE ebx
.B2.335:                        ; Preds .B2.169
        mov       eax, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_SCHEDULE] ;190.3
        jmp       .B2.163       ; Prob 100%                     ;190.3
                                ; LOE eax ebx
.B2.170:                        ; Preds .B2.169
        mov       ecx, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_SCHEDULE+32] ;210.3
        mov       eax, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_SCHEDULE] ;210.3
                                ; LOE eax ecx
.B2.171:                        ; Preds .B2.170 .B2.161
        imul      esi, ecx, -44                                 ;210.3
        add       eax, esi                                      ;210.3
        mov       edi, DWORD PTR [676+esp]                      ;210.3
        mov       ecx, DWORD PTR [eax+edi]                      ;210.3
        xor       eax, eax                                      ;
        xor       esi, esi                                      ;
        test      ecx, ecx                                      ;210.3
        jle       .B2.214       ; Prob 3%                       ;210.3
                                ; LOE ecx esi
.B2.172:                        ; Preds .B2.171
        xor       edx, edx                                      ;
        mov       DWORD PTR [764+esp], esi                      ;
        mov       DWORD PTR [772+esp], edx                      ;
        mov       DWORD PTR [688+esp], ecx                      ;
                                ; LOE
.B2.173:                        ; Preds .B2.212 .B2.172
        xor       eax, eax                                      ;212.5
        mov       DWORD PTR [592+esp], eax                      ;212.5
        push      32                                            ;212.5
        push      eax                                           ;212.5
        push      OFFSET FLAT: __STRLITPACK_123.0.2             ;212.5
        push      -2088435968                                   ;212.5
        push      1                                             ;212.5
        inc       DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_VEHGENID] ;211.5
        lea       edx, DWORD PTR [612+esp]                      ;212.5
        push      edx                                           ;212.5
        call      _for_read_seq_lis                             ;212.5
                                ; LOE
.B2.312:                        ; Preds .B2.173
        add       esp, 24                                       ;212.5
                                ; LOE
.B2.174:                        ; Preds .B2.312
        imul      eax, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_SCHEDULE+32], -44 ;212.5
        add       eax, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_SCHEDULE] ;212.5
        mov       edx, DWORD PTR [676+esp]                      ;212.5
        mov       edx, DWORD PTR [4+eax+edx]                    ;212.5
        test      edx, edx                                      ;212.5
        jle       .B2.179       ; Prob 3%                       ;212.5
                                ; LOE edx
.B2.175:                        ; Preds .B2.174
        mov       DWORD PTR [780+esp], edx                      ;
        xor       ebx, ebx                                      ;
        lea       esi, DWORD PTR [592+esp]                      ;
                                ; LOE ebx esi
.B2.176:                        ; Preds .B2.177 .B2.175
        imul      ecx, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_SCHEDULE+32], 44 ;212.5
        inc       ebx                                           ;212.5
        mov       edi, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_SCHEDULE] ;212.5
        neg       ecx                                           ;212.5
        add       edi, DWORD PTR [756+esp]                      ;212.5
        mov       DWORD PTR [784+esp], 4                        ;212.5
        mov       eax, DWORD PTR [40+ecx+edi]                   ;212.16
        neg       eax                                           ;212.5
        add       eax, DWORD PTR [772+esp]                      ;212.5
        mov       edx, DWORD PTR [36+ecx+edi]                   ;212.16
        imul      eax, edx                                      ;212.5
        add       edx, DWORD PTR [8+ecx+edi]                    ;212.5
        mov       ecx, DWORD PTR [32+edx+eax]                   ;212.16
        neg       ecx                                           ;212.5
        add       ecx, ebx                                      ;212.5
        imul      ecx, DWORD PTR [28+edx+eax]                   ;212.5
        add       ecx, DWORD PTR [edx+eax]                      ;212.5
        lea       eax, DWORD PTR [784+esp]                      ;212.5
        mov       DWORD PTR [788+esp], ecx                      ;212.5
        push      eax                                           ;212.5
        push      OFFSET FLAT: __STRLITPACK_124.0.2             ;212.5
        push      esi                                           ;212.5
        call      _for_read_seq_lis_xmit                        ;212.5
                                ; LOE ebx esi
.B2.313:                        ; Preds .B2.176
        add       esp, 12                                       ;212.5
                                ; LOE ebx esi
.B2.177:                        ; Preds .B2.313
        cmp       ebx, DWORD PTR [780+esp]                      ;212.5
        jb        .B2.176       ; Prob 82%                      ;212.5
                                ; LOE ebx esi
.B2.179:                        ; Preds .B2.177 .B2.174
        push      0                                             ;212.5
        push      OFFSET FLAT: __STRLITPACK_125.0.2             ;212.5
        lea       eax, DWORD PTR [600+esp]                      ;212.5
        push      eax                                           ;212.5
        call      _for_read_seq_lis_xmit                        ;212.5
                                ; LOE
.B2.314:                        ; Preds .B2.179
        add       esp, 12                                       ;212.5
                                ; LOE
.B2.180:                        ; Preds .B2.314
        imul      edx, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_SCHEDULE+32], -44 ;213.5
        mov       eax, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_SCHEDULE] ;213.5
        add       edx, DWORD PTR [676+esp]                      ;213.5
        mov       ecx, DWORD PTR [4+edx+eax]                    ;213.5
        test      ecx, ecx                                      ;213.5
        jle       .B2.196       ; Prob 50%                      ;213.5
                                ; LOE eax edx ecx
.B2.181:                        ; Preds .B2.180
        mov       edi, DWORD PTR [772+esp]                      ;214.7
        mov       ebx, DWORD PTR [40+edx+eax]                   ;214.138
        neg       ebx                                           ;214.7
        mov       esi, DWORD PTR [8+edx+eax]                    ;214.138
        lea       edi, DWORD PTR [1+edi]                        ;214.7
        add       ebx, edi                                      ;214.7
        imul      ebx, DWORD PTR [36+edx+eax]                   ;214.7
        movss     xmm4, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_SIMSTARTTIME] ;215.88
        movss     xmm3, DWORD PTR [_2il0floatpacket.3]          ;214.177
        mov       eax, DWORD PTR [esi+ebx]                      ;214.138
        mov       edx, DWORD PTR [32+esi+ebx]                   ;214.138
        mov       esi, DWORD PTR [28+esi+ebx]                   ;214.138
        imul      edx, esi                                      ;214.7
        sub       eax, edx                                      ;214.7
        mov       DWORD PTR [708+esp], edi                      ;214.7
        movss     xmm1, DWORD PTR [eax+esi]                     ;214.133
        cvttss2si edi, xmm1                                     ;214.47
        mov       ebx, edi                                      ;214.90
        lea       edx, DWORD PTR [edi*4]                        ;214.90
        shl       ebx, 6                                        ;214.90
        cvtsi2ss  xmm0, edi                                     ;214.133
        sub       ebx, edx                                      ;214.90
        mov       edi, 2                                        ;226.5
        cvtsi2ss  xmm5, ebx                                     ;214.90
        cvttss2si ebx, xmm4                                     ;215.124
        subss     xmm1, xmm0                                    ;214.132
        cvtsi2ss  xmm2, ebx                                     ;215.124
        mulss     xmm1, xmm3                                    ;214.177
        subss     xmm4, xmm2                                    ;215.123
        addss     xmm5, xmm1                                    ;214.7
        mulss     xmm4, xmm3                                    ;215.143
        mov       DWORD PTR [832+esp], edi                      ;226.5
        lea       edx, DWORD PTR [ebx*4]                        ;215.106
        shl       ebx, 6                                        ;215.106
        sub       ebx, edx                                      ;215.106
        lea       edx, DWORD PTR [-1+ecx]                       ;213.5
        cvtsi2ss  xmm0, ebx                                     ;215.106
        mov       DWORD PTR [852+esp], edx                      ;213.5
        cmp       ecx, 2                                        ;213.5
        subss     xmm5, xmm0                                    ;215.7
        subss     xmm5, xmm4                                    ;215.7
        movss     DWORD PTR [eax+esi], xmm5                     ;215.7
        jl        .B2.230       ; Prob 3%                       ;213.5
                                ; LOE eax esi xmm0 xmm4 xmm5
.B2.182:                        ; Preds .B2.181
        imul      ecx, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE+32], 176 ;
        xor       edx, edx                                      ;
        movss     DWORD PTR [812+esp], xmm0                     ;
        movss     DWORD PTR [808+esp], xmm4                     ;
        mov       ebx, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE] ;219.11
        mov       edi, DWORD PTR [672+esp]                      ;
        mov       DWORD PTR [848+esp], edx                      ;
        mov       DWORD PTR [652+esp], ebx                      ;
        mov       DWORD PTR [844+esp], edx                      ;
        add       edi, ebx                                      ;
        sub       edi, ecx                                      ;
        mov       DWORD PTR [752+esp], edi                      ;
        mov       DWORD PTR [656+esp], ecx                      ;
        mov       DWORD PTR [820+esp], eax                      ;
        mov       DWORD PTR [816+esp], esi                      ;
        mov       ebx, DWORD PTR [848+esp]                      ;
                                ; LOE ebx
.B2.183:                        ; Preds .B2.194 .B2.182
        mov       DWORD PTR [848+esp], ebx                      ;
        lea       edx, DWORD PTR [2+ebx]                        ;214.7
        mov       ebx, DWORD PTR [816+esp]                      ;214.7
        mov       esi, ebx                                      ;214.7
        imul      esi, edx                                      ;214.7
        mov       eax, DWORD PTR [820+esp]                      ;214.47
        mov       DWORD PTR [824+esp], edx                      ;214.7
        movss     xmm1, DWORD PTR [eax+esi]                     ;214.47
        cvttss2si edx, xmm1                                     ;214.133
        cvtsi2ss  xmm0, edx                                     ;214.133
        mov       edi, edx                                      ;214.90
        lea       ecx, DWORD PTR [edx*4]                        ;214.90
        shl       edi, 6                                        ;214.90
        subss     xmm1, xmm0                                    ;214.132
        sub       edi, ecx                                      ;214.90
        cvtsi2ss  xmm3, edi                                     ;214.90
        mulss     xmm1, DWORD PTR [_2il0floatpacket.3]          ;214.177
        mov       ecx, DWORD PTR [844+esp]                      ;217.52
        addss     xmm3, xmm1                                    ;214.7
        add       ecx, ebx                                      ;217.52
        subss     xmm3, DWORD PTR [812+esp]                     ;215.7
        mov       ebx, DWORD PTR [848+esp]                      ;217.52
        mov       DWORD PTR [844+esp], ecx                      ;217.52
        subss     xmm3, DWORD PTR [808+esp]                     ;215.7
        movss     DWORD PTR [eax+esi], xmm3                     ;215.7
        lea       esi, DWORD PTR [1+ebx]                        ;217.52
        movss     xmm2, DWORD PTR [ecx+eax]                     ;217.52
        movss     DWORD PTR [828+esp], xmm3                     ;215.7
        comiss    xmm2, xmm3                                    ;217.50
        jbe       .B2.194       ; Prob 50%                      ;217.50
                                ; LOE ebx esi bl bh
.B2.184:                        ; Preds .B2.183
        mov       eax, DWORD PTR [832+esp]                      ;213.5
        lea       edx, DWORD PTR [712+esp]                      ;218.11
        mov       DWORD PTR [592+esp], 0                        ;218.11
        mov       DWORD PTR [712+esp], 25                       ;218.11
        mov       DWORD PTR [716+esp], OFFSET FLAT: __STRLITPACK_23 ;218.11
        push      32                                            ;218.11
        push      edx                                           ;218.11
        push      OFFSET FLAT: __STRLITPACK_126.0.2             ;218.11
        push      -2088435968                                   ;218.11
        push      911                                           ;218.11
        mov       DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_INPUTTRANSIT$H.0.2], eax ;213.5
        lea       ecx, DWORD PTR [612+esp]                      ;218.11
        push      ecx                                           ;218.11
        call      _for_write_seq_lis                            ;218.11
                                ; LOE ebx esi bl bh
.B2.185:                        ; Preds .B2.184
        mov       DWORD PTR [616+esp], 0                        ;219.11
        lea       eax, DWORD PTR [744+esp]                      ;219.11
        mov       DWORD PTR [744+esp], 9                        ;219.11
        mov       DWORD PTR [748+esp], OFFSET FLAT: __STRLITPACK_21 ;219.11
        push      32                                            ;219.11
        push      eax                                           ;219.11
        push      OFFSET FLAT: __STRLITPACK_127.0.2             ;219.11
        push      -2088435968                                   ;219.11
        push      911                                           ;219.11
        lea       edx, DWORD PTR [636+esp]                      ;219.11
        push      edx                                           ;219.11
        call      _for_write_seq_lis                            ;219.11
                                ; LOE ebx esi bl bh
.B2.186:                        ; Preds .B2.185
        mov       eax, DWORD PTR [800+esp]                      ;219.11
        lea       edx, DWORD PTR [776+esp]                      ;219.11
        mov       DWORD PTR [776+esp], 6                        ;219.11
        mov       DWORD PTR [780+esp], eax                      ;219.11
        push      edx                                           ;219.11
        push      OFFSET FLAT: __STRLITPACK_128.0.2             ;219.11
        lea       ecx, DWORD PTR [648+esp]                      ;219.11
        push      ecx                                           ;219.11
        call      _for_write_seq_lis_xmit                       ;219.11
                                ; LOE ebx esi bl bh
.B2.187:                        ; Preds .B2.186
        mov       DWORD PTR [652+esp], 0                        ;220.11
        lea       eax, DWORD PTR [796+esp]                      ;220.11
        mov       DWORD PTR [796+esp], 11                       ;220.11
        mov       DWORD PTR [800+esp], OFFSET FLAT: __STRLITPACK_19 ;220.11
        push      32                                            ;220.11
        push      eax                                           ;220.11
        push      OFFSET FLAT: __STRLITPACK_129.0.2             ;220.11
        push      -2088435968                                   ;220.11
        push      911                                           ;220.11
        lea       edx, DWORD PTR [672+esp]                      ;220.11
        push      edx                                           ;220.11
        call      _for_write_seq_lis                            ;220.11
                                ; LOE ebx esi bl bh
.B2.188:                        ; Preds .B2.187
        mov       eax, DWORD PTR [792+esp]                      ;220.11
        lea       edx, DWORD PTR [844+esp]                      ;220.11
        mov       DWORD PTR [844+esp], eax                      ;220.11
        push      edx                                           ;220.11
        push      OFFSET FLAT: __STRLITPACK_130.0.2             ;220.11
        lea       ecx, DWORD PTR [684+esp]                      ;220.11
        push      ecx                                           ;220.11
        call      _for_write_seq_lis_xmit                       ;220.11
                                ; LOE ebx esi bl bh
.B2.189:                        ; Preds .B2.188
        mov       DWORD PTR [688+esp], 0                        ;221.11
        lea       eax, DWORD PTR [840+esp]                      ;221.11
        mov       DWORD PTR [840+esp], 13                       ;221.11
        mov       DWORD PTR [844+esp], OFFSET FLAT: __STRLITPACK_17 ;221.11
        push      32                                            ;221.11
        push      eax                                           ;221.11
        push      OFFSET FLAT: __STRLITPACK_131.0.2             ;221.11
        push      -2088435968                                   ;221.11
        push      911                                           ;221.11
        lea       edx, DWORD PTR [708+esp]                      ;221.11
        push      edx                                           ;221.11
        call      _for_write_seq_lis                            ;221.11
                                ; LOE ebx esi bl bh
.B2.315:                        ; Preds .B2.189
        add       esp, 120                                      ;221.11
                                ; LOE ebx esi bl bh
.B2.190:                        ; Preds .B2.315
        mov       eax, DWORD PTR [824+esp]                      ;221.11
        lea       edx, DWORD PTR [768+esp]                      ;221.11
        mov       DWORD PTR [768+esp], eax                      ;221.11
        push      edx                                           ;221.11
        push      OFFSET FLAT: __STRLITPACK_132.0.2             ;221.11
        lea       ecx, DWORD PTR [600+esp]                      ;221.11
        push      ecx                                           ;221.11
        call      _for_write_seq_lis_xmit                       ;221.11
                                ; LOE ebx esi bl bh
.B2.191:                        ; Preds .B2.190
        movss     xmm0, DWORD PTR [840+esp]                     ;221.11
        lea       eax, DWORD PTR [788+esp]                      ;221.11
        movss     DWORD PTR [788+esp], xmm0                     ;221.11
        push      eax                                           ;221.11
        push      OFFSET FLAT: __STRLITPACK_133.0.2             ;221.11
        lea       edx, DWORD PTR [612+esp]                      ;221.11
        push      edx                                           ;221.11
        call      _for_write_seq_lis_xmit                       ;221.11
                                ; LOE ebx esi bl bh
.B2.192:                        ; Preds .B2.191
        push      32                                            ;223.11
        xor       eax, eax                                      ;223.11
        push      eax                                           ;223.11
        push      eax                                           ;223.11
        push      -2088435968                                   ;223.11
        push      eax                                           ;223.11
        push      OFFSET FLAT: __STRLITPACK_134                 ;223.11
        call      _for_stop_core                                ;223.11
                                ; LOE ebx esi bl bh
.B2.316:                        ; Preds .B2.192
        add       esp, 48                                       ;223.11
                                ; LOE ebx esi bl bh
.B2.193:                        ; Preds .B2.316
        mov       DWORD PTR [764+esp], -1                       ;
                                ; LOE ebx esi bl bh
.B2.194:                        ; Preds .B2.193 .B2.183
        cmp       esi, DWORD PTR [852+esp]                      ;213.5
        lea       eax, DWORD PTR [3+ebx]                        ;226.5
        mov       DWORD PTR [832+esp], eax                      ;226.5
        mov       ebx, esi                                      ;213.5
        jb        .B2.183       ; Prob 82%                      ;213.5
                                ; LOE ebx
.B2.195:                        ; Preds .B2.194
        mov       eax, DWORD PTR [820+esp]                      ;
        mov       esi, DWORD PTR [816+esp]                      ;
        mov       edx, DWORD PTR [832+esp]                      ;213.5
        mov       ebx, DWORD PTR [652+esp]                      ;
        mov       ecx, DWORD PTR [656+esp]                      ;
        mov       DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_INPUTTRANSIT$H.0.2], edx ;213.5
        movss     xmm5, DWORD PTR [eax+esi]                     ;227.32
        jmp       .B2.197       ; Prob 100%                     ;227.32
                                ; LOE ecx ebx xmm5
.B2.196:                        ; Preds .B2.180
        mov       edi, DWORD PTR [772+esp]                      ;
        mov       ebx, DWORD PTR [40+edx+eax]                   ;227.32
        neg       ebx                                           ;
        mov       esi, DWORD PTR [8+edx+eax]                    ;227.32
        lea       edi, DWORD PTR [1+edi]                        ;
        add       ebx, edi                                      ;
        imul      ebx, DWORD PTR [36+edx+eax]                   ;
        imul      ecx, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE+32], 176 ;
        mov       edx, DWORD PTR [esi+ebx]                      ;227.32
        mov       eax, DWORD PTR [32+esi+ebx]                   ;227.32
        mov       ebx, DWORD PTR [28+esi+ebx]                   ;227.32
        imul      eax, ebx                                      ;227.32
        sub       edx, eax                                      ;227.32
        mov       DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_INPUTTRANSIT$H.0.2], 1 ;213.5
        mov       DWORD PTR [708+esp], edi                      ;
        movss     xmm5, DWORD PTR [edx+ebx]                     ;227.32
        mov       ebx, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE] ;227.32
                                ; LOE ecx ebx xmm5
.B2.197:                        ; Preds .B2.195 .B2.230 .B2.196
        neg       ecx                                           ;228.5
        mulss     xmm5, DWORD PTR [_2il0floatpacket.3]          ;227.69
        add       ecx, DWORD PTR [680+esp]                      ;228.5
        cvttss2si edi, xmm5                                     ;227.5
        mov       eax, DWORD PTR [164+ecx+ebx]                  ;228.5
        mov       edx, DWORD PTR [8+ecx+ebx]                    ;229.5
        xor       ebx, ebx                                      ;232.5
        mov       ecx, DWORD PTR [708+esp]                      ;230.5
        mov       DWORD PTR [592+esp], ebx                      ;232.5
        push      32                                            ;232.5
        push      ebx                                           ;232.5
        push      OFFSET FLAT: __STRLITPACK_135.0.2             ;232.5
        mov       esi, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_VEHGENID] ;227.5
        push      -2088435968                                   ;232.5
        push      1                                             ;232.5
        mov       DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_TRANSITREPOS+3996+esi*4], eax ;228.5
        lea       eax, DWORD PTR [612+esp]                      ;232.5
        push      eax                                           ;232.5
        mov       DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_TRANSITREPOS-4+esi*4], edi ;227.5
        mov       DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_TRANSITREPOS+7996+esi*4], edx ;229.5
        mov       DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_TRANSITREPOS+11996+esi*4], ecx ;230.5
        mov       DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_TRANSITREPOS+15996+esi*4], esi ;231.5
        call      _for_read_seq_lis                             ;232.5
                                ; LOE
.B2.317:                        ; Preds .B2.197
        add       esp, 24                                       ;232.5
                                ; LOE
.B2.198:                        ; Preds .B2.317
        imul      eax, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_SCHEDULE+32], -44 ;232.5
        add       eax, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_SCHEDULE] ;232.5
        mov       edx, DWORD PTR [676+esp]                      ;232.5
        mov       edx, DWORD PTR [4+eax+edx]                    ;232.5
        test      edx, edx                                      ;232.5
        jle       .B2.203       ; Prob 3%                       ;232.5
                                ; LOE edx
.B2.199:                        ; Preds .B2.198
        mov       DWORD PTR [836+esp], edx                      ;
        xor       ebx, ebx                                      ;
        lea       esi, DWORD PTR [592+esp]                      ;
                                ; LOE ebx esi
.B2.200:                        ; Preds .B2.201 .B2.199
        imul      ecx, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_SCHEDULE+32], 44 ;232.5
        inc       ebx                                           ;232.5
        mov       edi, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_SCHEDULE] ;232.5
        neg       ecx                                           ;232.5
        add       edi, DWORD PTR [756+esp]                      ;232.5
        mov       DWORD PTR [792+esp], 4                        ;232.5
        mov       eax, DWORD PTR [40+ecx+edi]                   ;232.16
        neg       eax                                           ;232.5
        add       eax, DWORD PTR [772+esp]                      ;232.5
        mov       edx, DWORD PTR [36+ecx+edi]                   ;232.16
        imul      eax, edx                                      ;232.5
        add       edx, DWORD PTR [8+ecx+edi]                    ;232.5
        mov       ecx, DWORD PTR [68+edx+eax]                   ;232.16
        neg       ecx                                           ;232.5
        add       ecx, ebx                                      ;232.5
        imul      ecx, DWORD PTR [64+edx+eax]                   ;232.5
        add       ecx, DWORD PTR [36+edx+eax]                   ;232.5
        lea       eax, DWORD PTR [792+esp]                      ;232.5
        mov       DWORD PTR [796+esp], ecx                      ;232.5
        push      eax                                           ;232.5
        push      OFFSET FLAT: __STRLITPACK_136.0.2             ;232.5
        push      esi                                           ;232.5
        call      _for_read_seq_lis_xmit                        ;232.5
                                ; LOE ebx esi
.B2.318:                        ; Preds .B2.200
        add       esp, 12                                       ;232.5
                                ; LOE ebx esi
.B2.201:                        ; Preds .B2.318
        cmp       ebx, DWORD PTR [836+esp]                      ;232.5
        jb        .B2.200       ; Prob 82%                      ;232.5
                                ; LOE ebx esi
.B2.203:                        ; Preds .B2.201 .B2.198
        push      0                                             ;232.5
        push      OFFSET FLAT: __STRLITPACK_137.0.2             ;232.5
        lea       eax, DWORD PTR [600+esp]                      ;232.5
        push      eax                                           ;232.5
        call      _for_read_seq_lis_xmit                        ;232.5
                                ; LOE
.B2.204:                        ; Preds .B2.203
        xor       eax, eax                                      ;233.5
        mov       DWORD PTR [604+esp], eax                      ;233.5
        push      32                                            ;233.5
        push      eax                                           ;233.5
        push      OFFSET FLAT: __STRLITPACK_138.0.2             ;233.5
        push      -2088435968                                   ;233.5
        push      1                                             ;233.5
        lea       edx, DWORD PTR [624+esp]                      ;233.5
        push      edx                                           ;233.5
        call      _for_read_seq_lis                             ;233.5
                                ; LOE
.B2.319:                        ; Preds .B2.204
        add       esp, 36                                       ;233.5
                                ; LOE
.B2.205:                        ; Preds .B2.319
        imul      eax, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_SCHEDULE+32], -44 ;233.5
        add       eax, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_SCHEDULE] ;233.5
        mov       edx, DWORD PTR [676+esp]                      ;233.5
        mov       edx, DWORD PTR [4+eax+edx]                    ;233.5
        test      edx, edx                                      ;233.5
        jle       .B2.210       ; Prob 3%                       ;233.5
                                ; LOE edx
.B2.206:                        ; Preds .B2.205
        mov       DWORD PTR [840+esp], edx                      ;
        xor       ebx, ebx                                      ;
        mov       esi, DWORD PTR [772+esp]                      ;
                                ; LOE ebx esi
.B2.207:                        ; Preds .B2.208 .B2.206
        imul      ecx, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_SCHEDULE+32], 44 ;233.5
        inc       ebx                                           ;233.5
        mov       edi, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_SCHEDULE] ;233.5
        neg       ecx                                           ;233.5
        add       edi, DWORD PTR [756+esp]                      ;233.5
        mov       DWORD PTR [800+esp], 4                        ;233.5
        mov       eax, DWORD PTR [40+ecx+edi]                   ;233.16
        neg       eax                                           ;233.5
        add       eax, esi                                      ;233.5
        mov       edx, DWORD PTR [36+ecx+edi]                   ;233.16
        imul      eax, edx                                      ;233.5
        add       edx, DWORD PTR [8+ecx+edi]                    ;233.5
        mov       ecx, DWORD PTR [104+edx+eax]                  ;233.16
        neg       ecx                                           ;233.5
        add       ecx, ebx                                      ;233.5
        imul      ecx, DWORD PTR [100+edx+eax]                  ;233.5
        add       ecx, DWORD PTR [72+edx+eax]                   ;233.5
        lea       eax, DWORD PTR [800+esp]                      ;233.5
        mov       DWORD PTR [804+esp], ecx                      ;233.5
        push      eax                                           ;233.5
        push      OFFSET FLAT: __STRLITPACK_139.0.2             ;233.5
        lea       edx, DWORD PTR [600+esp]                      ;233.5
        push      edx                                           ;233.5
        call      _for_read_seq_lis_xmit                        ;233.5
                                ; LOE ebx esi
.B2.320:                        ; Preds .B2.207
        add       esp, 12                                       ;233.5
                                ; LOE ebx esi
.B2.208:                        ; Preds .B2.320
        cmp       ebx, DWORD PTR [840+esp]                      ;233.5
        jb        .B2.207       ; Prob 82%                      ;233.5
                                ; LOE ebx esi
.B2.210:                        ; Preds .B2.208 .B2.205
        push      0                                             ;233.5
        push      OFFSET FLAT: __STRLITPACK_140.0.2             ;233.5
        lea       eax, DWORD PTR [600+esp]                      ;233.5
        push      eax                                           ;233.5
        call      _for_read_seq_lis_xmit                        ;233.5
                                ; LOE
.B2.211:                        ; Preds .B2.210
        xor       eax, eax                                      ;234.5
        mov       DWORD PTR [604+esp], eax                      ;234.5
        push      32                                            ;234.5
        push      eax                                           ;234.5
        push      OFFSET FLAT: __STRLITPACK_141.0.2             ;234.5
        push      -2088435968                                   ;234.5
        push      1                                             ;234.5
        lea       edx, DWORD PTR [624+esp]                      ;234.5
        push      edx                                           ;234.5
        call      _for_read_seq_lis                             ;234.5
                                ; LOE
.B2.321:                        ; Preds .B2.211
        add       esp, 36                                       ;234.5
                                ; LOE
.B2.212:                        ; Preds .B2.321
        mov       eax, DWORD PTR [708+esp]                      ;210.3
        mov       DWORD PTR [772+esp], eax                      ;210.3
        cmp       eax, DWORD PTR [688+esp]                      ;210.3
        jb        .B2.173       ; Prob 82%                      ;210.3
                                ; LOE
.B2.213:                        ; Preds .B2.212
        mov       esi, DWORD PTR [764+esp]                      ;
                                ; LOE esi
.B2.214:                        ; Preds .B2.171 .B2.213
        mov       ebx, DWORD PTR [460+esp]                      ;104.4
        inc       ebx                                           ;104.4
        mov       edx, DWORD PTR [680+esp]                      ;104.4
        mov       ecx, DWORD PTR [676+esp]                      ;104.4
        add       edx, 176                                      ;104.4
        add       ecx, 44                                       ;104.4
        mov       DWORD PTR [680+esp], edx                      ;104.4
        mov       DWORD PTR [676+esp], ecx                      ;104.4
        mov       DWORD PTR [460+esp], ebx                      ;104.4
        mov       DWORD PTR [468+esp], ebx                      ;104.4
        mov       DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_INPUTTRANSIT$I.0.2], ebx ;104.4
        cmp       ebx, DWORD PTR [464+esp]                      ;104.4
        jle       .B2.48        ; Prob 82%                      ;104.4
                                ; LOE esi
.B2.215:                        ; Preds .B2.214
        mov       ebx, DWORD PTR [668+esp]                      ;
                                ; LOE ebx esi
.B2.216:                        ; Preds .B2.215 .B2.269
        test      BYTE PTR [_DYNUST_TRANSIT_MODULE_mp_ROUTEERROR], 1 ;243.4
        jne       .B2.233       ; Prob 3%                       ;243.4
                                ; LOE ebx esi
.B2.217:                        ; Preds .B2.325 .B2.216
        test      bl, 1                                         ;244.4
        jne       .B2.232       ; Prob 3%                       ;244.4
                                ; LOE esi
.B2.218:                        ; Preds .B2.324 .B2.217
        test      esi, 1                                        ;245.4
        jne       .B2.231       ; Prob 3%                       ;245.4
                                ; LOE
.B2.219:                        ; Preds .B2.323 .B2.218 .B2.2
        push      OFFSET FLAT: __NLITPACK_2.0.2                 ;249.6
        push      OFFSET FLAT: __NLITPACK_1.0.2                 ;249.6
        push      OFFSET FLAT: _DYNUST_TRANSIT_MODULE_mp_TRANSITREPOS ;249.6
        call      _IMSLSORT                                     ;249.6
                                ; LOE
.B2.322:                        ; Preds .B2.219
        add       esp, 12                                       ;249.6
                                ; LOE
.B2.220:                        ; Preds .B2.322
        mov       eax, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_TRANSITREPOSPNTR] ;250.1
        inc       eax                                           ;250.1
        cmp       DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_TRANSITREPOS-4+eax*4], 0 ;251.43
        jge       .B2.224       ; Prob 36%                      ;251.43
                                ; LOE eax
.B2.223:                        ; Preds .B2.220 .B2.223
        inc       eax                                           ;252.3
        cmp       DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_TRANSITREPOS-4+eax*4], 0 ;251.43
        jl        .B2.223       ; Prob 82%                      ;251.43
                                ; LOE eax
.B2.224:                        ; Preds .B2.220 .B2.223
        mov       DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_TRANSITREPOSPNTR], eax ;94.1
                                ; LOE
.B2.225:                        ; Preds .B2.224
        add       esp, 868                                      ;254.1
        pop       ebx                                           ;254.1
        pop       edi                                           ;254.1
        pop       esi                                           ;254.1
        mov       esp, ebp                                      ;254.1
        pop       ebp                                           ;254.1
        ret                                                     ;254.1
                                ; LOE
.B2.226:                        ; Preds .B2.84 .B2.85           ; Infreq
        xor       ebx, ebx                                      ;115.14
        xor       esi, esi                                      ;115.14
        jmp       .B2.93        ; Prob 100%                     ;115.14
                                ; LOE eax edx ecx esi
.B2.228:                        ; Preds .B2.98                  ; Infreq
        mov       ebx, 1                                        ;
        jmp       .B2.102       ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx
.B2.229:                        ; Preds .B2.132                 ; Infreq
        mov       edx, 1                                        ;
        jmp       .B2.140       ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B2.230:                        ; Preds .B2.181                 ; Infreq
        mov       DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_INPUTTRANSIT$H.0.2], 2 ;213.5
        imul      ecx, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE+32], 176 ;
        mov       ebx, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE] ;228.5
        jmp       .B2.197       ; Prob 100%                     ;228.5
                                ; LOE ecx ebx xmm5
.B2.231:                        ; Preds .B2.218                 ; Infreq
        push      32                                            ;245.15
        xor       eax, eax                                      ;245.15
        push      eax                                           ;245.15
        push      eax                                           ;245.15
        push      -2088435968                                   ;245.15
        push      eax                                           ;245.15
        push      OFFSET FLAT: __STRLITPACK_144                 ;245.15
        call      _for_stop_core                                ;245.15
                                ; LOE
.B2.323:                        ; Preds .B2.231                 ; Infreq
        add       esp, 24                                       ;245.15
        jmp       .B2.219       ; Prob 100%                     ;245.15
                                ; LOE
.B2.232:                        ; Preds .B2.217                 ; Infreq
        push      32                                            ;244.14
        xor       eax, eax                                      ;244.14
        push      eax                                           ;244.14
        push      eax                                           ;244.14
        push      -2088435968                                   ;244.14
        push      eax                                           ;244.14
        push      OFFSET FLAT: __STRLITPACK_143                 ;244.14
        call      _for_stop_core                                ;244.14
                                ; LOE esi
.B2.324:                        ; Preds .B2.232                 ; Infreq
        add       esp, 24                                       ;244.14
        jmp       .B2.218       ; Prob 100%                     ;244.14
                                ; LOE esi
.B2.233:                        ; Preds .B2.216                 ; Infreq
        push      32                                            ;243.16
        xor       eax, eax                                      ;243.16
        push      eax                                           ;243.16
        push      eax                                           ;243.16
        push      -2088435968                                   ;243.16
        push      eax                                           ;243.16
        push      OFFSET FLAT: __STRLITPACK_142                 ;243.16
        call      _for_stop_core                                ;243.16
                                ; LOE ebx esi
.B2.325:                        ; Preds .B2.233                 ; Infreq
        add       esp, 24                                       ;243.16
        jmp       .B2.217       ; Prob 100%                     ;243.16
                                ; LOE ebx esi
.B2.234:                        ; Preds .B2.127                 ; Infreq
        mov       DWORD PTR [592+esp], 0                        ;175.6
        lea       eax, DWORD PTR [248+esp]                      ;175.6
        mov       DWORD PTR [248+esp], 16                       ;175.6
        mov       DWORD PTR [252+esp], OFFSET FLAT: __STRLITPACK_37 ;175.6
        push      32                                            ;175.6
        push      eax                                           ;175.6
        push      OFFSET FLAT: __STRLITPACK_111.0.2             ;175.6
        push      -2088435968                                   ;175.6
        push      911                                           ;175.6
        lea       edx, DWORD PTR [612+esp]                      ;175.6
        push      edx                                           ;175.6
        call      _for_write_seq_lis                            ;175.6
                                ; LOE ebx edi
.B2.235:                        ; Preds .B2.234                 ; Infreq
        mov       eax, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE+32] ;175.6
        imul      edx, eax, 176                                 ;175.6
        mov       esi, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE] ;175.6
        mov       ecx, DWORD PTR [696+esp]                      ;175.6
        mov       DWORD PTR [316+esp], edx                      ;175.6
        mov       DWORD PTR [280+esp], 6                        ;175.6
        add       ecx, esi                                      ;175.6
        sub       ecx, edx                                      ;175.6
        lea       edx, DWORD PTR [280+esp]                      ;175.6
        mov       DWORD PTR [284+esp], ecx                      ;175.6
        push      edx                                           ;175.6
        push      OFFSET FLAT: __STRLITPACK_112.0.2             ;175.6
        lea       edx, DWORD PTR [624+esp]                      ;175.6
        push      edx                                           ;175.6
        mov       DWORD PTR [508+esp], eax                      ;175.6
        call      _for_write_seq_lis_xmit                       ;175.6
                                ; LOE ebx esi edi
.B2.236:                        ; Preds .B2.235                 ; Infreq
        mov       DWORD PTR [408+esp], edi                      ;
        mov       edi, DWORD PTR [328+esp]                      ;176.6
        neg       edi                                           ;176.6
        add       edi, DWORD PTR [716+esp]                      ;176.6
        mov       DWORD PTR [560+esp], esi                      ;
        mov       ecx, DWORD PTR [20+edi+esi]                   ;176.6
        mov       edx, DWORD PTR [52+edi+esi]                   ;176.6
        mov       edi, DWORD PTR [48+edi+esi]                   ;176.6
        imul      ebx, edi                                      ;176.6
        imul      edx, edi                                      ;176.6
        imul      esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE+32], -44 ;176.6
        add       ebx, ecx                                      ;176.6
        sub       ebx, edx                                      ;176.6
        add       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE] ;176.6
        mov       DWORD PTR [344+esp], ecx                      ;176.6
        mov       DWORD PTR [628+esp], 0                        ;176.6
        imul      ebx, DWORD PTR [ebx], 44                      ;176.60
        mov       ecx, DWORD PTR [36+esi+ebx]                   ;176.6
        lea       ebx, DWORD PTR [436+esp]                      ;176.6
        mov       DWORD PTR [336+esp], edi                      ;176.6
        mov       DWORD PTR [360+esp], esi                      ;176.6
        mov       DWORD PTR [436+esp], ecx                      ;176.6
        push      32                                            ;176.6
        push      OFFSET FLAT: DYNUST_TRANSIT_MODULE_mp_INPUTTRANSIT$format_pack.0.2+340 ;176.6
        push      ebx                                           ;176.6
        push      OFFSET FLAT: __STRLITPACK_113.0.2             ;176.6
        push      -2088435968                                   ;176.6
        push      911                                           ;176.6
        lea       esi, DWORD PTR [652+esp]                      ;176.6
        push      esi                                           ;176.6
        mov       edi, DWORD PTR [436+esp]                      ;176.6
        mov       esi, DWORD PTR [588+esp]                      ;176.6
        mov       DWORD PTR [440+esp], edx                      ;176.6
        call      _for_write_seq_fmt                            ;176.6
                                ; LOE esi edi
.B2.237:                        ; Preds .B2.236                 ; Infreq
        mov       ecx, DWORD PTR [364+esp]                      ;176.6
        imul      ecx, edi                                      ;176.6
        mov       ebx, DWORD PTR [372+esp]                      ;176.6
        add       ebx, ecx                                      ;176.6
        mov       edx, DWORD PTR [440+esp]                      ;
        sub       ebx, edx                                      ;176.6
        mov       edx, DWORD PTR [388+esp]                      ;176.6
        imul      ecx, DWORD PTR [ebx], 44                      ;176.124
        lea       ebx, DWORD PTR [472+esp]                      ;176.6
        mov       edx, DWORD PTR [36+edx+ecx]                   ;176.6
        mov       DWORD PTR [472+esp], edx                      ;176.6
        push      ebx                                           ;176.6
        push      OFFSET FLAT: __STRLITPACK_114.0.2             ;176.6
        lea       edx, DWORD PTR [664+esp]                      ;176.6
        push      edx                                           ;176.6
        call      _for_write_seq_fmt_xmit                       ;176.6
                                ; LOE esi edi
.B2.326:                        ; Preds .B2.237                 ; Infreq
        mov       eax, DWORD PTR [548+esp]                      ;
        add       esp, 76                                       ;176.6
                                ; LOE eax esi edi al ah
.B2.238:                        ; Preds .B2.326                 ; Infreq
        mov       DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_ROUTEERROR], -1 ;177.7
        jmp       .B2.129       ; Prob 100%                     ;177.7
                                ; LOE eax esi edi
.B2.239:                        ; Preds .B2.121                 ; Infreq
        mov       ecx, DWORD PTR [672+esp]                      ;166.6
        mov       edx, DWORD PTR [524+esp]                      ;166.6
        mov       DWORD PTR [592+esp], 0                        ;166.6
        mov       DWORD PTR [240+esp], 6                        ;166.6
        add       edx, ecx                                      ;166.6
        sub       edx, DWORD PTR [476+esp]                      ;166.6
        lea       ecx, DWORD PTR [240+esp]                      ;166.6
        mov       DWORD PTR [244+esp], edx                      ;166.6
        push      32                                            ;166.6
        push      OFFSET FLAT: DYNUST_TRANSIT_MODULE_mp_INPUTTRANSIT$format_pack.0.2+248 ;166.6
        push      ecx                                           ;166.6
        push      OFFSET FLAT: __STRLITPACK_109.0.2             ;166.6
        push      -2088435968                                   ;166.6
        push      911                                           ;166.6
        lea       edx, DWORD PTR [616+esp]                      ;166.6
        push      edx                                           ;166.6
        mov       DWORD PTR [408+esp], eax                      ;166.6
        call      _for_write_seq_fmt                            ;166.6
                                ; LOE ebx esi edi
.B2.240:                        ; Preds .B2.239                 ; Infreq
        mov       eax, DWORD PTR [408+esp]                      ;
        lea       edx, DWORD PTR [420+esp]                      ;166.6
        mov       DWORD PTR [420+esp], eax                      ;166.6
        push      edx                                           ;166.6
        push      OFFSET FLAT: __STRLITPACK_110.0.2             ;166.6
        lea       ecx, DWORD PTR [628+esp]                      ;166.6
        push      ecx                                           ;166.6
        call      _for_write_seq_fmt_xmit                       ;166.6
                                ; LOE ebx esi edi
.B2.327:                        ; Preds .B2.240                 ; Infreq
        add       esp, 40                                       ;166.6
                                ; LOE ebx esi edi
.B2.241:                        ; Preds .B2.327                 ; Infreq
        mov       DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_ROUTEERROR], -1 ;167.6
        jmp       .B2.122       ; Prob 100%                     ;167.6
                                ; LOE ebx esi edi
.B2.242:                        ; Preds .B2.117                 ; Infreq
        sub       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE+32] ;155.80
        imul      edx, edx, 44                                  ;155.80
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE] ;155.3
        movsx     eax, WORD PTR [40+eax+edx]                    ;155.10
        test      eax, eax                                      ;155.80
        jg        .B2.118       ; Prob 84%                      ;155.80
                                ; LOE eax ebx esi edi
.B2.244:                        ; Preds .B2.242                 ; Infreq
        mov       DWORD PTR [592+esp], 0                        ;158.5
        lea       eax, DWORD PTR [24+esp]                       ;158.5
        mov       DWORD PTR [24+esp], 45                        ;158.5
        mov       DWORD PTR [28+esp], OFFSET FLAT: __STRLITPACK_43 ;158.5
        push      32                                            ;158.5
        push      eax                                           ;158.5
        push      OFFSET FLAT: __STRLITPACK_105.0.2             ;158.5
        push      -2088435968                                   ;158.5
        push      911                                           ;158.5
        lea       edx, DWORD PTR [612+esp]                      ;158.5
        push      edx                                           ;158.5
        call      _for_write_seq_lis                            ;158.5
                                ; LOE esi edi
.B2.245:                        ; Preds .B2.244                 ; Infreq
        mov       DWORD PTR [616+esp], 0                        ;159.5
        lea       eax, DWORD PTR [64+esp]                       ;159.5
        mov       DWORD PTR [64+esp], 12                        ;159.5
        mov       DWORD PTR [68+esp], OFFSET FLAT: __STRLITPACK_41 ;159.5
        push      32                                            ;159.5
        push      eax                                           ;159.5
        push      OFFSET FLAT: __STRLITPACK_106.0.2             ;159.5
        push      -2088435968                                   ;159.5
        push      911                                           ;159.5
        lea       edx, DWORD PTR [636+esp]                      ;159.5
        push      edx                                           ;159.5
        call      _for_write_seq_lis                            ;159.5
                                ; LOE esi edi
.B2.246:                        ; Preds .B2.245                 ; Infreq
        mov       eax, DWORD PTR [720+esp]                      ;159.5
        lea       ecx, DWORD PTR [96+esp]                       ;159.5
        mov       DWORD PTR [96+esp], 6                         ;159.5
        lea       edx, DWORD PTR [esi+eax]                      ;159.5
        sub       edx, DWORD PTR [524+esp]                      ;159.5
        mov       DWORD PTR [100+esp], edx                      ;159.5
        push      ecx                                           ;159.5
        push      OFFSET FLAT: __STRLITPACK_107.0.2             ;159.5
        lea       ebx, DWORD PTR [648+esp]                      ;159.5
        push      ebx                                           ;159.5
        call      _for_write_seq_lis_xmit                       ;159.5
                                ; LOE esi edi
.B2.247:                        ; Preds .B2.246                 ; Infreq
        push      32                                            ;160.5
        xor       eax, eax                                      ;160.5
        push      eax                                           ;160.5
        push      eax                                           ;160.5
        push      -2088435968                                   ;160.5
        push      eax                                           ;160.5
        push      OFFSET FLAT: __STRLITPACK_108                 ;160.5
        call      _for_stop_core                                ;160.5
                                ; LOE esi edi
.B2.328:                        ; Preds .B2.247                 ; Infreq
        add       esp, 84                                       ;160.5
        jmp       .B2.119       ; Prob 100%                     ;160.5
                                ; LOE esi edi
.B2.248:                        ; Preds .B2.115                 ; Infreq
        mov       edi, DWORD PTR [16+ebx+esi]                   ;143.3
        mov       ecx, DWORD PTR [692+esp]                      ;143.3
        imul      ecx, edi                                      ;143.3
        add       edx, ecx                                      ;143.80
        sub       edx, DWORD PTR [496+esp]                      ;143.80
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE+32] ;143.3
        neg       eax                                           ;143.80
        mov       edx, DWORD PTR [edx]                          ;143.10
        add       eax, edx                                      ;143.80
        imul      ecx, eax, 44                                  ;143.80
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE] ;143.10
        movsx     eax, WORD PTR [40+eax+ecx]                    ;143.10
        test      eax, eax                                      ;143.80
        jle       .B2.250       ; Prob 16%                      ;143.80
                                ; LOE eax edx ebx esi edi
.B2.249:                        ; Preds .B2.248                 ; Infreq
        mov       DWORD PTR [172+ebx+esi], eax                  ;144.5
        jmp       .B2.117       ; Prob 100%                     ;144.5
                                ; LOE edx ebx esi edi
.B2.250:                        ; Preds .B2.248                 ; Infreq
        mov       DWORD PTR [592+esp], 0                        ;146.5
        lea       eax, DWORD PTR [8+esp]                        ;146.5
        mov       DWORD PTR [8+esp], 40                         ;146.5
        mov       DWORD PTR [12+esp], OFFSET FLAT: __STRLITPACK_47 ;146.5
        push      32                                            ;146.5
        push      eax                                           ;146.5
        push      OFFSET FLAT: __STRLITPACK_101.0.2             ;146.5
        push      -2088435968                                   ;146.5
        push      911                                           ;146.5
        lea       ecx, DWORD PTR [612+esp]                      ;146.5
        push      ecx                                           ;146.5
        mov       DWORD PTR [80+esp], edx                       ;146.5
        call      _for_write_seq_lis                            ;146.5
                                ; LOE ebx esi edi
.B2.251:                        ; Preds .B2.250                 ; Infreq
        lea       eax, DWORD PTR [40+esp]                       ;147.5
        mov       DWORD PTR [616+esp], 0                        ;147.5
        mov       DWORD PTR [40+esp], 12                        ;147.5
        mov       DWORD PTR [44+esp], OFFSET FLAT: __STRLITPACK_45 ;147.5
        push      32                                            ;147.5
        push      eax                                           ;147.5
        push      OFFSET FLAT: __STRLITPACK_102.0.2             ;147.5
        push      -2088435968                                   ;147.5
        push      911                                           ;147.5
        lea       ecx, DWORD PTR [636+esp]                      ;147.5
        push      ecx                                           ;147.5
        call      _for_write_seq_lis                            ;147.5
                                ; LOE ebx esi edi
.B2.252:                        ; Preds .B2.251                 ; Infreq
        mov       eax, DWORD PTR [720+esp]                      ;147.5
        mov       DWORD PTR [80+esp], 6                         ;147.5
        lea       ecx, DWORD PTR [esi+eax]                      ;147.5
        sub       ecx, DWORD PTR [524+esp]                      ;147.5
        lea       eax, DWORD PTR [80+esp]                       ;147.5
        mov       DWORD PTR [84+esp], ecx                       ;147.5
        push      eax                                           ;147.5
        push      OFFSET FLAT: __STRLITPACK_103.0.2             ;147.5
        lea       ecx, DWORD PTR [648+esp]                      ;147.5
        push      ecx                                           ;147.5
        call      _for_write_seq_lis_xmit                       ;147.5
                                ; LOE ebx esi edi
.B2.253:                        ; Preds .B2.252                 ; Infreq
        xor       eax, eax                                      ;148.5
        push      32                                            ;148.5
        push      eax                                           ;148.5
        push      eax                                           ;148.5
        push      -2088435968                                   ;148.5
        push      eax                                           ;148.5
        push      OFFSET FLAT: __STRLITPACK_104                 ;148.5
        call      _for_stop_core                                ;148.5
                                ; LOE ebx esi edi
.B2.329:                        ; Preds .B2.253                 ; Infreq
        mov       edx, DWORD PTR [140+esp]                      ;
        add       esp, 84                                       ;148.5
        jmp       .B2.117       ; Prob 100%                     ;148.5
                                ; LOE edx ebx esi edi
.B2.254:                        ; Preds .B2.113                 ; Infreq
        push      32                                            ;136.17
        xor       eax, eax                                      ;136.17
        push      eax                                           ;136.17
        push      eax                                           ;136.17
        push      -2088435968                                   ;136.17
        push      eax                                           ;136.17
        push      OFFSET FLAT: __STRLITPACK_100                 ;136.17
        call      _for_stop_core                                ;136.17
                                ; LOE ebx esi
.B2.330:                        ; Preds .B2.254                 ; Infreq
        add       esp, 24                                       ;136.17
        jmp       .B2.114       ; Prob 100%                     ;136.17
                                ; LOE ebx esi
.B2.255:                        ; Preds .B2.111 .B2.112         ; Infreq
        mov       DWORD PTR [592+esp], 0                        ;133.5
        lea       eax, DWORD PTR [128+esp]                      ;133.5
        mov       DWORD PTR [128+esp], 31                       ;133.5
        mov       DWORD PTR [132+esp], OFFSET FLAT: __STRLITPACK_51 ;133.5
        push      32                                            ;133.5
        push      eax                                           ;133.5
        push      OFFSET FLAT: __STRLITPACK_97.0.2              ;133.5
        push      -2088435968                                   ;133.5
        push      911                                           ;133.5
        lea       edx, DWORD PTR [612+esp]                      ;133.5
        push      edx                                           ;133.5
        call      _for_write_seq_lis                            ;133.5
                                ; LOE ebx esi
.B2.256:                        ; Preds .B2.255                 ; Infreq
        mov       DWORD PTR [616+esp], 0                        ;134.5
        lea       eax, DWORD PTR [160+esp]                      ;134.5
        mov       DWORD PTR [160+esp], 21                       ;134.5
        mov       DWORD PTR [164+esp], OFFSET FLAT: __STRLITPACK_49 ;134.5
        push      32                                            ;134.5
        push      eax                                           ;134.5
        push      OFFSET FLAT: __STRLITPACK_98.0.2              ;134.5
        push      -2088435968                                   ;134.5
        push      911                                           ;134.5
        lea       edx, DWORD PTR [636+esp]                      ;134.5
        push      edx                                           ;134.5
        call      _for_write_seq_lis                            ;134.5
                                ; LOE ebx esi
.B2.257:                        ; Preds .B2.256                 ; Infreq
        mov       eax, DWORD PTR [508+esp]                      ;134.5
        lea       edx, DWORD PTR [232+esp]                      ;134.5
        mov       DWORD PTR [232+esp], eax                      ;134.5
        push      edx                                           ;134.5
        push      OFFSET FLAT: __STRLITPACK_99.0.2              ;134.5
        lea       ecx, DWORD PTR [648+esp]                      ;134.5
        push      ecx                                           ;134.5
        call      _for_write_seq_lis_xmit                       ;134.5
                                ; LOE ebx esi
.B2.331:                        ; Preds .B2.257                 ; Infreq
        add       esp, 60                                       ;134.5
        jmp       .B2.113       ; Prob 100%                     ;134.5
                                ; LOE ebx esi
.B2.258:                        ; Preds .B2.108                 ; Infreq
        mov       DWORD PTR [592+esp], 0                        ;122.9
        lea       eax, DWORD PTR [192+esp]                      ;122.9
        mov       DWORD PTR [192+esp], 33                       ;122.9
        mov       DWORD PTR [196+esp], OFFSET FLAT: __STRLITPACK_63 ;122.9
        push      32                                            ;122.9
        push      eax                                           ;122.9
        push      OFFSET FLAT: __STRLITPACK_88.0.2              ;122.9
        push      -2088435968                                   ;122.9
        push      911                                           ;122.9
        lea       ecx, DWORD PTR [612+esp]                      ;122.9
        push      ecx                                           ;122.9
        mov       DWORD PTR [308+esp], edx                      ;122.9
        call      _for_write_seq_lis                            ;122.9
                                ; LOE ebx edi
.B2.259:                        ; Preds .B2.258                 ; Infreq
        mov       ecx, DWORD PTR [696+esp]                      ;123.9
        mov       eax, DWORD PTR [668+esp]                      ;123.9
        mov       DWORD PTR [616+esp], 0                        ;123.9
        mov       DWORD PTR [224+esp], 6                        ;123.9
        lea       esi, DWORD PTR [eax+ecx]                      ;123.9
        sub       esi, DWORD PTR [356+esp]                      ;123.9
        lea       eax, DWORD PTR [224+esp]                      ;123.9
        mov       DWORD PTR [228+esp], esi                      ;123.9
        push      32                                            ;123.9
        push      OFFSET FLAT: DYNUST_TRANSIT_MODULE_mp_INPUTTRANSIT$format_pack.0.2+48 ;123.9
        push      eax                                           ;123.9
        push      OFFSET FLAT: __STRLITPACK_89.0.2              ;123.9
        push      -2088435968                                   ;123.9
        push      911                                           ;123.9
        lea       ecx, DWORD PTR [640+esp]                      ;123.9
        push      ecx                                           ;123.9
        call      _for_write_seq_fmt                            ;123.9
                                ; LOE ebx edi
.B2.260:                        ; Preds .B2.259                 ; Infreq
        lea       eax, DWORD PTR [372+esp]                      ;123.9
        mov       DWORD PTR [372+esp], edi                      ;123.9
        push      eax                                           ;123.9
        push      OFFSET FLAT: __STRLITPACK_90.0.2              ;123.9
        lea       ecx, DWORD PTR [652+esp]                      ;123.9
        push      ecx                                           ;123.9
        call      _for_write_seq_fmt_xmit                       ;123.9
                                ; LOE ebx edi
.B2.261:                        ; Preds .B2.260                 ; Infreq
        mov       edx, DWORD PTR [348+esp]                      ;
        lea       esi, DWORD PTR [392+esp]                      ;124.9
        sub       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE+32] ;124.9
        imul      edx, edx, 44                                  ;124.9
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE] ;124.9
        mov       DWORD PTR [656+esp], 0                        ;124.9
        mov       ecx, DWORD PTR [36+eax+edx]                   ;124.9
        mov       DWORD PTR [392+esp], ecx                      ;124.9
        push      32                                            ;124.9
        push      OFFSET FLAT: DYNUST_TRANSIT_MODULE_mp_INPUTTRANSIT$format_pack.0.2+144 ;124.9
        push      esi                                           ;124.9
        push      OFFSET FLAT: __STRLITPACK_91.0.2              ;124.9
        push      -2088435968                                   ;124.9
        push      911                                           ;124.9
        lea       eax, DWORD PTR [680+esp]                      ;124.9
        push      eax                                           ;124.9
        call      _for_write_seq_fmt                            ;124.9
                                ; LOE ebx edi
.B2.332:                        ; Preds .B2.261                 ; Infreq
        add       esp, 92                                       ;124.9
                                ; LOE ebx edi
.B2.262:                        ; Preds .B2.332                 ; Infreq
        mov       DWORD PTR [668+esp], -1                       ;
        jmp       .B2.109       ; Prob 100%                     ;
                                ; LOE ebx edi
.B2.263:                        ; Preds .B2.106                 ; Infreq
        mov       DWORD PTR [592+esp], 0                        ;127.7
        lea       eax, DWORD PTR [208+esp]                      ;127.7
        mov       DWORD PTR [208+esp], 61                       ;127.7
        mov       DWORD PTR [212+esp], OFFSET FLAT: __STRLITPACK_57 ;127.7
        push      32                                            ;127.7
        push      eax                                           ;127.7
        push      OFFSET FLAT: __STRLITPACK_92.0.2              ;127.7
        push      -2088435968                                   ;127.7
        push      911                                           ;127.7
        lea       edx, DWORD PTR [612+esp]                      ;127.7
        push      edx                                           ;127.7
        call      _for_write_seq_lis                            ;127.7
                                ; LOE ebx esi edi
.B2.264:                        ; Preds .B2.263                 ; Infreq
        mov       DWORD PTR [616+esp], 0                        ;128.7
        lea       eax, DWORD PTR [240+esp]                      ;128.7
        mov       DWORD PTR [240+esp], 8                        ;128.7
        mov       DWORD PTR [244+esp], OFFSET FLAT: __STRLITPACK_54 ;128.7
        push      32                                            ;128.7
        push      eax                                           ;128.7
        push      OFFSET FLAT: __STRLITPACK_93.0.2              ;128.7
        push      -2088435968                                   ;128.7
        push      911                                           ;128.7
        lea       edx, DWORD PTR [636+esp]                      ;128.7
        push      edx                                           ;128.7
        call      _for_write_seq_lis                            ;128.7
                                ; LOE ebx esi edi
.B2.265:                        ; Preds .B2.264                 ; Infreq
        mov       edx, DWORD PTR [720+esp]                      ;128.7
        mov       eax, DWORD PTR [692+esp]                      ;128.7
        mov       DWORD PTR [272+esp], 6                        ;128.7
        lea       ecx, DWORD PTR [eax+edx]                      ;128.7
        sub       ecx, DWORD PTR [380+esp]                      ;128.7
        lea       eax, DWORD PTR [272+esp]                      ;128.7
        mov       DWORD PTR [276+esp], ecx                      ;128.7
        push      eax                                           ;128.7
        push      OFFSET FLAT: __STRLITPACK_94.0.2              ;128.7
        lea       edx, DWORD PTR [648+esp]                      ;128.7
        push      edx                                           ;128.7
        call      _for_write_seq_lis_xmit                       ;128.7
                                ; LOE ebx esi edi
.B2.266:                        ; Preds .B2.265                 ; Infreq
        mov       DWORD PTR [292+esp], 4                        ;128.7
        lea       eax, DWORD PTR [292+esp]                      ;128.7
        mov       DWORD PTR [296+esp], OFFSET FLAT: __STRLITPACK_53 ;128.7
        push      eax                                           ;128.7
        push      OFFSET FLAT: __STRLITPACK_95.0.2              ;128.7
        lea       edx, DWORD PTR [660+esp]                      ;128.7
        push      edx                                           ;128.7
        call      _for_write_seq_lis_xmit                       ;128.7
                                ; LOE ebx esi edi
.B2.267:                        ; Preds .B2.266                 ; Infreq
        mov       DWORD PTR [456+esp], esi                      ;128.7
        lea       eax, DWORD PTR [456+esp]                      ;128.7
        push      eax                                           ;128.7
        push      OFFSET FLAT: __STRLITPACK_96.0.2              ;128.7
        lea       edx, DWORD PTR [672+esp]                      ;128.7
        push      edx                                           ;128.7
        call      _for_write_seq_lis_xmit                       ;128.7
                                ; LOE ebx edi
.B2.333:                        ; Preds .B2.267                 ; Infreq
        add       esp, 84                                       ;128.7
                                ; LOE ebx edi
.B2.268:                        ; Preds .B2.333                 ; Infreq
        mov       DWORD PTR [488+esp], -1                       ;129.7
        jmp       .B2.109       ; Prob 100%                     ;129.7
                                ; LOE ebx edi
.B2.269:                        ; Preds .B2.46                  ; Infreq
        xor       eax, eax                                      ;
        xor       esi, esi                                      ;
        jmp       .B2.216       ; Prob 100%                     ;
                                ; LOE ebx esi
.B2.270:                        ; Preds .B2.5                   ; Infreq
        mov       DWORD PTR [336+esp], 0                        ;88.6
        lea       eax, DWORD PTR [esp]                          ;88.6
        mov       DWORD PTR [esp], 35                           ;88.6
        mov       DWORD PTR [4+esp], OFFSET FLAT: __STRLITPACK_13 ;88.6
        push      32                                            ;88.6
        push      eax                                           ;88.6
        push      OFFSET FLAT: __STRLITPACK_146.0.7             ;88.6
        push      -2088435968                                   ;88.6
        push      911                                           ;88.6
        push      esi                                           ;88.6
        call      _for_write_seq_lis                            ;88.6
                                ; LOE esi
.B2.271:                        ; Preds .B2.270                 ; Infreq
        push      32                                            ;88.6
        xor       eax, eax                                      ;88.6
        push      eax                                           ;88.6
        push      eax                                           ;88.6
        push      -2088435968                                   ;88.6
        push      eax                                           ;88.6
        push      OFFSET FLAT: __STRLITPACK_147                 ;88.6
        call      _for_stop_core                                ;88.6
                                ; LOE esi
.B2.334:                        ; Preds .B2.271                 ; Infreq
        add       esp, 48                                       ;88.6
        jmp       .B2.6         ; Prob 100%                     ;88.6
        ALIGN     16
                                ; LOE esi
; mark_end;
_DYNUST_TRANSIT_MODULE_mp_INPUTTRANSIT ENDP
_TEXT	ENDS
_BSS	SEGMENT  DWORD PUBLIC FLAT  'BSS'
	ALIGN 004H
_DYNUST_TRANSIT_MODULE_mp_INPUTTRANSIT$I.0.2	DD 1 DUP (0H)	; pad
_DYNUST_TRANSIT_MODULE_mp_INPUTTRANSIT$H.0.2	DD 1 DUP (0H)	; pad
_BSS	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
DYNUST_TRANSIT_MODULE_mp_INPUTTRANSIT$format_pack.0.2	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	23
	DB	0
	DB	67
	DB	104
	DB	101
	DB	99
	DB	107
	DB	105
	DB	110
	DB	103
	DB	32
	DB	114
	DB	111
	DB	117
	DB	116
	DB	101
	DB	32
	DB	110
	DB	117
	DB	109
	DB	98
	DB	101
	DB	114
	DB	32
	DB	32
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	10
	DB	0
	DB	82
	DB	111
	DB	117
	DB	116
	DB	101
	DB	32
	DB	73
	DB	68
	DB	32
	DB	32
	DB	0
	DB	0
	DB	72
	DB	0
	DB	1
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	51
	DB	0
	DB	116
	DB	104
	DB	32
	DB	115
	DB	116
	DB	111
	DB	112
	DB	32
	DB	110
	DB	101
	DB	101
	DB	100
	DB	115
	DB	32
	DB	116
	DB	111
	DB	32
	DB	98
	DB	101
	DB	32
	DB	115
	DB	112
	DB	101
	DB	99
	DB	105
	DB	102
	DB	105
	DB	101
	DB	100
	DB	32
	DB	97
	DB	115
	DB	32
	DB	97
	DB	32
	DB	100
	DB	101
	DB	115
	DB	116
	DB	105
	DB	110
	DB	97
	DB	116
	DB	105
	DB	111
	DB	110
	DB	32
	DB	110
	DB	111
	DB	100
	DB	101
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	79
	DB	0
	DB	80
	DB	108
	DB	101
	DB	97
	DB	115
	DB	101
	DB	32
	DB	101
	DB	105
	DB	116
	DB	104
	DB	101
	DB	114
	DB	32
	DB	109
	DB	111
	DB	100
	DB	105
	DB	102
	DB	121
	DB	32
	DB	84
	DB	114
	DB	97
	DB	110
	DB	115
	DB	105
	DB	116
	DB	82
	DB	111
	DB	117
	DB	116
	DB	101
	DB	83
	DB	99
	DB	104
	DB	101
	DB	100
	DB	117
	DB	108
	DB	101
	DB	46
	DB	100
	DB	97
	DB	116
	DB	32
	DB	111
	DB	114
	DB	32
	DB	100
	DB	101
	DB	115
	DB	116
	DB	105
	DB	110
	DB	97
	DB	116
	DB	105
	DB	111
	DB	110
	DB	32
	DB	115
	DB	101
	DB	116
	DB	116
	DB	105
	DB	110
	DB	103
	DB	32
	DB	102
	DB	111
	DB	114
	DB	32
	DB	110
	DB	111
	DB	100
	DB	101
	DB	32
	DB	35
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	51
	DB	0
	DB	82
	DB	111
	DB	117
	DB	116
	DB	101
	DB	32
	DB	105
	DB	110
	DB	118
	DB	97
	DB	108
	DB	105
	DB	100
	DB	44
	DB	32
	DB	110
	DB	111
	DB	100
	DB	101
	DB	32
	DB	100
	DB	111
	DB	101
	DB	115
	DB	32
	DB	110
	DB	111
	DB	116
	DB	32
	DB	101
	DB	120
	DB	105
	DB	115
	DB	116
	DB	44
	DB	32
	DB	99
	DB	104
	DB	101
	DB	99
	DB	107
	DB	32
	DB	114
	DB	111
	DB	117
	DB	116
	DB	101
	DB	32
	DB	73
	DB	68
	DB	32
	DB	0
	DB	72
	DB	0
	DB	1
	DB	0
	DB	28
	DB	0
	DB	6
	DB	0
	DB	32
	DB	110
	DB	111
	DB	100
	DB	101
	DB	32
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	20
	DB	0
	DB	76
	DB	105
	DB	110
	DB	107
	DB	32
	DB	100
	DB	111
	DB	101
	DB	115
	DB	32
	DB	110
	DB	111
	DB	116
	DB	32
	DB	101
	DB	120
	DB	105
	DB	115
	DB	116
	DB	32
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	5
	DB	0
	DB	32
	DB	61
	DB	61
	DB	62
	DB	32
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	25
	DB	0
	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	84
	DB	114
	DB	97
	DB	110
	DB	115
	DB	105
	DB	116
	DB	32
	DB	82
	DB	111
	DB	117
	DB	116
	DB	101
	DB	32
	DB	35
	DB	32
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	5
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	12
	DB	0
	DB	82
	DB	79
	DB	85
	DB	84
	DB	69
	DB	32
	DB	73
	DB	68
	DB	32
	DB	61
	DB	32
	DB	32
	DB	72
	DB	0
	DB	1
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	33
	DB	0
	DB	82
	DB	111
	DB	117
	DB	116
	DB	101
	DB	32
	DB	108
	DB	105
	DB	110
	DB	101
	DB	32
	DB	99
	DB	111
	DB	117
	DB	110
	DB	116
	DB	101
	DB	100
	DB	32
	DB	35
	DB	32
	DB	111
	DB	102
	DB	32
	DB	115
	DB	116
	DB	111
	DB	112
	DB	115
	DB	32
	DB	61
	DB	32
	DB	32
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	5
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	33
	DB	0
	DB	83
	DB	99
	DB	104
	DB	101
	DB	100
	DB	117
	DB	108
	DB	101
	DB	32
	DB	105
	DB	110
	DB	100
	DB	105
	DB	99
	DB	97
	DB	116
	DB	101
	DB	100
	DB	32
	DB	35
	DB	32
	DB	111
	DB	102
	DB	32
	DB	115
	DB	116
	DB	111
	DB	112
	DB	115
	DB	32
	DB	61
	DB	32
	DB	32
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	5
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	39
	DB	0
	DB	82
	DB	111
	DB	117
	DB	116
	DB	101
	DB	32
	DB	110
	DB	111
	DB	100
	DB	101
	DB	115
	DB	32
	DB	115
	DB	112
	DB	101
	DB	99
	DB	105
	DB	102
	DB	105
	DB	101
	DB	100
	DB	32
	DB	97
	DB	115
	DB	32
	DB	97
	DB	32
	DB	98
	DB	117
	DB	115
	DB	32
	DB	115
	DB	116
	DB	111
	DB	112
	DB	32
	DB	61
	DB	62
	DB	32
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
_DATA	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
__STRLITPACK_70.0.2	DB	56
	DB	4
	DB	13
	DB	0
	DB	16
	DB	3
	DB	27
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_71.0.2	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_72.0.2	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	26
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_73.0.2	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_74.0.2	DB	26
	DB	3
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_75.0.2	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_76.0.2	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_77.0.2	DB	9
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_78.0.2	DB	9
	DB	3
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_79.0.2	DB	2
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_80.0.2	DB	9
	DB	5
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_81.0.2	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_82.0.2	DB	2
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_83.0.2	DB	5
	DB	5
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_84.0.2	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_85.0.2	DB	2
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_86.0.2	DB	7
	DB	5
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_87.0.2	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_92.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_93.0.2	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_94.0.2	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_95.0.2	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_96.0.2	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_88.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_89.0.2	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_90.0.2	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_91.0.2	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_97.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_98.0.2	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_99.0.2	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__NLITPACK_0.0.2	DD	4
__STRLITPACK_101.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_102.0.2	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_103.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_105.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_106.0.2	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_107.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_109.0.2	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_110.0.2	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_111.0.2	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_112.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_113.0.2	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_114.0.2	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_115.0.2	DB	9
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_116.0.2	DB	9
	DB	3
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_117.0.2	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_118.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_119.0.2	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_120.0.2	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_121.0.2	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_122.0.2	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_123.0.2	DB	2
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_124.0.2	DB	26
	DB	5
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_125.0.2	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_126.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_127.0.2	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_128.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_129.0.2	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_130.0.2	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_131.0.2	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_132.0.2	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_133.0.2	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_135.0.2	DB	2
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_136.0.2	DB	26
	DB	5
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_137.0.2	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_138.0.2	DB	2
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_139.0.2	DB	26
	DB	5
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_140.0.2	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_141.0.2	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__NLITPACK_2.0.2	DD	5
__NLITPACK_1.0.2	DD	1000
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_TRANSIT_MODULE_mp_INPUTTRANSIT
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_TRANSIT_MODULE_mp_QUERY_TRANSIT_VEHICLES
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_TRANSIT_MODULE_mp_QUERY_TRANSIT_VEHICLES
_DYNUST_TRANSIT_MODULE_mp_QUERY_TRANSIT_VEHICLES	PROC NEAR 
; parameter 1: 4 + esp
.B3.1:                          ; Preds .B3.0
        mov       edx, -1                                       ;268.1
        mov       eax, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_TRANSITREPOSPNTR] ;261.14
        movss     xmm0, DWORD PTR [_2il0floatpacket.6]          ;261.14
        movss     xmm1, DWORD PTR [_2il0floatpacket.7]          ;261.14
        cvtsi2ss  xmm3, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_TRANSITREPOS-4+eax*4] ;261.14
        divss     xmm3, DWORD PTR [_2il0floatpacket.5]          ;261.14
        divss     xmm3, DWORD PTR [_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT] ;261.14
        andps     xmm0, xmm3                                    ;261.14
        pxor      xmm3, xmm0                                    ;261.14
        movaps    xmm2, xmm3                                    ;261.14
        movss     xmm4, DWORD PTR [_2il0floatpacket.8]          ;261.14
        cmpltss   xmm2, xmm1                                    ;261.14
        andps     xmm1, xmm2                                    ;261.14
        movaps    xmm2, xmm3                                    ;261.14
        movaps    xmm6, xmm4                                    ;261.14
        addss     xmm2, xmm1                                    ;261.14
        addss     xmm6, xmm4                                    ;261.14
        subss     xmm2, xmm1                                    ;261.14
        movaps    xmm7, xmm2                                    ;261.14
        mov       ecx, DWORD PTR [4+esp]                        ;257.18
        subss     xmm7, xmm3                                    ;261.14
        movaps    xmm5, xmm7                                    ;261.14
        cmpless   xmm7, DWORD PTR [_2il0floatpacket.9]          ;261.14
        cmpnless  xmm5, xmm4                                    ;261.14
        andps     xmm5, xmm6                                    ;261.14
        andps     xmm7, xmm6                                    ;261.14
        subss     xmm2, xmm5                                    ;261.14
        addss     xmm2, xmm7                                    ;261.14
        orps      xmm2, xmm0                                    ;261.14
        cvtss2si  eax, xmm2                                     ;261.1
        cmp       eax, DWORD PTR [ecx]                          ;268.1
        mov       eax, 0                                        ;268.1
        cmovl     eax, edx                                      ;268.1
        ret                                                     ;268.1
        ALIGN     16
                                ; LOE
; mark_end;
_DYNUST_TRANSIT_MODULE_mp_QUERY_TRANSIT_VEHICLES ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_TRANSIT_MODULE_mp_QUERY_TRANSIT_VEHICLES
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_TRANSIT_MODULE_mp_GET_TRANSIT_VEH
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_TRANSIT_MODULE_mp_GET_TRANSIT_VEH
_DYNUST_TRANSIT_MODULE_mp_GET_TRANSIT_VEH	PROC NEAR 
; parameter 1: 12 + esp
; parameter 2: 16 + esp
; parameter 3: 20 + esp
; parameter 4: 24 + esp
; parameter 5: 28 + esp
; parameter 6: 32 + esp
.B4.1:                          ; Preds .B4.0
        push      esi                                           ;271.12
        push      esi                                           ;271.12
        mov       eax, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_TRANSITREPOSPNTR] ;274.19
        movss     xmm0, DWORD PTR [_2il0floatpacket.17]         ;274.14
        movss     xmm1, DWORD PTR [_2il0floatpacket.18]         ;274.14
        cvtsi2ss  xmm3, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_TRANSITREPOS-4+eax*4] ;274.14
        divss     xmm3, DWORD PTR [_2il0floatpacket.16]         ;274.14
        divss     xmm3, DWORD PTR [_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT] ;274.14
        andps     xmm0, xmm3                                    ;274.14
        pxor      xmm3, xmm0                                    ;274.14
        movaps    xmm2, xmm3                                    ;274.14
        movss     xmm4, DWORD PTR [_2il0floatpacket.19]         ;274.14
        cmpltss   xmm2, xmm1                                    ;274.14
        andps     xmm1, xmm2                                    ;274.14
        movaps    xmm2, xmm3                                    ;274.14
        movaps    xmm6, xmm4                                    ;274.14
        addss     xmm2, xmm1                                    ;274.14
        addss     xmm6, xmm4                                    ;274.14
        subss     xmm2, xmm1                                    ;274.14
        movaps    xmm7, xmm2                                    ;274.14
        mov       ecx, DWORD PTR [12+esp]                       ;271.12
        subss     xmm7, xmm3                                    ;274.14
        movaps    xmm5, xmm7                                    ;274.14
        cmpless   xmm7, DWORD PTR [_2il0floatpacket.20]         ;274.14
        cmpnless  xmm5, xmm4                                    ;274.14
        andps     xmm5, xmm6                                    ;274.14
        andps     xmm7, xmm6                                    ;274.14
        subss     xmm2, xmm5                                    ;274.14
        addss     xmm2, xmm7                                    ;274.14
        orps      xmm2, xmm0                                    ;274.14
        cvtss2si  edx, xmm2                                     ;274.1
        cmp       edx, DWORD PTR [ecx]                          ;275.14
        jge       .B4.3         ; Prob 50%                      ;275.14
                                ; LOE eax ebx ebp edi
.B4.2:                          ; Preds .B4.1
        mov       edx, DWORD PTR [32+esp]                       ;271.12
        mov       esi, DWORD PTR [16+esp]                       ;271.12
        mov       DWORD PTR [esp], edi                          ;
        mov       edi, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_TRANSITREPOS+7996+eax*4] ;276.3
        mov       ecx, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_TRANSITREPOS+3996+eax*4] ;277.3
        inc       eax                                           ;281.3
        mov       DWORD PTR [edx], edi                          ;276.3
        mov       DWORD PTR [esi], ecx                          ;277.3
        imul      esi, edi, 176                                 ;278.11
        imul      edi, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE+32], -176 ;278.3
        add       edi, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE] ;278.3
        mov       ecx, DWORD PTR [24+esp]                       ;278.3
        mov       DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_TRANSITREPOSPNTR], eax ;281.3
        mov       edx, DWORD PTR [172+esi+edi]                  ;278.3
        mov       DWORD PTR [ecx], edx                          ;278.3
        mov       edx, DWORD PTR [168+esi+edi]                  ;279.3
        mov       ecx, DWORD PTR [28+esp]                       ;279.3
        mov       esi, DWORD PTR [20+esp]                       ;280.3
        mov       edi, DWORD PTR [esp]                          ;281.3
        mov       DWORD PTR [ecx], edx                          ;279.3
        mov       DWORD PTR [esi], -1                           ;280.3
        jmp       .B4.4         ; Prob 100%                     ;280.3
                                ; LOE ebx ebp edi
.B4.3:                          ; Preds .B4.1
        mov       eax, DWORD PTR [20+esp]                       ;283.3
        mov       DWORD PTR [eax], 0                            ;283.3
                                ; LOE ebx ebp edi
.B4.4:                          ; Preds .B4.2 .B4.3
        pop       ecx                                           ;286.1
        pop       esi                                           ;286.1
        ret                                                     ;286.1
        ALIGN     16
                                ; LOE
; mark_end;
_DYNUST_TRANSIT_MODULE_mp_GET_TRANSIT_VEH ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_TRANSIT_MODULE_mp_GET_TRANSIT_VEH
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_TRANSIT_MODULE_mp_RETRIEVE_TRANSIT_PATH
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_TRANSIT_MODULE_mp_RETRIEVE_TRANSIT_PATH
_DYNUST_TRANSIT_MODULE_mp_RETRIEVE_TRANSIT_PATH	PROC NEAR 
; parameter 1: 32 + esp
; parameter 2: 36 + esp
.B5.1:                          ; Preds .B5.0
        push      esi                                           ;290.12
        push      edi                                           ;290.12
        push      ebx                                           ;290.12
        push      ebp                                           ;290.12
        sub       esp, 12                                       ;290.12
        mov       eax, DWORD PTR [32+esp]                       ;290.12
        mov       ebp, DWORD PTR [36+esp]                       ;290.12
        mov       ebx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY+32] ;297.10
        neg       ebx                                           ;297.36
        mov       edx, DWORD PTR [eax]                          ;297.10
        add       ebx, edx                                      ;297.36
        imul      edi, ebx, 76                                  ;297.36
        imul      ebp, DWORD PTR [ebp], 176                     ;297.36
        mov       eax, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE+32] ;297.10
        imul      ebx, eax, -176                                ;297.36
        mov       ecx, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE] ;297.10
        mov       esi, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY] ;297.7
        movsx     esi, WORD PTR [72+esi+edi]                    ;297.10
        lea       edi, DWORD PTR [ecx+ebp]                      ;297.36
        mov       ebx, DWORD PTR [16+ebx+edi]                   ;297.38
        lea       edi, DWORD PTR [-1+ebx]                       ;297.57
        cmp       esi, edi                                      ;297.36
        jge       .B5.5         ; Prob 50%                      ;297.36
                                ; LOE eax edx ecx ebx ebp esi
.B5.2:                          ; Preds .B5.1
        mov       eax, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_V_INCREASESIZE] ;298.23
        add       eax, esi                                      ;298.48
        cmp       eax, ebx                                      ;298.8
        cmovge    ebx, eax                                      ;298.8
        mov       DWORD PTR [esp], ebx                          ;298.8
        lea       ebx, DWORD PTR [esp]                          ;299.13
        push      ebx                                           ;299.13
        push      DWORD PTR [36+esp]                            ;299.13
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_P_SETUP ;299.13
                                ; LOE ebx
.B5.3:                          ; Preds .B5.2
        push      ebx                                           ;300.13
        push      DWORD PTR [44+esp]                            ;300.13
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_A_SETUP ;300.13
                                ; LOE
.B5.15:                         ; Preds .B5.3
        add       esp, 16                                       ;300.13
                                ; LOE
.B5.4:                          ; Preds .B5.15
        mov       edx, DWORD PTR [32+esp]                       ;301.8
        mov       eax, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY+32] ;301.8
        neg       eax                                           ;301.8
        mov       edx, DWORD PTR [edx]                          ;301.8
        add       eax, edx                                      ;301.8
        imul      ebp, eax, 76                                  ;301.8
        mov       ebx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY] ;301.36
        mov       esi, DWORD PTR [36+esp]                       ;
        mov       ecx, DWORD PTR [esp]                          ;301.36
        mov       WORD PTR [72+ebx+ebp], cx                     ;301.8
        mov       ecx, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE] ;304.7
        mov       eax, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE+32] ;304.7
        imul      ebp, DWORD PTR [esi], 176                     ;
                                ; LOE eax edx ecx ebp
.B5.5:                          ; Preds .B5.4 .B5.1
        imul      eax, eax, -176                                ;304.7
        mov       edi, 1                                        ;304.7
        add       ebp, ecx                                      ;304.7
        mov       WORD PTR [8+esp], di                          ;304.7
        mov       ebx, DWORD PTR [16+eax+ebp]                   ;304.18
        dec       ebx                                           ;304.37
        movsx     ebx, bx                                       ;304.7
        test      ebx, ebx                                      ;304.7
        jle       .B5.10        ; Prob 16%                      ;304.7
                                ; LOE edx ecx ebx edi
.B5.6:                          ; Preds .B5.5
        mov       esi, DWORD PTR [36+esp]                       ;
        mov       ebp, DWORD PTR [32+esp]                       ;
                                ; LOE ecx ebx ebp esi edi
.B5.7:                          ; Preds .B5.18 .B5.6
        mov       edx, DWORD PTR [esi]                          ;305.24
        sub       edx, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE+32] ;305.9
        imul      edx, edx, 176                                 ;305.9
        mov       eax, DWORD PTR [52+ecx+edx]                   ;305.24
        movsx     edi, di                                       ;305.24
        neg       eax                                           ;305.9
        lea       eax, DWORD PTR [1+eax+edi]                    ;305.9
        imul      eax, DWORD PTR [48+ecx+edx]                   ;305.9
        mov       ecx, DWORD PTR [20+ecx+edx]                   ;305.24
        lea       edx, DWORD PTR [4+esp]                        ;306.11
        cvtsi2ss  xmm0, DWORD PTR [ecx+eax]                     ;305.9
        movss     DWORD PTR [4+esp], xmm0                       ;305.9
        push      edx                                           ;306.11
        push      OFFSET FLAT: __NLITPACK_3.0.5                 ;306.11
        lea       ecx, DWORD PTR [16+esp]                       ;306.11
        push      ecx                                           ;306.11
        push      ebp                                           ;306.11
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_INSERT  ;306.11
                                ; LOE ebx ebp esi
.B5.16:                         ; Preds .B5.7
        add       esp, 16                                       ;306.11
                                ; LOE ebx ebp esi
.B5.8:                          ; Preds .B5.16
        dec       ebx                                           ;307.4
        movzx     edi, WORD PTR [8+esp]                         ;307.4
        inc       edi                                           ;307.4
        mov       WORD PTR [8+esp], di                          ;307.4
        test      ebx, ebx                                      ;307.4
        jle       .B5.9         ; Prob 18%                      ;307.4
                                ; LOE ebx ebp esi edi
.B5.18:                         ; Preds .B5.8
        mov       ecx, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE] ;297.10
        jmp       .B5.7         ; Prob 100%                     ;297.10
                                ; LOE ecx ebx ebp esi edi
.B5.9:                          ; Preds .B5.8
        mov       eax, DWORD PTR [32+esp]                       ;308.4
        mov       edx, DWORD PTR [eax]                          ;308.4
                                ; LOE edx
.B5.10:                         ; Preds .B5.5 .B5.9
        mov       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;308.4
        neg       ecx                                           ;308.4
        add       ecx, edx                                      ;308.4
        shl       ecx, 5                                        ;308.4
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;308.4
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_MASTERDEST+32] ;308.4
        neg       esi                                           ;308.4
        movsx     ebx, WORD PTR [20+edx+ecx]                    ;308.4
        add       esi, ebx                                      ;308.4
        mov       ebp, DWORD PTR [_DYNUST_MAIN_MODULE_mp_MASTERDEST] ;308.4
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DESTINATION+32] ;308.4
        neg       eax                                           ;308.4
        movsx     edi, WORD PTR [ebp+esi*2]                     ;308.4
        add       eax, edi                                      ;308.4
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DESTINATION] ;308.4
        cvtsi2ss  xmm0, DWORD PTR [edx+eax*4]                   ;308.4
        movss     DWORD PTR [4+esp], xmm0                       ;308.4
        lea       eax, DWORD PTR [4+esp]                        ;309.9
        push      eax                                           ;309.9
        push      OFFSET FLAT: __NLITPACK_3.0.5                 ;309.9
        lea       ecx, DWORD PTR [16+esp]                       ;309.9
        push      ecx                                           ;309.9
        push      DWORD PTR [44+esp]                            ;309.9
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_INSERT  ;309.9
                                ; LOE
.B5.11:                         ; Preds .B5.10
        mov       eax, DWORD PTR [52+esp]                       ;311.9
        mov       edx, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE+32] ;311.9
        neg       edx                                           ;311.9
        add       edx, DWORD PTR [eax]                          ;311.9
        imul      ecx, edx, 176                                 ;311.9
        mov       ebx, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE] ;311.9
        lea       ebp, DWORD PTR [16+ecx+ebx]                   ;311.9
        push      ebp                                           ;311.9
        push      DWORD PTR [52+esp]                            ;311.9
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ACTIVE_SIZE ;311.9
                                ; LOE
.B5.12:                         ; Preds .B5.11
        add       esp, 36                                       ;312.1
        pop       ebp                                           ;312.1
        pop       ebx                                           ;312.1
        pop       edi                                           ;312.1
        pop       esi                                           ;312.1
        ret                                                     ;312.1
        ALIGN     16
                                ; LOE
; mark_end;
_DYNUST_TRANSIT_MODULE_mp_RETRIEVE_TRANSIT_PATH ENDP
_TEXT	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
__NLITPACK_3.0.5	DD	1
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_TRANSIT_MODULE_mp_RETRIEVE_TRANSIT_PATH
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_TRANSIT_MODULE_mp_GENERATE_TRANSIT_VEH_ATTRIBUTES
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_TRANSIT_MODULE_mp_GENERATE_TRANSIT_VEH_ATTRIBUTES
_DYNUST_TRANSIT_MODULE_mp_GENERATE_TRANSIT_VEH_ATTRIBUTES	PROC NEAR 
; parameter 1: 28 + esp
; parameter 2: 32 + esp
; parameter 3: 36 + esp
; parameter 4: 40 + esp
; parameter 5: 44 + esp
.B6.1:                          ; Preds .B6.0
        push      esi                                           ;316.12
        push      edi                                           ;316.12
        push      ebx                                           ;316.12
        push      ebp                                           ;316.12
        sub       esp, 8                                        ;316.12
        movss     xmm1, DWORD PTR [_2il0floatpacket.28]         ;324.27
        movss     xmm0, DWORD PTR [_2il0floatpacket.29]         ;324.27
        andps     xmm0, xmm1                                    ;324.27
        pxor      xmm1, xmm0                                    ;324.27
        movss     xmm2, DWORD PTR [_2il0floatpacket.30]         ;324.27
        movaps    xmm3, xmm1                                    ;324.27
        movss     xmm4, DWORD PTR [_2il0floatpacket.31]         ;324.27
        cmpltss   xmm3, xmm2                                    ;324.27
        andps     xmm2, xmm3                                    ;324.27
        movaps    xmm3, xmm1                                    ;324.27
        movaps    xmm6, xmm4                                    ;324.27
        addss     xmm3, xmm2                                    ;324.27
        addss     xmm6, xmm4                                    ;324.27
        subss     xmm3, xmm2                                    ;324.27
        movaps    xmm7, xmm3                                    ;324.27
        mov       edx, DWORD PTR [32+esp]                       ;316.12
        subss     xmm7, xmm1                                    ;324.27
        movaps    xmm5, xmm7                                    ;324.27
        cmpless   xmm7, DWORD PTR [_2il0floatpacket.32]         ;324.27
        cmpnless  xmm5, xmm4                                    ;324.27
        andps     xmm5, xmm6                                    ;324.27
        andps     xmm7, xmm6                                    ;324.27
        mov       esi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;322.5
        subss     xmm3, xmm5                                    ;324.27
        shl       esi, 5                                        ;322.5
        addss     xmm3, xmm7                                    ;324.27
        mov       edx, DWORD PTR [edx]                          ;322.5
        mov       ebx, edx                                      ;322.5
        mov       eax, DWORD PTR [40+esp]                       ;316.12
        neg       esi                                           ;322.5
        shl       ebx, 5                                        ;322.5
        orps      xmm3, xmm0                                    ;324.27
        add       esi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;322.5
        mov       ebp, edx                                      ;324.5
        mov       ecx, DWORD PTR [eax]                          ;322.36
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;324.5
        mov       WORD PTR [22+ebx+esi], cx                     ;322.5
        cvtss2si  ecx, xmm3                                     ;324.27
        shl       eax, 8                                        ;324.5
        cvtsi2ss  xmm0, ecx                                     ;324.5
        neg       eax                                           ;324.5
        shl       ebp, 8                                        ;324.5
        add       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;324.5
        mov       edi, DWORD PTR [36+esp]                       ;316.12
        mov       DWORD PTR [4+esp], ebp                        ;324.5
        movss     DWORD PTR [156+ebp+eax], xmm0                 ;324.5
        mov       ebp, DWORD PTR [_DYNUST_MAIN_MODULE_mp_MASTERDEST+32] ;327.5
        neg       ebp                                           ;327.5
        add       ebp, DWORD PTR [edi]                          ;327.5
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_MASTERDEST] ;327.5
        mov       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE+32] ;325.5
        shl       ecx, 6                                        ;325.5
        movzx     ebp, WORD PTR [edi+ebp*2]                     ;327.5
        neg       ecx                                           ;325.5
        mov       DWORD PTR [esp], ebp                          ;327.5
        mov       WORD PTR [20+ebx+esi], bp                     ;327.5
        mov       ebp, DWORD PTR [28+esp]                       ;328.5
        shl       edx, 6                                        ;325.5
        add       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE] ;325.5
        mov       edi, DWORD PTR [ebp]                          ;328.5
        dec       edi                                           ;328.9
        cvtsi2ss  xmm1, edi                                     ;328.9
        mulss     xmm1, DWORD PTR [_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT] ;328.18
        mov       BYTE PTR [5+ebx+esi], 6                       ;323.5
        mov       BYTE PTR [56+edx+ecx], 0                      ;325.5
        mov       BYTE PTR [4+ebx+esi], 1                       ;326.5
        comiss    xmm1, DWORD PTR [_DYNUST_MAIN_MODULE_mp_STARTTM] ;328.33
        jb        .B6.4         ; Prob 50%                      ;328.33
                                ; LOE eax edx ecx ebx esi xmm1
.B6.2:                          ; Preds .B6.1
        movss     xmm0, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ENDTM] ;328.58
        comiss    xmm0, xmm1                                    ;328.73
        jbe       .B6.4         ; Prob 50%                      ;328.73
                                ; LOE eax edx ecx ebx esi xmm1
.B6.3:                          ; Preds .B6.2
        mov       ebp, DWORD PTR [4+esp]                        ;329.7
        mov       BYTE PTR [237+ebp+eax], 1                     ;329.7
        jmp       .B6.5         ; Prob 100%                     ;329.7
                                ; LOE eax edx ecx ebx esi xmm1
.B6.4:                          ; Preds .B6.2 .B6.1
        mov       ebp, DWORD PTR [4+esp]                        ;331.7
        inc       DWORD PTR [_DYNUST_MAIN_MODULE_mp_NUMNTAG]    ;332.7
        mov       BYTE PTR [237+ebp+eax], 0                     ;331.7
                                ; LOE eax edx ecx ebx esi xmm1
.B6.5:                          ; Preds .B6.3 .B6.4
        mov       edi, DWORD PTR [44+esp]                       ;316.12
        xor       ebp, ebp                                      ;335.5
        mov       DWORD PTR [60+edx+ecx], ebp                   ;335.5
        mov       DWORD PTR [52+edx+ecx], ebp                   ;336.5
        mov       ebp, DWORD PTR [edi]                          ;337.2
        movss     DWORD PTR [12+ebx+esi], xmm1                  ;334.5
        mov       DWORD PTR [28+ebx+esi], ebp                   ;337.2
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;338.2
        neg       esi                                           ;338.2
        add       esi, ebp                                      ;338.2
        imul      ebp, esi, 152                                 ;338.2
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;338.2
        mov       edi, DWORD PTR [4+esp]                        ;339.5
        mov       esi, DWORD PTR [20+ebx+ebp]                   ;338.2
        mov       ebx, 1                                        ;339.5
        mov       BYTE PTR [40+edi+eax], bl                     ;339.5
        mov       BYTE PTR [160+edi+eax], bl                    ;340.5
        mov       DWORD PTR [8+edx+ecx], esi                    ;338.2
        mov       eax, DWORD PTR [16+edx+ecx]                   ;341.5
        mov       ecx, DWORD PTR [48+edx+ecx]                   ;341.5
        add       ecx, ecx                                      ;341.5
        neg       ecx                                           ;341.5
        mov       edx, DWORD PTR [esp]                          ;341.5
        mov       WORD PTR [2+ecx+eax], dx                      ;341.5
        add       esp, 8                                        ;343.1
        pop       ebp                                           ;343.1
        pop       ebx                                           ;343.1
        pop       edi                                           ;343.1
        pop       esi                                           ;343.1
        ret                                                     ;343.1
        ALIGN     16
                                ; LOE
; mark_end;
_DYNUST_TRANSIT_MODULE_mp_GENERATE_TRANSIT_VEH_ATTRIBUTES ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_TRANSIT_MODULE_mp_GENERATE_TRANSIT_VEH_ATTRIBUTES
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_TRANSIT_MODULE_mp_CHECK_TRANSIT_RECORDS
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_TRANSIT_MODULE_mp_CHECK_TRANSIT_RECORDS
_DYNUST_TRANSIT_MODULE_mp_CHECK_TRANSIT_RECORDS	PROC NEAR 
; parameter 1: 8 + ebp
.B7.1:                          ; Preds .B7.0
        push      ebp                                           ;346.12
        mov       ebp, esp                                      ;346.12
        and       esp, -16                                      ;346.12
        push      esi                                           ;346.12
        push      edi                                           ;346.12
        push      ebx                                           ;346.12
        sub       esp, 116                                      ;346.12
        mov       DWORD PTR [64+esp], 0                         ;349.1
        lea       edi, DWORD PTR [64+esp]                       ;349.1
        mov       DWORD PTR [8+esp], 24                         ;349.1
        lea       eax, DWORD PTR [8+esp]                        ;349.1
        mov       DWORD PTR [12+esp], OFFSET FLAT: __STRLITPACK_16 ;349.1
        mov       DWORD PTR [16+esp], 7                         ;349.1
        mov       DWORD PTR [20+esp], OFFSET FLAT: __STRLITPACK_15 ;349.1
        push      32                                            ;349.1
        push      eax                                           ;349.1
        push      OFFSET FLAT: __STRLITPACK_145.0.7             ;349.1
        push      -2088435968                                   ;349.1
        push      1                                             ;349.1
        push      edi                                           ;349.1
        call      _for_open                                     ;349.1
                                ; LOE esi edi
.B7.33:                         ; Preds .B7.1
        add       esp, 24                                       ;349.1
                                ; LOE esi edi
.B7.2:                          ; Preds .B7.33
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], 0   ;350.10
        jne       .B7.29        ; Prob 5%                       ;350.10
                                ; LOE esi edi
.B7.42:                         ; Preds .B7.2
        xor       ebx, ebx                                      ;358.3
                                ; LOE ebx esi edi
.B7.3:                          ; Preds .B7.41 .B7.42
        mov       eax, 1                                        ;357.1
        mov       esi, eax                                      ;357.1
                                ; LOE ebx esi edi
.B7.4:                          ; Preds .B7.5 .B7.3
        mov       DWORD PTR [64+esp], ebx                       ;358.3
        push      32                                            ;358.3
        push      ebx                                           ;358.3
        push      OFFSET FLAT: __STRLITPACK_148.0.7             ;358.3
        push      -2088435968                                   ;358.3
        push      1                                             ;358.3
        push      edi                                           ;358.3
        call      _for_read_seq_lis                             ;358.3
                                ; LOE ebx esi edi
.B7.34:                         ; Preds .B7.4
        add       esp, 24                                       ;358.3
                                ; LOE ebx esi edi
.B7.5:                          ; Preds .B7.34
        inc       esi                                           ;359.1
        cmp       esi, 14                                       ;359.1
        jle       .B7.4         ; Prob 92%                      ;359.1
                                ; LOE ebx esi edi
.B7.6:                          ; Preds .B7.5
        mov       DWORD PTR [64+esp], 0                         ;360.1
        lea       eax, DWORD PTR [24+esp]                       ;360.1
        mov       DWORD PTR [24+esp], OFFSET FLAT: _DYNUST_TRANSIT_MODULE_mp_SIMSTARTTIME ;360.1
        push      32                                            ;360.1
        push      eax                                           ;360.1
        push      OFFSET FLAT: __STRLITPACK_149.0.7             ;360.1
        push      -2088435968                                   ;360.1
        push      1                                             ;360.1
        push      edi                                           ;360.1
        call      _for_read_seq_lis                             ;360.1
                                ; LOE ebx esi edi
.B7.7:                          ; Preds .B7.6
        mov       DWORD PTR [56+esp], ebx                       ;362.15
        lea       eax, DWORD PTR [56+esp]                       ;362.15
        push      ebx                                           ;362.15
        push      1                                             ;362.15
        push      eax                                           ;362.15
        call      _for_eof                                      ;362.15
                                ; LOE eax ebx esi edi
.B7.35:                         ; Preds .B7.7
        add       esp, 36                                       ;362.15
        mov       edx, eax                                      ;362.15
                                ; LOE edx ebx esi edi
.B7.8:                          ; Preds .B7.35
        mov       eax, ebx                                      ;
        test      dl, 1                                         ;362.15
        jne       .B7.27        ; Prob 6%                       ;362.15
                                ; LOE eax ebx esi edi
.B7.9:                          ; Preds .B7.8
        mov       esi, ebx                                      ;369.3
        mov       ebx, eax                                      ;369.3
                                ; LOE ebx esi edi
.B7.10:                         ; Preds .B7.9 .B7.25
        mov       DWORD PTR [64+esp], esi                       ;364.3
        inc       ebx                                           ;363.3
        push      32                                            ;364.3
        push      esi                                           ;364.3
        push      OFFSET FLAT: __STRLITPACK_150.0.7             ;364.3
        push      -2088435968                                   ;364.3
        push      1                                             ;364.3
        push      edi                                           ;364.3
        call      _for_read_seq_lis                             ;364.3
                                ; LOE ebx esi edi
.B7.11:                         ; Preds .B7.10
        mov       DWORD PTR [88+esp], esi                       ;365.3
        push      32                                            ;365.3
        push      esi                                           ;365.3
        push      OFFSET FLAT: __STRLITPACK_151.0.7             ;365.3
        push      -2088435968                                   ;365.3
        push      1                                             ;365.3
        push      edi                                           ;365.3
        call      _for_read_seq_lis                             ;365.3
                                ; LOE ebx esi edi
.B7.12:                         ; Preds .B7.11
        mov       DWORD PTR [112+esp], esi                      ;366.3
        push      32                                            ;366.3
        push      esi                                           ;366.3
        push      OFFSET FLAT: __STRLITPACK_152.0.7             ;366.3
        push      -2088435968                                   ;366.3
        push      1                                             ;366.3
        push      edi                                           ;366.3
        call      _for_read_seq_lis                             ;366.3
                                ; LOE ebx esi edi
.B7.13:                         ; Preds .B7.12
        mov       DWORD PTR [136+esp], esi                      ;367.3
        push      32                                            ;367.3
        push      esi                                           ;367.3
        push      OFFSET FLAT: __STRLITPACK_153.0.7             ;367.3
        push      -2088435968                                   ;367.3
        push      1                                             ;367.3
        push      edi                                           ;367.3
        call      _for_read_seq_lis                             ;367.3
                                ; LOE ebx esi edi
.B7.14:                         ; Preds .B7.13
        mov       DWORD PTR [160+esp], 0                        ;369.3
        lea       edx, DWORD PTR [192+esp]                      ;369.3
        mov       DWORD PTR [200+esp], edx                      ;369.3
        push      32                                            ;369.3
        lea       ecx, DWORD PTR [204+esp]                      ;369.3
        push      ecx                                           ;369.3
        push      OFFSET FLAT: __STRLITPACK_154.0.7             ;369.3
        push      -2088435968                                   ;369.3
        push      1                                             ;369.3
        push      edi                                           ;369.3
        call      _for_read_seq_lis                             ;369.3
                                ; LOE ebx esi edi
.B7.36:                         ; Preds .B7.14
        add       esp, 120                                      ;369.3
                                ; LOE ebx esi edi
.B7.15:                         ; Preds .B7.36
        lea       ecx, DWORD PTR [112+esp]                      ;369.3
        lea       edx, DWORD PTR [100+esp]                      ;369.3
        mov       DWORD PTR [112+esp], edx                      ;369.3
        push      ecx                                           ;369.3
        push      OFFSET FLAT: __STRLITPACK_155.0.7             ;369.3
        push      edi                                           ;369.3
        call      _for_read_seq_lis_xmit                        ;369.3
                                ; LOE ebx esi edi
.B7.37:                         ; Preds .B7.15
        add       esp, 12                                       ;369.3
                                ; LOE ebx esi edi
.B7.16:                         ; Preds .B7.37
        mov       edx, DWORD PTR [96+esp]                       ;372.3
        test      edx, edx                                      ;372.3
        jle       .B7.24        ; Prob 2%                       ;372.3
                                ; LOE edx ebx esi edi
.B7.17:                         ; Preds .B7.16
        mov       eax, 1                                        ;
        mov       DWORD PTR [108+esp], edx                      ;
        mov       DWORD PTR [28+esp], ebx                       ;
        mov       ebx, eax                                      ;
                                ; LOE ebx esi edi
.B7.18:                         ; Preds .B7.22 .B7.17
        mov       DWORD PTR [64+esp], esi                       ;373.5
        push      32                                            ;373.5
        push      esi                                           ;373.5
        push      OFFSET FLAT: __STRLITPACK_156.0.7             ;373.5
        push      -2088435968                                   ;373.5
        push      1                                             ;373.5
        push      edi                                           ;373.5
        call      _for_read_seq_lis                             ;373.5
                                ; LOE ebx esi edi
.B7.19:                         ; Preds .B7.18
        mov       DWORD PTR [88+esp], esi                       ;374.5
        push      32                                            ;374.5
        push      esi                                           ;374.5
        push      OFFSET FLAT: __STRLITPACK_157.0.7             ;374.5
        push      -2088435968                                   ;374.5
        push      1                                             ;374.5
        push      edi                                           ;374.5
        call      _for_read_seq_lis                             ;374.5
                                ; LOE ebx esi edi
.B7.20:                         ; Preds .B7.19
        mov       DWORD PTR [112+esp], esi                      ;375.5
        push      32                                            ;375.5
        push      esi                                           ;375.5
        push      OFFSET FLAT: __STRLITPACK_158.0.7             ;375.5
        push      -2088435968                                   ;375.5
        push      1                                             ;375.5
        push      edi                                           ;375.5
        call      _for_read_seq_lis                             ;375.5
                                ; LOE ebx esi edi
.B7.21:                         ; Preds .B7.20
        mov       DWORD PTR [136+esp], esi                      ;376.5
        push      32                                            ;376.5
        push      esi                                           ;376.5
        push      OFFSET FLAT: __STRLITPACK_159.0.7             ;376.5
        push      -2088435968                                   ;376.5
        push      1                                             ;376.5
        push      edi                                           ;376.5
        call      _for_read_seq_lis                             ;376.5
                                ; LOE ebx esi edi
.B7.38:                         ; Preds .B7.21
        add       esp, 96                                       ;376.5
                                ; LOE ebx esi edi
.B7.22:                         ; Preds .B7.38
        inc       ebx                                           ;377.3
        cmp       ebx, DWORD PTR [108+esp]                      ;377.3
        jle       .B7.18        ; Prob 82%                      ;377.3
                                ; LOE ebx esi edi
.B7.23:                         ; Preds .B7.22
        mov       ebx, DWORD PTR [28+esp]                       ;
                                ; LOE ebx esi edi
.B7.24:                         ; Preds .B7.23 .B7.16
        mov       DWORD PTR [32+esp], esi                       ;362.15
        push      esi                                           ;362.15
        push      1                                             ;362.15
        lea       edx, DWORD PTR [40+esp]                       ;362.15
        push      edx                                           ;362.15
        call      _for_eof                                      ;362.15
                                ; LOE eax ebx esi edi
.B7.39:                         ; Preds .B7.24
        add       esp, 12                                       ;362.15
                                ; LOE eax ebx esi edi
.B7.25:                         ; Preds .B7.39
        test      al, 1                                         ;362.15
        je        .B7.10        ; Prob 82%                      ;362.15
                                ; LOE ebx esi edi
.B7.26:                         ; Preds .B7.25
        mov       eax, ebx                                      ;
        mov       ebx, esi                                      ;
                                ; LOE eax ebx esi edi
.B7.27:                         ; Preds .B7.8 .B7.26
        mov       DWORD PTR [64+esp], ebx                       ;380.7
        push      32                                            ;380.7
        push      ebx                                           ;380.7
        push      OFFSET FLAT: __STRLITPACK_160.0.7             ;380.7
        mov       edx, DWORD PTR [8+ebp]                        ;346.12
        push      -2088435968                                   ;380.7
        push      1                                             ;380.7
        push      edi                                           ;380.7
        mov       DWORD PTR [edx], eax                          ;379.1
        call      _for_close                                    ;380.7
                                ; LOE esi
.B7.28:                         ; Preds .B7.27
        add       esp, 140                                      ;381.1
        pop       ebx                                           ;381.1
        pop       edi                                           ;381.1
        pop       esi                                           ;381.1
        mov       esp, ebp                                      ;381.1
        pop       ebp                                           ;381.1
        ret                                                     ;381.1
                                ; LOE
.B7.29:                         ; Preds .B7.2                   ; Infreq
        mov       DWORD PTR [64+esp], 0                         ;351.4
        lea       eax, DWORD PTR [esp]                          ;351.4
        mov       DWORD PTR [esp], 35                           ;351.4
        mov       DWORD PTR [4+esp], OFFSET FLAT: __STRLITPACK_13 ;351.4
        push      32                                            ;351.4
        push      eax                                           ;351.4
        push      OFFSET FLAT: __STRLITPACK_146.0.7             ;351.4
        push      -2088435968                                   ;351.4
        push      911                                           ;351.4
        push      edi                                           ;351.4
        call      _for_write_seq_lis                            ;351.4
                                ; LOE esi edi
.B7.30:                         ; Preds .B7.29                  ; Infreq
        push      32                                            ;352.4
        xor       ebx, ebx                                      ;352.4
        push      ebx                                           ;352.4
        push      ebx                                           ;352.4
        push      -2088435968                                   ;352.4
        push      ebx                                           ;352.4
        push      OFFSET FLAT: __STRLITPACK_147                 ;352.4
        call      _for_stop_core                                ;352.4
                                ; LOE ebx esi edi
.B7.41:                         ; Preds .B7.30                  ; Infreq
        add       esp, 48                                       ;352.4
        jmp       .B7.3         ; Prob 100%                     ;352.4
        ALIGN     16
                                ; LOE ebx esi edi
; mark_end;
_DYNUST_TRANSIT_MODULE_mp_CHECK_TRANSIT_RECORDS ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_TRANSIT_MODULE_mp_CHECK_TRANSIT_RECORDS
_BSS	SEGMENT  DWORD PUBLIC FLAT  'BSS'
	PUBLIC _DYNUST_TRANSIT_MODULE_mp_VEHGENID
_DYNUST_TRANSIT_MODULE_mp_VEHGENID	DD 1 DUP (0H)	; pad
	PUBLIC _DYNUST_TRANSIT_MODULE_mp_ROUTEERROR
_DYNUST_TRANSIT_MODULE_mp_ROUTEERROR	DD 1 DUP (0H)	; pad
_BSS	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
	DD 1 DUP (0H)	; pad
	PUBLIC _DYNUST_TRANSIT_MODULE_mp_M_SCHEDULE
_DYNUST_TRANSIT_MODULE_mp_M_SCHEDULE	DD	0
	DD	0
	DD	0
	DD	128
	DD	1
	DD	0
	DD	0
	DD	0
	DD	0
	DD 1 DUP (0H)	; pad
	PUBLIC _DYNUST_TRANSIT_MODULE_mp_M_ROUTE
_DYNUST_TRANSIT_MODULE_mp_M_ROUTE	DD	0
	DD	0
	DD	0
	DD	128
	DD	1
	DD	0
	DD	0
	DD	0
	DD	0
_DATA	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DD 3 DUP (0H)	; pad
_2il0floatpacket.2	DD	00098967fH,00098967fH,00098967fH,00098967fH
__STRLITPACK_69	DB	84
	DB	114
	DB	97
	DB	110
	DB	115
	DB	105
	DB	116
	DB	82
	DB	111
	DB	117
	DB	116
	DB	101
	DB	83
	DB	99
	DB	104
	DB	101
	DB	100
	DB	117
	DB	108
	DB	101
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_16	DB	84
	DB	114
	DB	97
	DB	110
	DB	115
	DB	105
	DB	116
	DB	82
	DB	111
	DB	117
	DB	116
	DB	101
	DB	83
	DB	99
	DB	104
	DB	101
	DB	100
	DB	117
	DB	108
	DB	101
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_15	DB	117
	DB	110
	DB	107
	DB	110
	DB	111
	DB	119
	DB	110
	DB	0
__STRLITPACK_145.0.7	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	26
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_13	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	119
	DB	104
	DB	101
	DB	110
	DB	32
	DB	111
	DB	112
	DB	101
	DB	110
	DB	105
	DB	110
	DB	103
	DB	32
	DB	84
	DB	114
	DB	97
	DB	110
	DB	115
	DB	105
	DB	116
	DB	82
	DB	111
	DB	117
	DB	116
	DB	101
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
__STRLITPACK_146.0.7	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_147	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_148.0.7	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_149.0.7	DB	26
	DB	3
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_150.0.7	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_151.0.7	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_152.0.7	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_153.0.7	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_154.0.7	DB	9
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_155.0.7	DB	9
	DB	3
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_156.0.7	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_157.0.7	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_158.0.7	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_159.0.7	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_160.0.7	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_68	DB	84
	DB	114
	DB	97
	DB	110
	DB	115
	DB	105
	DB	116
	DB	82
	DB	111
	DB	117
	DB	116
	DB	101
	DB	83
	DB	99
	DB	104
	DB	101
	DB	100
	DB	117
	DB	108
	DB	101
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_67	DB	117
	DB	110
	DB	107
	DB	110
	DB	111
	DB	119
	DB	110
	DB	0
__STRLITPACK_57	DB	84
	DB	114
	DB	97
	DB	110
	DB	115
	DB	105
	DB	116
	DB	32
	DB	82
	DB	111
	DB	117
	DB	116
	DB	101
	DB	32
	DB	110
	DB	111
	DB	100
	DB	101
	DB	32
	DB	100
	DB	111
	DB	101
	DB	115
	DB	32
	DB	110
	DB	111
	DB	116
	DB	32
	DB	101
	DB	120
	DB	105
	DB	115
	DB	116
	DB	32
	DB	105
	DB	110
	DB	32
	DB	84
	DB	114
	DB	97
	DB	110
	DB	115
	DB	105
	DB	116
	DB	82
	DB	111
	DB	117
	DB	116
	DB	101
	DB	83
	DB	99
	DB	104
	DB	101
	DB	100
	DB	117
	DB	108
	DB	101
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_54	DB	82
	DB	111
	DB	117
	DB	116
	DB	101
	DB	32
	DB	35
	DB	32
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_53	DB	78
	DB	79
	DB	68
	DB	69
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_63	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	84
	DB	114
	DB	97
	DB	110
	DB	115
	DB	105
	DB	116
	DB	82
	DB	111
	DB	117
	DB	116
	DB	101
	DB	83
	DB	99
	DB	104
	DB	101
	DB	100
	DB	117
	DB	108
	DB	101
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_51	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	114
	DB	101
	DB	97
	DB	100
	DB	105
	DB	110
	DB	103
	DB	32
	DB	116
	DB	104
	DB	101
	DB	32
	DB	114
	DB	111
	DB	117
	DB	116
	DB	101
	DB	32
	DB	102
	DB	105
	DB	108
	DB	101
	DB	0
__STRLITPACK_49	DB	80
	DB	108
	DB	101
	DB	97
	DB	115
	DB	101
	DB	32
	DB	99
	DB	104
	DB	101
	DB	99
	DB	107
	DB	32
	DB	82
	DB	111
	DB	117
	DB	116
	DB	101
	DB	32
	DB	35
	DB	32
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_100	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_47	DB	69
	DB	82
	DB	82
	DB	79
	DB	82
	DB	32
	DB	73
	DB	78
	DB	32
	DB	70
	DB	73
	DB	78
	DB	68
	DB	73
	DB	78
	DB	71
	DB	32
	DB	79
	DB	82
	DB	73
	DB	71
	DB	73
	DB	78
	DB	32
	DB	90
	DB	79
	DB	78
	DB	69
	DB	32
	DB	70
	DB	79
	DB	82
	DB	32
	DB	84
	DB	82
	DB	65
	DB	78
	DB	83
	DB	73
	DB	84
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_45	DB	82
	DB	79
	DB	85
	DB	84
	DB	69
	DB	32
	DB	78
	DB	85
	DB	77
	DB	66
	DB	69
	DB	82
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_104	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_43	DB	69
	DB	82
	DB	82
	DB	79
	DB	82
	DB	32
	DB	73
	DB	78
	DB	32
	DB	70
	DB	73
	DB	78
	DB	68
	DB	73
	DB	78
	DB	71
	DB	32
	DB	68
	DB	69
	DB	83
	DB	84
	DB	73
	DB	78
	DB	65
	DB	84
	DB	73
	DB	79
	DB	78
	DB	32
	DB	90
	DB	79
	DB	78
	DB	69
	DB	32
	DB	70
	DB	79
	DB	82
	DB	32
	DB	84
	DB	82
	DB	65
	DB	78
	DB	83
	DB	73
	DB	84
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_41	DB	82
	DB	79
	DB	85
	DB	84
	DB	69
	DB	32
	DB	78
	DB	85
	DB	77
	DB	66
	DB	69
	DB	82
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_108	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_37	DB	82
	DB	111
	DB	117
	DB	116
	DB	101
	DB	32
	DB	69
	DB	114
	DB	114
	DB	111
	DB	32
	DB	97
	DB	116
	DB	32
	DB	73
	DB	68
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_23	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	84
	DB	114
	DB	97
	DB	110
	DB	115
	DB	105
	DB	116
	DB	32
	DB	83
	DB	99
	DB	104
	DB	101
	DB	100
	DB	117
	DB	108
	DB	101
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_21	DB	82
	DB	79
	DB	85
	DB	84
	DB	69
	DB	32
	DB	73
	DB	68
	DB	32
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_19	DB	83
	DB	67
	DB	72
	DB	69
	DB	68
	DB	85
	DB	76
	DB	69
	DB	32
	DB	35
	DB	32
	DB	0
__STRLITPACK_17	DB	65
	DB	82
	DB	82
	DB	73
	DB	86
	DB	65
	DB	76
	DB	32
	DB	84
	DB	73
	DB	77
	DB	69
	DB	32
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_134	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_142	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_143	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_144	DB	0
	DB 3 DUP ( 0H)	; pad
_2il0floatpacket.3	DD	042c80000H
_2il0floatpacket.5	DD	042c80000H
_2il0floatpacket.6	DD	080000000H
_2il0floatpacket.7	DD	04b000000H
_2il0floatpacket.8	DD	03f000000H
_2il0floatpacket.9	DD	0bf000000H
_2il0floatpacket.16	DD	042c80000H
_2il0floatpacket.17	DD	080000000H
_2il0floatpacket.18	DD	04b000000H
_2il0floatpacket.19	DD	03f000000H
_2il0floatpacket.20	DD	0bf000000H
_2il0floatpacket.28	DD	043160000H
_2il0floatpacket.29	DD	080000000H
_2il0floatpacket.30	DD	04b000000H
_2il0floatpacket.31	DD	03f000000H
_2il0floatpacket.32	DD	0bf000000H
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NUMNTAG:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ENDTM:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_STARTTM:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_MASTERDEST:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_DESTINATION:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_LOGOUT:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ERROR:BYTE
EXTRN	_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE:BYTE
EXTRN	_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH:BYTE
EXTRN	_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND:BYTE
EXTRN	_DYNUST_VEH_PATH_ATT_MODULE_mp_V_INCREASESIZE:BYTE
EXTRN	_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY:BYTE
	COMM _DYNUST_TRANSIT_MODULE_mp_TRANSITREPOSPNTR:BYTE:4
	COMM _DYNUST_TRANSIT_MODULE_mp_TRANSITREPOS:BYTE:20000
	COMM _DYNUST_TRANSIT_MODULE_mp_TEMPA:BYTE:4000
	COMM _DYNUST_TRANSIT_MODULE_mp_SIMSTARTTIME:BYTE:4
	COMM _DYNUST_TRANSIT_MODULE_mp_NUMROUTES:BYTE:4
_DATA	ENDS
EXTRN	_for_eof:PROC
EXTRN	_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_P_SETUP:PROC
EXTRN	_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_A_SETUP:PROC
EXTRN	_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_INSERT:PROC
EXTRN	_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ACTIVE_SIZE:PROC
EXTRN	_for_stop_core:PROC
EXTRN	_for_write_seq_lis_xmit:PROC
EXTRN	_for_write_seq_fmt_xmit:PROC
EXTRN	_for_write_seq_lis:PROC
EXTRN	_for_allocate:PROC
EXTRN	_for_read_seq_lis_xmit:PROC
EXTRN	_for_write_seq_fmt:PROC
EXTRN	_for_read_seq_lis:PROC
EXTRN	_for_open:PROC
EXTRN	_for_alloc_allocatable:PROC
EXTRN	_for_check_mult_overflow:PROC
EXTRN	_for_close:PROC
EXTRN	_for_inquire:PROC
EXTRN	_DYNUST_NETWORK_MODULE_mp_GETFLINKFROMNODE:PROC
EXTRN	_IMSLSORT:PROC
EXTRN	__fltused:BYTE
	INCLUDELIB <ifconsol>
	INCLUDELIB <libifcoremt>
	INCLUDELIB <libifport>
	INCLUDELIB <libmmt>
	INCLUDELIB <LIBCMT>
	INCLUDELIB <libirc>
	INCLUDELIB <svml_dispmt>
	INCLUDELIB <OLDNAMES>
	END
