      SUBROUTINE SIM_DYNUST(maxintervals)
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~ Copyright (C) 2002-2011 The Arizona Board of Regents on Behalf of The University of Arizona.  ~~!
!~~ Developed by Yi-Chang Chiu, Ph.D. as par to the DynusT project.                               ~~!
!~~ This program is free software: you can redistribute it and/or modify it                       ~~!
!~~ under the terms of the GNU General Public License version 2 as published by                   ~~!
!~~ the Free Software Foundation.                                                                 ~~!
!~~ This program is distributed in the hope that it will be useful,                               ~~!
!~~ but WITHOUT ANY WARRANTY; without even the implied warranty of                                ~~!
!~~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                                          ~~!
!~~ See the GNU General Public License for more details.                                          ~~!
!~~ You should have received a copy of the GNU General Public License                             ~~!
!~~ along with this program (in the file COPYING).                                                ~~!
!~~ IF not, see <http://www.gnu.org/licenses/>.                                                   ~~!
!~~ Please see http://www.dynust.net for contact information and names                            ~~!
!~~ of individual contributors.                                                                   ~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!

      USE DYNUST_MAIN_MODULE
      USE DYNUST_VEH_MODULE
      USE DYNUST_VEH_PATH_ATT_MODULE
      USE DYNUST_CONGESTIONPRICING_MODULE
      USE DYNUST_FUEL_MODULE
      USE MEMORY_MODULE
      !USE DYNUST_MOVES_MODULE
      USE DYNUST_TDSP_ASTAR_MODULE
      USE IFQWIN
      INTEGER maxintervals
      INTEGER dynust_mode_flag
      CHARACTER(80) printstr1,printstr2
      INTEGER :: Size

!> > > > > > > > > >                < < < < < < < < < < <
!>>>>>>>>>>>>>>>>>>>CODES START HERE<<<<<<<<<<<<<<<<<<<<<
!> > > > > > > > > >                < < < < < < < < < < <

      dynust_mode_flag = 0


      CALL OPENFILE
      
      IF(logout > 0) WRITE(711,*) 'init, next input'
      CALL INITIALIZE
! --
      IF(logout > 0) WRITE(711,*) 'input, next title'

      CALL READ_INPUT()
      
     
      
	  IF(mod(nint(SimPeriod),EpocNum) /= 0) THEN
        WRITE(911,*) 'Error in setting up stagelenght and number of Epoches'
	    WRITE(911,*) 'SimPeriod should be multiples of the number of Epoches'
	    STOP
	  ENDIF


      IF(Mod(nint(nint(SimPeriod)/EpocNum/xminPerSimInt),simPerAgg) /= 0) THEN
        WRITE(911,*) 'Error in setting up the Epoch.dat and aggregation interval in system.dat'
	    WRITE(911,*) 'Epoch period resulted from the division of stage length and # of epochs'
	    WRITE(911,*) 'should be the multiple of the aggregation interval'
		WRITE(911,*) 'SimPeriod = ', SimPeriod
		WRITE(911,*) '# of Epochs = ', EpocNum
		WRITE(911,*) 'aggregation interval = ', simPerAgg
        WRITE(911,*) 'nint(nint(SimPeriod)/EpocNum/xminPerSimInt) = ',nint(nint(SimPeriod)/EpocNum/xminPerSimInt)
	    STOP
	  ENDIF


     EpocPeriod = ifix(SimPeriod/EpocNum)
	 iti_nuEP = nint((EpocPeriod/xminPerSimInt)/simPerAgg)          

	  IF(aggint < EpocNum.and.iteMax > 0) THEN
          WRITE(911,*) 'Error in setting up aggregation interval and # of Epoches'
		  WRITE(911,*) '# of aggregation intervals should be greater than # of Epoches'
		  WRITE(911,*) '# of epoches = ', EpocNum
		  WRITE(911,*) '# of aggregation intervals = ', aggint
          STOP
	  ENDIF


      class4=classpro(4)-classpro(3)

      stagest = 0.0
      IF(iso_ok == 1.or.iue_ok == 1) time_now=stagest*60

	  IF(RunMode /= 1.and.iteration == 0.and.iteMax > 0) THEN ! IF read vehicle file, check IF SO/UE vehicle exist
	   CALL check_miva_vehicle
	  ENDIF

      CALL printheader1
     
      IF(logout > 0) WRITE(711,*) 'CALL loop'

      icallksp = .false.
      
      CALL SIM_MAIN_LOOP(maxintervals)
      
      totalveh = justveh
      IF(RunMode == 1.OR.RunMode == 3.OR.RunMode == 4) maxid = justveh
      
      close(97)
      CALL ReSort_OutPut_Veh() ! oneshot 

      CALL printheader2
 
      CALL WRITE_SUMMARY_STAT(maxintervals)

      CALL CAL_PENALTY_MIVA

      IF(iteMax == 0.or.(iteMax > 0.and.(reach_converg.or.iteration == iteMax))) THEN ! this is the last UE iteration
       !CALL WriteGap
       IF(UETimeFlagW > 0) CALL WriteUETravelTime
      ENDIF      

      CALL WRITE_TOLL_REVENUE(maxintervals) ! to generation output for toll

! the followings are to update the congestion pricing rates
      IF(NSeg > 0) THEN
        CALL PriceCal
        CALL PriceOutput
        CLOSE(62)
        CLOSE(7778)
        CALL system('del .\toll.dat')
        CALL system('rename .\toll_CongP.dat toll.dat')
      ENDIF
      
      IF(FuelOut == 1.and.(iteration == iteMax.or.reach_converg == .true.)) THEN
        CALL WriteOutFuelConsum
        CALL WriteOutFuelConsum_City
      ENDIF  
      
      CALL DEALLOCATE_SIM
      !IF(MovesFlag > 0) CALL ClearMovesData

      CLOSE(6099)  

END SUBROUTINE

!~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>
SUBROUTINE printheader1
      USE DYNUST_MAIN_MODULE
      USE DYNUST_NETWORK_MODULE
      USE DYNUST_VEH_PATH_ATT_MODULE
      USE IFQWIN
      USE DYNUST_VEH_MODULE
      CHARACTER(80) printstr1, printstr2
      INTEGER maxintervals
      INTEGER dynust_mode_flag
      dynust_mode_flag = 0


! -- print output file headers      
! --
      CALL WRITE_TITLE()   
! --      
! -- print out static network information.
! --
      WRITE(666,*) ' '
      WRITE(666,*) '****************************************'
      WRITE(666,*) '*      Basic Information               *'
      WRITE(666,*) '****************************************'
      WRITE(666,*)
      WRITE(666,*) 'NETWORK DATA '
      WRITE(666,*) '------------ '
      WRITE(666,'( "    Number of Nodes                          : ",i7)') noofnodes_org
      WRITE(666,'( "    Number of Links                          : ",i7)') noofarcs_org
      WRITE(666,'( "    Number of Zones                          : ",i7)') nzones
      WRITE(666,*) '***************************************'
      WRITE(666,*)
      WRITE(666,*) 'INTERSECTION CONTROL DATA'
      WRITE(666,*) '-------------------------'
      nodetmp(:)=0

      DO i=1,noofnodes
         DO j=1,nu_control
           IF(m_dynust_network_node(i)%node(2) == j) nodetmp(j)=nodetmp(j)+1
         end do
      end do
! --
      WRITE(666,'( "    Number of No Control                     : ",i7)') nodetmp(1)
      WRITE(666,'( "    Number of Yield Signs                    : ",i7)') nodetmp(2)
      WRITE(666,'( "    Number of 4-Way STOP Signs               : ",i7)') nodetmp(3)
      WRITE(666,'( "    Number of 2-Way STOP Signs               : ",i7)') nodetmp(6)
      WRITE(666,'( "    Number of Pretimed Control               : ",i7)') nodetmp(4)
      WRITE(666,'( "    Number of Actuated Control               : ",i7)') nodetmp(5)
      WRITE(666,*) '***************************************'
      WRITE(666,*)
      WRITE(666,*) 'RAMP DATA '
      WRITE(666,*) '---------'
      WRITE(666,'( "    Number of Ramp Control                   : ",i7)') dec_num
      WRITE(666,'( "    Number of VMS Control                    : ",i7)') vms_num
      WRITE(666,*) '***************************************'
      WRITE(666,*)
      WRITE(666,*) 'SOLUTION MODE '
      WRITE(666,*) '-------------'
      IF(iteMax == 0) THEN
        WRITE(666,*) '  Execute One-Shot Simulation Mode'
	  ELSE
	  WRITE(666,*) '   Execute Iterative Consistency Algorithm(Equlilbrium)'
	  WRITE(666,'( "    Max. Number of Iterations                : ",i7)') iteMax
	  WRITE(666,'( "    Current Iteration                        : ",i7)') iteration
	  ENDIF
      WRITE(666,*) '***************************************'
      WRITE(666,*)
      WRITE(666,*) 'TIME PERIODS '
      WRITE(666,*) '------------'
      WRITE(666,'( "    Planning Horizon(min)                    : ",f9.1)') SimPeriod 
      WRITE(666,'( "    Aggregation Interval(# of Sim Int)       : ",i7)') simPerAgg
      WRITE(666,'( "    Assignment Interval(# of Sim Int)        : ",i7)') simPerAsg
	  WRITE(666,'( "    Max # of Iterations                      : ",i7)') iteMax
	  WRITE(666,'( "    mtc Threshold (# of Vehicles)            : ",f9.1)') mtc_diff
	  WRITE(666,'( "    Convergence Threshold - Gap Function %   : ",f5.2)') no_via/10.0
      WRITE(666,*) '***************************************'
      WRITE(666,*)
      WRITE(666,*) 'FOR CONGESTION PRICING DETAILS CHECK TOLL.DAT '
      WRITE(666,*) '------------------'
      WRITE(666,*)
      WRITE(666,*) 'VEHICLE LOADING MODE '
      WRITE(666,*) '--------------------'
      IF(RunMode == 1) THEN
	  WRITE(666,'( "    O-D Demand Table                                  ")')    
	  ELSEIF(RunMode == 0) THEN
	  WRITE(666,'( "    Vehicle File, Initial Path Generated by DynusT ")') 
	  ELSEIF(RunMode == 2) THEN
	  WRITE(666,'( "    Vehicle + Path File                               ")')
	  ELSEIF(RunMode == 3) THEN
	  WRITE(666,'( "    demand + Vehicle ile                              ")')
	  ELSEIF(RunMode == 4) THEN
	  WRITE(666,'( "    demand+Vehicle+Path File                          ")')


	  ENDIF
      WRITE(666,*)
      WRITE(666,'("Tripmakers perceive freeway travel time to be",f6.1," % lower than arterials")') TravelTimeWeight
      WRITE(666,*)
      WRITE(666,*) '***************************************'
      WRITE(666,*)
      WRITE(666,*) 'TRAFFIC MANAGEMENT STRATEGIES '
      WRITE(666,*) '-----------------------------'
	  IF(vms_num > 0) THEN
      WRITE(666,'( "    Number of Activated Dynamic Message Signs: ",i4)') vms_num
	  DO iv = 1, vms_num
	  IF(vmstype(iv) == 1) THEN
      WRITE(666,*) 
	  WRITE(666,'( "    VMS #   ",i3,"    Type:  Speed Advisory")') iv
  	  WRITE(666,'( "       Location",i5,"  --",i5,"   From min ",f6.1," To min ",f6.1)') m_dynust_network_node_nde(m_dynust_network_arc_nde(vms(iv,1))%iunod)%IntoOutNodeNum,m_dynust_network_node_nde(m_dynust_network_arc_nde(vms(iv,1))%idnod)%IntoOutNodeNum,vms_start(iv),vms_end(iv)
  	  WRITE(666,'( "       Speed Threshold  ",f5.1," mph.   Speed Adjustment Percentage: ",f5.1," %")') float(vms(iv,2)),float(vms(iv,3))

	  ELSEIF(vmstype(iv) == 2) THEN

      WRITE(666,*)
	  WRITE(666,'( "    VMS #   ",i3,"    Type:  Mandatory Detour")') iv
  	  WRITE(666,'( "       Location",i5,"  --",i5,"   From min ",f6.1," To min ",f6.1)') m_dynust_network_node_nde(m_dynust_network_arc_nde(vms(iv,1))%iunod)%IntoOutNodeNum,m_dynust_network_node_nde(m_dynust_network_arc_nde(vms(iv,1))%idnod)%IntoOutNodeNum,vms_start(iv),vms_end(iv)	 
	  WRITE(666,'( "       Diversion Applies To All Vehicles")') 

	  ELSEIF(vmstype(iv) == 3) THEN

	  WRITE(666,*)
      WRITE(666,'( "    VMS #   ",i3,"    Type:  Congestion Warning")') iv
  	  WRITE(666,'( "       Location",i5,"  --",i5,"   From min ",f6.1," To min ",f6.1)') m_dynust_network_node_nde(m_dynust_network_arc_nde(vms(iv,1))%iunod)%IntoOutNodeNum,m_dynust_network_node_nde(m_dynust_network_arc_nde(vms(iv,1))%idnod)%IntoOutNodeNum,vms_start(iv),vms_end(iv)     
      IF(vms(iv,3) == 1) THEN
	  WRITE(666,'( "       The Best Path is Assigned to Responded Vehicles")')

	  ELSE

	  WRITE(666,'( "       Random Paths Are Assigned to Responded Vehicles")')
	  ENDIF
	  WRITE(666,'( "     ",i5," % of Out-of-Vehicle Responsive Vehicles Respond to VMS")') vms(iv,2)
	  ENDIF
	  ENDDO
	  ELSE
	  WRITE(666,'( "    No Traffic Management Strategy Was Specified")')
	  ENDIF

      WRITE(666,*) '***************************************'
      WRITE(666,*)
      WRITE(666,*) 'CAPACITY REDUCTION '
      WRITE(666,*) '------------------ '
	  IF(inci_num > 0) THEN
       WRITE(666,*) '   -- Incident  --'
       DO ic = 1, inci_num
  	    WRITE(666,'( "       Location",i7,"  --",i7,"   From min ",f8.1," To min ",f8.1,",  ",f6.1," % Capacity Reduction")') m_dynust_network_node_nde(m_dynust_network_arc_nde(incil(ic))%iunod)%IntoOutNodeNum,m_dynust_network_node_nde(m_dynust_network_arc_nde(incil(ic))%idnod)%IntoOutNodeNum,inci(ic,1),inci(ic,2),inci(ic,3)*100
	   ENDDO
	  ENDIF
	  IF(WorkZoneNum > 0) THEN
       WRITE(666,*) '   -- Work Zone --'
       DO ic = 1, WorkZoneNum
  	    WRITE(666,'( "       Location",i7,"  --",i7,"   From min ",f8.1," To min ",f8.1,",  ",f4.1," % Capacity Reduction")') m_dynust_network_node_nde(WorkZone(ic)%FNode)%IntoOutNodeNum,m_dynust_network_node_nde(WorkZone(ic)%TNode)%IntoOutNodeNum,WorkZone(ic)%ST,WorkZone(ic)%ET,WorkZone(ic)%CapRed*100.0

	   ENDDO
	  ENDIF
	  IF(inci_num == 0.and.WorkZoneNum == 0)THEN
       WRITE(666,'( "    No Capacity Reduction Scenario Was Specified ")')
	  ENDIF

      IF(RunMode == 1) THEN
        printstr1 = "OD Table"
      ELSEIF(RunMode == 0) THEN       
        printstr1 = "Vehicle File"
      ELSEIF(RunMode == 2) THEN       
        printstr1 = "Vehicle+Path File"
      ELSEIF(RunMode == 3) THEN       
        printstr1 = "deman+Vehicle File"
      ELSE
        printstr1 = "demand+Veh+Path Files"       
      ENDIF

      WRITE(666,*)
      WRITE(666,*) '****************************************'
      WRITE(666,*) '*      Loading Information             *'
      WRITE(666,*) '****************************************'
      WRITE(666,*) 


END SUBROUTINE

!~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>
SUBROUTINE printheader2
      USE DYNUST_MAIN_MODULE
      USE DYNUST_VEH_PATH_ATT_MODULE
      USE DYNUST_VEH_MODULE

      INTEGER maxintervals
      INTEGER dynust_mode_flag
      dynust_mode_flag = 0

      s1 = 0
      s2 = 0
      s3 = 0
      s4 = 0
      s5 = 0
      DO is = 1, 5
         s1 = s1+readveh(1,is)
         s2 = s2+readveh(2,is)
         s3 = s3+readveh(3,is)                 
      ENDDO
      
      SUMMUC = sum(readveh(:,:))
          
      WRITE(666,*) '***************************************'
      WRITE(666,*)
      WRITE(666,*) 'VEHICLE TYPE PERCENTAGES'
      WRITE(666,*) '------------------------'
      IF(summuc > 0) THEN
        WRITE(666,' ( "    PC                                      : ",i10,f9.2," %")') sum(readveh(1,:)),sum(readveh(1,:))/summuc*100.0
        WRITE(666,' ( "    TRUCK                                   : ",i10,f9.2," %")') sum(readveh(2,:)),sum(readveh(2,:))/summuc*100.0
        WRITE(666,' ( "    HOV                                     : ",i10,f9.2," %")') sum(readveh(3,:)),sum(readveh(3,:))/summuc*100.0
        WRITE(666,' ( "    BUS                                     : ",i10,f9.2," %")') sum(readveh(6,:)),sum(readveh(6,:))/summuc*100.0
      ELSE
        WRITE(666,' ( "    PC                                      : ",i10,f9.2," %")') 0,0.0
        WRITE(666,' ( "    TRUCK                                   : ",i10,f9.2," %")') 0,0.0
        WRITE(666,' ( "    HOV                                     : ",i10,f9.2," %")') 0,0.0
        WRITE(666,' ( "    BUS                                     : ",i10,f9.2," %")') 0,0.0
      ENDIF

      WRITE(666,*) '***************************************'
      WRITE(666,*)
      WRITE(666,*) 'MTC CLASS PERCENTAGES'
      WRITE(666,*) '---------------------'
      IF(summuc > 0) THEN
        WRITE(666,'( "    Histoical/Habitual                      : ",i10,f9.2," %")') sum(readveh(:,1)),sum(readveh(:,1))/summuc*100.0
	    WRITE(666,'( "    System Optimal                          : ",i10,f9.2," %")') sum(readveh(:,2)),sum(readveh(:,2))/summuc*100.0
	    WRITE(666,'( "    User Optimal                            : ",i10,f9.2," %")') sum(readveh(:,3)),sum(readveh(:,3))/summuc*100.0
	    WRITE(666,'( "    Enroute Information                     : ",i10,f9.2," %")') sum(readveh(:,4)),sum(readveh(:,4))/summuc*100.0
	    WRITE(666,'( "    Pre-Trip Information                    : ",i10,f9.2," %")') sum(readveh(:,5)),sum(readveh(:,5))/summuc*100.0
      ELSE
        WRITE(666,'( "    Histoical/Habitual                      : ",i10,f9.2," %")') 0,0.0
	    WRITE(666,'( "    System Optimal                          : ",i10,f9.2," %")') 0,0.0
	    WRITE(666,'( "    User Optimal                            : ",i10,f9.2," %")') 0,0.0
	    WRITE(666,'( "    Enroute Information                     : ",i10,f9.2," %")') 0,0.0
	    WRITE(666,'( "    Pre-Trip Information                    : ",i10,f9.2," %")') 0,0.0
      ENDIF
END SUBROUTINE

