      SUBROUTINE WRITE_SUMMARY(VehID,EndID,ctime)
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~ Copyright (C) 2002-2011 The Arizona Board of Regents on Behalf of The University of Arizona.  ~~!
!~~ Developed by Yi-Chang Chiu, Ph.D. as par to the DynusT project.                               ~~!
!~~ This program is free software: you can redistribute it and/or modify it                       ~~!
!~~ under the terms of the GNU General Public License version 2 as published by                   ~~!
!~~ the Free Software Foundation.                                                                 ~~!
!~~ This program is distributed in the hope that it will be useful,                               ~~!
!~~ but WITHOUT ANY WARRANTY; without even the implied warranty of                                ~~!
!~~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                                          ~~!
!~~ See the GNU General Public License for more details.                                          ~~!
!~~ You should have received a copy of the GNU General Public License                             ~~!
!~~ along with this program (in the file COPYING).                                                ~~!
!~~ IF not, see <http://www.gnu.org/licenses/>.                                                   ~~!
!~~ Please see http://www.dynust.net for contact information and names                            ~~!
!~~ of individual contributors.                                                                   ~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
 
      USE DYNUST_MAIN_MODULE
      USE DYNUST_VEH_MODULE
      USE DYNUST_VEH_PATH_ATT_MODULE

      INTEGER VehID
      REAL ctime
	  LOGICAL EndID
	  INTEGER date_time (8)
	  CHARACTER (LEN = 12) REAL_CLOCK(3)

      REAL,save,dimension(nu_classes,2):: timeclass,disclass,STOPtimeclass,timeentry,totaltimeclass
      INTEGER,save,dimension(nu_classes,2):: numberclass
      INTEGER,save::AllNumberIMP
	  REAL,save::AlltimeIMP,AlldistIMP,AllSTOPIMP
      LOGICAL,save::AlloID = .false.
	  INTEGER,save::I_Tag_1_Stage,I_Tag_2_Stage,I_Tag_1_Roll,I_Tag_2_Roll
	  REAL,save::Trip_Time_Total,Trip_Time_Total_Itag1,Trip_Time_Total_Itag2,Trip_Time_Roll,Trip_Time_Roll_Itag1,Trip_Time_Roll_Itag2,Trip_Time_Total_W_Q,Trip_Time_Roll_W_Q
	  REAL,save::Trip_Distance_Total,Trip_Distance_Total_Itag1,Trip_Distance_Total_Itag2,Trip_Distance_Roll,Trip_Distance_Roll_Itag1,Trip_Distance_Roll_Itag2
      REAL,save::STOP_Time_Total,STOP_Time_Total_Itag1,STOP_Time_Total_Itag2,STOP_Time_Roll,STOP_Time_Roll_Itag1,STOP_Time_Roll_Itag2
      REAL,ALLOCATABLE::timeIMP(:,:),distIMP(:,:),STOPIMP(:,:),entryIMP(:,:),totalIMP(:,:)
      REAL,ALLOCATABLE::WZtimeIMP(:,:),WZdistIMP(:,:),WZSTOPIMP(:,:),WZentryIMP(:,:),WZtotalIMP(:,:)
	  INTEGER,ALLOCATABLE::numberIMP(:,:)
	  INTEGER,ALLOCATABLE::WZnumberIMP(:,:)
!> > > > > > > > > >                < < < < < < < < < < <
!>>>>>>>>>>>>>>>>>>>CODES START HERE<<<<<<<<<<<<<<<<<<<<<
!> > > > > > > > > >                < < < < < < < < < < <

      
	 IF(.not.AlloID) THEN
      IF(inci_num > 0) THEN
	    ALLOCATE(timeIMP(2,inci_num))
	    ALLOCATE(distIMP(2,inci_num))
	    ALLOCATE(STOPIMP(2,inci_num))
	    ALLOCATE(entryIMP(2,inci_num))
	    ALLOCATE(totalIMP(2,inci_num))
        ALLOCATE(numberIMP(2,inci_num))
	  timeIMP(:,:) = 0.0
	  distIMP(:,:) = 0.0
	  STOPIMP(:,:) = 0.0
	  entryIMP(:,:) = 0.0
	  totalIMP(:,:) = 0.0
      numberIMP(:,:) = 0.0

	  ENDIF
      IF(WorkZoneNum > 0) THEN
	    ALLOCATE(WZtimeIMP(2,WorkZoneNum))
	    ALLOCATE(WZdistIMP(2,WorkZoneNum))
	    ALLOCATE(WZSTOPIMP(2,WorkZoneNum))
	    ALLOCATE(WZentryIMP(2,WorkZoneNum))
	    ALLOCATE(WZtotalIMP(2,WorkZoneNum))
        ALLOCATE(WZnumberIMP(2,WorkZoneNum))
	  WZtimeIMP(:,:) = 0.0
	  WZdistIMP(:,:) = 0.0
	  WZSTOPIMP(:,:) = 0.0
	  WZentryIMP(:,:) = 0.0
	  WZtotalIMP(:,:) = 0.0
      WZnumberIMP(:,:) = 0.0

	  ENDIF
      
	  AlltimeIMP  = 0
	  AlldistIMP  = 0
	  AllSTOPIMP  = 0

      AlloID = .true.     
	 ENDIF

     IF(.not.IniID1) THEN
      timeclass(:,:) = 0.0
      numberclass(:,:) = 0
      disclass(:,:) = 0.0
      STOPtimeclass(:,:) = 0.0
      timeentry(:,:) =0.0
      totaltimeclass(:,:) = 0.0 
          
	  I_Tag_1_Stage=0
	  I_Tag_2_Stage=0
	  I_Tag_1_Roll=0
	  I_Tag_2_Roll=0

	  Trip_Time_Total=0.0
	  Trip_Time_Total_Itag1=0.0
	  Trip_Time_Total_Itag2=0.0
	  Trip_Time_Roll=0.0
	  Trip_Time_Roll_Itag1=0.0
	  Trip_Time_Roll_Itag2=0.0

	  Trip_Time_Total_W_Q=0.0
	  Trip_Time_Roll_W_Q=0.0

	  Trip_Distance_Total=0.0
	  Trip_Distance_Total_Itag1=0.0
	  Trip_Distance_Total_Itag2=0.0
	  Trip_Distance_Roll=0.0
	  Trip_Distance_Roll_Itag1=0.0
	  Trip_Distance_Roll_Itag2=0.0
      STOP_Time_Total=0.0
      STOP_Time_Total_Itag1=0.0
      STOP_Time_Total_Itag2=0.0
      STOP_Time_Roll=0.0
      STOP_Time_Roll_Itag1=0.0
      STOP_Time_Roll_Itag2=0.0
      IniID1=.true.

     ENDIF
      
	  IF(.not.EndID) THEN	
      i = VehID
	  IF(m_dynust_last_stand(i)%stime >= starttm.and.m_dynust_last_stand(i)%stime < endtm) THEN
       IF(m_dynust_veh(i)%itag == 2) THEN
        ihov = m_dynust_veh(i)%ioc
        iv=m_dynust_last_stand(i)%vehclass      
        totaltimeclass(iv,ihov)=totaltimeclass(iv,ihov)+(m_dynust_last_stand(i)%atime-m_dynust_last_stand(i)%stime)
        timeclass(iv,ihov)=timeclass(iv,ihov)+m_dynust_veh(i)%ttilnow
        numberclass(iv,ihov)=numberclass(iv,ihov)+1
        disclass(iv,ihov)=disclass(iv,ihov)+m_dynust_veh(i)%distans
        STOPtimeclass(iv,ihov)=STOPtimeclass(iv,ihov)+m_dynust_veh(i)%ttSTOP
        timeentry(iv,ihov)=timeentry(iv,ihov)+max(0.0,m_dynust_last_stand(i)%atime-m_dynust_last_stand(i)%stime-m_dynust_veh(i)%ttilnow)
       ENDIF


	  IF (m_dynust_veh(i)%itag == 1) THEN
	   I_Tag_1_Stage=I_Tag_1_Stage+1
	   Trip_Time_Total_Itag1=Trip_Time_Total_Itag1+m_dynust_veh(i)%Ttilnow
	   Trip_Distance_Total_Itag1=Trip_Distance_Total_Itag1+m_dynust_veh(i)%Distans
       STOP_Time_Total_Itag1=STOP_Time_Total_Itag1+m_dynust_veh(i)%ttSTOP
	  ENDIF


  	  IF (m_dynust_veh(i)%itag == 2) THEN
	   I_Tag_2_Stage=I_Tag_2_Stage+1
	   Trip_Time_Total_W_Q=Trip_Time_Total_W_Q+m_dynust_last_stand(i)%Atime-m_dynust_last_stand(i)%Stime
	   Trip_Time_Total_Itag2=Trip_Time_Total_Itag2+m_dynust_veh(i)%Ttilnow
	   Trip_Distance_Total_Itag2=Trip_Distance_Total_Itag2+m_dynust_veh(i)%Distans
       STOP_Time_Total_Itag2=STOP_Time_Total_Itag2+m_dynust_veh(i)%ttSTOP
	  ENDIF

      ENDIF
            
    ELSE !IF(EndID)

	  I_Tag_Roll=I_Tag_1_Roll+I_Tag_2_Roll
	  I_Tag_Total=I_Tag_1_Stage+I_Tag_2_Stage
	  Trip_Time_Total=Trip_Time_Total_Itag1+Trip_Time_Total_Itag2
	  Trip_Time_Roll=Trip_Time_Roll_Itag1+Trip_Time_Roll_Itag2
	  Trip_Distance_Total=Trip_Distance_Total_Itag1+Trip_Distance_Total_Itag2
	  Trip_Distance_Roll=Trip_Distance_Roll_Itag1+Trip_Distance_Roll_Itag2
      STOP_Time_Roll=STOP_Time_Roll_Itag1+STOP_Time_Roll_Itag2
      STOP_Time_Total=STOP_Time_Total_Itag1+STOP_Time_Total_Itag2


      WRITE(180,*) '**********************************************'
      WRITE(180,*) '**  Summaries for mtc Iteration Procedures  **'
      WRITE(180,*) '**********************************************'
      CALL DATE_AND_TIME(REAL_clock(1),REAL_clock(2),REAL_clock(3),DATE_TIME)
	  WRITE(180,'( "  PRINT DATE/TIME (yy/mm/dd/hh/mm/ss): ",6i4)') DATE_TIME(1),DATE_TIME(2),DATE_TIME(3),DATE_TIME(5),DATE_TIME(6),DATE_TIME(7)
	  WRITE(180,*) ' BASIC PARAMETERS'
      WRITE(180,*) ' Planning Horizon: ', horizon
      WRITE(180,*) ' Iterations Limt.: ', iteMax
      WRITE(180,*) ' Loading Factor  : ', multi
	  WRITE(180,*) ' Start Time of Collecting Stats:', starttm
	  WRITE(180,*) ' End   Time of Collecting Stats:', endtm
	  WRITE(180,*) ' Iteration:', iteration
	  WRITE(180,*) ' RoundPass:', RoundPass
      WRITE(180,*) '-----------------------------------------------'
      write (180,*) ' '
	  write (180,*) '  Vehicles Still in the Network   '
	  Write (180,'("     Number of Vehicles             = ",i15)') I_Tag_1_Stage
	  Write (180,'("     Total Travel Time w/o queueing = ",f15.3)')Trip_Time_Total_Itag1
	  Write (180,'("     Total Trip Distances           = ",f15.3)')Trip_Distance_Total_Itag1
      Write (180,'("     Total STOP Time                = ",f15.3)')STOP_Time_Total_Itag1

      IF (I_Tag_1_Stage > 0) THEN
        Write (180,'("     Average Travel Time            = ",f15.3)')Trip_Time_Total_Itag1/I_Tag_1_Stage
	    Write (180,'("     Average Trip Distance          = ",f15.3)')Trip_Distance_Total_Itag1/I_Tag_1_Stage
        Write (180,'("     Average STOP Time              = ",f15.3)')STOP_Time_Total_Itag1/I_Tag_1_Stage
        IF(Trip_Time_Total_Itag1 /= 0.0) THEN   
          Write (180,'("     Average travel Speed           = ",f15.3)')Trip_Distance_Total_Itag1/Trip_Time_Total_Itag1*60
        ENDIF 
      ELSE
        Write (180,'("     Average Travel Time            = ",f15.3)') 0.0
	    Write (180,'("     Average Trip Distance          = ",f15.3)') 0.0
        Write (180,'("     Average STOP Time              = ",f15.3)') 0.0
        Write (180,'("     Average travel Speed           = ",f15.3)') 0.0
	  ENDIF
	
      Write (180,*) '-----------------------------------------------'
	  write (180,*) '  Vehicles Outside the Network   '
      Write (180,'("     Number of Vehicles             = ",i15)') I_Tag_2_Stage
	  Write (180,'("     Total Travel Time w/o queueing = ",f15.3)') Trip_Time_Total_Itag2
	  Write (180,'("     Total Travel Time w queueing   = ",f15.3)') Trip_Time_Total_W_Q
	  Write (180,'("     Total Trip Distances           = ",f15.3)') Trip_Distance_Total_Itag2
      Write (180,'("     Total STOP Time                = ",f15.3)') STOP_Time_Total_Itag2
      IF (I_Tag_2_Stage > 0) THEN
	    write (180,'("     Average Travel Time            = ",f15.3)') Trip_Time_Total_Itag2/I_Tag_2_Stage
	    Write (180,'("     Average Trip Distance          = ",f15.3)') Trip_Distance_Total_Itag2/I_Tag_2_Stage
        Write (180,'("     Average STOP Time              = ",f15.3)') STOP_Time_Total_Itag2/I_Tag_2_Stage
        IF(Trip_Time_Total_Itag2 /= 0.0) THEN   
          Write (180,'("     Average travel Speed           = ",f15.3)') Trip_Distance_Total_Itag2/Trip_Time_Total_Itag2*60
        ENDIF
	  ENDIF


	  Write (180,*) '-----------------------------------------------'
	  Write (180,*)
	  Write (180,*)' For All Vehicles in the Network   '
	  Write (180,'("     Number of Vehicles             = ",i15)') I_Tag_Total
	  Write (180,'("     Total Travel Time w/o queueing = ",f15.3)') Trip_Time_Total
	  Write (180,'("     Total Trip Distances           = ",f15.3)') Trip_Distance_Total
      Write (180,'("     Total STOP Time                = ",f15.3)') STOP_Time_Total
      IF (I_Tag_Total > 0) THEN
	    Write (180,'("     Average Travel Time            = ",f15.3)') Trip_Time_Total/I_Tag_Total
!	    Write (1808,'("     Average Travel Time            = ",f15.3)') Trip_Time_Total/I_Tag_Total
	    Write (180,'("     Average Trip Distance          = ",f15.3)') Trip_Distance_Total/I_Tag_Total
        Write (180,'("     Average STOP Time              = ",f15.3)') STOP_Time_Total/I_Tag_Total
        IF(Trip_Time_Total /= 0.0) THEN    
          Write (180,'("     Average travel Speed           = ",f15.3)') Trip_Distance_Total/Trip_Time_Total*60
        ENDIF
	  ENDIF
	  Write (180,*) '-------------------------------------------------'
      write (180,*) 'The following mtc information is only for	    '
	  write (180,*) 'Those vehicles that have reached the destinations'
	  write (180,*) '-------------------------------------------------'

      DO 1201 ii=1,2
	    WRITE(180,*)
	    WRITE(180,*)
        IF(ii == 1) WRITE(180,*) ' LOV Vehicle -------------------------'
        IF(ii == 2) WRITE(180,*) ' HOV Vehicle -------------------------'
        DO 1200 i=1,nu_classes
          WRITE(180,*)' ----------------------------------------------'
          WRITE(180,'(" Class Number                 = ",i15)') i
          nc=numberclass(i,ii)
          IF(nc == 0) goto 1200
          WRITE(180,'(" number of vehicles           = ",i15)') nc
          WRITE(180,*)'------------------'
          WRITE(180,'("Total Overall Travel Time(min)= ",f15.3)') totaltimeclass(i,ii)
          WRITE(180,'("Total Trip Times(min)         = ",f15.3)') timeclass(i,ii)
          WRITE(180,'("Total Entry Queue Time(min)   = ",f15.3)') timeentry(i,ii)
          WRITE(180,'("Total Trip Distance(ml)       = ",f15.3)') disclass(i,ii)
          WRITE(180,'("Total STOP Time(min)          = ",f15.3)') STOPtimeclass(i,ii)
          WRITE(180,'("Average Overall Trip Time(min)= ",f15.3)') totaltimeclass(i,ii)/nc
          WRITE(180,'("Average Trip Times(min)       = ",f15.3)') timeclass(i,ii)/nc
          WRITE(180,'("Average Entry Q Time(min)     = ",f15.3)') timeentry(i,ii)/nc
          WRITE(180,'("Average STOP Time(min)        = ",f15.3)') STOPtimeclass(i,ii)/nc
          WRITE(180,'("Average Trip Distance(ml)     = ",f15.3)') disclass(i,ii)/nc
          WRITE(180,*)'----------------------------------'
1200  CONTINUE
1201  CONTINUE
1     FORMAT(f15.3)
2     FORMAT(i15)

      IF(inci_num > 0) THEN
	    IF(ALLOCATED(timeIMP)) DEALLOCATE(timeIMP)
	    IF(ALLOCATED(distIMP)) DEALLOCATE(distIMP)
	    IF(ALLOCATED(STOPIMP)) DEALLOCATE(STOPIMP)
	    IF(ALLOCATED(entryIMP)) DEALLOCATE(entryIMP)
	    IF(ALLOCATED(totalIMP)) DEALLOCATE(totalIMP)
        IF(ALLOCATED(numberIMP)) DEALLOCATE(numberIMP)
      ENDIF

      IF(WorkZoneNum > 0) THEN
	    IF(ALLOCATED(WZtimeIMP)) DEALLOCATE(WZtimeIMP)
	    IF(ALLOCATED(WZdistIMP)) DEALLOCATE(WZdistIMP)
	    IF(ALLOCATED(WZSTOPIMP)) DEALLOCATE(WZSTOPIMP)
	    IF(ALLOCATED(WZentryIMP)) DEALLOCATE(WZentryIMP)
	    IF(ALLOCATED(WZtotalIMP)) DEALLOCATE(WZtotalIMP)
        IF(ALLOCATED(WZnumberIMP)) DEALLOCATE(WZnumberIMP)
      ENDIF

    ENDIF !IF (EndID)


END SUBROUTINE
