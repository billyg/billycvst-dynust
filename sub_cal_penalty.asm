; mark_description "Intel(R) Visual Fortran Compiler XE for applications running on IA-32, Version 13.1.1.171 Build 20130313";
; mark_description "-S";
	.686P
 	.387
	OPTION DOTNAME
	ASSUME	CS:FLAT,DS:FLAT,SS:FLAT
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
TXTST0:
; -- Begin  _CAL_PENALTY
; mark_begin;
IF @Version GE 800
  .MMX
ELSEIF @Version GE 612
  .MMX
  MMWORD TEXTEQU <QWORD>
ENDIF
IF @Version GE 800
  .XMM
ELSEIF @Version GE 614
  .XMM
  XMMWORD TEXTEQU <OWORD>
ENDIF
       ALIGN     16
	PUBLIC _CAL_PENALTY
_CAL_PENALTY	PROC NEAR 
; parameter 1: 8 + ebx
.B1.1:                          ; Preds .B1.0
        push      ebx                                           ;1.12
        mov       ebx, esp                                      ;1.12
        and       esp, -16                                      ;1.12
        push      ebp                                           ;1.12
        push      ebp                                           ;1.12
        mov       ebp, DWORD PTR [4+ebx]                        ;1.12
        mov       DWORD PTR [4+esp], ebp                        ;1.12
        mov       ebp, esp                                      ;1.12
        sub       esp, 216                                      ;1.12
        xor       edx, edx                                      ;1.12
        mov       DWORD PTR [-132+ebp], esi                     ;1.12
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFARCS] ;1.12
        test      esi, esi                                      ;1.12
        mov       DWORD PTR [-128+ebp], edi                     ;1.12
        cmovg     edx, esi                                      ;1.12
        shl       edx, 2                                        ;1.12
        mov       eax, edx                                      ;1.12
        mov       DWORD PTR [-136+ebp], esp                     ;1.12
        call      __alloca_probe                                ;1.12
        and       esp, -16                                      ;1.12
        mov       eax, esp                                      ;1.12
                                ; LOE eax edx esi
.B1.156:                        ; Preds .B1.1
        mov       DWORD PTR [-80+ebp], eax                      ;1.12
        mov       eax, edx                                      ;1.12
        call      __alloca_probe                                ;1.12
        and       esp, -16                                      ;1.12
        mov       eax, esp                                      ;1.12
                                ; LOE eax esi
.B1.155:                        ; Preds .B1.156
        mov       DWORD PTR [-88+ebp], eax                      ;1.12
        test      esi, esi                                      ;35.5
        jle       .B1.8         ; Prob 50%                      ;35.5
                                ; LOE esi
.B1.2:                          ; Preds .B1.155
        cmp       esi, 24                                       ;35.5
        jle       .B1.59        ; Prob 0%                       ;35.5
                                ; LOE esi
.B1.3:                          ; Preds .B1.2
        lea       eax, DWORD PTR [esi*4]                        ;35.5
        push      eax                                           ;35.5
        push      0                                             ;35.5
        push      DWORD PTR [-88+ebp]                           ;35.5
        call      __intel_fast_memset                           ;35.5
                                ; LOE esi
.B1.157:                        ; Preds .B1.3
        add       esp, 12                                       ;35.5
                                ; LOE esi
.B1.5:                          ; Preds .B1.65 .B1.157 .B1.63
        cmp       esi, 24                                       ;36.5
        jle       .B1.50        ; Prob 0%                       ;36.5
                                ; LOE esi
.B1.6:                          ; Preds .B1.5
        lea       eax, DWORD PTR [esi*4]                        ;36.5
        push      eax                                           ;36.5
        push      0                                             ;36.5
        push      DWORD PTR [-80+ebp]                           ;36.5
        call      __intel_fast_memset                           ;36.5
                                ; LOE esi
.B1.158:                        ; Preds .B1.6
        add       esp, 12                                       ;36.5
                                ; LOE esi
.B1.8:                          ; Preds .B1.56 .B1.155 .B1.158 .B1.54
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIMPERAGG] ;43.37
        xor       edi, edi                                      ;43.5
        pxor      xmm2, xmm2                                    ;44.21
        divss     xmm2, DWORD PTR [_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT] ;44.21
        cvtsi2ss  xmm3, eax                                     ;43.37
        mov       edx, DWORD PTR [8+ebx]                        ;1.12
        divss     xmm2, xmm3                                    ;44.21
        mov       ecx, DWORD PTR [edx]                          ;43.5
        dec       ecx                                           ;43.26
        cvtsi2ss  xmm0, ecx                                     ;43.26
        divss     xmm0, xmm3                                    ;43.21
        movss     xmm1, DWORD PTR [_2il0floatpacket.6]          ;44.21
        andps     xmm1, xmm2                                    ;44.21
        pxor      xmm2, xmm1                                    ;44.21
        movss     xmm4, DWORD PTR [_2il0floatpacket.7]          ;44.21
        movaps    xmm5, xmm2                                    ;44.21
        cvttss2si edx, xmm0                                     ;43.21
        cmpltss   xmm5, xmm4                                    ;44.21
        andps     xmm4, xmm5                                    ;44.21
        movaps    xmm0, xmm2                                    ;44.21
        movss     xmm6, DWORD PTR [_2il0floatpacket.5]          ;44.21
        test      edx, edx                                      ;43.5
        addss     xmm0, xmm4                                    ;44.21
        cmovl     edx, edi                                      ;43.5
        test      esi, esi                                      ;47.6
        subss     xmm0, xmm4                                    ;44.21
        movaps    xmm4, xmm0                                    ;44.21
        lea       ecx, DWORD PTR [1+edx]                        ;43.5
        subss     xmm4, xmm2                                    ;44.21
        movaps    xmm2, xmm6                                    ;44.21
        addss     xmm2, xmm6                                    ;44.21
        movaps    xmm7, xmm4                                    ;44.21
        cmpless   xmm4, DWORD PTR [_2il0floatpacket.8]          ;44.21
        cmpnless  xmm7, xmm6                                    ;44.21
        andps     xmm7, xmm2                                    ;44.21
        andps     xmm4, xmm2                                    ;44.21
        subss     xmm0, xmm7                                    ;44.21
        addss     xmm0, xmm4                                    ;44.21
        orps      xmm0, xmm1                                    ;44.21
        jle       .B1.72        ; Prob 3%                       ;47.6
                                ; LOE eax edx ecx esi xmm0 xmm3
.B1.9:                          ; Preds .B1.8
        mov       DWORD PTR [-92+ebp], esi                      ;
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TDSPSTEP] ;59.11
        cmp       eax, esi                                      ;59.11
        mov       DWORD PTR [-100+ebp], eax                     ;
        pxor      xmm1, xmm1                                    ;
        cmovge    esi, eax                                      ;59.11
        xor       edi, edi                                      ;73.6
        test      edx, edx                                      ;73.6
        cvtss2si  eax, xmm0                                     ;44.21
        mov       DWORD PTR [-32+ebp], esi                      ;59.11
        mov       esi, 0                                        ;73.6
        cmovge    edi, edx                                      ;73.6
        test      eax, eax                                      ;73.6
        mov       DWORD PTR [-96+ebp], esi                      ;
        cmovl     eax, esi                                      ;73.6
        cmp       edi, eax                                      ;73.6
        mov       DWORD PTR [-36+ebp], 1                        ;47.6
        cmovl     eax, edi                                      ;73.6
        imul      esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32], -900 ;
        imul      edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32], -152 ;
        movss     xmm2, DWORD PTR [_2il0floatpacket.1]          ;52.71
        movss     xmm0, DWORD PTR [_2il0floatpacket.2]          ;53.19
        movss     DWORD PTR [-76+ebp], xmm3                     ;
        sub       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELTIME+44] ;
        imul      ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELTIME+40] ;
        add       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;
        add       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;
        mov       DWORD PTR [-52+ebp], esi                      ;
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELTIME+32] ;
        shl       esi, 2                                        ;
        mov       DWORD PTR [-56+ebp], edi                      ;
        lea       edi, DWORD PTR [4+edx*4]                      ;43.5
        mov       DWORD PTR [-84+ebp], edi                      ;43.5
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELTIME] ;
        sub       edi, esi                                      ;
        add       ecx, edi                                      ;
        mov       DWORD PTR [-164+ebp], eax                     ;73.6
        lea       eax, DWORD PTR [1+eax]                        ;73.6
        mov       DWORD PTR [-160+ebp], eax                     ;73.6
        mov       DWORD PTR [-140+ebp], ecx                     ;
        mov       DWORD PTR [-152+ebp], edx                     ;
        jmp       .B1.10        ; Prob 100%                     ;
                                ; LOE xmm1 xmm2
.B1.49:                         ; Preds .B1.48
        mov       DWORD PTR [-36+ebp], edx                      ;47.6
                                ; LOE xmm1 xmm2
.B1.10:                         ; Preds .B1.49 .B1.9
        cmp       DWORD PTR [-100+ebp], 0                       ;52.18
        jle       .B1.27        ; Prob 50%                      ;52.18
                                ; LOE xmm1 xmm2
.B1.11:                         ; Preds .B1.10
        imul      esi, DWORD PTR [-96+ebp], 900                 ;
        cmp       DWORD PTR [-100+ebp], 4                       ;52.18
        jl        .B1.68        ; Prob 10%                      ;52.18
                                ; LOE esi xmm1 xmm2
.B1.12:                         ; Preds .B1.11
        mov       eax, DWORD PTR [-52+ebp]                      ;52.18
        mov       edx, DWORD PTR [1196+eax+esi]                 ;52.18
        shl       edx, 2                                        ;52.18
        mov       eax, DWORD PTR [1164+eax+esi]                 ;52.18
        sub       eax, edx                                      ;52.18
        add       eax, 4                                        ;52.18
        and       eax, 15                                       ;52.18
        je        .B1.15        ; Prob 50%                      ;52.18
                                ; LOE eax esi xmm1 xmm2
.B1.13:                         ; Preds .B1.12
        test      al, 3                                         ;52.18
        jne       .B1.68        ; Prob 10%                      ;52.18
                                ; LOE eax esi xmm1 xmm2
.B1.14:                         ; Preds .B1.13
        neg       eax                                           ;52.18
        add       eax, 16                                       ;52.18
        shr       eax, 2                                        ;52.18
                                ; LOE eax esi xmm1 xmm2
.B1.15:                         ; Preds .B1.14 .B1.12
        lea       edx, DWORD PTR [4+eax]                        ;52.18
        cmp       edx, DWORD PTR [-100+ebp]                     ;52.18
        jg        .B1.68        ; Prob 10%                      ;52.18
                                ; LOE eax esi xmm1 xmm2
.B1.16:                         ; Preds .B1.15
        mov       edx, DWORD PTR [-100+ebp]                     ;52.18
        mov       ecx, edx                                      ;52.18
        sub       ecx, eax                                      ;52.18
        xor       edi, edi                                      ;
        and       ecx, 3                                        ;52.18
        neg       ecx                                           ;52.18
        add       ecx, edx                                      ;52.18
        mov       edx, DWORD PTR [-52+ebp]                      ;52.18
        mov       DWORD PTR [-60+ebp], edi                      ;
        mov       edi, DWORD PTR [1196+esi+edx]                 ;52.18
        shl       edi, 2                                        ;
        neg       edi                                           ;
        add       edi, DWORD PTR [1164+esi+edx]                 ;
        mov       DWORD PTR [-64+ebp], edi                      ;
        test      eax, eax                                      ;52.18
        jbe       .B1.20        ; Prob 10%                      ;52.18
                                ; LOE eax ecx esi edi xmm1 xmm2
.B1.17:                         ; Preds .B1.16
        xor       edx, edx                                      ;
        mov       DWORD PTR [-124+ebp], ecx                     ;
        mov       ecx, edi                                      ;
        mov       edi, DWORD PTR [-60+ebp]                      ;
                                ; LOE eax edx ecx esi edi xmm1 xmm2
.B1.18:                         ; Preds .B1.18 .B1.17
        add       edi, DWORD PTR [4+ecx+edx*4]                  ;52.18
        inc       edx                                           ;52.18
        cmp       edx, eax                                      ;52.18
        jb        .B1.18        ; Prob 82%                      ;52.18
                                ; LOE eax edx ecx esi edi xmm1 xmm2
.B1.19:                         ; Preds .B1.18
        mov       DWORD PTR [-60+ebp], edi                      ;
        mov       ecx, DWORD PTR [-124+ebp]                     ;
                                ; LOE eax ecx esi xmm1 xmm2
.B1.20:                         ; Preds .B1.16 .B1.19
        movd      xmm0, DWORD PTR [-60+ebp]                     ;52.18
        mov       edx, DWORD PTR [-64+ebp]                      ;52.18
                                ; LOE eax edx ecx esi xmm0 xmm1 xmm2
.B1.21:                         ; Preds .B1.21 .B1.20
        paddd     xmm0, XMMWORD PTR [4+edx+eax*4]               ;52.18
        add       eax, 4                                        ;52.18
        cmp       eax, ecx                                      ;52.18
        jb        .B1.21        ; Prob 82%                      ;52.18
                                ; LOE eax edx ecx esi xmm0 xmm1 xmm2
.B1.22:                         ; Preds .B1.21
        movdqa    xmm3, xmm0                                    ;52.18
        psrldq    xmm3, 8                                       ;52.18
        paddd     xmm0, xmm3                                    ;52.18
        movdqa    xmm4, xmm0                                    ;52.18
        psrldq    xmm4, 4                                       ;52.18
        paddd     xmm0, xmm4                                    ;52.18
        movd      eax, xmm0                                     ;52.18
                                ; LOE eax ecx esi xmm1 xmm2
.B1.23:                         ; Preds .B1.22 .B1.68
        cmp       ecx, DWORD PTR [-100+ebp]                     ;52.18
        jae       .B1.28        ; Prob 10%                      ;52.18
                                ; LOE eax ecx esi xmm1 xmm2
.B1.24:                         ; Preds .B1.23
        mov       edi, DWORD PTR [-52+ebp]                      ;52.18
        mov       edx, DWORD PTR [1196+esi+edi]                 ;52.18
        shl       edx, 2                                        ;
        neg       edx                                           ;
        add       edx, DWORD PTR [1164+esi+edi]                 ;
        mov       edi, DWORD PTR [-100+ebp]                     ;
                                ; LOE eax edx ecx esi edi xmm1 xmm2
.B1.25:                         ; Preds .B1.25 .B1.24
        add       eax, DWORD PTR [4+edx+ecx*4]                  ;52.18
        inc       ecx                                           ;52.18
        cmp       ecx, edi                                      ;52.18
        jb        .B1.25        ; Prob 82%                      ;52.18
        jmp       .B1.28        ; Prob 100%                     ;52.18
                                ; LOE eax edx ecx esi edi xmm1 xmm2
.B1.27:                         ; Preds .B1.10
        imul      esi, DWORD PTR [-96+ebp], 900                 ;
        xor       eax, eax                                      ;
                                ; LOE eax esi xmm1 xmm2
.B1.28:                         ; Preds .B1.25 .B1.23 .B1.27
        cvtsi2ss  xmm0, eax                                     ;52.18
        divss     xmm0, xmm2                                    ;52.11
        movss     xmm3, DWORD PTR [_2il0floatpacket.2]          ;53.19
        comiss    xmm3, xmm0                                    ;53.19
        jbe       .B1.30        ; Prob 50%                      ;53.19
                                ; LOE esi xmm0 xmm1 xmm2
.B1.29:                         ; Preds .B1.28
        push      32                                            ;54.13
        mov       DWORD PTR [-216+ebp], 0                       ;54.13
        lea       eax, DWORD PTR [-120+ebp]                     ;54.13
        push      eax                                           ;54.13
        push      OFFSET FLAT: __STRLITPACK_6.0.1               ;54.13
        push      -2088435968                                   ;54.13
        push      -1                                            ;54.13
        mov       DWORD PTR [-120+ebp], 13                      ;54.13
        lea       edx, DWORD PTR [-216+ebp]                     ;54.13
        push      edx                                           ;54.13
        mov       DWORD PTR [-116+ebp], OFFSET FLAT: __STRLITPACK_5 ;54.13
        movss     DWORD PTR [-148+ebp], xmm0                    ;54.13
        call      _for_write_seq_lis                            ;54.13
                                ; LOE esi
.B1.159:                        ; Preds .B1.29
        movss     xmm2, DWORD PTR [_2il0floatpacket.1]          ;
        add       esp, 24                                       ;54.13
        movss     xmm0, DWORD PTR [-148+ebp]                    ;
        pxor      xmm1, xmm1                                    ;
                                ; LOE esi xmm0 xmm1 xmm2
.B1.30:                         ; Preds .B1.159 .B1.28
        divss     xmm0, DWORD PTR [-76+ebp]                     ;56.11
        mov       edx, DWORD PTR [-96+ebp]                      ;56.11
        movaps    xmm3, xmm1                                    ;
        imul      edi, edx, 152                                 ;58.11
        mov       eax, DWORD PTR [-80+ebp]                      ;56.11
        mov       ecx, DWORD PTR [-56+ebp]                      ;58.17
        mov       DWORD PTR [-48+ebp], edi                      ;58.11
        movss     DWORD PTR [eax+edx*4], xmm0                   ;56.11
        mov       eax, DWORD PTR [164+ecx+edi]                  ;58.17
        imul      ecx, eax, 900                                 ;58.17
        mov       edx, DWORD PTR [-52+ebp]                      ;58.11
        mov       DWORD PTR [-72+ebp], eax                      ;58.17
        imul      edi, DWORD PTR [116+edx+ecx], -40             ;58.11
        mov       eax, DWORD PTR [84+edx+ecx]                   ;58.17
        cmp       DWORD PTR [-32+ebp], 0                        ;59.11
        movsx     edi, WORD PTR [76+edi+eax]                    ;58.17
        jle       .B1.42        ; Prob 3%                       ;59.11
                                ; LOE esi edi xmm0 xmm1 xmm2 xmm3
.B1.31:                         ; Preds .B1.30
        mov       edx, edi                                      ;
        xor       eax, eax                                      ;
        shr       edx, 31                                       ;
        add       edx, edi                                      ;
        sar       edx, 1                                        ;
        mov       DWORD PTR [-20+ebp], edx                      ;
        mov       DWORD PTR [-24+ebp], eax                      ;
        mov       DWORD PTR [-28+ebp], edi                      ;
        mov       DWORD PTR [-104+ebp], esi                     ;
                                ; LOE xmm0 xmm1 xmm2 xmm3
.B1.32:                         ; Preds .B1.40 .B1.31
        cmp       DWORD PTR [-28+ebp], 0                        ;60.18
        jle       .B1.152       ; Prob 16%                      ;60.18
                                ; LOE xmm0 xmm1 xmm2 xmm3
.B1.33:                         ; Preds .B1.32
        xor       ecx, ecx                                      ;
        cmp       DWORD PTR [-20+ebp], 0                        ;61.27
        jbe       .B1.151       ; Prob 11%                      ;61.27
                                ; LOE ecx xmm0 xmm1 xmm2 xmm3
.B1.34:                         ; Preds .B1.33
        mov       edx, DWORD PTR [-56+ebp]                      ;61.27
        xor       edi, edi                                      ;
        mov       esi, DWORD PTR [-48+ebp]                      ;61.27
        mov       DWORD PTR [-44+ebp], edi                      ;
        mov       DWORD PTR [-8+ebp], edi                       ;
        imul      edi, DWORD PTR [164+esi+edx], 900             ;61.27
        mov       esi, DWORD PTR [-52+ebp]                      ;
        mov       eax, DWORD PTR [-24+ebp]                      ;
        sub       eax, DWORD PTR [116+edi+esi]                  ;
        mov       esi, DWORD PTR [84+edi+esi]                   ;61.27
        lea       edx, DWORD PTR [eax+eax*4]                    ;
        mov       edi, DWORD PTR [40+esi+edx*8]                 ;61.27
        mov       eax, DWORD PTR [72+esi+edx*8]                 ;61.27
        mov       esi, DWORD PTR [68+esi+edx*8]                 ;61.27
        imul      eax, esi                                      ;
        sub       edi, eax                                      ;
        mov       eax, DWORD PTR [-8+ebp]                       ;
        mov       edx, DWORD PTR [-44+ebp]                      ;
        mov       DWORD PTR [-12+ebp], edi                      ;
        mov       DWORD PTR [-16+ebp], esi                      ;
                                ; LOE eax edx ecx xmm0 xmm1 xmm2 xmm3
.B1.35:                         ; Preds .B1.35 .B1.34
        mov       DWORD PTR [-8+ebp], eax                       ;
        lea       edi, DWORD PTR [1+edx+edx]                    ;61.27
        mov       eax, DWORD PTR [-16+ebp]                      ;61.27
        imul      edi, eax                                      ;61.27
        mov       esi, DWORD PTR [-12+ebp]                      ;61.27
        add       ecx, DWORD PTR [esi+edi]                      ;61.27
        lea       edi, DWORD PTR [2+edx+edx]                    ;61.27
        imul      edi, eax                                      ;61.27
        inc       edx                                           ;61.27
        mov       eax, DWORD PTR [-8+ebp]                       ;61.27
        add       eax, DWORD PTR [esi+edi]                      ;61.27
        cmp       edx, DWORD PTR [-20+ebp]                      ;61.27
        jb        .B1.35        ; Prob 64%                      ;61.27
                                ; LOE eax edx ecx xmm0 xmm1 xmm2 xmm3
.B1.36:                         ; Preds .B1.35
        add       ecx, eax                                      ;61.27
        lea       eax, DWORD PTR [1+edx+edx]                    ;61.27
                                ; LOE eax ecx xmm0 xmm1 xmm2 xmm3
.B1.37:                         ; Preds .B1.36 .B1.151
        cmp       eax, DWORD PTR [-28+ebp]                      ;61.27
        ja        .B1.39        ; Prob 50%                      ;61.27
                                ; LOE eax ecx xmm0 xmm1 xmm2 xmm3
.B1.38:                         ; Preds .B1.37
        mov       edx, DWORD PTR [-24+ebp]                      ;61.27
        mov       edi, DWORD PTR [-56+ebp]                      ;61.27
        mov       esi, DWORD PTR [-48+ebp]                      ;61.27
        mov       DWORD PTR [-40+ebp], ecx                      ;
        lea       ecx, DWORD PTR [edx+edx*4]                    ;61.27
        imul      edx, DWORD PTR [164+esi+edi], 900             ;61.27
        mov       esi, DWORD PTR [-52+ebp]                      ;61.27
        imul      edi, DWORD PTR [116+edx+esi], -40             ;61.27
        add       edi, DWORD PTR [84+edx+esi]                   ;61.27
        sub       eax, DWORD PTR [72+edi+ecx*8]                 ;61.27
        imul      eax, DWORD PTR [68+edi+ecx*8]                 ;61.27
        mov       edx, DWORD PTR [40+edi+ecx*8]                 ;61.27
        mov       ecx, DWORD PTR [-40+ebp]                      ;61.27
        add       ecx, DWORD PTR [edx+eax]                      ;61.27
                                ; LOE ecx xmm0 xmm1 xmm2 xmm3
.B1.39:                         ; Preds .B1.38 .B1.37
        cvtsi2ss  xmm4, ecx                                     ;61.27
                                ; LOE xmm0 xmm1 xmm2 xmm3 xmm4
.B1.40:                         ; Preds .B1.39 .B1.152
        mov       eax, DWORD PTR [-24+ebp]                      ;59.11
        addss     xmm3, xmm4                                    ;61.13
        inc       eax                                           ;59.11
        mov       DWORD PTR [-24+ebp], eax                      ;59.11
        cmp       eax, DWORD PTR [-32+ebp]                      ;59.11
        jb        .B1.32        ; Prob 82%                      ;59.11
                                ; LOE xmm0 xmm1 xmm2 xmm3
.B1.41:                         ; Preds .B1.40
        mov       edi, DWORD PTR [-28+ebp]                      ;
        mov       esi, DWORD PTR [-104+ebp]                     ;
                                ; LOE esi edi xmm0 xmm1 xmm2 xmm3
.B1.42:                         ; Preds .B1.30 .B1.41
        comiss    xmm1, xmm3                                    ;65.19
        jbe       .B1.44        ; Prob 50%                      ;65.19
                                ; LOE esi edi xmm0 xmm1 xmm2 xmm3
.B1.43:                         ; Preds .B1.42
        push      32                                            ;65.24
        mov       DWORD PTR [-216+ebp], 0                       ;65.24
        lea       eax, DWORD PTR [-112+ebp]                     ;65.24
        push      eax                                           ;65.24
        push      OFFSET FLAT: __STRLITPACK_7.0.1               ;65.24
        push      -2088435968                                   ;65.24
        push      -1                                            ;65.24
        mov       DWORD PTR [-112+ebp], 13                      ;65.24
        lea       edx, DWORD PTR [-216+ebp]                     ;65.24
        push      edx                                           ;65.24
        mov       DWORD PTR [-108+ebp], OFFSET FLAT: __STRLITPACK_4 ;65.24
        movss     DWORD PTR [-144+ebp], xmm3                    ;65.24
        movss     DWORD PTR [-148+ebp], xmm0                    ;65.24
        call      _for_write_seq_lis                            ;65.24
                                ; LOE esi edi
.B1.160:                        ; Preds .B1.43
        movss     xmm2, DWORD PTR [_2il0floatpacket.1]          ;
        add       esp, 24                                       ;65.24
        movss     xmm0, DWORD PTR [-148+ebp]                    ;
        pxor      xmm1, xmm1                                    ;
        movss     xmm3, DWORD PTR [-144+ebp]                    ;
                                ; LOE esi edi xmm0 xmm1 xmm2 xmm3
.B1.44:                         ; Preds .B1.160 .B1.42
        imul      edi, DWORD PTR [-32+ebp]                      ;66.69
        cvtsi2ss  xmm4, edi                                     ;66.69
        divss     xmm3, xmm4                                    ;66.11
        mov       edx, DWORD PTR [-88+ebp]                      ;66.11
        mov       eax, DWORD PTR [-72+ebp]                      ;66.11
        mov       ecx, DWORD PTR [-52+ebp]                      ;70.14
        mov       edi, DWORD PTR [-84+ebp]                      ;70.61
        movss     DWORD PTR [-4+edx+eax*4], xmm3                ;66.11
        mov       edx, DWORD PTR [1200+ecx+esi]                 ;70.14
        mov       eax, DWORD PTR [1232+ecx+esi]                 ;70.14
        shl       eax, 2                                        ;70.61
        lea       ecx, DWORD PTR [edi+edx]                      ;70.61
        sub       ecx, eax                                      ;70.61
        mov       DWORD PTR [-68+ebp], ecx                      ;70.61
        mov       ecx, DWORD PTR [ecx]                          ;70.14
        test      ecx, ecx                                      ;70.61
        jle       .B1.133       ; Prob 16%                      ;70.61
                                ; LOE eax edx ecx esi xmm0 xmm1 xmm2
.B1.45:                         ; Preds .B1.44
        mov       edx, DWORD PTR [-52+ebp]                      ;71.33
        cvtsi2ss  xmm0, ecx                                     ;71.89
        mov       eax, DWORD PTR [1092+edx+esi]                 ;71.33
        mov       esi, DWORD PTR [1124+edx+esi]                 ;71.33
        shl       esi, 2                                        ;71.7
        neg       esi                                           ;71.7
        add       eax, DWORD PTR [-84+ebp]                      ;71.7
        mov       ecx, DWORD PTR [-140+ebp]                     ;71.7
        mov       edi, DWORD PTR [-96+ebp]                      ;71.7
        cvtsi2ss  xmm3, DWORD PTR [esi+eax]                     ;71.33
        divss     xmm3, xmm2                                    ;71.78
        divss     xmm3, xmm0                                    ;71.88
        mulss     xmm3, xmm2                                    ;71.136
        cvttss2si edx, xmm3                                     ;71.7
        mov       DWORD PTR [4+ecx+edi*4], edx                  ;71.7
                                ; LOE edx xmm1 xmm2
.B1.46:                         ; Preds .B1.45 .B1.143 .B1.147 .B1.139
        test      edx, edx                                      ;107.35
        jge       .B1.48        ; Prob 84%                      ;107.35
                                ; LOE xmm1 xmm2
.B1.47:                         ; Preds .B1.46
        push      32                                            ;108.12
        mov       DWORD PTR [-216+ebp], 0                       ;108.12
        lea       eax, DWORD PTR [-184+ebp]                     ;108.12
        push      eax                                           ;108.12
        push      OFFSET FLAT: __STRLITPACK_8.0.1               ;108.12
        push      -2088435968                                   ;108.12
        push      911                                           ;108.12
        mov       DWORD PTR [-184+ebp], 35                      ;108.12
        lea       edx, DWORD PTR [-216+ebp]                     ;108.12
        push      edx                                           ;108.12
        mov       DWORD PTR [-180+ebp], OFFSET FLAT: __STRLITPACK_2 ;108.12
        call      _for_write_seq_lis                            ;108.12
                                ; LOE
.B1.161:                        ; Preds .B1.47
        movss     xmm2, DWORD PTR [_2il0floatpacket.1]          ;
        add       esp, 24                                       ;108.12
        pxor      xmm1, xmm1                                    ;
                                ; LOE xmm1 xmm2
.B1.48:                         ; Preds .B1.161 .B1.46
        mov       eax, DWORD PTR [-96+ebp]                      ;111.8
        lea       edx, DWORD PTR [2+eax]                        ;111.8
        inc       eax                                           ;47.6
        mov       DWORD PTR [-96+ebp], eax                      ;47.6
        cmp       eax, DWORD PTR [-92+ebp]                      ;47.6
        jb        .B1.49        ; Prob 82%                      ;47.6
        jmp       .B1.71        ; Prob 100%                     ;47.6
                                ; LOE edx xmm1 xmm2
.B1.50:                         ; Preds .B1.5                   ; Infreq
        cmp       esi, 8                                        ;36.5
        jl        .B1.58        ; Prob 10%                      ;36.5
                                ; LOE esi
.B1.51:                         ; Preds .B1.50                  ; Infreq
        mov       eax, esi                                      ;36.5
        xor       edx, edx                                      ;36.5
        mov       ecx, DWORD PTR [-80+ebp]                      ;36.5
        and       eax, -8                                       ;36.5
        pxor      xmm0, xmm0                                    ;36.5
                                ; LOE eax edx ecx esi xmm0
.B1.52:                         ; Preds .B1.52 .B1.51           ; Infreq
        movups    XMMWORD PTR [ecx+edx*4], xmm0                 ;36.5
        movups    XMMWORD PTR [16+ecx+edx*4], xmm0              ;36.5
        add       edx, 8                                        ;36.5
        cmp       edx, eax                                      ;36.5
        jb        .B1.52        ; Prob 82%                      ;36.5
                                ; LOE eax edx ecx esi xmm0
.B1.54:                         ; Preds .B1.52 .B1.58           ; Infreq
        cmp       eax, esi                                      ;36.5
        jae       .B1.8         ; Prob 10%                      ;36.5
                                ; LOE eax esi
.B1.55:                         ; Preds .B1.54                  ; Infreq
        mov       edx, DWORD PTR [-80+ebp]                      ;
        pxor      xmm0, xmm0                                    ;
                                ; LOE eax edx esi xmm0
.B1.56:                         ; Preds .B1.56 .B1.55           ; Infreq
        movss     DWORD PTR [edx+eax*4], xmm0                   ;36.5
        inc       eax                                           ;36.5
        cmp       eax, esi                                      ;36.5
        jb        .B1.56        ; Prob 82%                      ;36.5
        jmp       .B1.8         ; Prob 100%                     ;36.5
                                ; LOE eax edx esi xmm0
.B1.58:                         ; Preds .B1.50                  ; Infreq
        xor       eax, eax                                      ;36.5
        jmp       .B1.54        ; Prob 100%                     ;36.5
                                ; LOE eax esi
.B1.59:                         ; Preds .B1.2                   ; Infreq
        cmp       esi, 8                                        ;35.5
        jl        .B1.67        ; Prob 10%                      ;35.5
                                ; LOE esi
.B1.60:                         ; Preds .B1.59                  ; Infreq
        mov       eax, esi                                      ;35.5
        xor       edx, edx                                      ;35.5
        mov       ecx, DWORD PTR [-88+ebp]                      ;35.5
        and       eax, -8                                       ;35.5
        pxor      xmm0, xmm0                                    ;35.5
                                ; LOE eax edx ecx esi xmm0
.B1.61:                         ; Preds .B1.61 .B1.60           ; Infreq
        movups    XMMWORD PTR [ecx+edx*4], xmm0                 ;35.5
        movups    XMMWORD PTR [16+ecx+edx*4], xmm0              ;35.5
        add       edx, 8                                        ;35.5
        cmp       edx, eax                                      ;35.5
        jb        .B1.61        ; Prob 82%                      ;35.5
                                ; LOE eax edx ecx esi xmm0
.B1.63:                         ; Preds .B1.61 .B1.67           ; Infreq
        cmp       eax, esi                                      ;35.5
        jae       .B1.5         ; Prob 10%                      ;35.5
                                ; LOE eax esi
.B1.64:                         ; Preds .B1.63                  ; Infreq
        mov       edx, DWORD PTR [-88+ebp]                      ;
        pxor      xmm0, xmm0                                    ;
                                ; LOE eax edx esi xmm0
.B1.65:                         ; Preds .B1.64 .B1.65           ; Infreq
        movss     DWORD PTR [edx+eax*4], xmm0                   ;35.5
        inc       eax                                           ;35.5
        cmp       eax, esi                                      ;35.5
        jb        .B1.65        ; Prob 82%                      ;35.5
        jmp       .B1.5         ; Prob 100%                     ;35.5
                                ; LOE eax edx esi xmm0
.B1.67:                         ; Preds .B1.59                  ; Infreq
        xor       eax, eax                                      ;35.5
        jmp       .B1.63        ; Prob 100%                     ;35.5
                                ; LOE eax esi
.B1.68:                         ; Preds .B1.11 .B1.15 .B1.13    ; Infreq
        xor       eax, eax                                      ;
        xor       ecx, ecx                                      ;
        jmp       .B1.23        ; Prob 100%                     ;
                                ; LOE eax ecx esi xmm1 xmm2
.B1.71:                         ; Preds .B1.48                  ; Infreq
        mov       edx, DWORD PTR [-152+ebp]                     ;
        mov       eax, DWORD PTR [-100+ebp]                     ;
        mov       esi, DWORD PTR [-92+ebp]                      ;
                                ; LOE eax edx esi
.B1.72:                         ; Preds .B1.8 .B1.71            ; Infreq
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_PENFORPREVENTMOVE] ;114.49
        mov       DWORD PTR [-160+ebp], ecx                     ;114.49
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+44] ;114.7
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+36] ;114.7
        test      ecx, ecx                                      ;114.7
        mov       DWORD PTR [-148+ebp], edi                     ;114.7
        mov       DWORD PTR [-156+ebp], ecx                     ;114.7
        jle       .B1.74        ; Prob 10%                      ;114.7
                                ; LOE eax edx esi
.B1.73:                         ; Preds .B1.72                  ; Infreq
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+32] ;114.7
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+24] ;114.7
        test      edi, edi                                      ;114.7
        mov       DWORD PTR [-164+ebp], ecx                     ;114.7
        mov       DWORD PTR [-124+ebp], edi                     ;114.7
        jg        .B1.107       ; Prob 50%                      ;114.7
                                ; LOE eax edx esi edi
.B1.74:                         ; Preds .B1.114 .B1.130 .B1.72 .B1.73 ; Infreq
        mov       DWORD PTR [-124+ebp], 1                       ;116.7
        test      esi, esi                                      ;116.7
        jle       .B1.88        ; Prob 2%                       ;116.7
                                ; LOE eax edx esi
.B1.75:                         ; Preds .B1.74                  ; Infreq
        mov       DWORD PTR [-92+ebp], esi                      ;43.5
        lea       edx, DWORD PTR [5+edx+edx*4]                  ;43.5
        mov       DWORD PTR [-140+ebp], edx                     ;43.5
        mov       eax, 1                                        ;
        mov       edi, DWORD PTR [-124+ebp]                     ;43.5
                                ; LOE eax edi
.B1.76:                         ; Preds .B1.86 .B1.75           ; Infreq
        mov       ecx, eax                                      ;120.9
        sub       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;120.9
        imul      edx, ecx, 152                                 ;120.9
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;120.9
        mov       ecx, DWORD PTR [12+esi+edx]                   ;120.9
        movsx     edx, BYTE PTR [108+esi+edx]                   ;123.21
        test      edx, edx                                      ;123.10
        jle       .B1.86        ; Prob 2%                       ;123.10
                                ; LOE eax edx ecx esi edi
.B1.77:                         ; Preds .B1.76                  ; Infreq
        mov       eax, ecx                                      ;120.9
        mov       DWORD PTR [-36+ebp], edi                      ;47.6
        lea       edi, DWORD PTR [ecx*4]                        ;120.9
        shl       eax, 5                                        ;120.9
        mov       DWORD PTR [-68+ebp], 1                        ;
        sub       eax, edi                                      ;120.9
        shl       ecx, 3                                        ;120.9
        mov       DWORD PTR [-84+ebp], ecx                      ;120.9
        mov       DWORD PTR [-80+ebp], eax                      ;120.9
        mov       DWORD PTR [-76+ebp], edx                      ;120.9
                                ; LOE esi
.B1.78:                         ; Preds .B1.167 .B1.77          ; Infreq
        imul      edi, DWORD PTR [-36+ebp], 152                 ;124.11
        imul      ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32], 152 ;124.11
        add       edi, esi                                      ;124.11
        mov       DWORD PTR [-64+ebp], edi                      ;124.11
        sub       edi, ecx                                      ;124.11
        mov       eax, DWORD PTR [-68+ebp]                      ;124.11
        mov       edx, DWORD PTR [144+edi]                      ;124.11
        neg       edx                                           ;124.11
        add       edx, eax                                      ;124.11
        imul      edx, DWORD PTR [140+edi]                      ;124.11
        mov       edi, DWORD PTR [112+edi]                      ;124.11
        imul      edx, DWORD PTR [edi+edx], 152                 ;124.16
        add       esi, edx                                      ;124.11
        sub       esi, ecx                                      ;124.11
        neg       ecx                                           ;125.17
        mov       edx, DWORD PTR [-64+ebp]                      ;125.17
        mov       edi, DWORD PTR [144+ecx+edx]                  ;125.17
        neg       edi                                           ;125.17
        add       edi, eax                                      ;125.17
        lea       eax, DWORD PTR [-36+ebp]                      ;125.17
        imul      edi, DWORD PTR [140+ecx+edx]                  ;125.17
        add       edi, DWORD PTR [112+ecx+edx]                  ;125.17
        push      eax                                           ;125.17
        mov       esi, DWORD PTR [12+esi]                       ;124.11
        push      edi                                           ;125.17
        mov       DWORD PTR [-72+ebp], esi                      ;124.11
        call      _DYNUST_NETWORK_MODULE_mp_GETBSTMOVE          ;125.17
                                ; LOE eax
.B1.162:                        ; Preds .B1.78                  ; Infreq
        add       esp, 8                                        ;125.17
        mov       edi, eax                                      ;125.17
                                ; LOE edi
.B1.79:                         ; Preds .B1.162                 ; Infreq
        imul      esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+32], -28 ;132.70
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+44] ;132.14
        neg       edx                                           ;132.70
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE] ;132.11
        add       edx, edi                                      ;132.70
        add       eax, DWORD PTR [-80+ebp]                      ;132.70
        imul      edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+40] ;132.70
        add       eax, esi                                      ;132.70
        movsx     esi, BYTE PTR [11+eax+edx]                    ;132.75
        cmp       BYTE PTR [8+eax+edx], 0                       ;132.67
        jle       .B1.121       ; Prob 16%                      ;132.67
                                ; LOE esi edi
.B1.80:                         ; Preds .B1.79                  ; Infreq
        sub       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+44] ;
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+32] ;144.80
        imul      edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+40] ;
        shl       eax, 3                                        ;
        neg       eax                                           ;
        add       eax, DWORD PTR [-84+ebp]                      ;
        add       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE] ;
        add       edi, eax                                      ;
        movss     xmm1, DWORD PTR [edi]                         ;152.20
                                ; LOE esi edi xmm1
.B1.81:                         ; Preds .B1.80 .B1.125          ; Infreq
        pxor      xmm0, xmm0                                    ;152.64
        comiss    xmm0, xmm1                                    ;152.64
        ja        .B1.119       ; Prob 5%                       ;152.64
                                ; LOE esi edi
.B1.82:                         ; Preds .B1.165 .B1.81          ; Infreq
        test      esi, esi                                      ;159.70
        jle       .B1.84        ; Prob 16%                      ;159.70
                                ; LOE edi
.B1.83:                         ; Preds .B1.122 .B1.82          ; Infreq
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_PENFORPREVENTMOVE] ;160.20
        mov       DWORD PTR [edi], eax                          ;160.20
                                ; LOE
.B1.84:                         ; Preds .B1.127 .B1.128 .B1.82 .B1.83 ; Infreq
        mov       eax, DWORD PTR [-68+ebp]                      ;162.9
        inc       eax                                           ;162.9
        mov       DWORD PTR [-68+ebp], eax                      ;162.9
        cmp       eax, DWORD PTR [-76+ebp]                      ;162.9
        jg        .B1.85        ; Prob 18%                      ;162.9
                                ; LOE
.B1.167:                        ; Preds .B1.84                  ; Infreq
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;120.9
        jmp       .B1.78        ; Prob 100%                     ;120.9
                                ; LOE esi
.B1.85:                         ; Preds .B1.84                  ; Infreq
        mov       eax, DWORD PTR [-36+ebp]                      ;163.7
                                ; LOE eax
.B1.86:                         ; Preds .B1.85 .B1.76           ; Infreq
        inc       eax                                           ;163.7
        mov       edi, eax                                      ;163.7
        cmp       eax, DWORD PTR [-92+ebp]                      ;163.7
        jle       .B1.76        ; Prob 82%                      ;163.7
                                ; LOE eax edi
.B1.87:                         ; Preds .B1.86                  ; Infreq
        mov       DWORD PTR [-124+ebp], edi                     ;
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIMPERAGG] ;166.13
                                ; LOE eax
.B1.88:                         ; Preds .B1.87 .B1.74           ; Infreq
        mov       esi, DWORD PTR [8+ebx]                        ;166.5
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TDSPSTEP] ;166.13
        cmp       eax, ecx                                      ;166.41
        cmovge    ecx, eax                                      ;166.41
        mov       eax, DWORD PTR [esi]                          ;166.5
        dec       eax                                           ;166.13
        cdq                                                     ;166.8
        idiv      ecx                                           ;166.8
        test      edx, edx                                      ;166.41
        jne       .B1.105       ; Prob 50%                      ;166.41
                                ; LOE ecx
.B1.89:                         ; Preds .B1.88                  ; Infreq
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFARCS] ;167.6
        test      eax, eax                                      ;167.6
        jle       .B1.118       ; Prob 2%                       ;167.6
                                ; LOE eax ecx
.B1.90:                         ; Preds .B1.89                  ; Infreq
        imul      edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32], -900 ;
        xor       edx, edx                                      ;168.7
        add       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;
        xor       esi, esi                                      ;
        test      ecx, ecx                                      ;168.7
        jle       .B1.116       ; Prob 3%                       ;168.7
                                ; LOE eax edx ecx esi edi
.B1.91:                         ; Preds .B1.90                  ; Infreq
        mov       DWORD PTR [-172+ebp], edi                     ;
        mov       DWORD PTR [-176+ebp], eax                     ;
        mov       DWORD PTR [-140+ebp], ecx                     ;
                                ; LOE edx esi
.B1.92:                         ; Preds .B1.102 .B1.91          ; Infreq
        mov       ecx, DWORD PTR [-172+ebp]                     ;
        xor       eax, eax                                      ;
        mov       DWORD PTR [-156+ebp], esi                     ;
        mov       DWORD PTR [-160+ebp], edx                     ;
        imul      edi, DWORD PTR [1016+esi+ecx], -40            ;
        add       edi, DWORD PTR [984+esi+ecx]                  ;
        mov       DWORD PTR [-124+ebp], edi                     ;
                                ; LOE eax
.B1.93:                         ; Preds .B1.101 .B1.92          ; Infreq
        mov       ecx, DWORD PTR [-124+ebp]                     ;169.15
        lea       edx, DWORD PTR [eax+eax*4]                    ;170.16
        movsx     esi, WORD PTR [76+ecx+edx*8]                  ;169.15
        test      esi, esi                                      ;170.16
        jle       .B1.101       ; Prob 16%                      ;170.16
                                ; LOE eax edx ecx cl ch
.B1.94:                         ; Preds .B1.93                  ; Infreq
        mov       esi, ecx                                      ;170.21
        mov       ecx, DWORD PTR [72+esi+edx*8]                 ;170.21
        mov       esi, DWORD PTR [64+esi+edx*8]                 ;170.21
        test      esi, esi                                      ;170.21
        mov       DWORD PTR [-104+ebp], ecx                     ;170.21
        jle       .B1.101       ; Prob 50%                      ;170.21
                                ; LOE eax edx esi
.B1.95:                         ; Preds .B1.94                  ; Infreq
        mov       ecx, esi                                      ;170.21
        shr       ecx, 31                                       ;170.21
        add       ecx, esi                                      ;170.21
        sar       ecx, 1                                        ;170.21
        mov       DWORD PTR [-96+ebp], ecx                      ;170.21
        test      ecx, ecx                                      ;170.21
        jbe       .B1.132       ; Prob 11%                      ;170.21
                                ; LOE eax edx esi
.B1.96:                         ; Preds .B1.95                  ; Infreq
        mov       DWORD PTR [-144+ebp], esi                     ;
        xor       esi, esi                                      ;
        mov       DWORD PTR [-92+ebp], esi                      ;
        mov       esi, DWORD PTR [-124+ebp]                     ;170.21
        mov       ecx, DWORD PTR [-104+ebp]                     ;
        mov       DWORD PTR [-152+ebp], eax                     ;
        mov       edi, DWORD PTR [68+esi+edx*8]                 ;170.21
        imul      ecx, edi                                      ;
        mov       DWORD PTR [-100+ebp], edi                     ;170.21
        mov       edi, DWORD PTR [40+esi+edx*8]                 ;170.21
        sub       edi, ecx                                      ;
        mov       ecx, DWORD PTR [-92+ebp]                      ;
        mov       eax, edi                                      ;
        mov       DWORD PTR [-148+ebp], edx                     ;
                                ; LOE eax ecx
.B1.97:                         ; Preds .B1.97 .B1.96           ; Infreq
        mov       edx, DWORD PTR [-104+ebp]                     ;170.21
        xor       edi, edi                                      ;170.21
        mov       esi, DWORD PTR [-100+ebp]                     ;170.21
        mov       DWORD PTR [-92+ebp], ecx                      ;
        lea       edx, DWORD PTR [edx+ecx*2]                    ;170.21
        mov       ecx, esi                                      ;170.21
        imul      ecx, edx                                      ;170.21
        inc       edx                                           ;170.21
        imul      edx, esi                                      ;170.21
        mov       DWORD PTR [eax+ecx], edi                      ;170.21
        mov       ecx, DWORD PTR [-92+ebp]                      ;170.21
        inc       ecx                                           ;170.21
        mov       DWORD PTR [eax+edx], edi                      ;170.21
        cmp       ecx, DWORD PTR [-96+ebp]                      ;170.21
        jb        .B1.97        ; Prob 64%                      ;170.21
                                ; LOE eax ecx
.B1.98:                         ; Preds .B1.97                  ; Infreq
        mov       esi, DWORD PTR [-144+ebp]                     ;
        lea       ecx, DWORD PTR [1+ecx+ecx]                    ;170.21
        mov       edx, DWORD PTR [-148+ebp]                     ;
        mov       eax, DWORD PTR [-152+ebp]                     ;
                                ; LOE eax edx ecx esi
.B1.99:                         ; Preds .B1.98 .B1.132          ; Infreq
        lea       edi, DWORD PTR [-1+ecx]                       ;170.21
        cmp       esi, edi                                      ;170.21
        jbe       .B1.101       ; Prob 11%                      ;170.21
                                ; LOE eax edx ecx
.B1.100:                        ; Preds .B1.99                  ; Infreq
        mov       esi, DWORD PTR [-104+ebp]                     ;170.21
        mov       edi, DWORD PTR [-124+ebp]                     ;170.21
        lea       ecx, DWORD PTR [-1+ecx+esi]                   ;170.21
        sub       ecx, esi                                      ;170.21
        imul      ecx, DWORD PTR [68+edi+edx*8]                 ;170.21
        mov       edx, DWORD PTR [40+edi+edx*8]                 ;170.21
        mov       DWORD PTR [edx+ecx], 0                        ;170.21
                                ; LOE eax
.B1.101:                        ; Preds .B1.99 .B1.94 .B1.93 .B1.100 ; Infreq
        inc       eax                                           ;168.7
        cmp       eax, DWORD PTR [-140+ebp]                     ;168.7
        jb        .B1.93        ; Prob 82%                      ;168.7
                                ; LOE eax
.B1.102:                        ; Preds .B1.101                 ; Infreq
        mov       edx, DWORD PTR [-160+ebp]                     ;
        inc       edx                                           ;168.7
        mov       esi, DWORD PTR [-156+ebp]                     ;
        add       esi, 900                                      ;168.7
        cmp       edx, DWORD PTR [-176+ebp]                     ;168.7
        jb        .B1.92        ; Prob 82%                      ;168.7
                                ; LOE edx esi
.B1.103:                        ; Preds .B1.102                 ; Infreq
        mov       eax, DWORD PTR [-176+ebp]                     ;
                                ; LOE eax
.B1.104:                        ; Preds .B1.116 .B1.103         ; Infreq
        inc       eax                                           ;172.6
        mov       DWORD PTR [-36+ebp], eax                      ;172.6
        jmp       .B1.106       ; Prob 100%                     ;172.6
                                ; LOE
.B1.105:                        ; Preds .B1.88                  ; Infreq
        mov       eax, DWORD PTR [-124+ebp]                     ;47.6
        mov       DWORD PTR [-36+ebp], eax                      ;47.6
                                ; LOE
.B1.106:                        ; Preds .B1.118 .B1.104 .B1.105 ; Infreq
        mov       eax, DWORD PTR [-136+ebp]                     ;190.1
        mov       esp, eax                                      ;190.1
                                ; LOE
.B1.163:                        ; Preds .B1.106                 ; Infreq
        mov       esi, DWORD PTR [-132+ebp]                     ;190.1
        mov       edi, DWORD PTR [-128+ebp]                     ;190.1
        mov       esp, ebp                                      ;190.1
        pop       ebp                                           ;190.1
        mov       esp, ebx                                      ;190.1
        pop       ebx                                           ;190.1
        ret                                                     ;190.1
                                ; LOE
.B1.107:                        ; Preds .B1.73                  ; Infreq
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE] ;114.7
        mov       DWORD PTR [-176+ebp], ecx                     ;114.7
        mov       ecx, edi                                      ;
        shr       ecx, 31                                       ;
        add       ecx, edi                                      ;
        mov       edi, DWORD PTR [-148+ebp]                     ;
        mov       DWORD PTR [-168+ebp], edi                     ;
        sar       ecx, 1                                        ;
        mov       DWORD PTR [-152+ebp], edx                     ;
        mov       DWORD PTR [-172+ebp], 0                       ;
        mov       DWORD PTR [-104+ebp], ecx                     ;
        mov       DWORD PTR [-100+ebp], eax                     ;
        mov       DWORD PTR [-92+ebp], esi                      ;
        mov       edx, edi                                      ;
                                ; LOE edx
.B1.108:                        ; Preds .B1.113 .B1.129 .B1.107 ; Infreq
        cmp       DWORD PTR [-104+ebp], 0                       ;114.7
        jbe       .B1.131       ; Prob 10%                      ;114.7
                                ; LOE edx
.B1.109:                        ; Preds .B1.108                 ; Infreq
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+40] ;
        xor       esi, esi                                      ;
        mov       eax, DWORD PTR [-148+ebp]                     ;
        imul      eax, ecx                                      ;
        imul      ecx, edx                                      ;
        mov       edi, DWORD PTR [-164+ebp]                     ;
        neg       eax                                           ;
        mov       DWORD PTR [-140+ebp], esi                     ;
        add       eax, DWORD PTR [-176+ebp]                     ;
        mov       DWORD PTR [-168+ebp], edx                     ;
        lea       esi, DWORD PTR [edi*8]                        ;
        neg       esi                                           ;
        add       esi, eax                                      ;
        add       eax, ecx                                      ;
        mov       DWORD PTR [-144+ebp], eax                     ;
        lea       eax, DWORD PTR [ecx+edi*8]                    ;
        mov       ecx, DWORD PTR [-140+ebp]                     ;
        add       esi, eax                                      ;
        mov       edx, DWORD PTR [-144+ebp]                     ;
        mov       eax, ecx                                      ;
        mov       edi, DWORD PTR [-160+ebp]                     ;
                                ; LOE eax edx ecx esi edi
.B1.110:                        ; Preds .B1.110 .B1.109         ; Infreq
        inc       ecx                                           ;114.7
        mov       DWORD PTR [eax+edx], edi                      ;114.7
        mov       DWORD PTR [8+eax+esi], edi                    ;114.7
        add       eax, 16                                       ;114.7
        cmp       ecx, DWORD PTR [-104+ebp]                     ;114.7
        jb        .B1.110       ; Prob 64%                      ;114.7
                                ; LOE eax edx ecx esi edi
.B1.111:                        ; Preds .B1.110                 ; Infreq
        mov       edx, DWORD PTR [-168+ebp]                     ;
        lea       esi, DWORD PTR [1+ecx+ecx]                    ;114.7
                                ; LOE edx esi
.B1.112:                        ; Preds .B1.111 .B1.131         ; Infreq
        lea       eax, DWORD PTR [-1+esi]                       ;114.7
        cmp       eax, DWORD PTR [-124+ebp]                     ;114.7
        jae       .B1.129       ; Prob 10%                      ;114.7
                                ; LOE edx esi
.B1.113:                        ; Preds .B1.112                 ; Infreq
        mov       eax, edx                                      ;114.7
        inc       edx                                           ;114.7
        sub       eax, DWORD PTR [-148+ebp]                     ;114.7
        imul      eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+40] ;114.7
        mov       ecx, DWORD PTR [-176+ebp]                     ;114.7
        lea       edi, DWORD PTR [ecx+esi*8]                    ;114.7
        mov       esi, DWORD PTR [-160+ebp]                     ;114.7
        mov       DWORD PTR [-8+edi+eax], esi                   ;114.7
        mov       eax, DWORD PTR [-172+ebp]                     ;114.7
        inc       eax                                           ;114.7
        mov       DWORD PTR [-172+ebp], eax                     ;114.7
        cmp       eax, DWORD PTR [-156+ebp]                     ;114.7
        jb        .B1.108       ; Prob 82%                      ;114.7
                                ; LOE edx
.B1.114:                        ; Preds .B1.113                 ; Infreq
        mov       edx, DWORD PTR [-152+ebp]                     ;
        mov       eax, DWORD PTR [-100+ebp]                     ;
        mov       esi, DWORD PTR [-92+ebp]                      ;
        jmp       .B1.74        ; Prob 100%                     ;
                                ; LOE eax edx esi
.B1.116:                        ; Preds .B1.90 .B1.116          ; Infreq
        inc       edx                                           ;168.7
        cmp       edx, eax                                      ;168.7
        jb        .B1.116       ; Prob 82%                      ;168.7
        jmp       .B1.104       ; Prob 100%                     ;168.7
                                ; LOE eax edx
.B1.118:                        ; Preds .B1.89                  ; Infreq
        mov       DWORD PTR [-36+ebp], 1                        ;47.6
        jmp       .B1.106       ; Prob 100%                     ;47.6
                                ; LOE
.B1.119:                        ; Preds .B1.127 .B1.122 .B1.81  ; Infreq
        push      32                                            ;153.20
        mov       DWORD PTR [-216+ebp], 0                       ;153.20
        lea       eax, DWORD PTR [-168+ebp]                     ;153.20
        push      eax                                           ;153.20
        push      OFFSET FLAT: __STRLITPACK_9.0.1               ;153.20
        push      -2088435968                                   ;153.20
        push      911                                           ;153.20
        mov       DWORD PTR [-168+ebp], 31                      ;153.20
        lea       edx, DWORD PTR [-216+ebp]                     ;153.20
        push      edx                                           ;153.20
        mov       DWORD PTR [-164+ebp], OFFSET FLAT: __STRLITPACK_0 ;153.20
        call      _for_write_seq_lis                            ;153.20
                                ; LOE esi edi
.B1.164:                        ; Preds .B1.119                 ; Infreq
        add       esp, 24                                       ;153.20
                                ; LOE esi edi
.B1.120:                        ; Preds .B1.164                 ; Infreq
        push      32                                            ;154.20
        xor       eax, eax                                      ;154.20
        push      eax                                           ;154.20
        push      eax                                           ;154.20
        push      -2088435968                                   ;154.20
        push      eax                                           ;154.20
        push      OFFSET FLAT: __STRLITPACK_10                  ;154.20
        call      _for_stop_core                                ;154.20
                                ; LOE esi edi
.B1.165:                        ; Preds .B1.120                 ; Infreq
        add       esp, 24                                       ;154.20
        jmp       .B1.82        ; Prob 100%                     ;154.20
                                ; LOE esi edi
.B1.121:                        ; Preds .B1.79                  ; Infreq
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+44] ;135.18
        test      esi, esi                                      ;132.125
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE] ;135.134
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+40] ;135.18
        mov       DWORD PTR [-104+ebp], edx                     ;135.18
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+32] ;135.134
        mov       DWORD PTR [-100+ebp], eax                     ;135.134
        mov       DWORD PTR [-96+ebp], ecx                      ;135.18
        jle       .B1.123       ; Prob 16%                      ;132.125
                                ; LOE edx esi edi
.B1.122:                        ; Preds .B1.121                 ; Infreq
        sub       edi, DWORD PTR [-104+ebp]                     ;
        imul      edi, DWORD PTR [-96+ebp]                      ;
        pxor      xmm0, xmm0                                    ;152.64
        shl       edx, 3                                        ;
        neg       edx                                           ;
        add       edx, DWORD PTR [-84+ebp]                      ;
        add       edx, DWORD PTR [-100+ebp]                     ;
        add       edi, edx                                      ;
        comiss    xmm0, DWORD PTR [edi]                         ;152.64
        ja        .B1.119       ; Prob 5%                       ;152.64
        jmp       .B1.83        ; Prob 100%                     ;152.64
                                ; LOE esi edi
.B1.123:                        ; Preds .B1.121                 ; Infreq
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;133.8
        neg       eax                                           ;133.70
        add       eax, DWORD PTR [-72+ebp]                      ;133.70
        imul      ecx, eax, 900                                 ;133.70
        mov       DWORD PTR [-144+ebp], edx                     ;
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;133.5
        mov       DWORD PTR [-148+ebp], edi                     ;
        mov       DWORD PTR [-156+ebp], edx                     ;133.5
        mov       edi, DWORD PTR [120+edx+ecx]                  ;133.8
        imul      edx, DWORD PTR [152+edx+ecx], -40             ;133.70
        mov       eax, DWORD PTR [-140+ebp]                     ;133.70
        mov       DWORD PTR [-152+ebp], ecx                     ;133.70
        mov       DWORD PTR [-160+ebp], edx                     ;133.70
        lea       eax, DWORD PTR [edi+eax*8]                    ;133.70
        mov       edi, DWORD PTR [-148+ebp]                     ;133.70
        movsx     ecx, WORD PTR [36+edx+eax]                    ;133.8
        test      ecx, ecx                                      ;133.70
        mov       edx, DWORD PTR [-144+ebp]                     ;133.70
        jle       .B1.126       ; Prob 16%                      ;133.70
                                ; LOE eax edx esi edi dl dh
.B1.124:                        ; Preds .B1.123                 ; Infreq
        mov       DWORD PTR [-144+ebp], edx                     ;
        mov       edx, DWORD PTR [-160+ebp]                     ;134.18
        mov       DWORD PTR [-172+ebp], esi                     ;
        mov       esi, DWORD PTR [32+edx+eax]                   ;134.18
        neg       esi                                           ;134.81
        add       esi, edi                                      ;134.81
        imul      esi, DWORD PTR [28+edx+eax]                   ;134.81
        mov       ecx, DWORD PTR [edx+eax]                      ;134.18
        mov       edx, DWORD PTR [-144+ebp]                     ;134.81
        movsx     eax, WORD PTR [ecx+esi]                       ;134.18
        test      eax, eax                                      ;134.81
        mov       DWORD PTR [-176+ebp], eax                     ;134.18
        mov       esi, DWORD PTR [-172+ebp]                     ;134.81
        jle       .B1.126       ; Prob 16%                      ;134.81
                                ; LOE edx esi edi dl dh
.B1.125:                        ; Preds .B1.124                 ; Infreq
        mov       DWORD PTR [-144+ebp], edx                     ;
        mov       edx, DWORD PTR [-156+ebp]                     ;135.63
        mov       ecx, DWORD PTR [-152+ebp]                     ;135.63
        mov       DWORD PTR [-172+ebp], esi                     ;
        mov       eax, DWORD PTR [-140+ebp]                     ;135.18
        mov       esi, DWORD PTR [48+edx+ecx]                   ;135.63
        imul      edx, DWORD PTR [80+edx+ecx], -40              ;135.18
        cvtsi2ss  xmm0, DWORD PTR [-176+ebp]                    ;135.136
        lea       eax, DWORD PTR [esi+eax*8]                    ;135.18
        mov       esi, DWORD PTR [32+edx+eax]                   ;135.63
        neg       esi                                           ;135.18
        add       esi, edi                                      ;135.18
        imul      esi, DWORD PTR [28+edx+eax]                   ;135.18
        mov       ecx, DWORD PTR [edx+eax]                      ;135.63
        mov       eax, DWORD PTR [-144+ebp]                     ;135.18
        shl       eax, 3                                        ;135.18
        cvtsi2ss  xmm1, DWORD PTR [ecx+esi]                     ;135.63
        divss     xmm1, DWORD PTR [_2il0floatpacket.1]          ;135.124
        divss     xmm1, xmm0                                    ;135.18
        sub       edi, DWORD PTR [-104+ebp]                     ;135.18
        neg       eax                                           ;135.18
        imul      edi, DWORD PTR [-96+ebp]                      ;135.18
        add       eax, DWORD PTR [-84+ebp]                      ;135.18
        add       eax, DWORD PTR [-100+ebp]                     ;135.18
        mov       esi, DWORD PTR [-172+ebp]                     ;152.20
        movss     DWORD PTR [eax+edi], xmm1                     ;135.18
        add       edi, eax                                      ;
        movss     xmm1, DWORD PTR [edi]                         ;152.20
        jmp       .B1.81        ; Prob 100%                     ;152.20
                                ; LOE esi edi xmm1
.B1.126:                        ; Preds .B1.124 .B1.123         ; Infreq
        mov       eax, DWORD PTR [-88+ebp]                      ;143.25
        mov       ecx, DWORD PTR [-72+ebp]                      ;143.25
        pxor      xmm2, xmm2                                    ;143.184
        sub       edi, DWORD PTR [-104+ebp]                     ;
        imul      edi, DWORD PTR [-96+ebp]                      ;
        movss     xmm3, DWORD PTR [-4+eax+ecx*4]                ;143.25
        movaps    xmm1, xmm3                                    ;143.35
        sub       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;143.184
        imul      ecx, ecx, 152                                 ;143.184
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;143.37
        shl       edx, 3                                        ;
        neg       edx                                           ;
        mov       eax, DWORD PTR [16+eax+ecx]                   ;143.37
        sub       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;143.184
        add       edx, DWORD PTR [-84+ebp]                      ;
        imul      ecx, eax, 900                                 ;143.184
        add       edx, DWORD PTR [-100+ebp]                     ;
        add       edi, edx                                      ;
        mov       edx, DWORD PTR [-156+ebp]                     ;143.108
        cvtsi2ss  xmm0, DWORD PTR [8+edx+ecx]                   ;143.108
        mulss     xmm0, DWORD PTR [672+edx+ecx]                 ;143.107
        divss     xmm1, xmm0                                    ;143.35
        comiss    xmm1, xmm2                                    ;143.184
        jbe       .B1.128       ; Prob 50%                      ;143.184
                                ; LOE esi edi xmm3
.B1.127:                        ; Preds .B1.126                 ; Infreq
        mulss     xmm3, DWORD PTR [_2il0floatpacket.5]          ;144.24
        pxor      xmm0, xmm0                                    ;152.64
        movss     DWORD PTR [edi], xmm3                         ;144.24
        comiss    xmm0, xmm3                                    ;152.64
        ja        .B1.119       ; Prob 5%                       ;152.64
        jmp       .B1.84        ; Prob 100%                     ;152.64
                                ; LOE esi edi
.B1.128:                        ; Preds .B1.126                 ; Infreq
        mov       eax, 1028443341                               ;146.24
        mov       DWORD PTR [edi], eax                          ;146.24
        jmp       .B1.84        ; Prob 100%                     ;146.24
                                ; LOE
.B1.129:                        ; Preds .B1.112                 ; Infreq
        mov       eax, DWORD PTR [-172+ebp]                     ;114.7
        inc       edx                                           ;114.7
        inc       eax                                           ;114.7
        mov       DWORD PTR [-172+ebp], eax                     ;114.7
        cmp       eax, DWORD PTR [-156+ebp]                     ;114.7
        jb        .B1.108       ; Prob 82%                      ;114.7
                                ; LOE edx
.B1.130:                        ; Preds .B1.129                 ; Infreq
        mov       edx, DWORD PTR [-152+ebp]                     ;
        mov       eax, DWORD PTR [-100+ebp]                     ;
        mov       esi, DWORD PTR [-92+ebp]                      ;
        jmp       .B1.74        ; Prob 100%                     ;
                                ; LOE eax edx esi
.B1.131:                        ; Preds .B1.108                 ; Infreq
        mov       esi, 1                                        ;
        jmp       .B1.112       ; Prob 100%                     ;
                                ; LOE edx esi
.B1.132:                        ; Preds .B1.95                  ; Infreq
        mov       ecx, 1                                        ;
        jmp       .B1.99        ; Prob 100%                     ;
                                ; LOE eax edx ecx esi
.B1.133:                        ; Preds .B1.44                  ; Infreq
        cmp       DWORD PTR [-164+ebp], 0                       ;73.6
        jl        .B1.150       ; Prob 3%                       ;73.6
                                ; LOE eax edx esi xmm0 xmm1 xmm2
.B1.134:                        ; Preds .B1.133                 ; Infreq
        xor       ecx, ecx                                      ;
        mov       DWORD PTR [-156+ebp], ecx                     ;
        mov       DWORD PTR [-172+ebp], eax                     ;
        mov       DWORD PTR [-168+ebp], edx                     ;
        mov       DWORD PTR [-104+ebp], esi                     ;
                                ; LOE ecx xmm0 xmm1 xmm2
.B1.135:                        ; Preds .B1.137 .B1.134         ; Infreq
        mov       eax, DWORD PTR [-152+ebp]                     ;74.22
        lea       edx, DWORD PTR [1+ecx+eax]                    ;74.22
        test      edx, edx                                      ;74.22
        jle       .B1.137       ; Prob 16%                      ;74.22
                                ; LOE edx ecx xmm0 xmm1 xmm2
.B1.136:                        ; Preds .B1.135                 ; Infreq
        jg        L2            ; Prob 50%                      ;75.69
        mov       edx, 1                                        ;75.69
L2:                                                             ;
        mov       edi, DWORD PTR [-52+ebp]                      ;75.12
        mov       esi, DWORD PTR [-104+ebp]                     ;75.12
        mov       eax, DWORD PTR [1232+esi+edi]                 ;75.12
        neg       eax                                           ;75.69
        add       eax, edx                                      ;75.69
        mov       esi, DWORD PTR [1200+esi+edi]                 ;75.12
        cmp       DWORD PTR [esi+eax*4], 0                      ;75.69
        jg        .B1.149       ; Prob 20%                      ;75.69
                                ; LOE edx ecx edi xmm0 xmm1 xmm2
.B1.137:                        ; Preds .B1.136 .B1.135         ; Infreq
        mov       eax, DWORD PTR [-156+ebp]                     ;73.6
        dec       ecx                                           ;73.6
        inc       eax                                           ;73.6
        mov       DWORD PTR [-156+ebp], eax                     ;73.6
        cmp       eax, DWORD PTR [-160+ebp]                     ;73.6
        jb        .B1.135       ; Prob 82%                      ;73.6
                                ; LOE ecx xmm0 xmm1 xmm2
.B1.138:                        ; Preds .B1.137                 ; Infreq
        mov       edx, DWORD PTR [-140+ebp]                     ;107.13
        xor       eax, eax                                      ;
        mov       ecx, DWORD PTR [-96+ebp]                      ;107.13
        mov       esi, DWORD PTR [-104+ebp]                     ;
        mov       edx, DWORD PTR [4+edx+ecx*4]                  ;107.13
                                ; LOE eax edx esi xmm0 xmm1 xmm2
.B1.139:                        ; Preds .B1.138 .B1.149 .B1.150 ; Infreq
        test      al, 1                                         ;83.12
        jne       .B1.46        ; Prob 40%                      ;83.12
                                ; LOE edx esi xmm0 xmm1 xmm2
.B1.140:                        ; Preds .B1.139                 ; Infreq
        mov       eax, DWORD PTR [8+ebx]                        ;84.22
        cmp       DWORD PTR [eax], 1                            ;84.22
        je        .B1.148       ; Prob 16%                      ;84.22
                                ; LOE esi xmm0 xmm1 xmm2
.B1.142:                        ; Preds .B1.140 .B1.148         ; Infreq
        mov       eax, DWORD PTR [-52+ebp]                      ;89.63
        cmp       BYTE PTR [912+eax+esi], 2                     ;89.63
        je        .B1.144       ; Prob 16%                      ;89.63
                                ; LOE eax esi al ah xmm0 xmm1 xmm2
.B1.143:                        ; Preds .B1.142                 ; Infreq
        mov       edx, DWORD PTR [-56+ebp]                      ;90.44
        mov       eax, DWORD PTR [-48+ebp]                      ;90.44
        mov       ecx, DWORD PTR [-140+ebp]                     ;90.19
        mov       esi, DWORD PTR [-96+ebp]                      ;90.19
        movss     xmm3, DWORD PTR [172+edx+eax]                 ;90.44
        divss     xmm3, xmm0                                    ;90.73
        mulss     xmm3, xmm2                                    ;90.83
        cvttss2si edx, xmm3                                     ;90.19
        mov       DWORD PTR [4+ecx+esi*4], edx                  ;90.19
        jmp       .B1.46        ; Prob 100%                     ;90.19
                                ; LOE edx xmm1 xmm2
.B1.144:                        ; Preds .B1.142                 ; Infreq
        movss     xmm0, DWORD PTR [_2il0floatpacket.3]          ;92.68
        comiss    xmm0, DWORD PTR [916+eax+esi]                 ;92.68
        jbe       .B1.146       ; Prob 50%                      ;92.68
                                ; LOE eax esi al ah xmm1 xmm2
.B1.145:                        ; Preds .B1.144                 ; Infreq
        mov       edx, DWORD PTR [-56+ebp]                      ;96.46
        mov       ecx, DWORD PTR [-48+ebp]                      ;96.46
        mov       edi, DWORD PTR [1160+eax+esi]                 ;96.77
        shl       edi, 2                                        ;96.21
        movss     xmm3, DWORD PTR [172+edx+ecx]                 ;96.46
        neg       edi                                           ;96.21
        mov       edx, DWORD PTR [1128+eax+esi]                 ;96.77
        add       edx, DWORD PTR [-84+ebp]                      ;96.21
        mov       ecx, DWORD PTR [-140+ebp]                     ;96.21
        cvtsi2ss  xmm0, DWORD PTR [edi+edx]                     ;96.77
        divss     xmm0, xmm2                                    ;96.123
        divss     xmm3, xmm0                                    ;96.75
        mulss     xmm3, xmm2                                    ;96.134
        cvttss2si edx, xmm3                                     ;96.21
        mov       edi, DWORD PTR [-96+ebp]                      ;96.21
        mov       DWORD PTR [4+ecx+edi*4], edx                  ;96.21
        mov       ecx, DWORD PTR [1092+eax+esi]                 ;97.21
        mov       esi, DWORD PTR [1124+eax+esi]                 ;97.21
        shl       esi, 2                                        ;97.21
        neg       esi                                           ;97.21
        add       ecx, DWORD PTR [-84+ebp]                      ;97.21
        mov       DWORD PTR [esi+ecx], edx                      ;97.21
        jmp       .B1.147       ; Prob 100%                     ;97.21
                                ; LOE edx xmm1 xmm2
.B1.146:                        ; Preds .B1.144                 ; Infreq
        mov       ecx, DWORD PTR [-140+ebp]                     ;99.21
        mov       edx, 10100                                    ;99.21
        mov       edi, DWORD PTR [-96+ebp]                      ;99.21
        mov       DWORD PTR [4+ecx+edi*4], edx                  ;99.21
        mov       ecx, eax                                      ;100.21
        mov       eax, DWORD PTR [1092+ecx+esi]                 ;100.21
        mov       esi, DWORD PTR [1124+ecx+esi]                 ;100.21
        shl       esi, 2                                        ;100.21
        neg       esi                                           ;100.21
        add       eax, DWORD PTR [-84+ebp]                      ;100.21
        mov       DWORD PTR [esi+eax], edx                      ;100.21
                                ; LOE edx xmm1 xmm2
.B1.147:                        ; Preds .B1.145 .B1.146         ; Infreq
        mov       eax, DWORD PTR [-68+ebp]                      ;102.19
        mov       DWORD PTR [eax], 1                            ;102.19
        jmp       .B1.46        ; Prob 100%                     ;102.19
                                ; LOE edx xmm1 xmm2
.B1.148:                        ; Preds .B1.140                 ; Infreq
        mov       eax, DWORD PTR [-52+ebp]                      ;85.19
        movss     xmm0, DWORD PTR [1552+eax+esi]                ;85.19
        jmp       .B1.142       ; Prob 100%                     ;85.19
                                ; LOE esi xmm0 xmm1 xmm2
.B1.149:                        ; Preds .B1.136                 ; Infreq
        mov       esi, DWORD PTR [-104+ebp]                     ;
        mov       DWORD PTR [-176+ebp], edx                     ;
        mov       ecx, DWORD PTR [1092+edi+esi]                 ;76.38
        mov       edi, DWORD PTR [1124+edi+esi]                 ;76.38
        shl       edi, 2                                        ;76.13
        add       ecx, DWORD PTR [-84+ebp]                      ;76.13
        neg       edi                                           ;76.13
        add       ecx, edi                                      ;76.13
        mov       edi, DWORD PTR [-156+ebp]                     ;76.38
        shl       edi, 2                                        ;76.38
        neg       edi                                           ;76.38
        mov       eax, DWORD PTR [-172+ebp]                     ;
        mov       edx, DWORD PTR [-168+ebp]                     ;
        sub       edx, eax                                      ;76.13
        cvtsi2ss  xmm4, DWORD PTR [edi+ecx]                     ;76.38
        divss     xmm4, xmm2                                    ;76.86
        mov       eax, DWORD PTR [-176+ebp]                     ;76.97
        mov       ecx, DWORD PTR [-96+ebp]                      ;76.13
        cvtsi2ss  xmm3, DWORD PTR [edx+eax*4]                   ;76.97
        divss     xmm4, xmm3                                    ;76.96
        mulss     xmm4, xmm2                                    ;76.153
        cvttss2si edx, xmm4                                     ;76.13
        mov       eax, DWORD PTR [-140+ebp]                     ;76.13
        mov       DWORD PTR [4+eax+ecx*4], edx                  ;76.13
        mov       eax, -1                                       ;77.19
        jmp       .B1.139       ; Prob 100%                     ;77.19
                                ; LOE eax edx esi xmm0 xmm1 xmm2
.B1.150:                        ; Preds .B1.133                 ; Infreq
        mov       edx, DWORD PTR [-140+ebp]                     ;107.13
        xor       eax, eax                                      ;
        mov       ecx, DWORD PTR [-96+ebp]                      ;107.13
        mov       edx, DWORD PTR [4+edx+ecx*4]                  ;107.13
        jmp       .B1.139       ; Prob 100%                     ;107.13
                                ; LOE eax edx esi xmm0 xmm1 xmm2
.B1.151:                        ; Preds .B1.33                  ; Infreq
        mov       eax, 1                                        ;
        jmp       .B1.37        ; Prob 100%                     ;
                                ; LOE eax ecx xmm0 xmm1 xmm2 xmm3
.B1.152:                        ; Preds .B1.32                  ; Infreq
        movaps    xmm4, xmm1                                    ;61.13
        jmp       .B1.40        ; Prob 100%                     ;61.13
        ALIGN     16
                                ; LOE xmm0 xmm1 xmm2 xmm3 xmm4
; mark_end;
_CAL_PENALTY ENDP
_TEXT	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
__STRLITPACK_6.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_7.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_8.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_9.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _CAL_PENALTY
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _CAL_PENALTY_MIVA
; mark_begin;
       ALIGN     16
	PUBLIC _CAL_PENALTY_MIVA
_CAL_PENALTY_MIVA	PROC NEAR 
.B2.1:                          ; Preds .B2.0
        push      ebp                                           ;193.12
        mov       ebp, esp                                      ;193.12
        and       esp, -16                                      ;193.12
        push      esi                                           ;193.12
        push      edi                                           ;193.12
        push      ebx                                           ;193.12
        sub       esp, 164                                      ;193.12
        xor       eax, eax                                      ;211.5
        pxor      xmm3, xmm3                                    ;211.5
        movaps    xmm1, xmm3                                    ;211.21
        divss     xmm1, DWORD PTR [_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT] ;211.21
        cvtsi2ss  xmm4, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIMPERAGG] ;211.21
        divss     xmm1, xmm4                                    ;211.21
        movss     xmm2, DWORD PTR [_2il0floatpacket.12]         ;211.21
        andps     xmm2, xmm1                                    ;211.21
        pxor      xmm1, xmm2                                    ;211.21
        movss     xmm5, DWORD PTR [_2il0floatpacket.13]         ;211.21
        movaps    xmm6, xmm1                                    ;211.21
        movaps    xmm0, xmm1                                    ;211.21
        cmpltss   xmm6, xmm5                                    ;211.21
        andps     xmm5, xmm6                                    ;211.21
        movss     xmm7, DWORD PTR [_2il0floatpacket.11]         ;211.21
        addss     xmm0, xmm5                                    ;211.21
        movaps    xmm4, xmm7                                    ;211.21
        subss     xmm0, xmm5                                    ;211.21
        addss     xmm4, xmm7                                    ;211.21
        movaps    xmm5, xmm0                                    ;211.21
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELPENALTY+48] ;213.5
        subss     xmm5, xmm1                                    ;211.21
        movaps    xmm1, xmm5                                    ;211.21
        cmpless   xmm5, DWORD PTR [_2il0floatpacket.14]         ;211.21
        cmpnless  xmm1, xmm7                                    ;211.21
        andps     xmm1, xmm4                                    ;211.21
        andps     xmm5, xmm4                                    ;211.21
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_PENFORPREVENTMOVE] ;213.28
        subss     xmm0, xmm1                                    ;211.21
        mov       ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELPENALTY+56] ;213.5
        addss     xmm0, xmm5                                    ;211.21
        orps      xmm0, xmm2                                    ;211.21
        cvtss2si  edx, xmm0                                     ;211.21
        test      edx, edx                                      ;211.5
        movss     xmm0, DWORD PTR [_DYNUST_MAIN_MODULE_mp_PENFORPREVENTMOVE] ;213.28
        cmovl     edx, eax                                      ;211.5
        test      ecx, ecx                                      ;213.5
        mov       DWORD PTR [80+esp], edx                       ;211.5
        jle       .B2.25        ; Prob 10%                      ;213.5
                                ; LOE ecx ebx esi xmm0 xmm3
.B2.2:                          ; Preds .B2.1
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELPENALTY+24] ;213.5
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELPENALTY+44] ;213.5
        mov       DWORD PTR [20+esp], edx                       ;213.5
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELPENALTY+52] ;213.5
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELPENALTY+40] ;213.5
        mov       DWORD PTR [44+esp], eax                       ;213.5
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELPENALTY+36] ;213.5
        test      eax, eax                                      ;213.5
        mov       DWORD PTR [4+esp], edi                        ;213.5
        mov       DWORD PTR [48+esp], edx                       ;213.5
        mov       edx, 0                                        ;
        jle       .B2.25        ; Prob 10%                      ;213.5
                                ; LOE eax edx ecx ebx esi xmm0 xmm3
.B2.3:                          ; Preds .B2.2
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELPENALTY] ;213.5
        mov       DWORD PTR [36+esp], eax                       ;213.5
        mov       DWORD PTR [60+esp], edi                       ;213.5
        shufps    xmm0, xmm0, 0                                 ;213.5
        mov       DWORD PTR [16+esp], ecx                       ;213.5
        mov       DWORD PTR [esp], ebx                          ;213.5
        mov       DWORD PTR [40+esp], esi                       ;213.5
        mov       eax, DWORD PTR [20+esp]                       ;213.5
                                ; LOE eax edx xmm0 xmm3
.B2.4:                          ; Preds .B2.23 .B2.3
        mov       ecx, DWORD PTR [esp]                          ;
        xor       ebx, ebx                                      ;
        mov       esi, DWORD PTR [4+esp]                        ;
        mov       DWORD PTR [8+esp], ebx                        ;
        mov       ebx, DWORD PTR [44+esp]                       ;
        lea       edi, DWORD PTR [ecx+edx]                      ;
        imul      edi, esi                                      ;
        imul      ecx, esi                                      ;
        imul      ebx, DWORD PTR [48+esp]                       ;
        mov       esi, edi                                      ;
        sub       esi, ecx                                      ;
        test      eax, eax                                      ;213.5
        jle       .B2.122       ; Prob 0%                       ;213.5
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm3
.B2.5:                          ; Preds .B2.4
        mov       DWORD PTR [28+esp], ecx                       ;
        mov       DWORD PTR [24+esp], esi                       ;
        mov       DWORD PTR [32+esp], edi                       ;
        mov       DWORD PTR [56+esp], ebx                       ;
        mov       DWORD PTR [12+esp], edx                       ;
        mov       ecx, DWORD PTR [8+esp]                        ;
                                ; LOE eax ecx xmm0 xmm3
.B2.6:                          ; Preds .B2.21 .B2.118 .B2.5
        cmp       eax, 8                                        ;213.5
        jl        .B2.117       ; Prob 10%                      ;213.5
                                ; LOE eax ecx xmm0 xmm3
.B2.7:                          ; Preds .B2.6
        mov       edx, DWORD PTR [44+esp]                       ;213.5
        mov       esi, DWORD PTR [28+esp]                       ;213.5
        mov       ebx, DWORD PTR [60+esp]                       ;213.5
        add       edx, ecx                                      ;213.5
        imul      edx, DWORD PTR [48+esp]                       ;213.5
        sub       esi, edx                                      ;213.5
        neg       esi                                           ;213.5
        add       esi, DWORD PTR [32+esp]                       ;213.5
        add       ebx, esi                                      ;213.5
        sub       ebx, DWORD PTR [56+esp]                       ;213.5
        and       ebx, 15                                       ;213.5
        je        .B2.10        ; Prob 50%                      ;213.5
                                ; LOE eax edx ecx ebx esi xmm0 xmm3
.B2.8:                          ; Preds .B2.7
        test      bl, 3                                         ;213.5
        jne       .B2.117       ; Prob 10%                      ;213.5
                                ; LOE eax edx ecx ebx esi xmm0 xmm3
.B2.9:                          ; Preds .B2.8
        neg       ebx                                           ;213.5
        add       ebx, 16                                       ;213.5
        shr       ebx, 2                                        ;213.5
                                ; LOE eax edx ecx ebx esi xmm0 xmm3
.B2.10:                         ; Preds .B2.9 .B2.7
        lea       edi, DWORD PTR [8+ebx]                        ;213.5
        cmp       eax, edi                                      ;213.5
        jl        .B2.117       ; Prob 10%                      ;213.5
                                ; LOE eax edx ecx ebx esi xmm0 xmm3
.B2.11:                         ; Preds .B2.10
        mov       edi, eax                                      ;213.5
        sub       edi, ebx                                      ;213.5
        and       edi, 7                                        ;213.5
        add       esi, DWORD PTR [60+esp]                       ;
        neg       edi                                           ;213.5
        sub       esi, DWORD PTR [56+esp]                       ;
        add       edi, eax                                      ;213.5
        mov       DWORD PTR [52+esp], edi                       ;213.5
        test      ebx, ebx                                      ;213.5
        jbe       .B2.15        ; Prob 10%                      ;213.5
                                ; LOE eax edx ecx ebx esi xmm0 xmm3
.B2.12:                         ; Preds .B2.11
        xor       edi, edi                                      ;
        mov       eax, edi                                      ;
        mov       edi, DWORD PTR [40+esp]                       ;
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm3
.B2.13:                         ; Preds .B2.13 .B2.12
        mov       DWORD PTR [esi+eax*4], edi                    ;213.5
        inc       eax                                           ;213.5
        cmp       eax, ebx                                      ;213.5
        jb        .B2.13        ; Prob 82%                      ;213.5
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm3
.B2.14:                         ; Preds .B2.13
        mov       eax, DWORD PTR [20+esp]                       ;
                                ; LOE eax edx ecx ebx esi xmm0 xmm3
.B2.15:                         ; Preds .B2.11 .B2.14
        sub       edx, DWORD PTR [56+esp]                       ;
        add       edx, DWORD PTR [24+esp]                       ;
        add       edx, DWORD PTR [60+esp]                       ;
        mov       edi, DWORD PTR [52+esp]                       ;
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm3
.B2.16:                         ; Preds .B2.16 .B2.15
        movaps    XMMWORD PTR [esi+ebx*4], xmm0                 ;213.5
        movaps    XMMWORD PTR [16+edx+ebx*4], xmm0              ;213.5
        add       ebx, 8                                        ;213.5
        cmp       ebx, edi                                      ;213.5
        jb        .B2.16        ; Prob 82%                      ;213.5
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm3
.B2.17:                         ; Preds .B2.16
        mov       DWORD PTR [52+esp], edi                       ;
                                ; LOE eax ecx xmm0 xmm3
.B2.18:                         ; Preds .B2.17 .B2.117
        cmp       eax, DWORD PTR [52+esp]                       ;213.5
        jbe       .B2.118       ; Prob 10%                      ;213.5
                                ; LOE eax ecx xmm0 xmm3
.B2.19:                         ; Preds .B2.18
        mov       edx, DWORD PTR [44+esp]                       ;
        mov       ebx, DWORD PTR [52+esp]                       ;
        mov       esi, DWORD PTR [40+esp]                       ;
        add       edx, ecx                                      ;
        imul      edx, DWORD PTR [48+esp]                       ;
        neg       edx                                           ;
        add       edx, DWORD PTR [28+esp]                       ;
        neg       edx                                           ;
        add       edx, DWORD PTR [32+esp]                       ;
        add       edx, DWORD PTR [60+esp]                       ;
        sub       edx, DWORD PTR [56+esp]                       ;
                                ; LOE eax edx ecx ebx esi xmm0 xmm3
.B2.20:                         ; Preds .B2.20 .B2.19
        mov       DWORD PTR [edx+ebx*4], esi                    ;213.5
        inc       ebx                                           ;213.5
        cmp       ebx, eax                                      ;213.5
        jb        .B2.20        ; Prob 82%                      ;213.5
                                ; LOE eax edx ecx ebx esi xmm0 xmm3
.B2.21:                         ; Preds .B2.20
        inc       ecx                                           ;213.5
        cmp       ecx, DWORD PTR [36+esp]                       ;213.5
        jb        .B2.6         ; Prob 82%                      ;213.5
                                ; LOE eax ecx xmm0 xmm3
.B2.22:                         ; Preds .B2.118 .B2.21          ; Infreq
        DB        15                                            ;
        DB        31                                            ;
        DB        68                                            ;
        DB        0                                             ;
        DB        0                                             ;
        mov       edx, DWORD PTR [12+esp]                       ;
                                ; LOE eax edx xmm0 xmm3
.B2.23:                         ; Preds .B2.123 .B2.22          ; Infreq
        inc       edx                                           ;213.5
        cmp       edx, DWORD PTR [16+esp]                       ;213.5
        jb        .B2.4         ; Prob 82%                      ;213.5
                                ; LOE eax edx xmm0 xmm3
.B2.25:                         ; Preds .B2.23 .B2.2 .B2.1      ; Infreq
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELTIME+36] ;214.5
        test      eax, eax                                      ;214.5
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELTIME+44] ;214.5
        jle       .B2.27        ; Prob 10%                      ;214.5
                                ; LOE eax edx xmm3
.B2.26:                         ; Preds .B2.25                  ; Infreq
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELTIME+24] ;214.5
        test      ecx, ecx                                      ;214.5
        mov       ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELTIME+32] ;214.5
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELTIME] ;241.119
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELTIME+40] ;241.16
        jg        .B2.52        ; Prob 50%                      ;214.5
                                ; LOE eax edx ecx ebx esi edi xmm3
.B2.27:                         ; Preds .B2.111 .B2.114 .B2.56 .B2.25 .B2.26
                                ;                               ; Infreq
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFARCS] ;216.7
        test      ecx, ecx                                      ;216.7
        jle       .B2.37        ; Prob 3%                       ;216.7
                                ; LOE edx ecx xmm3
.B2.28:                         ; Preds .B2.27                  ; Infreq
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELTIME+40] ;241.16
        imul      edx, esi                                      ;
        movss     xmm0, DWORD PTR [_2il0floatpacket.9]          ;220.121
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_AGGINT] ;217.9
        mov       DWORD PTR [52+esp], eax                       ;217.9
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;249.9
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELTIME+32] ;241.16
        shl       edi, 2                                        ;
        mov       DWORD PTR [esp], eax                          ;249.9
        mov       ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELTIME] ;241.119
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;249.12
        sub       ebx, edi                                      ;
        mov       DWORD PTR [28+esp], eax                       ;249.12
        sub       ebx, edx                                      ;
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;219.14
        add       ebx, esi                                      ;
        mov       DWORD PTR [32+esp], eax                       ;219.14
        xor       eax, eax                                      ;
        mov       DWORD PTR [8+esp], ebx                        ;
        xor       edx, edx                                      ;
        mov       DWORD PTR [44+esp], esi                       ;
        mov       DWORD PTR [64+esp], ecx                       ;
                                ; LOE eax edx xmm0 xmm3
.B2.29:                         ; Preds .B2.35 .B2.28           ; Infreq
        cmp       DWORD PTR [52+esp], 0                         ;217.9
        jle       .B2.35        ; Prob 3%                       ;217.9
                                ; LOE eax edx xmm0 xmm3
.B2.30:                         ; Preds .B2.29                  ; Infreq
        imul      ebx, DWORD PTR [32+esp], -900                 ;
        mov       edi, edx                                      ;
        add       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;
        xor       ecx, ecx                                      ;
        mov       DWORD PTR [40+esp], edi                       ;
        mov       DWORD PTR [20+esp], edx                       ;
        mov       esi, DWORD PTR [1232+ebx+edi]                 ;219.14
        shl       esi, 2                                        ;
        neg       esi                                           ;
        add       esi, DWORD PTR [1200+ebx+edi]                 ;
        mov       DWORD PTR [48+esp], esi                       ;
        mov       esi, DWORD PTR [8+esp]                        ;
        mov       DWORD PTR [16+esp], eax                       ;
        lea       esi, DWORD PTR [4+esi+eax*4]                  ;
        mov       DWORD PTR [24+esp], esi                       ;
        mov       esi, DWORD PTR [28+esp]                       ;
        neg       esi                                           ;
        add       esi, eax                                      ;
        imul      esi, esi, 152                                 ;
        mov       DWORD PTR [12+esp], esi                       ;
        mov       esi, DWORD PTR [24+esp]                       ;
                                ; LOE ecx ebx esi xmm0 xmm3
.B2.31:                         ; Preds .B2.33 .B2.30           ; Infreq
        mov       eax, DWORD PTR [48+esp]                       ;219.14
        mov       eax, DWORD PTR [4+eax+ecx*4]                  ;219.14
        test      eax, eax                                      ;219.56
        jle       .B2.57        ; Prob 16%                      ;219.56
                                ; LOE eax ecx ebx esi xmm0 xmm3
.B2.32:                         ; Preds .B2.31                  ; Infreq
        mov       edi, DWORD PTR [40+esp]                       ;220.7
        mov       edx, ecx                                      ;220.7
        cvtsi2ss  xmm1, eax                                     ;220.79
        sub       edx, DWORD PTR [1124+ebx+edi]                 ;220.7
        mov       edi, DWORD PTR [1092+ebx+edi]                 ;220.28
        cvtsi2ss  xmm2, DWORD PTR [4+edi+edx*4]                 ;220.28
        divss     xmm2, xmm0                                    ;220.68
        divss     xmm2, xmm1                                    ;220.78
        mulss     xmm2, xmm0                                    ;220.121
        cvttss2si eax, xmm2                                     ;220.7
        mov       DWORD PTR [esi], eax                          ;220.7
                                ; LOE ecx ebx esi xmm0 xmm3
.B2.33:                         ; Preds .B2.65 .B2.32 .B2.66    ; Infreq
        inc       ecx                                           ;217.9
        add       esi, DWORD PTR [44+esp]                       ;217.9
        cmp       ecx, DWORD PTR [52+esp]                       ;217.9
        jb        .B2.31        ; Prob 82%                      ;217.9
                                ; LOE ecx ebx esi xmm0 xmm3
.B2.34:                         ; Preds .B2.33                  ; Infreq
        mov       edx, DWORD PTR [20+esp]                       ;
        mov       eax, DWORD PTR [16+esp]                       ;
                                ; LOE eax edx xmm0 xmm3
.B2.35:                         ; Preds .B2.34 .B2.29           ; Infreq
        inc       eax                                           ;216.7
        add       edx, 900                                      ;216.7
        cmp       eax, DWORD PTR [64+esp]                       ;216.7
        jb        .B2.29        ; Prob 82%                      ;216.7
                                ; LOE eax edx xmm0 xmm3
.B2.36:                         ; Preds .B2.35                  ; Infreq
        mov       ecx, DWORD PTR [64+esp]                       ;
        test      ecx, ecx                                      ;216.7
                                ; LOE ecx xmm3
.B2.37:                         ; Preds .B2.36 .B2.27           ; Infreq
        mov       edx, 1                                        ;247.8
        mov       ebx, edx                                      ;247.8
        jle       .B2.51        ; Prob 2%                       ;247.8
                                ; LOE edx ecx ebx xmm3
.B2.38:                         ; Preds .B2.37                  ; Infreq
        movss     xmm1, DWORD PTR [_2il0floatpacket.9]          ;265.110
        mov       eax, ebx                                      ;
        movss     xmm0, DWORD PTR [_2il0floatpacket.11]         ;291.113
        mov       DWORD PTR [64+esp], ecx                       ;291.113
                                ; LOE eax edx ebx
.B2.39:                         ; Preds .B2.49 .B2.38           ; Infreq
        mov       ecx, eax                                      ;249.9
        sub       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;249.9
        imul      esi, ecx, 152                                 ;249.9
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;249.9
        mov       edi, DWORD PTR [12+ecx+esi]                   ;249.9
        movsx     esi, BYTE PTR [108+ecx+esi]                   ;252.21
        test      esi, esi                                      ;252.10
        jle       .B2.49        ; Prob 2%                       ;252.10
                                ; LOE eax edx ecx ebx esi edi
.B2.40:                         ; Preds .B2.39                  ; Infreq
        mov       DWORD PTR [116+esp], edx                      ;216.7
        lea       eax, DWORD PTR [edi*4]                        ;249.9
        shl       edi, 5                                        ;249.9
        mov       edx, ebx                                      ;
        sub       edi, eax                                      ;249.9
        mov       DWORD PTR [92+esp], edi                       ;249.9
        mov       DWORD PTR [88+esp], eax                       ;249.9
        mov       DWORD PTR [124+esp], edx                      ;249.9
        mov       DWORD PTR [120+esp], esi                      ;249.9
                                ; LOE ecx
.B2.41:                         ; Preds .B2.133 .B2.40          ; Infreq
        imul      edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32], 152 ;253.11
        mov       ebx, ecx                                      ;253.11
        imul      eax, DWORD PTR [116+esp], 152                 ;253.11
        sub       ebx, edx                                      ;253.11
        add       ecx, eax                                      ;254.16
        neg       edx                                           ;254.16
        mov       edi, DWORD PTR [144+ebx+eax]                  ;253.11
        neg       edi                                           ;253.11
        add       edi, DWORD PTR [124+esp]                      ;253.11
        imul      edi, DWORD PTR [140+ebx+eax]                  ;253.11
        mov       esi, DWORD PTR [112+ebx+eax]                  ;253.11
        mov       eax, DWORD PTR [144+edx+ecx]                  ;254.16
        neg       eax                                           ;254.16
        add       eax, DWORD PTR [124+esp]                      ;254.16
        imul      esi, DWORD PTR [esi+edi], 152                 ;253.16
        imul      eax, DWORD PTR [140+edx+ecx]                  ;254.16
        add       eax, DWORD PTR [112+edx+ecx]                  ;254.16
        lea       ecx, DWORD PTR [116+esp]                      ;254.16
        mov       ebx, DWORD PTR [12+ebx+esi]                   ;253.11
        mov       DWORD PTR [128+esp], ebx                      ;253.11
        push      ecx                                           ;254.16
        push      eax                                           ;254.16
        call      _DYNUST_NETWORK_MODULE_mp_GETBSTMOVE          ;254.16
                                ; LOE eax
.B2.127:                        ; Preds .B2.41                  ; Infreq
        add       esp, 8                                        ;254.16
                                ; LOE eax
.B2.42:                         ; Preds .B2.127                 ; Infreq
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_AGGINT] ;258.11
        test      edx, edx                                      ;258.11
        mov       DWORD PTR [148+esp], edx                      ;258.11
        jle       .B2.47        ; Prob 2%                       ;258.11
                                ; LOE eax
.B2.43:                         ; Preds .B2.42                  ; Infreq
        cvtsi2ss  xmm0, eax                                     ;278.50
        imul      esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+32], -28 ;262.16
        divss     xmm0, DWORD PTR [_2il0floatpacket.9]          ;278.50
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;263.7
        mov       DWORD PTR [112+esp], ebx                      ;263.7
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+44] ;262.16
        neg       ebx                                           ;262.16
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE] ;262.13
        add       ebx, eax                                      ;262.16
        add       edi, DWORD PTR [92+esp]                       ;262.16
        imul      ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+40] ;262.16
        add       edi, esi                                      ;262.16
        mov       esi, DWORD PTR [128+esp]                      ;
        sub       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;
        movsx     ebx, BYTE PTR [11+edi+ebx]                    ;262.16
        imul      edi, esi, 900                                 ;
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELPENALTY+40] ;265.19
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELPENALTY+56] ;265.19
        neg       edx                                           ;
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELPENALTY+44] ;
        add       edx, eax                                      ;
        mov       DWORD PTR [152+esp], ecx                      ;265.19
        mov       DWORD PTR [108+esp], ecx                      ;
        imul      esi, ecx                                      ;
        imul      edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELPENALTY+52] ;
        cvttss2si ecx, xmm0                                     ;278.50
        mov       DWORD PTR [104+esp], edi                      ;
        pxor      xmm0, xmm0                                    ;278.50
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELPENALTY+32] ;
        shl       edi, 2                                        ;
        neg       edi                                           ;
        add       edi, DWORD PTR [88+esp]                       ;
        add       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELPENALTY] ;
        sub       edi, esi                                      ;
        mov       DWORD PTR [144+esp], 1                        ;
        add       edx, edi                                      ;
        mov       esi, DWORD PTR [144+esp]                      ;278.50
        mov       edi, DWORD PTR [108+esp]                      ;278.50
        mov       DWORD PTR [96+esp], ecx                       ;278.50
        mov       DWORD PTR [100+esp], edx                      ;278.50
        mov       DWORD PTR [132+esp], eax                      ;278.50
                                ; LOE ebx esi edi xmm0
.B2.44:                         ; Preds .B2.45 .B2.43           ; Infreq
        test      ebx, ebx                                      ;262.66
        jle       .B2.70        ; Prob 16%                      ;262.66
                                ; LOE ebx esi edi xmm0
.B2.45:                         ; Preds .B2.84 .B2.131 .B2.90 .B2.44 ; Infreq
        inc       esi                                           ;305.9
        add       edi, DWORD PTR [152+esp]                      ;305.9
        cmp       esi, DWORD PTR [148+esp]                      ;305.9
        jle       .B2.44        ; Prob 82%                      ;305.9
                                ; LOE ebx esi edi xmm0
.B2.47:                         ; Preds .B2.45 .B2.42           ; Infreq
        mov       eax, DWORD PTR [124+esp]                      ;306.9
        inc       eax                                           ;306.9
        mov       DWORD PTR [124+esp], eax                      ;306.9
        cmp       eax, DWORD PTR [120+esp]                      ;306.9
        jg        .B2.48        ; Prob 18%                      ;306.9
                                ; LOE
.B2.133:                        ; Preds .B2.47                  ; Infreq
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;249.9
        jmp       .B2.41        ; Prob 100%                     ;249.9
                                ; LOE ecx
.B2.48:                         ; Preds .B2.47                  ; Infreq
        mov       eax, DWORD PTR [116+esp]                      ;307.7
        mov       ebx, 1                                        ;
                                ; LOE eax ebx
.B2.49:                         ; Preds .B2.48 .B2.39           ; Infreq
        inc       eax                                           ;307.7
        mov       edx, eax                                      ;307.7
        cmp       eax, DWORD PTR [64+esp]                       ;307.7
        jle       .B2.39        ; Prob 82%                      ;307.7
                                ; LOE eax edx ebx
.B2.51:                         ; Preds .B2.49 .B2.37           ; Infreq
        add       esp, 164                                      ;309.1
        pop       ebx                                           ;309.1
        pop       edi                                           ;309.1
        pop       esi                                           ;309.1
        mov       esp, ebp                                      ;309.1
        pop       ebp                                           ;309.1
        ret                                                     ;309.1
                                ; LOE
.B2.52:                         ; Preds .B2.26                  ; Infreq
        mov       DWORD PTR [8+esp], edx                        ;
        imul      edx, esi                                      ;
        pxor      xmm0, xmm0                                    ;214.5
        mov       DWORD PTR [16+esp], eax                       ;
        xor       eax, eax                                      ;
        mov       DWORD PTR [20+esp], eax                       ;
        mov       eax, edi                                      ;
        shl       ebx, 2                                        ;
        sub       eax, ebx                                      ;
        sub       ebx, edx                                      ;
        add       eax, ebx                                      ;
        lea       ebx, DWORD PTR [ecx*4]                        ;
        add       eax, edx                                      ;
        mov       DWORD PTR [esp], edi                          ;
        mov       DWORD PTR [4+esp], eax                        ;
        mov       DWORD PTR [24+esp], ebx                       ;
        mov       DWORD PTR [12+esp], esi                       ;
        mov       DWORD PTR [28+esp], edi                       ;
        mov       ebx, DWORD PTR [20+esp]                       ;
        mov       eax, DWORD PTR [16+esp]                       ;
        mov       edx, DWORD PTR [8+esp]                        ;
        mov       esi, DWORD PTR [4+esp]                        ;
        mov       edi, DWORD PTR [esp]                          ;
        mov       DWORD PTR [32+esp], ecx                       ;
                                ; LOE ebx esi edi
.B2.53:                         ; Preds .B2.55 .B2.113 .B2.110 .B2.52 ; Infreq
        cmp       DWORD PTR [32+esp], 24                        ;214.5
        jle       .B2.95        ; Prob 0%                       ;214.5
                                ; LOE ebx esi edi
.B2.54:                         ; Preds .B2.53                  ; Infreq
        push      DWORD PTR [24+esp]                            ;214.5
        push      0                                             ;214.5
        push      esi                                           ;214.5
        call      __intel_fast_memset                           ;214.5
                                ; LOE ebx esi edi
.B2.128:                        ; Preds .B2.54                  ; Infreq
        add       esp, 12                                       ;214.5
                                ; LOE ebx esi edi
.B2.55:                         ; Preds .B2.128                 ; Infreq
        mov       edx, DWORD PTR [20+esp]                       ;214.5
        inc       edx                                           ;214.5
        mov       eax, DWORD PTR [12+esp]                       ;214.5
        add       edi, eax                                      ;214.5
        add       esi, eax                                      ;214.5
        add       ebx, eax                                      ;214.5
        mov       DWORD PTR [20+esp], edx                       ;214.5
        cmp       edx, DWORD PTR [16+esp]                       ;214.5
        jb        .B2.53        ; Prob 82%                      ;214.5
                                ; LOE ebx esi edi
.B2.56:                         ; Preds .B2.55                  ; Infreq
        mov       edx, DWORD PTR [8+esp]                        ;
        pxor      xmm3, xmm3                                    ;
        jmp       .B2.27        ; Prob 100%                     ;
                                ; LOE edx xmm3
.B2.57:                         ; Preds .B2.31                  ; Infreq
        xor       eax, eax                                      ;223.13
        test      ecx, ecx                                      ;223.13
        mov       edx, DWORD PTR [80+esp]                       ;223.13
        cmovge    eax, ecx                                      ;223.13
        cmp       eax, edx                                      ;223.13
        cmovge    eax, edx                                      ;223.13
        test      eax, eax                                      ;223.13
        lea       edi, DWORD PTR [1+eax]                        ;223.13
        mov       DWORD PTR [36+esp], edi                       ;223.13
        jl        .B2.69        ; Prob 3%                       ;223.13
                                ; LOE ecx ebx esi xmm0 xmm3
.B2.58:                         ; Preds .B2.57                  ; Infreq
        mov       DWORD PTR [4+esp], ebx                        ;
        xor       edx, edx                                      ;
        mov       DWORD PTR [24+esp], esi                       ;
        mov       eax, ecx                                      ;
        mov       ebx, DWORD PTR [48+esp]                       ;
                                ; LOE eax edx ecx ebx xmm0 xmm3
.B2.59:                         ; Preds .B2.63 .B2.58           ; Infreq
        lea       esi, DWORD PTR [1+eax]                        ;224.15
        test      esi, esi                                      ;224.15
        jle       .B2.61        ; Prob 16%                      ;224.15
                                ; LOE eax edx ecx ebx esi xmm0 xmm3
.B2.60:                         ; Preds .B2.59                  ; Infreq
        jg        L4            ; Prob 50%                      ;225.62
        mov       esi, 1                                        ;225.62
L4:                                                             ;
        mov       edi, DWORD PTR [ebx+esi*4]                    ;225.10
        test      edi, edi                                      ;225.62
        jg        .B2.67        ; Prob 20%                      ;225.62
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm3
.B2.61:                         ; Preds .B2.59 .B2.60           ; Infreq
        lea       esi, DWORD PTR [1+ecx+edx]                    ;231.20
        cmp       esi, DWORD PTR [52+esp]                       ;231.24
        jg        .B2.63        ; Prob 50%                      ;231.24
                                ; LOE eax edx ecx ebx esi xmm0 xmm3
.B2.62:                         ; Preds .B2.61                  ; Infreq
        mov       edi, DWORD PTR [52+esp]                       ;232.76
        cmp       esi, edi                                      ;232.76
        jl        L5            ; Prob 50%                      ;232.76
        mov       esi, edi                                      ;232.76
L5:                                                             ;
        mov       edi, DWORD PTR [ebx+esi*4]                    ;232.19
        test      edi, edi                                      ;232.76
        jg        .B2.67        ; Prob 20%                      ;232.76
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm3
.B2.63:                         ; Preds .B2.61 .B2.62           ; Infreq
        inc       edx                                           ;223.13
        dec       eax                                           ;223.13
        cmp       edx, DWORD PTR [36+esp]                       ;223.13
        jb        .B2.59        ; Prob 82%                      ;223.13
                                ; LOE eax edx ecx ebx xmm0 xmm3
.B2.64:                         ; Preds .B2.63                  ; Infreq
        mov       esi, DWORD PTR [24+esp]                       ;
        xor       eax, eax                                      ;
        mov       ebx, DWORD PTR [4+esp]                        ;
                                ; LOE eax ecx ebx esi xmm0 xmm3
.B2.65:                         ; Preds .B2.64 .B2.67 .B2.69    ; Infreq
        test      al, 1                                         ;240.12
        jne       .B2.33        ; Prob 40%                      ;240.12
                                ; LOE ecx ebx esi xmm0 xmm3
.B2.66:                         ; Preds .B2.65                  ; Infreq
        mov       edx, DWORD PTR [12+esp]                       ;241.36
        mov       eax, DWORD PTR [esp]                          ;241.36
        mov       edi, DWORD PTR [40+esp]                       ;241.16
        movss     xmm2, DWORD PTR [172+eax+edx]                 ;241.36
        mov       eax, ecx                                      ;241.16
        sub       eax, DWORD PTR [1160+ebx+edi]                 ;241.16
        mov       edi, DWORD PTR [1128+ebx+edi]                 ;241.67
        cvtsi2ss  xmm1, DWORD PTR [4+edi+eax*4]                 ;241.67
        divss     xmm1, xmm0                                    ;241.108
        divss     xmm2, xmm1                                    ;241.65
        mulss     xmm2, xmm0                                    ;241.119
        cvttss2si edx, xmm2                                     ;241.16
        mov       DWORD PTR [esi], edx                          ;241.16
        jmp       .B2.33        ; Prob 100%                     ;241.16
                                ; LOE ecx ebx esi xmm0 xmm3
.B2.67:                         ; Preds .B2.60 .B2.62           ; Infreq
        mov       edx, DWORD PTR [40+esp]                       ;233.32
        mov       eax, esi                                      ;
        mov       ebx, DWORD PTR [4+esp]                        ;
        cvtsi2ss  xmm1, edi                                     ;233.98
        sub       eax, DWORD PTR [1124+ebx+edx]                 ;233.12
        mov       edx, DWORD PTR [1092+ebx+edx]                 ;233.32
        mov       esi, DWORD PTR [24+esp]                       ;
        cvtsi2ss  xmm2, DWORD PTR [edx+eax*4]                   ;233.32
        divss     xmm2, xmm0                                    ;233.87
        divss     xmm2, xmm1                                    ;233.97
        mulss     xmm2, xmm0                                    ;233.154
        cvttss2si eax, xmm2                                     ;233.12
        mov       DWORD PTR [esi], eax                          ;233.12
        mov       eax, -1                                       ;234.18
        jmp       .B2.65        ; Prob 100%                     ;234.18
                                ; LOE eax ecx ebx esi xmm0 xmm3
.B2.69:                         ; Preds .B2.57                  ; Infreq
        xor       eax, eax                                      ;
        jmp       .B2.65        ; Prob 100%                     ;
                                ; LOE eax ecx ebx esi xmm0 xmm3
.B2.70:                         ; Preds .B2.44                  ; Infreq
        mov       eax, DWORD PTR [112+esp]                      ;263.72
        mov       ecx, DWORD PTR [104+esp]                      ;263.72
        imul      edx, DWORD PTR [152+eax+ecx], -40             ;263.72
        add       edx, DWORD PTR [120+eax+ecx]                  ;263.72
        lea       eax, DWORD PTR [esi+esi*4]                    ;263.72
        mov       DWORD PTR [136+esp], edx                      ;263.72
        movsx     edx, WORD PTR [36+edx+eax*8]                  ;263.10
        test      edx, edx                                      ;263.72
        jle       .B2.93        ; Prob 16%                      ;263.72
                                ; LOE eax ebx esi edi xmm0
.B2.71:                         ; Preds .B2.70                  ; Infreq
        mov       edx, DWORD PTR [136+esp]                      ;264.8
        mov       DWORD PTR [84+esp], ebx                       ;
        mov       ebx, DWORD PTR [32+edx+eax*8]                 ;264.8
        neg       ebx                                           ;264.70
        add       ebx, DWORD PTR [132+esp]                      ;264.70
        imul      ebx, DWORD PTR [28+edx+eax*8]                 ;264.70
        mov       ecx, DWORD PTR [edx+eax*8]                    ;264.8
        movsx     ecx, WORD PTR [ecx+ebx]                       ;264.8
        test      ecx, ecx                                      ;264.70
        mov       ebx, DWORD PTR [84+esp]                       ;264.70
        jle       .B2.93        ; Prob 16%                      ;264.70
                                ; LOE eax ecx ebx esi edi bl bh xmm0
.B2.72:                         ; Preds .B2.71                  ; Infreq
        mov       DWORD PTR [144+esp], esi                      ;
        mov       DWORD PTR [84+esp], ebx                       ;
        mov       ebx, DWORD PTR [112+esp]                      ;265.19
        mov       esi, DWORD PTR [104+esp]                      ;265.19
        cvtsi2ss  xmm1, ecx                                     ;265.122
        imul      edx, DWORD PTR [80+ebx+esi], -40              ;265.19
        add       edx, DWORD PTR [48+ebx+esi]                   ;265.19
        mov       ecx, DWORD PTR [100+esp]                      ;265.19
        mov       esi, DWORD PTR [144+esp]                      ;266.51
        mov       ebx, DWORD PTR [32+edx+eax*8]                 ;265.49
        neg       ebx                                           ;265.19
        add       ebx, DWORD PTR [132+esp]                      ;265.19
        imul      ebx, DWORD PTR [28+edx+eax*8]                 ;265.19
        mov       edx, DWORD PTR [edx+eax*8]                    ;265.49
        cvtsi2ss  xmm2, DWORD PTR [edx+ebx]                     ;265.49
        divss     xmm2, DWORD PTR [_2il0floatpacket.9]          ;265.110
        divss     xmm2, xmm1                                    ;265.19
        movss     DWORD PTR [edi+ecx], xmm2                     ;265.19
        mov       ebx, DWORD PTR [84+esp]                       ;266.51
        comiss    xmm0, xmm2                                    ;266.51
        jbe       .B2.74        ; Prob 50%                      ;266.51
                                ; LOE eax ebx esi edi bl bh xmm0
.B2.73:                         ; Preds .B2.72                  ; Infreq
        mov       DWORD PTR [16+esp], 0                         ;267.22
        lea       ecx, DWORD PTR [16+esp]                       ;267.22
        mov       DWORD PTR [56+esp], 23                        ;267.22
        lea       edx, DWORD PTR [56+esp]                       ;267.22
        mov       DWORD PTR [60+esp], OFFSET FLAT: __STRLITPACK_15 ;267.22
        push      32                                            ;267.22
        push      edx                                           ;267.22
        push      OFFSET FLAT: __STRLITPACK_16.0.2              ;267.22
        push      -2088435968                                   ;267.22
        push      -1                                            ;267.22
        push      ecx                                           ;267.22
        mov       DWORD PTR [100+esp], eax                      ;267.22
        call      _for_write_seq_lis                            ;267.22
                                ; LOE ebx esi edi bl bh
.B2.129:                        ; Preds .B2.73                  ; Infreq
        mov       eax, DWORD PTR [100+esp]                      ;
        add       esp, 24                                       ;267.22
        pxor      xmm0, xmm0                                    ;
                                ; LOE eax ebx esi edi bl bh xmm0
.B2.74:                         ; Preds .B2.129 .B2.72          ; Infreq
        mov       edx, -1                                       ;269.19
                                ; LOE eax edx ebx esi edi xmm0
.B2.75:                         ; Preds .B2.74 .B2.93           ; Infreq
        test      dl, 1                                         ;273.21
        jne       .B2.89        ; Prob 40%                      ;273.21
                                ; LOE eax ebx esi edi xmm0
.B2.76:                         ; Preds .B2.75                  ; Infreq
        mov       ecx, esi                                      ;274.40
        xor       edx, edx                                      ;274.12
        dec       ecx                                           ;274.40
        cmovs     ecx, edx                                      ;274.12
        mov       edx, DWORD PTR [80+esp]                       ;274.12
        cmp       ecx, edx                                      ;274.12
        cmovge    ecx, edx                                      ;274.12
        test      ecx, ecx                                      ;274.12
        jl        .B2.83        ; Prob 2%                       ;274.12
                                ; LOE eax ecx ebx esi edi xmm0
.B2.77:                         ; Preds .B2.76                  ; Infreq
        xor       edx, edx                                      ;
        mov       DWORD PTR [140+esp], ecx                      ;
        mov       DWORD PTR [76+esp], eax                       ;
        mov       DWORD PTR [84+esp], ebx                       ;
        mov       DWORD PTR [144+esp], esi                      ;
        mov       DWORD PTR [108+esp], edi                      ;
        mov       ecx, edx                                      ;
                                ; LOE edx ecx xmm0
.B2.78:                         ; Preds .B2.81 .B2.77           ; Infreq
        mov       eax, DWORD PTR [144+esp]                      ;275.27
        lea       ebx, DWORD PTR [eax+ecx]                      ;275.27
        test      ebx, ebx                                      ;275.27
        jle       .B2.81        ; Prob 16%                      ;275.27
                                ; LOE edx ecx ebx xmm0
.B2.79:                         ; Preds .B2.78                  ; Infreq
        mov       eax, 1                                        ;276.88
        cmovle    ebx, eax                                      ;276.88
        lea       eax, DWORD PTR [ebx+ebx*4]                    ;276.16
        mov       ebx, DWORD PTR [136+esp]                      ;276.16
        movsx     esi, WORD PTR [36+ebx+eax*8]                  ;276.16
        test      esi, esi                                      ;276.88
        jle       .B2.81        ; Prob 16%                      ;276.88
                                ; LOE eax edx ecx ebx bl bh xmm0
.B2.80:                         ; Preds .B2.79                  ; Infreq
        mov       edi, DWORD PTR [32+ebx+eax*8]                 ;277.17
        neg       edi                                           ;277.89
        add       edi, DWORD PTR [132+esp]                      ;277.89
        imul      edi, DWORD PTR [28+ebx+eax*8]                 ;277.89
        mov       esi, DWORD PTR [ebx+eax*8]                    ;277.17
        movsx     ebx, WORD PTR [esi+edi]                       ;277.17
        test      ebx, ebx                                      ;277.89
        jg        .B2.91        ; Prob 20%                      ;277.89
                                ; LOE eax edx ecx ebx xmm0
.B2.81:                         ; Preds .B2.80 .B2.79 .B2.78    ; Infreq
        inc       edx                                           ;287.20
        dec       ecx                                           ;287.20
        cmp       edx, DWORD PTR [140+esp]                      ;287.20
        jle       .B2.78        ; Prob 82%                      ;287.20
                                ; LOE edx ecx xmm0
.B2.82:                         ; Preds .B2.81                  ; Infreq
        mov       eax, DWORD PTR [76+esp]                       ;
        mov       ebx, DWORD PTR [84+esp]                       ;
        mov       esi, DWORD PTR [144+esp]                      ;
        mov       edi, DWORD PTR [108+esp]                      ;
                                ; LOE eax ebx esi edi xmm0
.B2.83:                         ; Preds .B2.82 .B2.76           ; Infreq
        mov       ecx, DWORD PTR [112+esp]                      ;290.155
        mov       edx, DWORD PTR [104+esp]                      ;290.155
        mov       DWORD PTR [84+esp], ebx                       ;
        mov       DWORD PTR [108+esp], edi                      ;
        imul      ebx, DWORD PTR [188+ecx+edx], -40             ;290.155
        cvtsi2ss  xmm1, DWORD PTR [8+ecx+edx]                   ;290.119
        add       ebx, DWORD PTR [156+ecx+edx]                  ;290.155
        mov       DWORD PTR [144+esp], esi                      ;
        mulss     xmm1, DWORD PTR [672+ecx+edx]                 ;290.118
        mov       edi, DWORD PTR [32+ebx+eax*8]                 ;290.22
        neg       edi                                           ;290.155
        add       edi, DWORD PTR [132+esp]                      ;290.155
        imul      edi, DWORD PTR [28+ebx+eax*8]                 ;290.155
        mov       esi, DWORD PTR [ebx+eax*8]                    ;290.22
        mov       ebx, DWORD PTR [84+esp]                       ;290.155
        movsx     eax, WORD PTR [esi+edi]                       ;290.22
        cvtsi2ss  xmm3, eax                                     ;290.22
        divss     xmm3, DWORD PTR [_2il0floatpacket.9]          ;290.76
        movaps    xmm2, xmm3                                    ;290.86
        divss     xmm2, xmm1                                    ;290.86
        mov       esi, DWORD PTR [144+esp]                      ;290.155
        mov       edi, DWORD PTR [108+esp]                      ;290.155
        comiss    xmm2, xmm0                                    ;290.155
        jbe       .B2.88        ; Prob 50%                      ;290.155
                                ; LOE ebx esi edi bl bh xmm0 xmm3
.B2.84:                         ; Preds .B2.83                  ; Infreq
        mulss     xmm3, DWORD PTR [_2il0floatpacket.11]         ;291.18
        mov       eax, DWORD PTR [100+esp]                      ;291.18
        comiss    xmm0, xmm3                                    ;292.51
        movss     DWORD PTR [eax+edi], xmm3                     ;291.18
        jbe       .B2.45        ; Prob 50%                      ;292.51
                                ; LOE ebx esi edi bl bh xmm0
.B2.85:                         ; Preds .B2.84                  ; Infreq
        mov       DWORD PTR [16+esp], 0                         ;293.22
        lea       eax, DWORD PTR [48+esp]                       ;293.22
        mov       DWORD PTR [48+esp], 23                        ;293.22
        mov       DWORD PTR [52+esp], OFFSET FLAT: __STRLITPACK_13 ;293.22
        push      32                                            ;293.22
        push      eax                                           ;293.22
        push      OFFSET FLAT: __STRLITPACK_18.0.2              ;293.22
        push      -2088435968                                   ;293.22
        push      -1                                            ;293.22
        lea       edx, DWORD PTR [36+esp]                       ;293.22
        push      edx                                           ;293.22
        call      _for_write_seq_lis                            ;293.22
                                ; LOE ebx esi edi bl bh
.B2.130:                        ; Preds .B2.85                  ; Infreq
        add       esp, 24                                       ;293.22
        pxor      xmm0, xmm0                                    ;
                                ; LOE ebx esi edi xmm0
.B2.86:                         ; Preds .B2.90 .B2.130          ; Infreq
        mov       DWORD PTR [16+esp], 0                         ;300.17
        lea       eax, DWORD PTR [esp]                          ;300.17
        mov       DWORD PTR [esp], 37                           ;300.17
        mov       DWORD PTR [4+esp], OFFSET FLAT: __STRLITPACK_11 ;300.17
        push      32                                            ;300.17
        push      eax                                           ;300.17
        push      OFFSET FLAT: __STRLITPACK_19.0.2              ;300.17
        push      -2088435968                                   ;300.17
        push      911                                           ;300.17
        lea       edx, DWORD PTR [36+esp]                       ;300.17
        push      edx                                           ;300.17
        call      _for_write_seq_lis                            ;300.17
                                ; LOE ebx esi edi
.B2.87:                         ; Preds .B2.86                  ; Infreq
        push      32                                            ;301.17
        xor       eax, eax                                      ;301.17
        push      eax                                           ;301.17
        push      eax                                           ;301.17
        push      -2088435968                                   ;301.17
        push      eax                                           ;301.17
        pxor      xmm0, xmm0                                    ;
        push      OFFSET FLAT: __STRLITPACK_20                  ;301.17
        call      _for_stop_core                                ;301.17
                                ; LOE ebx esi edi
.B2.131:                        ; Preds .B2.87                  ; Infreq
        add       esp, 48                                       ;301.17
        pxor      xmm0, xmm0                                    ;
        jmp       .B2.45        ; Prob 100%                     ;
                                ; LOE ebx esi edi xmm0
.B2.88:                         ; Preds .B2.83                  ; Infreq
        mov       eax, DWORD PTR [100+esp]                      ;296.19
        movss     xmm1, DWORD PTR [_2il0floatpacket.10]         ;296.19
        mov       DWORD PTR [eax+edi], 1028443341               ;296.19
        jmp       .B2.90        ; Prob 100%                     ;296.19
                                ; LOE ebx esi edi xmm0 xmm1
.B2.89:                         ; Preds .B2.132 .B2.91 .B2.75   ; Infreq
        mov       eax, DWORD PTR [100+esp]                      ;299.16
        movss     xmm1, DWORD PTR [eax+edi]                     ;299.16
                                ; LOE ebx esi edi xmm0 xmm1
.B2.90:                         ; Preds .B2.88 .B2.89           ; Infreq
        comiss    xmm0, xmm1                                    ;299.45
        ja        .B2.86        ; Prob 5%                       ;299.45
        jmp       .B2.45        ; Prob 100%                     ;299.45
                                ; LOE ebx esi edi xmm0
.B2.91:                         ; Preds .B2.80                  ; Infreq
        mov       edx, DWORD PTR [112+esp]                      ;278.18
        mov       ecx, DWORD PTR [104+esp]                      ;278.18
        mov       DWORD PTR [72+esp], eax                       ;
        mov       DWORD PTR [68+esp], ebx                       ;
        imul      eax, DWORD PTR [80+edx+ecx], -40              ;278.18
        add       eax, DWORD PTR [48+edx+ecx]                   ;278.18
        mov       edx, DWORD PTR [72+esp]                       ;278.50
        mov       edi, DWORD PTR [108+esp]                      ;
        mov       ebx, DWORD PTR [84+esp]                       ;
        mov       ecx, DWORD PTR [32+eax+edx*8]                 ;278.50
        neg       ecx                                           ;278.18
        add       ecx, DWORD PTR [96+esp]                       ;278.18
        imul      ecx, DWORD PTR [28+eax+edx*8]                 ;278.18
        mov       eax, DWORD PTR [eax+edx*8]                    ;278.50
        mov       esi, DWORD PTR [144+esp]                      ;
        mov       eax, DWORD PTR [eax+ecx]                      ;278.50
        cdq                                                     ;278.50
        mov       ecx, DWORD PTR [68+esp]                       ;278.50
        idiv      ecx                                           ;278.50
        cvtsi2ss  xmm1, eax                                     ;278.18
        mov       edx, DWORD PTR [100+esp]                      ;278.18
        comiss    xmm0, xmm1                                    ;280.56
        movss     DWORD PTR [edx+edi], xmm1                     ;278.18
        jbe       .B2.89        ; Prob 50%                      ;280.56
                                ; LOE ebx esi edi bl bh xmm0
.B2.92:                         ; Preds .B2.91                  ; Infreq
        mov       DWORD PTR [16+esp], 0                         ;281.26
        lea       edx, DWORD PTR [16+esp]                       ;281.26
        mov       DWORD PTR [8+esp], 23                         ;281.26
        lea       eax, DWORD PTR [8+esp]                        ;281.26
        mov       DWORD PTR [12+esp], OFFSET FLAT: __STRLITPACK_14 ;281.26
        push      32                                            ;281.26
        push      eax                                           ;281.26
        push      OFFSET FLAT: __STRLITPACK_17.0.2              ;281.26
        push      -2088435968                                   ;281.26
        push      -1                                            ;281.26
        push      edx                                           ;281.26
        call      _for_write_seq_lis                            ;281.26
                                ; LOE ebx esi edi bl bh
.B2.132:                        ; Preds .B2.92                  ; Infreq
        add       esp, 24                                       ;281.26
        pxor      xmm0, xmm0                                    ;
        jmp       .B2.89        ; Prob 100%                     ;
                                ; LOE ebx esi edi xmm0
.B2.93:                         ; Preds .B2.70 .B2.71           ; Infreq
        xor       edx, edx                                      ;
        jmp       .B2.75        ; Prob 100%                     ;
                                ; LOE eax edx ebx esi edi xmm0
.B2.95:                         ; Preds .B2.53                  ; Infreq
        cmp       DWORD PTR [32+esp], 4                         ;214.5
        jl        .B2.112       ; Prob 10%                      ;214.5
                                ; LOE ebx esi edi
.B2.96:                         ; Preds .B2.95                  ; Infreq
        mov       eax, DWORD PTR [28+esp]                       ;214.5
        add       eax, ebx                                      ;214.5
        and       eax, 15                                       ;214.5
        je        .B2.99        ; Prob 50%                      ;214.5
                                ; LOE eax ebx esi edi
.B2.97:                         ; Preds .B2.96                  ; Infreq
        test      al, 3                                         ;214.5
        jne       .B2.112       ; Prob 10%                      ;214.5
                                ; LOE eax ebx esi edi
.B2.98:                         ; Preds .B2.97                  ; Infreq
        neg       eax                                           ;214.5
        add       eax, 16                                       ;214.5
        shr       eax, 2                                        ;214.5
                                ; LOE eax ebx esi edi
.B2.99:                         ; Preds .B2.98 .B2.96           ; Infreq
        lea       edx, DWORD PTR [4+eax]                        ;214.5
        cmp       edx, DWORD PTR [32+esp]                       ;214.5
        jg        .B2.112       ; Prob 10%                      ;214.5
                                ; LOE eax ebx esi edi
.B2.100:                        ; Preds .B2.99                  ; Infreq
        mov       edx, DWORD PTR [32+esp]                       ;214.5
        mov       ecx, edx                                      ;214.5
        sub       ecx, eax                                      ;214.5
        and       ecx, 3                                        ;214.5
        neg       ecx                                           ;214.5
        add       ecx, edx                                      ;214.5
        mov       edx, DWORD PTR [28+esp]                       ;
        test      eax, eax                                      ;214.5
        mov       DWORD PTR [esp], ecx                          ;214.5
        lea       ecx, DWORD PTR [edx+ebx]                      ;
        mov       DWORD PTR [4+esp], ecx                        ;
        jbe       .B2.104       ; Prob 10%                      ;214.5
                                ; LOE eax ecx ebx esi edi cl ch
.B2.101:                        ; Preds .B2.100                 ; Infreq
        xor       edx, edx                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B2.102:                        ; Preds .B2.102 .B2.101         ; Infreq
        mov       DWORD PTR [ecx+edx*4], 0                      ;214.5
        inc       edx                                           ;214.5
        cmp       edx, eax                                      ;214.5
        jb        .B2.102       ; Prob 82%                      ;214.5
                                ; LOE eax edx ecx ebx esi edi
.B2.104:                        ; Preds .B2.102 .B2.100         ; Infreq
        mov       edx, DWORD PTR [esp]                          ;
        mov       ecx, DWORD PTR [4+esp]                        ;
        pxor      xmm0, xmm0                                    ;
                                ; LOE eax edx ecx ebx esi edi xmm0
.B2.105:                        ; Preds .B2.105 .B2.104         ; Infreq
        movdqa    XMMWORD PTR [ecx+eax*4], xmm0                 ;214.5
        add       eax, 4                                        ;214.5
        cmp       eax, edx                                      ;214.5
        jb        .B2.105       ; Prob 82%                      ;214.5
                                ; LOE eax edx ecx ebx esi edi xmm0
.B2.106:                        ; Preds .B2.105                 ; Infreq
        mov       DWORD PTR [esp], edx                          ;
                                ; LOE ebx esi edi
.B2.107:                        ; Preds .B2.106 .B2.112         ; Infreq
        mov       eax, DWORD PTR [esp]                          ;214.5
        cmp       eax, DWORD PTR [32+esp]                       ;214.5
        jae       .B2.113       ; Prob 10%                      ;214.5
                                ; LOE eax ebx esi edi al ah
.B2.108:                        ; Preds .B2.107                 ; Infreq
        mov       edx, DWORD PTR [32+esp]                       ;
                                ; LOE eax edx ebx esi edi
.B2.109:                        ; Preds .B2.109 .B2.108         ; Infreq
        mov       DWORD PTR [edi+eax*4], 0                      ;214.5
        inc       eax                                           ;214.5
        cmp       eax, edx                                      ;214.5
        jb        .B2.109       ; Prob 82%                      ;214.5
                                ; LOE eax edx ebx esi edi
.B2.110:                        ; Preds .B2.109                 ; Infreq
        mov       edx, DWORD PTR [20+esp]                       ;214.5
        inc       edx                                           ;214.5
        mov       eax, DWORD PTR [12+esp]                       ;214.5
        add       edi, eax                                      ;214.5
        add       esi, eax                                      ;214.5
        add       ebx, eax                                      ;214.5
        mov       DWORD PTR [20+esp], edx                       ;214.5
        cmp       edx, DWORD PTR [16+esp]                       ;214.5
        jb        .B2.53        ; Prob 82%                      ;214.5
                                ; LOE ebx esi edi
.B2.111:                        ; Preds .B2.110                 ; Infreq
        mov       edx, DWORD PTR [8+esp]                        ;
        pxor      xmm3, xmm3                                    ;
        jmp       .B2.27        ; Prob 100%                     ;
                                ; LOE edx xmm3
.B2.112:                        ; Preds .B2.95 .B2.99 .B2.97    ; Infreq
        xor       eax, eax                                      ;214.5
        mov       DWORD PTR [esp], eax                          ;214.5
        jmp       .B2.107       ; Prob 100%                     ;214.5
                                ; LOE ebx esi edi
.B2.113:                        ; Preds .B2.107                 ; Infreq
        mov       edx, DWORD PTR [20+esp]                       ;214.5
        inc       edx                                           ;214.5
        mov       eax, DWORD PTR [12+esp]                       ;214.5
        add       edi, eax                                      ;214.5
        add       esi, eax                                      ;214.5
        add       ebx, eax                                      ;214.5
        mov       DWORD PTR [20+esp], edx                       ;214.5
        cmp       edx, DWORD PTR [16+esp]                       ;214.5
        jb        .B2.53        ; Prob 82%                      ;214.5
                                ; LOE ebx esi edi
.B2.114:                        ; Preds .B2.113                 ; Infreq
        mov       edx, DWORD PTR [8+esp]                        ;
        pxor      xmm3, xmm3                                    ;
        jmp       .B2.27        ; Prob 100%                     ;
                                ; LOE edx xmm3
.B2.117:                        ; Preds .B2.6 .B2.10 .B2.8      ; Infreq
        xor       edx, edx                                      ;213.5
        mov       DWORD PTR [52+esp], edx                       ;213.5
        jmp       .B2.18        ; Prob 100%                     ;213.5
                                ; LOE eax ecx xmm0 xmm3
.B2.118:                        ; Preds .B2.18                  ; Infreq
        inc       ecx                                           ;213.5
        cmp       ecx, DWORD PTR [36+esp]                       ;213.5
        jb        .B2.6         ; Prob 82%                      ;213.5
        jmp       .B2.22        ; Prob 100%                     ;213.5
                                ; LOE eax ecx xmm0 xmm3
.B2.122:                        ; Preds .B2.4                   ; Infreq
        DB        15                                            ;
        DB        31                                            ;
        DB        0                                             ;
        mov       ecx, DWORD PTR [8+esp]                        ;
        mov       ebx, DWORD PTR [36+esp]                       ;
                                ; LOE eax edx ecx ebx xmm0 xmm3
.B2.123:                        ; Preds .B2.123 .B2.122         ; Infreq
        inc       ecx                                           ;213.5
        cmp       ecx, ebx                                      ;213.5
        jb        .B2.123       ; Prob 82%                      ;213.5
        jmp       .B2.23        ; Prob 100%                     ;213.5
        ALIGN     16
                                ; LOE eax edx ecx ebx xmm0 xmm3
; mark_end;
_CAL_PENALTY_MIVA ENDP
_TEXT	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_16.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_17.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_18.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_19.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _CAL_PENALTY_MIVA
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_5	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	115
	DB	109
	DB	112
	DB	115
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_4	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	113
	DB	109
	DB	112
	DB	115
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_2	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	116
	DB	114
	DB	97
	DB	118
	DB	101
	DB	108
	DB	32
	DB	116
	DB	105
	DB	109
	DB	101
	DB	32
	DB	105
	DB	110
	DB	32
	DB	112
	DB	101
	DB	110
	DB	97
	DB	108
	DB	116
	DB	121
	DB	32
	DB	99
	DB	97
	DB	108
	DB	0
__STRLITPACK_0	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	112
	DB	101
	DB	110
	DB	97
	DB	108
	DB	116
	DB	121
	DB	32
	DB	105
	DB	110
	DB	32
	DB	112
	DB	101
	DB	110
	DB	97
	DB	108
	DB	116
	DB	121
	DB	32
	DB	99
	DB	97
	DB	108
	DB	0
__STRLITPACK_10	DB	0
	DB 3 DUP ( 0H)	; pad
_2il0floatpacket.1	DD	042c80000H
_2il0floatpacket.2	DD	0b727c5acH
_2il0floatpacket.3	DD	03f733333H
_2il0floatpacket.4	DD	03d4ccccdH
_2il0floatpacket.5	DD	03f000000H
_2il0floatpacket.6	DD	080000000H
_2il0floatpacket.7	DD	04b000000H
_2il0floatpacket.8	DD	0bf000000H
__STRLITPACK_15	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	116
	DB	114
	DB	97
	DB	118
	DB	101
	DB	108
	DB	32
	DB	112
	DB	101
	DB	110
	DB	97
	DB	108
	DB	116
	DB	121
	DB	0
__STRLITPACK_14	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	116
	DB	114
	DB	97
	DB	118
	DB	101
	DB	108
	DB	32
	DB	112
	DB	101
	DB	110
	DB	97
	DB	108
	DB	116
	DB	121
	DB	0
__STRLITPACK_13	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	116
	DB	114
	DB	97
	DB	118
	DB	101
	DB	108
	DB	32
	DB	112
	DB	101
	DB	110
	DB	97
	DB	108
	DB	116
	DB	121
	DB	0
__STRLITPACK_11	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	116
	DB	114
	DB	97
	DB	118
	DB	101
	DB	108
	DB	112
	DB	101
	DB	110
	DB	97
	DB	108
	DB	116
	DB	121
	DB	32
	DB	105
	DB	110
	DB	32
	DB	112
	DB	101
	DB	110
	DB	97
	DB	108
	DB	116
	DB	121
	DB	32
	DB	99
	DB	97
	DB	108
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_20	DB	0
	DB 3 DUP ( 0H)	; pad
_2il0floatpacket.9	DD	042c80000H
_2il0floatpacket.10	DD	03d4ccccdH
_2il0floatpacket.11	DD	03f000000H
_2il0floatpacket.12	DD	080000000H
_2il0floatpacket.13	DD	04b000000H
_2il0floatpacket.14	DD	0bf000000H
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_PENFORPREVENTMOVE:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TRAVELTIME:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TDSPSTEP:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_SIMPERAGG:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NOOFARCS:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_AGGINT:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TRAVELPENALTY:BYTE
_DATA	ENDS
EXTRN	_for_stop_core:PROC
EXTRN	_for_write_seq_lis:PROC
EXTRN	_DYNUST_NETWORK_MODULE_mp_GETBSTMOVE:PROC
EXTRN	__intel_fast_memset:PROC
EXTRN	__alloca_probe:PROC
EXTRN	__fltused:BYTE
	INCLUDELIB <ifconsol>
	INCLUDELIB <libifcoremt>
	INCLUDELIB <libifport>
	INCLUDELIB <libmmt>
	INCLUDELIB <LIBCMT>
	INCLUDELIB <libirc>
	INCLUDELIB <svml_dispmt>
	INCLUDELIB <OLDNAMES>
	END
