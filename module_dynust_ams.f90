Module DYNUST_AMS_MODULE
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~ Copyright (C) 2002-2011 The Arizona Board of Regents on Behalf of The University of Arizona.  ~~!
!~~ Developed by Yi-Chang Chiu, Ph.D. as par to the DynusT project.                               ~~!
!~~ This program is free software: you can redistribute it and/or modify it                       ~~!
!~~ under the terms of the GNU General Public License version 2 as published by                   ~~!
!~~ the Free Software Foundation.                                                                 ~~!
!~~ This program is distributed in the hope that it will be useful,                               ~~!
!~~ but WITHOUT ANY WARRANTY; without even the implied warranty of                                ~~!
!~~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                                          ~~!
!~~ See the GNU General Public License for more details.                                          ~~!
!~~ You should have received a copy of the GNU General Public License                             ~~!
!~~ along with this program (in the file COPYING).                                                ~~!
!~~ IF not, see <http://www.gnu.org/licenses/>.                                                   ~~!
!~~ Please see http://www.dynust.net for contact information and names                            ~~!
!~~ of individual contributors.                                                                   ~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!

INTEGER, ALLOCATABLE::SIR(:) ! SIR(link_iden(i)) length for different facilities
LOGICAL::SIRExist=.false.

CONTAINS

SUBROUTINE FLOW_MODEL_UPDATE_AMTS
  USE DYNUST_MAIN_MODULE
  USE DYNUST_VEH_MODULE
  USE DYNUST_NETWORK_MODULE
  USE DYNUST_LINK_VEH_LIST_MODULE
  USE DYNUST_VEH_PATH_ATT_MODULE
  USE DYNUST_TRANSLINK_MODULE
  !USE DYNUST_MOVES_MODULE
  
  REAL tlength,tmp1
  INTEGER kj2, NNlk,I
  INTEGER LinkCOU, LinkCOD
  INTEGER VehID,VehIDNext
  INTEGER numvehlink      !the number of vehicles in a link
  REAL avervehlink(1:noofarcs)        !the average speed in a link
  INTEGER tmpvehID1
  INTEGER NlnkMv, NlnkMv2,iform
  INTEGER tmplinkID,tmpcounter1, Ranlane,movnumtmp,intmp,tmpupcount1,ICU
  REAL AdjSpeed, VSPValue
  
  INTEGER,ALLOCATABLE::icount3(:,:)
  
!> > > > > > > > > >                < < < < < < < < < < <
!>>>>>>>>>>>>>>>>>>>CODES START HERE<<<<<<<<<<<<<<<<<<<<<
!> > > > > > > > > >                < < < < < < < < < < <
  ALLOCATE(icount3(noofarcs,nu_mv))
  icount3(:,:) = 0
  
  movnumtmp=1
  m_dynust_network_arc_de(:)%vehicle_queue = 0
  avervehlink(:)=0
  
  
!$OMP PARALLEL PRIVATE(VSPValue,ICU,AdjSpeed,nmov,tmpvehID1,iform,nnlk,i,IH,IQ,kj,kj2,numvehlink,icount,icount2,pce1,pce2,VehID,SirEnd,VehIDNext,densityentry,NlnkMv,Nlnk,SirEndNext,capacityentry) NUM_THREADS(nThread)
!$OMP DO SCHEDULE(DYNAMIC)
DO I = 1, noofarcs
      IH = m_dynust_network_arc_de(i)%FlowModelnum
      IQ = VKFunc(IH)%regime
      kj = 0
      kj2= 0
	  numvehlink=0
	  icount = 1 ! how many vehicles has been scanned so far by sirend
      icount2 = 0		     
	  pce1 = 0
	  pce2 = 0

     IF(LinkVehList(i)%fid > 0) THEN
     DO tmpvehID1 = LinkVehList(i)%fid,1,-1
         IF(tmpVehID1 /= LinkVehList(i)%fid) pce1 = max(0.0,pce1 - m_dynust_veh(LinkVehList(i)%P(tmpvehID1+1))%vhpce/AttScale)
		 VehID=LinkVehList(i)%P(tmpvehID1)
         NMOV = m_dynust_veh(VehID)%nexlink(2) ! WHAT TURN TO MAKE AT THE INTERSECTION
!        UPDATE TRUCK PCE BASED ON THE TRUCK PCE UPDATES IN TRANSFER VEHICLE SUBROUTINE		 
		 IF((m_dynust_last_stand(VehID)%vehtype==2).or.(m_dynust_last_stand(VehID)%vehtype==6).or.(m_dynust_last_stand(VehID)%vehtype==7)) m_dynust_veh(VehID)%vhpce = m_dynust_network_arc_de(i)%DynPCE*AttScale
         IF(icount < (LinkVehList(i)%fid-tmpvehID1)+1) icount = (LinkVehList(i)%fid-tmpvehID1)+1
           IF(tmpvehID1 > 1) THEN ! THIS LINK HAS MORE THAN 1 VEHICLE AHEAD OF VEH OF INTEREST
             SirEnd = m_dynust_veh(VehID)%position-SIR(linkidmap(m_dynust_network_arc_nde(i)%link_iden))/5280.0
             
			 IF(LinkVehList(i)%fid > icount) THEN
			   VehIDNext = LinkVehList(i)%P(LinkVehList(i)%fid-icount)
!               NEXTMOV = m_dynust_veh(VehIDNext)%nexlink(2) !THIS IS THE UNIQUE AND COMPUTATIONALLY EFFECTIVE WAY OF DEALING WITH MULTI-LANE MOVEMENTS AND SPILLBACK AT INTERSECTION OR OFF RAMP
		       DO while (m_dynust_veh(VehIDNext)%position >= SirEND.and.((LinkVehList(i)%fid-icount) > 0))
	 		     VehIDNext = LinkVehList(i)%P(LinkVehList(i)%fid-icount)
                 IF(VehIDNext /= LinkVehList(i)%P(tmpvehID1)) pce1 = pce1 + m_dynust_veh(VehIDNext)%vhpce/AttScale
                 icount = icount + 1
 		       ENDDO
			 ENDIF
	       ELSE
		     pce1 = 0
		   ENDIF 

         ! SIR(link_iden(i)) is within the current link or the downstream node is destination
         IF(m_dynust_veh(VehID)%position >= SIR(linkidmap(m_dynust_network_arc_nde(i)%link_iden))/5280.0.OR.(m_dynust_veh_nde(VehID)%icurrnt >= vehatt_P_Size(VehID)-1)) THEN 
         
           IF(m_dynust_network_arc_de(i)%xl < 0.0001) THEN
		     densityentry = VKFunc(IH)%Kjam2
		   ELSE
		     densityentry = min(VKFunc(IH)%Kjam2*1.0,pce1/(((SIR(linkidmap(m_dynust_network_arc_nde(i)%link_iden))/5280.0/m_dynust_network_arc_nde(i)%s)*m_dynust_network_arc_de(i)%xl)))
           ENDIF
           
           IF(m_dynust_network_arc_de(i)%ShortLink) THEN 
              densityentry = min(densityentry,VKFunc(IH)%Kjam2/2.0) ! IF THIS LINK IS A SHORT LINK, THEN LIMIT THE DENSITYENTRY
           ENDIF

		   IF(densityentry < 0) THEN
              print *, 'error in density'
		   ENDIF

           IH = m_dynust_network_arc_de(i)%FlowModelnum
           IQ = VKFunc(IH)%regime
           IForm = VKFunc(IH)%form
	       CALL VehSpeed_update(i,IH,IQ,IForm,densityentry,AdjSpeed)
	       IF(MOVESFlag > 0.AND.m_dynust_network_arc_de(i)%isMovesLink.AND.(reach_converg.OR.iteration == iteMax)) THEN
		        m_dynust_veh(VehID)%VehSpeed_old = m_dynust_veh(VehID)%VehSpeed
		        m_dynust_veh(VehID)%VehSpeed     = AdjSpeed
		        m_dynust_veh(VehID)%accel        =(AdjSpeed-m_dynust_veh(VehID)%VehSpeed_old)/xminPerSimInt
	       ELSE
	            m_dynust_veh(VehID)%VehSpeed     = AdjSpeed
	       ENDIF 
		 
		 ELSE ! SIR(linkidmap(link_iden(i))) across multiple links

          
           IF(m_dynust_veh(VehID)%nexlink(1) < 1) THEN 
              ICU =  m_dynust_veh_nde(VehID)%icurrnt
              CALL RETRIEVE_NEXT_LINK(0.0,VehID,i,ICU,Nlnk,NlnkMv)
           ENDIF
           NNLK = m_dynust_veh(VehID)%nexlink(1)
           NMOV = m_dynust_veh(VehID)%nexlink(2)

           IF(m_dynust_network_arcmove_de(i,NMOV)%green < 1) THEN ! this movement is in red light and vehicle is farer than the speed times 6 sec - to account for dilemma zone
            capacityentry = (m_dynust_veh(VehID)%position/m_dynust_network_arc_nde(i)%s)*m_dynust_network_arc_de(i)%xl
          
           ELSE ! this movement has green, calculate the pce2 in the downstream link
             IF(LinkVehList(nnlk)%fid > 0.and.LinkVehList(nnlk)%fid > icount3(i,NMOV)) THEN 
               SirEndNext = max(0.0, m_dynust_network_arc_nde(nnlk)%s - ((SIR(linkidmap(m_dynust_network_arc_nde(i)%link_iden))/5280.0)-m_dynust_veh(VehID)%position)) ! position of the SIRend in the downstream link
	           VehIDNext = LinkVehList(nnlk)%P(LinkVehList(nnlk)%fid-icount3(i,NMOV))
			   DO while (m_dynust_veh(VehIDNext)%position >= SirENDNext.and.((LinkVehList(nnlk)%fid-icount3(i,NMOV)) > 0)) 
			     VehIDNext = LinkVehList(nnlk)%P(LinkVehList(nnlk)%fid-icount3(i,NMOV))
                 pce2 = pce2 + m_dynust_veh(VehIDNext)%vhpce/AttScale
                 icount3(i,NMOV) = icount3(i,NMOV) + 1
		       ENDDO
             ELSE ! no vehicle in next link
               icount3(i,NMOV) = 0
		 	   pce2 = 0
		     ENDIF
!!!uytr             capacityentry = (m_dynust_veh(VehID)%position/m_dynust_network_arc_nde(i)%s)*m_dynust_network_arc_de(i)%xl+((SIR(linkidmap(m_dynust_network_arc_nde(i)%link_iden))/5280.0-m_dynust_veh(VehID)%position)/m_dynust_network_arc_nde(NNlk)%s)*m_dynust_network_arc_de(NNlk)%xl
!            IF(SIR(linkidmap(m_dynust_network_arc_nde(i)%link_iden))/5280.0 < (m_dynust_network_arc_nde(i)%s)+m_dynust_network_arc_nde(nnlk)%s))) THEN ! SIREND DOES NOT EXTEND BEYOND NEXT LINK
            ! SIREND DOES NOT EXTEND BEYOND NEXT LINK
             IF(SIR(linkidmap(m_dynust_network_arc_nde(i)%link_iden))/5280.0 < m_dynust_veh(VehID)%position+m_dynust_network_arc_nde(nnlk)%s) THEN 
               capacityentry = (m_dynust_veh(VehID)%position/m_dynust_network_arc_nde(i)%s)*m_dynust_network_arc_de(i)%xl&
               +((SIR(linkidmap(m_dynust_network_arc_nde(i)%link_iden))/5280.0-m_dynust_veh(VehID)%position)/m_dynust_network_arc_nde(NNlk)%s)*m_dynust_network_arc_de(NNlk)%xl
             ELSE !SIR END EXTENDS BEYOND NEXT LINK DUE TO SHORT LINK
               capacityentry = (m_dynust_veh(VehID)%position/m_dynust_network_arc_nde(i)%s)*m_dynust_network_arc_de(i)%xl&
!             +((SIR(linkidmap(m_dynust_network_arc_nde(i)%link_iden))/5280.0-m_dynust_veh(VehID)%position)/m_dynust_network_arc_nde(NNlk)%s)*m_dynust_network_arc_de(NNlk)%xl
              +(SIR(linkidmap(m_dynust_network_arc_nde(i)%link_iden))/5280.0-m_dynust_veh(VehID)%position)*m_dynust_network_arc_de(NNlk)%xl/m_dynust_network_arc_nde(NNlk)%s
             ENDIF
           ENDIF
           ! IF NEXT LINK IS SHORT LINK, THEN USE MAX HALF OF THE JAM DENSITY TO SCALE PCE2
           IF(m_dynust_network_arc_de(NNlk)%ShortLink) THEN
              pce2 = min(pce2,VKFunc(IH)%Kjam2/2.0*m_dynust_network_arc_nde(NNlk)%s)
           ENDIF
		   IF(capacityentry < 0.0001) THEN
		     densityentry = VKFunc(IH)%Kjam2
		   ELSE
		     densityentry = min(VKFunc(IH)%Kjam2*1.0,(pce1+pce2)/capacityentry)
		   ENDIF
           IF(m_dynust_network_arc_de(i)%ShortLink) THEN 
              densityentry = min(densityentry,VKFunc(IH)%Kjam2/2.0) ! IF THIS LINK IS A SHORT LINK, THEN LIMIT THE DENSITYENTRY
           ENDIF

		   IF(densityentry < 0) THEN
              print *, 'error in density'
		   ENDIF
!!$OMP CRITICAL	
           IH = m_dynust_network_arc_de(i)%FlowModelnum
           IQ = VKFunc(IH)%regime
           IForm = VKFunc(IH)%form
	       CALL VehSpeed_update(i,IH,IQ,IForm,densityentry,AdjSpeed)
		   m_dynust_veh(VehID)%VehSpeed = AdjSpeed
!!$OMP END CRITICAL		   
         ENDIF

        IF(m_dynust_veh(VehID)%VehSpeed <= VKFunc(m_dynust_network_arc_de(i)%FlowModelnum)%V02) THEN
		   m_dynust_network_arc_de(i)%vehicle_queue=m_dynust_network_arc_de(i)%vehicle_queue + 1 !10 mph
        ENDIF
		numvehlink=numvehlink+1
        avervehlink(i)=avervehlink(i)+m_dynust_veh(VehID)%VehSpeed

!       Calculate Vehicle Specific Power (VSP) for MOVES        
!        IF(MOVESFlag > 0.AND.m_dynust_network_arc_de(i)%isMovesLink > 0.AND.(reach_converg.OR.iteration == iteMax)) THEN 
!            CALL VSPCalculation(VehID,VSPValue,i)
!            IF(MovesMode == 1) THEN
!                CALL OpModeCalculation_CO2(VehID,VSPValue,m_dynust_veh(VehID)%VehSpeed,m_dynust_network_arc_de(i)%isMovesLink)
!            ELSE
!                CALL OpModeCalculation_CriteriaPollutant(VehID,VSPValue,m_dynust_veh(VehID)%VehSpeed,m_dynust_network_arc_de(i)%isMovesLink)            
!            ENDIF
!        ENDIF
      ENDDO !tmpvehID1 = LinkVehList(i)%fid,1,-1
     ENDIF ! LinkVehList(i)%fid > 0

     IF (numvehlink < 1) THEN
        avervehlink(i)=(m_dynust_network_arc_de(i)%SpeedLimit+m_dynust_network_arc_de(i)%Vfadjust)/60.0
	 ELSE
        avervehlink(i)=avervehlink(i)/numvehlink*1.0
	 ENDIF		
	 m_dynust_network_arc_de(i)%v = avervehlink(i)
     IF(m_dynust_network_arc_de(i)%xl < 0.0001) THEN
	   m_dynust_network_arc_de(i)%c = VKFunc(IH)%Kjam2
	 ELSE
	   m_dynust_network_arc_de(i)%c = numvehlink/m_dynust_network_arc_de(i)%xl
	 ENDIF
ENDDO
!$OMP END DO
!$OMP END PARALLEL

DEALLOCATE(icount3)

END SUBROUTINE


!~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>
SUBROUTINE VehSpeed_update(i,IH,IQ,IForm,vehnum,adjSpeed)
USE DYNUST_MAIN_MODULE
USE DYNUST_VEH_MODULE
USE DYNUST_NETWORK_MODULE

INTEGER,INTENT(IN):: i,IH,IQ,IForm
REAL,INTENT(IN):: vehnum
REAL Localdensity
REAL,INTENT(INOUT):: adjSpeed
!> > > > > > > > > >                < < < < < < < < < < <
!>>>>>>>>>>>>>>>>>>>CODES START HERE<<<<<<<<<<<<<<<<<<<<<
!> > > > > > > > > >                < < < < < < < < < < <

Localdensity = vehnum
FreeSpeed = (m_dynust_network_arc_de(i)%SpeedLimit+m_dynust_network_arc_de(i)%Vfadjust)/60.0  
    IF(IForm == 1) THEN ! WITH ONLY ALPHA TERM
         IF(IQ == 1) THEN ! TWO REGIME
            IF(Localdensity <= VKFunc(IH)%KCut) THEN !FREE-FLOW REGIME
		      AdjSpeed = FreeSpeed
			ELSEIF((Localdensity > VKFunc(IH)%KCut).and.(Localdensity < VKFunc(IH)%Kjam2)) THEN ! Modified GreenShield Regime
              Vf2=(((m_dynust_network_arc_de(i)%SpeedLimit+m_dynust_network_arc_de(i)%Vfadjust)-VKFunc(IH)%V02)/(max(0.0,(1.0-(VKFunc(IH)%Kcut/float(VKFunc(IH)%Kjam2))))**VKFunc(IH)%alpha2))+VKFunc(IH)%V02
              AdjSpeed = min(FreeSpeed ,(Vf2-VKFunc(IH)%V02)/60.0*(max(0.0,(1-min(Localdensity,float(VKFunc(IH)%Kjam2))/VKFunc(IH)%Kjam2)))**VKFunc(IH)%alpha2 + VKFunc(IH)%V02/60.0)
              IF(AdjSpeed < 0) THEN
                WRITE(911,*) "ERROR IN GETTING VEHICLE SPEED IN AMS"
                WRITE(911,*) "AdjSpeed", AdjSpeed
                STOP
              ENDIF
            ELSE ! BEYOND JAM DENSITY
		      AdjSpeed = VKFunc(IH)%V02/60.0
			ENDIF
         ELSE ! SINGLE REGIME
            AdjSpeed = min(FreeSpeed ,(m_dynust_network_arc_de(i)%SpeedLimit+m_dynust_network_arc_de(i)%Vfadjust-VKFunc(IH)%V02)/60.0*(max(0.0,(1-min(Localdensity,float(VKFunc(IH)%Kjam2))/VKFunc(IH)%Kjam2)))**VKFunc(IH)%alpha2+VKFunc(IH)%V02/60.0)
            IF(AdjSpeed < 0) THEN
              WRITE(911,*) "ERROR IN GETTING VEHICLE SPEED IN AMS"
              WRITE(911,*) "AdjSpeed", AdjSpeed
              STOP
            ENDIF
		 ENDIF
    ELSEIF(IForm == 2) THEN ! WITH ALPHA AND BETA
         IF(IQ == 1) THEN ! TWO REGIME MODEL
            IF(Localdensity <= VKFunc(IH)%KCut) THEN !FREE-FLOW REGIME
		      AdjSpeed = FreeSpeed
			ELSEIF((Localdensity > VKFunc(IH)%KCut).and.(Localdensity <= VKFunc(IH)%Kjam2)) THEN ! SPEED REDUCTION REGIME

              PowerTerm = (1-(VKFunc(IH)%KCut/float(VKFunc(IH)%Kjam2))**VKFunc(IH)%beta2)**VKFunc(IH)%alpha2
              Vf2 = (((m_dynust_network_arc_de(i)%SpeedLimit+m_dynust_network_arc_de(i)%Vfadjust)-VKFunc(IH)%V02)/PowerTerm + VKFunc(IH)%V02)
              AdjSpeed = min(FreeSpeed,((Vf2-VKFunc(IH)%V02)*((1-(min(Localdensity,float(VKFunc(IH)%Kjam2))/VKFunc(IH)%Kjam2)**VKFunc(IH)%beta2)**VKFunc(IH)%alpha2) + VKFunc(IH)%V02)/60.0)
              IF(AdjSpeed < 0) THEN
                WRITE(911,*) "ERROR IN GETTING VEHICLE SPEED IN AMS"
                WRITE(911,*) "AdjSpeed", AdjSpeed
                STOP
              ENDIF
            ELSE ! BEYOND JAM DENSITY
		      AdjSpeed = VKFunc(IH)%V02/60.0
			ENDIF
         ELSE ! SINGLE REGIME
            AdjSpeed = min(FreeSpeed,((FreeSpeed-VKFunc(IH)%V02)*((1-(min(Localdensity,float(VKFunc(IH)%Kjam2))/VKFunc(IH)%Kjam2)**VKFunc(IH)%beta2)**VKFunc(IH)%alpha2) + VKFunc(IH)%V02)/60.0)
            IF(AdjSpeed < 0) THEN
              WRITE(911,*) "ERROR IN GETTING VEHICLE SPEED IN AMS"
              WRITE(911,*) "AdjSpeed", AdjSpeed
              STOP
            ENDIF
		 ENDIF
    ENDIF
END SUBROUTINE

END MODULE