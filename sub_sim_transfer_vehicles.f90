      SUBROUTINE SIM_TRANSFER_VEHICLES(l,t,tend,edtime)
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~ Copyright (C) 2002-2011 The Arizona Board of Regents on Behalf of The University of Arizona.  ~~!
!~~ Developed by Yi-Chang Chiu, Ph.D. as par to the DynusT project.                               ~~!
!~~ This program is free software: you can redistribute it and/or modify it                       ~~!
!~~ under the terms of the GNU General Public License version 2 as published by                   ~~!
!~~ the Free Software Foundation.                                                                 ~~!
!~~ This program is distributed in the hope that it will be useful,                               ~~!
!~~ but WITHOUT ANY WARRANTY; without even the implied warranty of                                ~~!
!~~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                                          ~~!
!~~ See the GNU General Public License for more details.                                          ~~!
!~~ You should have received a copy of the GNU General Public License                             ~~!
!~~ along with this program (in the file COPYING).                                                ~~!
!~~ IF not, see <http://www.gnu.org/licenses/>.                                                   ~~!
!~~ Please see http://www.dynust.net for contact information and names                            ~~!
!~~ of individual contributors.                                                                   ~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
    USE DYNUST_MAIN_MODULE
    USE DYNUST_VEH_MODULE
    USE DYNUST_NETWORK_MODULE
    USE TransLink_add_module
	USE DYNUST_VEH_PATH_ATT_MODULE
    USE DYNUST_LINK_VEH_LIST_MODULE
    USE DYNUST_TRANSLINK_MODULE
    USE DFPORT 
    USE RAND_GEN_INT
     
	INTEGER ImDest, igp
    INTEGER Index1D,Nlnkmv
	REAL value
	INTEGER ncan1, ncan2, ncan3, ncan4, ncan5,ncan6
	INTEGER aggtime,edtime,agstime
    INTEGER TKInd
    LOGICAL MFlag,Fexist,iconsol

!> > > > > > > > > >                < < < < < < < < < < <
!>>>>>>>>>>>>>>>>>>>CODES START HERE<<<<<<<<<<<<<<<<<<<<<
!> > > > > > > > > >                < < < < < < < < < < <

   iso = mod(l,simPerAgg)
   IF(iso == 0) iso = simPerAgg
                          
   m_dynust_network_arc_de(:)%vehicle_queue = 0
     
   
   DO 9 i = 1,noofarcs
   iconsol = .false.
        
!   ncan1 = max(0,nint(m_dynust_network_arc_de(i)%maxden*m_dynust_network_arc_de(i)%xl-m_dynust_network_arc_de(i)%pcetotal)) ! for checking physical capacity, should use physical PCE
!   ncan2 = m_dynust_network_arc_de(i)%inoutbuff%NVehOut
   r1 = ranxy(27) 
   IF(r1 < (m_dynust_network_arc_de(i)%MaxFlowRate*6.0-ifix(m_dynust_network_arc_de(i)%MaxFlowRate*6.0))) THEN  ! inflow rate to link i
	  ncan7 = ifix(m_dynust_network_arc_de(i)%MaxFlowRate*6.0)+1
   ELSE
 	  ncan7 = ifix(m_dynust_network_arc_de(i)%MaxFlowRate*6.0)
   ENDIF
   ncan8 = m_dynust_network_arc_de(i)%inoutbuff%NVehIn

     nc = ncan7 

      nb = 0
      iseek = 0
      IF(nc > 0) THEN
      DO while(nb < nc.and.iseek < ncan8)
      iseek = iseek + 1


     j  = TranLink_Value(i,iseek,1)
     in = TranLink_Value(i,iseek,2)

     jerror=0

      DO nlii=1,m_dynust_network_arc_nde(in)%llink%no
         IF(m_dynust_network_arc_nde(in)%llink%p(nlii) == i) THEN 
            nlindex=nlii
            jerror = 1
	        exit
         ENDIF
      ENDDO

      IF(jerror == 0) THEN

       WRITE(911,*) 'ERROR:   next link'
       WRITE(911,*) 'J I m_dynust_network_arc_nde(i)%idnod NL m_dynust_network_arc_nde(nl)%iunod',j,i,m_dynust_network_arc_nde(i)%idnod,nl,destination(m_dynust_last_stand(j)%jdest),m_dynust_last_stand(j)%isec, m_dynust_last_stand(j)%jdest,m_dynust_last_stand(j)%vehtype,m_dynust_veh(j)%DestVisit,m_dynust_veh(j)%NoOfIntDst
       WRITE(911,*) 'icu info vehclass',m_dynust_veh_nde(j)%icurrnt,m_dynust_veh_nde(j)%info,m_dynust_last_stand(j)%vehclass
       STOP
      ENDIF

      IF(m_dynust_network_arcmove_de(in,nlindex)%capacity >= m_dynust_veh(j)%vhpce/AttScale) THEN
         m_dynust_network_arcmove_de(in,nlindex)%capacity=m_dynust_network_arcmove_de(in,nlindex)%capacity-m_dynust_veh(j)%vhpce/AttScale
      ELSEIF(m_dynust_network_arcmove_de(in,nlindex)%capacity < m_dynust_veh(j)%vhpce/AttScale.and.m_dynust_network_arcmove_de(in,nlindex)%capacity >= 0.0001) THEN
         r1 = ranxy(28)
         IF(r1 < m_dynust_network_arcmove_de(in,nlindex)%capacity/(m_dynust_veh(j)%vhpce/AttScale)) THEN
	       m_dynust_network_arcmove_de(in,nlindex)%capacity=0.0
		 ELSE
	       m_dynust_network_arcmove_de(in,nlindex)%capacity=0.0
		   go to 800  
         ENDIF
      ELSE
         go to 800
      ENDIF
    
      nb = nb + 1            
      m_dynust_network_arc_de(i)%inoutbuff%NVehIn = m_dynust_network_arc_de(i)%inoutbuff%NVehIn - 1
      m_dynust_network_arc_de(in)%inoutbuff%NVehOut = m_dynust_network_arc_de(in)%inoutbuff%NVehOut - 1
      TranLink_Array(i)%P(iseek)%VehID = -1
      iconsol = .true.
      

          IF(NSigOpt > 0) THEN
            IF(SigOut(in,maxmove) == 1) THEN ! this node needs to output movement
               Ismove = MoveNoForLinkLL(in,i)
               SigOut(in,Ismove) = SigOut(in,Ismove) + 1
            ENDIF
          ENDIF
		  movetmp=getBstMove(in,i)
		  IF(movetmp < 1) THEN
		  WRITE(911,*) "moveturn error in vehicle transfer"
		  WRITE(911,*) "from link to link", in, i
		  WRITE(911,*) "check movement.dat"
		  WRITE(911,*) "from ",m_dynust_network_node_nde(m_dynust_network_arc_nde(in)%iunod)%IntoOutNodeNum, " to ",m_dynust_network_node_nde(m_dynust_network_arc_nde(in)%idnod)%IntoOutNodeNum, " missing ", m_dynust_network_node_nde(m_dynust_network_arc_nde(i)%idnod)%IntoOutNodeNum
		  STOP
		  ENDIF            
          
        aggtime = max(1,ifix((m_dynust_veh(j)%apar/xminPerSimInt)/simPerAgg)+1) ! apar: time arriving at the node

     IF(t < m_dynust_veh(j)%apar) THEN
        WRITE(911,*) "Error in veh transfer", t, m_dynust_veh(j)%apar
        WRITE(911,*) 'Please contact developers to resolve this issue'
        STOP
     ENDIF

!    ALLOCATE MEMORY ONLY FOR THOSE TURNS WITH VEHICLES PASSIING FROM LINK IN TO LINK I AT TIME AGGIME
     IF(m_dynust_network_arc_de(m_dynust_network_arc_nde(in)%fortobacklink)%link_outflow_count(aggtime)%psize < 1) THEN
       igp = max(1,backpointr(m_dynust_network_arc_nde(i)%iunod+1)-backpointr(m_dynust_network_arc_nde(i)%iunod))
       ALLOCATE(m_dynust_network_arc_de(m_dynust_network_arc_nde(in)%fortobacklink)%link_outflow_count(aggtime)%p(igp))
       m_dynust_network_arc_de(m_dynust_network_arc_nde(in)%fortobacklink)%link_outflow_count(aggtime)%p(:)=0
       m_dynust_network_arc_de(m_dynust_network_arc_nde(in)%fortobacklink)%link_outflow_count(aggtime)%psize=igp
       ALLOCATE(m_dynust_network_arc_de(m_dynust_network_arc_nde(in)%fortobacklink)%link_outflow_delay(aggtime)%p(igp))
       m_dynust_network_arc_de(m_dynust_network_arc_nde(in)%fortobacklink)%link_outflow_delay(aggtime)%p(:)=0
       m_dynust_network_arc_de(m_dynust_network_arc_nde(in)%fortobacklink)%link_outflow_delay(aggtime)%psize=igp
     ENDIF
!     ACCUMULATE LINK STAT AND TURN PENALTY
      m_dynust_network_arc_de(m_dynust_network_arc_nde(in)%fortobacklink)%link_outflow_count(aggtime)%p(movetmp)=m_dynust_network_arc_de(m_dynust_network_arc_nde(in)%fortobacklink)%link_outflow_count(aggtime)%p(movetmp)+1	 
	  m_dynust_network_arc_de(m_dynust_network_arc_nde(in)%fortobacklink)%link_outflow_delay(aggtime)%p(movetmp)=m_dynust_network_arc_de(m_dynust_network_arc_nde(in)%fortobacklink)%link_outflow_delay(aggtime)%p(movetmp)+max(0.0,(t-m_dynust_veh(j)%apar))*AttScale

	  m_dynust_veh(j)%ttSTOP= m_dynust_veh(j)%ttSTOP+t-m_dynust_veh(j)%apar
	 
      m_dynust_veh(j)%apar = 0
      m_dynust_veh(j)%aparindex=.false.
	  m_dynust_veh(j)%inpar = t
    
      IF(m_dynust_veh_nde(j)%icurrnt == 1) m_dynust_veh(j)%ttilFstNTrans = tend-m_dynust_veh(j)%tleft

      IF(m_dynust_veh_nde(j)%icurrnt == 1) THEN
        m_dynust_veh(j)%ttilnow = t-m_dynust_last_stand(j)%stime
      ENDIF
!     INSERT VEHICLE TRAVEL TIME TIL NOW
      CALL vehatt_Insert(j,m_dynust_veh_nde(j)%icurrnt,3,m_dynust_veh(j)%ttilnow) 

!!     CHECK IF THE CUMULATIVE DELAY EXCEEDS THE TOLERABLE DELAY
!      IF(ReadHistArr > 0.AND.RunMode == 2) THEN
!        evalue = vehatt_value(j,m_dynust_veh_nde(j)%icurrnt,4) ! INFLATED BY ATTSCALE
!        m_dynust_veh(j)%currentdelay = m_dynust_veh(j)%ttilnow*AttScale - evalue
!        IF(evalue > 0.AND.(m_dynust_veh(j)%currentdelay > m_dynust_veh(j)%delaytole).AND.m_dynust_veh(j)%DivStatus == 0) THEN ! CHECK EVALUE > 0 BECAUSE WHEN VEHICLE IS STILL IN THE NETWORK AT TIME OF SIMULATION TERMINATION, THE EVALUE = 0 FROM THE BASELINE CASE, IN THIS CASE, DON'T ACTIVATE THE RULE
!          m_dynust_veh(j)%DivStatus = 1 ! IN NEED FOR DIVERSION
!        ENDIF
!      ENDIF
      
      IF(RunMode == 1.or.(RunMode /= 1.and.m_dynust_veh(j)%NoOfIntDst == 1)) THEN
	   ImDest = 1
	  ELSE
	   ImDest = m_dynust_veh(j)%DestVisit
	  ENDIF

      IF(nint(vehatt_value(j,m_dynust_veh_nde(j)%icurrnt,1)) == destination(MasterDest(m_dynust_veh_nde(j)%IntDestZone(ImDest))))THEN
       evalue = m_dynust_veh(j)%ttSTOP + m_dynust_veh(j)%IntDestDwell(Imdest)/60.0
      ELSE
       evalue = m_dynust_veh(j)%ttSTOP
      ENDIF

      CALL vehatt_Insert(j,m_dynust_veh_nde(j)%icurrnt,2,evalue)

          IF(m_dynust_veh(j)%qflag) THEN
             m_dynust_veh(j)%qflag=.false.
             m_dynust_veh(j)%tqwait=0.0
          ENDIF

!         IF(m_dynust_veh_nde(j)%info == 1) THEN
!           IF(m_dynust_veh(j)%switch < 0) m_dynust_veh(j)%switch=iabs(m_dynust_veh(j)%switch)+1
!         ENDIF

         IF(m_dynust_network_arcmove_nde(in,nlindex)%move == 1) m_dynust_network_arc_de(in)%outleft=m_dynust_network_arc_de(in)%outleft+1
         m_dynust_network_arc_de(in)%outflow=m_dynust_network_arc_de(in)%outflow+1

         IF(m_dynust_veh(j)%tleft < 0.0) THEN
           WRITE(911,*) "error in veh transer"
           WRITE(911,*) "tleft incorrect", m_dynust_veh(j)%tleft
         ENDIF
         m_dynust_veh(j)%position=max(0.0,m_dynust_network_arc_nde(i)%s-(m_dynust_veh(j)%VehSpeed*m_dynust_veh(j)%tleft)) ! IF the link is too short, force the vehicle start at the STOP bar at the downstream link
         m_dynust_veh(j)%distans = m_dynust_veh(j)%distans + m_dynust_veh(j)%VehSpeed*m_dynust_veh(j)%tleft !!!!uytr
         m_dynust_veh(j)%ttilnow = m_dynust_veh(j)%ttilnow + m_dynust_veh(j)%tleft

         IF(m_dynust_last_stand(j)%vehtype /= 6) THEN ! IF NOT TRANSIT
           iab = m_dynust_network_arc_nde(i)%FortoBackLink
           itt = min(aggint,ifix((t/xminPerSimInt)/simPerAgg)+1)
           m_dynust_last_stand(j)%expense=m_dynust_last_stand(j)%expense+(m_dynust_veh(j)%vehSpeed*m_dynust_veh(j)%tleft)/m_dynust_network_arc_nde(i)%s*m_dynust_network_arc_de(iab)%costexp(itt,m_dynust_last_stand(j)%vehtype)
         ENDIF

         GuiTotalTime = GuiTotalTime + m_dynust_veh(j)%tleft

         IF(m_dynust_veh(j)%position < 0.0) THEN
           print *, 'error in veh transfer for xpar update'
           m_dynust_veh(j)%position=0.0
           m_dynust_veh(j)%distans=m_dynust_veh(j)%distans+m_dynust_network_arc_nde(i)%s
         ELSE
           m_dynust_veh(j)%distans = m_dynust_veh(j)%distans + m_dynust_network_arc_nde(in)%s ! INCREASED THE TRAVELLED DISTANCE BY THE INCOMING LINK LENGTH uytr
         ENDIF

         CALL linkVehList_remove(in,j)


         agstime = ifix((l-1)*xminPerSimInt)+1
         m_dynust_network_arc_de(in)%AccuVol(agstime)=m_dynust_network_arc_de(in)%AccuVol(agstime) + 1 
         IF(m_dynust_last_stand(j)%vehtype == 2) m_dynust_network_arc_de(in)%AccuVolT(agstime)=m_dynust_network_arc_de(in)%AccuVolT(agstime) + 1
         IF(m_dynust_last_stand(j)%vehtype == 3) m_dynust_network_arc_de(in)%AccuVolH(agstime)=m_dynust_network_arc_de(in)%AccuVolH(agstime) + 1
         
         m_dynust_network_arc_de(in)%npar=m_dynust_network_arc_de(in)%npar-1
	     m_dynust_network_arc_de(in)%volume=m_dynust_network_arc_de(in)%volume-1
		 IF(m_dynust_last_stand(j)%vehtype == 2.or.m_dynust_last_stand(j)%vehtype == 6.or.m_dynust_last_stand(j)%vehtype == 7) THEN
		    m_dynust_network_arc_de(in)%nTruck=m_dynust_network_arc_de(in)%nTruck-1
         ENDIF

         IF(m_dynust_network_arc_de(i)%volume < 0) THEN
           WRITE(911,*)'Negative volume on link',in
	     WRITE(911,*)'Please contact developers'
           STOP
         ENDIF

	   CALL linkVehList_insert(i,j,m_dynust_veh(j)%position) !LST

       m_dynust_network_arc_de(i)%npar=m_dynust_network_arc_de(i)%npar+1
	   m_dynust_network_arc_de(i)%volume=m_dynust_network_arc_de(i)%volume+1

!      vehtype = 2: TRUCK, 6: BUT, 7: OTEHR HEAVEY VEHICLES      
       IF(m_dynust_last_stand(j)%vehtype == 2.or.m_dynust_last_stand(j)%vehtype == 6.or.m_dynust_last_stand(j)%vehtype == 7) THEN
	     m_dynust_network_arc_de(i)%nTruck=m_dynust_network_arc_de(i)%nTruck+1
       ENDIF

       m_dynust_veh_nde(j)%icurrnt=m_dynust_veh_nde(j)%icurrnt + 1

       IF(m_dynust_veh_nde(j)%icurrnt<vehatt_P_Size(j)-1) THEN ! FIND THE NEXT LINK ONLY IF THE CURRENT LINK IS NOT LEADING TO THE FINAL DESTINATION. TO CONSIDER THE FACT THAT BUS MAY HAVE THE LAST NODE NOT BEING DESTINATION NODE DEFINED IN DESTINATION.DAT
         CALL RETRIEVE_NEXT_LINK(t,j,i,m_dynust_veh_nde(j)%icurrnt,Nlnk,NlnkMv)
       ENDIF

11    CONTINUE

800   ENDDO ! iseek
      ENDIF ! nc > 1
      
      IF(iconsol.and.mod(1,1) == 0) THEN
        CALL TranLink_consolidate(i)
!        CALL TranLink_consolidate_new(i)
      ENDIF


      IF(m_dynust_network_arc_de(i)%inoutbuff%NVehIn > 0) THEN
	    DO k = 1, m_dynust_network_arc_de(i)%inoutbuff%NVehIn
            j  = TranLink_Value(i,k,1)
            in  = TranLink_Value(i,k,2)
 			IF(.not.m_dynust_network_arc_nde(in)%link_ams) THEN
 			   m_dynust_network_arc_de(in)%vehicle_queue = m_dynust_network_arc_de(in)%vehicle_queue + 1
 			   mmv = getBstMove(in,i)
               m_dynust_network_arc_de(m_dynust_network_arc_nde(in)%fortobacklink)%link_qmp(iso)%p(mmv) = m_dynust_network_arc_de(m_dynust_network_arc_nde(in)%fortobacklink)%link_qmp(iso)%p(mmv) + 1
            ENDIF 
            IF(.not.m_dynust_veh(j)%qflag) THEN
               m_dynust_veh(j)%qflag=.True.
               m_dynust_veh(j)%position=0.0
 			ENDIF
            m_dynust_veh(j)%tqwait = m_dynust_veh(j)%tqwait + m_dynust_veh(j)%tleft
            m_dynust_veh(j)%ttilnow = m_dynust_veh(j)%ttilnow + m_dynust_veh(j)%tleft
            GuiTotalTime = GuiTotalTime + m_dynust_veh(j)%tleft  ! -- add to GUITotalTime
        ENDDO ! DO WHILE LOOK
      ENDIF ! IF NC >0

!    UPDATE LINK TRUCK PCE
     CALL LOOKUP_TRUCK_PCE(i)     
!    UPDATE TOTAL PCE
     m_dynust_network_arc_de(i)%pcetotal = m_dynust_network_arc_de(i)%TruckPct*m_dynust_network_arc_de(i)%npar*m_dynust_network_arc_de(i)%DynPCE+(1-m_dynust_network_arc_de(i)%TruckPct)*m_dynust_network_arc_de(i)%npar


9     CONTINUE


      IF(nSigOpt > 0) THEN
            IF(t > 0.01) THEN
            IF(mod(nint(t/xminPerSimInt),nint(SigOptInt/xminPerSimInt)) == 0.or.abs(nint(t/xminPerSimInt)-edtime) <= 1) THEN ! output the results
              WRITE(514,'("Time=> ",f6.1)')  t
              DO is2 = 1, NSigOpt
                DO itake = Backpointr(SigOptNid(is2)),Backpointr(SigOptNid(is2)+1)- 1
                   WRITE(514,'("From ", i7," To ", i7, " remaining PCE", f6.1, " K ",f6.1)') m_dynust_network_node_nde(m_dynust_network_arc_nde(m_dynust_network_arc_nde(itake)%BackToForLink)%iunod)%IntoOutNodeNum,m_dynust_network_node_nde(m_dynust_network_arc_nde(m_dynust_network_arc_nde(itake)%BackToForLink)%idnod)%IntoOutNodeNum, m_dynust_network_arc_de(m_dynust_network_arc_nde(itake)%BackToForLink)%pcetotal,m_dynust_network_arc_de(m_dynust_network_arc_nde(itake)%BackToForLink)%c
                   DO igs = 1, m_dynust_network_arc_nde(m_dynust_network_arc_nde(itake)%BackToForLink)%llink%no
                     WRITE(514,'("                                              To Node", i7," Outflow N",i6)') m_dynust_network_node_nde(m_dynust_network_arc_nde(m_dynust_network_arc_nde(m_dynust_network_arc_nde(itake)%BackToForLink)%llink%p(igs))%idnod)%IntoOutNodeNum,SigOut(m_dynust_network_arc_nde(itake)%BackToForLink,igs)
                     SigOut(m_dynust_network_arc_nde(itake)%BackToForLink,igs) = 0
                   ENDDO
                ENDDO
              ENDDO 
            ENDIF
          ENDIF 
     ENDIF

END SUBROUTINE


SUBROUTINE LOOKUP_TRUCK_PCE(i)
    USE DYNUST_MAIN_MODULE
    USE DYNUST_VEH_MODULE
    USE DYNUST_NETWORK_MODULE
    USE TransLink_add_module
	USE DYNUST_VEH_PATH_ATT_MODULE
    USE DYNUST_LINK_VEH_LIST_MODULE
    USE DYNUST_TRANSLINK_MODULE
    USE DFPORT 
    USE RAND_GEN_INT
    LOGICAL Mflag
    INTEGER TKInd
    Mflag = .false.
	
	IF(m_dynust_network_arc_nde(i)%link_iden < 99.and.m_dynust_network_arc_de(i)%npar > 0) THEN
        m_dynust_network_arc_de(i)%TruckPct = float(m_dynust_network_arc_de(i)%nTruck)/m_dynust_network_arc_de(i)%npar
        IF(m_dynust_network_arc_de(i)%nTruck > 0) THEN
	      DO ii = 1, TruckNum
             IF(ii == 1) THEN
              IF((m_dynust_network_arc_de(i)%TruckPct <= TruckBPnt(1)/100.0)) THEN
               TKInd = ii
               MFlag = .True.
               exit
			  ENDIF
			 ELSEIF(ii > 1) THEN
			  IF((m_dynust_network_arc_de(i)%TruckPct <= TruckBPnt(ii)/100.0).and.(m_dynust_network_arc_de(i)%TruckPct > TruckBPnt(ii-1)/100.0)) THEN
               TKInd = ii
               MFlag = .True.
               exit
              ENDIF
             ENDIF
	      ENDDO
	      IF(.not.MFlag) TKInd = TruckNum
          m_dynust_network_arc_de(i)%DynPCE = PCE(m_dynust_network_arc_de(i)%GRDInd,m_dynust_network_arc_de(i)%LENInd,TKInd) ! time-dependent PCE
	    ELSE
          m_dynust_network_arc_de(i)%DynPCE = 2.5
        ENDIF !nTruck(i) > 0
	ELSE  ! for downgrade, the PCE remains 2.5
        m_dynust_network_arc_de(i)%DynPCE = 2.5
	ENDIF ! Link_iden < 90


END SUBROUTINE