MODULE DYNUST_FUEL_MODULE
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~ Copyright (C) 2002-2011 The Arizona Board of Regents on Behalf of The University of Arizona.  ~~!
!~~ Developed by Yi-Chang Chiu, Ph.D. as par to the DynusT project.                               ~~!
!~~ This program is free software: you can redistribute it and/or modify it                       ~~!
!~~ under the terms of the GNU General Public License version 2 as published by                   ~~!
!~~ the Free Software Foundation.                                                                 ~~!
!~~ This program is distributed in the hope that it will be useful,                               ~~!
!~~ but WITHOUT ANY WARRANTY; without even the implied warranty of                                ~~!
!~~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                                          ~~!
!~~ See the GNU General Public License for more details.                                          ~~!
!~~ You should have received a copy of the GNU General Public License                             ~~!
!~~ along with this program (in the file COPYING).                                                ~~!
!~~ IF not, see <http://www.gnu.org/licenses/>.                                                   ~~!
!~~ Please see http://www.dynust.net for contact information and names                            ~~!
!~~ of individual contributors.                                                                   ~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
USE DYNUST_MAIN_MODULE
USE RNSET_INT
USE RAND_GEN_INT

INTEGER NumFuel

INTEGER NumGasst

TYPE FuelEcon
 REAL spd
 REAL ConsumRate 
END TYPE

TYPE Fuel
 REAL       mean, std, floor, ceil, minlevel
 INTEGER    NumCat,RanCt
 REAL,ALLOCATABLE::ran(:)
 TYPE(FuelEcon),POINTER:: Consum(:)
END TYPE
TYPE(Fuel),ALLOCATABLE:: FuELSEt(:)

TYPE VehFuelAtt
 REAL IniGas
 REAL CurGas
 LOGICAL FuelLow
END TYPE
TYPE(VehFuelAtt),ALLOCATABLE::VehFuel(:)

TYPE Gas
 INTEGER node
 REAL x, y
 INTEGER opn
 REAL cap
 INTEGER demand
 INTEGER served
END TYPE
TYPE(Gas),ALLOCATABLE::GasLoc(:)

TYPE COR
 REAL x
 REAL y
 INTEGER node
END TYPE
TYPE(cor),ALLOCATABLE::coord(:)

CONTAINS

SUBROUTINE ReadNAllocate_fuel(NoofVeh)
  USE DYNUST_MAIN_MODULE
  USE RAND_GEN_INT
  LOGICAL EXT2
  INTEGER err
!> > > > > > > > > >                < < < < < < < < < < <
!>>>>>>>>>>>>>>>>>>>CODES START HERE<<<<<<<<<<<<<<<<<<<<<
!> > > > > > > > > >                < < < < < < < < < < <

! read GasConsum.dat   
  INQUIRE (FILE='fuel_par.dat', EXIST = EXT2)
	 IF(EXT2) THEN
!	   OPEN(unit = 6057, File ='gasst.dat',status='old')
	   OPEN(unit = 6058, File ='fuel_par.dat',status='unknown')
!	   OPEN(unit = 6059, File ='all_fuel.dat',status='unknown')
	   OPEN(unit = 6060, File ='fuel_out.dat',status='unknown')
!	   OPEN(unit = 6056, File ='xy.dat',status='old')

	   READ(6058,*) NumFuel
       IF(NumFuel > 0) THEN
	     ALLOCATE(FuELSEt(NumFuel))
	     DO isee = 1, Numfuel
	       READ(6058,*) isee2,FuELSEt(isee)%mean,FuELSEt(isee)%std,FuELSEt(isee)%floor,FuELSEt(isee)%ceil,FuELSEt(isee)%minlevel
	       READ(6058,*) FuELSEt(isee)%NumCat
	       ALLOCATE(FuELSEt(isee)%Consum(FuELSEt(isee)%NumCat))
	       FuELSEt(isee)%Consum(:)%spd = 0
	       FuELSEt(isee)%Consum(:)%consumrate = 0	       
	       ALLOCATE(FuELSEt(isee)%ran(noofran))
	       FuELSEt(isee)%ran(:) = 0
	       DO J = 1, FuELSEt(isee)%NumCat
	         READ(6058,*) mtmp,FuELSEt(isee)%Consum(J)%spd,FuELSEt(isee)%Consum(J)%ConsumRate
	       ENDDO
	       FuELSEt(isee)%ranct = noofran ! initialize random number
	     ENDDO
       ENDIF

! read gas station informationi
!     NumGasst = 0
!     DO WHILE (.not.EOF(6057))
!      READ(6057,*) 
!      NumGasst = NumGasst + 1
!     ENDDO
!     REWIND(6057)
!     ALLOCATE(GasLoc(NumGasst))
!     GasLoc(:)%demand = 0
!     GasLoc(:)%served = 0
     
!     DO i = 1, NumGasst
!      READ(6057,*) GasLoc(i)%node,mtmp,GasLoc(i)%x,GasLoc(i)%y,GasLoc(i)%opn,GasLoc(i)%cap
!     ENDDO

! read xy coordinations
!	 ALLOCATE(coord(noofnodes_org))
!	 coord(:)%node = 0
!	 coord(:)%x = 0
!	 coord(:)%y = 0
!     DO i  = 1, noofnodes_org
!       READ(6056,*) coord(i)%node,coord(i)%x,coord(i)%y
!     ENDDO
    ELSE
     WRITE(911,*) "Error! Fuel consumption calcualtion is being specified in PARAMETER.dat"
     WRITE(911,*) "But no fuel_par.dat input file is available", STOP
	ENDIF    !IF(EXT2) THEN 

END SUBROUTINE

!~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>
SUBROUTINE ServedDemand
 USE DYNUST_MAIN_MODULE
 USE DYNUST_NETWORK_MODULE
 INTEGER served, nodtmp, nodeTMP
 REAL dis2
!> > > > > > > > > >                < < < < < < < < < < <
!>>>>>>>>>>>>>>>>>>>CODES START HERE<<<<<<<<<<<<<<<<<<<<<
!> > > > > > > > > >                < < < < < < < < < < <

 CLOSE(6059)
 OPEN(unit = 6059, File ='all_fuel.dat',status='OLD')
 OPEN(unit = 60599, File ='all_fuel_Served.dat',status='UNKNOWN')
 OPEN(unit = 6080, File ='GasDemand.dat',status='UNKNOWN')
 OPEN(unit = 6081, File ='GasDemand_served.dat',status='UNKNOWN')
 
 DO WHILE(.NOT.EOF(6059))
  READ(6059,*) j, nodeTMP,SIniGas,Sdistans,t
  served = 0
  distmp = 100000000
  iseethis = 0        
  DO i = 1, NumGasst
    nodtmp = OutToInNodeNum(nodeTMP)
    IF(GasLoc(i)%y >= coord(nodtmp)%y) THEN
      dis2 = SQRT((GasLoc(i)%x-coord(nodtmp)%x)**2+(GasLoc(i)%y-coord(nodtmp)%y)**2)
      IF(dis2 < distmp) THEN
        distmp = dis2
        iseethis = i
      ENDIF
    ENDIF
  ENDDO
  IF(iseethis > 0) THEN ! IF there is any station in the downstream of the vehicle. 
  IF(GasLoc(iseethis)%cap > 0) THEN
    GasLoc(iseethis)%cap = GasLoc(iseethis)%cap - VehFuel(j)%IniGas
    served = 1
    GasLoc(iseethis)%served = GasLoc(iseethis)%served + 1 
  ELSE
    served = 0
  ENDIF
  Gasloc(iseethis)%demand = Gasloc(iseethis)%demand + 1
  ENDIF
  WRITE(60599,'(2i7,2f10.3,f8.1,i5)') j, nodeTMP,SIniGas,Sdistans,t,served
 ENDDO !DO WHILE
 
 WRITE(6080,'(200i7)') (GasLoc(m)%demand,m = 1, NumGasst)
 WRITE(6081,'(200i7)') (GasLoc(m)%served,m = 1, NumGasst)
 
 CLOSE(6059)
 CLOSE(60599)

  
END SUBROUTINE

!~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>
SUBROUTINE OutputFuelDemand(i,j,t)
  USE DYNUST_MAIN_MODULE
  USE DYNUST_VEH_MODULE
  LOGICAL WarnFlg
  INTEGER i, j
  REAL t
!> > > > > > > > > >                < < < < < < < < < < <
!>>>>>>>>>>>>>>>>>>>CODES START HERE<<<<<<<<<<<<<<<<<<<<<
!> > > > > > > > > >                < < < < < < < < < < <
  
    dd = min(m_dynust_veh(j)%position,m_dynust_veh(j)%vehSpeed*xminPerSimInt)
    CALL UpdateFuelLevel(J,dd,WarnFlg)
END SUBROUTINE
!~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>

REAL FUNCTION rannor(ic)
 USE RNNOF_INT
 INTEGER ic ! vehicle type
 REAL,ALLOCATABLE:: rantmp(:)
 
 IF(FuELSEt(ic)%ranct == noofran) THEN
   ALLOCATE(rantmp(noofran))
   istrm = istrm + 1
   CALL RNSET(istrm)
   CALL RNNOA(noofran,rantmp)  
   CALL SSCAL(noofran,FuELSEt(ic)%std,rantmp,1)
   CALL SADD(noofran,FuELSEt(ic)%mean,rantmp,1)
   FuELSEt(ic)%ran(:)=rantmp(:)
   FuELSEt(ic)%ranct = 0    
   DEALLOCATE(rantmp)      
 ENDIF
 FuELSEt(ic)%ranct = FuELSEt(ic)%ranct + 1
 rannor = FuELSEt(ic)%ran(FuELSEt(ic)%ranct)
END FUNCTION

!~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>
SUBROUTINE SetFuelLevel(VehID)

 USE DYNUST_VEH_MODULE
 INTEGER VehID,Vls
 Vls = m_dynust_last_stand(VehID)%vehtype
 IF(Vls == 3) Vls = 1
 VehFuel(VehID)%IniGas = max(FuELSEt(Vls)%floor,rannor(Vls))
 VehFuel(VehID)%IniGas = min(FuELSEt(Vls)%ceil,VehFuel(VehID)%IniGas)
 VehFuel(VehID)%CurGas = VehFuel(VehID)%IniGas
END SUBROUTINE

!~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>
SUBROUTINE UpdateFuelLevel(VehID,dd,WarnFlg)
! the fuel consumption rate are stored in a 5-min increment array
! first find the value of the nearest 5 and THEN search it in the array
! IG is the index key. This is to take the remainder of 5 and round it up or down
! and THEN add it back to the floor with division of 5
 USE DYNUST_VEH_MODULE
 
 INTEGER VehCls, VehID
 LOGICAL WarnFlg
 REAL dd
 WarnFlg = .false.
 VehCls = m_dynust_last_stand(VehID)%vehtype
 IF(VehCls == 3) VehCls = 1
 IG = nint(MOD(m_dynust_veh(VehID)%VehSpeed*60.0,5.0)/5.0)+ifix(m_dynust_veh(VehID)%VehSpeed*60.0/5.0)
 VehFuel(VehID)%CurGas = VehFuel(VehID)%CurGas - dd/FuELSEt(VehCls)%Consum(ig)%ConsumRate
 IF(VehFuel(VehID)%CurGas < FuELSEt(VehCls)%minlevel) THEN
   WarnFlg = .true.
 ENDIF
END SUBROUTINE

!~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>
SUBROUTINE IniVehFuel
INTEGER err
	   ALLOCATE(VehFuel(NoofVeh),stat=err)
       IF(err /= 0) WRITE(911,*) "allocate VehFuel error - insufficient memory", STOP
	   VehFuel(:)%IniGas = 0
	   VehFuel(:)%CurGas = 0
	   VehFuel(:)%FuelLow = .false.
END SUBROUTINE

!~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>
SUBROUTINE DEALLOCATEFuel
    IF(NumFuel > 0) THEN
      DO i = 1, Numfuel
        DEALLOCATE(FuELSEt(i)%Consum)
        DEALLOCATE(FuELSEt(i)%ran)
      ENDDO
      DEALLOCATE(FuELSEt)
    ENDIF

    CLOSE(6056)
END SUBROUTINE

!~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>
SUBROUTINE WriteOutFuelConsum
  USE DYNUST_MAIN_MODULE
  USE DYNUST_VEH_MODULE
  INTEGER timetmp,Vls
  INTEGER, ALLOCATABLE::FuelOutCnt(:,:)
  REAL, ALLOCATABLE::FuelOutAmt(:,:)
  
  ALLOCATE(FuelOutcnt(2,NINT(SimPeriod)))
  ALLOCATE(FuelOutamt(2,NINT(SimPeriod)))
  FuelOutCnt(:,:) = 0
  FuelOutAmt(:,:) = 0
  
  DO i = 1, NoofVeh
   Vls = m_dynust_last_stand(i)%vehtype
   IF(Vls == 3) Vls = 1
   IF(m_dynust_last_stand(i)%stime > 0) THEN
     timetmp = ifix(m_dynust_last_stand(i)%stime)+1
     FuelOutAmt(Vls,timetmp) = FuelOutAmt(Vls,timetmp) + max(0.0,VehFuel(i)%IniGas - VehFuel(i)%CurGas)
     FuelOutcnt(Vls,timetmp) = FuelOutCnt(Vls,timetmp) + 1
   ENDIF
  ENDDO
  WRITE(6060,'( " ===== FUEL CONSUMPTION REPORT ====== ")')
  WRITE(6060,'("---- Auto ----")')
   DO I = 1, SimPeriod
     WRITE(6060,'("Time (min): ", f5.1," # of Veh", i7,"  Fuel Consumed(G): ", f12.3)') float(i), FuelOutCnt(1,i), FuelOutAmt(1,i)
   ENDDO
   WRITE(6060,*)
   WRITE(6060,'("Total # of autos:     ",i9," Total Fuel Consumption: ",f12.1)') sum(FuelOutCnt(1,:)), sum(FuelOutAmt(1,:))
   WRITE(6060,*)
   WRITE(6060,'("---- Truck ----")')
   DO I = 1, SimPeriod
     WRITE(6060,'("Time (min): ", f5.1," # of Veh", i7,"  Fuel Consumed(G): ", f12.3)') float(i), FuelOutCnt(2,i), FuelOutAmt(2,i)
   ENDDO   
   WRITE(6060,*)
   WRITE(6060,'("Total # of trucks:    ",i9," Total Fuel Consumption: ",f12.1)') sum(FuelOutCnt(2,:)), sum(FuelOutAmt(2,:))
   DEALLOCATE(FuelOutAmt)
   DEALLOCATE(FuelOutCnt) 
   CLOSE(6060)  
!   CLOSE(6059)
   CLOSE(6058)
END SUBROUTINE

!~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>
SUBROUTINE WriteOutFuelConsum_City
  USE DYNUST_MAIN_MODULE
  USE DYNUST_VEH_MODULE
  INTEGER timetmp,NumTown
  TYPE TOWNdata
    INTEGER:: nzones
    INTEGER,POINTER::p(:)
  ENDTYPE
  TYPE(TOWNDATA),ALLOCATABLE::Town(:)
  LOGICAL EXT2
  INTEGER ITP,Vls
  
  INTEGER, ALLOCATABLE::TTOutCnt(:,:,:,:),maps(:)
  REAL, ALLOCATABLE::TTOutAmt(:,:,:,:)
 ! read intput
  INQUIRE (FILE='input_ProcessTravelTimes.dat', EXIST = EXT2)
	 IF(EXT2) THEN
	   OPEN(unit = 6058, File = 'input_ProcessTravelTimes.dat',status='unknown')
	   OPEN(unit = 6059, File ='output_ProcessTravelTimes.dat',status='unknown')
	   
       ALLOCATE(maps(nzones))
       maps(:) = 0
       READ(6058,*) numTown
       ALLOCATE(town(numTown))
       DO I = 1, numTown
         READ(6058,*) Town(i)%nzones
       ENDDO
       REWIND(6058)
       READ(6058,*)
       DO I = 1, numTown
         ALLOCATE(town(i)%p(town(i)%nzones))
         town(i)%p(:) = 0
         READ(6058,*) ITP, town(i)%p(1:town(i)%nzones)
         DO J = 1, town(i)%nzones ! map the zones to city
           maps(town(i)%p(j)) = I
         ENDDO 
       ENDDO
	 ENDIF
 ! start processing travel time
	   ALLOCATE(TTOutCnt(2,numTown,numTown,NINT(SimPeriod)))
       ALLOCATE(TTOutAmt(2,numTown,numTown,NINT(SimPeriod)))
       TTOutCnt(:,:,:,:) = 0
       TTOutAmt(:,:,:,:) = 0
        
 DO i = 1, NoofVeh
   Vls = m_dynust_last_stand(i)%vehtype
   IF(Vls == 3) Vls = 1
   IF(m_dynust_last_stand(i)%stime > 0) THEN
     timetmp = ifix(m_dynust_last_stand(i)%stime)+1
     IO = maps(m_dynust_last_stand(i)%jorig)
     ID = maps(m_dynust_last_stand(i)%jdest)
     IF(IO > 0.and.ID > 0) THEN
       TTOutAmt(Vls,IO,ID,timetmp) = TTOutAmt(Vls,IO,ID,timetmp) + max(0.0,VehFuel(i)%IniGas - VehFuel(i)%CurGas)
       TTOutcnt(Vls,IO,ID,timetmp) = TTOutCnt(Vls,IO,ID,timetmp) + 1
     ENDIF
   ENDIF
  ENDDO
! output fuel consumption by town, time and vehicle type
DO I = 1, numTown
 DO J = 1, numTown
 WRITE(6059,'("=== From Zone ",i4," to zone === "i4)') i,j 
 DO IT = 1, NINT(SimPeriod)
   WRITE(6059,'("Time(min): ",f6.1," Auto Total: ",f8.1," Auto #: ",i7," Truck Total: ",f8.1," Truck #: ",i7)')float(it),TTOutAmt(1,i,j,IT),TTOutCnt(1,i,j,IT),TTOutAmt(2,i,j,IT),TTOutCnt(2,i,j,IT)
 ENDDO
 ENDDO  
ENDDO

CLOSE(6058)
CLOSE(6059)
DO I = 1, numTown
  DEALLOCATE(town(i)%p)
ENDDO
DEALLOCATE(town)

END SUBROUTINE


END MODULE