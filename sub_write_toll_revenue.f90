SUBROUTINE WRITE_TOLL_REVENUE(maxintervals)
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~ Copyright (C) 2002-2011 The Arizona Board of Regents on Behalf of The University of Arizona.  ~~!
!~~ Developed by Yi-Chang Chiu, Ph.D. as par to the DynusT project.                               ~~!
!~~ This program is free software: you can redistribute it and/or modify it                       ~~!
!~~ under the terms of the GNU General Public License version 2 as published by                   ~~!
!~~ the Free Software Foundation.                                                                 ~~!
!~~ This program is distributed in the hope that it will be useful,                               ~~!
!~~ but WITHOUT ANY WARRANTY; without even the implied warranty of                                ~~!
!~~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                                          ~~!
!~~ See the GNU General Public License for more details.                                          ~~!
!~~ You should have received a copy of the GNU General Public License                             ~~!
!~~ along with this program (in the file COPYING).                                                ~~!
!~~ IF not, see <http://www.gnu.org/licenses/>.                                                   ~~!
!~~ Please see http://www.dynust.net for contact information and names                            ~~!
!~~ of individual contributors.                                                                   ~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!

USE DYNUST_MAIN_MODULE
USE DYNUST_NETWORK_MODULE

INTEGER maxintervals
REAL TotalRev,Totalvmt,sovvol,sovvmt,tckvol,tckvmt,sovtoll,tcktoll,hovvmt,hovvol
!> > > > > > > > > >                < < < < < < < < < < <
!>>>>>>>>>>>>>>>>>>>CODES START HERE<<<<<<<<<<<<<<<<<<<<<
!> > > > > > > > > >                < < < < < < < < < < <

TotalRev = 0
TotalVmt = 0
sovtoll  =  0
tcktoll  =  0
sovvmt  =  0
tckvmt  =  0
sovvol = 0
tckvol = 0
hovvol = 0
hovvmt = 0

   IF(TollExist.and.NTollLink > 0) THEN
    IF(iteration == 0) OPEN(file="TollRevenue.dat",unit=5699,status="unknown")
      WRITE(5699,*)
      WRITE(5699,'("=====  Iteration ",i4,"  =====")') iteration

    DO is = 1, NTollLink
      sovvmt = 0
      tckvmt = 0
      sovvol = 0
      tckvol = 0
      sovtoll = 0
      tcktoll = 0
      hovvol = 0
      hovvmt = 0
      ilink  = Tolllink(is)%LinkNo
      IF(TollLink(is)%NTD > 0) THEN
         DO js = 1, TollLink(is)%NTD
!           istime = min(nint(SimPeriod),nint(max(1,ifix((TollLink(is)%scheme(js)%stime/xminPerSimInt))+1)*xminPerSimInt))
            IF(TollLink(is)%scheme(js)%stime-ifix(TollLink(is)%scheme(js)%stime) > 0.001) THEN  ! with decimal
              istime = max(1,ifix(TollLink(is)%scheme(js)%stime)+1)
            ELSE                                                                                ! no decimal
              istime = max(1,ifix(TollLink(is)%scheme(js)%stime))
            ENDIF
!           ietime = min(nint(SimPeriod),nint(max(1,min(maxintervals,ifix((TollLink(is)%scheme(js)%etime/xminPerSimInt))+1))*xminPerSimInt))
            IF(TollLink(is)%scheme(js)%etime-ifix(TollLink(is)%scheme(js)%etime) > 0.001) THEN  ! with decimal
              ietime = min(nint(SimPeriod),ifix(TollLink(is)%scheme(js)%etime)+1)
            ELSE                                                                                ! no decimal
              ietime = min(nint(SimPeriod),ifix(TollLink(is)%scheme(js)%etime))
            ENDIF
            sovvol = sovvol + max(0,sum(m_dynust_network_arc_de(ilink)%Accuvol(istime:ietime))-sum(m_dynust_network_arc_de(ilink)%AccuvolH(istime:ietime))-sum(m_dynust_network_arc_de(ilink)%AccuvolT(istime:ietime)))
            hovvol = hovvol + sum(m_dynust_network_arc_de(ilink)%AccuvolH(istime:ietime))
            tckvol = tckvol + sum(m_dynust_network_arc_de(ilink)%AccuvolT(istime:ietime))

            sovvmt = sovvmt + m_dynust_network_arc_nde(ilink)%s*(sum(m_dynust_network_arc_de(ilink)%Accuvol(istime:ietime))-sum(m_dynust_network_arc_de(ilink)%AccuvolH(istime:ietime))-sum(m_dynust_network_arc_de(ilink)%AccuvolT(istime:ietime)))
            hovvmt = hovvmt + m_dynust_network_arc_nde(ilink)%s*sum(m_dynust_network_arc_de(ilink)%AccuvolH(istime:ietime))            
            tckvmt = tckvmt + m_dynust_network_arc_nde(ilink)%s*sum(m_dynust_network_arc_de(ilink)%AccuvolT(istime:ietime))

            sovtoll = sovtoll + Tolllink(is)%scheme(js)%sov*max(0,(sum(m_dynust_network_arc_de(ilink)%Accuvol(istime:ietime))-sum(m_dynust_network_arc_de(ilink)%AccuvolH(istime:ietime))-sum(m_dynust_network_arc_de(ilink)%AccuvolT(istime:ietime)))) ! all links got converted to link-based in input subrouitine already
            tcktoll = tcktoll + Tolllink(is)%scheme(js)%tck*sum(m_dynust_network_arc_de(ilink)%AccuvolT(istime:ietime))
         ENDDO
      ENDIF   
            totalvmt = totalvmt + sovvmt + tckvmt + hovvmt
            totalrev = totalrev+ sovtoll + tcktoll

	  WRITE(5699,'("Link ",i7,"-> ",i7,"   VMT (sov, tck, hov) = ",3f9.1,"   Volume (sov, tck, hov) = ",3f9.1,"  Toll Revenue($) (sov, tck)= ",2f10.1)') &
	   m_dynust_network_node_nde(TollLink(is)%UNode)%IntoOutNodeNum,m_dynust_network_node_nde(TollLink(is)%DNode)%IntoOutNodeNum,sovvmt,tckvmt,hovvmt,sovvol,tckvol,hovvol,sovtoll,tcktoll
    ENDDO
      
	  WRITE(5699,'("Total VMT(miles) = ",f15.1,"  Total Revenue($) = ",f9.1)') TotalVmt,TotalRev
      WRITE(5699,*)
   ENDIF

END SUBROUTINE

