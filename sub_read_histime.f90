SUBROUTINE READ_HIST_TIME
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~ Copyright (C) 2002-2011 The Arizona Board of Regents on Behalf of The University of Arizona.  ~~!
!~~ Developed by Yi-Chang Chiu, Ph.D. as par to the DynusT project.                               ~~!
!~~ This program is free software: you can redistribute it and/or modify it                       ~~!
!~~ under the terms of the GNU General Public License version 2 as published by                   ~~!
!~~ the Free Software Foundation.                                                                 ~~!
!~~ This program is distributed in the hope that it will be useful,                               ~~!
!~~ but WITHOUT ANY WARRANTY; without even the implied warranty of                                ~~!
!~~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                                          ~~!
!~~ See the GNU General Public License for more details.                                          ~~!
!~~ You should have received a copy of the GNU General Public License                             ~~!
!~~ along with this program (in the file COPYING).                                                ~~!
!~~ IF not, see <http://www.gnu.org/licenses/>.                                                   ~~!
!~~ Please see http://www.dynust.net for contact information and names                            ~~!
!~~ of individual contributors.                                                                   ~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!

! THE PURPOSE OF THIS SUBROUTINE IS TO SORT THE ALTTIME.DAT BASED ON VEHICLE ID.
! SINCE VHEICLE AND PATH.DAT ARE IN THE ASCENDING VEHICLE ID ORDER, IT IS IMPORTANT TO HAVE THE ALTTIME_HIST.DAT TO BE IN THE SAME ORDER
! THIS IS NEEDED WHEN READING THE HISTORICAL EXPERIENCE NODE ARRIVAL TIME.
USE DYNUST_MAIN_MODULE
USE DYNUST_VEH_PATH_ATT_MODULE

TYPE TIMESETUP
  REAL,POINTER::A(:)
END TYPE
INTEGER,ALLOCATABLE::id(:,:),ntmp(:)
TYPE(TIMESETUP), ALLOCATABLE::xtime(:)

ALLOCATE(id(MaxVehiclesFile,2))
id(:,:) = 0
ALLOCATE(ntmp(MaxVehiclesFile))
ntmp(:) = 0
ALLOCATE(xtime(MaxVehiclesFile))


 OPEN(file='AltTime_pre.dat',unit=561,status='old',iostat=error)
  IF(error /= 0) THEN
    WRITE(911,*) 'Error when opening AltTime.dat'
    STOP
  ENDIF
 OPEN(file='AltPath_pre.dat',unit=562,status='old',iostat=error)
  IF(error /= 0) THEN
    WRITE(911,*) 'Error when opening AltPath.dat'
    STOP
  ENDIF
 OPEN(file='AltTime_hist.dat',unit=560,status='unknown',iostat=error)
  IF(error /= 0) THEN
    WRITE(911,*) 'Error when opening AltTime_hist.dat'
    STOP
  ENDIF

  DO i = 1, MaxVehiclesFile
    id(i,2) = i
    READ(562,'(i10,i8,1000i7)') inodesum, id(i,1), ivehtype, ijorig, ijdest, ntmp(i)
    ALLOCATE(xtime(i)%A(ntmp(i)-1))
    READ(561,'(1000f8.2)') (xtime(i)%A(k),k=1,ntmp(i)-1)
  ENDDO
  
  CALL IMSLSort(id,MaxVehiclesFile,2)
  
  DO i = 1, MaxVehiclesFile
    WRITE(560,'(1000f8.2)') (xtime(id(i,2))%A(k),k=1,ntmp(id(i,2))-1)
  ENDDO
  
  DO i = 1, MaxVehiclesFile
    DEALLOCATE(xtime(i)%A)
  ENDDO
  
  DEALLOCATE(id,ntmp,xtime)
  
  CLOSE(561)
  REWIND(560)
END SUBROUTINE