; mark_description "Intel(R) Visual Fortran Compiler XE for applications running on IA-32, Version 13.1.1.171 Build 20130313";
; mark_description "-S";
	.686P
 	.387
	OPTION DOTNAME
	ASSUME	CS:FLAT,DS:FLAT,SS:FLAT
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
TXTST0:
; -- Begin  _WRITE_TOLL_REVENUE
; mark_begin;
IF @Version GE 800
  .MMX
ELSEIF @Version GE 612
  .MMX
  MMWORD TEXTEQU <QWORD>
ENDIF
IF @Version GE 800
  .XMM
ELSEIF @Version GE 614
  .XMM
  XMMWORD TEXTEQU <OWORD>
ENDIF
       ALIGN     16
	PUBLIC _WRITE_TOLL_REVENUE
_WRITE_TOLL_REVENUE	PROC NEAR 
; parameter 1: 8 + ebp
.B1.1:                          ; Preds .B1.0
        push      ebp                                           ;1.12
        mov       ebp, esp                                      ;1.12
        and       esp, -16                                      ;1.12
        push      esi                                           ;1.12
        push      edi                                           ;1.12
        push      ebx                                           ;1.12
        sub       esp, 276                                      ;1.12
        test      BYTE PTR [_DYNUST_MAIN_MODULE_mp_TOLLEXIST], 1 ;41.7
        je        .B1.290       ; Prob 60%                      ;41.7
                                ; LOE
.B1.2:                          ; Preds .B1.1
        mov       ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NTOLLLINK] ;41.7
        test      ebx, ebx                                      ;41.31
        jle       .B1.290       ; Prob 16%                      ;41.31
                                ; LOE ebx
.B1.3:                          ; Preds .B1.2
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ITERATION] ;42.5
        test      esi, esi                                      ;42.18
        jne       .B1.6         ; Prob 50%                      ;42.18
                                ; LOE ebx esi
.B1.4:                          ; Preds .B1.3
        mov       DWORD PTR [16+esp], 0                         ;42.24
        lea       edx, DWORD PTR [16+esp]                       ;42.24
        mov       DWORD PTR [esp], 15                           ;42.24
        lea       eax, DWORD PTR [esp]                          ;42.24
        mov       DWORD PTR [4+esp], OFFSET FLAT: __STRLITPACK_16 ;42.24
        mov       DWORD PTR [8+esp], 7                          ;42.24
        mov       DWORD PTR [12+esp], OFFSET FLAT: __STRLITPACK_15 ;42.24
        push      32                                            ;42.24
        push      eax                                           ;42.24
        push      OFFSET FLAT: __STRLITPACK_17.0.1              ;42.24
        push      -2088435968                                   ;42.24
        push      5699                                          ;42.24
        push      edx                                           ;42.24
        call      _for_open                                     ;42.24
                                ; LOE
.B1.330:                        ; Preds .B1.4
        add       esp, 24                                       ;42.24
                                ; LOE
.B1.5:                          ; Preds .B1.330
        mov       ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NTOLLLINK] ;46.5
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ITERATION] ;44.7
                                ; LOE ebx esi
.B1.6:                          ; Preds .B1.3 .B1.5
        xor       eax, eax                                      ;43.7
        mov       DWORD PTR [16+esp], eax                       ;43.7
        push      32                                            ;43.7
        push      eax                                           ;43.7
        push      OFFSET FLAT: __STRLITPACK_18.0.1              ;43.7
        push      -2088435968                                   ;43.7
        push      5699                                          ;43.7
        lea       edx, DWORD PTR [36+esp]                       ;43.7
        push      edx                                           ;43.7
        call      _for_write_seq_lis                            ;43.7
                                ; LOE ebx esi
.B1.7:                          ; Preds .B1.6
        mov       DWORD PTR [40+esp], 0                         ;44.7
        lea       eax, DWORD PTR [72+esp]                       ;44.7
        mov       DWORD PTR [72+esp], esi                       ;44.7
        push      32                                            ;44.7
        push      OFFSET FLAT: WRITE_TOLL_REVENUE$format_pack.0.1 ;44.7
        push      eax                                           ;44.7
        push      OFFSET FLAT: __STRLITPACK_19.0.1              ;44.7
        push      -2088435968                                   ;44.7
        push      5699                                          ;44.7
        lea       edx, DWORD PTR [64+esp]                       ;44.7
        push      edx                                           ;44.7
        call      _for_write_seq_fmt                            ;44.7
                                ; LOE ebx
.B1.331:                        ; Preds .B1.7
        add       esp, 52                                       ;44.7
                                ; LOE ebx
.B1.8:                          ; Preds .B1.331
        test      ebx, ebx                                      ;46.5
        pxor      xmm0, xmm0                                    ;
        pxor      xmm5, xmm5                                    ;
        movss     DWORD PTR [60+esp], xmm0                      ;
        jle       .B1.287       ; Prob 2%                       ;46.5
                                ; LOE ebx xmm5
.B1.9:                          ; Preds .B1.8
        movss     xmm2, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIMPERIOD] ;66.28
        mov       ecx, 1                                        ;
        movss     xmm3, DWORD PTR [_2il0floatpacket.2]          ;66.28
        andps     xmm3, xmm2                                    ;66.28
        pxor      xmm2, xmm3                                    ;66.28
        movss     xmm6, DWORD PTR [_2il0floatpacket.3]          ;66.28
        movaps    xmm7, xmm2                                    ;66.28
        movaps    xmm1, xmm2                                    ;66.28
        cmpltss   xmm7, xmm6                                    ;66.28
        imul      edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TOLLLINK+32], -52 ;
        imul      eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE+32], -44 ;
        imul      edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32], -152 ;
        andps     xmm6, xmm7                                    ;66.28
        movss     xmm4, DWORD PTR [_2il0floatpacket.1]          ;59.82
        addss     xmm1, xmm6                                    ;66.28
        add       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TOLLLINK] ;
        subss     xmm1, xmm6                                    ;66.28
        movaps    xmm0, xmm1                                    ;66.28
        add       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE] ;
        subss     xmm0, xmm2                                    ;66.28
        movss     xmm2, DWORD PTR [_2il0floatpacket.4]          ;66.28
        movaps    xmm6, xmm0                                    ;66.28
        movaps    xmm7, xmm2                                    ;66.28
        cmpnless  xmm6, xmm2                                    ;66.28
        addss     xmm7, xmm2                                    ;66.28
        cmpless   xmm0, DWORD PTR [_2il0floatpacket.5]          ;66.28
        andps     xmm6, xmm7                                    ;66.28
        andps     xmm0, xmm7                                    ;66.28
        add       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;
        subss     xmm1, xmm6                                    ;66.28
        mov       DWORD PTR [52+esp], 52                        ;
        addss     xmm1, xmm0                                    ;66.28
        orps      xmm1, xmm3                                    ;66.28
        cvtss2si  esi, xmm1                                     ;66.28
        mov       DWORD PTR [108+esp], esi                      ;66.28
        imul      esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32], -900 ;
        movss     DWORD PTR [80+esp], xmm5                      ;
        add       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;
        mov       DWORD PTR [124+esp], esi                      ;
        mov       DWORD PTR [56+esp], edi                       ;
        mov       DWORD PTR [64+esp], eax                       ;
        mov       DWORD PTR [68+esp], edx                       ;
        mov       DWORD PTR [72+esp], ecx                       ;
        mov       DWORD PTR [76+esp], ebx                       ;
        lea       ebx, DWORD PTR [16+esp]                       ;
        mov       esi, DWORD PTR [52+esp]                       ;
                                ; LOE ebx esi
.B1.10:                         ; Preds .B1.285 .B1.9
        mov       eax, DWORD PTR [68+esp]                       ;55.7
        mov       edx, DWORD PTR [8+esi+eax]                    ;55.7
        mov       eax, DWORD PTR [12+esi+eax]                   ;56.10
        test      eax, eax                                      ;56.27
        jle       .B1.327       ; Prob 16%                      ;56.27
                                ; LOE eax edx ebx esi
.B1.11:                         ; Preds .B1.10
        mov       ecx, DWORD PTR [68+esp]                       ;
        pxor      xmm1, xmm1                                    ;
        pxor      xmm2, xmm2                                    ;
        movss     DWORD PTR [100+esp], xmm1                     ;
        pxor      xmm3, xmm3                                    ;
        imul      edi, DWORD PTR [48+esi+ecx], -24              ;
        pxor      xmm6, xmm6                                    ;
        pxor      xmm7, xmm7                                    ;
        pxor      xmm4, xmm4                                    ;
        movss     DWORD PTR [88+esp], xmm2                      ;
        pxor      xmm5, xmm5                                    ;
        movss     DWORD PTR [92+esp], xmm3                      ;
        pxor      xmm0, xmm0                                    ;
        movss     DWORD PTR [84+esp], xmm6                      ;
        add       edi, DWORD PTR [16+esi+ecx]                   ;
        imul      ecx, edx, 152                                 ;55.7
        imul      edx, edx, 900                                 ;55.7
        movss     DWORD PTR [96+esp], xmm7                      ;
        movss     DWORD PTR [240+esp], xmm4                     ;
        movss     DWORD PTR [244+esp], xmm5                     ;
        movss     xmm2, DWORD PTR [88+esp]                      ;55.7
        mov       DWORD PTR [268+esp], edi                      ;
        mov       edi, DWORD PTR [56+esp]                       ;74.31
        movss     xmm4, DWORD PTR [84+esp]                      ;55.7
        movss     xmm5, DWORD PTR [92+esp]                      ;55.7
        movss     xmm1, DWORD PTR [20+ecx+edi]                  ;74.31
        movss     DWORD PTR [236+esp], xmm1                     ;55.7
        mov       DWORD PTR [264+esp], 1                        ;
        mov       DWORD PTR [116+esp], edx                      ;55.7
        mov       DWORD PTR [228+esp], eax                      ;55.7
        mov       DWORD PTR [52+esp], esi                       ;55.7
        movss     xmm1, DWORD PTR [96+esp]                      ;55.7
        movss     xmm3, DWORD PTR [100+esp]                     ;55.7
                                ; LOE xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.12:                         ; Preds .B1.273 .B1.11
        mov       eax, DWORD PTR [264+esp]                      ;59.82
        mov       edx, DWORD PTR [268+esp]                      ;59.46
        lea       ecx, DWORD PTR [eax+eax*2]                    ;59.82
        mov       DWORD PTR [224+esp], ecx                      ;59.82
        movss     xmm7, DWORD PTR [edx+ecx*8]                   ;59.46
        cvttss2si ebx, xmm7                                     ;59.46
        cvtsi2ss  xmm6, ebx                                     ;59.46
        mov       DWORD PTR [220+esp], ebx                      ;59.46
        subss     xmm7, xmm6                                    ;59.45
        comiss    xmm7, DWORD PTR [_2il0floatpacket.1]          ;59.82
        jbe       .B1.14        ; Prob 50%                      ;59.82
                                ; LOE ecx ebx cl bl ch bh xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.13:                         ; Preds .B1.12
        mov       edx, ebx                                      ;60.15
        xor       eax, eax                                      ;60.15
        test      edx, edx                                      ;60.15
        cmovl     edx, eax                                      ;60.15
        inc       edx                                           ;60.15
        mov       DWORD PTR [220+esp], edx                      ;60.15
        jmp       .B1.15        ; Prob 100%                     ;60.15
                                ; LOE ecx cl ch xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.14:                         ; Preds .B1.12
        mov       edx, ebx                                      ;62.15
        mov       eax, 1                                        ;62.15
        test      edx, edx                                      ;62.15
        cmovle    edx, eax                                      ;62.15
        mov       DWORD PTR [220+esp], edx                      ;62.15
                                ; LOE ecx cl ch xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.15:                         ; Preds .B1.13 .B1.14
        mov       eax, DWORD PTR [268+esp]                      ;65.46
        mov       edx, ecx                                      ;65.46
        movss     xmm7, DWORD PTR [4+eax+edx*8]                 ;65.46
        cvttss2si eax, xmm7                                     ;65.46
        cvtsi2ss  xmm6, eax                                     ;65.46
        subss     xmm7, xmm6                                    ;65.45
        comiss    xmm7, DWORD PTR [_2il0floatpacket.1]          ;65.82
        jbe       .B1.17        ; Prob 50%                      ;65.82
                                ; LOE eax xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.16:                         ; Preds .B1.15
        inc       eax                                           ;66.79
        mov       edx, DWORD PTR [108+esp]                      ;66.15
        cmp       eax, edx                                      ;66.15
        cmovge    eax, edx                                      ;66.15
        jmp       .B1.18        ; Prob 100%                     ;66.15
                                ; LOE eax xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.17:                         ; Preds .B1.15
        mov       edx, DWORD PTR [108+esp]                      ;68.15
        cmp       eax, edx                                      ;68.15
        cmovge    eax, edx                                      ;68.15
                                ; LOE eax xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.18:                         ; Preds .B1.16 .B1.17
        sub       eax, DWORD PTR [220+esp]                      ;70.37
        inc       eax                                           ;70.72
        test      eax, eax                                      ;70.37
        jle       .B1.47        ; Prob 50%                      ;70.37
                                ; LOE eax xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.19:                         ; Preds .B1.18
        cmp       eax, 4                                        ;70.37
        jl        .B1.291       ; Prob 10%                      ;70.37
                                ; LOE eax xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.20:                         ; Preds .B1.19
        mov       ecx, DWORD PTR [124+esp]                      ;70.37
        mov       edx, DWORD PTR [116+esp]                      ;70.37
        mov       esi, DWORD PTR [560+edx+ecx]                  ;70.37
        neg       esi                                           ;70.37
        mov       ebx, DWORD PTR [528+edx+ecx]                  ;70.37
        add       esi, DWORD PTR [220+esp]                      ;70.37
        lea       esi, DWORD PTR [ebx+esi*4]                    ;70.37
        mov       ebx, esi                                      ;70.37
        and       ebx, 15                                       ;70.37
        je        .B1.23        ; Prob 50%                      ;70.37
                                ; LOE eax ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.21:                         ; Preds .B1.20
        test      bl, 3                                         ;70.37
        jne       .B1.291       ; Prob 10%                      ;70.37
                                ; LOE eax ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.22:                         ; Preds .B1.21
        neg       ebx                                           ;70.37
        add       ebx, 16                                       ;70.37
        shr       ebx, 2                                        ;70.37
                                ; LOE eax ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.23:                         ; Preds .B1.22 .B1.20
        lea       edx, DWORD PTR [4+ebx]                        ;70.37
        cmp       eax, edx                                      ;70.37
        jl        .B1.291       ; Prob 10%                      ;70.37
                                ; LOE eax ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.24:                         ; Preds .B1.23
        mov       edx, eax                                      ;70.37
        xor       ecx, ecx                                      ;
        sub       edx, ebx                                      ;70.37
        and       edx, 3                                        ;70.37
        neg       edx                                           ;70.37
        add       edx, eax                                      ;70.37
        test      ebx, ebx                                      ;70.37
        jbe       .B1.28        ; Prob 10%                      ;70.37
                                ; LOE eax edx ecx ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.25:                         ; Preds .B1.24
        xor       edi, edi                                      ;
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.26:                         ; Preds .B1.26 .B1.25
        add       ecx, DWORD PTR [esi+edi*4]                    ;70.37
        inc       edi                                           ;70.37
        cmp       edi, ebx                                      ;70.37
        jb        .B1.26        ; Prob 82%                      ;70.37
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.28:                         ; Preds .B1.26 .B1.24
        movd      xmm6, ecx                                     ;70.37
                                ; LOE eax edx ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5 xmm6
.B1.29:                         ; Preds .B1.29 .B1.28
        paddd     xmm6, XMMWORD PTR [esi+ebx*4]                 ;70.37
        add       ebx, 4                                        ;70.37
        cmp       ebx, edx                                      ;70.37
        jb        .B1.29        ; Prob 82%                      ;70.37
                                ; LOE eax edx ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5 xmm6
.B1.30:                         ; Preds .B1.29
        movdqa    xmm7, xmm6                                    ;70.37
        psrldq    xmm7, 8                                       ;70.37
        paddd     xmm6, xmm7                                    ;70.37
        movdqa    xmm7, xmm6                                    ;70.37
        psrldq    xmm7, 4                                       ;70.37
        paddd     xmm6, xmm7                                    ;70.37
        movd      ebx, xmm6                                     ;70.37
                                ; LOE eax edx ebx xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.31:                         ; Preds .B1.30 .B1.291
        cmp       edx, eax                                      ;70.37
        jae       .B1.35        ; Prob 10%                      ;70.37
                                ; LOE eax edx ebx xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.32:                         ; Preds .B1.31
        mov       edi, DWORD PTR [124+esp]                      ;70.37
        mov       esi, DWORD PTR [116+esp]                      ;70.37
        mov       ecx, DWORD PTR [560+esi+edi]                  ;70.37
        neg       ecx                                           ;
        mov       esi, DWORD PTR [528+esi+edi]                  ;70.37
        add       ecx, DWORD PTR [220+esp]                      ;
        lea       ecx, DWORD PTR [esi+ecx*4]                    ;
                                ; LOE eax edx ecx ebx xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.33:                         ; Preds .B1.33 .B1.32
        add       ebx, DWORD PTR [ecx+edx*4]                    ;70.37
        inc       edx                                           ;70.37
        cmp       edx, eax                                      ;70.37
        jb        .B1.33        ; Prob 82%                      ;70.37
                                ; LOE eax edx ecx ebx xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.35:                         ; Preds .B1.33 .B1.31
        cmp       eax, 4                                        ;70.96
        jl        .B1.48        ; Prob 10%                      ;70.96
                                ; LOE eax ebx xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.36:                         ; Preds .B1.35
        mov       ecx, DWORD PTR [124+esp]                      ;70.96
        mov       edx, DWORD PTR [116+esp]                      ;70.96
        mov       edi, DWORD PTR [220+esp]                      ;70.96
        sub       edi, DWORD PTR [632+edx+ecx]                  ;70.96
        mov       esi, DWORD PTR [600+edx+ecx]                  ;70.96
        lea       esi, DWORD PTR [esi+edi*4]                    ;70.96
        mov       DWORD PTR [188+esp], esi                      ;70.96
        and       esi, 15                                       ;70.96
        je        .B1.39        ; Prob 50%                      ;70.96
                                ; LOE eax ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.37:                         ; Preds .B1.36
        test      esi, 3                                        ;70.96
        jne       .B1.48        ; Prob 10%                      ;70.96
                                ; LOE eax ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.38:                         ; Preds .B1.37
        neg       esi                                           ;70.96
        add       esi, 16                                       ;70.96
        shr       esi, 2                                        ;70.96
                                ; LOE eax ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.39:                         ; Preds .B1.38 .B1.36
        lea       edx, DWORD PTR [4+esi]                        ;70.96
        cmp       eax, edx                                      ;70.96
        jl        .B1.48        ; Prob 10%                      ;70.96
                                ; LOE eax ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.40:                         ; Preds .B1.39
        mov       edx, eax                                      ;70.96
        xor       ecx, ecx                                      ;
        sub       edx, esi                                      ;70.96
        and       edx, 3                                        ;70.96
        neg       edx                                           ;70.96
        add       edx, eax                                      ;70.96
        test      esi, esi                                      ;70.96
        jbe       .B1.44        ; Prob 10%                      ;70.96
                                ; LOE eax edx ecx ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.41:                         ; Preds .B1.40
        xor       edi, edi                                      ;
        mov       DWORD PTR [132+esp], eax                      ;
        mov       eax, edi                                      ;
        mov       edi, DWORD PTR [188+esp]                      ;
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.42:                         ; Preds .B1.42 .B1.41
        add       ecx, DWORD PTR [edi+eax*4]                    ;70.96
        inc       eax                                           ;70.96
        cmp       eax, esi                                      ;70.96
        jb        .B1.42        ; Prob 82%                      ;70.96
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.43:                         ; Preds .B1.42
        mov       eax, DWORD PTR [132+esp]                      ;
                                ; LOE eax edx ecx ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.44:                         ; Preds .B1.40 .B1.43
        movd      xmm6, ecx                                     ;70.96
        mov       ecx, DWORD PTR [188+esp]                      ;70.96
                                ; LOE eax edx ecx ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5 xmm6
.B1.45:                         ; Preds .B1.45 .B1.44
        paddd     xmm6, XMMWORD PTR [ecx+esi*4]                 ;70.96
        add       esi, 4                                        ;70.96
        cmp       esi, edx                                      ;70.96
        jb        .B1.45        ; Prob 82%                      ;70.96
                                ; LOE eax edx ecx ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5 xmm6
.B1.46:                         ; Preds .B1.45
        movdqa    xmm7, xmm6                                    ;70.96
        psrldq    xmm7, 8                                       ;70.96
        paddd     xmm6, xmm7                                    ;70.96
        movdqa    xmm7, xmm6                                    ;70.96
        psrldq    xmm7, 4                                       ;70.96
        paddd     xmm6, xmm7                                    ;70.96
        movd      ecx, xmm6                                     ;70.96
        jmp       .B1.49        ; Prob 100%                     ;70.96
                                ; LOE eax edx ecx ebx xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.47:                         ; Preds .B1.18
        mov       ebx, 0                                        ;
        jle       .B1.66        ; Prob 50%                      ;70.96
                                ; LOE eax ebx xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.48:                         ; Preds .B1.39 .B1.37 .B1.47 .B1.35
        xor       ecx, ecx                                      ;
        xor       edx, edx                                      ;
                                ; LOE eax edx ecx ebx xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.49:                         ; Preds .B1.46 .B1.48
        cmp       edx, eax                                      ;70.96
        jae       .B1.53        ; Prob 10%                      ;70.96
                                ; LOE eax edx ecx ebx xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.50:                         ; Preds .B1.49
        mov       edi, DWORD PTR [124+esp]                      ;
        mov       esi, DWORD PTR [116+esp]                      ;
        mov       DWORD PTR [196+esp], ecx                      ;
        mov       ecx, DWORD PTR [220+esp]                      ;
        sub       ecx, DWORD PTR [632+esi+edi]                  ;
        mov       esi, DWORD PTR [600+esi+edi]                  ;70.96
        lea       esi, DWORD PTR [esi+ecx*4]                    ;
        mov       ecx, DWORD PTR [196+esp]                      ;
                                ; LOE eax edx ecx ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.51:                         ; Preds .B1.51 .B1.50
        add       ecx, DWORD PTR [esi+edx*4]                    ;70.96
        inc       edx                                           ;70.96
        cmp       edx, eax                                      ;70.96
        jb        .B1.51        ; Prob 82%                      ;70.96
                                ; LOE eax edx ecx ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.53:                         ; Preds .B1.51 .B1.49
        test      eax, eax                                      ;70.156
        jle       .B1.135       ; Prob 50%                      ;70.156
                                ; LOE eax ecx ebx xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.54:                         ; Preds .B1.53
        cmp       eax, 4                                        ;70.156
        jl        .B1.67        ; Prob 10%                      ;70.156
                                ; LOE eax ecx ebx xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.55:                         ; Preds .B1.54
        mov       edi, DWORD PTR [124+esp]                      ;70.156
        mov       esi, DWORD PTR [116+esp]                      ;70.156
        mov       edx, DWORD PTR [220+esp]                      ;70.156
        sub       edx, DWORD PTR [596+esi+edi]                  ;70.156
        mov       esi, DWORD PTR [564+esi+edi]                  ;70.156
        lea       esi, DWORD PTR [esi+edx*4]                    ;70.156
        mov       DWORD PTR [156+esp], esi                      ;70.156
        and       esi, 15                                       ;70.156
        je        .B1.58        ; Prob 50%                      ;70.156
                                ; LOE eax ecx ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.56:                         ; Preds .B1.55
        test      esi, 3                                        ;70.156
        jne       .B1.67        ; Prob 10%                      ;70.156
                                ; LOE eax ecx ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.57:                         ; Preds .B1.56
        neg       esi                                           ;70.156
        add       esi, 16                                       ;70.156
        shr       esi, 2                                        ;70.156
                                ; LOE eax ecx ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.58:                         ; Preds .B1.57 .B1.55
        lea       edx, DWORD PTR [4+esi]                        ;70.156
        cmp       eax, edx                                      ;70.156
        jl        .B1.67        ; Prob 10%                      ;70.156
                                ; LOE eax ecx ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.59:                         ; Preds .B1.58
        mov       edx, eax                                      ;70.156
        xor       edi, edi                                      ;
        sub       edx, esi                                      ;70.156
        and       edx, 3                                        ;70.156
        neg       edx                                           ;70.156
        add       edx, eax                                      ;70.156
        mov       DWORD PTR [260+esp], edi                      ;
        test      esi, esi                                      ;70.156
        jbe       .B1.63        ; Prob 10%                      ;70.156
                                ; LOE eax edx ecx ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.60:                         ; Preds .B1.59
        xor       edi, edi                                      ;
        mov       DWORD PTR [140+esp], ebx                      ;
        mov       DWORD PTR [132+esp], eax                      ;
        mov       eax, edi                                      ;
        mov       ebx, DWORD PTR [260+esp]                      ;
        mov       edi, DWORD PTR [156+esp]                      ;
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.61:                         ; Preds .B1.61 .B1.60
        add       ebx, DWORD PTR [edi+eax*4]                    ;70.156
        inc       eax                                           ;70.156
        cmp       eax, esi                                      ;70.156
        jb        .B1.61        ; Prob 82%                      ;70.156
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.62:                         ; Preds .B1.61
        mov       DWORD PTR [260+esp], ebx                      ;
        mov       ebx, DWORD PTR [140+esp]                      ;
        mov       eax, DWORD PTR [132+esp]                      ;
                                ; LOE eax edx ecx ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.63:                         ; Preds .B1.59 .B1.62
        movd      xmm6, DWORD PTR [260+esp]                     ;70.156
        mov       edi, DWORD PTR [156+esp]                      ;70.156
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5 xmm6
.B1.64:                         ; Preds .B1.64 .B1.63
        paddd     xmm6, XMMWORD PTR [edi+esi*4]                 ;70.156
        add       esi, 4                                        ;70.156
        cmp       esi, edx                                      ;70.156
        jb        .B1.64        ; Prob 82%                      ;70.156
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5 xmm6
.B1.65:                         ; Preds .B1.64
        movdqa    xmm7, xmm6                                    ;70.156
        psrldq    xmm7, 8                                       ;70.156
        paddd     xmm6, xmm7                                    ;70.156
        movdqa    xmm7, xmm6                                    ;70.156
        psrldq    xmm7, 4                                       ;70.156
        paddd     xmm6, xmm7                                    ;70.156
        movd      DWORD PTR [252+esp], xmm6                     ;70.156
        jmp       .B1.68        ; Prob 100%                     ;70.156
                                ; LOE eax edx ecx ebx xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.66:                         ; Preds .B1.47
        mov       ecx, 0                                        ;
        jle       .B1.135       ; Prob 50%                      ;70.156
                                ; LOE eax ecx ebx xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.67:                         ; Preds .B1.58 .B1.56 .B1.66 .B1.54
        xor       edx, edx                                      ;
        mov       DWORD PTR [252+esp], edx                      ;
                                ; LOE eax edx ecx ebx xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.68:                         ; Preds .B1.65 .B1.67
        cmp       edx, eax                                      ;70.156
        jae       .B1.72        ; Prob 10%                      ;70.156
                                ; LOE eax edx ecx ebx xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.69:                         ; Preds .B1.68
        mov       edi, DWORD PTR [124+esp]                      ;
        mov       esi, DWORD PTR [116+esp]                      ;
        mov       DWORD PTR [200+esp], edx                      ;
        mov       edx, DWORD PTR [220+esp]                      ;
        sub       edx, DWORD PTR [596+esi+edi]                  ;
        mov       esi, DWORD PTR [564+esi+edi]                  ;70.156
        mov       edi, DWORD PTR [252+esp]                      ;
        lea       esi, DWORD PTR [esi+edx*4]                    ;
        mov       edx, DWORD PTR [200+esp]                      ;
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.70:                         ; Preds .B1.70 .B1.69
        add       edi, DWORD PTR [esi+edx*4]                    ;70.156
        inc       edx                                           ;70.156
        cmp       edx, eax                                      ;70.156
        jb        .B1.70        ; Prob 82%                      ;70.156
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.71:                         ; Preds .B1.70
        mov       DWORD PTR [252+esp], edi                      ;
                                ; LOE eax ecx ebx xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.72:                         ; Preds .B1.71 .B1.68
        sub       ebx, ecx                                      ;70.37
        xor       edx, edx                                      ;70.13
        sub       ebx, DWORD PTR [252+esp]                      ;70.37
        cmovs     ebx, edx                                      ;70.13
        test      eax, eax                                      ;71.31
        cvtsi2ss  xmm6, ebx                                     ;70.31
        addss     xmm2, xmm6                                    ;70.13
        jle       .B1.138       ; Prob 50%                      ;71.31
                                ; LOE eax xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.73:                         ; Preds .B1.72
        cmp       eax, 4                                        ;71.31
        jl        .B1.298       ; Prob 10%                      ;71.31
                                ; LOE eax xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.74:                         ; Preds .B1.73
        mov       ecx, DWORD PTR [124+esp]                      ;71.31
        mov       edx, DWORD PTR [116+esp]                      ;71.31
        mov       esi, DWORD PTR [220+esp]                      ;71.31
        sub       esi, DWORD PTR [632+edx+ecx]                  ;71.31
        mov       ebx, DWORD PTR [600+edx+ecx]                  ;71.31
        lea       ebx, DWORD PTR [ebx+esi*4]                    ;71.31
        mov       esi, ebx                                      ;71.31
        and       esi, 15                                       ;71.31
        je        .B1.77        ; Prob 50%                      ;71.31
                                ; LOE eax ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.75:                         ; Preds .B1.74
        test      esi, 3                                        ;71.31
        jne       .B1.298       ; Prob 10%                      ;71.31
                                ; LOE eax ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.76:                         ; Preds .B1.75
        neg       esi                                           ;71.31
        add       esi, 16                                       ;71.31
        shr       esi, 2                                        ;71.31
                                ; LOE eax ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.77:                         ; Preds .B1.76 .B1.74
        lea       edx, DWORD PTR [4+esi]                        ;71.31
        cmp       eax, edx                                      ;71.31
        jl        .B1.298       ; Prob 10%                      ;71.31
                                ; LOE eax ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.78:                         ; Preds .B1.77
        mov       edx, eax                                      ;71.31
        xor       ecx, ecx                                      ;
        sub       edx, esi                                      ;71.31
        and       edx, 3                                        ;71.31
        neg       edx                                           ;71.31
        add       edx, eax                                      ;71.31
        test      esi, esi                                      ;71.31
        jbe       .B1.82        ; Prob 10%                      ;71.31
                                ; LOE eax edx ecx ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.79:                         ; Preds .B1.78
        xor       edi, edi                                      ;
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.80:                         ; Preds .B1.80 .B1.79
        add       ecx, DWORD PTR [ebx+edi*4]                    ;71.31
        inc       edi                                           ;71.31
        cmp       edi, esi                                      ;71.31
        jb        .B1.80        ; Prob 82%                      ;71.31
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.82:                         ; Preds .B1.80 .B1.78
        movd      xmm6, ecx                                     ;71.31
                                ; LOE eax edx ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5 xmm6
.B1.83:                         ; Preds .B1.83 .B1.82
        paddd     xmm6, XMMWORD PTR [ebx+esi*4]                 ;71.31
        add       esi, 4                                        ;71.31
        cmp       esi, edx                                      ;71.31
        jb        .B1.83        ; Prob 82%                      ;71.31
                                ; LOE eax edx ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5 xmm6
.B1.84:                         ; Preds .B1.83
        movdqa    xmm7, xmm6                                    ;71.31
        psrldq    xmm7, 8                                       ;71.31
        paddd     xmm6, xmm7                                    ;71.31
        movdqa    xmm7, xmm6                                    ;71.31
        psrldq    xmm7, 4                                       ;71.31
        paddd     xmm6, xmm7                                    ;71.31
        movd      ebx, xmm6                                     ;71.31
                                ; LOE eax edx ebx xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.85:                         ; Preds .B1.84 .B1.298
        cmp       edx, eax                                      ;71.31
        jae       .B1.89        ; Prob 10%                      ;71.31
                                ; LOE eax edx ebx xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.86:                         ; Preds .B1.85
        mov       edi, DWORD PTR [124+esp]                      ;
        mov       esi, DWORD PTR [116+esp]                      ;
        mov       ecx, DWORD PTR [220+esp]                      ;
        sub       ecx, DWORD PTR [632+esi+edi]                  ;
        mov       esi, DWORD PTR [600+esi+edi]                  ;71.31
        lea       ecx, DWORD PTR [esi+ecx*4]                    ;
                                ; LOE eax edx ecx ebx xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.87:                         ; Preds .B1.87 .B1.86
        add       ebx, DWORD PTR [ecx+edx*4]                    ;71.31
        inc       edx                                           ;71.31
        cmp       edx, eax                                      ;71.31
        jb        .B1.87        ; Prob 82%                      ;71.31
                                ; LOE eax edx ecx ebx xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.89:                         ; Preds .B1.87 .B1.85
        cvtsi2ss  xmm6, ebx                                     ;71.31
        test      eax, eax                                      ;72.31
        addss     xmm4, xmm6                                    ;71.13
        jle       .B1.138       ; Prob 50%                      ;72.31
                                ; LOE eax xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.90:                         ; Preds .B1.89
        cmp       eax, 4                                        ;72.31
        jl        .B1.299       ; Prob 10%                      ;72.31
                                ; LOE eax xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.91:                         ; Preds .B1.90
        mov       ecx, DWORD PTR [124+esp]                      ;72.31
        mov       edx, DWORD PTR [116+esp]                      ;72.31
        mov       esi, DWORD PTR [220+esp]                      ;72.31
        sub       esi, DWORD PTR [596+edx+ecx]                  ;72.31
        mov       ebx, DWORD PTR [564+edx+ecx]                  ;72.31
        lea       ebx, DWORD PTR [ebx+esi*4]                    ;72.31
        mov       esi, ebx                                      ;72.31
        and       esi, 15                                       ;72.31
        je        .B1.94        ; Prob 50%                      ;72.31
                                ; LOE eax ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.92:                         ; Preds .B1.91
        test      esi, 3                                        ;72.31
        jne       .B1.299       ; Prob 10%                      ;72.31
                                ; LOE eax ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.93:                         ; Preds .B1.92
        neg       esi                                           ;72.31
        add       esi, 16                                       ;72.31
        shr       esi, 2                                        ;72.31
                                ; LOE eax ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.94:                         ; Preds .B1.93 .B1.91
        lea       edx, DWORD PTR [4+esi]                        ;72.31
        cmp       eax, edx                                      ;72.31
        jl        .B1.299       ; Prob 10%                      ;72.31
                                ; LOE eax ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.95:                         ; Preds .B1.94
        mov       edx, eax                                      ;72.31
        xor       ecx, ecx                                      ;
        sub       edx, esi                                      ;72.31
        and       edx, 3                                        ;72.31
        neg       edx                                           ;72.31
        add       edx, eax                                      ;72.31
        test      esi, esi                                      ;72.31
        jbe       .B1.99        ; Prob 10%                      ;72.31
                                ; LOE eax edx ecx ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.96:                         ; Preds .B1.95
        xor       edi, edi                                      ;
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.97:                         ; Preds .B1.97 .B1.96
        add       ecx, DWORD PTR [ebx+edi*4]                    ;72.31
        inc       edi                                           ;72.31
        cmp       edi, esi                                      ;72.31
        jb        .B1.97        ; Prob 82%                      ;72.31
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.99:                         ; Preds .B1.97 .B1.95
        movd      xmm6, ecx                                     ;72.31
                                ; LOE eax edx ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5 xmm6
.B1.100:                        ; Preds .B1.100 .B1.99
        paddd     xmm6, XMMWORD PTR [ebx+esi*4]                 ;72.31
        add       esi, 4                                        ;72.31
        cmp       esi, edx                                      ;72.31
        jb        .B1.100       ; Prob 82%                      ;72.31
                                ; LOE eax edx ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5 xmm6
.B1.101:                        ; Preds .B1.100
        movdqa    xmm7, xmm6                                    ;72.31
        psrldq    xmm7, 8                                       ;72.31
        paddd     xmm6, xmm7                                    ;72.31
        movdqa    xmm7, xmm6                                    ;72.31
        psrldq    xmm7, 4                                       ;72.31
        paddd     xmm6, xmm7                                    ;72.31
        movd      ebx, xmm6                                     ;72.31
                                ; LOE eax edx ebx xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.102:                        ; Preds .B1.101 .B1.299
        cmp       edx, eax                                      ;72.31
        jae       .B1.106       ; Prob 10%                      ;72.31
                                ; LOE eax edx ebx xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.103:                        ; Preds .B1.102
        mov       edi, DWORD PTR [124+esp]                      ;
        mov       esi, DWORD PTR [116+esp]                      ;
        mov       ecx, DWORD PTR [220+esp]                      ;
        sub       ecx, DWORD PTR [596+esi+edi]                  ;
        mov       esi, DWORD PTR [564+esi+edi]                  ;72.31
        lea       ecx, DWORD PTR [esi+ecx*4]                    ;
                                ; LOE eax edx ecx ebx xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.104:                        ; Preds .B1.104 .B1.103
        add       ebx, DWORD PTR [ecx+edx*4]                    ;72.31
        inc       edx                                           ;72.31
        cmp       edx, eax                                      ;72.31
        jb        .B1.104       ; Prob 82%                      ;72.31
                                ; LOE eax edx ecx ebx xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.106:                        ; Preds .B1.104 .B1.102
        cvtsi2ss  xmm6, ebx                                     ;72.31
        test      eax, eax                                      ;74.66
        addss     xmm5, xmm6                                    ;72.13
        jle       .B1.138       ; Prob 50%                      ;74.66
                                ; LOE eax xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.107:                        ; Preds .B1.106
        cmp       eax, 4                                        ;74.66
        jl        .B1.300       ; Prob 10%                      ;74.66
                                ; LOE eax xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.108:                        ; Preds .B1.107
        mov       ecx, DWORD PTR [124+esp]                      ;74.66
        mov       edx, DWORD PTR [116+esp]                      ;74.66
        mov       esi, DWORD PTR [560+edx+ecx]                  ;74.66
        neg       esi                                           ;74.66
        mov       ebx, DWORD PTR [528+edx+ecx]                  ;74.66
        add       esi, DWORD PTR [220+esp]                      ;74.66
        lea       ebx, DWORD PTR [ebx+esi*4]                    ;74.66
        mov       esi, ebx                                      ;74.66
        and       esi, 15                                       ;74.66
        je        .B1.111       ; Prob 50%                      ;74.66
                                ; LOE eax ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.109:                        ; Preds .B1.108
        test      esi, 3                                        ;74.66
        jne       .B1.300       ; Prob 10%                      ;74.66
                                ; LOE eax ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.110:                        ; Preds .B1.109
        neg       esi                                           ;74.66
        add       esi, 16                                       ;74.66
        shr       esi, 2                                        ;74.66
                                ; LOE eax ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.111:                        ; Preds .B1.110 .B1.108
        lea       edx, DWORD PTR [4+esi]                        ;74.66
        cmp       eax, edx                                      ;74.66
        jl        .B1.300       ; Prob 10%                      ;74.66
                                ; LOE eax ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.112:                        ; Preds .B1.111
        mov       edx, eax                                      ;74.66
        xor       ecx, ecx                                      ;
        sub       edx, esi                                      ;74.66
        and       edx, 3                                        ;74.66
        neg       edx                                           ;74.66
        add       edx, eax                                      ;74.66
        test      esi, esi                                      ;74.66
        jbe       .B1.116       ; Prob 10%                      ;74.66
                                ; LOE eax edx ecx ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.113:                        ; Preds .B1.112
        xor       edi, edi                                      ;
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.114:                        ; Preds .B1.114 .B1.113
        add       ecx, DWORD PTR [ebx+edi*4]                    ;74.66
        inc       edi                                           ;74.66
        cmp       edi, esi                                      ;74.66
        jb        .B1.114       ; Prob 82%                      ;74.66
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.116:                        ; Preds .B1.114 .B1.112
        movd      xmm6, ecx                                     ;74.66
                                ; LOE eax edx ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5 xmm6
.B1.117:                        ; Preds .B1.117 .B1.116
        paddd     xmm6, XMMWORD PTR [ebx+esi*4]                 ;74.66
        add       esi, 4                                        ;74.66
        cmp       esi, edx                                      ;74.66
        jb        .B1.117       ; Prob 82%                      ;74.66
                                ; LOE eax edx ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5 xmm6
.B1.118:                        ; Preds .B1.117
        movdqa    xmm7, xmm6                                    ;74.66
        psrldq    xmm7, 8                                       ;74.66
        paddd     xmm6, xmm7                                    ;74.66
        movdqa    xmm7, xmm6                                    ;74.66
        psrldq    xmm7, 4                                       ;74.66
        paddd     xmm6, xmm7                                    ;74.66
        movd      ebx, xmm6                                     ;74.66
                                ; LOE eax edx ebx xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.119:                        ; Preds .B1.118 .B1.300
        cmp       edx, eax                                      ;74.66
        jae       .B1.123       ; Prob 10%                      ;74.66
                                ; LOE eax edx ebx xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.120:                        ; Preds .B1.119
        mov       edi, DWORD PTR [124+esp]                      ;74.66
        mov       esi, DWORD PTR [116+esp]                      ;74.66
        mov       ecx, DWORD PTR [560+esi+edi]                  ;74.66
        neg       ecx                                           ;
        mov       esi, DWORD PTR [528+esi+edi]                  ;74.66
        add       ecx, DWORD PTR [220+esp]                      ;
        lea       ecx, DWORD PTR [esi+ecx*4]                    ;
                                ; LOE eax edx ecx ebx xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.121:                        ; Preds .B1.121 .B1.120
        add       ebx, DWORD PTR [ecx+edx*4]                    ;74.66
        inc       edx                                           ;74.66
        cmp       edx, eax                                      ;74.66
        jb        .B1.121       ; Prob 82%                      ;74.66
                                ; LOE eax edx ecx ebx xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.123:                        ; Preds .B1.121 .B1.119
        cmp       eax, 4                                        ;74.125
        jl        .B1.139       ; Prob 10%                      ;74.125
                                ; LOE eax ebx xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.124:                        ; Preds .B1.123
        mov       ecx, DWORD PTR [124+esp]                      ;74.125
        mov       edx, DWORD PTR [116+esp]                      ;74.125
        mov       edi, DWORD PTR [220+esp]                      ;74.125
        sub       edi, DWORD PTR [632+edx+ecx]                  ;74.125
        mov       esi, DWORD PTR [600+edx+ecx]                  ;74.125
        lea       esi, DWORD PTR [esi+edi*4]                    ;74.125
        mov       DWORD PTR [192+esp], esi                      ;74.125
        and       esi, 15                                       ;74.125
        je        .B1.127       ; Prob 50%                      ;74.125
                                ; LOE eax ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.125:                        ; Preds .B1.124
        test      esi, 3                                        ;74.125
        jne       .B1.139       ; Prob 10%                      ;74.125
                                ; LOE eax ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.126:                        ; Preds .B1.125
        neg       esi                                           ;74.125
        add       esi, 16                                       ;74.125
        shr       esi, 2                                        ;74.125
                                ; LOE eax ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.127:                        ; Preds .B1.126 .B1.124
        lea       edx, DWORD PTR [4+esi]                        ;74.125
        cmp       eax, edx                                      ;74.125
        jl        .B1.139       ; Prob 10%                      ;74.125
                                ; LOE eax ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.128:                        ; Preds .B1.127
        mov       edx, eax                                      ;74.125
        xor       ecx, ecx                                      ;
        sub       edx, esi                                      ;74.125
        and       edx, 3                                        ;74.125
        neg       edx                                           ;74.125
        add       edx, eax                                      ;74.125
        test      esi, esi                                      ;74.125
        jbe       .B1.132       ; Prob 10%                      ;74.125
                                ; LOE eax edx ecx ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.129:                        ; Preds .B1.128
        xor       edi, edi                                      ;
        mov       DWORD PTR [132+esp], eax                      ;
        mov       eax, edi                                      ;
        mov       edi, DWORD PTR [192+esp]                      ;
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.130:                        ; Preds .B1.130 .B1.129
        add       ecx, DWORD PTR [edi+eax*4]                    ;74.125
        inc       eax                                           ;74.125
        cmp       eax, esi                                      ;74.125
        jb        .B1.130       ; Prob 82%                      ;74.125
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.131:                        ; Preds .B1.130
        mov       eax, DWORD PTR [132+esp]                      ;
                                ; LOE eax edx ecx ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.132:                        ; Preds .B1.128 .B1.131
        movd      xmm6, ecx                                     ;74.125
        mov       ecx, DWORD PTR [192+esp]                      ;74.125
                                ; LOE eax edx ecx ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5 xmm6
.B1.133:                        ; Preds .B1.133 .B1.132
        paddd     xmm6, XMMWORD PTR [ecx+esi*4]                 ;74.125
        add       esi, 4                                        ;74.125
        cmp       esi, edx                                      ;74.125
        jb        .B1.133       ; Prob 82%                      ;74.125
                                ; LOE eax edx ecx ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5 xmm6
.B1.134:                        ; Preds .B1.133
        movdqa    xmm7, xmm6                                    ;74.125
        psrldq    xmm7, 8                                       ;74.125
        paddd     xmm6, xmm7                                    ;74.125
        movdqa    xmm7, xmm6                                    ;74.125
        psrldq    xmm7, 4                                       ;74.125
        paddd     xmm6, xmm7                                    ;74.125
        movd      ecx, xmm6                                     ;74.125
        jmp       .B1.140       ; Prob 100%                     ;74.125
                                ; LOE eax edx ecx ebx xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.135:                        ; Preds .B1.66 .B1.53
        xor       edx, edx                                      ;70.37
        sub       ebx, ecx                                      ;70.37
        cmovs     ebx, edx                                      ;70.13
        test      eax, eax                                      ;70.13
        cvtsi2ss  xmm6, ebx                                     ;70.31
        addss     xmm2, xmm6                                    ;70.13
                                ; LOE eax xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.138:                        ; Preds .B1.89 .B1.135 .B1.72 .B1.106
        mov       ebx, 0                                        ;
        jle       .B1.157       ; Prob 50%                      ;74.125
                                ; LOE eax ebx xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.139:                        ; Preds .B1.127 .B1.125 .B1.138 .B1.123
        xor       ecx, ecx                                      ;
        xor       edx, edx                                      ;
                                ; LOE eax edx ecx ebx xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.140:                        ; Preds .B1.134 .B1.139
        cmp       edx, eax                                      ;74.125
        jae       .B1.144       ; Prob 10%                      ;74.125
                                ; LOE eax edx ecx ebx xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.141:                        ; Preds .B1.140
        mov       edi, DWORD PTR [124+esp]                      ;
        mov       esi, DWORD PTR [116+esp]                      ;
        mov       DWORD PTR [204+esp], ecx                      ;
        mov       ecx, DWORD PTR [220+esp]                      ;
        sub       ecx, DWORD PTR [632+esi+edi]                  ;
        mov       esi, DWORD PTR [600+esi+edi]                  ;74.125
        lea       esi, DWORD PTR [esi+ecx*4]                    ;
        mov       ecx, DWORD PTR [204+esp]                      ;
                                ; LOE eax edx ecx ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.142:                        ; Preds .B1.142 .B1.141
        add       ecx, DWORD PTR [esi+edx*4]                    ;74.125
        inc       edx                                           ;74.125
        cmp       edx, eax                                      ;74.125
        jb        .B1.142       ; Prob 82%                      ;74.125
                                ; LOE eax edx ecx ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.144:                        ; Preds .B1.142 .B1.140
        test      eax, eax                                      ;74.185
        jle       .B1.226       ; Prob 50%                      ;74.185
                                ; LOE eax ecx ebx xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.145:                        ; Preds .B1.144
        cmp       eax, 4                                        ;74.185
        jl        .B1.158       ; Prob 10%                      ;74.185
                                ; LOE eax ecx ebx xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.146:                        ; Preds .B1.145
        mov       edi, DWORD PTR [124+esp]                      ;74.185
        mov       esi, DWORD PTR [116+esp]                      ;74.185
        mov       edx, DWORD PTR [220+esp]                      ;74.185
        sub       edx, DWORD PTR [596+esi+edi]                  ;74.185
        mov       esi, DWORD PTR [564+esi+edi]                  ;74.185
        lea       esi, DWORD PTR [esi+edx*4]                    ;74.185
        mov       DWORD PTR [164+esp], esi                      ;74.185
        and       esi, 15                                       ;74.185
        je        .B1.149       ; Prob 50%                      ;74.185
                                ; LOE eax ecx ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.147:                        ; Preds .B1.146
        test      esi, 3                                        ;74.185
        jne       .B1.158       ; Prob 10%                      ;74.185
                                ; LOE eax ecx ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.148:                        ; Preds .B1.147
        neg       esi                                           ;74.185
        add       esi, 16                                       ;74.185
        shr       esi, 2                                        ;74.185
                                ; LOE eax ecx ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.149:                        ; Preds .B1.148 .B1.146
        lea       edx, DWORD PTR [4+esi]                        ;74.185
        cmp       eax, edx                                      ;74.185
        jl        .B1.158       ; Prob 10%                      ;74.185
                                ; LOE eax ecx ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.150:                        ; Preds .B1.149
        mov       edx, eax                                      ;74.185
        xor       edi, edi                                      ;
        sub       edx, esi                                      ;74.185
        and       edx, 3                                        ;74.185
        neg       edx                                           ;74.185
        add       edx, eax                                      ;74.185
        mov       DWORD PTR [256+esp], edi                      ;
        test      esi, esi                                      ;74.185
        jbe       .B1.154       ; Prob 10%                      ;74.185
                                ; LOE eax edx ecx ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.151:                        ; Preds .B1.150
        xor       edi, edi                                      ;
        mov       DWORD PTR [148+esp], ebx                      ;
        mov       DWORD PTR [132+esp], eax                      ;
        mov       eax, edi                                      ;
        mov       ebx, DWORD PTR [256+esp]                      ;
        mov       edi, DWORD PTR [164+esp]                      ;
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.152:                        ; Preds .B1.152 .B1.151
        add       ebx, DWORD PTR [edi+eax*4]                    ;74.185
        inc       eax                                           ;74.185
        cmp       eax, esi                                      ;74.185
        jb        .B1.152       ; Prob 82%                      ;74.185
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.153:                        ; Preds .B1.152
        mov       DWORD PTR [256+esp], ebx                      ;
        mov       ebx, DWORD PTR [148+esp]                      ;
        mov       eax, DWORD PTR [132+esp]                      ;
                                ; LOE eax edx ecx ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.154:                        ; Preds .B1.150 .B1.153
        movd      xmm6, DWORD PTR [256+esp]                     ;74.185
        mov       edi, DWORD PTR [164+esp]                      ;74.185
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5 xmm6
.B1.155:                        ; Preds .B1.155 .B1.154
        paddd     xmm6, XMMWORD PTR [edi+esi*4]                 ;74.185
        add       esi, 4                                        ;74.185
        cmp       esi, edx                                      ;74.185
        jb        .B1.155       ; Prob 82%                      ;74.185
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5 xmm6
.B1.156:                        ; Preds .B1.155
        movdqa    xmm7, xmm6                                    ;74.185
        psrldq    xmm7, 8                                       ;74.185
        paddd     xmm6, xmm7                                    ;74.185
        movdqa    xmm7, xmm6                                    ;74.185
        psrldq    xmm7, 4                                       ;74.185
        paddd     xmm6, xmm7                                    ;74.185
        movd      DWORD PTR [248+esp], xmm6                     ;74.185
        jmp       .B1.159       ; Prob 100%                     ;74.185
                                ; LOE eax edx ecx ebx xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.157:                        ; Preds .B1.138
        mov       ecx, 0                                        ;
        jle       .B1.226       ; Prob 50%                      ;74.185
                                ; LOE eax ecx ebx xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.158:                        ; Preds .B1.149 .B1.147 .B1.157 .B1.145
        xor       edx, edx                                      ;
        mov       DWORD PTR [248+esp], edx                      ;
                                ; LOE eax edx ecx ebx xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.159:                        ; Preds .B1.156 .B1.158
        cmp       edx, eax                                      ;74.185
        jae       .B1.163       ; Prob 10%                      ;74.185
                                ; LOE eax edx ecx ebx xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.160:                        ; Preds .B1.159
        mov       edi, DWORD PTR [124+esp]                      ;
        mov       esi, DWORD PTR [116+esp]                      ;
        mov       DWORD PTR [208+esp], edx                      ;
        mov       edx, DWORD PTR [220+esp]                      ;
        sub       edx, DWORD PTR [596+esi+edi]                  ;
        mov       esi, DWORD PTR [564+esi+edi]                  ;74.185
        mov       edi, DWORD PTR [248+esp]                      ;
        lea       esi, DWORD PTR [esi+edx*4]                    ;
        mov       edx, DWORD PTR [208+esp]                      ;
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.161:                        ; Preds .B1.161 .B1.160
        add       edi, DWORD PTR [esi+edx*4]                    ;74.185
        inc       edx                                           ;74.185
        cmp       edx, eax                                      ;74.185
        jb        .B1.161       ; Prob 82%                      ;74.185
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.162:                        ; Preds .B1.161
        mov       DWORD PTR [248+esp], edi                      ;
                                ; LOE eax ecx ebx xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.163:                        ; Preds .B1.162 .B1.159
        sub       ebx, ecx                                      ;74.66
        sub       ebx, DWORD PTR [248+esp]                      ;74.66
        cvtsi2ss  xmm6, ebx                                     ;74.66
        mulss     xmm6, DWORD PTR [236+esp]                     ;74.64
        test      eax, eax                                      ;75.65
        addss     xmm0, xmm6                                    ;74.13
        jle       .B1.229       ; Prob 50%                      ;75.65
                                ; LOE eax xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.164:                        ; Preds .B1.163
        cmp       eax, 4                                        ;75.65
        jl        .B1.311       ; Prob 10%                      ;75.65
                                ; LOE eax xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.165:                        ; Preds .B1.164
        mov       ecx, DWORD PTR [124+esp]                      ;75.65
        mov       edx, DWORD PTR [116+esp]                      ;75.65
        mov       esi, DWORD PTR [220+esp]                      ;75.65
        sub       esi, DWORD PTR [632+edx+ecx]                  ;75.65
        mov       ebx, DWORD PTR [600+edx+ecx]                  ;75.65
        lea       ebx, DWORD PTR [ebx+esi*4]                    ;75.65
        mov       esi, ebx                                      ;75.65
        and       esi, 15                                       ;75.65
        je        .B1.168       ; Prob 50%                      ;75.65
                                ; LOE eax ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.166:                        ; Preds .B1.165
        test      esi, 3                                        ;75.65
        jne       .B1.311       ; Prob 10%                      ;75.65
                                ; LOE eax ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.167:                        ; Preds .B1.166
        neg       esi                                           ;75.65
        add       esi, 16                                       ;75.65
        shr       esi, 2                                        ;75.65
                                ; LOE eax ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.168:                        ; Preds .B1.167 .B1.165
        lea       edx, DWORD PTR [4+esi]                        ;75.65
        cmp       eax, edx                                      ;75.65
        jl        .B1.311       ; Prob 10%                      ;75.65
                                ; LOE eax ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.169:                        ; Preds .B1.168
        mov       edx, eax                                      ;75.65
        xor       ecx, ecx                                      ;
        sub       edx, esi                                      ;75.65
        and       edx, 3                                        ;75.65
        neg       edx                                           ;75.65
        add       edx, eax                                      ;75.65
        test      esi, esi                                      ;75.65
        jbe       .B1.173       ; Prob 10%                      ;75.65
                                ; LOE eax edx ecx ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.170:                        ; Preds .B1.169
        xor       edi, edi                                      ;
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.171:                        ; Preds .B1.171 .B1.170
        add       ecx, DWORD PTR [ebx+edi*4]                    ;75.65
        inc       edi                                           ;75.65
        cmp       edi, esi                                      ;75.65
        jb        .B1.171       ; Prob 82%                      ;75.65
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.173:                        ; Preds .B1.171 .B1.169
        movd      xmm6, ecx                                     ;75.65
                                ; LOE eax edx ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5 xmm6
.B1.174:                        ; Preds .B1.174 .B1.173
        paddd     xmm6, XMMWORD PTR [ebx+esi*4]                 ;75.65
        add       esi, 4                                        ;75.65
        cmp       esi, edx                                      ;75.65
        jb        .B1.174       ; Prob 82%                      ;75.65
                                ; LOE eax edx ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5 xmm6
.B1.175:                        ; Preds .B1.174
        movdqa    xmm7, xmm6                                    ;75.65
        psrldq    xmm7, 8                                       ;75.65
        paddd     xmm6, xmm7                                    ;75.65
        movdqa    xmm7, xmm6                                    ;75.65
        psrldq    xmm7, 4                                       ;75.65
        paddd     xmm6, xmm7                                    ;75.65
        movd      ebx, xmm6                                     ;75.65
                                ; LOE eax edx ebx xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.176:                        ; Preds .B1.175 .B1.311
        cmp       edx, eax                                      ;75.65
        jae       .B1.180       ; Prob 10%                      ;75.65
                                ; LOE eax edx ebx xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.177:                        ; Preds .B1.176
        mov       edi, DWORD PTR [124+esp]                      ;
        mov       esi, DWORD PTR [116+esp]                      ;
        mov       ecx, DWORD PTR [220+esp]                      ;
        sub       ecx, DWORD PTR [632+esi+edi]                  ;
        mov       esi, DWORD PTR [600+esi+edi]                  ;75.65
        lea       ecx, DWORD PTR [esi+ecx*4]                    ;
                                ; LOE eax edx ecx ebx xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.178:                        ; Preds .B1.178 .B1.177
        add       ebx, DWORD PTR [ecx+edx*4]                    ;75.65
        inc       edx                                           ;75.65
        cmp       edx, eax                                      ;75.65
        jb        .B1.178       ; Prob 82%                      ;75.65
                                ; LOE eax edx ecx ebx xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.180:                        ; Preds .B1.178 .B1.176
        cvtsi2ss  xmm6, ebx                                     ;75.65
        mulss     xmm6, DWORD PTR [236+esp]                     ;75.64
        test      eax, eax                                      ;76.65
        addss     xmm1, xmm6                                    ;75.13
        jle       .B1.229       ; Prob 50%                      ;76.65
                                ; LOE eax xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.181:                        ; Preds .B1.180
        cmp       eax, 4                                        ;76.65
        jl        .B1.312       ; Prob 10%                      ;76.65
                                ; LOE eax xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.182:                        ; Preds .B1.181
        mov       ecx, DWORD PTR [124+esp]                      ;76.65
        mov       edx, DWORD PTR [116+esp]                      ;76.65
        mov       esi, DWORD PTR [220+esp]                      ;76.65
        sub       esi, DWORD PTR [596+edx+ecx]                  ;76.65
        mov       ebx, DWORD PTR [564+edx+ecx]                  ;76.65
        lea       ebx, DWORD PTR [ebx+esi*4]                    ;76.65
        mov       esi, ebx                                      ;76.65
        and       esi, 15                                       ;76.65
        je        .B1.185       ; Prob 50%                      ;76.65
                                ; LOE eax ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.183:                        ; Preds .B1.182
        test      esi, 3                                        ;76.65
        jne       .B1.312       ; Prob 10%                      ;76.65
                                ; LOE eax ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.184:                        ; Preds .B1.183
        neg       esi                                           ;76.65
        add       esi, 16                                       ;76.65
        shr       esi, 2                                        ;76.65
                                ; LOE eax ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.185:                        ; Preds .B1.184 .B1.182
        lea       edx, DWORD PTR [4+esi]                        ;76.65
        cmp       eax, edx                                      ;76.65
        jl        .B1.312       ; Prob 10%                      ;76.65
                                ; LOE eax ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.186:                        ; Preds .B1.185
        mov       edx, eax                                      ;76.65
        xor       ecx, ecx                                      ;
        sub       edx, esi                                      ;76.65
        and       edx, 3                                        ;76.65
        neg       edx                                           ;76.65
        add       edx, eax                                      ;76.65
        test      esi, esi                                      ;76.65
        jbe       .B1.190       ; Prob 10%                      ;76.65
                                ; LOE eax edx ecx ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.187:                        ; Preds .B1.186
        xor       edi, edi                                      ;
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.188:                        ; Preds .B1.188 .B1.187
        add       ecx, DWORD PTR [ebx+edi*4]                    ;76.65
        inc       edi                                           ;76.65
        cmp       edi, esi                                      ;76.65
        jb        .B1.188       ; Prob 82%                      ;76.65
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.190:                        ; Preds .B1.188 .B1.186
        movd      xmm6, ecx                                     ;76.65
                                ; LOE eax edx ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5 xmm6
.B1.191:                        ; Preds .B1.191 .B1.190
        paddd     xmm6, XMMWORD PTR [ebx+esi*4]                 ;76.65
        add       esi, 4                                        ;76.65
        cmp       esi, edx                                      ;76.65
        jb        .B1.191       ; Prob 82%                      ;76.65
                                ; LOE eax edx ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5 xmm6
.B1.192:                        ; Preds .B1.191
        movdqa    xmm7, xmm6                                    ;76.65
        psrldq    xmm7, 8                                       ;76.65
        paddd     xmm6, xmm7                                    ;76.65
        movdqa    xmm7, xmm6                                    ;76.65
        psrldq    xmm7, 4                                       ;76.65
        paddd     xmm6, xmm7                                    ;76.65
        movd      ebx, xmm6                                     ;76.65
                                ; LOE eax edx ebx xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.193:                        ; Preds .B1.192 .B1.312
        cmp       edx, eax                                      ;76.65
        jae       .B1.197       ; Prob 10%                      ;76.65
                                ; LOE eax edx ebx xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.194:                        ; Preds .B1.193
        mov       edi, DWORD PTR [124+esp]                      ;
        mov       esi, DWORD PTR [116+esp]                      ;
        mov       ecx, DWORD PTR [220+esp]                      ;
        sub       ecx, DWORD PTR [596+esi+edi]                  ;
        mov       esi, DWORD PTR [564+esi+edi]                  ;76.65
        lea       ecx, DWORD PTR [esi+ecx*4]                    ;
                                ; LOE eax edx ecx ebx xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.195:                        ; Preds .B1.195 .B1.194
        add       ebx, DWORD PTR [ecx+edx*4]                    ;76.65
        inc       edx                                           ;76.65
        cmp       edx, eax                                      ;76.65
        jb        .B1.195       ; Prob 82%                      ;76.65
                                ; LOE eax edx ecx ebx xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.197:                        ; Preds .B1.195 .B1.193
        cvtsi2ss  xmm6, ebx                                     ;76.65
        mulss     xmm6, DWORD PTR [236+esp]                     ;76.64
        test      eax, eax                                      ;78.68
        addss     xmm3, xmm6                                    ;76.13
        jle       .B1.229       ; Prob 50%                      ;78.68
                                ; LOE eax xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.198:                        ; Preds .B1.197
        cmp       eax, 4                                        ;78.68
        jl        .B1.313       ; Prob 10%                      ;78.68
                                ; LOE eax xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.199:                        ; Preds .B1.198
        mov       ecx, DWORD PTR [124+esp]                      ;78.68
        mov       edx, DWORD PTR [116+esp]                      ;78.68
        mov       esi, DWORD PTR [560+edx+ecx]                  ;78.68
        neg       esi                                           ;78.68
        mov       ebx, DWORD PTR [528+edx+ecx]                  ;78.68
        add       esi, DWORD PTR [220+esp]                      ;78.68
        lea       ebx, DWORD PTR [ebx+esi*4]                    ;78.68
        mov       esi, ebx                                      ;78.68
        and       esi, 15                                       ;78.68
        je        .B1.202       ; Prob 50%                      ;78.68
                                ; LOE eax ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.200:                        ; Preds .B1.199
        test      esi, 3                                        ;78.68
        jne       .B1.313       ; Prob 10%                      ;78.68
                                ; LOE eax ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.201:                        ; Preds .B1.200
        neg       esi                                           ;78.68
        add       esi, 16                                       ;78.68
        shr       esi, 2                                        ;78.68
                                ; LOE eax ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.202:                        ; Preds .B1.201 .B1.199
        lea       edx, DWORD PTR [4+esi]                        ;78.68
        cmp       eax, edx                                      ;78.68
        jl        .B1.313       ; Prob 10%                      ;78.68
                                ; LOE eax ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.203:                        ; Preds .B1.202
        mov       edx, eax                                      ;78.68
        xor       ecx, ecx                                      ;
        sub       edx, esi                                      ;78.68
        and       edx, 3                                        ;78.68
        neg       edx                                           ;78.68
        add       edx, eax                                      ;78.68
        test      esi, esi                                      ;78.68
        jbe       .B1.207       ; Prob 10%                      ;78.68
                                ; LOE eax edx ecx ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.204:                        ; Preds .B1.203
        xor       edi, edi                                      ;
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.205:                        ; Preds .B1.205 .B1.204
        add       ecx, DWORD PTR [ebx+edi*4]                    ;78.68
        inc       edi                                           ;78.68
        cmp       edi, esi                                      ;78.68
        jb        .B1.205       ; Prob 82%                      ;78.68
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.207:                        ; Preds .B1.205 .B1.203
        movd      xmm6, ecx                                     ;78.68
                                ; LOE eax edx ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5 xmm6
.B1.208:                        ; Preds .B1.208 .B1.207
        paddd     xmm6, XMMWORD PTR [ebx+esi*4]                 ;78.68
        add       esi, 4                                        ;78.68
        cmp       esi, edx                                      ;78.68
        jb        .B1.208       ; Prob 82%                      ;78.68
                                ; LOE eax edx ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5 xmm6
.B1.209:                        ; Preds .B1.208
        movdqa    xmm7, xmm6                                    ;78.68
        psrldq    xmm7, 8                                       ;78.68
        paddd     xmm6, xmm7                                    ;78.68
        movdqa    xmm7, xmm6                                    ;78.68
        psrldq    xmm7, 4                                       ;78.68
        paddd     xmm6, xmm7                                    ;78.68
        movd      ebx, xmm6                                     ;78.68
                                ; LOE eax edx ebx xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.210:                        ; Preds .B1.209 .B1.313
        cmp       edx, eax                                      ;78.68
        jae       .B1.214       ; Prob 10%                      ;78.68
                                ; LOE eax edx ebx xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.211:                        ; Preds .B1.210
        mov       edi, DWORD PTR [124+esp]                      ;78.68
        mov       esi, DWORD PTR [116+esp]                      ;78.68
        mov       ecx, DWORD PTR [560+esi+edi]                  ;78.68
        neg       ecx                                           ;
        mov       esi, DWORD PTR [528+esi+edi]                  ;78.68
        add       ecx, DWORD PTR [220+esp]                      ;
        lea       ecx, DWORD PTR [esi+ecx*4]                    ;
                                ; LOE eax edx ecx ebx xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.212:                        ; Preds .B1.212 .B1.211
        add       ebx, DWORD PTR [ecx+edx*4]                    ;78.68
        inc       edx                                           ;78.68
        cmp       edx, eax                                      ;78.68
        jb        .B1.212       ; Prob 82%                      ;78.68
                                ; LOE eax edx ecx ebx xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.214:                        ; Preds .B1.212 .B1.210
        cmp       eax, 4                                        ;78.127
        jl        .B1.230       ; Prob 10%                      ;78.127
                                ; LOE eax ebx xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.215:                        ; Preds .B1.214
        mov       ecx, DWORD PTR [124+esp]                      ;78.127
        mov       edx, DWORD PTR [116+esp]                      ;78.127
        mov       edi, DWORD PTR [220+esp]                      ;78.127
        sub       edi, DWORD PTR [632+edx+ecx]                  ;78.127
        mov       esi, DWORD PTR [600+edx+ecx]                  ;78.127
        lea       esi, DWORD PTR [esi+edi*4]                    ;78.127
        mov       DWORD PTR [180+esp], esi                      ;78.127
        and       esi, 15                                       ;78.127
        je        .B1.218       ; Prob 50%                      ;78.127
                                ; LOE eax ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.216:                        ; Preds .B1.215
        test      esi, 3                                        ;78.127
        jne       .B1.230       ; Prob 10%                      ;78.127
                                ; LOE eax ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.217:                        ; Preds .B1.216
        neg       esi                                           ;78.127
        add       esi, 16                                       ;78.127
        shr       esi, 2                                        ;78.127
                                ; LOE eax ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.218:                        ; Preds .B1.217 .B1.215
        lea       edx, DWORD PTR [4+esi]                        ;78.127
        cmp       eax, edx                                      ;78.127
        jl        .B1.230       ; Prob 10%                      ;78.127
                                ; LOE eax ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.219:                        ; Preds .B1.218
        mov       edx, eax                                      ;78.127
        xor       ecx, ecx                                      ;
        sub       edx, esi                                      ;78.127
        and       edx, 3                                        ;78.127
        neg       edx                                           ;78.127
        add       edx, eax                                      ;78.127
        test      esi, esi                                      ;78.127
        jbe       .B1.223       ; Prob 10%                      ;78.127
                                ; LOE eax edx ecx ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.220:                        ; Preds .B1.219
        xor       edi, edi                                      ;
        mov       DWORD PTR [132+esp], eax                      ;
        mov       eax, edi                                      ;
        mov       edi, DWORD PTR [180+esp]                      ;
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.221:                        ; Preds .B1.221 .B1.220
        add       ecx, DWORD PTR [edi+eax*4]                    ;78.127
        inc       eax                                           ;78.127
        cmp       eax, esi                                      ;78.127
        jb        .B1.221       ; Prob 82%                      ;78.127
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.222:                        ; Preds .B1.221
        mov       eax, DWORD PTR [132+esp]                      ;
                                ; LOE eax edx ecx ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.223:                        ; Preds .B1.219 .B1.222
        movd      xmm6, ecx                                     ;78.127
        mov       ecx, DWORD PTR [180+esp]                      ;78.127
                                ; LOE eax edx ecx ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5 xmm6
.B1.224:                        ; Preds .B1.224 .B1.223
        paddd     xmm6, XMMWORD PTR [ecx+esi*4]                 ;78.127
        add       esi, 4                                        ;78.127
        cmp       esi, edx                                      ;78.127
        jb        .B1.224       ; Prob 82%                      ;78.127
                                ; LOE eax edx ecx ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5 xmm6
.B1.225:                        ; Preds .B1.224
        movdqa    xmm7, xmm6                                    ;78.127
        psrldq    xmm7, 8                                       ;78.127
        paddd     xmm6, xmm7                                    ;78.127
        movdqa    xmm7, xmm6                                    ;78.127
        psrldq    xmm7, 4                                       ;78.127
        paddd     xmm6, xmm7                                    ;78.127
        movd      ecx, xmm6                                     ;78.127
        jmp       .B1.231       ; Prob 100%                     ;78.127
                                ; LOE eax edx ecx ebx xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.226:                        ; Preds .B1.157 .B1.144
        sub       ebx, ecx                                      ;74.66
        cvtsi2ss  xmm6, ebx                                     ;74.66
        mulss     xmm6, DWORD PTR [236+esp]                     ;74.64
        test      eax, eax                                      ;74.13
        addss     xmm0, xmm6                                    ;74.13
                                ; LOE eax xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.229:                        ; Preds .B1.180 .B1.226 .B1.163 .B1.197
        mov       ebx, 0                                        ;
        jle       .B1.248       ; Prob 50%                      ;78.127
                                ; LOE eax ebx xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.230:                        ; Preds .B1.218 .B1.216 .B1.229 .B1.214
        xor       ecx, ecx                                      ;
        xor       edx, edx                                      ;
                                ; LOE eax edx ecx ebx xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.231:                        ; Preds .B1.225 .B1.230
        cmp       edx, eax                                      ;78.127
        jae       .B1.235       ; Prob 10%                      ;78.127
                                ; LOE eax edx ecx ebx xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.232:                        ; Preds .B1.231
        mov       edi, DWORD PTR [124+esp]                      ;
        mov       esi, DWORD PTR [116+esp]                      ;
        mov       DWORD PTR [212+esp], ecx                      ;
        mov       ecx, DWORD PTR [220+esp]                      ;
        sub       ecx, DWORD PTR [632+esi+edi]                  ;
        mov       esi, DWORD PTR [600+esi+edi]                  ;78.127
        lea       esi, DWORD PTR [esi+ecx*4]                    ;
        mov       ecx, DWORD PTR [212+esp]                      ;
                                ; LOE eax edx ecx ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.233:                        ; Preds .B1.233 .B1.232
        add       ecx, DWORD PTR [esi+edx*4]                    ;78.127
        inc       edx                                           ;78.127
        cmp       edx, eax                                      ;78.127
        jb        .B1.233       ; Prob 82%                      ;78.127
                                ; LOE eax edx ecx ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.235:                        ; Preds .B1.233 .B1.231
        test      eax, eax                                      ;78.187
        jle       .B1.254       ; Prob 50%                      ;78.187
                                ; LOE eax ecx ebx xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.236:                        ; Preds .B1.235
        cmp       eax, 4                                        ;78.187
        jl        .B1.249       ; Prob 10%                      ;78.187
                                ; LOE eax ecx ebx xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.237:                        ; Preds .B1.236
        mov       edi, DWORD PTR [124+esp]                      ;78.187
        mov       esi, DWORD PTR [116+esp]                      ;78.187
        mov       edx, DWORD PTR [220+esp]                      ;78.187
        sub       edx, DWORD PTR [596+esi+edi]                  ;78.187
        mov       esi, DWORD PTR [564+esi+edi]                  ;78.187
        lea       edi, DWORD PTR [esi+edx*4]                    ;78.187
        mov       DWORD PTR [172+esp], edi                      ;78.187
        and       edi, 15                                       ;78.187
        je        .B1.240       ; Prob 50%                      ;78.187
                                ; LOE eax ecx ebx edi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.238:                        ; Preds .B1.237
        test      edi, 3                                        ;78.187
        jne       .B1.249       ; Prob 10%                      ;78.187
                                ; LOE eax ecx ebx edi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.239:                        ; Preds .B1.238
        neg       edi                                           ;78.187
        add       edi, 16                                       ;78.187
        shr       edi, 2                                        ;78.187
                                ; LOE eax ecx ebx edi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.240:                        ; Preds .B1.239 .B1.237
        lea       edx, DWORD PTR [4+edi]                        ;78.187
        cmp       eax, edx                                      ;78.187
        jl        .B1.249       ; Prob 10%                      ;78.187
                                ; LOE eax ecx ebx edi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.241:                        ; Preds .B1.240
        mov       edx, eax                                      ;78.187
        xor       esi, esi                                      ;
        sub       edx, edi                                      ;78.187
        and       edx, 3                                        ;78.187
        neg       edx                                           ;78.187
        add       edx, eax                                      ;78.187
        mov       DWORD PTR [232+esp], edx                      ;78.187
        test      edi, edi                                      ;78.187
        jbe       .B1.245       ; Prob 10%                      ;78.187
                                ; LOE eax ecx ebx esi edi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.242:                        ; Preds .B1.241
        mov       DWORD PTR [132+esp], eax                      ;
        xor       edx, edx                                      ;
        mov       eax, DWORD PTR [172+esp]                      ;
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.243:                        ; Preds .B1.243 .B1.242
        add       esi, DWORD PTR [eax+edx*4]                    ;78.187
        inc       edx                                           ;78.187
        cmp       edx, edi                                      ;78.187
        jb        .B1.243       ; Prob 82%                      ;78.187
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.244:                        ; Preds .B1.243
        mov       eax, DWORD PTR [132+esp]                      ;
                                ; LOE eax ecx ebx esi edi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.245:                        ; Preds .B1.241 .B1.244
        mov       edx, DWORD PTR [232+esp]                      ;78.187
        movd      xmm6, esi                                     ;78.187
        mov       esi, DWORD PTR [172+esp]                      ;78.187
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5 xmm6
.B1.246:                        ; Preds .B1.246 .B1.245
        paddd     xmm6, XMMWORD PTR [esi+edi*4]                 ;78.187
        add       edi, 4                                        ;78.187
        cmp       edi, edx                                      ;78.187
        jb        .B1.246       ; Prob 82%                      ;78.187
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5 xmm6
.B1.247:                        ; Preds .B1.246
        movdqa    xmm7, xmm6                                    ;78.187
        psrldq    xmm7, 8                                       ;78.187
        paddd     xmm6, xmm7                                    ;78.187
        movdqa    xmm7, xmm6                                    ;78.187
        psrldq    xmm7, 4                                       ;78.187
        paddd     xmm6, xmm7                                    ;78.187
        mov       DWORD PTR [232+esp], edx                      ;
        movd      esi, xmm6                                     ;78.187
        jmp       .B1.250       ; Prob 100%                     ;78.187
                                ; LOE eax ecx ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.248:                        ; Preds .B1.229
        mov       ecx, 0                                        ;
        jle       .B1.254       ; Prob 50%                      ;78.187
                                ; LOE eax ecx ebx xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.249:                        ; Preds .B1.240 .B1.238 .B1.248 .B1.236
        xor       esi, esi                                      ;
        mov       DWORD PTR [232+esp], esi                      ;
                                ; LOE eax ecx ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.250:                        ; Preds .B1.247 .B1.249
        cmp       eax, DWORD PTR [232+esp]                      ;78.187
        jbe       .B1.255       ; Prob 10%                      ;78.187
                                ; LOE eax ecx ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.251:                        ; Preds .B1.250
        mov       DWORD PTR [216+esp], esi                      ;
        mov       edi, DWORD PTR [124+esp]                      ;
        mov       esi, DWORD PTR [116+esp]                      ;
        mov       edx, DWORD PTR [220+esp]                      ;
        sub       edx, DWORD PTR [596+esi+edi]                  ;
        mov       esi, DWORD PTR [564+esi+edi]                  ;78.187
        mov       edi, DWORD PTR [232+esp]                      ;
        lea       edx, DWORD PTR [esi+edx*4]                    ;
        mov       esi, DWORD PTR [216+esp]                      ;
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.252:                        ; Preds .B1.252 .B1.251
        add       esi, DWORD PTR [edx+edi*4]                    ;78.187
        inc       edi                                           ;78.187
        cmp       edi, eax                                      ;78.187
        jb        .B1.252       ; Prob 82%                      ;78.187
        jmp       .B1.255       ; Prob 100%                     ;78.187
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.254:                        ; Preds .B1.248 .B1.235
        xor       esi, esi                                      ;
                                ; LOE eax ecx ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.255:                        ; Preds .B1.252 .B1.250 .B1.254
        sub       ebx, ecx                                      ;78.68
        xor       edx, edx                                      ;78.13
        sub       ebx, esi                                      ;78.68
        mov       ecx, DWORD PTR [268+esp]                      ;78.60
        cmovs     ebx, edx                                      ;78.13
        test      eax, eax                                      ;79.61
        cvtsi2ss  xmm6, ebx                                     ;78.61
        mov       ebx, DWORD PTR [224+esp]                      ;78.60
        movss     xmm7, DWORD PTR [240+esp]                     ;78.13
        mulss     xmm6, DWORD PTR [8+ecx+ebx*8]                 ;78.60
        addss     xmm7, xmm6                                    ;78.13
        movss     DWORD PTR [240+esp], xmm7                     ;78.13
        jle       .B1.272       ; Prob 50%                      ;79.61
                                ; LOE eax xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.256:                        ; Preds .B1.255
        cmp       eax, 4                                        ;79.61
        jl        .B1.324       ; Prob 10%                      ;79.61
                                ; LOE eax xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.257:                        ; Preds .B1.256
        mov       ecx, DWORD PTR [124+esp]                      ;79.61
        mov       edx, DWORD PTR [116+esp]                      ;79.61
        mov       esi, DWORD PTR [220+esp]                      ;79.61
        sub       esi, DWORD PTR [596+edx+ecx]                  ;79.61
        mov       ebx, DWORD PTR [564+edx+ecx]                  ;79.61
        lea       ebx, DWORD PTR [ebx+esi*4]                    ;79.61
        mov       esi, ebx                                      ;79.61
        and       esi, 15                                       ;79.61
        je        .B1.260       ; Prob 50%                      ;79.61
                                ; LOE eax ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.258:                        ; Preds .B1.257
        test      esi, 3                                        ;79.61
        jne       .B1.324       ; Prob 10%                      ;79.61
                                ; LOE eax ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.259:                        ; Preds .B1.258
        neg       esi                                           ;79.61
        add       esi, 16                                       ;79.61
        shr       esi, 2                                        ;79.61
                                ; LOE eax ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.260:                        ; Preds .B1.259 .B1.257
        lea       edx, DWORD PTR [4+esi]                        ;79.61
        cmp       eax, edx                                      ;79.61
        jl        .B1.324       ; Prob 10%                      ;79.61
                                ; LOE eax ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.261:                        ; Preds .B1.260
        mov       edx, eax                                      ;79.61
        xor       ecx, ecx                                      ;
        sub       edx, esi                                      ;79.61
        and       edx, 3                                        ;79.61
        neg       edx                                           ;79.61
        add       edx, eax                                      ;79.61
        test      esi, esi                                      ;79.61
        jbe       .B1.265       ; Prob 10%                      ;79.61
                                ; LOE eax edx ecx ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.262:                        ; Preds .B1.261
        xor       edi, edi                                      ;
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.263:                        ; Preds .B1.263 .B1.262
        add       ecx, DWORD PTR [ebx+edi*4]                    ;79.61
        inc       edi                                           ;79.61
        cmp       edi, esi                                      ;79.61
        jb        .B1.263       ; Prob 82%                      ;79.61
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.265:                        ; Preds .B1.263 .B1.261
        movd      xmm6, ecx                                     ;79.61
                                ; LOE eax edx ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5 xmm6
.B1.266:                        ; Preds .B1.266 .B1.265
        paddd     xmm6, XMMWORD PTR [ebx+esi*4]                 ;79.61
        add       esi, 4                                        ;79.61
        cmp       esi, edx                                      ;79.61
        jb        .B1.266       ; Prob 82%                      ;79.61
                                ; LOE eax edx ebx esi xmm0 xmm1 xmm2 xmm3 xmm4 xmm5 xmm6
.B1.267:                        ; Preds .B1.266
        movdqa    xmm7, xmm6                                    ;79.61
        psrldq    xmm7, 8                                       ;79.61
        paddd     xmm6, xmm7                                    ;79.61
        movdqa    xmm7, xmm6                                    ;79.61
        psrldq    xmm7, 4                                       ;79.61
        paddd     xmm6, xmm7                                    ;79.61
        movd      ebx, xmm6                                     ;79.61
                                ; LOE eax edx ebx xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.268:                        ; Preds .B1.267 .B1.324
        cmp       edx, eax                                      ;79.61
        jae       .B1.273       ; Prob 10%                      ;79.61
                                ; LOE eax edx ebx xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.269:                        ; Preds .B1.268
        mov       edi, DWORD PTR [124+esp]                      ;
        mov       esi, DWORD PTR [116+esp]                      ;
        mov       ecx, DWORD PTR [220+esp]                      ;
        sub       ecx, DWORD PTR [596+esi+edi]                  ;
        mov       esi, DWORD PTR [564+esi+edi]                  ;79.61
        lea       ecx, DWORD PTR [esi+ecx*4]                    ;
                                ; LOE eax edx ecx ebx xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.270:                        ; Preds .B1.270 .B1.269
        add       ebx, DWORD PTR [ecx+edx*4]                    ;79.61
        inc       edx                                           ;79.61
        cmp       edx, eax                                      ;79.61
        jb        .B1.270       ; Prob 82%                      ;79.61
        jmp       .B1.273       ; Prob 100%                     ;79.61
                                ; LOE eax edx ecx ebx xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.272:                        ; Preds .B1.255
        xor       ebx, ebx                                      ;
                                ; LOE ebx xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.273:                        ; Preds .B1.270 .B1.268 .B1.272
        cvtsi2ss  xmm6, ebx                                     ;79.61
        mov       eax, DWORD PTR [268+esp]                      ;79.60
        mov       edx, DWORD PTR [224+esp]                      ;79.60
        movss     xmm7, DWORD PTR [244+esp]                     ;79.13
        mov       ecx, DWORD PTR [264+esp]                      ;80.10
        mulss     xmm6, DWORD PTR [12+eax+edx*8]                ;79.60
        inc       ecx                                           ;80.10
        mov       DWORD PTR [264+esp], ecx                      ;80.10
        addss     xmm7, xmm6                                    ;79.13
        movss     DWORD PTR [244+esp], xmm7                     ;79.13
        cmp       ecx, DWORD PTR [228+esp]                      ;80.10
        jle       .B1.12        ; Prob 82%                      ;80.10
                                ; LOE xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.274:                        ; Preds .B1.273
        movss     DWORD PTR [88+esp], xmm2                      ;
        lea       ebx, DWORD PTR [16+esp]                       ;
        movss     DWORD PTR [96+esp], xmm1                      ;
        movss     DWORD PTR [84+esp], xmm4                      ;
        movss     DWORD PTR [92+esp], xmm5                      ;
        movss     DWORD PTR [100+esp], xmm3                     ;
        mov       esi, DWORD PTR [52+esp]                       ;
                                ; LOE ebx esi xmm0
.B1.275:                        ; Preds .B1.274 .B1.327
        movss     xmm1, DWORD PTR [80+esp]                      ;82.33
        movss     xmm2, DWORD PTR [60+esp]                      ;83.32
        addss     xmm1, xmm0                                    ;82.33
        addss     xmm2, DWORD PTR [240+esp]                     ;83.32
        addss     xmm1, DWORD PTR [100+esp]                     ;82.42
        addss     xmm2, DWORD PTR [244+esp]                     ;83.13
        addss     xmm1, DWORD PTR [96+esp]                      ;82.13
        mov       eax, DWORD PTR [68+esp]                       ;86.5
        mov       edx, DWORD PTR [64+esp]                       ;85.4
        movss     DWORD PTR [80+esp], xmm1                      ;82.13
        imul      ecx, DWORD PTR [esi+eax], 44                  ;86.5
        movss     DWORD PTR [60+esp], xmm2                      ;83.13
        mov       edi, DWORD PTR [36+edx+ecx]                   ;85.4
        mov       DWORD PTR [16+esp], 0                         ;85.4
        mov       DWORD PTR [112+esp], edi                      ;85.4
        push      32                                            ;85.4
        push      OFFSET FLAT: WRITE_TOLL_REVENUE$format_pack.0.1+56 ;85.4
        lea       eax, DWORD PTR [120+esp]                      ;85.4
        push      eax                                           ;85.4
        push      OFFSET FLAT: __STRLITPACK_20.0.1              ;85.4
        push      -2088435968                                   ;85.4
        push      5699                                          ;85.4
        push      ebx                                           ;85.4
        movss     DWORD PTR [132+esp], xmm0                     ;85.4
        call      _for_write_seq_fmt                            ;85.4
                                ; LOE ebx esi
.B1.276:                        ; Preds .B1.275
        mov       eax, DWORD PTR [96+esp]                       ;86.66
        mov       edx, DWORD PTR [92+esp]                       ;85.4
        imul      ecx, DWORD PTR [4+esi+eax], 44                ;86.66
        lea       eax, DWORD PTR [148+esp]                      ;85.4
        mov       edi, DWORD PTR [36+edx+ecx]                   ;85.4
        mov       DWORD PTR [148+esp], edi                      ;85.4
        push      eax                                           ;85.4
        push      OFFSET FLAT: __STRLITPACK_21.0.1              ;85.4
        push      ebx                                           ;85.4
        call      _for_write_seq_fmt_xmit                       ;85.4
                                ; LOE ebx esi
.B1.277:                        ; Preds .B1.276
        movss     xmm0, DWORD PTR [144+esp]                     ;
        lea       eax, DWORD PTR [168+esp]                      ;85.4
        movss     DWORD PTR [168+esp], xmm0                     ;85.4
        push      eax                                           ;85.4
        push      OFFSET FLAT: __STRLITPACK_22.0.1              ;85.4
        push      ebx                                           ;85.4
        call      _for_write_seq_fmt_xmit                       ;85.4
                                ; LOE ebx esi
.B1.278:                        ; Preds .B1.277
        movss     xmm0, DWORD PTR [152+esp]                     ;85.4
        lea       eax, DWORD PTR [188+esp]                      ;85.4
        movss     DWORD PTR [188+esp], xmm0                     ;85.4
        push      eax                                           ;85.4
        push      OFFSET FLAT: __STRLITPACK_23.0.1              ;85.4
        push      ebx                                           ;85.4
        call      _for_write_seq_fmt_xmit                       ;85.4
                                ; LOE ebx esi
.B1.279:                        ; Preds .B1.278
        movss     xmm0, DWORD PTR [160+esp]                     ;85.4
        lea       eax, DWORD PTR [208+esp]                      ;85.4
        movss     DWORD PTR [208+esp], xmm0                     ;85.4
        push      eax                                           ;85.4
        push      OFFSET FLAT: __STRLITPACK_24.0.1              ;85.4
        push      ebx                                           ;85.4
        call      _for_write_seq_fmt_xmit                       ;85.4
                                ; LOE ebx esi
.B1.280:                        ; Preds .B1.279
        movss     xmm0, DWORD PTR [164+esp]                     ;85.4
        lea       eax, DWORD PTR [228+esp]                      ;85.4
        movss     DWORD PTR [228+esp], xmm0                     ;85.4
        push      eax                                           ;85.4
        push      OFFSET FLAT: __STRLITPACK_25.0.1              ;85.4
        push      ebx                                           ;85.4
        call      _for_write_seq_fmt_xmit                       ;85.4
                                ; LOE ebx esi
.B1.281:                        ; Preds .B1.280
        movss     xmm0, DWORD PTR [180+esp]                     ;85.4
        lea       eax, DWORD PTR [248+esp]                      ;85.4
        movss     DWORD PTR [248+esp], xmm0                     ;85.4
        push      eax                                           ;85.4
        push      OFFSET FLAT: __STRLITPACK_26.0.1              ;85.4
        push      ebx                                           ;85.4
        call      _for_write_seq_fmt_xmit                       ;85.4
                                ; LOE ebx esi
.B1.282:                        ; Preds .B1.281
        movss     xmm0, DWORD PTR [184+esp]                     ;85.4
        lea       eax, DWORD PTR [268+esp]                      ;85.4
        movss     DWORD PTR [268+esp], xmm0                     ;85.4
        push      eax                                           ;85.4
        push      OFFSET FLAT: __STRLITPACK_27.0.1              ;85.4
        push      ebx                                           ;85.4
        call      _for_write_seq_fmt_xmit                       ;85.4
                                ; LOE ebx esi
.B1.283:                        ; Preds .B1.282
        movss     xmm0, DWORD PTR [352+esp]                     ;85.4
        lea       eax, DWORD PTR [288+esp]                      ;85.4
        movss     DWORD PTR [288+esp], xmm0                     ;85.4
        push      eax                                           ;85.4
        push      OFFSET FLAT: __STRLITPACK_28.0.1              ;85.4
        push      ebx                                           ;85.4
        call      _for_write_seq_fmt_xmit                       ;85.4
                                ; LOE ebx esi
.B1.332:                        ; Preds .B1.283
        add       esp, 124                                      ;85.4
                                ; LOE ebx esi
.B1.284:                        ; Preds .B1.332
        movss     xmm0, DWORD PTR [244+esp]                     ;85.4
        lea       eax, DWORD PTR [184+esp]                      ;85.4
        movss     DWORD PTR [184+esp], xmm0                     ;85.4
        push      eax                                           ;85.4
        push      OFFSET FLAT: __STRLITPACK_29.0.1              ;85.4
        push      ebx                                           ;85.4
        call      _for_write_seq_fmt_xmit                       ;85.4
                                ; LOE ebx esi
.B1.333:                        ; Preds .B1.284
        add       esp, 12                                       ;85.4
                                ; LOE ebx esi
.B1.285:                        ; Preds .B1.333
        mov       eax, DWORD PTR [72+esp]                       ;87.5
        add       esi, 52                                       ;87.5
        inc       eax                                           ;87.5
        mov       DWORD PTR [72+esp], eax                       ;87.5
        cmp       eax, DWORD PTR [76+esp]                       ;87.5
        jle       .B1.10        ; Prob 82%                      ;87.5
                                ; LOE ebx esi
.B1.286:                        ; Preds .B1.285
        movss     xmm5, DWORD PTR [80+esp]                      ;
                                ; LOE xmm5
.B1.287:                        ; Preds .B1.8 .B1.286
        mov       DWORD PTR [16+esp], 0                         ;89.4
        lea       eax, DWORD PTR [56+esp]                       ;89.4
        movss     DWORD PTR [56+esp], xmm5                      ;89.4
        push      32                                            ;89.4
        push      OFFSET FLAT: WRITE_TOLL_REVENUE$format_pack.0.1+244 ;89.4
        push      eax                                           ;89.4
        push      OFFSET FLAT: __STRLITPACK_30.0.1              ;89.4
        push      -2088435968                                   ;89.4
        push      5699                                          ;89.4
        lea       edx, DWORD PTR [40+esp]                       ;89.4
        push      edx                                           ;89.4
        call      _for_write_seq_fmt                            ;89.4
                                ; LOE
.B1.288:                        ; Preds .B1.287
        movss     xmm0, DWORD PTR [88+esp]                      ;89.4
        lea       eax, DWORD PTR [92+esp]                       ;89.4
        movss     DWORD PTR [92+esp], xmm0                      ;89.4
        push      eax                                           ;89.4
        push      OFFSET FLAT: __STRLITPACK_31.0.1              ;89.4
        lea       edx, DWORD PTR [52+esp]                       ;89.4
        push      edx                                           ;89.4
        call      _for_write_seq_fmt_xmit                       ;89.4
                                ; LOE
.B1.289:                        ; Preds .B1.288
        xor       eax, eax                                      ;90.7
        mov       DWORD PTR [56+esp], eax                       ;90.7
        push      32                                            ;90.7
        push      eax                                           ;90.7
        push      OFFSET FLAT: __STRLITPACK_32.0.1              ;90.7
        push      -2088435968                                   ;90.7
        push      5699                                          ;90.7
        lea       edx, DWORD PTR [76+esp]                       ;90.7
        push      edx                                           ;90.7
        call      _for_write_seq_lis                            ;90.7
                                ; LOE
.B1.334:                        ; Preds .B1.289
        add       esp, 64                                       ;90.7
                                ; LOE
.B1.290:                        ; Preds .B1.1 .B1.2 .B1.334
        add       esp, 276                                      ;93.1
        pop       ebx                                           ;93.1
        pop       edi                                           ;93.1
        pop       esi                                           ;93.1
        mov       esp, ebp                                      ;93.1
        pop       ebp                                           ;93.1
        ret                                                     ;93.1
                                ; LOE
.B1.291:                        ; Preds .B1.19 .B1.23 .B1.21    ; Infreq
        xor       ebx, ebx                                      ;
        xor       edx, edx                                      ;
        jmp       .B1.31        ; Prob 100%                     ;
                                ; LOE eax edx ebx xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.298:                        ; Preds .B1.73 .B1.77 .B1.75    ; Infreq
        xor       ebx, ebx                                      ;
        xor       edx, edx                                      ;
        jmp       .B1.85        ; Prob 100%                     ;
                                ; LOE eax edx ebx xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.299:                        ; Preds .B1.90 .B1.94 .B1.92    ; Infreq
        xor       ebx, ebx                                      ;
        xor       edx, edx                                      ;
        jmp       .B1.102       ; Prob 100%                     ;
                                ; LOE eax edx ebx xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.300:                        ; Preds .B1.107 .B1.111 .B1.109 ; Infreq
        xor       ebx, ebx                                      ;
        xor       edx, edx                                      ;
        jmp       .B1.119       ; Prob 100%                     ;
                                ; LOE eax edx ebx xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.311:                        ; Preds .B1.164 .B1.168 .B1.166 ; Infreq
        xor       ebx, ebx                                      ;
        xor       edx, edx                                      ;
        jmp       .B1.176       ; Prob 100%                     ;
                                ; LOE eax edx ebx xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.312:                        ; Preds .B1.181 .B1.185 .B1.183 ; Infreq
        xor       ebx, ebx                                      ;
        xor       edx, edx                                      ;
        jmp       .B1.193       ; Prob 100%                     ;
                                ; LOE eax edx ebx xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.313:                        ; Preds .B1.198 .B1.202 .B1.200 ; Infreq
        xor       ebx, ebx                                      ;
        xor       edx, edx                                      ;
        jmp       .B1.210       ; Prob 100%                     ;
                                ; LOE eax edx ebx xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.324:                        ; Preds .B1.256 .B1.260 .B1.258 ; Infreq
        xor       ebx, ebx                                      ;
        xor       edx, edx                                      ;
        jmp       .B1.268       ; Prob 100%                     ;
                                ; LOE eax edx ebx xmm0 xmm1 xmm2 xmm3 xmm4 xmm5
.B1.327:                        ; Preds .B1.10                  ; Infreq
        pxor      xmm1, xmm1                                    ;
        pxor      xmm2, xmm2                                    ;
        pxor      xmm3, xmm3                                    ;
        pxor      xmm4, xmm4                                    ;
        pxor      xmm5, xmm5                                    ;
        pxor      xmm6, xmm6                                    ;
        pxor      xmm7, xmm7                                    ;
        pxor      xmm0, xmm0                                    ;
        movss     DWORD PTR [100+esp], xmm1                     ;
        movss     DWORD PTR [88+esp], xmm2                      ;
        movss     DWORD PTR [92+esp], xmm3                      ;
        movss     DWORD PTR [240+esp], xmm4                     ;
        movss     DWORD PTR [244+esp], xmm5                     ;
        movss     DWORD PTR [84+esp], xmm6                      ;
        movss     DWORD PTR [96+esp], xmm7                      ;
        jmp       .B1.275       ; Prob 100%                     ;
        ALIGN     16
                                ; LOE ebx esi xmm0
; mark_end;
_WRITE_TOLL_REVENUE ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
WRITE_TOLL_REVENUE$format_pack.0.1	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	17
	DB	0
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	32
	DB	32
	DB	73
	DB	116
	DB	101
	DB	114
	DB	97
	DB	116
	DB	105
	DB	111
	DB	110
	DB	32
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	4
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	7
	DB	0
	DB	32
	DB	32
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	5
	DB	0
	DB	76
	DB	105
	DB	110
	DB	107
	DB	32
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	3
	DB	0
	DB	45
	DB	62
	DB	32
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	25
	DB	0
	DB	32
	DB	32
	DB	32
	DB	86
	DB	77
	DB	84
	DB	32
	DB	40
	DB	115
	DB	111
	DB	118
	DB	44
	DB	32
	DB	116
	DB	99
	DB	107
	DB	44
	DB	32
	DB	104
	DB	111
	DB	118
	DB	41
	DB	32
	DB	61
	DB	32
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	1
	DB	3
	DB	0
	DB	0
	DB	0
	DB	9
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	28
	DB	0
	DB	32
	DB	32
	DB	32
	DB	86
	DB	111
	DB	108
	DB	117
	DB	109
	DB	101
	DB	32
	DB	40
	DB	115
	DB	111
	DB	118
	DB	44
	DB	32
	DB	116
	DB	99
	DB	107
	DB	44
	DB	32
	DB	104
	DB	111
	DB	118
	DB	41
	DB	32
	DB	61
	DB	32
	DB	33
	DB	0
	DB	0
	DB	1
	DB	3
	DB	0
	DB	0
	DB	0
	DB	9
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	30
	DB	0
	DB	32
	DB	32
	DB	84
	DB	111
	DB	108
	DB	108
	DB	32
	DB	82
	DB	101
	DB	118
	DB	101
	DB	110
	DB	117
	DB	101
	DB	40
	DB	36
	DB	41
	DB	32
	DB	40
	DB	115
	DB	111
	DB	118
	DB	44
	DB	32
	DB	116
	DB	99
	DB	107
	DB	41
	DB	61
	DB	32
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	1
	DB	2
	DB	0
	DB	0
	DB	0
	DB	10
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	19
	DB	0
	DB	84
	DB	111
	DB	116
	DB	97
	DB	108
	DB	32
	DB	86
	DB	77
	DB	84
	DB	40
	DB	109
	DB	105
	DB	108
	DB	101
	DB	115
	DB	41
	DB	32
	DB	61
	DB	32
	DB	0
	DB	33
	DB	0
	DB	0
	DB	1
	DB	1
	DB	0
	DB	0
	DB	0
	DB	15
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	21
	DB	0
	DB	32
	DB	32
	DB	84
	DB	111
	DB	116
	DB	97
	DB	108
	DB	32
	DB	82
	DB	101
	DB	118
	DB	101
	DB	110
	DB	117
	DB	101
	DB	40
	DB	36
	DB	41
	DB	32
	DB	61
	DB	32
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	1
	DB	1
	DB	0
	DB	0
	DB	0
	DB	9
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
_DATA	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
__STRLITPACK_17.0.1	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	26
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_18.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_19.0.1	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_20.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_21.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_22.0.1	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_23.0.1	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_24.0.1	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_25.0.1	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_26.0.1	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_27.0.1	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_28.0.1	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_29.0.1	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_30.0.1	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_31.0.1	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_32.0.1	DB	1
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _WRITE_TOLL_REVENUE
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_16	DB	84
	DB	111
	DB	108
	DB	108
	DB	82
	DB	101
	DB	118
	DB	101
	DB	110
	DB	117
	DB	101
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
__STRLITPACK_15	DB	117
	DB	110
	DB	107
	DB	110
	DB	111
	DB	119
	DB	110
	DB	0
_2il0floatpacket.1	DD	03a83126fH
_2il0floatpacket.2	DD	080000000H
_2il0floatpacket.3	DD	04b000000H
_2il0floatpacket.4	DD	03f000000H
_2il0floatpacket.5	DD	0bf000000H
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_SIMPERIOD:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TOLLLINK:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ITERATION:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NTOLLLINK:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TOLLEXIST:BYTE
_DATA	ENDS
EXTRN	_for_write_seq_fmt_xmit:PROC
EXTRN	_for_write_seq_fmt:PROC
EXTRN	_for_write_seq_lis:PROC
EXTRN	_for_open:PROC
EXTRN	__fltused:BYTE
	INCLUDELIB <ifconsol>
	INCLUDELIB <libifcoremt>
	INCLUDELIB <libifport>
	INCLUDELIB <libmmt>
	INCLUDELIB <LIBCMT>
	INCLUDELIB <libirc>
	INCLUDELIB <svml_dispmt>
	INCLUDELIB <OLDNAMES>
	END
