; mark_description "Intel(R) Visual Fortran Compiler XE for applications running on IA-32, Version 13.1.1.171 Build 20130313";
; mark_description "-S";
	.686P
 	.387
	OPTION DOTNAME
	ASSUME	CS:FLAT,DS:FLAT,SS:FLAT
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
TXTST0:
; -- Begin  _GENERATE_VEH_ATTRIBUTES
; mark_begin;
IF @Version GE 800
  .MMX
ELSEIF @Version GE 612
  .MMX
  MMWORD TEXTEQU <QWORD>
ENDIF
IF @Version GE 800
  .XMM
ELSEIF @Version GE 614
  .XMM
  XMMWORD TEXTEQU <OWORD>
ENDIF
       ALIGN     16
	PUBLIC _GENERATE_VEH_ATTRIBUTES
_GENERATE_VEH_ATTRIBUTES	PROC NEAR 
; parameter 1: 8 + ebp
; parameter 2: 12 + ebp
; parameter 3: 16 + ebp
; parameter 4: 20 + ebp
; parameter 5: 24 + ebp
.B1.1:                          ; Preds .B1.0
        push      ebp                                           ;1.18
        mov       ebp, esp                                      ;1.18
        and       esp, -16                                      ;1.18
        push      esi                                           ;1.18
        push      edi                                           ;1.18
        push      ebx                                           ;1.18
        sub       esp, 196                                      ;1.18
        test      BYTE PTR [_DYNUST_MAIN_MODULE_mp_CHECKFLAGINIT], 1 ;38.13
        je        .B1.164       ; Prob 7%                       ;38.13
                                ; LOE
.B1.2:                          ; Preds .B1.175 .B1.1 .B1.172 .B1.166
        test      BYTE PTR [_DYNUST_MAIN_MODULE_mp_CHECKFLAG], 1 ;46.13
        jne       .B1.22        ; Prob 40%                      ;46.13
                                ; LOE
.B1.3:                          ; Preds .B1.2
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NUMACTZONE+36] ;47.7
        test      esi, esi                                      ;47.7
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NZONES] ;47.25
        jle       .B1.22        ; Prob 11%                      ;47.7
                                ; LOE esi edi
.B1.4:                          ; Preds .B1.3
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NUMACTZONE+24] ;47.7
        test      eax, eax                                      ;47.7
        mov       ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NUMACTZONE] ;47.7
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NUMACTZONE+40] ;47.7
        jle       .B1.22        ; Prob 50%                      ;47.7
                                ; LOE eax ecx ebx esi edi
.B1.5:                          ; Preds .B1.4
        xor       edx, edx                                      ;
        movd      xmm0, edi                                     ;47.7
        mov       DWORD PTR [8+esp], ebx                        ;
        mov       DWORD PTR [4+esp], edx                        ;
        mov       DWORD PTR [12+esp], edx                       ;
        mov       DWORD PTR [20+esp], esi                       ;
        pshufd    xmm0, xmm0, 0                                 ;47.7
        mov       DWORD PTR [16+esp], ecx                       ;
        mov       DWORD PTR [32+esp], ebx                       ;
        mov       DWORD PTR [24+esp], edi                       ;
        mov       esi, DWORD PTR [4+esp]                        ;
        mov       edx, DWORD PTR [8+esp]                        ;
                                ; LOE eax edx esi xmm0
.B1.6:                          ; Preds .B1.21 .B1.5
        cmp       eax, 4                                        ;47.7
        jl        .B1.124       ; Prob 10%                      ;47.7
                                ; LOE eax edx esi xmm0
.B1.7:                          ; Preds .B1.6
        mov       ecx, DWORD PTR [32+esp]                       ;47.7
        add       ecx, esi                                      ;47.7
        and       ecx, 15                                       ;47.7
        je        .B1.10        ; Prob 50%                      ;47.7
                                ; LOE eax edx ecx esi xmm0
.B1.8:                          ; Preds .B1.7
        test      cl, 3                                         ;47.7
        jne       .B1.124       ; Prob 10%                      ;47.7
                                ; LOE eax edx ecx esi xmm0
.B1.9:                          ; Preds .B1.8
        neg       ecx                                           ;47.7
        add       ecx, 16                                       ;47.7
        shr       ecx, 2                                        ;47.7
                                ; LOE eax edx ecx esi xmm0
.B1.10:                         ; Preds .B1.9 .B1.7
        lea       ebx, DWORD PTR [4+ecx]                        ;47.7
        cmp       eax, ebx                                      ;47.7
        jl        .B1.124       ; Prob 10%                      ;47.7
                                ; LOE eax edx ecx esi xmm0
.B1.11:                         ; Preds .B1.10
        mov       ebx, eax                                      ;47.7
        sub       ebx, ecx                                      ;47.7
        mov       edi, DWORD PTR [32+esp]                       ;
        and       ebx, 3                                        ;47.7
        neg       ebx                                           ;47.7
        add       ebx, eax                                      ;47.7
        test      ecx, ecx                                      ;47.7
        lea       edi, DWORD PTR [edi+esi]                      ;
        mov       DWORD PTR [36+esp], edi                       ;
        jbe       .B1.15        ; Prob 11%                      ;47.7
                                ; LOE eax edx ecx ebx esi edi xmm0
.B1.12:                         ; Preds .B1.11
        mov       DWORD PTR [28+esp], 0                         ;
        mov       DWORD PTR [8+esp], edx                        ;
        mov       DWORD PTR [4+esp], eax                        ;
        mov       eax, DWORD PTR [28+esp]                       ;
        mov       edx, edi                                      ;
        mov       edi, DWORD PTR [24+esp]                       ;
                                ; LOE eax edx ecx ebx esi edi xmm0
.B1.13:                         ; Preds .B1.13 .B1.12
        mov       DWORD PTR [edx+eax*4], edi                    ;47.7
        inc       eax                                           ;47.7
        cmp       eax, ecx                                      ;47.7
        jb        .B1.13        ; Prob 82%                      ;47.7
                                ; LOE eax edx ecx ebx esi edi xmm0
.B1.14:                         ; Preds .B1.13
        mov       edx, DWORD PTR [8+esp]                        ;
        mov       eax, DWORD PTR [4+esp]                        ;
                                ; LOE eax edx ecx ebx esi xmm0
.B1.15:                         ; Preds .B1.11 .B1.14
        mov       edi, DWORD PTR [36+esp]                       ;
                                ; LOE eax edx ecx ebx esi edi xmm0
.B1.16:                         ; Preds .B1.16 .B1.15
        movdqa    XMMWORD PTR [edi+ecx*4], xmm0                 ;47.7
        add       ecx, 4                                        ;47.7
        cmp       ecx, ebx                                      ;47.7
        jb        .B1.16        ; Prob 82%                      ;47.7
                                ; LOE eax edx ecx ebx esi edi xmm0
.B1.18:                         ; Preds .B1.16 .B1.124
        cmp       ebx, eax                                      ;47.7
        jae       .B1.21        ; Prob 11%                      ;47.7
                                ; LOE eax edx ebx esi xmm0
.B1.19:                         ; Preds .B1.18
        mov       ecx, DWORD PTR [24+esp]                       ;
                                ; LOE eax edx ecx ebx esi xmm0
.B1.20:                         ; Preds .B1.20 .B1.19
        mov       DWORD PTR [edx+ebx*4], ecx                    ;47.7
        inc       ebx                                           ;47.7
        cmp       ebx, eax                                      ;47.7
        jb        .B1.20        ; Prob 82%                      ;47.7
                                ; LOE eax edx ecx ebx esi xmm0
.B1.21:                         ; Preds .B1.18 .B1.20
        mov       ebx, DWORD PTR [12+esp]                       ;47.7
        inc       ebx                                           ;47.7
        mov       ecx, DWORD PTR [16+esp]                       ;47.7
        add       edx, ecx                                      ;47.7
        add       esi, ecx                                      ;47.7
        mov       DWORD PTR [12+esp], ebx                       ;47.7
        cmp       ebx, DWORD PTR [20+esp]                       ;47.7
        jb        .B1.6         ; Prob 82%                      ;47.7
                                ; LOE eax edx esi xmm0
.B1.22:                         ; Preds .B1.21 .B1.2 .B1.3 .B1.4
        push      OFFSET FLAT: __NLITPACK_0.0.1                 ;52.10
        call      _RANXY                                        ;52.10
                                ; LOE f1
.B1.201:                        ; Preds .B1.22
        fstp      DWORD PTR [12+esp]                            ;52.10
        movss     xmm5, DWORD PTR [12+esp]                      ;52.10
        add       esp, 4                                        ;52.10
                                ; LOE xmm5
.B1.23:                         ; Preds .B1.201
        mov       ecx, DWORD PTR [12+ebp]                       ;1.18
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;54.5
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_BACKPOINTR] ;54.5
        mov       ebx, DWORD PTR [ecx]                          ;54.5
        sub       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;54.115
        imul      ecx, ebx, 152                                 ;54.115
        mov       esi, DWORD PTR [28+edx+ecx]                   ;54.5
        sub       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_BACKPOINTR+32] ;54.115
        mov       edi, DWORD PTR [4+eax+esi*4]                  ;54.9
        sub       edi, DWORD PTR [eax+esi*4]                    ;54.9
        test      edi, edi                                      ;54.111
        jle       .B1.162       ; Prob 16%                      ;54.111
                                ; LOE edx ecx xmm5
.B1.24:                         ; Preds .B1.23 .B1.162
        movss     xmm2, DWORD PTR [_2il0floatpacket.5]          ;57.74
        mulss     xmm5, xmm2                                    ;57.33
        mulss     xmm5, DWORD PTR [20+edx+ecx]                  ;57.33
        movss     xmm1, DWORD PTR [_2il0floatpacket.7]          ;57.33
        andps     xmm1, xmm5                                    ;57.33
        pxor      xmm5, xmm1                                    ;57.33
        movss     xmm4, DWORD PTR [_2il0floatpacket.8]          ;57.33
        movaps    xmm3, xmm5                                    ;57.33
        movaps    xmm0, xmm5                                    ;57.33
        cmpltss   xmm3, xmm4                                    ;57.33
        andps     xmm4, xmm3                                    ;57.33
        mov       ebx, DWORD PTR [20+ebp]                       ;57.7
        addss     xmm0, xmm4                                    ;57.33
        mov       ebx, DWORD PTR [ebx]                          ;57.7
        mov       edx, ebx                                      ;57.7
        mov       esi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;57.82
        sub       edx, esi                                      ;57.7
        shl       edx, 8                                        ;57.7
        subss     xmm0, xmm4                                    ;57.33
        movaps    xmm3, xmm0                                    ;57.33
        mov       edi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;57.82
        subss     xmm3, xmm5                                    ;57.33
        movss     xmm5, DWORD PTR [_2il0floatpacket.9]          ;57.33
        movaps    xmm6, xmm3                                    ;57.33
        movaps    xmm7, xmm5                                    ;57.33
        cmpnless  xmm6, xmm5                                    ;57.33
        addss     xmm7, xmm5                                    ;57.33
        cmpless   xmm3, DWORD PTR [_2il0floatpacket.10]         ;57.33
        andps     xmm6, xmm7                                    ;57.33
        andps     xmm3, xmm7                                    ;57.33
        subss     xmm0, xmm6                                    ;57.33
        addss     xmm0, xmm3                                    ;57.33
        orps      xmm0, xmm1                                    ;57.33
        cvtss2si  eax, xmm0                                     ;57.33
        cvtsi2ss  xmm0, eax                                     ;57.33
        divss     xmm0, xmm2                                    ;57.7
        movss     DWORD PTR [240+edi+edx], xmm0                 ;57.7
                                ; LOE ebx esi edi
.B1.25:                         ; Preds .B1.24 .B1.163
        neg       esi                                           ;60.5
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE+32] ;60.36
        add       esi, ebx                                      ;60.5
        neg       edx                                           ;60.5
        shl       esi, 8                                        ;60.5
        add       edx, ebx                                      ;60.5
        shl       edx, 6                                        ;60.5
        mov       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE] ;60.36
        mov       eax, DWORD PTR [240+edi+esi]                  ;60.5
        mov       DWORD PTR [8+ecx+edx], eax                    ;60.5
        mov       eax, DWORD PTR [8+ebp]                        ;63.5
        movss     xmm1, DWORD PTR [eax]                         ;63.5
        mov       eax, DWORD PTR [eax]                          ;63.5
        comiss    xmm1, DWORD PTR [_DYNUST_MAIN_MODULE_mp_STARTTM] ;63.10
        jb        .B1.28        ; Prob 50%                      ;63.10
                                ; LOE eax edx ecx ebx esi edi xmm1
.B1.26:                         ; Preds .B1.25
        movss     xmm0, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ENDTM] ;63.10
        comiss    xmm0, xmm1                                    ;63.27
        jbe       .B1.28        ; Prob 50%                      ;63.27
                                ; LOE eax edx ecx ebx esi edi
.B1.27:                         ; Preds .B1.26
        mov       BYTE PTR [237+edi+esi], 1                     ;64.7
        jmp       .B1.29        ; Prob 100%                     ;64.7
                                ; LOE eax edx ecx ebx
.B1.28:                         ; Preds .B1.25 .B1.26
        mov       BYTE PTR [237+edi+esi], 0                     ;66.7
        inc       DWORD PTR [_DYNUST_MAIN_MODULE_mp_NUMNTAG]    ;67.7
                                ; LOE eax edx ecx ebx
.B1.29:                         ; Preds .B1.28 .B1.27
        mov       edi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;69.5
        neg       edi                                           ;69.5
        add       edi, ebx                                      ;69.5
        shl       edi, 5                                        ;69.5
        mov       esi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;69.5
        mov       DWORD PTR [4+ecx+edx], ebx                    ;78.5
        test      BYTE PTR [_DYNUST_MAIN_MODULE_mp_CHECKFLAG], 1 ;83.13
        mov       DWORD PTR [12+esi+edi], eax                   ;69.5
        je        .B1.31        ; Prob 60%                      ;83.13
                                ; LOE
.B1.30:                         ; Preds .B1.29
        test      BYTE PTR [_DYNUST_MAIN_MODULE_mp_MISFLAG], 1  ;83.26
        je        .B1.101       ; Prob 60%                      ;83.26
                                ; LOE
.B1.31:                         ; Preds .B1.29 .B1.30
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NZONES] ;84.27
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NUMACTZONE+36] ;84.9
        test      edi, edi                                      ;84.9
        mov       DWORD PTR [36+esp], eax                       ;84.27
        jle       .B1.33        ; Prob 11%                      ;84.9
                                ; LOE edi
.B1.32:                         ; Preds .B1.31
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NUMACTZONE+24] ;84.9
        test      edx, edx                                      ;84.9
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NUMACTZONE] ;84.9
        mov       ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NUMACTZONE+40] ;84.9
        jg        .B1.84        ; Prob 50%                      ;84.9
                                ; LOE edx ebx esi edi
.B1.33:                         ; Preds .B1.100 .B1.31 .B1.32
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DSTMP2+36] ;95.9
        mov       ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DSTMP2+44] ;95.9
        mov       DWORD PTR [140+esp], eax                      ;95.9
        cmp       DWORD PTR [36+esp], 0                         ;85.9
        jle       .B1.153       ; Prob 2%                       ;85.9
                                ; LOE ebx
.B1.34:                         ; Preds .B1.33
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DEMORIGACU+32] ;86.12
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DEMORIGACU] ;86.12
        mov       DWORD PTR [52+esp], esi                       ;86.12
        mov       DWORD PTR [68+esp], edi                       ;86.12
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DEMORIGACU+44] ;86.12
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DEMORIGACU+40] ;86.12
        mov       DWORD PTR [64+esp], esi                       ;86.12
        mov       DWORD PTR [60+esp], edi                       ;86.12
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DEMORIGACUT+32] ;88.12
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DEMORIGACUT] ;88.12
        mov       DWORD PTR [56+esp], esi                       ;88.12
        mov       DWORD PTR [48+esp], edi                       ;88.12
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DEMORIGACUT+44] ;88.12
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DEMORIGACUT+40] ;88.12
        mov       DWORD PTR [44+esp], esi                       ;88.12
        mov       DWORD PTR [40+esp], edi                       ;88.12
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DEMORIGACUH+32] ;91.12
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DEMORIGACUH] ;91.12
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DSTMP2+56] ;95.9
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DSTMP2+40] ;95.9
        mov       DWORD PTR [24+esp], esi                       ;91.12
        mov       DWORD PTR [20+esp], edi                       ;91.12
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DEMORIGACUH+44] ;91.12
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DEMORIGACUH+40] ;91.12
        mov       DWORD PTR [28+esp], edx                       ;95.9
        mov       DWORD PTR [152+esp], eax                      ;95.9
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DSTMP2] ;95.9
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DSTMP2+52] ;95.9
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DSTMP2+32] ;95.9
        mov       DWORD PTR [16+esp], esi                       ;91.12
        mov       DWORD PTR [4+esp], edi                        ;91.12
        cmp       DWORD PTR [140+esp], 0                        ;86.12
        jg        .B1.38        ; Prob 50%                      ;86.12
                                ; LOE eax edx ecx ebx
.B1.35:                         ; Preds .B1.34
        mov       edx, DWORD PTR [36+esp]                       ;
        xor       eax, eax                                      ;
                                ; LOE eax edx
.B1.36:                         ; Preds .B1.35 .B1.36
        inc       eax                                           ;93.12
        cmp       eax, edx                                      ;91.12
        jb        .B1.36        ; Prob 82%                      ;91.12
        jmp       .B1.101       ; Prob 100%                     ;91.12
                                ; LOE eax edx
.B1.38:                         ; Preds .B1.34
        mov       esi, DWORD PTR [140+esp]                      ;
        mov       edi, esi                                      ;
        shr       edi, 31                                       ;
        add       edi, esi                                      ;
        sar       edi, 1                                        ;
        mov       DWORD PTR [144+esp], 0                        ;
        mov       DWORD PTR [80+esp], ecx                       ;
        mov       DWORD PTR [76+esp], edx                       ;
        mov       DWORD PTR [32+esp], eax                       ;
        mov       DWORD PTR [156+esp], edi                      ;
        mov       DWORD PTR [72+esp], ebx                       ;
                                ; LOE
.B1.39:                         ; Preds .B1.83 .B1.143 .B1.38
        cmp       DWORD PTR [156+esp], 0                        ;86.12
        jbe       .B1.152       ; Prob 10%                      ;86.12
                                ; LOE
.B1.40:                         ; Preds .B1.39
        mov       esi, DWORD PTR [28+esp]                       ;
        mov       ecx, DWORD PTR [76+esp]                       ;
        imul      esi, ecx                                      ;
        mov       edi, DWORD PTR [72+esp]                       ;
        neg       esi                                           ;
        mov       ebx, DWORD PTR [152+esp]                      ;
        imul      edi, ebx                                      ;
        mov       eax, DWORD PTR [32+esp]                       ;
        add       esi, DWORD PTR [80+esp]                       ;
        mov       DWORD PTR [84+esp], 0                         ;
        lea       edx, DWORD PTR [eax*8]                        ;
        mov       eax, DWORD PTR [144+esp]                      ;
        imul      eax, ecx                                      ;
        sub       ecx, edi                                      ;
        add       ecx, esi                                      ;
        add       edi, ebx                                      ;
        add       esi, DWORD PTR [76+esp]                       ;
        add       ecx, edi                                      ;
        sub       ecx, edx                                      ;
        sub       esi, edx                                      ;
        add       ecx, eax                                      ;
        add       eax, esi                                      ;
        mov       DWORD PTR [96+esp], eax                       ;
        mov       eax, DWORD PTR [64+esp]                       ;
        mov       esi, DWORD PTR [60+esp]                       ;
        imul      eax, esi                                      ;
        mov       ebx, DWORD PTR [52+esp]                       ;
        mov       DWORD PTR [172+esp], ecx                      ;
        mov       ecx, DWORD PTR [144+esp]                      ;
        mov       edi, DWORD PTR [68+esp]                       ;
        lea       edx, DWORD PTR [ebx*4]                        ;
        imul      ecx, esi                                      ;
        mov       ebx, edi                                      ;
        sub       ebx, edx                                      ;
        sub       edx, eax                                      ;
        sub       eax, esi                                      ;
        add       ebx, edx                                      ;
        neg       eax                                           ;
        add       ebx, esi                                      ;
        add       eax, edi                                      ;
        add       ebx, ecx                                      ;
        add       eax, ecx                                      ;
        mov       edx, DWORD PTR [84+esp]                       ;
        mov       ecx, DWORD PTR [96+esp]                       ;
        mov       DWORD PTR [160+esp], eax                      ;
        mov       eax, edx                                      ;
                                ; LOE eax edx ecx ebx
.B1.41:                         ; Preds .B1.41 .B1.40
        mov       esi, DWORD PTR [160+esp]                      ;86.12
        mov       edi, DWORD PTR [esi+eax*8]                    ;86.12
        mov       DWORD PTR [8+ecx+edx*2], edi                  ;86.12
        mov       edi, DWORD PTR [172+esp]                      ;86.12
        mov       esi, DWORD PTR [4+ebx+eax*8]                  ;86.12
        inc       eax                                           ;86.12
        mov       DWORD PTR [8+edi+edx*2], esi                  ;86.12
        add       edx, DWORD PTR [152+esp]                      ;86.12
        cmp       eax, DWORD PTR [156+esp]                      ;86.12
        jb        .B1.41        ; Prob 63%                      ;86.12
                                ; LOE eax edx ecx ebx
.B1.42:                         ; Preds .B1.41
        mov       DWORD PTR [84+esp], eax                       ;
        lea       esi, DWORD PTR [1+eax+eax]                    ;86.12
                                ; LOE esi
.B1.43:                         ; Preds .B1.42 .B1.152
        lea       edx, DWORD PTR [-1+esi]                       ;86.12
        cmp       edx, DWORD PTR [140+esp]                      ;86.12
        jae       .B1.45        ; Prob 10%                      ;86.12
                                ; LOE esi
.B1.44:                         ; Preds .B1.43
        mov       ecx, DWORD PTR [64+esp]                       ;86.12
        neg       ecx                                           ;86.12
        mov       eax, DWORD PTR [144+esp]                      ;86.12
        add       ecx, eax                                      ;86.12
        mov       ebx, DWORD PTR [60+esp]                       ;86.12
        imul      ecx, ebx                                      ;86.12
        mov       DWORD PTR [100+esp], ecx                      ;86.12
        mov       ecx, DWORD PTR [68+esp]                       ;86.12
        mov       edi, DWORD PTR [72+esp]                       ;86.12
        mov       edx, DWORD PTR [152+esp]                      ;86.12
        imul      edi, edx                                      ;86.12
        add       ebx, ecx                                      ;86.12
        lea       ecx, DWORD PTR [ebx+esi*4]                    ;86.12
        mov       ebx, DWORD PTR [28+esp]                       ;86.12
        neg       ebx                                           ;86.12
        dec       esi                                           ;86.12
        add       ebx, eax                                      ;86.12
        imul      esi, edx                                      ;86.12
        mov       edx, DWORD PTR [32+esp]                       ;86.12
        mov       eax, DWORD PTR [76+esp]                       ;86.12
        imul      ebx, eax                                      ;86.12
        sub       eax, edi                                      ;86.12
        add       eax, DWORD PTR [80+esp]                       ;86.12
        add       eax, edi                                      ;86.12
        lea       edi, DWORD PTR [edx*8]                        ;86.12
        sub       eax, edi                                      ;86.12
        mov       edx, DWORD PTR [144+esp]                      ;87.12
        lea       eax, DWORD PTR [8+esi+eax]                    ;86.12
        mov       esi, DWORD PTR [100+esp]                      ;86.12
        mov       ecx, DWORD PTR [-4+ecx+esi]                   ;86.12
        mov       DWORD PTR [eax+ebx], ecx                      ;86.12
        lea       ebx, DWORD PTR [1+edx]                        ;87.12
        mov       DWORD PTR [148+esp], ebx                      ;87.12
        jmp       .B1.46        ; Prob 100%                     ;87.12
                                ; LOE
.B1.45:                         ; Preds .B1.43
        mov       eax, DWORD PTR [144+esp]                      ;87.12
        lea       edx, DWORD PTR [1+eax]                        ;87.12
        mov       DWORD PTR [148+esp], edx                      ;87.12
                                ; LOE
.B1.46:                         ; Preds .B1.45 .B1.44
        cmp       DWORD PTR [156+esp], 0                        ;87.12
        jbe       .B1.151       ; Prob 10%                      ;87.12
                                ; LOE
.B1.47:                         ; Preds .B1.46
        mov       ecx, DWORD PTR [28+esp]                       ;
        mov       edi, DWORD PTR [76+esp]                       ;
        imul      ecx, edi                                      ;
        mov       eax, DWORD PTR [72+esp]                       ;
        neg       ecx                                           ;
        mov       edx, DWORD PTR [152+esp]                      ;
        imul      eax, edx                                      ;
        mov       esi, DWORD PTR [32+esp]                       ;
        add       ecx, DWORD PTR [80+esp]                       ;
        mov       DWORD PTR [104+esp], 0                        ;
        lea       ebx, DWORD PTR [esi*8]                        ;
        mov       esi, DWORD PTR [144+esp]                      ;
        imul      esi, edi                                      ;
        sub       edi, eax                                      ;
        add       edi, ecx                                      ;
        add       eax, edx                                      ;
        add       ecx, DWORD PTR [76+esp]                       ;
        add       edi, eax                                      ;
        sub       edi, ebx                                      ;
        sub       ecx, ebx                                      ;
        mov       edx, DWORD PTR [104+esp]                      ;
        add       edi, esi                                      ;
        mov       ebx, DWORD PTR [156+esp]                      ;
        add       esi, ecx                                      ;
        mov       ecx, DWORD PTR [148+esp]                      ;
        mov       eax, edx                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B1.48:                         ; Preds .B1.48 .B1.47
        inc       edx                                           ;87.12
        mov       WORD PTR [12+esi+eax*2], cx                   ;87.12
        mov       WORD PTR [12+edi+eax*2], cx                   ;87.12
        add       eax, DWORD PTR [152+esp]                      ;87.12
        cmp       edx, ebx                                      ;87.12
        jb        .B1.48        ; Prob 63%                      ;87.12
                                ; LOE eax edx ecx ebx esi edi
.B1.49:                         ; Preds .B1.48
        lea       edx, DWORD PTR [1+edx+edx]                    ;87.12
        lea       eax, DWORD PTR [-1+edx]                       ;87.12
        cmp       eax, DWORD PTR [140+esp]                      ;87.12
        jae       .B1.51        ; Prob 10%                      ;87.12
                                ; LOE edx
.B1.50:                         ; Preds .B1.49 .B1.151
        mov       esi, DWORD PTR [72+esp]                       ;87.12
        dec       edx                                           ;87.12
        mov       ecx, DWORD PTR [152+esp]                      ;87.12
        imul      esi, ecx                                      ;87.12
        imul      edx, ecx                                      ;87.12
        mov       eax, DWORD PTR [28+esp]                       ;87.12
        neg       eax                                           ;87.12
        add       eax, DWORD PTR [144+esp]                      ;87.12
        mov       edi, DWORD PTR [32+esp]                       ;87.12
        mov       ebx, DWORD PTR [76+esp]                       ;87.12
        imul      eax, ebx                                      ;87.12
        sub       ebx, esi                                      ;87.12
        add       ebx, DWORD PTR [80+esp]                       ;87.12
        add       ebx, esi                                      ;87.12
        lea       esi, DWORD PTR [edi*8]                        ;87.12
        sub       ebx, esi                                      ;87.12
        lea       ecx, DWORD PTR [12+edx+ebx]                   ;87.12
        mov       edx, DWORD PTR [148+esp]                      ;87.12
        mov       WORD PTR [ecx+eax], dx                        ;87.12
                                ; LOE
.B1.51:                         ; Preds .B1.49 .B1.151 .B1.50
        cmp       DWORD PTR [156+esp], 0                        ;88.12
        jbe       .B1.150       ; Prob 10%                      ;88.12
                                ; LOE
.B1.52:                         ; Preds .B1.51
        mov       esi, DWORD PTR [28+esp]                       ;
        mov       ecx, DWORD PTR [76+esp]                       ;
        imul      esi, ecx                                      ;
        mov       edi, DWORD PTR [72+esp]                       ;
        neg       esi                                           ;
        mov       ebx, DWORD PTR [152+esp]                      ;
        imul      edi, ebx                                      ;
        mov       eax, DWORD PTR [32+esp]                       ;
        add       esi, DWORD PTR [80+esp]                       ;
        mov       DWORD PTR [88+esp], 0                         ;
        lea       edx, DWORD PTR [eax*8]                        ;
        mov       eax, DWORD PTR [144+esp]                      ;
        imul      eax, ecx                                      ;
        sub       ecx, edi                                      ;
        add       ecx, esi                                      ;
        add       edi, ebx                                      ;
        add       esi, DWORD PTR [76+esp]                       ;
        add       ecx, edi                                      ;
        sub       ecx, edx                                      ;
        sub       esi, edx                                      ;
        add       ecx, eax                                      ;
        add       eax, esi                                      ;
        mov       DWORD PTR [108+esp], eax                      ;
        mov       eax, DWORD PTR [44+esp]                       ;
        mov       esi, DWORD PTR [40+esp]                       ;
        imul      eax, esi                                      ;
        mov       ebx, DWORD PTR [56+esp]                       ;
        mov       DWORD PTR [176+esp], ecx                      ;
        mov       ecx, DWORD PTR [144+esp]                      ;
        mov       edi, DWORD PTR [48+esp]                       ;
        lea       edx, DWORD PTR [ebx*4]                        ;
        imul      ecx, esi                                      ;
        mov       ebx, edi                                      ;
        sub       ebx, edx                                      ;
        sub       edx, eax                                      ;
        sub       eax, esi                                      ;
        add       ebx, edx                                      ;
        neg       eax                                           ;
        add       ebx, esi                                      ;
        add       eax, edi                                      ;
        add       ebx, ecx                                      ;
        add       eax, ecx                                      ;
        mov       edx, DWORD PTR [88+esp]                       ;
        mov       ecx, DWORD PTR [108+esp]                      ;
        mov       DWORD PTR [164+esp], eax                      ;
        mov       eax, edx                                      ;
                                ; LOE eax edx ecx ebx
.B1.53:                         ; Preds .B1.53 .B1.52
        mov       esi, DWORD PTR [164+esp]                      ;88.12
        mov       edi, DWORD PTR [esi+eax*8]                    ;88.12
        mov       DWORD PTR [16+ecx+edx*2], edi                 ;88.12
        mov       edi, DWORD PTR [176+esp]                      ;88.12
        mov       esi, DWORD PTR [4+ebx+eax*8]                  ;88.12
        inc       eax                                           ;88.12
        mov       DWORD PTR [16+edi+edx*2], esi                 ;88.12
        add       edx, DWORD PTR [152+esp]                      ;88.12
        cmp       eax, DWORD PTR [156+esp]                      ;88.12
        jb        .B1.53        ; Prob 63%                      ;88.12
                                ; LOE eax edx ecx ebx
.B1.54:                         ; Preds .B1.53
        mov       DWORD PTR [88+esp], eax                       ;
        mov       esi, eax                                      ;88.12
        lea       esi, DWORD PTR [1+esi+esi]                    ;88.12
        lea       edx, DWORD PTR [-1+esi]                       ;88.12
        cmp       edx, DWORD PTR [140+esp]                      ;88.12
        jae       .B1.56        ; Prob 10%                      ;88.12
                                ; LOE esi
.B1.55:                         ; Preds .B1.54 .B1.150
        mov       ecx, DWORD PTR [44+esp]                       ;88.12
        neg       ecx                                           ;88.12
        mov       eax, DWORD PTR [144+esp]                      ;88.12
        add       ecx, eax                                      ;88.12
        mov       ebx, DWORD PTR [40+esp]                       ;88.12
        imul      ecx, ebx                                      ;88.12
        mov       DWORD PTR [112+esp], ecx                      ;88.12
        mov       ecx, DWORD PTR [48+esp]                       ;88.12
        mov       edi, DWORD PTR [72+esp]                       ;88.12
        mov       edx, DWORD PTR [152+esp]                      ;88.12
        imul      edi, edx                                      ;88.12
        add       ebx, ecx                                      ;88.12
        lea       ecx, DWORD PTR [ebx+esi*4]                    ;88.12
        mov       ebx, DWORD PTR [28+esp]                       ;88.12
        neg       ebx                                           ;88.12
        dec       esi                                           ;88.12
        add       ebx, eax                                      ;88.12
        imul      esi, edx                                      ;88.12
        mov       edx, DWORD PTR [32+esp]                       ;88.12
        mov       eax, DWORD PTR [76+esp]                       ;88.12
        imul      ebx, eax                                      ;88.12
        sub       eax, edi                                      ;88.12
        add       eax, DWORD PTR [80+esp]                       ;88.12
        add       eax, edi                                      ;88.12
        lea       edi, DWORD PTR [edx*8]                        ;88.12
        sub       eax, edi                                      ;88.12
        lea       eax, DWORD PTR [16+esi+eax]                   ;88.12
        mov       esi, DWORD PTR [112+esp]                      ;88.12
        mov       ecx, DWORD PTR [-4+ecx+esi]                   ;88.12
        mov       DWORD PTR [eax+ebx], ecx                      ;88.12
                                ; LOE
.B1.56:                         ; Preds .B1.54 .B1.150 .B1.55
        cmp       DWORD PTR [156+esp], 0                        ;89.12
        jbe       .B1.149       ; Prob 10%                      ;89.12
                                ; LOE
.B1.57:                         ; Preds .B1.56
        mov       edx, DWORD PTR [28+esp]                       ;
        mov       esi, DWORD PTR [76+esp]                       ;
        mov       ebx, DWORD PTR [72+esp]                       ;
        mov       ecx, DWORD PTR [152+esp]                      ;
        imul      edx, esi                                      ;
        imul      ebx, ecx                                      ;
        imul      esi, DWORD PTR [36+esp]                       ;
        mov       edi, DWORD PTR [32+esp]                       ;
        add       esi, DWORD PTR [80+esp]                       ;
        mov       DWORD PTR [116+esp], 0                        ;
        lea       eax, DWORD PTR [edi*8]                        ;
        mov       edi, ebx                                      ;
        add       ebx, edx                                      ;
        sub       edi, eax                                      ;
        neg       ebx                                           ;
        sub       eax, ecx                                      ;
        add       ebx, esi                                      ;
        sub       esi, edx                                      ;
        add       edi, ebx                                      ;
        sub       esi, eax                                      ;
        mov       edx, 1065353216                               ;89.12
        mov       ecx, DWORD PTR [116+esp]                      ;
        mov       eax, ecx                                      ;
        mov       ebx, edi                                      ;
        mov       edi, DWORD PTR [156+esp]                      ;
                                ; LOE eax edx ecx ebx esi edi
.B1.58:                         ; Preds .B1.58 .B1.57
        inc       ecx                                           ;89.12
        mov       DWORD PTR [16+ebx+eax*2], edx                 ;89.12
        mov       DWORD PTR [16+esi+eax*2], edx                 ;89.12
        add       eax, DWORD PTR [152+esp]                      ;89.12
        cmp       ecx, edi                                      ;89.12
        jb        .B1.58        ; Prob 63%                      ;89.12
                                ; LOE eax edx ecx ebx esi edi
.B1.59:                         ; Preds .B1.58
        lea       eax, DWORD PTR [1+ecx+ecx]                    ;89.12
                                ; LOE eax
.B1.60:                         ; Preds .B1.59 .B1.149
        lea       edx, DWORD PTR [-1+eax]                       ;89.12
        cmp       edx, DWORD PTR [140+esp]                      ;89.12
        jae       .B1.62        ; Prob 10%                      ;89.12
                                ; LOE eax
.B1.61:                         ; Preds .B1.60
        mov       edx, DWORD PTR [32+esp]                       ;89.12
        dec       eax                                           ;89.12
        mov       ebx, DWORD PTR [36+esp]                       ;89.12
        sub       ebx, DWORD PTR [28+esp]                       ;89.12
        imul      eax, DWORD PTR [152+esp]                      ;89.12
        lea       ecx, DWORD PTR [edx*8]                        ;89.12
        imul      ebx, DWORD PTR [76+esp]                       ;89.12
        neg       ecx                                           ;89.12
        add       ecx, DWORD PTR [80+esp]                       ;89.12
        lea       eax, DWORD PTR [16+eax+ecx]                   ;89.12
        mov       DWORD PTR [eax+ebx], 1065353216               ;89.12
                                ; LOE
.B1.62:                         ; Preds .B1.60 .B1.61
        cmp       DWORD PTR [156+esp], 0                        ;90.12
        jbe       .B1.148       ; Prob 10%                      ;90.12
                                ; LOE
.B1.63:                         ; Preds .B1.62
        mov       ecx, DWORD PTR [28+esp]                       ;
        mov       edi, DWORD PTR [76+esp]                       ;
        imul      ecx, edi                                      ;
        mov       eax, DWORD PTR [72+esp]                       ;
        neg       ecx                                           ;
        mov       edx, DWORD PTR [152+esp]                      ;
        imul      eax, edx                                      ;
        mov       esi, DWORD PTR [32+esp]                       ;
        add       ecx, DWORD PTR [80+esp]                       ;
        mov       DWORD PTR [120+esp], 0                        ;
        lea       ebx, DWORD PTR [esi*8]                        ;
        mov       esi, DWORD PTR [144+esp]                      ;
        imul      esi, edi                                      ;
        sub       edi, eax                                      ;
        add       edi, ecx                                      ;
        add       eax, edx                                      ;
        add       ecx, DWORD PTR [76+esp]                       ;
        add       edi, eax                                      ;
        sub       edi, ebx                                      ;
        sub       ecx, ebx                                      ;
        mov       edx, DWORD PTR [120+esp]                      ;
        add       edi, esi                                      ;
        mov       ebx, DWORD PTR [156+esp]                      ;
        add       esi, ecx                                      ;
        mov       ecx, DWORD PTR [148+esp]                      ;
        mov       eax, edx                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B1.64:                         ; Preds .B1.64 .B1.63
        inc       edx                                           ;90.12
        mov       WORD PTR [20+esi+eax*2], cx                   ;90.12
        mov       WORD PTR [20+edi+eax*2], cx                   ;90.12
        add       eax, DWORD PTR [152+esp]                      ;90.12
        cmp       edx, ebx                                      ;90.12
        jb        .B1.64        ; Prob 63%                      ;90.12
                                ; LOE eax edx ecx ebx esi edi
.B1.65:                         ; Preds .B1.64
        lea       edx, DWORD PTR [1+edx+edx]                    ;90.12
        lea       eax, DWORD PTR [-1+edx]                       ;90.12
        cmp       eax, DWORD PTR [140+esp]                      ;90.12
        jae       .B1.67        ; Prob 10%                      ;90.12
                                ; LOE edx
.B1.66:                         ; Preds .B1.65 .B1.148
        mov       esi, DWORD PTR [72+esp]                       ;90.12
        dec       edx                                           ;90.12
        mov       ecx, DWORD PTR [152+esp]                      ;90.12
        imul      esi, ecx                                      ;90.12
        imul      edx, ecx                                      ;90.12
        mov       eax, DWORD PTR [28+esp]                       ;90.12
        neg       eax                                           ;90.12
        add       eax, DWORD PTR [144+esp]                      ;90.12
        mov       edi, DWORD PTR [32+esp]                       ;90.12
        mov       ebx, DWORD PTR [76+esp]                       ;90.12
        imul      eax, ebx                                      ;90.12
        sub       ebx, esi                                      ;90.12
        add       ebx, DWORD PTR [80+esp]                       ;90.12
        add       ebx, esi                                      ;90.12
        lea       esi, DWORD PTR [edi*8]                        ;90.12
        sub       ebx, esi                                      ;90.12
        lea       ecx, DWORD PTR [20+edx+ebx]                   ;90.12
        mov       edx, DWORD PTR [148+esp]                      ;90.12
        mov       WORD PTR [ecx+eax], dx                        ;90.12
                                ; LOE
.B1.67:                         ; Preds .B1.65 .B1.148 .B1.66
        cmp       DWORD PTR [156+esp], 0                        ;91.12
        jbe       .B1.147       ; Prob 10%                      ;91.12
                                ; LOE
.B1.68:                         ; Preds .B1.67
        mov       esi, DWORD PTR [28+esp]                       ;
        mov       ecx, DWORD PTR [76+esp]                       ;
        imul      esi, ecx                                      ;
        mov       edi, DWORD PTR [72+esp]                       ;
        neg       esi                                           ;
        mov       ebx, DWORD PTR [152+esp]                      ;
        imul      edi, ebx                                      ;
        mov       eax, DWORD PTR [32+esp]                       ;
        add       esi, DWORD PTR [80+esp]                       ;
        mov       DWORD PTR [92+esp], 0                         ;
        lea       edx, DWORD PTR [eax*8]                        ;
        mov       eax, DWORD PTR [144+esp]                      ;
        imul      eax, ecx                                      ;
        sub       ecx, edi                                      ;
        add       ecx, esi                                      ;
        add       edi, ebx                                      ;
        add       esi, DWORD PTR [76+esp]                       ;
        add       ecx, edi                                      ;
        sub       ecx, edx                                      ;
        sub       esi, edx                                      ;
        add       ecx, eax                                      ;
        add       eax, esi                                      ;
        mov       DWORD PTR [124+esp], eax                      ;
        mov       eax, DWORD PTR [16+esp]                       ;
        mov       esi, DWORD PTR [4+esp]                        ;
        imul      eax, esi                                      ;
        mov       ebx, DWORD PTR [24+esp]                       ;
        mov       DWORD PTR [180+esp], ecx                      ;
        mov       ecx, DWORD PTR [144+esp]                      ;
        mov       edi, DWORD PTR [20+esp]                       ;
        lea       edx, DWORD PTR [ebx*4]                        ;
        imul      ecx, esi                                      ;
        mov       ebx, edi                                      ;
        sub       ebx, edx                                      ;
        sub       edx, eax                                      ;
        sub       eax, esi                                      ;
        add       ebx, edx                                      ;
        neg       eax                                           ;
        add       ebx, esi                                      ;
        add       eax, edi                                      ;
        add       ebx, ecx                                      ;
        add       eax, ecx                                      ;
        mov       edx, DWORD PTR [92+esp]                       ;
        mov       ecx, DWORD PTR [124+esp]                      ;
        mov       DWORD PTR [168+esp], eax                      ;
        mov       eax, edx                                      ;
                                ; LOE eax edx ecx ebx
.B1.69:                         ; Preds .B1.69 .B1.68
        mov       esi, DWORD PTR [168+esp]                      ;91.12
        mov       edi, DWORD PTR [esi+eax*8]                    ;91.12
        mov       DWORD PTR [24+ecx+edx*2], edi                 ;91.12
        mov       edi, DWORD PTR [180+esp]                      ;91.12
        mov       esi, DWORD PTR [4+ebx+eax*8]                  ;91.12
        inc       eax                                           ;91.12
        mov       DWORD PTR [24+edi+edx*2], esi                 ;91.12
        add       edx, DWORD PTR [152+esp]                      ;91.12
        cmp       eax, DWORD PTR [156+esp]                      ;91.12
        jb        .B1.69        ; Prob 63%                      ;91.12
                                ; LOE eax edx ecx ebx
.B1.70:                         ; Preds .B1.69
        mov       DWORD PTR [92+esp], eax                       ;
        mov       esi, eax                                      ;91.12
        lea       esi, DWORD PTR [1+esi+esi]                    ;91.12
        lea       edx, DWORD PTR [-1+esi]                       ;91.12
        cmp       edx, DWORD PTR [140+esp]                      ;91.12
        jae       .B1.72        ; Prob 10%                      ;91.12
                                ; LOE esi
.B1.71:                         ; Preds .B1.70 .B1.147
        mov       ecx, DWORD PTR [16+esp]                       ;91.12
        neg       ecx                                           ;91.12
        mov       eax, DWORD PTR [144+esp]                      ;91.12
        add       ecx, eax                                      ;91.12
        mov       ebx, DWORD PTR [4+esp]                        ;91.12
        imul      ecx, ebx                                      ;91.12
        mov       DWORD PTR [128+esp], ecx                      ;91.12
        mov       ecx, DWORD PTR [20+esp]                       ;91.12
        mov       edi, DWORD PTR [72+esp]                       ;91.12
        mov       edx, DWORD PTR [152+esp]                      ;91.12
        imul      edi, edx                                      ;91.12
        add       ebx, ecx                                      ;91.12
        lea       ecx, DWORD PTR [ebx+esi*4]                    ;91.12
        mov       ebx, DWORD PTR [28+esp]                       ;91.12
        neg       ebx                                           ;91.12
        dec       esi                                           ;91.12
        add       ebx, eax                                      ;91.12
        imul      esi, edx                                      ;91.12
        mov       edx, DWORD PTR [32+esp]                       ;91.12
        mov       eax, DWORD PTR [76+esp]                       ;91.12
        imul      ebx, eax                                      ;91.12
        sub       eax, edi                                      ;91.12
        add       eax, DWORD PTR [80+esp]                       ;91.12
        add       eax, edi                                      ;91.12
        lea       edi, DWORD PTR [edx*8]                        ;91.12
        sub       eax, edi                                      ;91.12
        lea       eax, DWORD PTR [24+esi+eax]                   ;91.12
        mov       esi, DWORD PTR [128+esp]                      ;91.12
        mov       ecx, DWORD PTR [-4+ecx+esi]                   ;91.12
        mov       DWORD PTR [eax+ebx], ecx                      ;91.12
                                ; LOE
.B1.72:                         ; Preds .B1.70 .B1.147 .B1.71
        cmp       DWORD PTR [156+esp], 0                        ;92.12
        jbe       .B1.146       ; Prob 10%                      ;92.12
                                ; LOE
.B1.73:                         ; Preds .B1.72
        mov       edx, DWORD PTR [28+esp]                       ;
        mov       esi, DWORD PTR [76+esp]                       ;
        mov       ebx, DWORD PTR [72+esp]                       ;
        mov       ecx, DWORD PTR [152+esp]                      ;
        imul      edx, esi                                      ;
        imul      ebx, ecx                                      ;
        imul      esi, DWORD PTR [36+esp]                       ;
        mov       edi, DWORD PTR [32+esp]                       ;
        add       esi, DWORD PTR [80+esp]                       ;
        mov       DWORD PTR [132+esp], 0                        ;
        lea       eax, DWORD PTR [edi*8]                        ;
        mov       edi, ebx                                      ;
        add       ebx, edx                                      ;
        sub       edi, eax                                      ;
        neg       ebx                                           ;
        sub       eax, ecx                                      ;
        add       ebx, esi                                      ;
        sub       esi, edx                                      ;
        add       edi, ebx                                      ;
        sub       esi, eax                                      ;
        mov       edx, 1065353216                               ;92.12
        mov       ecx, DWORD PTR [132+esp]                      ;
        mov       eax, ecx                                      ;
        mov       ebx, edi                                      ;
        mov       edi, DWORD PTR [156+esp]                      ;
                                ; LOE eax edx ecx ebx esi edi
.B1.74:                         ; Preds .B1.74 .B1.73
        inc       ecx                                           ;92.12
        mov       DWORD PTR [24+ebx+eax*2], edx                 ;92.12
        mov       DWORD PTR [24+esi+eax*2], edx                 ;92.12
        add       eax, DWORD PTR [152+esp]                      ;92.12
        cmp       ecx, edi                                      ;92.12
        jb        .B1.74        ; Prob 63%                      ;92.12
                                ; LOE eax edx ecx ebx esi edi
.B1.75:                         ; Preds .B1.74
        lea       eax, DWORD PTR [1+ecx+ecx]                    ;92.12
                                ; LOE eax
.B1.76:                         ; Preds .B1.75 .B1.146
        lea       edx, DWORD PTR [-1+eax]                       ;92.12
        cmp       edx, DWORD PTR [140+esp]                      ;92.12
        jae       .B1.78        ; Prob 10%                      ;92.12
                                ; LOE eax
.B1.77:                         ; Preds .B1.76
        mov       edx, DWORD PTR [32+esp]                       ;92.12
        dec       eax                                           ;92.12
        mov       ebx, DWORD PTR [36+esp]                       ;92.12
        sub       ebx, DWORD PTR [28+esp]                       ;92.12
        imul      eax, DWORD PTR [152+esp]                      ;92.12
        lea       ecx, DWORD PTR [edx*8]                        ;92.12
        imul      ebx, DWORD PTR [76+esp]                       ;92.12
        neg       ecx                                           ;92.12
        add       ecx, DWORD PTR [80+esp]                       ;92.12
        lea       eax, DWORD PTR [24+eax+ecx]                   ;92.12
        mov       DWORD PTR [eax+ebx], 1065353216               ;92.12
                                ; LOE
.B1.78:                         ; Preds .B1.76 .B1.77
        cmp       DWORD PTR [156+esp], 0                        ;93.12
        jbe       .B1.145       ; Prob 10%                      ;93.12
                                ; LOE
.B1.79:                         ; Preds .B1.78
        mov       ecx, DWORD PTR [28+esp]                       ;
        mov       edi, DWORD PTR [76+esp]                       ;
        imul      ecx, edi                                      ;
        mov       eax, DWORD PTR [72+esp]                       ;
        neg       ecx                                           ;
        mov       edx, DWORD PTR [152+esp]                      ;
        imul      eax, edx                                      ;
        mov       esi, DWORD PTR [32+esp]                       ;
        add       ecx, DWORD PTR [80+esp]                       ;
        mov       DWORD PTR [136+esp], 0                        ;
        lea       ebx, DWORD PTR [esi*8]                        ;
        mov       esi, DWORD PTR [144+esp]                      ;
        imul      esi, edi                                      ;
        sub       edi, eax                                      ;
        add       edi, ecx                                      ;
        add       eax, edx                                      ;
        add       ecx, DWORD PTR [76+esp]                       ;
        add       edi, eax                                      ;
        sub       edi, ebx                                      ;
        sub       ecx, ebx                                      ;
        mov       edx, DWORD PTR [136+esp]                      ;
        add       edi, esi                                      ;
        mov       ebx, DWORD PTR [156+esp]                      ;
        add       esi, ecx                                      ;
        mov       ecx, DWORD PTR [148+esp]                      ;
        mov       eax, edx                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B1.80:                         ; Preds .B1.80 .B1.79
        inc       edx                                           ;93.12
        mov       WORD PTR [28+esi+eax*2], cx                   ;93.12
        mov       WORD PTR [28+edi+eax*2], cx                   ;93.12
        add       eax, DWORD PTR [152+esp]                      ;93.12
        cmp       edx, ebx                                      ;93.12
        jb        .B1.80        ; Prob 63%                      ;93.12
                                ; LOE eax edx ecx ebx esi edi
.B1.81:                         ; Preds .B1.80
        lea       edx, DWORD PTR [1+edx+edx]                    ;93.12
                                ; LOE edx
.B1.82:                         ; Preds .B1.81 .B1.145
        lea       eax, DWORD PTR [-1+edx]                       ;93.12
        cmp       eax, DWORD PTR [140+esp]                      ;93.12
        jae       .B1.143       ; Prob 10%                      ;93.12
                                ; LOE edx
.B1.83:                         ; Preds .B1.82
        mov       esi, DWORD PTR [72+esp]                       ;93.12
        dec       edx                                           ;93.12
        mov       ecx, DWORD PTR [152+esp]                      ;93.12
        imul      esi, ecx                                      ;93.12
        imul      edx, ecx                                      ;93.12
        mov       eax, DWORD PTR [28+esp]                       ;93.12
        neg       eax                                           ;93.12
        add       eax, DWORD PTR [144+esp]                      ;93.12
        mov       edi, DWORD PTR [32+esp]                       ;93.12
        mov       ebx, DWORD PTR [76+esp]                       ;93.12
        imul      eax, ebx                                      ;93.12
        sub       ebx, esi                                      ;93.12
        add       ebx, DWORD PTR [80+esp]                       ;93.12
        add       ebx, esi                                      ;93.12
        lea       esi, DWORD PTR [edi*8]                        ;93.12
        sub       ebx, esi                                      ;93.12
        mov       ecx, DWORD PTR [148+esp]                      ;93.12
        mov       DWORD PTR [144+esp], ecx                      ;91.12
        cmp       ecx, DWORD PTR [36+esp]                       ;91.12
        lea       edx, DWORD PTR [28+edx+ebx]                   ;93.12
        mov       WORD PTR [edx+eax], cx                        ;93.12
        jb        .B1.39        ; Prob 82%                      ;91.12
        jmp       .B1.130       ; Prob 100%                     ;91.12
                                ; LOE edi
.B1.84:                         ; Preds .B1.32
        xor       ecx, ecx                                      ;
        mov       eax, esi                                      ;
        movd      xmm0, DWORD PTR [36+esp]                      ;84.9
        mov       DWORD PTR [4+esp], ecx                        ;
        mov       DWORD PTR [40+esp], esi                       ;
        pshufd    xmm0, xmm0, 0                                 ;84.9
        mov       DWORD PTR [16+esp], ecx                       ;
        mov       DWORD PTR [20+esp], ebx                       ;
        mov       DWORD PTR [24+esp], edi                       ;
        mov       esi, DWORD PTR [4+esp]                        ;
                                ; LOE eax edx esi xmm0
.B1.85:                         ; Preds .B1.100 .B1.84
        cmp       edx, 4                                        ;84.9
        jl        .B1.154       ; Prob 10%                      ;84.9
                                ; LOE eax edx esi xmm0
.B1.86:                         ; Preds .B1.85
        mov       ecx, DWORD PTR [40+esp]                       ;84.9
        lea       ebx, DWORD PTR [ecx+esi]                      ;84.9
        and       ebx, 15                                       ;84.9
        je        .B1.89        ; Prob 50%                      ;84.9
                                ; LOE eax edx ebx esi xmm0
.B1.87:                         ; Preds .B1.86
        test      bl, 3                                         ;84.9
        jne       .B1.154       ; Prob 10%                      ;84.9
                                ; LOE eax edx ebx esi xmm0
.B1.88:                         ; Preds .B1.87
        neg       ebx                                           ;84.9
        add       ebx, 16                                       ;84.9
        shr       ebx, 2                                        ;84.9
                                ; LOE eax edx ebx esi xmm0
.B1.89:                         ; Preds .B1.88 .B1.86
        lea       ecx, DWORD PTR [4+ebx]                        ;84.9
        cmp       edx, ecx                                      ;84.9
        jl        .B1.154       ; Prob 10%                      ;84.9
                                ; LOE eax edx ebx esi xmm0
.B1.90:                         ; Preds .B1.89
        mov       ecx, edx                                      ;84.9
        sub       ecx, ebx                                      ;84.9
        mov       edi, DWORD PTR [40+esp]                       ;
        and       ecx, 3                                        ;84.9
        neg       ecx                                           ;84.9
        add       ecx, edx                                      ;84.9
        test      ebx, ebx                                      ;84.9
        lea       edi, DWORD PTR [edi+esi]                      ;
        mov       DWORD PTR [44+esp], edi                       ;
        jbe       .B1.94        ; Prob 10%                      ;84.9
                                ; LOE eax edx ecx ebx esi edi xmm0
.B1.91:                         ; Preds .B1.90
        mov       DWORD PTR [32+esp], 0                         ;
        mov       DWORD PTR [4+esp], eax                        ;
        mov       DWORD PTR [28+esp], edx                       ;
        mov       eax, DWORD PTR [32+esp]                       ;
        mov       edx, edi                                      ;
        mov       edi, DWORD PTR [36+esp]                       ;
                                ; LOE eax edx ecx ebx esi edi xmm0
.B1.92:                         ; Preds .B1.92 .B1.91
        mov       DWORD PTR [edx+eax*4], edi                    ;84.9
        inc       eax                                           ;84.9
        cmp       eax, ebx                                      ;84.9
        jb        .B1.92        ; Prob 82%                      ;84.9
                                ; LOE eax edx ecx ebx esi edi xmm0
.B1.93:                         ; Preds .B1.92
        mov       eax, DWORD PTR [4+esp]                        ;
        mov       edx, DWORD PTR [28+esp]                       ;
                                ; LOE eax edx ecx ebx esi xmm0
.B1.94:                         ; Preds .B1.90 .B1.93
        mov       edi, DWORD PTR [44+esp]                       ;
                                ; LOE eax edx ecx ebx esi edi xmm0
.B1.95:                         ; Preds .B1.95 .B1.94
        movdqa    XMMWORD PTR [edi+ebx*4], xmm0                 ;84.9
        add       ebx, 4                                        ;84.9
        cmp       ebx, ecx                                      ;84.9
        jb        .B1.95        ; Prob 82%                      ;84.9
                                ; LOE eax edx ecx ebx esi edi xmm0
.B1.97:                         ; Preds .B1.95 .B1.154
        cmp       ecx, edx                                      ;84.9
        jae       .B1.100       ; Prob 10%                      ;84.9
                                ; LOE eax edx ecx esi xmm0
.B1.98:                         ; Preds .B1.97
        mov       ebx, DWORD PTR [36+esp]                       ;
                                ; LOE eax edx ecx ebx esi xmm0
.B1.99:                         ; Preds .B1.99 .B1.98
        mov       DWORD PTR [eax+ecx*4], ebx                    ;84.9
        inc       ecx                                           ;84.9
        cmp       ecx, edx                                      ;84.9
        jb        .B1.99        ; Prob 82%                      ;84.9
                                ; LOE eax edx ecx ebx esi xmm0
.B1.100:                        ; Preds .B1.97 .B1.99
        mov       ebx, DWORD PTR [16+esp]                       ;84.9
        inc       ebx                                           ;84.9
        mov       ecx, DWORD PTR [20+esp]                       ;84.9
        add       eax, ecx                                      ;84.9
        add       esi, ecx                                      ;84.9
        mov       DWORD PTR [16+esp], ebx                       ;84.9
        cmp       ebx, DWORD PTR [24+esp]                       ;84.9
        jb        .B1.85        ; Prob 82%                      ;84.9
        jmp       .B1.33        ; Prob 100%                     ;84.9
                                ; LOE eax edx esi xmm0
.B1.101:                        ; Preds .B1.140 .B1.138 .B1.36 .B1.131 .B1.153
                                ;       .B1.30
        push      OFFSET FLAT: __NLITPACK_1.0.1                 ;100.13
        call      _RANXY                                        ;100.13
                                ; LOE f1
.B1.202:                        ; Preds .B1.101
        fstp      DWORD PTR [12+esp]                            ;100.13
        movss     xmm0, DWORD PTR [12+esp]                      ;100.13
        add       esp, 4                                        ;100.13
                                ; LOE xmm0
.B1.102:                        ; Preds .B1.202
        mov       ecx, DWORD PTR [20+ebp]                       ;102.8
        mov       esi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;102.8
        mov       edi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;102.8
        mov       edx, DWORD PTR [ecx]                          ;102.8
        mov       DWORD PTR [28+esp], edx                       ;102.8
        shl       edx, 5                                        ;102.8
        shl       edi, 5                                        ;102.8
        add       edx, esi                                      ;102.8
        sub       edx, edi                                      ;102.8
        mov       DWORD PTR [20+esp], edi                       ;102.8
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NUMACTZONE+44] ;102.20
        neg       edi                                           ;102.8
        mov       ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NUMACTZONE+32] ;102.8
        mov       DWORD PTR [32+esp], edx                       ;102.8
        neg       ebx                                           ;102.8
        movsx     ecx, BYTE PTR [5+edx]                         ;102.8
        add       ebx, ecx                                      ;102.8
        movsx     edx, WORD PTR [22+edx]                        ;102.8
        add       edi, edx                                      ;102.8
        imul      edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NUMACTZONE+40] ;102.8
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NUMACTZONE] ;102.8
        mov       DWORD PTR [24+esp], esi                       ;102.8
        lea       esi, DWORD PTR [eax+ebx*4]                    ;102.8
        mov       eax, 1                                        ;
        mov       ebx, DWORD PTR [esi+edi]                      ;102.8
        test      ebx, ebx                                      ;102.8
        mov       DWORD PTR [36+esp], ebx                       ;102.8
        jle       .B1.107       ; Prob 2%                       ;102.8
                                ; LOE eax edx ecx xmm0
.B1.103:                        ; Preds .B1.102
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DSTMP2+56] ;103.17
        mov       ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DSTMP2+52] ;103.17
        sub       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DSTMP2+44] ;
        imul      esi, ebx                                      ;
        imul      edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DSTMP2+40] ;
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DSTMP2] ;103.9
        sub       edi, esi                                      ;
        add       edx, edi                                      ;
        sub       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DSTMP2+32] ;
        mov       DWORD PTR [16+esp], ebx                       ;103.17
        mov       DWORD PTR [4+esp], ebx                        ;
        mov       ebx, DWORD PTR [16+esp]                       ;
        lea       edx, DWORD PTR [edx+ecx*8]                    ;
        mov       ecx, DWORD PTR [4+esp]                        ;
        mov       esi, DWORD PTR [36+esp]                       ;
                                ; LOE eax edx ecx ebx esi xmm0
.B1.104:                        ; Preds .B1.105 .B1.103
        movss     xmm1, DWORD PTR [ecx+edx]                     ;103.17
        comiss    xmm1, xmm0                                    ;103.15
        ja        .B1.107       ; Prob 20%                      ;103.15
                                ; LOE eax edx ecx ebx esi xmm0
.B1.105:                        ; Preds .B1.104
        inc       eax                                           ;104.8
        add       ecx, ebx                                      ;104.8
        cmp       eax, esi                                      ;104.8
        jle       .B1.104       ; Prob 82%                      ;104.8
                                ; LOE eax edx ecx ebx esi xmm0
.B1.107:                        ; Preds .B1.104 .B1.105 .B1.102
        mov       ecx, DWORD PTR [32+esp]                       ;106.8
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_CONZONETMP+32] ;107.8
        neg       edx                                           ;107.8
        mov       DWORD PTR [4+esp], eax                        ;
        add       edx, eax                                      ;107.8
        mov       WORD PTR [20+ecx], ax                         ;106.8
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_CONZONETMP+40] ;107.8
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_CONZONETMP+44] ;107.8
        imul      eax, esi                                      ;107.8
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_CONZONETMP] ;107.8
        sub       ebx, eax                                      ;107.8
        mov       eax, 1                                        ;111.8
        add       ebx, esi                                      ;107.8
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM+32] ;107.8
        neg       edi                                           ;107.8
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM] ;107.8
        add       edi, DWORD PTR [ebx+edx*4]                    ;107.8
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;111.8
        mov       ebx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE] ;113.68
        mov       edi, DWORD PTR [esi+edi*4]                    ;107.8
        mov       DWORD PTR [24+ecx], edi                       ;107.8
        mov       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;111.8
        neg       ecx                                           ;111.8
        mov       esi, DWORD PTR [28+esp]                       ;111.8
        add       ecx, esi                                      ;111.8
        sub       esi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE+32] ;113.8
        shl       ecx, 8                                        ;111.8
        shl       esi, 6                                        ;113.8
        mov       BYTE PTR [40+edx+ecx], al                     ;111.8
        mov       BYTE PTR [160+edx+ecx], al                    ;112.8
        mov       eax, DWORD PTR [48+ebx+esi]                   ;113.8
        add       eax, eax                                      ;113.8
        neg       eax                                           ;113.8
        mov       edx, DWORD PTR [16+ebx+esi]                   ;113.8
        mov       ecx, DWORD PTR [20+ebp]                       ;115.15
        mov       ebx, DWORD PTR [4+esp]                        ;113.8
        mov       WORD PTR [2+eax+edx], bx                      ;113.8
        mov       edi, DWORD PTR [ecx]                          ;115.15
        shl       edi, 5                                        ;115.15
        add       edi, DWORD PTR [24+esp]                       ;115.8
        sub       edi, DWORD PTR [20+esp]                       ;115.8
        mov       edx, DWORD PTR [24+ebp]                       ;115.8
        push      OFFSET FLAT: __NLITPACK_2.0.1                 ;117.13
        movsx     eax, WORD PTR [20+edi]                        ;115.15
        mov       DWORD PTR [edx], eax                          ;115.8
        call      _RANXY                                        ;117.13
                                ; LOE f1
.B1.203:                        ; Preds .B1.107
        fstp      DWORD PTR [12+esp]                            ;117.13
        movss     xmm0, DWORD PTR [12+esp]                      ;117.13
        add       esp, 4                                        ;117.13
                                ; LOE xmm0
.B1.108:                        ; Preds .B1.203
        mov       eax, DWORD PTR [20+ebp]                       ;118.13
        mov       ebx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;118.8
        neg       ebx                                           ;118.8
        mov       esi, DWORD PTR [eax]                          ;118.13
        add       ebx, esi                                      ;118.8
        shl       ebx, 5                                        ;118.8
        mov       eax, 1                                        ;
        mov       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;118.8
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VEHPROFILE+32] ;121.10
        neg       edi                                           ;
        movsx     edx, BYTE PTR [5+ecx+ebx]                     ;118.13
        add       edi, edx                                      ;
        shl       edi, 2                                        ;
        lea       edi, DWORD PTR [edi+edi*8]                    ;
        add       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VEHPROFILE] ;
                                ; LOE eax edx ecx ebx esi edi xmm0
.B1.109:                        ; Preds .B1.110 .B1.108
        movss     xmm1, DWORD PTR [12+edi+eax*4]                ;121.18
        comiss    xmm1, xmm0                                    ;121.16
        ja        .B1.112       ; Prob 20%                      ;121.16
                                ; LOE eax edx ecx ebx esi edi xmm0
.B1.110:                        ; Preds .B1.109
        inc       eax                                           ;122.8
        cmp       eax, 5                                        ;122.8
        jle       .B1.109       ; Prob 80%                      ;122.8
                                ; LOE eax edx ecx ebx esi edi xmm0
.B1.112:                        ; Preds .B1.109 .B1.110
        mov       BYTE PTR [4+ecx+ebx], al                      ;123.8
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_READVEH+44] ;125.26
        neg       ecx                                           ;125.8
        add       ecx, eax                                      ;125.8
        imul      ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_READVEH+40] ;125.8
        mov       ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_READVEH+32] ;125.8
        neg       ebx                                           ;125.8
        add       ebx, edx                                      ;125.8
        add       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_READVEH] ;125.8
        inc       DWORD PTR [ecx+ebx*4]                         ;125.8
        cmp       eax, 4                                        ;127.43
        je        .B1.114       ; Prob 16%                      ;127.43
                                ; LOE edx esi
.B1.113:                        ; Preds .B1.112
        cmp       edx, 4                                        ;127.82
        jl        .B1.115       ; Prob 50%                      ;127.82
                                ; LOE esi
.B1.114:                        ; Preds .B1.113 .B1.112
        shl       esi, 6                                        ;128.11
        mov       ebx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE] ;128.11
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE+32] ;128.11
        mov       edx, eax                                      ;128.11
        shl       edx, 6                                        ;128.11
        lea       ecx, DWORD PTR [ebx+esi]                      ;128.11
        sub       ecx, edx                                      ;128.11
        mov       BYTE PTR [56+ecx], 1                          ;128.11
        jmp       .B1.116       ; Prob 100%                     ;128.11
                                ; LOE eax ebx esi
.B1.115:                        ; Preds .B1.113
        mov       ebx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE] ;130.8
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE+32] ;130.8
        shl       esi, 6                                        ;
                                ; LOE eax ebx esi
.B1.116:                        ; Preds .B1.114 .B1.115
        shl       eax, 6                                        ;130.8
        add       esi, ebx                                      ;130.8
        neg       eax                                           ;130.8
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_RIBFA] ;130.8
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_COM_FRAC] ;132.8
        push      OFFSET FLAT: __NLITPACK_3.0.1                 ;134.13
        mov       DWORD PTR [60+eax+esi], edx                   ;130.8
        mov       DWORD PTR [52+eax+esi], ecx                   ;132.8
        call      _RANXY                                        ;134.13
                                ; LOE f1
.B1.204:                        ; Preds .B1.116
        fstp      st(0)                                         ;
        add       esp, 4                                        ;134.13
                                ; LOE
.B1.117:                        ; Preds .B1.204
        mov       eax, DWORD PTR [20+ebp]                       ;136.11
        mov       esi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;136.8
        neg       esi                                           ;136.42
        mov       edx, DWORD PTR [eax]                          ;136.11
        add       esi, edx                                      ;136.42
        shl       esi, 5                                        ;136.42
        mov       ebx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;136.8
        cmp       BYTE PTR [5+ebx+esi], 2                       ;136.42
        jne       .B1.119       ; Prob 84%                      ;136.42
                                ; LOE edx ebx esi
.B1.118:                        ; Preds .B1.117
        movss     xmm3, DWORD PTR [_2il0floatpacket.6]          ;137.32
        movss     xmm0, DWORD PTR [_2il0floatpacket.7]          ;137.32
        andps     xmm0, xmm3                                    ;137.32
        pxor      xmm3, xmm0                                    ;137.32
        movss     xmm2, DWORD PTR [_2il0floatpacket.8]          ;137.32
        movaps    xmm1, xmm3                                    ;137.32
        movss     xmm4, DWORD PTR [_2il0floatpacket.9]          ;137.32
        cmpltss   xmm1, xmm2                                    ;137.32
        andps     xmm2, xmm1                                    ;137.32
        movaps    xmm1, xmm3                                    ;137.32
        movaps    xmm6, xmm4                                    ;137.32
        addss     xmm1, xmm2                                    ;137.32
        addss     xmm6, xmm4                                    ;137.32
        subss     xmm1, xmm2                                    ;137.32
        movaps    xmm7, xmm1                                    ;137.32
        mov       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;137.32
        subss     xmm7, xmm3                                    ;137.32
        movaps    xmm5, xmm7                                    ;137.32
        neg       ecx                                           ;137.10
        add       ecx, edx                                      ;137.10
        cmpnless  xmm5, xmm4                                    ;137.32
        cmpless   xmm7, DWORD PTR [_2il0floatpacket.10]         ;137.32
        andps     xmm5, xmm6                                    ;137.32
        andps     xmm7, xmm6                                    ;137.32
        shl       ecx, 8                                        ;137.10
        subss     xmm1, xmm5                                    ;137.32
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;137.32
        addss     xmm1, xmm7                                    ;137.32
        orps      xmm1, xmm0                                    ;137.32
        cvtss2si  eax, xmm1                                     ;137.32
        cvtsi2ss  xmm0, eax                                     ;137.10
        movss     DWORD PTR [156+edx+ecx], xmm0                 ;137.10
                                ; LOE ebx esi
.B1.119:                        ; Preds .B1.117 .B1.118
        movzx     eax, BYTE PTR [4+ebx+esi]                     ;141.8
        inc       DWORD PTR [_DYNUST_MAIN_MODULE_mp_MTC_VEH-4+eax*4] ;141.8
        test      BYTE PTR [_DYNUST_MAIN_MODULE_mp_CHECKFLAG], 1 ;143.15
        jne       .B1.121       ; Prob 40%                      ;143.15
                                ; LOE
.B1.120:                        ; Preds .B1.119
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_CHECKFLAG], -1 ;143.26
                                ; LOE
.B1.121:                        ; Preds .B1.119 .B1.120
        test      BYTE PTR [_DYNUST_MAIN_MODULE_mp_CHECKFLAGINIT], 1 ;144.15
        jne       .B1.123       ; Prob 40%                      ;144.15
                                ; LOE
.B1.122:                        ; Preds .B1.121
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_CHECKFLAGINIT], -1 ;144.30
                                ; LOE
.B1.123:                        ; Preds .B1.121 .B1.122
        add       esp, 196                                      ;151.1
        pop       ebx                                           ;151.1
        pop       edi                                           ;151.1
        pop       esi                                           ;151.1
        mov       esp, ebp                                      ;151.1
        pop       ebp                                           ;151.1
        ret                                                     ;151.1
                                ; LOE
.B1.124:                        ; Preds .B1.6 .B1.10 .B1.8      ; Infreq
        xor       ebx, ebx                                      ;47.7
        jmp       .B1.18        ; Prob 100%                     ;47.7
                                ; LOE eax edx ebx esi xmm0
.B1.130:                        ; Preds .B1.83                  ; Infreq
        mov       ecx, DWORD PTR [80+esp]                       ;
        mov       edx, DWORD PTR [76+esp]                       ;
        mov       eax, edi                                      ;
        mov       ebx, DWORD PTR [72+esp]                       ;
                                ; LOE eax edx ecx ebx
.B1.131:                        ; Preds .B1.130 .B1.144 .B1.208 ; Infreq
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DSTMP2+24] ;95.9
        test      esi, esi                                      ;95.9
        mov       DWORD PTR [40+esp], esi                       ;95.9
        jle       .B1.101       ; Prob 50%                      ;95.9
                                ; LOE eax edx ecx ebx esi
.B1.132:                        ; Preds .B1.131                 ; Infreq
        mov       edi, esi                                      ;
        shr       edi, 31                                       ;
        add       edi, esi                                      ;
        mov       esi, DWORD PTR [36+esp]                       ;
        imul      ebx, DWORD PTR [152+esp]                      ;
        imul      esi, edx                                      ;
        imul      edx, DWORD PTR [28+esp]                       ;
        mov       DWORD PTR [32+esp], eax                       ;
        shl       eax, 3                                        ;
        neg       eax                                           ;
        add       ecx, esi                                      ;
        add       eax, ebx                                      ;
        add       ebx, edx                                      ;
        sar       edi, 1                                        ;
        mov       DWORD PTR [4+esp], edi                        ;
        mov       edi, ecx                                      ;
        sub       ecx, ebx                                      ;
        sub       edi, edx                                      ;
        add       eax, ecx                                      ;
        mov       edx, DWORD PTR [32+esp]                       ;
        mov       DWORD PTR [20+esp], 0                         ;
        mov       DWORD PTR [16+esp], edi                       ;
        mov       edi, DWORD PTR [20+esp]                       ;
        lea       eax, DWORD PTR [eax+edx*8]                    ;
        mov       ecx, DWORD PTR [4+esp]                        ;
        mov       DWORD PTR [24+esp], eax                       ;
        mov       eax, edi                                      ;
                                ; LOE eax ecx edi
.B1.133:                        ; Preds .B1.138 .B1.140 .B1.132 ; Infreq
        test      ecx, ecx                                      ;95.9
        jbe       .B1.142       ; Prob 11%                      ;95.9
                                ; LOE eax ecx edi
.B1.134:                        ; Preds .B1.133                 ; Infreq
        mov       esi, DWORD PTR [24+esp]                       ;
        xor       ebx, ebx                                      ;
        mov       edx, DWORD PTR [16+esp]                       ;
        mov       DWORD PTR [20+esp], eax                       ;
        add       esi, edi                                      ;
        mov       DWORD PTR [4+esp], edi                        ;
        add       edx, edi                                      ;
        mov       eax, ebx                                      ;
        mov       edi, 1065353216                               ;
                                ; LOE eax edx ecx ebx esi edi
.B1.135:                        ; Preds .B1.135 .B1.134         ; Infreq
        inc       ebx                                           ;95.9
        mov       DWORD PTR [eax+edx], edi                      ;95.9
        mov       DWORD PTR [8+eax+esi], edi                    ;95.9
        add       eax, 16                                       ;95.9
        cmp       ebx, ecx                                      ;95.9
        jb        .B1.135       ; Prob 64%                      ;95.9
                                ; LOE eax edx ecx ebx esi edi
.B1.136:                        ; Preds .B1.135                 ; Infreq
        mov       edi, DWORD PTR [4+esp]                        ;
        lea       edx, DWORD PTR [1+ebx+ebx]                    ;95.9
        mov       eax, DWORD PTR [20+esp]                       ;
                                ; LOE eax edx ecx edi
.B1.137:                        ; Preds .B1.136 .B1.142         ; Infreq
        lea       ebx, DWORD PTR [-1+edx]                       ;95.9
        cmp       ebx, DWORD PTR [40+esp]                       ;95.9
        jae       .B1.140       ; Prob 11%                      ;95.9
                                ; LOE eax edx ecx edi
.B1.138:                        ; Preds .B1.137                 ; Infreq
        mov       ebx, DWORD PTR [24+esp]                       ;95.9
        inc       eax                                           ;95.9
        lea       edx, DWORD PTR [ebx+edx*8]                    ;95.9
        mov       DWORD PTR [-8+edx+edi], 1065353216            ;95.9
        add       edi, DWORD PTR [152+esp]                      ;95.9
        cmp       eax, DWORD PTR [140+esp]                      ;95.9
        jb        .B1.133       ; Prob 82%                      ;95.9
        jmp       .B1.101       ; Prob 100%                     ;95.9
                                ; LOE eax ecx edi
.B1.140:                        ; Preds .B1.137                 ; Infreq
        inc       eax                                           ;95.9
        add       edi, DWORD PTR [152+esp]                      ;95.9
        cmp       eax, DWORD PTR [140+esp]                      ;95.9
        jb        .B1.133       ; Prob 82%                      ;95.9
        jmp       .B1.101       ; Prob 100%                     ;95.9
                                ; LOE eax ecx edi
.B1.142:                        ; Preds .B1.133                 ; Infreq
        mov       edx, 1                                        ;
        jmp       .B1.137       ; Prob 100%                     ;
                                ; LOE eax edx ecx edi
.B1.143:                        ; Preds .B1.82                  ; Infreq
        mov       eax, DWORD PTR [148+esp]                      ;91.12
        mov       DWORD PTR [144+esp], eax                      ;91.12
        cmp       eax, DWORD PTR [36+esp]                       ;91.12
        jb        .B1.39        ; Prob 82%                      ;91.12
                                ; LOE
.B1.144:                        ; Preds .B1.143                 ; Infreq
        mov       ecx, DWORD PTR [80+esp]                       ;
        mov       edx, DWORD PTR [76+esp]                       ;
        mov       eax, DWORD PTR [32+esp]                       ;
        mov       ebx, DWORD PTR [72+esp]                       ;
        jmp       .B1.131       ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx
.B1.145:                        ; Preds .B1.78                  ; Infreq
        mov       edx, 1                                        ;
        jmp       .B1.82        ; Prob 100%                     ;
                                ; LOE edx
.B1.146:                        ; Preds .B1.72                  ; Infreq
        mov       eax, 1                                        ;
        jmp       .B1.76        ; Prob 100%                     ;
                                ; LOE eax
.B1.147:                        ; Preds .B1.67                  ; Infreq
        mov       eax, 1                                        ;
        mov       esi, eax                                      ;
        cmp       DWORD PTR [140+esp], 0                        ;91.12
        ja        .B1.71        ; Prob 90%                      ;91.12
        jmp       .B1.72        ; Prob 100%                     ;91.12
                                ; LOE esi
.B1.148:                        ; Preds .B1.62                  ; Infreq
        mov       edx, 1                                        ;
        cmp       DWORD PTR [140+esp], 0                        ;90.12
        ja        .B1.66        ; Prob 90%                      ;90.12
        jmp       .B1.67        ; Prob 100%                     ;90.12
                                ; LOE edx
.B1.149:                        ; Preds .B1.56                  ; Infreq
        mov       eax, 1                                        ;
        jmp       .B1.60        ; Prob 100%                     ;
                                ; LOE eax
.B1.150:                        ; Preds .B1.51                  ; Infreq
        mov       eax, 1                                        ;
        mov       esi, eax                                      ;
        cmp       DWORD PTR [140+esp], 0                        ;88.12
        ja        .B1.55        ; Prob 90%                      ;88.12
        jmp       .B1.56        ; Prob 100%                     ;88.12
                                ; LOE esi
.B1.151:                        ; Preds .B1.46                  ; Infreq
        mov       edx, 1                                        ;
        cmp       DWORD PTR [140+esp], 0                        ;87.12
        ja        .B1.50        ; Prob 90%                      ;87.12
        jmp       .B1.51        ; Prob 100%                     ;87.12
                                ; LOE edx
.B1.152:                        ; Preds .B1.39                  ; Infreq
        mov       eax, 1                                        ;
        mov       esi, eax                                      ;
        jmp       .B1.43        ; Prob 100%                     ;
                                ; LOE esi
.B1.153:                        ; Preds .B1.33                  ; Infreq
        cmp       DWORD PTR [140+esp], 0                        ;95.9
        jle       .B1.101       ; Prob 10%                      ;95.9
                                ; LOE ebx
.B1.208:                        ; Preds .B1.153                 ; Infreq
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DSTMP2+56] ;95.9
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DSTMP2+40] ;95.9
        mov       DWORD PTR [28+esp], edx                       ;95.9
        mov       DWORD PTR [152+esp], eax                      ;95.9
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DSTMP2] ;95.9
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DSTMP2+52] ;95.9
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DSTMP2+32] ;95.9
        jmp       .B1.131       ; Prob 100%                     ;95.9
                                ; LOE eax edx ecx ebx
.B1.154:                        ; Preds .B1.85 .B1.89 .B1.87    ; Infreq
        xor       ecx, ecx                                      ;84.9
        jmp       .B1.97        ; Prob 100%                     ;84.9
                                ; LOE eax edx ecx esi xmm0
.B1.162:                        ; Preds .B1.23                  ; Infreq
        movsx     eax, WORD PTR [148+edx+ecx]                   ;54.120
        cmp       eax, 5                                        ;54.162
        je        .B1.24        ; Prob 16%                      ;54.162
                                ; LOE edx ecx xmm5
.B1.163:                        ; Preds .B1.162                 ; Infreq
        movss     xmm5, DWORD PTR [20+edx+ecx]                  ;55.38
        movss     xmm2, DWORD PTR [_2il0floatpacket.5]          ;55.71
        mulss     xmm5, xmm2                                    ;55.33
        movss     xmm1, DWORD PTR [_2il0floatpacket.7]          ;55.33
        andps     xmm1, xmm5                                    ;55.33
        pxor      xmm5, xmm1                                    ;55.33
        movss     xmm4, DWORD PTR [_2il0floatpacket.8]          ;55.33
        movaps    xmm3, xmm5                                    ;55.33
        movaps    xmm0, xmm5                                    ;55.33
        cmpltss   xmm3, xmm4                                    ;55.33
        andps     xmm4, xmm3                                    ;55.33
        movss     xmm6, DWORD PTR [_2il0floatpacket.9]          ;55.33
        addss     xmm0, xmm4                                    ;55.33
        mov       ebx, DWORD PTR [20+ebp]                       ;55.7
        subss     xmm0, xmm4                                    ;55.33
        movaps    xmm3, xmm0                                    ;55.33
        mov       ebx, DWORD PTR [ebx]                          ;55.7
        mov       edx, ebx                                      ;55.7
        mov       esi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;55.79
        sub       edx, esi                                      ;55.7
        shl       edx, 8                                        ;55.7
        subss     xmm3, xmm5                                    ;55.33
        movaps    xmm5, xmm6                                    ;55.33
        movaps    xmm7, xmm3                                    ;55.33
        mov       edi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;55.79
        addss     xmm5, xmm6                                    ;55.33
        cmpnless  xmm7, xmm6                                    ;55.33
        cmpless   xmm3, DWORD PTR [_2il0floatpacket.10]         ;55.33
        andps     xmm7, xmm5                                    ;55.33
        andps     xmm3, xmm5                                    ;55.33
        subss     xmm0, xmm7                                    ;55.33
        addss     xmm0, xmm3                                    ;55.33
        orps      xmm0, xmm1                                    ;55.33
        cvtss2si  eax, xmm0                                     ;55.33
        cvtsi2ss  xmm0, eax                                     ;55.33
        divss     xmm0, xmm2                                    ;55.7
        movss     DWORD PTR [240+edi+edx], xmm0                 ;55.7
        jmp       .B1.25        ; Prob 100%                     ;55.7
                                ; LOE ebx esi edi
.B1.164:                        ; Preds .B1.1                   ; Infreq
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NZONES] ;39.7
        xor       edx, edx                                      ;39.7
        mov       eax, 1                                        ;39.7
        test      esi, esi                                      ;39.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_DSTMP2+32], eax ;39.7
        cmovle    esi, edx                                      ;39.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_DSTMP2+44], eax ;39.7
        mov       edi, 3                                        ;39.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_DSTMP2+56], eax ;39.7
        mov       eax, 24                                       ;39.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_DSTMP2+8], edx ;39.7
        lea       edx, DWORD PTR [esp]                          ;39.7
        push      eax                                           ;39.7
        push      esi                                           ;39.7
        push      esi                                           ;39.7
        push      edi                                           ;39.7
        push      edx                                           ;39.7
        mov       ecx, 8                                        ;39.7
        lea       ebx, DWORD PTR [esi+esi*2]                    ;39.7
        shl       ebx, 3                                        ;39.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_DSTMP2+12], 133 ;39.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_DSTMP2+4], ecx ;39.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_DSTMP2+16], edi ;39.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_DSTMP2+24], edi ;39.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_DSTMP2+36], esi ;39.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_DSTMP2+48], esi ;39.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_DSTMP2+28], ecx ;39.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_DSTMP2+40], eax ;39.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_DSTMP2+52], ebx ;39.7
        call      _for_check_mult_overflow                      ;39.7
                                ; LOE eax
.B1.165:                        ; Preds .B1.164                 ; Infreq
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DSTMP2+12] ;39.7
        and       eax, 1                                        ;39.7
        and       edx, 1                                        ;39.7
        shl       eax, 4                                        ;39.7
        add       edx, edx                                      ;39.7
        or        edx, eax                                      ;39.7
        or        edx, 262144                                   ;39.7
        push      edx                                           ;39.7
        push      OFFSET FLAT: _DYNUST_MAIN_MODULE_mp_DSTMP2    ;39.7
        push      DWORD PTR [28+esp]                            ;39.7
        call      _for_alloc_allocatable                        ;39.7
                                ; LOE
.B1.206:                        ; Preds .B1.165                 ; Infreq
        add       esp, 32                                       ;39.7
                                ; LOE
.B1.166:                        ; Preds .B1.206                 ; Infreq
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DSTMP2+48] ;40.7
        test      eax, eax                                      ;40.7
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DSTMP2+56] ;40.7
        mov       DWORD PTR [12+esp], eax                       ;40.7
        jle       .B1.2         ; Prob 10%                      ;40.7
                                ; LOE edx
.B1.167:                        ; Preds .B1.166                 ; Infreq
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DSTMP2+52] ;41.7
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DSTMP2+40] ;41.7
        mov       ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DSTMP2+44] ;
        imul      edx, edi                                      ;
        imul      ebx, eax                                      ;
        mov       DWORD PTR [4+esp], edi                        ;41.7
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DSTMP2+32] ;
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DSTMP2] ;41.7
        mov       DWORD PTR [8+esp], esi                        ;41.7
        sub       esi, edx                                      ;
        mov       DWORD PTR [32+esp], eax                       ;41.7
        lea       eax, DWORD PTR [edi*8]                        ;
        sub       edx, ebx                                      ;
        sub       ebx, eax                                      ;
        add       esi, edx                                      ;
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DSTMP2+24] ;
        add       esi, ebx                                      ;
        mov       edx, eax                                      ;
        shr       edx, 31                                       ;
        add       edx, eax                                      ;
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DSTMP2+36] ;40.7
        lea       esi, DWORD PTR [esi+edi*8]                    ;
        sar       edx, 1                                        ;
        mov       DWORD PTR [36+esp], ecx                       ;40.7
        xor       ecx, ecx                                      ;
        mov       DWORD PTR [16+esp], ecx                       ;
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_DSTMP2+36], 0 ;40.7
        jle       .B1.172       ; Prob 10%                      ;40.7
                                ; LOE eax edx ecx esi cl ch
.B1.168:                        ; Preds .B1.167                 ; Infreq
        mov       DWORD PTR [28+esp], edx                       ;
                                ; LOE eax ecx esi
.B1.169:                        ; Preds .B1.170 .B1.168         ; Infreq
        test      eax, eax                                      ;40.7
        jg        .B1.184       ; Prob 50%                      ;40.7
                                ; LOE eax ecx esi
.B1.170:                        ; Preds .B1.197 .B1.191 .B1.169 ; Infreq
        mov       edx, DWORD PTR [16+esp]                       ;40.7
        inc       edx                                           ;40.7
        add       ecx, DWORD PTR [4+esp]                        ;40.7
        mov       DWORD PTR [16+esp], edx                       ;40.7
        cmp       edx, DWORD PTR [12+esp]                       ;40.7
        jb        .B1.169       ; Prob 81%                      ;40.7
                                ; LOE eax ecx esi
.B1.172:                        ; Preds .B1.170 .B1.167         ; Infreq
        mov       ebx, eax                                      ;
        xor       ecx, ecx                                      ;
        shr       ebx, 31                                       ;
        xor       edx, edx                                      ;
        add       ebx, eax                                      ;
        sar       ebx, 1                                        ;
        cmp       DWORD PTR [36+esp], 0                         ;41.7
        jle       .B1.2         ; Prob 10%                      ;41.7
                                ; LOE eax edx ecx ebx
.B1.173:                        ; Preds .B1.172                 ; Infreq
        mov       DWORD PTR [24+esp], ebx                       ;
                                ; LOE eax edx ecx
.B1.174:                        ; Preds .B1.175 .B1.173         ; Infreq
        test      eax, eax                                      ;41.7
        jg        .B1.177       ; Prob 50%                      ;41.7
                                ; LOE eax edx ecx
.B1.175:                        ; Preds .B1.193 .B1.174         ; Infreq
        inc       ecx                                           ;41.7
        add       edx, DWORD PTR [4+esp]                        ;41.7
        cmp       ecx, DWORD PTR [12+esp]                       ;41.7
        jb        .B1.174       ; Prob 81%                      ;41.7
        jmp       .B1.2         ; Prob 100%                     ;41.7
                                ; LOE eax edx ecx
.B1.177:                        ; Preds .B1.174                 ; Infreq
        mov       ebx, DWORD PTR [8+esp]                        ;
        xor       esi, esi                                      ;
        mov       DWORD PTR [28+esp], esi                       ;
        mov       DWORD PTR [20+esp], edx                       ;
        mov       DWORD PTR [16+esp], ecx                       ;
        lea       edi, DWORD PTR [ebx+edx]                      ;
        mov       DWORD PTR [44+esp], edi                       ;
        mov       ebx, edi                                      ;
        mov       DWORD PTR [56+esp], eax                       ;
        mov       edx, esi                                      ;
        mov       ecx, DWORD PTR [24+esp]                       ;
                                ; LOE edx ecx ebx esi
.B1.178:                        ; Preds .B1.183 .B1.192 .B1.177 ; Infreq
        test      ecx, ecx                                      ;41.7
        jbe       .B1.194       ; Prob 10%                      ;41.7
                                ; LOE edx ecx ebx esi
.B1.179:                        ; Preds .B1.178                 ; Infreq
        xor       eax, eax                                      ;
        mov       DWORD PTR [28+esp], edx                       ;
        mov       DWORD PTR [40+esp], esi                       ;
        xor       esi, esi                                      ;
                                ; LOE eax ecx ebx esi
.B1.180:                        ; Preds .B1.180 .B1.179         ; Infreq
        mov       edx, eax                                      ;41.7
        inc       eax                                           ;41.7
        shl       edx, 4                                        ;41.7
        cmp       eax, ecx                                      ;41.7
        mov       WORD PTR [4+edx+ebx], si                      ;41.7
        mov       WORD PTR [12+edx+ebx], si                     ;41.7
        jb        .B1.180       ; Prob 64%                      ;41.7
                                ; LOE eax ecx ebx esi
.B1.181:                        ; Preds .B1.180                 ; Infreq
        mov       edx, DWORD PTR [28+esp]                       ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;41.7
        mov       esi, DWORD PTR [40+esp]                       ;
                                ; LOE eax edx ecx ebx esi
.B1.182:                        ; Preds .B1.181 .B1.194         ; Infreq
        lea       edi, DWORD PTR [-1+eax]                       ;41.7
        cmp       edi, DWORD PTR [56+esp]                       ;41.7
        jae       .B1.192       ; Prob 10%                      ;41.7
                                ; LOE eax edx ecx ebx esi
.B1.183:                        ; Preds .B1.182                 ; Infreq
        mov       edi, DWORD PTR [44+esp]                       ;41.7
        inc       esi                                           ;41.7
        lea       edi, DWORD PTR [edi+eax*8]                    ;41.7
        xor       eax, eax                                      ;41.7
        mov       WORD PTR [-4+edi+edx], ax                     ;41.7
        mov       eax, DWORD PTR [32+esp]                       ;41.7
        add       ebx, eax                                      ;41.7
        add       edx, eax                                      ;41.7
        cmp       esi, DWORD PTR [36+esp]                       ;41.7
        jb        .B1.178       ; Prob 82%                      ;41.7
        jmp       .B1.193       ; Prob 100%                     ;41.7
                                ; LOE edx ecx ebx esi
.B1.184:                        ; Preds .B1.169                 ; Infreq
        mov       edi, DWORD PTR [8+esp]                        ;
        xor       edx, edx                                      ;
        mov       DWORD PTR [40+esp], edx                       ;
        lea       ebx, DWORD PTR [esi+ecx]                      ;
        mov       DWORD PTR [48+esp], ebx                       ;
        mov       DWORD PTR [56+esp], eax                       ;
        add       edi, ecx                                      ;
        mov       DWORD PTR [52+esp], edi                       ;
        mov       DWORD PTR [24+esp], ecx                       ;
        mov       DWORD PTR [20+esp], esi                       ;
        mov       eax, edx                                      ;
        mov       ebx, DWORD PTR [28+esp]                       ;
                                ; LOE eax edx ebx
.B1.185:                        ; Preds .B1.190 .B1.196 .B1.184 ; Infreq
        test      ebx, ebx                                      ;40.7
        jbe       .B1.198       ; Prob 10%                      ;40.7
                                ; LOE eax edx ebx
.B1.186:                        ; Preds .B1.185                 ; Infreq
        mov       edi, DWORD PTR [52+esp]                       ;
        xor       esi, esi                                      ;
        mov       ecx, DWORD PTR [48+esp]                       ;
        mov       DWORD PTR [40+esp], eax                       ;
        add       edi, eax                                      ;
        mov       DWORD PTR [44+esp], edx                       ;
        add       ecx, eax                                      ;
        mov       eax, esi                                      ;
        xor       edx, edx                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B1.187:                        ; Preds .B1.187 .B1.186         ; Infreq
        inc       esi                                           ;40.7
        mov       DWORD PTR [eax+ecx], edx                      ;40.7
        mov       DWORD PTR [8+eax+edi], edx                    ;40.7
        add       eax, 16                                       ;40.7
        cmp       esi, ebx                                      ;40.7
        jb        .B1.187       ; Prob 64%                      ;40.7
                                ; LOE eax edx ecx ebx esi edi
.B1.188:                        ; Preds .B1.187                 ; Infreq
        mov       eax, DWORD PTR [40+esp]                       ;
        lea       ecx, DWORD PTR [1+esi+esi]                    ;40.7
        mov       edx, DWORD PTR [44+esp]                       ;
                                ; LOE eax edx ecx ebx
.B1.189:                        ; Preds .B1.188 .B1.198         ; Infreq
        lea       esi, DWORD PTR [-1+ecx]                       ;40.7
        cmp       esi, DWORD PTR [56+esp]                       ;40.7
        jae       .B1.196       ; Prob 10%                      ;40.7
                                ; LOE eax edx ecx ebx
.B1.190:                        ; Preds .B1.189                 ; Infreq
        mov       esi, DWORD PTR [52+esp]                       ;40.7
        inc       edx                                           ;40.7
        lea       ecx, DWORD PTR [esi+ecx*8]                    ;40.7
        mov       DWORD PTR [-8+ecx+eax], 0                     ;40.7
        add       eax, DWORD PTR [32+esp]                       ;40.7
        cmp       edx, DWORD PTR [36+esp]                       ;40.7
        jb        .B1.185       ; Prob 82%                      ;40.7
                                ; LOE eax edx ebx
.B1.191:                        ; Preds .B1.190                 ; Infreq
        mov       DWORD PTR [28+esp], ebx                       ;
        mov       ecx, DWORD PTR [24+esp]                       ;
        mov       esi, DWORD PTR [20+esp]                       ;
        mov       eax, DWORD PTR [56+esp]                       ;
        jmp       .B1.170       ; Prob 100%                     ;
                                ; LOE eax ecx esi
.B1.192:                        ; Preds .B1.182                 ; Infreq
        inc       esi                                           ;41.7
        mov       eax, DWORD PTR [32+esp]                       ;41.7
        add       ebx, eax                                      ;41.7
        add       edx, eax                                      ;41.7
        cmp       esi, DWORD PTR [36+esp]                       ;41.7
        jb        .B1.178       ; Prob 82%                      ;41.7
                                ; LOE edx ecx ebx esi
.B1.193:                        ; Preds .B1.183 .B1.192         ; Infreq
        mov       DWORD PTR [24+esp], ecx                       ;
        mov       edx, DWORD PTR [20+esp]                       ;
        mov       ecx, DWORD PTR [16+esp]                       ;
        mov       eax, DWORD PTR [56+esp]                       ;
        jmp       .B1.175       ; Prob 100%                     ;
                                ; LOE eax edx ecx
.B1.194:                        ; Preds .B1.178                 ; Infreq
        mov       eax, 1                                        ;
        jmp       .B1.182       ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B1.196:                        ; Preds .B1.189                 ; Infreq
        inc       edx                                           ;40.7
        add       eax, DWORD PTR [32+esp]                       ;40.7
        cmp       edx, DWORD PTR [36+esp]                       ;40.7
        jb        .B1.185       ; Prob 82%                      ;40.7
                                ; LOE eax edx ebx
.B1.197:                        ; Preds .B1.196                 ; Infreq
        mov       DWORD PTR [28+esp], ebx                       ;
        mov       ecx, DWORD PTR [24+esp]                       ;
        mov       esi, DWORD PTR [20+esp]                       ;
        mov       eax, DWORD PTR [56+esp]                       ;
        jmp       .B1.170       ; Prob 100%                     ;
                                ; LOE eax ecx esi
.B1.198:                        ; Preds .B1.185                 ; Infreq
        mov       ecx, 1                                        ;
        jmp       .B1.189       ; Prob 100%                     ;
        ALIGN     16
                                ; LOE eax edx ecx ebx
; mark_end;
_GENERATE_VEH_ATTRIBUTES ENDP
_TEXT	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
__NLITPACK_0.0.1	DD	19
__NLITPACK_1.0.1	DD	20
__NLITPACK_2.0.1	DD	23
__NLITPACK_3.0.1	DD	24
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _GENERATE_VEH_ATTRIBUTES
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
_2il0floatpacket.5	DD	047c35000H
_2il0floatpacket.6	DD	043160000H
_2il0floatpacket.7	DD	080000000H
_2il0floatpacket.8	DD	04b000000H
_2il0floatpacket.9	DD	03f000000H
_2il0floatpacket.10	DD	0bf000000H
_2il0floatpacket.11	DD	03f800000H
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
EXTRN	_DYNUST_MAIN_MODULE_mp_MTC_VEH:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_COM_FRAC:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_RIBFA:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_READVEH:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_VEHPROFILE:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_CONZONETMP:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_DEMORIGACUH:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_DEMORIGACUT:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_DEMORIGACU:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_MISFLAG:BYTE
EXTRN	_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NUMNTAG:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ENDTM:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_STARTTM:BYTE
EXTRN	_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE:BYTE
EXTRN	_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_BACKPOINTR:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NUMACTZONE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_CHECKFLAG:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NZONES:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_DSTMP2:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_CHECKFLAGINIT:BYTE
_DATA	ENDS
EXTRN	_for_alloc_allocatable:PROC
EXTRN	_for_check_mult_overflow:PROC
EXTRN	_RANXY:PROC
EXTRN	__fltused:BYTE
	INCLUDELIB <ifconsol>
	INCLUDELIB <libifcoremt>
	INCLUDELIB <libifport>
	INCLUDELIB <libmmt>
	INCLUDELIB <LIBCMT>
	INCLUDELIB <libirc>
	INCLUDELIB <svml_dispmt>
	INCLUDELIB <OLDNAMES>
	END
