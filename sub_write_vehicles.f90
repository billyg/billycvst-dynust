    SUBROUTINE write_vehicles(maxveh,isj,kj,fu1,isee,fu2)
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~ Copyright (C) 2002-2011 The Arizona Board of Regents on Behalf of The University of Arizona.  ~~!
!~~ Developed by Yi-Chang Chiu, Ph.D. as par to the DynusT project.                               ~~!
!~~ This program is free software: you can redistribute it and/or modify it                       ~~!
!~~ under the terms of the GNU General Public License version 2 as published by                   ~~!
!~~ the Free Software Foundation.                                                                 ~~!
!~~ This program is distributed in the hope that it will be useful,                               ~~!
!~~ but WITHOUT ANY WARRANTY; without even the implied warranty of                                ~~!
!~~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                                          ~~!
!~~ See the GNU General Public License for more details.                                          ~~!
!~~ You should have received a copy of the GNU General Public License                             ~~!
!~~ along with this program (in the file COPYING).                                                ~~!
!~~ IF not, see <http://www.gnu.org/licenses/>.                                                   ~~!
!~~ Please see http://www.dynust.net for contact information and names                            ~~!
!~~ of individual contributors.                                                                   ~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
    USE DYNUST_MAIN_MODULE
    USE DYNUST_VEH_MODULE
    USE DYNUST_NETWORK_MODULE
	USE DYNUST_VEH_PATH_ATT_MODULE
	USE DYNUST_FUEL_MODULE
 
    INTEGER, intent(in)::kj,isj
    INTEGER maxveh,fu1,fu2,isee
!> > > > > > > > > >                < < < < < < < < < < <
!>>>>>>>>>>>>>>>>>>>CODES START HERE<<<<<<<<<<<<<<<<<<<<<
!> > > > > > > > > >                < < < < < < < < < < <

!    IF(isj == 1) THEN
!     WRITE(fu1,*) maxveh, noofSTOPs ,"   # of vehicles in the file, Max # of STOPs"
!!	 WRITE(fu1,*) '    #  usec   dsec  stime vehcls vehtype ioc #ONode #IntDe info ribf comp izone'
!     WRITE(fu1,'("        #   usec   dsec   stime vehcls vehtype ioc #ONode #IntDe info ribf    comp   izone Evac  InitPos   IniGas")') 	
!!                       1      1      2    0.00     3     1     1    21     1     0  0.0000  0.0000    1    0    0.002    
!    ENDIF
	  m = 1     
        IF(FuelOut > 0.and.(iteration == iteMax.or.reach_converg == .true.)) THEN
!         WRITE(fu1,301) kj,m_dynust_network_node_nde(m_dynust_network_arc_nde(m_dynust_last_stand(kj)%isec)%iunod)%IntoOutNodeNum,m_dynust_network_node_nde(m_dynust_network_arc_nde(m_dynust_last_stand(kj)%isec)%idnod)%IntoOutNodeNum,m_dynust_last_stand(kj)%stime,m_dynust_last_stand(kj)%vehclass,m_dynust_last_stand(kj)%vehtype,m_dynust_veh(kj)%ioc,vehatt_P_Size(kj),m_dynust_veh(kj)%NoOfIntDst,m_dynust_veh_nde(kj)%info,m_dynust_veh_nde(kj)%ribf,m_dynust_veh_nde(kj)%compliance,m_dynust_last_stand(kj)%jorig,m_dynust_veh(kj)%EvacSta,m_dynust_veh_nde(kj)%xparinit,VehFuel(kj)%IniGas
          WRITE(fu1,301) m_dynust_veh_nde(kj)%ID,m_dynust_network_node_nde(m_dynust_network_arc_nde(m_dynust_last_stand(kj)%isec)%iunod)%IntoOutNodeNum,m_dynust_network_node_nde(m_dynust_network_arc_nde(m_dynust_last_stand(kj)%isec)%idnod)%IntoOutNodeNum,m_dynust_last_stand(kj)%stime,m_dynust_last_stand(kj)%vehclass,m_dynust_last_stand(kj)%vehtype,m_dynust_veh(kj)%ioc,vehatt_P_Size(kj),m_dynust_veh(kj)%NoOfIntDst,m_dynust_veh_nde(kj)%info,m_dynust_veh_nde(kj)%ribf,m_dynust_veh_nde(kj)%compliance,m_dynust_last_stand(kj)%jorig,m_dynust_veh(kj)%EvacSta,m_dynust_veh_nde(kj)%xparinit/m_dynust_network_arc_nde(m_dynust_last_stand(kj)%isec)%s,VehFuel(kj)%IniGas
        ELSE
!         WRITE(fu1,301) kj,m_dynust_network_node_nde(m_dynust_network_arc_nde(m_dynust_last_stand(kj)%isec)%iunod)%IntoOutNodeNum,m_dynust_network_node_nde(m_dynust_network_arc_nde(m_dynust_last_stand(kj)%isec)%idnod)%IntoOutNodeNum,m_dynust_last_stand(kj)%stime,m_dynust_last_stand(kj)%vehclass,m_dynust_last_stand(kj)%vehtype,m_dynust_veh(kj)%ioc,vehatt_P_Size(kj),m_dynust_veh(kj)%NoOfIntDst,m_dynust_veh_nde(kj)%info,m_dynust_veh_nde(kj)%ribf,m_dynust_veh_nde(kj)%compliance,m_dynust_last_stand(kj)%jorig,m_dynust_veh(kj)%EvacSta,m_dynust_veh_nde(kj)%xparinit
          WRITE(fu1,301) m_dynust_veh_nde(kj)%ID,m_dynust_network_node_nde(m_dynust_network_arc_nde(m_dynust_last_stand(kj)%isec)%iunod)%IntoOutNodeNum,m_dynust_network_node_nde(m_dynust_network_arc_nde(m_dynust_last_stand(kj)%isec)%idnod)%IntoOutNodeNum,m_dynust_last_stand(kj)%stime,m_dynust_last_stand(kj)%vehclass,m_dynust_last_stand(kj)%vehtype,m_dynust_veh(kj)%ioc,vehatt_P_Size(kj),m_dynust_veh(kj)%NoOfIntDst,m_dynust_veh_nde(kj)%info,m_dynust_veh_nde(kj)%ribf,m_dynust_veh_nde(kj)%compliance,m_dynust_last_stand(kj)%jorig,m_dynust_veh(kj)%EvacSta,m_dynust_veh_nde(kj)%xparinit/m_dynust_network_arc_nde(m_dynust_last_stand(kj)%isec)%s
        ENDIF
 	    DO j_ah=1,m_dynust_veh(kj)%NoOfIntDst ! write out zone number in terms original zone number not master_zone

          IF(m_dynust_network_node(nint(vehatt_Value(kj,vehatt_P_Size(kj)-1,1)))%iConZone(2) > 0) THEN
            WRITE(fu1,400) m_dynust_network_node(nint(vehatt_Value(kj,vehatt_P_Size(kj)-1,1)))%iConZone(2),m_dynust_veh(kj)%IntDestDwell(j_ah)/60.0
          ELSE
            WRITE(fu1,400) m_dynust_network_node_nde(nint(vehatt_Value(kj,vehatt_P_Size(kj)-1,1)))%izone,m_dynust_veh(kj)%IntDestDwell(j_ah)/60.0
		  ENDIF
        ENDDO
        IF(isee == 1) WRITE(fu2,700) m_dynust_network_node_nde(m_dynust_network_arc_nde(m_dynust_last_stand(kj)%isec)%iunod)%IntoOutNodeNum,(m_dynust_network_node_nde(nint(vehatt_Value(kj,js,1)))%IntoOutNodeNum,js=1,vehatt_P_Size(kj)-1)


301   FORMAT(i9,2i7,f8.2,6i6,2f8.4,2i5,f12.8,f6.1)
400   FORMAT(i12,f7.2)
700   FORMAT(1000i7)
302	  FORMAT(i6)

    END SUBROUTINE
   
!~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>
SUBROUTINE ReSort_OutPut_Veh()
USE DYNUST_MAIN_MODULE
USE DYNUST_VEH_PATH_ATT_MODULE

!//COMMENTED BY YI-CHANG CHIU, MARCH, 2010
! This is to resort vehicle by departure time so that the output_vehicle.dat lists vehicles correctly. 

INTEGER,ALLOCATABLE,DIMENSION(:)::xjtmp,xutmp,xdtmp,xvcltmp,xvcl2tmp,xhovtmp,xntmp,xndestmp,xinfotmp,xszone,xevac,xgastmp,xjdest_tmp,xwait_tmp
REAL,ALLOCATABLE,DIMENSION(:)::xstimetp,xribftmp,xcomptmp,xxparini
INTEGER,ALLOCATABLE,DIMENSION(:,:)::ID

TYPE PATH_TYPE
    INTEGER,POINTER::NODE(:)
END TYPE
TYPE(PATH_TYPE),ALLOCATABLE::xjpath_tmp(:)
!> > > > > > > > > >                < < < < < < < < < < <
!>>>>>>>>>>>>>>>>>>>CODES START HERE<<<<<<<<<<<<<<<<<<<<<
!> > > > > > > > > >                < < < < < < < < < < <

CLOSE(97)
CLOSE(98)
OPEN(file="output_vehicle.dat",unit = 97,status='old')
OPEN(file="output_path.dat",unit = 98,status='old')
OPEN(file="output_vehicle_fn.dat",unit = 997,status='unknown')
OPEN(file="output_path_fn.dat",unit = 998,status='unknown')
!OPEN(file="output_vehicle_map.dat",unit = 9997,status='unknown')


!  READ(97,*)  isee
!  READ(97,*)
  WRITE(997,*) totalveh, noofSTOPs ,"   # of vehicles in the file, Max # of STOPs"
  WRITE(997,'("      #   usec   dsec   stime vehcls vehtype ioc #ONode #IntDe info ribf    comp   izone Evac InitPos")') 
! WRITE(9997,*) "altpath/vehtrajectory ID, output_vehicle/vehicle ID"
  ALLOCATE(id(totalveh,2))
  ALLOCATE(xjtmp(totalveh))
  ALLOCATE(xutmp(totalveh))
  ALLOCATE(xdtmp(totalveh))
  ALLOCATE(xvcltmp(totalveh))
  ALLOCATE(xvcl2tmp(totalveh))
  ALLOCATE(xhovtmp(totalveh))
  ALLOCATE(xntmp(totalveh))
  ALLOCATE(xndestmp(totalveh))
  ALLOCATE(xinfotmp(totalveh))
  ALLOCATE(xszone(totalveh))
  ALLOCATE(xevac(totalveh))
  ALLOCATE(xgastmp(totalveh))
  ALLOCATE(xstimetp(totalveh))
  ALLOCATE(xribftmp(totalveh))
  ALLOCATE(xcomptmp(totalveh))
  ALLOCATE(xxparini(totalveh))
  ALLOCATE(xjdest_tmp(totalveh))
  ALLOCATE(xwait_tmp(totalveh))
  ALLOCATE(xjpath_tmp(totalveh))

DO is = 1, totalveh  
  IF(FuelOut > 0) THEN
    READ(97,301)  Xjtmp(is),Xutmp(is),Xdtmp(is),Xstimetp(is),Xvcltmp(is),Xvcl2tmp(is),Xhovtmp(is),Xntmp(is),xndestmp(is),xinfotmp(is),xribftmp(is),xcomptmp(is),xszone(is),xevac(is),xxparini(is),xgastmp(is)
  ELSE  
    READ(97,301)  Xjtmp(is),Xutmp(is),Xdtmp(is),Xstimetp(is),Xvcltmp(is),Xvcl2tmp(is),Xhovtmp(is),Xntmp(is),xndestmp(is),xinfotmp(is),xribftmp(is),xcomptmp(is),xszone(is),xevac(is),xxparini(is)
  ENDIF
  READ(97,400)  xjdest_tmp(is),xwait_tmp(is)
! id(is,2) = is
  id(is,2) = xjtmp(is)
  id(is,1) = nint(Xstimetp(is)*10)
  ALLOCATE(xjpath_tmp(is)%node(xntmp(is)))
  READ(98,700) (xjpath_tmp(is)%node(k),k=1,xntmp(is))
ENDDO

Call IMSLSort(id,totalveh,2)
DO ih = 1, totalveh
  is = id(ih,2)
  IF(FuelOut > 0) THEN
    WRITE(997,301)  Xjtmp(is),Xutmp(is),Xdtmp(is),Xstimetp(is),Xvcltmp(is),Xvcl2tmp(is),xhovtmp(is),Xntmp(is),xndestmp(is),xinfotmp(is),xribftmp(is),xcomptmp(is),xszone(is),xevac(is),xxparini(is),xgastmp(is)
  ELSE
    WRITE(997,301)  Xjtmp(is),Xutmp(is),Xdtmp(is),Xstimetp(is),Xvcltmp(is),Xvcl2tmp(is),xhovtmp(is),Xntmp(is),xndestmp(is),xinfotmp(is),xribftmp(is),xcomptmp(is),xszone(is),xevac(is),xxparini(is)
  ENDIF
  WRITE(997,400) xjdest_tmp(is),xwait_tmp(is)
  WRITE(998,700) (xjpath_tmp(is)%node(k),k=1,xntmp(is))
ENDDO

close(97)
close(98)
close(997)
close(998)
!close(9997)

CALL system('del .\output_vehicle.dat output_path.dat') 
CALL system('rename .\output_vehicle_fn.dat output_vehicle.dat')
CALL system('rename .\output_path_fn.dat output_path.dat')


301   FORMAT(i9,2i7,f8.2,6i6,2f8.4,2i5,f12.8,f6.1)
400   FORMAT(i12,f7.2)
700   FORMAT(1000i7)
  DEALLOCATE(id)
  DEALLOCATE(xjtmp)
  DEALLOCATE(xutmp)
  DEALLOCATE(xdtmp)
  DEALLOCATE(xvcltmp)
  DEALLOCATE(xvcl2tmp)
  DEALLOCATE(xhovtmp)
  DEALLOCATE(xntmp)
  DEALLOCATE(xndestmp)
  DEALLOCATE(xinfotmp)
  DEALLOCATE(xszone)
  DEALLOCATE(xevac)
  DEALLOCATE(xgastmp)
  DEALLOCATE(xstimetp)
  DEALLOCATE(xribftmp)
  DEALLOCATE(xcomptmp)
  DEALLOCATE(xxparini)
  DEALLOCATE(xjdest_tmp)
  DEALLOCATE(xwait_tmp)
  DO ih = 1, totalveh
    DEALLOCATE(xjpath_tmp(ih)%node)
  ENDDO
  DEALLOCATE(xjpath_tmp)
END SUBROUTINE

!~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>
SUBROUTINE WriteUETravelTime
  USE DYNUST_MAIN_MODULE
  USE DYNUST_VEH_PATH_ATT_MODULE
  USE IFQWIN

  OPEN(file='UETravelTime.bip',unit=2000,status='unknown',form='unformatted',iostat=error)
  IF(error /= 0) THEN
    WRITE(911,*) 'Error when opening UETravelTime.bip'
    STOP
  ENDIF

  OPEN(file='UEPenalty.bip',   unit=2001,status='unknown',form='unformatted',iostat=error)  
  IF(error /= 0) THEN
    WRITE(911,*) 'Error when opening UEPenalty.bip'
    STOP
  ENDIF

  OPEN(file='UEIndexFile.ind',   unit=2002,status='unknown',iostat=error)  
  IF(error /= 0) THEN
    WRITE(911,*) 'Error when opening UEIndexFile.ind'
    STOP
  ENDIF
    
  WRITE(2000) TravelTime ! write out the RoutePredsor to the binary file binpath.bip
  WRITE(2001) TravelPenalty
  WRITE(2002,*) noofarcs, aggint, maxmove
  
  close(2000)
  close(2001)
  close(2002)
END SUBROUTINE

!~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>
SUBROUTINE ReadUETravelTime
  USE DYNUST_MAIN_MODULE
  USE DYNUST_VEH_PATH_ATT_MODULE
  USE IFQWIN

  LOGICAL::EXT1, EXT2
  
  INQUIRE (FILE='UETravelTime.bip',EXIST = EXT1)
  IF(EXT1)THEN
  OPEN(file='UETravelTime.bip',unit=2000,status='unknown',form='unformatted',iostat=error)
  IF(error /= 0) THEN
    WRITE(911,*) 'Error when opening UETravelTime.bip'
    STOP
  ENDIF
  ELSE
    WRITE(911,*) "UETravelTime.bip does not exist when requested"
    STOP
  ENDIF

  INQUIRE (FILE='UEPenalty.bip',EXIST = EXT1)
  IF(EXT1)THEN
  OPEN(file='UEPenalty.bip',unit=2001,status='unknown',form='unformatted',iostat=error)  
  IF(error /= 0) THEN
    WRITE(911,*) 'Error when opening UEPenalty.bip'
    STOP
  ENDIF
  ELSE
    WRITE(911,*) "UEPenalty.bip does not exist when requested"
    STOP
  ENDIF

  INQUIRE (FILE='UEIndexFile.ind',EXIST = EXT1)
  IF(EXT1)THEN
  OPEN(file='UEIndexFile.ind',   unit=2002,status='unknown',iostat=error)  
  IF(error /= 0) THEN
    WRITE(911,*) 'Error when opening UEIndexFile.ind'
    STOP
  ENDIF
  ELSE
    WRITE(911,*) "UEIndexFile does not exist when requested"
    STOP
  ENDIF
  
  READ(2002,*) noofarcstmp, iti_nutmp, maxmovetmp

  ALLOCATE(UETravelTime(noofarcstmp,iti_nutmp))
  UETravelTime(:,:) = 0
  
  ALLOCATE(UETravelPenalty(noofarcstmp,iti_nutmp, maxmovetmp))
  UETravelPenalty(:,:,:) = 0

  READ(2000) UETravelTime ! write out the RoutePredsor to the binary file binpath.bip
  READ(2001) UETravelPenalty
  
END SUBROUTINE

!~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>
REAL FUNCTION ranxy(id)
 USE DYNUST_MAIN_MODULE
 INTEGER id
 IF(ranct >= noofran) THEN
!   istrm = istrm + 1
!   CALL RNSET(istrm)
!   CALL RAND_GEN(ranx,iopt=iopti)  
   ranct = 0          
 ENDIF
 ranct = ranct + 1
 ranxy = ranx(ranct)
 
 END FUNCTION
   