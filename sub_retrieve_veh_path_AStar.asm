; mark_description "Intel(R) Visual Fortran Compiler XE for applications running on IA-32, Version 13.1.1.171 Build 20130313";
; mark_description "-S";
	.686P
 	.387
	OPTION DOTNAME
	ASSUME	CS:FLAT,DS:FLAT,SS:FLAT
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
TXTST0:
; -- Begin  _RETRIEVE_VEH_PATH_ASTAR
; mark_begin;
IF @Version GE 800
  .MMX
ELSEIF @Version GE 612
  .MMX
  MMWORD TEXTEQU <QWORD>
ENDIF
IF @Version GE 800
  .XMM
ELSEIF @Version GE 614
  .XMM
  XMMWORD TEXTEQU <OWORD>
ENDIF
       ALIGN     16
	PUBLIC _RETRIEVE_VEH_PATH_ASTAR
_RETRIEVE_VEH_PATH_ASTAR	PROC NEAR 
; parameter 1: 8 + ebx
; parameter 2: 12 + ebx
; parameter 3: 16 + ebx
; parameter 4: 20 + ebx
; parameter 5: 24 + ebx
; parameter 6: 28 + ebx
; parameter 7: 32 + ebx
.B1.1:                          ; Preds .B1.0
        push      ebx                                           ;1.12
        mov       ebx, esp                                      ;1.12
        and       esp, -16                                      ;1.12
        push      ebp                                           ;1.12
        push      ebp                                           ;1.12
        mov       ebp, DWORD PTR [4+ebx]                        ;1.12
        mov       DWORD PTR [4+esp], ebp                        ;1.12
        mov       ebp, esp                                      ;1.12
        sub       esp, 104                                      ;1.12
        xor       eax, eax                                      ;1.12
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_MAXNU_PA] ;1.12
        test      edx, edx                                      ;1.12
        mov       DWORD PTR [-28+ebp], edi                      ;1.12
        cmovg     eax, edx                                      ;1.12
        shl       eax, 2                                        ;1.12
        mov       DWORD PTR [-32+ebp], esi                      ;1.12
        mov       DWORD PTR [-36+ebp], esp                      ;1.12
        call      __alloca_probe                                ;1.12
        and       esp, -16                                      ;1.12
        mov       eax, esp                                      ;1.12
                                ; LOE eax edx
.B1.50:                         ; Preds .B1.1
        mov       edi, eax                                      ;1.12
        test      edx, edx                                      ;41.7
        jle       .B1.4         ; Prob 50%                      ;41.7
                                ; LOE edx edi
.B1.2:                          ; Preds .B1.50
        cmp       edx, 24                                       ;41.7
        jle       .B1.27        ; Prob 0%                       ;41.7
                                ; LOE edx edi
.B1.3:                          ; Preds .B1.2
        shl       edx, 2                                        ;41.7
        push      edx                                           ;41.7
        push      0                                             ;41.7
        push      edi                                           ;41.7
        call      __intel_fast_memset                           ;41.7
                                ; LOE edi
.B1.51:                         ; Preds .B1.3
        add       esp, 12                                       ;41.7
                                ; LOE edi
.B1.4:                          ; Preds .B1.41 .B1.51 .B1.50 .B1.39
        movss     xmm3, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TIME_NOW] ;42.17
        divss     xmm3, DWORD PTR [_2il0floatpacket.7]          ;42.17
        movss     xmm4, DWORD PTR [_2il0floatpacket.8]          ;42.17
        andps     xmm4, xmm3                                    ;42.17
        movaps    xmm2, xmm4                                    ;42.17
        movss     xmm1, DWORD PTR [_2il0floatpacket.9]          ;42.17
        pxor      xmm2, xmm3                                    ;42.17
        movss     xmm5, DWORD PTR [_2il0floatpacket.10]         ;42.17
        cmpltss   xmm2, xmm1                                    ;42.17
        andps     xmm2, xmm1                                    ;42.17
        orps      xmm2, xmm4                                    ;42.17
        movaps    xmm7, xmm2                                    ;42.17
        mov       ecx, DWORD PTR [8+ebx]                        ;1.12
        addss     xmm7, xmm3                                    ;42.17
        mov       esi, DWORD PTR [ecx]                          ;44.7
        subss     xmm7, xmm2                                    ;42.17
        movaps    xmm6, xmm7                                    ;42.17
        mov       DWORD PTR [-68+ebp], esi                      ;44.7
        subss     xmm6, xmm3                                    ;42.17
        movss     xmm3, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIMPERIOD] ;45.14
        cmpnless  xmm6, xmm4                                    ;42.17
        andps     xmm6, xmm5                                    ;42.17
        movss     xmm4, DWORD PTR [_2il0floatpacket.11]         ;45.14
        subss     xmm7, xmm6                                    ;42.17
        cvtss2si  edx, xmm7                                     ;42.17
        inc       edx                                           ;42.38
        movaps    xmm6, xmm4                                    ;45.14
        cvtsi2ss  xmm0, edx                                     ;42.7
        addss     xmm6, xmm4                                    ;45.14
        movss     DWORD PTR [-56+ebp], xmm0                     ;42.7
        movss     xmm0, DWORD PTR [_2il0floatpacket.8]          ;45.14
        andps     xmm0, xmm3                                    ;45.14
        pxor      xmm3, xmm0                                    ;45.14
        movaps    xmm2, xmm3                                    ;45.14
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;44.7
        cmpltss   xmm2, xmm1                                    ;45.14
        andps     xmm1, xmm2                                    ;45.14
        movaps    xmm2, xmm3                                    ;45.14
        neg       edx                                           ;44.7
        addss     xmm2, xmm1                                    ;45.14
        add       edx, esi                                      ;44.7
        subss     xmm2, xmm1                                    ;45.14
        movaps    xmm1, xmm2                                    ;45.14
        shl       edx, 5                                        ;44.7
        subss     xmm1, xmm3                                    ;45.14
        movaps    xmm5, xmm1                                    ;45.14
        cmpless   xmm1, DWORD PTR [_2il0floatpacket.12]         ;45.14
        cmpnless  xmm5, xmm4                                    ;45.14
        andps     xmm5, xmm6                                    ;45.14
        andps     xmm1, xmm6                                    ;45.14
        mov       esi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;44.7
        subss     xmm2, xmm5                                    ;45.14
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DESTINATION+32] ;44.7
        addss     xmm2, xmm1                                    ;45.14
        neg       eax                                           ;44.7
        orps      xmm2, xmm0                                    ;45.14
        movsx     ecx, WORD PTR [20+esi+edx]                    ;44.7
        add       eax, ecx                                      ;44.7
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DESTINATION] ;44.7
        mov       eax, DWORD PTR [edx+eax*4]                    ;44.7
        lea       edx, DWORD PTR [-48+ebp]                      ;47.12
        mov       DWORD PTR [-52+ebp], eax                      ;44.7
        cvtss2si  eax, xmm2                                     ;45.7
        push      edx                                           ;47.12
        mov       DWORD PTR [-48+ebp], eax                      ;45.7
        call      _DYNUST_TDSP_ASTAR_MODULE_mp_CREATE_ASTAR_SCAN_ELIGIBLE_SET ;47.12
                                ; LOE edi
.B1.52:                         ; Preds .B1.4
        add       esp, 4                                        ;47.12
                                ; LOE edi
.B1.5:                          ; Preds .B1.52
        call      _DYNUST_TDSP_ASTAR_MODULE_mp_CREATE_ASTAR_FIXED_LABEL_SET ;48.12
                                ; LOE edi
.B1.6:                          ; Preds .B1.5
        push      OFFSET FLAT: _DYNUST_MAIN_MODULE_mp_NOOFARCS  ;49.12
        call      _DYNUST_TDSP_ASTAR_MODULE_mp_CREATE_LABELS    ;49.12
                                ; LOE edi
.B1.53:                         ; Preds .B1.6
        add       esp, 4                                        ;49.12
                                ; LOE edi
.B1.7:                          ; Preds .B1.53
        push      OFFSET FLAT: _DYNUST_MAIN_MODULE_mp_NOOFNODES_ORG ;51.12
        push      OFFSET FLAT: __NLITPACK_0.0.1                 ;51.12
        mov       esi, DWORD PTR [12+ebx]                       ;1.12
        lea       eax, DWORD PTR [-56+ebp]                      ;51.12
        push      eax                                           ;51.12
        lea       edx, DWORD PTR [-52+ebp]                      ;51.12
        push      edx                                           ;51.12
        push      esi                                           ;51.12
        call      _DYNUST_TDSP_ASTAR_MODULE_mp_ASTAR_INITIALIZATION ;51.12
                                ; LOE esi edi
.B1.54:                         ; Preds .B1.7
        add       esp, 20                                       ;51.12
                                ; LOE esi edi
.B1.8:                          ; Preds .B1.54
        mov       eax, OFFSET FLAT: __NLITPACK_0.0.1            ;52.12
        lea       edx, DWORD PTR [-56+ebp]                      ;52.12
        push      eax                                           ;52.12
        push      eax                                           ;52.12
        push      edx                                           ;52.12
        lea       ecx, DWORD PTR [-44+ebp]                      ;52.12
        push      ecx                                           ;52.12
        lea       eax, DWORD PTR [-52+ebp]                      ;52.12
        push      eax                                           ;52.12
        push      esi                                           ;52.12
        call      _DYNUST_TDSP_ASTAR_MODULE_mp_ASTAR_GET_PATH   ;52.12
                                ; LOE esi edi
.B1.55:                         ; Preds .B1.8
        add       esp, 24                                       ;52.12
                                ; LOE esi edi
.B1.9:                          ; Preds .B1.55
        lea       eax, DWORD PTR [-40+ebp]                      ;53.12
        push      eax                                           ;53.12
        push      edi                                           ;53.12
        lea       edx, DWORD PTR [-44+ebp]                      ;53.12
        push      edx                                           ;53.12
        push      esi                                           ;53.12
        call      _DYNUST_TDSP_ASTAR_MODULE_mp_RETRIEVETDSP     ;53.12
                                ; LOE edi
.B1.56:                         ; Preds .B1.9
        add       esp, 16                                       ;53.12
                                ; LOE edi
.B1.10:                         ; Preds .B1.56
        lea       eax, DWORD PTR [-48+ebp]                      ;55.12
        push      eax                                           ;55.12
        call      _DYNUST_TDSP_ASTAR_MODULE_mp_DESTROY_ASTAR_SCAN_ELIGIBLE_SET ;55.12
                                ; LOE edi
.B1.57:                         ; Preds .B1.10
        add       esp, 4                                        ;55.12
                                ; LOE edi
.B1.11:                         ; Preds .B1.57
        call      _DYNUST_TDSP_ASTAR_MODULE_mp_DESTROY_ASTAR_FIXED_LABEL_SET ;56.12
                                ; LOE edi
.B1.12:                         ; Preds .B1.11
        call      _DYNUST_TDSP_ASTAR_MODULE_mp_DESTROY_LABELS   ;57.12
                                ; LOE edi
.B1.13:                         ; Preds .B1.12
        mov       esi, DWORD PTR [-40+ebp]                      ;59.7
        cmp       DWORD PTR [edi], 900000                       ;61.22
        jle       .B1.15        ; Prob 50%                      ;61.22
                                ; LOE esi edi
.B1.14:                         ; Preds .B1.13
        push      32                                            ;62.9
        mov       DWORD PTR [-104+ebp], 0                       ;62.9
        lea       eax, DWORD PTR [-64+ebp]                      ;62.9
        push      eax                                           ;62.9
        push      OFFSET FLAT: __STRLITPACK_1.0.1               ;62.9
        push      -2088435968                                   ;62.9
        push      -1                                            ;62.9
        mov       DWORD PTR [-64+ebp], 14                       ;62.9
        lea       edx, DWORD PTR [-104+ebp]                     ;62.9
        push      edx                                           ;62.9
        mov       DWORD PTR [-60+ebp], OFFSET FLAT: __STRLITPACK_0 ;62.9
        call      _for_write_seq_lis                            ;62.9
                                ; LOE esi edi
.B1.58:                         ; Preds .B1.14
        add       esp, 24                                       ;62.9
                                ; LOE esi edi
.B1.15:                         ; Preds .B1.58 .B1.13
        mov       ecx, DWORD PTR [20+ebx]                       ;1.12
        mov       eax, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY] ;64.7
        mov       edx, DWORD PTR [ecx]                          ;64.10
        imul      ecx, DWORD PTR [-68+ebp], 76                  ;64.32
        mov       DWORD PTR [-24+ebp], ecx                      ;64.32
        add       eax, ecx                                      ;64.32
        imul      ecx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY+32], -76 ;64.32
        mov       DWORD PTR [-72+ebp], edx                      ;64.10
        lea       edx, DWORD PTR [-1+esi+edx]                   ;64.45
        movsx     eax, WORD PTR [72+ecx+eax]                    ;64.10
        cmp       eax, edx                                      ;64.32
        mov       DWORD PTR [-20+ebp], edx                      ;64.45
        jge       .B1.19        ; Prob 50%                      ;64.32
                                ; LOE eax esi edi
.B1.16:                         ; Preds .B1.15
        mov       edx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_V_INCREASESIZE] ;65.23
        lea       ecx, DWORD PTR [-68+ebp]                      ;66.13
        add       edx, eax                                      ;65.44
        mov       eax, DWORD PTR [-20+ebp]                      ;65.8
        cmp       edx, eax                                      ;65.8
        push      ecx                                           ;66.13
        push      DWORD PTR [8+ebx]                             ;66.13
        cmovl     edx, eax                                      ;65.8
        mov       DWORD PTR [-68+ebp], edx                      ;65.8
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_P_SETUP ;66.13
                                ; LOE esi edi
.B1.59:                         ; Preds .B1.16
        add       esp, 8                                        ;66.13
                                ; LOE esi edi
.B1.17:                         ; Preds .B1.59
        lea       eax, DWORD PTR [-68+ebp]                      ;67.13
        push      eax                                           ;67.13
        push      DWORD PTR [8+ebx]                             ;67.13
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_A_SETUP ;67.13
                                ; LOE esi edi
.B1.60:                         ; Preds .B1.17
        add       esp, 8                                        ;67.13
                                ; LOE esi edi
.B1.18:                         ; Preds .B1.60
        imul      ecx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY+32], -76 ;68.8
        add       ecx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY] ;68.8
        mov       edx, DWORD PTR [-24+ebp]                      ;68.8
        mov       eax, DWORD PTR [-68+ebp]                      ;68.32
        mov       WORD PTR [72+edx+ecx], ax                     ;68.8
                                ; LOE esi edi
.B1.19:                         ; Preds .B1.15 .B1.18
        test      esi, esi                                      ;71.7
        jle       .B1.24        ; Prob 2%                       ;71.7
                                ; LOE esi edi
.B1.20:                         ; Preds .B1.19
        mov       eax, 1                                        ;
        lea       edx, DWORD PTR [edi+esi*4]                    ;
        mov       edi, DWORD PTR [-72+ebp]                      ;
        mov       DWORD PTR [-16+ebp], esi                      ;
        mov       esi, eax                                      ;
        mov       DWORD PTR [-8+ebp], edx                       ;
                                ; LOE esi edi
.B1.21:                         ; Preds .B1.22 .B1.20
        mov       ecx, DWORD PTR [-8+ebp]                       ;72.9
        lea       edx, DWORD PTR [esi*4]                        ;72.24
        neg       edx                                           ;72.24
        mov       WORD PTR [-4+ebp], di                         ;73.9
        cvtsi2ss  xmm0, DWORD PTR [edx+ecx]                     ;72.9
        movss     DWORD PTR [-12+ebp], xmm0                     ;72.9
        lea       edx, DWORD PTR [-12+ebp]                      ;74.11
        push      edx                                           ;74.11
        push      OFFSET FLAT: __NLITPACK_1.0.1                 ;74.11
        lea       edx, DWORD PTR [-4+ebp]                       ;74.11
        push      edx                                           ;74.11
        push      DWORD PTR [8+ebx]                             ;74.11
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_INSERT  ;74.11
                                ; LOE esi edi
.B1.61:                         ; Preds .B1.21
        add       esp, 16                                       ;74.11
                                ; LOE esi edi
.B1.22:                         ; Preds .B1.61
        inc       esi                                           ;75.4
        inc       edi                                           ;75.4
        cmp       esi, DWORD PTR [-16+ebp]                      ;75.4
        jle       .B1.21        ; Prob 82%                      ;75.4
                                ; LOE esi edi
.B1.23:                         ; Preds .B1.22
        mov       esi, DWORD PTR [-16+ebp]                      ;
                                ; LOE esi
.B1.24:                         ; Preds .B1.23 .B1.19
        mov       eax, DWORD PTR [-20+ebp]                      ;77.9
        lea       edx, DWORD PTR [-16+ebp]                      ;77.9
        push      edx                                           ;77.9
        push      DWORD PTR [8+ebx]                             ;77.9
        mov       DWORD PTR [-16+ebp], eax                      ;77.9
        add       esi, DWORD PTR [-72+ebp]                      ;77.37
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ACTIVE_SIZE ;77.9
                                ; LOE esi
.B1.62:                         ; Preds .B1.24
        add       esp, 8                                        ;77.9
                                ; LOE esi
.B1.25:                         ; Preds .B1.62
        mov       eax, DWORD PTR [28+ebx]                       ;1.12
        cmp       DWORD PTR [eax], 1                            ;79.14
        je        .B1.46        ; Prob 16%                      ;79.14
                                ; LOE esi
.B1.26:                         ; Preds .B1.64 .B1.46 .B1.25
        mov       eax, DWORD PTR [-36+ebp]                      ;84.1
        mov       esp, eax                                      ;84.1
                                ; LOE
.B1.63:                         ; Preds .B1.26
        mov       esi, DWORD PTR [-32+ebp]                      ;84.1
        mov       edi, DWORD PTR [-28+ebp]                      ;84.1
        mov       esp, ebp                                      ;84.1
        pop       ebp                                           ;84.1
        mov       esp, ebx                                      ;84.1
        pop       ebx                                           ;84.1
        ret                                                     ;84.1
                                ; LOE
.B1.27:                         ; Preds .B1.2                   ; Infreq
        cmp       edx, 4                                        ;41.7
        jl        .B1.43        ; Prob 10%                      ;41.7
                                ; LOE edx edi
.B1.28:                         ; Preds .B1.27                  ; Infreq
        mov       eax, edi                                      ;41.7
        and       eax, 15                                       ;41.7
        je        .B1.31        ; Prob 50%                      ;41.7
                                ; LOE eax edx edi
.B1.29:                         ; Preds .B1.28                  ; Infreq
        test      al, 3                                         ;41.7
        jne       .B1.43        ; Prob 10%                      ;41.7
                                ; LOE eax edx edi
.B1.30:                         ; Preds .B1.29                  ; Infreq
        neg       eax                                           ;41.7
        add       eax, 16                                       ;41.7
        shr       eax, 2                                        ;41.7
                                ; LOE eax edx edi
.B1.31:                         ; Preds .B1.30 .B1.28           ; Infreq
        lea       ecx, DWORD PTR [4+eax]                        ;41.7
        cmp       edx, ecx                                      ;41.7
        jl        .B1.43        ; Prob 10%                      ;41.7
                                ; LOE eax edx edi
.B1.32:                         ; Preds .B1.31                  ; Infreq
        mov       esi, edx                                      ;41.7
        sub       esi, eax                                      ;41.7
        and       esi, 3                                        ;41.7
        neg       esi                                           ;41.7
        add       esi, edx                                      ;41.7
        test      eax, eax                                      ;41.7
        jbe       .B1.36        ; Prob 10%                      ;41.7
                                ; LOE eax edx esi edi
.B1.33:                         ; Preds .B1.32                  ; Infreq
        xor       ecx, ecx                                      ;
                                ; LOE eax edx ecx esi edi
.B1.34:                         ; Preds .B1.34 .B1.33           ; Infreq
        mov       DWORD PTR [edi+ecx*4], 0                      ;41.7
        inc       ecx                                           ;41.7
        cmp       ecx, eax                                      ;41.7
        jb        .B1.34        ; Prob 82%                      ;41.7
                                ; LOE eax edx ecx esi edi
.B1.36:                         ; Preds .B1.34 .B1.32           ; Infreq
        pxor      xmm0, xmm0                                    ;41.7
                                ; LOE eax edx esi edi xmm0
.B1.37:                         ; Preds .B1.37 .B1.36           ; Infreq
        movdqa    XMMWORD PTR [edi+eax*4], xmm0                 ;41.7
        add       eax, 4                                        ;41.7
        cmp       eax, esi                                      ;41.7
        jb        .B1.37        ; Prob 82%                      ;41.7
                                ; LOE eax edx esi edi xmm0
.B1.39:                         ; Preds .B1.37 .B1.43           ; Infreq
        cmp       esi, edx                                      ;41.7
        jae       .B1.4         ; Prob 10%                      ;41.7
                                ; LOE edx esi edi
.B1.41:                         ; Preds .B1.39 .B1.41           ; Infreq
        mov       DWORD PTR [edi+esi*4], 0                      ;41.7
        inc       esi                                           ;41.7
        cmp       esi, edx                                      ;41.7
        jb        .B1.41        ; Prob 82%                      ;41.7
        jmp       .B1.4         ; Prob 100%                     ;41.7
                                ; LOE edx esi edi
.B1.43:                         ; Preds .B1.27 .B1.31 .B1.29    ; Infreq
        xor       esi, esi                                      ;41.7
        jmp       .B1.39        ; Prob 100%                     ;41.7
                                ; LOE edx esi edi
.B1.46:                         ; Preds .B1.25                  ; Infreq
        imul      edx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY+32], -76 ;79.18
        add       edx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY] ;79.18
        mov       eax, DWORD PTR [-24+ebp]                      ;79.38
        movsx     ecx, WORD PTR [72+eax+edx]                    ;79.38
        cmp       esi, ecx                                      ;79.35
        jg        .B1.26        ; Prob 50%                      ;79.35
                                ; LOE esi
.B1.47:                         ; Preds .B1.46                  ; Infreq
        mov       DWORD PTR [-72+ebp], esi                      ;80.11
        lea       eax, DWORD PTR [-72+ebp]                      ;80.11
        push      eax                                           ;80.11
        push      DWORD PTR [8+ebx]                             ;80.11
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_CLEAR   ;80.11
                                ; LOE
.B1.64:                         ; Preds .B1.47                  ; Infreq
        add       esp, 8                                        ;80.11
        jmp       .B1.26        ; Prob 100%                     ;80.11
        ALIGN     16
                                ; LOE
; mark_end;
_RETRIEVE_VEH_PATH_ASTAR ENDP
_TEXT	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
__NLITPACK_0.0.1	DD	0
__STRLITPACK_1.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__NLITPACK_1.0.1	DD	1
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _RETRIEVE_VEH_PATH_ASTAR
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
__STRLITPACK_0	DB	102
	DB	111
	DB	117
	DB	110
	DB	100
	DB	32
	DB	99
	DB	101
	DB	110
	DB	116
	DB	114
	DB	111
	DB	105
	DB	100
	DB	0
	DB 1 DUP ( 0H)	; pad
_2il0floatpacket.7	DD	042700000H
_2il0floatpacket.8	DD	080000000H
_2il0floatpacket.9	DD	04b000000H
_2il0floatpacket.10	DD	03f800000H
_2il0floatpacket.11	DD	03f000000H
_2il0floatpacket.12	DD	0bf000000H
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
EXTRN	_DYNUST_VEH_PATH_ATT_MODULE_mp_V_INCREASESIZE:BYTE
EXTRN	_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NOOFNODES_ORG:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NOOFARCS:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_SIMPERIOD:BYTE
EXTRN	_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_DESTINATION:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TIME_NOW:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_MAXNU_PA:BYTE
_DATA	ENDS
EXTRN	_for_write_seq_lis:PROC
EXTRN	_DYNUST_TDSP_ASTAR_MODULE_mp_CREATE_ASTAR_SCAN_ELIGIBLE_SET:PROC
EXTRN	_DYNUST_TDSP_ASTAR_MODULE_mp_CREATE_ASTAR_FIXED_LABEL_SET:PROC
EXTRN	_DYNUST_TDSP_ASTAR_MODULE_mp_CREATE_LABELS:PROC
EXTRN	_DYNUST_TDSP_ASTAR_MODULE_mp_ASTAR_INITIALIZATION:PROC
EXTRN	_DYNUST_TDSP_ASTAR_MODULE_mp_ASTAR_GET_PATH:PROC
EXTRN	_DYNUST_TDSP_ASTAR_MODULE_mp_RETRIEVETDSP:PROC
EXTRN	_DYNUST_TDSP_ASTAR_MODULE_mp_DESTROY_ASTAR_SCAN_ELIGIBLE_SET:PROC
EXTRN	_DYNUST_TDSP_ASTAR_MODULE_mp_DESTROY_ASTAR_FIXED_LABEL_SET:PROC
EXTRN	_DYNUST_TDSP_ASTAR_MODULE_mp_DESTROY_LABELS:PROC
EXTRN	_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_P_SETUP:PROC
EXTRN	_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_A_SETUP:PROC
EXTRN	_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_INSERT:PROC
EXTRN	_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ACTIVE_SIZE:PROC
EXTRN	_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_CLEAR:PROC
EXTRN	__intel_fast_memset:PROC
EXTRN	__alloca_probe:PROC
EXTRN	__fltused:BYTE
	INCLUDELIB <ifconsol>
	INCLUDELIB <libifcoremt>
	INCLUDELIB <libifport>
	INCLUDELIB <libmmt>
	INCLUDELIB <LIBCMT>
	INCLUDELIB <libirc>
	INCLUDELIB <svml_dispmt>
	INCLUDELIB <OLDNAMES>
	END
