; mark_description "Intel(R) Visual Fortran Compiler XE for applications running on IA-32, Version 13.1.1.171 Build 20130313";
; mark_description "-S";
	.686P
 	.387
	OPTION DOTNAME
	ASSUME	CS:FLAT,DS:FLAT,SS:FLAT
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
TXTST0:
; -- Begin  _READ_VEHICLES
; mark_begin;
IF @Version GE 800
  .MMX
ELSEIF @Version GE 612
  .MMX
  MMWORD TEXTEQU <QWORD>
ENDIF
IF @Version GE 800
  .XMM
ELSEIF @Version GE 614
  .XMM
  XMMWORD TEXTEQU <OWORD>
ENDIF
       ALIGN     16
	PUBLIC _READ_VEHICLES
_READ_VEHICLES	PROC NEAR 
; parameter 1: 8 + ebp
; parameter 2: 12 + ebp
.B1.1:                          ; Preds .B1.0
        push      ebp                                           ;1.12
        mov       ebp, esp                                      ;1.12
        and       esp, -16                                      ;1.12
        push      esi                                           ;1.12
        push      edi                                           ;1.12
        push      ebx                                           ;1.12
        sub       esp, 660                                      ;1.12
        mov       eax, DWORD PTR [12+ebp]                       ;1.12
        cmp       DWORD PTR [eax], 1                            ;53.10
        jne       .B1.3         ; Prob 84%                      ;53.10
                                ; LOE
.B1.2:                          ; Preds .B1.1
        imul      edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VEHPROFILE+32], -36 ;54.6
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VEHPROFILE] ;54.6
        mov       DWORD PTR [_READ_VEHICLES$VEHTMP.0.1+8], 1065353216 ;56.6
        movss     xmm0, DWORD PTR [44+edx+ecx]                  ;54.6
        mov       eax, DWORD PTR [44+edx+ecx]                   ;54.6
        mov       DWORD PTR [_READ_VEHICLES$VEHTMP.0.1], eax    ;54.6
        addss     xmm0, DWORD PTR [80+edx+ecx]                  ;55.6
        movss     DWORD PTR [_READ_VEHICLES$VEHTMP.0.1+4], xmm0 ;55.6
                                ; LOE
.B1.3:                          ; Preds .B1.1 .B1.2
        mov       ebx, DWORD PTR [_READ_VEHICLES$EOFID.0.1]     ;58.5
        test      bl, 1                                         ;58.13
        je        .B1.5         ; Prob 60%                      ;58.13
                                ; LOE ebx
.B1.4:                          ; Preds .B1.314 .B1.3 .B1.311
        add       esp, 660                                      ;439.7
        pop       ebx                                           ;439.7
        pop       edi                                           ;439.7
        pop       esi                                           ;439.7
        mov       esp, ebp                                      ;439.7
        pop       ebp                                           ;439.7
        ret                                                     ;439.7
                                ; LOE
.B1.5:                          ; Preds .B1.3
        xor       esi, esi                                      ;59.5
                                ; LOE ebx esi
.B1.6:                          ; Preds .B1.7 .B1.5
        imul      eax, esi, 4000000                             ;59.5
        push      4000000                                       ;59.5
        push      0                                             ;59.5
        lea       edx, DWORD PTR [_READ_VEHICLES$IDARRAY.0.1+eax] ;59.5
        push      edx                                           ;59.5
        call      __intel_fast_memset                           ;59.5
                                ; LOE ebx esi
.B1.481:                        ; Preds .B1.6
        add       esp, 12                                       ;59.5
                                ; LOE ebx esi
.B1.7:                          ; Preds .B1.481
        inc       esi                                           ;59.5
        cmp       esi, 2                                        ;59.5
        jb        .B1.6         ; Prob 52%                      ;59.5
                                ; LOE ebx esi
.B1.8:                          ; Preds .B1.7
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+24] ;62.5
        test      eax, eax                                      ;62.5
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;62.5
        jle       .B1.15        ; Prob 50%                      ;62.5
                                ; LOE eax edx ebx
.B1.9:                          ; Preds .B1.8
        mov       esi, eax                                      ;62.5
        shr       esi, 31                                       ;62.5
        add       esi, eax                                      ;62.5
        sar       esi, 1                                        ;62.5
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;62.5
        test      esi, esi                                      ;62.5
        jbe       .B1.421       ; Prob 10%                      ;62.5
                                ; LOE eax edx ecx ebx esi
.B1.10:                         ; Preds .B1.9
        xor       edi, edi                                      ;
        mov       DWORD PTR [esp], edx                          ;
        mov       DWORD PTR [460+esp], ebx                      ;
        xor       ebx, ebx                                      ;
        mov       edx, ebx                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B1.11:                         ; Preds .B1.11 .B1.10
        inc       edi                                           ;62.5
        mov       WORD PTR [684+edx+ecx], bx                    ;62.5
        mov       WORD PTR [1584+edx+ecx], bx                   ;62.5
        add       edx, 1800                                     ;62.5
        cmp       edi, esi                                      ;62.5
        jb        .B1.11        ; Prob 63%                      ;62.5
                                ; LOE eax edx ecx ebx esi edi
.B1.12:                         ; Preds .B1.11
        mov       edx, DWORD PTR [esp]                          ;
        lea       esi, DWORD PTR [1+edi+edi]                    ;62.5
        mov       ebx, DWORD PTR [460+esp]                      ;
                                ; LOE eax edx ecx ebx esi
.B1.13:                         ; Preds .B1.12 .B1.421
        lea       edi, DWORD PTR [-1+esi]                       ;62.5
        cmp       eax, edi                                      ;62.5
        jbe       .B1.15        ; Prob 10%                      ;62.5
                                ; LOE edx ecx ebx esi
.B1.14:                         ; Preds .B1.13
        mov       eax, edx                                      ;62.5
        add       esi, edx                                      ;62.5
        neg       eax                                           ;62.5
        xor       edx, edx                                      ;62.5
        add       eax, esi                                      ;62.5
        imul      esi, eax, 900                                 ;62.5
        mov       WORD PTR [-216+ecx+esi], dx                   ;62.5
                                ; LOE ebx
.B1.15:                         ; Preds .B1.14 .B1.13 .B1.8
        mov       eax, DWORD PTR [8+ebp]                        ;1.12
        movss     xmm0, DWORD PTR [_2il0floatpacket.2]          ;63.16
        movss     xmm1, DWORD PTR [eax]                         ;63.5
        movss     DWORD PTR [28+esp], xmm1                      ;63.5
        comiss    xmm0, xmm1                                    ;63.16
        ja        .B1.422       ; Prob 5%                       ;63.16
                                ; LOE ebx
.B1.16:                         ; Preds .B1.15
        mov       edx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLODTMP+36] ;69.9
        test      edx, edx                                      ;69.9
        mov       ecx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLODTMP+44] ;69.9
        jle       .B1.18        ; Prob 10%                      ;69.9
                                ; LOE edx ecx ebx
.B1.17:                         ; Preds .B1.16
        mov       eax, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLODTMP+24] ;69.9
        test      eax, eax                                      ;69.9
        jg        .B1.130       ; Prob 50%                      ;69.9
                                ; LOE eax edx ecx ebx
.B1.18:                         ; Preds .B1.418 .B1.415 .B1.17 .B1.16
        mov       esi, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLODZONE+24] ;70.9
        test      esi, esi                                      ;70.9
        jle       .B1.21        ; Prob 50%                      ;70.9
                                ; LOE ebx esi
.B1.19:                         ; Preds .B1.18
        mov       ecx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLODZONE] ;70.9
        cmp       esi, 24                                       ;70.9
        jle       .B1.135       ; Prob 0%                       ;70.9
                                ; LOE ecx ebx esi
.B1.20:                         ; Preds .B1.19
        shl       esi, 2                                        ;70.9
        push      esi                                           ;70.9
        push      0                                             ;70.9
        push      ecx                                           ;70.9
        call      __intel_fast_memset                           ;70.9
                                ; LOE ebx
.B1.482:                        ; Preds .B1.20
        add       esp, 12                                       ;70.9
                                ; LOE ebx
.B1.21:                         ; Preds .B1.149 .B1.147 .B1.482 .B1.431 .B1.18
                                ;      
        movss     xmm0, DWORD PTR [28+esp]                      ;74.16
        pxor      xmm1, xmm1                                    ;74.16
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_IVCT], 0    ;72.5
        ucomiss   xmm0, xmm1                                    ;74.16
        jne       .B1.23        ; Prob 84%                      ;74.16
        jp        .B1.23        ; Prob 0%                       ;74.16
                                ; LOE ebx
.B1.22:                         ; Preds .B1.21
        xor       eax, eax                                      ;75.4
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_JRESTORE], eax ;75.4
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_IREAD_VEH_COUNT], eax ;76.4
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_IREAD_VEH_FLAG], 1 ;77.7
                                ; LOE ebx
.B1.23:                         ; Preds .B1.21 .B1.22
        movss     xmm0, DWORD PTR [_2il0floatpacket.3]          ;337.127
        movss     xmm6, DWORD PTR [_2il0floatpacket.10]         ;255.29
        andps     xmm6, xmm0                                    ;255.29
        pxor      xmm0, xmm6                                    ;255.29
        movss     xmm7, DWORD PTR [_2il0floatpacket.11]         ;255.29
        movaps    xmm1, xmm0                                    ;255.29
        movaps    xmm5, xmm0                                    ;255.29
        cmpltss   xmm1, xmm7                                    ;255.29
        andps     xmm1, xmm7                                    ;255.29
        movss     xmm3, DWORD PTR [_2il0floatpacket.5]          ;253.32
        addss     xmm5, xmm1                                    ;255.29
        mov       DWORD PTR [380+esp], 0                        ;
        subss     xmm5, xmm1                                    ;255.29
        movaps    xmm2, xmm5                                    ;255.29
        mov       DWORD PTR [460+esp], ebx                      ;191.65
        subss     xmm2, xmm0                                    ;255.29
        movss     xmm0, DWORD PTR [_2il0floatpacket.12]         ;255.29
        movaps    xmm4, xmm2                                    ;255.29
        movaps    xmm1, xmm0                                    ;255.29
        cmpnless  xmm4, xmm0                                    ;255.29
        addss     xmm1, xmm0                                    ;255.29
        cmpless   xmm2, DWORD PTR [_2il0floatpacket.13]         ;255.29
        andps     xmm4, xmm1                                    ;255.29
        andps     xmm2, xmm1                                    ;255.29
        subss     xmm5, xmm4                                    ;255.29
        movss     xmm4, DWORD PTR [_2il0floatpacket.10]         ;253.32
        andps     xmm4, xmm3                                    ;253.32
        addss     xmm5, xmm2                                    ;255.29
        pxor      xmm3, xmm4                                    ;253.32
        orps      xmm5, xmm6                                    ;255.29
        movaps    xmm6, xmm3                                    ;253.32
        movaps    xmm2, xmm3                                    ;253.32
        cvtss2si  eax, xmm5                                     ;255.29
        cmpltss   xmm6, xmm7                                    ;253.32
        cvtsi2ss  xmm5, eax                                     ;255.4
        andps     xmm7, xmm6                                    ;253.32
        movss     DWORD PTR [220+esp], xmm5                     ;191.65
        addss     xmm2, xmm7                                    ;253.32
        subss     xmm2, xmm7                                    ;253.32
        movaps    xmm6, xmm2                                    ;253.32
        subss     xmm6, xmm3                                    ;253.32
        movaps    xmm3, xmm6                                    ;253.32
        cmpless   xmm6, DWORD PTR [_2il0floatpacket.13]         ;253.32
        cmpnless  xmm3, xmm0                                    ;253.32
        andps     xmm3, xmm1                                    ;253.32
        andps     xmm6, xmm1                                    ;253.32
        subss     xmm2, xmm3                                    ;253.32
        addss     xmm2, xmm6                                    ;253.32
        orps      xmm2, xmm4                                    ;253.32
        cvtss2si  edx, xmm2                                     ;253.32
        cvtsi2ss  xmm2, edx                                     ;253.7
        movss     DWORD PTR [236+esp], xmm2                     ;191.65
                                ; LOE
.B1.24:                         ; Preds .B1.198 .B1.196 .B1.195 .B1.194 .B1.193
                                ;       .B1.224 .B1.191 .B1.129 .B1.209 .B1.23
                                ;      
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_IREAD_VEH_COUNT] ;80.1
        cmp       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_MAXVEHICLESFILE] ;80.24
        jne       .B1.27        ; Prob 50%                      ;80.24
                                ; LOE
.B1.25:                         ; Preds .B1.24
        test      BYTE PTR [460+esp], 1                         ;80.52
        jne       .B1.27        ; Prob 40%                      ;80.52
                                ; LOE
.B1.26:                         ; Preds .B1.25
        mov       DWORD PTR [_READ_VEHICLES$EOFID.0.1], -1      ;81.7
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_IREAD_VEH_FLAG] ;92.2
        mov       DWORD PTR [460+esp], -1                       ;
        jmp       .B1.28        ; Prob 100%                     ;
                                ; LOE eax
.B1.27:                         ; Preds .B1.24 .B1.25
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_IREAD_VEH_FLAG] ;86.3
        cmp       eax, 1                                        ;86.21
        je        .B1.376       ; Prob 16%                      ;86.21
                                ; LOE eax
.B1.28:                         ; Preds .B1.397 .B1.396 .B1.393 .B1.27 .B1.26
                                ;      
        test      eax, eax                                      ;92.24
        jne       .B1.30        ; Prob 38%                      ;92.24
                                ; LOE
.B1.29:                         ; Preds .B1.28
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_IREAD_VEH_FLAG], 1 ;92.30
                                ; LOE
.B1.30:                         ; Preds .B1.28 .B1.29
        movss     xmm4, DWORD PTR [_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT] ;94.7
        movss     xmm2, DWORD PTR [_READ_VEHICLES$STIMETP.0.1]  ;94.7
        divss     xmm2, xmm4                                    ;94.7
        movss     xmm5, DWORD PTR [_2il0floatpacket.10]         ;94.7
        andps     xmm5, xmm2                                    ;94.7
        pxor      xmm2, xmm5                                    ;94.7
        movss     xmm0, DWORD PTR [_2il0floatpacket.11]         ;94.7
        movaps    xmm1, xmm2                                    ;94.7
        movaps    xmm3, xmm2                                    ;94.7
        cmpltss   xmm1, xmm0                                    ;94.7
        andps     xmm1, xmm0                                    ;94.7
        mov       eax, DWORD PTR [8+ebp]                        ;94.35
        addss     xmm3, xmm1                                    ;94.7
        subss     xmm3, xmm1                                    ;94.7
        movss     xmm1, DWORD PTR [_2il0floatpacket.12]         ;94.7
        movaps    xmm7, xmm3                                    ;94.7
        subss     xmm7, xmm2                                    ;94.7
        movaps    xmm2, xmm1                                    ;94.7
        addss     xmm2, xmm1                                    ;94.7
        movaps    xmm6, xmm7                                    ;94.7
        cmpless   xmm7, DWORD PTR [_2il0floatpacket.13]         ;94.7
        cmpnless  xmm6, xmm1                                    ;94.7
        andps     xmm6, xmm2                                    ;94.7
        andps     xmm7, xmm2                                    ;94.7
        subss     xmm3, xmm6                                    ;94.7
        cvtsi2ss  xmm6, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TDSPSTEP] ;94.35
        addss     xmm3, xmm7                                    ;94.7
        orps      xmm3, xmm5                                    ;94.7
        movss     xmm5, DWORD PTR [eax]                         ;94.35
        divss     xmm5, xmm4                                    ;94.35
        cvtss2si  ecx, xmm3                                     ;94.7
        addss     xmm6, xmm5                                    ;94.35
        movss     xmm5, DWORD PTR [_2il0floatpacket.10]         ;94.35
        andps     xmm5, xmm6                                    ;94.35
        pxor      xmm6, xmm5                                    ;94.35
        movaps    xmm7, xmm6                                    ;94.35
        movaps    xmm4, xmm6                                    ;94.35
        cmpltss   xmm7, xmm0                                    ;94.35
        andps     xmm0, xmm7                                    ;94.35
        addss     xmm4, xmm0                                    ;94.35
        subss     xmm4, xmm0                                    ;94.35
        movaps    xmm7, xmm4                                    ;94.35
        subss     xmm7, xmm6                                    ;94.35
        movaps    xmm6, xmm7                                    ;94.35
        cmpless   xmm7, DWORD PTR [_2il0floatpacket.13]         ;94.35
        cmpnless  xmm6, xmm1                                    ;94.35
        andps     xmm6, xmm2                                    ;94.35
        andps     xmm7, xmm2                                    ;94.35
        subss     xmm4, xmm6                                    ;94.35
        addss     xmm4, xmm7                                    ;94.35
        orps      xmm4, xmm5                                    ;94.35
        cvtss2si  edx, xmm4                                     ;94.35
        sub       ecx, edx                                      ;94.7
        jns       .B1.249       ; Prob 20%                      ;94.74
                                ; LOE
.B1.31:                         ; Preds .B1.30
        test      BYTE PTR [460+esp], 1                         ;94.87
        jne       .B1.249       ; Prob 20%                      ;94.87
                                ; LOE
.B1.32:                         ; Preds .B1.31
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE+32] ;102.9
        neg       edx                                           ;
        mov       eax, DWORD PTR [380+esp]                      ;97.7
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_JUSTVEH] ;95.7
        inc       eax                                           ;97.7
        mov       esi, DWORD PTR [_READ_VEHICLES$JTMP.0.1]      ;98.7
        mov       DWORD PTR [380+esp], eax                      ;97.7
        lea       ebx, DWORD PTR [1+ecx]                        ;95.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_JUSTVEH], ebx ;95.7
        mov       DWORD PTR [_READ_VEHICLES$IDARRAY.0.1-4+eax*4], esi ;98.7
        lea       ecx, DWORD PTR [1+edx+ecx]                    ;
        mov       DWORD PTR [_READ_VEHICLES$IDARRAY.0.1+3999996+eax*4], ebx ;99.7
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_RUNMODE] ;101.7
        shl       ecx, 6                                        ;
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE] ;102.9
        test      eax, -3                                       ;101.22
        jne       .B1.34        ; Prob 50%                      ;101.22
                                ; LOE eax edx ecx ebx esi
.B1.33:                         ; Preds .B1.32
        mov       DWORD PTR [4+edx+ecx], esi                    ;102.9
        jmp       .B1.39        ; Prob 100%                     ;102.9
                                ; LOE esi
.B1.34:                         ; Preds .B1.32
        cmp       eax, 4                                        ;105.9
        mov       DWORD PTR [4+edx+ecx], ebx                    ;104.9
        je        .B1.36        ; Prob 33%                      ;105.9
                                ; LOE eax ebx esi
.B1.35:                         ; Preds .B1.34
        cmp       eax, 3                                        ;105.9
        jne       .B1.39        ; Prob 50%                      ;105.9
                                ; LOE ebx esi
.B1.36:                         ; Preds .B1.34 .B1.35
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_MIXEDVEHGENMAP], 0 ;105.62
        jle       .B1.39        ; Prob 16%                      ;105.62
                                ; LOE ebx esi
.B1.37:                         ; Preds .B1.36
        mov       DWORD PTR [608+esp], 0                        ;106.12
        lea       edi, DWORD PTR [608+esp]                      ;106.12
        mov       DWORD PTR [528+esp], esi                      ;106.12
        lea       eax, DWORD PTR [528+esp]                      ;106.12
        push      32                                            ;106.12
        push      OFFSET FLAT: READ_VEHICLES$format_pack.0.1+256 ;106.12
        push      eax                                           ;106.12
        push      OFFSET FLAT: __STRLITPACK_81.0.1              ;106.12
        push      -2088435968                                   ;106.12
        push      9987                                          ;106.12
        push      edi                                           ;106.12
        call      _for_write_seq_fmt                            ;106.12
                                ; LOE ebx esi edi
.B1.38:                         ; Preds .B1.37
        mov       DWORD PTR [564+esp], ebx                      ;106.12
        lea       eax, DWORD PTR [564+esp]                      ;106.12
        push      eax                                           ;106.12
        push      OFFSET FLAT: __STRLITPACK_82.0.1              ;106.12
        push      edi                                           ;106.12
        call      _for_write_seq_fmt_xmit                       ;106.12
                                ; LOE esi
.B1.483:                        ; Preds .B1.38
        add       esp, 40                                       ;106.12
                                ; LOE esi
.B1.39:                         ; Preds .B1.35 .B1.36 .B1.483 .B1.33
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_MAXID] ;109.7
        cmp       esi, eax                                      ;109.15
        cmovge    eax, esi                                      ;109.15
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_MAXID], eax ;109.24
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_KEEPTYPE], 0 ;112.19
        jne       .B1.45        ; Prob 78%                      ;112.19
                                ; LOE
.B1.41:                         ; Preds .B1.39
        push      OFFSET FLAT: __NLITPACK_0.0.1                 ;113.13
        call      _RANXY                                        ;113.13
                                ; LOE f1
.B1.484:                        ; Preds .B1.41
        fstp      DWORD PTR [524+esp]                           ;113.13
        movss     xmm0, DWORD PTR [524+esp]                     ;113.13
        add       esp, 4                                        ;113.13
                                ; LOE xmm0
.B1.42:                         ; Preds .B1.484
        mov       ebx, 1                                        ;114.8
                                ; LOE ebx xmm0
.B1.43:                         ; Preds .B1.44 .B1.42
        movss     xmm1, DWORD PTR [_READ_VEHICLES$VEHTMP.0.1-4+ebx*4] ;115.18
        comiss    xmm1, xmm0                                    ;115.16
        ja        .B1.247       ; Prob 20%                      ;115.16
                                ; LOE ebx xmm0
.B1.44:                         ; Preds .B1.43
        inc       ebx                                           ;119.8
        cmp       ebx, 3                                        ;119.8
        jle       .B1.43        ; Prob 66%                      ;119.8
                                ; LOE ebx xmm0
.B1.45:                         ; Preds .B1.39 .B1.44
        mov       ebx, DWORD PTR [_READ_VEHICLES$IVCL2TMP.0.1]  ;122.7
                                ; LOE ebx
.B1.46:                         ; Preds .B1.247 .B1.45
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_KEEPCLS], 0 ;123.18
        jne       .B1.55        ; Prob 50%                      ;123.18
                                ; LOE ebx
.B1.47:                         ; Preds .B1.46
        push      OFFSET FLAT: __NLITPACK_0.0.1                 ;124.13
        call      _RANXY                                        ;124.13
                                ; LOE ebx f1
.B1.485:                        ; Preds .B1.47
        fstp      DWORD PTR [524+esp]                           ;124.13
        movss     xmm0, DWORD PTR [524+esp]                     ;124.13
        add       esp, 4                                        ;124.13
                                ; LOE ebx xmm0
.B1.48:                         ; Preds .B1.485
        mov       eax, 3                                        ;
        cmp       ebx, 6                                        ;
        cmove     ebx, eax                                      ;
        mov       edx, 1                                        ;
        sub       ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VEHPROFILE+32] ;
        shl       ebx, 2                                        ;
        lea       eax, DWORD PTR [ebx+ebx*8]                    ;
        add       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VEHPROFILE] ;
                                ; LOE eax edx xmm0
.B1.49:                         ; Preds .B1.50 .B1.48
        movss     xmm1, DWORD PTR [12+eax+edx*4]                ;126.18
        comiss    xmm1, xmm0                                    ;126.16
        ja        .B1.154       ; Prob 20%                      ;126.16
                                ; LOE eax edx xmm0
.B1.50:                         ; Preds .B1.49
        inc       edx                                           ;130.8
        cmp       edx, 5                                        ;130.8
        jle       .B1.49        ; Prob 80%                      ;130.8
                                ; LOE eax edx xmm0
.B1.51:                         ; Preds .B1.50
        mov       edx, DWORD PTR [_READ_VEHICLES$IVCLTMP.0.1]   ;131.8
                                ; LOE edx
.B1.52:                         ; Preds .B1.154 .B1.51
        cmp       edx, 4                                        ;131.19
        je        .B1.54        ; Prob 16%                      ;131.19
                                ; LOE edx
.B1.53:                         ; Preds .B1.52
        mov       ebx, DWORD PTR [_READ_VEHICLES$IVCL2TMP.0.1]  ;134.33
        jmp       .B1.56        ; Prob 100%                     ;134.33
                                ; LOE edx ebx
.B1.54:                         ; Preds .B1.52
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_IER_OK], 1  ;131.25
        mov       ebx, DWORD PTR [_READ_VEHICLES$IVCL2TMP.0.1]  ;134.33
        jmp       .B1.56        ; Prob 100%                     ;134.33
                                ; LOE edx ebx
.B1.55:                         ; Preds .B1.46
        mov       edx, DWORD PTR [_READ_VEHICLES$IVCLTMP.0.1]   ;134.33
                                ; LOE edx ebx
.B1.56:                         ; Preds .B1.54 .B1.53 .B1.55
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_READVEH+44] ;134.33
        neg       esi                                           ;134.7
        add       esi, edx                                      ;134.7
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_READVEH+32] ;134.7
        neg       edx                                           ;134.7
        add       edx, ebx                                      ;134.7
        imul      esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_READVEH+40] ;134.7
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_READVEH] ;134.7
        lea       ecx, DWORD PTR [eax+edx*4]                    ;134.7
        inc       DWORD PTR [ecx+esi]                           ;134.7
        cmp       ebx, 2                                        ;136.7
        jne       .B1.58        ; Prob 67%                      ;136.7
                                ; LOE ebx
.B1.57:                         ; Preds .B1.56
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRUCKOK], -1 ;137.9
        jmp       .B1.60        ; Prob 100%                     ;137.9
                                ; LOE
.B1.58:                         ; Preds .B1.56
        cmp       ebx, 3                                        ;136.7
        jne       .B1.60        ; Prob 50%                      ;136.7
                                ; LOE
.B1.59:                         ; Preds .B1.58
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_HOVOK], -1  ;139.9
                                ; LOE
.B1.60:                         ; Preds .B1.58 .B1.57 .B1.59
        mov       esi, DWORD PTR [_READ_VEHICLES$JTMP.0.1]      ;142.7
        test      esi, esi                                      ;142.15
        jne       .B1.65        ; Prob 50%                      ;142.15
                                ; LOE esi
.B1.61:                         ; Preds .B1.60
        mov       DWORD PTR [608+esp], 0                        ;143.6
        lea       ebx, DWORD PTR [608+esp]                      ;143.6
        mov       DWORD PTR [472+esp], 20                       ;143.6
        lea       eax, DWORD PTR [472+esp]                      ;143.6
        mov       DWORD PTR [476+esp], OFFSET FLAT: __STRLITPACK_61 ;143.6
        push      32                                            ;143.6
        push      eax                                           ;143.6
        push      OFFSET FLAT: __STRLITPACK_83.0.1              ;143.6
        push      -2088435968                                   ;143.6
        push      911                                           ;143.6
        push      ebx                                           ;143.6
        call      _for_write_seq_lis                            ;143.6
                                ; LOE ebx esi
.B1.62:                         ; Preds .B1.61
        mov       DWORD PTR [632+esp], 0                        ;144.6
        lea       eax, DWORD PTR [504+esp]                      ;144.6
        mov       DWORD PTR [504+esp], 55                       ;144.6
        mov       DWORD PTR [508+esp], OFFSET FLAT: __STRLITPACK_59 ;144.6
        push      32                                            ;144.6
        push      eax                                           ;144.6
        push      OFFSET FLAT: __STRLITPACK_84.0.1              ;144.6
        push      -2088435968                                   ;144.6
        push      911                                           ;144.6
        push      ebx                                           ;144.6
        call      _for_write_seq_lis                            ;144.6
                                ; LOE ebx esi
.B1.63:                         ; Preds .B1.62
        mov       DWORD PTR [656+esp], 0                        ;145.9
        lea       eax, DWORD PTR [544+esp]                      ;145.9
        mov       DWORD PTR [544+esp], 27                       ;145.9
        mov       DWORD PTR [548+esp], OFFSET FLAT: __STRLITPACK_57 ;145.9
        push      32                                            ;145.9
        push      eax                                           ;145.9
        push      OFFSET FLAT: __STRLITPACK_85.0.1              ;145.9
        push      -2088435968                                   ;145.9
        push      911                                           ;145.9
        push      ebx                                           ;145.9
        call      _for_write_seq_lis                            ;145.9
                                ; LOE esi
.B1.64:                         ; Preds .B1.63
        push      32                                            ;146.3
        xor       eax, eax                                      ;146.3
        push      eax                                           ;146.3
        push      eax                                           ;146.3
        push      -2088435968                                   ;146.3
        push      eax                                           ;146.3
        push      OFFSET FLAT: __STRLITPACK_86                  ;146.3
        call      _for_stop_core                                ;146.3
                                ; LOE esi
.B1.486:                        ; Preds .B1.64
        add       esp, 96                                       ;146.3
                                ; LOE esi
.B1.65:                         ; Preds .B1.60 .B1.486
        mov       ebx, DWORD PTR [_READ_VEHICLES$NDESTMP.0.1]   ;148.4
        cmp       ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFSTOPS] ;148.15
        jle       .B1.71        ; Prob 50%                      ;148.15
                                ; LOE ebx esi
.B1.66:                         ; Preds .B1.65
        mov       DWORD PTR [608+esp], 0                        ;149.6
        lea       edi, DWORD PTR [608+esp]                      ;149.6
        mov       DWORD PTR [488+esp], 22                       ;149.6
        lea       eax, DWORD PTR [488+esp]                      ;149.6
        mov       DWORD PTR [492+esp], OFFSET FLAT: __STRLITPACK_55 ;149.6
        push      32                                            ;149.6
        push      eax                                           ;149.6
        push      OFFSET FLAT: __STRLITPACK_87.0.1              ;149.6
        push      -2088435968                                   ;149.6
        push      911                                           ;149.6
        push      edi                                           ;149.6
        call      _for_write_seq_lis                            ;149.6
                                ; LOE ebx esi edi
.B1.67:                         ; Preds .B1.66
        mov       DWORD PTR [632+esp], 0                        ;150.6
        lea       eax, DWORD PTR [528+esp]                      ;150.6
        mov       DWORD PTR [528+esp], 39                       ;150.6
        mov       DWORD PTR [532+esp], OFFSET FLAT: __STRLITPACK_53 ;150.6
        push      32                                            ;150.6
        push      eax                                           ;150.6
        push      OFFSET FLAT: __STRLITPACK_88.0.1              ;150.6
        push      -2088435968                                   ;150.6
        push      911                                           ;150.6
        push      edi                                           ;150.6
        call      _for_write_seq_lis                            ;150.6
                                ; LOE ebx esi edi
.B1.68:                         ; Preds .B1.67
        mov       DWORD PTR [656+esp], 0                        ;151.6
        lea       eax, DWORD PTR [560+esp]                      ;151.6
        mov       DWORD PTR [560+esp], 25                       ;151.6
        mov       DWORD PTR [564+esp], OFFSET FLAT: __STRLITPACK_51 ;151.6
        push      32                                            ;151.6
        push      eax                                           ;151.6
        push      OFFSET FLAT: __STRLITPACK_89.0.1              ;151.6
        push      -2088435968                                   ;151.6
        push      911                                           ;151.6
        push      edi                                           ;151.6
        call      _for_write_seq_lis                            ;151.6
                                ; LOE ebx esi edi
.B1.69:                         ; Preds .B1.68
        mov       DWORD PTR [648+esp], esi                      ;151.6
        lea       eax, DWORD PTR [648+esp]                      ;151.6
        push      eax                                           ;151.6
        push      OFFSET FLAT: __STRLITPACK_90.0.1              ;151.6
        push      edi                                           ;151.6
        call      _for_write_seq_lis_xmit                       ;151.6
                                ; LOE ebx
.B1.70:                         ; Preds .B1.69
        push      32                                            ;152.6
        xor       eax, eax                                      ;152.6
        push      eax                                           ;152.6
        push      eax                                           ;152.6
        push      -2088435968                                   ;152.6
        push      eax                                           ;152.6
        push      OFFSET FLAT: __STRLITPACK_91                  ;152.6
        call      _for_stop_core                                ;152.6
                                ; LOE ebx
.B1.487:                        ; Preds .B1.70
        add       esp, 108                                      ;152.6
                                ; LOE ebx
.B1.71:                         ; Preds .B1.65 .B1.487
        test      ebx, ebx                                      ;154.4
        jle       .B1.84        ; Prob 2%                       ;154.4
                                ; LOE ebx
.B1.72:                         ; Preds .B1.71
        mov       eax, 1                                        ;154.4
        cmp       ebx, 1                                        ;154.4
        mov       DWORD PTR [268+esp], ebx                      ;
        cmovb     eax, ebx                                      ;154.4
        xor       edi, edi                                      ;154.4
        lea       esi, DWORD PTR [608+esp]                      ;
        mov       DWORD PTR [540+esp], eax                      ;
        lea       ebx, DWORD PTR [600+esp]                      ;
                                ; LOE ebx esi edi
.B1.73:                         ; Preds .B1.75 .B1.72
        mov       DWORD PTR [608+esp], 0                        ;155.11
        lea       eax, DWORD PTR [_READ_VEHICLES$JDEST_TMP.0.1+edi*4] ;155.11
        mov       DWORD PTR [600+esp], 4                        ;155.11
        mov       DWORD PTR [604+esp], eax                      ;155.11
        push      32                                            ;155.11
        push      OFFSET FLAT: READ_VEHICLES$format_pack.0.1+88 ;155.11
        push      ebx                                           ;155.11
        push      OFFSET FLAT: __STRLITPACK_92.0.1              ;155.11
        push      -2088435968                                   ;155.11
        push      500                                           ;155.11
        push      esi                                           ;155.11
        call      _for_read_seq_fmt                             ;155.11
                                ; LOE ebx esi edi
.B1.74:                         ; Preds .B1.73
        mov       DWORD PTR [668+esp], 4                        ;155.11
        lea       edx, DWORD PTR [668+esp]                      ;155.11
        lea       eax, DWORD PTR [_READ_VEHICLES$WAIT_TMP.0.1+edi*4] ;155.11
        mov       DWORD PTR [672+esp], eax                      ;155.11
        push      edx                                           ;155.11
        push      OFFSET FLAT: __STRLITPACK_93.0.1              ;155.11
        push      esi                                           ;155.11
        call      _for_read_seq_fmt_xmit                        ;155.11
                                ; LOE ebx esi edi
.B1.488:                        ; Preds .B1.74
        add       esp, 40                                       ;155.11
                                ; LOE ebx esi edi
.B1.75:                         ; Preds .B1.488
        inc       edi                                           ;154.4
        cmp       edi, DWORD PTR [540+esp]                      ;154.4
        jb        .B1.73        ; Prob 99%                      ;154.4
                                ; LOE ebx esi edi
.B1.76:                         ; Preds .B1.75
        mov       ebx, DWORD PTR [268+esp]                      ;
        lea       edi, DWORD PTR [-1+ebx]                       ;154.4
        cmp       ebx, 2                                        ;154.4
        jb        .B1.83        ; Prob 0%                       ;154.4
                                ; LOE esi edi
.B1.77:                         ; Preds .B1.76
        xor       ebx, ebx                                      ;
                                ; LOE ebx esi edi
.B1.78:                         ; Preds .B1.81 .B1.77
        mov       DWORD PTR [608+esp], 0                        ;155.11
        lea       eax, DWORD PTR [_READ_VEHICLES$JDEST_TMP.0.1+4+ebx*4] ;155.11
        mov       DWORD PTR [600+esp], 4                        ;155.11
        mov       DWORD PTR [604+esp], eax                      ;155.11
        push      32                                            ;155.11
        push      OFFSET FLAT: READ_VEHICLES$format_pack.0.1+88 ;155.11
        lea       edx, DWORD PTR [608+esp]                      ;155.11
        push      edx                                           ;155.11
        push      OFFSET FLAT: __STRLITPACK_92.0.1              ;155.11
        push      -2088435968                                   ;155.11
        push      500                                           ;155.11
        push      esi                                           ;155.11
        call      _for_read_seq_fmt                             ;155.11
                                ; LOE ebx esi edi
.B1.79:                         ; Preds .B1.78
        mov       DWORD PTR [668+esp], 4                        ;155.11
        lea       edx, DWORD PTR [668+esp]                      ;155.11
        lea       eax, DWORD PTR [_READ_VEHICLES$WAIT_TMP.0.1+4+ebx*4] ;155.11
        mov       DWORD PTR [672+esp], eax                      ;155.11
        push      edx                                           ;155.11
        push      OFFSET FLAT: __STRLITPACK_93.0.1              ;155.11
        push      esi                                           ;155.11
        call      _for_read_seq_fmt_xmit                        ;155.11
                                ; LOE ebx esi edi
.B1.489:                        ; Preds .B1.79
        add       esp, 40                                       ;155.11
                                ; LOE ebx esi edi
.B1.80:                         ; Preds .B1.489
        mov       eax, DWORD PTR [_READ_VEHICLES$JDEST_TMP.0.1+4+ebx*4] ;158.16
        cmp       eax, DWORD PTR [_READ_VEHICLES$JDEST_TMP.0.1+ebx*4] ;158.32
        je        .B1.244       ; Prob 5%                       ;158.32
                                ; LOE ebx esi edi
.B1.81:                         ; Preds .B1.80 .B1.513
        inc       ebx                                           ;154.4
        cmp       ebx, edi                                      ;154.4
        jb        .B1.78        ; Prob 99%                      ;154.4
                                ; LOE ebx esi edi
.B1.83:                         ; Preds .B1.81 .B1.76
        mov       ebx, DWORD PTR [_READ_VEHICLES$NDESTMP.0.1]   ;188.37
                                ; LOE ebx
.B1.84:                         ; Preds .B1.83 .B1.71
        inc       DWORD PTR [_DYNUST_MAIN_MODULE_mp_JRESTORE]   ;166.4
        inc       DWORD PTR [_DYNUST_MAIN_MODULE_mp_IREAD_VEH_COUNT] ;167.7
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_RUNMODE] ;171.7
        cmp       eax, 3                                        ;171.7
        je        .B1.86        ; Prob 33%                      ;171.7
                                ; LOE eax ebx
.B1.85:                         ; Preds .B1.84
        test      eax, eax                                      ;171.7
        jne       .B1.92        ; Prob 50%                      ;171.7
                                ; LOE ebx
.B1.86:                         ; Preds .B1.84 .B1.85
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_IVCT]  ;172.9
        inc       esi                                           ;172.9
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_IVCT], esi  ;172.9
        cmp       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TMPARYSIZE] ;173.17
        jle       .B1.91        ; Prob 50%                      ;173.17
                                ; LOE ebx esi
.B1.87:                         ; Preds .B1.86
        mov       DWORD PTR [608+esp], 0                        ;174.9
        lea       edi, DWORD PTR [608+esp]                      ;174.9
        mov       DWORD PTR [312+esp], 44                       ;174.9
        lea       eax, DWORD PTR [312+esp]                      ;174.9
        mov       DWORD PTR [316+esp], OFFSET FLAT: __STRLITPACK_45 ;174.9
        push      32                                            ;174.9
        push      eax                                           ;174.9
        push      OFFSET FLAT: __STRLITPACK_97.0.1              ;174.9
        push      -2088435968                                   ;174.9
        push      911                                           ;174.9
        push      edi                                           ;174.9
        call      _for_write_seq_lis                            ;174.9
                                ; LOE ebx esi edi
.B1.88:                         ; Preds .B1.87
        mov       DWORD PTR [632+esp], 0                        ;175.12
        lea       eax, DWORD PTR [344+esp]                      ;175.12
        mov       DWORD PTR [344+esp], 83                       ;175.12
        mov       DWORD PTR [348+esp], OFFSET FLAT: __STRLITPACK_43 ;175.12
        push      32                                            ;175.12
        push      eax                                           ;175.12
        push      OFFSET FLAT: __STRLITPACK_98.0.1              ;175.12
        push      -2088435968                                   ;175.12
        push      911                                           ;175.12
        push      edi                                           ;175.12
        call      _for_write_seq_lis                            ;175.12
                                ; LOE ebx esi edi
.B1.89:                         ; Preds .B1.88
        mov       DWORD PTR [656+esp], 0                        ;176.9
        lea       eax, DWORD PTR [376+esp]                      ;176.9
        mov       DWORD PTR [376+esp], 40                       ;176.9
        mov       DWORD PTR [380+esp], OFFSET FLAT: __STRLITPACK_41 ;176.9
        push      32                                            ;176.9
        push      eax                                           ;176.9
        push      OFFSET FLAT: __STRLITPACK_99.0.1              ;176.9
        push      -2088435968                                   ;176.9
        push      911                                           ;176.9
        push      edi                                           ;176.9
        call      _for_write_seq_lis                            ;176.9
                                ; LOE ebx esi
.B1.90:                         ; Preds .B1.89
        push      32                                            ;177.9
        xor       eax, eax                                      ;177.9
        push      eax                                           ;177.9
        push      eax                                           ;177.9
        push      -2088435968                                   ;177.9
        push      eax                                           ;177.9
        push      OFFSET FLAT: __STRLITPACK_100                 ;177.9
        call      _for_stop_core                                ;177.9
                                ; LOE ebx esi
.B1.490:                        ; Preds .B1.90
        add       esp, 96                                       ;177.9
                                ; LOE ebx esi
.B1.91:                         ; Preds .B1.86 .B1.490
        mov       edx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLODTMP+40] ;180.9
        mov       ecx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLODTMP+44] ;180.9
        imul      ecx, edx                                      ;180.9
        mov       edi, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLODTMP] ;180.9
        sub       edi, ecx                                      ;180.9
        sub       esi, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLODTMP+32] ;180.9
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_JUSTVEH] ;180.9
        mov       ecx, DWORD PTR [_READ_VEHICLES$JDEST_TMP.0.1] ;181.9
        lea       esi, DWORD PTR [edi+esi*4]                    ;180.9
        mov       edi, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLODZONE+32] ;182.9
        neg       edi                                           ;182.9
        add       edi, ecx                                      ;182.9
        mov       DWORD PTR [esi+edx*2], eax                    ;180.9
        mov       DWORD PTR [esi+edx], ecx                      ;181.9
        mov       edx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLODZONE] ;182.9
        mov       DWORD PTR [edx+edi*4], -1                     ;182.9
        jmp       .B1.93        ; Prob 100%                     ;182.9
                                ; LOE eax ebx
.B1.92:                         ; Preds .B1.85
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_JUSTVEH] ;186.7
                                ; LOE eax ebx
.B1.93:                         ; Preds .B1.91 .B1.92
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;188.37
        neg       edx                                           ;188.7
        add       edx, eax                                      ;188.7
        shl       edx, 8                                        ;188.7
        mov       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;188.37
        test      ebx, ebx                                      ;189.7
        mov       DWORD PTR [_READ_VEHICLES$JM.0.1], eax        ;186.7
        mov       DWORD PTR [340+esp], ecx                      ;188.37
        mov       BYTE PTR [160+ecx+edx], bl                    ;188.7
        jle       .B1.102       ; Prob 0%                       ;189.7
                                ; LOE eax edx ebx
.B1.94:                         ; Preds .B1.93
        mov       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE] ;190.52
        cmp       ebx, 16                                       ;189.7
        mov       esi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE+32] ;190.52
        mov       DWORD PTR [308+esp], ecx                      ;190.52
        mov       DWORD PTR [300+esp], esi                      ;190.52
        jl        .B1.243       ; Prob 10%                      ;189.7
                                ; LOE eax edx ebx
.B1.95:                         ; Preds .B1.94
        mov       esi, DWORD PTR [340+esp]                      ;191.13
        mov       ecx, ebx                                      ;189.7
        and       ecx, -16                                      ;189.7
        mov       DWORD PTR [444+esp], ecx                      ;189.7
        mov       edi, DWORD PTR [232+esi+edx]                  ;191.13
        add       edi, edi                                      ;
        neg       edi                                           ;
        add       edi, DWORD PTR [200+esi+edx]                  ;
        mov       esi, eax                                      ;
        sub       esi, DWORD PTR [300+esp]                      ;
        shl       esi, 6                                        ;
        mov       ecx, DWORD PTR [308+esp]                      ;190.13
        mov       DWORD PTR [356+esp], edi                      ;
        mov       DWORD PTR [348+esp], 0                        ;
        mov       edi, DWORD PTR [48+ecx+esi]                   ;190.13
        add       edi, edi                                      ;
        neg       edi                                           ;
        add       edi, DWORD PTR [16+ecx+esi]                   ;
        mov       DWORD PTR [268+esp], ebx                      ;
        movaps    xmm0, XMMWORD PTR [_2il0floatpacket.6]        ;
        mov       ecx, DWORD PTR [444+esp]                      ;
        mov       ebx, edi                                      ;
        mov       esi, DWORD PTR [356+esp]                      ;
        mov       edi, DWORD PTR [348+esp]                      ;
                                ; LOE eax edx ecx ebx esi edi xmm0
.B1.96:                         ; Preds .B1.96 .B1.95
        movaps    xmm1, XMMWORD PTR [_READ_VEHICLES$WAIT_TMP.0.1+edi*4] ;191.51
        movaps    xmm2, XMMWORD PTR [_READ_VEHICLES$WAIT_TMP.0.1+16+edi*4] ;191.51
        mulps     xmm1, xmm0                                    ;191.65
        mulps     xmm2, xmm0                                    ;191.65
        cvttps2dq xmm4, xmm1                                    ;191.13
        cvttps2dq xmm3, xmm2                                    ;191.13
        movaps    xmm7, XMMWORD PTR [_READ_VEHICLES$WAIT_TMP.0.1+32+edi*4] ;191.51
        pslld     xmm4, 16                                      ;191.13
        movaps    xmm1, XMMWORD PTR [_READ_VEHICLES$WAIT_TMP.0.1+48+edi*4] ;191.51
        pslld     xmm3, 16                                      ;191.13
        mulps     xmm7, xmm0                                    ;191.65
        psrad     xmm4, 16                                      ;191.13
        mulps     xmm1, xmm0                                    ;191.65
        psrad     xmm3, 16                                      ;191.13
        packssdw  xmm4, xmm3                                    ;191.13
        cvttps2dq xmm3, xmm7                                    ;191.13
        cvttps2dq xmm2, xmm1                                    ;191.13
        movdqu    XMMWORD PTR [2+esi+edi*2], xmm4               ;191.13
        pslld     xmm3, 16                                      ;191.13
        pslld     xmm2, 16                                      ;191.13
        movdqa    xmm6, XMMWORD PTR [_READ_VEHICLES$JDEST_TMP.0.1+edi*4] ;190.13
        psrad     xmm3, 16                                      ;191.13
        movdqa    xmm5, XMMWORD PTR [_READ_VEHICLES$JDEST_TMP.0.1+16+edi*4] ;190.13
        pslld     xmm6, 16                                      ;190.13
        pslld     xmm5, 16                                      ;190.13
        psrad     xmm6, 16                                      ;190.13
        psrad     xmm5, 16                                      ;190.13
        psrad     xmm2, 16                                      ;191.13
        movdqa    xmm4, XMMWORD PTR [_READ_VEHICLES$JDEST_TMP.0.1+48+edi*4] ;190.13
        packssdw  xmm6, xmm5                                    ;190.13
        pslld     xmm4, 16                                      ;190.13
        movdqa    xmm5, XMMWORD PTR [_READ_VEHICLES$JDEST_TMP.0.1+32+edi*4] ;190.13
        psrad     xmm4, 16                                      ;190.13
        pslld     xmm5, 16                                      ;190.13
        psrad     xmm5, 16                                      ;190.13
        packssdw  xmm3, xmm2                                    ;191.13
        packssdw  xmm5, xmm4                                    ;190.13
        movdqu    XMMWORD PTR [2+ebx+edi*2], xmm6               ;190.13
        movdqu    XMMWORD PTR [18+esi+edi*2], xmm3              ;191.13
        movdqu    XMMWORD PTR [18+ebx+edi*2], xmm5              ;190.13
        add       edi, 16                                       ;189.7
        cmp       edi, ecx                                      ;189.7
        jb        .B1.96        ; Prob 99%                      ;189.7
                                ; LOE eax edx ecx ebx esi edi xmm0
.B1.97:                         ; Preds .B1.96
        mov       DWORD PTR [444+esp], ecx                      ;
        mov       ebx, DWORD PTR [268+esp]                      ;
                                ; LOE eax edx ebx
.B1.98:                         ; Preds .B1.97 .B1.243
        cmp       ebx, DWORD PTR [444+esp]                      ;189.7
        jbe       .B1.102       ; Prob 3%                       ;189.7
                                ; LOE eax edx ebx
.B1.99:                         ; Preds .B1.98
        mov       esi, DWORD PTR [340+esp]                      ;191.13
        sub       eax, DWORD PTR [300+esp]                      ;
        mov       edi, DWORD PTR [308+esp]                      ;190.13
        shl       eax, 6                                        ;
        mov       ecx, DWORD PTR [232+esi+edx]                  ;191.13
        add       ecx, ecx                                      ;
        neg       ecx                                           ;
        add       ecx, DWORD PTR [200+esi+edx]                  ;
        mov       edx, DWORD PTR [48+edi+eax]                   ;190.13
        add       edx, edx                                      ;
        neg       edx                                           ;
        add       edx, DWORD PTR [16+edi+eax]                   ;
        movss     xmm1, DWORD PTR [_2il0floatpacket.7]          ;
        mov       edi, DWORD PTR [444+esp]                      ;
                                ; LOE edx ecx ebx edi xmm1
.B1.100:                        ; Preds .B1.100 .B1.99
        movss     xmm0, DWORD PTR [_READ_VEHICLES$WAIT_TMP.0.1+edi*4] ;191.51
        mulss     xmm0, xmm1                                    ;191.65
        cvttss2si eax, xmm0                                     ;191.13
        mov       esi, DWORD PTR [_READ_VEHICLES$JDEST_TMP.0.1+edi*4] ;190.52
        mov       WORD PTR [2+ecx+edi*2], ax                    ;191.13
        mov       WORD PTR [2+edx+edi*2], si                    ;190.13
        inc       edi                                           ;189.7
        cmp       edi, ebx                                      ;189.7
        jb        .B1.100       ; Prob 99%                      ;189.7
                                ; LOE edx ecx ebx edi xmm1
.B1.102:                        ; Preds .B1.100 .B1.93 .B1.98
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM] ;193.11
        mov       ebx, DWORD PTR [_READ_VEHICLES$IUTMP.0.1]     ;193.14
        mov       DWORD PTR [412+esp], ebx                      ;193.14
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM+32] ;193.11
        shl       edi, 2                                        ;193.39
        lea       ebx, DWORD PTR [esi+ebx*4]                    ;193.39
        mov       edx, ebx                                      ;193.39
        sub       edx, edi                                      ;193.39
        mov       eax, DWORD PTR [_READ_VEHICLES$IDTMP.0.1]     ;196.13
        mov       DWORD PTR [452+esp], eax                      ;196.13
        cmp       DWORD PTR [edx], 0                            ;193.36
        lea       ecx, DWORD PTR [eax*4]                        ;199.55
        jle       .B1.236       ; Prob 16%                      ;193.36
                                ; LOE ecx ebx esi edi
.B1.103:                        ; Preds .B1.102
        mov       eax, esi                                      ;193.39
        sub       eax, edi                                      ;193.39
        cmp       DWORD PTR [eax+ecx], 0                        ;193.65
        jle       .B1.236       ; Prob 16%                      ;193.65
                                ; LOE ebx esi edi
.B1.104:                        ; Preds .B1.103 .B1.512
        mov       eax, DWORD PTR [452+esp]                      ;199.16
        sub       ebx, edi                                      ;199.16
        push      OFFSET FLAT: __NLITPACK_0.0.1                 ;199.16
        lea       edx, DWORD PTR [esi+eax*4]                    ;199.16
        sub       edx, edi                                      ;199.16
        push      edx                                           ;199.16
        push      ebx                                           ;199.16
        call      _DYNUST_NETWORK_MODULE_mp_GETFLINKFROMNODE    ;199.16
                                ; LOE eax
.B1.491:                        ; Preds .B1.104
        add       esp, 12                                       ;199.16
        mov       ebx, eax                                      ;199.16
                                ; LOE ebx
.B1.105:                        ; Preds .B1.491
        test      ebx, ebx                                      ;200.19
        jle       .B1.231       ; Prob 16%                      ;200.19
                                ; LOE ebx
.B1.106:                        ; Preds .B1.510 .B1.105
        mov       eax, DWORD PTR [_READ_VEHICLES$JM.0.1]        ;205.11
        mov       edi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;205.11
        mov       DWORD PTR [388+esp], eax                      ;205.11
        sub       eax, edi                                      ;205.11
        shl       eax, 5                                        ;205.11
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;205.11
        mov       DWORD PTR [364+esp], eax                      ;205.11
        mov       DWORD PTR [372+esp], edx                      ;205.11
        mov       DWORD PTR [28+edx+eax], ebx                   ;205.11
        mov       eax, DWORD PTR [_READ_VEHICLES$IZONETMP.0.1]  ;207.11
        test      eax, eax                                      ;207.23
        jne       .B1.108       ; Prob 50%                      ;207.23
                                ; LOE eax edx ebx edi dl dh
.B1.107:                        ; Preds .B1.106
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;208.13
        neg       eax                                           ;208.13
        add       eax, ebx                                      ;208.13
        imul      ecx, eax, 900                                 ;208.13
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;208.13
        mov       eax, DWORD PTR [372+esp]                      ;208.13
        mov       esi, DWORD PTR [740+edx+ecx]                  ;208.45
        add       esi, esi                                      ;208.13
        neg       esi                                           ;208.13
        add       esi, DWORD PTR [708+edx+ecx]                  ;208.13
        mov       edx, DWORD PTR [364+esp]                      ;208.13
        mov       DWORD PTR [228+esp], esi                      ;208.13
        movsx     ecx, WORD PTR [4+esi]                         ;208.13
        test      ecx, ecx                                      ;209.46
        mov       WORD PTR [22+eax+edx], cx                     ;208.13
        jle       .B1.155       ; Prob 16%                      ;209.46
        jmp       .B1.109       ; Prob 100%                     ;209.46
                                ; LOE ebx edi
.B1.108:                        ; Preds .B1.106
        mov       ecx, DWORD PTR [364+esp]                      ;217.13
        mov       WORD PTR [22+edx+ecx], ax                     ;217.13
                                ; LOE edi
.B1.109:                        ; Preds .B1.497 .B1.107 .B1.108
        mov       edx, DWORD PTR [388+esp]                      ;220.11
        mov       eax, edx                                      ;220.11
        mov       ebx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;220.38
        sub       eax, ebx                                      ;220.11
        shl       eax, 8                                        ;220.11
        mov       esi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;220.38
        mov       ecx, DWORD PTR [_READ_VEHICLES$IEVAC.0.1]     ;220.38
        mov       DWORD PTR [396+esp], esi                      ;220.38
        mov       BYTE PTR [24+esi+eax], cl                     ;220.11
        mov       ecx, DWORD PTR [364+esp]                      ;222.11
        mov       esi, DWORD PTR [372+esp]                      ;222.11
        mov       eax, DWORD PTR [_READ_VEHICLES$IVCLTMP.0.1]   ;222.44
        mov       BYTE PTR [4+esi+ecx], al                      ;222.11
        mov       esi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE] ;225.14
        mov       DWORD PTR [404+esp], esi                      ;225.14
        mov       esi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE+32] ;225.14
        sub       edx, esi                                      ;
        shl       edx, 6                                        ;
        mov       DWORD PTR [436+esp], edx                      ;
        cmp       al, 4                                         ;224.47
        je        .B1.230       ; Prob 16%                      ;224.47
                                ; LOE edx ebx esi edi dl dh
.B1.110:                        ; Preds .B1.109
        mov       edx, DWORD PTR [404+esp]                      ;229.14
        mov       ecx, DWORD PTR [436+esp]                      ;229.14
        mov       eax, DWORD PTR [_READ_VEHICLES$INFOTMP.0.1]   ;229.42
        mov       BYTE PTR [56+edx+ecx], al                     ;229.14
        mov       eax, DWORD PTR [_READ_VEHICLES$RIBFTMP.0.1]   ;230.8
        mov       DWORD PTR [60+edx+ecx], eax                   ;230.8
        mov       eax, DWORD PTR [_READ_VEHICLES$COMPTMP.0.1]   ;231.8
        mov       DWORD PTR [52+edx+ecx], eax                   ;231.8
                                ; LOE ebx esi edi
.B1.111:                        ; Preds .B1.230 .B1.110
        mov       ecx, DWORD PTR [364+esp]                      ;234.5
        mov       edx, DWORD PTR [372+esp]                      ;234.5
        mov       eax, DWORD PTR [_READ_VEHICLES$IVCL2TMP.0.1]  ;234.37
        cmp       eax, 6                                        ;236.17
        mov       DWORD PTR [420+esp], eax                      ;234.37
        mov       BYTE PTR [5+edx+ecx], al                      ;234.5
        je        .B1.225       ; Prob 16%                      ;236.17
                                ; LOE ebx esi edi
.B1.112:                        ; Preds .B1.111
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_FUELOUT], 0 ;243.22
        jle       .B1.117       ; Prob 16%                      ;243.22
                                ; LOE ebx esi edi
.B1.113:                        ; Preds .B1.112 .B1.228
        movss     xmm0, DWORD PTR [_2il0floatpacket.2]          ;244.23
        mov       eax, DWORD PTR [_READ_VEHICLES$GASTMP.0.1]    ;244.13
        comiss    xmm0, DWORD PTR [_READ_VEHICLES$GASTMP.0.1]   ;244.23
        ja        .B1.115       ; Prob 22%                      ;244.23
                                ; LOE eax ebx esi edi al ah
.B1.114:                        ; Preds .B1.113
        mov       edx, DWORD PTR [_DYNUST_FUEL_MODULE_mp_VEHFUEL+32] ;247.15
        neg       edx                                           ;247.15
        add       edx, DWORD PTR [388+esp]                      ;247.15
        mov       ecx, eax                                      ;247.15
        lea       eax, DWORD PTR [edx*8]                        ;247.15
        lea       edx, DWORD PTR [eax+edx*4]                    ;247.15
        mov       eax, DWORD PTR [_DYNUST_FUEL_MODULE_mp_VEHFUEL] ;247.15
        mov       DWORD PTR [eax+edx], ecx                      ;247.15
        mov       DWORD PTR [4+eax+edx], ecx                    ;248.15
        jmp       .B1.117       ; Prob 100%                     ;248.15
                                ; LOE ebx esi edi
.B1.115:                        ; Preds .B1.113
        push      OFFSET FLAT: _READ_VEHICLES$JM.0.1            ;245.20
        call      _DYNUST_FUEL_MODULE_mp_SETFUELLEVEL           ;245.20
                                ; LOE
.B1.492:                        ; Preds .B1.115
        add       esp, 4                                        ;245.20
                                ; LOE
.B1.116:                        ; Preds .B1.492
        mov       esi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE] ;258.11
        mov       ebx, DWORD PTR [_READ_VEHICLES$IVCL2TMP.0.1]  ;252.5
        mov       eax, DWORD PTR [_READ_VEHICLES$JM.0.1]        ;253.7
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;253.32
        mov       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;257.11
        mov       DWORD PTR [404+esp], esi                      ;258.11
        mov       DWORD PTR [420+esp], ebx                      ;252.5
        mov       esi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE+32] ;258.11
        mov       DWORD PTR [388+esp], eax                      ;253.7
        mov       DWORD PTR [396+esp], edx                      ;253.32
        mov       ebx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;253.32
        mov       DWORD PTR [372+esp], ecx                      ;257.11
        mov       edi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;257.11
                                ; LOE ebx esi edi
.B1.117:                        ; Preds .B1.116 .B1.114 .B1.112
        cmp       DWORD PTR [420+esp], 5                        ;252.5
        je        .B1.119       ; Prob 33%                      ;252.5
                                ; LOE ebx esi edi
.B1.118:                        ; Preds .B1.117
        cmp       DWORD PTR [420+esp], 2                        ;252.5
        jne       .B1.120       ; Prob 50%                      ;252.5
                                ; LOE ebx esi edi
.B1.119:                        ; Preds .B1.117 .B1.118
        neg       ebx                                           ;253.7
        add       ebx, DWORD PTR [388+esp]                      ;253.7
        shl       ebx, 8                                        ;253.7
        add       ebx, DWORD PTR [396+esp]                      ;253.7
        movss     xmm0, DWORD PTR [236+esp]                     ;253.7
        movss     DWORD PTR [156+ebx], xmm0                     ;253.7
        jmp       .B1.121       ; Prob 100%                     ;253.7
                                ; LOE ebx esi edi
.B1.120:                        ; Preds .B1.228 .B1.118
        neg       ebx                                           ;255.4
        add       ebx, DWORD PTR [388+esp]                      ;255.4
        shl       ebx, 8                                        ;255.4
        add       ebx, DWORD PTR [396+esp]                      ;255.4
        movss     xmm0, DWORD PTR [220+esp]                     ;255.4
        movss     DWORD PTR [156+ebx], xmm0                     ;255.4
                                ; LOE ebx esi edi
.B1.121:                        ; Preds .B1.119 .B1.120
        neg       edi                                           ;257.11
        neg       esi                                           ;258.11
        mov       eax, DWORD PTR [388+esp]                      ;257.11
        add       edi, eax                                      ;257.11
        add       esi, eax                                      ;258.11
        mov       edx, 1                                        ;258.11
        shl       edi, 5                                        ;257.11
        shl       esi, 6                                        ;258.11
        add       edi, DWORD PTR [372+esp]                      ;257.11
        add       esi, DWORD PTR [404+esp]                      ;258.11
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;260.13
        movss     xmm1, DWORD PTR [_READ_VEHICLES$XPARTMP.0.1]  ;259.11
        movss     xmm0, DWORD PTR [_2il0floatpacket.4]          ;259.22
        movss     xmm2, DWORD PTR [_READ_VEHICLES$STIMETP.0.1]  ;257.11
        mov       DWORD PTR [428+esp], ecx                      ;260.13
        mov       WORD PTR [12+esi], dx                         ;258.11
        mov       ecx, DWORD PTR [28+edi]                       ;260.16
        imul      eax, ecx, 152                                 ;260.16
        imul      edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32], 152 ;
        movss     DWORD PTR [12+edi], xmm2                      ;257.11
        comiss    xmm0, xmm1                                    ;259.22
        jbe       .B1.124       ; Prob 50%                      ;259.22
                                ; LOE eax edx ecx ebx esi edi xmm1 xmm2
.B1.122:                        ; Preds .B1.121
        add       eax, DWORD PTR [428+esp]                      ;260.81
        sub       eax, edx                                      ;260.81
        movsx     edx, WORD PTR [148+eax]                       ;260.16
        cmp       edx, 5                                        ;260.81
        je        .B1.170       ; Prob 5%                       ;260.81
                                ; LOE eax ecx ebx esi edi xmm2
.B1.123:                        ; Preds .B1.122
        mov       eax, DWORD PTR [20+eax]                       ;265.15
        mov       DWORD PTR [8+esi], eax                        ;265.15
        mov       DWORD PTR [240+ebx], eax                      ;266.15
        jmp       .B1.125       ; Prob 100%                     ;266.15
                                ; LOE ecx ebx esi edi xmm2
.B1.124:                        ; Preds .B1.121
        add       eax, DWORD PTR [428+esp]                      ;269.12
        sub       eax, edx                                      ;269.12
        mulss     xmm1, DWORD PTR [20+eax]                      ;269.12
        movss     DWORD PTR [8+esi], xmm1                       ;269.12
        movss     DWORD PTR [240+ebx], xmm1                     ;270.12
                                ; LOE ecx ebx esi edi xmm2
.B1.125:                        ; Preds .B1.171 .B1.123 .B1.124
        mov       eax, DWORD PTR [48+esi]                       ;274.41
        add       eax, eax                                      ;274.11
        neg       eax                                           ;274.11
        add       eax, DWORD PTR [16+esi]                       ;274.11
        movzx     esi, BYTE PTR [4+edi]                         ;275.8
        mov       BYTE PTR [40+ebx], 1                          ;273.8
        movzx     edx, WORD PTR [2+eax]                         ;274.11
        mov       WORD PTR [20+edi], dx                         ;274.11
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;277.5
        neg       edi                                           ;277.5
        add       edi, ecx                                      ;277.5
        imul      eax, edi, 900                                 ;277.5
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;277.5
        inc       DWORD PTR [_DYNUST_MAIN_MODULE_mp_MTC_VEH-4+esi*4] ;275.8
        inc       WORD PTR [684+ecx+eax]                        ;277.120
        comiss    xmm2, DWORD PTR [_DYNUST_MAIN_MODULE_mp_STARTTM] ;278.44
        jb        .B1.128       ; Prob 50%                      ;278.44
                                ; LOE ebx xmm2
.B1.126:                        ; Preds .B1.125
        movss     xmm0, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ENDTM] ;278.59
        comiss    xmm0, xmm2                                    ;278.89
        jbe       .B1.128       ; Prob 50%                      ;278.89
                                ; LOE ebx
.B1.127:                        ; Preds .B1.126
        mov       BYTE PTR [237+ebx], 1                         ;279.7
        jmp       .B1.129       ; Prob 100%                     ;279.7
                                ; LOE ebx
.B1.128:                        ; Preds .B1.125 .B1.126
        mov       BYTE PTR [237+ebx], 0                         ;281.13
                                ; LOE ebx
.B1.129:                        ; Preds .B1.128 .B1.127
        mov       eax, DWORD PTR [_READ_VEHICLES$IHOVTMP.0.1]   ;283.34
        mov       BYTE PTR [236+ebx], al                        ;283.11
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_RUNMODE], 1 ;286.22
        je        .B1.24        ; Prob 82%                      ;286.22
        jmp       .B1.172       ; Prob 100%                     ;286.22
                                ; LOE
.B1.130:                        ; Preds .B1.17
        mov       DWORD PTR [esp], ecx                          ;
        lea       edi, DWORD PTR [eax*4]                        ;
        mov       DWORD PTR [16+esp], edi                       ;
        xor       esi, esi                                      ;
        mov       edi, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLODTMP] ;
        pxor      xmm0, xmm0                                    ;375.10
        mov       DWORD PTR [460+esp], ebx                      ;
        mov       DWORD PTR [12+esp], edi                       ;
        mov       DWORD PTR [20+esp], eax                       ;
        mov       DWORD PTR [4+esp], edx                        ;
        mov       DWORD PTR [8+esp], ecx                        ;
        mov       ebx, DWORD PTR [esp]                          ;
                                ; LOE ebx esi
.B1.131:                        ; Preds .B1.531 .B1.417 .B1.414 .B1.130
        cmp       DWORD PTR [20+esp], 24                        ;69.9
        jle       .B1.399       ; Prob 0%                       ;69.9
                                ; LOE ebx esi
.B1.132:                        ; Preds .B1.131
        mov       eax, DWORD PTR [8+esp]                        ;69.9
        neg       eax                                           ;69.9
        add       eax, ebx                                      ;69.9
        imul      eax, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLODTMP+40] ;69.9
        add       eax, DWORD PTR [12+esp]                       ;69.9
        push      DWORD PTR [16+esp]                            ;69.9
        push      0                                             ;69.9
        push      eax                                           ;69.9
        call      __intel_fast_memset                           ;69.9
                                ; LOE ebx esi
.B1.493:                        ; Preds .B1.132
        add       esp, 12                                       ;69.9
                                ; LOE ebx esi
.B1.133:                        ; Preds .B1.493
        inc       esi                                           ;69.9
        inc       ebx                                           ;69.9
        cmp       esi, DWORD PTR [4+esp]                        ;69.9
        jae       .B1.415       ; Prob 18%                      ;69.9
                                ; LOE ebx esi
.B1.531:                        ; Preds .B1.133
        mov       eax, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLODTMP] ;180.9
        mov       DWORD PTR [12+esp], eax                       ;180.9
        jmp       .B1.131       ; Prob 100%                     ;180.9
                                ; LOE ebx esi
.B1.135:                        ; Preds .B1.19                  ; Infreq
        cmp       esi, 4                                        ;70.9
        jl        .B1.151       ; Prob 10%                      ;70.9
                                ; LOE ecx ebx esi
.B1.136:                        ; Preds .B1.135                 ; Infreq
        mov       edx, ecx                                      ;70.9
        and       edx, 15                                       ;70.9
        je        .B1.139       ; Prob 50%                      ;70.9
                                ; LOE edx ecx ebx esi
.B1.137:                        ; Preds .B1.136                 ; Infreq
        test      dl, 3                                         ;70.9
        jne       .B1.151       ; Prob 10%                      ;70.9
                                ; LOE edx ecx ebx esi
.B1.138:                        ; Preds .B1.137                 ; Infreq
        neg       edx                                           ;70.9
        add       edx, 16                                       ;70.9
        shr       edx, 2                                        ;70.9
                                ; LOE edx ecx ebx esi
.B1.139:                        ; Preds .B1.138 .B1.136         ; Infreq
        lea       eax, DWORD PTR [4+edx]                        ;70.9
        cmp       esi, eax                                      ;70.9
        jl        .B1.151       ; Prob 10%                      ;70.9
                                ; LOE edx ecx ebx esi
.B1.140:                        ; Preds .B1.139                 ; Infreq
        mov       eax, esi                                      ;70.9
        sub       eax, edx                                      ;70.9
        and       eax, 3                                        ;70.9
        neg       eax                                           ;70.9
        add       eax, esi                                      ;70.9
        test      edx, edx                                      ;70.9
        jbe       .B1.144       ; Prob 10%                      ;70.9
                                ; LOE eax edx ecx ebx esi
.B1.141:                        ; Preds .B1.140                 ; Infreq
        xor       edi, edi                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B1.142:                        ; Preds .B1.142 .B1.141         ; Infreq
        mov       DWORD PTR [ecx+edi*4], 0                      ;70.9
        inc       edi                                           ;70.9
        cmp       edi, edx                                      ;70.9
        jb        .B1.142       ; Prob 82%                      ;70.9
                                ; LOE eax edx ecx ebx esi edi
.B1.144:                        ; Preds .B1.142 .B1.140         ; Infreq
        pxor      xmm0, xmm0                                    ;70.9
                                ; LOE eax edx ecx ebx esi xmm0
.B1.145:                        ; Preds .B1.145 .B1.144         ; Infreq
        movdqa    XMMWORD PTR [ecx+edx*4], xmm0                 ;70.9
        add       edx, 4                                        ;70.9
        cmp       edx, eax                                      ;70.9
        jb        .B1.145       ; Prob 82%                      ;70.9
                                ; LOE eax edx ecx ebx esi xmm0
.B1.147:                        ; Preds .B1.145 .B1.151         ; Infreq
        cmp       eax, esi                                      ;70.9
        jae       .B1.21        ; Prob 10%                      ;70.9
                                ; LOE eax ecx ebx esi
.B1.149:                        ; Preds .B1.147 .B1.149         ; Infreq
        mov       DWORD PTR [ecx+eax*4], 0                      ;70.9
        inc       eax                                           ;70.9
        cmp       eax, esi                                      ;70.9
        jb        .B1.149       ; Prob 82%                      ;70.9
        jmp       .B1.21        ; Prob 100%                     ;70.9
                                ; LOE eax ecx ebx esi
.B1.151:                        ; Preds .B1.135 .B1.139 .B1.137 ; Infreq
        xor       eax, eax                                      ;70.9
        jmp       .B1.147       ; Prob 100%                     ;70.9
                                ; LOE eax ecx ebx esi
.B1.154:                        ; Preds .B1.49                  ; Infreq
        mov       DWORD PTR [_READ_VEHICLES$IVCLTMP.0.1], edx   ;127.12
        jmp       .B1.52        ; Prob 100%                     ;127.12
                                ; LOE edx
.B1.155:                        ; Preds .B1.107                 ; Infreq
        mov       DWORD PTR [608+esp], 0                        ;210.15
        lea       edx, DWORD PTR [608+esp]                      ;210.15
        mov       DWORD PTR [160+esp], 41                       ;210.15
        lea       eax, DWORD PTR [160+esp]                      ;210.15
        mov       DWORD PTR [164+esp], OFFSET FLAT: __STRLITPACK_27 ;210.15
        push      32                                            ;210.15
        push      eax                                           ;210.15
        push      OFFSET FLAT: __STRLITPACK_113.0.1             ;210.15
        push      -2088435968                                   ;210.15
        push      911                                           ;210.15
        push      edx                                           ;210.15
        call      _for_write_seq_lis                            ;210.15
                                ; LOE ebx edi
.B1.156:                        ; Preds .B1.155                 ; Infreq
        mov       DWORD PTR [632+esp], 0                        ;211.15
        lea       eax, DWORD PTR [192+esp]                      ;211.15
        mov       DWORD PTR [192+esp], 10                       ;211.15
        mov       DWORD PTR [196+esp], OFFSET FLAT: __STRLITPACK_25 ;211.15
        push      32                                            ;211.15
        push      eax                                           ;211.15
        push      OFFSET FLAT: __STRLITPACK_114.0.1             ;211.15
        push      -2088435968                                   ;211.15
        push      911                                           ;211.15
        lea       edx, DWORD PTR [652+esp]                      ;211.15
        push      edx                                           ;211.15
        call      _for_write_seq_lis                            ;211.15
                                ; LOE ebx edi
.B1.157:                        ; Preds .B1.156                 ; Infreq
        mov       eax, DWORD PTR [436+esp]                      ;211.15
        lea       edx, DWORD PTR [264+esp]                      ;211.15
        mov       DWORD PTR [264+esp], eax                      ;211.15
        push      edx                                           ;211.15
        push      OFFSET FLAT: __STRLITPACK_115.0.1             ;211.15
        lea       ecx, DWORD PTR [664+esp]                      ;211.15
        push      ecx                                           ;211.15
        call      _for_write_seq_lis_xmit                       ;211.15
                                ; LOE ebx edi
.B1.158:                        ; Preds .B1.157                 ; Infreq
        mov       DWORD PTR [668+esp], 0                        ;212.15
        lea       eax, DWORD PTR [236+esp]                      ;212.15
        mov       DWORD PTR [236+esp], 19                       ;212.15
        mov       DWORD PTR [240+esp], OFFSET FLAT: __STRLITPACK_23 ;212.15
        push      32                                            ;212.15
        push      eax                                           ;212.15
        push      OFFSET FLAT: __STRLITPACK_116.0.1             ;212.15
        push      -2088435968                                   ;212.15
        push      911                                           ;212.15
        lea       edx, DWORD PTR [688+esp]                      ;212.15
        push      edx                                           ;212.15
        call      _for_write_seq_lis                            ;212.15
                                ; LOE ebx edi
.B1.159:                        ; Preds .B1.158                 ; Infreq
        sub       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;212.15
        imul      ebx, ebx, 152                                 ;212.15
        imul      edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE+32], 44 ;212.15
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;212.15
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE] ;212.15
        mov       DWORD PTR [240+esp], eax                      ;212.15
        imul      esi, DWORD PTR [28+ecx+ebx], 44               ;212.51
        add       esi, eax                                      ;212.15
        sub       esi, edx                                      ;212.15
        mov       DWORD PTR [216+esp], edx                      ;212.15
        lea       edx, DWORD PTR [308+esp]                      ;212.15
        mov       DWORD PTR [292+esp], ecx                      ;212.15
        mov       eax, DWORD PTR [36+esi]                       ;212.15
        mov       DWORD PTR [308+esp], eax                      ;212.15
        push      edx                                           ;212.15
        push      OFFSET FLAT: __STRLITPACK_117.0.1             ;212.15
        lea       ecx, DWORD PTR [700+esp]                      ;212.15
        push      ecx                                           ;212.15
        call      _for_write_seq_lis_xmit                       ;212.15
                                ; LOE ebx edi bl bh
.B1.160:                        ; Preds .B1.159                 ; Infreq
        mov       edx, ebx                                      ;212.131
        lea       esi, DWORD PTR [328+esp]                      ;212.15
        mov       eax, DWORD PTR [304+esp]                      ;212.131
        imul      ecx, DWORD PTR [24+eax+edx], 44               ;212.131
        add       ecx, DWORD PTR [252+esp]                      ;212.15
        sub       ecx, DWORD PTR [228+esp]                      ;212.15
        mov       ebx, DWORD PTR [36+ecx]                       ;212.15
        mov       DWORD PTR [328+esp], ebx                      ;212.15
        push      esi                                           ;212.15
        push      OFFSET FLAT: __STRLITPACK_118.0.1             ;212.15
        lea       eax, DWORD PTR [712+esp]                      ;212.15
        push      eax                                           ;212.15
        call      _for_write_seq_lis_xmit                       ;212.15
                                ; LOE edi
.B1.494:                        ; Preds .B1.160                 ; Infreq
        add       esp, 108                                      ;212.15
                                ; LOE edi
.B1.161:                        ; Preds .B1.494                 ; Infreq
        mov       DWORD PTR [608+esp], 0                        ;213.15
        lea       eax, DWORD PTR [184+esp]                      ;213.15
        mov       DWORD PTR [184+esp], 20                       ;213.15
        mov       DWORD PTR [188+esp], OFFSET FLAT: __STRLITPACK_21 ;213.15
        push      32                                            ;213.15
        push      eax                                           ;213.15
        push      OFFSET FLAT: __STRLITPACK_119.0.1             ;213.15
        push      -2088435968                                   ;213.15
        push      911                                           ;213.15
        lea       edx, DWORD PTR [628+esp]                      ;213.15
        push      edx                                           ;213.15
        call      _for_write_seq_lis                            ;213.15
                                ; LOE edi
.B1.162:                        ; Preds .B1.161                 ; Infreq
        push      0                                             ;213.15
        push      OFFSET FLAT: __STRLITPACK_120.0.1             ;213.15
        lea       eax, DWORD PTR [640+esp]                      ;213.15
        push      eax                                           ;213.15
        call      _for_write_seq_lis_xmit                       ;213.15
                                ; LOE edi
.B1.495:                        ; Preds .B1.162                 ; Infreq
        add       esp, 36                                       ;213.15
                                ; LOE edi
.B1.163:                        ; Preds .B1.495                 ; Infreq
        mov       eax, DWORD PTR [228+esp]                      ;213.101
        movsx     eax, WORD PTR [2+eax]                         ;213.101
        test      eax, eax                                      ;213.15
        jle       .B1.168       ; Prob 2%                       ;213.15
                                ; LOE eax edi
.B1.164:                        ; Preds .B1.163                 ; Infreq
        mov       DWORD PTR [124+esp], edi                      ;
        mov       esi, 1                                        ;
        mov       DWORD PTR [212+esp], eax                      ;
        lea       ebx, DWORD PTR [568+esp]                      ;
        mov       edi, DWORD PTR [228+esp]                      ;
                                ; LOE ebx esi edi
.B1.165:                        ; Preds .B1.166 .B1.164         ; Infreq
        movzx     eax, WORD PTR [edi+esi*2]                     ;213.15
        mov       WORD PTR [568+esp], ax                        ;213.15
        push      ebx                                           ;213.15
        push      OFFSET FLAT: __STRLITPACK_121.0.1             ;213.15
        lea       edx, DWORD PTR [616+esp]                      ;213.15
        push      edx                                           ;213.15
        call      _for_write_seq_lis_xmit                       ;213.15
                                ; LOE ebx esi edi
.B1.496:                        ; Preds .B1.165                 ; Infreq
        add       esp, 12                                       ;213.15
                                ; LOE ebx esi edi
.B1.166:                        ; Preds .B1.496                 ; Infreq
        inc       esi                                           ;213.15
        cmp       esi, DWORD PTR [212+esp]                      ;213.15
        jle       .B1.165       ; Prob 82%                      ;213.15
                                ; LOE ebx esi edi
.B1.167:                        ; Preds .B1.166                 ; Infreq
        mov       edi, DWORD PTR [124+esp]                      ;
                                ; LOE edi
.B1.168:                        ; Preds .B1.163 .B1.167         ; Infreq
        push      0                                             ;213.15
        push      OFFSET FLAT: __STRLITPACK_122.0.1             ;213.15
        lea       eax, DWORD PTR [616+esp]                      ;213.15
        push      eax                                           ;213.15
        call      _for_write_seq_lis_xmit                       ;213.15
                                ; LOE edi
.B1.169:                        ; Preds .B1.168                 ; Infreq
        push      32                                            ;214.15
        xor       eax, eax                                      ;214.15
        push      eax                                           ;214.15
        push      eax                                           ;214.15
        push      -2088435968                                   ;214.15
        push      eax                                           ;214.15
        push      OFFSET FLAT: __STRLITPACK_123                 ;214.15
        call      _for_stop_core                                ;214.15
                                ; LOE edi
.B1.497:                        ; Preds .B1.169                 ; Infreq
        add       esp, 36                                       ;214.15
        jmp       .B1.109       ; Prob 100%                     ;214.15
                                ; LOE edi
.B1.170:                        ; Preds .B1.122                 ; Infreq
        push      OFFSET FLAT: __NLITPACK_1.0.1                 ;261.20
        call      _RANXY                                        ;261.20
                                ; LOE f1
.B1.498:                        ; Preds .B1.170                 ; Infreq
        fstp      DWORD PTR [524+esp]                           ;261.20
        movss     xmm0, DWORD PTR [524+esp]                     ;261.20
        add       esp, 4                                        ;261.20
                                ; LOE xmm0
.B1.171:                        ; Preds .B1.498                 ; Infreq
        mov       edi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;262.15
        neg       edi                                           ;262.15
        mov       edx, DWORD PTR [_READ_VEHICLES$JM.0.1]        ;262.15
        add       edi, edx                                      ;262.15
        shl       edi, 5                                        ;262.15
        mov       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;262.15
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;262.15
        mov       ebx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;262.103
        mov       esi, DWORD PTR [28+ecx+edi]                   ;262.47
        neg       ebx                                           ;262.15
        sub       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;262.15
        add       ebx, edx                                      ;262.15
        imul      esi, esi, 152                                 ;262.15
        add       edi, ecx                                      ;
        movss     xmm2, DWORD PTR [12+edi]                      ;278.14
        mulss     xmm0, DWORD PTR [20+eax+esi]                  ;262.15
        mov       esi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE+32] ;263.103
        neg       esi                                           ;263.15
        add       esi, edx                                      ;263.15
        shl       ebx, 8                                        ;262.15
        shl       esi, 6                                        ;263.15
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE] ;263.15
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;262.15
        mov       ecx, DWORD PTR [28+edi]                       ;277.63
        movss     DWORD PTR [8+edx+esi], xmm0                   ;263.15
        add       esi, edx                                      ;
        movss     DWORD PTR [240+eax+ebx], xmm0                 ;262.15
        add       ebx, eax                                      ;
        jmp       .B1.125       ; Prob 100%                     ;
                                ; LOE ecx ebx esi edi xmm2
.B1.172:                        ; Preds .B1.129                 ; Infreq
        push      40000                                         ;287.14
        push      0                                             ;287.14
        push      OFFSET FLAT: _READ_VEHICLES$JPATH_TMP.0.1     ;287.14
        call      __intel_fast_memset                           ;287.14
                                ; LOE
.B1.173:                        ; Preds .B1.172                 ; Infreq
        mov       DWORD PTR [620+esp], 0                        ;289.12
        lea       edx, DWORD PTR [620+esp]                      ;289.12
        mov       DWORD PTR [476+esp], OFFSET FLAT: _READ_VEHICLES$MTMP.0.1 ;289.12
        lea       eax, DWORD PTR [476+esp]                      ;289.12
        push      32                                            ;289.12
        push      OFFSET FLAT: READ_VEHICLES$format_pack.0.1    ;289.12
        push      eax                                           ;289.12
        push      OFFSET FLAT: __STRLITPACK_126.0.1             ;289.12
        push      -2088435968                                   ;289.12
        push      550                                           ;289.12
        push      edx                                           ;289.12
        call      _for_read_seq_fmt                             ;289.12
                                ; LOE
.B1.174:                        ; Preds .B1.173                 ; Infreq
        mov       ecx, DWORD PTR [_READ_VEHICLES$NTMP.0.1]      ;289.12
        xor       edx, edx                                      ;289.12
        dec       ecx                                           ;289.12
        lea       ebx, DWORD PTR [176+esp]                      ;289.12
        cmovs     ecx, edx                                      ;289.12
        mov       eax, 1                                        ;289.12
        mov       DWORD PTR [176+esp], eax                      ;289.12
        mov       DWORD PTR [180+esp], OFFSET FLAT: _READ_VEHICLES$JPATH_TMP.0.1 ;289.12
        mov       DWORD PTR [184+esp], eax                      ;289.12
        mov       DWORD PTR [188+esp], ecx                      ;289.12
        mov       DWORD PTR [192+esp], 4                        ;289.12
        push      ebx                                           ;289.12
        push      OFFSET FLAT: __STRLITPACK_127.0.1             ;289.12
        lea       esi, DWORD PTR [656+esp]                      ;289.12
        push      esi                                           ;289.12
        call      _for_read_seq_fmt_xmit                        ;289.12
                                ; LOE
.B1.499:                        ; Preds .B1.174                 ; Infreq
        add       esp, 52                                       ;289.12
                                ; LOE
.B1.175:                        ; Preds .B1.499                 ; Infreq
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_LOGOUT], 0  ;290.24
        jle       .B1.178       ; Prob 16%                      ;290.24
                                ; LOE
.B1.176:                        ; Preds .B1.175                 ; Infreq
        mov       DWORD PTR [608+esp], 0                        ;290.29
        lea       eax, DWORD PTR [200+esp]                      ;290.29
        mov       DWORD PTR [200+esp], 19                       ;290.29
        mov       DWORD PTR [204+esp], OFFSET FLAT: __STRLITPACK_17 ;290.29
        push      32                                            ;290.29
        push      eax                                           ;290.29
        push      OFFSET FLAT: __STRLITPACK_128.0.1             ;290.29
        push      -2088435968                                   ;290.29
        push      711                                           ;290.29
        lea       edx, DWORD PTR [628+esp]                      ;290.29
        push      edx                                           ;290.29
        call      _for_write_seq_lis                            ;290.29
                                ; LOE
.B1.177:                        ; Preds .B1.176                 ; Infreq
        mov       eax, DWORD PTR [_READ_VEHICLES$JTMP.0.1]      ;290.29
        lea       edx, DWORD PTR [320+esp]                      ;290.29
        mov       DWORD PTR [320+esp], eax                      ;290.29
        push      edx                                           ;290.29
        push      OFFSET FLAT: __STRLITPACK_129.0.1             ;290.29
        lea       ecx, DWORD PTR [640+esp]                      ;290.29
        push      ecx                                           ;290.29
        call      _for_write_seq_lis_xmit                       ;290.29
                                ; LOE
.B1.500:                        ; Preds .B1.177                 ; Infreq
        add       esp, 36                                       ;290.29
                                ; LOE
.B1.178:                        ; Preds .B1.500 .B1.175         ; Infreq
        mov       esi, DWORD PTR [_READ_VEHICLES$NTMP.0.1]      ;292.11
        lea       ebx, DWORD PTR [-1+esi]                       ;292.11
        test      ebx, ebx                                      ;292.11
        jle       .B1.183       ; Prob 2%                       ;292.11
                                ; LOE ebx esi
.B1.179:                        ; Preds .B1.178                 ; Infreq
        mov       esi, 1                                        ;
        lea       edi, DWORD PTR [468+esp]                      ;
                                ; LOE ebx esi edi
.B1.180:                        ; Preds .B1.181 .B1.179         ; Infreq
        mov       edx, DWORD PTR [_READ_VEHICLES$JPATH_TMP.0.1-4+esi*4] ;294.29
        sub       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM+32] ;294.14
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM] ;294.14
        mov       WORD PTR [532+esp], si                        ;293.17
        cvtsi2ss  xmm0, DWORD PTR [eax+edx*4]                   ;294.14
        movss     DWORD PTR [468+esp], xmm0                     ;294.14
        push      edi                                           ;298.15
        push      OFFSET FLAT: __NLITPACK_2.0.1                 ;298.15
        lea       ecx, DWORD PTR [540+esp]                      ;298.15
        push      ecx                                           ;298.15
        push      OFFSET FLAT: _READ_VEHICLES$JM.0.1            ;298.15
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_INSERT  ;298.15
                                ; LOE ebx esi edi
.B1.501:                        ; Preds .B1.180                 ; Infreq
        add       esp, 16                                       ;298.15
                                ; LOE ebx esi edi
.B1.181:                        ; Preds .B1.501                 ; Infreq
        inc       esi                                           ;299.11
        cmp       esi, ebx                                      ;299.11
        jle       .B1.180       ; Prob 82%                      ;299.11
                                ; LOE ebx esi edi
.B1.182:                        ; Preds .B1.181                 ; Infreq
        mov       esi, DWORD PTR [_READ_VEHICLES$NTMP.0.1]      ;301.21
                                ; LOE esi
.B1.183:                        ; Preds .B1.178 .B1.182         ; Infreq
        mov       edi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE+32] ;300.14
        neg       edi                                           ;300.14
        add       edi, DWORD PTR [_READ_VEHICLES$JM.0.1]        ;300.14
        shl       edi, 6                                        ;300.14
        mov       ebx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE] ;300.14
        mov       eax, DWORD PTR [_READ_VEHICLES$NDESTMP.0.1]   ;300.14
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_MASTERDEST+32] ;300.14
        sub       eax, DWORD PTR [48+ebx+edi]                   ;300.14
        neg       edx                                           ;300.14
        mov       ebx, DWORD PTR [16+ebx+edi]                   ;300.14
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_MASTERDEST] ;300.14
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DESTINATION+32] ;300.14
        movsx     eax, WORD PTR [ebx+eax*2]                     ;300.14
        add       edx, eax                                      ;300.14
        neg       ecx                                           ;300.14
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DESTINATION] ;300.14
        mov       WORD PTR [532+esp], si                        ;301.11
        lea       esi, DWORD PTR [468+esp]                      ;302.19
        movsx     edx, WORD PTR [edi+edx*2]                     ;300.14
        add       ecx, edx                                      ;300.14
        cvtsi2ss  xmm0, DWORD PTR [eax+ecx*4]                   ;300.14
        movss     DWORD PTR [468+esp], xmm0                     ;300.14
        push      esi                                           ;302.19
        push      OFFSET FLAT: __NLITPACK_2.0.1                 ;302.19
        lea       eax, DWORD PTR [540+esp]                      ;302.19
        push      eax                                           ;302.19
        push      OFFSET FLAT: _READ_VEHICLES$JM.0.1            ;302.19
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_INSERT  ;302.19
                                ; LOE
.B1.184:                        ; Preds .B1.183                 ; Infreq
        push      OFFSET FLAT: _READ_VEHICLES$NTMP.0.1          ;304.19
        push      OFFSET FLAT: _READ_VEHICLES$JM.0.1            ;304.19
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ACTIVE_SIZE ;304.19
                                ; LOE
.B1.185:                        ; Preds .B1.184                 ; Infreq
        mov       edi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE+32] ;305.14
        neg       edi                                           ;305.14
        add       edi, DWORD PTR [_READ_VEHICLES$JM.0.1]        ;305.14
        shl       edi, 6                                        ;305.14
        mov       esi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE] ;305.14
        mov       edx, DWORD PTR [_READ_VEHICLES$NDESTMP.0.1]   ;305.14
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_MASTERDEST+32] ;305.14
        sub       edx, DWORD PTR [48+esi+edi]                   ;305.14
        neg       ecx                                           ;305.14
        mov       esi, DWORD PTR [16+esi+edi]                   ;305.14
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_MASTERDEST] ;305.14
        mov       ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DESTINATION+32] ;305.14
        movsx     edx, WORD PTR [esi+edx*2]                     ;305.14
        add       ecx, edx                                      ;305.14
        neg       ebx                                           ;305.14
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DESTINATION] ;305.14
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE+32] ;305.14
        movsx     ecx, WORD PTR [edi+ecx*2]                     ;305.14
        add       ebx, ecx                                      ;305.14
        neg       eax                                           ;305.14
        mov       ecx, DWORD PTR [_READ_VEHICLES$JM.0.1]        ;308.19
        sub       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;308.19
        add       eax, DWORD PTR [edx+ebx*4]                    ;305.14
        imul      ebx, eax, 44                                  ;305.14
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE] ;305.14
        shl       ecx, 5                                        ;308.19
        mov       edx, DWORD PTR [_READ_VEHICLES$NTMP.0.1]      ;305.30
        mov       eax, DWORD PTR [36+eax+ebx]                   ;305.14
        mov       ebx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;308.19
        push      OFFSET FLAT: _READ_VEHICLES$JM.0.1            ;308.19
        mov       DWORD PTR [_READ_VEHICLES$JPATH_TMP.0.1-4+edx*4], eax ;305.14
        lea       esi, DWORD PTR [28+ecx+ebx]                   ;308.19
        push      esi                                           ;308.19
        call      _DYNUST_LINK_VEH_LIST_MODULE_mp_ENTRYQUEUE_INSERT ;308.19
                                ; LOE
.B1.502:                        ; Preds .B1.185                 ; Infreq
        add       esp, 32                                       ;308.19
                                ; LOE
.B1.186:                        ; Preds .B1.502                 ; Infreq
        mov       edx, DWORD PTR [_READ_VEHICLES$JM.0.1]        ;309.14
        sub       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;309.14
        shl       edx, 5                                        ;309.14
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;309.14
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;309.14
        neg       ecx                                           ;309.14
        add       ecx, DWORD PTR [28+eax+edx]                   ;309.14
        imul      esi, ecx, 900                                 ;309.14
        movss     xmm1, DWORD PTR [_READ_VEHICLES$STIMETP.0.1]  ;311.14
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;309.14
        inc       WORD PTR [692+ebx+esi]                        ;309.143
        comiss    xmm1, DWORD PTR [_DYNUST_MAIN_MODULE_mp_STARTTM] ;311.25
        jb        .B1.189       ; Prob 50%                      ;311.25
                                ; LOE xmm1
.B1.187:                        ; Preds .B1.186                 ; Infreq
        movss     xmm0, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ENDTM] ;311.25
        comiss    xmm0, xmm1                                    ;311.48
        jbe       .B1.189       ; Prob 50%                      ;311.48
                                ; LOE
.B1.188:                        ; Preds .B1.187                 ; Infreq
        inc       DWORD PTR [_DYNUST_MAIN_MODULE_mp_NUMCARS]    ;311.57
                                ; LOE
.B1.189:                        ; Preds .B1.186 .B1.187 .B1.188 ; Infreq
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_RUNMODE] ;314.29
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_READHISTARR], 0 ;314.29
        jle       .B1.223       ; Prob 16%                      ;314.29
                                ; LOE eax
.B1.190:                        ; Preds .B1.189                 ; Infreq
        cmp       eax, 2                                        ;314.45
        je        .B1.214       ; Prob 16%                      ;314.45
                                ; LOE eax
.B1.191:                        ; Preds .B1.190                 ; Infreq
        cmp       eax, 4                                        ;326.14
        jne       .B1.24        ; Prob 67%                      ;326.14
                                ; LOE
.B1.192:                        ; Preds .B1.223 .B1.224 .B1.191 ; Infreq
        mov       eax, DWORD PTR [_READ_VEHICLES$IVCLTMP.0.1]   ;327.16
        cmp       eax, 4                                        ;327.16
        je        .B1.195       ; Prob 33%                      ;327.16
                                ; LOE eax
.B1.193:                        ; Preds .B1.192                 ; Infreq
        cmp       eax, 5                                        ;327.16
        jne       .B1.24        ; Prob 50%                      ;327.16
                                ; LOE
.B1.194:                        ; Preds .B1.193                 ; Infreq
        cmp       DWORD PTR [_READ_VEHICLES$IVCL2TMP.0.1], 6    ;327.62
        je        .B1.24        ; Prob 16%                      ;327.62
                                ; LOE
.B1.195:                        ; Preds .B1.192 .B1.194         ; Infreq
        movss     xmm0, DWORD PTR [_READ_VEHICLES$STIMETP.0.1]  ;327.32
        comiss    xmm0, DWORD PTR [_DYNUST_MAIN_MODULE_mp_INFOSTARTTIME] ;327.80
        jb        .B1.24        ; Prob 50%                      ;327.80
                                ; LOE
.B1.196:                        ; Preds .B1.195                 ; Infreq
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_INCI_NUM], 0 ;327.110
        jle       .B1.24        ; Prob 16%                      ;327.110
                                ; LOE
.B1.197:                        ; Preds .B1.196                 ; Infreq
        mov       edx, DWORD PTR [_READ_VEHICLES$JM.0.1]        ;329.25
        lea       esi, DWORD PTR [116+esp]                      ;333.23
        sub       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;329.18
        xor       ebx, ebx                                      ;330.18
        shl       edx, 5                                        ;329.18
        lea       edi, DWORD PTR [108+esp]                      ;333.23
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;329.18
        mov       DWORD PTR [84+esp], 1                         ;328.18
        mov       DWORD PTR [100+esp], ebx                      ;330.18
        mov       ecx, DWORD PTR [28+eax+edx]                   ;329.18
        lea       edx, DWORD PTR [84+esp]                       ;333.23
        mov       DWORD PTR [92+esp], ecx                       ;329.18
        lea       ecx, DWORD PTR [92+esp]                       ;333.23
        mov       DWORD PTR [108+esp], ebx                      ;331.18
        lea       eax, DWORD PTR [100+esp]                      ;333.23
        mov       DWORD PTR [116+esp], ebx                      ;332.18
        push      esi                                           ;333.23
        push      edi                                           ;333.23
        push      eax                                           ;333.23
        push      DWORD PTR [12+ebp]                            ;333.23
        push      edx                                           ;333.23
        push      ecx                                           ;333.23
        push      OFFSET FLAT: _READ_VEHICLES$JM.0.1            ;333.23
        call      _DYNUST_ROUTE_SWITCH_MODULE_mp_GET_AUTO_REMAIN_TIME ;333.23
                                ; LOE
.B1.503:                        ; Preds .B1.197                 ; Infreq
        add       esp, 28                                       ;333.23
                                ; LOE
.B1.198:                        ; Preds .B1.503                 ; Infreq
        test      BYTE PTR [116+esp], 1                         ;334.21
        je        .B1.24        ; Prob 60%                      ;334.21
                                ; LOE
.B1.199:                        ; Preds .B1.198                 ; Infreq
        mov       eax, DWORD PTR [12+ebp]                       ;336.22
        lea       ecx, DWORD PTR [56+esp]                       ;336.22
        mov       DWORD PTR [608+esp], 0                        ;336.22
        mov       edx, DWORD PTR [eax]                          ;336.22
        dec       edx                                           ;336.168
        cvtsi2ss  xmm0, edx                                     ;336.168
        mulss     xmm0, DWORD PTR [_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT] ;336.22
        movss     DWORD PTR [56+esp], xmm0                      ;336.22
        push      32                                            ;336.22
        push      OFFSET FLAT: READ_VEHICLES$format_pack.0.1+324 ;336.22
        push      ecx                                           ;336.22
        push      OFFSET FLAT: __STRLITPACK_132.0.1             ;336.22
        push      -2088435968                                   ;336.22
        push      8456                                          ;336.22
        lea       ebx, DWORD PTR [632+esp]                      ;336.22
        push      ebx                                           ;336.22
        call      _for_write_seq_fmt                            ;336.22
                                ; LOE
.B1.200:                        ; Preds .B1.199                 ; Infreq
        mov       ebx, DWORD PTR [_READ_VEHICLES$JM.0.1]        ;336.22
        lea       eax, DWORD PTR [92+esp]                       ;336.22
        mov       DWORD PTR [92+esp], ebx                       ;336.22
        push      eax                                           ;336.22
        push      OFFSET FLAT: __STRLITPACK_133.0.1             ;336.22
        lea       edx, DWORD PTR [644+esp]                      ;336.22
        push      edx                                           ;336.22
        call      _for_write_seq_fmt_xmit                       ;336.22
                                ; LOE ebx
.B1.201:                        ; Preds .B1.200                 ; Infreq
        mov       esi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;336.22
        lea       ecx, DWORD PTR [112+esp]                      ;336.22
        neg       esi                                           ;336.22
        add       esi, ebx                                      ;336.22
        shl       esi, 5                                        ;336.22
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;336.22
        mov       DWORD PTR [44+esp], eax                       ;336.22
        movzx     edx, WORD PTR [22+eax+esi]                    ;336.22
        mov       WORD PTR [112+esp], dx                        ;336.22
        push      ecx                                           ;336.22
        push      OFFSET FLAT: __STRLITPACK_134.0.1             ;336.22
        lea       edi, DWORD PTR [656+esp]                      ;336.22
        push      edi                                           ;336.22
        call      _for_write_seq_fmt_xmit                       ;336.22
                                ; LOE ebx esi
.B1.202:                        ; Preds .B1.201                 ; Infreq
        mov       eax, DWORD PTR [56+esp]                       ;336.22
        lea       ecx, DWORD PTR [132+esp]                      ;336.22
        movzx     edx, WORD PTR [20+eax+esi]                    ;336.22
        mov       WORD PTR [132+esp], dx                        ;336.22
        push      ecx                                           ;336.22
        push      OFFSET FLAT: __STRLITPACK_135.0.1             ;336.22
        lea       esi, DWORD PTR [668+esp]                      ;336.22
        push      esi                                           ;336.22
        call      _for_write_seq_fmt_xmit                       ;336.22
                                ; LOE ebx
.B1.203:                        ; Preds .B1.202                 ; Infreq
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE+32] ;336.22
        lea       esi, DWORD PTR [152+esp]                      ;336.22
        neg       edx                                           ;336.22
        add       edx, ebx                                      ;336.22
        shl       edx, 6                                        ;336.22
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE] ;336.22
        movzx     ecx, WORD PTR [12+eax+edx]                    ;336.22
        mov       WORD PTR [152+esp], cx                        ;336.22
        push      esi                                           ;336.22
        push      OFFSET FLAT: __STRLITPACK_136.0.1             ;336.22
        lea       edi, DWORD PTR [680+esp]                      ;336.22
        push      edi                                           ;336.22
        call      _for_write_seq_fmt_xmit                       ;336.22
                                ; LOE ebx
.B1.204:                        ; Preds .B1.203                 ; Infreq
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;336.22
        neg       eax                                           ;336.22
        add       eax, DWORD PTR [168+esp]                      ;336.22
        imul      edx, eax, 152                                 ;336.22
        imul      edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE+32], -44 ;336.22
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;336.22
        add       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE] ;336.22
        mov       DWORD PTR [76+esp], edx                       ;336.22
        imul      ecx, DWORD PTR [28+esi+edx], 44               ;336.279
        lea       edx, DWORD PTR [172+esp]                      ;336.22
        mov       eax, DWORD PTR [36+ecx+edi]                   ;336.22
        mov       DWORD PTR [128+esp], edi                      ;336.22
        mov       DWORD PTR [172+esp], eax                      ;336.22
        push      edx                                           ;336.22
        push      OFFSET FLAT: __STRLITPACK_137.0.1             ;336.22
        lea       ecx, DWORD PTR [692+esp]                      ;336.22
        push      ecx                                           ;336.22
        call      _for_write_seq_fmt_xmit                       ;336.22
                                ; LOE ebx esi
.B1.205:                        ; Preds .B1.204                 ; Infreq
        mov       eax, DWORD PTR [88+esp]                       ;337.22
        lea       edi, DWORD PTR [192+esp]                      ;336.22
        mov       ecx, DWORD PTR [140+esp]                      ;336.22
        imul      edx, DWORD PTR [24+esi+eax], 44               ;337.22
        mov       esi, DWORD PTR [36+edx+ecx]                   ;336.22
        mov       DWORD PTR [192+esp], esi                      ;336.22
        push      edi                                           ;336.22
        push      OFFSET FLAT: __STRLITPACK_138.0.1             ;336.22
        lea       eax, DWORD PTR [704+esp]                      ;336.22
        push      eax                                           ;336.22
        call      _for_write_seq_fmt_xmit                       ;336.22
                                ; LOE ebx
.B1.206:                        ; Preds .B1.205                 ; Infreq
        sub       ebx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;336.22
        lea       eax, DWORD PTR [212+esp]                      ;336.22
        shl       ebx, 8                                        ;336.22
        mov       esi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;336.22
        cvtsi2ss  xmm0, DWORD PTR [244+esi+ebx]                 ;337.101
        divss     xmm0, DWORD PTR [_2il0floatpacket.3]          ;336.22
        movss     DWORD PTR [212+esp], xmm0                     ;336.22
        push      eax                                           ;336.22
        push      OFFSET FLAT: __STRLITPACK_139.0.1             ;336.22
        lea       edx, DWORD PTR [716+esp]                      ;336.22
        push      edx                                           ;336.22
        call      _for_write_seq_fmt_xmit                       ;336.22
                                ; LOE ebx esi
.B1.207:                        ; Preds .B1.206                 ; Infreq
        cvtsi2ss  xmm0, DWORD PTR [248+esi+ebx]                 ;337.138
        divss     xmm0, DWORD PTR [_2il0floatpacket.3]          ;336.22
        movss     DWORD PTR [232+esp], xmm0                     ;336.22
        lea       eax, DWORD PTR [232+esp]                      ;336.22
        push      eax                                           ;336.22
        push      OFFSET FLAT: __STRLITPACK_140.0.1             ;336.22
        lea       edx, DWORD PTR [728+esp]                      ;336.22
        push      edx                                           ;336.22
        call      _for_write_seq_fmt_xmit                       ;336.22
                                ; LOE
.B1.504:                        ; Preds .B1.207                 ; Infreq
        add       esp, 124                                      ;336.22
                                ; LOE
.B1.208:                        ; Preds .B1.504                 ; Infreq
        mov       ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_IVCT]  ;344.17
        inc       ebx                                           ;344.17
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_IVCT], ebx  ;344.17
        cmp       ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TMPARYSIZE] ;345.25
        jg        .B1.210       ; Prob 18%                      ;345.25
                                ; LOE ebx
.B1.209:                        ; Preds .B1.505 .B1.208         ; Infreq
        mov       eax, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLODTMP+40] ;352.17
        mov       ecx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLODTMP+44] ;352.17
        imul      ecx, eax                                      ;352.17
        mov       edx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLODTMP+32] ;352.17
        sub       ebx, edx                                      ;352.17
        mov       esi, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLODTMP] ;352.17
        sub       esi, ecx                                      ;352.17
        mov       ecx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLODZONE+32] ;354.17
        neg       ecx                                           ;354.17
        mov       edx, DWORD PTR [_READ_VEHICLES$JDEST_TMP.0.1] ;353.17
        add       ecx, edx                                      ;354.17
        lea       edi, DWORD PTR [esi+ebx*4]                    ;352.17
        mov       ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_JUSTVEH] ;352.17
        mov       DWORD PTR [edi+eax*2], ebx                    ;352.17
        mov       DWORD PTR [edi+eax], edx                      ;353.17
        mov       eax, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLODZONE] ;354.17
        mov       DWORD PTR [eax+ecx*4], -1                     ;354.17
        jmp       .B1.24        ; Prob 100%                     ;354.17
                                ; LOE
.B1.210:                        ; Preds .B1.208                 ; Infreq
        mov       DWORD PTR [608+esp], 0                        ;346.16
        lea       eax, DWORD PTR [8+esp]                        ;346.16
        mov       DWORD PTR [8+esp], 44                         ;346.16
        mov       DWORD PTR [12+esp], OFFSET FLAT: __STRLITPACK_11 ;346.16
        push      32                                            ;346.16
        push      eax                                           ;346.16
        push      OFFSET FLAT: __STRLITPACK_141.0.1             ;346.16
        push      -2088435968                                   ;346.16
        push      911                                           ;346.16
        lea       edx, DWORD PTR [628+esp]                      ;346.16
        push      edx                                           ;346.16
        call      _for_write_seq_lis                            ;346.16
                                ; LOE ebx
.B1.211:                        ; Preds .B1.210                 ; Infreq
        mov       DWORD PTR [632+esp], 0                        ;347.19
        lea       eax, DWORD PTR [40+esp]                       ;347.19
        mov       DWORD PTR [40+esp], 83                        ;347.19
        mov       DWORD PTR [44+esp], OFFSET FLAT: __STRLITPACK_9 ;347.19
        push      32                                            ;347.19
        push      eax                                           ;347.19
        push      OFFSET FLAT: __STRLITPACK_142.0.1             ;347.19
        push      -2088435968                                   ;347.19
        push      911                                           ;347.19
        lea       edx, DWORD PTR [652+esp]                      ;347.19
        push      edx                                           ;347.19
        call      _for_write_seq_lis                            ;347.19
                                ; LOE ebx
.B1.212:                        ; Preds .B1.211                 ; Infreq
        mov       DWORD PTR [656+esp], 0                        ;348.16
        lea       eax, DWORD PTR [72+esp]                       ;348.16
        mov       DWORD PTR [72+esp], 40                        ;348.16
        mov       DWORD PTR [76+esp], OFFSET FLAT: __STRLITPACK_7 ;348.16
        push      32                                            ;348.16
        push      eax                                           ;348.16
        push      OFFSET FLAT: __STRLITPACK_143.0.1             ;348.16
        push      -2088435968                                   ;348.16
        push      911                                           ;348.16
        lea       edx, DWORD PTR [676+esp]                      ;348.16
        push      edx                                           ;348.16
        call      _for_write_seq_lis                            ;348.16
                                ; LOE ebx
.B1.213:                        ; Preds .B1.212                 ; Infreq
        push      32                                            ;349.16
        xor       eax, eax                                      ;349.16
        push      eax                                           ;349.16
        push      eax                                           ;349.16
        push      -2088435968                                   ;349.16
        push      eax                                           ;349.16
        push      OFFSET FLAT: __STRLITPACK_144                 ;349.16
        call      _for_stop_core                                ;349.16
                                ; LOE ebx
.B1.505:                        ; Preds .B1.213                 ; Infreq
        add       esp, 96                                       ;349.16
        jmp       .B1.209       ; Prob 100%                     ;349.16
                                ; LOE ebx
.B1.214:                        ; Preds .B1.190                 ; Infreq
        mov       DWORD PTR [608+esp], 0                        ;315.16
        lea       eax, DWORD PTR [60+esp]                       ;315.16
        mov       DWORD PTR [128+esp], eax                      ;315.16
        lea       edx, DWORD PTR [128+esp]                      ;315.16
        push      32                                            ;315.16
        push      OFFSET FLAT: READ_VEHICLES$format_pack.0.1+304 ;315.16
        push      edx                                           ;315.16
        push      OFFSET FLAT: __STRLITPACK_130.0.1             ;315.16
        push      -2088435968                                   ;315.16
        push      560                                           ;315.16
        lea       ecx, DWORD PTR [632+esp]                      ;315.16
        push      ecx                                           ;315.16
        call      _for_read_seq_fmt                             ;315.16
                                ; LOE
.B1.215:                        ; Preds .B1.214                 ; Infreq
        mov       ecx, DWORD PTR [_READ_VEHICLES$NTMP.0.1]      ;315.16
        xor       edx, edx                                      ;315.16
        dec       ecx                                           ;315.16
        lea       ebx, DWORD PTR [60+esp]                       ;315.16
        cmovs     ecx, edx                                      ;315.16
        mov       eax, 1                                        ;315.16
        mov       DWORD PTR [60+esp], eax                       ;315.16
        mov       DWORD PTR [64+esp], OFFSET FLAT: _READ_VEHICLES$JTIME_TMP.0.1 ;315.16
        mov       DWORD PTR [68+esp], eax                       ;315.16
        mov       DWORD PTR [72+esp], ecx                       ;315.16
        mov       DWORD PTR [76+esp], 4                         ;315.16
        push      ebx                                           ;315.16
        push      OFFSET FLAT: __STRLITPACK_131.0.1             ;315.16
        lea       esi, DWORD PTR [644+esp]                      ;315.16
        push      esi                                           ;315.16
        call      _for_read_seq_fmt_xmit                        ;315.16
                                ; LOE
.B1.506:                        ; Preds .B1.215                 ; Infreq
        add       esp, 40                                       ;315.16
                                ; LOE
.B1.216:                        ; Preds .B1.506                 ; Infreq
        mov       esi, DWORD PTR [_READ_VEHICLES$NTMP.0.1]      ;316.16
        dec       esi                                           ;316.16
        test      esi, esi                                      ;316.16
        jle       .B1.221       ; Prob 2%                       ;316.16
                                ; LOE esi
.B1.217:                        ; Preds .B1.216                 ; Infreq
        mov       ebx, 1                                        ;
        lea       edi, DWORD PTR [468+esp]                      ;
                                ; LOE ebx esi edi
.B1.218:                        ; Preds .B1.219 .B1.217         ; Infreq
        mov       eax, DWORD PTR [_READ_VEHICLES$JTIME_TMP.0.1-4+ebx*4] ;318.18
        mov       WORD PTR [532+esp], bx                        ;317.18
        mov       DWORD PTR [468+esp], eax                      ;318.18
        push      edi                                           ;319.23
        push      OFFSET FLAT: __NLITPACK_3.0.1                 ;319.23
        lea       edx, DWORD PTR [540+esp]                      ;319.23
        push      edx                                           ;319.23
        push      OFFSET FLAT: _READ_VEHICLES$JM.0.1            ;319.23
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_INSERT  ;319.23
                                ; LOE ebx esi edi
.B1.507:                        ; Preds .B1.218                 ; Infreq
        add       esp, 16                                       ;319.23
                                ; LOE ebx esi edi
.B1.219:                        ; Preds .B1.507                 ; Infreq
        inc       ebx                                           ;320.16
        cmp       ebx, esi                                      ;320.16
        jle       .B1.218       ; Prob 99%                      ;320.16
                                ; LOE ebx esi edi
.B1.221:                        ; Preds .B1.219 .B1.216         ; Infreq
        push      OFFSET FLAT: _READ_VEHICLES$JM.0.1            ;321.21
        call      _DYNUST_VEH_MODULE_mp_SETDELAYTOLE            ;321.21
                                ; LOE
.B1.508:                        ; Preds .B1.221                 ; Infreq
        add       esp, 4                                        ;321.21
                                ; LOE
.B1.222:                        ; Preds .B1.508                 ; Infreq
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_RUNMODE] ;326.14
                                ; LOE eax
.B1.223:                        ; Preds .B1.222 .B1.189         ; Infreq
        cmp       eax, 4                                        ;326.14
        je        .B1.192       ; Prob 33%                      ;326.14
                                ; LOE eax
.B1.224:                        ; Preds .B1.223                 ; Infreq
        cmp       eax, 2                                        ;326.14
        jne       .B1.24        ; Prob 50%                      ;326.14
        jmp       .B1.192       ; Prob 100%                     ;326.14
                                ; LOE
.B1.225:                        ; Preds .B1.111                 ; Infreq
        inc       DWORD PTR [_DYNUST_VEH_MODULE_mp_NOOFBUSES]   ;237.7
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_LOGOUT], 0  ;240.17
        jle       .B1.228       ; Prob 16%                      ;240.17
                                ; LOE ebx esi edi
.B1.226:                        ; Preds .B1.225                 ; Infreq
        mov       DWORD PTR [608+esp], 0                        ;240.22
        lea       edx, DWORD PTR [608+esp]                      ;240.22
        mov       DWORD PTR [192+esp], 6                        ;240.22
        lea       eax, DWORD PTR [192+esp]                      ;240.22
        mov       DWORD PTR [196+esp], OFFSET FLAT: __STRLITPACK_19 ;240.22
        push      32                                            ;240.22
        push      eax                                           ;240.22
        push      OFFSET FLAT: __STRLITPACK_124.0.1             ;240.22
        push      -2088435968                                   ;240.22
        push      711                                           ;240.22
        push      edx                                           ;240.22
        call      _for_write_seq_lis                            ;240.22
                                ; LOE ebx esi edi
.B1.227:                        ; Preds .B1.226                 ; Infreq
        mov       eax, DWORD PTR [412+esp]                      ;240.22
        lea       edx, DWORD PTR [288+esp]                      ;240.22
        mov       DWORD PTR [288+esp], eax                      ;240.22
        push      edx                                           ;240.22
        push      OFFSET FLAT: __STRLITPACK_125.0.1             ;240.22
        lea       ecx, DWORD PTR [640+esp]                      ;240.22
        push      ecx                                           ;240.22
        call      _for_write_seq_lis_xmit                       ;240.22
                                ; LOE ebx esi edi
.B1.509:                        ; Preds .B1.227                 ; Infreq
        add       esp, 36                                       ;240.22
                                ; LOE ebx esi edi
.B1.228:                        ; Preds .B1.225 .B1.509         ; Infreq
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_FUELOUT], 0 ;243.22
        jg        .B1.113       ; Prob 84%                      ;243.22
        jmp       .B1.120       ; Prob 100%                     ;243.22
                                ; LOE ebx esi edi
.B1.230:                        ; Preds .B1.109                 ; Infreq
        mov       eax, DWORD PTR [404+esp]                      ;225.14
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_RIBFA] ;226.14
        mov       DWORD PTR [60+eax+edx], ecx                   ;226.14
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_COM_FRAC] ;227.14
        mov       BYTE PTR [56+eax+edx], 1                      ;225.14
        mov       DWORD PTR [52+eax+edx], ecx                   ;227.14
        jmp       .B1.111       ; Prob 100%                     ;227.14
                                ; LOE ebx esi edi
.B1.231:                        ; Preds .B1.105                 ; Infreq
        mov       DWORD PTR [608+esp], 0                        ;201.14
        lea       esi, DWORD PTR [608+esp]                      ;201.14
        mov       DWORD PTR [240+esp], 21                       ;201.14
        lea       eax, DWORD PTR [240+esp]                      ;201.14
        mov       DWORD PTR [244+esp], OFFSET FLAT: __STRLITPACK_33 ;201.14
        push      32                                            ;201.14
        push      eax                                           ;201.14
        push      OFFSET FLAT: __STRLITPACK_108.0.1             ;201.14
        push      -2088435968                                   ;201.14
        push      911                                           ;201.14
        push      esi                                           ;201.14
        call      _for_write_seq_lis                            ;201.14
                                ; LOE ebx esi
.B1.232:                        ; Preds .B1.231                 ; Infreq
        mov       DWORD PTR [632+esp], 0                        ;202.14
        lea       eax, DWORD PTR [272+esp]                      ;202.14
        mov       DWORD PTR [272+esp], 15                       ;202.14
        mov       DWORD PTR [276+esp], OFFSET FLAT: __STRLITPACK_30 ;202.14
        push      32                                            ;202.14
        push      eax                                           ;202.14
        push      OFFSET FLAT: __STRLITPACK_109.0.1             ;202.14
        push      -2088435968                                   ;202.14
        push      911                                           ;202.14
        push      esi                                           ;202.14
        call      _for_write_seq_lis                            ;202.14
                                ; LOE ebx esi
.B1.233:                        ; Preds .B1.232                 ; Infreq
        mov       eax, DWORD PTR [_READ_VEHICLES$JTMP.0.1]      ;202.14
        lea       edx, DWORD PTR [352+esp]                      ;202.14
        mov       DWORD PTR [352+esp], eax                      ;202.14
        push      edx                                           ;202.14
        push      OFFSET FLAT: __STRLITPACK_110.0.1             ;202.14
        push      esi                                           ;202.14
        call      _for_write_seq_lis_xmit                       ;202.14
                                ; LOE ebx esi
.B1.234:                        ; Preds .B1.233                 ; Infreq
        mov       DWORD PTR [316+esp], 29                       ;202.14
        lea       eax, DWORD PTR [316+esp]                      ;202.14
        mov       DWORD PTR [320+esp], OFFSET FLAT: __STRLITPACK_29 ;202.14
        push      eax                                           ;202.14
        push      OFFSET FLAT: __STRLITPACK_111.0.1             ;202.14
        push      esi                                           ;202.14
        call      _for_write_seq_lis_xmit                       ;202.14
                                ; LOE ebx
.B1.235:                        ; Preds .B1.234                 ; Infreq
        push      32                                            ;203.14
        xor       eax, eax                                      ;203.14
        push      eax                                           ;203.14
        push      eax                                           ;203.14
        push      -2088435968                                   ;203.14
        push      eax                                           ;203.14
        push      OFFSET FLAT: __STRLITPACK_112                 ;203.14
        call      _for_stop_core                                ;203.14
                                ; LOE ebx
.B1.510:                        ; Preds .B1.235                 ; Infreq
        add       esp, 96                                       ;203.14
        jmp       .B1.106       ; Prob 100%                     ;203.14
                                ; LOE ebx
.B1.236:                        ; Preds .B1.102 .B1.103         ; Infreq
        mov       DWORD PTR [608+esp], 0                        ;194.13
        lea       edx, DWORD PTR [608+esp]                      ;194.13
        mov       DWORD PTR [272+esp], 29                       ;194.13
        lea       eax, DWORD PTR [272+esp]                      ;194.13
        mov       DWORD PTR [276+esp], OFFSET FLAT: __STRLITPACK_39 ;194.13
        push      32                                            ;194.13
        push      eax                                           ;194.13
        push      OFFSET FLAT: __STRLITPACK_101.0.1             ;194.13
        push      -2088435968                                   ;194.13
        push      911                                           ;194.13
        push      edx                                           ;194.13
        call      _for_write_seq_lis                            ;194.13
                                ; LOE ebx esi edi
.B1.237:                        ; Preds .B1.236                 ; Infreq
        mov       DWORD PTR [632+esp], 0                        ;195.13
        lea       eax, DWORD PTR [304+esp]                      ;195.13
        mov       DWORD PTR [304+esp], 5                        ;195.13
        mov       DWORD PTR [308+esp], OFFSET FLAT: __STRLITPACK_37 ;195.13
        push      32                                            ;195.13
        push      eax                                           ;195.13
        push      OFFSET FLAT: __STRLITPACK_102.0.1             ;195.13
        push      -2088435968                                   ;195.13
        push      911                                           ;195.13
        lea       edx, DWORD PTR [652+esp]                      ;195.13
        push      edx                                           ;195.13
        call      _for_write_seq_lis                            ;195.13
                                ; LOE ebx esi edi
.B1.238:                        ; Preds .B1.237                 ; Infreq
        mov       eax, DWORD PTR [_READ_VEHICLES$JTMP.0.1]      ;195.13
        lea       edx, DWORD PTR [592+esp]                      ;195.13
        mov       DWORD PTR [592+esp], eax                      ;195.13
        push      edx                                           ;195.13
        push      OFFSET FLAT: __STRLITPACK_103.0.1             ;195.13
        lea       ecx, DWORD PTR [664+esp]                      ;195.13
        push      ecx                                           ;195.13
        call      _for_write_seq_lis_xmit                       ;195.13
                                ; LOE ebx esi edi
.B1.239:                        ; Preds .B1.238                 ; Infreq
        mov       DWORD PTR [668+esp], 0                        ;196.13
        lea       eax, DWORD PTR [348+esp]                      ;196.13
        mov       DWORD PTR [348+esp], 23                       ;196.13
        mov       DWORD PTR [352+esp], OFFSET FLAT: __STRLITPACK_35 ;196.13
        push      32                                            ;196.13
        push      eax                                           ;196.13
        push      OFFSET FLAT: __STRLITPACK_104.0.1             ;196.13
        push      -2088435968                                   ;196.13
        push      911                                           ;196.13
        lea       edx, DWORD PTR [688+esp]                      ;196.13
        push      edx                                           ;196.13
        call      _for_write_seq_lis                            ;196.13
                                ; LOE ebx esi edi
.B1.240:                        ; Preds .B1.239                 ; Infreq
        mov       eax, DWORD PTR [496+esp]                      ;196.13
        lea       edx, DWORD PTR [636+esp]                      ;196.13
        mov       DWORD PTR [636+esp], eax                      ;196.13
        push      edx                                           ;196.13
        push      OFFSET FLAT: __STRLITPACK_105.0.1             ;196.13
        lea       ecx, DWORD PTR [700+esp]                      ;196.13
        push      ecx                                           ;196.13
        call      _for_write_seq_lis_xmit                       ;196.13
                                ; LOE ebx esi edi
.B1.241:                        ; Preds .B1.240                 ; Infreq
        mov       eax, DWORD PTR [548+esp]                      ;196.13
        lea       edx, DWORD PTR [656+esp]                      ;196.13
        mov       DWORD PTR [656+esp], eax                      ;196.13
        push      edx                                           ;196.13
        push      OFFSET FLAT: __STRLITPACK_106.0.1             ;196.13
        lea       ecx, DWORD PTR [712+esp]                      ;196.13
        push      ecx                                           ;196.13
        call      _for_write_seq_lis_xmit                       ;196.13
                                ; LOE ebx esi edi
.B1.511:                        ; Preds .B1.241                 ; Infreq
        add       esp, 108                                      ;196.13
                                ; LOE ebx esi edi
.B1.242:                        ; Preds .B1.511                 ; Infreq
        push      32                                            ;197.13
        xor       eax, eax                                      ;197.13
        push      eax                                           ;197.13
        push      eax                                           ;197.13
        push      -2088435968                                   ;197.13
        push      eax                                           ;197.13
        push      OFFSET FLAT: __STRLITPACK_107                 ;197.13
        call      _for_stop_core                                ;197.13
                                ; LOE ebx esi edi
.B1.512:                        ; Preds .B1.242                 ; Infreq
        add       esp, 24                                       ;197.13
        jmp       .B1.104       ; Prob 100%                     ;197.13
                                ; LOE ebx esi edi
.B1.243:                        ; Preds .B1.94                  ; Infreq
        xor       ecx, ecx                                      ;189.7
        mov       DWORD PTR [444+esp], ecx                      ;189.7
        jmp       .B1.98        ; Prob 100%                     ;189.7
                                ; LOE eax edx ebx
.B1.244:                        ; Preds .B1.80                  ; Infreq
        mov       DWORD PTR [608+esp], 0                        ;159.18
        lea       eax, DWORD PTR [584+esp]                      ;159.18
        mov       DWORD PTR [584+esp], 20                       ;159.18
        mov       DWORD PTR [588+esp], OFFSET FLAT: __STRLITPACK_49 ;159.18
        push      32                                            ;159.18
        push      eax                                           ;159.18
        push      OFFSET FLAT: __STRLITPACK_94.0.1              ;159.18
        push      -2088435968                                   ;159.18
        push      911                                           ;159.18
        push      esi                                           ;159.18
        call      _for_write_seq_lis                            ;159.18
                                ; LOE ebx esi edi
.B1.245:                        ; Preds .B1.244                 ; Infreq
        mov       DWORD PTR [632+esp], 0                        ;160.6
        lea       eax, DWORD PTR [616+esp]                      ;160.6
        mov       DWORD PTR [616+esp], 41                       ;160.6
        mov       DWORD PTR [620+esp], OFFSET FLAT: __STRLITPACK_47 ;160.6
        push      32                                            ;160.6
        push      eax                                           ;160.6
        push      OFFSET FLAT: __STRLITPACK_95.0.1              ;160.6
        push      -2088435968                                   ;160.6
        push      911                                           ;160.6
        push      esi                                           ;160.6
        call      _for_write_seq_lis                            ;160.6
                                ; LOE ebx esi edi
.B1.246:                        ; Preds .B1.245                 ; Infreq
        push      32                                            ;161.6
        xor       eax, eax                                      ;161.6
        push      eax                                           ;161.6
        push      eax                                           ;161.6
        push      -2088435968                                   ;161.6
        push      eax                                           ;161.6
        push      OFFSET FLAT: __STRLITPACK_96                  ;161.6
        call      _for_stop_core                                ;161.6
                                ; LOE ebx esi edi
.B1.513:                        ; Preds .B1.246                 ; Infreq
        add       esp, 72                                       ;161.6
        jmp       .B1.81        ; Prob 100%                     ;161.6
                                ; LOE ebx esi edi
.B1.247:                        ; Preds .B1.43                  ; Infreq
        mov       DWORD PTR [_READ_VEHICLES$IVCL2TMP.0.1], ebx  ;116.12
        jmp       .B1.46        ; Prob 100%                     ;116.12
                                ; LOE ebx
.B1.249:                        ; Preds .B1.30 .B1.31           ; Infreq
        mov       ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_IVCT]  ;372.7
        test      ebx, ebx                                      ;372.15
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_IREAD_VEH_FLAG], 0 ;369.7
        jle       .B1.311       ; Prob 24%                      ;372.15
                                ; LOE ebx
.B1.250:                        ; Preds .B1.249                 ; Infreq
        mov       eax, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLOD+12] ;373.10
        test      al, 1                                         ;373.13
        je        .B1.253       ; Prob 60%                      ;373.13
                                ; LOE eax ebx
.B1.251:                        ; Preds .B1.250                 ; Infreq
        mov       edx, eax                                      ;373.32
        and       eax, 1                                        ;373.32
        shr       edx, 1                                        ;373.32
        add       eax, eax                                      ;373.32
        and       edx, 1                                        ;373.32
        shl       edx, 2                                        ;373.32
        or        edx, eax                                      ;373.32
        or        edx, 262144                                   ;373.32
        push      edx                                           ;373.32
        push      DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLOD] ;373.32
        call      _for_dealloc_allocatable                      ;373.32
                                ; LOE ebx
.B1.514:                        ; Preds .B1.251                 ; Infreq
        add       esp, 8                                        ;373.32
                                ; LOE ebx
.B1.252:                        ; Preds .B1.514                 ; Infreq
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLOD], 0 ;373.32
                                ; LOE ebx
.B1.253:                        ; Preds .B1.250 .B1.252         ; Infreq
        xor       edx, edx                                      ;374.10
        test      ebx, ebx                                      ;374.10
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLOD+8], edx ;374.10
        cmovle    ebx, edx                                      ;374.10
        mov       eax, 2                                        ;374.10
        lea       edx, DWORD PTR [372+esp]                      ;374.10
        push      8                                             ;374.10
        push      ebx                                           ;374.10
        push      eax                                           ;374.10
        push      edx                                           ;374.10
        mov       esi, 4                                        ;374.10
        mov       ecx, 1                                        ;374.10
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLOD+12], 133 ;374.10
        lea       edi, DWORD PTR [ebx*4]                        ;374.10
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLOD+4], esi ;374.10
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLOD+16], eax ;374.10
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLOD+32], ecx ;374.10
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLOD+24], ebx ;374.10
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLOD+44], ecx ;374.10
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLOD+36], eax ;374.10
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLOD+28], esi ;374.10
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLOD+40], edi ;374.10
        call      _for_check_mult_overflow                      ;374.10
                                ; LOE eax
.B1.254:                        ; Preds .B1.253                 ; Infreq
        mov       edx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLOD+12] ;374.10
        and       eax, 1                                        ;374.10
        and       edx, 1                                        ;374.10
        shl       eax, 4                                        ;374.10
        add       edx, edx                                      ;374.10
        or        edx, eax                                      ;374.10
        or        edx, 262144                                   ;374.10
        push      edx                                           ;374.10
        push      OFFSET FLAT: _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLOD ;374.10
        push      DWORD PTR [396+esp]                           ;374.10
        call      _for_alloc_allocatable                        ;374.10
                                ; LOE
.B1.516:                        ; Preds .B1.254                 ; Infreq
        add       esp, 28                                       ;374.10
                                ; LOE
.B1.255:                        ; Preds .B1.516                 ; Infreq
        mov       eax, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLOD+36] ;375.10
        test      eax, eax                                      ;375.10
        mov       ecx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLOD+44] ;375.10
        jle       .B1.257       ; Prob 10%                      ;375.10
                                ; LOE eax ecx
.B1.256:                        ; Preds .B1.255                 ; Infreq
        mov       ebx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLOD+40] ;376.10
        mov       esi, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLOD+24] ;375.10
        test      esi, esi                                      ;375.10
        mov       edx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLOD+32] ;375.10
        mov       edi, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLOD] ;376.10
        mov       DWORD PTR [156+esp], ebx                      ;376.10
        jg        .B1.307       ; Prob 50%                      ;375.10
        jmp       .B1.533       ; Prob 100%                     ;375.10
                                ; LOE eax edx ecx esi edi
.B1.257:                        ; Preds .B1.368 .B1.255 .B1.533 .B1.371 ; Infreq
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_IVCT]  ;376.10
        jle       .B1.259       ; Prob 10%                      ;376.10
                                ; LOE eax esi
.B1.258:                        ; Preds .B1.257                 ; Infreq
        mov       edx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLOD+40] ;376.10
        test      esi, esi                                      ;376.10
        mov       DWORD PTR [212+esp], edx                      ;376.10
        mov       edx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLOD] ;376.10
        mov       DWORD PTR [300+esp], edx                      ;376.10
        mov       ecx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLODTMP+32] ;376.10
        mov       ebx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLODTMP] ;376.10
        mov       edi, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLODTMP+40] ;376.10
        mov       edx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLOD+32] ;376.10
        jg        .B1.303       ; Prob 50%                      ;376.10
                                ; LOE eax edx ecx ebx esi edi
.B1.259:                        ; Preds .B1.344 .B1.306 .B1.257 .B1.258 ; Infreq
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_UETIMEFLAGR], 0 ;378.26
        jle       .B1.323       ; Prob 6%                       ;378.26
                                ; LOE
.B1.260:                        ; Preds .B1.259                 ; Infreq
        mov       eax, DWORD PTR [12+ebp]                       ;379.13
        xor       ecx, ecx                                      ;379.13
        cvtsi2ss  xmm0, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIMPERAGG] ;379.44
        mov       edx, DWORD PTR [eax]                          ;379.13
        add       edx, -2                                       ;379.35
        cvtsi2ss  xmm1, edx                                     ;379.35
        divss     xmm1, xmm0                                    ;379.29
        cvttss2si ebx, xmm1                                     ;379.29
        test      ebx, ebx                                      ;379.13
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_UETRAVELTIME+32] ;380.13
        cmovl     ebx, ecx                                      ;379.13
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELTIME+32] ;380.13
        inc       ebx                                           ;379.13
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELTIME+24] ;380.13
        test      edx, edx                                      ;380.13
        mov       DWORD PTR [124+esp], ebx                      ;379.13
        mov       DWORD PTR [208+esp], esi                      ;380.13
        mov       DWORD PTR [228+esp], edi                      ;380.13
        jle       .B1.281       ; Prob 0%                       ;380.13
                                ; LOE edx edi
.B1.261:                        ; Preds .B1.260                 ; Infreq
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELTIME] ;380.13
        cmp       edx, 4                                        ;380.13
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELTIME+40] ;380.13
        mov       ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_UETRAVELTIME] ;380.13
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_UETRAVELTIME+40] ;380.13
        mov       DWORD PTR [156+esp], eax                      ;380.13
        mov       DWORD PTR [esp], ecx                          ;380.13
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELTIME+44] ;380.13
        mov       DWORD PTR [132+esp], ebx                      ;380.13
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_UETRAVELTIME+44] ;380.13
        mov       DWORD PTR [52+esp], esi                       ;380.13
        jl        .B1.321       ; Prob 10%                      ;380.13
                                ; LOE eax edx ecx edi
.B1.262:                        ; Preds .B1.261                 ; Infreq
        mov       esi, DWORD PTR [124+esp]                      ;379.13
        lea       ebx, DWORD PTR [edi*4]                        ;380.13
        mov       DWORD PTR [268+esp], ebx                      ;380.13
        mov       ebx, DWORD PTR [esp]                          ;379.13
        imul      esi, ebx                                      ;379.13
        imul      ebx, eax                                      ;380.13
        mov       edi, DWORD PTR [156+esp]                      ;380.13
        mov       DWORD PTR [220+esp], esi                      ;379.13
        mov       DWORD PTR [212+esp], ebx                      ;380.13
        add       esi, edi                                      ;380.13
        sub       esi, ebx                                      ;380.13
        and       esi, 15                                       ;380.13
        mov       DWORD PTR [236+esp], esi                      ;380.13
        mov       esi, DWORD PTR [268+esp]                      ;380.13
        je        .B1.265       ; Prob 50%                      ;380.13
                                ; LOE eax edx ecx esi
.B1.263:                        ; Preds .B1.262                 ; Infreq
        test      BYTE PTR [236+esp], 3                         ;380.13
        jne       .B1.321       ; Prob 10%                      ;380.13
                                ; LOE eax edx ecx esi
.B1.264:                        ; Preds .B1.263                 ; Infreq
        mov       ebx, DWORD PTR [236+esp]                      ;380.13
        neg       ebx                                           ;380.13
        add       ebx, 16                                       ;380.13
        shr       ebx, 2                                        ;380.13
        mov       DWORD PTR [236+esp], ebx                      ;380.13
                                ; LOE eax edx ecx esi
.B1.265:                        ; Preds .B1.264 .B1.262         ; Infreq
        mov       DWORD PTR [4+esp], ecx                        ;
        mov       ecx, DWORD PTR [236+esp]                      ;380.13
        lea       ebx, DWORD PTR [4+ecx]                        ;380.13
        mov       ecx, DWORD PTR [4+esp]                        ;380.13
        cmp       edx, ebx                                      ;380.13
        jl        .B1.321       ; Prob 10%                      ;380.13
                                ; LOE eax edx ecx esi cl ch
.B1.266:                        ; Preds .B1.265                 ; Infreq
        mov       ebx, edx                                      ;380.13
        neg       esi                                           ;
        mov       DWORD PTR [4+esp], eax                        ;
        mov       eax, DWORD PTR [236+esp]                      ;380.13
        sub       ebx, eax                                      ;380.13
        and       ebx, 3                                        ;380.13
        neg       ebx                                           ;380.13
        mov       DWORD PTR [300+esp], edx                      ;
        add       ebx, edx                                      ;380.13
        mov       edx, DWORD PTR [220+esp]                      ;
        mov       edi, DWORD PTR [228+esp]                      ;
        add       esi, DWORD PTR [156+esp]                      ;
        mov       DWORD PTR [308+esp], ebx                      ;380.13
        lea       ebx, DWORD PTR [edx+edi*4]                    ;
        mov       edi, DWORD PTR [124+esp]                      ;379.13
        add       esi, ebx                                      ;
        sub       esi, DWORD PTR [212+esp]                      ;
        mov       DWORD PTR [268+esp], esi                      ;
        mov       esi, DWORD PTR [208+esp]                      ;
        mov       edx, DWORD PTR [52+esp]                       ;379.13
        imul      edi, edx                                      ;379.13
        imul      edx, ecx                                      ;
        mov       DWORD PTR [340+esp], edx                      ;
        lea       ebx, DWORD PTR [esi*4]                        ;
        neg       ebx                                           ;
        lea       esi, DWORD PTR [edi+esi*4]                    ;
        add       ebx, DWORD PTR [132+esp]                      ;
        add       esi, ebx                                      ;
        sub       esi, edx                                      ;
        mov       DWORD PTR [364+esp], esi                      ;
        test      eax, eax                                      ;380.13
        mov       DWORD PTR [356+esp], ebx                      ;
        mov       DWORD PTR [348+esp], edi                      ;379.13
        mov       eax, DWORD PTR [4+esp]                        ;380.13
        mov       esi, DWORD PTR [268+esp]                      ;380.13
        mov       edx, DWORD PTR [300+esp]                      ;380.13
        jbe       .B1.270       ; Prob 10%                      ;380.13
                                ; LOE eax edx ecx esi al dl cl ah dh ch
.B1.267:                        ; Preds .B1.266                 ; Infreq
        xor       ebx, ebx                                      ;
        mov       DWORD PTR [300+esp], edx                      ;
        mov       DWORD PTR [4+esp], eax                        ;
        movss     xmm1, DWORD PTR [_2il0floatpacket.3]          ;
        mov       edx, ebx                                      ;
        mov       ebx, DWORD PTR [364+esp]                      ;
        mov       edi, DWORD PTR [236+esp]                      ;
                                ; LOE edx ecx ebx esi edi xmm1
.B1.268:                        ; Preds .B1.268 .B1.267         ; Infreq
        movss     xmm0, DWORD PTR [ebx+edx*4]                   ;380.13
        mulss     xmm0, xmm1                                    ;380.60
        cvttss2si eax, xmm0                                     ;380.13
        mov       DWORD PTR [esi+edx*4], eax                    ;380.13
        inc       edx                                           ;380.13
        cmp       edx, edi                                      ;380.13
        jb        .B1.268       ; Prob 82%                      ;380.13
                                ; LOE edx ecx ebx esi edi xmm1
.B1.269:                        ; Preds .B1.268                 ; Infreq
        mov       DWORD PTR [236+esp], edi                      ;
        mov       eax, DWORD PTR [4+esp]                        ;
        mov       edx, DWORD PTR [300+esp]                      ;
                                ; LOE eax edx ecx esi al dl ah dh
.B1.270:                        ; Preds .B1.266 .B1.269         ; Infreq
        mov       edi, DWORD PTR [348+esp]                      ;380.13
        sub       edi, DWORD PTR [340+esp]                      ;380.13
        mov       ebx, DWORD PTR [356+esp]                      ;380.13
        add       ebx, edi                                      ;380.13
        mov       edi, DWORD PTR [208+esp]                      ;380.13
        add       edi, DWORD PTR [236+esp]                      ;380.13
        movaps    xmm0, XMMWORD PTR [_2il0floatpacket.8]        ;380.60
        lea       ebx, DWORD PTR [ebx+edi*4]                    ;380.13
        test      bl, 15                                        ;380.13
        je        .B1.274       ; Prob 60%                      ;380.13
                                ; LOE eax edx ecx esi al dl ah dh xmm0
.B1.271:                        ; Preds .B1.270                 ; Infreq
        mov       DWORD PTR [300+esp], edx                      ;
        mov       edx, DWORD PTR [308+esp]                      ;
        mov       ebx, DWORD PTR [364+esp]                      ;
        mov       edi, DWORD PTR [236+esp]                      ;
                                ; LOE eax edx ecx ebx esi edi xmm0
.B1.272:                        ; Preds .B1.271 .B1.272         ; Infreq
        movups    xmm1, XMMWORD PTR [ebx+edi*4]                 ;380.13
        mulps     xmm1, xmm0                                    ;380.60
        cvttps2dq xmm2, xmm1                                    ;380.13
        movdqa    XMMWORD PTR [esi+edi*4], xmm2                 ;380.13
        add       edi, 4                                        ;380.13
        cmp       edi, edx                                      ;380.13
        jb        .B1.272       ; Prob 82%                      ;380.13
        jmp       .B1.276       ; Prob 100%                     ;380.13
                                ; LOE eax edx ecx ebx esi edi xmm0
.B1.274:                        ; Preds .B1.270                 ; Infreq
        mov       DWORD PTR [300+esp], edx                      ;
        mov       edx, DWORD PTR [308+esp]                      ;
        mov       ebx, DWORD PTR [364+esp]                      ;
        mov       edi, DWORD PTR [236+esp]                      ;
                                ; LOE eax edx ecx ebx esi edi xmm0
.B1.275:                        ; Preds .B1.274 .B1.275         ; Infreq
        movaps    xmm1, XMMWORD PTR [ebx+edi*4]                 ;380.13
        mulps     xmm1, xmm0                                    ;380.60
        cvttps2dq xmm2, xmm1                                    ;380.13
        movdqa    XMMWORD PTR [esi+edi*4], xmm2                 ;380.13
        add       edi, 4                                        ;380.13
        cmp       edi, edx                                      ;380.13
        jb        .B1.275       ; Prob 82%                      ;380.13
                                ; LOE eax edx ecx ebx esi edi xmm0
.B1.276:                        ; Preds .B1.272 .B1.275         ; Infreq
        mov       DWORD PTR [308+esp], edx                      ;
        mov       edx, DWORD PTR [300+esp]                      ;
                                ; LOE eax edx ecx
.B1.277:                        ; Preds .B1.276 .B1.321         ; Infreq
        cmp       edx, DWORD PTR [308+esp]                      ;380.13
        jbe       .B1.281       ; Prob 10%                      ;380.13
                                ; LOE eax edx ecx
.B1.278:                        ; Preds .B1.277                 ; Infreq
        neg       eax                                           ;
        neg       ecx                                           ;
        mov       ebx, DWORD PTR [124+esp]                      ;
        add       eax, ebx                                      ;
        add       ecx, ebx                                      ;
        imul      eax, DWORD PTR [esp]                          ;
        imul      ecx, DWORD PTR [52+esp]                       ;
        movss     xmm1, DWORD PTR [_2il0floatpacket.3]          ;
        add       eax, DWORD PTR [156+esp]                      ;
        add       ecx, DWORD PTR [132+esp]                      ;
        mov       esi, DWORD PTR [308+esp]                      ;
                                ; LOE eax edx ecx esi xmm1
.B1.279:                        ; Preds .B1.279 .B1.278         ; Infreq
        movss     xmm0, DWORD PTR [ecx+esi*4]                   ;380.13
        mulss     xmm0, xmm1                                    ;380.60
        cvttss2si ebx, xmm0                                     ;380.13
        mov       DWORD PTR [eax+esi*4], ebx                    ;380.13
        inc       esi                                           ;380.13
        cmp       esi, edx                                      ;380.13
        jb        .B1.279       ; Prob 82%                      ;380.13
                                ; LOE eax edx ecx esi xmm1
.B1.281:                        ; Preds .B1.279 .B1.260 .B1.277 ; Infreq
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+36] ;381.13
        test      eax, eax                                      ;381.13
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+44] ;381.13
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_UETRAVELPENALTY+56] ;381.13
        mov       DWORD PTR [208+esp], eax                      ;381.13
        jle       .B1.283       ; Prob 11%                      ;381.13
                                ; LOE edx esi
.B1.282:                        ; Preds .B1.281                 ; Infreq
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+32] ;381.13
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE] ;381.13
        mov       DWORD PTR [4+esp], ecx                        ;381.13
        mov       DWORD PTR [228+esp], edi                      ;381.13
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+40] ;381.13
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_UETRAVELPENALTY] ;381.13
        mov       DWORD PTR [220+esp], ecx                      ;381.13
        mov       DWORD PTR [236+esp], edi                      ;381.13
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_UETRAVELPENALTY+52] ;381.13
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_UETRAVELPENALTY+44] ;381.13
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+24] ;381.13
        test      ebx, ebx                                      ;381.13
        mov       DWORD PTR [212+esp], ecx                      ;381.13
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_UETRAVELPENALTY+32] ;381.13
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_UETRAVELPENALTY+40] ;381.13
        mov       DWORD PTR [300+esp], ebx                      ;381.13
        mov       DWORD PTR [esp], edi                          ;381.13
        jg        .B1.296       ; Prob 50%                      ;381.13
                                ; LOE eax edx ecx ebx esi bl bh
.B1.283:                        ; Preds .B1.319 .B1.302 .B1.522 .B1.281 .B1.282
                                ;                               ; Infreq
        push      DWORD PTR [12+ebp]                            ;385.16
        push      OFFSET FLAT: __NLITPACK_4.0.1                 ;385.16
        call      _TDSP_MAIN                                    ;385.16
                                ; LOE
.B1.284:                        ; Preds .B1.283                 ; Infreq
        push      OFFSET FLAT: __NLITPACK_5.0.1                 ;386.16
        push      OFFSET FLAT: _DYNUST_MAIN_MODULE_mp_IVCT      ;386.16
        push      DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLOD] ;386.16
        call      _IMSLSORT                                     ;386.16
                                ; LOE
.B1.517:                        ; Preds .B1.284                 ; Infreq
        add       esp, 20                                       ;386.16
                                ; LOE
.B1.285:                        ; Preds .B1.517                 ; Infreq
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOF_MASTER_DESTINATIONS] ;390.11
        test      esi, esi                                      ;390.11
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_CALLKSPREADVEH], -1 ;389.11
        jle       .B1.311       ; Prob 2%                       ;390.11
                                ; LOE esi
.B1.286:                        ; Preds .B1.285                 ; Infreq
        mov       edx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLOD+44] ;410.27
        mov       edi, 4                                        ;
        mov       ebx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLOD] ;410.18
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_IVCT]  ;410.43
        mov       DWORD PTR [124+esp], edx                      ;410.27
        mov       DWORD PTR [4+esp], ebx                        ;410.18
        mov       ebx, 1                                        ;
        mov       edx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLOD+40] ;410.27
        mov       DWORD PTR [132+esp], ebx                      ;
        mov       DWORD PTR [236+esp], ecx                      ;
        mov       eax, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLOD+32] ;410.18
        mov       DWORD PTR [212+esp], edi                      ;
        mov       DWORD PTR [52+esp], edx                       ;
        mov       DWORD PTR [esp], esi                          ;
        mov       ecx, ebx                                      ;
                                ; LOE eax ecx ebx
.B1.287:                        ; Preds .B1.294 .B1.286         ; Infreq
        mov       edi, DWORD PTR [124+esp]                      ;410.18
        lea       edx, DWORD PTR [eax*4]                        ;410.18
        mov       esi, DWORD PTR [52+esp]                       ;410.18
        neg       edx                                           ;410.18
        imul      edi, esi                                      ;410.18
        add       edx, DWORD PTR [212+esp]                      ;410.18
        sub       edi, esi                                      ;410.18
        sub       edx, edi                                      ;410.18
        mov       esi, DWORD PTR [4+esp]                        ;410.43
        cmp       ecx, DWORD PTR [esi+edx]                      ;410.43
        jne       .B1.294       ; Prob 10%                      ;410.43
                                ; LOE eax ecx ebx
.B1.288:                        ; Preds .B1.287                 ; Infreq
        cmp       ebx, DWORD PTR [236+esp]                      ;410.60
        jg        .B1.294       ; Prob 20%                      ;410.60
                                ; LOE eax ecx ebx
.B1.289:                        ; Preds .B1.288                 ; Infreq
        mov       DWORD PTR [132+esp], ecx                      ;
        lea       edi, DWORD PTR [eax*4]                        ;
        mov       eax, DWORD PTR [124+esp]                      ;
        imul      eax, DWORD PTR [52+esp]                       ;
        mov       ecx, DWORD PTR [4+esp]                        ;
        mov       edx, DWORD PTR [52+esp]                       ;
        mov       DWORD PTR [220+esp], edi                      ;
                                ; LOE eax edx ecx ebx
.B1.290:                        ; Preds .B1.292 .B1.289         ; Infreq
        sub       ecx, eax                                      ;412.25
        lea       esi, DWORD PTR [edx+edx]                      ;412.49
        mov       eax, DWORD PTR [220+esp]                      ;412.25
        sub       esi, eax                                      ;412.25
        add       esi, ecx                                      ;412.25
        sub       edx, eax                                      ;412.25
        mov       edi, DWORD PTR [212+esp]                      ;412.25
        add       ecx, edx                                      ;412.25
        add       ecx, DWORD PTR [212+esp]                      ;412.25
        lea       edx, DWORD PTR [208+esp]                      ;412.25
        mov       eax, OFFSET FLAT: __NLITPACK_2.0.1            ;412.25
        add       edi, esi                                      ;412.25
        mov       DWORD PTR [228+esp], edi                      ;412.25
        push      edx                                           ;412.25
        mov       edi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;412.25
        neg       edi                                           ;412.25
        add       edi, DWORD PTR [esi+ebx*4]                    ;412.25
        shl       edi, 5                                        ;412.25
        push      eax                                           ;412.25
        mov       esi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;412.25
        push      ecx                                           ;412.25
        push      eax                                           ;412.25
        push      OFFSET FLAT: _DYNUST_MAIN_MODULE_mp_IPINIT    ;412.25
        lea       esi, DWORD PTR [28+edi+esi]                   ;412.25
        push      esi                                           ;412.25
        push      DWORD PTR [252+esp]                           ;412.25
        call      _RETRIEVE_VEH_PATH_ASTAR                      ;412.25
                                ; LOE ebx
.B1.518:                        ; Preds .B1.290                 ; Infreq
        add       esp, 28                                       ;412.25
                                ; LOE ebx
.B1.291:                        ; Preds .B1.518                 ; Infreq
        inc       ebx                                           ;413.20
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_IVCT]  ;414.20
        cmp       ebx, eax                                      ;414.29
        mov       DWORD PTR [236+esp], eax                      ;414.20
        jg        .B1.311       ; Prob 4%                       ;414.29
                                ; LOE ebx
.B1.292:                        ; Preds .B1.291                 ; Infreq
        mov       eax, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLOD+44] ;410.27
        mov       edx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLOD+40] ;410.27
        mov       DWORD PTR [124+esp], eax                      ;410.27
        imul      eax, edx                                      ;410.18
        mov       esi, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLOD+32] ;410.18
        mov       DWORD PTR [156+esp], esi                      ;410.18
        mov       ecx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLOD] ;410.18
        lea       edi, DWORD PTR [esi*4]                        ;410.18
        mov       DWORD PTR [220+esp], edi                      ;410.18
        lea       esi, DWORD PTR [ebx*4]                        ;388.11
        mov       DWORD PTR [212+esp], esi                      ;388.11
        sub       esi, edi                                      ;410.18
        mov       edi, eax                                      ;410.18
        sub       edi, edx                                      ;410.18
        sub       esi, edi                                      ;410.18
        mov       edi, DWORD PTR [132+esp]                      ;410.43
        cmp       edi, DWORD PTR [ecx+esi]                      ;410.43
        je        .B1.290       ; Prob 82%                      ;410.43
                                ; LOE eax edx ecx ebx edi
.B1.293:                        ; Preds .B1.292                 ; Infreq
        mov       DWORD PTR [4+esp], ecx                        ;
        mov       DWORD PTR [52+esp], edx                       ;
        mov       ecx, edi                                      ;
        mov       eax, DWORD PTR [156+esp]                      ;
                                ; LOE eax ecx ebx
.B1.294:                        ; Preds .B1.293 .B1.288 .B1.287 ; Infreq
        inc       ecx                                           ;418.11
        cmp       ecx, DWORD PTR [esp]                          ;418.11
        jle       .B1.287       ; Prob 82%                      ;418.11
        jmp       .B1.311       ; Prob 100%                     ;418.11
                                ; LOE eax ecx ebx
.B1.296:                        ; Preds .B1.282                 ; Infreq
        imul      esi, DWORD PTR [220+esp]                      ;
        imul      edx, DWORD PTR [212+esp]                      ;
        mov       edi, ebx                                      ;
        shr       edi, 31                                       ;
        add       edi, ebx                                      ;
        mov       ebx, DWORD PTR [4+esp]                        ;
        shl       ebx, 3                                        ;
        sar       edi, 1                                        ;
        mov       DWORD PTR [308+esp], edi                      ;
        mov       edi, DWORD PTR [228+esp]                      ;
        sub       edi, ebx                                      ;
        sub       ebx, esi                                      ;
        add       edi, ebx                                      ;
        add       edi, esi                                      ;
        mov       DWORD PTR [156+esp], edi                      ;
        mov       edi, DWORD PTR [124+esp]                      ;379.13
        imul      edi, ecx                                      ;379.13
        imul      ecx, DWORD PTR [esp]                          ;
        mov       ebx, DWORD PTR [236+esp]                      ;
        mov       esi, ebx                                      ;
        shl       eax, 2                                        ;
        sub       esi, eax                                      ;
        sub       eax, edx                                      ;
        add       esi, eax                                      ;
        add       edx, edi                                      ;
        mov       DWORD PTR [52+esp], ecx                       ;
        add       esi, edx                                      ;
        sub       esi, DWORD PTR [52+esp]                       ;
        sub       ecx, edi                                      ;
        mov       DWORD PTR [132+esp], 0                        ;
        sub       ebx, ecx                                      ;
        mov       DWORD PTR [268+esp], esi                      ;
        mov       DWORD PTR [236+esp], ebx                      ;
        mov       esi, DWORD PTR [132+esp]                      ;
                                ; LOE esi
.B1.297:                        ; Preds .B1.302 .B1.319 .B1.296 ; Infreq
        cmp       DWORD PTR [308+esp], 0                        ;381.13
        jbe       .B1.318       ; Prob 10%                      ;381.13
                                ; LOE esi
.B1.298:                        ; Preds .B1.297                 ; Infreq
        mov       edi, DWORD PTR [212+esp]                      ;
        xor       edx, edx                                      ;
        imul      edi, esi                                      ;
        mov       ebx, DWORD PTR [220+esp]                      ;
        imul      ebx, esi                                      ;
        mov       eax, DWORD PTR [268+esp]                      ;
        mov       ecx, DWORD PTR [156+esp]                      ;
        mov       DWORD PTR [132+esp], esi                      ;
        add       eax, edi                                      ;
        add       edi, DWORD PTR [236+esp]                      ;
        add       ecx, ebx                                      ;
        add       ebx, DWORD PTR [228+esp]                      ;
        mov       esi, edx                                      ;
        mov       DWORD PTR [esp], edi                          ;
                                ; LOE eax edx ecx ebx esi
.B1.299:                        ; Preds .B1.299 .B1.298         ; Infreq
        mov       edi, DWORD PTR [esp]                          ;381.13
        mov       edi, DWORD PTR [edi+edx*8]                    ;381.13
        mov       DWORD PTR [esi+ebx], edi                      ;381.13
        mov       edi, DWORD PTR [4+eax+edx*8]                  ;381.13
        inc       edx                                           ;381.13
        mov       DWORD PTR [8+esi+ecx], edi                    ;381.13
        add       esi, 16                                       ;381.13
        cmp       edx, DWORD PTR [308+esp]                      ;381.13
        jb        .B1.299       ; Prob 64%                      ;381.13
                                ; LOE eax edx ecx ebx esi
.B1.300:                        ; Preds .B1.299                 ; Infreq
        mov       esi, DWORD PTR [132+esp]                      ;
        lea       ebx, DWORD PTR [1+edx+edx]                    ;381.13
                                ; LOE ebx esi
.B1.301:                        ; Preds .B1.300 .B1.318         ; Infreq
        lea       eax, DWORD PTR [-1+ebx]                       ;381.13
        cmp       eax, DWORD PTR [300+esp]                      ;381.13
        jae       .B1.319       ; Prob 10%                      ;381.13
                                ; LOE ebx esi
.B1.302:                        ; Preds .B1.301                 ; Infreq
        mov       edi, DWORD PTR [268+esp]                      ;381.13
        mov       ecx, DWORD PTR [212+esp]                      ;381.13
        imul      ecx, esi                                      ;381.13
        mov       edx, DWORD PTR [220+esp]                      ;381.13
        lea       eax, DWORD PTR [edi+ebx*4]                    ;381.13
        imul      edx, esi                                      ;381.13
        inc       esi                                           ;381.13
        mov       edi, DWORD PTR [156+esp]                      ;381.13
        cmp       esi, DWORD PTR [208+esp]                      ;381.13
        lea       edi, DWORD PTR [edi+ebx*8]                    ;381.13
        mov       ebx, DWORD PTR [-4+eax+ecx]                   ;381.13
        mov       DWORD PTR [-8+edi+edx], ebx                   ;381.13
        jb        .B1.297       ; Prob 82%                      ;381.13
        jmp       .B1.283       ; Prob 100%                     ;381.13
                                ; LOE esi
.B1.303:                        ; Preds .B1.258                 ; Infreq
        mov       DWORD PTR [208+esp], edi                      ;
        xor       edi, edi                                      ;
        shl       ecx, 2                                        ;
        shl       edx, 2                                        ;
        sub       ebx, ecx                                      ;
        xor       ecx, ecx                                      ;
        mov       DWORD PTR [132+esp], edi                      ;
        sub       DWORD PTR [300+esp], edx                      ;
        lea       edx, DWORD PTR [esi*4]                        ;
        mov       DWORD PTR [52+esp], ecx                       ;
        mov       DWORD PTR [220+esp], ebx                      ;
        mov       edi, ecx                                      ;
        mov       DWORD PTR [228+esp], esi                      ;
        mov       DWORD PTR [156+esp], edx                      ;
        mov       DWORD PTR [4+esp], eax                        ;
        mov       ebx, ecx                                      ;
        mov       esi, DWORD PTR [132+esp]                      ;
                                ; LOE ebx esi edi
.B1.304:                        ; Preds .B1.306 .B1.344 .B1.303 ; Infreq
        cmp       DWORD PTR [228+esp], 24                       ;376.10
        jle       .B1.325       ; Prob 0%                       ;376.10
                                ; LOE ebx esi edi
.B1.305:                        ; Preds .B1.304                 ; Infreq
        mov       edx, DWORD PTR [220+esp]                      ;376.10
        mov       eax, DWORD PTR [300+esp]                      ;376.10
        push      DWORD PTR [156+esp]                           ;376.10
        lea       edx, DWORD PTR [4+edi+edx]                    ;376.10
        push      edx                                           ;376.10
        lea       eax, DWORD PTR [4+ebx+eax]                    ;376.10
        push      eax                                           ;376.10
        call      __intel_fast_memcpy                           ;376.10
                                ; LOE ebx esi edi
.B1.519:                        ; Preds .B1.305                 ; Infreq
        add       esp, 12                                       ;376.10
                                ; LOE ebx esi edi
.B1.306:                        ; Preds .B1.341 .B1.519         ; Infreq
        inc       esi                                           ;376.10
        add       edi, DWORD PTR [208+esp]                      ;376.10
        add       ebx, DWORD PTR [212+esp]                      ;376.10
        cmp       esi, DWORD PTR [4+esp]                        ;376.10
        jb        .B1.304       ; Prob 82%                      ;376.10
        jmp       .B1.259       ; Prob 100%                     ;376.10
                                ; LOE ebx esi edi
.B1.307:                        ; Preds .B1.256                 ; Infreq
        imul      ecx, DWORD PTR [156+esp]                      ;
        pxor      xmm0, xmm0                                    ;375.10
        mov       DWORD PTR [4+esp], eax                        ;
        xor       eax, eax                                      ;
        mov       DWORD PTR [esp], eax                          ;
        mov       eax, edi                                      ;
        shl       edx, 2                                        ;
        mov       ebx, ecx                                      ;
        sub       eax, edx                                      ;
        sub       edx, ecx                                      ;
        add       eax, edx                                      ;
        neg       ebx                                           ;
        mov       DWORD PTR [208+esp], edi                      ;
        add       edi, ecx                                      ;
        add       ecx, eax                                      ;
        lea       edx, DWORD PTR [esi*4]                        ;
        add       ebx, edi                                      ;
        mov       DWORD PTR [132+esp], ebx                      ;
        mov       ebx, DWORD PTR [esp]                          ;
        mov       eax, DWORD PTR [4+esp]                        ;
        mov       edi, ebx                                      ;
        mov       DWORD PTR [52+esp], edx                       ;
        mov       DWORD PTR [124+esp], ecx                      ;
                                ; LOE ebx esi edi
.B1.308:                        ; Preds .B1.310 .B1.370 .B1.367 .B1.307 ; Infreq
        cmp       esi, 24                                       ;375.10
        jle       .B1.352       ; Prob 0%                       ;375.10
                                ; LOE ebx esi edi
.B1.309:                        ; Preds .B1.308                 ; Infreq
        mov       eax, DWORD PTR [124+esp]                      ;375.10
        push      DWORD PTR [52+esp]                            ;375.10
        push      0                                             ;375.10
        lea       edx, DWORD PTR [eax+ebx]                      ;375.10
        push      edx                                           ;375.10
        call      __intel_fast_memset                           ;375.10
                                ; LOE ebx esi edi
.B1.520:                        ; Preds .B1.309                 ; Infreq
        add       esp, 12                                       ;375.10
                                ; LOE ebx esi edi
.B1.310:                        ; Preds .B1.520                 ; Infreq
        inc       edi                                           ;375.10
        add       ebx, DWORD PTR [156+esp]                      ;375.10
        cmp       edi, DWORD PTR [4+esp]                        ;375.10
        jb        .B1.308       ; Prob 82%                      ;375.10
        jmp       .B1.368       ; Prob 100%                     ;375.10
                                ; LOE ebx esi edi
.B1.311:                        ; Preds .B1.291 .B1.294 .B1.249 .B1.285 ; Infreq
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_IVCT], 0    ;421.8
        cmp       DWORD PTR [380+esp], 0                        ;434.4
        jle       .B1.4         ; Prob 2%                       ;434.4
                                ; LOE
.B1.312:                        ; Preds .B1.311                 ; Infreq
        mov       esi, DWORD PTR [380+esp]                      ;
        mov       ebx, 1                                        ;
                                ; LOE ebx esi
.B1.313:                        ; Preds .B1.314 .B1.312         ; Infreq
        push      OFFSET FLAT: __NLITPACK_7.0.1                 ;435.16
        push      OFFSET FLAT: __NLITPACK_2.0.1                 ;435.16
        push      OFFSET FLAT: __NLITPACK_6.0.1                 ;435.16
        lea       eax, DWORD PTR [_READ_VEHICLES$IDARRAY.0.1+3999996+ebx*4] ;435.16
        push      eax                                           ;435.16
        push      eax                                           ;435.16
        push      OFFSET FLAT: _DYNUST_MAIN_MODULE_mp_TOTALVEH  ;435.16
        call      _WRITE_VEHICLES                               ;435.16
                                ; LOE ebx esi
.B1.521:                        ; Preds .B1.313                 ; Infreq
        add       esp, 24                                       ;435.16
                                ; LOE ebx esi
.B1.314:                        ; Preds .B1.521                 ; Infreq
        inc       ebx                                           ;436.7
        cmp       ebx, esi                                      ;436.7
        jle       .B1.313       ; Prob 99%                      ;436.7
        jmp       .B1.4         ; Prob 100%                     ;436.7
                                ; LOE ebx esi
.B1.318:                        ; Preds .B1.297                 ; Infreq
        mov       ebx, 1                                        ;
        jmp       .B1.301       ; Prob 100%                     ;
                                ; LOE ebx esi
.B1.319:                        ; Preds .B1.301                 ; Infreq
        inc       esi                                           ;381.13
        cmp       esi, DWORD PTR [208+esp]                      ;381.13
        jb        .B1.297       ; Prob 82%                      ;381.13
        jmp       .B1.283       ; Prob 100%                     ;381.13
                                ; LOE esi
.B1.321:                        ; Preds .B1.263 .B1.261 .B1.265 ; Infreq
        xor       ebx, ebx                                      ;380.13
        mov       DWORD PTR [308+esp], ebx                      ;380.13
        jmp       .B1.277       ; Prob 100%                     ;380.13
                                ; LOE eax edx ecx
.B1.323:                        ; Preds .B1.259                 ; Infreq
        push      DWORD PTR [12+ebp]                            ;383.18
        call      _CAL_PENALTY                                  ;383.18
                                ; LOE
.B1.522:                        ; Preds .B1.323                 ; Infreq
        add       esp, 4                                        ;383.18
        jmp       .B1.283       ; Prob 100%                     ;383.18
                                ; LOE
.B1.325:                        ; Preds .B1.304                 ; Infreq
        cmp       DWORD PTR [228+esp], 4                        ;376.10
        jl        .B1.346       ; Prob 10%                      ;376.10
                                ; LOE ebx esi edi
.B1.326:                        ; Preds .B1.325                 ; Infreq
        mov       eax, DWORD PTR [300+esp]                      ;376.10
        lea       eax, DWORD PTR [4+ebx+eax]                    ;376.10
        and       eax, 15                                       ;376.10
        je        .B1.329       ; Prob 50%                      ;376.10
                                ; LOE eax ebx esi edi
.B1.327:                        ; Preds .B1.326                 ; Infreq
        test      al, 3                                         ;376.10
        jne       .B1.346       ; Prob 10%                      ;376.10
                                ; LOE eax ebx esi edi
.B1.328:                        ; Preds .B1.327                 ; Infreq
        neg       eax                                           ;376.10
        add       eax, 16                                       ;376.10
        shr       eax, 2                                        ;376.10
                                ; LOE eax ebx esi edi
.B1.329:                        ; Preds .B1.328 .B1.326         ; Infreq
        lea       edx, DWORD PTR [4+eax]                        ;376.10
        cmp       edx, DWORD PTR [228+esp]                      ;376.10
        jg        .B1.346       ; Prob 10%                      ;376.10
                                ; LOE eax ebx esi edi
.B1.330:                        ; Preds .B1.329                 ; Infreq
        mov       ecx, DWORD PTR [228+esp]                      ;376.10
        mov       edx, ecx                                      ;376.10
        sub       edx, eax                                      ;376.10
        and       edx, 3                                        ;376.10
        neg       edx                                           ;376.10
        add       edx, ecx                                      ;376.10
        mov       ecx, DWORD PTR [300+esp]                      ;
        test      eax, eax                                      ;376.10
        lea       ecx, DWORD PTR [ecx+ebx]                      ;
        mov       DWORD PTR [268+esp], ecx                      ;
        mov       ecx, DWORD PTR [220+esp]                      ;
        lea       ecx, DWORD PTR [ecx+edi]                      ;
        mov       DWORD PTR [236+esp], ecx                      ;
        jbe       .B1.334       ; Prob 10%                      ;376.10
                                ; LOE eax edx ebx esi edi
.B1.331:                        ; Preds .B1.330                 ; Infreq
        xor       ecx, ecx                                      ;
        mov       DWORD PTR [52+esp], ebx                       ;
        mov       DWORD PTR [124+esp], edi                      ;
        mov       DWORD PTR [132+esp], esi                      ;
        mov       ebx, ecx                                      ;
        mov       esi, DWORD PTR [236+esp]                      ;
        mov       edi, DWORD PTR [268+esp]                      ;
                                ; LOE eax edx ebx esi edi
.B1.332:                        ; Preds .B1.332 .B1.331         ; Infreq
        mov       ecx, DWORD PTR [4+esi+ebx*4]                  ;376.10
        mov       DWORD PTR [4+edi+ebx*4], ecx                  ;376.10
        inc       ebx                                           ;376.10
        cmp       ebx, eax                                      ;376.10
        jb        .B1.332       ; Prob 82%                      ;376.10
                                ; LOE eax edx ebx esi edi
.B1.333:                        ; Preds .B1.332                 ; Infreq
        mov       ebx, DWORD PTR [52+esp]                       ;
        mov       edi, DWORD PTR [124+esp]                      ;
        mov       esi, DWORD PTR [132+esp]                      ;
                                ; LOE eax edx ebx esi edi
.B1.334:                        ; Preds .B1.330 .B1.333         ; Infreq
        mov       DWORD PTR [esp], edx                          ;
        mov       edx, DWORD PTR [220+esp]                      ;376.10
        add       edx, edi                                      ;376.10
        lea       edx, DWORD PTR [4+edx+eax*4]                  ;376.10
        test      dl, 15                                        ;376.10
        mov       edx, DWORD PTR [esp]                          ;376.10
        je        .B1.338       ; Prob 60%                      ;376.10
                                ; LOE eax edx ebx esi edi dl dh
.B1.335:                        ; Preds .B1.334                 ; Infreq
        mov       DWORD PTR [132+esp], esi                      ;
        mov       esi, DWORD PTR [236+esp]                      ;
        mov       ecx, DWORD PTR [268+esp]                      ;
                                ; LOE eax edx ecx ebx esi edi
.B1.336:                        ; Preds .B1.336 .B1.335         ; Infreq
        movdqu    xmm0, XMMWORD PTR [4+esi+eax*4]               ;376.10
        movdqa    XMMWORD PTR [4+ecx+eax*4], xmm0               ;376.10
        add       eax, 4                                        ;376.10
        cmp       eax, edx                                      ;376.10
        jb        .B1.336       ; Prob 82%                      ;376.10
        jmp       .B1.340       ; Prob 100%                     ;376.10
                                ; LOE eax edx ecx ebx esi edi
.B1.338:                        ; Preds .B1.334                 ; Infreq
        mov       DWORD PTR [132+esp], esi                      ;
        mov       esi, DWORD PTR [236+esp]                      ;
        mov       ecx, DWORD PTR [268+esp]                      ;
                                ; LOE eax edx ecx ebx esi edi
.B1.339:                        ; Preds .B1.339 .B1.338         ; Infreq
        movdqa    xmm0, XMMWORD PTR [4+esi+eax*4]               ;376.10
        movdqa    XMMWORD PTR [4+ecx+eax*4], xmm0               ;376.10
        add       eax, 4                                        ;376.10
        cmp       eax, edx                                      ;376.10
        jb        .B1.339       ; Prob 82%                      ;376.10
                                ; LOE eax edx ecx ebx esi edi
.B1.340:                        ; Preds .B1.336 .B1.339         ; Infreq
        mov       esi, DWORD PTR [132+esp]                      ;
                                ; LOE edx ebx esi edi
.B1.341:                        ; Preds .B1.340 .B1.346         ; Infreq
        cmp       edx, DWORD PTR [228+esp]                      ;376.10
        jae       .B1.306       ; Prob 10%                      ;376.10
                                ; LOE edx ebx esi edi
.B1.342:                        ; Preds .B1.341                 ; Infreq
        mov       ecx, DWORD PTR [220+esp]                      ;
        mov       eax, DWORD PTR [300+esp]                      ;
        mov       DWORD PTR [124+esp], edi                      ;
        mov       DWORD PTR [132+esp], esi                      ;
        add       ecx, edi                                      ;
        add       eax, ebx                                      ;
        mov       esi, ecx                                      ;
        mov       edi, DWORD PTR [228+esp]                      ;
                                ; LOE eax edx ebx esi edi
.B1.343:                        ; Preds .B1.343 .B1.342         ; Infreq
        mov       ecx, DWORD PTR [4+esi+edx*4]                  ;376.10
        mov       DWORD PTR [4+eax+edx*4], ecx                  ;376.10
        inc       edx                                           ;376.10
        cmp       edx, edi                                      ;376.10
        jb        .B1.343       ; Prob 82%                      ;376.10
                                ; LOE eax edx ebx esi edi
.B1.344:                        ; Preds .B1.343                 ; Infreq
        mov       edi, DWORD PTR [124+esp]                      ;
        mov       esi, DWORD PTR [132+esp]                      ;
        inc       esi                                           ;376.10
        add       edi, DWORD PTR [208+esp]                      ;376.10
        add       ebx, DWORD PTR [212+esp]                      ;376.10
        cmp       esi, DWORD PTR [4+esp]                        ;376.10
        jb        .B1.304       ; Prob 82%                      ;376.10
        jmp       .B1.259       ; Prob 100%                     ;376.10
                                ; LOE ebx esi edi
.B1.346:                        ; Preds .B1.325 .B1.329 .B1.327 ; Infreq
        xor       edx, edx                                      ;376.10
        jmp       .B1.341       ; Prob 100%                     ;376.10
                                ; LOE edx ebx esi edi
.B1.352:                        ; Preds .B1.308                 ; Infreq
        cmp       esi, 4                                        ;375.10
        jl        .B1.369       ; Prob 10%                      ;375.10
                                ; LOE ebx esi edi
.B1.353:                        ; Preds .B1.352                 ; Infreq
        mov       eax, DWORD PTR [132+esp]                      ;375.10
        add       eax, ebx                                      ;375.10
        and       eax, 15                                       ;375.10
        je        .B1.356       ; Prob 50%                      ;375.10
                                ; LOE eax ebx esi edi
.B1.354:                        ; Preds .B1.353                 ; Infreq
        test      al, 3                                         ;375.10
        jne       .B1.369       ; Prob 10%                      ;375.10
                                ; LOE eax ebx esi edi
.B1.355:                        ; Preds .B1.354                 ; Infreq
        neg       eax                                           ;375.10
        add       eax, 16                                       ;375.10
        shr       eax, 2                                        ;375.10
                                ; LOE eax ebx esi edi
.B1.356:                        ; Preds .B1.355 .B1.353         ; Infreq
        lea       edx, DWORD PTR [4+eax]                        ;375.10
        cmp       esi, edx                                      ;375.10
        jl        .B1.369       ; Prob 10%                      ;375.10
                                ; LOE eax ebx esi edi
.B1.357:                        ; Preds .B1.356                 ; Infreq
        mov       edx, esi                                      ;375.10
        sub       edx, eax                                      ;375.10
        mov       ecx, DWORD PTR [208+esp]                      ;
        and       edx, 3                                        ;375.10
        neg       edx                                           ;375.10
        add       edx, esi                                      ;375.10
        test      eax, eax                                      ;375.10
        lea       ecx, DWORD PTR [ecx+ebx]                      ;
        mov       DWORD PTR [212+esp], ecx                      ;
        jbe       .B1.361       ; Prob 10%                      ;375.10
                                ; LOE eax edx ebx esi edi
.B1.358:                        ; Preds .B1.357                 ; Infreq
        xor       ecx, ecx                                      ;
        mov       DWORD PTR [esp], esi                          ;
        mov       esi, DWORD PTR [212+esp]                      ;
                                ; LOE eax edx ecx ebx esi edi
.B1.359:                        ; Preds .B1.359 .B1.358         ; Infreq
        mov       DWORD PTR [esi+ecx*4], 0                      ;375.10
        inc       ecx                                           ;375.10
        cmp       ecx, eax                                      ;375.10
        jb        .B1.359       ; Prob 82%                      ;375.10
                                ; LOE eax edx ecx ebx esi edi
.B1.360:                        ; Preds .B1.359                 ; Infreq
        mov       esi, DWORD PTR [esp]                          ;
                                ; LOE eax edx ebx esi edi
.B1.361:                        ; Preds .B1.357 .B1.360         ; Infreq
        mov       ecx, DWORD PTR [212+esp]                      ;
        pxor      xmm0, xmm0                                    ;
                                ; LOE eax edx ecx ebx esi edi xmm0
.B1.362:                        ; Preds .B1.362 .B1.361         ; Infreq
        movdqa    XMMWORD PTR [ecx+eax*4], xmm0                 ;375.10
        add       eax, 4                                        ;375.10
        cmp       eax, edx                                      ;375.10
        jb        .B1.362       ; Prob 82%                      ;375.10
                                ; LOE eax edx ecx ebx esi edi xmm0
.B1.364:                        ; Preds .B1.362 .B1.369         ; Infreq
        cmp       edx, esi                                      ;375.10
        jae       .B1.370       ; Prob 10%                      ;375.10
                                ; LOE edx ebx esi edi
.B1.365:                        ; Preds .B1.364                 ; Infreq
        mov       eax, DWORD PTR [208+esp]                      ;
        add       eax, ebx                                      ;
                                ; LOE eax edx ebx esi edi
.B1.366:                        ; Preds .B1.366 .B1.365         ; Infreq
        mov       DWORD PTR [eax+edx*4], 0                      ;375.10
        inc       edx                                           ;375.10
        cmp       edx, esi                                      ;375.10
        jb        .B1.366       ; Prob 82%                      ;375.10
                                ; LOE eax edx ebx esi edi
.B1.367:                        ; Preds .B1.366                 ; Infreq
        inc       edi                                           ;375.10
        add       ebx, DWORD PTR [156+esp]                      ;375.10
        cmp       edi, DWORD PTR [4+esp]                        ;375.10
        jb        .B1.308       ; Prob 82%                      ;375.10
                                ; LOE ebx esi edi
.B1.368:                        ; Preds .B1.310 .B1.367         ; Infreq
        mov       eax, DWORD PTR [4+esp]                        ;
        test      eax, eax                                      ;375.10
        jmp       .B1.257       ; Prob 100%                     ;375.10
                                ; LOE eax
.B1.369:                        ; Preds .B1.352 .B1.356 .B1.354 ; Infreq
        xor       edx, edx                                      ;375.10
        jmp       .B1.364       ; Prob 100%                     ;375.10
                                ; LOE edx ebx esi edi
.B1.370:                        ; Preds .B1.364                 ; Infreq
        inc       edi                                           ;375.10
        add       ebx, DWORD PTR [156+esp]                      ;375.10
        cmp       edi, DWORD PTR [4+esp]                        ;375.10
        jb        .B1.308       ; Prob 82%                      ;375.10
                                ; LOE ebx esi edi
.B1.371:                        ; Preds .B1.370                 ; Infreq
        mov       eax, DWORD PTR [4+esp]                        ;
        test      eax, eax                                      ;375.10
        jmp       .B1.257       ; Prob 100%                     ;375.10
                                ; LOE eax
.B1.376:                        ; Preds .B1.27                  ; Infreq
        mov       DWORD PTR [608+esp], 0                        ;87.5
        lea       ebx, DWORD PTR [608+esp]                      ;87.5
        mov       DWORD PTR [336+esp], OFFSET FLAT: _READ_VEHICLES$JTMP.0.1 ;87.5
        lea       eax, DWORD PTR [336+esp]                      ;87.5
        push      32                                            ;87.5
        push      OFFSET FLAT: READ_VEHICLES$format_pack.0.1+120 ;87.5
        push      eax                                           ;87.5
        push      OFFSET FLAT: __STRLITPACK_65.0.1              ;87.5
        push      -2088435968                                   ;87.5
        push      500                                           ;87.5
        push      ebx                                           ;87.5
        call      _for_read_seq_fmt                             ;87.5
                                ; LOE ebx
.B1.377:                        ; Preds .B1.376                 ; Infreq
        mov       DWORD PTR [372+esp], OFFSET FLAT: _READ_VEHICLES$IUTMP.0.1 ;87.5
        lea       eax, DWORD PTR [372+esp]                      ;87.5
        push      eax                                           ;87.5
        push      OFFSET FLAT: __STRLITPACK_66.0.1              ;87.5
        push      ebx                                           ;87.5
        call      _for_read_seq_fmt_xmit                        ;87.5
                                ; LOE ebx
.B1.378:                        ; Preds .B1.377                 ; Infreq
        mov       DWORD PTR [392+esp], OFFSET FLAT: _READ_VEHICLES$IDTMP.0.1 ;87.5
        lea       eax, DWORD PTR [392+esp]                      ;87.5
        push      eax                                           ;87.5
        push      OFFSET FLAT: __STRLITPACK_67.0.1              ;87.5
        push      ebx                                           ;87.5
        call      _for_read_seq_fmt_xmit                        ;87.5
                                ; LOE ebx
.B1.379:                        ; Preds .B1.378                 ; Infreq
        mov       DWORD PTR [412+esp], OFFSET FLAT: _READ_VEHICLES$STIMETP.0.1 ;87.5
        lea       eax, DWORD PTR [412+esp]                      ;87.5
        push      eax                                           ;87.5
        push      OFFSET FLAT: __STRLITPACK_68.0.1              ;87.5
        push      ebx                                           ;87.5
        call      _for_read_seq_fmt_xmit                        ;87.5
                                ; LOE ebx
.B1.380:                        ; Preds .B1.379                 ; Infreq
        mov       DWORD PTR [432+esp], OFFSET FLAT: _READ_VEHICLES$IVCLTMP.0.1 ;87.5
        lea       eax, DWORD PTR [432+esp]                      ;87.5
        push      eax                                           ;87.5
        push      OFFSET FLAT: __STRLITPACK_69.0.1              ;87.5
        push      ebx                                           ;87.5
        call      _for_read_seq_fmt_xmit                        ;87.5
                                ; LOE ebx
.B1.381:                        ; Preds .B1.380                 ; Infreq
        mov       DWORD PTR [452+esp], OFFSET FLAT: _READ_VEHICLES$IVCL2TMP.0.1 ;87.5
        lea       eax, DWORD PTR [452+esp]                      ;87.5
        push      eax                                           ;87.5
        push      OFFSET FLAT: __STRLITPACK_70.0.1              ;87.5
        push      ebx                                           ;87.5
        call      _for_read_seq_fmt_xmit                        ;87.5
                                ; LOE ebx
.B1.382:                        ; Preds .B1.381                 ; Infreq
        mov       DWORD PTR [472+esp], OFFSET FLAT: _READ_VEHICLES$IHOVTMP.0.1 ;87.5
        lea       eax, DWORD PTR [472+esp]                      ;87.5
        push      eax                                           ;87.5
        push      OFFSET FLAT: __STRLITPACK_71.0.1              ;87.5
        push      ebx                                           ;87.5
        call      _for_read_seq_fmt_xmit                        ;87.5
                                ; LOE ebx
.B1.383:                        ; Preds .B1.382                 ; Infreq
        mov       DWORD PTR [492+esp], OFFSET FLAT: _READ_VEHICLES$NTMP.0.1 ;87.5
        lea       eax, DWORD PTR [492+esp]                      ;87.5
        push      eax                                           ;87.5
        push      OFFSET FLAT: __STRLITPACK_72.0.1              ;87.5
        push      ebx                                           ;87.5
        call      _for_read_seq_fmt_xmit                        ;87.5
                                ; LOE ebx
.B1.384:                        ; Preds .B1.383                 ; Infreq
        mov       DWORD PTR [512+esp], OFFSET FLAT: _READ_VEHICLES$NDESTMP.0.1 ;87.5
        lea       eax, DWORD PTR [512+esp]                      ;87.5
        push      eax                                           ;87.5
        push      OFFSET FLAT: __STRLITPACK_73.0.1              ;87.5
        push      ebx                                           ;87.5
        call      _for_read_seq_fmt_xmit                        ;87.5
                                ; LOE ebx
.B1.523:                        ; Preds .B1.384                 ; Infreq
        add       esp, 124                                      ;87.5
                                ; LOE ebx
.B1.385:                        ; Preds .B1.523                 ; Infreq
        mov       DWORD PTR [408+esp], OFFSET FLAT: _READ_VEHICLES$INFOTMP.0.1 ;87.5
        lea       eax, DWORD PTR [408+esp]                      ;87.5
        push      eax                                           ;87.5
        push      OFFSET FLAT: __STRLITPACK_74.0.1              ;87.5
        push      ebx                                           ;87.5
        call      _for_read_seq_fmt_xmit                        ;87.5
                                ; LOE ebx
.B1.386:                        ; Preds .B1.385                 ; Infreq
        mov       DWORD PTR [428+esp], OFFSET FLAT: _READ_VEHICLES$RIBFTMP.0.1 ;87.5
        lea       eax, DWORD PTR [428+esp]                      ;87.5
        push      eax                                           ;87.5
        push      OFFSET FLAT: __STRLITPACK_75.0.1              ;87.5
        push      ebx                                           ;87.5
        call      _for_read_seq_fmt_xmit                        ;87.5
                                ; LOE ebx
.B1.387:                        ; Preds .B1.386                 ; Infreq
        mov       DWORD PTR [448+esp], OFFSET FLAT: _READ_VEHICLES$COMPTMP.0.1 ;87.5
        lea       eax, DWORD PTR [448+esp]                      ;87.5
        push      eax                                           ;87.5
        push      OFFSET FLAT: __STRLITPACK_76.0.1              ;87.5
        push      ebx                                           ;87.5
        call      _for_read_seq_fmt_xmit                        ;87.5
                                ; LOE ebx
.B1.388:                        ; Preds .B1.387                 ; Infreq
        mov       DWORD PTR [468+esp], OFFSET FLAT: _READ_VEHICLES$IZONETMP.0.1 ;87.5
        lea       eax, DWORD PTR [468+esp]                      ;87.5
        push      eax                                           ;87.5
        push      OFFSET FLAT: __STRLITPACK_77.0.1              ;87.5
        push      ebx                                           ;87.5
        call      _for_read_seq_fmt_xmit                        ;87.5
                                ; LOE ebx
.B1.389:                        ; Preds .B1.388                 ; Infreq
        mov       DWORD PTR [488+esp], OFFSET FLAT: _READ_VEHICLES$IEVAC.0.1 ;87.5
        lea       eax, DWORD PTR [488+esp]                      ;87.5
        push      eax                                           ;87.5
        push      OFFSET FLAT: __STRLITPACK_78.0.1              ;87.5
        push      ebx                                           ;87.5
        call      _for_read_seq_fmt_xmit                        ;87.5
                                ; LOE ebx
.B1.390:                        ; Preds .B1.389                 ; Infreq
        mov       DWORD PTR [508+esp], OFFSET FLAT: _READ_VEHICLES$XPARTMP.0.1 ;87.5
        lea       eax, DWORD PTR [508+esp]                      ;87.5
        push      eax                                           ;87.5
        push      OFFSET FLAT: __STRLITPACK_79.0.1              ;87.5
        push      ebx                                           ;87.5
        call      _for_read_seq_fmt_xmit                        ;87.5
                                ; LOE ebx
.B1.391:                        ; Preds .B1.390                 ; Infreq
        mov       DWORD PTR [528+esp], OFFSET FLAT: _READ_VEHICLES$GASTMP.0.1 ;87.5
        lea       eax, DWORD PTR [528+esp]                      ;87.5
        push      eax                                           ;87.5
        push      OFFSET FLAT: __STRLITPACK_80.0.1              ;87.5
        push      ebx                                           ;87.5
        call      _for_read_seq_fmt_xmit                        ;87.5
                                ; LOE
.B1.524:                        ; Preds .B1.391                 ; Infreq
        add       esp, 84                                       ;87.5
                                ; LOE
.B1.392:                        ; Preds .B1.524                 ; Infreq
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_KEEPCLS], 1 ;88.16
        je        .B1.394       ; Prob 16%                      ;88.16
                                ; LOE
.B1.393:                        ; Preds .B1.392                 ; Infreq
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_IREAD_VEH_FLAG] ;92.2
        jmp       .B1.28        ; Prob 100%                     ;92.2
                                ; LOE eax
.B1.394:                        ; Preds .B1.392                 ; Infreq
        mov       eax, DWORD PTR [_READ_VEHICLES$IVCLTMP.0.1]   ;88.16
        cmp       eax, 4                                        ;88.33
        je        .B1.398       ; Prob 16%                      ;88.33
                                ; LOE eax
.B1.395:                        ; Preds .B1.394                 ; Infreq
        cmp       eax, 3                                        ;89.33
        je        .B1.397       ; Prob 16%                      ;89.33
                                ; LOE
.B1.396:                        ; Preds .B1.398 .B1.395         ; Infreq
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_IREAD_VEH_FLAG] ;92.2
        jmp       .B1.28        ; Prob 100%                     ;92.2
                                ; LOE eax
.B1.397:                        ; Preds .B1.395                 ; Infreq
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_IUE_OK], 1  ;89.39
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_IREAD_VEH_FLAG] ;92.2
        jmp       .B1.28        ; Prob 100%                     ;92.2
                                ; LOE eax
.B1.398:                        ; Preds .B1.394                 ; Infreq
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_IER_OK], 1  ;88.39
        jmp       .B1.396       ; Prob 100%                     ;88.39
                                ; LOE
.B1.399:                        ; Preds .B1.131                 ; Infreq
        cmp       DWORD PTR [20+esp], 4                         ;69.9
        jl        .B1.416       ; Prob 10%                      ;69.9
                                ; LOE ebx esi
.B1.400:                        ; Preds .B1.399                 ; Infreq
        mov       eax, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLODTMP+40] ;69.9
        mov       edx, DWORD PTR [8+esp]                        ;69.9
        imul      edx, eax                                      ;69.9
        imul      eax, ebx                                      ;69.9
        neg       edx                                           ;69.9
        add       edx, DWORD PTR [12+esp]                       ;69.9
        mov       DWORD PTR [esp], eax                          ;69.9
        add       eax, edx                                      ;69.9
        and       eax, 15                                       ;69.9
        je        .B1.403       ; Prob 50%                      ;69.9
                                ; LOE eax edx ebx esi
.B1.401:                        ; Preds .B1.400                 ; Infreq
        test      al, 3                                         ;69.9
        jne       .B1.416       ; Prob 10%                      ;69.9
                                ; LOE eax edx ebx esi
.B1.402:                        ; Preds .B1.401                 ; Infreq
        neg       eax                                           ;69.9
        add       eax, 16                                       ;69.9
        shr       eax, 2                                        ;69.9
                                ; LOE eax edx ebx esi
.B1.403:                        ; Preds .B1.402 .B1.400         ; Infreq
        lea       ecx, DWORD PTR [4+eax]                        ;69.9
        cmp       ecx, DWORD PTR [20+esp]                       ;69.9
        jg        .B1.416       ; Prob 10%                      ;69.9
                                ; LOE eax edx ebx esi
.B1.404:                        ; Preds .B1.403                 ; Infreq
        mov       edi, DWORD PTR [20+esp]                       ;69.9
        mov       ecx, edi                                      ;69.9
        sub       ecx, eax                                      ;69.9
        and       ecx, 3                                        ;69.9
        neg       ecx                                           ;69.9
        add       edx, DWORD PTR [esp]                          ;
        add       ecx, edi                                      ;69.9
        test      eax, eax                                      ;69.9
        jbe       .B1.408       ; Prob 10%                      ;69.9
                                ; LOE eax edx ecx ebx esi
.B1.405:                        ; Preds .B1.404                 ; Infreq
        xor       edi, edi                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B1.406:                        ; Preds .B1.406 .B1.405         ; Infreq
        mov       DWORD PTR [edx+edi*4], 0                      ;69.9
        inc       edi                                           ;69.9
        cmp       edi, eax                                      ;69.9
        jb        .B1.406       ; Prob 82%                      ;69.9
                                ; LOE eax edx ecx ebx esi edi
.B1.408:                        ; Preds .B1.406 .B1.404         ; Infreq
        pxor      xmm0, xmm0                                    ;
                                ; LOE eax edx ecx ebx esi xmm0
.B1.409:                        ; Preds .B1.409 .B1.408         ; Infreq
        movdqa    XMMWORD PTR [edx+eax*4], xmm0                 ;69.9
        add       eax, 4                                        ;69.9
        cmp       eax, ecx                                      ;69.9
        jb        .B1.409       ; Prob 82%                      ;69.9
                                ; LOE eax edx ecx ebx esi xmm0
.B1.411:                        ; Preds .B1.409 .B1.416         ; Infreq
        cmp       ecx, DWORD PTR [20+esp]                       ;69.9
        jae       .B1.417       ; Prob 10%                      ;69.9
                                ; LOE ecx ebx esi
.B1.412:                        ; Preds .B1.411                 ; Infreq
        mov       eax, DWORD PTR [8+esp]                        ;
        neg       eax                                           ;
        add       eax, ebx                                      ;
        imul      eax, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLODTMP+40] ;
        add       eax, DWORD PTR [12+esp]                       ;
        mov       edx, DWORD PTR [20+esp]                       ;
                                ; LOE eax edx ecx ebx esi
.B1.413:                        ; Preds .B1.413 .B1.412         ; Infreq
        mov       DWORD PTR [eax+ecx*4], 0                      ;69.9
        inc       ecx                                           ;69.9
        cmp       ecx, edx                                      ;69.9
        jb        .B1.413       ; Prob 82%                      ;69.9
                                ; LOE eax edx ecx ebx esi
.B1.414:                        ; Preds .B1.413                 ; Infreq
        inc       esi                                           ;69.9
        inc       ebx                                           ;69.9
        cmp       esi, DWORD PTR [4+esp]                        ;69.9
        jb        .B1.131       ; Prob 82%                      ;69.9
                                ; LOE ebx esi
.B1.415:                        ; Preds .B1.133 .B1.414         ; Infreq
        mov       ebx, DWORD PTR [460+esp]                      ;
        jmp       .B1.18        ; Prob 100%                     ;
                                ; LOE ebx
.B1.416:                        ; Preds .B1.399 .B1.403 .B1.401 ; Infreq
        xor       ecx, ecx                                      ;69.9
        jmp       .B1.411       ; Prob 100%                     ;69.9
                                ; LOE ecx ebx esi
.B1.417:                        ; Preds .B1.411                 ; Infreq
        inc       esi                                           ;69.9
        inc       ebx                                           ;69.9
        cmp       esi, DWORD PTR [4+esp]                        ;69.9
        jb        .B1.131       ; Prob 82%                      ;69.9
                                ; LOE ebx esi
.B1.418:                        ; Preds .B1.417                 ; Infreq
        mov       ebx, DWORD PTR [460+esp]                      ;
        jmp       .B1.18        ; Prob 100%                     ;
                                ; LOE ebx
.B1.421:                        ; Preds .B1.9                   ; Infreq
        mov       esi, 1                                        ;
        jmp       .B1.13        ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B1.422:                        ; Preds .B1.15                  ; Infreq
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TMPARYSIZE] ;64.9
        xor       ecx, ecx                                      ;64.9
        test      eax, eax                                      ;64.9
        cmovle    eax, ecx                                      ;64.9
        mov       esi, 1                                        ;64.9
        mov       edx, 2                                        ;64.9
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLODTMP+32], esi ;64.9
        mov       edi, 4                                        ;64.9
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLODTMP+44], esi ;64.9
        lea       esi, DWORD PTR [68+esp]                       ;64.9
        push      8                                             ;64.9
        push      eax                                           ;64.9
        push      edx                                           ;64.9
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLODTMP+8], ecx ;64.9
        lea       ecx, DWORD PTR [eax*4]                        ;64.9
        push      esi                                           ;64.9
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLODTMP+12], 133 ;64.9
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLODTMP+4], edi ;64.9
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLODTMP+16], edx ;64.9
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLODTMP+24], eax ;64.9
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLODTMP+36], edx ;64.9
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLODTMP+28], edi ;64.9
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLODTMP+40], ecx ;64.9
        call      _for_check_mult_overflow                      ;64.9
                                ; LOE eax ebx
.B1.423:                        ; Preds .B1.422                 ; Infreq
        mov       edx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLODTMP+12] ;64.9
        and       eax, 1                                        ;64.9
        and       edx, 1                                        ;64.9
        shl       eax, 4                                        ;64.9
        add       edx, edx                                      ;64.9
        or        edx, eax                                      ;64.9
        or        edx, 262144                                   ;64.9
        push      edx                                           ;64.9
        push      OFFSET FLAT: _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLODTMP ;64.9
        push      DWORD PTR [92+esp]                            ;64.9
        call      _for_alloc_allocatable                        ;64.9
                                ; LOE ebx
.B1.526:                        ; Preds .B1.423                 ; Infreq
        add       esp, 28                                       ;64.9
                                ; LOE ebx
.B1.424:                        ; Preds .B1.526                 ; Infreq
        mov       edx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLODTMP+36] ;65.9
        test      edx, edx                                      ;65.9
        mov       ecx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLODTMP+44] ;65.9
        jle       .B1.426       ; Prob 10%                      ;65.9
                                ; LOE edx ecx ebx
.B1.425:                        ; Preds .B1.424                 ; Infreq
        mov       esi, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLODTMP] ;65.9
        mov       DWORD PTR [24+esp], esi                       ;65.9
        mov       esi, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLODTMP+40] ;65.9
        mov       edi, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLODTMP+24] ;65.9
        test      edi, edi                                      ;65.9
        mov       eax, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLODTMP+32] ;65.9
        mov       DWORD PTR [16+esp], esi                       ;65.9
        jg        .B1.432       ; Prob 50%                      ;65.9
                                ; LOE eax edx ecx ebx edi
.B1.426:                        ; Preds .B1.436 .B1.475 .B1.472 .B1.425 .B1.424
                                ;                               ; Infreq
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NZONES] ;66.9
        xor       edx, edx                                      ;66.9
        test      esi, esi                                      ;66.9
        cmovle    esi, edx                                      ;66.9
        mov       ecx, 4                                        ;66.9
        lea       edi, DWORD PTR [76+esp]                       ;66.9
        push      ecx                                           ;66.9
        push      esi                                           ;66.9
        push      2                                             ;66.9
        push      edi                                           ;66.9
        mov       eax, 1                                        ;66.9
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLODZONE+12], 133 ;66.9
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLODZONE+4], ecx ;66.9
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLODZONE+16], eax ;66.9
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLODZONE+8], edx ;66.9
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLODZONE+32], eax ;66.9
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLODZONE+24], esi ;66.9
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLODZONE+28], ecx ;66.9
        call      _for_check_mult_overflow                      ;66.9
                                ; LOE eax ebx
.B1.427:                        ; Preds .B1.426                 ; Infreq
        mov       edx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLODZONE+12] ;66.9
        and       eax, 1                                        ;66.9
        and       edx, 1                                        ;66.9
        shl       eax, 4                                        ;66.9
        add       edx, edx                                      ;66.9
        or        edx, eax                                      ;66.9
        or        edx, 262144                                   ;66.9
        push      edx                                           ;66.9
        push      OFFSET FLAT: _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLODZONE ;66.9
        push      DWORD PTR [100+esp]                           ;66.9
        call      _for_alloc_allocatable                        ;66.9
                                ; LOE ebx
.B1.528:                        ; Preds .B1.427                 ; Infreq
        add       esp, 28                                       ;66.9
                                ; LOE ebx
.B1.428:                        ; Preds .B1.528                 ; Infreq
        mov       edx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLODZONE+24] ;67.9
        test      edx, edx                                      ;67.9
        jle       .B1.431       ; Prob 10%                      ;67.9
                                ; LOE edx ebx
.B1.429:                        ; Preds .B1.428                 ; Infreq
        mov       eax, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLODZONE] ;67.9
        cmp       edx, 24                                       ;67.9
        mov       DWORD PTR [esp], eax                          ;67.9
        jle       .B1.437       ; Prob 0%                       ;67.9
                                ; LOE eax edx ebx al ah
.B1.430:                        ; Preds .B1.429                 ; Infreq
        shl       edx, 2                                        ;67.9
        push      edx                                           ;67.9
        push      0                                             ;67.9
        push      DWORD PTR [8+esp]                             ;67.9
        call      __intel_fast_memset                           ;67.9
                                ; LOE ebx
.B1.529:                        ; Preds .B1.430                 ; Infreq
        add       esp, 12                                       ;67.9
                                ; LOE ebx
.B1.431:                        ; Preds .B1.428 .B1.451 .B1.449 .B1.529 ; Infreq
        mov       eax, DWORD PTR [8+ebp]                        ;74.5
        movss     xmm0, DWORD PTR [eax]                         ;74.5
        movss     DWORD PTR [28+esp], xmm0                      ;74.5
        jmp       .B1.21        ; Prob 100%                     ;74.5
                                ; LOE ebx
.B1.432:                        ; Preds .B1.425                 ; Infreq
        imul      ecx, DWORD PTR [16+esp]                       ;
        xor       esi, esi                                      ;
        pxor      xmm0, xmm0                                    ;65.9
        shl       eax, 2                                        ;
        mov       DWORD PTR [8+esp], edx                        ;
        mov       edx, DWORD PTR [24+esp]                       ;
        sub       edx, eax                                      ;
        sub       eax, ecx                                      ;
        add       edx, eax                                      ;
        lea       eax, DWORD PTR [edi*4]                        ;
        add       edx, ecx                                      ;
        mov       DWORD PTR [12+esp], edx                       ;
        mov       DWORD PTR [460+esp], ebx                      ;
        mov       edx, DWORD PTR [8+esp]                        ;
        mov       ebx, esi                                      ;
        mov       DWORD PTR [4+esp], eax                        ;
                                ; LOE ebx esi edi
.B1.433:                        ; Preds .B1.435 .B1.474 .B1.471 .B1.432 ; Infreq
        cmp       edi, 24                                       ;65.9
        jle       .B1.456       ; Prob 0%                       ;65.9
                                ; LOE ebx esi edi
.B1.434:                        ; Preds .B1.433                 ; Infreq
        mov       eax, DWORD PTR [12+esp]                       ;65.9
        push      DWORD PTR [4+esp]                             ;65.9
        push      0                                             ;65.9
        lea       edx, DWORD PTR [eax+esi]                      ;65.9
        push      edx                                           ;65.9
        call      __intel_fast_memset                           ;65.9
                                ; LOE ebx esi edi
.B1.530:                        ; Preds .B1.434                 ; Infreq
        add       esp, 12                                       ;65.9
                                ; LOE ebx esi edi
.B1.435:                        ; Preds .B1.530                 ; Infreq
        inc       ebx                                           ;65.9
        add       esi, DWORD PTR [16+esp]                       ;65.9
        cmp       ebx, DWORD PTR [8+esp]                        ;65.9
        jb        .B1.433       ; Prob 82%                      ;65.9
                                ; LOE ebx esi edi
.B1.436:                        ; Preds .B1.435                 ; Infreq
        mov       ebx, DWORD PTR [460+esp]                      ;
        jmp       .B1.426       ; Prob 100%                     ;
                                ; LOE ebx
.B1.437:                        ; Preds .B1.429                 ; Infreq
        cmp       edx, 4                                        ;67.9
        jl        .B1.453       ; Prob 10%                      ;67.9
                                ; LOE eax edx ebx al ah
.B1.438:                        ; Preds .B1.437                 ; Infreq
        mov       ecx, eax                                      ;67.9
        and       ecx, 15                                       ;67.9
        je        .B1.441       ; Prob 50%                      ;67.9
                                ; LOE edx ecx ebx
.B1.439:                        ; Preds .B1.438                 ; Infreq
        test      cl, 3                                         ;67.9
        jne       .B1.453       ; Prob 10%                      ;67.9
                                ; LOE edx ecx ebx
.B1.440:                        ; Preds .B1.439                 ; Infreq
        neg       ecx                                           ;67.9
        add       ecx, 16                                       ;67.9
        shr       ecx, 2                                        ;67.9
                                ; LOE edx ecx ebx
.B1.441:                        ; Preds .B1.440 .B1.438         ; Infreq
        lea       eax, DWORD PTR [4+ecx]                        ;67.9
        cmp       edx, eax                                      ;67.9
        jl        .B1.453       ; Prob 10%                      ;67.9
                                ; LOE edx ecx ebx
.B1.442:                        ; Preds .B1.441                 ; Infreq
        mov       eax, edx                                      ;67.9
        sub       eax, ecx                                      ;67.9
        and       eax, 3                                        ;67.9
        neg       eax                                           ;67.9
        add       eax, edx                                      ;67.9
        test      ecx, ecx                                      ;67.9
        jbe       .B1.446       ; Prob 0%                       ;67.9
                                ; LOE eax edx ecx ebx
.B1.443:                        ; Preds .B1.442                 ; Infreq
        mov       edi, DWORD PTR [esp]                          ;
        xor       esi, esi                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B1.444:                        ; Preds .B1.444 .B1.443         ; Infreq
        mov       DWORD PTR [edi+esi*4], 0                      ;67.9
        inc       esi                                           ;67.9
        cmp       esi, ecx                                      ;67.9
        jb        .B1.444       ; Prob 82%                      ;67.9
                                ; LOE eax edx ecx ebx esi edi
.B1.446:                        ; Preds .B1.444 .B1.442         ; Infreq
        mov       esi, DWORD PTR [esp]                          ;67.9
        pxor      xmm0, xmm0                                    ;67.9
                                ; LOE eax edx ecx ebx esi xmm0
.B1.447:                        ; Preds .B1.447 .B1.446         ; Infreq
        movdqa    XMMWORD PTR [esi+ecx*4], xmm0                 ;67.9
        add       ecx, 4                                        ;67.9
        cmp       ecx, eax                                      ;67.9
        jb        .B1.447       ; Prob 82%                      ;67.9
                                ; LOE eax edx ecx ebx esi xmm0
.B1.449:                        ; Preds .B1.447 .B1.453         ; Infreq
        cmp       eax, edx                                      ;67.9
        jae       .B1.431       ; Prob 0%                       ;67.9
                                ; LOE eax edx ebx
.B1.450:                        ; Preds .B1.449                 ; Infreq
        mov       ecx, DWORD PTR [esp]                          ;
                                ; LOE eax edx ecx ebx
.B1.451:                        ; Preds .B1.451 .B1.450         ; Infreq
        mov       DWORD PTR [ecx+eax*4], 0                      ;67.9
        inc       eax                                           ;67.9
        cmp       eax, edx                                      ;67.9
        jb        .B1.451       ; Prob 82%                      ;67.9
        jmp       .B1.431       ; Prob 100%                     ;67.9
                                ; LOE eax edx ecx ebx
.B1.453:                        ; Preds .B1.437 .B1.441 .B1.439 ; Infreq
        xor       eax, eax                                      ;67.9
        jmp       .B1.449       ; Prob 100%                     ;67.9
                                ; LOE eax edx ebx
.B1.456:                        ; Preds .B1.433                 ; Infreq
        cmp       edi, 4                                        ;65.9
        jl        .B1.473       ; Prob 10%                      ;65.9
                                ; LOE ebx esi edi
.B1.457:                        ; Preds .B1.456                 ; Infreq
        mov       eax, DWORD PTR [24+esp]                       ;65.9
        add       eax, esi                                      ;65.9
        and       eax, 15                                       ;65.9
        je        .B1.460       ; Prob 50%                      ;65.9
                                ; LOE eax ebx esi edi
.B1.458:                        ; Preds .B1.457                 ; Infreq
        test      al, 3                                         ;65.9
        jne       .B1.473       ; Prob 10%                      ;65.9
                                ; LOE eax ebx esi edi
.B1.459:                        ; Preds .B1.458                 ; Infreq
        neg       eax                                           ;65.9
        add       eax, 16                                       ;65.9
        shr       eax, 2                                        ;65.9
                                ; LOE eax ebx esi edi
.B1.460:                        ; Preds .B1.459 .B1.457         ; Infreq
        lea       edx, DWORD PTR [4+eax]                        ;65.9
        cmp       edi, edx                                      ;65.9
        jl        .B1.473       ; Prob 10%                      ;65.9
                                ; LOE eax ebx esi edi
.B1.461:                        ; Preds .B1.460                 ; Infreq
        mov       edx, edi                                      ;65.9
        sub       edx, eax                                      ;65.9
        mov       ecx, DWORD PTR [24+esp]                       ;
        and       edx, 3                                        ;65.9
        neg       edx                                           ;65.9
        add       edx, edi                                      ;65.9
        test      eax, eax                                      ;65.9
        lea       ecx, DWORD PTR [ecx+esi]                      ;
        mov       DWORD PTR [20+esp], ecx                       ;
        jbe       .B1.465       ; Prob 10%                      ;65.9
                                ; LOE eax edx ebx esi edi
.B1.462:                        ; Preds .B1.461                 ; Infreq
        xor       ecx, ecx                                      ;
        mov       DWORD PTR [esp], edi                          ;
        mov       edi, DWORD PTR [20+esp]                       ;
                                ; LOE eax edx ecx ebx esi edi
.B1.463:                        ; Preds .B1.463 .B1.462         ; Infreq
        mov       DWORD PTR [edi+ecx*4], 0                      ;65.9
        inc       ecx                                           ;65.9
        cmp       ecx, eax                                      ;65.9
        jb        .B1.463       ; Prob 82%                      ;65.9
                                ; LOE eax edx ecx ebx esi edi
.B1.464:                        ; Preds .B1.463                 ; Infreq
        mov       edi, DWORD PTR [esp]                          ;
                                ; LOE eax edx ebx esi edi
.B1.465:                        ; Preds .B1.461 .B1.464         ; Infreq
        mov       ecx, DWORD PTR [20+esp]                       ;
        pxor      xmm0, xmm0                                    ;
                                ; LOE eax edx ecx ebx esi edi xmm0
.B1.466:                        ; Preds .B1.466 .B1.465         ; Infreq
        movdqa    XMMWORD PTR [ecx+eax*4], xmm0                 ;65.9
        add       eax, 4                                        ;65.9
        cmp       eax, edx                                      ;65.9
        jb        .B1.466       ; Prob 82%                      ;65.9
                                ; LOE eax edx ecx ebx esi edi xmm0
.B1.468:                        ; Preds .B1.466 .B1.473         ; Infreq
        cmp       edx, edi                                      ;65.9
        jae       .B1.474       ; Prob 10%                      ;65.9
                                ; LOE edx ebx esi edi
.B1.469:                        ; Preds .B1.468                 ; Infreq
        mov       eax, DWORD PTR [24+esp]                       ;
        add       eax, esi                                      ;
                                ; LOE eax edx ebx esi edi
.B1.470:                        ; Preds .B1.470 .B1.469         ; Infreq
        mov       DWORD PTR [eax+edx*4], 0                      ;65.9
        inc       edx                                           ;65.9
        cmp       edx, edi                                      ;65.9
        jb        .B1.470       ; Prob 82%                      ;65.9
                                ; LOE eax edx ebx esi edi
.B1.471:                        ; Preds .B1.470                 ; Infreq
        inc       ebx                                           ;65.9
        add       esi, DWORD PTR [16+esp]                       ;65.9
        cmp       ebx, DWORD PTR [8+esp]                        ;65.9
        jb        .B1.433       ; Prob 82%                      ;65.9
                                ; LOE ebx esi edi
.B1.472:                        ; Preds .B1.471                 ; Infreq
        mov       ebx, DWORD PTR [460+esp]                      ;
        jmp       .B1.426       ; Prob 100%                     ;
                                ; LOE ebx
.B1.473:                        ; Preds .B1.456 .B1.460 .B1.458 ; Infreq
        xor       edx, edx                                      ;65.9
        jmp       .B1.468       ; Prob 100%                     ;65.9
                                ; LOE edx ebx esi edi
.B1.474:                        ; Preds .B1.468                 ; Infreq
        inc       ebx                                           ;65.9
        add       esi, DWORD PTR [16+esp]                       ;65.9
        cmp       ebx, DWORD PTR [8+esp]                        ;65.9
        jb        .B1.433       ; Prob 82%                      ;65.9
                                ; LOE ebx esi edi
.B1.475:                        ; Preds .B1.474                 ; Infreq
        mov       ebx, DWORD PTR [460+esp]                      ;
        jmp       .B1.426       ; Prob 100%                     ;
                                ; LOE ebx
.B1.533:                        ; Preds .B1.256                 ; Infreq
        test      eax, eax                                      ;375.10
        jmp       .B1.257       ; Prob 100%                     ;375.10
        ALIGN     16
                                ; LOE eax
; mark_end;
_READ_VEHICLES ENDP
_TEXT	ENDS
_BSS	SEGMENT  DWORD PUBLIC FLAT  'BSS'
	ALIGN 004H
_READ_VEHICLES$IDARRAY.0.1	DB ?	; pad
	ORG $+7999998	; pad
	DB ?	; pad
_READ_VEHICLES$JDEST_TMP.0.1	DB ?	; pad
	ORG $+3998	; pad
	DB ?	; pad
	DB ?	; pad
	ORG $+606	; pad
	DB ?	; pad
_READ_VEHICLES$JPATH_TMP.0.1	DB ?	; pad
	ORG $+39998	; pad
	DB ?	; pad
_READ_VEHICLES$JTIME_TMP.0.1	DB ?	; pad
	ORG $+3998	; pad
	DB ?	; pad
	DB ?	; pad
	ORG $+606	; pad
	DB ?	; pad
_READ_VEHICLES$VEHTMP.0.1	DD 3 DUP (0H)	; pad
	DD 1 DUP (0H)	; pad
_READ_VEHICLES$WAIT_TMP.0.1	DD 10 DUP (0H)	; pad
_READ_VEHICLES$EOFID.0.1	DD 1 DUP (0H)	; pad
_READ_VEHICLES$JTMP.0.1	DD 1 DUP (0H)	; pad
_READ_VEHICLES$IUTMP.0.1	DD 1 DUP (0H)	; pad
_READ_VEHICLES$IDTMP.0.1	DD 1 DUP (0H)	; pad
_READ_VEHICLES$STIMETP.0.1	DD 1 DUP (0H)	; pad
_READ_VEHICLES$IVCLTMP.0.1	DD 1 DUP (0H)	; pad
_READ_VEHICLES$IVCL2TMP.0.1	DD 1 DUP (0H)	; pad
_READ_VEHICLES$IHOVTMP.0.1	DD 1 DUP (0H)	; pad
_READ_VEHICLES$NTMP.0.1	DD 1 DUP (0H)	; pad
_READ_VEHICLES$NDESTMP.0.1	DD 1 DUP (0H)	; pad
_READ_VEHICLES$INFOTMP.0.1	DD 1 DUP (0H)	; pad
_READ_VEHICLES$RIBFTMP.0.1	DD 1 DUP (0H)	; pad
_READ_VEHICLES$COMPTMP.0.1	DD 1 DUP (0H)	; pad
_READ_VEHICLES$IZONETMP.0.1	DD 1 DUP (0H)	; pad
_READ_VEHICLES$IEVAC.0.1	DD 1 DUP (0H)	; pad
_READ_VEHICLES$XPARTMP.0.1	DD 1 DUP (0H)	; pad
_READ_VEHICLES$GASTMP.0.1	DD 1 DUP (0H)	; pad
_READ_VEHICLES$JM.0.1	DD 1 DUP (0H)	; pad
_READ_VEHICLES$MTMP.0.1	DD 1 DUP (0H)	; pad
_BSS	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
READ_VEHICLES$format_pack.0.1	DB	54
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	-24
	DB	3
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	5
	DB	0
	DB	0
	DB	0
	DB	6
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	4
	DB	1
	DB	0
	DB	0
	DB	0
	DB	8
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	6
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	4
	DB	2
	DB	0
	DB	0
	DB	0
	DB	8
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	2
	DB	0
	DB	0
	DB	0
	DB	6
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	12
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	2
	DB	1
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	9
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	2
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	2
	DB	1
	DB	0
	DB	0
	DB	0
	DB	8
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	6
	DB	0
	DB	0
	DB	0
	DB	6
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	4
	DB	2
	DB	0
	DB	0
	DB	0
	DB	8
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	2
	DB	0
	DB	0
	DB	0
	DB	5
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	8
	DB	1
	DB	0
	DB	0
	DB	0
	DB	12
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	1
	DB	1
	DB	0
	DB	0
	DB	0
	DB	6
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	4
	DB	0
	DB	0
	DB	0
	DB	12
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	2
	DB	1
	DB	0
	DB	0
	DB	0
	DB	10
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	22
	DB	0
	DB	79
	DB	114
	DB	105
	DB	103
	DB	105
	DB	110
	DB	97
	DB	108
	DB	32
	DB	73
	DB	68
	DB	32
	DB	45
	DB	45
	DB	62
	DB	32
	DB	78
	DB	101
	DB	119
	DB	32
	DB	73
	DB	68
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	2
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	2
	DB	-24
	DB	3
	DB	0
	DB	0
	DB	8
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	11
	DB	0
	DB	80
	DB	114
	DB	101
	DB	84
	DB	32
	DB	84
	DB	105
	DB	109
	DB	101
	DB	58
	DB	32
	DB	0
	DB	33
	DB	0
	DB	0
	DB	1
	DB	1
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	11
	DB	0
	DB	32
	DB	86
	DB	101
	DB	104
	DB	105
	DB	99
	DB	108
	DB	101
	DB	32
	DB	110
	DB	111
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	8
	DB	0
	DB	32
	DB	79
	DB	68
	DB	32
	DB	122
	DB	111
	DB	110
	DB	101
	DB	36
	DB	0
	DB	0
	DB	0
	DB	2
	DB	0
	DB	0
	DB	0
	DB	6
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	13
	DB	0
	DB	32
	DB	35
	DB	67
	DB	117
	DB	114
	DB	110
	DB	44
	DB	85
	DB	78
	DB	44
	DB	68
	DB	78
	DB	32
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	4
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	2
	DB	0
	DB	0
	DB	0
	DB	6
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	18
	DB	0
	DB	32
	DB	100
	DB	101
	DB	108
	DB	97
	DB	121
	DB	32
	DB	116
	DB	111
	DB	108
	DB	101
	DB	58
	DB	40
	DB	109
	DB	105
	DB	110
	DB	41
	DB	32
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	1
	DB	1
	DB	0
	DB	0
	DB	0
	DB	6
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	18
	DB	0
	DB	32
	DB	99
	DB	117
	DB	114
	DB	110
	DB	32
	DB	100
	DB	101
	DB	108
	DB	97
	DB	121
	DB	58
	DB	40
	DB	109
	DB	105
	DB	110
	DB	41
	DB	32
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	1
	DB	1
	DB	0
	DB	0
	DB	0
	DB	6
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
_DATA	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
__STRLITPACK_65.0.1	DB	9
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_66.0.1	DB	9
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_67.0.1	DB	9
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_68.0.1	DB	26
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_69.0.1	DB	9
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_70.0.1	DB	9
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_71.0.1	DB	9
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_72.0.1	DB	9
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_73.0.1	DB	9
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_74.0.1	DB	9
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_75.0.1	DB	26
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_76.0.1	DB	26
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_77.0.1	DB	9
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_78.0.1	DB	9
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_79.0.1	DB	26
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_80.0.1	DB	26
	DB	3
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_81.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_82.0.1	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__NLITPACK_0.0.1	DD	15
__STRLITPACK_83.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_84.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_85.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_87.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_88.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_89.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_90.0.1	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_92.0.1	DB	9
	DB	5
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_93.0.1	DB	26
	DB	5
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_94.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_95.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_97.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_98.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_99.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_101.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_102.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_103.0.1	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_104.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_105.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_106.0.1	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_108.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_109.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_110.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_111.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_113.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_114.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_115.0.1	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_116.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_117.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_118.0.1	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_119.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_120.0.1	DB	2
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_121.0.1	DB	7
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_122.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_124.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_125.0.1	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__NLITPACK_1.0.1	DD	155
__STRLITPACK_126.0.1	DB	9
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_127.0.1	DB	9
	DB	7
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_128.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_129.0.1	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__NLITPACK_2.0.1	DD	1
__STRLITPACK_130.0.1	DB	26
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_131.0.1	DB	26
	DB	7
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__NLITPACK_3.0.1	DD	4
__STRLITPACK_132.0.1	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_133.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_134.0.1	DB	7
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_135.0.1	DB	7
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_136.0.1	DB	7
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_137.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_138.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_139.0.1	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_140.0.1	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_141.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_142.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_143.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__NLITPACK_4.0.1	DD	0
__NLITPACK_5.0.1	DD	2
__NLITPACK_7.0.1	DD	98
__NLITPACK_6.0.1	DD	97
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _READ_VEHICLES
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _CHECK_MIVA_VEHICLE
; mark_begin;
       ALIGN     16
	PUBLIC _CHECK_MIVA_VEHICLE
_CHECK_MIVA_VEHICLE	PROC NEAR 
.B2.1:                          ; Preds .B2.0
        push      ebp                                           ;444.12
        mov       ebp, esp                                      ;444.12
        and       esp, -16                                      ;444.12
        push      edi                                           ;444.12
        push      ebx                                           ;444.12
        sub       esp, 216                                      ;444.12
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_MAXVEHICLESFILE] ;467.3
        test      edx, edx                                      ;467.3
        jle       .B2.47        ; Prob 2%                       ;467.3
                                ; LOE edx esi
.B2.2:                          ; Preds .B2.1
        mov       eax, 1                                        ;
        mov       DWORD PTR [esp], esi                          ;
        lea       ebx, DWORD PTR [32+esp]                       ;
        mov       esi, eax                                      ;
        mov       edi, edx                                      ;
                                ; LOE ebx esi edi
.B2.3:                          ; Preds .B2.25 .B2.2
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ISO_OK], 1  ;468.16
        je        .B2.38        ; Prob 16%                      ;468.16
                                ; LOE ebx esi edi
.B2.4:                          ; Preds .B2.3 .B2.38
        mov       DWORD PTR [32+esp], 0                         ;469.6
        lea       ecx, DWORD PTR [24+esp]                       ;469.6
        mov       DWORD PTR [112+esp], ecx                      ;469.6
        lea       ecx, DWORD PTR [112+esp]                      ;469.6
        push      32                                            ;469.6
        push      OFFSET FLAT: CHECK_MIVA_VEHICLE$format_pack.0.2 ;469.6
        push      ecx                                           ;469.6
        push      OFFSET FLAT: __STRLITPACK_151.0.2             ;469.6
        push      -2088435968                                   ;469.6
        push      500                                           ;469.6
        push      ebx                                           ;469.6
        call      _for_read_seq_fmt                             ;469.6
                                ; LOE ebx esi edi
.B2.5:                          ; Preds .B2.4
        lea       ecx, DWORD PTR [56+esp]                       ;469.6
        mov       DWORD PTR [148+esp], ecx                      ;469.6
        lea       ecx, DWORD PTR [148+esp]                      ;469.6
        push      ecx                                           ;469.6
        push      OFFSET FLAT: __STRLITPACK_152.0.2             ;469.6
        push      ebx                                           ;469.6
        call      _for_read_seq_fmt_xmit                        ;469.6
                                ; LOE ebx esi edi
.B2.6:                          ; Preds .B2.5
        lea       ecx, DWORD PTR [104+esp]                      ;469.6
        mov       DWORD PTR [168+esp], ecx                      ;469.6
        lea       ecx, DWORD PTR [168+esp]                      ;469.6
        push      ecx                                           ;469.6
        push      OFFSET FLAT: __STRLITPACK_153.0.2             ;469.6
        push      ebx                                           ;469.6
        call      _for_read_seq_fmt_xmit                        ;469.6
                                ; LOE ebx esi edi
.B2.7:                          ; Preds .B2.6
        lea       ecx, DWORD PTR [120+esp]                      ;469.6
        mov       DWORD PTR [188+esp], ecx                      ;469.6
        lea       ecx, DWORD PTR [188+esp]                      ;469.6
        push      ecx                                           ;469.6
        push      OFFSET FLAT: __STRLITPACK_154.0.2             ;469.6
        push      ebx                                           ;469.6
        call      _for_read_seq_fmt_xmit                        ;469.6
                                ; LOE ebx esi edi
.B2.8:                          ; Preds .B2.7
        lea       ecx, DWORD PTR [136+esp]                      ;469.6
        mov       DWORD PTR [208+esp], ecx                      ;469.6
        lea       ecx, DWORD PTR [208+esp]                      ;469.6
        push      ecx                                           ;469.6
        push      OFFSET FLAT: __STRLITPACK_155.0.2             ;469.6
        push      ebx                                           ;469.6
        call      _for_read_seq_fmt_xmit                        ;469.6
                                ; LOE ebx esi edi
.B2.9:                          ; Preds .B2.8
        lea       ecx, DWORD PTR [152+esp]                      ;469.6
        mov       DWORD PTR [228+esp], ecx                      ;469.6
        lea       ecx, DWORD PTR [228+esp]                      ;469.6
        push      ecx                                           ;469.6
        push      OFFSET FLAT: __STRLITPACK_156.0.2             ;469.6
        push      ebx                                           ;469.6
        call      _for_read_seq_fmt_xmit                        ;469.6
                                ; LOE ebx esi edi
.B2.10:                         ; Preds .B2.9
        lea       ecx, DWORD PTR [168+esp]                      ;469.6
        mov       DWORD PTR [248+esp], ecx                      ;469.6
        lea       ecx, DWORD PTR [248+esp]                      ;469.6
        push      ecx                                           ;469.6
        push      OFFSET FLAT: __STRLITPACK_157.0.2             ;469.6
        push      ebx                                           ;469.6
        call      _for_read_seq_fmt_xmit                        ;469.6
                                ; LOE ebx esi edi
.B2.11:                         ; Preds .B2.10
        lea       ecx, DWORD PTR [184+esp]                      ;469.6
        mov       DWORD PTR [268+esp], ecx                      ;469.6
        lea       ecx, DWORD PTR [268+esp]                      ;469.6
        push      ecx                                           ;469.6
        push      OFFSET FLAT: __STRLITPACK_158.0.2             ;469.6
        push      ebx                                           ;469.6
        call      _for_read_seq_fmt_xmit                        ;469.6
                                ; LOE ebx esi edi
.B2.12:                         ; Preds .B2.11
        lea       ecx, DWORD PTR [200+esp]                      ;469.6
        mov       DWORD PTR [288+esp], ecx                      ;469.6
        lea       ecx, DWORD PTR [288+esp]                      ;469.6
        push      ecx                                           ;469.6
        push      OFFSET FLAT: __STRLITPACK_159.0.2             ;469.6
        push      ebx                                           ;469.6
        call      _for_read_seq_fmt_xmit                        ;469.6
                                ; LOE ebx esi edi
.B2.42:                         ; Preds .B2.12
        add       esp, 124                                      ;469.6
                                ; LOE ebx esi edi
.B2.13:                         ; Preds .B2.42
        lea       ecx, DWORD PTR [92+esp]                       ;469.6
        mov       DWORD PTR [184+esp], ecx                      ;469.6
        lea       ecx, DWORD PTR [184+esp]                      ;469.6
        push      ecx                                           ;469.6
        push      OFFSET FLAT: __STRLITPACK_160.0.2             ;469.6
        push      ebx                                           ;469.6
        call      _for_read_seq_fmt_xmit                        ;469.6
                                ; LOE ebx esi edi
.B2.14:                         ; Preds .B2.13
        lea       ecx, DWORD PTR [108+esp]                      ;469.6
        mov       DWORD PTR [204+esp], ecx                      ;469.6
        lea       ecx, DWORD PTR [204+esp]                      ;469.6
        push      ecx                                           ;469.6
        push      OFFSET FLAT: __STRLITPACK_161.0.2             ;469.6
        push      ebx                                           ;469.6
        call      _for_read_seq_fmt_xmit                        ;469.6
                                ; LOE ebx esi edi
.B2.15:                         ; Preds .B2.14
        lea       ecx, DWORD PTR [124+esp]                      ;469.6
        mov       DWORD PTR [224+esp], ecx                      ;469.6
        lea       ecx, DWORD PTR [224+esp]                      ;469.6
        push      ecx                                           ;469.6
        push      OFFSET FLAT: __STRLITPACK_162.0.2             ;469.6
        push      ebx                                           ;469.6
        call      _for_read_seq_fmt_xmit                        ;469.6
                                ; LOE ebx esi edi
.B2.16:                         ; Preds .B2.15
        lea       ecx, DWORD PTR [140+esp]                      ;469.6
        mov       DWORD PTR [244+esp], ecx                      ;469.6
        lea       ecx, DWORD PTR [244+esp]                      ;469.6
        push      ecx                                           ;469.6
        push      OFFSET FLAT: __STRLITPACK_163.0.2             ;469.6
        push      ebx                                           ;469.6
        call      _for_read_seq_fmt_xmit                        ;469.6
                                ; LOE ebx esi edi
.B2.43:                         ; Preds .B2.16
        add       esp, 48                                       ;469.6
                                ; LOE ebx esi edi
.B2.17:                         ; Preds .B2.43
        mov       ecx, DWORD PTR [72+esp]                       ;470.3
        cmp       ecx, 2                                        ;470.14
        je        .B2.37        ; Prob 16%                      ;470.14
                                ; LOE ecx ebx esi edi
.B2.18:                         ; Preds .B2.17
        cmp       ecx, 3                                        ;471.17
        jne       .B2.20        ; Prob 84%                      ;471.17
                                ; LOE ebx esi edi
.B2.19:                         ; Preds .B2.18
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_IUE_OK], 1  ;471.23
                                ; LOE ebx esi edi
.B2.20:                         ; Preds .B2.18 .B2.37 .B2.19
        mov       edx, DWORD PTR [88+esp]                       ;472.3
        test      edx, edx                                      ;472.3
        jle       .B2.25        ; Prob 2%                       ;472.3
                                ; LOE edx ebx esi edi
.B2.21:                         ; Preds .B2.20
        mov       eax, 1                                        ;
        xor       ebx, ebx                                      ;
        mov       DWORD PTR [8+esp], esi                        ;
        mov       esi, eax                                      ;
        mov       DWORD PTR [4+esp], edi                        ;
        mov       edi, edx                                      ;
                                ; LOE ebx esi edi
.B2.22:                         ; Preds .B2.23 .B2.21
        mov       DWORD PTR [32+esp], ebx                       ;473.11
        push      32                                            ;473.11
        push      ebx                                           ;473.11
        push      OFFSET FLAT: __STRLITPACK_164.0.2             ;473.11
        push      -2088435968                                   ;473.11
        push      500                                           ;473.11
        lea       ecx, DWORD PTR [52+esp]                       ;473.11
        push      ecx                                           ;473.11
        call      _for_read_seq_lis                             ;473.11
                                ; LOE ebx esi edi
.B2.44:                         ; Preds .B2.22
        add       esp, 24                                       ;473.11
                                ; LOE ebx esi edi
.B2.23:                         ; Preds .B2.44
        inc       esi                                           ;474.6
        cmp       esi, edi                                      ;474.6
        jle       .B2.22        ; Prob 82%                      ;474.6
                                ; LOE ebx esi edi
.B2.24:                         ; Preds .B2.23
        mov       esi, DWORD PTR [8+esp]                        ;
        lea       ebx, DWORD PTR [32+esp]                       ;
        mov       edi, DWORD PTR [4+esp]                        ;
                                ; LOE ebx esi edi
.B2.25:                         ; Preds .B2.24 .B2.20
        inc       esi                                           ;475.3
        cmp       esi, edi                                      ;475.3
        jle       .B2.3         ; Prob 82%                      ;475.3
                                ; LOE ebx esi edi
.B2.26:                         ; Preds .B2.38 .B2.25
        mov       esi, DWORD PTR [esp]                          ;
        xor       edi, edi                                      ;
                                ; LOE ebx esi edi
.B2.27:                         ; Preds .B2.26 .B2.47
        mov       DWORD PTR [32+esp], 0                         ;478.3
        push      32                                            ;478.3
        push      -2088435968                                   ;478.3
        push      500                                           ;478.3
        push      ebx                                           ;478.3
        call      _for_rewind                                   ;478.3
                                ; LOE ebx esi edi
.B2.28:                         ; Preds .B2.27
        mov       DWORD PTR [48+esp], edi                       ;479.3
        push      32                                            ;479.3
        push      edi                                           ;479.3
        push      OFFSET FLAT: __STRLITPACK_165.0.2             ;479.3
        push      -2088435968                                   ;479.3
        push      500                                           ;479.3
        push      ebx                                           ;479.3
        call      _for_read_seq_lis                             ;479.3
                                ; LOE ebx esi edi
.B2.29:                         ; Preds .B2.28
        mov       DWORD PTR [72+esp], edi                       ;480.3
        push      32                                            ;480.3
        push      edi                                           ;480.3
        push      OFFSET FLAT: __STRLITPACK_166.0.2             ;480.3
        push      -2088435968                                   ;480.3
        push      500                                           ;480.3
        push      ebx                                           ;480.3
        call      _for_read_seq_lis                             ;480.3
                                ; LOE ebx esi edi
.B2.45:                         ; Preds .B2.29
        add       esp, 64                                       ;480.3
                                ; LOE ebx esi edi
.B2.30:                         ; Preds .B2.45
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ISO_OK], 0  ;482.15
        jle       .B2.32        ; Prob 16%                      ;482.15
                                ; LOE ebx esi edi
.B2.31:                         ; Preds .B2.46 .B2.32 .B2.30
        add       esp, 216                                      ;490.1
        pop       ebx                                           ;490.1
        pop       edi                                           ;490.1
        mov       esp, ebp                                      ;490.1
        pop       ebp                                           ;490.1
        ret                                                     ;490.1
                                ; LOE
.B2.32:                         ; Preds .B2.30                  ; Infreq
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_IUE_OK], 0  ;482.30
        jg        .B2.31        ; Prob 84%                      ;482.30
                                ; LOE ebx esi edi
.B2.33:                         ; Preds .B2.32                  ; Infreq
        mov       DWORD PTR [32+esp], 0                         ;483.7
        lea       eax, DWORD PTR [esp]                          ;483.7
        mov       DWORD PTR [esp], 69                           ;483.7
        mov       DWORD PTR [4+esp], OFFSET FLAT: __STRLITPACK_149 ;483.7
        push      32                                            ;483.7
        push      eax                                           ;483.7
        push      OFFSET FLAT: __STRLITPACK_167.0.2             ;483.7
        push      -2088435968                                   ;483.7
        push      911                                           ;483.7
        push      ebx                                           ;483.7
        call      _for_write_seq_lis                            ;483.7
                                ; LOE ebx esi edi
.B2.34:                         ; Preds .B2.33                  ; Infreq
        mov       DWORD PTR [56+esp], 0                         ;484.7
        lea       eax, DWORD PTR [32+esp]                       ;484.7
        mov       DWORD PTR [32+esp], 58                        ;484.7
        mov       DWORD PTR [36+esp], OFFSET FLAT: __STRLITPACK_147 ;484.7
        push      32                                            ;484.7
        push      eax                                           ;484.7
        push      OFFSET FLAT: __STRLITPACK_168.0.2             ;484.7
        push      -2088435968                                   ;484.7
        push      911                                           ;484.7
        push      ebx                                           ;484.7
        call      _for_write_seq_lis                            ;484.7
                                ; LOE ebx esi edi
.B2.35:                         ; Preds .B2.34                  ; Infreq
        mov       eax, 32                                       ;485.4
        lea       edx, DWORD PTR [64+esp]                       ;485.4
        mov       DWORD PTR [80+esp], 0                         ;485.4
        mov       DWORD PTR [64+esp], eax                       ;485.4
        mov       DWORD PTR [68+esp], OFFSET FLAT: __STRLITPACK_145 ;485.4
        push      eax                                           ;485.4
        push      edx                                           ;485.4
        push      OFFSET FLAT: __STRLITPACK_169.0.2             ;485.4
        push      -2088435968                                   ;485.4
        push      911                                           ;485.4
        push      ebx                                           ;485.4
        call      _for_write_seq_lis                            ;485.4
                                ; LOE esi edi
.B2.36:                         ; Preds .B2.35                  ; Infreq
        push      32                                            ;486.4
        push      edi                                           ;486.4
        push      edi                                           ;486.4
        push      -2088435968                                   ;486.4
        push      edi                                           ;486.4
        push      OFFSET FLAT: __STRLITPACK_170                 ;486.4
        call      _for_stop_core                                ;486.4
                                ; LOE esi
.B2.46:                         ; Preds .B2.36                  ; Infreq
        add       esp, 96                                       ;486.4
        jmp       .B2.31        ; Prob 100%                     ;486.4
                                ; LOE esi
.B2.37:                         ; Preds .B2.17                  ; Infreq
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ISO_OK], 1  ;470.20
        jmp       .B2.20        ; Prob 100%                     ;470.20
                                ; LOE ebx esi edi
.B2.38:                         ; Preds .B2.3                   ; Infreq
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_IUE_OK], 1  ;468.32
        je        .B2.26        ; Prob 20%                      ;468.32
        jmp       .B2.4         ; Prob 100%                     ;468.32
                                ; LOE ebx esi edi
.B2.47:                         ; Preds .B2.1                   ; Infreq
        xor       edi, edi                                      ;473.11
        lea       ebx, DWORD PTR [32+esp]                       ;473.11
        jmp       .B2.27        ; Prob 100%                     ;473.11
        ALIGN     16
                                ; LOE ebx esi edi
; mark_end;
_CHECK_MIVA_VEHICLE ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
	DD 4 DUP (0H)	; pad
CHECK_MIVA_VEHICLE$format_pack.0.2	DB	54
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	9
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	2
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	2
	DB	1
	DB	0
	DB	0
	DB	0
	DB	8
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	6
	DB	0
	DB	0
	DB	0
	DB	6
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	4
	DB	2
	DB	0
	DB	0
	DB	0
	DB	8
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	2
	DB	0
	DB	0
	DB	0
	DB	5
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	8
	DB	1
	DB	0
	DB	0
	DB	0
	DB	12
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	1
	DB	1
	DB	0
	DB	0
	DB	0
	DB	6
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
_DATA	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
__STRLITPACK_151.0.2	DB	9
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_152.0.2	DB	9
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_153.0.2	DB	9
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_154.0.2	DB	26
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_155.0.2	DB	9
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_156.0.2	DB	9
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_157.0.2	DB	9
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_158.0.2	DB	9
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_159.0.2	DB	9
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_160.0.2	DB	9
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_161.0.2	DB	26
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_162.0.2	DB	26
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_163.0.2	DB	9
	DB	3
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_164.0.2	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_165.0.2	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_166.0.2	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_167.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_168.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_169.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _CHECK_MIVA_VEHICLE
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DD 7 DUP (0H)	; pad
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_43	DB	73
	DB	110
	DB	32
	DB	71
	DB	85
	DB	73
	DB	44
	DB	32
	DB	112
	DB	108
	DB	101
	DB	97
	DB	115
	DB	101
	DB	32
	DB	103
	DB	111
	DB	32
	DB	116
	DB	111
	DB	32
	DB	112
	DB	114
	DB	111
	DB	106
	DB	101
	DB	99
	DB	116
	DB	45
	DB	62
	DB	115
	DB	99
	DB	101
	DB	110
	DB	97
	DB	114
	DB	105
	DB	111
	DB	32
	DB	100
	DB	97
	DB	116
	DB	97
	DB	45
	DB	62
	DB	32
	DB	115
	DB	99
	DB	101
	DB	110
	DB	97
	DB	114
	DB	105
	DB	111
	DB	45
	DB	62
	DB	32
	DB	97
	DB	100
	DB	118
	DB	97
	DB	110
	DB	99
	DB	101
	DB	100
	DB	32
	DB	80
	DB	65
	DB	82
	DB	65
	DB	77
	DB	69
	DB	84
	DB	69
	DB	82
	DB	32
	DB	115
	DB	101
	DB	116
	DB	116
	DB	105
	DB	110
	DB	103
	DB	0
	DD 3 DUP (0H)	; pad
__STRLITPACK_9	DB	73
	DB	110
	DB	32
	DB	71
	DB	85
	DB	73
	DB	44
	DB	32
	DB	112
	DB	108
	DB	101
	DB	97
	DB	115
	DB	101
	DB	32
	DB	103
	DB	111
	DB	32
	DB	116
	DB	111
	DB	32
	DB	112
	DB	114
	DB	111
	DB	106
	DB	101
	DB	99
	DB	116
	DB	45
	DB	62
	DB	115
	DB	99
	DB	101
	DB	110
	DB	97
	DB	114
	DB	105
	DB	111
	DB	32
	DB	100
	DB	97
	DB	116
	DB	97
	DB	45
	DB	62
	DB	32
	DB	115
	DB	99
	DB	101
	DB	110
	DB	97
	DB	114
	DB	105
	DB	111
	DB	45
	DB	62
	DB	32
	DB	97
	DB	100
	DB	118
	DB	97
	DB	110
	DB	99
	DB	101
	DB	100
	DB	32
	DB	80
	DB	65
	DB	82
	DB	65
	DB	77
	DB	69
	DB	84
	DB	69
	DB	82
	DB	32
	DB	115
	DB	101
	DB	116
	DB	116
	DB	105
	DB	110
	DB	103
	DB	0
	DD 3 DUP (0H)	; pad
__STRLITPACK_149	DB	73
	DB	116
	DB	101
	DB	114
	DB	97
	DB	116
	DB	105
	DB	118
	DB	101
	DB	32
	DB	109
	DB	111
	DB	100
	DB	101
	DB	32
	DB	97
	DB	110
	DB	100
	DB	32
	DB	118
	DB	101
	DB	104
	DB	105
	DB	99
	DB	108
	DB	101
	DB	32
	DB	102
	DB	105
	DB	108
	DB	101
	DB	32
	DB	108
	DB	111
	DB	97
	DB	100
	DB	105
	DB	110
	DB	103
	DB	32
	DB	109
	DB	111
	DB	100
	DB	101
	DB	108
	DB	32
	DB	104
	DB	97
	DB	115
	DB	32
	DB	98
	DB	101
	DB	101
	DB	110
	DB	32
	DB	115
	DB	112
	DB	101
	DB	99
	DB	105
	DB	102
	DB	105
	DB	101
	DB	100
	DB	44
	DB	32
	DB	98
	DB	117
	DB	116
	DB	0
	DD 2 DUP (0H)	; pad
	DB 2 DUP ( 0H)	; pad
_2il0floatpacket.6	DD	042700000H,042700000H,042700000H,042700000H
_2il0floatpacket.8	DD	042c80000H,042c80000H,042c80000H,042c80000H
__STRLITPACK_61	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	118
	DB	101
	DB	104
	DB	105
	DB	99
	DB	108
	DB	101
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_59	DB	84
	DB	104
	DB	101
	DB	32
	DB	110
	DB	117
	DB	109
	DB	98
	DB	101
	DB	114
	DB	32
	DB	111
	DB	102
	DB	32
	DB	118
	DB	101
	DB	104
	DB	105
	DB	99
	DB	108
	DB	101
	DB	115
	DB	32
	DB	108
	DB	105
	DB	115
	DB	116
	DB	101
	DB	100
	DB	32
	DB	105
	DB	110
	DB	32
	DB	116
	DB	104
	DB	105
	DB	115
	DB	32
	DB	102
	DB	105
	DB	108
	DB	101
	DB	32
	DB	105
	DB	115
	DB	32
	DB	108
	DB	101
	DB	115
	DB	115
	DB	32
	DB	116
	DB	104
	DB	97
	DB	110
	DB	0
__STRLITPACK_57	DB	115
	DB	112
	DB	101
	DB	99
	DB	105
	DB	102
	DB	105
	DB	101
	DB	100
	DB	32
	DB	97
	DB	116
	DB	32
	DB	116
	DB	104
	DB	101
	DB	32
	DB	102
	DB	105
	DB	114
	DB	115
	DB	116
	DB	32
	DB	108
	DB	105
	DB	110
	DB	101
	DB	0
__STRLITPACK_86	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_55	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	116
	DB	114
	DB	105
	DB	112
	DB	99
	DB	104
	DB	97
	DB	105
	DB	110
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_53	DB	116
	DB	104
	DB	101
	DB	32
	DB	109
	DB	97
	DB	120
	DB	105
	DB	109
	DB	117
	DB	109
	DB	32
	DB	110
	DB	117
	DB	109
	DB	98
	DB	101
	DB	114
	DB	32
	DB	111
	DB	102
	DB	32
	DB	83
	DB	84
	DB	79
	DB	80
	DB	115
	DB	32
	DB	105
	DB	115
	DB	32
	DB	101
	DB	120
	DB	99
	DB	101
	DB	101
	DB	100
	DB	101
	DB	100
	DB	0
__STRLITPACK_51	DB	99
	DB	104
	DB	101
	DB	99
	DB	107
	DB	32
	DB	116
	DB	104
	DB	101
	DB	32
	DB	118
	DB	101
	DB	104
	DB	105
	DB	99
	DB	108
	DB	101
	DB	32
	DB	110
	DB	117
	DB	109
	DB	98
	DB	101
	DB	114
	DB	32
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_91	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_49	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	118
	DB	101
	DB	104
	DB	105
	DB	99
	DB	108
	DB	101
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_47	DB	32
	DB	102
	DB	111
	DB	117
	DB	110
	DB	100
	DB	32
	DB	116
	DB	104
	DB	101
	DB	32
	DB	115
	DB	97
	DB	109
	DB	101
	DB	32
	DB	100
	DB	101
	DB	115
	DB	116
	DB	32
	DB	105
	DB	110
	DB	32
	DB	99
	DB	111
	DB	110
	DB	115
	DB	101
	DB	99
	DB	117
	DB	116
	DB	105
	DB	118
	DB	101
	DB	32
	DB	111
	DB	114
	DB	100
	DB	101
	DB	114
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_96	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_45	DB	78
	DB	101
	DB	101
	DB	100
	DB	32
	DB	116
	DB	111
	DB	32
	DB	105
	DB	110
	DB	99
	DB	114
	DB	101
	DB	97
	DB	115
	DB	101
	DB	32
	DB	116
	DB	109
	DB	112
	DB	97
	DB	114
	DB	121
	DB	115
	DB	105
	DB	122
	DB	101
	DB	32
	DB	105
	DB	110
	DB	32
	DB	80
	DB	65
	DB	82
	DB	65
	DB	77
	DB	69
	DB	84
	DB	69
	DB	82
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_41	DB	79
	DB	114
	DB	32
	DB	111
	DB	110
	DB	101
	DB	32
	DB	99
	DB	97
	DB	110
	DB	32
	DB	100
	DB	105
	DB	114
	DB	101
	DB	99
	DB	116
	DB	108
	DB	121
	DB	32
	DB	109
	DB	111
	DB	100
	DB	105
	DB	102
	DB	121
	DB	32
	DB	80
	DB	65
	DB	82
	DB	65
	DB	77
	DB	69
	DB	84
	DB	69
	DB	82
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_100	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_39	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	114
	DB	101
	DB	97
	DB	100
	DB	105
	DB	110
	DB	103
	DB	32
	DB	118
	DB	101
	DB	104
	DB	105
	DB	99
	DB	108
	DB	101
	DB	32
	DB	102
	DB	105
	DB	108
	DB	101
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_37	DB	73
	DB	68
	DB	32
	DB	61
	DB	32
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_35	DB	85
	DB	80
	DB	32
	DB	65
	DB	78
	DB	68
	DB	32
	DB	68
	DB	79
	DB	87
	DB	78
	DB	83
	DB	84
	DB	82
	DB	69
	DB	65
	DB	77
	DB	32
	DB	78
	DB	79
	DB	68
	DB	69
	DB	83
	DB	0
__STRLITPACK_107	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_33	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	114
	DB	101
	DB	97
	DB	100
	DB	32
	DB	118
	DB	101
	DB	104
	DB	105
	DB	99
	DB	108
	DB	101
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_30	DB	99
	DB	104
	DB	101
	DB	99
	DB	107
	DB	32
	DB	118
	DB	101
	DB	104
	DB	105
	DB	99
	DB	108
	DB	101
	DB	32
	DB	35
	DB	0
__STRLITPACK_29	DB	117
	DB	112
	DB	115
	DB	116
	DB	114
	DB	101
	DB	97
	DB	109
	DB	32
	DB	97
	DB	110
	DB	100
	DB	32
	DB	100
	DB	111
	DB	119
	DB	110
	DB	115
	DB	116
	DB	114
	DB	101
	DB	97
	DB	109
	DB	32
	DB	110
	DB	111
	DB	100
	DB	101
	DB	115
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_112	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_27	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	114
	DB	101
	DB	97
	DB	100
	DB	105
	DB	110
	DB	103
	DB	32
	DB	111
	DB	114
	DB	105
	DB	103
	DB	105
	DB	110
	DB	32
	DB	122
	DB	111
	DB	110
	DB	101
	DB	32
	DB	102
	DB	111
	DB	114
	DB	32
	DB	118
	DB	101
	DB	104
	DB	105
	DB	99
	DB	108
	DB	101
	DB	115
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_25	DB	86
	DB	101
	DB	104
	DB	105
	DB	99
	DB	108
	DB	101
	DB	32
	DB	73
	DB	68
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_23	DB	86
	DB	101
	DB	104
	DB	32
	DB	71
	DB	101
	DB	110
	DB	101
	DB	114
	DB	97
	DB	116
	DB	105
	DB	111
	DB	110
	DB	32
	DB	108
	DB	105
	DB	110
	DB	107
	DB	0
__STRLITPACK_21	DB	71
	DB	101
	DB	110
	DB	32
	DB	76
	DB	105
	DB	110
	DB	107
	DB	32
	DB	105
	DB	110
	DB	32
	DB	116
	DB	104
	DB	101
	DB	32
	DB	90
	DB	111
	DB	110
	DB	101
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_123	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_19	DB	66
	DB	117
	DB	115
	DB	32
	DB	35
	DB	32
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_17	DB	82
	DB	69
	DB	65
	DB	68
	DB	32
	DB	86
	DB	69
	DB	72
	DB	73
	DB	67
	DB	76
	DB	69
	DB	32
	DB	35
	DB	32
	DB	80
	DB	65
	DB	84
	DB	72
	DB	0
__STRLITPACK_11	DB	78
	DB	101
	DB	101
	DB	100
	DB	32
	DB	116
	DB	111
	DB	32
	DB	105
	DB	110
	DB	99
	DB	114
	DB	101
	DB	97
	DB	115
	DB	101
	DB	32
	DB	116
	DB	109
	DB	112
	DB	97
	DB	114
	DB	121
	DB	115
	DB	105
	DB	122
	DB	101
	DB	32
	DB	105
	DB	110
	DB	32
	DB	80
	DB	65
	DB	82
	DB	65
	DB	77
	DB	69
	DB	84
	DB	69
	DB	82
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_7	DB	79
	DB	114
	DB	32
	DB	111
	DB	110
	DB	101
	DB	32
	DB	99
	DB	97
	DB	110
	DB	32
	DB	100
	DB	105
	DB	114
	DB	101
	DB	99
	DB	116
	DB	108
	DB	121
	DB	32
	DB	109
	DB	111
	DB	100
	DB	105
	DB	102
	DB	121
	DB	32
	DB	80
	DB	65
	DB	82
	DB	65
	DB	77
	DB	69
	DB	84
	DB	69
	DB	82
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_144	DB	0
	DB 3 DUP ( 0H)	; pad
_2il0floatpacket.2	DD	03dcccccdH
_2il0floatpacket.3	DD	042c80000H
_2il0floatpacket.4	DD	0322bcc77H
_2il0floatpacket.5	DD	043160000H
_2il0floatpacket.7	DD	042700000H
_2il0floatpacket.9	DD	03f800000H
_2il0floatpacket.10	DD	080000000H
_2il0floatpacket.11	DD	04b000000H
_2il0floatpacket.12	DD	03f000000H
_2il0floatpacket.13	DD	0bf000000H
__STRLITPACK_147	DB	78
	DB	111
	DB	32
	DB	85
	DB	69
	DB	32
	DB	111
	DB	114
	DB	32
	DB	83
	DB	79
	DB	32
	DB	118
	DB	101
	DB	104
	DB	105
	DB	99
	DB	108
	DB	101
	DB	115
	DB	32
	DB	119
	DB	101
	DB	114
	DB	101
	DB	32
	DB	102
	DB	111
	DB	117
	DB	110
	DB	100
	DB	32
	DB	102
	DB	111
	DB	114
	DB	32
	DB	116
	DB	104
	DB	101
	DB	32
	DB	105
	DB	116
	DB	101
	DB	114
	DB	97
	DB	116
	DB	105
	DB	118
	DB	101
	DB	32
	DB	109
	DB	116
	DB	99
	DB	32
	DB	109
	DB	111
	DB	100
	DB	101
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_145	DB	80
	DB	108
	DB	101
	DB	97
	DB	115
	DB	101
	DB	32
	DB	99
	DB	104
	DB	101
	DB	99
	DB	107
	DB	32
	DB	118
	DB	101
	DB	104
	DB	105
	DB	99
	DB	108
	DB	101
	DB	46
	DB	100
	DB	97
	DB	116
	DB	32
	DB	115
	DB	101
	DB	116
	DB	116
	DB	105
	DB	110
	DB	103
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_170	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
EXTRN	_DYNUST_MAIN_MODULE_mp_TOTALVEH:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_IPINIT:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NOOF_MASTER_DESTINATIONS:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_CALLKSPREADVEH:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_UETRAVELPENALTY:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_UETRAVELTIME:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TRAVELTIME:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_SIMPERAGG:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_UETIMEFLAGR:BYTE
EXTRN	_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLOD:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_INCI_NUM:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_INFOSTARTTIME:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_READHISTARR:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NUMCARS:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_MASTERDEST:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_DESTINATION:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ENDTM:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_STARTTM:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_MTC_VEH:BYTE
EXTRN	_DYNUST_FUEL_MODULE_mp_VEHFUEL:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_FUELOUT:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_LOGOUT:BYTE
EXTRN	_DYNUST_VEH_MODULE_mp_NOOFBUSES:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_COM_FRAC:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_RIBFA:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE:BYTE
EXTRN	_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM:BYTE
EXTRN	_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NOOFSTOPS:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_HOVOK:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TRUCKOK:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_READVEH:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_KEEPTYPE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_MAXID:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_MIXEDVEHGENMAP:BYTE
EXTRN	_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_RUNMODE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_JUSTVEH:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TDSPSTEP:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_IUE_OK:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_IER_OK:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_KEEPCLS:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_MAXVEHICLESFILE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_IREAD_VEH_FLAG:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_IREAD_VEH_COUNT:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_JRESTORE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_IVCT:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NZONES:BYTE
EXTRN	_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLODZONE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TMPARYSIZE:BYTE
EXTRN	_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHLODTMP:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_VEHPROFILE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ISO_OK:BYTE
_DATA	ENDS
EXTRN	_for_rewind:PROC
EXTRN	_for_read_seq_lis:PROC
EXTRN	_for_dealloc_allocatable:PROC
EXTRN	_for_write_seq_lis_xmit:PROC
EXTRN	_for_stop_core:PROC
EXTRN	_for_write_seq_lis:PROC
EXTRN	_for_write_seq_fmt_xmit:PROC
EXTRN	_for_write_seq_fmt:PROC
EXTRN	_for_read_seq_fmt_xmit:PROC
EXTRN	_for_read_seq_fmt:PROC
EXTRN	_for_alloc_allocatable:PROC
EXTRN	_for_check_mult_overflow:PROC
EXTRN	_DYNUST_NETWORK_MODULE_mp_GETFLINKFROMNODE:PROC
EXTRN	_DYNUST_FUEL_MODULE_mp_SETFUELLEVEL:PROC
EXTRN	_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_INSERT:PROC
EXTRN	_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ACTIVE_SIZE:PROC
EXTRN	_DYNUST_LINK_VEH_LIST_MODULE_mp_ENTRYQUEUE_INSERT:PROC
EXTRN	_DYNUST_VEH_MODULE_mp_SETDELAYTOLE:PROC
EXTRN	_DYNUST_ROUTE_SWITCH_MODULE_mp_GET_AUTO_REMAIN_TIME:PROC
EXTRN	_RANXY:PROC
EXTRN	_CAL_PENALTY:PROC
EXTRN	_TDSP_MAIN:PROC
EXTRN	_IMSLSORT:PROC
EXTRN	_RETRIEVE_VEH_PATH_ASTAR:PROC
EXTRN	_WRITE_VEHICLES:PROC
EXTRN	__intel_fast_memcpy:PROC
EXTRN	__intel_fast_memset:PROC
EXTRN	__fltused:BYTE
	INCLUDELIB <ifconsol>
	INCLUDELIB <libifcoremt>
	INCLUDELIB <libifport>
	INCLUDELIB <libmmt>
	INCLUDELIB <LIBCMT>
	INCLUDELIB <libirc>
	INCLUDELIB <svml_dispmt>
	INCLUDELIB <OLDNAMES>
	END
