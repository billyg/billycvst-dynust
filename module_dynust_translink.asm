; mark_description "Intel(R) Visual Fortran Compiler XE for applications running on IA-32, Version 13.1.1.171 Build 20130313";
; mark_description "-S";
	.686P
 	.387
	OPTION DOTNAME
	ASSUME	CS:FLAT,DS:FLAT,SS:FLAT
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
TXTST0:
; -- Begin  _DYNUST_TRANSLINK_MODULE$
; mark_begin;
IF @Version GE 800
  .MMX
ELSEIF @Version GE 612
  .MMX
  MMWORD TEXTEQU <QWORD>
ENDIF
IF @Version GE 800
  .XMM
ELSEIF @Version GE 614
  .XMM
  XMMWORD TEXTEQU <OWORD>
ENDIF
       ALIGN     16
	PUBLIC _DYNUST_TRANSLINK_MODULE$
_DYNUST_TRANSLINK_MODULE$	PROC NEAR 
.B1.1:                          ; Preds .B1.0
        ret                                                     ;1.8
        ALIGN     16
                                ; LOE
; mark_end;
_DYNUST_TRANSLINK_MODULE$ ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_TRANSLINK_MODULE$
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_TRANSLINK_MODULE_mp_TRANLINK_2DSETUP
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_TRANSLINK_MODULE_mp_TRANLINK_2DSETUP
_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_2DSETUP	PROC NEAR 
; parameter 1: 8 + ebp
.B2.1:                          ; Preds .B2.0
        push      ebp                                           ;48.12
        mov       ebp, esp                                      ;48.12
        and       esp, -16                                      ;48.12
        push      esi                                           ;48.12
        push      edi                                           ;48.12
        push      ebx                                           ;48.12
        sub       esp, 132                                      ;48.12
        mov       esi, DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY+12] ;53.3
        test      esi, 1                                        ;53.7
        je        .B2.13        ; Prob 60%                      ;53.7
                                ; LOE esi
.B2.2:                          ; Preds .B2.1
        mov       eax, DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAYSIZE] ;54.10
        test      eax, eax                                      ;54.10
        jle       .B2.10        ; Prob 2%                       ;54.10
                                ; LOE eax esi
.B2.3:                          ; Preds .B2.2
        imul      ebx, DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY+32], -40 ;
        mov       edi, 1                                        ;
        add       ebx, DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY] ;
        mov       DWORD PTR [4+esp], eax                        ;
        mov       DWORD PTR [esp], esi                          ;
                                ; LOE ebx edi
.B2.4:                          ; Preds .B2.8 .B2.3
        lea       esi, DWORD PTR [edi+edi*4]                    ;54.10
        movsx     eax, WORD PTR [36+ebx+esi*8]                  ;54.10
        test      eax, eax                                      ;54.10
        jle       .B2.8         ; Prob 79%                      ;54.10
                                ; LOE ebx esi edi
.B2.5:                          ; Preds .B2.4
        mov       eax, DWORD PTR [12+ebx+esi*8]                 ;54.10
        mov       edx, eax                                      ;54.10
        shr       edx, 1                                        ;54.10
        and       eax, 1                                        ;54.10
        and       edx, 1                                        ;54.10
        add       eax, eax                                      ;54.10
        shl       edx, 2                                        ;54.10
        or        edx, 1                                        ;54.10
        or        edx, eax                                      ;54.10
        or        edx, 262144                                   ;54.10
        push      edx                                           ;54.10
        push      DWORD PTR [ebx+esi*8]                         ;54.10
        call      _for_dealloc_allocatable                      ;54.10
                                ; LOE eax ebx esi edi
.B2.33:                         ; Preds .B2.5
        add       esp, 8                                        ;54.10
                                ; LOE eax ebx esi edi
.B2.6:                          ; Preds .B2.33
        and       DWORD PTR [12+ebx+esi*8], -2                  ;54.10
        mov       DWORD PTR [ebx+esi*8], 0                      ;54.10
        test      eax, eax                                      ;54.10
        mov       DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_VECTORERROR], eax ;54.10
        jne       .B2.25        ; Prob 5%                       ;54.10
                                ; LOE ebx esi edi
.B2.7:                          ; Preds .B2.6 .B2.40
        xor       eax, eax                                      ;54.10
        mov       WORD PTR [36+ebx+esi*8], ax                   ;54.10
        mov       WORD PTR [38+ebx+esi*8], ax                   ;54.10
                                ; LOE ebx edi
.B2.8:                          ; Preds .B2.4 .B2.7
        inc       edi                                           ;54.10
        cmp       edi, DWORD PTR [4+esp]                        ;54.10
        jle       .B2.4         ; Prob 82%                      ;54.10
                                ; LOE ebx edi
.B2.9:                          ; Preds .B2.8
        mov       esi, DWORD PTR [esp]                          ;
                                ; LOE esi
.B2.10:                         ; Preds .B2.9 .B2.2
        mov       edx, esi                                      ;54.10
        mov       eax, esi                                      ;54.10
        shr       edx, 1                                        ;54.10
        and       eax, 1                                        ;54.10
        and       edx, 1                                        ;54.10
        add       eax, eax                                      ;54.10
        shl       edx, 2                                        ;54.10
        or        edx, 1                                        ;54.10
        or        edx, eax                                      ;54.10
        or        edx, 262144                                   ;54.10
        push      edx                                           ;54.10
        push      DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY] ;54.10
        call      _for_dealloc_allocatable                      ;54.10
                                ; LOE eax esi
.B2.34:                         ; Preds .B2.10
        add       esp, 8                                        ;54.10
                                ; LOE eax esi
.B2.11:                         ; Preds .B2.34
        and       esi, -2                                       ;54.10
        mov       DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY], 0 ;54.10
        test      eax, eax                                      ;54.10
        mov       DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY+12], esi ;54.10
        mov       DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_VECTORERROR], eax ;54.10
        jne       .B2.23        ; Prob 5%                       ;54.10
                                ; LOE esi
.B2.12:                         ; Preds .B2.39 .B2.11
        mov       DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAYSIZE], 0 ;54.10
                                ; LOE esi
.B2.13:                         ; Preds .B2.12 .B2.1
        mov       ebx, DWORD PTR [8+ebp]                        ;48.12
        test      esi, 1                                        ;59.13
        jne       .B2.21        ; Prob 40%                      ;59.13
                                ; LOE ebx
.B2.14:                         ; Preds .B2.13
        xor       edi, edi                                      ;60.5
        lea       edx, DWORD PTR [128+esp]                      ;60.5
        mov       eax, DWORD PTR [ebx]                          ;60.5
        test      eax, eax                                      ;60.5
        push      40                                            ;60.5
        cmovl     eax, edi                                      ;60.5
        push      eax                                           ;60.5
        push      2                                             ;60.5
        push      edx                                           ;60.5
        call      _for_check_mult_overflow                      ;60.5
                                ; LOE eax ebx edi
.B2.15:                         ; Preds .B2.14
        mov       edx, DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY+12] ;60.5
        and       eax, 1                                        ;60.5
        and       edx, 1                                        ;60.5
        add       edx, edx                                      ;60.5
        shl       eax, 4                                        ;60.5
        or        edx, 1                                        ;60.5
        or        edx, eax                                      ;60.5
        or        edx, 262144                                   ;60.5
        push      edx                                           ;60.5
        push      OFFSET FLAT: _DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY ;60.5
        push      DWORD PTR [152+esp]                           ;60.5
        call      _for_alloc_allocatable                        ;60.5
                                ; LOE eax ebx edi
.B2.36:                         ; Preds .B2.15
        add       esp, 28                                       ;60.5
        mov       esi, eax                                      ;60.5
                                ; LOE ebx esi edi
.B2.16:                         ; Preds .B2.36
        test      esi, esi                                      ;60.5
        je        .B2.19        ; Prob 50%                      ;60.5
                                ; LOE ebx esi edi
.B2.17:                         ; Preds .B2.16
        mov       DWORD PTR [80+esp], 0                         ;62.5
        lea       edx, DWORD PTR [80+esp]                       ;62.5
        mov       DWORD PTR [120+esp], 26                       ;62.5
        lea       eax, DWORD PTR [120+esp]                      ;62.5
        mov       DWORD PTR [124+esp], OFFSET FLAT: __STRLITPACK_26 ;62.5
        push      32                                            ;62.5
        push      eax                                           ;62.5
        push      OFFSET FLAT: __STRLITPACK_28.0.2              ;62.5
        push      -2088435968                                   ;62.5
        push      911                                           ;62.5
        push      edx                                           ;62.5
        mov       DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_VECTORERROR], esi ;60.5
        call      _for_write_seq_lis                            ;62.5
                                ; LOE ebx edi
.B2.18:                         ; Preds .B2.17
        push      32                                            ;63.4
        push      edi                                           ;63.4
        push      edi                                           ;63.4
        push      -2088435968                                   ;63.4
        push      edi                                           ;63.4
        push      OFFSET FLAT: __STRLITPACK_29                  ;63.4
        call      _for_stop_core                                ;63.4
                                ; LOE ebx
.B2.37:                         ; Preds .B2.18
        add       esp, 48                                       ;63.4
        jmp       .B2.21        ; Prob 100%                     ;63.4
                                ; LOE ebx
.B2.19:                         ; Preds .B2.16
        mov       eax, 40                                       ;60.5
        mov       edx, 1                                        ;60.5
        mov       DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY+12], 133 ;60.5
        mov       DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY+4], eax ;60.5
        mov       DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY+16], edx ;60.5
        mov       DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY+8], edi ;60.5
        mov       DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY+32], edx ;60.5
        lea       edx, DWORD PTR [116+esp]                      ;60.5
        mov       ecx, DWORD PTR [ebx]                          ;60.5
        test      ecx, ecx                                      ;60.5
        push      eax                                           ;60.5
        cmovge    edi, ecx                                      ;60.5
        push      edi                                           ;60.5
        push      2                                             ;60.5
        push      edx                                           ;60.5
        mov       DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY+24], edi ;60.5
        mov       DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY+28], eax ;60.5
        call      _for_check_mult_overflow                      ;60.5
                                ; LOE ebx esi
.B2.38:                         ; Preds .B2.19
        add       esp, 16                                       ;60.5
                                ; LOE ebx esi
.B2.20:                         ; Preds .B2.38
        mov       DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_VECTORERROR], esi ;60.5
                                ; LOE ebx
.B2.21:                         ; Preds .B2.37 .B2.13 .B2.20
        mov       esi, DWORD PTR [ebx]                          ;68.3
        test      esi, esi                                      ;68.3
        jg        .B2.28        ; Prob 2%                       ;68.3
                                ; LOE ebx esi
.B2.22:                         ; Preds .B2.30 .B2.21
        mov       DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAYSIZE], esi ;73.3
        add       esp, 132                                      ;76.1
        pop       ebx                                           ;76.1
        pop       edi                                           ;76.1
        pop       esi                                           ;76.1
        mov       esp, ebp                                      ;76.1
        pop       ebp                                           ;76.1
        ret                                                     ;76.1
                                ; LOE
.B2.23:                         ; Preds .B2.11                  ; Infreq
        mov       DWORD PTR [esp], 0                            ;54.10
        lea       edx, DWORD PTR [esp]                          ;54.10
        mov       DWORD PTR [32+esp], 16                        ;54.10
        lea       eax, DWORD PTR [32+esp]                       ;54.10
        mov       DWORD PTR [36+esp], OFFSET FLAT: __STRLITPACK_0 ;54.10
        push      32                                            ;54.10
        push      eax                                           ;54.10
        push      OFFSET FLAT: __STRLITPACK_55.0.13             ;54.10
        push      -2088435968                                   ;54.10
        push      911                                           ;54.10
        push      edx                                           ;54.10
        call      _for_write_seq_lis                            ;54.10
                                ; LOE esi
.B2.24:                         ; Preds .B2.23                  ; Infreq
        push      32                                            ;54.10
        xor       eax, eax                                      ;54.10
        push      eax                                           ;54.10
        push      eax                                           ;54.10
        push      -2088435968                                   ;54.10
        push      eax                                           ;54.10
        push      OFFSET FLAT: __STRLITPACK_56                  ;54.10
        call      _for_stop_core                                ;54.10
                                ; LOE esi
.B2.39:                         ; Preds .B2.24                  ; Infreq
        add       esp, 48                                       ;54.10
        jmp       .B2.12        ; Prob 100%                     ;54.10
                                ; LOE esi
.B2.25:                         ; Preds .B2.6                   ; Infreq
        mov       DWORD PTR [48+esp], 0                         ;54.10
        lea       edx, DWORD PTR [48+esp]                       ;54.10
        mov       DWORD PTR [40+esp], 37                        ;54.10
        lea       eax, DWORD PTR [40+esp]                       ;54.10
        mov       DWORD PTR [44+esp], OFFSET FLAT: __STRLITPACK_2 ;54.10
        push      32                                            ;54.10
        push      eax                                           ;54.10
        push      OFFSET FLAT: __STRLITPACK_52.0.11             ;54.10
        push      -2088435968                                   ;54.10
        push      911                                           ;54.10
        push      edx                                           ;54.10
        call      _for_write_seq_lis                            ;54.10
                                ; LOE ebx esi edi
.B2.26:                         ; Preds .B2.25                  ; Infreq
        mov       DWORD PTR [72+esp], 0                         ;54.10
        lea       eax, DWORD PTR [136+esp]                      ;54.10
        mov       DWORD PTR [136+esp], edi                      ;54.10
        push      32                                            ;54.10
        push      eax                                           ;54.10
        push      OFFSET FLAT: __STRLITPACK_53.0.11             ;54.10
        push      -2088435968                                   ;54.10
        push      911                                           ;54.10
        lea       edx, DWORD PTR [92+esp]                       ;54.10
        push      edx                                           ;54.10
        call      _for_write_seq_lis                            ;54.10
                                ; LOE ebx esi edi
.B2.27:                         ; Preds .B2.26                  ; Infreq
        push      32                                            ;54.10
        xor       eax, eax                                      ;54.10
        push      eax                                           ;54.10
        push      eax                                           ;54.10
        push      -2088435968                                   ;54.10
        push      eax                                           ;54.10
        push      OFFSET FLAT: __STRLITPACK_54                  ;54.10
        call      _for_stop_core                                ;54.10
                                ; LOE ebx esi edi
.B2.40:                         ; Preds .B2.27                  ; Infreq
        add       esp, 72                                       ;54.10
        jmp       .B2.7         ; Prob 100%                     ;54.10
                                ; LOE ebx esi edi
.B2.28:                         ; Preds .B2.21                  ; Infreq
        imul      eax, DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY+32], -40 ;
        xor       ecx, ecx                                      ;
        add       eax, DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY] ;
        xor       edx, edx                                      ;
                                ; LOE eax edx ecx ebx esi
.B2.29:                         ; Preds .B2.29 .B2.28           ; Infreq
        lea       edi, DWORD PTR [ecx+ecx*4]                    ;69.4
        inc       ecx                                           ;68.3
        mov       WORD PTR [76+eax+edi*8], dx                   ;69.4
        cmp       ecx, esi                                      ;68.3
        mov       WORD PTR [78+eax+edi*8], dx                   ;69.4
        jb        .B2.29        ; Prob 99%                      ;68.3
                                ; LOE eax edx ecx ebx esi
.B2.30:                         ; Preds .B2.29                  ; Infreq
        mov       esi, DWORD PTR [ebx]                          ;73.3
        jmp       .B2.22        ; Prob 100%                     ;73.3
        ALIGN     16
                                ; LOE esi
; mark_end;
_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_2DSETUP ENDP
_TEXT	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
__STRLITPACK_28.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_TRANSLINK_MODULE_mp_TRANLINK_2DSETUP
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_TRANSLINK_MODULE_mp_TRANLINK_AVSIZEINCRE
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_TRANSLINK_MODULE_mp_TRANLINK_AVSIZEINCRE
_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_AVSIZEINCRE	PROC NEAR 
; parameter 1: 4 + esp
.B3.1:                          ; Preds .B3.0
        mov       eax, DWORD PTR [4+esp]                        ;152.12
        mov       edx, DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY+32] ;153.1
        neg       edx                                           ;153.1
        add       edx, DWORD PTR [eax]                          ;153.1
        mov       ecx, DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY] ;153.1
        lea       eax, DWORD PTR [edx+edx*4]                    ;153.1
        inc       WORD PTR [38+ecx+eax*8]                       ;153.55
        ret                                                     ;154.1
        ALIGN     16
                                ; LOE
; mark_end;
_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_AVSIZEINCRE ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_TRANSLINK_MODULE_mp_TRANLINK_AVSIZEINCRE
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_TRANSLINK_MODULE_mp_TRANLINK_AVSIZEDECRE
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_TRANSLINK_MODULE_mp_TRANLINK_AVSIZEDECRE
_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_AVSIZEDECRE	PROC NEAR 
; parameter 1: 4 + esp
.B4.1:                          ; Preds .B4.0
        mov       eax, DWORD PTR [4+esp]                        ;157.12
        mov       edx, DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY+32] ;158.1
        neg       edx                                           ;158.1
        add       edx, DWORD PTR [eax]                          ;158.1
        mov       ecx, DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY] ;158.1
        lea       eax, DWORD PTR [edx+edx*4]                    ;158.1
        dec       WORD PTR [38+ecx+eax*8]                       ;158.55
        ret                                                     ;159.1
        ALIGN     16
                                ; LOE
; mark_end;
_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_AVSIZEDECRE ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_TRANSLINK_MODULE_mp_TRANLINK_AVSIZEDECRE
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_TRANSLINK_MODULE_mp_TRANLINK_SCAN
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_TRANSLINK_MODULE_mp_TRANLINK_SCAN
_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_SCAN	PROC NEAR 
; parameter 1: 40 + esp
; parameter 2: 44 + esp
; parameter 3: 48 + esp
; parameter 4: 52 + esp
; parameter 5: 56 + esp
.B5.1:                          ; Preds .B5.0
        push      esi                                           ;162.12
        push      edi                                           ;162.12
        push      ebx                                           ;162.12
        push      ebp                                           ;162.12
        sub       esp, 20                                       ;162.12
        mov       ebp, DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY+32] ;177.3
        mov       eax, DWORD PTR [44+esp]                       ;162.12
        mov       ecx, DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY] ;177.3
        mov       DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_SCAN$IFOUND.0.6], 0 ;169.3
        lea       ebp, DWORD PTR [ebp+ebp*4]                    ;177.32
        shl       ebp, 3                                        ;177.32
        mov       edx, DWORD PTR [eax]                          ;177.6
        mov       eax, ebp                                      ;177.32
        neg       eax                                           ;177.32
        lea       esi, DWORD PTR [edx+edx*4]                    ;177.32
        lea       esi, DWORD PTR [ecx+esi*8]                    ;177.32
        movsx     ebx, WORD PTR [38+eax+esi]                    ;177.6
        test      ebx, ebx                                      ;177.32
        jle       .B5.18        ; Prob 16%                      ;177.32
                                ; LOE ebx ebp esi
.B5.2:                          ; Preds .B5.1
        mov       edx, DWORD PTR [48+esp]                       ;162.12
        mov       eax, ebx                                      ;181.5
        mov       edi, DWORD PTR [56+esp]                       ;190.7
        movss     xmm0, DWORD PTR [edx]                         ;183.7
        mov       edx, ebp                                      ;182.13
        neg       edx                                           ;182.13
        mov       DWORD PTR [edi], 0                            ;190.7
        mov       edi, ebx                                      ;
        mov       ecx, DWORD PTR [28+edx+esi]                   ;182.13
        imul      edi, ecx                                      ;
        mov       DWORD PTR [8+esp], edi                        ;
        mov       edi, ecx                                      ;
        neg       edi                                           ;
        mov       DWORD PTR [12+esp], edi                       ;
        mov       edi, esi                                      ;
        imul      ecx, DWORD PTR [32+edx+esi]                   ;
        sub       edi, ebp                                      ;
        mov       edx, DWORD PTR [edi]                          ;182.13
        mov       edi, DWORD PTR [8+esp]                        ;
        sub       edx, ecx                                      ;
        mov       ecx, DWORD PTR [12+esp]                       ;
                                ; LOE eax edx ecx ebx ebp esi edi xmm0
.B5.3:                          ; Preds .B5.4 .B5.2
        comiss    xmm0, DWORD PTR [8+edi+edx]                   ;183.17
        ja        .B5.16        ; Prob 20%                      ;183.17
                                ; LOE eax edx ecx ebx ebp esi edi xmm0
.B5.4:                          ; Preds .B5.3
        dec       eax                                           ;188.5
        add       edi, ecx                                      ;188.5
        test      eax, eax                                      ;188.5
        jg        .B5.3         ; Prob 82%                      ;188.5
                                ; LOE eax edx ecx ebx ebp esi edi xmm0
.B5.6:                          ; Preds .B5.4 .B5.16
        mov       eax, DWORD PTR [56+esp]                       ;190.7
        mov       DWORD PTR [eax], 1                            ;190.7
        mov       eax, 1                                        ;
                                ; LOE eax ebx ebp esi
.B5.7:                          ; Preds .B5.6 .B5.17
        neg       ebp                                           ;192.16
        movsx     edx, WORD PTR [36+ebp+esi]                    ;192.18
        cmp       ebx, edx                                      ;192.16
        jl        .B5.10        ; Prob 50%                      ;192.16
                                ; LOE eax edx ebx
.B5.8:                          ; Preds .B5.7
        add       edx, 50                                       ;193.9
        mov       DWORD PTR [8+esp], edx                        ;193.9
        lea       edx, DWORD PTR [8+esp]                        ;194.14
        mov       eax, DWORD PTR [44+esp]                       ;194.14
        call      _DYNUST_TRANSLINK_MODULE_mp_TRANLINK_SETUP.   ;194.14
                                ; LOE ebx
.B5.9:                          ; Preds .B5.8
        mov       eax, DWORD PTR [56+esp]                       ;197.5
        mov       eax, DWORD PTR [eax]                          ;197.5
                                ; LOE eax ebx
.B5.10:                         ; Preds .B5.9 .B5.7
        cmp       eax, ebx                                      ;197.17
        jg        .B5.15        ; Prob 50%                      ;197.17
                                ; LOE eax ebx
.B5.11:                         ; Preds .B5.10
        cmp       ebx, eax                                      ;198.8
        jl        .B5.15        ; Prob 10%                      ;198.8
                                ; LOE eax ebx
.B5.12:                         ; Preds .B5.11
        imul      ecx, DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY+32], -40 ;
        add       ecx, DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY] ;
        mov       DWORD PTR [12+esp], eax                       ;
                                ; LOE ecx ebx
.B5.13:                         ; Preds .B5.13 .B5.12
        mov       edx, DWORD PTR [44+esp]                       ;199.43
        mov       DWORD PTR [16+esp], ebx                       ;
        mov       eax, DWORD PTR [edx]                          ;199.43
        lea       esi, DWORD PTR [eax+eax*4]                    ;199.11
        mov       edi, DWORD PTR [28+ecx+esi*8]                 ;199.43
        lea       eax, DWORD PTR [1+ebx]                        ;199.11
        imul      ebx, edi                                      ;199.11
        mov       ebp, DWORD PTR [32+ecx+esi*8]                 ;199.43
        imul      ebp, edi                                      ;199.11
        imul      edi, eax                                      ;199.11
        add       ebx, DWORD PTR [ecx+esi*8]                    ;199.11
        add       edi, DWORD PTR [ecx+esi*8]                    ;199.11
        sub       ebx, ebp                                      ;199.11
        sub       edi, ebp                                      ;199.11
        mov       ebp, DWORD PTR [ebx]                          ;199.11
        mov       DWORD PTR [edi], ebp                          ;199.11
        mov       esi, DWORD PTR [edx]                          ;200.44
        lea       edi, DWORD PTR [esi+esi*4]                    ;200.11
        mov       esi, DWORD PTR [16+esp]                       ;200.11
        mov       ebx, DWORD PTR [28+ecx+edi*8]                 ;200.44
        imul      esi, ebx                                      ;200.11
        mov       ebp, DWORD PTR [32+ecx+edi*8]                 ;200.44
        imul      ebp, ebx                                      ;200.11
        imul      ebx, eax                                      ;200.11
        add       esi, DWORD PTR [ecx+edi*8]                    ;200.11
        add       ebx, DWORD PTR [ecx+edi*8]                    ;200.11
        sub       esi, ebp                                      ;200.11
        sub       ebx, ebp                                      ;200.11
        mov       edi, DWORD PTR [4+esi]                        ;200.11
        mov       DWORD PTR [4+ebx], edi                        ;200.11
        mov       edx, DWORD PTR [edx]                          ;201.44
        mov       ebx, DWORD PTR [16+esp]                       ;201.11
        mov       edi, ebx                                      ;201.11
        dec       ebx                                           ;202.8
        lea       ebp, DWORD PTR [edx+edx*4]                    ;201.11
        mov       edx, DWORD PTR [28+ecx+ebp*8]                 ;201.44
        imul      edi, edx                                      ;201.11
        imul      eax, edx                                      ;201.11
        mov       esi, DWORD PTR [32+ecx+ebp*8]                 ;201.44
        imul      esi, edx                                      ;201.11
        add       edi, DWORD PTR [ecx+ebp*8]                    ;201.11
        add       eax, DWORD PTR [ecx+ebp*8]                    ;201.11
        sub       edi, esi                                      ;201.11
        sub       eax, esi                                      ;201.11
        cmp       ebx, DWORD PTR [12+esp]                       ;202.8
        mov       edx, DWORD PTR [8+edi]                        ;201.11
        mov       DWORD PTR [8+eax], edx                        ;201.11
        jge       .B5.13        ; Prob 82%                      ;202.8
                                ; LOE ecx ebx
.B5.15:                         ; Preds .B5.13 .B5.11 .B5.10
        add       esp, 20                                       ;212.1
        pop       ebp                                           ;212.1
        pop       ebx                                           ;212.1
        pop       edi                                           ;212.1
        pop       esi                                           ;212.1
        ret                                                     ;212.1
                                ; LOE
.B5.16:                         ; Preds .B5.3                   ; Infreq
        inc       eax                                           ;184.6
        je        .B5.6         ; Prob 50%                      ;189.17
                                ; LOE eax ebx ebp esi
.B5.17:                         ; Preds .B5.16                  ; Infreq
        mov       edx, DWORD PTR [56+esp]                       ;190.7
        mov       DWORD PTR [edx], eax                          ;190.7
        jmp       .B5.7         ; Prob 100%                     ;190.7
                                ; LOE eax ebx ebp esi
.B5.18:                         ; Preds .B5.1                   ; Infreq
        mov       eax, DWORD PTR [56+esp]                       ;205.4
        mov       DWORD PTR [eax], 1                            ;205.4
        add       esp, 20                                       ;205.4
        pop       ebp                                           ;205.4
        pop       ebx                                           ;205.4
        pop       edi                                           ;205.4
        pop       esi                                           ;205.4
        ret                                                     ;205.4
        ALIGN     16
                                ; LOE
; mark_end;
_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_SCAN ENDP
_TEXT	ENDS
_BSS	SEGMENT  DWORD PUBLIC FLAT  'BSS'
	ALIGN 004H
_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_SCAN$IFOUND.0.6	DD 1 DUP (0H)	; pad
_BSS	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_TRANSLINK_MODULE_mp_TRANLINK_SCAN
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_TRANSLINK_MODULE_mp_TRANLINK_SETUP
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_TRANSLINK_MODULE_mp_TRANLINK_SETUP
_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_SETUP	PROC NEAR 
; parameter 1: eax
; parameter 2: edx
.B6.1:                          ; Preds .B6.0
        mov       eax, DWORD PTR [4+esp]                        ;79.12
        mov       edx, DWORD PTR [8+esp]                        ;79.12
	PUBLIC _DYNUST_TRANSLINK_MODULE_mp_TRANLINK_SETUP.
_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_SETUP.::
        push      ebp                                           ;79.12
        mov       ebp, esp                                      ;79.12
        and       esp, -16                                      ;79.12
        push      esi                                           ;79.12
        push      edi                                           ;79.12
        push      ebx                                           ;79.12
        sub       esp, 180                                      ;79.12
        mov       ecx, DWORD PTR [eax]                          ;88.8
        sub       ecx, DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY+32] ;88.33
        mov       DWORD PTR [144+esp], eax                      ;79.12
        mov       eax, DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY] ;88.4
        mov       DWORD PTR [128+esp], edx                      ;79.12
        lea       edx, DWORD PTR [ecx+ecx*4]                    ;88.33
        movsx     ecx, WORD PTR [36+eax+edx*8]                  ;88.8
        test      ecx, ecx                                      ;88.33
        mov       DWORD PTR [132+esp], ecx                      ;88.8
        jle       .B6.49        ; Prob 16%                      ;88.33
                                ; LOE
.B6.2:                          ; Preds .B6.1
        mov       eax, 0                                        ;90.6
        lea       ecx, DWORD PTR [112+esp]                      ;90.6
        mov       edx, DWORD PTR [132+esp]                      ;90.6
        push      12                                            ;90.6
        cmovl     edx, eax                                      ;90.6
        push      edx                                           ;90.6
        push      2                                             ;90.6
        push      ecx                                           ;90.6
        call      _for_check_mult_overflow                      ;90.6
                                ; LOE eax
.B6.3:                          ; Preds .B6.2
        and       eax, 1                                        ;90.6
        lea       edx, DWORD PTR [24+esp]                       ;90.6
        shl       eax, 4                                        ;90.6
        or        eax, 262145                                   ;90.6
        push      eax                                           ;90.6
        push      edx                                           ;90.6
        push      DWORD PTR [136+esp]                           ;90.6
        call      _for_allocate                                 ;90.6
                                ; LOE eax
.B6.60:                         ; Preds .B6.3
        add       esp, 28                                       ;90.6
        mov       ebx, eax                                      ;90.6
                                ; LOE ebx
.B6.4:                          ; Preds .B6.60
        test      ebx, ebx                                      ;90.6
        je        .B6.7         ; Prob 50%                      ;90.6
                                ; LOE ebx
.B6.5:                          ; Preds .B6.4
        mov       DWORD PTR [48+esp], 0                         ;92.5
        lea       edx, DWORD PTR [48+esp]                       ;92.5
        mov       DWORD PTR [88+esp], 54                        ;92.5
        lea       eax, DWORD PTR [88+esp]                       ;92.5
        mov       DWORD PTR [92+esp], OFFSET FLAT: __STRLITPACK_24 ;92.5
        push      32                                            ;92.5
        push      eax                                           ;92.5
        push      OFFSET FLAT: __STRLITPACK_30.0.3              ;92.5
        push      -2088435968                                   ;92.5
        push      911                                           ;92.5
        push      edx                                           ;92.5
        mov       DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_VECTORERROR], ebx ;90.6
        call      _for_write_seq_lis                            ;92.5
                                ; LOE
.B6.6:                          ; Preds .B6.5
        push      32                                            ;93.5
        xor       eax, eax                                      ;93.5
        push      eax                                           ;93.5
        push      eax                                           ;93.5
        push      -2088435968                                   ;93.5
        push      eax                                           ;93.5
        push      OFFSET FLAT: __STRLITPACK_31                  ;93.5
        call      _for_stop_core                                ;93.5
                                ; LOE
.B6.61:                         ; Preds .B6.6
        add       esp, 48                                       ;93.5
        jmp       .B6.9         ; Prob 100%                     ;93.5
                                ; LOE
.B6.7:                          ; Preds .B6.4
        mov       esi, DWORD PTR [144+esp]                      ;90.6
        mov       ecx, 1                                        ;90.6
        mov       edi, DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY+32] ;90.6
        xor       eax, eax                                      ;90.6
        neg       edi                                           ;90.6
        mov       edx, 12                                       ;90.6
        add       edi, DWORD PTR [esi]                          ;90.6
        mov       DWORD PTR [24+esp], ecx                       ;90.6
        mov       DWORD PTR [40+esp], ecx                       ;90.6
        mov       ecx, DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY] ;90.6
        lea       esi, DWORD PTR [edi+edi*4]                    ;90.6
        mov       DWORD PTR [20+esp], 5                         ;90.6
        mov       DWORD PTR [12+esp], edx                       ;90.6
        movsx     ecx, WORD PTR [36+ecx+esi*8]                  ;90.6
        test      ecx, ecx                                      ;90.6
        mov       DWORD PTR [16+esp], eax                       ;90.6
        cmovl     ecx, eax                                      ;90.6
        lea       eax, DWORD PTR [44+esp]                       ;90.6
        mov       DWORD PTR [32+esp], ecx                       ;90.6
        mov       DWORD PTR [36+esp], edx                       ;90.6
        push      edx                                           ;90.6
        push      ecx                                           ;90.6
        push      2                                             ;90.6
        push      eax                                           ;90.6
        call      _for_check_mult_overflow                      ;90.6
                                ; LOE ebx
.B6.62:                         ; Preds .B6.7
        add       esp, 16                                       ;90.6
                                ; LOE ebx
.B6.8:                          ; Preds .B6.62
        mov       DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_VECTORERROR], ebx ;90.6
                                ; LOE
.B6.9:                          ; Preds .B6.61 .B6.8
        mov       ebx, DWORD PTR [144+esp]                      ;97.17
        mov       esi, DWORD PTR [ebx]                          ;97.17
        sub       esi, DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY+32] ;97.6
        mov       ebx, DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY] ;97.6
        lea       esi, DWORD PTR [esi+esi*4]                    ;97.6
        movsx     eax, WORD PTR [36+ebx+esi*8]                  ;97.17
        test      eax, eax                                      ;97.6
        mov       DWORD PTR [108+esp], eax                      ;97.17
        jle       .B6.16        ; Prob 50%                      ;97.6
                                ; LOE ebx esi
.B6.10:                         ; Preds .B6.9
        mov       eax, DWORD PTR [8+esp]                        ;98.23
        mov       edi, DWORD PTR [108+esp]                      ;97.6
        mov       DWORD PTR [104+esp], eax                      ;98.23
        mov       eax, edi                                      ;97.6
        shr       eax, 31                                       ;97.6
        add       eax, edi                                      ;97.6
        mov       edx, DWORD PTR [36+esp]                       ;98.5
        sar       eax, 1                                        ;97.6
        mov       DWORD PTR [116+esp], edx                      ;98.5
        test      eax, eax                                      ;97.6
        mov       ecx, DWORD PTR [40+esp]                       ;98.5
        mov       DWORD PTR [140+esp], eax                      ;97.6
        mov       edx, DWORD PTR [ebx+esi*8]                    ;98.23
        jbe       .B6.44        ; Prob 3%                       ;97.6
                                ; LOE edx ecx ebx esi
.B6.11:                         ; Preds .B6.10
        mov       DWORD PTR [80+esp], ebx                       ;
        xor       edi, edi                                      ;
        mov       eax, DWORD PTR [28+ebx+esi*8]                 ;98.23
        mov       ebx, DWORD PTR [32+ebx+esi*8]                 ;98.23
        imul      ebx, eax                                      ;
        neg       ebx                                           ;
        add       ebx, edx                                      ;
        mov       DWORD PTR [96+esp], ebx                       ;
        mov       DWORD PTR [136+esp], eax                      ;98.23
        mov       eax, ecx                                      ;
        mov       ebx, DWORD PTR [116+esp]                      ;
        imul      eax, ebx                                      ;
        neg       eax                                           ;
        add       eax, DWORD PTR [104+esp]                      ;
        mov       DWORD PTR [esp], edx                          ;
        mov       DWORD PTR [100+esp], edi                      ;
        lea       edx, DWORD PTR [ebx+eax]                      ;
        mov       DWORD PTR [124+esp], edx                      ;
        lea       eax, DWORD PTR [eax+ebx*2]                    ;
        mov       DWORD PTR [120+esp], eax                      ;
        mov       DWORD PTR [4+esp], ecx                        ;
        mov       edx, edi                                      ;
        mov       ecx, DWORD PTR [96+esp]                       ;
        mov       eax, DWORD PTR [100+esp]                      ;
        mov       DWORD PTR [84+esp], esi                       ;
        ALIGN     16
                                ; LOE eax edx ecx
.B6.12:                         ; Preds .B6.12 .B6.11
        mov       edi, DWORD PTR [124+esp]                      ;98.5
        lea       ebx, DWORD PTR [1+eax+eax]                    ;98.5
        imul      ebx, DWORD PTR [136+esp]                      ;98.5
        mov       esi, DWORD PTR [ecx+ebx]                      ;98.5
        mov       DWORD PTR [edi+edx*2], esi                    ;98.5
        mov       esi, DWORD PTR [4+ecx+ebx]                    ;99.5
        mov       DWORD PTR [4+edi+edx*2], esi                  ;99.5
        lea       esi, DWORD PTR [2+eax+eax]                    ;98.5
        imul      esi, DWORD PTR [136+esp]                      ;98.5
        inc       eax                                           ;97.6
        mov       ebx, DWORD PTR [8+ecx+ebx]                    ;100.5
        mov       DWORD PTR [8+edi+edx*2], ebx                  ;100.5
        mov       ebx, DWORD PTR [120+esp]                      ;98.5
        mov       edi, DWORD PTR [ecx+esi]                      ;98.5
        mov       DWORD PTR [ebx+edx*2], edi                    ;98.5
        mov       edi, DWORD PTR [4+ecx+esi]                    ;99.5
        mov       esi, DWORD PTR [8+ecx+esi]                    ;100.5
        mov       DWORD PTR [4+ebx+edx*2], edi                  ;99.5
        mov       DWORD PTR [8+ebx+edx*2], esi                  ;100.5
        add       edx, DWORD PTR [116+esp]                      ;97.6
        cmp       eax, DWORD PTR [140+esp]                      ;97.6
        jb        .B6.12        ; Prob 64%                      ;97.6
                                ; LOE eax edx ecx
.B6.13:                         ; Preds .B6.12
        mov       DWORD PTR [100+esp], eax                      ;
        mov       edx, DWORD PTR [esp]                          ;
        mov       ecx, DWORD PTR [4+esp]                        ;
        mov       ebx, DWORD PTR [80+esp]                       ;
        lea       edi, DWORD PTR [1+eax+eax]                    ;97.6
        mov       esi, DWORD PTR [84+esp]                       ;
                                ; LOE edx ecx ebx esi edi
.B6.14:                         ; Preds .B6.13 .B6.44
        lea       eax, DWORD PTR [-1+edi]                       ;97.6
        cmp       eax, DWORD PTR [108+esp]                      ;97.6
        jae       .B6.17        ; Prob 3%                       ;97.6
                                ; LOE edx ecx ebx esi edi
.B6.15:                         ; Preds .B6.14
        mov       eax, DWORD PTR [32+ebx+esi*8]                 ;98.23
        neg       ecx                                           ;98.5
        neg       eax                                           ;98.5
        add       ecx, edi                                      ;98.5
        add       eax, edi                                      ;98.5
        imul      eax, DWORD PTR [28+ebx+esi*8]                 ;98.5
        imul      ecx, DWORD PTR [116+esp]                      ;98.5
        mov       DWORD PTR [80+esp], ebx                       ;
        mov       ebx, DWORD PTR [104+esp]                      ;98.5
        mov       edi, DWORD PTR [edx+eax]                      ;98.5
        mov       DWORD PTR [ebx+ecx], edi                      ;98.5
        mov       edi, DWORD PTR [4+edx+eax]                    ;99.5
        mov       eax, DWORD PTR [8+edx+eax]                    ;100.5
        mov       DWORD PTR [4+ebx+ecx], edi                    ;99.5
        mov       DWORD PTR [8+ebx+ecx], eax                    ;100.5
        mov       ebx, DWORD PTR [80+esp]                       ;100.5
        jmp       .B6.17        ; Prob 100%                     ;100.5
                                ; LOE edx ebx esi
.B6.16:                         ; Preds .B6.9
        mov       edx, DWORD PTR [ebx+esi*8]                    ;105.4
                                ; LOE edx ebx esi
.B6.17:                         ; Preds .B6.15 .B6.14 .B6.16
        mov       eax, DWORD PTR [12+ebx+esi*8]                 ;105.15
        mov       ecx, eax                                      ;105.4
        shr       ecx, 1                                        ;105.4
        and       ecx, 1                                        ;105.4
        mov       DWORD PTR [esp], eax                          ;105.15
        and       eax, 1                                        ;105.4
        shl       ecx, 2                                        ;105.4
        add       eax, eax                                      ;105.4
        or        ecx, 1                                        ;105.4
        or        ecx, eax                                      ;105.4
        or        ecx, 262144                                   ;105.4
        push      ecx                                           ;105.4
        push      edx                                           ;105.4
        call      _for_dealloc_allocatable                      ;105.4
                                ; LOE eax ebx esi
.B6.63:                         ; Preds .B6.17
        add       esp, 8                                        ;105.4
                                ; LOE eax ebx esi
.B6.18:                         ; Preds .B6.63
        mov       edx, DWORD PTR [esp]                          ;105.4
        and       edx, -2                                       ;105.4
        mov       DWORD PTR [ebx+esi*8], 0                      ;105.4
        test      eax, eax                                      ;106.19
        mov       DWORD PTR [12+ebx+esi*8], edx                 ;105.4
        mov       DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_VECTORERROR], eax ;105.4
        jne       .B6.47        ; Prob 5%                       ;106.19
                                ; LOE
.B6.19:                         ; Preds .B6.69 .B6.18
        mov       eax, DWORD PTR [128+esp]                      ;113.4
        xor       edx, edx                                      ;113.4
        lea       ebx, DWORD PTR [116+esp]                      ;113.4
        push      12                                            ;113.4
        mov       ecx, DWORD PTR [eax]                          ;113.4
        test      ecx, ecx                                      ;113.4
        cmovl     ecx, edx                                      ;113.4
        push      ecx                                           ;113.4
        push      2                                             ;113.4
        push      ebx                                           ;113.4
        call      _for_check_mult_overflow                      ;113.4
                                ; LOE eax
.B6.20:                         ; Preds .B6.19
        mov       edx, DWORD PTR [160+esp]                      ;113.4
        and       eax, 1                                        ;113.4
        mov       ecx, DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY+32] ;113.4
        neg       ecx                                           ;113.4
        add       ecx, DWORD PTR [edx]                          ;113.4
        mov       ebx, DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY] ;113.4
        shl       eax, 4                                        ;113.4
        or        eax, 262145                                   ;113.4
        lea       esi, DWORD PTR [ecx+ecx*4]                    ;113.4
        push      eax                                           ;113.4
        lea       edi, DWORD PTR [ebx+esi*8]                    ;113.4
        push      edi                                           ;113.4
        push      DWORD PTR [140+esp]                           ;113.4
        call      _for_allocate                                 ;113.4
                                ; LOE eax
.B6.65:                         ; Preds .B6.20
        add       esp, 28                                       ;113.4
        mov       ebx, eax                                      ;113.4
                                ; LOE ebx
.B6.21:                         ; Preds .B6.65
        test      ebx, ebx                                      ;113.4
        je        .B6.25        ; Prob 50%                      ;113.4
                                ; LOE ebx
.B6.22:                         ; Preds .B6.21
        mov       DWORD PTR [48+esp], 0                         ;115.7
        lea       edx, DWORD PTR [48+esp]                       ;115.7
        mov       DWORD PTR [96+esp], 47                        ;115.7
        lea       eax, DWORD PTR [96+esp]                       ;115.7
        mov       DWORD PTR [100+esp], OFFSET FLAT: __STRLITPACK_20 ;115.7
        push      32                                            ;115.7
        push      eax                                           ;115.7
        push      OFFSET FLAT: __STRLITPACK_34.0.3              ;115.7
        push      -2088435968                                   ;115.7
        push      911                                           ;115.7
        push      edx                                           ;115.7
        mov       DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_VECTORERROR], ebx ;113.4
        call      _for_write_seq_lis                            ;115.7
                                ; LOE
.B6.23:                         ; Preds .B6.22
        push      32                                            ;116.4
        xor       eax, eax                                      ;116.4
        push      eax                                           ;116.4
        push      eax                                           ;116.4
        push      -2088435968                                   ;116.4
        push      eax                                           ;116.4
        push      OFFSET FLAT: __STRLITPACK_35                  ;116.4
        call      _for_stop_core                                ;116.4
                                ; LOE
.B6.66:                         ; Preds .B6.23
        add       esp, 48                                       ;116.4
                                ; LOE
.B6.24:                         ; Preds .B6.66
        mov       eax, DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY] ;133.31
        mov       DWORD PTR [124+esp], eax                      ;133.31
        mov       esi, DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY+32] ;133.31
        mov       eax, DWORD PTR [8+esp]                        ;135.4
        jmp       .B6.27        ; Prob 100%                     ;135.4
                                ; LOE eax esi
.B6.25:                         ; Preds .B6.21
        mov       esi, DWORD PTR [144+esp]                      ;113.13
        xor       edi, edi                                      ;113.4
        mov       ecx, DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY] ;113.4
        mov       eax, DWORD PTR [esi]                          ;113.13
        mov       esi, 12                                       ;113.4
        sub       eax, DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY+32] ;113.4
        lea       edx, DWORD PTR [eax+eax*4]                    ;113.4
        mov       eax, 1                                        ;113.4
        mov       DWORD PTR [16+ecx+edx*8], eax                 ;113.4
        mov       DWORD PTR [32+ecx+edx*8], eax                 ;113.4
        mov       eax, DWORD PTR [128+esp]                      ;113.4
        mov       DWORD PTR [12+ecx+edx*8], 5                   ;113.4
        mov       DWORD PTR [4+ecx+edx*8], esi                  ;113.4
        mov       eax, DWORD PTR [eax]                          ;113.4
        test      eax, eax                                      ;113.4
        mov       DWORD PTR [8+ecx+edx*8], edi                  ;113.4
        cmovl     eax, edi                                      ;113.4
        mov       DWORD PTR [24+ecx+edx*8], eax                 ;113.4
        mov       DWORD PTR [28+ecx+edx*8], esi                 ;113.4
        lea       edx, DWORD PTR [80+esp]                       ;113.4
        push      esi                                           ;113.4
        push      eax                                           ;113.4
        push      2                                             ;113.4
        push      edx                                           ;113.4
        call      _for_check_mult_overflow                      ;113.4
                                ; LOE ebx
.B6.67:                         ; Preds .B6.25
        add       esp, 16                                       ;113.4
                                ; LOE ebx
.B6.26:                         ; Preds .B6.67
        mov       eax, DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY] ;133.31
        mov       DWORD PTR [124+esp], eax                      ;133.31
        mov       DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_VECTORERROR], ebx ;113.4
        mov       esi, DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY+32] ;133.31
        mov       eax, DWORD PTR [8+esp]                        ;135.4
                                ; LOE eax esi
.B6.27:                         ; Preds .B6.24 .B6.26
        mov       ebx, DWORD PTR [36+esp]                       ;121.40
        lea       esi, DWORD PTR [esi+esi*4]                    ;
        mov       ecx, DWORD PTR [132+esp]                      ;121.6
        mov       DWORD PTR [136+esp], ebx                      ;121.40
        mov       ebx, ecx                                      ;121.6
        shr       ebx, 31                                       ;121.6
        add       ebx, ecx                                      ;121.6
        sar       ebx, 1                                        ;121.6
        shl       esi, 3                                        ;
        mov       edx, DWORD PTR [40+esp]                       ;121.40
        test      ebx, ebx                                      ;121.6
        mov       DWORD PTR [120+esp], esi                      ;
        jbe       .B6.46        ; Prob 0%                       ;121.6
                                ; LOE eax edx ebx
.B6.28:                         ; Preds .B6.27
        mov       DWORD PTR [108+esp], ebx                      ;
        mov       edi, edx                                      ;
        mov       ebx, DWORD PTR [124+esp]                      ;
        xor       ecx, ecx                                      ;
        sub       ebx, DWORD PTR [120+esp]                      ;
        mov       DWORD PTR [156+esp], ebx                      ;
        mov       ebx, DWORD PTR [136+esp]                      ;
        imul      edi, ebx                                      ;
        neg       edi                                           ;
        add       edi, eax                                      ;
        mov       DWORD PTR [148+esp], ecx                      ;
        mov       DWORD PTR [84+esp], edx                       ;
        mov       DWORD PTR [104+esp], eax                      ;
        lea       esi, DWORD PTR [ebx+edi]                      ;
        mov       DWORD PTR [152+esp], esi                      ;
        lea       esi, DWORD PTR [edi+ebx*2]                    ;
        mov       DWORD PTR [140+esp], esi                      ;
                                ; LOE ecx
.B6.29:                         ; Preds .B6.29 .B6.28
        mov       eax, DWORD PTR [144+esp]                      ;121.6
        mov       ebx, DWORD PTR [148+esp]                      ;121.6
        mov       edx, DWORD PTR [eax]                          ;121.6
        lea       edi, DWORD PTR [1+ebx+ebx]                    ;121.6
        mov       DWORD PTR [160+esp], edi                      ;121.6
        lea       esi, DWORD PTR [edx+edx*4]                    ;121.6
        mov       edx, DWORD PTR [156+esp]                      ;121.6
        mov       ebx, DWORD PTR [32+edx+esi*8]                 ;121.6
        neg       ebx                                           ;121.6
        add       ebx, edi                                      ;121.6
        imul      ebx, DWORD PTR [28+edx+esi*8]                 ;121.6
        mov       edi, DWORD PTR [edx+esi*8]                    ;121.6
        mov       esi, DWORD PTR [152+esp]                      ;121.6
        mov       esi, DWORD PTR [esi+ecx*2]                    ;121.6
        mov       DWORD PTR [edi+ebx], esi                      ;121.6
        mov       ebx, DWORD PTR [eax]                          ;122.6
        lea       edi, DWORD PTR [ebx+ebx*4]                    ;122.6
        mov       ebx, DWORD PTR [32+edx+edi*8]                 ;122.6
        neg       ebx                                           ;122.6
        add       ebx, DWORD PTR [160+esp]                      ;122.6
        imul      ebx, DWORD PTR [28+edx+edi*8]                 ;122.6
        mov       esi, DWORD PTR [edx+edi*8]                    ;122.6
        mov       edi, DWORD PTR [152+esp]                      ;122.6
        mov       edi, DWORD PTR [4+edi+ecx*2]                  ;122.6
        mov       DWORD PTR [4+esi+ebx], edi                    ;122.6
        mov       ebx, DWORD PTR [eax]                          ;123.6
        mov       edi, DWORD PTR [160+esp]                      ;123.6
        lea       esi, DWORD PTR [ebx+ebx*4]                    ;123.6
        sub       edi, DWORD PTR [32+edx+esi*8]                 ;123.6
        imul      edi, DWORD PTR [28+edx+esi*8]                 ;123.6
        mov       ebx, DWORD PTR [edx+esi*8]                    ;123.6
        mov       esi, DWORD PTR [152+esp]                      ;123.6
        mov       esi, DWORD PTR [8+esi+ecx*2]                  ;123.6
        mov       DWORD PTR [8+ebx+edi], esi                    ;123.6
        mov       ebx, DWORD PTR [eax]                          ;121.6
        lea       edi, DWORD PTR [ebx+ebx*4]                    ;121.6
        mov       ebx, DWORD PTR [148+esp]                      ;121.6
        mov       esi, DWORD PTR [32+edx+edi*8]                 ;121.6
        neg       esi                                           ;121.6
        lea       ebx, DWORD PTR [2+ebx+ebx]                    ;121.6
        mov       DWORD PTR [164+esp], ebx                      ;121.6
        add       esi, ebx                                      ;121.6
        imul      esi, DWORD PTR [28+edx+edi*8]                 ;121.6
        mov       ebx, DWORD PTR [edx+edi*8]                    ;121.6
        mov       edi, DWORD PTR [140+esp]                      ;121.6
        mov       edi, DWORD PTR [edi+ecx*2]                    ;121.6
        mov       DWORD PTR [ebx+esi], edi                      ;121.6
        mov       ebx, DWORD PTR [eax]                          ;122.6
        lea       esi, DWORD PTR [ebx+ebx*4]                    ;122.6
        mov       ebx, DWORD PTR [140+esp]                      ;122.6
        mov       edi, DWORD PTR [32+edx+esi*8]                 ;122.6
        neg       edi                                           ;122.6
        add       edi, DWORD PTR [164+esp]                      ;122.6
        imul      edi, DWORD PTR [28+edx+esi*8]                 ;122.6
        mov       esi, DWORD PTR [edx+esi*8]                    ;122.6
        mov       ebx, DWORD PTR [4+ebx+ecx*2]                  ;122.6
        mov       DWORD PTR [4+esi+edi], ebx                    ;122.6
        mov       eax, DWORD PTR [eax]                          ;123.6
        mov       ebx, DWORD PTR [164+esp]                      ;123.6
        lea       eax, DWORD PTR [eax+eax*4]                    ;123.6
        sub       ebx, DWORD PTR [32+edx+eax*8]                 ;123.6
        imul      ebx, DWORD PTR [28+edx+eax*8]                 ;123.6
        mov       edi, DWORD PTR [edx+eax*8]                    ;123.6
        mov       edx, DWORD PTR [140+esp]                      ;123.6
        DB        15                                            ;123.6
        DB        31                                            ;123.6
        DB        0                                             ;123.6
        mov       esi, DWORD PTR [8+edx+ecx*2]                  ;123.6
        mov       DWORD PTR [8+edi+ebx], esi                    ;123.6
        mov       ebx, DWORD PTR [148+esp]                      ;121.6
        inc       ebx                                           ;121.6
        add       ecx, DWORD PTR [136+esp]                      ;121.6
        mov       DWORD PTR [148+esp], ebx                      ;121.6
        cmp       ebx, DWORD PTR [108+esp]                      ;121.6
        jb        .B6.29        ; Prob 64%                      ;121.6
                                ; LOE ecx ebx bl bh
.B6.30:                         ; Preds .B6.29
        mov       ecx, ebx                                      ;121.6
        mov       edx, DWORD PTR [84+esp]                       ;
        mov       eax, DWORD PTR [104+esp]                      ;
        lea       ebx, DWORD PTR [1+ecx+ecx]                    ;121.6
                                ; LOE eax edx ebx
.B6.31:                         ; Preds .B6.30 .B6.46
        lea       ecx, DWORD PTR [-1+ebx]                       ;121.6
        cmp       ecx, DWORD PTR [132+esp]                      ;121.6
        jae       .B6.33        ; Prob 0%                       ;121.6
                                ; LOE eax edx ebx
.B6.32:                         ; Preds .B6.31
        mov       ecx, DWORD PTR [144+esp]                      ;121.6
        neg       edx                                           ;121.6
        add       edx, ebx                                      ;121.6
        imul      edx, DWORD PTR [136+esp]                      ;121.6
        mov       edi, DWORD PTR [ecx]                          ;121.6
        mov       ecx, DWORD PTR [124+esp]                      ;121.6
        sub       ecx, DWORD PTR [120+esp]                      ;121.6
        mov       DWORD PTR [84+esp], edx                       ;121.6
        lea       edi, DWORD PTR [edi+edi*4]                    ;121.6
        mov       edx, DWORD PTR [eax+edx]                      ;121.6
        mov       esi, DWORD PTR [32+ecx+edi*8]                 ;121.6
        neg       esi                                           ;121.6
        add       esi, ebx                                      ;121.6
        imul      esi, DWORD PTR [28+ecx+edi*8]                 ;121.6
        mov       edi, DWORD PTR [ecx+edi*8]                    ;121.6
        mov       DWORD PTR [108+esp], ecx                      ;121.6
        mov       DWORD PTR [edi+esi], edx                      ;121.6
        mov       edi, DWORD PTR [144+esp]                      ;122.6
        mov       esi, DWORD PTR [edi]                          ;122.6
        lea       edx, DWORD PTR [esi+esi*4]                    ;122.6
        mov       esi, DWORD PTR [32+ecx+edx*8]                 ;122.6
        neg       esi                                           ;122.6
        add       esi, ebx                                      ;122.6
        imul      esi, DWORD PTR [28+ecx+edx*8]                 ;122.6
        mov       edx, DWORD PTR [ecx+edx*8]                    ;122.6
        mov       ecx, DWORD PTR [84+esp]                       ;122.6
        mov       ecx, DWORD PTR [4+eax+ecx]                    ;122.6
        mov       DWORD PTR [4+edx+esi], ecx                    ;122.6
        mov       edi, DWORD PTR [edi]                          ;123.6
        mov       edx, DWORD PTR [108+esp]                      ;123.6
        mov       esi, DWORD PTR [84+esp]                       ;123.6
        lea       ecx, DWORD PTR [edi+edi*4]                    ;123.6
        sub       ebx, DWORD PTR [32+edx+ecx*8]                 ;123.6
        imul      ebx, DWORD PTR [28+edx+ecx*8]                 ;123.6
        mov       edx, DWORD PTR [edx+ecx*8]                    ;123.6
        mov       edi, DWORD PTR [8+eax+esi]                    ;123.6
        mov       DWORD PTR [8+edx+ebx], edi                    ;123.6
                                ; LOE eax
.B6.33:                         ; Preds .B6.32 .B6.31
        mov       edx, DWORD PTR [128+esp]                      ;127.4
        mov       ecx, DWORD PTR [edx]                          ;127.4
        cmp       ecx, DWORD PTR [132+esp]                      ;127.4
        jle       .B6.41        ; Prob 50%                      ;127.4
                                ; LOE eax ecx
.B6.34:                         ; Preds .B6.33
        mov       ebx, ecx                                      ;127.4
        sub       ebx, DWORD PTR [132+esp]                      ;127.4
        mov       edx, ebx                                      ;127.4
        shr       edx, 31                                       ;127.4
        add       edx, ebx                                      ;127.4
        sar       edx, 1                                        ;127.4
        test      edx, edx                                      ;127.4
        jbe       .B6.45        ; Prob 10%                      ;127.4
                                ; LOE eax edx ecx
.B6.35:                         ; Preds .B6.34
        mov       ebx, DWORD PTR [124+esp]                      ;
        xor       esi, esi                                      ;
        mov       DWORD PTR [84+esp], ecx                       ;
        sub       ebx, DWORD PTR [120+esp]                      ;
        mov       ecx, DWORD PTR [144+esp]                      ;
        mov       DWORD PTR [136+esp], esi                      ;
        mov       DWORD PTR [108+esp], edx                      ;
        mov       DWORD PTR [104+esp], eax                      ;
                                ; LOE ecx ebx
.B6.36:                         ; Preds .B6.36 .B6.35
        mov       edx, DWORD PTR [ecx]                          ;128.6
        mov       eax, DWORD PTR [132+esp]                      ;128.6
        mov       esi, DWORD PTR [136+esp]                      ;128.6
        lea       edx, DWORD PTR [edx+edx*4]                    ;128.6
        lea       edi, DWORD PTR [eax+esi*2]                    ;128.6
        mov       DWORD PTR [140+esp], edi                      ;128.6
        mov       esi, DWORD PTR [ebx+edx*8]                    ;128.6
        lea       eax, DWORD PTR [1+edi]                        ;128.6
        mov       edi, DWORD PTR [32+ebx+edx*8]                 ;128.6
        neg       edi                                           ;128.6
        add       edi, eax                                      ;128.6
        imul      edi, DWORD PTR [28+ebx+edx*8]                 ;128.6
        xor       edx, edx                                      ;128.6
        mov       DWORD PTR [esi+edi], edx                      ;128.6
        mov       edi, DWORD PTR [ecx]                          ;129.6
        lea       edi, DWORD PTR [edi+edi*4]                    ;129.6
        mov       esi, DWORD PTR [32+ebx+edi*8]                 ;129.6
        neg       esi                                           ;129.6
        add       esi, eax                                      ;129.6
        imul      esi, DWORD PTR [28+ebx+edi*8]                 ;129.6
        mov       edi, DWORD PTR [ebx+edi*8]                    ;129.6
        mov       DWORD PTR [4+edi+esi], edx                    ;129.6
        mov       esi, DWORD PTR [ecx]                          ;130.6
        lea       esi, DWORD PTR [esi+esi*4]                    ;130.6
        sub       eax, DWORD PTR [32+ebx+esi*8]                 ;130.6
        imul      eax, DWORD PTR [28+ebx+esi*8]                 ;130.6
        mov       edi, DWORD PTR [ebx+esi*8]                    ;130.6
        mov       DWORD PTR [8+edi+eax], edx                    ;130.6
        mov       esi, DWORD PTR [ecx]                          ;128.6
        mov       eax, DWORD PTR [140+esp]                      ;128.6
        add       eax, 2                                        ;128.6
        lea       esi, DWORD PTR [esi+esi*4]                    ;128.6
        mov       edi, DWORD PTR [32+ebx+esi*8]                 ;128.6
        neg       edi                                           ;128.6
        add       edi, eax                                      ;128.6
        imul      edi, DWORD PTR [28+ebx+esi*8]                 ;128.6
        mov       esi, DWORD PTR [ebx+esi*8]                    ;128.6
        mov       DWORD PTR [esi+edi], edx                      ;128.6
        mov       edi, DWORD PTR [ecx]                          ;129.6
        lea       edi, DWORD PTR [edi+edi*4]                    ;129.6
        mov       esi, DWORD PTR [32+ebx+edi*8]                 ;129.6
        neg       esi                                           ;129.6
        add       esi, eax                                      ;129.6
        imul      esi, DWORD PTR [28+ebx+edi*8]                 ;129.6
        mov       edi, DWORD PTR [ebx+edi*8]                    ;129.6
        mov       DWORD PTR [4+edi+esi], edx                    ;129.6
        mov       esi, DWORD PTR [ecx]                          ;130.6
        lea       esi, DWORD PTR [esi+esi*4]                    ;130.6
        sub       eax, DWORD PTR [32+ebx+esi*8]                 ;130.6
        imul      eax, DWORD PTR [28+ebx+esi*8]                 ;130.6
        mov       edi, DWORD PTR [ebx+esi*8]                    ;130.6
        mov       DWORD PTR [8+edi+eax], edx                    ;130.6
        mov       eax, DWORD PTR [136+esp]                      ;127.4
        inc       eax                                           ;127.4
        mov       DWORD PTR [136+esp], eax                      ;127.4
        cmp       eax, DWORD PTR [108+esp]                      ;127.4
        jb        .B6.36        ; Prob 63%                      ;127.4
                                ; LOE eax ecx ebx al ah
.B6.37:                         ; Preds .B6.36
        mov       esi, eax                                      ;
        lea       edx, DWORD PTR [1+esi+esi]                    ;127.4
        mov       ecx, DWORD PTR [84+esp]                       ;
        mov       eax, DWORD PTR [104+esp]                      ;
                                ; LOE eax edx ecx
.B6.38:                         ; Preds .B6.37 .B6.45
        sub       ecx, DWORD PTR [132+esp]                      ;127.4
        lea       ebx, DWORD PTR [-1+edx]                       ;127.4
        cmp       ecx, ebx                                      ;127.4
        jbe       .B6.40        ; Prob 10%                      ;127.4
                                ; LOE eax edx
.B6.39:                         ; Preds .B6.38
        mov       esi, DWORD PTR [144+esp]                      ;128.6
        mov       edi, DWORD PTR [124+esp]                      ;128.6
        sub       edi, DWORD PTR [120+esp]                      ;128.6
        mov       ebx, DWORD PTR [esi]                          ;128.6
        mov       DWORD PTR [104+esp], eax                      ;
        add       edx, DWORD PTR [132+esp]                      ;128.6
        lea       eax, DWORD PTR [ebx+ebx*4]                    ;128.6
        xor       ebx, ebx                                      ;128.6
        mov       ecx, DWORD PTR [32+edi+eax*8]                 ;128.6
        neg       ecx                                           ;128.6
        add       ecx, edx                                      ;128.6
        imul      ecx, DWORD PTR [28+edi+eax*8]                 ;128.6
        mov       eax, DWORD PTR [edi+eax*8]                    ;128.6
        mov       DWORD PTR [eax+ecx], ebx                      ;128.6
        mov       ecx, DWORD PTR [esi]                          ;129.6
        lea       ecx, DWORD PTR [ecx+ecx*4]                    ;129.6
        mov       eax, DWORD PTR [32+edi+ecx*8]                 ;129.6
        neg       eax                                           ;129.6
        add       eax, edx                                      ;129.6
        imul      eax, DWORD PTR [28+edi+ecx*8]                 ;129.6
        mov       ecx, DWORD PTR [edi+ecx*8]                    ;129.6
        mov       DWORD PTR [4+ecx+eax], ebx                    ;129.6
        mov       esi, DWORD PTR [esi]                          ;130.6
        lea       eax, DWORD PTR [esi+esi*4]                    ;130.6
        sub       edx, DWORD PTR [32+edi+eax*8]                 ;130.6
        imul      edx, DWORD PTR [28+edi+eax*8]                 ;130.6
        mov       edi, DWORD PTR [edi+eax*8]                    ;130.6
        mov       eax, DWORD PTR [104+esp]                      ;130.6
        mov       DWORD PTR [8+edi+edx], ebx                    ;130.6
                                ; LOE eax
.B6.40:                         ; Preds .B6.38 .B6.39
        mov       edx, DWORD PTR [128+esp]                      ;133.31
        mov       ecx, DWORD PTR [edx]                          ;133.31
                                ; LOE eax ecx
.B6.41:                         ; Preds .B6.40 .B6.33
        mov       ebx, DWORD PTR [144+esp]                      ;133.4
        mov       esi, DWORD PTR [124+esp]                      ;133.4
        mov       edx, DWORD PTR [ebx]                          ;133.4
        lea       edi, DWORD PTR [edx+edx*4]                    ;133.4
        lea       ebx, DWORD PTR [esi+edi*8]                    ;133.4
        sub       ebx, DWORD PTR [120+esp]                      ;133.4
        mov       WORD PTR [36+ebx], cx                         ;133.4
        mov       ebx, DWORD PTR [20+esp]                       ;135.4
        mov       edx, ebx                                      ;135.4
        shr       edx, 1                                        ;135.4
        mov       ecx, ebx                                      ;135.4
        and       edx, 1                                        ;135.4
        and       ecx, 1                                        ;135.4
        shl       edx, 2                                        ;135.4
        add       ecx, ecx                                      ;135.4
        or        edx, ecx                                      ;135.4
        or        edx, 262144                                   ;135.4
        push      edx                                           ;135.4
        push      eax                                           ;135.4
        call      _for_dealloc_allocatable                      ;135.4
                                ; LOE ebx
.B6.68:                         ; Preds .B6.41
        add       esp, 8                                        ;135.4
                                ; LOE ebx
.B6.42:                         ; Preds .B6.68
        and       ebx, -2                                       ;135.4
        mov       DWORD PTR [8+esp], 0                          ;135.4
        mov       DWORD PTR [20+esp], ebx                       ;135.4
                                ; LOE
.B6.43:                         ; Preds .B6.42
        add       esp, 180                                      ;148.1
        pop       ebx                                           ;148.1
        pop       edi                                           ;148.1
        pop       esi                                           ;148.1
        mov       esp, ebp                                      ;148.1
        pop       ebp                                           ;148.1
        ret                                                     ;148.1
                                ; LOE
.B6.44:                         ; Preds .B6.10                  ; Infreq
        mov       edi, 1                                        ;
        jmp       .B6.14        ; Prob 100%                     ;
                                ; LOE edx ecx ebx esi edi
.B6.45:                         ; Preds .B6.34                  ; Infreq
        mov       edx, 1                                        ;
        jmp       .B6.38        ; Prob 100%                     ;
                                ; LOE eax edx ecx
.B6.46:                         ; Preds .B6.27                  ; Infreq
        mov       ebx, 1                                        ;
        jmp       .B6.31        ; Prob 100%                     ;
                                ; LOE eax edx ebx
.B6.47:                         ; Preds .B6.18                  ; Infreq
        mov       DWORD PTR [48+esp], 0                         ;107.6
        lea       edx, DWORD PTR [48+esp]                       ;107.6
        mov       DWORD PTR [esp], 49                           ;107.6
        lea       eax, DWORD PTR [esp]                          ;107.6
        mov       DWORD PTR [4+esp], OFFSET FLAT: __STRLITPACK_22 ;107.6
        push      32                                            ;107.6
        push      eax                                           ;107.6
        push      OFFSET FLAT: __STRLITPACK_32.0.3              ;107.6
        push      -2088435968                                   ;107.6
        push      911                                           ;107.6
        push      edx                                           ;107.6
        call      _for_write_seq_lis                            ;107.6
                                ; LOE
.B6.48:                         ; Preds .B6.47                  ; Infreq
        push      32                                            ;108.6
        xor       eax, eax                                      ;108.6
        push      eax                                           ;108.6
        push      eax                                           ;108.6
        push      -2088435968                                   ;108.6
        push      eax                                           ;108.6
        push      OFFSET FLAT: __STRLITPACK_33                  ;108.6
        call      _for_stop_core                                ;108.6
                                ; LOE
.B6.69:                         ; Preds .B6.48                  ; Infreq
        add       esp, 48                                       ;108.6
        jmp       .B6.19        ; Prob 100%                     ;108.6
                                ; LOE
.B6.49:                         ; Preds .B6.1                   ; Infreq
        mov       eax, DWORD PTR [128+esp]                      ;139.4
        xor       edx, edx                                      ;139.4
        lea       ebx, DWORD PTR [4+esp]                        ;139.4
        push      12                                            ;139.4
        mov       ecx, DWORD PTR [eax]                          ;139.4
        test      ecx, ecx                                      ;139.4
        cmovl     ecx, edx                                      ;139.4
        push      ecx                                           ;139.4
        push      2                                             ;139.4
        push      ebx                                           ;139.4
        call      _for_check_mult_overflow                      ;139.4
                                ; LOE eax
.B6.50:                         ; Preds .B6.49                  ; Infreq
        mov       edx, DWORD PTR [160+esp]                      ;139.4
        and       eax, 1                                        ;139.4
        mov       ecx, DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY+32] ;139.4
        neg       ecx                                           ;139.4
        add       ecx, DWORD PTR [edx]                          ;139.4
        mov       ebx, DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY] ;139.4
        shl       eax, 4                                        ;139.4
        or        eax, 262145                                   ;139.4
        lea       esi, DWORD PTR [ecx+ecx*4]                    ;139.4
        push      eax                                           ;139.4
        lea       edi, DWORD PTR [ebx+esi*8]                    ;139.4
        push      edi                                           ;139.4
        push      DWORD PTR [28+esp]                            ;139.4
        call      _for_allocate                                 ;139.4
                                ; LOE eax
.B6.71:                         ; Preds .B6.50                  ; Infreq
        add       esp, 28                                       ;139.4
        mov       ebx, eax                                      ;139.4
                                ; LOE ebx
.B6.51:                         ; Preds .B6.71                  ; Infreq
        test      ebx, ebx                                      ;139.4
        je        .B6.54        ; Prob 50%                      ;139.4
                                ; LOE ebx
.B6.52:                         ; Preds .B6.51                  ; Infreq
        mov       DWORD PTR [48+esp], 0                         ;141.7
        lea       edx, DWORD PTR [48+esp]                       ;141.7
        mov       DWORD PTR [8+esp], 47                         ;141.7
        lea       eax, DWORD PTR [8+esp]                        ;141.7
        mov       DWORD PTR [12+esp], OFFSET FLAT: __STRLITPACK_18 ;141.7
        push      32                                            ;141.7
        push      eax                                           ;141.7
        push      OFFSET FLAT: __STRLITPACK_36.0.3              ;141.7
        push      -2088435968                                   ;141.7
        push      911                                           ;141.7
        push      edx                                           ;141.7
        mov       DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_VECTORERROR], ebx ;139.4
        call      _for_write_seq_lis                            ;141.7
                                ; LOE
.B6.53:                         ; Preds .B6.52                  ; Infreq
        push      32                                            ;142.4
        xor       eax, eax                                      ;142.4
        push      eax                                           ;142.4
        push      eax                                           ;142.4
        push      -2088435968                                   ;142.4
        push      eax                                           ;142.4
        push      OFFSET FLAT: __STRLITPACK_37                  ;142.4
        call      _for_stop_core                                ;142.4
                                ; LOE
.B6.72:                         ; Preds .B6.53                  ; Infreq
        add       esp, 48                                       ;142.4
        jmp       .B6.56        ; Prob 100%                     ;142.4
                                ; LOE
.B6.54:                         ; Preds .B6.51                  ; Infreq
        mov       ecx, DWORD PTR [144+esp]                      ;139.13
        mov       esi, 1                                        ;139.4
        mov       edx, DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY] ;139.4
        xor       edi, edi                                      ;139.4
        mov       eax, DWORD PTR [ecx]                          ;139.13
        mov       ecx, 12                                       ;139.4
        sub       eax, DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY+32] ;139.4
        lea       eax, DWORD PTR [eax+eax*4]                    ;139.4
        mov       DWORD PTR [16+edx+eax*8], esi                 ;139.4
        mov       DWORD PTR [32+edx+eax*8], esi                 ;139.4
        mov       esi, DWORD PTR [128+esp]                      ;139.4
        mov       DWORD PTR [8+edx+eax*8], edi                  ;139.4
        mov       DWORD PTR [12+edx+eax*8], 5                   ;139.4
        mov       esi, DWORD PTR [esi]                          ;139.4
        test      esi, esi                                      ;139.4
        mov       DWORD PTR [4+edx+eax*8], ecx                  ;139.4
        cmovge    edi, esi                                      ;139.4
        mov       DWORD PTR [24+edx+eax*8], edi                 ;139.4
        mov       DWORD PTR [28+edx+eax*8], ecx                 ;139.4
        lea       edx, DWORD PTR [esp]                          ;139.4
        push      ecx                                           ;139.4
        push      edi                                           ;139.4
        push      2                                             ;139.4
        push      edx                                           ;139.4
        call      _for_check_mult_overflow                      ;139.4
                                ; LOE ebx
.B6.73:                         ; Preds .B6.54                  ; Infreq
        add       esp, 16                                       ;139.4
                                ; LOE ebx
.B6.55:                         ; Preds .B6.73                  ; Infreq
        mov       DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_VECTORERROR], ebx ;139.4
                                ; LOE
.B6.56:                         ; Preds .B6.72 .B6.55           ; Infreq
        mov       eax, DWORD PTR [144+esp]                      ;144.4
        mov       edx, DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY+32] ;144.31
        neg       edx                                           ;144.4
        add       edx, DWORD PTR [eax]                          ;144.4
        mov       ecx, DWORD PTR [128+esp]                      ;144.31
        mov       esi, DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY] ;144.31
        lea       edi, DWORD PTR [edx+edx*4]                    ;144.4
        mov       ebx, DWORD PTR [ecx]                          ;144.31
        mov       WORD PTR [36+esi+edi*8], bx                   ;144.4
        add       esp, 180                                      ;144.4
        pop       ebx                                           ;144.4
        pop       edi                                           ;144.4
        pop       esi                                           ;144.4
        mov       esp, ebp                                      ;144.4
        pop       ebp                                           ;144.4
        ret                                                     ;144.4
        ALIGN     16
                                ; LOE
; mark_end;
_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_SETUP ENDP
_TEXT	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_36.0.3	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_30.0.3	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_32.0.3	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_34.0.3	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_TRANSLINK_MODULE_mp_TRANLINK_SETUP
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ERASE
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ERASE
_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ERASE	PROC NEAR 
; parameter 1: 8 + ebp
; parameter 2: 12 + ebp
.B7.1:                          ; Preds .B7.0
        push      ebp                                           ;215.12
        mov       ebp, esp                                      ;215.12
        and       esp, -16                                      ;215.12
        push      esi                                           ;215.12
        push      edi                                           ;215.12
        push      ebx                                           ;215.12
        sub       esp, 52                                       ;215.12
        mov       eax, DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY+32] ;217.3
        mov       ecx, DWORD PTR [8+ebp]                        ;215.12
        mov       edx, DWORD PTR [12+ebp]                       ;215.12
        lea       eax, DWORD PTR [eax+eax*4]                    ;217.9
        mov       ebx, DWORD PTR [ecx]                          ;217.12
        shl       eax, 3                                        ;217.9
        mov       esi, DWORD PTR [edx]                          ;217.3
        mov       edx, DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY] ;217.3
        lea       edi, DWORD PTR [ebx+ebx*4]                    ;217.9
        mov       DWORD PTR [12+esp], eax                       ;217.9
        neg       eax                                           ;217.9
        mov       DWORD PTR [esp], edi                          ;217.9
        lea       ecx, DWORD PTR [edi*8]                        ;217.9
        mov       DWORD PTR [4+esp], esi                        ;217.3
        lea       edi, DWORD PTR [edx+edi*8]                    ;217.9
        movsx     eax, WORD PTR [38+eax+edi]                    ;217.12
        cmp       esi, eax                                      ;217.9
        mov       DWORD PTR [8+esp], eax                        ;217.12
        mov       eax, DWORD PTR [12+esp]                       ;217.9
        jne       .B7.15        ; Prob 50%                      ;217.9
                                ; LOE eax edx ecx ebx esi edi al ah
.B7.2:                          ; Preds .B7.1
        mov       ebx, eax                                      ;218.5
        sub       edx, eax                                      ;
        neg       ebx                                           ;218.5
        mov       eax, DWORD PTR [esp]                          ;
        mov       esi, DWORD PTR [32+ebx+edi]                   ;218.5
        lea       eax, DWORD PTR [edx+eax*8]                    ;
        cmp       DWORD PTR [24+ebx+edi], 0                     ;218.5
        jle       .B7.44        ; Prob 10%                      ;218.5
                                ; LOE eax edx ecx esi
.B7.3:                          ; Preds .B7.2
        mov       edi, 1                                        ;
                                ; LOE eax edx esi edi
.B7.4:                          ; Preds .B7.4 .B7.3
        mov       ebx, DWORD PTR [32+eax]                       ;218.5
        inc       edi                                           ;218.5
        neg       ebx                                           ;218.5
        add       ebx, esi                                      ;218.5
        inc       esi                                           ;218.5
        imul      ebx, DWORD PTR [28+eax]                       ;218.5
        mov       ecx, DWORD PTR [eax]                          ;218.5
        mov       eax, DWORD PTR [8+ebp]                        ;218.5
        mov       DWORD PTR [ecx+ebx], 0                        ;218.5
        mov       ecx, DWORD PTR [eax]                          ;218.5
        lea       eax, DWORD PTR [ecx+ecx*4]                    ;218.5
        lea       ecx, DWORD PTR [eax*8]                        ;218.5
        lea       eax, DWORD PTR [edx+eax*8]                    ;218.5
        mov       ebx, DWORD PTR [24+eax]                       ;218.5
        cmp       edi, ebx                                      ;218.5
        jle       .B7.4         ; Prob 82%                      ;218.5
                                ; LOE eax edx ecx ebx esi edi
.B7.6:                          ; Preds .B7.4 .B7.44
        mov       edi, DWORD PTR [32+eax]                       ;219.5
        test      ebx, ebx                                      ;219.5
        jle       .B7.10        ; Prob 10%                      ;219.5
                                ; LOE eax edx ecx ebx edi
.B7.7:                          ; Preds .B7.6
        mov       esi, 1                                        ;
                                ; LOE eax edx ecx esi edi
.B7.8:                          ; Preds .B7.8 .B7.7
        mov       ebx, DWORD PTR [32+eax]                       ;219.5
        inc       esi                                           ;219.5
        neg       ebx                                           ;219.5
        add       ebx, edi                                      ;219.5
        inc       edi                                           ;219.5
        imul      ebx, DWORD PTR [28+eax]                       ;219.5
        mov       ecx, DWORD PTR [edx+ecx]                      ;219.5
        mov       eax, DWORD PTR [8+ebp]                        ;219.5
        mov       DWORD PTR [4+ecx+ebx], 0                      ;219.5
        mov       ecx, DWORD PTR [eax]                          ;219.5
        lea       eax, DWORD PTR [ecx+ecx*4]                    ;219.5
        lea       ecx, DWORD PTR [eax*8]                        ;219.5
        lea       eax, DWORD PTR [edx+eax*8]                    ;219.5
        mov       ebx, DWORD PTR [24+eax]                       ;219.5
        cmp       esi, ebx                                      ;219.5
        jle       .B7.8         ; Prob 82%                      ;219.5
                                ; LOE eax edx ecx ebx esi edi
.B7.9:                          ; Preds .B7.8
        mov       edi, DWORD PTR [32+eax]                       ;220.5
                                ; LOE eax edx ecx ebx edi
.B7.10:                         ; Preds .B7.9 .B7.6
        test      ebx, ebx                                      ;220.5
        jle       .B7.14        ; Prob 10%                      ;220.5
                                ; LOE eax edx ecx edi
.B7.11:                         ; Preds .B7.10
        mov       ebx, 1                                        ;
                                ; LOE eax edx ecx ebx edi
.B7.12:                         ; Preds .B7.12 .B7.11
        mov       esi, DWORD PTR [32+eax]                       ;220.5
        inc       ebx                                           ;220.5
        neg       esi                                           ;220.5
        add       esi, edi                                      ;220.5
        inc       edi                                           ;220.5
        imul      esi, DWORD PTR [28+eax]                       ;220.5
        mov       ecx, DWORD PTR [edx+ecx]                      ;220.5
        mov       eax, DWORD PTR [8+ebp]                        ;220.5
        mov       DWORD PTR [8+ecx+esi], 0                      ;220.5
        mov       ecx, DWORD PTR [eax]                          ;220.5
        lea       eax, DWORD PTR [ecx+ecx*4]                    ;220.5
        lea       ecx, DWORD PTR [eax*8]                        ;220.5
        lea       eax, DWORD PTR [edx+eax*8]                    ;220.5
        cmp       ebx, DWORD PTR [24+eax]                       ;220.5
        jle       .B7.12        ; Prob 82%                      ;220.5
                                ; LOE eax edx ecx ebx edi
.B7.14:                         ; Preds .B7.10 .B7.12
        xor       edx, edx                                      ;221.5
        mov       WORD PTR [38+eax], dx                         ;221.5
        add       esp, 52                                       ;221.5
        pop       ebx                                           ;221.5
        pop       edi                                           ;221.5
        pop       esi                                           ;221.5
        mov       esp, ebp                                      ;221.5
        pop       ebp                                           ;221.5
        ret                                                     ;221.5
                                ; LOE
.B7.15:                         ; Preds .B7.1
        jge       .B7.48        ; Prob 5%                       ;222.13
                                ; LOE eax edx ecx ebx esi al ah
.B7.16:                         ; Preds .B7.15
        mov       edi, DWORD PTR [8+esp]                        ;223.2
        sub       edx, eax                                      ;
        mov       DWORD PTR [esp], ecx                          ;
        mov       ecx, edi                                      ;223.2
        sub       ecx, esi                                      ;223.2
        mov       DWORD PTR [12+esp], ecx                       ;223.2
        cmp       edi, esi                                      ;223.2
        mov       ecx, DWORD PTR [esp]                          ;223.2
        jle       .B7.20        ; Prob 10%                      ;223.2
                                ; LOE edx ecx ebx esi
.B7.17:                         ; Preds .B7.16
        mov       ecx, esi                                      ;
        xor       eax, eax                                      ;
        mov       DWORD PTR [esp], esi                          ;
        mov       DWORD PTR [16+esp], ecx                       ;
        mov       DWORD PTR [8+esp], edx                        ;
        mov       esi, DWORD PTR [4+esp]                        ;
                                ; LOE eax ebx esi
.B7.18:                         ; Preds .B7.54 .B7.17
        mov       edi, DWORD PTR [8+esp]                        ;224.48
        lea       ecx, DWORD PTR [ebx+ebx*4]                    ;224.9
        neg       esi                                           ;224.9
        mov       edx, DWORD PTR [28+edi+ecx*8]                 ;224.48
        mov       ebx, DWORD PTR [32+edi+ecx*8]                 ;224.48
        imul      ebx, edx                                      ;224.9
        mov       ecx, DWORD PTR [edi+ecx*8]                    ;224.48
        mov       edi, edx                                      ;224.9
        sub       ecx, ebx                                      ;224.9
        mov       ebx, DWORD PTR [16+esp]                       ;224.9
        inc       ebx                                           ;224.9
        mov       DWORD PTR [16+esp], ebx                       ;224.9
        imul      edi, ebx                                      ;224.9
        mov       ebx, DWORD PTR [esp]                          ;224.9
        add       esi, ebx                                      ;224.9
        mov       edi, DWORD PTR [ecx+edi]                      ;224.9
        lea       esi, DWORD PTR [1+eax+esi]                    ;224.9
        imul      edx, esi                                      ;224.9
        mov       DWORD PTR [ecx+edx], edi                      ;224.9
        mov       edx, DWORD PTR [8+ebp]                        ;225.48
        mov       ecx, DWORD PTR [8+esp]                        ;225.48
        mov       edx, DWORD PTR [edx]                          ;225.48
        lea       edx, DWORD PTR [edx+edx*4]                    ;225.9
        mov       esi, DWORD PTR [28+ecx+edx*8]                 ;225.48
        mov       edi, DWORD PTR [32+ecx+edx*8]                 ;225.48
        imul      edi, esi                                      ;225.9
        mov       edx, DWORD PTR [ecx+edx*8]                    ;225.48
        sub       edx, edi                                      ;225.9
        mov       edi, DWORD PTR [12+ebp]                       ;225.9
        mov       ecx, DWORD PTR [16+esp]                       ;225.9
        imul      ecx, esi                                      ;225.9
        mov       edi, DWORD PTR [edi]                          ;225.9
        neg       edi                                           ;225.9
        add       edi, ebx                                      ;225.9
        mov       ecx, DWORD PTR [4+edx+ecx]                    ;225.9
        lea       edi, DWORD PTR [1+eax+edi]                    ;225.9
        imul      esi, edi                                      ;225.9
        mov       DWORD PTR [4+edx+esi], ecx                    ;225.9
        mov       esi, DWORD PTR [8+ebp]                        ;226.48
        mov       edi, DWORD PTR [8+esp]                        ;226.48
        mov       edx, DWORD PTR [esi]                          ;226.48
        lea       ecx, DWORD PTR [edx+edx*4]                    ;226.9
        mov       edx, DWORD PTR [28+edi+ecx*8]                 ;226.48
        mov       esi, DWORD PTR [32+edi+ecx*8]                 ;226.48
        mov       edi, DWORD PTR [edi+ecx*8]                    ;226.48
        mov       ecx, DWORD PTR [12+ebp]                       ;226.9
        imul      esi, edx                                      ;226.9
        mov       ecx, DWORD PTR [ecx]                          ;226.9
        sub       edi, esi                                      ;226.9
        neg       ecx                                           ;226.9
        add       ecx, ebx                                      ;226.9
        mov       esi, DWORD PTR [16+esp]                       ;226.9
        imul      esi, edx                                      ;226.9
        lea       ebx, DWORD PTR [1+eax+ecx]                    ;226.9
        inc       eax                                           ;223.2
        imul      edx, ebx                                      ;226.9
        mov       ecx, DWORD PTR [8+edi+esi]                    ;226.9
        mov       DWORD PTR [8+edi+edx], ecx                    ;226.9
        cmp       eax, DWORD PTR [12+esp]                       ;223.2
        jae       .B7.19        ; Prob 18%                      ;223.2
                                ; LOE eax
.B7.54:                         ; Preds .B7.18
        mov       esi, DWORD PTR [12+ebp]                       ;217.3
        mov       edx, DWORD PTR [8+ebp]                        ;217.3
        mov       esi, DWORD PTR [esi]                          ;217.3
        mov       ebx, DWORD PTR [edx]                          ;217.3
        jmp       .B7.18        ; Prob 100%                     ;217.3
                                ; LOE eax ebx esi
.B7.19:                         ; Preds .B7.18
        mov       eax, DWORD PTR [8+ebp]                        ;228.5
        mov       esi, DWORD PTR [12+ebp]                       ;228.5
        mov       edx, DWORD PTR [8+esp]                        ;
        mov       ebx, DWORD PTR [eax]                          ;228.5
        mov       esi, DWORD PTR [esi]                          ;228.5
        lea       ecx, DWORD PTR [ebx+ebx*4]                    ;
        shl       ecx, 3                                        ;
                                ; LOE edx ecx ebx esi
.B7.20:                         ; Preds .B7.16 .B7.19
        add       ecx, edx                                      ;228.5
        movsx     edi, WORD PTR [36+ecx]                        ;228.5
        add       edi, esi                                      ;228.5
        movsx     eax, WORD PTR [38+ecx]                        ;228.5
        sub       edi, eax                                      ;228.5
        sub       eax, esi                                      ;228.5
        test      edi, edi                                      ;228.5
        jle       .B7.42        ; Prob 50%                      ;228.5
                                ; LOE eax edx ecx ebx edi
.B7.21:                         ; Preds .B7.20
        mov       esi, edi                                      ;228.5
        shr       esi, 31                                       ;228.5
        add       esi, edi                                      ;228.5
        sar       esi, 1                                        ;228.5
        test      esi, esi                                      ;228.5
        jbe       .B7.47        ; Prob 10%                      ;228.5
                                ; LOE eax edx ebx esi edi
.B7.22:                         ; Preds .B7.21
        mov       DWORD PTR [4+esp], eax                        ;
        xor       ecx, ecx                                      ;
        mov       DWORD PTR [8+esp], esi                        ;
        mov       DWORD PTR [esp], edi                          ;
        mov       eax, DWORD PTR [8+ebp]                        ;
                                ; LOE eax edx ecx ebx
.B7.23:                         ; Preds .B7.57 .B7.22
        mov       edi, DWORD PTR [4+esp]                        ;228.5
        lea       ebx, DWORD PTR [ebx+ebx*4]                    ;228.5
        mov       DWORD PTR [12+esp], ecx                       ;
        lea       esi, DWORD PTR [1+edi+ecx*2]                  ;228.5
        mov       ecx, DWORD PTR [32+edx+ebx*8]                 ;228.5
        neg       ecx                                           ;228.5
        add       ecx, esi                                      ;228.5
        inc       esi                                           ;228.5
        imul      ecx, DWORD PTR [28+edx+ebx*8]                 ;228.5
        mov       edi, DWORD PTR [edx+ebx*8]                    ;228.5
        xor       ebx, ebx                                      ;228.5
        mov       DWORD PTR [edi+ecx], ebx                      ;228.5
        mov       ecx, DWORD PTR [eax]                          ;228.5
        lea       ecx, DWORD PTR [ecx+ecx*4]                    ;228.5
        sub       esi, DWORD PTR [32+edx+ecx*8]                 ;228.5
        imul      esi, DWORD PTR [28+edx+ecx*8]                 ;228.5
        mov       edi, DWORD PTR [edx+ecx*8]                    ;228.5
        mov       ecx, DWORD PTR [12+esp]                       ;228.5
        inc       ecx                                           ;228.5
        mov       DWORD PTR [edi+esi], ebx                      ;228.5
        cmp       ecx, DWORD PTR [8+esp]                        ;228.5
        jae       .B7.24        ; Prob 36%                      ;228.5
                                ; LOE eax edx ecx
.B7.57:                         ; Preds .B7.23
        mov       ebx, DWORD PTR [eax]                          ;217.12
        jmp       .B7.23        ; Prob 100%                     ;217.12
                                ; LOE eax edx ecx ebx
.B7.24:                         ; Preds .B7.23
        mov       ebx, DWORD PTR [8+ebp]                        ;228.5
        lea       ecx, DWORD PTR [1+ecx+ecx]                    ;228.5
        mov       eax, DWORD PTR [4+esp]                        ;
        mov       edi, DWORD PTR [esp]                          ;
        mov       ebx, DWORD PTR [ebx]                          ;228.5
                                ; LOE eax edx ecx ebx edi
.B7.25:                         ; Preds .B7.24 .B7.47
        lea       esi, DWORD PTR [-1+ecx]                       ;228.5
        cmp       edi, esi                                      ;228.5
        jbe       .B7.27        ; Prob 10%                      ;228.5
                                ; LOE eax edx ecx ebx
.B7.26:                         ; Preds .B7.25
        mov       esi, DWORD PTR [8+ebp]                        ;228.5
        lea       ebx, DWORD PTR [ebx+ebx*4]                    ;228.5
        add       ecx, eax                                      ;228.5
        mov       eax, DWORD PTR [edx+ebx*8]                    ;228.5
        sub       ecx, DWORD PTR [32+edx+ebx*8]                 ;228.5
        imul      ecx, DWORD PTR [28+edx+ebx*8]                 ;228.5
        mov       DWORD PTR [eax+ecx], 0                        ;228.5
        mov       ebx, DWORD PTR [esi]                          ;228.5
                                ; LOE edx ebx
.B7.27:                         ; Preds .B7.25 .B7.26
        mov       esi, DWORD PTR [12+ebp]                       ;229.5
        lea       ecx, DWORD PTR [ebx+ebx*4]                    ;
        lea       ecx, DWORD PTR [edx+ecx*8]                    ;
        movsx     eax, WORD PTR [38+ecx]                        ;229.5
        mov       edi, eax                                      ;229.5
        mov       esi, DWORD PTR [esi]                          ;229.5
        sub       edi, esi                                      ;229.5
        mov       DWORD PTR [4+esp], edi                        ;229.5
        movsx     edi, WORD PTR [36+ecx]                        ;229.5
        add       esi, edi                                      ;229.5
        sub       esi, eax                                      ;229.5
        mov       eax, DWORD PTR [4+esp]                        ;229.5
        test      esi, esi                                      ;229.5
        jle       .B7.42        ; Prob 50%                      ;229.5
                                ; LOE eax edx ecx ebx esi al ah
.B7.28:                         ; Preds .B7.27
        mov       edi, esi                                      ;229.5
        shr       edi, 31                                       ;229.5
        add       edi, esi                                      ;229.5
        sar       edi, 1                                        ;229.5
        test      edi, edi                                      ;229.5
        jbe       .B7.46        ; Prob 10%                      ;229.5
                                ; LOE eax edx ebx esi edi al ah
.B7.29:                         ; Preds .B7.28
        mov       DWORD PTR [4+esp], eax                        ;
        xor       ecx, ecx                                      ;
        mov       DWORD PTR [8+esp], edi                        ;
        mov       DWORD PTR [esp], esi                          ;
        mov       eax, DWORD PTR [8+ebp]                        ;
                                ; LOE eax edx ecx ebx
.B7.30:                         ; Preds .B7.56 .B7.29
        mov       edi, DWORD PTR [4+esp]                        ;229.5
        lea       ebx, DWORD PTR [ebx+ebx*4]                    ;229.5
        mov       DWORD PTR [12+esp], ecx                       ;
        lea       esi, DWORD PTR [1+edi+ecx*2]                  ;229.5
        mov       ecx, DWORD PTR [32+edx+ebx*8]                 ;229.5
        neg       ecx                                           ;229.5
        add       ecx, esi                                      ;229.5
        inc       esi                                           ;229.5
        imul      ecx, DWORD PTR [28+edx+ebx*8]                 ;229.5
        mov       edi, DWORD PTR [edx+ebx*8]                    ;229.5
        xor       ebx, ebx                                      ;229.5
        mov       DWORD PTR [4+edi+ecx], ebx                    ;229.5
        mov       ecx, DWORD PTR [eax]                          ;229.5
        lea       ecx, DWORD PTR [ecx+ecx*4]                    ;229.5
        sub       esi, DWORD PTR [32+edx+ecx*8]                 ;229.5
        imul      esi, DWORD PTR [28+edx+ecx*8]                 ;229.5
        mov       edi, DWORD PTR [edx+ecx*8]                    ;229.5
        mov       ecx, DWORD PTR [12+esp]                       ;229.5
        inc       ecx                                           ;229.5
        mov       DWORD PTR [4+edi+esi], ebx                    ;229.5
        cmp       ecx, DWORD PTR [8+esp]                        ;229.5
        jae       .B7.31        ; Prob 36%                      ;229.5
                                ; LOE eax edx ecx
.B7.56:                         ; Preds .B7.30
        mov       ebx, DWORD PTR [eax]                          ;217.12
        jmp       .B7.30        ; Prob 100%                     ;217.12
                                ; LOE eax edx ecx ebx
.B7.31:                         ; Preds .B7.30
        mov       ebx, DWORD PTR [8+ebp]                        ;229.5
        lea       ecx, DWORD PTR [1+ecx+ecx]                    ;229.5
        mov       eax, DWORD PTR [4+esp]                        ;
        mov       esi, DWORD PTR [esp]                          ;
        mov       ebx, DWORD PTR [ebx]                          ;229.5
                                ; LOE eax edx ecx ebx esi al ah
.B7.32:                         ; Preds .B7.31 .B7.46
        lea       edi, DWORD PTR [-1+ecx]                       ;229.5
        cmp       esi, edi                                      ;229.5
        jbe       .B7.34        ; Prob 10%                      ;229.5
                                ; LOE eax edx ecx ebx al ah
.B7.33:                         ; Preds .B7.32
        mov       esi, DWORD PTR [8+ebp]                        ;229.5
        lea       ebx, DWORD PTR [ebx+ebx*4]                    ;229.5
        add       ecx, eax                                      ;229.5
        mov       eax, DWORD PTR [edx+ebx*8]                    ;229.5
        sub       ecx, DWORD PTR [32+edx+ebx*8]                 ;229.5
        imul      ecx, DWORD PTR [28+edx+ebx*8]                 ;229.5
        mov       DWORD PTR [4+eax+ecx], 0                      ;229.5
        mov       ebx, DWORD PTR [esi]                          ;229.5
                                ; LOE edx ebx
.B7.34:                         ; Preds .B7.32 .B7.33
        mov       esi, DWORD PTR [12+ebp]                       ;230.5
        lea       ecx, DWORD PTR [ebx+ebx*4]                    ;
        lea       ecx, DWORD PTR [edx+ecx*8]                    ;
        movsx     eax, WORD PTR [38+ecx]                        ;230.5
        mov       edi, eax                                      ;230.5
        mov       esi, DWORD PTR [esi]                          ;230.5
        sub       edi, esi                                      ;230.5
        mov       DWORD PTR [4+esp], edi                        ;230.5
        movsx     edi, WORD PTR [36+ecx]                        ;230.5
        add       esi, edi                                      ;230.5
        sub       esi, eax                                      ;230.5
        mov       eax, DWORD PTR [4+esp]                        ;230.5
        test      esi, esi                                      ;230.5
        jle       .B7.42        ; Prob 50%                      ;230.5
                                ; LOE eax edx ecx ebx esi al ah
.B7.35:                         ; Preds .B7.34
        mov       edi, esi                                      ;230.5
        shr       edi, 31                                       ;230.5
        add       edi, esi                                      ;230.5
        sar       edi, 1                                        ;230.5
        test      edi, edi                                      ;230.5
        jbe       .B7.45        ; Prob 10%                      ;230.5
                                ; LOE eax edx ebx esi edi al ah
.B7.36:                         ; Preds .B7.35
        mov       DWORD PTR [4+esp], eax                        ;
        xor       ecx, ecx                                      ;
        mov       DWORD PTR [8+esp], edi                        ;
        mov       DWORD PTR [esp], esi                          ;
        mov       eax, DWORD PTR [8+ebp]                        ;
                                ; LOE eax edx ecx ebx
.B7.37:                         ; Preds .B7.55 .B7.36
        mov       DWORD PTR [12+esp], ecx                       ;
        lea       edi, DWORD PTR [ebx+ebx*4]                    ;230.5
        mov       ebx, DWORD PTR [4+esp]                        ;230.5
        mov       esi, DWORD PTR [32+edx+edi*8]                 ;230.5
        neg       esi                                           ;230.5
        lea       ebx, DWORD PTR [1+ebx+ecx*2]                  ;230.5
        xor       ecx, ecx                                      ;230.5
        add       esi, ebx                                      ;230.5
        inc       ebx                                           ;230.5
        imul      esi, DWORD PTR [28+edx+edi*8]                 ;230.5
        mov       edi, DWORD PTR [edx+edi*8]                    ;230.5
        mov       DWORD PTR [8+edi+esi], ecx                    ;230.5
        mov       esi, DWORD PTR [eax]                          ;230.5
        lea       esi, DWORD PTR [esi+esi*4]                    ;230.5
        sub       ebx, DWORD PTR [32+edx+esi*8]                 ;230.5
        imul      ebx, DWORD PTR [28+edx+esi*8]                 ;230.5
        mov       edi, DWORD PTR [edx+esi*8]                    ;230.5
        mov       DWORD PTR [8+edi+ebx], ecx                    ;230.5
        mov       ecx, DWORD PTR [12+esp]                       ;230.5
        inc       ecx                                           ;230.5
        cmp       ecx, DWORD PTR [8+esp]                        ;230.5
        jae       .B7.38        ; Prob 36%                      ;230.5
                                ; LOE eax edx ecx
.B7.55:                         ; Preds .B7.37
        mov       ebx, DWORD PTR [eax]                          ;217.12
        jmp       .B7.37        ; Prob 100%                     ;217.12
                                ; LOE eax edx ecx ebx
.B7.38:                         ; Preds .B7.37
        mov       ebx, DWORD PTR [8+ebp]                        ;230.5
        lea       ecx, DWORD PTR [1+ecx+ecx]                    ;230.5
        mov       esi, DWORD PTR [esp]                          ;
        mov       eax, DWORD PTR [4+esp]                        ;
        mov       ebx, DWORD PTR [ebx]                          ;230.5
                                ; LOE eax edx ecx ebx esi al ah
.B7.39:                         ; Preds .B7.38 .B7.45
        lea       edi, DWORD PTR [-1+ecx]                       ;230.5
        cmp       esi, edi                                      ;230.5
        jbe       .B7.41        ; Prob 10%                      ;230.5
                                ; LOE eax edx ecx ebx al ah
.B7.40:                         ; Preds .B7.39
        mov       esi, DWORD PTR [8+ebp]                        ;230.5
        lea       ebx, DWORD PTR [ebx+ebx*4]                    ;230.5
        add       ecx, eax                                      ;230.5
        mov       eax, DWORD PTR [edx+ebx*8]                    ;230.5
        sub       ecx, DWORD PTR [32+edx+ebx*8]                 ;230.5
        imul      ecx, DWORD PTR [28+edx+ebx*8]                 ;230.5
        mov       DWORD PTR [8+eax+ecx], 0                      ;230.5
        mov       ebx, DWORD PTR [esi]                          ;230.5
                                ; LOE edx ebx
.B7.41:                         ; Preds .B7.39 .B7.40
        lea       eax, DWORD PTR [ebx+ebx*4]                    ;
        lea       ecx, DWORD PTR [edx+eax*8]                    ;
        mov       edx, DWORD PTR [12+ebp]                       ;231.33
        movzx     eax, WORD PTR [38+ecx]                        ;231.33
        sub       eax, DWORD PTR [edx]                          ;231.33
                                ; LOE eax ecx
.B7.42:                         ; Preds .B7.27 .B7.20 .B7.41 .B7.34
        mov       WORD PTR [38+ecx], ax                         ;231.5
                                ; LOE
.B7.43:                         ; Preds .B7.42
        add       esp, 52                                       ;236.1
        pop       ebx                                           ;236.1
        pop       edi                                           ;236.1
        pop       esi                                           ;236.1
        mov       esp, ebp                                      ;236.1
        pop       ebp                                           ;236.1
        ret                                                     ;236.1
                                ; LOE
.B7.44:                         ; Preds .B7.2                   ; Infreq
        mov       ebx, DWORD PTR [24+eax]                       ;219.5
        jmp       .B7.6         ; Prob 100%                     ;219.5
                                ; LOE eax edx ecx ebx
.B7.45:                         ; Preds .B7.35                  ; Infreq
        mov       ecx, 1                                        ;
        jmp       .B7.39        ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi al ah
.B7.46:                         ; Preds .B7.28                  ; Infreq
        mov       ecx, 1                                        ;
        jmp       .B7.32        ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi al ah
.B7.47:                         ; Preds .B7.21                  ; Infreq
        mov       ecx, 1                                        ;
        jmp       .B7.25        ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx edi
.B7.48:                         ; Preds .B7.15                  ; Infreq
        mov       DWORD PTR [esp], 0                            ;233.5
        lea       edx, DWORD PTR [esp]                          ;233.5
        mov       DWORD PTR [32+esp], 23                        ;233.5
        lea       eax, DWORD PTR [32+esp]                       ;233.5
        mov       DWORD PTR [36+esp], OFFSET FLAT: __STRLITPACK_16 ;233.5
        push      32                                            ;233.5
        push      eax                                           ;233.5
        push      OFFSET FLAT: __STRLITPACK_38.0.7              ;233.5
        push      -2088435968                                   ;233.5
        push      911                                           ;233.5
        push      edx                                           ;233.5
        call      _for_write_seq_lis                            ;233.5
                                ; LOE
.B7.49:                         ; Preds .B7.48                  ; Infreq
        push      32                                            ;234.5
        xor       eax, eax                                      ;234.5
        push      eax                                           ;234.5
        push      eax                                           ;234.5
        push      -2088435968                                   ;234.5
        push      eax                                           ;234.5
        push      OFFSET FLAT: __STRLITPACK_39                  ;234.5
        call      _for_stop_core                                ;234.5
                                ; LOE
.B7.52:                         ; Preds .B7.49                  ; Infreq
        add       esp, 100                                      ;234.5
        pop       ebx                                           ;234.5
        pop       edi                                           ;234.5
        pop       esi                                           ;234.5
        mov       esp, ebp                                      ;234.5
        pop       ebp                                           ;234.5
        ret                                                     ;234.5
        ALIGN     16
                                ; LOE
; mark_end;
_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ERASE ENDP
_TEXT	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_38.0.7	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ERASE
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_TRANSLINK_MODULE_mp_TRANLINK_INSERT
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_TRANSLINK_MODULE_mp_TRANLINK_INSERT
_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_INSERT	PROC NEAR 
; parameter 1: 24 + esp
; parameter 2: 28 + esp
; parameter 3: 32 + esp
; parameter 4: 36 + esp
.B8.1:                          ; Preds .B8.0
        push      ebx                                           ;239.12
        push      ebp                                           ;239.12
        sub       esp, 12                                       ;239.12
        mov       ebx, DWORD PTR [24+esp]                       ;239.12
        mov       eax, DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY+32] ;242.3
        neg       eax                                           ;244.15
        add       eax, DWORD PTR [ebx]                          ;244.15
        mov       edx, DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY] ;242.3
        mov       ebp, DWORD PTR [28+esp]                       ;239.12
        lea       ecx, DWORD PTR [eax+eax*4]                    ;244.15
        movsx     eax, WORD PTR [36+edx+ecx*8]                  ;242.13
        cmp       eax, DWORD PTR [ebp]                          ;244.15
        jge       .B8.3         ; Prob 50%                      ;244.15
                                ; LOE eax ebx ebp esi edi
.B8.2:                          ; Preds .B8.1
        add       eax, 50                                       ;245.3
        lea       edx, DWORD PTR [8+esp]                        ;246.11
        mov       DWORD PTR [8+esp], eax                        ;245.3
        mov       eax, ebx                                      ;246.11
        call      _DYNUST_TRANSLINK_MODULE_mp_TRANLINK_SETUP.   ;246.11
                                ; LOE ebx ebp esi edi
.B8.3:                          ; Preds .B8.2 .B8.1
        mov       eax, DWORD PTR [32+esp]                       ;239.12
        mov       eax, DWORD PTR [eax]                          ;249.3
        cmp       eax, 1                                        ;249.3
        je        .B8.8         ; Prob 25%                      ;249.3
                                ; LOE eax ebx ebp esi edi
.B8.4:                          ; Preds .B8.3
        cmp       eax, 2                                        ;249.3
        jne       .B8.6         ; Prob 67%                      ;249.3
                                ; LOE eax ebx ebp esi edi
.B8.5:                          ; Preds .B8.4
        imul      eax, DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY+32], -40 ;252.5
        mov       edx, DWORD PTR [ebx]                          ;252.5
        add       eax, DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY] ;252.5
        lea       ecx, DWORD PTR [edx+edx*4]                    ;252.5
        mov       ebx, DWORD PTR [32+eax+ecx*8]                 ;252.5
        neg       ebx                                           ;252.5
        add       ebx, DWORD PTR [ebp]                          ;252.5
        imul      ebx, DWORD PTR [28+eax+ecx*8]                 ;252.5
        mov       ebp, DWORD PTR [36+esp]                       ;252.5
        mov       eax, DWORD PTR [eax+ecx*8]                    ;252.5
        mov       ebp, DWORD PTR [ebp]                          ;252.5
        mov       DWORD PTR [4+eax+ebx], ebp                    ;252.5
        add       esp, 12                                       ;252.5
        pop       ebp                                           ;252.5
        pop       ebx                                           ;252.5
        ret                                                     ;252.5
                                ; LOE esi edi
.B8.6:                          ; Preds .B8.4
        cmp       eax, 3                                        ;249.3
        jne       .B8.9         ; Prob 50%                      ;249.3
                                ; LOE ebx ebp esi edi
.B8.7:                          ; Preds .B8.6
        mov       ecx, DWORD PTR [36+esp]                       ;254.45
        mov       edx, DWORD PTR [ebx]                          ;254.5
        imul      eax, DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY+32], -40 ;254.5
        cvtsi2ss  xmm0, DWORD PTR [ecx]                         ;254.45
        divss     xmm0, DWORD PTR [_2il0floatpacket.18]         ;254.5
        add       eax, DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY] ;254.5
        lea       ebx, DWORD PTR [edx+edx*4]                    ;254.5
        mov       edx, DWORD PTR [32+eax+ebx*8]                 ;254.5
        neg       edx                                           ;254.5
        add       edx, DWORD PTR [ebp]                          ;254.5
        imul      edx, DWORD PTR [28+eax+ebx*8]                 ;254.5
        mov       ebp, DWORD PTR [eax+ebx*8]                    ;254.5
        movss     DWORD PTR [8+ebp+edx], xmm0                   ;254.5
        add       esp, 12                                       ;254.5
        pop       ebp                                           ;254.5
        pop       ebx                                           ;254.5
        ret                                                     ;254.5
                                ; LOE esi edi
.B8.8:                          ; Preds .B8.3
        imul      eax, DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY+32], -40 ;250.5
        mov       edx, DWORD PTR [ebx]                          ;250.5
        add       eax, DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY] ;250.5
        lea       ecx, DWORD PTR [edx+edx*4]                    ;250.5
        mov       ebx, DWORD PTR [32+eax+ecx*8]                 ;250.5
        neg       ebx                                           ;250.5
        add       ebx, DWORD PTR [ebp]                          ;250.5
        imul      ebx, DWORD PTR [28+eax+ecx*8]                 ;250.5
        mov       ebp, DWORD PTR [36+esp]                       ;250.5
        mov       eax, DWORD PTR [eax+ecx*8]                    ;250.5
        mov       ebp, DWORD PTR [ebp]                          ;250.5
        mov       DWORD PTR [eax+ebx], ebp                      ;250.5
                                ; LOE esi edi
.B8.9:                          ; Preds .B8.6 .B8.8
        add       esp, 12                                       ;256.1
        pop       ebp                                           ;256.1
        pop       ebx                                           ;256.1
        ret                                                     ;256.1
        ALIGN     16
                                ; LOE
; mark_end;
_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_INSERT ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_TRANSLINK_MODULE_mp_TRANLINK_INSERT
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_TRANSLINK_MODULE_mp_TRANLINK_VALUE
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_TRANSLINK_MODULE_mp_TRANLINK_VALUE
_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_VALUE	PROC NEAR 
; parameter 1: 8 + ebp
; parameter 2: 12 + ebp
; parameter 3: 16 + ebp
.B9.1:                          ; Preds .B9.0
        push      ebp                                           ;260.18
        mov       ebp, esp                                      ;260.18
        and       esp, -16                                      ;260.18
        push      esi                                           ;260.18
        push      edi                                           ;260.18
        push      ebx                                           ;260.18
        sub       esp, 100                                      ;260.18
        mov       ecx, DWORD PTR [8+ebp]                        ;260.18
        mov       eax, DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY] ;263.3
        mov       esi, DWORD PTR [12+ebp]                       ;260.18
        mov       edi, DWORD PTR [ecx]                          ;263.16
        mov       ecx, DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY+32] ;263.3
        mov       ebx, DWORD PTR [16+ebp]                       ;260.18
        mov       esi, DWORD PTR [esi]                          ;265.3
        lea       edx, DWORD PTR [edi+edi*4]                    ;263.3
        lea       eax, DWORD PTR [eax+edx*8]                    ;263.3
        lea       edx, DWORD PTR [ecx+ecx*4]                    ;263.3
        shl       edx, 3                                        ;263.3
        mov       ecx, edx                                      ;263.3
        neg       edx                                           ;
        neg       ecx                                           ;263.3
        add       edx, eax                                      ;
        mov       DWORD PTR [44+esp], edx                       ;
        movsx     edi, WORD PTR [38+ecx+eax]                    ;263.16
        movsx     edx, WORD PTR [36+ecx+eax]                    ;265.17
        cmp       esi, edx                                      ;265.15
        mov       DWORD PTR [36+esp], edi                       ;263.16
        mov       DWORD PTR [40+esp], edx                       ;265.17
        mov       edi, DWORD PTR [ebx]                          ;272.3
        mov       ebx, 0                                        ;
        mov       edx, DWORD PTR [44+esp]                       ;265.15
        jg        .B9.3         ; Prob 50%                      ;265.15
                                ; LOE eax edx ecx ebx esi edi dl dh
.B9.2:                          ; Preds .B9.1
        mov       DWORD PTR [56+esp], ebx                       ;
        mov       DWORD PTR [44+esp], edx                       ;
        mov       DWORD PTR [52+esp], ecx                       ;
        mov       DWORD PTR [48+esp], eax                       ;
        jmp       .B9.10        ; Prob 100%                     ;
                                ; LOE esi edi
.B9.3:                          ; Preds .B9.1
        mov       DWORD PTR [56+esp], ebx                       ;
        mov       DWORD PTR [44+esp], edx                       ;
        mov       DWORD PTR [52+esp], ecx                       ;
        mov       DWORD PTR [48+esp], eax                       ;
                                ; LOE esi edi
.B9.4:                          ; Preds .B9.27 .B9.3
        mov       DWORD PTR [esp], 0                            ;266.6
        lea       ebx, DWORD PTR [esp]                          ;266.6
        mov       DWORD PTR [64+esp], 30                        ;266.6
        lea       eax, DWORD PTR [64+esp]                       ;266.6
        mov       DWORD PTR [68+esp], OFFSET FLAT: __STRLITPACK_14 ;266.6
        push      32                                            ;266.6
        push      eax                                           ;266.6
        push      OFFSET FLAT: __STRLITPACK_40.0.9              ;266.6
        push      -2088435968                                   ;266.6
        push      911                                           ;266.6
        push      ebx                                           ;266.6
        call      _for_write_seq_lis                            ;266.6
                                ; LOE ebx esi edi
.B9.5:                          ; Preds .B9.4
        mov       DWORD PTR [24+esp], 0                         ;267.6
        lea       eax, DWORD PTR [96+esp]                       ;267.6
        mov       DWORD PTR [96+esp], 9                         ;267.6
        mov       DWORD PTR [100+esp], OFFSET FLAT: __STRLITPACK_12 ;267.6
        push      32                                            ;267.6
        push      eax                                           ;267.6
        push      OFFSET FLAT: __STRLITPACK_41.0.9              ;267.6
        push      -2088435968                                   ;267.6
        push      911                                           ;267.6
        push      ebx                                           ;267.6
        call      _for_write_seq_lis                            ;267.6
                                ; LOE ebx esi edi
.B9.6:                          ; Preds .B9.5
        mov       DWORD PTR [136+esp], esi                      ;267.6
        lea       eax, DWORD PTR [136+esp]                      ;267.6
        push      eax                                           ;267.6
        push      OFFSET FLAT: __STRLITPACK_42.0.9              ;267.6
        push      ebx                                           ;267.6
        call      _for_write_seq_lis_xmit                       ;267.6
                                ; LOE ebx esi edi
.B9.7:                          ; Preds .B9.6
        mov       DWORD PTR [60+esp], 0                         ;268.3
        lea       eax, DWORD PTR [140+esp]                      ;268.3
        mov       DWORD PTR [140+esp], 26                       ;268.3
        mov       DWORD PTR [144+esp], OFFSET FLAT: __STRLITPACK_10 ;268.3
        push      32                                            ;268.3
        push      eax                                           ;268.3
        push      OFFSET FLAT: __STRLITPACK_43.0.9              ;268.3
        push      -2088435968                                   ;268.3
        push      911                                           ;268.3
        push      ebx                                           ;268.3
        call      _for_write_seq_lis                            ;268.3
                                ; LOE ebx esi edi
.B9.8:                          ; Preds .B9.7
        mov       eax, DWORD PTR [124+esp]                      ;268.3
        lea       edx, DWORD PTR [180+esp]                      ;268.3
        mov       WORD PTR [180+esp], ax                        ;268.3
        push      edx                                           ;268.3
        push      OFFSET FLAT: __STRLITPACK_44.0.9              ;268.3
        push      ebx                                           ;268.3
        call      _for_write_seq_lis_xmit                       ;268.3
                                ; LOE esi edi
.B9.9:                          ; Preds .B9.8
        push      32                                            ;269.6
        xor       eax, eax                                      ;269.6
        push      eax                                           ;269.6
        push      eax                                           ;269.6
        push      -2088435968                                   ;269.6
        push      eax                                           ;269.6
        push      OFFSET FLAT: __STRLITPACK_45                  ;269.6
        call      _for_stop_core                                ;269.6
                                ; LOE esi edi
.B9.31:                         ; Preds .B9.9
        add       esp, 120                                      ;269.6
                                ; LOE esi edi
.B9.10:                         ; Preds .B9.31 .B9.2
        cmp       edi, 1                                        ;272.3
        je        .B9.24        ; Prob 25%                      ;272.3
                                ; LOE esi edi
.B9.11:                         ; Preds .B9.10
        mov       ebx, DWORD PTR [56+esp]                       ;
        cmp       edi, 2                                        ;272.3
        mov       edx, DWORD PTR [44+esp]                       ;
        mov       ecx, DWORD PTR [52+esp]                       ;
        mov       eax, DWORD PTR [48+esp]                       ;
        jne       .B9.13        ; Prob 67%                      ;272.3
                                ; LOE eax edx ecx ebx esi edi al dl cl bl ah dh ch bh
.B9.12:                         ; Preds .B9.11
        mov       ebx, DWORD PTR [32+ecx+eax]                   ;275.23
        neg       ebx                                           ;275.6
        add       ebx, esi                                      ;275.6
        imul      ebx, DWORD PTR [28+ecx+eax]                   ;275.6
        mov       eax, DWORD PTR [edx]                          ;275.23
        mov       ebx, DWORD PTR [4+eax+ebx]                    ;275.6
        jmp       .B9.15        ; Prob 100%                     ;275.6
                                ; LOE ebx esi
.B9.13:                         ; Preds .B9.11
        cmp       edi, 3                                        ;272.3
        jne       .B9.17        ; Prob 50%                      ;272.3
                                ; LOE eax edx ecx ebx esi al dl cl bl ah dh ch bh
.B9.14:                         ; Preds .B9.13
        mov       ebx, DWORD PTR [32+ecx+eax]                   ;277.23
        neg       ebx                                           ;277.23
        add       ebx, esi                                      ;277.23
        imul      ebx, DWORD PTR [28+ecx+eax]                   ;277.23
        movss     xmm3, DWORD PTR [_2il0floatpacket.22]         ;277.23
        movss     xmm0, DWORD PTR [_2il0floatpacket.23]         ;277.23
        movss     xmm1, DWORD PTR [_2il0floatpacket.24]         ;277.23
        movss     xmm4, DWORD PTR [_2il0floatpacket.25]         ;277.23
        mov       eax, DWORD PTR [edx]                          ;277.23
        movaps    xmm6, xmm4                                    ;277.23
        addss     xmm6, xmm4                                    ;277.23
        mulss     xmm3, DWORD PTR [8+eax+ebx]                   ;277.23
        andps     xmm0, xmm3                                    ;277.23
        pxor      xmm3, xmm0                                    ;277.23
        movaps    xmm2, xmm3                                    ;277.23
        cmpltss   xmm2, xmm1                                    ;277.23
        andps     xmm1, xmm2                                    ;277.23
        movaps    xmm2, xmm3                                    ;277.23
        addss     xmm2, xmm1                                    ;277.23
        subss     xmm2, xmm1                                    ;277.23
        movaps    xmm7, xmm2                                    ;277.23
        subss     xmm7, xmm3                                    ;277.23
        movaps    xmm5, xmm7                                    ;277.23
        cmpless   xmm7, DWORD PTR [_2il0floatpacket.26]         ;277.23
        cmpnless  xmm5, xmm4                                    ;277.23
        andps     xmm5, xmm6                                    ;277.23
        andps     xmm7, xmm6                                    ;277.23
        subss     xmm2, xmm5                                    ;277.23
        addss     xmm2, xmm7                                    ;277.23
        orps      xmm2, xmm0                                    ;277.23
        cvtss2si  ebx, xmm2                                     ;277.6
                                ; LOE ebx esi
.B9.15:                         ; Preds .B9.12 .B9.14
        test      ebx, ebx                                      ;287.21
        jle       .B9.34        ; Prob 16%                      ;287.21
                                ; LOE ebx esi
.B9.16:                         ; Preds .B9.25 .B9.15
        cmp       esi, DWORD PTR [36+esp]                       ;287.36
        jle       .B9.23        ; Prob 50%                      ;287.36
                                ; LOE ebx
.B9.34:                         ; Preds .B9.15 .B9.16
        lea       esi, DWORD PTR [esp]                          ;266.6
        jmp       .B9.19        ; Prob 100%                     ;266.6
                                ; LOE ebx esi
.B9.17:                         ; Preds .B9.13
        mov       DWORD PTR [esp], 0                            ;279.5
        lea       esi, DWORD PTR [esp]                          ;279.5
        mov       DWORD PTR [48+esp], 24                        ;279.5
        lea       eax, DWORD PTR [48+esp]                       ;279.5
        mov       DWORD PTR [52+esp], OFFSET FLAT: __STRLITPACK_8 ;279.5
        push      32                                            ;279.5
        push      eax                                           ;279.5
        push      OFFSET FLAT: __STRLITPACK_46.0.9              ;279.5
        push      -2088435968                                   ;279.5
        push      911                                           ;279.5
        push      esi                                           ;279.5
        call      _for_write_seq_lis                            ;279.5
                                ; LOE ebx esi bl bh
.B9.18:                         ; Preds .B9.17
        push      32                                            ;280.5
        xor       eax, eax                                      ;280.5
        push      eax                                           ;280.5
        push      eax                                           ;280.5
        push      -2088435968                                   ;280.5
        push      eax                                           ;280.5
        push      OFFSET FLAT: __STRLITPACK_47                  ;280.5
        call      _for_stop_core                                ;280.5
                                ; LOE ebx esi bl bh
.B9.32:                         ; Preds .B9.18
        add       esp, 48                                       ;280.5
                                ; LOE ebx esi
.B9.19:                         ; Preds .B9.32 .B9.34
        mov       DWORD PTR [esp], 0                            ;288.6
        lea       eax, DWORD PTR [32+esp]                       ;288.6
        mov       DWORD PTR [32+esp], 24                        ;288.6
        mov       DWORD PTR [36+esp], OFFSET FLAT: __STRLITPACK_6 ;288.6
        push      32                                            ;288.6
        push      eax                                           ;288.6
        push      OFFSET FLAT: __STRLITPACK_48.0.9              ;288.6
        push      -2088435968                                   ;288.6
        push      911                                           ;288.6
        push      esi                                           ;288.6
        call      _for_write_seq_lis                            ;288.6
                                ; LOE ebx esi
.B9.20:                         ; Preds .B9.19
        mov       DWORD PTR [24+esp], 0                         ;289.6
        lea       eax, DWORD PTR [64+esp]                       ;289.6
        mov       DWORD PTR [64+esp], 23                        ;289.6
        mov       DWORD PTR [68+esp], OFFSET FLAT: __STRLITPACK_4 ;289.6
        push      32                                            ;289.6
        push      eax                                           ;289.6
        push      OFFSET FLAT: __STRLITPACK_49.0.9              ;289.6
        push      -2088435968                                   ;289.6
        push      911                                           ;289.6
        push      esi                                           ;289.6
        call      _for_write_seq_lis                            ;289.6
                                ; LOE ebx esi
.B9.21:                         ; Preds .B9.20
        mov       DWORD PTR [104+esp], ebx                      ;289.6
        lea       eax, DWORD PTR [104+esp]                      ;289.6
        push      eax                                           ;289.6
        push      OFFSET FLAT: __STRLITPACK_50.0.9              ;289.6
        push      esi                                           ;289.6
        call      _for_write_seq_lis_xmit                       ;289.6
                                ; LOE ebx
.B9.22:                         ; Preds .B9.21
        push      32                                            ;290.3
        xor       eax, eax                                      ;290.3
        push      eax                                           ;290.3
        push      eax                                           ;290.3
        push      -2088435968                                   ;290.3
        push      eax                                           ;290.3
        push      OFFSET FLAT: __STRLITPACK_51                  ;290.3
        call      _for_stop_core                                ;290.3
                                ; LOE ebx
.B9.33:                         ; Preds .B9.22
        add       esp, 84                                       ;290.3
                                ; LOE ebx
.B9.23:                         ; Preds .B9.16 .B9.33
        mov       eax, ebx                                      ;293.1
        add       esp, 100                                      ;293.1
        pop       ebx                                           ;293.1
        pop       edi                                           ;293.1
        pop       esi                                           ;293.1
        mov       esp, ebp                                      ;293.1
        pop       ebp                                           ;293.1
        ret                                                     ;293.1
                                ; LOE
.B9.24:                         ; Preds .B9.10
        mov       eax, DWORD PTR [44+esp]                       ;275.23
        mov       DWORD PTR [32+esp], edi                       ;275.23
        mov       edx, DWORD PTR [eax]                          ;275.23
        mov       eax, DWORD PTR [12+ebp]                       ;275.23
                                ; LOE eax edx esi
.B9.25:                         ; Preds .B9.26 .B9.24
        mov       ebx, DWORD PTR [48+esp]                       ;273.6
        mov       edi, esi                                      ;273.6
        mov       ecx, DWORD PTR [52+esp]                       ;273.6
        sub       edi, DWORD PTR [32+ecx+ebx]                   ;273.6
        imul      edi, DWORD PTR [28+ecx+ebx]                   ;273.6
        mov       ebx, DWORD PTR [edx+edi]                      ;273.6
        test      ebx, ebx                                      ;282.36
        jg        .B9.16        ; Prob 18%                      ;282.36
                                ; LOE eax edx ebx esi
.B9.26:                         ; Preds .B9.25
        inc       esi                                           ;283.4
        mov       DWORD PTR [eax], esi                          ;283.4
        cmp       esi, DWORD PTR [40+esp]                       ;265.15
        jle       .B9.25        ; Prob 50%                      ;265.15
                                ; LOE eax edx ebx esi
.B9.27:                         ; Preds .B9.26
        DB        102                                           ;
        DB        15                                            ;
        DB        31                                            ;
        DB        68                                            ;
        DB        0                                             ;
        DB        0                                             ;
        mov       DWORD PTR [56+esp], ebx                       ;
        mov       edi, DWORD PTR [32+esp]                       ;
        jmp       .B9.4         ; Prob 100%                     ;
        ALIGN     16
                                ; LOE esi edi
; mark_end;
_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_VALUE ENDP
_TEXT	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_40.0.9	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_41.0.9	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_42.0.9	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_43.0.9	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_44.0.9	DB	7
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_46.0.9	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_48.0.9	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_49.0.9	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_50.0.9	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_TRANSLINK_MODULE_mp_TRANLINK_VALUE
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_TRANSLINK_MODULE_mp_TRANLINK_SIZE
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_TRANSLINK_MODULE_mp_TRANLINK_SIZE
_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_SIZE	PROC NEAR 
; parameter 1: 20 + esp
.B10.1:                         ; Preds .B10.0
        push      esi                                           ;296.18
        push      edi                                           ;296.18
        push      ebx                                           ;296.18
        push      ebp                                           ;296.18
        mov       eax, DWORD PTR [20+esp]                       ;296.18
        mov       ebx, DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY+32] ;297.4
        mov       ecx, DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY] ;297.4
        mov       edx, DWORD PTR [eax]                          ;297.8
        lea       eax, DWORD PTR [edx+edx*4]                    ;297.4
        lea       edx, DWORD PTR [ebx+ebx*4]                    ;297.4
        shl       edx, 3                                        ;297.4
        lea       esi, DWORD PTR [ecx+eax*8]                    ;297.4
        mov       edi, edx                                      ;297.4
        neg       edx                                           ;298.4
        neg       edi                                           ;297.4
        add       edx, esi                                      ;298.4
        movsx     eax, WORD PTR [36+edi+esi]                    ;297.8
        mov       ebx, eax                                      ;
        mov       ecx, DWORD PTR [28+edi+esi]                   ;298.14
        mov       ebp, ecx                                      ;
        imul      ebx, ecx                                      ;
        neg       ebp                                           ;
        imul      ecx, DWORD PTR [32+edi+esi]                   ;298.4
        mov       edx, DWORD PTR [edx]                          ;298.14
        lea       edi, DWORD PTR [edx+ebx]                      ;298.4
        sub       edi, ecx                                      ;298.4
        cmp       DWORD PTR [edi], 0                            ;298.44
        jg        .B10.5        ; Prob 36%                      ;298.44
                                ; LOE eax edx ecx ebx ebp
.B10.2:                         ; Preds .B10.1
        sub       edx, ecx                                      ;
                                ; LOE eax edx ebx ebp
.B10.3:                         ; Preds .B10.3 .B10.2
        add       ebx, ebp                                      ;299.8
        dec       eax                                           ;299.8
        cmp       DWORD PTR [ebx+edx], 0                        ;298.44
        jle       .B10.3        ; Prob 82%                      ;298.44
                                ; LOE eax edx ebx ebp
.B10.5:                         ; Preds .B10.3 .B10.1
        pop       ebp                                           ;303.1
        pop       ebx                                           ;303.1
        pop       edi                                           ;303.1
        pop       esi                                           ;303.1
        ret                                                     ;303.1
        ALIGN     16
                                ; LOE
; mark_end;
_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_SIZE ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_TRANSLINK_MODULE_mp_TRANLINK_SIZE
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_TRANSLINK_MODULE_mp_TRANLINK_REMOVE
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_TRANSLINK_MODULE_mp_TRANLINK_REMOVE
_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_REMOVE	PROC NEAR 
; parameter 1: 8 + ebp
.B11.1:                         ; Preds .B11.0
        push      ebp                                           ;306.12
        mov       ebp, esp                                      ;306.12
        and       esp, -16                                      ;306.12
        push      esi                                           ;306.12
        push      edi                                           ;306.12
        push      ebx                                           ;306.12
        sub       esp, 52                                       ;306.12
        imul      edi, DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY+32], -40 ;309.32
        mov       eax, DWORD PTR [8+ebp]                        ;306.12
        add       edi, DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY] ;309.32
        mov       esi, DWORD PTR [eax]                          ;309.7
        lea       ebx, DWORD PTR [esi+esi*4]                    ;309.7
        movsx     edx, WORD PTR [36+edi+ebx*8]                  ;309.7
        test      edx, edx                                      ;309.32
        jle       .B11.5        ; Prob 79%                      ;309.32
                                ; LOE ebx esi edi
.B11.2:                         ; Preds .B11.1
        mov       eax, DWORD PTR [12+edi+ebx*8]                 ;310.16
        mov       edx, eax                                      ;310.5
        shr       edx, 1                                        ;310.5
        and       edx, 1                                        ;310.5
        mov       DWORD PTR [esp], eax                          ;310.16
        and       eax, 1                                        ;310.5
        shl       edx, 2                                        ;310.5
        add       eax, eax                                      ;310.5
        or        edx, 1                                        ;310.5
        or        edx, eax                                      ;310.5
        or        edx, 262144                                   ;310.5
        push      edx                                           ;310.5
        push      DWORD PTR [edi+ebx*8]                         ;310.5
        call      _for_dealloc_allocatable                      ;310.5
                                ; LOE eax ebx esi edi
.B11.11:                        ; Preds .B11.2
        add       esp, 8                                        ;310.5
                                ; LOE eax ebx esi edi
.B11.3:                         ; Preds .B11.11
        mov       edx, DWORD PTR [esp]                          ;310.5
        and       edx, -2                                       ;310.5
        mov       DWORD PTR [edi+ebx*8], 0                      ;310.5
        test      eax, eax                                      ;311.17
        mov       DWORD PTR [12+edi+ebx*8], edx                 ;310.5
        mov       DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_VECTORERROR], eax ;310.5
        jne       .B11.6        ; Prob 5%                       ;311.17
                                ; LOE ebx esi edi
.B11.13:                        ; Preds .B11.3
        xor       esi, esi                                      ;316.5
                                ; LOE ebx esi edi
.B11.4:                         ; Preds .B11.12 .B11.13
        mov       WORD PTR [36+edi+ebx*8], si                   ;316.5
        mov       WORD PTR [38+edi+ebx*8], si                   ;317.5
                                ; LOE
.B11.5:                         ; Preds .B11.1 .B11.4
        add       esp, 52                                       ;320.1
        pop       ebx                                           ;320.1
        pop       edi                                           ;320.1
        pop       esi                                           ;320.1
        mov       esp, ebp                                      ;320.1
        pop       ebp                                           ;320.1
        ret                                                     ;320.1
                                ; LOE
.B11.6:                         ; Preds .B11.3                  ; Infreq
        mov       DWORD PTR [esp], 0                            ;312.4
        lea       edx, DWORD PTR [esp]                          ;312.4
        mov       DWORD PTR [32+esp], 37                        ;312.4
        lea       eax, DWORD PTR [32+esp]                       ;312.4
        mov       DWORD PTR [36+esp], OFFSET FLAT: __STRLITPACK_2 ;312.4
        push      32                                            ;312.4
        push      eax                                           ;312.4
        push      OFFSET FLAT: __STRLITPACK_52.0.11             ;312.4
        push      -2088435968                                   ;312.4
        push      911                                           ;312.4
        push      edx                                           ;312.4
        call      _for_write_seq_lis                            ;312.4
                                ; LOE ebx esi edi
.B11.7:                         ; Preds .B11.6                  ; Infreq
        mov       DWORD PTR [24+esp], 0                         ;313.4
        lea       eax, DWORD PTR [64+esp]                       ;313.4
        mov       DWORD PTR [64+esp], esi                       ;313.4
        push      32                                            ;313.4
        push      eax                                           ;313.4
        push      OFFSET FLAT: __STRLITPACK_53.0.11             ;313.4
        push      -2088435968                                   ;313.4
        push      911                                           ;313.4
        lea       edx, DWORD PTR [44+esp]                       ;313.4
        push      edx                                           ;313.4
        call      _for_write_seq_lis                            ;313.4
                                ; LOE ebx edi
.B11.8:                         ; Preds .B11.7                  ; Infreq
        push      32                                            ;314.4
        xor       esi, esi                                      ;314.4
        push      esi                                           ;314.4
        push      esi                                           ;314.4
        push      -2088435968                                   ;314.4
        push      esi                                           ;314.4
        push      OFFSET FLAT: __STRLITPACK_54                  ;314.4
        call      _for_stop_core                                ;314.4
                                ; LOE ebx esi edi
.B11.12:                        ; Preds .B11.8                  ; Infreq
        add       esp, 72                                       ;314.4
        jmp       .B11.4        ; Prob 100%                     ;314.4
        ALIGN     16
                                ; LOE ebx esi edi
; mark_end;
_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_REMOVE ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_TRANSLINK_MODULE_mp_TRANLINK_REMOVE
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_TRANSLINK_MODULE_mp_TRANLINK_CLEAR
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_TRANSLINK_MODULE_mp_TRANLINK_CLEAR
_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_CLEAR	PROC NEAR 
; parameter 1: 44 + esp
; parameter 2: 48 + esp
.B12.1:                         ; Preds .B12.0
        push      esi                                           ;323.12
        push      edi                                           ;323.12
        push      ebx                                           ;323.12
        push      ebp                                           ;323.12
        sub       esp, 24                                       ;323.12
        mov       ecx, DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY+32] ;329.4
        mov       edx, DWORD PTR [44+esp]                       ;323.12
        mov       ebp, DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY] ;329.4
        mov       eax, DWORD PTR [48+esp]                       ;323.12
        lea       ecx, DWORD PTR [ecx+ecx*4]                    ;329.4
        shl       ecx, 3                                        ;329.4
        mov       ebx, DWORD PTR [edx]                          ;329.19
        mov       edi, ecx                                      ;329.4
        neg       edi                                           ;329.4
        mov       DWORD PTR [12+esp], ebx                       ;329.19
        lea       esi, DWORD PTR [ebx+ebx*4]                    ;329.19
        lea       ebx, DWORD PTR [ebp+esi*8]                    ;329.4
        mov       esi, DWORD PTR [eax]                          ;329.4
        movsx     ebx, WORD PTR [36+edi+ebx]                    ;329.19
        cmp       ebx, esi                                      ;329.4
        jl        .B12.8        ; Prob 50%                      ;329.4
                                ; LOE edx ecx ebx ebp esi
.B12.2:                         ; Preds .B12.1
        sub       ebx, esi                                      ;329.4
        inc       ebx                                           ;329.4
        mov       eax, ebx                                      ;329.4
        shr       eax, 31                                       ;329.4
        add       eax, ebx                                      ;329.4
        sar       eax, 1                                        ;329.4
        mov       DWORD PTR [20+esp], eax                       ;329.4
        test      eax, eax                                      ;329.4
        jbe       .B12.9        ; Prob 10%                      ;329.4
                                ; LOE edx ecx ebx ebp esi
.B12.3:                         ; Preds .B12.2
        mov       edi, ebp                                      ;
        xor       eax, eax                                      ;
        sub       edi, ecx                                      ;
        mov       DWORD PTR [8+esp], ebx                        ;
        mov       DWORD PTR [esp], ebp                          ;
        mov       DWORD PTR [16+esp], esi                       ;
        mov       DWORD PTR [4+esp], ecx                        ;
        xor       ecx, ecx                                      ;
        mov       ebp, DWORD PTR [12+esp]                       ;
        mov       ebx, edi                                      ;
                                ; LOE eax edx ecx ebx ebp
.B12.4:                         ; Preds .B12.12 .B12.3
        lea       edi, DWORD PTR [ebp+ebp*4]                    ;330.6
        mov       ebp, DWORD PTR [16+esp]                       ;330.6
        mov       esi, DWORD PTR [32+ebx+edi*8]                 ;330.6
        neg       esi                                           ;330.6
        lea       ebp, DWORD PTR [ebp+eax*2]                    ;330.6
        inc       eax                                           ;329.4
        add       esi, ebp                                      ;330.6
        imul      esi, DWORD PTR [28+ebx+edi*8]                 ;330.6
        mov       edi, DWORD PTR [ebx+edi*8]                    ;330.6
        mov       DWORD PTR [edi+esi], ecx                      ;330.6
        mov       esi, DWORD PTR [edx]                          ;331.6
        lea       esi, DWORD PTR [esi+esi*4]                    ;331.6
        mov       edi, DWORD PTR [32+ebx+esi*8]                 ;331.6
        neg       edi                                           ;331.6
        add       edi, ebp                                      ;331.6
        imul      edi, DWORD PTR [28+ebx+esi*8]                 ;331.6
        mov       esi, DWORD PTR [ebx+esi*8]                    ;331.6
        mov       DWORD PTR [4+esi+edi], ecx                    ;331.6
        mov       edi, DWORD PTR [edx]                          ;332.6
        lea       edi, DWORD PTR [edi+edi*4]                    ;332.6
        mov       esi, DWORD PTR [32+ebx+edi*8]                 ;332.6
        neg       esi                                           ;332.6
        add       esi, ebp                                      ;332.6
        inc       ebp                                           ;330.6
        imul      esi, DWORD PTR [28+ebx+edi*8]                 ;332.6
        mov       edi, DWORD PTR [ebx+edi*8]                    ;332.6
        mov       DWORD PTR [8+edi+esi], ecx                    ;332.6
        mov       esi, DWORD PTR [edx]                          ;330.6
        lea       esi, DWORD PTR [esi+esi*4]                    ;330.6
        mov       edi, DWORD PTR [32+ebx+esi*8]                 ;330.6
        neg       edi                                           ;330.6
        add       edi, ebp                                      ;330.6
        imul      edi, DWORD PTR [28+ebx+esi*8]                 ;330.6
        mov       esi, DWORD PTR [ebx+esi*8]                    ;330.6
        mov       DWORD PTR [esi+edi], ecx                      ;330.6
        mov       edi, DWORD PTR [edx]                          ;331.6
        lea       edi, DWORD PTR [edi+edi*4]                    ;331.6
        mov       esi, DWORD PTR [32+ebx+edi*8]                 ;331.6
        neg       esi                                           ;331.6
        add       esi, ebp                                      ;331.6
        imul      esi, DWORD PTR [28+ebx+edi*8]                 ;331.6
        mov       edi, DWORD PTR [ebx+edi*8]                    ;331.6
        mov       DWORD PTR [4+edi+esi], ecx                    ;331.6
        mov       esi, DWORD PTR [edx]                          ;332.6
        lea       esi, DWORD PTR [esi+esi*4]                    ;332.6
        sub       ebp, DWORD PTR [32+ebx+esi*8]                 ;332.6
        imul      ebp, DWORD PTR [28+ebx+esi*8]                 ;332.6
        mov       edi, DWORD PTR [ebx+esi*8]                    ;332.6
        cmp       eax, DWORD PTR [20+esp]                       ;329.4
        mov       DWORD PTR [8+edi+ebp], ecx                    ;332.6
        jae       .B12.5        ; Prob 37%                      ;329.4
                                ; LOE eax edx ecx ebx
.B12.12:                        ; Preds .B12.4
        mov       ebp, DWORD PTR [edx]                          ;329.19
        jmp       .B12.4        ; Prob 100%                     ;329.19
                                ; LOE eax edx ecx ebx ebp
.B12.5:                         ; Preds .B12.4
        mov       ebx, DWORD PTR [8+esp]                        ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;329.4
        mov       esi, DWORD PTR [16+esp]                       ;
        mov       ecx, DWORD PTR [4+esp]                        ;
        mov       ebp, DWORD PTR [esp]                          ;
                                ; LOE eax edx ecx ebx ebp esi
.B12.6:                         ; Preds .B12.5 .B12.9
        lea       edi, DWORD PTR [-1+eax]                       ;329.4
        cmp       ebx, edi                                      ;329.4
        jbe       .B12.8        ; Prob 10%                      ;329.4
                                ; LOE eax edx ecx ebp esi
.B12.7:                         ; Preds .B12.6
        mov       ebx, DWORD PTR [edx]                          ;330.6
        sub       ebp, ecx                                      ;330.6
        xor       ecx, ecx                                      ;330.6
        lea       eax, DWORD PTR [-1+eax+esi]                   ;330.6
        lea       edi, DWORD PTR [ebx+ebx*4]                    ;330.6
        mov       ebx, DWORD PTR [32+ebp+edi*8]                 ;330.6
        neg       ebx                                           ;330.6
        add       ebx, eax                                      ;330.6
        imul      ebx, DWORD PTR [28+ebp+edi*8]                 ;330.6
        mov       esi, DWORD PTR [ebp+edi*8]                    ;330.6
        mov       DWORD PTR [esi+ebx], ecx                      ;330.6
        mov       esi, DWORD PTR [edx]                          ;331.6
        lea       edi, DWORD PTR [esi+esi*4]                    ;331.6
        mov       ebx, DWORD PTR [32+ebp+edi*8]                 ;331.6
        neg       ebx                                           ;331.6
        add       ebx, eax                                      ;331.6
        imul      ebx, DWORD PTR [28+ebp+edi*8]                 ;331.6
        mov       edi, DWORD PTR [ebp+edi*8]                    ;331.6
        mov       DWORD PTR [4+edi+ebx], ecx                    ;331.6
        mov       edx, DWORD PTR [edx]                          ;332.6
        lea       ebx, DWORD PTR [edx+edx*4]                    ;332.6
        mov       edx, DWORD PTR [32+ebp+ebx*8]                 ;332.6
        sub       eax, edx                                      ;332.6
        imul      eax, DWORD PTR [28+ebp+ebx*8]                 ;332.6
        mov       ebp, DWORD PTR [ebp+ebx*8]                    ;332.6
        mov       DWORD PTR [8+ebp+eax], ecx                    ;332.6
                                ; LOE
.B12.8:                         ; Preds .B12.6 .B12.1 .B12.7
        add       esp, 24                                       ;335.1
        pop       ebp                                           ;335.1
        pop       ebx                                           ;335.1
        pop       edi                                           ;335.1
        pop       esi                                           ;335.1
        ret                                                     ;335.1
                                ; LOE
.B12.9:                         ; Preds .B12.2                  ; Infreq
        mov       eax, 1                                        ;
        jmp       .B12.6        ; Prob 100%                     ;
        ALIGN     16
                                ; LOE eax edx ecx ebx ebp esi
; mark_end;
_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_CLEAR ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_TRANSLINK_MODULE_mp_TRANLINK_CLEAR
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_TRANSLINK_MODULE_mp_TRANLINK_2DREMOVE
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_TRANSLINK_MODULE_mp_TRANLINK_2DREMOVE
_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_2DREMOVE	PROC NEAR 
.B13.1:                         ; Preds .B13.0
        push      ebp                                           ;338.12
        mov       ebp, esp                                      ;338.12
        and       esp, -16                                      ;338.12
        push      esi                                           ;338.12
        sub       esp, 92                                       ;338.12
        mov       edx, DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAYSIZE] ;342.3
        test      edx, edx                                      ;342.3
        jle       .B13.9        ; Prob 2%                       ;342.3
                                ; LOE edx ebx edi
.B13.2:                         ; Preds .B13.1
        imul      esi, DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY+32], -40 ;
        mov       eax, 1                                        ;
        add       esi, DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY] ;
        mov       DWORD PTR [8+esp], edx                        ;
        mov       DWORD PTR [4+esp], edi                        ;
        mov       DWORD PTR [esp], ebx                          ;
        mov       ebx, eax                                      ;
                                ; LOE ebx esi
.B13.3:                         ; Preds .B13.7 .B13.2
        lea       edi, DWORD PTR [ebx+ebx*4]                    ;343.10
        movsx     edx, WORD PTR [36+esi+edi*8]                  ;343.10
        test      edx, edx                                      ;343.10
        jle       .B13.7        ; Prob 79%                      ;343.10
                                ; LOE ebx esi edi
.B13.4:                         ; Preds .B13.3
        mov       edx, DWORD PTR [12+esi+edi*8]                 ;343.10
        mov       ecx, edx                                      ;343.10
        shr       ecx, 1                                        ;343.10
        and       edx, 1                                        ;343.10
        and       ecx, 1                                        ;343.10
        add       edx, edx                                      ;343.10
        shl       ecx, 2                                        ;343.10
        or        ecx, 1                                        ;343.10
        or        ecx, edx                                      ;343.10
        or        ecx, 262144                                   ;343.10
        push      ecx                                           ;343.10
        push      DWORD PTR [esi+edi*8]                         ;343.10
        call      _for_dealloc_allocatable                      ;343.10
                                ; LOE eax ebx esi edi
.B13.20:                        ; Preds .B13.4
        add       esp, 8                                        ;343.10
                                ; LOE eax ebx esi edi
.B13.5:                         ; Preds .B13.20
        and       DWORD PTR [12+esi+edi*8], -2                  ;343.10
        mov       DWORD PTR [esi+edi*8], 0                      ;343.10
        test      eax, eax                                      ;343.10
        mov       DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_VECTORERROR], eax ;343.10
        jne       .B13.15       ; Prob 5%                       ;343.10
                                ; LOE ebx esi edi
.B13.6:                         ; Preds .B13.5 .B13.23
        xor       edx, edx                                      ;343.10
        mov       WORD PTR [36+esi+edi*8], dx                   ;343.10
        mov       WORD PTR [38+esi+edi*8], dx                   ;343.10
                                ; LOE ebx esi
.B13.7:                         ; Preds .B13.3 .B13.6
        inc       ebx                                           ;344.3
        cmp       ebx, DWORD PTR [8+esp]                        ;344.3
        jle       .B13.3        ; Prob 82%                      ;344.3
                                ; LOE ebx esi
.B13.8:                         ; Preds .B13.7
        mov       edi, DWORD PTR [4+esp]                        ;
        mov       ebx, DWORD PTR [esp]                          ;
                                ; LOE ebx edi
.B13.9:                         ; Preds .B13.8 .B13.1
        mov       esi, DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY+12] ;347.3
        test      esi, 1                                        ;347.7
        je        .B13.12       ; Prob 60%                      ;347.7
                                ; LOE ebx esi edi
.B13.10:                        ; Preds .B13.9
        mov       edx, esi                                      ;348.5
        mov       eax, esi                                      ;348.5
        shr       edx, 1                                        ;348.5
        and       eax, 1                                        ;348.5
        and       edx, 1                                        ;348.5
        add       eax, eax                                      ;348.5
        shl       edx, 2                                        ;348.5
        or        edx, 1                                        ;348.5
        or        edx, eax                                      ;348.5
        or        edx, 262144                                   ;348.5
        push      edx                                           ;348.5
        push      DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY] ;348.5
        call      _for_dealloc_allocatable                      ;348.5
                                ; LOE eax ebx esi edi
.B13.21:                        ; Preds .B13.10
        add       esp, 8                                        ;348.5
                                ; LOE eax ebx esi edi
.B13.11:                        ; Preds .B13.21
        and       esi, -2                                       ;348.5
        mov       DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY], 0 ;348.5
        test      eax, eax                                      ;349.17
        mov       DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY+12], esi ;348.5
        mov       DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_VECTORERROR], eax ;348.5
        jne       .B13.13       ; Prob 5%                       ;349.17
                                ; LOE ebx edi
.B13.12:                        ; Preds .B13.22 .B13.11 .B13.9
        mov       DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAYSIZE], 0 ;354.3
        add       esp, 92                                       ;356.1
        pop       esi                                           ;356.1
        mov       esp, ebp                                      ;356.1
        pop       ebp                                           ;356.1
        ret                                                     ;356.1
                                ; LOE
.B13.13:                        ; Preds .B13.11                 ; Infreq
        mov       DWORD PTR [esp], 0                            ;350.5
        lea       edx, DWORD PTR [esp]                          ;350.5
        mov       DWORD PTR [32+esp], 16                        ;350.5
        lea       eax, DWORD PTR [32+esp]                       ;350.5
        mov       DWORD PTR [36+esp], OFFSET FLAT: __STRLITPACK_0 ;350.5
        push      32                                            ;350.5
        push      eax                                           ;350.5
        push      OFFSET FLAT: __STRLITPACK_55.0.13             ;350.5
        push      -2088435968                                   ;350.5
        push      911                                           ;350.5
        push      edx                                           ;350.5
        call      _for_write_seq_lis                            ;350.5
                                ; LOE ebx edi
.B13.14:                        ; Preds .B13.13                 ; Infreq
        push      32                                            ;351.5
        xor       eax, eax                                      ;351.5
        push      eax                                           ;351.5
        push      eax                                           ;351.5
        push      -2088435968                                   ;351.5
        push      eax                                           ;351.5
        push      OFFSET FLAT: __STRLITPACK_56                  ;351.5
        call      _for_stop_core                                ;351.5
                                ; LOE ebx edi
.B13.22:                        ; Preds .B13.14                 ; Infreq
        add       esp, 48                                       ;351.5
        jmp       .B13.12       ; Prob 100%                     ;351.5
                                ; LOE ebx edi
.B13.15:                        ; Preds .B13.5                  ; Infreq
        mov       DWORD PTR [48+esp], 0                         ;343.10
        lea       ecx, DWORD PTR [48+esp]                       ;343.10
        mov       DWORD PTR [40+esp], 37                        ;343.10
        lea       edx, DWORD PTR [40+esp]                       ;343.10
        mov       DWORD PTR [44+esp], OFFSET FLAT: __STRLITPACK_2 ;343.10
        push      32                                            ;343.10
        push      edx                                           ;343.10
        push      OFFSET FLAT: __STRLITPACK_52.0.11             ;343.10
        push      -2088435968                                   ;343.10
        push      911                                           ;343.10
        push      ecx                                           ;343.10
        call      _for_write_seq_lis                            ;343.10
                                ; LOE ebx esi edi
.B13.16:                        ; Preds .B13.15                 ; Infreq
        mov       DWORD PTR [72+esp], 0                         ;343.10
        lea       edx, DWORD PTR [104+esp]                      ;343.10
        mov       DWORD PTR [104+esp], ebx                      ;343.10
        push      32                                            ;343.10
        push      edx                                           ;343.10
        push      OFFSET FLAT: __STRLITPACK_53.0.11             ;343.10
        push      -2088435968                                   ;343.10
        push      911                                           ;343.10
        lea       ecx, DWORD PTR [92+esp]                       ;343.10
        push      ecx                                           ;343.10
        call      _for_write_seq_lis                            ;343.10
                                ; LOE ebx esi edi
.B13.17:                        ; Preds .B13.16                 ; Infreq
        push      32                                            ;343.10
        xor       edx, edx                                      ;343.10
        push      edx                                           ;343.10
        push      edx                                           ;343.10
        push      -2088435968                                   ;343.10
        push      edx                                           ;343.10
        push      OFFSET FLAT: __STRLITPACK_54                  ;343.10
        call      _for_stop_core                                ;343.10
                                ; LOE ebx esi edi
.B13.23:                        ; Preds .B13.17                 ; Infreq
        add       esp, 72                                       ;343.10
        jmp       .B13.6        ; Prob 100%                     ;343.10
        ALIGN     16
                                ; LOE ebx esi edi
; mark_end;
_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_2DREMOVE ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_TRANSLINK_MODULE_mp_TRANLINK_2DREMOVE
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
	PUBLIC _DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY
_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY	DD	0
	DD	0
	DD	0
	DD	128
	DD	1
	DD	0
	DD	0
	DD	0
	DD	0
_DATA	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_2	DB	68
	DB	69
	DB	65
	DB	76
	DB	76
	DB	79
	DB	67
	DB	65
	DB	84
	DB	69
	DB	32
	DB	84
	DB	114
	DB	97
	DB	110
	DB	76
	DB	105
	DB	110
	DB	107
	DB	95
	DB	65
	DB	114
	DB	114
	DB	97
	DB	121
	DB	32
	DB	118
	DB	101
	DB	99
	DB	116
	DB	111
	DB	114
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_52.0.11	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_53.0.11	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_54	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_0	DB	84
	DB	114
	DB	97
	DB	110
	DB	76
	DB	105
	DB	110
	DB	107
	DB	32
	DB	68
	DB	101
	DB	115
	DB	116
	DB	111
	DB	114
	DB	121
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_55.0.13	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_56	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_26	DB	84
	DB	114
	DB	97
	DB	110
	DB	76
	DB	105
	DB	110
	DB	107
	DB	95
	DB	65
	DB	114
	DB	114
	DB	97
	DB	121
	DB	32
	DB	83
	DB	101
	DB	116
	DB	117
	DB	112
	DB	32
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_29	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_18	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	97
	DB	108
	DB	108
	DB	111
	DB	99
	DB	97
	DB	116
	DB	105
	DB	110
	DB	103
	DB	32
	DB	84
	DB	114
	DB	97
	DB	110
	DB	76
	DB	105
	DB	110
	DB	107
	DB	95
	DB	65
	DB	114
	DB	114
	DB	97
	DB	121
	DB	32
	DB	118
	DB	101
	DB	99
	DB	116
	DB	111
	DB	114
	DB	32
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	0
__STRLITPACK_37	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_24	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	97
	DB	108
	DB	108
	DB	111
	DB	99
	DB	97
	DB	116
	DB	105
	DB	110
	DB	103
	DB	32
	DB	116
	DB	109
	DB	112
	DB	80
	DB	32
	DB	118
	DB	101
	DB	99
	DB	116
	DB	111
	DB	114
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	84
	DB	114
	DB	97
	DB	110
	DB	76
	DB	105
	DB	110
	DB	107
	DB	95
	DB	83
	DB	101
	DB	116
	DB	117
	DB	112
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_31	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_22	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	100
	DB	101
	DB	97
	DB	108
	DB	108
	DB	111
	DB	99
	DB	97
	DB	116
	DB	105
	DB	110
	DB	103
	DB	32
	DB	84
	DB	114
	DB	97
	DB	110
	DB	76
	DB	105
	DB	110
	DB	107
	DB	95
	DB	65
	DB	114
	DB	114
	DB	97
	DB	121
	DB	32
	DB	118
	DB	101
	DB	99
	DB	116
	DB	111
	DB	114
	DB	32
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_33	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_20	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	97
	DB	108
	DB	108
	DB	111
	DB	99
	DB	97
	DB	116
	DB	105
	DB	110
	DB	103
	DB	32
	DB	84
	DB	114
	DB	97
	DB	110
	DB	76
	DB	105
	DB	110
	DB	107
	DB	95
	DB	65
	DB	114
	DB	114
	DB	97
	DB	121
	DB	32
	DB	118
	DB	101
	DB	99
	DB	116
	DB	111
	DB	114
	DB	32
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	0
__STRLITPACK_35	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_16	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	84
	DB	114
	DB	97
	DB	110
	DB	76
	DB	105
	DB	110
	DB	107
	DB	95
	DB	69
	DB	114
	DB	97
	DB	115
	DB	101
	DB	0
__STRLITPACK_39	DB	0
	DB 3 DUP ( 0H)	; pad
_2il0floatpacket.18	DD	0447a0000H
__STRLITPACK_14	DB	84
	DB	114
	DB	97
	DB	110
	DB	76
	DB	105
	DB	110
	DB	107
	DB	32
	DB	71
	DB	101
	DB	116
	DB	86
	DB	97
	DB	108
	DB	117
	DB	101
	DB	32
	DB	118
	DB	101
	DB	99
	DB	116
	DB	111
	DB	114
	DB	32
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_12	DB	73
	DB	110
	DB	100
	DB	101
	DB	120
	DB	49
	DB	68
	DB	32
	DB	61
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_10	DB	84
	DB	114
	DB	97
	DB	110
	DB	76
	DB	105
	DB	110
	DB	107
	DB	95
	DB	65
	DB	114
	DB	114
	DB	97
	DB	121
	DB	40
	DB	105
	DB	116
	DB	41
	DB	37
	DB	80
	DB	83
	DB	105
	DB	122
	DB	101
	DB	32
	DB	61
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_45	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_8	DB	103
	DB	101
	DB	116
	DB	32
	DB	84
	DB	114
	DB	97
	DB	110
	DB	76
	DB	105
	DB	110
	DB	107
	DB	95
	DB	118
	DB	97
	DB	108
	DB	117
	DB	101
	DB	32
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_47	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_6	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	84
	DB	114
	DB	97
	DB	110
	DB	115
	DB	76
	DB	105
	DB	110
	DB	107
	DB	95
	DB	86
	DB	97
	DB	108
	DB	117
	DB	101
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_4	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	84
	DB	114
	DB	97
	DB	110
	DB	76
	DB	105
	DB	110
	DB	107
	DB	95
	DB	86
	DB	97
	DB	108
	DB	117
	DB	101
	DB	0
__STRLITPACK_51	DB	0
	DB 3 DUP ( 0H)	; pad
_2il0floatpacket.22	DD	0447a0000H
_2il0floatpacket.23	DD	080000000H
_2il0floatpacket.24	DD	04b000000H
_2il0floatpacket.25	DD	03f000000H
_2il0floatpacket.26	DD	0bf000000H
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
	COMM _DYNUST_TRANSLINK_MODULE_mp_VECTORERROR:BYTE:4
	COMM _DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAYSIZE:BYTE:4
_DATA	ENDS
EXTRN	_for_write_seq_lis_xmit:PROC
EXTRN	_for_dealloc_allocatable:PROC
EXTRN	_for_allocate:PROC
EXTRN	_for_stop_core:PROC
EXTRN	_for_write_seq_lis:PROC
EXTRN	_for_alloc_allocatable:PROC
EXTRN	_for_check_mult_overflow:PROC
EXTRN	__fltused:BYTE
	INCLUDELIB <ifconsol>
	INCLUDELIB <libifcoremt>
	INCLUDELIB <libifport>
	INCLUDELIB <libmmt>
	INCLUDELIB <LIBCMT>
	INCLUDELIB <libirc>
	INCLUDELIB <svml_dispmt>
	INCLUDELIB <OLDNAMES>
	END
