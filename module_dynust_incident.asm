; mark_description "Intel(R) Visual Fortran Compiler XE for applications running on IA-32, Version 13.1.1.171 Build 20130313";
; mark_description "-S";
	.686P
 	.387
	OPTION DOTNAME
	ASSUME	CS:FLAT,DS:FLAT,SS:FLAT
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
TXTST0:
; -- Begin  _INCIDENT_MODULE$
; mark_begin;
IF @Version GE 800
  .MMX
ELSEIF @Version GE 612
  .MMX
  MMWORD TEXTEQU <QWORD>
ENDIF
IF @Version GE 800
  .XMM
ELSEIF @Version GE 614
  .XMM
  XMMWORD TEXTEQU <OWORD>
ENDIF
       ALIGN     16
	PUBLIC _INCIDENT_MODULE$
_INCIDENT_MODULE$	PROC NEAR 
.B1.1:                          ; Preds .B1.0
        ret                                                     ;1.8
        ALIGN     16
                                ; LOE
; mark_end;
_INCIDENT_MODULE$ ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _INCIDENT_MODULE$
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _INCIDENT_MODULE_mp_INCI_ACTIVATE
; mark_begin;
       ALIGN     16
	PUBLIC _INCIDENT_MODULE_mp_INCI_ACTIVATE
_INCIDENT_MODULE_mp_INCI_ACTIVATE	PROC NEAR 
; parameter 1: 16 + esp
; parameter 2: 20 + esp
.B2.1:                          ; Preds .B2.0
        push      esi                                           ;25.12
        push      ebx                                           ;25.12
        push      ebp                                           ;25.12
        mov       ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_INCI+44] ;28.11
        mov       eax, DWORD PTR [16+esp]                       ;25.12
        neg       ebx                                           ;28.6
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_INCI+32] ;28.6
        add       ebx, 3                                        ;28.6
        shl       edx, 2                                        ;28.6
        neg       edx                                           ;28.6
        imul      ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_INCI+40] ;28.6
        add       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_INCI]  ;28.6
        mov       ebp, DWORD PTR [eax]                          ;28.11
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_INCIL+32] ;29.6
        shl       eax, 2                                        ;29.6
        neg       eax                                           ;29.6
        lea       ecx, DWORD PTR [edx+ebp*4]                    ;28.6
        movss     xmm1, DWORD PTR [ecx+ebx]                     ;28.6
        mov       ebx, DWORD PTR [ecx+ebx]                      ;28.6
        imul      ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32], -900 ;
        add       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_INCIL] ;29.6
        add       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;
        mov       eax, DWORD PTR [eax+ebp*4]                    ;29.6
        imul      edx, eax, 900                                 ;29.6
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_INCCLSFLAG], 0 ;32.20
        jne       .B2.6         ; Prob 50%                      ;32.20
                                ; LOE eax edx ecx ebx ebp edi xmm1
.B2.2:                          ; Preds .B2.1
        movsx     esi, BYTE PTR [668+edx+ecx]                   ;33.42
        cvtsi2ss  xmm0, esi                                     ;33.42
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;33.42
        neg       esi                                           ;33.8
        add       esi, eax                                      ;33.8
        imul      esi, esi, 152                                 ;33.8
        movss     xmm2, DWORD PTR [_2il0floatpacket.3]          ;33.8
        comiss    xmm1, DWORD PTR [_2il0floatpacket.2]          ;34.16
        subss     xmm2, xmm1                                    ;33.116
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;33.42
        mulss     xmm0, DWORD PTR [20+eax+esi]                  ;33.79
        mulss     xmm0, xmm2                                    ;33.8
        movss     DWORD PTR [672+edx+ecx], xmm0                 ;33.8
        jbe       .B2.4         ; Prob 50%                      ;34.16
                                ; LOE edx ecx ebx ebp edi xmm2
.B2.3:                          ; Preds .B2.2
        mov       DWORD PTR [432+edx+ecx], 0                    ;35.10
        jmp       .B2.6         ; Prob 100%                     ;35.10
                                ; LOE edx ecx ebx edi
.B2.4:                          ; Preds .B2.2
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_INCISTARTFLAG+32] ;37.9
        shl       eax, 2                                        ;37.12
        neg       eax                                           ;37.12
        add       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_INCISTARTFLAG] ;37.12
        test      BYTE PTR [eax+ebp*4], 1                       ;37.17
        jne       .B2.6         ; Prob 40%                      ;37.17
                                ; LOE edx ecx ebx edi xmm2
.B2.5:                          ; Preds .B2.4
        movss     xmm0, DWORD PTR [432+edx+ecx]                 ;37.79
        mulss     xmm0, xmm2                                    ;37.36
        movss     DWORD PTR [432+edx+ecx], xmm0                 ;37.36
                                ; LOE edx ecx ebx edi
.B2.6:                          ; Preds .B2.1 .B2.3 .B2.5 .B2.4
        mov       BYTE PTR [12+edx+ecx], 2                      ;40.6
        mov       DWORD PTR [16+edx+ecx], ebx                   ;41.6
        pop       ebp                                           ;42.1
        pop       ebx                                           ;42.1
        pop       esi                                           ;42.1
        ret                                                     ;42.1
        ALIGN     16
                                ; LOE
; mark_end;
_INCIDENT_MODULE_mp_INCI_ACTIVATE ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _INCIDENT_MODULE_mp_INCI_ACTIVATE
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _INCIDENT_MODULE_mp_INCI_ADD
; mark_begin;
       ALIGN     16
	PUBLIC _INCIDENT_MODULE_mp_INCI_ADD
_INCIDENT_MODULE_mp_INCI_ADD	PROC NEAR 
; parameter 1: 4 + esp
.B3.1:                          ; Preds .B3.0
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_LISTTOTAL] ;49.7
        inc       edx                                           ;49.7
        mov       ecx, DWORD PTR [4+esp]                        ;46.12
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_INCILIST+32] ;50.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_LISTTOTAL], edx ;49.7
        sub       edx, eax                                      ;50.7
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_INCILIST] ;50.7
        mov       ecx, DWORD PTR [ecx]                          ;50.7
        mov       DWORD PTR [eax+edx*4], ecx                    ;50.7
        ret                                                     ;53.1
        ALIGN     16
                                ; LOE
; mark_end;
_INCIDENT_MODULE_mp_INCI_ADD ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _INCIDENT_MODULE_mp_INCI_ADD
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _INCIDENT_MODULE_mp_INCI_CAPACITY_RESTORE
; mark_begin;
       ALIGN     16
	PUBLIC _INCIDENT_MODULE_mp_INCI_CAPACITY_RESTORE
_INCIDENT_MODULE_mp_INCI_CAPACITY_RESTORE	PROC NEAR 
; parameter 1: 48 + esp
; parameter 2: 52 + esp
.B4.1:                          ; Preds .B4.0
        push      esi                                           ;56.12
        push      edi                                           ;56.12
        push      ebx                                           ;56.12
        push      ebp                                           ;56.12
        sub       esp, 28                                       ;56.12
        mov       edx, DWORD PTR [48+esp]                       ;56.12
        imul      ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32], 900 ;57.7
        imul      ebp, DWORD PTR [edx], 900                     ;57.7
        imul      edx, DWORD PTR [edx], 152                     ;58.4
        neg       ebx                                           ;57.7
        add       ebp, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;57.7
        add       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;58.4
        mov       eax, DWORD PTR [436+ebx+ebp]                  ;57.7
        mov       DWORD PTR [432+ebx+ebp], eax                  ;57.7
        imul      eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32], 152 ;58.4
        movsx     ecx, BYTE PTR [668+ebx+ebp]                   ;58.38
        neg       eax                                           ;58.4
        cvtsi2ss  xmm0, ecx                                     ;58.38
        mulss     xmm0, DWORD PTR [20+eax+edx]                  ;58.4
        mov       edx, DWORD PTR [12+eax+edx]                   ;61.7
        movss     DWORD PTR [672+ebx+ebp], xmm0                 ;58.4
        mov       BYTE PTR [12+ebx+ebp], 3                      ;59.4
        mov       DWORD PTR [16+ebx+ebp], 0                     ;60.4
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ITERATION], 0 ;62.17
        jne       .B4.9         ; Prob 50%                      ;62.17
                                ; LOE edx
.B4.2:                          ; Preds .B4.1
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_COST+44] ;62.23
        mov       DWORD PTR [24+esp], eax                       ;62.23
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_COST+36] ;62.23
        test      eax, eax                                      ;62.23
        jle       .B4.9         ; Prob 10%                      ;62.23
                                ; LOE eax edx
.B4.3:                          ; Preds .B4.2
        mov       ebx, eax                                      ;62.23
        shr       ebx, 31                                       ;62.23
        add       ebx, eax                                      ;62.23
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_COST]  ;62.23
        sar       ebx, 1                                        ;62.23
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_COST+32] ;62.23
        test      ebx, ebx                                      ;62.23
        mov       DWORD PTR [8+esp], esi                        ;62.23
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_COST+40] ;62.23
        mov       DWORD PTR [12+esp], ecx                       ;62.23
        mov       DWORD PTR [16+esp], ebx                       ;62.23
        jbe       .B4.10        ; Prob 0%                       ;62.23
                                ; LOE eax edx ecx esi cl ch
.B4.4:                          ; Preds .B4.3
        mov       DWORD PTR [4+esp], eax                        ;
        xor       ebx, ebx                                      ;
        mov       ebp, DWORD PTR [8+esp]                        ;
        mov       eax, DWORD PTR [24+esp]                       ;
        imul      eax, esi                                      ;
        lea       ebp, DWORD PTR [ebp+edx*4]                    ;
        mov       edi, ebp                                      ;
        sub       edi, eax                                      ;
        add       eax, esi                                      ;
        add       edi, eax                                      ;
        shl       ecx, 2                                        ;
        sub       edi, ecx                                      ;
        sub       ebp, ecx                                      ;
        mov       DWORD PTR [20+esp], edi                       ;
        xor       ecx, ecx                                      ;
        xor       edi, edi                                      ;
        mov       DWORD PTR [esp], edx                          ;
        mov       eax, DWORD PTR [20+esp]                       ;
        mov       edx, DWORD PTR [16+esp]                       ;
                                ; LOE eax edx ecx ebx ebp esi edi
.B4.5:                          ; Preds .B4.5 .B4.4
        inc       ebx                                           ;62.23
        mov       DWORD PTR [ebp+ecx*2], edi                    ;62.23
        mov       DWORD PTR [eax+ecx*2], edi                    ;62.23
        add       ecx, esi                                      ;62.23
        cmp       ebx, edx                                      ;62.23
        jb        .B4.5         ; Prob 63%                      ;62.23
                                ; LOE eax edx ecx ebx ebp esi edi
.B4.6:                          ; Preds .B4.5
        mov       eax, DWORD PTR [4+esp]                        ;
        lea       ebp, DWORD PTR [1+ebx+ebx]                    ;62.23
        mov       edx, DWORD PTR [esp]                          ;
                                ; LOE eax edx ebp esi
.B4.7:                          ; Preds .B4.6 .B4.10
        lea       ecx, DWORD PTR [-1+ebp]                       ;62.23
        cmp       eax, ecx                                      ;62.23
        jbe       .B4.9         ; Prob 0%                       ;62.23
                                ; LOE edx ebp esi
.B4.8:                          ; Preds .B4.7
        mov       eax, DWORD PTR [24+esp]                       ;62.23
        mov       ebx, eax                                      ;62.23
        neg       ebx                                           ;62.23
        sub       edx, DWORD PTR [12+esp]                       ;62.23
        lea       ecx, DWORD PTR [-1+ebp+eax]                   ;62.23
        add       ebx, ecx                                      ;62.23
        imul      ebx, esi                                      ;62.23
        add       ebx, DWORD PTR [8+esp]                        ;62.23
        mov       DWORD PTR [ebx+edx*4], 0                      ;62.23
                                ; LOE
.B4.9:                          ; Preds .B4.2 .B4.7 .B4.1 .B4.8
        add       esp, 28                                       ;63.1
        pop       ebp                                           ;63.1
        pop       ebx                                           ;63.1
        pop       edi                                           ;63.1
        pop       esi                                           ;63.1
        ret                                                     ;63.1
                                ; LOE
.B4.10:                         ; Preds .B4.3                   ; Infreq
        mov       ebp, 1                                        ;
        jmp       .B4.7         ; Prob 100%                     ;
        ALIGN     16
                                ; LOE eax edx ebp esi
; mark_end;
_INCIDENT_MODULE_mp_INCI_CAPACITY_RESTORE ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _INCIDENT_MODULE_mp_INCI_CAPACITY_RESTORE
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _INCIDENT_MODULE_mp_INCI_SCAN
; mark_begin;
       ALIGN     16
	PUBLIC _INCIDENT_MODULE_mp_INCI_SCAN
_INCIDENT_MODULE_mp_INCI_SCAN	PROC NEAR 
; parameter 1: 84 + esp
.B5.1:                          ; Preds .B5.0
        push      esi                                           ;66.12
        push      edi                                           ;66.12
        push      ebx                                           ;66.12
        push      ebp                                           ;66.12
        sub       esp, 64                                       ;66.12
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_INCI_NUM] ;68.7
        test      eax, eax                                      ;68.7
        mov       DWORD PTR [60+esp], eax                       ;68.7
        jle       .B5.26        ; Prob 2%                       ;68.7
                                ; LOE
.B5.2:                          ; Preds .B5.1
        mov       edx, DWORD PTR [84+esp]                       ;66.12
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_INCI+40] ;69.19
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_INCI+32] ;69.10
        movss     xmm0, DWORD PTR [edx]                         ;69.10
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_INCI+44] ;69.19
        mov       ebp, edx                                      ;
        imul      ebp, edi                                      ;
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_INCI]  ;69.10
        mov       DWORD PTR [4+esp], esi                        ;69.10
        sub       ecx, ebp                                      ;
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_INCCLSFLAG] ;70.18
        lea       ebp, DWORD PTR [esi*4]                        ;
        mov       esi, edi                                      ;
        sub       esi, ebp                                      ;
        add       esi, ecx                                      ;
        mov       DWORD PTR [44+esp], esi                       ;
        lea       esi, DWORD PTR [edi+edi]                      ;69.38
        sub       esi, ebp                                      ;
        lea       edi, DWORD PTR [edi+edi*2]                    ;70.18
        add       esi, ecx                                      ;
        sub       edi, ebp                                      ;
        mov       DWORD PTR [40+esp], esi                       ;
        add       ecx, edi                                      ;
        imul      esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32], -900 ;
        imul      edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32], -152 ;
        mov       DWORD PTR [28+esp], eax                       ;70.18
        mov       ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_INCIL+32] ;77.13
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_INCISTARTFLAG+32] ;76.6
        add       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;
        mov       DWORD PTR [36+esp], esi                       ;
        lea       ebp, DWORD PTR [ebx*4]                        ;
        neg       ebp                                           ;
        lea       esi, DWORD PTR [eax*4]                        ;
        neg       esi                                           ;
        add       ebp, DWORD PTR [_DYNUST_MAIN_MODULE_mp_INCIL] ;
        add       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_INCISTARTFLAG] ;
        mov       DWORD PTR [esp], 1                            ;
        add       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;
        mov       DWORD PTR [48+esp], ebx                       ;
        mov       DWORD PTR [24+esp], esi                       ;
        mov       DWORD PTR [16+esp], ebp                       ;
        mov       DWORD PTR [32+esp], edi                       ;
        mov       DWORD PTR [8+esp], eax                        ;
        mov       DWORD PTR [12+esp], edx                       ;
        mov       DWORD PTR [20+esp], ecx                       ;
        mov       ebx, DWORD PTR [40+esp]                       ;
        mov       ebp, DWORD PTR [44+esp]                       ;
        mov       esi, DWORD PTR [esp]                          ;
                                ; LOE ebx ebp esi xmm0
.B5.3:                          ; Preds .B5.11 .B5.2
        comiss    xmm0, DWORD PTR [ebp+esi*4]                   ;69.16
        jb        .B5.11        ; Prob 50%                      ;69.16
                                ; LOE ebx ebp esi xmm0
.B5.4:                          ; Preds .B5.3
        movss     xmm1, DWORD PTR [ebx+esi*4]                   ;69.38
        comiss    xmm1, xmm0                                    ;69.36
        jbe       .B5.11        ; Prob 78%                      ;69.36
                                ; LOE ebx ebp esi xmm0
.B5.5:                          ; Preds .B5.4
        mov       edx, DWORD PTR [16+esp]                       ;70.18
        mov       eax, DWORD PTR [20+esp]                       ;70.18
        mov       edx, DWORD PTR [edx+esi*4]                    ;70.18
        imul      ecx, edx, 900                                 ;70.18
        movss     xmm2, DWORD PTR [eax+esi*4]                   ;70.18
        mov       eax, DWORD PTR [eax+esi*4]                    ;70.18
        cmp       DWORD PTR [28+esp], 0                         ;70.18
        jne       .B5.10        ; Prob 50%                      ;70.18
                                ; LOE eax edx ecx ebx ebp esi xmm0 xmm2
.B5.6:                          ; Preds .B5.5
        mov       DWORD PTR [esp], eax                          ;
        mov       eax, DWORD PTR [36+esp]                       ;70.18
        movss     xmm3, DWORD PTR [_2il0floatpacket.17]         ;70.18
        imul      edx, edx, 152                                 ;70.18
        subss     xmm3, xmm2                                    ;70.18
        comiss    xmm2, DWORD PTR [_2il0floatpacket.16]         ;70.18
        movsx     edi, BYTE PTR [668+ecx+eax]                   ;70.18
        cvtsi2ss  xmm1, edi                                     ;70.18
        mulss     xmm1, xmm3                                    ;70.18
        mov       edi, DWORD PTR [32+esp]                       ;70.18
        mulss     xmm1, DWORD PTR [20+edx+edi]                  ;70.18
        movss     DWORD PTR [672+ecx+eax], xmm1                 ;70.18
        mov       eax, DWORD PTR [esp]                          ;70.18
        jbe       .B5.8         ; Prob 50%                      ;70.18
                                ; LOE eax ecx ebx ebp esi al ah xmm0 xmm3
.B5.7:                          ; Preds .B5.6
        mov       edx, DWORD PTR [36+esp]                       ;70.18
        mov       DWORD PTR [432+ecx+edx], 0                    ;70.18
        jmp       .B5.10        ; Prob 100%                     ;70.18
                                ; LOE eax ecx ebx ebp esi xmm0
.B5.8:                          ; Preds .B5.6
        mov       edx, DWORD PTR [24+esp]                       ;70.18
        test      BYTE PTR [edx+esi*4], 1                       ;70.18
        jne       .B5.10        ; Prob 40%                      ;70.18
                                ; LOE eax ecx ebx ebp esi al ah xmm0 xmm3
.B5.9:                          ; Preds .B5.8
        mov       edx, DWORD PTR [36+esp]                       ;70.18
        mulss     xmm3, DWORD PTR [432+ecx+edx]                 ;70.18
        movss     DWORD PTR [432+ecx+edx], xmm3                 ;70.18
                                ; LOE eax ecx ebx ebp esi xmm0
.B5.10:                         ; Preds .B5.5 .B5.7 .B5.9 .B5.8
        mov       edx, DWORD PTR [36+esp]                       ;70.18
        mov       DWORD PTR [16+ecx+edx], eax                   ;70.18
        mov       eax, DWORD PTR [24+esp]                       ;71.13
        mov       BYTE PTR [12+ecx+edx], 2                      ;70.18
        mov       DWORD PTR [eax+esi*4], -1                     ;71.13
                                ; LOE ebx ebp esi xmm0
.B5.11:                         ; Preds .B5.4 .B5.3 .B5.10
        inc       esi                                           ;73.7
        cmp       esi, DWORD PTR [60+esp]                       ;73.7
        jle       .B5.3         ; Prob 82%                      ;73.7
                                ; LOE ebx ebp esi xmm0
.B5.12:                         ; Preds .B5.11
        mov       ebx, DWORD PTR [48+esp]                       ;
        shl       ebx, 2                                        ;
        neg       ebx                                           ;
        mov       eax, DWORD PTR [8+esp]                        ;
        add       ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_INCIL] ;
        shl       eax, 2                                        ;
        mov       DWORD PTR [48+esp], ebx                       ;
        neg       eax                                           ;
        imul      ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32], -900 ;
        mov       edx, DWORD PTR [12+esp]                       ;
        neg       edx                                           ;
        add       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_INCISTARTFLAG] ;
        add       edx, 2                                        ;
        mov       DWORD PTR [8+esp], eax                        ;
        imul      eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32], -152 ;
        imul      edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_INCI+40] ;
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ITERATION] ;78.18
        mov       DWORD PTR [56+esp], edi                       ;78.18
        mov       edi, DWORD PTR [4+esp]                        ;
        shl       edi, 2                                        ;
        add       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;
        neg       edi                                           ;
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_COST+44] ;78.18
        mov       ebp, DWORD PTR [_DYNUST_MAIN_MODULE_mp_COST+36] ;78.18
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_COST+40] ;78.18
        mov       DWORD PTR [52+esp], ebx                       ;
        mov       ebx, ebp                                      ;
        imul      ecx, esi                                      ;
        add       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_INCI]  ;
        shr       ebx, 31                                       ;
        add       edx, edi                                      ;
        add       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;
        mov       edi, esi                                      ;
        add       ebx, ebp                                      ;
        neg       edi                                           ;
        mov       DWORD PTR [44+esp], eax                       ;
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_COST+32] ;
        shl       eax, 2                                        ;
        mov       DWORD PTR [32+esp], ebp                       ;78.18
        sub       edi, eax                                      ;
        sar       ebx, 1                                        ;
        mov       ebp, DWORD PTR [_DYNUST_MAIN_MODULE_mp_COST]  ;
        add       edi, ebp                                      ;
        mov       DWORD PTR [36+esp], ebx                       ;
        mov       ebx, ebp                                      ;
        mov       DWORD PTR [40+esp], esi                       ;78.18
        sub       esi, eax                                      ;
        sub       ebx, ecx                                      ;
        sub       ecx, eax                                      ;
        mov       DWORD PTR [16+esp], 0                         ;
        add       ebp, esi                                      ;
        add       ebx, ecx                                      ;
        mov       DWORD PTR [20+esp], ebx                       ;
        mov       DWORD PTR [28+esp], edi                       ;
        mov       eax, DWORD PTR [8+esp]                        ;
        mov       ebx, DWORD PTR [48+esp]                       ;
        mov       ecx, DWORD PTR [16+esp]                       ;
        mov       DWORD PTR [24+esp], ebp                       ;
                                ; LOE eax edx ecx xmm0
.B5.13:                         ; Preds .B5.24 .B5.12
        comiss    xmm0, DWORD PTR [4+edx+ecx*4]                 ;75.15
        jb        .B5.24        ; Prob 50%                      ;75.15
                                ; LOE eax edx ecx xmm0
.B5.14:                         ; Preds .B5.13
        test      BYTE PTR [4+eax+ecx*4], 1                     ;76.9
        je        .B5.24        ; Prob 60%                      ;76.9
                                ; LOE eax edx ecx xmm0
.B5.15:                         ; Preds .B5.14
        mov       ebx, DWORD PTR [48+esp]                       ;77.13
        mov       esi, DWORD PTR [52+esp]                       ;78.18
        imul      ebp, DWORD PTR [4+ebx+ecx*4], 900             ;77.13
        imul      ebx, DWORD PTR [4+ebx+ecx*4], 152             ;77.13
        mov       edi, DWORD PTR [436+ebp+esi]                  ;78.18
        mov       DWORD PTR [432+ebp+esi], edi                  ;78.18
        movsx     edi, BYTE PTR [668+ebp+esi]                   ;78.18
        cvtsi2ss  xmm1, edi                                     ;78.18
        mov       edi, DWORD PTR [44+esp]                       ;78.18
        mov       BYTE PTR [12+ebp+esi], 3                      ;78.18
        mov       DWORD PTR [16+ebp+esi], 0                     ;78.18
        mulss     xmm1, DWORD PTR [20+ebx+edi]                  ;78.18
        movss     DWORD PTR [672+ebp+esi], xmm1                 ;78.18
        mov       ebp, DWORD PTR [12+ebx+edi]                   ;78.18
        cmp       DWORD PTR [56+esp], 0                         ;78.18
        jne       .B5.23        ; Prob 50%                      ;78.18
                                ; LOE eax edx ecx ebp xmm0
.B5.16:                         ; Preds .B5.15
        cmp       DWORD PTR [32+esp], 0                         ;78.18
        jle       .B5.23        ; Prob 50%                      ;78.18
                                ; LOE eax edx ecx ebp xmm0
.B5.17:                         ; Preds .B5.16
        cmp       DWORD PTR [36+esp], 0                         ;78.18
        jbe       .B5.27        ; Prob 10%                      ;78.18
                                ; LOE eax edx ecx ebp xmm0
.B5.18:                         ; Preds .B5.17
        mov       edi, DWORD PTR [24+esp]                       ;
        xor       esi, esi                                      ;
        mov       ebx, DWORD PTR [20+esp]                       ;
        mov       DWORD PTR [esp], ebp                          ;
        lea       edi, DWORD PTR [edi+ebp*4]                    ;
        mov       DWORD PTR [4+esp], edi                        ;
        lea       ebx, DWORD PTR [ebx+ebp*4]                    ;
        mov       DWORD PTR [8+esp], eax                        ;
        xor       edi, edi                                      ;
        mov       DWORD PTR [12+esp], edx                       ;
        mov       DWORD PTR [16+esp], ecx                       ;
        mov       eax, esi                                      ;
        mov       edx, DWORD PTR [4+esp]                        ;
        mov       ecx, DWORD PTR [36+esp]                       ;
        mov       ebp, DWORD PTR [40+esp]                       ;
                                ; LOE eax edx ecx ebx ebp esi edi xmm0
.B5.19:                         ; Preds .B5.19 .B5.18
        inc       esi                                           ;78.18
        mov       DWORD PTR [ebx+eax*2], edi                    ;78.18
        mov       DWORD PTR [edx+eax*2], edi                    ;78.18
        add       eax, ebp                                      ;78.18
        cmp       esi, ecx                                      ;78.18
        jb        .B5.19        ; Prob 63%                      ;78.18
                                ; LOE eax edx ecx ebx ebp esi edi xmm0
.B5.20:                         ; Preds .B5.19
        mov       ebp, DWORD PTR [esp]                          ;
        lea       ebx, DWORD PTR [1+esi+esi]                    ;78.18
        mov       eax, DWORD PTR [8+esp]                        ;
        mov       edx, DWORD PTR [12+esp]                       ;
        mov       ecx, DWORD PTR [16+esp]                       ;
                                ; LOE eax edx ecx ebx ebp xmm0
.B5.21:                         ; Preds .B5.20 .B5.27
        lea       esi, DWORD PTR [-1+ebx]                       ;78.18
        cmp       esi, DWORD PTR [32+esp]                       ;78.18
        jae       .B5.23        ; Prob 10%                      ;78.18
                                ; LOE eax edx ecx ebx ebp xmm0
.B5.22:                         ; Preds .B5.21
        imul      ebx, DWORD PTR [40+esp]                       ;78.18
        mov       esi, DWORD PTR [28+esp]                       ;78.18
        lea       ebp, DWORD PTR [esi+ebp*4]                    ;78.18
        mov       DWORD PTR [ebx+ebp], 0                        ;78.18
                                ; LOE eax edx ecx xmm0
.B5.23:                         ; Preds .B5.21 .B5.16 .B5.15 .B5.22
        mov       DWORD PTR [4+eax+ecx*4], 0                    ;79.10
                                ; LOE eax edx ecx xmm0
.B5.24:                         ; Preds .B5.14 .B5.13 .B5.23
        inc       ecx                                           ;74.7
        cmp       ecx, DWORD PTR [60+esp]                       ;74.7
        jb        .B5.13        ; Prob 82%                      ;74.7
                                ; LOE eax edx ecx xmm0
.B5.26:                         ; Preds .B5.24 .B5.1
        add       esp, 64                                       ;83.1
        pop       ebp                                           ;83.1
        pop       ebx                                           ;83.1
        pop       edi                                           ;83.1
        pop       esi                                           ;83.1
        ret                                                     ;83.1
                                ; LOE
.B5.27:                         ; Preds .B5.17                  ; Infreq
        mov       ebx, 1                                        ;
        jmp       .B5.21        ; Prob 100%                     ;
        ALIGN     16
                                ; LOE eax edx ecx ebx ebp xmm0
; mark_end;
_INCIDENT_MODULE_mp_INCI_SCAN ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _INCIDENT_MODULE_mp_INCI_SCAN
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
_2il0floatpacket.2	DD	03f7fbe77H
_2il0floatpacket.3	DD	03f800000H
_2il0floatpacket.16	DD	03f7fbe77H
_2il0floatpacket.17	DD	03f800000H
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
EXTRN	_DYNUST_MAIN_MODULE_mp_INCI_NUM:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_COST:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ITERATION:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_INCILIST:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_LISTTOTAL:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_INCISTARTFLAG:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_INCCLSFLAG:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_INCIL:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_INCI:BYTE
_DATA	ENDS
EXTRN	__fltused:BYTE
	INCLUDELIB <ifconsol>
	INCLUDELIB <libifcoremt>
	INCLUDELIB <libifport>
	INCLUDELIB <libmmt>
	INCLUDELIB <LIBCMT>
	INCLUDELIB <libirc>
	INCLUDELIB <svml_dispmt>
	INCLUDELIB <OLDNAMES>
	END
