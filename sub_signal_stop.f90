      SUBROUTINE SIGNAL_STOP(IntoOutNodeNumber)
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~ Copyright (C) 2002-2011 The Arizona Board of Regents on Behalf of The University of Arizona.  ~~!
!~~ Developed by Yi-Chang Chiu, Ph.D. as par to the DynusT project.                               ~~!
!~~ This program is free software: you can redistribute it and/or modify it                       ~~!
!~~ under the terms of the GNU General Public License version 2 as published by                   ~~!
!~~ the Free Software Foundation.                                                                 ~~!
!~~ This program is distributed in the hope that it will be useful,                               ~~!
!~~ but WITHOUT ANY WARRANTY; without even the implied warranty of                                ~~!
!~~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                                          ~~!
!~~ See the GNU General Public License for more details.                                          ~~!
!~~ You should have received a copy of the GNU General Public License                             ~~!
!~~ along with this program (in the file COPYING).                                                ~~!
!~~ IF not, see <http://www.gnu.org/licenses/>.                                                   ~~!
!~~ Please see http://www.dynust.net for contact information and names                            ~~!
!~~ of individual contributors.                                                                   ~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
      USE DYNUST_MAIN_MODULE
      USE DYNUST_NETWORK_MODULE
      
	  INTEGER i1, i2, IntoOutNodeNumber
	  REAL g1
!> > > > > > > > > >                < < < < < < < < < < <
!>>>>>>>>>>>>>>>>>>>CODES START HERE<<<<<<<<<<<<<<<<<<<<<
!> > > > > > > > > >                < < < < < < < < < < <
  
      i1=backpointr(IntoOutNodeNumber)
      i2=backpointr(IntoOutNodeNumber+1)-1
        DO i=i1,i2
		  ik=m_dynust_network_arc_nde(i)%BackToForLink
          m_dynust_network_arc_de(ik)%total_count = 0
          m_dynust_network_arcmove_de(ik,1:m_dynust_network_arc_nde(ik)%llink%no)%green=xminPerSimInt*60 ! STOP control assumes to have full 6 sec but the saturation flow rate will be adjusted by number of conflicting links having vehicles present
		 DO j=i1,i2
		   IF(j /= i.and.m_dynust_network_arc_de(m_dynust_network_arc_nde(j)%BackToForLink)%vehicle_queue > 0) m_dynust_network_arc_de(ik)%total_count=m_dynust_network_arc_de(ik)%total_count+1 ! total_count is to check for link ik, how many conflicting approaches has vehicles present
		 ENDDO

        ENDDO
      END SUBROUTINE
