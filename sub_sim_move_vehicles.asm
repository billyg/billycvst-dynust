; mark_description "Intel(R) Visual Fortran Compiler XE for applications running on IA-32, Version 13.1.1.171 Build 20130313";
; mark_description "-S";
	.686P
 	.387
	OPTION DOTNAME
	ASSUME	CS:FLAT,DS:FLAT,SS:FLAT
;ident "-defaultlib:ifmodintr.lib"
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
TXTST0:
; -- Begin  _SIM_MOVE_VEHICLES
; mark_begin;
IF @Version GE 800
  .MMX
ELSEIF @Version GE 612
  .MMX
  MMWORD TEXTEQU <QWORD>
ENDIF
IF @Version GE 800
  .XMM
ELSEIF @Version GE 614
  .XMM
  XMMWORD TEXTEQU <OWORD>
ENDIF
       ALIGN     16
	PUBLIC _SIM_MOVE_VEHICLES
_SIM_MOVE_VEHICLES	PROC NEAR 
; parameter 1: 8 + ebp
; parameter 2: 12 + ebp
.B1.1:                          ; Preds .B1.0
        push      ebp                                           ;1.12
        mov       ebp, esp                                      ;1.12
        and       esp, -16                                      ;1.12
        push      esi                                           ;1.12
        push      edi                                           ;1.12
        push      ebx                                           ;1.12
        sub       esp, 324                                      ;1.12
        xor       ecx, ecx                                      ;44.23
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFARCS] ;58.1
        test      eax, eax                                      ;58.1
        mov       DWORD PTR [72+esp], ecx                       ;44.23
        cmovle    eax, ecx                                      ;58.1
        mov       esi, 4                                        ;58.1
        mov       edx, 2                                        ;58.1
        mov       DWORD PTR [92+esp], ecx                       ;44.23
        mov       ebx, 1                                        ;58.1
        mov       DWORD PTR [80+esp], ecx                       ;58.1
        lea       edi, DWORD PTR [124+esp]                      ;58.1
        mov       DWORD PTR [84+esp], 133                       ;58.1
        mov       DWORD PTR [76+esp], esi                       ;58.1
        lea       ecx, DWORD PTR [eax*4]                        ;58.1
        mov       DWORD PTR [88+esp], edx                       ;58.1
        mov       DWORD PTR [104+esp], ebx                      ;58.1
        mov       DWORD PTR [96+esp], eax                       ;58.1
        mov       DWORD PTR [116+esp], ebx                      ;58.1
        mov       DWORD PTR [108+esp], esi                      ;58.1
        mov       DWORD PTR [100+esp], esi                      ;58.1
        mov       DWORD PTR [112+esp], ecx                      ;58.1
        push      16                                            ;58.1
        push      eax                                           ;58.1
        push      edx                                           ;58.1
        push      edi                                           ;58.1
        call      _for_check_mult_overflow                      ;58.1
                                ; LOE eax
.B1.2:                          ; Preds .B1.1
        mov       edx, DWORD PTR [100+esp]                      ;58.1
        and       eax, 1                                        ;58.1
        and       edx, 1                                        ;58.1
        lea       ecx, DWORD PTR [88+esp]                       ;58.1
        shl       eax, 4                                        ;58.1
        add       edx, edx                                      ;58.1
        or        edx, eax                                      ;58.1
        or        edx, 262144                                   ;58.1
        push      edx                                           ;58.1
        push      ecx                                           ;58.1
        push      DWORD PTR [148+esp]                           ;58.1
        call      _for_alloc_allocatable                        ;58.1
                                ; LOE
.B1.301:                        ; Preds .B1.2
        add       esp, 28                                       ;58.1
                                ; LOE
.B1.3:                          ; Preds .B1.301
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+24] ;60.1
        test      ebx, ebx                                      ;60.1
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;60.1
        jle       .B1.10        ; Prob 50%                      ;60.1
                                ; LOE eax ebx
.B1.4:                          ; Preds .B1.3
        mov       edi, ebx                                      ;60.1
        shr       edi, 31                                       ;60.1
        add       edi, ebx                                      ;60.1
        sar       edi, 1                                        ;60.1
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;60.1
        test      edi, edi                                      ;60.1
        jbe       .B1.297       ; Prob 10%                      ;60.1
                                ; LOE eax ebx esi edi
.B1.5:                          ; Preds .B1.4
        xor       ecx, ecx                                      ;
        mov       DWORD PTR [esp], eax                          ;
        xor       edx, edx                                      ;
        xor       eax, eax                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B1.6:                          ; Preds .B1.6 .B1.5
        inc       ecx                                           ;60.1
        mov       DWORD PTR [664+edx+esi], eax                  ;60.1
        mov       DWORD PTR [1564+edx+esi], eax                 ;60.1
        add       edx, 1800                                     ;60.1
        cmp       ecx, edi                                      ;60.1
        jb        .B1.6         ; Prob 63%                      ;60.1
                                ; LOE eax edx ecx ebx esi edi
.B1.7:                          ; Preds .B1.6
        mov       eax, DWORD PTR [esp]                          ;
        lea       ecx, DWORD PTR [1+ecx+ecx]                    ;60.1
                                ; LOE eax ecx ebx esi
.B1.8:                          ; Preds .B1.7 .B1.297
        lea       edx, DWORD PTR [-1+ecx]                       ;60.1
        cmp       ebx, edx                                      ;60.1
        jbe       .B1.10        ; Prob 10%                      ;60.1
                                ; LOE eax ecx esi
.B1.9:                          ; Preds .B1.8
        mov       edx, eax                                      ;60.1
        add       ecx, eax                                      ;60.1
        neg       edx                                           ;60.1
        add       edx, ecx                                      ;60.1
        imul      eax, edx, 900                                 ;60.1
        mov       DWORD PTR [-236+esi+eax], 0                   ;60.1
                                ; LOE
.B1.10:                         ; Preds .B1.8 .B1.3 .B1.9
        mov       edx, DWORD PTR [108+esp]                      ;62.1
        test      edx, edx                                      ;62.1
        mov       ecx, DWORD PTR [116+esp]                      ;62.1
        jle       .B1.12        ; Prob 10%                      ;62.1
                                ; LOE edx ecx
.B1.11:                         ; Preds .B1.10
        mov       eax, DWORD PTR [96+esp]                       ;62.1
        test      eax, eax                                      ;62.1
        jg        .B1.219       ; Prob 50%                      ;62.1
                                ; LOE eax edx ecx
.B1.12:                         ; Preds .B1.290 .B1.222 .B1.10 .B1.11
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VEHJO] ;65.1
        test      edi, edi                                      ;65.10
        mov       ebx, DWORD PTR [8+ebp]                        ;1.12
        jle       .B1.19        ; Prob 16%                      ;65.10
                                ; LOE ebx edi
.B1.13:                         ; Preds .B1.12
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ITEMAX] ;67.18
        test      esi, esi                                      ;67.33
        je        .B1.18        ; Prob 50%                      ;67.33
                                ; LOE ebx esi edi
.B1.14:                         ; Preds .B1.13
        jle       .B1.19        ; Prob 16%                      ;67.49
                                ; LOE ebx esi edi
.B1.15:                         ; Preds .B1.14
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ITERATION] ;67.49
        mov       eax, ecx                                      ;67.59
        cdq                                                     ;67.59
        idiv      edi                                           ;67.59
        test      edx, edx                                      ;67.80
        jne       .B1.17        ; Prob 50%                      ;67.80
                                ; LOE ecx ebx esi
.B1.16:                         ; Preds .B1.15
        test      ecx, ecx                                      ;67.99
        jg        .B1.18        ; Prob 84%                      ;67.99
                                ; LOE ecx ebx esi
.B1.17:                         ; Preds .B1.16 .B1.15
        cmp       ecx, esi                                      ;67.117
        jne       .B1.19        ; Prob 50%                      ;67.117
                                ; LOE ebx
.B1.18:                         ; Preds .B1.13 .B1.16 .B1.17
        mov       eax, DWORD PTR [ebx]                          ;68.13
        lea       ecx, DWORD PTR [16+esp]                       ;68.13
        mov       DWORD PTR [16+esp], 0                         ;68.13
        lea       edx, DWORD PTR [120+esp]                      ;68.13
        mov       DWORD PTR [120+esp], eax                      ;68.13
        push      32                                            ;68.13
        push      OFFSET FLAT: SIM_MOVE_VEHICLES$format_pack.0.1+32 ;68.13
        push      edx                                           ;68.13
        push      OFFSET FLAT: __STRLITPACK_7.0.1               ;68.13
        push      -2088435968                                   ;68.13
        push      888                                           ;68.13
        push      ecx                                           ;68.13
        call      _for_write_seq_fmt                            ;68.13
                                ; LOE ebx
.B1.302:                        ; Preds .B1.18
        add       esp, 28                                       ;68.13
                                ; LOE ebx
.B1.19:                         ; Preds .B1.302 .B1.14 .B1.12 .B1.17
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFARCS] ;77.4
        mov       eax, 1                                        ;77.4
        test      esi, esi                                      ;77.4
        jle       .B1.274       ; Prob 2%                       ;77.4
                                ; LOE eax ebx esi
.B1.20:                         ; Preds .B1.19
        mov       edi, DWORD PTR [12+ebp]                       ;1.12
        mov       ecx, DWORD PTR [ebx]                          ;108.15
        mov       DWORD PTR [248+esp], ecx                      ;108.15
        mov       ecx, DWORD PTR [edi]                          ;122.45
        movss     xmm5, DWORD PTR [ebx]                         ;108.15
        mov       edx, DWORD PTR [_SIM_MOVE_VEHICLES$FLAG.0.1]  ;364.21
        mov       DWORD PTR [180+esp], edx                      ;364.21
        lea       edi, DWORD PTR [-1+ecx]                       ;122.58
        cvtsi2ss  xmm1, edi                                     ;266.37
        movss     xmm4, DWORD PTR [_2il0floatpacket.8]          ;96.65
        mov       edx, 1                                        ;
        movss     xmm3, DWORD PTR [_2il0floatpacket.9]          ;142.99
        movss     xmm2, DWORD PTR [_2il0floatpacket.10]         ;302.87
        mov       DWORD PTR [276+esp], edi                      ;122.58
        movss     xmm0, DWORD PTR [_2il0floatpacket.11]         ;157.36
        movss     DWORD PTR [184+esp], xmm1                     ;157.36
        movss     DWORD PTR [252+esp], xmm5                     ;157.36
        mov       DWORD PTR [212+esp], ecx                      ;157.36
        mov       DWORD PTR [228+esp], esi                      ;157.36
                                ; LOE eax edx
.B1.21:                         ; Preds .B1.146 .B1.20
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;79.5
        neg       ecx                                           ;79.46
        add       ecx, edx                                      ;79.46
        imul      esi, ecx, 152                                 ;79.46
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;79.5
        movsx     edi, WORD PTR [148+ebx+esi]                   ;79.8
        cmp       edi, 99                                       ;79.46
        je        .B1.146       ; Prob 16%                      ;79.46
                                ; LOE eax edx
.B1.22:                         ; Preds .B1.21
        imul      ecx, edx, 44                                  ;93.5
        mov       edi, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST] ;93.5
        mov       ebx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST+32] ;93.5
        mov       DWORD PTR [216+esp], edi                      ;93.5
        mov       DWORD PTR [224+esp], ecx                      ;93.5
        lea       esi, DWORD PTR [edi+ecx]                      ;93.5
        imul      edi, ebx, -44                                 ;93.5
        mov       ecx, DWORD PTR [28+edi+esi]                   ;93.15
        mov       DWORD PTR [220+esp], ebx                      ;93.5
        mov       ebx, DWORD PTR [32+edi+esi]                   ;93.15
        imul      ebx, ecx                                      ;93.5
        mov       esi, DWORD PTR [edi+esi]                      ;93.15
        sub       esi, ebx                                      ;93.5
        mov       edi, DWORD PTR [224+esp]                      ;93.38
        cmp       DWORD PTR [ecx+esi], 0                        ;93.38
        jle       .B1.146       ; Prob 3%                       ;93.38
                                ; LOE eax edx edi
.B1.23:                         ; Preds .B1.22
        mov       DWORD PTR [280+esp], eax                      ;77.4
        mov       ebx, 1                                        ;
        imul      eax, DWORD PTR [220+esp], -44                 ;
        add       edi, DWORD PTR [216+esp]                      ;
        add       edi, eax                                      ;
        mov       esi, DWORD PTR [36+edi]                       ;93.56
        test      esi, esi                                      ;93.53
        jle       .B1.146       ; Prob 1%                       ;93.53
                                ; LOE edx ebx esi edi
.B1.24:                         ; Preds .B1.23
        mov       DWORD PTR [284+esp], edx                      ;
                                ; LOE ebx esi edi
.B1.25:                         ; Preds .B1.144 .B1.24
        mov       eax, ebx                                      ;94.9
        sub       eax, DWORD PTR [32+edi]                       ;94.9
        imul      eax, DWORD PTR [28+edi]                       ;94.9
        mov       ecx, DWORD PTR [edi]                          ;94.11
        mov       edx, DWORD PTR [ecx+eax]                      ;94.9
        mov       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;95.9
        neg       ecx                                           ;95.12
        add       ecx, edx                                      ;95.12
        shl       ecx, 8                                        ;95.12
        mov       DWORD PTR [320+esp], edx                      ;94.9
        mov       DWORD PTR [316+esp], edx                      ;94.9
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;95.9
        test      BYTE PTR [20+edx+ecx], 1                      ;95.17
        jne       .B1.141       ; Prob 40%                      ;95.17
                                ; LOE edx ecx ebx esi edi
.B1.26:                         ; Preds .B1.25
        mov       eax, DWORD PTR [284+esp]                      ;101.81
        sub       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;101.81
        imul      edi, eax, 152                                 ;101.81
        movss     xmm1, DWORD PTR [12+edx+ecx]                  ;99.43
        movss     xmm2, DWORD PTR [_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT] ;99.43
        movaps    xmm0, xmm1                                    ;99.67
        movss     xmm4, DWORD PTR [240+edx+ecx]                 ;96.41
        movaps    xmm3, xmm4                                    ;99.13
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;101.13
        mov       eax, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY+32] ;101.13
        neg       eax                                           ;101.81
        add       eax, DWORD PTR [24+esi+edi]                   ;101.81
        mov       DWORD PTR [288+esp], esi                      ;101.13
        mov       esi, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY] ;101.13
        mulss     xmm0, xmm2                                    ;99.67
        lea       eax, DWORD PTR [eax+eax*2]                    ;101.81
        shl       eax, 4                                        ;101.81
        subss     xmm3, xmm0                                    ;99.13
        mov       DWORD PTR [292+esp], edi                      ;101.81
        movsx     edi, BYTE PTR [esi+eax]                       ;101.16
        and       edi, -2                                       ;101.81
        cmp       edi, 4                                        ;101.81
        je        .B1.252       ; Prob 16%                      ;101.81
                                ; LOE edx ecx ebx xmm0 xmm1 xmm2 xmm3 xmm4
.B1.27:                         ; Preds .B1.26 .B1.252 .B1.253
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;105.13
        neg       eax                                           ;105.48
        add       eax, DWORD PTR [320+esp]                      ;105.48
        shl       eax, 5                                        ;105.48
        mov       esi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;105.13
        mov       DWORD PTR [304+esp], esi                      ;105.13
        cmp       BYTE PTR [4+esi+eax], 3                       ;105.48
        jne       .B1.29        ; Prob 84%                      ;105.48
                                ; LOE eax edx ecx ebx xmm0 xmm1 xmm2 xmm3 xmm4
.B1.28:                         ; Preds .B1.27
        movss     xmm5, DWORD PTR [252+esp]                     ;108.39
        divss     xmm5, xmm2                                    ;108.39
        cvtsi2ss  xmm6, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIMPERAGG] ;108.55
        divss     xmm5, xmm6                                    ;108.32
        mov       DWORD PTR [160+esp], edx                      ;
        pxor      xmm7, xmm7                                    ;111.15
        mov       edx, DWORD PTR [288+esp]                      ;107.15
        mov       esi, DWORD PTR [292+esp]                      ;107.15
        mov       DWORD PTR [204+esp], ebx                      ;
        mov       DWORD PTR [156+esp], ecx                      ;
        divss     xmm0, DWORD PTR [20+edx+esi]                  ;109.117
        mov       edi, DWORD PTR [12+edx+esi]                   ;107.15
        sub       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;109.15
        imul      ebx, edi, 900                                 ;109.15
        cvttss2si edi, xmm5                                     ;108.32
        inc       edi                                           ;108.65
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_AGGINT] ;108.15
        cmp       edi, edx                                      ;109.15
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;109.117
        cmovl     edx, edi                                      ;109.15
        mov       edi, DWORD PTR [304+esp]                      ;109.148
        sub       edx, DWORD PTR [840+ecx+ebx]                  ;109.15
        movsx     esi, BYTE PTR [5+edi+eax]                     ;109.148
        sub       esi, DWORD PTR [852+ecx+ebx]                  ;109.15
        imul      esi, DWORD PTR [848+ecx+ebx]                  ;109.15
        add       esi, DWORD PTR [808+ecx+ebx]                  ;109.15
        mov       ebx, DWORD PTR [204+esp]                      ;111.15
        mulss     xmm0, DWORD PTR [esi+edx*4]                   ;109.147
        mov       edx, DWORD PTR [156+esp]                      ;110.8
        addss     xmm0, DWORD PTR [8+edi+eax]                   ;109.15
        movss     DWORD PTR [8+edi+eax], xmm0                   ;109.15
        movss     xmm0, DWORD PTR [_2il0floatpacket.12]         ;110.8
        mov       eax, DWORD PTR [160+esp]                      ;110.8
        maxss     xmm0, xmm1                                    ;110.8
        movaps    xmm1, xmm4                                    ;110.8
        divss     xmm1, xmm0                                    ;110.8
        movss     DWORD PTR [152+eax+edx], xmm1                 ;110.8
        subss     xmm2, xmm1                                    ;111.61
        maxss     xmm7, xmm2                                    ;111.15
        movss     DWORD PTR [100+eax+edx], xmm7                 ;111.15
                                ; LOE ebx xmm3 xmm4
.B1.29:                         ; Preds .B1.27 .B1.28
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_FUELOUT], 0 ;114.24
        jle       .B1.34        ; Prob 16%                      ;114.24
                                ; LOE ebx xmm3 xmm4
.B1.30:                         ; Preds .B1.29
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ITERATION] ;114.24
        cmp       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ITEMAX] ;114.43
        jne       .B1.33        ; Prob 50%                      ;114.43
                                ; LOE ebx xmm3 xmm4
.B1.31:                         ; Preds .B1.30 .B1.33
        lea       edx, DWORD PTR [280+esp]                      ;116.20
        lea       eax, DWORD PTR [316+esp]                      ;116.20
        push      DWORD PTR [8+ebp]                             ;116.20
        push      eax                                           ;116.20
        push      edx                                           ;116.20
        movss     DWORD PTR [252+esp], xmm3                     ;116.20
        movss     DWORD PTR [256+esp], xmm4                     ;116.20
        call      _DYNUST_FUEL_MODULE_mp_OUTPUTFUELDEMAND       ;116.20
                                ; LOE ebx
.B1.303:                        ; Preds .B1.31
        movss     xmm4, DWORD PTR [256+esp]                     ;
        movss     xmm3, DWORD PTR [252+esp]                     ;
        add       esp, 12                                       ;116.20
                                ; LOE ebx xmm3 xmm4
.B1.32:                         ; Preds .B1.303
        mov       eax, DWORD PTR [316+esp]                      ;122.17
        mov       DWORD PTR [320+esp], eax                      ;122.17
        jmp       .B1.34        ; Prob 100%                     ;122.17
                                ; LOE ebx xmm3 xmm4
.B1.33:                         ; Preds .B1.30
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_REACH_CONVERG], -1 ;114.70
        je        .B1.31        ; Prob 5%                       ;114.70
                                ; LOE ebx xmm3 xmm4
.B1.34:                         ; Preds .B1.33 .B1.32 .B1.29
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE+32] ;122.13
        neg       edx                                           ;122.119
        add       edx, DWORD PTR [320+esp]                      ;122.119
        shl       edx, 6                                        ;122.119
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE] ;122.13
        movsx     ecx, WORD PTR [12+eax+edx]                    ;122.17
        cmp       ecx, 1                                        ;122.45
        jle       .B1.38        ; Prob 16%                      ;122.45
                                ; LOE ebx xmm3 xmm4
.B1.35:                         ; Preds .B1.34
        mov       eax, DWORD PTR [276+esp]                      ;122.53
        cdq                                                     ;122.53
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TDSPSTEP] ;122.58
        idiv      ecx                                           ;122.53
        test      edx, edx                                      ;122.71
        jne       .B1.38        ; Prob 50%                      ;122.71
                                ; LOE ebx xmm3 xmm4
.B1.36:                         ; Preds .B1.35
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;122.48
        neg       edx                                           ;122.119
        add       edx, DWORD PTR [320+esp]                      ;122.119
        shl       edx, 5                                        ;122.119
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;122.48
        cmp       BYTE PTR [4+eax+edx], 4                       ;122.114
        je        .B1.224       ; Prob 16%                      ;122.114
                                ; LOE ebx xmm3 xmm4
.B1.38:                         ; Preds .B1.34 .B1.227 .B1.224 .B1.36 .B1.35
                                ;      
        mov       esi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;142.17
        mov       edi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;142.17
                                ; LOE ebx esi edi xmm3 xmm4
.B1.39:                         ; Preds .B1.231 .B1.230 .B1.38
        neg       edi                                           ;
        add       edi, DWORD PTR [320+esp]                      ;
        pxor      xmm0, xmm0                                    ;141.21
        shl       edi, 8                                        ;
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;
        neg       ecx                                           ;
        mov       edx, DWORD PTR [280+esp]                      ;142.20
        add       ecx, edx                                      ;
        mov       DWORD PTR [312+esp], edi                      ;
        movss     xmm2, DWORD PTR [12+esi+edi]                  ;142.20
        imul      edi, ecx, 900                                 ;
        comiss    xmm3, xmm0                                    ;141.21
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;142.20
        mov       DWORD PTR [308+esp], eax                      ;142.20
        mov       DWORD PTR [284+esp], edx                      ;142.20
        movsx     eax, BYTE PTR [680+eax+edi]                   ;142.20
        mov       DWORD PTR [296+esp], edi                      ;
        mov       DWORD PTR [300+esp], eax                      ;142.20
        mov       edi, DWORD PTR [312+esp]                      ;141.21
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC] ;141.21
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC+32] ;141.21
        jbe       .B1.61        ; Prob 50%                      ;141.21
                                ; LOE eax edx ebx esi edi xmm2 xmm3 xmm4
.B1.40:                         ; Preds .B1.39
        neg       edx                                           ;142.45
        add       edx, DWORD PTR [300+esp]                      ;142.45
        shl       edx, 5                                        ;142.45
        movss     xmm0, DWORD PTR [_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT] ;143.44
        cvtsi2ss  xmm1, DWORD PTR [8+eax+edx]                   ;142.48
        divss     xmm1, DWORD PTR [_2il0floatpacket.9]          ;142.99
        comiss    xmm1, xmm2                                    ;142.45
        jb        .B1.42        ; Prob 50%                      ;142.45
                                ; LOE ebx esi edi xmm0 xmm3 xmm4
.B1.41:                         ; Preds .B1.40
        movss     xmm1, DWORD PTR [84+esi+edi]                  ;143.44
        addss     xmm1, xmm0                                    ;143.19
        movss     DWORD PTR [84+esi+edi], xmm1                  ;143.19
                                ; LOE ebx esi edi xmm0 xmm3 xmm4
.B1.42:                         ; Preds .B1.40 .B1.41
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;148.20
        neg       eax                                           ;148.45
        add       eax, DWORD PTR [284+esp]                      ;148.45
        imul      ecx, eax, 152                                 ;148.45
        movss     xmm2, DWORD PTR [88+esi+edi]                  ;152.43
        movss     xmm5, DWORD PTR [_DYNUST_MAIN_MODULE_mp_GUITOTALTIME] ;154.17
        addss     xmm2, xmm0                                    ;152.17
        addss     xmm5, xmm0                                    ;154.17
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;148.20
        movss     DWORD PTR [88+esi+edi], xmm2                  ;152.17
        movss     DWORD PTR [_DYNUST_MAIN_MODULE_mp_GUITOTALTIME], xmm5 ;154.17
        movss     xmm1, DWORD PTR [20+edx+ecx]                  ;148.47
        minss     xmm1, xmm3                                    ;148.45
        movss     DWORD PTR [240+esi+edi], xmm1                 ;149.21
        mov       edi, DWORD PTR [296+esp]                      ;156.20
        mov       esi, DWORD PTR [308+esp]                      ;156.20
        mov       esi, DWORD PTR [448+esi+edi]                  ;156.20
        test      esi, esi                                      ;156.61
        jle       .B1.49        ; Prob 16%                      ;156.61
                                ; LOE ebx esi xmm3 xmm4
.B1.43:                         ; Preds .B1.42
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DETECTOR+40] ;162.31
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DETECTOR] ;162.25
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DETECTOR+44] ;162.31
        imul      edx, ecx                                      ;162.25
        movss     xmm0, DWORD PTR [_2il0floatpacket.11]         ;157.21
        mulss     xmm4, xmm0                                    ;157.21
        mulss     xmm3, xmm0                                    ;158.21
        mov       DWORD PTR [236+esp], esi                      ;
        lea       edi, DWORD PTR [edi+esi*4]                    ;162.25
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DETECTOR+32] ;162.25
        lea       esi, DWORD PTR [ecx*4]                        ;162.31
        neg       esi                                           ;162.25
        lea       ecx, DWORD PTR [ecx+ecx*4]                    ;163.31
        add       esi, edx                                      ;162.25
        neg       ecx                                           ;163.31
        neg       esi                                           ;162.25
        add       edx, ecx                                      ;163.25
        shl       eax, 2                                        ;162.25
        add       esi, edi                                      ;162.25
        sub       esi, eax                                      ;162.25
        sub       edi, edx                                      ;163.25
        sub       edi, eax                                      ;163.25
        cvtsi2ss  xmm1, DWORD PTR [esi]                         ;166.34
        mov       eax, DWORD PTR [edi]                          ;163.25
        mov       esi, DWORD PTR [236+esp]                      ;166.32
        comiss    xmm4, xmm1                                    ;166.32
        jbe       .B1.47        ; Prob 50%                      ;166.32
                                ; LOE eax ebx esi xmm1 xmm3 xmm4
.B1.44:                         ; Preds .B1.43
        cvtsi2ss  xmm0, eax                                     ;166.54
        comiss    xmm0, xmm3                                    ;166.52
        jbe       .B1.46        ; Prob 50%                      ;166.52
                                ; LOE eax ebx esi xmm0 xmm1 xmm3 xmm4
.B1.45:                         ; Preds .B1.44
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DETECTOR_LENGTH+32] ;167.37
        shl       edx, 2                                        ;167.25
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_OCCUP+32] ;167.25
        neg       edx                                           ;167.25
        shl       ecx, 2                                        ;167.25
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_OCCUP] ;167.25
        neg       ecx                                           ;167.25
        add       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DETECTOR_LENGTH] ;167.25
        lea       edi, DWORD PTR [eax+esi*4]                    ;167.25
        movss     xmm0, DWORD PTR [edx+esi*4]                   ;167.49
        mov       esi, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST+32] ;380.9
        neg       esi                                           ;
        addss     xmm0, DWORD PTR [ecx+edi]                     ;167.25
        add       esi, DWORD PTR [284+esp]                      ;
        movss     DWORD PTR [ecx+edi], xmm0                     ;167.25
        imul      edi, esi, 44                                  ;
        add       edi, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST] ;
        mov       esi, DWORD PTR [36+edi]                       ;380.19
        jmp       .B1.142       ; Prob 100%                     ;380.19
                                ; LOE ebx esi edi
.B1.46:                         ; Preds .B1.44
        comiss    xmm1, xmm4                                    ;168.36
        ja        .B1.48        ; Prob 50%                      ;168.36
        jmp       .B1.51        ; Prob 100%                     ;168.36
                                ; LOE eax ebx esi xmm0 xmm1 xmm3 xmm4
.B1.47:                         ; Preds .B1.43
        comiss    xmm1, xmm4                                    ;168.36
        jbe       .B1.54        ; Prob 50%                      ;168.36
                                ; LOE eax ebx esi xmm1 xmm3 xmm4
.B1.331:                        ; Preds .B1.47
        cvtsi2ss  xmm0, eax                                     ;166.54
                                ; LOE eax ebx esi xmm0 xmm1 xmm3 xmm4
.B1.48:                         ; Preds .B1.46 .B1.331
        comiss    xmm3, xmm0                                    ;168.56
        jbe       .B1.50        ; Prob 50%                      ;168.56
                                ; LOE eax ebx esi xmm0 xmm1 xmm3 xmm4
.B1.49:                         ; Preds .B1.42 .B1.54 .B1.332 .B1.55 .B1.56
                                ;       .B1.48
        mov       eax, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST+32] ;380.9
        neg       eax                                           ;
        add       eax, DWORD PTR [284+esp]                      ;
        imul      edi, eax, 44                                  ;
        add       edi, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST] ;
        mov       esi, DWORD PTR [36+edi]                       ;380.19
        jmp       .B1.142       ; Prob 100%                     ;380.19
                                ; LOE ebx esi edi
.B1.50:                         ; Preds .B1.48
        comiss    xmm4, xmm1                                    ;170.36
        jbe       .B1.332       ; Prob 50%                      ;170.36
                                ; LOE eax ebx esi xmm0 xmm1 xmm3 xmm4
.B1.51:                         ; Preds .B1.46 .B1.50
        comiss    xmm3, xmm0                                    ;170.56
        jbe       .B1.332       ; Prob 50%                      ;170.56
                                ; LOE eax ebx esi xmm1 xmm3 xmm4
.B1.52:                         ; Preds .B1.51
        comiss    xmm1, xmm3                                    ;170.76
        jbe       .B1.332       ; Prob 50%                      ;170.76
                                ; LOE eax ebx esi xmm1 xmm3 xmm4
.B1.53:                         ; Preds .B1.52
        mov       edx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST+32] ;380.9
        subss     xmm1, xmm3                                    ;171.55
        neg       edx                                           ;
        add       edx, DWORD PTR [284+esp]                      ;
        imul      edi, edx, 44                                  ;
        sub       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_OCCUP+32] ;171.25
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_OCCUP] ;171.25
        add       edi, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST] ;
        addss     xmm1, DWORD PTR [eax+esi*4]                   ;171.25
        movss     DWORD PTR [eax+esi*4], xmm1                   ;171.25
        mov       esi, DWORD PTR [36+edi]                       ;380.19
        jmp       .B1.142       ; Prob 100%                     ;380.19
                                ; LOE ebx esi edi
.B1.54:                         ; Preds .B1.47
        jbe       .B1.49        ; Prob 50%                      ;172.36
                                ; LOE eax ebx esi xmm3 xmm4
.B1.55:                         ; Preds .B1.332 .B1.54
        cvtsi2ss  xmm0, eax                                     ;172.58
        comiss    xmm4, xmm0                                    ;172.56
        jbe       .B1.49        ; Prob 50%                      ;172.56
                                ; LOE ebx esi xmm0 xmm3 xmm4
.B1.56:                         ; Preds .B1.55
        comiss    xmm0, xmm3                                    ;172.76
        jbe       .B1.49        ; Prob 50%                      ;172.76
                                ; LOE ebx esi xmm0 xmm4
.B1.57:                         ; Preds .B1.56
        mov       edx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST+32] ;380.9
        subss     xmm4, xmm0                                    ;173.57
        neg       edx                                           ;
        add       edx, DWORD PTR [284+esp]                      ;
        imul      edi, edx, 44                                  ;
        sub       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_OCCUP+32] ;173.25
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_OCCUP] ;173.25
        add       edi, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST] ;
        addss     xmm4, DWORD PTR [eax+esi*4]                   ;173.25
        movss     DWORD PTR [eax+esi*4], xmm4                   ;173.25
        mov       esi, DWORD PTR [36+edi]                       ;380.19
        jmp       .B1.142       ; Prob 100%                     ;380.19
                                ; LOE ebx esi edi
.B1.61:                         ; Preds .B1.39
        neg       edx                                           ;179.45
        add       edx, DWORD PTR [300+esp]                      ;179.45
        shl       edx, 5                                        ;179.45
        movss     xmm0, DWORD PTR [152+esi+edi]                 ;180.70
        cvtsi2ss  xmm1, DWORD PTR [8+eax+edx]                   ;179.48
        divss     xmm1, DWORD PTR [_2il0floatpacket.9]          ;179.99
        comiss    xmm1, xmm2                                    ;179.45
        jb        .B1.63        ; Prob 50%                      ;179.45
                                ; LOE ebx esi edi xmm0
.B1.62:                         ; Preds .B1.61
        movss     xmm1, DWORD PTR [84+esi+edi]                  ;180.45
        addss     xmm1, xmm0                                    ;180.20
        movss     DWORD PTR [84+esi+edi], xmm1                  ;180.20
                                ; LOE ebx esi edi xmm0
.B1.63:                         ; Preds .B1.61 .B1.62
        movss     xmm3, DWORD PTR [28+esi+edi]                  ;185.39
        xor       ecx, ecx                                      ;185.17
        movaps    xmm2, xmm3                                    ;185.60
        divss     xmm2, DWORD PTR [_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT] ;185.60
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIMPERAGG] ;185.76
        cvtsi2ss  xmm1, edx                                     ;185.76
        divss     xmm2, xmm1                                    ;185.33
        mov       eax, DWORD PTR [248+esp]                      ;182.17
        mov       DWORD PTR [16+esi+edi], eax                   ;182.17
        cvttss2si eax, xmm2                                     ;185.33
        test      eax, eax                                      ;185.17
        mov       DWORD PTR [20+esi+edi], -1                    ;183.17
        cmovl     eax, ecx                                      ;185.17
        mov       DWORD PTR [264+esp], edx                      ;185.76
        mov       DWORD PTR [272+esp], eax                      ;185.17
        comiss    xmm3, DWORD PTR [252+esp]                     ;187.22
        ja        .B1.250       ; Prob 5%                       ;187.22
                                ; LOE ebx esi edi xmm0 xmm3
.B1.64:                         ; Preds .B1.320 .B1.63
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ITEMAX], 0  ;192.21
        jne       .B1.67        ; Prob 50%                      ;192.21
                                ; LOE ebx esi edi xmm0 xmm3
.B1.65:                         ; Preds .B1.64
        mov       eax, DWORD PTR [212+esp]                      ;192.30
        cdq                                                     ;192.30
        mov       ecx, DWORD PTR [264+esp]                      ;192.30
        idiv      ecx                                           ;192.30
        test      edx, edx                                      ;192.47
        jne       .B1.67        ; Prob 50%                      ;192.47
                                ; LOE ebx esi edi xmm0 xmm3
.B1.66:                         ; Preds .B1.65
        mov       DWORD PTR [232+esp], esi                      ;
        xor       edx, edx                                      ;193.15
        mov       ecx, DWORD PTR [296+esp]                      ;193.15
        mov       esi, DWORD PTR [308+esp]                      ;193.15
        mov       DWORD PTR [312+esp], edi                      ;
        mov       eax, DWORD PTR [272+esp]                      ;193.15
        mov       edi, DWORD PTR [224+esi+ecx]                  ;193.15
        shl       edi, 2                                        ;193.15
        neg       edi                                           ;193.15
        add       edi, DWORD PTR [192+esi+ecx]                  ;193.15
        mov       DWORD PTR [4+edi+eax*4], edx                  ;193.15
        mov       edi, DWORD PTR [332+esi+ecx]                  ;194.21
        shl       edi, 2                                        ;194.21
        neg       edi                                           ;194.21
        add       edi, DWORD PTR [300+esi+ecx]                  ;194.21
        mov       esi, DWORD PTR [232+esp]                      ;194.21
        mov       DWORD PTR [4+edi+eax*4], edx                  ;194.21
        mov       edi, DWORD PTR [312+esp]                      ;194.21
        jmp       .B1.68        ; Prob 100%                     ;194.21
                                ; LOE ebx esi edi xmm0
.B1.67:                         ; Preds .B1.64 .B1.65
        movss     xmm1, DWORD PTR [252+esp]                     ;196.118
        mov       edx, DWORD PTR [296+esp]                      ;196.61
        subss     xmm1, xmm3                                    ;196.118
        mov       ecx, DWORD PTR [308+esp]                      ;196.61
        pxor      xmm3, xmm3                                    ;196.15
        mov       DWORD PTR [312+esp], edi                      ;
        maxss     xmm3, xmm1                                    ;196.15
        mov       edi, DWORD PTR [224+ecx+edx]                  ;196.61
        shl       edi, 2                                        ;196.15
        neg       edi                                           ;196.15
        add       edi, DWORD PTR [192+ecx+edx]                  ;196.15
        mov       eax, DWORD PTR [272+esp]                      ;196.61
        mov       DWORD PTR [232+esp], esi                      ;
        mulss     xmm3, DWORD PTR [_2il0floatpacket.10]         ;196.141
        cvtsi2ss  xmm2, DWORD PTR [4+edi+eax*4]                 ;196.61
        addss     xmm3, xmm2                                    ;196.107
        cvttss2si esi, xmm3                                     ;196.15
        mov       DWORD PTR [4+edi+eax*4], esi                  ;196.15
        mov       esi, DWORD PTR [332+ecx+edx]                  ;197.68
        shl       esi, 2                                        ;197.21
        neg       esi                                           ;197.21
        add       esi, DWORD PTR [300+ecx+edx]                  ;197.21
        mov       edi, DWORD PTR [312+esp]                      ;197.21
        inc       DWORD PTR [4+esi+eax*4]                       ;197.21
        mov       esi, DWORD PTR [232+esp]                      ;197.21
                                ; LOE ebx esi edi xmm0
.B1.68:                         ; Preds .B1.67 .B1.66
        mov       edx, DWORD PTR [320+esp]                      ;203.48
        sub       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE+32] ;203.48
        movss     xmm1, DWORD PTR [88+esi+edi]                  ;202.43
        shl       edx, 6                                        ;203.48
        addss     xmm1, xmm0                                    ;202.17
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE] ;203.17
        mov       DWORD PTR [28+esi+edi], 0                     ;200.11
        movss     DWORD PTR [88+esi+edi], xmm1                  ;202.17
        movsx     ecx, WORD PTR [12+eax+edx]                    ;203.20
        cmp       ecx, 1                                        ;203.48
        mov       DWORD PTR [268+esp], edx                      ;203.48
        mov       DWORD PTR [256+esp], eax                      ;203.17
        mov       DWORD PTR [260+esp], ecx                      ;203.20
        jne       .B1.70        ; Prob 84%                      ;203.48
                                ; LOE ebx esi edi xmm0
.B1.69:                         ; Preds .B1.68
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;203.94
        neg       edx                                           ;203.54
        movss     xmm1, DWORD PTR [252+esp]                     ;203.54
        add       edx, DWORD PTR [320+esp]                      ;203.54
        addss     xmm1, xmm0                                    ;203.54
        shl       edx, 5                                        ;203.54
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;203.94
        movss     DWORD PTR [eax+edx], xmm1                     ;203.54
                                ; LOE ebx esi edi xmm0
.B1.70:                         ; Preds .B1.68 .B1.69
        movss     xmm1, DWORD PTR [_DYNUST_MAIN_MODULE_mp_GUITOTALTIME] ;205.17
        movsx     edx, BYTE PTR [160+esi+edi]                   ;209.20
        cmp       edx, 1                                        ;209.47
        addss     xmm1, xmm0                                    ;205.17
        movss     DWORD PTR [_DYNUST_MAIN_MODULE_mp_GUITOTALTIME], xmm1 ;205.17
        jle       .B1.88        ; Prob 6%                       ;209.47
                                ; LOE edx ebx esi edi
.B1.71:                         ; Preds .B1.70
        movsx     eax, BYTE PTR [40+esi+edi]                    ;209.81
        cmp       eax, edx                                      ;209.81
        jge       .B1.88        ; Prob 22%                      ;209.81
                                ; LOE eax ebx esi edi
.B1.72:                         ; Preds .B1.71
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ITERATION], 0 ;212.34
        jne       .B1.76        ; Prob 50%                      ;212.34
                                ; LOE eax ebx esi edi
.B1.73:                         ; Preds .B1.72
        mov       edi, DWORD PTR [200+esp]                      ;213.22
        sub       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;213.57
        imul      esi, edi, 152                                 ;213.57
        mov       DWORD PTR [204+esp], ebx                      ;
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;213.19
        mov       ecx, DWORD PTR [256+esp]                      ;213.22
        mov       edx, DWORD PTR [24+ebx+esi]                   ;213.22
        mov       DWORD PTR [208+esp], edx                      ;213.22
        mov       edx, DWORD PTR [268+esp]                      ;213.22
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_MASTERDEST+32] ;213.22
        neg       esi                                           ;213.57
        mov       edi, DWORD PTR [16+ecx+edx]                   ;213.22
        mov       ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DESTINATION+32] ;213.22
        neg       ebx                                           ;213.57
        lea       edi, DWORD PTR [edi+eax*2]                    ;213.57
        mov       eax, DWORD PTR [48+ecx+edx]                   ;213.22
        add       eax, eax                                      ;213.57
        neg       eax                                           ;213.57
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_MASTERDEST] ;213.22
        movsx     eax, WORD PTR [eax+edi]                       ;213.22
        add       esi, eax                                      ;213.57
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DESTINATION] ;213.22
        movsx     ecx, WORD PTR [edx+esi*2]                     ;213.22
        add       ebx, ecx                                      ;213.57
        mov       esi, DWORD PTR [208+esp]                      ;213.57
        cmp       esi, DWORD PTR [eax+ebx*4]                    ;213.57
        mov       ebx, DWORD PTR [204+esp]                      ;213.57
        jne       .B1.79        ; Prob 50%                      ;213.57
                                ; LOE ebx bl bh
.B1.74:                         ; Preds .B1.73
        mov       DWORD PTR [_SIM_MOVE_VEHICLES$ARRIVEINTDEST.0.1], -1 ;213.145
        mov       eax, -1                                       ;
        jmp       .B1.80        ; Prob 100%                     ;
                                ; LOE eax ebx
.B1.76:                         ; Preds .B1.72
        mov       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;215.16
        neg       ecx                                           ;215.55
        add       ecx, DWORD PTR [320+esp]                      ;215.55
        shl       ecx, 5                                        ;215.55
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;215.16
        movsx     edx, BYTE PTR [4+edx+ecx]                     ;215.19
        and       edx, -2                                       ;215.55
        cmp       edx, 2                                        ;215.55
        je        .B1.234       ; Prob 16%                      ;215.55
                                ; LOE eax ebx esi edi
.B1.77:                         ; Preds .B1.76
        mov       edx, DWORD PTR [164+esi+edi]                  ;218.54
        mov       esi, DWORD PTR [196+esi+edi]                  ;218.54
        add       esi, esi                                      ;218.51
        neg       esi                                           ;218.51
        lea       eax, DWORD PTR [edx+eax*2]                    ;218.51
        movsx     ecx, WORD PTR [esi+eax]                       ;218.51
        cmp       ecx, DWORD PTR [260+esp]                      ;218.51
        jne       .B1.79        ; Prob 50%                      ;218.51
                                ; LOE ebx
.B1.78:                         ; Preds .B1.77
        mov       DWORD PTR [_SIM_MOVE_VEHICLES$ARRIVEINTDEST.0.1], -1 ;218.110
        mov       eax, -1                                       ;
        jmp       .B1.80        ; Prob 100%                     ;
                                ; LOE eax ebx
.B1.79:                         ; Preds .B1.234 .B1.73 .B1.77
        mov       DWORD PTR [_SIM_MOVE_VEHICLES$ARRIVEINTDEST.0.1], 0 ;211.21
        xor       eax, eax                                      ;
                                ; LOE eax ebx
.B1.80:                         ; Preds .B1.74 .B1.235 .B1.78 .B1.79
        test      al, 1                                         ;221.21
        je        .B1.88        ; Prob 60%                      ;221.21
                                ; LOE ebx
.B1.81:                         ; Preds .B1.80
        lea       esi, DWORD PTR [316+esp]                      ;222.24
        lea       edi, DWORD PTR [280+esp]                      ;222.24
        push      esi                                           ;222.24
        push      edi                                           ;222.24
        call      _DYNUST_LINK_VEH_LIST_MODULE_mp_TRIPCHAIN_INSERT ;222.24
                                ; LOE ebx esi edi
.B1.82:                         ; Preds .B1.81
        push      esi                                           ;223.27
        push      edi                                           ;223.27
        call      _DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST_REMOVE ;223.27
                                ; LOE ebx
.B1.304:                        ; Preds .B1.82
        add       esp, 16                                       ;223.27
                                ; LOE ebx
.B1.83:                         ; Preds .B1.304
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;225.25
        neg       ecx                                           ;225.25
        mov       eax, DWORD PTR [280+esp]                      ;225.57
        add       ecx, eax                                      ;225.25
        mov       DWORD PTR [284+esp], eax                      ;225.57
        imul      eax, ecx, 900                                 ;225.25
        mov       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;226.25
        neg       ecx                                           ;226.102
        mov       esi, DWORD PTR [316+esp]                      ;226.28
        add       ecx, esi                                      ;226.102
        shl       ecx, 5                                        ;226.102
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;225.25
        mov       edi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;226.25
        dec       DWORD PTR [640+edx+eax]                       ;225.25
        movsx     ecx, BYTE PTR [5+edi+ecx]                     ;226.28
        cmp       ecx, 2                                        ;226.59
        je        .B1.86        ; Prob 16%                      ;226.59
                                ; LOE eax edx ecx ebx esi
.B1.84:                         ; Preds .B1.83
        cmp       ecx, 5                                        ;226.98
        je        .B1.86        ; Prob 16%                      ;226.98
                                ; LOE eax edx ecx ebx esi
.B1.85:                         ; Preds .B1.84
        cmp       ecx, 7                                        ;226.137
        jne       .B1.87        ; Prob 84%                      ;226.137
                                ; LOE eax edx ebx esi
.B1.86:                         ; Preds .B1.83 .B1.84 .B1.85
        dec       DWORD PTR [644+edx+eax]                       ;227.20
                                ; LOE eax edx ebx esi
.B1.87:                         ; Preds .B1.85 .B1.86
        dec       DWORD PTR [396+edx+eax]                       ;229.22
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;240.22
        neg       edx                                           ;240.22
        add       edx, esi                                      ;240.22
        shl       edx, 8                                        ;240.22
        mov       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;240.22
        mov       DWORD PTR [204+esp], ebx                      ;
        mov       ebx, DWORD PTR [232+ecx+edx]                  ;240.50
        add       ebx, ebx                                      ;240.22
        mov       eax, DWORD PTR [200+ecx+edx]                  ;240.50
        neg       ebx                                           ;240.22
        movsx     edi, BYTE PTR [40+ecx+edx]                    ;240.50
        lea       eax, DWORD PTR [eax+edi*2]                    ;240.22
        movsx     ebx, WORD PTR [ebx+eax]                       ;240.50
        cvtsi2ss  xmm0, ebx                                     ;240.50
        divss     xmm0, DWORD PTR [_2il0floatpacket.9]          ;240.105
        mov       ebx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE+32] ;241.22
        neg       ebx                                           ;241.22
        subss     xmm0, DWORD PTR [100+ecx+edx]                 ;240.22
        add       ebx, esi                                      ;241.22
        mov       esi, DWORD PTR [164+ecx+edx]                  ;241.22
        shl       ebx, 6                                        ;241.22
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE] ;241.22
        movss     DWORD PTR [32+ecx+edx], xmm0                  ;240.22
        lea       esi, DWORD PTR [esi+edi*2]                    ;241.22
        mov       edi, DWORD PTR [196+ecx+edx]                  ;241.22
        add       edi, edi                                      ;241.22
        neg       edi                                           ;241.22
        movzx     eax, WORD PTR [12+eax+ebx]                    ;241.22
        mov       ebx, DWORD PTR [204+esp]                      ;380.19
        mov       WORD PTR [edi+esi], ax                        ;241.22
        xor       edi, edi                                      ;242.25
        mov       DWORD PTR [152+ecx+edx], edi                  ;242.25
        mov       DWORD PTR [100+ecx+edx], edi                  ;243.25
        mov       edx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST+32] ;380.9
        neg       edx                                           ;
        add       edx, DWORD PTR [284+esp]                      ;
        imul      edi, edx, 44                                  ;
        add       edi, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST] ;
        mov       esi, DWORD PTR [36+edi]                       ;380.19
        jmp       .B1.142       ; Prob 100%                     ;380.19
                                ; LOE ebx esi edi
.B1.88:                         ; Preds .B1.70 .B1.71 .B1.80
        lea       eax, DWORD PTR [316+esp]                      ;249.51
        push      eax                                           ;249.51
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_P_SIZE  ;249.51
                                ; LOE eax ebx
.B1.305:                        ; Preds .B1.88
        add       esp, 4                                        ;249.51
                                ; LOE eax ebx
.B1.89:                         ; Preds .B1.305
        mov       esi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE+32] ;249.17
        dec       eax                                           ;249.67
        neg       esi                                           ;249.48
        mov       edx, DWORD PTR [316+esp]                      ;249.20
        add       esi, edx                                      ;249.48
        shl       esi, 6                                        ;249.48
        mov       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE] ;249.17
        movsx     esi, WORD PTR [12+ecx+esi]                    ;249.20
        cmp       esi, eax                                      ;249.48
        jl        .B1.111       ; Prob 50%                      ;249.48
                                ; LOE edx ebx esi
.B1.90:                         ; Preds .B1.89
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;250.21
        sub       edx, eax                                      ;250.21
        shl       edx, 8                                        ;250.21
        mov       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;250.21
        movsx     eax, BYTE PTR [237+ecx+edx]                   ;251.24
        test      eax, eax                                      ;251.45
        mov       BYTE PTR [96+ecx+edx], 1                      ;250.21
        jne       .B1.92        ; Prob 50%                      ;251.45
                                ; LOE eax edx ecx ebx
.B1.91:                         ; Preds .B1.90
        mov       edi, DWORD PTR [112+esp]                      ;252.25
        mov       esi, DWORD PTR [116+esp]                      ;252.25
        imul      esi, edi                                      ;252.25
        mov       DWORD PTR [132+esp], edx                      ;
        mov       edx, DWORD PTR [104+esp]                      ;252.41
        mov       DWORD PTR [136+esp], ecx                      ;
        mov       ecx, edi                                      ;252.25
        shl       edx, 2                                        ;252.25
        sub       ecx, esi                                      ;252.25
        mov       eax, DWORD PTR [72+esp]                       ;252.25
        add       edi, edi                                      ;253.41
        sub       eax, edx                                      ;252.25
        sub       edi, esi                                      ;253.25
        add       ecx, eax                                      ;252.25
        add       eax, edi                                      ;253.25
        mov       edx, DWORD PTR [280+esp]                      ;252.25
        inc       DWORD PTR [ecx+edx*4]                         ;252.25
        dec       DWORD PTR [eax+edx*4]                         ;253.25
        mov       ecx, DWORD PTR [136+esp]                      ;253.25
        mov       edx, DWORD PTR [132+esp]                      ;253.25
        jmp       .B1.94        ; Prob 100%                     ;253.25
                                ; LOE edx ecx ebx
.B1.92:                         ; Preds .B1.90
        cmp       eax, 1                                        ;254.49
        jne       .B1.94        ; Prob 84%                      ;254.49
                                ; LOE edx ecx ebx
.B1.93:                         ; Preds .B1.92
        mov       edi, DWORD PTR [112+esp]                      ;255.25
        mov       esi, DWORD PTR [116+esp]                      ;255.25
        imul      esi, edi                                      ;255.25
        mov       DWORD PTR [132+esp], edx                      ;
        mov       edx, DWORD PTR [104+esp]                      ;255.43
        mov       DWORD PTR [136+esp], ecx                      ;
        lea       ecx, DWORD PTR [edi+edi*2]                    ;255.43
        shl       edx, 2                                        ;255.25
        sub       ecx, esi                                      ;255.25
        mov       eax, DWORD PTR [72+esp]                       ;255.25
        shl       edi, 2                                        ;256.43
        sub       eax, edx                                      ;255.25
        add       ecx, eax                                      ;255.25
        sub       edi, esi                                      ;256.25
        mov       edx, DWORD PTR [280+esp]                      ;255.25
        add       eax, edi                                      ;256.25
        inc       DWORD PTR [ecx+edx*4]                         ;255.25
        dec       DWORD PTR [eax+edx*4]                         ;256.25
        mov       ecx, DWORD PTR [136+esp]                      ;256.25
        mov       edx, DWORD PTR [132+esp]                      ;256.25
                                ; LOE edx ecx ebx
.B1.94:                         ; Preds .B1.92 .B1.91 .B1.93
        mov       BYTE PTR [237+ecx+edx], 2                     ;258.21
        lea       esi, DWORD PTR [280+esp]                      ;261.26
        push      DWORD PTR [8+ebp]                             ;261.26
        lea       eax, DWORD PTR [320+esp]                      ;261.26
        push      eax                                           ;261.26
        push      esi                                           ;261.26
        call      _CAL_ACCUMU_STAT                              ;261.26
                                ; LOE ebx esi
.B1.95:                         ; Preds .B1.94
        lea       eax, DWORD PTR [328+esp]                      ;264.26
        push      eax                                           ;264.26
        push      esi                                           ;264.26
        call      _DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST_REMOVE ;264.26
                                ; LOE ebx
.B1.306:                        ; Preds .B1.95
        add       esp, 20                                       ;264.26
                                ; LOE ebx
.B1.96:                         ; Preds .B1.306
        movss     xmm0, DWORD PTR [_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT] ;266.37
        mulss     xmm0, DWORD PTR [184+esp]                     ;266.31
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;267.21
        cvttss2si edx, xmm0                                     ;266.31
        neg       eax                                           ;267.21
        lea       edx, DWORD PTR [4+edx*4]                      ;266.21
        add       eax, DWORD PTR [280+esp]                      ;267.21
        imul      eax, eax, 900                                 ;267.21
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;267.21
        mov       esi, DWORD PTR [560+ecx+eax]                  ;267.65
        shl       esi, 2                                        ;267.21
        mov       edi, DWORD PTR [528+ecx+eax]                  ;267.65
        neg       esi                                           ;267.21
        add       edi, edx                                      ;267.21
        inc       DWORD PTR [esi+edi]                           ;267.21
        mov       edi, DWORD PTR [316+esp]                      ;268.24
        sub       edi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;268.55
        shl       edi, 5                                        ;268.55
        mov       esi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;268.21
        movsx     esi, BYTE PTR [5+esi+edi]                     ;268.24
        cmp       esi, 2                                        ;268.55
        je        .B1.237       ; Prob 16%                      ;268.55
                                ; LOE eax edx ecx ebx esi
.B1.97:                         ; Preds .B1.96
        cmp       esi, 3                                        ;269.55
        jne       .B1.99        ; Prob 84%                      ;269.55
                                ; LOE eax edx ecx ebx
.B1.98:                         ; Preds .B1.97
        mov       esi, DWORD PTR [600+ecx+eax]                  ;269.106
        add       edx, esi                                      ;269.61
        mov       eax, DWORD PTR [632+ecx+eax]                  ;269.106
        shl       eax, 2                                        ;269.61
        neg       eax                                           ;269.61
        inc       DWORD PTR [eax+edx]                           ;269.61
                                ; LOE ebx
.B1.99:                         ; Preds .B1.237 .B1.97 .B1.98
        lea       eax, DWORD PTR [316+esp]                      ;271.26
        push      eax                                           ;271.26
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_A_REMOVE ;271.26
                                ; LOE ebx
.B1.100:                        ; Preds .B1.99
        lea       eax, DWORD PTR [320+esp]                      ;272.26
        push      eax                                           ;272.26
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_P_REMOVE ;272.26
                                ; LOE ebx
.B1.307:                        ; Preds .B1.100
        add       esp, 8                                        ;272.26
                                ; LOE ebx
.B1.101:                        ; Preds .B1.307
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;273.21
        neg       eax                                           ;273.24
        mov       edx, DWORD PTR [316+esp]                      ;273.24
        add       eax, edx                                      ;273.24
        shl       eax, 8                                        ;273.24
        mov       esi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;273.21
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;274.25
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;274.25
        neg       ecx                                           ;
        mov       DWORD PTR [196+esp], edi                      ;274.25
        mov       edi, DWORD PTR [280+esp]                      ;274.28
        add       ecx, edi                                      ;
        mov       DWORD PTR [188+esp], edx                      ;273.24
        imul      edx, ecx, 900                                 ;
        mov       DWORD PTR [192+esp], esi                      ;273.21
        mov       DWORD PTR [284+esp], edi                      ;274.28
        test      BYTE PTR [112+esi+eax], 1                     ;273.24
        je        .B1.106       ; Prob 60%                      ;273.24
                                ; LOE eax edx ebx
.B1.102:                        ; Preds .B1.101
        mov       ecx, DWORD PTR [196+esp]                      ;274.28
        mov       esi, DWORD PTR [400+ecx+edx]                  ;274.28
        test      esi, esi                                      ;274.69
        mov       DWORD PTR [128+esp], esi                      ;274.28
        jle       .B1.105       ; Prob 16%                      ;274.69
                                ; LOE eax edx ebx
.B1.103:                        ; Preds .B1.102
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;274.69
        neg       ecx                                           ;274.72
        add       ecx, DWORD PTR [284+esp]                      ;274.72
        imul      edi, ecx, 152                                 ;274.72
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;274.69
        test      BYTE PTR [esi+edi], 1                         ;274.82
        jne       .B1.105       ; Prob 40%                      ;274.82
                                ; LOE eax edx ebx
.B1.104:                        ; Preds .B1.103
        mov       esi, DWORD PTR [196+esp]                      ;275.29
        mov       ecx, DWORD PTR [128+esp]                      ;275.29
        dec       ecx                                           ;275.29
        mov       DWORD PTR [400+esi+edx], ecx                  ;275.29
                                ; LOE eax edx ebx
.B1.105:                        ; Preds .B1.103 .B1.102 .B1.104
        mov       ecx, DWORD PTR [192+esp]                      ;277.25
        mov       DWORD PTR [112+ecx+eax], 0                    ;277.25
                                ; LOE eax edx ebx
.B1.106:                        ; Preds .B1.101 .B1.105
        mov       edi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;280.21
        neg       edi                                           ;280.98
        add       edi, DWORD PTR [188+esp]                      ;280.98
        shl       edi, 5                                        ;280.98
        mov       ecx, DWORD PTR [196+esp]                      ;279.15
        mov       esi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;280.21
        dec       DWORD PTR [640+ecx+edx]                       ;279.15
        movsx     ecx, BYTE PTR [5+esi+edi]                     ;280.24
        cmp       ecx, 2                                        ;280.55
        je        .B1.109       ; Prob 16%                      ;280.55
                                ; LOE eax edx ecx ebx
.B1.107:                        ; Preds .B1.106
        cmp       ecx, 5                                        ;280.94
        je        .B1.109       ; Prob 16%                      ;280.94
                                ; LOE eax edx ecx ebx
.B1.108:                        ; Preds .B1.107
        cmp       ecx, 7                                        ;280.133
        jne       .B1.110       ; Prob 84%                      ;280.133
                                ; LOE eax edx ebx
.B1.109:                        ; Preds .B1.108 .B1.107 .B1.106
        mov       ecx, DWORD PTR [196+esp]                      ;281.19
        dec       DWORD PTR [644+ecx+edx]                       ;281.19
                                ; LOE eax edx ebx
.B1.110:                        ; Preds .B1.108 .B1.109
        mov       edi, DWORD PTR [196+esp]                      ;283.18
        mov       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE] ;294.15
        dec       DWORD PTR [396+edi+edx]                       ;283.18
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE+32] ;294.15
        neg       edx                                           ;294.15
        add       edx, DWORD PTR [188+esp]                      ;294.15
        mov       edi, DWORD PTR [192+esp]                      ;293.18
        shl       edx, 6                                        ;294.15
        movsx     esi, BYTE PTR [40+edi+eax]                    ;294.15
        mov       DWORD PTR [240+edi+eax], 0                    ;293.18
        sub       esi, DWORD PTR [196+edi+eax]                  ;294.15
        mov       edi, DWORD PTR [164+edi+eax]                  ;294.15
        movzx     eax, WORD PTR [12+ecx+edx]                    ;294.15
        mov       edx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST+32] ;380.9
        neg       edx                                           ;
        add       edx, DWORD PTR [284+esp]                      ;
        mov       WORD PTR [edi+esi*2], ax                      ;294.15
        imul      edi, edx, 44                                  ;
        add       edi, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST] ;
        mov       esi, DWORD PTR [36+edi]                       ;380.19
        jmp       .B1.142       ; Prob 100%                     ;380.19
                                ; LOE ebx esi edi
.B1.111:                        ; Preds .B1.89
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_READHISTARR], 0 ;300.36
        jle       .B1.113       ; Prob 16%                      ;300.36
                                ; LOE edx ebx esi
.B1.112:                        ; Preds .B1.111
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_RUNMODE], 2 ;300.52
        je        .B1.238       ; Prob 16%                      ;300.52
                                ; LOE edx ebx esi
.B1.113:                        ; Preds .B1.111 .B1.238 .B1.239 .B1.240 .B1.242
                                ;       .B1.243 .B1.244 .B1.245 .B1.112 .B1.249
                                ;       .B1.248
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS_NUM], 0 ;324.32
        jle       .B1.127       ; Prob 16%                      ;324.32
                                ; LOE edx ebx
.B1.114:                        ; Preds .B1.113
        imul      ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32], 152 ;325.72
        imul      esi, DWORD PTR [280+esp], 152                 ;325.72
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;325.25
        mov       DWORD PTR [152+esp], edi                      ;325.25
        sub       edi, ecx                                      ;325.72
        mov       DWORD PTR [144+esp], esi                      ;325.72
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NODEWVMS+32] ;325.25
        mov       DWORD PTR [148+esp], ecx                      ;325.72
        mov       edi, DWORD PTR [24+edi+esi]                   ;325.28
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NODEWVMS] ;325.72
        mov       ecx, esi                                      ;325.72
        shl       eax, 2                                        ;325.72
        sub       ecx, eax                                      ;325.72
        mov       ecx, DWORD PTR [ecx+edi*4]                    ;325.28
        test      ecx, ecx                                      ;325.72
        jle       .B1.127       ; Prob 16%                      ;325.72
                                ; LOE eax edx ecx ebx esi
.B1.115:                        ; Preds .B1.114
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS_START+32] ;326.29
        shl       edi, 2                                        ;326.91
        neg       edi                                           ;326.91
        add       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS_START] ;326.91
        movss     xmm0, DWORD PTR [252+esp]                     ;326.34
        comiss    xmm0, DWORD PTR [edi+ecx*4]                   ;326.34
        jb        .B1.127       ; Prob 50%                      ;326.34
                                ; LOE eax edx ecx ebx esi
.B1.116:                        ; Preds .B1.115
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS_END+32] ;326.34
        shl       edi, 2                                        ;326.91
        neg       edi                                           ;326.91
        add       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS_END] ;326.91
        movss     xmm0, DWORD PTR [edi+ecx*4]                   ;326.101
        comiss    xmm0, DWORD PTR [252+esp]                     ;326.98
        jb        .B1.127       ; Prob 50%                      ;326.98
                                ; LOE eax edx ecx ebx esi
.B1.117:                        ; Preds .B1.116
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPE+32] ;327.46
        shl       edi, 2                                        ;327.46
        neg       edi                                           ;327.46
        add       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMSTYPE] ;327.46
        mov       ecx, DWORD PTR [edi+ecx*4]                    ;327.46
        cmp       ecx, 1                                        ;327.46
        je        .B1.127       ; Prob 20%                      ;327.46
                                ; LOE eax edx ecx ebx esi
.B1.118:                        ; Preds .B1.117
        cmp       ecx, 2                                        ;327.46
        je        .B1.125       ; Prob 25%                      ;327.46
                                ; LOE eax edx ecx ebx esi
.B1.119:                        ; Preds .B1.118
        cmp       ecx, 3                                        ;327.46
        jne       .B1.122       ; Prob 67%                      ;327.46
                                ; LOE eax edx ecx ebx esi
.B1.120:                        ; Preds .B1.119
        mov       ecx, DWORD PTR [152+esp]                      ;333.39
        mov       edx, DWORD PTR [148+esp]                      ;333.39
        neg       edx                                           ;333.39
        add       ecx, DWORD PTR [144+esp]                      ;333.39
        mov       edi, DWORD PTR [24+edx+ecx]                   ;333.39
        lea       esi, DWORD PTR [esi+edi*4]                    ;333.39
        sub       esi, eax                                      ;333.39
        push      esi                                           ;333.39
        lea       eax, DWORD PTR [320+esp]                      ;333.39
        push      eax                                           ;333.39
        lea       eax, DWORD PTR [288+esp]                      ;333.39
        push      eax                                           ;333.39
        push      DWORD PTR [8+ebp]                             ;333.39
        call      _VARIABLE_MESSAGE_SIGN_MODULE_mp_VMS_DIVERSION ;333.39
        jmp       .B1.310       ; Prob 100%                     ;333.39
                                ; LOE ebx
.B1.122:                        ; Preds .B1.119
        cmp       ecx, 5                                        ;327.46
        jne       .B1.127       ; Prob 50%                      ;327.46
                                ; LOE eax edx ebx esi
.B1.123:                        ; Preds .B1.122
        mov       ecx, DWORD PTR [152+esp]                      ;335.39
        mov       edx, DWORD PTR [148+esp]                      ;335.39
        neg       edx                                           ;335.39
        add       ecx, DWORD PTR [144+esp]                      ;335.39
        mov       edi, DWORD PTR [24+edx+ecx]                   ;335.39
        lea       esi, DWORD PTR [esi+edi*4]                    ;335.39
        sub       esi, eax                                      ;335.39
        push      esi                                           ;335.39
        lea       eax, DWORD PTR [320+esp]                      ;335.39
        push      eax                                           ;335.39
        lea       eax, DWORD PTR [288+esp]                      ;335.39
        push      eax                                           ;335.39
        push      DWORD PTR [8+ebp]                             ;335.39
        call      _VARIABLE_MESSAGE_SIGN_MODULE_mp_VMS_MULTI    ;335.39
        jmp       .B1.310       ; Prob 100%                     ;335.39
                                ; LOE ebx
.B1.125:                        ; Preds .B1.118
        mov       ecx, DWORD PTR [152+esp]                      ;331.39
        mov       edx, DWORD PTR [148+esp]                      ;331.39
        neg       edx                                           ;331.39
        add       ecx, DWORD PTR [144+esp]                      ;331.39
        mov       edi, DWORD PTR [24+edx+ecx]                   ;331.39
        lea       esi, DWORD PTR [esi+edi*4]                    ;331.39
        sub       esi, eax                                      ;331.39
        push      esi                                           ;331.39
        lea       eax, DWORD PTR [320+esp]                      ;331.39
        push      eax                                           ;331.39
        lea       eax, DWORD PTR [288+esp]                      ;331.39
        push      eax                                           ;331.39
        push      DWORD PTR [8+ebp]                             ;331.39
        call      _VARIABLE_MESSAGE_SIGN_MODULE_mp_VMS_DETOUR   ;331.39
                                ; LOE ebx
.B1.310:                        ; Preds .B1.123 .B1.120 .B1.125
        add       esp, 16                                       ;331.39
                                ; LOE ebx
.B1.126:                        ; Preds .B1.310
        mov       edx, DWORD PTR [316+esp]                      ;342.51
                                ; LOE edx ebx
.B1.127:                        ; Preds .B1.126 .B1.122 .B1.117 .B1.116 .B1.115
                                ;       .B1.114 .B1.113
        sub       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE+32] ;342.26
        shl       edx, 6                                        ;342.26
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE] ;342.26
        lea       esi, DWORD PTR [12+edx+eax]                   ;342.26
        lea       edx, DWORD PTR [168+esp]                      ;342.26
        push      edx                                           ;342.26
        lea       ecx, DWORD PTR [168+esp]                      ;342.26
        push      ecx                                           ;342.26
        push      esi                                           ;342.26
        lea       edi, DWORD PTR [292+esp]                      ;342.26
        push      edi                                           ;342.26
        lea       eax, DWORD PTR [332+esp]                      ;342.26
        push      eax                                           ;342.26
        push      DWORD PTR [8+ebp]                             ;342.26
        call      _RETRIEVE_NEXT_LINK                           ;342.26
                                ; LOE ebx
.B1.311:                        ; Preds .B1.127
        add       esp, 24                                       ;342.26
                                ; LOE ebx
.B1.128:                        ; Preds .B1.311
        mov       eax, DWORD PTR [316+esp]                      ;343.26
        sub       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;343.21
        shl       eax, 8                                        ;343.21
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;343.21
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;346.18
        neg       edi                                           ;346.60
        mov       ecx, DWORD PTR [148+edx+eax]                  ;343.26
        shl       ecx, 2                                        ;343.21
        neg       ecx                                           ;343.21
        add       ecx, DWORD PTR [116+edx+eax]                  ;343.21
        mov       DWORD PTR [20+edx+eax], -1                    ;344.21
        mov       esi, DWORD PTR [4+ecx]                        ;343.21
        add       edi, esi                                      ;346.60
        mov       DWORD PTR [200+esp], esi                      ;343.21
        imul      esi, edi, 152                                 ;346.60
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;346.18
        movsx     edi, WORD PTR [148+ecx+esi]                   ;346.21
        cmp       edi, 6                                        ;346.60
        jne       .B1.130       ; Prob 84%                      ;346.60
                                ; LOE eax edx ebx
.B1.129:                        ; Preds .B1.128
        mov       DWORD PTR [36+edx+eax], -1                    ;346.66
                                ; LOE eax edx ebx
.B1.130:                        ; Preds .B1.128 .B1.129
        movss     xmm0, DWORD PTR [_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT] ;351.26
        lea       edi, DWORD PTR [176+esp]                      ;351.26
        movss     xmm1, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TIME_NOW] ;351.26
        lea       esi, DWORD PTR [200+esp]                      ;351.26
        subss     xmm0, DWORD PTR [152+edx+eax]                 ;351.68
        mulss     xmm0, DWORD PTR [_2il0floatpacket.9]          ;351.93
        lea       edx, DWORD PTR [172+esp]                      ;351.26
        subss     xmm1, xmm0                                    ;351.26
        movss     DWORD PTR [172+esp], xmm1                     ;351.26
        push      edi                                           ;351.26
        lea       eax, DWORD PTR [320+esp]                      ;351.26
        push      eax                                           ;351.26
        push      edx                                           ;351.26
        push      esi                                           ;351.26
        push      DWORD PTR [8+ebp]                             ;351.26
        call      _DYNUST_TRANSLINK_MODULE_mp_TRANLINK_SCAN     ;351.26
                                ; LOE ebx esi edi
.B1.312:                        ; Preds .B1.130
        add       esp, 20                                       ;351.26
                                ; LOE ebx esi edi
.B1.131:                        ; Preds .B1.312
        cmp       DWORD PTR [176+esp], 0                        ;352.33
        jg        .B1.133       ; Prob 60%                      ;352.33
                                ; LOE ebx esi edi
.B1.132:                        ; Preds .B1.131
        mov       eax, DWORD PTR [280+esp]                      ;380.19
        mov       DWORD PTR [_SIM_MOVE_VEHICLES$FLAG.0.1], -1   ;353.25
        mov       DWORD PTR [284+esp], eax                      ;380.19
        mov       DWORD PTR [180+esp], -1                       ;
        jmp       .B1.138       ; Prob 100%                     ;
                                ; LOE ebx
.B1.133:                        ; Preds .B1.131
        lea       eax, DWORD PTR [316+esp]                      ;356.26
        push      eax                                           ;356.26
        push      OFFSET FLAT: __NLITPACK_3.0.1                 ;356.26
        push      edi                                           ;356.26
        push      esi                                           ;356.26
        call      _DYNUST_TRANSLINK_MODULE_mp_TRANLINK_INSERT   ;356.26
                                ; LOE ebx esi edi
.B1.134:                        ; Preds .B1.133
        lea       eax, DWORD PTR [296+esp]                      ;357.26
        push      eax                                           ;357.26
        push      OFFSET FLAT: __NLITPACK_4.0.1                 ;357.26
        push      edi                                           ;357.26
        push      esi                                           ;357.26
        call      _DYNUST_TRANSLINK_MODULE_mp_TRANLINK_INSERT   ;357.26
                                ; LOE ebx esi edi
.B1.135:                        ; Preds .B1.134
        mov       edx, DWORD PTR [348+esp]                      ;358.56
        sub       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;358.56
        shl       edx, 8                                        ;358.56
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;358.56
        movss     xmm1, DWORD PTR [_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT] ;358.56
        movss     xmm4, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TIME_NOW] ;358.56
        movss     xmm0, DWORD PTR [_2il0floatpacket.13]         ;358.56
        movss     xmm2, DWORD PTR [_2il0floatpacket.14]         ;358.56
        movss     xmm5, DWORD PTR [_2il0floatpacket.15]         ;358.56
        movaps    xmm7, xmm5                                    ;358.56
        subss     xmm1, DWORD PTR [152+eax+edx]                 ;358.56
        addss     xmm7, xmm5                                    ;358.56
        mulss     xmm1, DWORD PTR [_2il0floatpacket.9]          ;358.56
        subss     xmm4, xmm1                                    ;358.56
        mulss     xmm4, DWORD PTR [_2il0floatpacket.8]          ;358.56
        lea       eax, DWORD PTR [172+esp]                      ;358.26
        andps     xmm0, xmm4                                    ;358.56
        pxor      xmm4, xmm0                                    ;358.56
        movaps    xmm3, xmm4                                    ;358.56
        movaps    xmm1, xmm4                                    ;358.56
        cmpltss   xmm3, xmm2                                    ;358.56
        andps     xmm2, xmm3                                    ;358.56
        addss     xmm1, xmm2                                    ;358.56
        subss     xmm1, xmm2                                    ;358.56
        movaps    xmm2, xmm1                                    ;358.56
        subss     xmm2, xmm4                                    ;358.56
        movaps    xmm6, xmm2                                    ;358.56
        cmpless   xmm2, DWORD PTR [_2il0floatpacket.16]         ;358.56
        cmpnless  xmm6, xmm5                                    ;358.56
        andps     xmm6, xmm7                                    ;358.56
        andps     xmm2, xmm7                                    ;358.56
        subss     xmm1, xmm6                                    ;358.56
        addss     xmm1, xmm2                                    ;358.56
        orps      xmm1, xmm0                                    ;358.56
        cvtss2si  ecx, xmm1                                     ;358.26
        mov       DWORD PTR [172+esp], ecx                      ;358.26
        push      eax                                           ;358.26
        push      OFFSET FLAT: __NLITPACK_5.0.1                 ;358.26
        push      edi                                           ;358.26
        push      esi                                           ;358.26
        call      _DYNUST_TRANSLINK_MODULE_mp_TRANLINK_INSERT   ;358.26
                                ; LOE ebx esi
.B1.136:                        ; Preds .B1.135
        push      esi                                           ;359.26
        call      _DYNUST_TRANSLINK_MODULE_mp_TRANLINK_AVSIZEINCRE ;359.26
                                ; LOE ebx
.B1.313:                        ; Preds .B1.136
        add       esp, 52                                       ;359.26
                                ; LOE ebx
.B1.137:                        ; Preds .B1.313
        mov       edx, DWORD PTR [280+esp]                      ;361.66
        imul      eax, DWORD PTR [200+esp], 900                 ;360.21
        imul      ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32], 900 ;360.21
        imul      esi, edx, 900                                 ;361.21
        sub       eax, ecx                                      ;360.21
        sub       esi, ecx                                      ;361.21
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;360.21
        mov       DWORD PTR [284+esp], edx                      ;361.66
        inc       DWORD PTR [700+edi+eax]                       ;360.21
        inc       DWORD PTR [704+esi+edi]                       ;361.21
                                ; LOE ebx
.B1.138:                        ; Preds .B1.132 .B1.137
        test      BYTE PTR [180+esp], 1                         ;364.24
        je        .B1.140       ; Prob 60%                      ;364.24
                                ; LOE ebx
.B1.139:                        ; Preds .B1.138
        mov       eax, DWORD PTR [284+esp]                      ;
        sub       eax, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST+32] ;
        imul      edi, eax, 44                                  ;
        add       edi, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST] ;
        mov       DWORD PTR [_SIM_MOVE_VEHICLES$FLAG.0.1], 0    ;365.25
        mov       DWORD PTR [180+esp], 0                        ;
        mov       esi, DWORD PTR [36+edi]                       ;380.19
        jmp       .B1.142       ; Prob 100%                     ;380.19
                                ; LOE ebx esi edi
.B1.140:                        ; Preds .B1.138
        mov       eax, DWORD PTR [284+esp]                      ;
        sub       eax, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST+32] ;
        imul      edi, eax, 44                                  ;
        add       edi, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST] ;
        mov       esi, DWORD PTR [36+edi]                       ;380.19
        jmp       .B1.142       ; Prob 100%                     ;380.19
                                ; LOE ebx esi edi
.B1.141:                        ; Preds .B1.25
        mov       DWORD PTR [152+edx+ecx], 0                    ;375.13
        pxor      xmm0, xmm0                                    ;376.13
        maxss     xmm0, DWORD PTR [_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT] ;376.13
        movss     DWORD PTR [100+edx+ecx], xmm0                 ;376.13
                                ; LOE ebx esi edi
.B1.142:                        ; Preds .B1.57 .B1.53 .B1.49 .B1.45 .B1.110
                                ;       .B1.139 .B1.140 .B1.87 .B1.141
        inc       ebx                                           ;379.9
        cmp       ebx, esi                                      ;380.17
        jg        .B1.145       ; Prob 1%                       ;380.17
                                ; LOE ebx esi edi
.B1.143:                        ; Preds .B1.142
        cmp       ebx, 100000                                   ;381.17
        jg        .B1.272       ; Prob 5%                       ;381.17
                                ; LOE ebx esi edi
.B1.144:                        ; Preds .B1.321 .B1.143
        mov       edx, DWORD PTR [32+edi]                       ;93.15
        neg       edx                                           ;93.5
        add       edx, ebx                                      ;93.5
        imul      edx, DWORD PTR [28+edi]                       ;93.5
        mov       eax, DWORD PTR [edi]                          ;93.15
        cmp       DWORD PTR [eax+edx], 0                        ;93.38
        jg        .B1.25        ; Prob 82%                      ;93.38
                                ; LOE ebx esi edi
.B1.145:                        ; Preds .B1.142 .B1.144
        mov       edx, DWORD PTR [284+esp]                      ;
                                ; LOE edx
.B1.146:                        ; Preds .B1.145 .B1.23 .B1.22 .B1.21
        inc       edx                                           ;77.4
        mov       eax, edx                                      ;77.4
        cmp       edx, DWORD PTR [228+esp]                      ;77.4
        jle       .B1.21        ; Prob 82%                      ;77.4
                                ; LOE eax edx
.B1.147:                        ; Preds .B1.146
        mov       DWORD PTR [280+esp], edx                      ;77.4
                                ; LOE
.B1.148:                        ; Preds .B1.147 .B1.274
        mov       edx, DWORD PTR [96+esp]                       ;402.29
        test      edx, edx                                      ;402.29
        mov       eax, DWORD PTR [72+esp]                       ;407.1
        jle       .B1.262       ; Prob 10%                      ;402.29
                                ; LOE eax edx
.B1.149:                        ; Preds .B1.148
        mov       ecx, DWORD PTR [112+esp]                      ;402.29
        cmp       edx, 4                                        ;402.29
        mov       ebx, DWORD PTR [116+esp]                      ;402.29
        mov       DWORD PTR [12+esp], ecx                       ;402.29
        mov       DWORD PTR [4+esp], ebx                        ;402.29
        jl        .B1.254       ; Prob 10%                      ;402.29
                                ; LOE eax edx ecx cl ch
.B1.150:                        ; Preds .B1.149
        mov       esi, ecx                                      ;402.29
        imul      esi, DWORD PTR [4+esp]                        ;402.29
        lea       ebx, DWORD PTR [eax+ecx]                      ;402.29
        sub       ebx, esi                                      ;402.29
        and       ebx, 15                                       ;402.29
        je        .B1.153       ; Prob 50%                      ;402.29
                                ; LOE eax edx ebx esi
.B1.151:                        ; Preds .B1.150
        test      bl, 3                                         ;402.29
        jne       .B1.254       ; Prob 10%                      ;402.29
                                ; LOE eax edx ebx esi
.B1.152:                        ; Preds .B1.151
        neg       ebx                                           ;402.29
        add       ebx, 16                                       ;402.29
        shr       ebx, 2                                        ;402.29
                                ; LOE eax edx ebx esi
.B1.153:                        ; Preds .B1.152 .B1.150
        lea       ecx, DWORD PTR [4+ebx]                        ;402.29
        cmp       edx, ecx                                      ;402.29
        jl        .B1.254       ; Prob 10%                      ;402.29
                                ; LOE eax edx ebx esi
.B1.154:                        ; Preds .B1.153
        mov       ecx, edx                                      ;402.29
        neg       esi                                           ;
        sub       ecx, ebx                                      ;402.29
        xor       edi, edi                                      ;
        and       ecx, 3                                        ;402.29
        add       esi, DWORD PTR [12+esp]                       ;
        neg       ecx                                           ;402.29
        add       ecx, edx                                      ;402.29
        add       esi, eax                                      ;
        mov       DWORD PTR [48+esp], edi                       ;
        test      ebx, ebx                                      ;402.29
        jbe       .B1.158       ; Prob 0%                       ;402.29
                                ; LOE eax edx ecx ebx esi
.B1.155:                        ; Preds .B1.154
        xor       edi, edi                                      ;
        mov       DWORD PTR [esp], edx                          ;
        mov       edx, edi                                      ;
        mov       edi, DWORD PTR [48+esp]                       ;
                                ; LOE eax edx ecx ebx esi edi
.B1.156:                        ; Preds .B1.156 .B1.155
        add       edi, DWORD PTR [esi+edx*4]                    ;402.29
        inc       edx                                           ;402.29
        cmp       edx, ebx                                      ;402.29
        jb        .B1.156       ; Prob 82%                      ;402.29
                                ; LOE eax edx ecx ebx esi edi
.B1.157:                        ; Preds .B1.156
        mov       DWORD PTR [48+esp], edi                       ;
        mov       edx, DWORD PTR [esp]                          ;
                                ; LOE eax edx ecx ebx esi
.B1.158:                        ; Preds .B1.154 .B1.157
        movd      xmm0, DWORD PTR [48+esp]                      ;402.29
                                ; LOE eax edx ecx ebx esi xmm0
.B1.159:                        ; Preds .B1.159 .B1.158
        paddd     xmm0, XMMWORD PTR [esi+ebx*4]                 ;402.29
        add       ebx, 4                                        ;402.29
        cmp       ebx, ecx                                      ;402.29
        jb        .B1.159       ; Prob 82%                      ;402.29
                                ; LOE eax edx ecx ebx esi xmm0
.B1.160:                        ; Preds .B1.159
        movdqa    xmm1, xmm0                                    ;402.29
        psrldq    xmm1, 8                                       ;402.29
        paddd     xmm0, xmm1                                    ;402.29
        movdqa    xmm2, xmm0                                    ;402.29
        psrldq    xmm2, 4                                       ;402.29
        paddd     xmm0, xmm2                                    ;402.29
        movd      esi, xmm0                                     ;402.29
                                ; LOE eax edx ecx esi
.B1.161:                        ; Preds .B1.160 .B1.254
        cmp       ecx, edx                                      ;402.29
        jae       .B1.165       ; Prob 0%                       ;402.29
                                ; LOE eax edx ecx esi
.B1.162:                        ; Preds .B1.161
        mov       edi, DWORD PTR [12+esp]                       ;
        mov       ebx, edi                                      ;
        imul      ebx, DWORD PTR [4+esp]                        ;
        neg       ebx                                           ;
        add       ebx, edi                                      ;
        add       ebx, eax                                      ;
                                ; LOE eax edx ecx ebx esi
.B1.163:                        ; Preds .B1.163 .B1.162
        add       esi, DWORD PTR [ebx+ecx*4]                    ;402.29
        inc       ecx                                           ;402.29
        cmp       ecx, edx                                      ;402.29
        jb        .B1.163       ; Prob 82%                      ;402.29
                                ; LOE eax edx ecx ebx esi
.B1.165:                        ; Preds .B1.163 .B1.161
        add       DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOUT_NONTAG], esi ;402.1
        test      edx, edx                                      ;403.21
        jle       .B1.262       ; Prob 0%                       ;403.21
                                ; LOE eax edx
.B1.166:                        ; Preds .B1.165
        cmp       edx, 4                                        ;403.21
        jl        .B1.255       ; Prob 10%                      ;403.21
                                ; LOE eax edx
.B1.167:                        ; Preds .B1.166
        mov       ebx, DWORD PTR [12+esp]                       ;403.21
        mov       ecx, ebx                                      ;403.21
        imul      ecx, DWORD PTR [4+esp]                        ;403.21
        mov       DWORD PTR [48+esp], ecx                       ;403.21
        lea       esi, DWORD PTR [ebx+ebx]                      ;403.21
        lea       ebx, DWORD PTR [eax+ebx*2]                    ;403.21
        sub       ebx, ecx                                      ;403.21
        and       ebx, 15                                       ;403.21
        je        .B1.170       ; Prob 50%                      ;403.21
                                ; LOE eax edx ebx esi
.B1.168:                        ; Preds .B1.167
        test      bl, 3                                         ;403.21
        jne       .B1.255       ; Prob 10%                      ;403.21
                                ; LOE eax edx ebx esi
.B1.169:                        ; Preds .B1.168
        neg       ebx                                           ;403.21
        add       ebx, 16                                       ;403.21
        shr       ebx, 2                                        ;403.21
                                ; LOE eax edx ebx esi
.B1.170:                        ; Preds .B1.169 .B1.167
        lea       ecx, DWORD PTR [4+ebx]                        ;403.21
        cmp       edx, ecx                                      ;403.21
        jl        .B1.255       ; Prob 10%                      ;403.21
                                ; LOE eax edx ebx esi
.B1.171:                        ; Preds .B1.170
        mov       ecx, edx                                      ;403.21
        xor       edi, edi                                      ;
        sub       ecx, ebx                                      ;403.21
        and       ecx, 3                                        ;403.21
        sub       esi, DWORD PTR [48+esp]                       ;
        neg       ecx                                           ;403.21
        add       ecx, edx                                      ;403.21
        add       esi, eax                                      ;
        mov       DWORD PTR [52+esp], edi                       ;
        test      ebx, ebx                                      ;403.21
        jbe       .B1.175       ; Prob 10%                      ;403.21
                                ; LOE eax edx ecx ebx esi
.B1.172:                        ; Preds .B1.171
        xor       edi, edi                                      ;
        mov       DWORD PTR [esp], edx                          ;
        mov       edx, edi                                      ;
        mov       edi, DWORD PTR [52+esp]                       ;
                                ; LOE eax edx ecx ebx esi edi
.B1.173:                        ; Preds .B1.173 .B1.172
        add       edi, DWORD PTR [esi+edx*4]                    ;403.21
        inc       edx                                           ;403.21
        cmp       edx, ebx                                      ;403.21
        jb        .B1.173       ; Prob 82%                      ;403.21
                                ; LOE eax edx ecx ebx esi edi
.B1.174:                        ; Preds .B1.173
        mov       DWORD PTR [52+esp], edi                       ;
        mov       edx, DWORD PTR [esp]                          ;
                                ; LOE eax edx ecx ebx esi
.B1.175:                        ; Preds .B1.171 .B1.174
        movd      xmm0, DWORD PTR [52+esp]                      ;403.21
                                ; LOE eax edx ecx ebx esi xmm0
.B1.176:                        ; Preds .B1.176 .B1.175
        paddd     xmm0, XMMWORD PTR [esi+ebx*4]                 ;403.21
        add       ebx, 4                                        ;403.21
        cmp       ebx, ecx                                      ;403.21
        jb        .B1.176       ; Prob 82%                      ;403.21
                                ; LOE eax edx ecx ebx esi xmm0
.B1.177:                        ; Preds .B1.176
        movdqa    xmm1, xmm0                                    ;403.21
        psrldq    xmm1, 8                                       ;403.21
        paddd     xmm0, xmm1                                    ;403.21
        movdqa    xmm2, xmm0                                    ;403.21
        psrldq    xmm2, 4                                       ;403.21
        paddd     xmm0, xmm2                                    ;403.21
        movd      esi, xmm0                                     ;403.21
                                ; LOE eax edx ecx esi
.B1.178:                        ; Preds .B1.177 .B1.255
        cmp       ecx, edx                                      ;403.21
        jae       .B1.182       ; Prob 10%                      ;403.21
                                ; LOE eax edx ecx esi
.B1.179:                        ; Preds .B1.178
        mov       ebx, DWORD PTR [4+esp]                        ;
        neg       ebx                                           ;
        add       ebx, 2                                        ;
        imul      ebx, DWORD PTR [12+esp]                       ;
        add       ebx, eax                                      ;
                                ; LOE eax edx ecx ebx esi
.B1.180:                        ; Preds .B1.180 .B1.179
        add       esi, DWORD PTR [ebx+ecx*4]                    ;403.21
        inc       ecx                                           ;403.21
        cmp       ecx, edx                                      ;403.21
        jb        .B1.180       ; Prob 82%                      ;403.21
                                ; LOE eax edx ecx ebx esi
.B1.182:                        ; Preds .B1.180 .B1.178
        add       DWORD PTR [_DYNUST_MAIN_MODULE_mp_NUMNTAG], esi ;403.1
        test      edx, edx                                      ;404.23
        jle       .B1.262       ; Prob 0%                       ;404.23
                                ; LOE eax edx
.B1.183:                        ; Preds .B1.182
        cmp       edx, 4                                        ;404.23
        jl        .B1.256       ; Prob 10%                      ;404.23
                                ; LOE eax edx
.B1.184:                        ; Preds .B1.183
        mov       ecx, DWORD PTR [12+esp]                       ;404.23
        lea       esi, DWORD PTR [ecx+ecx*2]                    ;404.23
        imul      ecx, DWORD PTR [4+esp]                        ;404.23
        mov       DWORD PTR [48+esp], ecx                       ;404.23
        lea       ebx, DWORD PTR [eax+esi]                      ;404.23
        sub       ebx, ecx                                      ;404.23
        and       ebx, 15                                       ;404.23
        je        .B1.187       ; Prob 50%                      ;404.23
                                ; LOE eax edx ebx esi
.B1.185:                        ; Preds .B1.184
        test      bl, 3                                         ;404.23
        jne       .B1.256       ; Prob 10%                      ;404.23
                                ; LOE eax edx ebx esi
.B1.186:                        ; Preds .B1.185
        neg       ebx                                           ;404.23
        add       ebx, 16                                       ;404.23
        shr       ebx, 2                                        ;404.23
                                ; LOE eax edx ebx esi
.B1.187:                        ; Preds .B1.186 .B1.184
        lea       ecx, DWORD PTR [4+ebx]                        ;404.23
        cmp       edx, ecx                                      ;404.23
        jl        .B1.256       ; Prob 10%                      ;404.23
                                ; LOE eax edx ebx esi
.B1.188:                        ; Preds .B1.187
        mov       ecx, edx                                      ;404.23
        xor       edi, edi                                      ;
        sub       ecx, ebx                                      ;404.23
        and       ecx, 3                                        ;404.23
        sub       esi, DWORD PTR [48+esp]                       ;
        neg       ecx                                           ;404.23
        add       ecx, edx                                      ;404.23
        add       esi, eax                                      ;
        mov       DWORD PTR [52+esp], edi                       ;
        test      ebx, ebx                                      ;404.23
        jbe       .B1.192       ; Prob 10%                      ;404.23
                                ; LOE eax edx ecx ebx esi
.B1.189:                        ; Preds .B1.188
        xor       edi, edi                                      ;
        mov       DWORD PTR [esp], edx                          ;
        mov       edx, edi                                      ;
        mov       edi, DWORD PTR [52+esp]                       ;
                                ; LOE eax edx ecx ebx esi edi
.B1.190:                        ; Preds .B1.190 .B1.189
        add       edi, DWORD PTR [esi+edx*4]                    ;404.23
        inc       edx                                           ;404.23
        cmp       edx, ebx                                      ;404.23
        jb        .B1.190       ; Prob 82%                      ;404.23
                                ; LOE eax edx ecx ebx esi edi
.B1.191:                        ; Preds .B1.190
        mov       DWORD PTR [52+esp], edi                       ;
        mov       edx, DWORD PTR [esp]                          ;
                                ; LOE eax edx ecx ebx esi
.B1.192:                        ; Preds .B1.188 .B1.191
        movd      xmm0, DWORD PTR [52+esp]                      ;404.23
                                ; LOE eax edx ecx ebx esi xmm0
.B1.193:                        ; Preds .B1.193 .B1.192
        paddd     xmm0, XMMWORD PTR [esi+ebx*4]                 ;404.23
        add       ebx, 4                                        ;404.23
        cmp       ebx, ecx                                      ;404.23
        jb        .B1.193       ; Prob 82%                      ;404.23
                                ; LOE eax edx ecx ebx esi xmm0
.B1.194:                        ; Preds .B1.193
        movdqa    xmm1, xmm0                                    ;404.23
        psrldq    xmm1, 8                                       ;404.23
        paddd     xmm0, xmm1                                    ;404.23
        movdqa    xmm2, xmm0                                    ;404.23
        psrldq    xmm2, 4                                       ;404.23
        paddd     xmm0, xmm2                                    ;404.23
        movd      esi, xmm0                                     ;404.23
                                ; LOE eax edx ecx esi
.B1.195:                        ; Preds .B1.194 .B1.256
        cmp       ecx, edx                                      ;404.23
        jae       .B1.199       ; Prob 10%                      ;404.23
                                ; LOE eax edx ecx esi
.B1.196:                        ; Preds .B1.195
        mov       ebx, DWORD PTR [4+esp]                        ;
        neg       ebx                                           ;
        add       ebx, 3                                        ;
        imul      ebx, DWORD PTR [12+esp]                       ;
        add       ebx, eax                                      ;
                                ; LOE eax edx ecx ebx esi
.B1.197:                        ; Preds .B1.197 .B1.196
        add       esi, DWORD PTR [ebx+ecx*4]                    ;404.23
        inc       ecx                                           ;404.23
        cmp       ecx, edx                                      ;404.23
        jb        .B1.197       ; Prob 82%                      ;404.23
                                ; LOE eax edx ecx ebx esi
.B1.199:                        ; Preds .B1.197 .B1.195
        add       DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOUT_TAG], esi ;404.1
        test      edx, edx                                      ;405.21
        jle       .B1.262       ; Prob 0%                       ;405.21
                                ; LOE eax edx
.B1.200:                        ; Preds .B1.199
        cmp       edx, 4                                        ;405.21
        jl        .B1.257       ; Prob 10%                      ;405.21
                                ; LOE eax edx
.B1.201:                        ; Preds .B1.200
        mov       ebx, DWORD PTR [12+esp]                       ;405.21
        mov       ecx, ebx                                      ;405.21
        imul      ecx, DWORD PTR [4+esp]                        ;405.21
        mov       DWORD PTR [48+esp], ecx                       ;405.21
        lea       esi, DWORD PTR [ebx*4]                        ;405.21
        lea       ebx, DWORD PTR [eax+ebx*4]                    ;405.21
        sub       ebx, ecx                                      ;405.21
        and       ebx, 15                                       ;405.21
        je        .B1.204       ; Prob 50%                      ;405.21
                                ; LOE eax edx ebx esi
.B1.202:                        ; Preds .B1.201
        test      bl, 3                                         ;405.21
        jne       .B1.257       ; Prob 10%                      ;405.21
                                ; LOE eax edx ebx esi
.B1.203:                        ; Preds .B1.202
        neg       ebx                                           ;405.21
        add       ebx, 16                                       ;405.21
        shr       ebx, 2                                        ;405.21
                                ; LOE eax edx ebx esi
.B1.204:                        ; Preds .B1.203 .B1.201
        lea       ecx, DWORD PTR [4+ebx]                        ;405.21
        cmp       edx, ecx                                      ;405.21
        jl        .B1.257       ; Prob 10%                      ;405.21
                                ; LOE eax edx ebx esi
.B1.205:                        ; Preds .B1.204
        mov       ecx, edx                                      ;405.21
        xor       edi, edi                                      ;
        sub       ecx, ebx                                      ;405.21
        and       ecx, 3                                        ;405.21
        sub       esi, DWORD PTR [48+esp]                       ;
        neg       ecx                                           ;405.21
        add       ecx, edx                                      ;405.21
        add       esi, eax                                      ;
        mov       DWORD PTR [52+esp], edi                       ;
        test      ebx, ebx                                      ;405.21
        jbe       .B1.209       ; Prob 10%                      ;405.21
                                ; LOE eax edx ecx ebx esi
.B1.206:                        ; Preds .B1.205
        xor       edi, edi                                      ;
        mov       DWORD PTR [esp], edx                          ;
        mov       edx, edi                                      ;
        mov       edi, DWORD PTR [52+esp]                       ;
                                ; LOE eax edx ecx ebx esi edi
.B1.207:                        ; Preds .B1.207 .B1.206
        add       edi, DWORD PTR [esi+edx*4]                    ;405.21
        inc       edx                                           ;405.21
        cmp       edx, ebx                                      ;405.21
        jb        .B1.207       ; Prob 82%                      ;405.21
                                ; LOE eax edx ecx ebx esi edi
.B1.208:                        ; Preds .B1.207
        mov       DWORD PTR [52+esp], edi                       ;
        mov       edx, DWORD PTR [esp]                          ;
                                ; LOE eax edx ecx ebx esi
.B1.209:                        ; Preds .B1.205 .B1.208
        movd      xmm0, DWORD PTR [52+esp]                      ;405.21
                                ; LOE eax edx ecx ebx esi xmm0
.B1.210:                        ; Preds .B1.210 .B1.209
        paddd     xmm0, XMMWORD PTR [esi+ebx*4]                 ;405.21
        add       ebx, 4                                        ;405.21
        cmp       ebx, ecx                                      ;405.21
        jb        .B1.210       ; Prob 82%                      ;405.21
                                ; LOE eax edx ecx ebx esi xmm0
.B1.211:                        ; Preds .B1.210
        movdqa    xmm1, xmm0                                    ;405.21
        psrldq    xmm1, 8                                       ;405.21
        paddd     xmm0, xmm1                                    ;405.21
        movdqa    xmm2, xmm0                                    ;405.21
        psrldq    xmm2, 4                                       ;405.21
        paddd     xmm0, xmm2                                    ;405.21
        movd      ebx, xmm0                                     ;405.21
                                ; LOE eax edx ecx ebx
.B1.212:                        ; Preds .B1.211 .B1.257
        cmp       ecx, edx                                      ;405.21
        jae       .B1.216       ; Prob 10%                      ;405.21
                                ; LOE eax edx ecx ebx
.B1.213:                        ; Preds .B1.212
        mov       esi, DWORD PTR [4+esp]                        ;
        neg       esi                                           ;
        add       esi, 4                                        ;
        imul      esi, DWORD PTR [12+esp]                       ;
        add       esi, eax                                      ;
        mov       DWORD PTR [4+esp], esi                        ;
                                ; LOE eax edx ecx ebx esi
.B1.214:                        ; Preds .B1.214 .B1.213
        add       ebx, DWORD PTR [esi+ecx*4]                    ;405.21
        inc       ecx                                           ;405.21
        cmp       ecx, edx                                      ;405.21
        jb        .B1.214       ; Prob 82%                      ;405.21
                                ; LOE eax edx ecx ebx esi
.B1.216:                        ; Preds .B1.214 .B1.262 .B1.212
        mov       edx, DWORD PTR [84+esp]                       ;407.1
        mov       ecx, edx                                      ;407.1
        shr       ecx, 1                                        ;407.1
        and       edx, 1                                        ;407.1
        and       ecx, 1                                        ;407.1
        add       edx, edx                                      ;407.1
        shl       ecx, 2                                        ;407.1
        or        ecx, edx                                      ;407.1
        or        ecx, 262144                                   ;407.1
        push      ecx                                           ;407.1
        push      eax                                           ;407.1
        add       DWORD PTR [_DYNUST_MAIN_MODULE_mp_NUMCARS], ebx ;405.1
        call      _for_dealloc_allocatable                      ;407.1
                                ; LOE
.B1.218:                        ; Preds .B1.216
        add       esp, 332                                      ;414.1
        pop       ebx                                           ;414.1
        pop       edi                                           ;414.1
        pop       esi                                           ;414.1
        mov       esp, ebp                                      ;414.1
        pop       ebp                                           ;414.1
        ret                                                     ;414.1
                                ; LOE
.B1.219:                        ; Preds .B1.11
        mov       edi, DWORD PTR [72+esp]                       ;
        lea       ebx, DWORD PTR [eax*4]                        ;
        mov       DWORD PTR [12+esp], ebx                       ;
        xor       esi, esi                                      ;
        mov       DWORD PTR [esp], edi                          ;
        mov       ebx, ecx                                      ;
        mov       DWORD PTR [16+esp], eax                       ;
        pxor      xmm0, xmm0                                    ;62.1
        mov       DWORD PTR [4+esp], edx                        ;
        mov       DWORD PTR [8+esp], ecx                        ;
                                ; LOE ebx esi
.B1.220:                        ; Preds .B1.335 .B1.290 .B1.219
        cmp       DWORD PTR [16+esp], 24                        ;62.1
        jle       .B1.275       ; Prob 0%                       ;62.1
                                ; LOE ebx esi
.B1.221:                        ; Preds .B1.220
        mov       eax, DWORD PTR [8+esp]                        ;62.1
        neg       eax                                           ;62.1
        add       eax, ebx                                      ;62.1
        imul      eax, DWORD PTR [112+esp]                      ;62.1
        add       eax, DWORD PTR [esp]                          ;62.1
        push      DWORD PTR [12+esp]                            ;62.1
        push      0                                             ;62.1
        push      eax                                           ;62.1
        call      __intel_fast_memset                           ;62.1
                                ; LOE ebx esi
.B1.315:                        ; Preds .B1.221
        add       esp, 12                                       ;62.1
                                ; LOE ebx esi
.B1.222:                        ; Preds .B1.315
        inc       esi                                           ;62.1
        inc       ebx                                           ;62.1
        cmp       esi, DWORD PTR [4+esp]                        ;62.1
        jae       .B1.12        ; Prob 18%                      ;62.1
                                ; LOE ebx esi
.B1.335:                        ; Preds .B1.222
        mov       eax, DWORD PTR [72+esp]                       ;252.25
        mov       DWORD PTR [esp], eax                          ;252.25
        jmp       .B1.220       ; Prob 100%                     ;252.25
                                ; LOE ebx esi
.B1.224:                        ; Preds .B1.36                  ; Infreq
        movss     xmm0, DWORD PTR [252+esp]                     ;122.125
        comiss    xmm0, DWORD PTR [_DYNUST_MAIN_MODULE_mp_INFOSTARTTIME] ;122.125
        jb        .B1.38        ; Prob 78%                      ;122.125
                                ; LOE ebx xmm3 xmm4
.B1.226:                        ; Preds .B1.224                 ; Infreq
        push      OFFSET FLAT: __NLITPACK_0.0.1                 ;123.21
        movss     DWORD PTR [244+esp], xmm3                     ;123.21
        movss     DWORD PTR [248+esp], xmm4                     ;123.21
        call      _RANXY                                        ;123.21
                                ; LOE ebx f1
.B1.316:                        ; Preds .B1.226                 ; Infreq
        fstp      DWORD PTR [52+esp]                            ;123.21
        movss     xmm4, DWORD PTR [248+esp]                     ;
        movss     xmm3, DWORD PTR [244+esp]                     ;
        movss     xmm1, DWORD PTR [52+esp]                      ;123.21
        add       esp, 4                                        ;123.21
                                ; LOE ebx xmm1 xmm3 xmm4
.B1.227:                        ; Preds .B1.316                 ; Infreq
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE+32] ;124.17
        neg       edx                                           ;124.22
        mov       eax, DWORD PTR [316+esp]                      ;124.25
        add       edx, eax                                      ;124.22
        shl       edx, 6                                        ;124.22
        mov       DWORD PTR [320+esp], eax                      ;124.25
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE] ;124.17
        movss     xmm0, DWORD PTR [52+eax+edx]                  ;124.25
        comiss    xmm0, xmm1                                    ;124.22
        jb        .B1.38        ; Prob 50%                      ;124.22
                                ; LOE eax edx ebx xmm3 xmm4
.B1.228:                        ; Preds .B1.227                 ; Infreq
        movsx     eax, WORD PTR [12+eax+edx]                    ;125.42
        lea       esi, DWORD PTR [316+esp]                      ;125.26
        mov       DWORD PTR [8+esp], eax                        ;125.26
        lea       ecx, DWORD PTR [280+esp]                      ;125.26
        lea       edx, DWORD PTR [8+esp]                        ;125.26
        push      DWORD PTR [12+ebp]                            ;125.26
        push      edx                                           ;125.26
        push      ecx                                           ;125.26
        push      esi                                           ;125.26
        movss     DWORD PTR [256+esp], xmm3                     ;125.26
        movss     DWORD PTR [260+esp], xmm4                     ;125.26
        call      _DYNUST_ROUTE_SWITCH_MODULE_mp_INFO_SWITCH    ;125.26
                                ; LOE ebx
.B1.229:                        ; Preds .B1.228                 ; Infreq
        mov       eax, DWORD PTR [332+esp]                      ;126.26
        lea       ecx, DWORD PTR [184+esp]                      ;126.26
        sub       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE+32] ;126.26
        lea       esi, DWORD PTR [180+esp]                      ;126.26
        shl       eax, 6                                        ;126.26
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE] ;126.26
        push      ecx                                           ;126.26
        push      esi                                           ;126.26
        lea       edi, DWORD PTR [12+eax+edx]                   ;126.26
        push      edi                                           ;126.26
        lea       eax, DWORD PTR [308+esp]                      ;126.26
        push      eax                                           ;126.26
        lea       edx, DWORD PTR [348+esp]                      ;126.26
        push      edx                                           ;126.26
        push      DWORD PTR [8+ebp]                             ;126.26
        call      _RETRIEVE_NEXT_LINK                           ;126.26
                                ; LOE ebx
.B1.317:                        ; Preds .B1.229                 ; Infreq
        movss     xmm4, DWORD PTR [284+esp]                     ;
        movss     xmm3, DWORD PTR [280+esp]                     ;
        add       esp, 40                                       ;126.26
                                ; LOE ebx xmm3 xmm4
.B1.230:                        ; Preds .B1.317                 ; Infreq
        mov       eax, DWORD PTR [316+esp]                      ;127.26
        mov       edi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;127.21
        mov       DWORD PTR [320+esp], eax                      ;127.26
        sub       eax, edi                                      ;127.21
        shl       eax, 8                                        ;127.21
        mov       esi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;127.21
        mov       DWORD PTR [12+esp], eax                       ;127.21
        mov       edx, DWORD PTR [148+esi+eax]                  ;127.26
        shl       edx, 2                                        ;127.21
        neg       edx                                           ;127.21
        add       edx, DWORD PTR [116+esi+eax]                  ;127.21
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;128.18
        neg       eax                                           ;128.60
        mov       ecx, DWORD PTR [4+edx]                        ;127.21
        add       eax, ecx                                      ;128.60
        mov       DWORD PTR [200+esp], ecx                      ;127.21
        imul      ecx, eax, 152                                 ;128.60
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;128.18
        movsx     eax, WORD PTR [148+edx+ecx]                   ;128.21
        cmp       eax, 6                                        ;128.60
        jne       .B1.39        ; Prob 84%                      ;128.60
                                ; LOE ebx esi edi xmm3 xmm4
.B1.231:                        ; Preds .B1.230                 ; Infreq
        mov       eax, DWORD PTR [12+esp]                       ;128.66
        mov       DWORD PTR [36+esi+eax], -1                    ;128.66
        jmp       .B1.39        ; Prob 100%                     ;128.66
                                ; LOE ebx esi edi xmm3 xmm4
.B1.234:                        ; Preds .B1.76                  ; Infreq
        mov       edi, DWORD PTR [200+esp]                      ;216.26
        sub       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;216.61
        imul      esi, edi, 152                                 ;216.61
        mov       DWORD PTR [204+esp], ebx                      ;
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;216.23
        mov       ecx, DWORD PTR [256+esp]                      ;216.26
        mov       edx, DWORD PTR [24+ebx+esi]                   ;216.26
        mov       DWORD PTR [68+esp], edx                       ;216.26
        mov       edx, DWORD PTR [268+esp]                      ;216.26
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_MASTERDEST+32] ;216.26
        neg       esi                                           ;216.61
        mov       edi, DWORD PTR [16+ecx+edx]                   ;216.26
        mov       ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DESTINATION+32] ;216.26
        neg       ebx                                           ;216.61
        lea       edi, DWORD PTR [edi+eax*2]                    ;216.61
        mov       eax, DWORD PTR [48+ecx+edx]                   ;216.26
        add       eax, eax                                      ;216.61
        neg       eax                                           ;216.61
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_MASTERDEST] ;216.26
        movsx     eax, WORD PTR [eax+edi]                       ;216.26
        add       esi, eax                                      ;216.61
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DESTINATION] ;216.26
        movsx     ecx, WORD PTR [edx+esi*2]                     ;216.26
        add       ebx, ecx                                      ;216.61
        mov       esi, DWORD PTR [68+esp]                       ;216.61
        cmp       esi, DWORD PTR [eax+ebx*4]                    ;216.61
        mov       ebx, DWORD PTR [204+esp]                      ;216.61
        jne       .B1.79        ; Prob 50%                      ;216.61
                                ; LOE ebx bl bh
.B1.235:                        ; Preds .B1.234                 ; Infreq
        mov       DWORD PTR [_SIM_MOVE_VEHICLES$ARRIVEINTDEST.0.1], -1 ;216.149
        mov       eax, -1                                       ;
        jmp       .B1.80        ; Prob 100%                     ;
                                ; LOE eax ebx
.B1.237:                        ; Preds .B1.96                  ; Infreq
        mov       esi, DWORD PTR [564+ecx+eax]                  ;268.106
        add       edx, esi                                      ;268.61
        mov       eax, DWORD PTR [596+ecx+eax]                  ;268.106
        shl       eax, 2                                        ;268.61
        neg       eax                                           ;268.61
        inc       DWORD PTR [eax+edx]                           ;268.61
        jmp       .B1.99        ; Prob 100%                     ;268.61
                                ; LOE ebx
.B1.238:                        ; Preds .B1.112                 ; Infreq
        cmp       esi, 5                                        ;300.89
        jle       .B1.113       ; Prob 50%                      ;300.89
                                ; LOE edx ebx esi
.B1.239:                        ; Preds .B1.238                 ; Infreq
        mov       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;300.56
        neg       ecx                                           ;300.133
        add       ecx, edx                                      ;300.133
        shl       ecx, 5                                        ;300.133
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;300.56
        cmp       BYTE PTR [4+eax+ecx], 4                       ;300.129
        je        .B1.113       ; Prob 16%                      ;300.129
                                ; LOE eax edx ecx ebx esi
.B1.240:                        ; Preds .B1.239                 ; Infreq
        cmp       BYTE PTR [5+eax+ecx], 6                       ;300.169
        je        .B1.113       ; Prob 16%                      ;300.169
                                ; LOE edx ebx esi
.B1.241:                        ; Preds .B1.240                 ; Infreq
        mov       DWORD PTR [56+esp], esi                       ;301.33
        lea       eax, DWORD PTR [56+esp]                       ;301.33
        push      OFFSET FLAT: __NLITPACK_1.0.1                 ;301.33
        push      eax                                           ;301.33
        lea       edx, DWORD PTR [324+esp]                      ;301.33
        push      edx                                           ;301.33
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_VALUE   ;301.33
                                ; LOE ebx f1
.B1.318:                        ; Preds .B1.241                 ; Infreq
        fstp      DWORD PTR [60+esp]                            ;301.33
        movss     xmm2, DWORD PTR [60+esp]                      ;301.33
        add       esp, 12                                       ;301.33
                                ; LOE ebx xmm2
.B1.242:                        ; Preds .B1.318                 ; Infreq
        mov       edi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;302.24
        neg       edi                                           ;302.24
        pxor      xmm1, xmm1                                    ;303.34
        mov       edx, DWORD PTR [316+esp]                      ;302.58
        add       edi, edx                                      ;302.24
        shl       edi, 5                                        ;302.24
        mov       esi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;302.24
        movss     xmm0, DWORD PTR [252+esp]                     ;302.57
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;302.97
        neg       eax                                           ;302.24
        subss     xmm0, DWORD PTR [12+esi+edi]                  ;302.57
        mulss     xmm0, DWORD PTR [_2il0floatpacket.10]         ;302.87
        add       eax, edx                                      ;302.24
        shl       eax, 8                                        ;302.24
        subss     xmm0, xmm2                                    ;302.97
        comiss    xmm2, xmm1                                    ;303.34
        cvttss2si esi, xmm0                                     ;302.24
        mov       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;302.97
        mov       DWORD PTR [248+ecx+eax], esi                  ;302.24
        jbe       .B1.113       ; Prob 50%                      ;303.34
                                ; LOE eax edx ecx ebx esi
.B1.243:                        ; Preds .B1.242                 ; Infreq
        cmp       esi, DWORD PTR [244+ecx+eax]                  ;303.72
        jle       .B1.113       ; Prob 50%                      ;303.72
                                ; LOE eax edx ecx ebx
.B1.244:                        ; Preds .B1.243                 ; Infreq
        cmp       BYTE PTR [252+ecx+eax], 0                     ;303.131
        jne       .B1.113       ; Prob 50%                      ;303.131
                                ; LOE edx ebx
.B1.245:                        ; Preds .B1.244                 ; Infreq
        mov       eax, DWORD PTR [280+esp]                      ;303.140
        sub       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;303.135
        imul      esi, eax, 900                                 ;303.135
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;303.100
        cmp       BYTE PTR [12+ecx+esi], 2                      ;303.183
        je        .B1.113       ; Prob 16%                      ;303.183
                                ; LOE edx ebx
.B1.246:                        ; Preds .B1.245                 ; Infreq
        push      OFFSET FLAT: __NLITPACK_2.0.1                 ;305.32
        call      _RANXY                                        ;305.32
                                ; LOE ebx f1
.B1.247:                        ; Preds .B1.246                 ; Infreq
        fstp      st(0)                                         ;
        mov       edx, DWORD PTR [320+esp]                      ;316.32
        lea       esi, DWORD PTR [172+esp]                      ;316.32
        sub       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE+32] ;316.32
        lea       eax, DWORD PTR [284+esp]                      ;316.32
        shl       edx, 6                                        ;316.32
        lea       edi, DWORD PTR [168+esp]                      ;316.32
        mov       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE] ;316.32
        push      esi                                           ;316.32
        push      edi                                           ;316.32
        lea       edx, DWORD PTR [12+edx+ecx]                   ;316.32
        push      edx                                           ;316.32
        push      eax                                           ;316.32
        lea       eax, DWORD PTR [336+esp]                      ;316.32
        push      eax                                           ;316.32
        push      DWORD PTR [8+ebp]                             ;316.32
        call      _RETRIEVE_NEXT_LINK                           ;316.32
                                ; LOE ebx
.B1.319:                        ; Preds .B1.247                 ; Infreq
        add       esp, 28                                       ;316.32
                                ; LOE ebx
.B1.248:                        ; Preds .B1.319                 ; Infreq
        mov       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;317.27
        neg       ecx                                           ;317.27
        mov       edx, DWORD PTR [316+esp]                      ;317.32
        add       ecx, edx                                      ;317.27
        shl       ecx, 8                                        ;317.27
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;317.27
        mov       DWORD PTR [esp], eax                          ;317.27
        mov       DWORD PTR [4+esp], ecx                        ;317.27
        mov       esi, DWORD PTR [148+eax+ecx]                  ;317.32
        shl       esi, 2                                        ;317.27
        neg       esi                                           ;317.27
        add       esi, DWORD PTR [116+eax+ecx]                  ;317.27
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;318.27
        neg       eax                                           ;318.69
        mov       edi, DWORD PTR [4+esi]                        ;317.27
        add       eax, edi                                      ;318.69
        imul      esi, eax, 152                                 ;318.69
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;318.27
        mov       DWORD PTR [200+esp], edi                      ;317.27
        movsx     edi, WORD PTR [148+ecx+esi]                   ;318.30
        cmp       edi, 6                                        ;318.69
        jne       .B1.113       ; Prob 84%                      ;318.69
                                ; LOE edx ebx
.B1.249:                        ; Preds .B1.248                 ; Infreq
        mov       eax, DWORD PTR [esp]                          ;318.75
        mov       ecx, DWORD PTR [4+esp]                        ;318.75
        mov       DWORD PTR [36+eax+ecx], -1                    ;318.75
        jmp       .B1.113       ; Prob 100%                     ;318.75
                                ; LOE edx ebx
.B1.250:                        ; Preds .B1.63                  ; Infreq
        push      32                                            ;190.21
        xor       eax, eax                                      ;190.21
        push      eax                                           ;190.21
        push      eax                                           ;190.21
        push      -2088435968                                   ;190.21
        push      eax                                           ;190.21
        push      OFFSET FLAT: __STRLITPACK_8                   ;190.21
        movss     DWORD PTR [84+esp], xmm3                      ;190.21
        movss     DWORD PTR [88+esp], xmm0                      ;190.21
        call      _for_stop_core                                ;190.21
                                ; LOE ebx esi edi
.B1.320:                        ; Preds .B1.250                 ; Infreq
        movss     xmm0, DWORD PTR [88+esp]                      ;
        movss     xmm3, DWORD PTR [84+esp]                      ;
        add       esp, 24                                       ;190.21
        jmp       .B1.64        ; Prob 100%                     ;190.21
                                ; LOE ebx esi edi xmm0 xmm3
.B1.252:                        ; Preds .B1.26                  ; Infreq
        pxor      xmm5, xmm5                                    ;102.16
        comiss    xmm5, xmm3                                    ;102.16
        jb        .B1.27        ; Prob 50%                      ;102.16
                                ; LOE edx ecx ebx xmm0 xmm1 xmm2 xmm3 xmm4
.B1.253:                        ; Preds .B1.252                 ; Infreq
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;102.38
        neg       eax                                           ;102.38
        add       eax, DWORD PTR [284+esp]                      ;102.38
        imul      edi, eax, 900                                 ;102.38
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;102.38
        mov       DWORD PTR [664+esi+edi], -1                   ;102.38
        jmp       .B1.27        ; Prob 100%                     ;102.38
                                ; LOE edx ecx ebx xmm0 xmm1 xmm2 xmm3 xmm4
.B1.254:                        ; Preds .B1.149 .B1.153 .B1.151 ; Infreq
        xor       esi, esi                                      ;
        xor       ecx, ecx                                      ;
        jmp       .B1.161       ; Prob 100%                     ;
                                ; LOE eax edx ecx esi
.B1.255:                        ; Preds .B1.166 .B1.170 .B1.168 ; Infreq
        xor       esi, esi                                      ;
        xor       ecx, ecx                                      ;
        jmp       .B1.178       ; Prob 100%                     ;
                                ; LOE eax edx ecx esi
.B1.256:                        ; Preds .B1.183 .B1.187 .B1.185 ; Infreq
        xor       esi, esi                                      ;
        xor       ecx, ecx                                      ;
        jmp       .B1.195       ; Prob 100%                     ;
                                ; LOE eax edx ecx esi
.B1.257:                        ; Preds .B1.200 .B1.204 .B1.202 ; Infreq
        xor       ebx, ebx                                      ;
        xor       ecx, ecx                                      ;
        jmp       .B1.212       ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx
.B1.262:                        ; Preds .B1.148 .B1.165 .B1.182 .B1.199 ; Infreq
        xor       ebx, ebx                                      ;
        jmp       .B1.216       ; Prob 100%                     ;
                                ; LOE eax ebx
.B1.272:                        ; Preds .B1.143                 ; Infreq
        push      32                                            ;383.13
        xor       eax, eax                                      ;383.13
        push      eax                                           ;383.13
        push      eax                                           ;383.13
        push      -2088435968                                   ;383.13
        push      eax                                           ;383.13
        push      OFFSET FLAT: __STRLITPACK_9                   ;383.13
        call      _for_stop_core                                ;383.13
                                ; LOE ebx esi edi
.B1.321:                        ; Preds .B1.272                 ; Infreq
        add       esp, 24                                       ;383.13
        jmp       .B1.144       ; Prob 100%                     ;383.13
                                ; LOE ebx esi edi
.B1.274:                        ; Preds .B1.19                  ; Infreq
        mov       DWORD PTR [280+esp], 1                        ;77.4
        jmp       .B1.148       ; Prob 100%                     ;77.4
                                ; LOE
.B1.275:                        ; Preds .B1.220                 ; Infreq
        cmp       DWORD PTR [16+esp], 4                         ;62.1
        jl        .B1.292       ; Prob 10%                      ;62.1
                                ; LOE ebx esi
.B1.276:                        ; Preds .B1.275                 ; Infreq
        mov       eax, DWORD PTR [112+esp]                      ;62.1
        mov       edx, DWORD PTR [8+esp]                        ;62.1
        imul      edx, eax                                      ;62.1
        imul      eax, ebx                                      ;62.1
        neg       edx                                           ;62.1
        add       edx, DWORD PTR [esp]                          ;62.1
        mov       DWORD PTR [20+esp], eax                       ;62.1
        add       eax, edx                                      ;62.1
        and       eax, 15                                       ;62.1
        je        .B1.279       ; Prob 50%                      ;62.1
                                ; LOE eax edx ebx esi
.B1.277:                        ; Preds .B1.276                 ; Infreq
        test      al, 3                                         ;62.1
        jne       .B1.292       ; Prob 10%                      ;62.1
                                ; LOE eax edx ebx esi
.B1.278:                        ; Preds .B1.277                 ; Infreq
        neg       eax                                           ;62.1
        add       eax, 16                                       ;62.1
        shr       eax, 2                                        ;62.1
                                ; LOE eax edx ebx esi
.B1.279:                        ; Preds .B1.278 .B1.276         ; Infreq
        lea       ecx, DWORD PTR [4+eax]                        ;62.1
        cmp       ecx, DWORD PTR [16+esp]                       ;62.1
        jg        .B1.292       ; Prob 10%                      ;62.1
                                ; LOE eax edx ebx esi
.B1.280:                        ; Preds .B1.279                 ; Infreq
        mov       edi, DWORD PTR [16+esp]                       ;62.1
        mov       ecx, edi                                      ;62.1
        sub       ecx, eax                                      ;62.1
        and       ecx, 3                                        ;62.1
        neg       ecx                                           ;62.1
        add       edx, DWORD PTR [20+esp]                       ;
        add       ecx, edi                                      ;62.1
        test      eax, eax                                      ;62.1
        jbe       .B1.284       ; Prob 10%                      ;62.1
                                ; LOE eax edx ecx ebx esi
.B1.281:                        ; Preds .B1.280                 ; Infreq
        xor       edi, edi                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B1.282:                        ; Preds .B1.282 .B1.281         ; Infreq
        mov       DWORD PTR [edx+edi*4], 0                      ;62.1
        inc       edi                                           ;62.1
        cmp       edi, eax                                      ;62.1
        jb        .B1.282       ; Prob 82%                      ;62.1
                                ; LOE eax edx ecx ebx esi edi
.B1.284:                        ; Preds .B1.282 .B1.280         ; Infreq
        pxor      xmm0, xmm0                                    ;
                                ; LOE eax edx ecx ebx esi xmm0
.B1.285:                        ; Preds .B1.285 .B1.284         ; Infreq
        movdqa    XMMWORD PTR [edx+eax*4], xmm0                 ;62.1
        add       eax, 4                                        ;62.1
        cmp       eax, ecx                                      ;62.1
        jb        .B1.285       ; Prob 82%                      ;62.1
                                ; LOE eax edx ecx ebx esi xmm0
.B1.287:                        ; Preds .B1.285 .B1.292         ; Infreq
        cmp       ecx, DWORD PTR [16+esp]                       ;62.1
        jae       .B1.290       ; Prob 10%                      ;62.1
                                ; LOE ecx ebx esi
.B1.288:                        ; Preds .B1.287                 ; Infreq
        mov       eax, DWORD PTR [8+esp]                        ;
        neg       eax                                           ;
        add       eax, ebx                                      ;
        imul      eax, DWORD PTR [112+esp]                      ;
        add       eax, DWORD PTR [esp]                          ;
        mov       edx, DWORD PTR [16+esp]                       ;
                                ; LOE eax edx ecx ebx esi
.B1.289:                        ; Preds .B1.289 .B1.288         ; Infreq
        mov       DWORD PTR [eax+ecx*4], 0                      ;62.1
        inc       ecx                                           ;62.1
        cmp       ecx, edx                                      ;62.1
        jb        .B1.289       ; Prob 82%                      ;62.1
                                ; LOE eax edx ecx ebx esi
.B1.290:                        ; Preds .B1.287 .B1.289         ; Infreq
        inc       esi                                           ;62.1
        inc       ebx                                           ;62.1
        cmp       esi, DWORD PTR [4+esp]                        ;62.1
        jb        .B1.220       ; Prob 82%                      ;62.1
        jmp       .B1.12        ; Prob 100%                     ;62.1
                                ; LOE ebx esi
.B1.292:                        ; Preds .B1.275 .B1.279 .B1.277 ; Infreq
        xor       ecx, ecx                                      ;62.1
        jmp       .B1.287       ; Prob 100%                     ;62.1
                                ; LOE ecx ebx esi
.B1.297:                        ; Preds .B1.4                   ; Infreq
        mov       ecx, 1                                        ;
        jmp       .B1.8         ; Prob 100%                     ;
                                ; LOE eax ecx ebx esi
.B1.332:                        ; Preds .B1.50 .B1.51 .B1.52    ; Infreq
        comiss    xmm1, xmm4                                    ;168.36
        ja        .B1.55        ; Prob 50%                      ;168.36
        jmp       .B1.49        ; Prob 100%                     ;168.36
        ALIGN     16
                                ; LOE eax ebx esi xmm3 xmm4
; mark_end;
_SIM_MOVE_VEHICLES ENDP
_TEXT	ENDS
_BSS	SEGMENT  DWORD PUBLIC FLAT  'BSS'
	ALIGN 004H
_SIM_MOVE_VEHICLES$FLAG.0.1	DD 1 DUP (0H)	; pad
_SIM_MOVE_VEHICLES$ARRIVEINTDEST.0.1	DD 1 DUP (0H)	; pad
_BSS	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
SIM_MOVE_VEHICLES$format_pack.0.1	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	7
	DB	0
	DB	106
	DB	112
	DB	97
	DB	116
	DB	104
	DB	58
	DB	32
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	10
	DB	0
	DB	0
	DB	0
	DB	5
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	7
	DB	0
	DB	116
	DB	105
	DB	109
	DB	101
	DB	61
	DB	62
	DB	32
	DB	0
	DB	33
	DB	0
	DB	0
	DB	1
	DB	1
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
_DATA	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
__STRLITPACK_7.0.1	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__NLITPACK_0.0.1	DD	26
__NLITPACK_1.0.1	DD	4
__NLITPACK_2.0.1	DD	11
__NLITPACK_3.0.1	DD	1
__NLITPACK_4.0.1	DD	2
__NLITPACK_5.0.1	DD	3
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _SIM_MOVE_VEHICLES
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
__STRLITPACK_8	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_9	DB	0
	DB 3 DUP ( 0H)	; pad
_2il0floatpacket.8	DD	0447a0000H
_2il0floatpacket.9	DD	042700000H
_2il0floatpacket.10	DD	042c80000H
_2il0floatpacket.11	DD	045a50000H
_2il0floatpacket.12	DD	038d1b717H
_2il0floatpacket.13	DD	080000000H
_2il0floatpacket.14	DD	04b000000H
_2il0floatpacket.15	DD	03f000000H
_2il0floatpacket.16	DD	0bf000000H
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
EXTRN	_DYNUST_MAIN_MODULE_mp_NUMCARS:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NOUT_TAG:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NUMNTAG:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NOUT_NONTAG:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TIME_NOW:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_VMSTYPE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_VMS_END:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_VMS_START:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NODEWVMS:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_VMS_NUM:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_RUNMODE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_READHISTARR:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_MASTERDEST:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_DESTINATION:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_DETECTOR_LENGTH:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_OCCUP:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_DETECTOR:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_GUITOTALTIME:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_VKFUNC:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_INFOSTARTTIME:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TDSPSTEP:BYTE
EXTRN	_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_REACH_CONVERG:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_FUELOUT:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_SIMPERAGG:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_AGGINT:BYTE
EXTRN	_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND:BYTE
EXTRN	_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY:BYTE
EXTRN	_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH:BYTE
EXTRN	_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ITERATION:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ITEMAX:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_VEHJO:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NOOFARCS:BYTE
_DATA	ENDS
EXTRN	_for_dealloc_allocatable:PROC
EXTRN	_for_stop_core:PROC
EXTRN	_for_write_seq_fmt:PROC
EXTRN	_for_alloc_allocatable:PROC
EXTRN	_for_check_mult_overflow:PROC
EXTRN	_DYNUST_FUEL_MODULE_mp_OUTPUTFUELDEMAND:PROC
EXTRN	_DYNUST_ROUTE_SWITCH_MODULE_mp_INFO_SWITCH:PROC
EXTRN	_DYNUST_LINK_VEH_LIST_MODULE_mp_TRIPCHAIN_INSERT:PROC
EXTRN	_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST_REMOVE:PROC
EXTRN	_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_P_SIZE:PROC
EXTRN	_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_A_REMOVE:PROC
EXTRN	_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_P_REMOVE:PROC
EXTRN	_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_VALUE:PROC
EXTRN	_VARIABLE_MESSAGE_SIGN_MODULE_mp_VMS_DETOUR:PROC
EXTRN	_VARIABLE_MESSAGE_SIGN_MODULE_mp_VMS_DIVERSION:PROC
EXTRN	_VARIABLE_MESSAGE_SIGN_MODULE_mp_VMS_MULTI:PROC
EXTRN	_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_SCAN:PROC
EXTRN	_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_INSERT:PROC
EXTRN	_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_AVSIZEINCRE:PROC
EXTRN	_CAL_ACCUMU_STAT:PROC
EXTRN	_RANXY:PROC
EXTRN	_RETRIEVE_NEXT_LINK:PROC
EXTRN	__intel_fast_memset:PROC
EXTRN	__fltused:BYTE
	INCLUDELIB <ifconsol>
	INCLUDELIB <libifcoremt>
	INCLUDELIB <libifport>
	INCLUDELIB <libmmt>
	INCLUDELIB <LIBCMT>
	INCLUDELIB <libirc>
	INCLUDELIB <svml_dispmt>
	INCLUDELIB <OLDNAMES>
	END
