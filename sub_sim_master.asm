; mark_description "Intel(R) Visual Fortran Compiler XE for applications running on IA-32, Version 13.1.1.171 Build 20130313";
; mark_description "-S";
	.686P
 	.387
	OPTION DOTNAME
	ASSUME	CS:FLAT,DS:FLAT,SS:FLAT
;ident "-defaultlib:ifmodintr.lib"
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
TXTST0:
; -- Begin  _SIM_MASTER
; mark_begin;
IF @Version GE 800
  .MMX
ELSEIF @Version GE 612
  .MMX
  MMWORD TEXTEQU <QWORD>
ENDIF
IF @Version GE 800
  .XMM
ELSEIF @Version GE 614
  .XMM
  XMMWORD TEXTEQU <OWORD>
ENDIF
       ALIGN     16
	PUBLIC _SIM_MASTER
_SIM_MASTER	PROC NEAR 
; parameter 1: 44 + esp
; parameter 2: 48 + esp
; parameter 3: 52 + esp
.B1.1:                          ; Preds .B1.0
        push      esi                                           ;1.17
        push      edi                                           ;1.17
        push      ebx                                           ;1.17
        push      ebp                                           ;1.17
        sub       esp, 24                                       ;1.17
        mov       ebx, DWORD PTR [48+esp]                       ;1.17
        lea       eax, DWORD PTR [16+esp]                       ;33.12
        movss     xmm0, DWORD PTR [_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT] ;31.7
        addss     xmm0, DWORD PTR [ebx]                         ;31.7
        movss     DWORD PTR [12+esp], xmm0                      ;31.7
        push      eax                                           ;33.12
        call      _for_cpusec                                   ;33.12
                                ; LOE ebx
.B1.2:                          ; Preds .B1.1
        push      ebx                                           ;34.12
        call      _SIM_LOAD_VEHICLES                            ;34.12
                                ; LOE ebx
.B1.25:                         ; Preds .B1.2
        add       esp, 8                                        ;34.12
                                ; LOE ebx
.B1.3:                          ; Preds .B1.25
        call      _DYNUST_AMS_MODULE_mp_FLOW_MODEL_UPDATE_AMTS  ;36.12
                                ; LOE ebx
.B1.4:                          ; Preds .B1.3
        mov       ecx, DWORD PTR [44+esp]                       ;1.17
        mov       ebp, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIMPERAGG] ;38.7
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFARCS] ;40.7
        mov       eax, DWORD PTR [ecx]                          ;38.7
        cdq                                                     ;38.7
        idiv      ebp                                           ;38.7
        test      edx, edx                                      ;39.20
        cmove     edx, ebp                                      ;39.20
        test      esi, esi                                      ;40.7
        jle       .B1.11        ; Prob 50%                      ;40.7
                                ; LOE edx ebx esi
.B1.5:                          ; Preds .B1.4
        mov       eax, esi                                      ;40.7
        shr       eax, 31                                       ;40.7
        add       eax, esi                                      ;40.7
        sar       eax, 1                                        ;40.7
        mov       ebp, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;41.9
        test      eax, eax                                      ;40.7
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;41.9
        mov       DWORD PTR [20+esp], eax                       ;40.7
        jbe       .B1.22        ; Prob 3%                       ;40.7
                                ; LOE edx ecx ebx ebp esi
.B1.6:                          ; Preds .B1.5
        imul      edi, ecx, -900                                ;
        xor       eax, eax                                      ;
        movss     xmm0, DWORD PTR [_2il0floatpacket.3]          ;41.80
        add       edi, ebp                                      ;
        mov       DWORD PTR [4+esp], ebp                        ;
        mov       DWORD PTR [esp], esi                          ;
        mov       DWORD PTR [8+esp], ecx                        ;
        mov       ebp, eax                                      ;
        mov       esi, edi                                      ;
        ALIGN     16
                                ; LOE eax edx ebp esi xmm0
.B1.7:                          ; Preds .B1.7 .B1.6
        movss     xmm1, DWORD PTR [1552+ebp+esi]                ;41.52
        inc       eax                                           ;40.7
        mulss     xmm1, xmm0                                    ;41.80
        cvttss2si ebx, xmm1                                     ;41.9
        mov       ecx, DWORD PTR [1196+ebp+esi]                 ;41.9
        shl       ecx, 2                                        ;41.9
        movss     xmm2, DWORD PTR [2452+ebp+esi]                ;41.52
        neg       ecx                                           ;41.9
        mov       edi, DWORD PTR [1164+ebp+esi]                 ;41.9
        mulss     xmm2, xmm0                                    ;41.80
        lea       edi, DWORD PTR [edi+edx*4]                    ;41.9
        mov       DWORD PTR [ecx+edi], ebx                      ;41.9
        cvttss2si ecx, xmm2                                     ;41.9
        mov       edi, DWORD PTR [2096+ebp+esi]                 ;41.9
        shl       edi, 2                                        ;41.9
        mov       ebx, DWORD PTR [2064+ebp+esi]                 ;41.9
        neg       edi                                           ;41.9
        add       ebp, 1800                                     ;40.7
        cmp       eax, DWORD PTR [20+esp]                       ;40.7
        lea       ebx, DWORD PTR [ebx+edx*4]                    ;41.9
        mov       DWORD PTR [edi+ebx], ecx                      ;41.9
        jb        .B1.7         ; Prob 63%                      ;40.7
                                ; LOE eax edx ebp esi xmm0
.B1.8:                          ; Preds .B1.7
        mov       ecx, DWORD PTR [8+esp]                        ;
        lea       edi, DWORD PTR [1+eax+eax]                    ;40.7
        mov       ebp, DWORD PTR [4+esp]                        ;
        mov       esi, DWORD PTR [esp]                          ;
        mov       ebx, DWORD PTR [48+esp]                       ;
                                ; LOE edx ecx ebx ebp esi edi
.B1.9:                          ; Preds .B1.8 .B1.22
        lea       eax, DWORD PTR [-1+edi]                       ;40.7
        cmp       esi, eax                                      ;40.7
        jbe       .B1.11        ; Prob 3%                       ;40.7
                                ; LOE edx ecx ebx ebp edi
.B1.10:                         ; Preds .B1.9
        neg       ecx                                           ;41.9
        add       ecx, edi                                      ;41.9
        imul      ecx, ecx, 900                                 ;41.9
        movss     xmm0, DWORD PTR [_2il0floatpacket.3]          ;41.80
        mulss     xmm0, DWORD PTR [652+ebp+ecx]                 ;41.80
        cvttss2si esi, xmm0                                     ;41.9
        mov       eax, DWORD PTR [296+ebp+ecx]                  ;41.9
        sub       edx, eax                                      ;41.9
        mov       ebp, DWORD PTR [264+ebp+ecx]                  ;41.9
        mov       DWORD PTR [ebp+edx*4], esi                    ;41.9
                                ; LOE ebx
.B1.11:                         ; Preds .B1.9 .B1.4 .B1.10
        push      ebx                                           ;44.9
        call      _SIGNAL_INTERSECTION_CONTROL                  ;44.9
                                ; LOE ebx
.B1.12:                         ; Preds .B1.11
        push      ebx                                           ;45.12
        call      _CAL_LINK_DISCHARGE_RATE                      ;45.12
                                ; LOE ebx
.B1.13:                         ; Preds .B1.12
        push      DWORD PTR [52+esp]                            ;46.9
        push      ebx                                           ;46.9
        call      _SIM_MOVE_VEHICLES                            ;46.9
                                ; LOE ebx
.B1.14:                         ; Preds .B1.13
        lea       eax, DWORD PTR [16+esp]                       ;47.12
        push      eax                                           ;47.12
        call      _for_cpusec                                   ;47.12
                                ; LOE ebx
.B1.15:                         ; Preds .B1.14
        lea       ebp, DWORD PTR [32+esp]                       ;50.12
        push      DWORD PTR [72+esp]                            ;50.12
        push      ebp                                           ;50.12
        push      ebx                                           ;50.12
        push      DWORD PTR [76+esp]                            ;50.12
        call      _SIM_TRANSFER_VEHICLES                        ;50.12
                                ; LOE ebx ebp
.B1.26:                         ; Preds .B1.15
        add       esp, 36                                       ;50.12
                                ; LOE ebx ebp
.B1.16:                         ; Preds .B1.26
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ITEMAX] ;52.6
        test      eax, eax                                      ;52.17
        je        .B1.20        ; Prob 50%                      ;52.17
                                ; LOE eax ebx ebp
.B1.17:                         ; Preds .B1.16
        jle       .B1.21        ; Prob 16%                      ;52.33
                                ; LOE eax ebx ebp
.B1.18:                         ; Preds .B1.17
        cmp       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ITERATION] ;52.52
        je        .B1.20        ; Prob 50%                      ;52.52
                                ; LOE ebx ebp
.B1.19:                         ; Preds .B1.18
        test      BYTE PTR [_DYNUST_MAIN_MODULE_mp_REACH_CONVERG], 1 ;52.65
        je        .B1.21        ; Prob 85%                      ;52.65
                                ; LOE ebx ebp
.B1.20:                         ; Preds .B1.19 .B1.18 .B1.16
        push      ebp                                           ;52.83
        push      ebx                                           ;52.83
        push      DWORD PTR [52+esp]                            ;52.83
        call      _CAL_LINK_MOE                                 ;52.83
                                ; LOE
.B1.27:                         ; Preds .B1.20
        add       esp, 12                                       ;52.83
                                ; LOE
.B1.21:                         ; Preds .B1.27 .B1.19 .B1.17
        add       esp, 24                                       ;54.6
        pop       ebp                                           ;54.6
        pop       ebx                                           ;54.6
        pop       edi                                           ;54.6
        pop       esi                                           ;54.6
        ret                                                     ;54.6
                                ; LOE
.B1.22:                         ; Preds .B1.5                   ; Infreq
        mov       edi, 1                                        ;
        jmp       .B1.9         ; Prob 100%                     ;
        ALIGN     16
                                ; LOE edx ecx ebx ebp esi edi
; mark_end;
_SIM_MASTER ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _SIM_MASTER
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
_2il0floatpacket.3	DD	042c80000H
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
EXTRN	_DYNUST_MAIN_MODULE_mp_REACH_CONVERG:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ITERATION:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ITEMAX:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NOOFARCS:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_SIMPERAGG:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT:BYTE
_DATA	ENDS
EXTRN	_for_cpusec:PROC
EXTRN	_DYNUST_AMS_MODULE_mp_FLOW_MODEL_UPDATE_AMTS:PROC
EXTRN	_SIM_LOAD_VEHICLES:PROC
EXTRN	_SIGNAL_INTERSECTION_CONTROL:PROC
EXTRN	_CAL_LINK_DISCHARGE_RATE:PROC
EXTRN	_SIM_MOVE_VEHICLES:PROC
EXTRN	_SIM_TRANSFER_VEHICLES:PROC
EXTRN	_CAL_LINK_MOE:PROC
EXTRN	__fltused:BYTE
	INCLUDELIB <ifconsol>
	INCLUDELIB <libifcoremt>
	INCLUDELIB <libifport>
	INCLUDELIB <libmmt>
	INCLUDELIB <LIBCMT>
	INCLUDELIB <libirc>
	INCLUDELIB <svml_dispmt>
	INCLUDELIB <OLDNAMES>
	END
