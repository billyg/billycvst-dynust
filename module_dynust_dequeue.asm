; mark_description "Intel(R) Visual Fortran Compiler XE for applications running on IA-32, Version 13.1.1.171 Build 20130313";
; mark_description "-S";
	.686P
 	.387
	OPTION DOTNAME
	ASSUME	CS:FLAT,DS:FLAT,SS:FLAT
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
TXTST0:
; -- Begin  _DEQUEUE$
; mark_begin;
IF @Version GE 800
  .MMX
ELSEIF @Version GE 612
  .MMX
  MMWORD TEXTEQU <QWORD>
ENDIF
IF @Version GE 800
  .XMM
ELSEIF @Version GE 614
  .XMM
  XMMWORD TEXTEQU <OWORD>
ENDIF
       ALIGN     16
	PUBLIC _DEQUEUE$
_DEQUEUE$	PROC NEAR 
.B1.1:                          ; Preds .B1.0
        ret                                                     ;1.8
        ALIGN     16
                                ; LOE
; mark_end;
_DEQUEUE$ ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DEQUEUE$
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DEQUEUE_mp_SET_QUEUE_SIZE
; mark_begin;
       ALIGN     16
	PUBLIC _DEQUEUE_mp_SET_QUEUE_SIZE
_DEQUEUE_mp_SET_QUEUE_SIZE	PROC NEAR 
; parameter 1: 4 + esp
.B2.1:                          ; Preds .B2.0
        mov       eax, DWORD PTR [4+esp]                        ;34.12
        mov       edx, DWORD PTR [eax]                          ;37.5
        mov       DWORD PTR [_DEQUEUE_mp_M], edx                ;37.5
        ret                                                     ;39.1
        ALIGN     16
                                ; LOE
; mark_end;
_DEQUEUE_mp_SET_QUEUE_SIZE ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DEQUEUE_mp_SET_QUEUE_SIZE
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DEQUEUE_mp_CREATE_QUEUE_ARRAY
; mark_begin;
       ALIGN     16
	PUBLIC _DEQUEUE_mp_CREATE_QUEUE_ARRAY
_DEQUEUE_mp_CREATE_QUEUE_ARRAY	PROC NEAR 
; parameter 1: 8 + ebp
.B3.1:                          ; Preds .B3.0
        push      ebp                                           ;42.12
        mov       ebp, esp                                      ;42.12
        and       esp, -16                                      ;42.12
        push      esi                                           ;42.12
        push      edi                                           ;42.12
        push      ebx                                           ;42.12
        sub       esp, 36                                       ;42.12
        mov       esi, 4                                        ;45.5
        mov       edx, DWORD PTR [_DEQUEUE_mp_M]                ;45.5
        xor       eax, eax                                      ;45.5
        test      edx, edx                                      ;45.5
        lea       ecx, DWORD PTR [12+esp]                       ;45.5
        push      esi                                           ;45.5
        cmovle    edx, eax                                      ;45.5
        mov       ebx, DWORD PTR [8+ebp]                        ;42.12
        push      edx                                           ;45.5
        push      2                                             ;45.5
        push      ecx                                           ;45.5
        mov       DWORD PTR [28+ebx], 5                         ;45.5
        mov       DWORD PTR [20+ebx], esi                       ;45.5
        mov       DWORD PTR [32+ebx], 1                         ;45.5
        mov       DWORD PTR [24+ebx], eax                       ;45.5
        mov       DWORD PTR [48+ebx], eax                       ;45.5
        mov       DWORD PTR [40+ebx], edx                       ;45.5
        mov       DWORD PTR [44+ebx], esi                       ;45.5
        call      _for_check_mult_overflow                      ;45.5
                                ; LOE eax ebx esi
.B3.2:                          ; Preds .B3.1
        and       eax, 1                                        ;45.5
        lea       edx, DWORD PTR [16+ebx]                       ;45.5
        shl       eax, 4                                        ;45.5
        or        eax, 262144                                   ;45.5
        push      eax                                           ;45.5
        push      edx                                           ;45.5
        push      DWORD PTR [36+esp]                            ;45.5
        call      _for_allocate                                 ;45.5
                                ; LOE ebx esi
.B3.3:                          ; Preds .B3.2
        mov       edx, DWORD PTR [_DEQUEUE_mp_M]                ;46.5
        xor       eax, eax                                      ;46.5
        test      edx, edx                                      ;46.5
        lea       ecx, DWORD PTR [44+esp]                       ;46.5
        push      esi                                           ;46.5
        cmovle    edx, eax                                      ;46.5
        push      edx                                           ;46.5
        push      2                                             ;46.5
        push      ecx                                           ;46.5
        mov       DWORD PTR [64+ebx], 5                         ;46.5
        mov       DWORD PTR [56+ebx], esi                       ;46.5
        mov       DWORD PTR [68+ebx], 1                         ;46.5
        mov       DWORD PTR [60+ebx], eax                       ;46.5
        mov       DWORD PTR [84+ebx], eax                       ;46.5
        mov       DWORD PTR [76+ebx], edx                       ;46.5
        mov       DWORD PTR [80+ebx], esi                       ;46.5
        call      _for_check_mult_overflow                      ;46.5
                                ; LOE eax ebx
.B3.4:                          ; Preds .B3.3
        and       eax, 1                                        ;46.5
        lea       edx, DWORD PTR [52+ebx]                       ;46.5
        shl       eax, 4                                        ;46.5
        or        eax, 262144                                   ;46.5
        push      eax                                           ;46.5
        push      edx                                           ;46.5
        push      DWORD PTR [68+esp]                            ;46.5
        call      _for_allocate                                 ;46.5
                                ; LOE ebx
.B3.66:                         ; Preds .B3.4
        add       esp, 56                                       ;46.5
                                ; LOE ebx
.B3.5:                          ; Preds .B3.66
        mov       edx, DWORD PTR [48+ebx]                       ;48.5
        mov       eax, DWORD PTR [_DEQUEUE_mp_M]                ;47.5
        mov       DWORD PTR [8+esp], edx                        ;48.5
        mov       edx, DWORD PTR [40+ebx]                       ;48.5
        test      edx, edx                                      ;48.5
        mov       DWORD PTR [ebx], eax                          ;47.5
        jle       .B3.29        ; Prob 0%                       ;48.5
                                ; LOE edx ebx
.B3.6:                          ; Preds .B3.5
        mov       eax, DWORD PTR [44+ebx]                       ;48.5
        cmp       eax, 4                                        ;48.5
        mov       ecx, DWORD PTR [16+ebx]                       ;48.5
        jne       .B3.23        ; Prob 50%                      ;48.5
                                ; LOE eax edx ecx ebx
.B3.7:                          ; Preds .B3.6
        cmp       edx, 4                                        ;48.5
        jl        .B3.54        ; Prob 10%                      ;48.5
                                ; LOE edx ecx ebx
.B3.8:                          ; Preds .B3.7
        mov       esi, ecx                                      ;48.5
        and       esi, 15                                       ;48.5
        je        .B3.11        ; Prob 50%                      ;48.5
                                ; LOE edx ecx ebx esi
.B3.9:                          ; Preds .B3.8
        test      esi, 3                                        ;48.5
        jne       .B3.54        ; Prob 10%                      ;48.5
                                ; LOE edx ecx ebx esi
.B3.10:                         ; Preds .B3.9
        neg       esi                                           ;48.5
        add       esi, 16                                       ;48.5
        shr       esi, 2                                        ;48.5
                                ; LOE edx ecx ebx esi
.B3.11:                         ; Preds .B3.10 .B3.8
        lea       eax, DWORD PTR [4+esi]                        ;48.5
        cmp       edx, eax                                      ;48.5
        jl        .B3.54        ; Prob 10%                      ;48.5
                                ; LOE edx ecx ebx esi
.B3.12:                         ; Preds .B3.11
        mov       eax, edx                                      ;48.5
        sub       eax, esi                                      ;48.5
        and       eax, 3                                        ;48.5
        neg       eax                                           ;48.5
        add       eax, edx                                      ;48.5
        test      esi, esi                                      ;48.5
        jbe       .B3.16        ; Prob 10%                      ;48.5
                                ; LOE eax edx ecx ebx esi
.B3.13:                         ; Preds .B3.12
        xor       edi, edi                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B3.14:                         ; Preds .B3.14 .B3.13
        mov       DWORD PTR [ecx+edi*4], 0                      ;48.5
        inc       edi                                           ;48.5
        cmp       edi, esi                                      ;48.5
        jb        .B3.14        ; Prob 82%                      ;48.5
                                ; LOE eax edx ecx ebx esi edi
.B3.16:                         ; Preds .B3.14 .B3.12
        pxor      xmm0, xmm0                                    ;48.5
                                ; LOE eax edx ecx ebx esi xmm0
.B3.17:                         ; Preds .B3.17 .B3.16
        movdqa    XMMWORD PTR [ecx+esi*4], xmm0                 ;48.5
        add       esi, 4                                        ;48.5
        cmp       esi, eax                                      ;48.5
        jb        .B3.17        ; Prob 82%                      ;48.5
                                ; LOE eax edx ecx ebx esi xmm0
.B3.19:                         ; Preds .B3.17 .B3.54
        cmp       eax, edx                                      ;48.5
        jae       .B3.29        ; Prob 10%                      ;48.5
                                ; LOE eax edx ecx ebx
.B3.21:                         ; Preds .B3.19 .B3.21
        mov       DWORD PTR [ecx+eax*4], 0                      ;48.5
        inc       eax                                           ;48.5
        cmp       eax, edx                                      ;48.5
        jb        .B3.21        ; Prob 82%                      ;48.5
        jmp       .B3.29        ; Prob 100%                     ;48.5
                                ; LOE eax edx ecx ebx
.B3.23:                         ; Preds .B3.6
        mov       esi, edx                                      ;48.5
        shr       esi, 31                                       ;48.5
        add       esi, edx                                      ;48.5
        sar       esi, 1                                        ;48.5
        mov       DWORD PTR [20+esp], esi                       ;48.5
        test      esi, esi                                      ;48.5
        jbe       .B3.61        ; Prob 10%                      ;48.5
                                ; LOE eax edx ecx ebx
.B3.24:                         ; Preds .B3.23
        xor       esi, esi                                      ;
        lea       edi, DWORD PTR [ecx+eax]                      ;
        mov       DWORD PTR [4+esp], edi                        ;
        xor       edi, edi                                      ;
        mov       DWORD PTR [esp], edx                          ;
        mov       edx, esi                                      ;
        mov       ebx, DWORD PTR [4+esp]                        ;
                                ; LOE eax edx ecx ebx esi edi
.B3.25:                         ; Preds .B3.25 .B3.24
        inc       esi                                           ;48.5
        mov       DWORD PTR [ecx+edx*2], edi                    ;48.5
        mov       DWORD PTR [ebx+edx*2], edi                    ;48.5
        add       edx, eax                                      ;48.5
        cmp       esi, DWORD PTR [20+esp]                       ;48.5
        jb        .B3.25        ; Prob 63%                      ;48.5
                                ; LOE eax edx ecx ebx esi edi
.B3.26:                         ; Preds .B3.25
        mov       edx, DWORD PTR [esp]                          ;
        lea       esi, DWORD PTR [1+esi+esi]                    ;48.5
        mov       ebx, DWORD PTR [8+ebp]                        ;
                                ; LOE eax edx ecx ebx esi
.B3.27:                         ; Preds .B3.26 .B3.61
        lea       edi, DWORD PTR [-1+esi]                       ;48.5
        cmp       edx, edi                                      ;48.5
        jbe       .B3.29        ; Prob 10%                      ;48.5
                                ; LOE eax ecx ebx esi
.B3.28:                         ; Preds .B3.27
        mov       edi, DWORD PTR [8+esp]                        ;48.5
        mov       edx, edi                                      ;48.5
        neg       edx                                           ;48.5
        lea       esi, DWORD PTR [-1+esi+edi]                   ;48.5
        add       edx, esi                                      ;48.5
        imul      edx, eax                                      ;48.5
        mov       DWORD PTR [ecx+edx], 0                        ;48.5
                                ; LOE ebx
.B3.29:                         ; Preds .B3.21 .B3.5 .B3.19 .B3.27 .B3.28
                                ;      
        mov       esi, DWORD PTR [76+ebx]                       ;49.5
        test      esi, esi                                      ;49.5
        mov       edx, DWORD PTR [84+ebx]                       ;49.5
        jle       .B3.53        ; Prob 0%                       ;49.5
                                ; LOE edx ebx esi
.B3.30:                         ; Preds .B3.29
        mov       ecx, DWORD PTR [80+ebx]                       ;49.5
        cmp       ecx, 4                                        ;49.5
        mov       eax, DWORD PTR [52+ebx]                       ;49.5
        jne       .B3.47        ; Prob 50%                      ;49.5
                                ; LOE eax edx ecx esi
.B3.31:                         ; Preds .B3.30
        cmp       esi, 8                                        ;49.5
        jl        .B3.57        ; Prob 10%                      ;49.5
                                ; LOE eax esi
.B3.32:                         ; Preds .B3.31
        mov       edx, eax                                      ;49.5
        and       edx, 15                                       ;49.5
        je        .B3.35        ; Prob 50%                      ;49.5
                                ; LOE eax edx esi
.B3.33:                         ; Preds .B3.32
        test      dl, 3                                         ;49.5
        jne       .B3.57        ; Prob 10%                      ;49.5
                                ; LOE eax edx esi
.B3.34:                         ; Preds .B3.33
        neg       edx                                           ;49.5
        add       edx, 16                                       ;49.5
        shr       edx, 2                                        ;49.5
                                ; LOE eax edx esi
.B3.35:                         ; Preds .B3.34 .B3.32
        lea       ecx, DWORD PTR [8+edx]                        ;49.5
        cmp       esi, ecx                                      ;49.5
        jl        .B3.57        ; Prob 10%                      ;49.5
                                ; LOE eax edx esi
.B3.36:                         ; Preds .B3.35
        mov       ecx, esi                                      ;49.5
        sub       ecx, edx                                      ;49.5
        and       ecx, 7                                        ;49.5
        neg       ecx                                           ;49.5
        add       ecx, esi                                      ;49.5
        test      edx, edx                                      ;49.5
        jbe       .B3.40        ; Prob 10%                      ;49.5
                                ; LOE eax edx ecx esi
.B3.37:                         ; Preds .B3.36
        xor       ebx, ebx                                      ;
        xor       edi, edi                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B3.38:                         ; Preds .B3.38 .B3.37
        mov       DWORD PTR [eax+ebx*4], edi                    ;49.5
        inc       ebx                                           ;49.5
        cmp       ebx, edx                                      ;49.5
        jb        .B3.38        ; Prob 82%                      ;49.5
                                ; LOE eax edx ecx ebx esi edi
.B3.40:                         ; Preds .B3.38 .B3.36
        pxor      xmm0, xmm0                                    ;49.5
                                ; LOE eax edx ecx esi xmm0
.B3.41:                         ; Preds .B3.41 .B3.40
        movaps    XMMWORD PTR [eax+edx*4], xmm0                 ;49.5
        movaps    XMMWORD PTR [16+eax+edx*4], xmm0              ;49.5
        add       edx, 8                                        ;49.5
        cmp       edx, ecx                                      ;49.5
        jb        .B3.41        ; Prob 82%                      ;49.5
                                ; LOE eax edx ecx esi xmm0
.B3.43:                         ; Preds .B3.41 .B3.57
        cmp       ecx, esi                                      ;49.5
        jae       .B3.53        ; Prob 10%                      ;49.5
                                ; LOE eax ecx esi
.B3.44:                         ; Preds .B3.43
        xor       edx, edx                                      ;
                                ; LOE eax edx ecx esi
.B3.45:                         ; Preds .B3.45 .B3.44
        mov       DWORD PTR [eax+ecx*4], edx                    ;49.5
        inc       ecx                                           ;49.5
        cmp       ecx, esi                                      ;49.5
        jb        .B3.45        ; Prob 82%                      ;49.5
        jmp       .B3.53        ; Prob 100%                     ;49.5
                                ; LOE eax edx ecx esi
.B3.47:                         ; Preds .B3.30
        mov       ebx, esi                                      ;49.5
        shr       ebx, 31                                       ;49.5
        add       ebx, esi                                      ;49.5
        sar       ebx, 1                                        ;49.5
        mov       DWORD PTR [20+esp], ebx                       ;49.5
        test      ebx, ebx                                      ;49.5
        jbe       .B3.60        ; Prob 10%                      ;49.5
                                ; LOE eax edx ecx esi
.B3.48:                         ; Preds .B3.47
        xor       ebx, ebx                                      ;
        lea       edi, DWORD PTR [eax+ecx]                      ;
        mov       DWORD PTR [8+esp], edi                        ;
        xor       edi, edi                                      ;
        mov       DWORD PTR [4+esp], esi                        ;
        mov       DWORD PTR [esp], edx                          ;
        mov       edx, ebx                                      ;
        mov       esi, DWORD PTR [8+esp]                        ;
                                ; LOE eax edx ecx ebx esi edi
.B3.49:                         ; Preds .B3.49 .B3.48
        inc       ebx                                           ;49.5
        mov       DWORD PTR [eax+edx*2], edi                    ;49.5
        mov       DWORD PTR [esi+edx*2], edi                    ;49.5
        add       edx, ecx                                      ;49.5
        cmp       ebx, DWORD PTR [20+esp]                       ;49.5
        jb        .B3.49        ; Prob 63%                      ;49.5
                                ; LOE eax edx ecx ebx esi edi
.B3.50:                         ; Preds .B3.49
        mov       esi, DWORD PTR [4+esp]                        ;
        lea       ebx, DWORD PTR [1+ebx+ebx]                    ;49.5
        mov       edx, DWORD PTR [esp]                          ;
                                ; LOE eax edx ecx ebx esi
.B3.51:                         ; Preds .B3.50 .B3.60
        lea       edi, DWORD PTR [-1+ebx]                       ;49.5
        cmp       esi, edi                                      ;49.5
        jbe       .B3.53        ; Prob 10%                      ;49.5
                                ; LOE eax edx ecx ebx
.B3.52:                         ; Preds .B3.51
        mov       esi, edx                                      ;49.5
        lea       edx, DWORD PTR [-1+ebx+edx]                   ;49.5
        neg       esi                                           ;49.5
        add       esi, edx                                      ;49.5
        imul      esi, ecx                                      ;49.5
        mov       DWORD PTR [eax+esi], 0                        ;49.5
                                ; LOE
.B3.53:                         ; Preds .B3.45 .B3.51 .B3.29 .B3.43 .B3.52
                                ;      
        add       esp, 36                                       ;51.1
        pop       ebx                                           ;51.1
        pop       edi                                           ;51.1
        pop       esi                                           ;51.1
        mov       esp, ebp                                      ;51.1
        pop       ebp                                           ;51.1
        ret                                                     ;51.1
                                ; LOE
.B3.54:                         ; Preds .B3.7 .B3.11 .B3.9      ; Infreq
        xor       eax, eax                                      ;48.5
        jmp       .B3.19        ; Prob 100%                     ;48.5
                                ; LOE eax edx ecx ebx
.B3.57:                         ; Preds .B3.31 .B3.35 .B3.33    ; Infreq
        xor       ecx, ecx                                      ;49.5
        jmp       .B3.43        ; Prob 100%                     ;49.5
                                ; LOE eax ecx esi
.B3.60:                         ; Preds .B3.47                  ; Infreq
        mov       ebx, 1                                        ;
        jmp       .B3.51        ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B3.61:                         ; Preds .B3.23                  ; Infreq
        mov       esi, 1                                        ;
        jmp       .B3.27        ; Prob 100%                     ;
        ALIGN     16
                                ; LOE eax edx ecx ebx esi
; mark_end;
_DEQUEUE_mp_CREATE_QUEUE_ARRAY ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DEQUEUE_mp_CREATE_QUEUE_ARRAY
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DEQUEUE_mp_DESTROY_QUEUE_ARRAY
; mark_begin;
       ALIGN     16
	PUBLIC _DEQUEUE_mp_DESTROY_QUEUE_ARRAY
_DEQUEUE_mp_DESTROY_QUEUE_ARRAY	PROC NEAR 
; parameter 1: 8 + ebp
.B4.1:                          ; Preds .B4.0
        push      ebp                                           ;54.12
        mov       ebp, esp                                      ;54.12
        and       esp, -16                                      ;54.12
        push      esi                                           ;54.12
        push      edi                                           ;54.12
        push      ebx                                           ;54.12
        sub       esp, 52                                       ;54.12
        mov       esi, DWORD PTR [8+ebp]                        ;54.12
        mov       edi, DWORD PTR [4+esi]                        ;59.21
        test      edi, edi                                      ;59.21
        mov       eax, DWORD PTR [16+esi]                       ;63.5
        mov       ebx, DWORD PTR [52+esi]                       ;64.5
        jle       .B4.12        ; Prob 6%                       ;59.21
                                ; LOE eax ebx esi edi
.B4.2:                          ; Preds .B4.1
        mov       edx, DWORD PTR [esi]                          ;60.14
        mov       ecx, DWORD PTR [8+esi]                        ;60.14
        mov       DWORD PTR [40+esp], edx                       ;60.14
        jle       .B4.17        ; Prob 16%                      ;60.14
                                ; LOE eax ecx ebx esi edi
.B4.3:                          ; Preds .B4.2
        mov       DWORD PTR [44+esp], ebx                       ;
        mov       ebx, ecx                                      ;
        mov       DWORD PTR [48+esp], eax                       ;
                                ; LOE ebx esi edi
.B4.4:                          ; Preds .B4.10 .B4.3
        xor       edx, edx                                      ;60.14
                                ; LOE edx ebx esi edi
.B4.5:                          ; Preds .B4.17 .B4.4
        test      dl, 1                                         ;60.14
        jne       .B4.15        ; Prob 3%                       ;60.14
                                ; LOE ebx esi edi
.B4.6:                          ; Preds .B4.5
        mov       eax, DWORD PTR [40+esp]                       ;60.14
        lea       edx, DWORD PTR [-1+eax]                       ;60.14
        cmp       ebx, edx                                      ;60.14
        jge       .B4.8         ; Prob 50%                      ;60.14
                                ; LOE ebx esi edi
.B4.7:                          ; Preds .B4.6
        inc       ebx                                           ;60.14
        mov       DWORD PTR [8+esi], ebx                        ;60.14
        jmp       .B4.9         ; Prob 100%                     ;60.14
                                ; LOE ebx esi edi
.B4.8:                          ; Preds .B4.6
        mov       DWORD PTR [8+esi], 0                          ;60.14
        xor       ebx, ebx                                      ;
                                ; LOE ebx esi edi
.B4.9:                          ; Preds .B4.7 .B4.8
        mov       ecx, DWORD PTR [48+esi]                       ;60.14
        xor       eax, eax                                      ;60.14
        neg       ecx                                           ;60.14
        dec       edi                                           ;60.14
        add       ecx, ebx                                      ;60.14
        imul      ecx, DWORD PTR [44+esi]                       ;60.14
        mov       edx, DWORD PTR [48+esp]                       ;60.14
        mov       DWORD PTR [4+esi], edi                        ;60.14
        mov       DWORD PTR [edx+ecx], eax                      ;60.14
        mov       ecx, DWORD PTR [84+esi]                       ;60.14
        neg       ecx                                           ;60.14
        add       ecx, ebx                                      ;60.14
        imul      ecx, DWORD PTR [80+esi]                       ;60.14
        mov       edx, DWORD PTR [44+esp]                       ;60.14
        mov       DWORD PTR [edx+ecx], eax                      ;60.14
                                ; LOE ebx esi edi
.B4.10:                         ; Preds .B4.21 .B4.9
        test      edi, edi                                      ;59.21
        jg        .B4.4         ; Prob 84%                      ;59.21
                                ; LOE ebx esi edi
.B4.11:                         ; Preds .B4.10
        mov       ebx, DWORD PTR [44+esp]                       ;
        mov       eax, DWORD PTR [48+esp]                       ;
                                ; LOE eax ebx esi
.B4.12:                         ; Preds .B4.1 .B4.11
        mov       edi, DWORD PTR [28+esi]                       ;63.17
        mov       ecx, edi                                      ;63.5
        shr       ecx, 1                                        ;63.5
        mov       edx, edi                                      ;63.5
        and       ecx, 1                                        ;63.5
        and       edx, 1                                        ;63.5
        shl       ecx, 2                                        ;63.5
        add       edx, edx                                      ;63.5
        or        ecx, edx                                      ;63.5
        or        ecx, 262144                                   ;63.5
        push      ecx                                           ;63.5
        push      eax                                           ;63.5
        call      _for_dealloc_allocatable                      ;63.5
                                ; LOE ebx esi edi
.B4.13:                         ; Preds .B4.12
        and       edi, -2                                       ;63.5
        mov       DWORD PTR [28+esi], edi                       ;63.5
        mov       edi, DWORD PTR [64+esi]                       ;64.17
        mov       edx, edi                                      ;64.5
        shr       edx, 1                                        ;64.5
        mov       eax, edi                                      ;64.5
        and       edx, 1                                        ;64.5
        and       eax, 1                                        ;64.5
        shl       edx, 2                                        ;64.5
        add       eax, eax                                      ;64.5
        or        edx, eax                                      ;64.5
        or        edx, 262144                                   ;64.5
        push      edx                                           ;64.5
        push      ebx                                           ;64.5
        mov       DWORD PTR [16+esi], 0                         ;63.5
        call      _for_dealloc_allocatable                      ;64.5
                                ; LOE esi edi
.B4.14:                         ; Preds .B4.13
        and       edi, -2                                       ;64.5
        mov       DWORD PTR [52+esi], 0                         ;64.5
        mov       DWORD PTR [64+esi], edi                       ;64.5
        add       esp, 68                                       ;66.1
        pop       ebx                                           ;66.1
        pop       edi                                           ;66.1
        pop       esi                                           ;66.1
        mov       esp, ebp                                      ;66.1
        pop       ebp                                           ;66.1
        ret                                                     ;66.1
                                ; LOE
.B4.15:                         ; Preds .B4.5                   ; Infreq
        mov       DWORD PTR [esp], 0                            ;60.14
        lea       eax, DWORD PTR [32+esp]                       ;60.14
        mov       DWORD PTR [32+esp], 42                        ;60.14
        mov       DWORD PTR [36+esp], OFFSET FLAT: __STRLITPACK_3 ;60.14
        push      32                                            ;60.14
        push      eax                                           ;60.14
        push      OFFSET FLAT: __STRLITPACK_8.0.7               ;60.14
        push      -2088435968                                   ;60.14
        push      -1                                            ;60.14
        lea       edx, DWORD PTR [20+esp]                       ;60.14
        push      edx                                           ;60.14
        call      _for_write_seq_lis                            ;60.14
                                ; LOE ebx esi edi
.B4.16:                         ; Preds .B4.15                  ; Infreq
        push      32                                            ;60.14
        xor       eax, eax                                      ;60.14
        push      eax                                           ;60.14
        push      eax                                           ;60.14
        push      -2088435968                                   ;60.14
        push      eax                                           ;60.14
        push      OFFSET FLAT: __STRLITPACK_9                   ;60.14
        call      _for_stop_core                                ;60.14
                                ; LOE ebx esi edi
.B4.21:                         ; Preds .B4.16                  ; Infreq
        add       esp, 48                                       ;60.14
        jmp       .B4.10        ; Prob 100%                     ;60.14
                                ; LOE ebx esi edi
.B4.17:                         ; Preds .B4.2                   ; Infreq
        mov       DWORD PTR [44+esp], ebx                       ;60.14
        mov       edx, -1                                       ;60.14
        mov       DWORD PTR [48+esp], eax                       ;60.14
        mov       ebx, ecx                                      ;60.14
        jmp       .B4.5         ; Prob 100%                     ;60.14
        ALIGN     16
                                ; LOE edx ebx esi edi
; mark_end;
_DEQUEUE_mp_DESTROY_QUEUE_ARRAY ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DEQUEUE_mp_DESTROY_QUEUE_ARRAY
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DEQUEUE_mp_ENQUEUE_BACK_ARRAY
; mark_begin;
       ALIGN     16
	PUBLIC _DEQUEUE_mp_ENQUEUE_BACK_ARRAY
_DEQUEUE_mp_ENQUEUE_BACK_ARRAY	PROC NEAR 
; parameter 1: 8 + ebp
; parameter 2: 12 + ebp
; parameter 3: 16 + ebp
.B5.1:                          ; Preds .B5.0
        push      ebp                                           ;69.12
        mov       ebp, esp                                      ;69.12
        and       esp, -16                                      ;69.12
        push      esi                                           ;69.12
        push      edi                                           ;69.12
        push      ebx                                           ;69.12
        sub       esp, 52                                       ;69.12
        mov       eax, DWORD PTR [8+ebp]                        ;69.12
        mov       edx, DWORD PTR [4+eax]                        ;74.9
        mov       esi, DWORD PTR [eax]                          ;74.9
        cmp       edx, esi                                      ;74.9
        jl        .B5.3         ; Prob 60%                      ;74.9
                                ; LOE edx esi
.B5.2:                          ; Preds .B5.1
        mov       DWORD PTR [esp], 0                            ;75.9
        lea       edx, DWORD PTR [esp]                          ;75.9
        mov       DWORD PTR [32+esp], 40                        ;75.9
        lea       eax, DWORD PTR [32+esp]                       ;75.9
        mov       DWORD PTR [36+esp], OFFSET FLAT: __STRLITPACK_5 ;75.9
        push      32                                            ;75.9
        push      eax                                           ;75.9
        push      OFFSET FLAT: __STRLITPACK_6.0.5               ;75.9
        push      -2088435968                                   ;75.9
        push      -1                                            ;75.9
        push      edx                                           ;75.9
        call      _for_write_seq_lis                            ;75.9
                                ; LOE
.B5.7:                          ; Preds .B5.2
        add       esp, 76                                       ;75.9
        pop       ebx                                           ;75.9
        pop       edi                                           ;75.9
        pop       esi                                           ;75.9
        mov       esp, ebp                                      ;75.9
        pop       ebp                                           ;75.9
        ret                                                     ;75.9
                                ; LOE
.B5.3:                          ; Preds .B5.1
        mov       ecx, DWORD PTR [8+ebp]                        ;77.17
        dec       esi                                           ;79.48
        mov       DWORD PTR [esp], edx                          ;
        mov       edi, DWORD PTR [16+ebp]                       ;78.9
        mov       ebx, DWORD PTR [48+ecx]                       ;77.9
        mov       edx, DWORD PTR [12+ecx]                       ;77.9
        neg       ebx                                           ;78.9
        add       ebx, edx                                      ;78.9
        imul      ebx, DWORD PTR [44+ecx]                       ;78.9
        cmp       edx, esi                                      ;82.13
        cvttss2si edi, DWORD PTR [edi]                          ;78.9
        mov       eax, DWORD PTR [16+ecx]                       ;77.17
        mov       esi, 0                                        ;82.13
        mov       DWORD PTR [eax+ebx], edi                      ;78.9
        lea       eax, DWORD PTR [1+edx]                        ;82.13
        cmovne    esi, eax                                      ;82.13
        mov       eax, DWORD PTR [esp]                          ;84.9
        inc       eax                                           ;84.9
        mov       DWORD PTR [12+ecx], esi                       ;80.13
        mov       DWORD PTR [4+ecx], eax                        ;84.9
                                ; LOE
.B5.4:                          ; Preds .B5.3
        add       esp, 52                                       ;87.1
        pop       ebx                                           ;87.1
        pop       edi                                           ;87.1
        pop       esi                                           ;87.1
        mov       esp, ebp                                      ;87.1
        pop       ebp                                           ;87.1
        ret                                                     ;87.1
        ALIGN     16
                                ; LOE
; mark_end;
_DEQUEUE_mp_ENQUEUE_BACK_ARRAY ENDP
_TEXT	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
__STRLITPACK_6.0.5	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DEQUEUE_mp_ENQUEUE_BACK_ARRAY
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DEQUEUE_mp_ENQUEUE_FRONT_ARRAY
; mark_begin;
       ALIGN     16
	PUBLIC _DEQUEUE_mp_ENQUEUE_FRONT_ARRAY
_DEQUEUE_mp_ENQUEUE_FRONT_ARRAY	PROC NEAR 
; parameter 1: 8 + ebp
; parameter 2: 12 + ebp
; parameter 3: 16 + ebp
.B6.1:                          ; Preds .B6.0
        push      ebp                                           ;90.12
        mov       ebp, esp                                      ;90.12
        and       esp, -16                                      ;90.12
        push      esi                                           ;90.12
        push      edi                                           ;90.12
        push      ebx                                           ;90.12
        sub       esp, 52                                       ;90.12
        mov       eax, DWORD PTR [8+ebp]                        ;90.12
        mov       edx, DWORD PTR [4+eax]                        ;95.9
        mov       eax, DWORD PTR [eax]                          ;95.9
        cmp       edx, eax                                      ;95.9
        jl        .B6.3         ; Prob 60%                      ;95.9
                                ; LOE eax edx
.B6.2:                          ; Preds .B6.1
        mov       DWORD PTR [esp], 0                            ;96.9
        lea       edx, DWORD PTR [esp]                          ;96.9
        mov       DWORD PTR [32+esp], 40                        ;96.9
        lea       eax, DWORD PTR [32+esp]                       ;96.9
        mov       DWORD PTR [36+esp], OFFSET FLAT: __STRLITPACK_4 ;96.9
        push      32                                            ;96.9
        push      eax                                           ;96.9
        push      OFFSET FLAT: __STRLITPACK_7.0.6               ;96.9
        push      -2088435968                                   ;96.9
        push      -1                                            ;96.9
        push      edx                                           ;96.9
        call      _for_write_seq_lis                            ;96.9
                                ; LOE
.B6.7:                          ; Preds .B6.2
        add       esp, 76                                       ;96.9
        pop       ebx                                           ;96.9
        pop       edi                                           ;96.9
        pop       esi                                           ;96.9
        mov       esp, ebp                                      ;96.9
        pop       ebp                                           ;96.9
        ret                                                     ;96.9
                                ; LOE
.B6.3:                          ; Preds .B6.1
        mov       ecx, DWORD PTR [8+ebp]                        ;98.9
        dec       eax                                           ;101.13
        mov       DWORD PTR [esp], edx                          ;
        mov       esi, DWORD PTR [12+ebp]                       ;98.9
        mov       ebx, DWORD PTR [48+ecx]                       ;98.9
        mov       edx, DWORD PTR [8+ecx]                        ;98.9
        neg       ebx                                           ;98.9
        add       ebx, edx                                      ;98.9
        imul      ebx, DWORD PTR [44+ecx]                       ;98.9
        mov       edi, DWORD PTR [16+ecx]                       ;98.17
        mov       esi, DWORD PTR [esi]                          ;98.9
        mov       DWORD PTR [edi+ebx], esi                      ;98.9
        mov       esi, DWORD PTR [84+ecx]                       ;99.9
        neg       esi                                           ;99.9
        add       esi, edx                                      ;99.9
        imul      esi, DWORD PTR [80+ecx]                       ;99.9
        test      edx, edx                                      ;101.13
        mov       edi, DWORD PTR [16+ebp]                       ;99.9
        mov       ebx, DWORD PTR [52+ecx]                       ;99.17
        mov       edi, DWORD PTR [edi]                          ;99.9
        mov       DWORD PTR [ebx+esi], edi                      ;99.9
        lea       ebx, DWORD PTR [-1+edx]                       ;103.13
        cmove     ebx, eax                                      ;101.13
        mov       eax, DWORD PTR [esp]                          ;105.9
        inc       eax                                           ;105.9
        mov       DWORD PTR [8+ecx], ebx                        ;101.13
        mov       DWORD PTR [4+ecx], eax                        ;105.9
                                ; LOE
.B6.4:                          ; Preds .B6.3
        add       esp, 52                                       ;108.1
        pop       ebx                                           ;108.1
        pop       edi                                           ;108.1
        pop       esi                                           ;108.1
        mov       esp, ebp                                      ;108.1
        pop       ebp                                           ;108.1
        ret                                                     ;108.1
        ALIGN     16
                                ; LOE
; mark_end;
_DEQUEUE_mp_ENQUEUE_FRONT_ARRAY ENDP
_TEXT	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_7.0.6	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DEQUEUE_mp_ENQUEUE_FRONT_ARRAY
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DEQUEUE_mp_DISQUEUE_FRONT_ARRAY
; mark_begin;
       ALIGN     16
	PUBLIC _DEQUEUE_mp_DISQUEUE_FRONT_ARRAY
_DEQUEUE_mp_DISQUEUE_FRONT_ARRAY	PROC NEAR 
; parameter 1: 8 + ebp
; parameter 2: 12 + ebp
; parameter 3: 16 + ebp
.B7.1:                          ; Preds .B7.0
        push      ebp                                           ;111.12
        mov       ebp, esp                                      ;111.12
        and       esp, -16                                      ;111.12
        push      esi                                           ;111.12
        push      edi                                           ;111.12
        push      ebx                                           ;111.12
        sub       esp, 52                                       ;111.12
        mov       edi, DWORD PTR [8+ebp]                        ;111.12
        mov       eax, DWORD PTR [4+edi]                        ;116.9
        test      eax, eax                                      ;116.9
        jle       .B7.4         ; Prob 3%                       ;116.9
                                ; LOE eax edi
.B7.2:                          ; Preds .B7.1
        mov       ecx, DWORD PTR [8+edi]                        ;122.12
        xor       esi, esi                                      ;125.13
        mov       DWORD PTR [esp], eax                          ;
        mov       eax, DWORD PTR [edi]                          ;122.28
        dec       eax                                           ;122.48
        lea       ebx, DWORD PTR [1+ecx]                        ;123.13
        cmp       ecx, eax                                      ;125.13
        mov       edx, DWORD PTR [48+edi]                       ;127.18
        cmovge    ebx, esi                                      ;125.13
        neg       edx                                           ;127.9
        add       edx, ebx                                      ;127.9
        imul      edx, DWORD PTR [44+edi]                       ;127.9
        mov       ecx, DWORD PTR [16+edi]                       ;127.26
        mov       DWORD PTR [4+esp], edx                        ;127.9
        mov       DWORD PTR [8+edi], ebx                        ;123.13
        mov       eax, DWORD PTR [ecx+edx]                      ;127.9
        mov       edx, DWORD PTR [12+ebp]                       ;127.9
        mov       DWORD PTR [edx], eax                          ;127.9
        mov       edx, DWORD PTR [84+edi]                       ;128.17
        neg       edx                                           ;128.9
        add       edx, ebx                                      ;128.9
        imul      edx, DWORD PTR [80+edi]                       ;128.9
        mov       eax, DWORD PTR [52+edi]                       ;128.25
        mov       ebx, DWORD PTR [16+ebp]                       ;128.9
        mov       edi, DWORD PTR [eax+edx]                      ;128.9
        mov       DWORD PTR [ebx], edi                          ;128.9
        mov       edi, DWORD PTR [4+esp]                        ;129.9
        mov       DWORD PTR [ecx+edi], esi                      ;129.9
        mov       DWORD PTR [eax+edx], esi                      ;130.9
        mov       edx, DWORD PTR [8+ebp]                        ;131.9
        mov       eax, DWORD PTR [esp]                          ;131.9
        dec       eax                                           ;131.9
        mov       DWORD PTR [4+edx], eax                        ;131.9
                                ; LOE
.B7.3:                          ; Preds .B7.2
        add       esp, 52                                       ;134.1
        pop       ebx                                           ;134.1
        pop       edi                                           ;134.1
        pop       esi                                           ;134.1
        mov       esp, ebp                                      ;134.1
        pop       ebp                                           ;134.1
        ret                                                     ;134.1
                                ; LOE
.B7.4:                          ; Preds .B7.1                   ; Infreq
        mov       DWORD PTR [esp], 0                            ;117.9
        lea       edx, DWORD PTR [esp]                          ;117.9
        mov       DWORD PTR [32+esp], 42                        ;117.9
        lea       eax, DWORD PTR [32+esp]                       ;117.9
        mov       DWORD PTR [36+esp], OFFSET FLAT: __STRLITPACK_3 ;117.9
        push      32                                            ;117.9
        push      eax                                           ;117.9
        push      OFFSET FLAT: __STRLITPACK_8.0.7               ;117.9
        push      -2088435968                                   ;117.9
        push      -1                                            ;117.9
        push      edx                                           ;117.9
        call      _for_write_seq_lis                            ;117.9
                                ; LOE
.B7.5:                          ; Preds .B7.4                   ; Infreq
        push      32                                            ;118.9
        xor       ebx, ebx                                      ;118.9
        push      ebx                                           ;118.9
        push      ebx                                           ;118.9
        push      -2088435968                                   ;118.9
        push      ebx                                           ;118.9
        push      OFFSET FLAT: __STRLITPACK_9                   ;118.9
        call      _for_stop_core                                ;118.9
                                ; LOE ebx
.B7.6:                          ; Preds .B7.5                   ; Infreq
        mov       eax, DWORD PTR [12+ebp]                       ;119.9
        mov       edx, DWORD PTR [16+ebp]                       ;120.9
        mov       DWORD PTR [eax], ebx                          ;119.9
        mov       DWORD PTR [edx], ebx                          ;120.9
        add       esp, 100                                      ;120.9
        pop       ebx                                           ;120.9
        pop       edi                                           ;120.9
        pop       esi                                           ;120.9
        mov       esp, ebp                                      ;120.9
        pop       ebp                                           ;120.9
        ret                                                     ;120.9
        ALIGN     16
                                ; LOE
; mark_end;
_DEQUEUE_mp_DISQUEUE_FRONT_ARRAY ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DEQUEUE_mp_DISQUEUE_FRONT_ARRAY
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DEQUEUE_mp_DISQUEUE_BACK_ARRAY
; mark_begin;
       ALIGN     16
	PUBLIC _DEQUEUE_mp_DISQUEUE_BACK_ARRAY
_DEQUEUE_mp_DISQUEUE_BACK_ARRAY	PROC NEAR 
; parameter 1: 8 + ebp
; parameter 2: 12 + ebp
; parameter 3: 16 + ebp
.B8.1:                          ; Preds .B8.0
        push      ebp                                           ;137.12
        mov       ebp, esp                                      ;137.12
        and       esp, -16                                      ;137.12
        push      esi                                           ;137.12
        push      edi                                           ;137.12
        push      ebx                                           ;137.12
        sub       esp, 52                                       ;137.12
        mov       edi, DWORD PTR [8+ebp]                        ;137.12
        mov       eax, DWORD PTR [4+edi]                        ;142.9
        test      eax, eax                                      ;142.9
        jg        .B8.4         ; Prob 60%                      ;142.9
                                ; LOE eax edi
.B8.2:                          ; Preds .B8.1
        mov       DWORD PTR [esp], 0                            ;143.9
        lea       edx, DWORD PTR [esp]                          ;143.9
        mov       DWORD PTR [32+esp], 42                        ;143.9
        lea       eax, DWORD PTR [32+esp]                       ;143.9
        mov       DWORD PTR [36+esp], OFFSET FLAT: __STRLITPACK_2 ;143.9
        push      32                                            ;143.9
        push      eax                                           ;143.9
        push      OFFSET FLAT: __STRLITPACK_10.0.8              ;143.9
        push      -2088435968                                   ;143.9
        push      -1                                            ;143.9
        push      edx                                           ;143.9
        call      _for_write_seq_lis                            ;143.9
                                ; LOE
.B8.3:                          ; Preds .B8.2
        mov       eax, DWORD PTR [12+ebp]                       ;144.9
        xor       edx, edx                                      ;144.9
        mov       ecx, DWORD PTR [16+ebp]                       ;145.9
        mov       DWORD PTR [eax], edx                          ;144.9
        mov       DWORD PTR [ecx], edx                          ;145.9
        add       esp, 76                                       ;145.9
        pop       ebx                                           ;145.9
        pop       edi                                           ;145.9
        pop       esi                                           ;145.9
        mov       esp, ebp                                      ;145.9
        pop       ebp                                           ;145.9
        ret                                                     ;145.9
                                ; LOE
.B8.4:                          ; Preds .B8.1
        mov       ecx, DWORD PTR [12+edi]                       ;147.12
        mov       esi, DWORD PTR [edi]                          ;150.28
        dec       esi                                           ;150.13
        test      ecx, ecx                                      ;150.13
        mov       edx, DWORD PTR [12+ebp]                       ;152.9
        lea       ebx, DWORD PTR [-1+ecx]                       ;148.13
        cmovg     esi, ebx                                      ;150.13
        mov       ebx, DWORD PTR [48+edi]                       ;152.18
        neg       ebx                                           ;152.9
        add       ebx, esi                                      ;152.9
        imul      ebx, DWORD PTR [44+edi]                       ;152.9
        mov       ecx, DWORD PTR [16+edi]                       ;152.26
        mov       DWORD PTR [esp], eax                          ;
        mov       DWORD PTR [12+edi], esi                       ;148.13
        mov       eax, DWORD PTR [ecx+ebx]                      ;152.9
        mov       DWORD PTR [edx], eax                          ;152.9
        mov       edx, DWORD PTR [84+edi]                       ;153.17
        neg       edx                                           ;153.9
        add       edx, esi                                      ;153.9
        imul      edx, DWORD PTR [80+edi]                       ;153.9
        mov       eax, DWORD PTR [52+edi]                       ;153.25
        mov       esi, DWORD PTR [16+ebp]                       ;153.9
        mov       edi, DWORD PTR [eax+edx]                      ;153.9
        mov       DWORD PTR [esi], edi                          ;153.9
        xor       edi, edi                                      ;154.9
        mov       DWORD PTR [ecx+ebx], edi                      ;154.9
        mov       DWORD PTR [eax+edx], edi                      ;155.9
        mov       edx, DWORD PTR [8+ebp]                        ;156.9
        mov       eax, DWORD PTR [esp]                          ;156.9
        dec       eax                                           ;156.9
        mov       DWORD PTR [4+edx], eax                        ;156.9
                                ; LOE
.B8.5:                          ; Preds .B8.4
        add       esp, 52                                       ;159.1
        pop       ebx                                           ;159.1
        pop       edi                                           ;159.1
        pop       esi                                           ;159.1
        mov       esp, ebp                                      ;159.1
        pop       ebp                                           ;159.1
        ret                                                     ;159.1
        ALIGN     16
                                ; LOE
; mark_end;
_DEQUEUE_mp_DISQUEUE_BACK_ARRAY ENDP
_TEXT	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_10.0.8	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DEQUEUE_mp_DISQUEUE_BACK_ARRAY
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DEQUEUE_mp_IS_EMPTY_ARRAY
; mark_begin;
       ALIGN     16
	PUBLIC _DEQUEUE_mp_IS_EMPTY_ARRAY
_DEQUEUE_mp_IS_EMPTY_ARRAY	PROC NEAR 
; parameter 1: 4 + esp
.B9.1:                          ; Preds .B9.0
        mov       ecx, -1                                       ;168.1
        mov       edx, DWORD PTR [4+esp]                        ;162.15
        xor       eax, eax                                      ;168.1
        cmp       DWORD PTR [4+edx], 0                          ;168.1
        cmovle    eax, ecx                                      ;168.1
        ret                                                     ;168.1
        ALIGN     16
                                ; LOE
; mark_end;
_DEQUEUE_mp_IS_EMPTY_ARRAY ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DEQUEUE_mp_IS_EMPTY_ARRAY
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DEQUEUE_mp_IS_FULL_ARRAY
; mark_begin;
       ALIGN     16
	PUBLIC _DEQUEUE_mp_IS_FULL_ARRAY
_DEQUEUE_mp_IS_FULL_ARRAY	PROC NEAR 
; parameter 1: 4 + esp
.B10.1:                         ; Preds .B10.0
        mov       edx, -1                                       ;177.1
        mov       eax, DWORD PTR [4+esp]                        ;171.15
        mov       ecx, DWORD PTR [4+eax]                        ;175.10
        cmp       ecx, DWORD PTR [eax]                          ;177.1
        mov       eax, 0                                        ;177.1
        cmovge    eax, edx                                      ;177.1
        ret                                                     ;177.1
        ALIGN     16
                                ; LOE
; mark_end;
_DEQUEUE_mp_IS_FULL_ARRAY ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DEQUEUE_mp_IS_FULL_ARRAY
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DEQUEUE_mp_PRINT_QUEUE_ARRAY
; mark_begin;
       ALIGN     16
	PUBLIC _DEQUEUE_mp_PRINT_QUEUE_ARRAY
_DEQUEUE_mp_PRINT_QUEUE_ARRAY	PROC NEAR 
; parameter 1: 8 + ebp
.B11.1:                         ; Preds .B11.0
        push      ebp                                           ;180.12
        mov       ebp, esp                                      ;180.12
        and       esp, -16                                      ;180.12
        push      esi                                           ;180.12
        push      edi                                           ;180.12
        push      ebx                                           ;180.12
        sub       esp, 68                                       ;180.12
        xor       eax, eax                                      ;182.5
        mov       DWORD PTR [16+esp], eax                       ;182.5
        lea       ebx, DWORD PTR [16+esp]                       ;182.5
        push      32                                            ;182.5
        push      eax                                           ;182.5
        push      OFFSET FLAT: __STRLITPACK_11.0.11             ;182.5
        push      -2088435968                                   ;182.5
        push      -1                                            ;182.5
        push      ebx                                           ;182.5
        call      _for_write_seq_lis                            ;182.5
                                ; LOE ebx
.B11.15:                        ; Preds .B11.1
        add       esp, 24                                       ;182.5
                                ; LOE ebx
.B11.2:                         ; Preds .B11.15
        mov       ecx, DWORD PTR [_DEQUEUE_mp_M]                ;182.5
        dec       ecx                                           ;182.5
        js        .B11.7        ; Prob 2%                       ;182.5
                                ; LOE ecx ebx
.B11.3:                         ; Preds .B11.2
        mov       edx, DWORD PTR [8+ebp]                        ;182.25
        xor       eax, eax                                      ;
        mov       DWORD PTR [4+esp], ecx                        ;
        mov       ebx, eax                                      ;
        mov       esi, DWORD PTR [16+edx]                       ;182.25
        mov       edi, DWORD PTR [48+edx]                       ;182.17
        mov       edx, DWORD PTR [44+edx]                       ;182.17
        imul      edi, edx                                      ;
        mov       DWORD PTR [esp], edx                          ;
        sub       esi, edi                                      ;
        xor       edi, edi                                      ;
                                ; LOE ebx esi edi
.B11.4:                         ; Preds .B11.5 .B11.3
        mov       edx, DWORD PTR [edi+esi]                      ;182.5
        lea       ecx, DWORD PTR [64+esp]                       ;182.5
        mov       DWORD PTR [64+esp], edx                       ;182.5
        push      ecx                                           ;182.5
        push      OFFSET FLAT: __STRLITPACK_12.0.11             ;182.5
        lea       edx, DWORD PTR [24+esp]                       ;182.5
        push      edx                                           ;182.5
        call      _for_write_seq_lis_xmit                       ;182.5
                                ; LOE ebx esi edi
.B11.16:                        ; Preds .B11.4
        add       esp, 12                                       ;182.5
                                ; LOE ebx esi edi
.B11.5:                         ; Preds .B11.16
        inc       ebx                                           ;182.5
        add       edi, DWORD PTR [esp]                          ;182.5
        cmp       ebx, DWORD PTR [4+esp]                        ;182.5
        jle       .B11.4        ; Prob 82%                      ;182.5
                                ; LOE ebx esi edi
.B11.6:                         ; Preds .B11.5
        lea       ebx, DWORD PTR [16+esp]                       ;
                                ; LOE ebx
.B11.7:                         ; Preds .B11.6 .B11.2
        push      0                                             ;182.5
        push      OFFSET FLAT: __STRLITPACK_13.0.11             ;182.5
        push      ebx                                           ;182.5
        call      _for_write_seq_lis_xmit                       ;182.5
                                ; LOE ebx
.B11.8:                         ; Preds .B11.7
        mov       DWORD PTR [28+esp], 0                         ;183.5
        lea       eax, DWORD PTR [12+esp]                       ;183.5
        mov       DWORD PTR [12+esp], 26                        ;183.5
        mov       DWORD PTR [16+esp], OFFSET FLAT: __STRLITPACK_0 ;183.5
        push      32                                            ;183.5
        push      eax                                           ;183.5
        push      OFFSET FLAT: __STRLITPACK_14.0.11             ;183.5
        push      -2088435968                                   ;183.5
        push      -1                                            ;183.5
        push      ebx                                           ;183.5
        call      _for_write_seq_lis                            ;183.5
                                ; LOE ebx
.B11.9:                         ; Preds .B11.8
        mov       eax, DWORD PTR [8+ebp]                        ;183.5
        lea       ecx, DWORD PTR [44+esp]                       ;183.5
        mov       edx, DWORD PTR [8+eax]                        ;183.5
        mov       DWORD PTR [44+esp], edx                       ;183.5
        push      ecx                                           ;183.5
        push      OFFSET FLAT: __STRLITPACK_15.0.11             ;183.5
        push      ebx                                           ;183.5
        call      _for_write_seq_lis_xmit                       ;183.5
                                ; LOE ebx
.B11.10:                        ; Preds .B11.9
        mov       eax, DWORD PTR [8+ebp]                        ;183.5
        lea       ecx, DWORD PTR [96+esp]                       ;183.5
        mov       edx, DWORD PTR [12+eax]                       ;183.5
        mov       DWORD PTR [96+esp], edx                       ;183.5
        push      ecx                                           ;183.5
        push      OFFSET FLAT: __STRLITPACK_16.0.11             ;183.5
        push      ebx                                           ;183.5
        call      _for_write_seq_lis_xmit                       ;183.5
                                ; LOE ebx
.B11.11:                        ; Preds .B11.10
        mov       eax, DWORD PTR [8+ebp]                        ;183.5
        lea       ecx, DWORD PTR [116+esp]                      ;183.5
        mov       edx, DWORD PTR [4+eax]                        ;183.5
        mov       DWORD PTR [116+esp], edx                      ;183.5
        push      ecx                                           ;183.5
        push      OFFSET FLAT: __STRLITPACK_17.0.11             ;183.5
        push      ebx                                           ;183.5
        call      _for_write_seq_lis_xmit                       ;183.5
                                ; LOE
.B11.12:                        ; Preds .B11.11
        add       esp, 140                                      ;185.1
        pop       ebx                                           ;185.1
        pop       edi                                           ;185.1
        pop       esi                                           ;185.1
        mov       esp, ebp                                      ;185.1
        pop       ebp                                           ;185.1
        ret                                                     ;185.1
        ALIGN     16
                                ; LOE
; mark_end;
_DEQUEUE_mp_PRINT_QUEUE_ARRAY ENDP
_TEXT	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_11.0.11	DB	2
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_12.0.11	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_13.0.11	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_14.0.11	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_15.0.11	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_16.0.11	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_17.0.11	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DEQUEUE_mp_PRINT_QUEUE_ARRAY
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_3	DB	32
	DB	65
	DB	116
	DB	116
	DB	101
	DB	109
	DB	112
	DB	116
	DB	32
	DB	116
	DB	111
	DB	32
	DB	68
	DB	105
	DB	115
	DB	113
	DB	117
	DB	101
	DB	117
	DB	101
	DB	32
	DB	119
	DB	104
	DB	101
	DB	110
	DB	32
	DB	113
	DB	117
	DB	101
	DB	117
	DB	101
	DB	32
	DB	105
	DB	115
	DB	32
	DB	101
	DB	109
	DB	112
	DB	116
	DB	121
	DB	46
	DB	32
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_8.0.7	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_9	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_5	DB	32
	DB	65
	DB	116
	DB	116
	DB	101
	DB	109
	DB	112
	DB	116
	DB	32
	DB	116
	DB	111
	DB	32
	DB	69
	DB	110
	DB	113
	DB	117
	DB	101
	DB	117
	DB	101
	DB	32
	DB	119
	DB	104
	DB	101
	DB	110
	DB	32
	DB	113
	DB	117
	DB	101
	DB	117
	DB	101
	DB	32
	DB	105
	DB	115
	DB	32
	DB	102
	DB	117
	DB	108
	DB	108
	DB	46
	DB	32
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_4	DB	32
	DB	65
	DB	116
	DB	116
	DB	101
	DB	109
	DB	112
	DB	116
	DB	32
	DB	116
	DB	111
	DB	32
	DB	69
	DB	110
	DB	113
	DB	117
	DB	101
	DB	117
	DB	101
	DB	32
	DB	119
	DB	104
	DB	101
	DB	110
	DB	32
	DB	113
	DB	117
	DB	101
	DB	117
	DB	101
	DB	32
	DB	105
	DB	115
	DB	32
	DB	102
	DB	117
	DB	108
	DB	108
	DB	46
	DB	32
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_2	DB	32
	DB	65
	DB	116
	DB	116
	DB	101
	DB	109
	DB	112
	DB	116
	DB	32
	DB	116
	DB	111
	DB	32
	DB	68
	DB	105
	DB	115
	DB	113
	DB	117
	DB	101
	DB	117
	DB	101
	DB	32
	DB	119
	DB	104
	DB	101
	DB	110
	DB	32
	DB	113
	DB	117
	DB	101
	DB	117
	DB	101
	DB	32
	DB	105
	DB	115
	DB	32
	DB	101
	DB	109
	DB	112
	DB	116
	DB	121
	DB	46
	DB	32
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_0	DB	70
	DB	114
	DB	111
	DB	110
	DB	116
	DB	32
	DB	66
	DB	97
	DB	99
	DB	107
	DB	32
	DB	80
	DB	111
	DB	105
	DB	110
	DB	101
	DB	114
	DB	32
	DB	97
	DB	110
	DB	100
	DB	32
	DB	115
	DB	105
	DB	122
	DB	101
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
	COMM _DEQUEUE_mp_M:BYTE:4
_DATA	ENDS
EXTRN	_for_write_seq_lis_xmit:PROC
EXTRN	_for_stop_core:PROC
EXTRN	_for_write_seq_lis:PROC
EXTRN	_for_dealloc_allocatable:PROC
EXTRN	_for_allocate:PROC
EXTRN	_for_check_mult_overflow:PROC
EXTRN	__fltused:BYTE
	INCLUDELIB <ifconsol>
	INCLUDELIB <libifcoremt>
	INCLUDELIB <libifport>
	INCLUDELIB <libmmt>
	INCLUDELIB <LIBCMT>
	INCLUDELIB <libirc>
	INCLUDELIB <svml_dispmt>
	INCLUDELIB <OLDNAMES>
	END
