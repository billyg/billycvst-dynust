MODULE DYNUST_TRANSIT_MODULE
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~ Copyright (C) 2002-2011 The Arizona Board of Regents on Behalf of The University of Arizona.  ~~!
!~~ Developed by Yi-Chang Chiu, Ph.D. as par to the DynusT project.                               ~~!
!~~ This program is free software: you can redistribute it and/or modify it                       ~~!
!~~ under the terms of the GNU General Public License version 2 as published by                   ~~!
!~~ the Free Software Foundation.                                                                 ~~!
!~~ This program is distributed in the hope that it will be useful,                               ~~!
!~~ but WITHOUT ANY WARRANTY; without even the implied warranty of                                ~~!
!~~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                                          ~~!
!~~ See the GNU General Public License for more details.                                          ~~!
!~~ You should have received a copy of the GNU General Public License                             ~~!
!~~ along with this program (in the file COPYING).                                                ~~!
!~~ IF not, see <http://www.gnu.org/licenses/>.                                                   ~~!
!~~ Please see http://www.dynust.net for contact information and names                            ~~!
!~~ of individual contributors.                                                                   ~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
USE DYNUST_NETWORK_MODULE

INTERFACE
  SUBROUTINE IMSLSort(Ain,ROWS,COLS)     
     INTEGER ROWS,COLS
     INTEGER, INTENT(inout), DIMENSION(ROWS,COLS) :: AIN
  END SUBROUTINE
END INTERFACE
INTEGER   :: NumRoutes
REAL      :: SimStartTime
LOGICAL   :: RouteError = .false.
INTEGER   :: VehGenID = 0
INTEGER   :: TempA(1000)
TYPE Route
  CHARACTER(LEN=6)      :: id
  INTEGER               :: rNum
  INTEGER               :: RouteType
  INTEGER               :: NumNode
  INTEGER,POINTER       :: Node(:)
  INTEGER(1),POINTER    :: StopType(:)
  INTEGER(2),POINTER    :: StopCapacity(:)
  INTEGER(1),POINTER    :: R2SMap(:)
  INTEGER               :: GenLink
  INTEGER               :: destZ
  INTEGER               :: origZ
END TYPE

TYPE Trip
  REAL,POINTER          :: DepartTime(:)
  REAL,POINTER          :: Boarding(:)
  REAL,POINTER          :: Aliting(:)
  INTEGER,POINTER       :: VehID
END TYPE

TYPE Schedule
  INTEGER               :: NumTrip
  INTEGER               :: NumStop
  TYPE(Trip),POINTER    :: Stinfo(:)
END TYPE

TYPE(Route), ALLOCATABLE:: m_route(:)
TYPE(Schedule), ALLOCATABLE:: m_schedule(:)
INTEGER TransitRepos(1000,5) !1: VehID, 2: departure time, rounded to integer, 3: Route ID, 4: schedule ID
INTEGER,SAVE:: TransitReposPntr

TYPE TransitRunTimeData
    INTEGER             :: ID
    INTEGER             :: CurrentLink
END TYPE

TYPE TransitGroup
    TYPE(TransitRunTimeData),DIMENSION(1000):: TransitRunTime
END TYPE


CONTAINS
!~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>
SUBROUTINE InputTransit

LOGICAL EXT2,destflag,ifoundbug,Errorflag
INTEGER :: I = 0
INTEGER :: h = 0

destflag = .false.
INQUIRE (FILE='TransitRouteSchedule.dat', EXIST = EXT2)

IF(EXT2) THEN ! file exist
CLOSE(1) ! close the file for CHECK_TRANSIT_RECORDS to read
CALL CHECK_TRANSIT_RECORDS(NumRoutes) ! check records and fine number of routes
ALLOCATE(m_route(NumRoutes)) 
ALLOCATE(m_schedule(NumRoutes))

! Create a repository to hold there vehicles ordered by departure tiem and referene to their route and scheduld id
TransitRepos(:,:) = 9999999 
TransitReposPntr = 0

! read the file again
OPEN(file='TransitRouteSchedule.dat',unit=1,status='unknown')
DO is = 1, 14  ! skip header lines
  READ(1,*)
ENDDO
READ(1,*) SimStartTime

!// read route and schedules/trips for each route
DO 100 I = 1, NumRoutes 
  IF(logout > 0) WRITE(711,'("Checking route number  ",i7)') i
  m_route(i)%rNum = I
  READ(1,*) m_route(i)%id, m_route(i)%RouteType, m_route(i)%NumNode
  ALLOCATE(m_route(i)%Node(m_route(i)%NumNode))
  ALLOCATE(m_route(i)%StopType(m_route(i)%NumNode))
  ALLOCATE(m_route(i)%StopCapacity(m_route(i)%NumNode))
  ALLOCATE(m_route(i)%R2SMap(m_route(i)%NumNode))
  READ(1,*) (m_route(i)%Node(j),        j=1,m_route(i)%NumNode)
  READ(1,*) (m_route(i)%StopType(j),    j=1,m_route(i)%NumNode)
  READ(1,*) (m_route(i)%StopCapacity(j),j=1,m_route(i)%NumNode)
             m_route(i)%R2SMap(:) = 0
  ErrorFlag = .false.
  DO ih =1, m_route(i)%NumNode
    IF(OutToInNodeNum(m_route(i)%node(ih)) > 0) THEN
      m_route(i)%node(ih) = OutToInNodeNum(m_route(i)%node(ih))
      IF(m_route(i)%StopType(ih)>0.AND.m_dynust_network_node(m_route(i)%node(ih))%dzone < 1) THEN ! THE BUS STOP NEEDS TO BE A DESTINATINO NODE
        destflag = .true.
        WRITE(911,*) "Error in TransitRouteSchedule.dat"
        WRITE(911,'("Route ID  ",A,I7,"th stop needs to be specified as a destination node")') m_route(i)%id,ih
        WRITE(911,'("Please either modify TransitRouteSchedule.dat or destination setting for node #",i7)') m_dynust_network_node_nde(m_route(i)%node(ih))%IntoOutNodeNum
      ENDIF
    ELSE
      WRITE(911,*) "Transit Route node does not exist in TransitRouteSchedule.dat"
      WRITE(911,*) "Route # ", m_route(i)%ID, "NODE", m_route(i)%node(ih)
      ErrorFlag = .true.    
    ENDIF
  ENDDO
  IF(m_route(i)%Node(1)<=0.OR.m_route(i)%Node(2)<=0) THEN
    WRITE(911,*) "Error in reading the route file"
    WRITE(911,*) "Please check Route # ", i
  ENDIF 
  IF(ErrorFlag) STOP
! FIND GENERATION LINK  
  m_route(i)%GenLink = GetFLinkFromNode((m_route(i)%Node(1)),(m_route(i)%Node(2)),4)

! FIND ORIGIN ZONE  
  IF(m_dynust_network_node((m_route(i)%Node(1)))%iConZone(1)>0) then
    m_route(i)%origZ = m_dynust_network_node((m_route(i)%node(1)))%iConZone(2)
  ELSEIF(m_dynust_network_node_nde((m_route(i)%node(m_route(i)%NumNode)))%izone>0) THEN
    m_route(i)%origZ = m_dynust_network_node_nde((m_route(i)%node(m_route(i)%NumNode)))%izone
  ELSE
    WRITE(911,*) "ERROR IN FINDING ORIGIN ZONE FOR TRANSIT"
    WRITE(911,*) "ROUTE NUMBER", m_route(i)%id
    STOP
  ENDIF
  
! FIND DESTINATION ZONE
!// check if the all route nodes exist
  IF(m_dynust_network_node((m_route(i)%node(m_route(i)%NumNode)))%dzone>0) THEN
    m_route(i)%DestZ = m_dynust_network_node((m_route(i)%node(m_route(i)%NumNode)))%dzone
  ELSEIF(m_dynust_network_node_nde((m_route(i)%node(m_route(i)%NumNode)))%izone>0) THEN
    m_route(i)%DestZ = m_dynust_network_node_nde((m_route(i)%node(m_route(i)%NumNode)))%izone  
  ELSE
    WRITE(911,*) "ERROR IN FINDING DESTINATION ZONE FOR TRANSIT"
    WRITE(911,*) "ROUTE NUMBER", m_route(i)%id
    STOP
  ENDIF
     
     
  DO iam = 1, m_route(i)%NumNode
   IF((m_route(i)%node(iam)) < 1) THEN  ! this node does not exist
     WRITE(911,'("Route invalid, node does not exist, check route ID ",A," node ",i7)') m_route(i)%id, m_route(i)%node(iam)
     RouteError = .true.
!     STOP stop at the end of reading
   ENDIF
  ENDDO
!// check if all link exist
  DO iam = 1, m_route(i)%NumNode-1
   IAC = GetFLinkFromNode((m_route(i)%node(iam)),(m_route(i)%node(iam+1)),4)
   IF(IAC < 1) THEN
     WRITE(911,*) "Route Erro at ID", m_route(i)%id
     WRITE(911,'("Link does not exist ", i7, " ==> ",i7)') m_dynust_network_node_nde(m_route(i)%node(iam))%IntoOutNodeNum, m_dynust_network_node_nde(m_route(i)%node(iam+1))%IntoOutNodeNum
      RouteError = .true.
!     STOP ! stop at the end of reading
   ENDIF
  ENDDO
!// check how many nodes are specified as stops in the stoptype record and store in IC counter
  IC = 0
  DO J = 1, m_route(i)%NumNode
    IF(m_route(i)%StopType(j) > 0) THEN
      ic = ic + 1
      m_route(i)%R2SMap(j) = ic
    ENDIF
  ENDDO
!// start reading schedule/trip info
  READ(1,*) m_schedule(i)%NumTrip, m_schedule(i)%NumStop
  IF(m_schedule(i)%NumStop /= IC) THEN ! the stops in the schecule should be the same as IC, or throw in an error
    WRITE(911,'("Error in Transit Route # ",i5)') i
    WRITE(911,'("ROUTE ID =  ", A)') m_route(I)%id
    WRITE(911,'("Route line counted # of stops =  ",i5)') IC
    WRITE(911,'("Schedule indicated # of stops =  ",i5)') m_schedule(i)%NumStop
    WRITE(911,'("Route nodes specified as a bus stop => ")')
    DO isee = 1, m_route(i)%NumNode
      IF(m_route(i)%StopType(isee) > 0) WRITE(911,*) m_dynust_network_node_nde(m_route(i)%node(isee))%IntoOutNodeNum
    ENDDO
    RouteError = .true.
  ENDIF
!//start reading schedule details
  ALLOCATE(m_schedule(i)%Stinfo(m_schedule(i)%NumTrip))
  DO isee = 1, m_schedule(i)%NumTrip
    ALLOCATE(m_schedule(i)%Stinfo(isee)%DepartTime(m_schedule(i)%NumStop))
    ALLOCATE(m_schedule(i)%Stinfo(isee)%Boarding(m_schedule(i)%NumStop))
    ALLOCATE(m_schedule(i)%Stinfo(isee)%Aliting(m_schedule(i)%NumStop))  
  ENDDO
  ifoundbug = .false.
  DO m = 1, m_schedule(i)%NumTrip
    VehGenID = VehGenID + 1
    READ(1,*) (m_schedule(i)%Stinfo(m)%DepartTime(n),n = 1, m_schedule(i)%NumStop)  
    DO h = 1, m_schedule(i)%NumStop ! converting DepartTime to min with reference to start of simulation
      m_schedule(i)%Stinfo(m)%DepartTime(h) = ifix(m_schedule(i)%Stinfo(m)%DepartTime(h))*60+(m_schedule(i)%Stinfo(m)%DepartTime(h)-ifix(m_schedule(i)%Stinfo(m)%DepartTime(h)))*100
      m_schedule(i)%Stinfo(m)%DepartTime(h) = m_schedule(i)%Stinfo(m)%DepartTime(h) - (ifix(SimStartTime)*60+(SimStartTime-ifix(SimStartTime))*100)
      IF(h > 1) THEN
        IF(m_schedule(i)%Stinfo(m)%DepartTime(h) < m_schedule(i)%Stinfo(m)%DepartTime(h-1)) THEN
          WRITE(911,*) "Error in Transit Schedule"
          WRITE(911,*) "ROUTE ID ", m_route(i)%ID
          WRITE(911,*) "SCHEDULE # ", m
          WRITE(911,*) "ARRIVAL TIME ",h,m_schedule(i)%Stinfo(m)%DepartTime(h)
          ifoundbug = .true.
          STOP
        ENDIF
      ENDIF
    ENDDO
    TransitRepos(VehGenID,1) = m_schedule(i)%Stinfo(m)%DepartTime(1)*AttScale  ! departure time
    TransitRepos(VehGenID,2) = m_route(i)%GenLink                              ! generation link id
    TransitRepos(VehGenID,3) = m_route(i)%rNum                                 ! global route number
    TransitRepos(VehGenID,4) = m                                               ! schedule id
    TransitRepos(VehGenID,5) = VehGenID                                        ! to be replaced by actual id at veh generation
    READ(1,*) (m_schedule(i)%Stinfo(m)%Boarding(n),  n = 1, m_schedule(i)%NumStop)  
    READ(1,*) (m_schedule(i)%Stinfo(m)%Aliting(n),   n = 1, m_schedule(i)%NumStop)  
    READ(1,*)        
!    call write_TransitVehicles(VehGenID,VehGenID,VehGenID,227,1,228)
!    TempA(1:m_route(i)%NumNode) = m_route(i)%Node(1:m_route(i)%NumNode)
!    call write_TransitVehicles(VehGenID,VehGenID,VehGenID,227,   1,228, m_route(i)%node(1), m_route(i)%node(2), m_route(i)%node(NumNode), m_schedule(i)%Stinfo(m)%DepartTime(1), m_route(i)%NumNode, TempA)    
  ENDDO
  
!  READ(1,*) ! READ THE SEPARATOR LINE  
100 CONTINUE

IF(RouteError) STOP
IF(destflag) STOP
IF(IFOUNDBUG) STOP

END IF ! IF TransitRoute.dat exists

CALL IMSLsort(TransitRepos,1000,5) ! SORT BY DEPARTURE TIME ROUTE ID, SCHEDULE ID, AND INTERNAL VEHICLE ID
TransitReposPntr = TransitReposPntr + 1
DO WHILE (TransitRepos(TransitReposPntr,1)<0) 
  TransitReposPntr = TransitReposPntr + 1
ENDDO
END SUBROUTINE 

!~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>
LOGICAL FUNCTION Query_Transit_Vehicles(linter) ! LINTER IS L
LOGICAL TransitExist
INTEGER linter,checktime
REAL timelinter
checktime = (nint(TransitRepos(TransitReposPntr,1)/Attscale/xminPerSimInt))
IF(checktime <=(linter-1)) THEN ! NEXT VEHICLE IS TO BE GENERATED AT THE LOOP ID LINTER
  Query_Transit_vehicles = .True.
ELSE
  Query_Transit_vehicles = .False.
ENDIF

END FUNCTION

!~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>
SUBROUTINE GET_TRANSIT_VEH(linter,genlk,TransitExist,morig,mdest,iRouteID)
INTEGER linter,genlk,checktime,morig,mdest,iRouteID
LOGICAL TransitExist
checktime = (nint(TransitRepos(TransitReposPntr,1)/Attscale/xminPerSimInt))
IF(checktime <= (linter-1)) THEN 
  iRouteID = TransitRepos(TransitReposPntr,3)
  genlk = TransitRepos(TransitReposPntr,2)
  morig = m_route(iRouteID)%origZ
  mdest = m_route(iRouteID)%destZ
  TransitExist = .true.
  TransitReposPntr = TransitReposPntr + 1
ELSE
  TransitExist = .false.
ENDIF

END SUBROUTINE


!~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>
SUBROUTINE Retrieve_Transit_Path(vehid,iR)
      USE DYNUST_VEH_MODULE
      USE DYNUST_VEH_PATH_ATT_MODULE
      INTEGER iR,vehid
      REAL evalue 
      INTEGER(2) IS

      IF(vehatt_Array(vehid)%PSize < m_route(iR)%NumNode-1) THEN
       isizetmp = max(vehatt_Array(vehid)%PSize+v_IncreaseSize,m_route(iR)%NumNode) ! +1 IS TO INCLUDE THE CENTROID
       CALL vehatt_P_Setup(vehid,isizetmp)
       CALL vehatt_A_Setup(vehid,isizetmp) ! INITIAL SET
       vehatt_Array(vehid)%PSize = isizetmp ! UPDATE PSize
      ENDIF 

      DO is = 1, m_route(iR)%NumNode-1 ! START FROM THE 2ND NODE
        evalue = float(m_route(iR)%node(is+1))
	    CALL vehatt_Insert(vehid,is,1,evalue)
	  ENDDO
	  evalue = destination(MasterDest(m_dynust_last_stand(vehid)%jdest))
	  CALL vehatt_Insert(vehid,is,1,evalue)
	  
	  CALL vehatt_Active_Size(vehid,m_route(iR)%NumNode)
END SUBROUTINE


!~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>
SUBROUTINE GENERATE_TRANSIT_VEH_ATTRIBUTES(linter,j,jdst,iz,genlk)
    USE DYNUST_MAIN_MODULE
    USE DYNUST_VEH_MODULE
    USE DYNUST_NETWORK_MODULE
    INTEGER linter,genlk

    m_dynust_last_stand(j)%jorig = iz
    m_dynust_last_stand(j)%vehtype = 6 ! TRANSIT VEHICLE TYPE
    m_dynust_veh(j)%vhpce=nint(1.5*AttScale) ! ACCORDING TO HCM 1998, it should be 1.5 as default
    m_dynust_veh_nde(j)%info = 0
    m_dynust_last_stand(j)%vehclass = 1 ! HISTORICAL PATH
    m_dynust_last_stand(j)%jdest = MasterDest(jdst)
    IF((linter-1)*xminPerSimInt >= starttm.and.(linter-1)*xminPerSimInt < endtm) THEN
      m_dynust_veh(j)%itag = 1
    ELSE
      m_dynust_veh(j)%itag = 0
      numNtag = numNtag + 1	 	    
    ENDIF
    m_dynust_last_stand(j)%stime = (linter-1)*xminPerSimInt
    m_dynust_veh_nde(j)%ribf= 0
    m_dynust_veh_nde(j)%compliance = 0
	m_dynust_last_stand(j)%isec = genlk            ! SET GENERATION LINK
	m_dynust_veh_nde(j)%xparinit = m_dynust_network_arc_nde(genlk)%s  ! SET INITIAL LOADING POSITION TO THE TAIL OF THE LINK AS BUT IS LOADED FROM THE UPSTREAM NODE
    m_dynust_veh(j)%DestVisit = 1 
    m_dynust_veh(j)%NoOfIntDst = 1
    m_dynust_veh_nde(j)%IntDestZone(m_dynust_veh(j)%NoOfIntDst)=m_dynust_last_stand(j)%jdest ! JDEST CARRIES ORIGINAL ZONE NUMBER
	
END SUBROUTINE

!~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>
SUBROUTINE CHECK_TRANSIT_RECORDS(numRoutes)
INTEGER I, J, numRoutes

OPEN(file='TransitRouteSchedule.dat',unit=1,status='unknown')
IF(error /= 0) THEN
   WRITE(911,*) 'Error when opening TransitRoute.dat'
   STOP
ENDIF
I = 1
J = 0

DO is = 1, 14  ! SKIP HEADER LINES
  READ(1,*)
ENDDO
READ(1,*) SimStartTime

DO WHILE(.NOT.EOF(1))
  j = j + 1
  READ(1,*) 
  READ(1,*) 
  READ(1,*) 
  READ(1,*) 
!// START READING THE FIRST LINE
  READ(1,*) NumTrip, NumStop

! START READING SDCHEDULE DETAILS
  DO m = 1, NumTrip
    READ(1,*) !(m_schedule(i)%Stinfo(m)%DepartTime(n),n = 1, m_schedule(i)%NumStop)  
    READ(1,*) !(m_schedule(i)%Stinfo(m)%Boarding(n),  n = 1, m_schedule(i)%NumStop)  
    READ(1,*) !(m_schedule(i)%Stinfo(m)%Aliting(n),   n = 1, m_schedule(i)%NumStop)  
    READ(1,*)        
  ENDDO
END DO
numRoutes = J
CLOSE(1)
END SUBROUTINE
END MODULE
