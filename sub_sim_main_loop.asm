; mark_description "Intel(R) Visual Fortran Compiler XE for applications running on IA-32, Version 13.1.1.171 Build 20130313";
; mark_description "-S";
	.686P
 	.387
	OPTION DOTNAME
	ASSUME	CS:FLAT,DS:FLAT,SS:FLAT
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
TXTST0:
; -- Begin  _SIM_MAIN_LOOP
; mark_begin;
IF @Version GE 800
  .MMX
ELSEIF @Version GE 612
  .MMX
  MMWORD TEXTEQU <QWORD>
ENDIF
IF @Version GE 800
  .XMM
ELSEIF @Version GE 614
  .XMM
  XMMWORD TEXTEQU <OWORD>
ENDIF
       ALIGN     16
	PUBLIC _SIM_MAIN_LOOP
_SIM_MAIN_LOOP	PROC NEAR 
; parameter 1: 8 + ebx
.B1.1:                          ; Preds .B1.0
        push      ebx                                           ;1.12
        mov       ebx, esp                                      ;1.12
        and       esp, -16                                      ;1.12
        push      ebp                                           ;1.12
        push      ebp                                           ;1.12
        mov       ebp, DWORD PTR [4+ebx]                        ;1.12
        mov       DWORD PTR [4+esp], ebp                        ;1.12
        mov       ebp, esp                                      ;1.12
        sub       esp, 1048                                     ;1.12
        mov       eax, 1                                        ;47.25
        mov       DWORD PTR [-808+ebp], eax                     ;47.25
        mov       ecx, 128                                      ;47.39
        mov       DWORD PTR [-768+ebp], eax                     ;46.25
        mov       edx, 2                                        ;47.39
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DEC_NUM] ;1.12
        test      eax, eax                                      ;1.12
        mov       DWORD PTR [-396+ebp], esi                     ;1.12
        mov       esi, 0                                        ;47.39
        cmovle    eax, esi                                      ;1.12
        shl       eax, 2                                        ;1.12
        mov       DWORD PTR [-392+ebp], edi                     ;1.12
        mov       DWORD PTR [-920+ebp], esi                     ;47.39
        mov       DWORD PTR [-916+ebp], esi                     ;47.39
        mov       DWORD PTR [-912+ebp], esi                     ;47.39
        mov       DWORD PTR [-908+ebp], ecx                     ;47.39
        mov       DWORD PTR [-904+ebp], edx                     ;47.39
        mov       DWORD PTR [-900+ebp], esi                     ;47.39
        mov       DWORD PTR [-896+ebp], esi                     ;47.39
        mov       DWORD PTR [-892+ebp], esi                     ;47.39
        mov       DWORD PTR [-888+ebp], esi                     ;47.39
        mov       DWORD PTR [-884+ebp], esi                     ;47.39
        mov       DWORD PTR [-880+ebp], esi                     ;47.39
        mov       DWORD PTR [-876+ebp], esi                     ;47.39
        mov       DWORD PTR [-824+ebp], esi                     ;47.25
        mov       DWORD PTR [-820+ebp], esi                     ;47.25
        mov       DWORD PTR [-816+ebp], esi                     ;47.25
        mov       DWORD PTR [-812+ebp], ecx                     ;47.25
        mov       DWORD PTR [-804+ebp], esi                     ;47.25
        mov       DWORD PTR [-800+ebp], esi                     ;47.25
        mov       DWORD PTR [-796+ebp], esi                     ;47.25
        mov       DWORD PTR [-792+ebp], esi                     ;47.25
        mov       DWORD PTR [-872+ebp], esi                     ;46.37
        mov       DWORD PTR [-868+ebp], esi                     ;46.37
        mov       DWORD PTR [-864+ebp], esi                     ;46.37
        mov       DWORD PTR [-860+ebp], ecx                     ;46.37
        mov       DWORD PTR [-856+ebp], edx                     ;46.37
        mov       DWORD PTR [-852+ebp], esi                     ;46.37
        mov       DWORD PTR [-848+ebp], esi                     ;46.37
        mov       DWORD PTR [-844+ebp], esi                     ;46.37
        mov       DWORD PTR [-840+ebp], esi                     ;46.37
        mov       DWORD PTR [-836+ebp], esi                     ;46.37
        mov       DWORD PTR [-832+ebp], esi                     ;46.37
        mov       DWORD PTR [-828+ebp], esi                     ;46.37
        mov       DWORD PTR [-784+ebp], esi                     ;46.25
        mov       DWORD PTR [-780+ebp], esi                     ;46.25
        mov       DWORD PTR [-776+ebp], esi                     ;46.25
        mov       DWORD PTR [-772+ebp], ecx                     ;46.25
        mov       DWORD PTR [-764+ebp], esi                     ;46.25
        mov       DWORD PTR [-760+ebp], esi                     ;46.25
        mov       DWORD PTR [-756+ebp], esi                     ;46.25
        mov       DWORD PTR [-752+ebp], esi                     ;46.25
        mov       DWORD PTR [-400+ebp], esp                     ;1.12
        call      __alloca_probe                                ;1.12
        and       esp, -16                                      ;1.12
        mov       eax, esp                                      ;1.12
                                ; LOE eax
.B1.383:                        ; Preds .B1.1
        mov       DWORD PTR [-132+ebp], eax                     ;1.12
        xor       eax, eax                                      ;53.7
        push      eax                                           ;55.7
        push      eax                                           ;55.7
        push      22                                            ;55.7
        push      OFFSET FLAT: __STRLITPACK_46                  ;55.7
        push      eax                                           ;55.7
        push      80                                            ;55.7
        push      OFFSET FLAT: _SIM_MAIN_LOOP$PRINTSTR1.0.1     ;55.7
        mov       DWORD PTR [-404+ebp], eax                     ;53.7
        call      _for_cpystr                                   ;55.7
                                ; LOE
.B1.2:                          ; Preds .B1.383
        add       esp, -8                                       ;56.12
        mov       edx, OFFSET FLAT: __NLITPACK_0.0.1            ;56.12
        mov       eax, OFFSET FLAT: _SIM_MAIN_LOOP$PRINTSTR1.0.1 ;56.12
        mov       ecx, OFFSET FLAT: __NLITPACK_1.0.1            ;56.12
        mov       esi, OFFSET FLAT: __NLITPACK_2.0.1            ;56.12
        mov       DWORD PTR [12+esp], edx                       ;56.12
        mov       DWORD PTR [16+esp], edx                       ;56.12
        mov       DWORD PTR [20+esp], esi                       ;56.12
        mov       DWORD PTR [24+esp], esi                       ;56.12
        mov       DWORD PTR [28+esp], OFFSET FLAT: __NLITPACK_3.0.1 ;56.12
        mov       DWORD PTR [32+esp], 80                        ;56.12
        call      _PRINTSCREEN.                                 ;56.12
                                ; LOE
.B1.384:                        ; Preds .B1.2
        add       esp, 36                                       ;56.12
                                ; LOE
.B1.3:                          ; Preds .B1.384
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_STARTTIME] ;59.10
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ENDTIME] ;59.10
        cmp       edx, esi                                      ;59.10
        mov       DWORD PTR [-188+ebp], esi                     ;59.10
        jl        .B1.458       ; Prob 10%                      ;59.10
                                ; LOE edx esi
.B1.4:                          ; Preds .B1.3
        mov       eax, DWORD PTR [_SIM_MAIN_LOOP$ICOUNT_STOP.0.1] ;315.33
        xor       ecx, ecx                                      ;
        movss     xmm4, DWORD PTR [_2il0floatpacket.20]         ;256.59
        movss     xmm3, DWORD PTR [_2il0floatpacket.21]         ;72.93
        movaps    xmm2, XMMWORD PTR [_2il0floatpacket.22]       ;153.61
        mov       DWORD PTR [-408+ebp], ecx                     ;115.46
        mov       DWORD PTR [-244+ebp], eax                     ;115.46
        mov       DWORD PTR [-204+ebp], edx                     ;115.46
                                ; LOE esi
.B1.5:                          ; Preds .B1.245 .B1.4
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_LOGOUT] ;62.7
        test      edi, edi                                      ;62.17
        jle       .B1.10        ; Prob 16%                      ;62.17
                                ; LOE esi edi
.B1.6:                          ; Preds .B1.5
        push      32                                            ;62.22
        mov       DWORD PTR [-440+ebp], 0                       ;62.22
        lea       eax, DWORD PTR [-360+ebp]                     ;62.22
        push      eax                                           ;62.22
        push      OFFSET FLAT: __STRLITPACK_47.0.1              ;62.22
        push      -2088435968                                   ;62.22
        push      711                                           ;62.22
        mov       DWORD PTR [-360+ebp], 5                       ;62.22
        lea       edx, DWORD PTR [-440+ebp]                     ;62.22
        push      edx                                           ;62.22
        mov       DWORD PTR [-356+ebp], OFFSET FLAT: __STRLITPACK_44 ;62.22
        call      _for_write_seq_lis                            ;62.22
                                ; LOE esi edi
.B1.385:                        ; Preds .B1.6
        add       esp, 24                                       ;62.22
                                ; LOE esi edi
.B1.7:                          ; Preds .B1.385
        mov       DWORD PTR [-200+ebp], esi                     ;62.22
        lea       eax, DWORD PTR [-200+ebp]                     ;62.22
        push      eax                                           ;62.22
        push      OFFSET FLAT: __STRLITPACK_48.0.1              ;62.22
        lea       edx, DWORD PTR [-440+ebp]                     ;62.22
        push      edx                                           ;62.22
        call      _for_write_seq_lis_xmit                       ;62.22
                                ; LOE esi edi
.B1.386:                        ; Preds .B1.7
        add       esp, 12                                       ;62.22
                                ; LOE esi edi
.B1.8:                          ; Preds .B1.386
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_STARTTIME] ;62.22
        lea       edx, DWORD PTR [-192+ebp]                     ;62.22
        push      edx                                           ;62.22
        push      OFFSET FLAT: __STRLITPACK_49.0.1              ;62.22
        mov       DWORD PTR [-192+ebp], eax                     ;62.22
        lea       ecx, DWORD PTR [-440+ebp]                     ;62.22
        push      ecx                                           ;62.22
        call      _for_write_seq_lis_xmit                       ;62.22
                                ; LOE esi edi
.B1.387:                        ; Preds .B1.8
        add       esp, 12                                       ;62.22
                                ; LOE esi edi
.B1.9:                          ; Preds .B1.387
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ENDTIME] ;62.22
        lea       edx, DWORD PTR [-176+ebp]                     ;62.22
        push      edx                                           ;62.22
        push      OFFSET FLAT: __STRLITPACK_50.0.1              ;62.22
        mov       DWORD PTR [-176+ebp], eax                     ;62.22
        lea       ecx, DWORD PTR [-440+ebp]                     ;62.22
        push      ecx                                           ;62.22
        call      _for_write_seq_lis_xmit                       ;62.22
                                ; LOE esi edi
.B1.388:                        ; Preds .B1.9
        add       esp, 12                                       ;62.22
                                ; LOE esi edi
.B1.10:                         ; Preds .B1.388 .B1.5
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIMPERAGG] ;64.40
        lea       eax, DWORD PTR [-1+esi]                       ;64.29
        cvtsi2ss  xmm0, eax                                     ;64.29
        cvtsi2ss  xmm1, edx                                     ;64.40
        movss     DWORD PTR [-160+ebp], xmm0                    ;64.29
        xor       ecx, ecx                                      ;64.8
        divss     xmm0, xmm1                                    ;64.24
        mov       DWORD PTR [-152+ebp], eax                     ;64.29
        cvttss2si eax, xmm0                                     ;64.24
        test      eax, eax                                      ;64.8
        mov       DWORD PTR [-72+ebp], edx                      ;64.40
        cmovl     eax, ecx                                      ;64.8
        test      edi, edi                                      ;66.17
        movss     DWORD PTR [-48+ebp], xmm1                     ;64.40
        mov       DWORD PTR [-156+ebp], eax                     ;64.8
        lea       edx, DWORD PTR [1+eax]                        ;64.8
        mov       DWORD PTR [-124+ebp], edx                     ;64.8
        jle       .B1.377       ; Prob 16%                      ;66.17
                                ; LOE esi edi
.B1.11:                         ; Preds .B1.10
        push      32                                            ;66.22
        mov       DWORD PTR [-440+ebp], 0                       ;66.22
        lea       eax, DWORD PTR [-352+ebp]                     ;66.22
        push      eax                                           ;66.22
        push      OFFSET FLAT: __STRLITPACK_51.0.1              ;66.22
        push      -2088435968                                   ;66.22
        push      711                                           ;66.22
        mov       DWORD PTR [-352+ebp], 7                       ;66.22
        lea       edx, DWORD PTR [-440+ebp]                     ;66.22
        push      edx                                           ;66.22
        mov       DWORD PTR [-348+ebp], OFFSET FLAT: __STRLITPACK_42 ;66.22
        call      _for_write_seq_lis                            ;66.22
                                ; LOE esi edi
.B1.389:                        ; Preds .B1.11
        add       esp, 24                                       ;66.22
                                ; LOE esi edi
.B1.12:                         ; Preds .B1.389
        mov       eax, DWORD PTR [-124+ebp]                     ;66.22
        lea       edx, DWORD PTR [-184+ebp]                     ;66.22
        push      edx                                           ;66.22
        push      OFFSET FLAT: __STRLITPACK_52.0.1              ;66.22
        mov       DWORD PTR [-184+ebp], eax                     ;66.22
        lea       ecx, DWORD PTR [-440+ebp]                     ;66.22
        push      ecx                                           ;66.22
        call      _for_write_seq_lis_xmit                       ;66.22
                                ; LOE esi edi
.B1.390:                        ; Preds .B1.12
        add       esp, 12                                       ;66.22
                                ; LOE esi edi
.B1.13:                         ; Preds .B1.390
        mov       eax, DWORD PTR [-152+ebp]                     ;68.10
        cdq                                                     ;68.10
        mov       ecx, DWORD PTR [-72+ebp]                      ;68.10
        idiv      ecx                                           ;68.10
        test      edx, edx                                      ;68.29
        jne       .B1.72        ; Prob 50%                      ;68.29
                                ; LOE esi edi
.B1.14:                         ; Preds .B1.13 .B1.377
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFARCS] ;69.9
        test      eax, eax                                      ;69.9
        jle       .B1.71        ; Prob 2%                       ;69.9
                                ; LOE eax esi edi
.B1.15:                         ; Preds .B1.14
        imul      ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32], -152 ;
        imul      edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32], -900 ;
        add       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;
        mov       DWORD PTR [-16+ebp], ecx                      ;
        mov       ecx, DWORD PTR [-156+ebp]                     ;64.8
        add       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;
        mov       DWORD PTR [-36+ebp], edx                      ;
        mov       DWORD PTR [-80+ebp], 1                        ;
        lea       edx, DWORD PTR [4+ecx*4]                      ;64.8
        mov       DWORD PTR [-84+ebp], edx                      ;64.8
        lea       edx, DWORD PTR [5+ecx+ecx*4]                  ;64.8
        mov       DWORD PTR [-20+ebp], edx                      ;64.8
        mov       DWORD PTR [-76+ebp], eax                      ;64.8
        mov       DWORD PTR [-116+ebp], edi                     ;64.8
        mov       DWORD PTR [-68+ebp], esi                      ;64.8
                                ; LOE
.B1.16:                         ; Preds .B1.69 .B1.15
        cmp       DWORD PTR [-72+ebp], 0                        ;70.16
        jle       .B1.33        ; Prob 50%                      ;70.16
                                ; LOE
.B1.17:                         ; Preds .B1.16
        imul      eax, DWORD PTR [-80+ebp], 900                 ;
        cmp       DWORD PTR [-72+ebp], 4                        ;70.16
        jl        .B1.281       ; Prob 10%                      ;70.16
                                ; LOE eax
.B1.18:                         ; Preds .B1.17
        mov       edx, DWORD PTR [-36+ebp]                      ;70.16
        mov       ecx, DWORD PTR [296+edx+eax]                  ;70.16
        shl       ecx, 2                                        ;70.16
        mov       edx, DWORD PTR [264+edx+eax]                  ;70.16
        sub       edx, ecx                                      ;70.16
        add       edx, 4                                        ;70.16
        and       edx, 15                                       ;70.16
        je        .B1.21        ; Prob 50%                      ;70.16
                                ; LOE eax edx
.B1.19:                         ; Preds .B1.18
        test      dl, 3                                         ;70.16
        jne       .B1.281       ; Prob 10%                      ;70.16
                                ; LOE eax edx
.B1.20:                         ; Preds .B1.19
        neg       edx                                           ;70.16
        add       edx, 16                                       ;70.16
        shr       edx, 2                                        ;70.16
                                ; LOE eax edx
.B1.21:                         ; Preds .B1.20 .B1.18
        lea       ecx, DWORD PTR [4+edx]                        ;70.16
        cmp       ecx, DWORD PTR [-72+ebp]                      ;70.16
        jg        .B1.281       ; Prob 10%                      ;70.16
                                ; LOE eax edx
.B1.22:                         ; Preds .B1.21
        mov       ecx, DWORD PTR [-72+ebp]                      ;70.16
        mov       esi, ecx                                      ;70.16
        sub       esi, edx                                      ;70.16
        mov       edi, DWORD PTR [-36+ebp]                      ;70.16
        and       esi, 3                                        ;70.16
        neg       esi                                           ;70.16
        add       esi, ecx                                      ;70.16
        mov       ecx, DWORD PTR [296+eax+edi]                  ;70.16
        shl       ecx, 2                                        ;
        neg       ecx                                           ;
        add       ecx, DWORD PTR [264+eax+edi]                  ;
        mov       DWORD PTR [-60+ebp], 0                        ;
        test      edx, edx                                      ;70.16
        mov       DWORD PTR [-64+ebp], ecx                      ;
        jbe       .B1.26        ; Prob 10%                      ;70.16
                                ; LOE eax edx esi
.B1.23:                         ; Preds .B1.22
        xor       ecx, ecx                                      ;
        mov       DWORD PTR [-88+ebp], esi                      ;
        mov       esi, DWORD PTR [-64+ebp]                      ;
        mov       edi, DWORD PTR [-60+ebp]                      ;
                                ; LOE eax edx ecx esi edi
.B1.24:                         ; Preds .B1.24 .B1.23
        add       edi, DWORD PTR [4+esi+ecx*4]                  ;70.16
        inc       ecx                                           ;70.16
        cmp       ecx, edx                                      ;70.16
        jb        .B1.24        ; Prob 82%                      ;70.16
                                ; LOE eax edx ecx esi edi
.B1.25:                         ; Preds .B1.24
        mov       DWORD PTR [-60+ebp], edi                      ;
        mov       esi, DWORD PTR [-88+ebp]                      ;
                                ; LOE eax edx esi
.B1.26:                         ; Preds .B1.22 .B1.25
        movd      xmm0, DWORD PTR [-60+ebp]                     ;70.16
        mov       ecx, DWORD PTR [-64+ebp]                      ;70.16
                                ; LOE eax edx ecx esi xmm0
.B1.27:                         ; Preds .B1.27 .B1.26
        paddd     xmm0, XMMWORD PTR [4+ecx+edx*4]               ;70.16
        add       edx, 4                                        ;70.16
        cmp       edx, esi                                      ;70.16
        jb        .B1.27        ; Prob 82%                      ;70.16
                                ; LOE eax edx ecx esi xmm0
.B1.28:                         ; Preds .B1.27
        movdqa    xmm1, xmm0                                    ;70.16
        psrldq    xmm1, 8                                       ;70.16
        paddd     xmm0, xmm1                                    ;70.16
        movdqa    xmm2, xmm0                                    ;70.16
        psrldq    xmm2, 4                                       ;70.16
        paddd     xmm0, xmm2                                    ;70.16
        movd      edx, xmm0                                     ;70.16
                                ; LOE eax edx esi
.B1.29:                         ; Preds .B1.28 .B1.281
        cmp       esi, DWORD PTR [-72+ebp]                      ;70.16
        jae       .B1.34        ; Prob 10%                      ;70.16
                                ; LOE eax edx esi
.B1.30:                         ; Preds .B1.29
        mov       edi, DWORD PTR [-36+ebp]                      ;70.16
        mov       ecx, DWORD PTR [296+eax+edi]                  ;70.16
        shl       ecx, 2                                        ;
        neg       ecx                                           ;
        add       ecx, DWORD PTR [264+eax+edi]                  ;
        mov       edi, DWORD PTR [-72+ebp]                      ;
                                ; LOE eax edx ecx esi edi
.B1.31:                         ; Preds .B1.31 .B1.30
        add       edx, DWORD PTR [4+ecx+esi*4]                  ;70.16
        inc       esi                                           ;70.16
        cmp       esi, edi                                      ;70.16
        jb        .B1.31        ; Prob 82%                      ;70.16
        jmp       .B1.34        ; Prob 100%                     ;70.16
                                ; LOE eax edx ecx esi edi
.B1.33:                         ; Preds .B1.16
        imul      eax, DWORD PTR [-80+ebp], 900                 ;
        xor       edx, edx                                      ;
                                ; LOE eax edx
.B1.34:                         ; Preds .B1.31 .B1.29 .B1.33
        mov       ecx, DWORD PTR [-36+ebp]                      ;72.12
        mov       esi, DWORD PTR [228+ecx+eax]                  ;72.12
        mov       ecx, DWORD PTR [260+ecx+eax]                  ;72.12
        shl       ecx, 2                                        ;
        add       esi, DWORD PTR [-84+ebp]                      ;
        neg       ecx                                           ;
        cmp       DWORD PTR [-68+ebp], 1                        ;71.15
        je        .B1.286       ; Prob 16%                      ;71.15
                                ; LOE eax edx ecx esi
.B1.35:                         ; Preds .B1.34
        cvtsi2ss  xmm1, edx                                     ;70.16
        movss     xmm0, DWORD PTR [_2il0floatpacket.21]         ;70.10
        divss     xmm1, xmm0                                    ;70.10
        divss     xmm1, DWORD PTR [-48+ebp]                     ;74.67
        mulss     xmm1, xmm0                                    ;74.77
        cvttss2si eax, xmm1                                     ;74.13
        mov       DWORD PTR [ecx+esi], eax                      ;74.13
                                ; LOE
.B1.36:                         ; Preds .B1.286 .B1.35
        imul      edx, DWORD PTR [-80+ebp], 152                 ;79.53
        mov       eax, DWORD PTR [-16+ebp]                      ;79.53
        mov       DWORD PTR [-24+ebp], edx                      ;79.53
        cmp       BYTE PTR [32+eax+edx], 0                      ;79.53
        jle       .B1.69        ; Prob 16%                      ;79.53
                                ; LOE eax edx al dl ah dh
.B1.37:                         ; Preds .B1.36
        cmp       DWORD PTR [-116+ebp], 0                       ;80.20
        jle       .B1.285       ; Prob 16%                      ;80.20
                                ; LOE eax edx al dl ah dh
.B1.38:                         ; Preds .B1.37
        push      32                                            ;80.25
        mov       DWORD PTR [-440+ebp], 0                       ;80.25
        lea       eax, DWORD PTR [-168+ebp]                     ;80.25
        push      eax                                           ;80.25
        push      OFFSET FLAT: __STRLITPACK_53.0.1              ;80.25
        push      -2088435968                                   ;80.25
        push      711                                           ;80.25
        mov       DWORD PTR [-168+ebp], 106                     ;80.25
        lea       edx, DWORD PTR [-440+ebp]                     ;80.25
        push      edx                                           ;80.25
        mov       DWORD PTR [-164+ebp], OFFSET FLAT: __STRLITPACK_40 ;80.25
        call      _for_write_seq_lis                            ;80.25
                                ; LOE
.B1.391:                        ; Preds .B1.38
        add       esp, 24                                       ;80.25
                                ; LOE
.B1.39:                         ; Preds .B1.391
        mov       eax, DWORD PTR [-80+ebp]                      ;80.25
        lea       edx, DWORD PTR [-120+ebp]                     ;80.25
        push      edx                                           ;80.25
        push      OFFSET FLAT: __STRLITPACK_54.0.1              ;80.25
        mov       DWORD PTR [-120+ebp], eax                     ;80.25
        lea       ecx, DWORD PTR [-440+ebp]                     ;80.25
        push      ecx                                           ;80.25
        call      _for_write_seq_lis_xmit                       ;80.25
                                ; LOE
.B1.392:                        ; Preds .B1.39
        add       esp, 12                                       ;80.25
                                ; LOE
.B1.40:                         ; Preds .B1.392
        mov       eax, DWORD PTR [-124+ebp]                     ;80.25
        lea       edx, DWORD PTR [-112+ebp]                     ;80.25
        push      edx                                           ;80.25
        push      OFFSET FLAT: __STRLITPACK_55.0.1              ;80.25
        mov       DWORD PTR [-112+ebp], eax                     ;80.25
        lea       ecx, DWORD PTR [-440+ebp]                     ;80.25
        push      ecx                                           ;80.25
        call      _for_write_seq_lis_xmit                       ;80.25
                                ; LOE
.B1.393:                        ; Preds .B1.40
        add       esp, 12                                       ;80.25
                                ; LOE
.B1.41:                         ; Preds .B1.393
        mov       eax, DWORD PTR [-16+ebp]                      ;80.159
        mov       edx, DWORD PTR [-24+ebp]                      ;80.159
        mov       edi, DWORD PTR [-36+ebp]                      ;80.159
        mov       esi, DWORD PTR [-20+ebp]                      ;80.25
        imul      eax, DWORD PTR [12+eax+edx], 900              ;80.159
        mov       ecx, DWORD PTR [156+edi+eax]                  ;80.159
        imul      edi, DWORD PTR [188+edi+eax], -40             ;80.25
        lea       eax, DWORD PTR [-440+ebp]                     ;80.25
        lea       edx, DWORD PTR [ecx+esi*8]                    ;80.25
        movsx     ecx, WORD PTR [36+edi+edx]                    ;80.25
        lea       esi, DWORD PTR [-104+ebp]                     ;80.25
        push      esi                                           ;80.25
        push      OFFSET FLAT: __STRLITPACK_56.0.1              ;80.25
        push      eax                                           ;80.25
        mov       DWORD PTR [-28+ebp], ecx                      ;80.25
        mov       WORD PTR [-104+ebp], cx                       ;80.25
        call      _for_write_seq_lis_xmit                       ;80.25
                                ; LOE
.B1.394:                        ; Preds .B1.41
        add       esp, 12                                       ;80.25
                                ; LOE
.B1.42:                         ; Preds .B1.394 .B1.285
        cmp       DWORD PTR [-28+ebp], 0                        ;81.10
        jle       .B1.69        ; Prob 3%                       ;81.10
                                ; LOE
.B1.43:                         ; Preds .B1.42
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TDSPSTEP] ;83.12
        xor       ecx, ecx                                      ;
        mov       eax, DWORD PTR [-72+ebp]                      ;83.12
        cmp       eax, edx                                      ;83.12
        movss     xmm6, DWORD PTR [-48+ebp]                     ;
        pxor      xmm0, xmm0                                    ;
        cmovge    edx, eax                                      ;83.12
        mov       eax, edx                                      ;
        sar       eax, 2                                        ;
        shr       eax, 29                                       ;
        add       eax, edx                                      ;
        sar       eax, 3                                        ;
        mov       DWORD PTR [-32+ebp], edx                      ;
        mov       DWORD PTR [-12+ebp], eax                      ;
        movss     xmm5, DWORD PTR [_2il0floatpacket.21]         ;
        mov       edx, DWORD PTR [-36+ebp]                      ;
                                ; LOE edx ecx xmm5 xmm6
.B1.44:                         ; Preds .B1.67 .B1.43
        cmp       DWORD PTR [-32+ebp], 0                        ;83.12
        jle       .B1.66        ; Prob 50%                      ;83.12
                                ; LOE edx ecx xmm5 xmm6
.B1.45:                         ; Preds .B1.44
        cmp       DWORD PTR [-12+ebp], 0                        ;83.12
        pxor      xmm0, xmm0                                    ;
        jbe       .B1.283       ; Prob 3%                       ;83.12
                                ; LOE edx ecx xmm0 xmm5 xmm6
.B1.46:                         ; Preds .B1.45
        mov       edi, DWORD PTR [-24+ebp]                      ;84.25
        lea       eax, DWORD PTR [1+ecx]                        ;
        mov       DWORD PTR [-52+ebp], eax                      ;
        xor       esi, esi                                      ;
        mov       eax, DWORD PTR [-16+ebp]                      ;84.25
        mov       DWORD PTR [-56+ebp], esi                      ;
        pxor      xmm4, xmm4                                    ;
        mov       DWORD PTR [-44+ebp], ecx                      ;
        pxor      xmm2, xmm2                                    ;
        imul      edi, DWORD PTR [12+edi+eax], 900              ;84.25
        pxor      xmm3, xmm3                                    ;
        pxor      xmm1, xmm1                                    ;
        imul      eax, DWORD PTR [116+edi+edx], -40             ;
        add       eax, DWORD PTR [84+edi+edx]                   ;
        mov       DWORD PTR [-36+ebp], edx                      ;
                                ; LOE eax xmm0 xmm1 xmm2 xmm3 xmm4
.B1.47:                         ; Preds .B1.47 .B1.46
        mov       esi, DWORD PTR [-56+ebp]                      ;84.13
        pxor      xmm5, xmm5                                    ;84.25
        mov       ecx, DWORD PTR [-52+ebp]                      ;84.13
        pxor      xmm7, xmm7                                    ;84.25
        pxor      xmm6, xmm6                                    ;84.25
        lea       edx, DWORD PTR [esi+esi*4]                    ;84.13
        shl       edx, 6                                        ;84.13
        mov       esi, DWORD PTR [72+eax+edx]                   ;84.25
        neg       esi                                           ;84.13
        add       esi, ecx                                      ;84.13
        imul      esi, DWORD PTR [68+eax+edx]                   ;84.13
        mov       edi, DWORD PTR [40+eax+edx]                   ;84.25
        cvtsi2ss  xmm5, DWORD PTR [edi+esi]                     ;84.25
        mov       esi, DWORD PTR [112+eax+edx]                  ;84.25
        addss     xmm0, xmm5                                    ;84.13
        neg       esi                                           ;84.13
        add       esi, ecx                                      ;84.13
        imul      esi, DWORD PTR [108+eax+edx]                  ;84.13
        mov       edi, DWORD PTR [80+eax+edx]                   ;84.25
        cvtsi2ss  xmm7, DWORD PTR [edi+esi]                     ;84.25
        mov       esi, DWORD PTR [152+eax+edx]                  ;84.25
        addss     xmm7, xmm4                                    ;84.13
        neg       esi                                           ;84.13
        pxor      xmm4, xmm4                                    ;84.25
        add       esi, ecx                                      ;84.13
        imul      esi, DWORD PTR [148+eax+edx]                  ;84.13
        mov       edi, DWORD PTR [120+eax+edx]                  ;84.25
        cvtsi2ss  xmm6, DWORD PTR [edi+esi]                     ;84.25
        mov       esi, DWORD PTR [192+eax+edx]                  ;84.25
        addss     xmm6, xmm2                                    ;84.13
        neg       esi                                           ;84.13
        pxor      xmm2, xmm2                                    ;84.25
        add       esi, ecx                                      ;84.13
        imul      esi, DWORD PTR [188+eax+edx]                  ;84.13
        mov       edi, DWORD PTR [160+eax+edx]                  ;84.25
        cvtsi2ss  xmm5, DWORD PTR [edi+esi]                     ;84.25
        mov       esi, DWORD PTR [232+eax+edx]                  ;84.25
        addss     xmm5, xmm3                                    ;84.13
        neg       esi                                           ;84.13
        pxor      xmm3, xmm3                                    ;84.25
        add       esi, ecx                                      ;84.13
        imul      esi, DWORD PTR [228+eax+edx]                  ;84.13
        mov       edi, DWORD PTR [200+eax+edx]                  ;84.25
        cvtsi2ss  xmm2, DWORD PTR [edi+esi]                     ;84.25
        mov       esi, DWORD PTR [272+eax+edx]                  ;84.25
        addss     xmm1, xmm2                                    ;84.13
        neg       esi                                           ;84.13
        add       esi, ecx                                      ;84.13
        imul      esi, DWORD PTR [268+eax+edx]                  ;84.13
        mov       edi, DWORD PTR [240+eax+edx]                  ;84.25
        cvtsi2ss  xmm4, DWORD PTR [edi+esi]                     ;84.25
        mov       esi, DWORD PTR [312+eax+edx]                  ;84.25
        addss     xmm4, xmm7                                    ;84.13
        neg       esi                                           ;84.13
        add       esi, ecx                                      ;84.13
        imul      esi, DWORD PTR [308+eax+edx]                  ;84.13
        mov       edi, DWORD PTR [280+eax+edx]                  ;84.25
        cvtsi2ss  xmm2, DWORD PTR [edi+esi]                     ;84.25
        DB        15                                            ;84.25
        DB        31                                            ;84.25
        DB        64                                            ;84.25
        DB        0                                             ;84.25
        mov       edi, DWORD PTR [352+eax+edx]                  ;84.25
        addss     xmm2, xmm6                                    ;84.13
        neg       edi                                           ;84.13
        add       edi, ecx                                      ;84.13
        imul      edi, DWORD PTR [348+eax+edx]                  ;84.13
        mov       edx, DWORD PTR [320+eax+edx]                  ;84.25
        mov       ecx, DWORD PTR [-56+ebp]                      ;83.12
        inc       ecx                                           ;83.12
        cvtsi2ss  xmm3, DWORD PTR [edx+edi]                     ;84.25
        mov       DWORD PTR [-56+ebp], ecx                      ;83.12
        addss     xmm3, xmm5                                    ;84.13
        cmp       ecx, DWORD PTR [-12+ebp]                      ;83.12
        jb        .B1.47        ; Prob 99%                      ;83.12
                                ; LOE eax ecx cl ch xmm0 xmm1 xmm2 xmm3 xmm4
.B1.48:                         ; Preds .B1.47
        mov       esi, ecx                                      ;
        lea       eax, DWORD PTR [1+esi*8]                      ;83.12
        movss     xmm6, DWORD PTR [-48+ebp]                     ;
        addss     xmm0, xmm4                                    ;83.12
        mov       ecx, DWORD PTR [-44+ebp]                      ;
        addss     xmm0, xmm2                                    ;83.12
        mov       edx, DWORD PTR [-36+ebp]                      ;
        addss     xmm0, xmm3                                    ;83.12
        movss     xmm5, DWORD PTR [_2il0floatpacket.21]         ;
        addss     xmm0, xmm1                                    ;83.12
        mov       DWORD PTR [-8+ebp], eax                       ;83.12
                                ; LOE eax edx ecx al ah xmm0 xmm5 xmm6
.B1.49:                         ; Preds .B1.48 .B1.283
        cmp       eax, DWORD PTR [-32+ebp]                      ;83.12
        ja        .B1.65        ; Prob 50%                      ;83.12
                                ; LOE edx ecx xmm0 xmm5 xmm6
.B1.50:                         ; Preds .B1.49
        mov       eax, DWORD PTR [-32+ebp]                      ;83.12
        sub       eax, DWORD PTR [-8+ebp]                       ;83.12
        jmp       DWORD PTR [..1..TPKT.5_0+eax*4]               ;83.12
                                ; LOE edx ecx xmm0 xmm5 xmm6
..1.5_0.TAG.6::
.B1.52:                         ; Preds .B1.50
        mov       eax, DWORD PTR [-16+ebp]                      ;84.25
        mov       edi, DWORD PTR [-24+ebp]                      ;84.25
        imul      edi, DWORD PTR [12+edi+eax], 900              ;84.25
        lea       eax, DWORD PTR [1+ecx]                        ;84.13
        mov       esi, DWORD PTR [116+edi+edx]                  ;84.25
        neg       esi                                           ;84.13
        add       esi, DWORD PTR [-8+ebp]                       ;84.13
        mov       edi, DWORD PTR [84+edi+edx]                   ;84.25
        lea       esi, DWORD PTR [esi+esi*4]                    ;84.13
        sub       eax, DWORD PTR [272+edi+esi*8]                ;84.13
        imul      eax, DWORD PTR [268+edi+esi*8]                ;84.13
        mov       esi, DWORD PTR [240+edi+esi*8]                ;84.25
        cvtsi2ss  xmm1, DWORD PTR [esi+eax]                     ;84.25
        addss     xmm0, xmm1                                    ;84.13
                                ; LOE edx ecx xmm0 xmm5 xmm6
..1.5_0.TAG.5::
.B1.54:                         ; Preds .B1.50 .B1.52
        mov       eax, DWORD PTR [-16+ebp]                      ;84.25
        mov       edi, DWORD PTR [-24+ebp]                      ;84.25
        imul      edi, DWORD PTR [12+edi+eax], 900              ;84.25
        lea       eax, DWORD PTR [1+ecx]                        ;84.13
        mov       esi, DWORD PTR [116+edi+edx]                  ;84.25
        neg       esi                                           ;84.13
        add       esi, DWORD PTR [-8+ebp]                       ;84.13
        mov       edi, DWORD PTR [84+edi+edx]                   ;84.25
        lea       esi, DWORD PTR [esi+esi*4]                    ;84.13
        sub       eax, DWORD PTR [232+edi+esi*8]                ;84.13
        imul      eax, DWORD PTR [228+edi+esi*8]                ;84.13
        mov       esi, DWORD PTR [200+edi+esi*8]                ;84.25
        cvtsi2ss  xmm1, DWORD PTR [esi+eax]                     ;84.25
        addss     xmm0, xmm1                                    ;84.13
                                ; LOE edx ecx xmm0 xmm5 xmm6
..1.5_0.TAG.4::
.B1.56:                         ; Preds .B1.50 .B1.54
        mov       eax, DWORD PTR [-16+ebp]                      ;84.25
        mov       edi, DWORD PTR [-24+ebp]                      ;84.25
        imul      edi, DWORD PTR [12+edi+eax], 900              ;84.25
        lea       eax, DWORD PTR [1+ecx]                        ;84.13
        mov       esi, DWORD PTR [116+edi+edx]                  ;84.25
        neg       esi                                           ;84.13
        add       esi, DWORD PTR [-8+ebp]                       ;84.13
        mov       edi, DWORD PTR [84+edi+edx]                   ;84.25
        lea       esi, DWORD PTR [esi+esi*4]                    ;84.13
        sub       eax, DWORD PTR [192+edi+esi*8]                ;84.13
        imul      eax, DWORD PTR [188+edi+esi*8]                ;84.13
        mov       esi, DWORD PTR [160+edi+esi*8]                ;84.25
        cvtsi2ss  xmm1, DWORD PTR [esi+eax]                     ;84.25
        addss     xmm0, xmm1                                    ;84.13
                                ; LOE edx ecx xmm0 xmm5 xmm6
..1.5_0.TAG.3::
.B1.58:                         ; Preds .B1.50 .B1.56
        mov       eax, DWORD PTR [-16+ebp]                      ;84.25
        mov       edi, DWORD PTR [-24+ebp]                      ;84.25
        imul      edi, DWORD PTR [12+edi+eax], 900              ;84.25
        lea       eax, DWORD PTR [1+ecx]                        ;84.13
        mov       esi, DWORD PTR [116+edi+edx]                  ;84.25
        neg       esi                                           ;84.13
        add       esi, DWORD PTR [-8+ebp]                       ;84.13
        mov       edi, DWORD PTR [84+edi+edx]                   ;84.25
        lea       esi, DWORD PTR [esi+esi*4]                    ;84.13
        sub       eax, DWORD PTR [152+edi+esi*8]                ;84.13
        imul      eax, DWORD PTR [148+edi+esi*8]                ;84.13
        mov       esi, DWORD PTR [120+edi+esi*8]                ;84.25
        cvtsi2ss  xmm1, DWORD PTR [esi+eax]                     ;84.25
        addss     xmm0, xmm1                                    ;84.13
                                ; LOE edx ecx xmm0 xmm5 xmm6
..1.5_0.TAG.2::
.B1.60:                         ; Preds .B1.50 .B1.58
        mov       eax, DWORD PTR [-16+ebp]                      ;84.25
        mov       edi, DWORD PTR [-24+ebp]                      ;84.25
        imul      edi, DWORD PTR [12+edi+eax], 900              ;84.25
        lea       eax, DWORD PTR [1+ecx]                        ;84.13
        mov       esi, DWORD PTR [116+edi+edx]                  ;84.25
        neg       esi                                           ;84.13
        add       esi, DWORD PTR [-8+ebp]                       ;84.13
        mov       edi, DWORD PTR [84+edi+edx]                   ;84.25
        lea       esi, DWORD PTR [esi+esi*4]                    ;84.13
        sub       eax, DWORD PTR [112+edi+esi*8]                ;84.13
        imul      eax, DWORD PTR [108+edi+esi*8]                ;84.13
        mov       esi, DWORD PTR [80+edi+esi*8]                 ;84.25
        cvtsi2ss  xmm1, DWORD PTR [esi+eax]                     ;84.25
        addss     xmm0, xmm1                                    ;84.13
                                ; LOE edx ecx xmm0 xmm5 xmm6
..1.5_0.TAG.1::
.B1.62:                         ; Preds .B1.50 .B1.60
        mov       eax, DWORD PTR [-16+ebp]                      ;84.25
        mov       edi, DWORD PTR [-24+ebp]                      ;84.25
        imul      edi, DWORD PTR [12+edi+eax], 900              ;84.25
        lea       eax, DWORD PTR [1+ecx]                        ;84.13
        mov       esi, DWORD PTR [116+edi+edx]                  ;84.25
        neg       esi                                           ;84.13
        add       esi, DWORD PTR [-8+ebp]                       ;84.13
        mov       edi, DWORD PTR [84+edi+edx]                   ;84.25
        lea       esi, DWORD PTR [esi+esi*4]                    ;84.13
        sub       eax, DWORD PTR [72+edi+esi*8]                 ;84.13
        imul      eax, DWORD PTR [68+edi+esi*8]                 ;84.13
        mov       esi, DWORD PTR [40+edi+esi*8]                 ;84.25
        cvtsi2ss  xmm1, DWORD PTR [esi+eax]                     ;84.25
        addss     xmm0, xmm1                                    ;84.13
                                ; LOE edx ecx xmm0 xmm5 xmm6
..1.5_0.TAG.0::
.B1.64:                         ; Preds .B1.50 .B1.62
        mov       eax, DWORD PTR [-16+ebp]                      ;84.25
        inc       ecx                                           ;84.13
        mov       edi, DWORD PTR [-24+ebp]                      ;84.25
        imul      edi, DWORD PTR [12+edi+eax], 900              ;84.25
        add       edi, edx                                      ;84.13
        mov       DWORD PTR [-40+ebp], edi                      ;84.13
        mov       esi, DWORD PTR [116+edi]                      ;84.25
        neg       esi                                           ;84.13
        add       esi, DWORD PTR [-8+ebp]                       ;84.13
        mov       edi, DWORD PTR [84+edi]                       ;84.25
        lea       esi, DWORD PTR [esi+esi*4]                    ;84.13
        mov       eax, DWORD PTR [32+edi+esi*8]                 ;84.25
        neg       eax                                           ;84.13
        add       eax, ecx                                      ;84.13
        imul      eax, DWORD PTR [28+edi+esi*8]                 ;84.13
        mov       esi, DWORD PTR [edi+esi*8]                    ;84.25
        cvtsi2ss  xmm1, DWORD PTR [esi+eax]                     ;84.25
        mov       eax, DWORD PTR [-40+ebp]                      ;84.13
        addss     xmm0, xmm1                                    ;84.13
        jmp       .B1.67        ; Prob 100%                     ;84.13
                                ; LOE eax edx ecx xmm0 xmm5 xmm6
.B1.65:                         ; Preds .B1.49
        mov       esi, DWORD PTR [-16+ebp]                      ;86.12
        inc       ecx                                           ;
        mov       eax, DWORD PTR [-24+ebp]                      ;86.12
        imul      eax, DWORD PTR [12+eax+esi], 900              ;86.12
        add       eax, edx                                      ;
        jmp       .B1.67        ; Prob 100%                     ;
                                ; LOE eax edx ecx xmm0 xmm5 xmm6
.B1.66:                         ; Preds .B1.44
        mov       esi, DWORD PTR [-16+ebp]                      ;86.12
        inc       ecx                                           ;
        mov       eax, DWORD PTR [-24+ebp]                      ;86.12
        pxor      xmm0, xmm0                                    ;
        imul      eax, DWORD PTR [12+eax+esi], 900              ;86.12
        add       eax, edx                                      ;
                                ; LOE eax edx ecx xmm0 xmm5 xmm6
.B1.67:                         ; Preds .B1.64 .B1.65 .B1.66
        divss     xmm0, xmm6                                    ;86.116
        mov       DWORD PTR [-36+ebp], edx                      ;
        mov       edi, DWORD PTR [156+eax]                      ;86.12
        mov       edx, DWORD PTR [-20+ebp]                      ;86.12
        mulss     xmm0, xmm5                                    ;86.127
        lea       esi, DWORD PTR [edi+edx*8]                    ;86.12
        imul      edi, DWORD PTR [188+eax], -40                 ;86.12
        cvttss2si edx, xmm0                                     ;86.12
        mov       eax, DWORD PTR [32+edi+esi]                   ;86.12
        neg       eax                                           ;86.12
        add       eax, ecx                                      ;86.12
        imul      eax, DWORD PTR [28+edi+esi]                   ;86.12
        mov       esi, DWORD PTR [edi+esi]                      ;86.12
        cmp       ecx, DWORD PTR [-28+ebp]                      ;81.10
        mov       WORD PTR [esi+eax], dx                        ;86.12
        mov       edx, DWORD PTR [-36+ebp]                      ;81.10
        jb        .B1.44        ; Prob 82%                      ;81.10
                                ; LOE edx ecx dl dh xmm5 xmm6
.B1.68:                         ; Preds .B1.67
        DB        144                                           ;
        mov       DWORD PTR [-36+ebp], edx                      ;
                                ; LOE
.B1.69:                         ; Preds .B1.36 .B1.68 .B1.42
        mov       eax, DWORD PTR [-80+ebp]                      ;90.9
        inc       eax                                           ;90.9
        mov       DWORD PTR [-80+ebp], eax                      ;90.9
        cmp       eax, DWORD PTR [-76+ebp]                      ;90.9
        jle       .B1.16        ; Prob 82%                      ;90.9
                                ; LOE
.B1.70:                         ; Preds .B1.69
        mov       edi, DWORD PTR [-116+ebp]                     ;
        mov       esi, DWORD PTR [-68+ebp]                      ;
                                ; LOE esi edi
.B1.71:                         ; Preds .B1.70 .B1.14
        test      edi, edi                                      ;93.17
        jle       .B1.378       ; Prob 16%                      ;93.17
                                ; LOE esi
.B1.72:                         ; Preds .B1.13 .B1.71
        push      32                                            ;93.22
        mov       DWORD PTR [-440+ebp], 0                       ;93.22
        lea       eax, DWORD PTR [-344+ebp]                     ;93.22
        push      eax                                           ;93.22
        push      OFFSET FLAT: __STRLITPACK_57.0.1              ;93.22
        push      -2088435968                                   ;93.22
        push      711                                           ;93.22
        mov       DWORD PTR [-344+ebp], 16                      ;93.22
        lea       edx, DWORD PTR [-440+ebp]                     ;93.22
        push      edx                                           ;93.22
        mov       DWORD PTR [-340+ebp], OFFSET FLAT: __STRLITPACK_38 ;93.22
        call      _for_write_seq_lis                            ;93.22
                                ; LOE esi
.B1.395:                        ; Preds .B1.72
        add       esp, 24                                       ;93.22
                                ; LOE esi
.B1.73:                         ; Preds .B1.395
        push      32                                            ;97.22
        xor       eax, eax                                      ;95.7
        lea       edx, DWORD PTR [-256+ebp]                     ;97.22
        push      edx                                           ;97.22
        push      OFFSET FLAT: __STRLITPACK_58.0.1              ;97.22
        push      -2088435968                                   ;97.22
        push      711                                           ;97.22
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ICALLKSP], eax ;95.7
        lea       ecx, DWORD PTR [-440+ebp]                     ;97.22
        push      ecx                                           ;97.22
        mov       DWORD PTR [-440+ebp], eax                     ;97.22
        mov       DWORD PTR [-256+ebp], 16                      ;97.22
        mov       DWORD PTR [-252+ebp], OFFSET FLAT: __STRLITPACK_36 ;97.22
        call      _for_write_seq_lis                            ;97.22
                                ; LOE esi
.B1.396:                        ; Preds .B1.73
        add       esp, 24                                       ;97.22
                                ; LOE esi
.B1.74:                         ; Preds .B1.378 .B1.396
        movss     xmm2, DWORD PTR [_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT] ;99.16
        movss     xmm1, DWORD PTR [_2il0floatpacket.19]         ;100.38
        mulss     xmm1, xmm2                                    ;100.38
        movss     xmm0, DWORD PTR [-160+ebp]                    ;99.7
        mulss     xmm0, xmm2                                    ;99.7
        addss     xmm1, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TIME_NOW] ;100.7
        movss     DWORD PTR [-180+ebp], xmm0                    ;99.7
        movss     DWORD PTR [_DYNUST_MAIN_MODULE_mp_TIME_NOW], xmm1 ;100.7
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ITERATION], 0 ;102.20
        jne       .B1.76        ; Prob 50%                      ;102.20
                                ; LOE esi xmm2
.B1.75:                         ; Preds .B1.74
        test      BYTE PTR [_DYNUST_MAIN_MODULE_mp_CALLAVG], 1  ;102.34
        je        .B1.287       ; Prob 7%                       ;102.34
                                ; LOE esi xmm2
.B1.76:                         ; Preds .B1.288 .B1.75 .B1.74
        movss     xmm5, DWORD PTR [_2il0floatpacket.28]         ;107.16
        mov       eax, esi                                      ;107.10
        divss     xmm5, xmm2                                    ;107.16
        movss     xmm1, DWORD PTR [_2il0floatpacket.24]         ;107.16
        andps     xmm1, xmm5                                    ;107.16
        pxor      xmm5, xmm1                                    ;107.16
        movss     xmm3, DWORD PTR [_2il0floatpacket.25]         ;107.16
        movaps    xmm4, xmm5                                    ;107.16
        movaps    xmm0, xmm5                                    ;107.16
        cmpltss   xmm4, xmm3                                    ;107.16
        cdq                                                     ;107.10
        andps     xmm3, xmm4                                    ;107.16
        movss     xmm6, DWORD PTR [_2il0floatpacket.26]         ;107.16
        addss     xmm0, xmm3                                    ;107.16
        subss     xmm0, xmm3                                    ;107.16
        movaps    xmm3, xmm0                                    ;107.16
        subss     xmm3, xmm5                                    ;107.16
        movaps    xmm5, xmm6                                    ;107.16
        addss     xmm5, xmm6                                    ;107.16
        movaps    xmm7, xmm3                                    ;107.16
        cmpless   xmm3, DWORD PTR [_2il0floatpacket.27]         ;107.16
        cmpnless  xmm7, xmm6                                    ;107.16
        andps     xmm7, xmm5                                    ;107.16
        andps     xmm3, xmm5                                    ;107.16
        subss     xmm0, xmm7                                    ;107.16
        addss     xmm0, xmm3                                    ;107.16
        orps      xmm0, xmm1                                    ;107.16
        cvtss2si  ecx, xmm0                                     ;107.16
        idiv      ecx                                           ;107.10
        test      edx, edx                                      ;107.39
        jne       .B1.79        ; Prob 78%                      ;107.39
                                ; LOE esi xmm2
.B1.77:                         ; Preds .B1.76
        lea       eax, DWORD PTR [-188+ebp]                     ;108.11
        push      eax                                           ;108.11
        call      _NEXTA_STAT                                   ;108.11
                                ; LOE
.B1.397:                        ; Preds .B1.77
        add       esp, 4                                        ;108.11
                                ; LOE
.B1.78:                         ; Preds .B1.397
        mov       esi, DWORD PTR [-188+ebp]                     ;115.17
        movss     xmm2, DWORD PTR [_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT] ;115.17
                                ; LOE esi xmm2
.B1.79:                         ; Preds .B1.78 .B1.76
        cvtsi2ss  xmm0, esi                                     ;115.17
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ISIG]  ;113.5
        cmp       ecx, 1                                        ;113.13
        mulss     xmm2, xmm0                                    ;115.18
        jle       .B1.84        ; Prob 16%                      ;113.13
                                ; LOE ecx xmm2
.B1.80:                         ; Preds .B1.79
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_STRTSIG+32] ;115.18
        mov       edx, 2                                        ;
        shl       eax, 2                                        ;
        neg       eax                                           ;
        add       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_STRTSIG] ;
        movss     xmm1, DWORD PTR [_2il0floatpacket.23]         ;
                                ; LOE eax edx ecx xmm1 xmm2
.B1.81:                         ; Preds .B1.82 .B1.80
        movaps    xmm0, xmm2                                    ;115.32
        subss     xmm0, DWORD PTR [eax+edx*4]                   ;115.32
        comiss    xmm1, xmm0                                    ;115.46
        ja        .B1.376       ; Prob 20%                      ;115.46
                                ; LOE eax edx ecx xmm1 xmm2
.B1.82:                         ; Preds .B1.81
        inc       edx                                           ;119.6
        cmp       edx, ecx                                      ;119.6
        jle       .B1.81        ; Prob 82%                      ;119.6
                                ; LOE eax edx ecx xmm1 xmm2
.B1.84:                         ; Preds .B1.82 .B1.79 .B1.376
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_WORKZONENUM], 0 ;122.20
        jle       .B1.87        ; Prob 16%                      ;122.20
                                ; LOE xmm2
.B1.85:                         ; Preds .B1.84
        movss     DWORD PTR [-208+ebp], xmm2                    ;122.25
        lea       eax, DWORD PTR [-208+ebp]                     ;122.25
        push      eax                                           ;122.25
        call      _WORKZONE_MODULE_mp_WZ_SCAN                   ;122.25
                                ; LOE
.B1.398:                        ; Preds .B1.85
        add       esp, 4                                        ;122.25
                                ; LOE
.B1.86:                         ; Preds .B1.398
        cvtsi2ss  xmm2, DWORD PTR [-188+ebp]                    ;124.20
        mulss     xmm2, DWORD PTR [_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT] ;124.10
                                ; LOE xmm2
.B1.87:                         ; Preds .B1.86 .B1.84
        movss     DWORD PTR [-172+ebp], xmm2                    ;124.10
        lea       eax, DWORD PTR [-172+ebp]                     ;124.10
        push      eax                                           ;124.10
        call      _INCIDENT_MODULE_mp_INCI_SCAN                 ;124.10
                                ; LOE
.B1.399:                        ; Preds .B1.87
        add       esp, 4                                        ;124.10
                                ; LOE
.B1.88:                         ; Preds .B1.399
        mov       eax, DWORD PTR [-188+ebp]                     ;126.5
        dec       eax                                           ;126.13
        cdq                                                     ;126.8
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TDSPSTEP] ;126.13
        idiv      ecx                                           ;126.8
        test      edx, edx                                      ;126.26
        jne       .B1.144       ; Prob 78%                      ;126.26
                                ; LOE
.B1.89:                         ; Preds .B1.88
        lea       eax, DWORD PTR [-188+ebp]                     ;127.14
        push      eax                                           ;127.14
        call      _CAL_PENALTY                                  ;127.14
                                ; LOE
.B1.400:                        ; Preds .B1.89
        add       esp, 4                                        ;127.14
                                ; LOE
.B1.90:                         ; Preds .B1.400
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_UETIMEFLAGR], 0 ;128.24
        jle       .B1.144       ; Prob 16%                      ;128.24
                                ; LOE
.B1.91:                         ; Preds .B1.90
        mov       eax, DWORD PTR [-188+ebp]                     ;129.13
        xor       edx, edx                                      ;129.13
        add       eax, -2                                       ;129.35
        cvtsi2ss  xmm1, eax                                     ;129.35
        cvtsi2ss  xmm0, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIMPERAGG] ;129.44
        divss     xmm1, xmm0                                    ;129.29
        cvttss2si ecx, xmm1                                     ;129.29
        test      ecx, ecx                                      ;129.13
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ITI_NUTMP] ;130.13
        cmovl     ecx, edx                                      ;129.13
        inc       ecx                                           ;129.13
        cmp       ecx, esi                                      ;130.13
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_INCI_NUM] ;132.13
        cmovl     esi, ecx                                      ;130.13
        test      edi, edi                                      ;132.25
        mov       DWORD PTR [-612+ebp], ecx                     ;129.13
        mov       DWORD PTR [-628+ebp], esi                     ;130.13
        mov       DWORD PTR [-472+ebp], edi                     ;132.13
        jg        .B1.93        ; Prob 21%                      ;132.25
                                ; LOE edi
.B1.92:                         ; Preds .B1.91
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELTIME+32] ;153.13
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+44] ;154.13
        mov       DWORD PTR [-732+ebp], eax                     ;153.13
        mov       DWORD PTR [-700+ebp], edx                     ;154.13
        jmp       .B1.113       ; Prob 100%                     ;154.13
                                ; LOE
.B1.93:                         ; Preds .B1.91
        mov       ecx, 4                                        ;133.17
        mov       edx, 0                                        ;133.17
        mov       esi, edi                                      ;133.17
        lea       edi, DWORD PTR [-788+ebp]                     ;133.17
        push      ecx                                           ;133.17
        cmovle    esi, edx                                      ;133.17
        mov       eax, 1                                        ;133.17
        push      esi                                           ;133.17
        push      2                                             ;133.17
        push      edi                                           ;133.17
        mov       DWORD PTR [-772+ebp], 133                     ;133.17
        mov       DWORD PTR [-780+ebp], ecx                     ;133.17
        mov       DWORD PTR [-768+ebp], eax                     ;133.17
        mov       DWORD PTR [-776+ebp], edx                     ;133.17
        mov       DWORD PTR [-752+ebp], eax                     ;133.17
        mov       DWORD PTR [-760+ebp], esi                     ;133.17
        mov       DWORD PTR [-756+ebp], ecx                     ;133.17
        call      _for_check_mult_overflow                      ;133.17
                                ; LOE eax
.B1.401:                        ; Preds .B1.93
        add       esp, 16                                       ;133.17
                                ; LOE eax
.B1.94:                         ; Preds .B1.401
        mov       edx, DWORD PTR [-772+ebp]                     ;133.17
        and       eax, 1                                        ;133.17
        and       edx, 1                                        ;133.17
        lea       ecx, DWORD PTR [-784+ebp]                     ;133.17
        shl       eax, 4                                        ;133.17
        add       edx, edx                                      ;133.17
        or        edx, eax                                      ;133.17
        or        edx, 262144                                   ;133.17
        push      edx                                           ;133.17
        push      ecx                                           ;133.17
        push      DWORD PTR [-788+ebp]                          ;133.17
        call      _for_alloc_allocatable                        ;133.17
                                ; LOE
.B1.402:                        ; Preds .B1.94
        add       esp, 12                                       ;133.17
                                ; LOE
.B1.95:                         ; Preds .B1.402
        mov       ecx, DWORD PTR [-760+ebp]                     ;134.17
        test      ecx, ecx                                      ;134.17
        jle       .B1.98        ; Prob 50%                      ;134.17
                                ; LOE ecx
.B1.96:                         ; Preds .B1.95
        mov       edx, DWORD PTR [-784+ebp]                     ;134.17
        cmp       ecx, 24                                       ;134.17
        jle       .B1.289       ; Prob 0%                       ;134.17
                                ; LOE edx ecx
.B1.97:                         ; Preds .B1.96
        shl       ecx, 2                                        ;134.17
        push      ecx                                           ;134.17
        push      0                                             ;134.17
        push      edx                                           ;134.17
        call      __intel_fast_memset                           ;134.17
                                ; LOE
.B1.403:                        ; Preds .B1.97
        add       esp, 12                                       ;134.17
                                ; LOE
.B1.98:                         ; Preds .B1.295 .B1.403 .B1.95 .B1.293
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_INCI_NUM] ;135.17
        mov       ecx, 4                                        ;135.17
        xor       edx, edx                                      ;135.17
        test      eax, eax                                      ;135.17
        push      ecx                                           ;135.17
        cmovle    eax, edx                                      ;135.17
        mov       esi, 1                                        ;135.17
        push      edx                                           ;135.17
        push      eax                                           ;135.17
        mov       DWORD PTR [-840+ebp], esi                     ;135.17
        mov       DWORD PTR [-828+ebp], esi                     ;135.17
        lea       esi, DWORD PTR [-748+ebp]                     ;135.17
        push      3                                             ;135.17
        push      esi                                           ;135.17
        mov       DWORD PTR [-860+ebp], 133                     ;135.17
        lea       edi, DWORD PTR [eax*4]                        ;135.17
        mov       DWORD PTR [-868+ebp], ecx                     ;135.17
        mov       DWORD PTR [-856+ebp], 2                       ;135.17
        mov       DWORD PTR [-864+ebp], edx                     ;135.17
        mov       DWORD PTR [-848+ebp], eax                     ;135.17
        mov       DWORD PTR [-836+ebp], edx                     ;135.17
        mov       DWORD PTR [-844+ebp], ecx                     ;135.17
        mov       DWORD PTR [-832+ebp], edi                     ;135.17
        call      _for_check_mult_overflow                      ;135.17
                                ; LOE eax
.B1.404:                        ; Preds .B1.98
        add       esp, 20                                       ;135.17
                                ; LOE eax
.B1.99:                         ; Preds .B1.404
        mov       edx, DWORD PTR [-860+ebp]                     ;135.17
        and       eax, 1                                        ;135.17
        and       edx, 1                                        ;135.17
        lea       ecx, DWORD PTR [-872+ebp]                     ;135.17
        shl       eax, 4                                        ;135.17
        add       edx, edx                                      ;135.17
        or        edx, eax                                      ;135.17
        or        edx, 262144                                   ;135.17
        push      edx                                           ;135.17
        push      ecx                                           ;135.17
        push      DWORD PTR [-748+ebp]                          ;135.17
        call      _for_alloc_allocatable                        ;135.17
                                ; LOE
.B1.405:                        ; Preds .B1.99
        add       esp, 12                                       ;135.17
                                ; LOE
.B1.100:                        ; Preds .B1.405
        mov       eax, DWORD PTR [-836+ebp]                     ;136.17
        test      eax, eax                                      ;136.17
        mov       ecx, DWORD PTR [-828+ebp]                     ;136.17
        mov       DWORD PTR [-476+ebp], eax                     ;136.17
        jle       .B1.102       ; Prob 10%                      ;136.17
                                ; LOE ecx
.B1.101:                        ; Preds .B1.100
        mov       eax, DWORD PTR [-832+ebp]                     ;139.21
        mov       edi, DWORD PTR [-848+ebp]                     ;136.17
        test      edi, edi                                      ;136.17
        mov       esi, DWORD PTR [-872+ebp]                     ;139.21
        mov       edx, DWORD PTR [-840+ebp]                     ;136.17
        mov       DWORD PTR [-480+ebp], eax                     ;139.21
        jg        .B1.276       ; Prob 50%                      ;136.17
                                ; LOE edx ecx esi edi
.B1.102:                        ; Preds .B1.371 .B1.279 .B1.100 .B1.101
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_INCI_NUM] ;137.17
        test      ecx, ecx                                      ;137.17
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELTIME+32] ;153.13
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+44] ;154.13
        mov       DWORD PTR [-472+ebp], ecx                     ;137.17
        mov       DWORD PTR [-732+ebp], eax                     ;153.13
        mov       DWORD PTR [-700+ebp], edx                     ;154.13
        jle       .B1.362       ; Prob 2%                       ;137.17
                                ; LOE eax al ah
.B1.103:                        ; Preds .B1.102
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELTIME+44] ;138.36
        neg       ecx                                           ;
        add       ecx, DWORD PTR [-612+ebp]                     ;
        mov       edi, eax                                      ;
        imul      ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELTIME+40] ;
        movss     xmm0, DWORD PTR [_2il0floatpacket.21]         ;
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELTIME] ;138.21
        shl       edi, 2                                        ;
        sub       esi, edi                                      ;
        add       ecx, esi                                      ;
        mov       DWORD PTR [-460+ebp], ecx                     ;
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_INCIL+32] ;
        shl       ecx, 2                                        ;
        neg       ecx                                           ;
        add       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_INCIL] ;
        mov       DWORD PTR [-464+ebp], ecx                     ;
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+32] ;
        shl       ecx, 3                                        ;
        mov       esi, DWORD PTR [-752+ebp]                     ;
        shl       esi, 2                                        ;
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE] ;
        neg       esi                                           ;
        sub       edi, ecx                                      ;
        imul      ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32], -152 ;
        add       esi, DWORD PTR [-784+ebp]                     ;
        mov       DWORD PTR [-468+ebp], esi                     ;
        mov       esi, DWORD PTR [-840+ebp]                     ;
        shl       esi, 2                                        ;
        mov       DWORD PTR [-548+ebp], edi                     ;
        mov       edi, DWORD PTR [-872+ebp]                     ;
        sub       edi, esi                                      ;
        add       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;
        mov       eax, DWORD PTR [-832+ebp]                     ;139.21
        mov       esi, DWORD PTR [-476+ebp]                     ;
        mov       DWORD PTR [-536+ebp], ecx                     ;
        mov       ecx, esi                                      ;
        shr       ecx, 31                                       ;
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+40] ;139.21
        add       ecx, esi                                      ;
        mov       DWORD PTR [-260+ebp], eax                     ;139.21
        lea       esi, DWORD PTR [eax+edi]                      ;
        mov       DWORD PTR [-532+ebp], esi                     ;
        neg       eax                                           ;
        mov       esi, DWORD PTR [-700+ebp]                     ;
        add       eax, edi                                      ;
        imul      esi, edx                                      ;
        mov       DWORD PTR [-248+ebp], edx                     ;139.21
        mov       DWORD PTR [-520+ebp], edi                     ;
        lea       edi, DWORD PTR [edx+esi]                      ;
        neg       edx                                           ;
        mov       DWORD PTR [-524+ebp], eax                     ;
        mov       eax, DWORD PTR [-548+ebp]                     ;
        sub       eax, esi                                      ;
        add       esi, edx                                      ;
        sar       ecx, 1                                        ;
        add       edi, eax                                      ;
        mov       DWORD PTR [-552+ebp], 0                       ;139.21
        add       eax, esi                                      ;
        mov       DWORD PTR [-300+ebp], ecx                     ;
        mov       DWORD PTR [-528+ebp], edi                     ;
        mov       ecx, DWORD PTR [-552+ebp]                     ;
        mov       DWORD PTR [-544+ebp], eax                     ;
                                ; LOE ecx xmm0
.B1.104:                        ; Preds .B1.111 .B1.103
        mov       edx, DWORD PTR [-464+ebp]                     ;138.36
        mov       eax, DWORD PTR [-460+ebp]                     ;138.36
        mov       esi, DWORD PTR [-468+ebp]                     ;138.21
        mov       edx, DWORD PTR [4+edx+ecx*4]                  ;138.36
        cmp       DWORD PTR [-476+ebp], 0                       ;139.21
        cvtsi2ss  xmm1, DWORD PTR [eax+edx*4]                   ;138.36
        divss     xmm1, xmm0                                    ;138.21
        movss     DWORD PTR [4+esi+ecx*4], xmm1                 ;138.21
        jle       .B1.111       ; Prob 50%                      ;139.21
                                ; LOE edx ecx xmm0
.B1.105:                        ; Preds .B1.104
        cmp       DWORD PTR [-300+ebp], 0                       ;139.21
        jbe       .B1.298       ; Prob 11%                      ;139.21
                                ; LOE edx ecx xmm0
.B1.106:                        ; Preds .B1.105
        mov       edi, DWORD PTR [-520+ebp]                     ;
        xor       eax, eax                                      ;
        mov       DWORD PTR [-540+ebp], edx                     ;
        mov       DWORD PTR [-552+ebp], ecx                     ;
        lea       esi, DWORD PTR [edi+ecx*4]                    ;
        mov       edi, DWORD PTR [-532+ebp]                     ;
        mov       DWORD PTR [-268+ebp], esi                     ;
        lea       esi, DWORD PTR [edi+ecx*4]                    ;
        mov       edi, DWORD PTR [-464+ebp]                     ;139.21
        mov       DWORD PTR [-284+ebp], esi                     ;
        imul      esi, DWORD PTR [4+edi+ecx*4], 152             ;139.21
        mov       edi, DWORD PTR [-536+ebp]                     ;139.21
        mov       esi, DWORD PTR [12+esi+edi]                   ;139.21
        mov       edi, DWORD PTR [-548+ebp]                     ;
        lea       edi, DWORD PTR [edi+esi*8]                    ;
        mov       DWORD PTR [-276+ebp], edi                     ;
        mov       edi, DWORD PTR [-528+ebp]                     ;
        lea       edi, DWORD PTR [edi+esi*8]                    ;
        xor       esi, esi                                      ;
        mov       DWORD PTR [-292+ebp], edi                     ;
        mov       edx, esi                                      ;
                                ; LOE eax edx esi xmm0
.B1.107:                        ; Preds .B1.107 .B1.106
        mov       ecx, DWORD PTR [-276+ebp]                     ;139.21
        inc       eax                                           ;139.21
        mov       edi, DWORD PTR [ecx+edx*2]                    ;139.21
        mov       ecx, DWORD PTR [-268+ebp]                     ;139.21
        mov       DWORD PTR [4+ecx+esi*2], edi                  ;139.21
        mov       edi, DWORD PTR [-292+ebp]                     ;139.21
        mov       ecx, DWORD PTR [edi+edx*2]                    ;139.21
        mov       edi, DWORD PTR [-284+ebp]                     ;139.21
        add       edx, DWORD PTR [-248+ebp]                     ;139.21
        mov       DWORD PTR [4+edi+esi*2], ecx                  ;139.21
        add       esi, DWORD PTR [-260+ebp]                     ;139.21
        cmp       eax, DWORD PTR [-300+ebp]                     ;139.21
        jb        .B1.107       ; Prob 64%                      ;139.21
                                ; LOE eax edx esi xmm0
.B1.108:                        ; Preds .B1.107
        mov       edx, DWORD PTR [-540+ebp]                     ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;139.21
        mov       ecx, DWORD PTR [-552+ebp]                     ;
                                ; LOE eax edx ecx xmm0
.B1.109:                        ; Preds .B1.108 .B1.298
        lea       esi, DWORD PTR [-1+eax]                       ;139.21
        cmp       esi, DWORD PTR [-476+ebp]                     ;139.21
        jae       .B1.111       ; Prob 11%                      ;139.21
                                ; LOE eax edx ecx xmm0
.B1.110:                        ; Preds .B1.109
        mov       edi, eax                                      ;139.21
        imul      esi, edx, 152                                 ;139.21
        imul      edi, DWORD PTR [-248+ebp]                     ;139.21
        imul      eax, DWORD PTR [-260+ebp]                     ;139.21
        mov       edx, DWORD PTR [-536+ebp]                     ;139.21
        add       edi, DWORD PTR [-544+ebp]                     ;139.21
        add       eax, DWORD PTR [-524+ebp]                     ;139.21
        mov       edx, DWORD PTR [12+edx+esi]                   ;139.21
        mov       esi, DWORD PTR [edi+edx*8]                    ;139.21
        mov       DWORD PTR [4+eax+ecx*4], esi                  ;139.21
                                ; LOE ecx xmm0
.B1.111:                        ; Preds .B1.104 .B1.109 .B1.110
        lea       eax, DWORD PTR [2+ecx]                        ;140.17
        inc       ecx                                           ;139.21
        cmp       ecx, DWORD PTR [-472+ebp]                     ;139.21
        jb        .B1.104       ; Prob 81%                      ;139.21
                                ; LOE eax ecx xmm0
.B1.112:                        ; Preds .B1.111
        mov       DWORD PTR [-408+ebp], eax                     ;
                                ; LOE
.B1.113:                        ; Preds .B1.112 .B1.362 .B1.92
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_UETRAVELTIME+32] ;153.13
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELTIME+24] ;153.13
        test      edx, edx                                      ;153.13
        mov       DWORD PTR [-572+ebp], eax                     ;153.13
        jle       .B1.134       ; Prob 0%                       ;153.13
                                ; LOE edx
.B1.114:                        ; Preds .B1.113
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELTIME+40] ;153.13
        cmp       edx, 4                                        ;153.13
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELTIME] ;153.13
        mov       DWORD PTR [-664+ebp], eax                     ;153.13
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_UETRAVELTIME] ;153.13
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_UETRAVELTIME+44] ;153.13
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_UETRAVELTIME+40] ;153.13
        mov       DWORD PTR [-660+ebp], ecx                     ;153.13
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELTIME+44] ;153.13
        mov       DWORD PTR [-576+ebp], esi                     ;153.13
        mov       DWORD PTR [-600+ebp], edi                     ;153.13
        mov       DWORD PTR [-580+ebp], eax                     ;153.13
        jl        .B1.299       ; Prob 10%                      ;153.13
                                ; LOE edx ecx
.B1.115:                        ; Preds .B1.114
        mov       esi, DWORD PTR [-612+ebp]                     ;153.13
        sub       esi, ecx                                      ;153.13
        imul      esi, DWORD PTR [-664+ebp]                     ;153.13
        add       esi, DWORD PTR [-660+ebp]                     ;153.13
        mov       DWORD PTR [-656+ebp], esi                     ;153.13
        and       esi, 15                                       ;153.13
        je        .B1.118       ; Prob 50%                      ;153.13
                                ; LOE edx ecx esi
.B1.116:                        ; Preds .B1.115
        test      esi, 3                                        ;153.13
        jne       .B1.299       ; Prob 10%                      ;153.13
                                ; LOE edx ecx esi
.B1.117:                        ; Preds .B1.116
        neg       esi                                           ;153.13
        add       esi, 16                                       ;153.13
        shr       esi, 2                                        ;153.13
                                ; LOE edx ecx esi
.B1.118:                        ; Preds .B1.117 .B1.115
        lea       eax, DWORD PTR [4+esi]                        ;153.13
        cmp       edx, eax                                      ;153.13
        jl        .B1.299       ; Prob 10%                      ;153.13
                                ; LOE edx ecx esi
.B1.119:                        ; Preds .B1.118
        mov       DWORD PTR [-608+ebp], ecx                     ;
        mov       ecx, edx                                      ;153.13
        sub       ecx, esi                                      ;153.13
        and       ecx, 3                                        ;153.13
        neg       ecx                                           ;153.13
        mov       DWORD PTR [-564+ebp], esi                     ;
        add       ecx, edx                                      ;153.13
        mov       DWORD PTR [-604+ebp], edx                     ;
        mov       edi, DWORD PTR [-572+ebp]                     ;
        mov       esi, DWORD PTR [-628+ebp]                     ;130.13
        mov       edx, DWORD PTR [-580+ebp]                     ;130.13
        imul      esi, edx                                      ;130.13
        lea       eax, DWORD PTR [edi*4]                        ;
        imul      edx, DWORD PTR [-600+ebp]                     ;
        neg       eax                                           ;
        add       eax, DWORD PTR [-576+ebp]                     ;
        mov       DWORD PTR [-596+ebp], ecx                     ;153.13
        lea       ecx, DWORD PTR [esi+edi*4]                    ;
        add       ecx, eax                                      ;
        mov       DWORD PTR [-588+ebp], esi                     ;130.13
        sub       ecx, edx                                      ;
        mov       esi, DWORD PTR [-564+ebp]                     ;153.13
        test      esi, esi                                      ;153.13
        mov       DWORD PTR [-592+ebp], edx                     ;
        mov       DWORD PTR [-568+ebp], ecx                     ;
        mov       DWORD PTR [-584+ebp], eax                     ;
        mov       ecx, DWORD PTR [-608+ebp]                     ;153.13
        mov       edx, DWORD PTR [-604+ebp]                     ;153.13
        jbe       .B1.123       ; Prob 10%                      ;153.13
                                ; LOE edx ecx esi dl cl dh ch
.B1.120:                        ; Preds .B1.119
        xor       eax, eax                                      ;
        mov       DWORD PTR [-608+ebp], ecx                     ;
        mov       DWORD PTR [-604+ebp], edx                     ;
        movss     xmm1, DWORD PTR [_2il0floatpacket.21]         ;
        mov       edx, eax                                      ;
        mov       ecx, DWORD PTR [-568+ebp]                     ;
        mov       edi, DWORD PTR [-656+ebp]                     ;
                                ; LOE edx ecx esi edi xmm1
.B1.121:                        ; Preds .B1.121 .B1.120
        movss     xmm0, DWORD PTR [ecx+edx*4]                   ;153.13
        mulss     xmm0, xmm1                                    ;153.61
        cvttss2si eax, xmm0                                     ;153.13
        mov       DWORD PTR [edi+edx*4], eax                    ;153.13
        inc       edx                                           ;153.13
        cmp       edx, esi                                      ;153.13
        jb        .B1.121       ; Prob 82%                      ;153.13
                                ; LOE edx ecx esi edi xmm1
.B1.122:                        ; Preds .B1.121
        mov       ecx, DWORD PTR [-608+ebp]                     ;
        mov       edx, DWORD PTR [-604+ebp]                     ;
                                ; LOE edx ecx esi dl cl dh ch
.B1.123:                        ; Preds .B1.119 .B1.122
        mov       edi, DWORD PTR [-588+ebp]                     ;153.13
        sub       edi, DWORD PTR [-592+ebp]                     ;153.13
        mov       eax, DWORD PTR [-584+ebp]                     ;153.13
        add       eax, edi                                      ;153.13
        mov       edi, DWORD PTR [-572+ebp]                     ;153.13
        add       edi, esi                                      ;153.13
        lea       eax, DWORD PTR [eax+edi*4]                    ;153.13
        test      al, 15                                        ;153.13
        je        .B1.127       ; Prob 60%                      ;153.13
                                ; LOE edx ecx esi dl cl dh ch
.B1.124:                        ; Preds .B1.123
        mov       DWORD PTR [-604+ebp], edx                     ;
        movaps    xmm2, XMMWORD PTR [_2il0floatpacket.22]       ;
        mov       eax, DWORD PTR [-596+ebp]                     ;
        mov       edx, DWORD PTR [-568+ebp]                     ;
        mov       edi, DWORD PTR [-656+ebp]                     ;
                                ; LOE eax edx ecx esi edi xmm2
.B1.125:                        ; Preds .B1.125 .B1.124
        movups    xmm0, XMMWORD PTR [edx+esi*4]                 ;153.13
        mulps     xmm0, xmm2                                    ;153.61
        cvttps2dq xmm1, xmm0                                    ;153.13
        movdqa    XMMWORD PTR [edi+esi*4], xmm1                 ;153.13
        add       esi, 4                                        ;153.13
        cmp       esi, eax                                      ;153.13
        jb        .B1.125       ; Prob 82%                      ;153.13
        jmp       .B1.129       ; Prob 100%                     ;153.13
                                ; LOE eax edx ecx esi edi xmm2
.B1.127:                        ; Preds .B1.123
        mov       DWORD PTR [-604+ebp], edx                     ;
        movaps    xmm2, XMMWORD PTR [_2il0floatpacket.22]       ;
        mov       eax, DWORD PTR [-596+ebp]                     ;
        mov       edx, DWORD PTR [-568+ebp]                     ;
        mov       edi, DWORD PTR [-656+ebp]                     ;
                                ; LOE eax edx ecx esi edi xmm2
.B1.128:                        ; Preds .B1.128 .B1.127
        movaps    xmm0, XMMWORD PTR [edx+esi*4]                 ;153.13
        mulps     xmm0, xmm2                                    ;153.61
        cvttps2dq xmm1, xmm0                                    ;153.13
        movdqa    XMMWORD PTR [edi+esi*4], xmm1                 ;153.13
        add       esi, 4                                        ;153.13
        cmp       esi, eax                                      ;153.13
        jb        .B1.128       ; Prob 82%                      ;153.13
                                ; LOE eax edx ecx esi edi xmm2
.B1.129:                        ; Preds .B1.125 .B1.128
        mov       DWORD PTR [-596+ebp], eax                     ;
        mov       edx, DWORD PTR [-604+ebp]                     ;
                                ; LOE edx ecx
.B1.130:                        ; Preds .B1.129 .B1.299
        cmp       edx, DWORD PTR [-596+ebp]                     ;153.13
        jbe       .B1.134       ; Prob 10%                      ;153.13
                                ; LOE edx ecx
.B1.131:                        ; Preds .B1.130
        mov       eax, DWORD PTR [-600+ebp]                     ;
        neg       ecx                                           ;
        neg       eax                                           ;
        add       eax, DWORD PTR [-628+ebp]                     ;
        imul      eax, DWORD PTR [-580+ebp]                     ;
        movss     xmm1, DWORD PTR [_2il0floatpacket.21]         ;
        add       ecx, DWORD PTR [-612+ebp]                     ;
        imul      ecx, DWORD PTR [-664+ebp]                     ;
        add       eax, DWORD PTR [-576+ebp]                     ;
        mov       DWORD PTR [-600+ebp], eax                     ;
        add       ecx, DWORD PTR [-660+ebp]                     ;
        mov       esi, eax                                      ;
        mov       edi, DWORD PTR [-596+ebp]                     ;
                                ; LOE edx ecx esi edi xmm1
.B1.132:                        ; Preds .B1.132 .B1.131
        movss     xmm0, DWORD PTR [esi+edi*4]                   ;153.13
        mulss     xmm0, xmm1                                    ;153.61
        cvttss2si eax, xmm0                                     ;153.13
        mov       DWORD PTR [ecx+edi*4], eax                    ;153.13
        inc       edi                                           ;153.13
        cmp       edi, edx                                      ;153.13
        jb        .B1.132       ; Prob 82%                      ;153.13
                                ; LOE edx ecx esi edi xmm1
.B1.134:                        ; Preds .B1.132 .B1.113 .B1.130
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+36] ;154.13
        test      edx, edx                                      ;154.13
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_UETRAVELPENALTY+56] ;154.13
        mov       DWORD PTR [-240+ebp], edx                     ;154.13
        jle       .B1.136       ; Prob 10%                      ;154.13
                                ; LOE eax
.B1.135:                        ; Preds .B1.134
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE] ;158.17
        mov       DWORD PTR [-224+ebp], edi                     ;158.17
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+40] ;158.17
        mov       DWORD PTR [-228+ebp], edi                     ;158.17
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_UETRAVELPENALTY] ;154.13
        mov       DWORD PTR [-216+ebp], edi                     ;154.13
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_UETRAVELPENALTY+52] ;154.13
        mov       DWORD PTR [-232+ebp], edi                     ;154.13
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_UETRAVELPENALTY+44] ;154.13
        mov       DWORD PTR [-652+ebp], edi                     ;154.13
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+24] ;154.13
        test      esi, esi                                      ;154.13
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_UETRAVELPENALTY+40] ;154.13
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_UETRAVELPENALTY+32] ;154.13
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+32] ;154.13
        mov       DWORD PTR [-196+ebp], esi                     ;154.13
        mov       DWORD PTR [-648+ebp], edi                     ;154.13
        jg        .B1.269       ; Prob 50%                      ;154.13
                                ; LOE eax edx ecx
.B1.136:                        ; Preds .B1.134 .B1.356 .B1.275 .B1.135
        cmp       DWORD PTR [-472+ebp], 0                       ;156.25
        jle       .B1.144       ; Prob 16%                      ;156.25
                                ; LOE
.B1.137:                        ; Preds .B1.136
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_INCIL+32] ;157.61
        shl       edx, 2                                        ;157.17
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_INCIL] ;157.61
        neg       edx                                           ;157.17
        mov       esi, DWORD PTR [-752+ebp]                     ;157.49
        mov       edi, DWORD PTR [-408+ebp]                     ;157.17
        shl       esi, 2                                        ;157.17
        mov       ecx, DWORD PTR [-784+ebp]                     ;157.17
        neg       esi                                           ;157.17
        lea       eax, DWORD PTR [eax+edi*4]                    ;157.17
        mov       eax, DWORD PTR [edx+eax]                      ;157.17
        mov       DWORD PTR [-704+ebp], eax                     ;157.17
        lea       edx, DWORD PTR [ecx+edi*4]                    ;157.17
        movss     xmm0, DWORD PTR [esi+edx]                     ;157.49
        mulss     xmm0, DWORD PTR [_2il0floatpacket.21]         ;157.61
        mov       esi, DWORD PTR [-612+ebp]                     ;157.17
        sub       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELTIME+44] ;157.17
        imul      esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELTIME+40] ;157.17
        cvttss2si ecx, xmm0                                     ;157.17
        mov       edx, DWORD PTR [-732+ebp]                     ;157.17
        neg       edx                                           ;157.17
        add       edx, eax                                      ;157.17
        add       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELTIME] ;157.17
        mov       edi, DWORD PTR [-828+ebp]                     ;158.17
        mov       DWORD PTR [-708+ebp], edi                     ;158.17
        mov       DWORD PTR [esi+edx*4], ecx                    ;157.17
        cmp       DWORD PTR [-240+ebp], 0                       ;158.17
        jle       .B1.144       ; Prob 50%                      ;158.17
                                ; LOE
.B1.138:                        ; Preds .B1.137
        mov       edx, DWORD PTR [-840+ebp]                     ;158.17
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;158.17
        mov       DWORD PTR [-716+ebp], edx                     ;158.17
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+40] ;158.17
        mov       DWORD PTR [-720+ebp], esi                     ;158.17
        mov       esi, DWORD PTR [-240+ebp]                     ;158.17
        mov       DWORD PTR [-384+ebp], edx                     ;158.17
        mov       edx, esi                                      ;158.17
        shr       edx, 31                                       ;158.17
        mov       ecx, DWORD PTR [-832+ebp]                     ;158.17
        add       edx, esi                                      ;158.17
        mov       eax, DWORD PTR [-872+ebp]                     ;158.17
        mov       DWORD PTR [-388+ebp], ecx                     ;158.17
        sar       edx, 1                                        ;158.17
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE] ;158.17
        test      edx, edx                                      ;158.17
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;158.17
        mov       DWORD PTR [-712+ebp], eax                     ;158.17
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+32] ;158.17
        mov       DWORD PTR [-724+ebp], edi                     ;158.17
        mov       DWORD PTR [-728+ebp], ecx                     ;158.17
        mov       DWORD PTR [-372+ebp], edx                     ;158.17
        jbe       .B1.300       ; Prob 10%                      ;158.17
                                ; LOE eax
.B1.139:                        ; Preds .B1.138
        mov       ecx, DWORD PTR [-408+ebp]                     ;
        xor       edi, edi                                      ;
        mov       edx, DWORD PTR [-712+ebp]                     ;
        mov       DWORD PTR [-1016+ebp], eax                    ;
        mov       DWORD PTR [-744+ebp], edi                     ;
        mov       edi, DWORD PTR [-720+ebp]                     ;158.17
        lea       esi, DWORD PTR [edx+ecx*4]                    ;
        mov       DWORD PTR [-740+ebp], esi                     ;
        lea       edx, DWORD PTR [eax*8]                        ;
        mov       eax, DWORD PTR [-704+ebp]                     ;158.17
        neg       edx                                           ;
        sub       eax, DWORD PTR [-728+ebp]                     ;158.17
        imul      eax, eax, 152                                 ;158.17
        mov       ecx, DWORD PTR [-700+ebp]                     ;
        mov       esi, DWORD PTR [-384+ebp]                     ;
        imul      ecx, esi                                      ;
        add       edx, DWORD PTR [-724+ebp]                     ;
        mov       eax, DWORD PTR [12+edi+eax]                   ;158.17
        lea       edi, DWORD PTR [edx+eax*8]                    ;
        sub       edx, ecx                                      ;
        add       ecx, esi                                      ;
        add       edx, ecx                                      ;
        mov       DWORD PTR [-376+ebp], edi                     ;
        mov       edi, DWORD PTR [-716+ebp]                     ;
        mov       ecx, DWORD PTR [-388+ebp]                     ;
        lea       edx, DWORD PTR [edx+eax*8]                    ;
        mov       DWORD PTR [-368+ebp], edx                     ;
        mov       edx, DWORD PTR [-708+ebp]                     ;
        lea       esi, DWORD PTR [edi*4]                        ;
        imul      edx, ecx                                      ;
        neg       esi                                           ;
        mov       eax, DWORD PTR [-740+ebp]                     ;
        add       esi, eax                                      ;
        add       esi, ecx                                      ;
        lea       ecx, DWORD PTR [edx+edi*4]                    ;
        sub       eax, ecx                                      ;
        add       edx, eax                                      ;
        mov       DWORD PTR [-364+ebp], esi                     ;
        mov       esi, DWORD PTR [-744+ebp]                     ;
        mov       ecx, esi                                      ;
        mov       DWORD PTR [-380+ebp], edx                     ;
        mov       eax, esi                                      ;
                                ; LOE eax ecx esi
.B1.140:                        ; Preds .B1.140 .B1.139
        mov       edx, DWORD PTR [-380+ebp]                     ;158.17
        inc       eax                                           ;158.17
        mov       edi, DWORD PTR [edx+ecx*2]                    ;158.17
        mov       edx, DWORD PTR [-376+ebp]                     ;158.17
        mov       DWORD PTR [edx+esi*2], edi                    ;158.17
        mov       edi, DWORD PTR [-364+ebp]                     ;158.17
        mov       edx, DWORD PTR [edi+ecx*2]                    ;158.17
        mov       edi, DWORD PTR [-368+ebp]                     ;158.17
        add       ecx, DWORD PTR [-388+ebp]                     ;158.17
        mov       DWORD PTR [edi+esi*2], edx                    ;158.17
        add       esi, DWORD PTR [-384+ebp]                     ;158.17
        cmp       eax, DWORD PTR [-372+ebp]                     ;158.17
        jb        .B1.140       ; Prob 63%                      ;158.17
                                ; LOE eax ecx esi
.B1.141:                        ; Preds .B1.140
        mov       DWORD PTR [-744+ebp], eax                     ;
        mov       edx, eax                                      ;158.17
        mov       eax, DWORD PTR [-1016+ebp]                    ;
        lea       esi, DWORD PTR [1+edx+edx]                    ;158.17
                                ; LOE eax esi
.B1.142:                        ; Preds .B1.141 .B1.300
        lea       edx, DWORD PTR [-1+esi]                       ;158.17
        cmp       edx, DWORD PTR [-240+ebp]                     ;158.17
        jae       .B1.144       ; Prob 10%                      ;158.17
                                ; LOE eax esi
.B1.143:                        ; Preds .B1.142
        mov       edi, DWORD PTR [-708+ebp]                     ;158.17
        neg       eax                                           ;158.17
        mov       ecx, edi                                      ;158.17
        neg       ecx                                           ;158.17
        lea       edx, DWORD PTR [-1+esi+edi]                   ;158.17
        mov       edi, DWORD PTR [-712+ebp]                     ;158.17
        add       ecx, edx                                      ;158.17
        mov       edx, DWORD PTR [-408+ebp]                     ;158.17
        imul      ecx, DWORD PTR [-388+ebp]                     ;158.17
        lea       edx, DWORD PTR [edi+edx*4]                    ;158.17
        mov       edi, DWORD PTR [-716+ebp]                     ;158.17
        shl       edi, 2                                        ;158.17
        sub       edx, edi                                      ;158.17
        mov       edi, DWORD PTR [-704+ebp]                     ;158.17
        sub       edi, DWORD PTR [-728+ebp]                     ;158.17
        imul      edi, edi, 152                                 ;158.17
        mov       DWORD PTR [-736+ebp], edx                     ;158.17
        mov       edx, DWORD PTR [-720+ebp]                     ;158.17
        add       eax, DWORD PTR [12+edx+edi]                   ;158.17
        mov       edi, DWORD PTR [-700+ebp]                     ;158.17
        mov       edx, edi                                      ;158.17
        neg       edx                                           ;158.17
        lea       esi, DWORD PTR [-1+esi+edi]                   ;158.17
        add       edx, esi                                      ;158.17
        imul      edx, DWORD PTR [-384+ebp]                     ;158.17
        mov       esi, DWORD PTR [-736+ebp]                     ;158.17
        add       edx, DWORD PTR [-724+ebp]                     ;158.17
        mov       ecx, DWORD PTR [esi+ecx]                      ;158.17
        mov       DWORD PTR [edx+eax*8], ecx                    ;158.17
                                ; LOE
.B1.144:                        ; Preds .B1.143 .B1.142 .B1.137 .B1.136 .B1.90
                                ;       .B1.88
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_LOGOUT], 0  ;167.15
        jle       .B1.146       ; Prob 16%                      ;167.15
                                ; LOE
.B1.145:                        ; Preds .B1.144
        push      32                                            ;167.20
        mov       DWORD PTR [-440+ebp], 0                       ;167.20
        lea       eax, DWORD PTR [-336+ebp]                     ;167.20
        push      eax                                           ;167.20
        push      OFFSET FLAT: __STRLITPACK_59.0.1              ;167.20
        push      -2088435968                                   ;167.20
        push      711                                           ;167.20
        mov       DWORD PTR [-336+ebp], 39                      ;167.20
        lea       edx, DWORD PTR [-440+ebp]                     ;167.20
        push      edx                                           ;167.20
        mov       DWORD PTR [-332+ebp], OFFSET FLAT: __STRLITPACK_34 ;167.20
        call      _for_write_seq_lis                            ;167.20
                                ; LOE
.B1.406:                        ; Preds .B1.145
        add       esp, 24                                       ;167.20
                                ; LOE
.B1.146:                        ; Preds .B1.406 .B1.144
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_RUNMODE] ;169.5
        mov       eax, esi                                      ;169.22
        and       eax, -3                                       ;169.22
        cmp       eax, 1                                        ;169.22
        je        .B1.350       ; Prob 16%                      ;169.22
                                ; LOE esi
.B1.147:                        ; Preds .B1.146
        cmp       esi, 4                                        ;169.50
        je        .B1.350       ; Prob 16%                      ;169.50
                                ; LOE esi
.B1.148:                        ; Preds .B1.147 .B1.354 .B1.352 .B1.351 .B1.350
                                ;      
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ICALLKSP], 0 ;173.5
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_READHISTARR], 0 ;175.22
        jle       .B1.150       ; Prob 16%                      ;175.22
                                ; LOE esi
.B1.149:                        ; Preds .B1.148
        cmp       esi, 2                                        ;175.38
        je        .B1.349       ; Prob 16%                      ;175.38
                                ; LOE esi
.B1.150:                        ; Preds .B1.148 .B1.149
        mov       eax, esi                                      ;175.61
        and       eax, -3                                       ;175.61
        cmp       eax, 1                                        ;175.61
        je        .B1.348       ; Prob 16%                      ;175.61
                                ; LOE esi
.B1.151:                        ; Preds .B1.150
        cmp       esi, 4                                        ;175.89
        je        .B1.348       ; Prob 16%                      ;175.89
                                ; LOE esi
.B1.152:                        ; Preds .B1.151 .B1.348 .B1.349
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS_NUM], 0 ;176.19
        jle       .B1.346       ; Prob 16%                      ;176.19
                                ; LOE esi
.B1.153:                        ; Preds .B1.152 .B1.346
        mov       eax, DWORD PTR [-188+ebp]                     ;176.22
        dec       eax                                           ;176.48
        cdq                                                     ;176.43
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TDSPSTEP] ;176.48
        idiv      ecx                                           ;176.43
        test      edx, edx                                      ;176.61
        jne       .B1.169       ; Prob 50%                      ;176.61
                                ; LOE esi
.B1.154:                        ; Preds .B1.153
        test      BYTE PTR [_DYNUST_MAIN_MODULE_mp_CALLKSPREADVEH], 1 ;176.76
        jne       .B1.169       ; Prob 71%                      ;176.76
                                ; LOE esi
.B1.156:                        ; Preds .B1.349 .B1.154
        lea       eax, DWORD PTR [-188+ebp]                     ;177.14
        push      eax                                           ;177.14
        lea       edx, DWORD PTR [-404+ebp]                     ;177.14
        push      edx                                           ;177.14
        call      _TDSP_MAIN                                    ;177.14
                                ; LOE
.B1.407:                        ; Preds .B1.156
        add       esp, 8                                        ;177.14
                                ; LOE
.B1.157:                        ; Preds .B1.407
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOF_MASTER_DESTINATIONS] ;199.9
        test      eax, eax                                      ;199.9
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ITERATION] ;247.14
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_RUNMODE] ;247.2
        jle       .B1.170       ; Prob 2%                       ;199.9
                                ; LOE eax edx esi
.B1.158:                        ; Preds .B1.157
        mov       edi, 1                                        ;
        mov       DWORD PTR [-448+ebp], edx                     ;
        mov       ecx, edi                                      ;
        mov       DWORD PTR [-444+ebp], edi                     ;
        mov       DWORD PTR [-136+ebp], eax                     ;
        mov       DWORD PTR [-308+ebp], esi                     ;
                                ; LOE ecx
.B1.159:                        ; Preds .B1.167 .B1.158
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NZONES] ;201.13
        test      edx, edx                                      ;201.13
        jle       .B1.163       ; Prob 2%                       ;201.13
                                ; LOE edx ecx
.B1.160:                        ; Preds .B1.159
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_MASTERDEST+32] ;202.8
        mov       esi, 1                                        ;
        add       eax, eax                                      ;
        neg       eax                                           ;
        add       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_MASTERDEST] ;
                                ; LOE eax edx ecx esi
.B1.161:                        ; Preds .B1.162 .B1.160
        movsx     edi, WORD PTR [eax+esi*2]                     ;202.11
        cmp       edi, ecx                                      ;202.26
        je        .B1.314       ; Prob 20%                      ;202.26
                                ; LOE eax edx ecx esi
.B1.162:                        ; Preds .B1.161
        inc       esi                                           ;206.4
        cmp       esi, edx                                      ;206.4
        jle       .B1.161       ; Prob 82%                      ;206.4
                                ; LOE eax edx ecx esi
.B1.163:                        ; Preds .B1.159 .B1.162
        xor       eax, eax                                      ;
                                ; LOE eax ecx
.B1.164:                        ; Preds .B1.314 .B1.163
        test      al, 1                                         ;207.16
        je        .B1.167       ; Prob 60%                      ;207.16
                                ; LOE ecx
.B1.165:                        ; Preds .B1.164
        mov       eax, DWORD PTR [-308+ebp]                     ;231.34
        and       eax, -3                                       ;231.34
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ICALLKSP], -1 ;230.17
        cmp       eax, 1                                        ;231.34
        je        .B1.301       ; Prob 16%                      ;231.34
                                ; LOE ecx
.B1.166:                        ; Preds .B1.165
        cmp       DWORD PTR [-308+ebp], 4                       ;231.62
        je        .B1.301       ; Prob 16%                      ;231.62
                                ; LOE ecx
.B1.167:                        ; Preds .B1.166 .B1.312 .B1.304 .B1.303 .B1.302
                                ;       .B1.301 .B1.164
        inc       ecx                                           ;244.3
        cmp       ecx, DWORD PTR [-136+ebp]                     ;244.3
        jle       .B1.159       ; Prob 82%                      ;244.3
                                ; LOE ecx
.B1.168:                        ; Preds .B1.167
        mov       edx, DWORD PTR [-448+ebp]                     ;
        mov       esi, DWORD PTR [-308+ebp]                     ;
        jmp       .B1.170       ; Prob 100%                     ;
                                ; LOE edx esi
.B1.169:                        ; Preds .B1.346 .B1.154 .B1.153
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ITERATION] ;247.14
                                ; LOE edx esi
.B1.170:                        ; Preds .B1.157 .B1.168 .B1.169
        cmp       esi, 1                                        ;247.14
        je        .B1.182       ; Prob 16%                      ;247.14
                                ; LOE edx esi
.B1.171:                        ; Preds .B1.170
        test      edx, edx                                      ;247.34
        jne       .B1.182       ; Prob 50%                      ;247.34
                                ; LOE edx esi
.B1.172:                        ; Preds .B1.171
        test      esi, -3                                       ;248.21
        jne       .B1.180       ; Prob 50%                      ;248.21
                                ; LOE
.B1.173:                        ; Preds .B1.172
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+24] ;248.39
        test      edx, edx                                      ;248.39
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;248.39
        jle       .B1.180       ; Prob 50%                      ;248.39
                                ; LOE eax edx
.B1.174:                        ; Preds .B1.173
        mov       esi, edx                                      ;248.39
        shr       esi, 31                                       ;248.39
        add       esi, edx                                      ;248.39
        sar       esi, 1                                        ;248.39
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;248.39
        test      esi, esi                                      ;248.39
        jbe       .B1.316       ; Prob 10%                      ;248.39
                                ; LOE eax edx ecx esi
.B1.175:                        ; Preds .B1.174
        xor       edi, edi                                      ;
        mov       DWORD PTR [-556+ebp], eax                     ;
        mov       DWORD PTR [-560+ebp], edx                     ;
        xor       edx, edx                                      ;
        mov       eax, edx                                      ;
                                ; LOE eax edx ecx esi edi
.B1.176:                        ; Preds .B1.176 .B1.175
        inc       edi                                           ;248.39
        mov       WORD PTR [684+eax+ecx], dx                    ;248.39
        mov       WORD PTR [1584+eax+ecx], dx                   ;248.39
        add       eax, 1800                                     ;248.39
        cmp       edi, esi                                      ;248.39
        jb        .B1.176       ; Prob 64%                      ;248.39
                                ; LOE eax edx ecx esi edi
.B1.177:                        ; Preds .B1.176
        mov       edx, DWORD PTR [-560+ebp]                     ;
        lea       esi, DWORD PTR [1+edi+edi]                    ;248.39
        mov       eax, DWORD PTR [-556+ebp]                     ;
                                ; LOE eax edx ecx esi
.B1.178:                        ; Preds .B1.177 .B1.316
        lea       edi, DWORD PTR [-1+esi]                       ;248.39
        cmp       edx, edi                                      ;248.39
        jbe       .B1.180       ; Prob 10%                      ;248.39
                                ; LOE eax ecx esi
.B1.179:                        ; Preds .B1.178
        mov       edx, eax                                      ;248.39
        add       esi, eax                                      ;248.39
        neg       edx                                           ;248.39
        xor       eax, eax                                      ;248.39
        add       edx, esi                                      ;248.39
        imul      esi, edx, 900                                 ;248.39
        mov       WORD PTR [-216+ecx+esi], ax                   ;248.39
                                ; LOE
.B1.180:                        ; Preds .B1.173 .B1.178 .B1.172 .B1.179
        mov       eax, DWORD PTR [-188+ebp]                     ;249.6
        dec       eax                                           ;249.14
        cdq                                                     ;249.9
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TDSPSTEP] ;249.14
        idiv      ecx                                           ;249.9
        test      edx, edx                                      ;249.27
        jne       .B1.200       ; Prob 78%                      ;249.27
                                ; LOE
.B1.181:                        ; Preds .B1.180
        lea       eax, DWORD PTR [-188+ebp]                     ;250.15
        push      eax                                           ;250.15
        lea       edx, DWORD PTR [-180+ebp]                     ;250.15
        push      edx                                           ;250.15
        call      _READ_VEHICLES                                ;250.15
                                ; LOE
.B1.408:                        ; Preds .B1.181
        add       esp, 8                                        ;250.15
        jmp       .B1.200       ; Prob 100%                     ;250.15
                                ; LOE
.B1.182:                        ; Preds .B1.170 .B1.171
        test      edx, edx                                      ;252.19
        jle       .B1.200       ; Prob 16%                      ;252.19
                                ; LOE
.B1.183:                        ; Preds .B1.182
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+24] ;253.6
        test      eax, eax                                      ;253.6
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;253.6
        jle       .B1.190       ; Prob 50%                      ;253.6
                                ; LOE eax ecx
.B1.184:                        ; Preds .B1.183
        mov       esi, eax                                      ;253.6
        shr       esi, 31                                       ;253.6
        add       esi, eax                                      ;253.6
        sar       esi, 1                                        ;253.6
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;253.6
        test      esi, esi                                      ;253.6
        jbe       .B1.317       ; Prob 10%                      ;253.6
                                ; LOE eax edx ecx esi
.B1.185:                        ; Preds .B1.184
        xor       edi, edi                                      ;
        mov       DWORD PTR [-328+ebp], eax                     ;
        mov       DWORD PTR [-324+ebp], ecx                     ;
        xor       ecx, ecx                                      ;
        mov       eax, ecx                                      ;
                                ; LOE eax edx ecx esi edi
.B1.186:                        ; Preds .B1.186 .B1.185
        inc       edi                                           ;253.6
        mov       WORD PTR [684+eax+edx], cx                    ;253.6
        mov       WORD PTR [1584+eax+edx], cx                   ;253.6
        add       eax, 1800                                     ;253.6
        cmp       edi, esi                                      ;253.6
        jb        .B1.186       ; Prob 64%                      ;253.6
                                ; LOE eax edx ecx esi edi
.B1.187:                        ; Preds .B1.186
        mov       eax, DWORD PTR [-328+ebp]                     ;
        lea       esi, DWORD PTR [1+edi+edi]                    ;253.6
        mov       ecx, DWORD PTR [-324+ebp]                     ;
                                ; LOE eax edx ecx esi
.B1.188:                        ; Preds .B1.187 .B1.317
        lea       edi, DWORD PTR [-1+esi]                       ;253.6
        cmp       eax, edi                                      ;253.6
        jbe       .B1.190       ; Prob 10%                      ;253.6
                                ; LOE edx ecx esi
.B1.189:                        ; Preds .B1.188
        mov       eax, ecx                                      ;253.6
        add       esi, ecx                                      ;253.6
        neg       eax                                           ;253.6
        add       eax, esi                                      ;253.6
        xor       esi, esi                                      ;253.6
        imul      edi, eax, 900                                 ;253.6
        mov       WORD PTR [-216+edx+edi], si                   ;253.6
                                ; LOE ecx
.B1.190:                        ; Preds .B1.183 .B1.188 .B1.189
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_JTOTAL] ;255.9
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_JRESTORE] ;255.9
        cmp       eax, edi                                      ;255.9
        mov       DWORD PTR [-148+ebp], eax                     ;255.9
        jl        .B1.199       ; Prob 10%                      ;255.9
                                ; LOE ecx edi
.B1.191:                        ; Preds .B1.190
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;256.13
        shl       eax, 5                                        ;
        neg       eax                                           ;
        add       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;
        mov       DWORD PTR [-140+ebp], eax                     ;
        imul      eax, ecx, -900                                ;
        movss     xmm2, DWORD PTR [-180+ebp]                    ;256.20
        movss     xmm1, DWORD PTR [_DYNUST_MAIN_MODULE_mp_STARTTM] ;259.16
        movss     xmm0, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ENDTM] ;259.61
        movss     xmm5, DWORD PTR [_2il0floatpacket.20]         ;
        mov       esi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE+32] ;258.13
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;260.17
        shl       esi, 6                                        ;
        shl       edx, 8                                        ;
        neg       esi                                           ;
        add       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;
        neg       edx                                           ;
        add       esi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE] ;
        add       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;
        mov       DWORD PTR [-144+ebp], eax                     ;
                                ; LOE edx esi edi xmm0 xmm1 xmm2 xmm5
.B1.192:                        ; Preds .B1.197 .B1.191
        mov       eax, edi                                      ;256.59
        shl       eax, 5                                        ;256.59
        mov       ecx, DWORD PTR [-140+ebp]                     ;256.20
        movss     xmm4, DWORD PTR [12+ecx+eax]                  ;256.20
        movaps    xmm3, xmm4                                    ;256.49
        subss     xmm3, xmm2                                    ;256.49
        andps     xmm3, XMMWORD PTR [_2il0floatpacket.29]       ;256.16
        comiss    xmm3, xmm5                                    ;256.59
        ja        .B1.199       ; Prob 20%                      ;256.59
                                ; LOE eax edx ecx esi edi cl ch xmm0 xmm1 xmm2 xmm4 xmm5
.B1.193:                        ; Preds .B1.192
        imul      ecx, DWORD PTR [28+ecx+eax], 900              ;257.71
        mov       eax, DWORD PTR [-144+ebp]                     ;257.128
        inc       WORD PTR [684+eax+ecx]                        ;257.128
        mov       ecx, edi                                      ;258.13
        shl       ecx, 6                                        ;258.13
        mov       eax, 1                                        ;258.13
        comiss    xmm4, xmm1                                    ;259.46
        mov       WORD PTR [12+esi+ecx], ax                     ;258.13
        jb        .B1.196       ; Prob 50%                      ;259.46
                                ; LOE edx esi edi xmm0 xmm1 xmm2 xmm4 xmm5
.B1.194:                        ; Preds .B1.193
        comiss    xmm0, xmm4                                    ;259.91
        jbe       .B1.196       ; Prob 50%                      ;259.91
                                ; LOE edx esi edi xmm0 xmm1 xmm2 xmm5
.B1.195:                        ; Preds .B1.194
        mov       eax, edi                                      ;260.17
        shl       eax, 8                                        ;260.17
        mov       BYTE PTR [237+edx+eax], 1                     ;260.17
        jmp       .B1.197       ; Prob 100%                     ;260.17
                                ; LOE edx esi edi xmm0 xmm1 xmm2 xmm5
.B1.196:                        ; Preds .B1.193 .B1.194
        mov       eax, edi                                      ;263.14
        shl       eax, 8                                        ;263.14
        mov       BYTE PTR [237+edx+eax], 0                     ;263.14
                                ; LOE edx esi edi xmm0 xmm1 xmm2 xmm5
.B1.197:                        ; Preds .B1.196 .B1.195
        inc       edi                                           ;265.9
        cmp       edi, DWORD PTR [-148+ebp]                     ;265.9
        jle       .B1.192       ; Prob 82%                      ;265.9
                                ; LOE edx esi edi xmm0 xmm1 xmm2 xmm5
.B1.199:                        ; Preds .B1.192 .B1.197 .B1.190
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_JRESTORE], edi ;266.9
                                ; LOE
.B1.200:                        ; Preds .B1.408 .B1.180 .B1.182 .B1.199
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_VMS_NUM], 0 ;274.16
        jle       .B1.202       ; Prob 41%                      ;274.16
                                ; LOE
.B1.201:                        ; Preds .B1.200
        lea       eax, DWORD PTR [-180+ebp]                     ;274.21
        push      eax                                           ;274.21
        call      _VARIABLE_MESSAGE_SIGN_MODULE_mp_VMS_MAIN     ;274.21
                                ; LOE
.B1.409:                        ; Preds .B1.201
        add       esp, 4                                        ;274.21
                                ; LOE
.B1.202:                        ; Preds .B1.409 .B1.200
        push      OFFSET FLAT: _DYNUST_MAIN_MODULE_mp_ENDTIME   ;277.10
        lea       eax, DWORD PTR [-180+ebp]                     ;277.10
        push      eax                                           ;277.10
        lea       edx, DWORD PTR [-188+ebp]                     ;277.10
        push      edx                                           ;277.10
        call      _SIM_MASTER                                   ;277.10
                                ; LOE
.B1.410:                        ; Preds .B1.202
        add       esp, 12                                       ;277.10
                                ; LOE
.B1.203:                        ; Preds .B1.410
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ITERATION] ;281.5
        test      ecx, ecx                                      ;281.18
        jl        .B1.211       ; Prob 16%                      ;281.18
                                ; LOE ecx
.B1.204:                        ; Preds .B1.203
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_NUMCARS], 0 ;282.19
        jne       .B1.211       ; Prob 50%                      ;282.19
                                ; LOE ecx
.B1.205:                        ; Preds .B1.204
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_NUMNTAG], 0 ;282.36
        jne       .B1.211       ; Prob 50%                      ;282.36
                                ; LOE ecx
.B1.206:                        ; Preds .B1.205
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_RUNMODE] ;283.8
        mov       eax, edx                                      ;283.23
        and       eax, -3                                       ;283.23
        cmp       eax, 1                                        ;283.23
        je        .B1.325       ; Prob 16%                      ;283.23
                                ; LOE edx ecx
.B1.207:                        ; Preds .B1.206
        cmp       edx, 4                                        ;283.51
        je        .B1.325       ; Prob 16%                      ;283.51
                                ; LOE edx ecx
.B1.208:                        ; Preds .B1.207
        mov       esi, DWORD PTR [-188+ebp]                     ;367.7
                                ; LOE edx ecx esi
.B1.209:                        ; Preds .B1.326 .B1.208
        cmp       edx, 2                                        ;289.103
        je        .B1.318       ; Prob 16%                      ;289.103
        jmp       .B1.212       ; Prob 100%                     ;289.103
                                ; LOE ecx esi
.B1.211:                        ; Preds .B1.203 .B1.205 .B1.204
        mov       esi, DWORD PTR [-188+ebp]                     ;302.10
                                ; LOE ecx esi
.B1.212:                        ; Preds .B1.318 .B1.209 .B1.211
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DEC_NUM] ;300.8
        test      eax, eax                                      ;300.19
        mov       DWORD PTR [-128+ebp], eax                     ;300.8
        jle       .B1.222       ; Prob 16%                      ;300.19
                                ; LOE ecx esi
.B1.213:                        ; Preds .B1.212
        mov       eax, esi                                      ;302.13
        cdq                                                     ;302.13
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NRATE] ;302.10
        idiv      edi                                           ;302.13
        test      edx, edx                                      ;302.26
        jne       .B1.222       ; Prob 50%                      ;302.26
                                ; LOE ecx esi
.B1.214:                        ; Preds .B1.213
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_DLLFLAGRM], 0 ;303.24
        jne       .B1.217       ; Prob 78%                      ;303.24
                                ; LOE
.B1.215:                        ; Preds .B1.214
        lea       eax, DWORD PTR [-644+ebp]                     ;304.18
        push      eax                                           ;304.18
        call      _RAMP_METERING                                ;304.18
                                ; LOE
.B1.411:                        ; Preds .B1.215
        add       esp, 4                                        ;304.18
        jmp       .B1.221       ; Prob 100%                     ;304.18
                                ; LOE
.B1.217:                        ; Preds .B1.214
        mov       esi, 1                                        ;306.13
                                ; LOE esi
.B1.218:                        ; Preds .B1.217 .B1.219
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DETECTOR+40] ;307.54
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DETECTOR+44] ;307.54
        imul      eax, ecx                                      ;307.37
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DETECTOR] ;307.37
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DETECTOR+32] ;307.37
        sub       edi, eax                                      ;307.37
        shl       edx, 2                                        ;307.37
        lea       eax, DWORD PTR [ecx+ecx]                      ;307.37
        sub       eax, edx                                      ;307.37
        lea       ecx, DWORD PTR [ecx+ecx*2]                    ;307.37
        sub       ecx, edx                                      ;307.37
        add       eax, edi                                      ;307.37
        add       edi, ecx                                      ;307.37
        push      OFFSET FLAT: __NLITPACK_4.0.1                 ;307.37
        lea       eax, DWORD PTR [eax+esi*4]                    ;307.37
        lea       edx, DWORD PTR [edi+esi*4]                    ;307.37
        push      edx                                           ;307.37
        push      eax                                           ;307.37
        call      _DYNUST_NETWORK_MODULE_mp_GETFLINKFROMNODE    ;307.37
                                ; LOE eax esi
.B1.412:                        ; Preds .B1.218
        add       esp, 12                                       ;307.37
                                ; LOE eax esi
.B1.219:                        ; Preds .B1.412
        mov       edx, DWORD PTR [-132+ebp]                     ;307.17
        mov       DWORD PTR [-4+edx+esi*4], eax                 ;307.17
        inc       esi                                           ;308.13
        cmp       esi, DWORD PTR [-128+ebp]                     ;308.13
        jle       .B1.218       ; Prob 82%                      ;308.13
                                ; LOE esi
.B1.220:                        ; Preds .B1.219
        add       esp, -84                                      ;309.17
        lea       eax, DWORD PTR [-180+ebp]                     ;309.17
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DET_LINK] ;309.17
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DETECTOR_RAMP] ;309.17
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_RAMP_START] ;309.17
        mov       DWORD PTR [esp], OFFSET FLAT: _DYNUST_MAIN_MODULE_mp_DLLFLAGRM ;309.17
        mov       DWORD PTR [4+esp], OFFSET FLAT: _DYNUST_MAIN_MODULE_mp_NOOFFLOWMODEL ;309.17
        mov       DWORD PTR [8+esp], OFFSET FLAT: _DYNUST_MAIN_MODULE_mp_NOOFNODES ;309.17
        mov       DWORD PTR [12+esp], OFFSET FLAT: _DYNUST_MAIN_MODULE_mp_NOOFARCS ;309.17
        mov       DWORD PTR [16+esp], eax                       ;309.17
        mov       DWORD PTR [20+esp], OFFSET FLAT: _DYNUST_MAIN_MODULE_mp_DEC_NUM ;309.17
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_RAMP_END] ;309.17
        mov       DWORD PTR [24+esp], edx                       ;309.17
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_RAMP_PAR] ;309.17
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;309.17
        mov       DWORD PTR [28+esp], ecx                       ;309.17
        mov       DWORD PTR [32+esp], esi                       ;309.17
        mov       DWORD PTR [36+esp], edi                       ;309.17
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DETECTOR] ;309.17
        mov       DWORD PTR [40+esp], eax                       ;309.17
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_BACKPOINTR] ;309.17
        mov       DWORD PTR [44+esp], edx                       ;309.17
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC] ;309.17
        mov       DWORD PTR [48+esp], OFFSET FLAT: _DYNUST_MAIN_MODULE_mp_NRATE ;309.17
        mov       DWORD PTR [52+esp], OFFSET FLAT: _DYNUST_MAIN_MODULE_mp_XMINPERSIMINT ;309.17
        mov       DWORD PTR [56+esp], ecx                       ;309.17
        mov       DWORD PTR [60+esp], esi                       ;309.17
        mov       DWORD PTR [64+esp], edi                       ;309.17
        mov       eax, DWORD PTR [-132+ebp]                     ;309.17
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_OCCUP] ;309.17
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DETECTOR_LENGTH] ;309.17
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;309.17
        mov       DWORD PTR [68+esp], eax                       ;309.17
        mov       DWORD PTR [72+esp], edx                       ;309.17
        mov       DWORD PTR [76+esp], ecx                       ;309.17
        mov       DWORD PTR [80+esp], esi                       ;309.17
        call      _RAMP_METER_DLL                               ;309.17
                                ; LOE
.B1.413:                        ; Preds .B1.220
        add       esp, 84                                       ;309.17
                                ; LOE
.B1.221:                        ; Preds .B1.411 .B1.413
        mov       esi, DWORD PTR [-188+ebp]                     ;367.7
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ITERATION] ;319.7
                                ; LOE ecx esi
.B1.222:                        ; Preds .B1.221 .B1.213 .B1.212
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NUMCARS] ;315.7
        cmp       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_OLDNUMCARS] ;315.18
        jne       .B1.224       ; Prob 50%                      ;315.18
                                ; LOE edx ecx esi
.B1.223:                        ; Preds .B1.222
        mov       eax, DWORD PTR [-244+ebp]                     ;315.33
        inc       eax                                           ;315.33
        mov       DWORD PTR [-244+ebp], eax                     ;315.33
        mov       DWORD PTR [_SIM_MAIN_LOOP$ICOUNT_STOP.0.1], eax ;315.33
        jmp       .B1.225       ; Prob 100%                     ;315.33
                                ; LOE edx ecx esi
.B1.224:                        ; Preds .B1.222
        xor       eax, eax                                      ;
        mov       DWORD PTR [_SIM_MAIN_LOOP$ICOUNT_STOP.0.1], 0 ;316.33
        mov       DWORD PTR [-244+ebp], eax                     ;
                                ; LOE edx ecx esi
.B1.225:                        ; Preds .B1.223 .B1.224
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_OLDNUMCARS], edx ;317.7
        test      ecx, ecx                                      ;319.20
        jle       .B1.228       ; Prob 16%                      ;319.20
                                ; LOE ecx esi
.B1.226:                        ; Preds .B1.225
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_STOP_COUNT_THRESHOLD] ;320.7
        lea       edx, DWORD PTR [eax+eax*4]                    ;320.45
        add       edx, edx                                      ;320.45
        cmp       edx, DWORD PTR [-244+ebp]                     ;320.22
        jg        .B1.228       ; Prob 50%                      ;320.22
                                ; LOE eax ecx esi
.B1.227:                        ; Preds .B1.226
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NINTS] ;320.22
        sub       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_BEGINT+32] ;320.64
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_BEGINT] ;320.22
        movss     xmm0, DWORD PTR [-180+ebp]                    ;320.22
        comiss    xmm0, DWORD PTR [4+edx+edi*4]                 ;320.61
        jae       .B1.328       ; Prob 20%                      ;320.61
                                ; LOE eax ecx esi
.B1.228:                        ; Preds .B1.227 .B1.226 .B1.225
        mov       eax, esi                                      ;331.7
        cdq                                                     ;331.7
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TDSPSTEP] ;331.4
        idiv      edi                                           ;331.7
        test      edx, edx                                      ;331.23
        jne       .B1.245       ; Prob 50%                      ;331.23
                                ; LOE ecx esi
.B1.229:                        ; Preds .B1.228
        push      32                                            ;336.7
        push      OFFSET FLAT: SIM_MAIN_LOOP$format_pack.0.1+152 ;336.7
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_JUSTVEH] ;332.7
        lea       edx, DWORD PTR [-640+ebp]                     ;336.7
        push      edx                                           ;336.7
        push      OFFSET FLAT: __STRLITPACK_71.0.1              ;336.7
        mov       DWORD PTR [-632+ebp], ecx                     ;336.7
        lea       ecx, DWORD PTR [-440+ebp]                     ;336.7
        push      -2088435968                                   ;336.7
        sub       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_JUSTVEH_I] ;332.7
        push      ecx                                           ;336.7
        mov       DWORD PTR [-316+ebp], eax                     ;332.7
        mov       DWORD PTR [-440+ebp], 0                       ;336.7
        mov       DWORD PTR [-640+ebp], 80                      ;336.7
        mov       DWORD PTR [-636+ebp], OFFSET FLAT: _SIM_MAIN_LOOP$PRINTSTR1.0.1 ;336.7
        call      _for_write_int_fmt                            ;336.7
                                ; LOE
.B1.414:                        ; Preds .B1.229
        add       esp, 24                                       ;336.7
                                ; LOE
.B1.230:                        ; Preds .B1.414
        xor       edx, edx                                      ;338.32
        mov       edi, 80                                       ;338.32
        push      edx                                           ;338.7
        push      edi                                           ;338.7
        mov       eax, OFFSET FLAT: _SIM_MAIN_LOOP$PRINTSTR1.0.1 ;338.32
        lea       ecx, DWORD PTR [-696+ebp]                     ;338.7
        push      eax                                           ;338.7
        push      edx                                           ;338.7
        push      2                                             ;338.7
        push      ecx                                           ;338.7
        mov       DWORD PTR [-696+ebp], OFFSET FLAT: __STRLITPACK_11 ;338.32
        mov       DWORD PTR [-688+ebp], 11                      ;338.32
        mov       DWORD PTR [-684+ebp], edx                     ;338.32
        mov       DWORD PTR [-680+ebp], eax                     ;338.32
        mov       DWORD PTR [-672+ebp], edi                     ;338.32
        mov       DWORD PTR [-668+ebp], edx                     ;338.32
        call      _for_concat                                   ;338.7
                                ; LOE edi
.B1.231:                        ; Preds .B1.230
        add       esp, -12                                      ;339.12
        mov       eax, OFFSET FLAT: _SIM_MAIN_LOOP$PRINTSTR1.0.1 ;339.12
        mov       edx, OFFSET FLAT: __NLITPACK_5.0.1            ;339.12
        mov       ecx, OFFSET FLAT: __NLITPACK_1.0.1            ;339.12
        mov       DWORD PTR [12+esp], OFFSET FLAT: __NLITPACK_0.0.1 ;339.12
        mov       DWORD PTR [16+esp], OFFSET FLAT: __NLITPACK_6.0.1 ;339.12
        mov       DWORD PTR [20+esp], OFFSET FLAT: __NLITPACK_7.0.1 ;339.12
        mov       DWORD PTR [24+esp], OFFSET FLAT: __NLITPACK_8.0.1 ;339.12
        mov       DWORD PTR [28+esp], OFFSET FLAT: __NLITPACK_3.0.1 ;339.12
        mov       DWORD PTR [32+esp], 80                        ;339.12
        call      _PRINTSCREEN.                                 ;339.12
                                ; LOE edi
.B1.416:                        ; Preds .B1.231
        add       esp, 36                                       ;339.12
                                ; LOE edi
.B1.232:                        ; Preds .B1.416
        xor       eax, eax                                      ;341.7
        push      eax                                           ;341.7
        push      eax                                           ;341.7
        push      43                                            ;341.7
        push      OFFSET FLAT: __STRLITPACK_10                  ;341.7
        push      eax                                           ;341.7
        push      80                                            ;341.7
        push      OFFSET FLAT: _SIM_MAIN_LOOP$PRINTSTR1.0.1     ;341.7
        call      _for_cpystr                                   ;341.7
                                ; LOE edi
.B1.233:                        ; Preds .B1.232
        add       esp, -8                                       ;343.12
        mov       esi, OFFSET FLAT: __NLITPACK_10.0.1           ;343.12
        mov       edx, OFFSET FLAT: __NLITPACK_0.0.1            ;343.12
        mov       eax, OFFSET FLAT: _SIM_MAIN_LOOP$PRINTSTR1.0.1 ;343.12
        mov       ecx, OFFSET FLAT: __NLITPACK_9.0.1            ;343.12
        mov       DWORD PTR [12+esp], esi                       ;343.12
        mov       DWORD PTR [16+esp], edx                       ;343.12
        mov       DWORD PTR [20+esp], OFFSET FLAT: __NLITPACK_11.0.1 ;343.12
        mov       DWORD PTR [24+esp], OFFSET FLAT: __NLITPACK_8.0.1 ;343.12
        mov       DWORD PTR [28+esp], esi                       ;343.12
        mov       DWORD PTR [32+esp], 80                        ;343.12
        call      _PRINTSCREEN.                                 ;343.12
                                ; LOE edi
.B1.418:                        ; Preds .B1.233
        add       esp, 36                                       ;343.12
                                ; LOE edi
.B1.234:                        ; Preds .B1.418
        cvtsi2ss  xmm0, DWORD PTR [-188+ebp]                    ;345.33
        push      32                                            ;345.7
        push      OFFSET FLAT: SIM_MAIN_LOOP$format_pack.0.1+172 ;345.7
        mulss     xmm0, DWORD PTR [_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT] ;345.7
        lea       eax, DWORD PTR [-624+ebp]                     ;345.7
        push      eax                                           ;345.7
        push      OFFSET FLAT: __STRLITPACK_72.0.1              ;345.7
        push      -2088435968                                   ;345.7
        mov       DWORD PTR [-440+ebp], 0                       ;345.7
        lea       edx, DWORD PTR [-440+ebp]                     ;345.7
        push      edx                                           ;345.7
        mov       DWORD PTR [-624+ebp], 80                      ;345.7
        mov       DWORD PTR [-620+ebp], OFFSET FLAT: _SIM_MAIN_LOOP$PRINTSTR2.0.1 ;345.7
        movss     DWORD PTR [-616+ebp], xmm0                    ;345.7
        call      _for_write_int_fmt                            ;345.7
                                ; LOE edi
.B1.419:                        ; Preds .B1.234
        add       esp, 24                                       ;345.7
                                ; LOE edi
.B1.235:                        ; Preds .B1.419
        push      edi                                           ;346.19
        push      OFFSET FLAT: _SIM_MAIN_LOOP$PRINTSTR2.0.1     ;346.19
        push      edi                                           ;346.19
        lea       esi, DWORD PTR [-1000+ebp]                    ;346.19
        push      esi                                           ;346.19
        call      _for_trim                                     ;346.19
                                ; LOE eax esi
.B1.420:                        ; Preds .B1.235
        add       esp, 16                                       ;346.19
                                ; LOE eax esi
.B1.236:                        ; Preds .B1.420
        xor       ecx, ecx                                      ;346.7
        cdq                                                     ;346.7
        push      ecx                                           ;346.7
        push      edx                                           ;346.7
        push      eax                                           ;346.7
        push      esi                                           ;346.7
        push      ecx                                           ;346.7
        push      80                                            ;346.7
        push      OFFSET FLAT: _SIM_MAIN_LOOP$PRINTSTR2.0.1     ;346.7
        call      _for_cpystr                                   ;346.7
                                ; LOE
.B1.237:                        ; Preds .B1.236
        add       esp, -8                                       ;348.12
        mov       esi, OFFSET FLAT: __NLITPACK_3.0.1            ;348.12
        mov       eax, OFFSET FLAT: _SIM_MAIN_LOOP$PRINTSTR2.0.1 ;348.12
        mov       edx, OFFSET FLAT: __NLITPACK_12.0.1           ;348.12
        mov       ecx, OFFSET FLAT: __NLITPACK_13.0.1           ;348.12
        mov       DWORD PTR [12+esp], esi                       ;348.12
        mov       DWORD PTR [16+esp], esi                       ;348.12
        mov       DWORD PTR [20+esp], OFFSET FLAT: __NLITPACK_14.0.1 ;348.12
        mov       DWORD PTR [24+esp], OFFSET FLAT: __NLITPACK_6.0.1 ;348.12
        mov       DWORD PTR [28+esp], esi                       ;348.12
        mov       DWORD PTR [32+esp], 80                        ;348.12
        call      _PRINTSCREEN.                                 ;348.12
                                ; LOE
.B1.422:                        ; Preds .B1.237
        add       esp, 36                                       ;348.12
                                ; LOE
.B1.238:                        ; Preds .B1.422
        mov       esi, DWORD PTR [-188+ebp]                     ;349.23
        lea       eax, DWORD PTR [-304+ebp]                     ;349.7
        cvtsi2ss  xmm0, esi                                     ;349.23
        push      32                                            ;349.7
        push      OFFSET FLAT: SIM_MAIN_LOOP$format_pack.0.1    ;349.7
        push      eax                                           ;349.7
        push      OFFSET FLAT: __STRLITPACK_73.0.1              ;349.7
        mulss     xmm0, DWORD PTR [_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT] ;349.7
        lea       edx, DWORD PTR [-440+ebp]                     ;349.7
        push      -2088435968                                   ;349.7
        push      666                                           ;349.7
        push      edx                                           ;349.7
        mov       DWORD PTR [-440+ebp], 0                       ;349.7
        movss     DWORD PTR [-304+ebp], xmm0                    ;349.7
        call      _for_write_seq_fmt                            ;349.7
                                ; LOE esi
.B1.423:                        ; Preds .B1.238
        add       esp, 28                                       ;349.7
                                ; LOE esi
.B1.239:                        ; Preds .B1.423
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_JUSTVEH] ;349.7
        lea       edx, DWORD PTR [-296+ebp]                     ;349.7
        push      edx                                           ;349.7
        push      OFFSET FLAT: __STRLITPACK_74.0.1              ;349.7
        mov       DWORD PTR [-320+ebp], eax                     ;349.7
        lea       ecx, DWORD PTR [-440+ebp]                     ;349.7
        push      ecx                                           ;349.7
        mov       DWORD PTR [-296+ebp], eax                     ;349.7
        call      _for_write_seq_fmt_xmit                       ;349.7
                                ; LOE esi
.B1.424:                        ; Preds .B1.239
        add       esp, 12                                       ;349.7
                                ; LOE esi
.B1.240:                        ; Preds .B1.424
        mov       eax, DWORD PTR [-316+ebp]                     ;349.7
        lea       edx, DWORD PTR [-288+ebp]                     ;349.7
        push      edx                                           ;349.7
        push      OFFSET FLAT: __STRLITPACK_75.0.1              ;349.7
        mov       DWORD PTR [-288+ebp], eax                     ;349.7
        lea       ecx, DWORD PTR [-440+ebp]                     ;349.7
        push      ecx                                           ;349.7
        call      _for_write_seq_fmt_xmit                       ;349.7
                                ; LOE esi
.B1.425:                        ; Preds .B1.240
        add       esp, 12                                       ;349.7
                                ; LOE esi
.B1.241:                        ; Preds .B1.425
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOUT_NONTAG_I] ;349.7
        lea       edx, DWORD PTR [-280+ebp]                     ;349.7
        push      edx                                           ;349.7
        push      OFFSET FLAT: __STRLITPACK_76.0.1              ;349.7
        neg       eax                                           ;349.7
        lea       ecx, DWORD PTR [-440+ebp]                     ;349.7
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOUT_NONTAG] ;349.7
        add       eax, edi                                      ;349.7
        push      ecx                                           ;349.7
        mov       DWORD PTR [-280+ebp], eax                     ;349.7
        call      _for_write_seq_fmt_xmit                       ;349.7
                                ; LOE esi edi
.B1.426:                        ; Preds .B1.241
        add       esp, 12                                       ;349.7
                                ; LOE esi edi
.B1.242:                        ; Preds .B1.426
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOUT_TAG_I] ;349.7
        lea       ecx, DWORD PTR [-272+ebp]                     ;349.7
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOUT_TAG] ;349.7
        neg       edx                                           ;349.7
        push      ecx                                           ;349.7
        mov       DWORD PTR [-312+ebp], eax                     ;349.7
        add       edx, eax                                      ;349.7
        push      OFFSET FLAT: __STRLITPACK_77.0.1              ;349.7
        mov       DWORD PTR [-272+ebp], edx                     ;349.7
        lea       eax, DWORD PTR [-440+ebp]                     ;349.7
        push      eax                                           ;349.7
        call      _for_write_seq_fmt_xmit                       ;349.7
                                ; LOE esi edi
.B1.427:                        ; Preds .B1.242
        add       esp, 12                                       ;349.7
                                ; LOE esi edi
.B1.243:                        ; Preds .B1.427
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NUMCARS] ;349.7
        lea       edx, DWORD PTR [-264+ebp]                     ;349.7
        push      edx                                           ;349.7
        push      OFFSET FLAT: __STRLITPACK_78.0.1              ;349.7
        mov       DWORD PTR [-264+ebp], eax                     ;349.7
        lea       ecx, DWORD PTR [-440+ebp]                     ;349.7
        push      ecx                                           ;349.7
        call      _for_write_seq_fmt_xmit                       ;349.7
                                ; LOE esi edi
.B1.428:                        ; Preds .B1.243
        add       esp, 12                                       ;349.7
                                ; LOE esi edi
.B1.244:                        ; Preds .B1.428
        mov       eax, DWORD PTR [-320+ebp]                     ;352.10
        mov       edx, DWORD PTR [-312+ebp]                     ;354.10
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_JUSTVEH_I], eax ;352.10
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOUT_NONTAG_I], edi ;353.10
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOUT_TAG_I], edx ;354.10
                                ; LOE esi
.B1.245:                        ; Preds .B1.244 .B1.228
        inc       esi                                           ;59.10
        mov       DWORD PTR [-188+ebp], esi                     ;59.10
        cmp       esi, DWORD PTR [-204+ebp]                     ;59.10
        jle       .B1.5         ; Prob 82%                      ;59.10
                                ; LOE esi
.B1.246:                        ; Preds .B1.245
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ENDTIME] ;367.7
        lea       edi, DWORD PTR [-440+ebp]                     ;
                                ; LOE edx esi edi
.B1.247:                        ; Preds .B1.246 .B1.335 .B1.324 .B1.458
        mov       eax, DWORD PTR [8+ebx]                        ;1.12
        cmp       esi, edx                                      ;367.7
        push      32                                            ;369.7
        cmovl     edx, esi                                      ;367.7
        mov       DWORD PTR [eax], edx                          ;367.7
        lea       edx, DWORD PTR [-664+ebp]                     ;369.7
        push      edx                                           ;369.7
        push      OFFSET FLAT: __STRLITPACK_79.0.1              ;369.7
        push      -2088435968                                   ;369.7
        push      901                                           ;369.7
        push      edi                                           ;369.7
        mov       DWORD PTR [-440+ebp], 0                       ;369.7
        mov       DWORD PTR [-664+ebp], 17                      ;369.7
        mov       DWORD PTR [-660+ebp], OFFSET FLAT: __STRLITPACK_1 ;369.7
        mov       DWORD PTR [-656+ebp], 7                       ;369.7
        mov       DWORD PTR [-652+ebp], OFFSET FLAT: __STRLITPACK_0 ;369.7
        call      _for_open                                     ;369.7
                                ; LOE edi
.B1.429:                        ; Preds .B1.247
        add       esp, 24                                       ;369.7
                                ; LOE edi
.B1.248:                        ; Preds .B1.429
        mov       esi, DWORD PTR [-188+ebp]                     ;370.31
        lea       eax, DWORD PTR [-608+ebp]                     ;370.4
        cvtsi2ss  xmm2, esi                                     ;370.31
        movss     xmm1, DWORD PTR [_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT] ;370.31
        mulss     xmm2, xmm1                                    ;370.32
        movss     xmm0, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIMPERIOD] ;370.4
        push      32                                            ;370.4
        movaps    xmm3, xmm0                                    ;370.4
        push      eax                                           ;370.4
        push      OFFSET FLAT: __STRLITPACK_80.0.1              ;370.4
        push      -2088435968                                   ;370.4
        push      901                                           ;370.4
        push      edi                                           ;370.4
        mov       DWORD PTR [-440+ebp], 0                       ;370.4
        minss     xmm3, xmm2                                    ;370.4
        movss     DWORD PTR [-1016+ebp], xmm1                   ;370.31
        movss     DWORD PTR [-608+ebp], xmm3                    ;370.4
        movss     DWORD PTR [-628+ebp], xmm0                    ;370.4
        call      _for_write_seq_lis                            ;370.4
                                ; LOE esi edi
.B1.430:                        ; Preds .B1.248
        movss     xmm0, DWORD PTR [-628+ebp]                    ;
        add       esp, 24                                       ;370.4
                                ; LOE esi edi xmm0
.B1.249:                        ; Preds .B1.430
        divss     xmm0, DWORD PTR [-1016+ebp]                   ;370.53
        movss     xmm1, DWORD PTR [_2il0floatpacket.24]         ;370.53
        lea       edx, DWORD PTR [-600+ebp]                     ;370.4
        andps     xmm1, xmm0                                    ;370.53
        pxor      xmm0, xmm1                                    ;370.53
        movss     xmm2, DWORD PTR [_2il0floatpacket.25]         ;370.53
        movaps    xmm3, xmm0                                    ;370.53
        movaps    xmm7, xmm0                                    ;370.53
        cmpltss   xmm3, xmm2                                    ;370.53
        andps     xmm2, xmm3                                    ;370.53
        push      edx                                           ;370.4
        push      OFFSET FLAT: __STRLITPACK_81.0.1              ;370.4
        push      edi                                           ;370.4
        addss     xmm7, xmm2                                    ;370.53
        subss     xmm7, xmm2                                    ;370.53
        movaps    xmm6, xmm7                                    ;370.53
        subss     xmm6, xmm0                                    ;370.53
        movss     xmm0, DWORD PTR [_2il0floatpacket.26]         ;370.53
        movaps    xmm5, xmm0                                    ;370.53
        movaps    xmm4, xmm6                                    ;370.53
        addss     xmm5, xmm0                                    ;370.53
        cmpnless  xmm4, xmm0                                    ;370.53
        cmpless   xmm6, DWORD PTR [_2il0floatpacket.27]         ;370.53
        andps     xmm4, xmm5                                    ;370.53
        andps     xmm6, xmm5                                    ;370.53
        subss     xmm7, xmm4                                    ;370.53
        addss     xmm7, xmm6                                    ;370.53
        orps      xmm7, xmm1                                    ;370.53
        cvtss2si  eax, xmm7                                     ;370.53
        cmp       eax, esi                                      ;370.4
        cmovl     esi, eax                                      ;370.4
        mov       DWORD PTR [-600+ebp], esi                     ;370.4
        call      _for_write_seq_lis_xmit                       ;370.4
                                ; LOE edi
.B1.431:                        ; Preds .B1.249
        add       esp, 12                                       ;370.4
                                ; LOE edi
.B1.250:                        ; Preds .B1.431
        push      32                                            ;371.10
        xor       eax, eax                                      ;371.10
        push      eax                                           ;371.10
        push      OFFSET FLAT: __STRLITPACK_82.0.1              ;371.10
        push      -2088435968                                   ;371.10
        push      901                                           ;371.10
        push      edi                                           ;371.10
        mov       DWORD PTR [-440+ebp], eax                     ;371.10
        call      _for_close                                    ;371.10
                                ; LOE
.B1.432:                        ; Preds .B1.250
        add       esp, 24                                       ;371.10
                                ; LOE
.B1.251:                        ; Preds .B1.432
        lea       eax, DWORD PTR [-188+ebp]                     ;372.12
        push      eax                                           ;372.12
        call      _NEXTA_STAT                                   ;372.12
                                ; LOE
.B1.433:                        ; Preds .B1.251
        add       esp, 4                                        ;372.12
                                ; LOE
.B1.252:                        ; Preds .B1.433
        mov       edx, DWORD PTR [-772+ebp]                     ;374.7
        test      dl, 1                                         ;374.10
        je        .B1.255       ; Prob 60%                      ;374.10
                                ; LOE edx
.B1.253:                        ; Preds .B1.252
        mov       ecx, edx                                      ;374.31
        mov       eax, edx                                      ;374.31
        shr       ecx, 1                                        ;374.31
        and       eax, 1                                        ;374.31
        and       ecx, 1                                        ;374.31
        add       eax, eax                                      ;374.31
        shl       ecx, 2                                        ;374.31
        or        ecx, eax                                      ;374.31
        or        ecx, 262144                                   ;374.31
        push      ecx                                           ;374.31
        push      DWORD PTR [-784+ebp]                          ;374.31
        mov       DWORD PTR [-1016+ebp], edx                    ;374.31
        call      _for_dealloc_allocatable                      ;374.31
                                ; LOE
.B1.434:                        ; Preds .B1.253
        mov       edx, DWORD PTR [-1016+ebp]                    ;
        add       esp, 8                                        ;374.31
                                ; LOE edx dl dh
.B1.254:                        ; Preds .B1.434
        and       edx, -2                                       ;374.31
        mov       DWORD PTR [-784+ebp], 0                       ;374.31
        mov       DWORD PTR [-772+ebp], edx                     ;374.31
                                ; LOE edx
.B1.255:                        ; Preds .B1.254 .B1.252
        mov       edi, DWORD PTR [-860+ebp]                     ;375.7
        test      edi, 1                                        ;375.10
        je        .B1.258       ; Prob 60%                      ;375.10
                                ; LOE edx edi
.B1.256:                        ; Preds .B1.255
        mov       ecx, edi                                      ;375.30
        mov       eax, edi                                      ;375.30
        shr       ecx, 1                                        ;375.30
        and       eax, 1                                        ;375.30
        and       ecx, 1                                        ;375.30
        add       eax, eax                                      ;375.30
        shl       ecx, 2                                        ;375.30
        or        ecx, eax                                      ;375.30
        or        ecx, 262144                                   ;375.30
        push      ecx                                           ;375.30
        push      DWORD PTR [-872+ebp]                          ;375.30
        mov       DWORD PTR [-1016+ebp], edx                    ;375.30
        call      _for_dealloc_allocatable                      ;375.30
                                ; LOE edi
.B1.435:                        ; Preds .B1.256
        mov       edx, DWORD PTR [-1016+ebp]                    ;
        add       esp, 8                                        ;375.30
                                ; LOE edx edi dl dh
.B1.257:                        ; Preds .B1.435
        and       edi, -2                                       ;375.30
        mov       DWORD PTR [-872+ebp], 0                       ;375.30
        mov       DWORD PTR [-860+ebp], edi                     ;375.30
                                ; LOE edx edi
.B1.258:                        ; Preds .B1.257 .B1.255
        mov       esi, DWORD PTR [-812+ebp]                     ;376.7
        test      esi, 1                                        ;376.10
        je        .B1.261       ; Prob 60%                      ;376.10
                                ; LOE edx esi edi
.B1.259:                        ; Preds .B1.258
        mov       ecx, esi                                      ;376.33
        mov       eax, esi                                      ;376.33
        shr       ecx, 1                                        ;376.33
        and       eax, 1                                        ;376.33
        and       ecx, 1                                        ;376.33
        add       eax, eax                                      ;376.33
        shl       ecx, 2                                        ;376.33
        or        ecx, eax                                      ;376.33
        or        ecx, 262144                                   ;376.33
        push      ecx                                           ;376.33
        push      DWORD PTR [-824+ebp]                          ;376.33
        mov       DWORD PTR [-1016+ebp], edx                    ;376.33
        call      _for_dealloc_allocatable                      ;376.33
                                ; LOE esi edi
.B1.436:                        ; Preds .B1.259
        mov       edx, DWORD PTR [-1016+ebp]                    ;
        add       esp, 8                                        ;376.33
                                ; LOE edx esi edi dl dh
.B1.260:                        ; Preds .B1.436
        and       esi, -2                                       ;376.33
        mov       DWORD PTR [-824+ebp], 0                       ;376.33
        mov       DWORD PTR [-812+ebp], esi                     ;376.33
                                ; LOE edx esi edi
.B1.261:                        ; Preds .B1.260 .B1.258
        mov       eax, DWORD PTR [-908+ebp]                     ;377.7
        test      al, 1                                         ;377.10
        mov       DWORD PTR [-628+ebp], eax                     ;377.7
        je        .B1.264       ; Prob 60%                      ;377.10
                                ; LOE eax edx esi edi al ah
.B1.262:                        ; Preds .B1.261
        mov       ecx, eax                                      ;377.32
        mov       eax, ecx                                      ;377.32
        shr       eax, 1                                        ;377.32
        and       ecx, 1                                        ;377.32
        and       eax, 1                                        ;377.32
        add       ecx, ecx                                      ;377.32
        shl       eax, 2                                        ;377.32
        or        eax, ecx                                      ;377.32
        or        eax, 262144                                   ;377.32
        push      eax                                           ;377.32
        push      DWORD PTR [-920+ebp]                          ;377.32
        mov       DWORD PTR [-1016+ebp], edx                    ;377.32
        call      _for_dealloc_allocatable                      ;377.32
                                ; LOE esi edi
.B1.437:                        ; Preds .B1.262
        mov       edx, DWORD PTR [-1016+ebp]                    ;
        add       esp, 8                                        ;377.32
                                ; LOE edx esi edi dl dh
.B1.263:                        ; Preds .B1.437
        mov       eax, DWORD PTR [-628+ebp]                     ;377.32
        and       eax, -2                                       ;377.32
        mov       DWORD PTR [-920+ebp], 0                       ;377.32
        mov       DWORD PTR [-628+ebp], eax                     ;377.32
        mov       DWORD PTR [-908+ebp], eax                     ;377.32
                                ; LOE edx esi edi
.B1.264:                        ; Preds .B1.263 .B1.261
        test      dl, 1                                         ;379.1
        jne       .B1.342       ; Prob 3%                       ;379.1
                                ; LOE edx esi edi
.B1.265:                        ; Preds .B1.264 .B1.343
        test      edi, 1                                        ;379.1
        jne       .B1.340       ; Prob 3%                       ;379.1
                                ; LOE esi edi
.B1.266:                        ; Preds .B1.265 .B1.341
        test      esi, 1                                        ;379.1
        jne       .B1.338       ; Prob 3%                       ;379.1
                                ; LOE esi
.B1.267:                        ; Preds .B1.266 .B1.339
        test      BYTE PTR [-628+ebp], 1                        ;379.1
        jne       .B1.336       ; Prob 3%                       ;379.1
                                ; LOE
.B1.268:                        ; Preds .B1.267 .B1.337
        mov       eax, DWORD PTR [-400+ebp]                     ;379.1
        mov       esp, eax                                      ;379.1
                                ; LOE
.B1.438:                        ; Preds .B1.268
        mov       esi, DWORD PTR [-396+ebp]                     ;379.1
        mov       edi, DWORD PTR [-392+ebp]                     ;379.1
        mov       esp, ebp                                      ;379.1
        pop       ebp                                           ;379.1
        mov       esp, ebx                                      ;379.1
        pop       ebx                                           ;379.1
        ret                                                     ;379.1
                                ; LOE
.B1.269:                        ; Preds .B1.135
        mov       esi, DWORD PTR [-700+ebp]                     ;
        imul      esi, DWORD PTR [-228+ebp]                     ;
        imul      eax, DWORD PTR [-232+ebp]                     ;
        shl       ecx, 3                                        ;
        mov       edi, DWORD PTR [-224+ebp]                     ;
        sub       edi, ecx                                      ;
        sub       ecx, esi                                      ;
        add       edi, ecx                                      ;
        mov       ecx, DWORD PTR [-196+ebp]                     ;
        add       esi, edi                                      ;
        mov       DWORD PTR [-220+ebp], esi                     ;
        mov       esi, ecx                                      ;
        shr       esi, 31                                       ;
        add       esi, ecx                                      ;
        sar       esi, 1                                        ;
        mov       DWORD PTR [-108+ebp], esi                     ;
        mov       esi, DWORD PTR [-216+ebp]                     ;
        mov       edi, esi                                      ;
        shl       edx, 2                                        ;
        sub       edi, edx                                      ;
        sub       edx, eax                                      ;
        mov       DWORD PTR [-212+ebp], edi                     ;
        mov       edi, DWORD PTR [-648+ebp]                     ;130.13
        mov       ecx, DWORD PTR [-628+ebp]                     ;130.13
        imul      ecx, edi                                      ;130.13
        imul      edi, DWORD PTR [-652+ebp]                     ;
        mov       DWORD PTR [-648+ebp], edi                     ;
        sub       edi, ecx                                      ;
        sub       esi, edi                                      ;
        add       eax, ecx                                      ;
        mov       DWORD PTR [-216+ebp], esi                     ;
        mov       esi, DWORD PTR [-212+ebp]                     ;
        add       esi, edx                                      ;
        add       esi, eax                                      ;
        sub       esi, DWORD PTR [-648+ebp]                     ;
        mov       DWORD PTR [-236+ebp], 0                       ;
        mov       DWORD PTR [-212+ebp], esi                     ;
        mov       edx, DWORD PTR [-236+ebp]                     ;
                                ; LOE edx
.B1.270:                        ; Preds .B1.275 .B1.356 .B1.269
        cmp       DWORD PTR [-108+ebp], 0                       ;154.13
        jbe       .B1.358       ; Prob 11%                      ;154.13
                                ; LOE edx
.B1.271:                        ; Preds .B1.270
        mov       esi, DWORD PTR [-228+ebp]                     ;
        xor       eax, eax                                      ;
        imul      esi, edx                                      ;
        mov       ecx, DWORD PTR [-220+ebp]                     ;
        mov       DWORD PTR [-236+ebp], edx                     ;
        lea       edi, DWORD PTR [ecx+esi]                      ;
        mov       ecx, DWORD PTR [-232+ebp]                     ;
        imul      ecx, edx                                      ;
        mov       DWORD PTR [-92+ebp], edi                      ;
        mov       edi, DWORD PTR [-212+ebp]                     ;
        add       esi, DWORD PTR [-224+ebp]                     ;
        mov       DWORD PTR [-96+ebp], esi                      ;
        add       edi, ecx                                      ;
        add       ecx, DWORD PTR [-216+ebp]                     ;
        mov       DWORD PTR [-100+ebp], ecx                     ;
        mov       ecx, eax                                      ;
        mov       edx, edi                                      ;
                                ; LOE eax edx ecx
.B1.272:                        ; Preds .B1.272 .B1.271
        mov       esi, DWORD PTR [-100+ebp]                     ;154.13
        mov       edi, DWORD PTR [esi+eax*8]                    ;154.13
        mov       esi, DWORD PTR [-96+ebp]                      ;154.13
        mov       DWORD PTR [ecx+esi], edi                      ;154.13
        mov       esi, DWORD PTR [-92+ebp]                      ;154.13
        mov       edi, DWORD PTR [4+edx+eax*8]                  ;154.13
        inc       eax                                           ;154.13
        mov       DWORD PTR [8+ecx+esi], edi                    ;154.13
        add       ecx, 16                                       ;154.13
        cmp       eax, DWORD PTR [-108+ebp]                     ;154.13
        jb        .B1.272       ; Prob 64%                      ;154.13
                                ; LOE eax edx ecx
.B1.273:                        ; Preds .B1.272
        mov       edx, DWORD PTR [-236+ebp]                     ;
        lea       edi, DWORD PTR [1+eax+eax]                    ;154.13
                                ; LOE edx edi
.B1.274:                        ; Preds .B1.273 .B1.358
        lea       eax, DWORD PTR [-1+edi]                       ;154.13
        cmp       eax, DWORD PTR [-196+ebp]                     ;154.13
        jae       .B1.356       ; Prob 11%                      ;154.13
                                ; LOE edx edi
.B1.275:                        ; Preds .B1.274
        mov       eax, DWORD PTR [-212+ebp]                     ;154.13
        mov       esi, DWORD PTR [-232+ebp]                     ;154.13
        imul      esi, edx                                      ;154.13
        mov       DWORD PTR [-236+ebp], edx                     ;
        lea       ecx, DWORD PTR [eax+edi*4]                    ;154.13
        mov       eax, DWORD PTR [-228+ebp]                     ;154.13
        imul      eax, edx                                      ;154.13
        mov       edx, DWORD PTR [-220+ebp]                     ;154.13
        lea       edx, DWORD PTR [edx+edi*8]                    ;154.13
        mov       edi, DWORD PTR [-4+ecx+esi]                   ;154.13
        mov       DWORD PTR [-8+edx+eax], edi                   ;154.13
        mov       edx, DWORD PTR [-236+ebp]                     ;154.13
        inc       edx                                           ;154.13
        cmp       edx, DWORD PTR [-240+ebp]                     ;154.13
        jb        .B1.270       ; Prob 82%                      ;154.13
        jmp       .B1.136       ; Prob 100%                     ;154.13
                                ; LOE edx
.B1.276:                        ; Preds .B1.101
        imul      ecx, DWORD PTR [-480+ebp]                     ;
        xor       eax, eax                                      ;
        mov       DWORD PTR [-508+ebp], eax                     ;
        mov       eax, edi                                      ;136.17
        and       eax, -8                                       ;136.17
        mov       DWORD PTR [-488+ebp], eax                     ;136.17
        mov       eax, esi                                      ;
        shl       edx, 2                                        ;
        sub       eax, edx                                      ;
        sub       edx, ecx                                      ;
        add       eax, edx                                      ;
        mov       edx, DWORD PTR [-508+ebp]                     ;
        add       eax, ecx                                      ;
        mov       DWORD PTR [-512+ebp], esi                     ;
        lea       ecx, DWORD PTR [edi*4]                        ;
        mov       DWORD PTR [-452+ebp], edi                     ;
        mov       DWORD PTR [-500+ebp], esi                     ;
        mov       DWORD PTR [-484+ebp], eax                     ;
        mov       DWORD PTR [-504+ebp], ecx                     ;
        mov       esi, edx                                      ;
        mov       edx, DWORD PTR [-512+ebp]                     ;
        mov       edi, DWORD PTR [-508+ebp]                     ;
                                ; LOE eax edx esi edi
.B1.277:                        ; Preds .B1.279 .B1.371 .B1.276
        cmp       DWORD PTR [-452+ebp], 24                      ;136.17
        jle       .B1.364       ; Prob 0%                       ;136.17
                                ; LOE eax edx esi edi
.B1.278:                        ; Preds .B1.277
        push      DWORD PTR [-504+ebp]                          ;136.17
        push      0                                             ;136.17
        push      eax                                           ;136.17
        mov       DWORD PTR [-516+ebp], eax                     ;136.17
        mov       DWORD PTR [-512+ebp], edx                     ;136.17
        call      __intel_fast_memset                           ;136.17
                                ; LOE esi edi
.B1.439:                        ; Preds .B1.278
        mov       edx, DWORD PTR [-512+ebp]                     ;
        add       esp, 12                                       ;136.17
        mov       eax, DWORD PTR [-516+ebp]                     ;
                                ; LOE eax edx esi edi al dl ah dh
.B1.279:                        ; Preds .B1.368 .B1.439
        inc       edi                                           ;136.17
        mov       ecx, DWORD PTR [-480+ebp]                     ;136.17
        add       eax, ecx                                      ;136.17
        add       edx, ecx                                      ;136.17
        add       esi, ecx                                      ;136.17
        cmp       edi, DWORD PTR [-476+ebp]                     ;136.17
        jb        .B1.277       ; Prob 82%                      ;136.17
        jmp       .B1.102       ; Prob 100%                     ;136.17
                                ; LOE eax edx esi edi
.B1.281:                        ; Preds .B1.19 .B1.17 .B1.21    ; Infreq
        xor       edx, edx                                      ;
        xor       esi, esi                                      ;
        jmp       .B1.29        ; Prob 100%                     ;
                                ; LOE eax edx esi
.B1.283:                        ; Preds .B1.45                  ; Infreq
        mov       eax, 1                                        ;
        mov       DWORD PTR [-8+ebp], eax                       ;
        jmp       .B1.49        ; Prob 100%                     ;
                                ; LOE eax edx ecx al ah xmm0 xmm5 xmm6
.B1.285:                        ; Preds .B1.37                  ; Infreq
        mov       edi, DWORD PTR [-36+ebp]                      ;81.22
        mov       esi, DWORD PTR [-20+ebp]                      ;81.22
        imul      eax, DWORD PTR [12+eax+edx], 900              ;81.22
        mov       ecx, DWORD PTR [156+edi+eax]                  ;81.22
        imul      edi, DWORD PTR [188+edi+eax], -40             ;81.22
        lea       edx, DWORD PTR [ecx+esi*8]                    ;81.22
        movsx     ecx, WORD PTR [36+edi+edx]                    ;81.22
        mov       DWORD PTR [-28+ebp], ecx                      ;81.22
        jmp       .B1.42        ; Prob 100%                     ;81.22
                                ; LOE
.B1.286:                        ; Preds .B1.34                  ; Infreq
        mov       edx, DWORD PTR [-36+ebp]                      ;72.63
        movss     xmm0, DWORD PTR [652+edx+eax]                 ;72.63
        mulss     xmm0, DWORD PTR [_2il0floatpacket.21]         ;72.93
        cvttss2si eax, xmm0                                     ;72.12
        mov       DWORD PTR [ecx+esi], eax                      ;72.12
        jmp       .B1.36        ; Prob 100%                     ;72.12
                                ; LOE
.B1.287:                        ; Preds .B1.75                  ; Infreq
        call      _MEMORY_MODULE_mp_MEMALLOCATE_ASSIGNMENT      ;103.15
                                ; LOE
.B1.288:                        ; Preds .B1.287                 ; Infreq
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_CALLAVG], -1 ;104.10
        mov       esi, DWORD PTR [-188+ebp]                     ;107.7
        movss     xmm2, DWORD PTR [_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT] ;107.16
        jmp       .B1.76        ; Prob 100%                     ;107.16
                                ; LOE esi xmm2
.B1.289:                        ; Preds .B1.96                  ; Infreq
        cmp       ecx, 8                                        ;134.17
        jl        .B1.297       ; Prob 10%                      ;134.17
                                ; LOE edx ecx
.B1.290:                        ; Preds .B1.289                 ; Infreq
        mov       eax, ecx                                      ;134.17
        xor       esi, esi                                      ;134.17
        and       eax, -8                                       ;134.17
        pxor      xmm0, xmm0                                    ;134.17
                                ; LOE eax edx ecx esi xmm0
.B1.291:                        ; Preds .B1.291 .B1.290         ; Infreq
        movups    XMMWORD PTR [edx+esi*4], xmm0                 ;134.17
        movups    XMMWORD PTR [16+edx+esi*4], xmm0              ;134.17
        add       esi, 8                                        ;134.17
        cmp       esi, eax                                      ;134.17
        jb        .B1.291       ; Prob 82%                      ;134.17
                                ; LOE eax edx ecx esi xmm0
.B1.293:                        ; Preds .B1.291 .B1.297         ; Infreq
        cmp       eax, ecx                                      ;134.17
        jae       .B1.98        ; Prob 10%                      ;134.17
                                ; LOE eax edx ecx
.B1.294:                        ; Preds .B1.293                 ; Infreq
        xor       esi, esi                                      ;
                                ; LOE eax edx ecx esi
.B1.295:                        ; Preds .B1.295 .B1.294         ; Infreq
        mov       DWORD PTR [edx+eax*4], esi                    ;134.17
        inc       eax                                           ;134.17
        cmp       eax, ecx                                      ;134.17
        jb        .B1.295       ; Prob 82%                      ;134.17
        jmp       .B1.98        ; Prob 100%                     ;134.17
                                ; LOE eax edx ecx esi
.B1.297:                        ; Preds .B1.289                 ; Infreq
        xor       eax, eax                                      ;134.17
        jmp       .B1.293       ; Prob 100%                     ;134.17
                                ; LOE eax edx ecx
.B1.298:                        ; Preds .B1.105                 ; Infreq
        mov       eax, 1                                        ;
        jmp       .B1.109       ; Prob 100%                     ;
                                ; LOE eax edx ecx xmm0
.B1.299:                        ; Preds .B1.114 .B1.118 .B1.116 ; Infreq
        xor       eax, eax                                      ;153.13
        mov       DWORD PTR [-596+ebp], eax                     ;153.13
        jmp       .B1.130       ; Prob 100%                     ;153.13
                                ; LOE edx ecx
.B1.300:                        ; Preds .B1.138                 ; Infreq
        mov       esi, 1                                        ;
        jmp       .B1.142       ; Prob 100%                     ;
                                ; LOE eax esi
.B1.301:                        ; Preds .B1.165 .B1.166         ; Infreq
        cmp       DWORD PTR [-448+ebp], 0                       ;231.82
        jne       .B1.167       ; Prob 50%                      ;231.82
                                ; LOE ecx
.B1.302:                        ; Preds .B1.301                 ; Infreq
        mov       esi, DWORD PTR [-188+ebp]                     ;231.67
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TDSPSTEP] ;231.98
        lea       eax, DWORD PTR [-1+esi]                       ;231.98
        cdq                                                     ;231.93
        idiv      edi                                           ;231.93
        test      edx, edx                                      ;231.111
        jne       .B1.167       ; Prob 50%                      ;231.111
                                ; LOE ecx esi
.B1.303:                        ; Preds .B1.302                 ; Infreq
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NINTS] ;231.120
        sub       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_BEGINT+32] ;231.125
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_BEGINT] ;231.120
        cvtsi2ss  xmm1, esi                                     ;231.120
        movss     xmm0, DWORD PTR [4+eax+edx*4]                 ;231.125
        divss     xmm0, DWORD PTR [_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT] ;231.140
        comiss    xmm0, xmm1                                    ;231.122
        jb        .B1.167       ; Prob 50%                      ;231.122
                                ; LOE ecx
.B1.304:                        ; Preds .B1.303                 ; Infreq
        mov       edx, DWORD PTR [-444+ebp]                     ;232.12
        mov       DWORD PTR [-1028+ebp], ecx                    ;
        mov       ecx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHAIN+44] ;232.22
        mov       esi, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHAIN+40] ;232.22
        lea       eax, DWORD PTR [edx*4]                        ;232.12
        mov       DWORD PTR [-1044+ebp], ecx                    ;232.22
        imul      ecx, esi                                      ;232.12
        mov       DWORD PTR [-1024+ebp], eax                    ;232.12
        sub       ecx, esi                                      ;232.12
        mov       eax, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHAIN] ;232.12
        mov       edi, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHAIN+32] ;232.12
        mov       DWORD PTR [-1048+ebp], esi                    ;232.22
        mov       DWORD PTR [-1040+ebp], edi                    ;232.12
        lea       edx, DWORD PTR [eax+edx*4]                    ;232.12
        sub       edx, ecx                                      ;232.12
        lea       esi, DWORD PTR [edi*4]                        ;232.12
        sub       edx, esi                                      ;232.12
        mov       ecx, DWORD PTR [-1028+ebp]                    ;232.35
        cmp       ecx, DWORD PTR [edx]                          ;232.35
        jne       .B1.167       ; Prob 10%                      ;232.35
                                ; LOE eax ecx edi cl ch
.B1.305:                        ; Preds .B1.304                 ; Infreq
        mov       esi, DWORD PTR [-444+ebp]                     ;
        mov       edx, edi                                      ;
        mov       DWORD PTR [-1028+ebp], ecx                    ;
        lea       edi, DWORD PTR [eax+esi*4]                    ;
        mov       esi, DWORD PTR [-1044+ebp]                    ;
        imul      esi, DWORD PTR [-1048+ebp]                    ;
        shl       edx, 2                                        ;
        mov       DWORD PTR [-1020+ebp], edx                    ;
        mov       edx, DWORD PTR [-1048+ebp]                    ;
                                ; LOE eax edx esi edi
.B1.306:                        ; Preds .B1.311 .B1.305         ; Infreq
        lea       ecx, DWORD PTR [edx*4]                        ;234.25
        neg       ecx                                           ;234.70
        add       ecx, esi                                      ;234.70
        sub       edi, ecx                                      ;234.70
        sub       edi, DWORD PTR [-1020+ebp]                    ;234.70
        mov       ecx, DWORD PTR [edi]                          ;234.28
        sub       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;234.70
        shl       ecx, 5                                        ;234.70
        mov       edi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;234.25
        cmp       BYTE PTR [5+edi+ecx], 6                       ;234.70
        je        .B1.310       ; Prob 41%                      ;234.70
                                ; LOE eax edx esi
.B1.307:                        ; Preds .B1.306                 ; Infreq
        mov       ecx, DWORD PTR [-1044+ebp]                    ;235.35
        imul      ecx, edx                                      ;235.35
        mov       edi, DWORD PTR [-1040+ebp]                    ;235.35
        sub       eax, ecx                                      ;235.35
        shl       edi, 2                                        ;235.35
        lea       ecx, DWORD PTR [edx*4]                        ;235.35
        sub       ecx, edi                                      ;235.35
        add       ecx, eax                                      ;235.35
        mov       esi, DWORD PTR [-1024+ebp]                    ;235.35
        add       ecx, esi                                      ;235.35
        mov       DWORD PTR [-1036+ebp], ecx                    ;235.35
        lea       ecx, DWORD PTR [edx+edx*2]                    ;235.35
        sub       ecx, edi                                      ;235.35
        sub       edx, edi                                      ;235.35
        add       ecx, eax                                      ;235.35
        add       eax, edx                                      ;235.35
        add       ecx, esi                                      ;235.35
        lea       edx, DWORD PTR [-1012+ebp]                    ;235.35
        mov       DWORD PTR [-1032+ebp], ecx                    ;235.35
        mov       ecx, OFFSET FLAT: __NLITPACK_3.0.1            ;235.35
        push      edx                                           ;235.35
        push      ecx                                           ;235.35
        add       eax, esi                                      ;235.35
        push      eax                                           ;235.35
        push      ecx                                           ;235.35
        push      OFFSET FLAT: _DYNUST_MAIN_MODULE_mp_IPINIT    ;235.35
        push      DWORD PTR [-1032+ebp]                         ;235.35
        push      DWORD PTR [-1036+ebp]                         ;235.35
        call      _RETRIEVE_VEH_PATH_ASTAR                      ;235.35
                                ; LOE
.B1.440:                        ; Preds .B1.307                 ; Infreq
        add       esp, 28                                       ;235.35
                                ; LOE
.B1.308:                        ; Preds .B1.440                 ; Infreq
        mov       esi, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHAIN+40] ;237.55
        mov       eax, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHAIN+44] ;237.55
        imul      eax, esi                                      ;237.34
        mov       edi, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHAIN] ;237.53
        mov       ecx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHAIN+32] ;237.53
        sub       edi, eax                                      ;237.34
        shl       ecx, 2                                        ;237.34
        lea       eax, DWORD PTR [esi*4]                        ;237.34
        sub       eax, ecx                                      ;237.34
        lea       esi, DWORD PTR [esi+esi*2]                    ;237.34
        sub       esi, ecx                                      ;237.34
        add       eax, edi                                      ;237.34
        add       edi, esi                                      ;237.34
        lea       ecx, DWORD PTR [-1008+ebp]                    ;237.34
        mov       edx, DWORD PTR [-1024+ebp]                    ;237.34
        add       eax, edx                                      ;237.34
        add       edi, edx                                      ;237.34
        lea       edx, DWORD PTR [-1004+ebp]                    ;237.34
        push      edx                                           ;237.34
        push      ecx                                           ;237.34
        push      OFFSET FLAT: __NLITPACK_3.0.1                 ;237.34
        push      edi                                           ;237.34
        push      eax                                           ;237.34
        lea       edi, DWORD PTR [-644+ebp]                     ;237.34
        push      edi                                           ;237.34
        call      _RETRIEVE_NEXT_LINK                           ;237.34
                                ; LOE
.B1.441:                        ; Preds .B1.308                 ; Infreq
        add       esp, 24                                       ;237.34
                                ; LOE
.B1.309:                        ; Preds .B1.441                 ; Infreq
        mov       ecx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHAIN+32] ;240.13
        mov       esi, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHAIN+44] ;240.16
        mov       edx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHAIN+40] ;240.16
        mov       eax, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHAIN] ;240.13
        lea       edi, DWORD PTR [ecx*4]                        ;
        mov       DWORD PTR [-1044+ebp], esi                    ;240.16
        mov       DWORD PTR [-1040+ebp], ecx                    ;240.13
        imul      esi, edx                                      ;
        mov       DWORD PTR [-1020+ebp], edi                    ;
                                ; LOE eax edx esi
.B1.310:                        ; Preds .B1.309 .B1.306         ; Infreq
        mov       ecx, DWORD PTR [-444+ebp]                     ;239.16
        inc       ecx                                           ;239.16
        mov       DWORD PTR [-444+ebp], ecx                     ;239.16
        lea       edi, DWORD PTR [ecx*4]                        ;196.9
        mov       DWORD PTR [-1024+ebp], edi                    ;196.9
        lea       edi, DWORD PTR [eax+ecx*4]                    ;240.29
        mov       ecx, esi                                      ;240.29
        sub       ecx, edx                                      ;240.29
        neg       ecx                                           ;240.29
        add       ecx, edi                                      ;240.29
        sub       ecx, DWORD PTR [-1020+ebp]                    ;240.29
        mov       ecx, DWORD PTR [ecx]                          ;240.16
        test      ecx, ecx                                      ;240.29
        jle       .B1.312       ; Prob 20%                      ;240.29
                                ; LOE eax edx ecx esi edi
.B1.311:                        ; Preds .B1.310                 ; Infreq
        cmp       ecx, DWORD PTR [-1028+ebp]                    ;232.35
        je        .B1.306       ; Prob 82%                      ;232.35
                                ; LOE eax edx esi edi
.B1.312:                        ; Preds .B1.310 .B1.311         ; Infreq
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ITERATION] ;231.50
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_RUNMODE] ;231.17
        mov       ecx, DWORD PTR [-1028+ebp]                    ;
        mov       DWORD PTR [-448+ebp], eax                     ;231.50
        mov       DWORD PTR [-308+ebp], edx                     ;231.17
        jmp       .B1.167       ; Prob 100%                     ;231.17
                                ; LOE ecx
.B1.314:                        ; Preds .B1.161                 ; Infreq
        mov       eax, -1                                       ;203.21
        jmp       .B1.164       ; Prob 100%                     ;203.21
                                ; LOE eax ecx
.B1.316:                        ; Preds .B1.174                 ; Infreq
        mov       esi, 1                                        ;
        jmp       .B1.178       ; Prob 100%                     ;
                                ; LOE eax edx ecx esi
.B1.317:                        ; Preds .B1.184                 ; Infreq
        mov       esi, 1                                        ;
        jmp       .B1.188       ; Prob 100%                     ;
                                ; LOE eax edx ecx esi
.B1.318:                        ; Preds .B1.209                 ; Infreq
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_JUSTVEH] ;289.103
        cmp       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_MAXVEHICLESFILE] ;289.120
        jne       .B1.212       ; Prob 80%                      ;289.120
                                ; LOE ecx esi
.B1.319:                        ; Preds .B1.326 .B1.318         ; Infreq
        lea       edi, DWORD PTR [-440+ebp]                     ;
                                ; LOE esi edi
.B1.320:                        ; Preds .B1.319                 ; Infreq
        push      32                                            ;290.11
        mov       DWORD PTR [-440+ebp], 0                       ;290.11
        lea       eax, DWORD PTR [-1048+ebp]                    ;290.11
        push      eax                                           ;290.11
        push      OFFSET FLAT: __STRLITPACK_60.0.1              ;290.11
        push      -2088435968                                   ;290.11
        push      666                                           ;290.11
        push      edi                                           ;290.11
        mov       DWORD PTR [-1048+ebp], 50                     ;290.11
        mov       DWORD PTR [-1044+ebp], OFFSET FLAT: __STRLITPACK_32 ;290.11
        call      _for_write_seq_lis                            ;290.11
                                ; LOE esi edi
.B1.442:                        ; Preds .B1.320                 ; Infreq
        add       esp, 24                                       ;290.11
                                ; LOE esi edi
.B1.321:                        ; Preds .B1.442                 ; Infreq
        push      32                                            ;291.11
        mov       DWORD PTR [-440+ebp], 0                       ;291.11
        lea       eax, DWORD PTR [-1040+ebp]                    ;291.11
        push      eax                                           ;291.11
        push      OFFSET FLAT: __STRLITPACK_61.0.1              ;291.11
        push      -2088435968                                   ;291.11
        push      666                                           ;291.11
        push      edi                                           ;291.11
        mov       DWORD PTR [-1040+ebp], 50                     ;291.11
        mov       DWORD PTR [-1036+ebp], OFFSET FLAT: __STRLITPACK_30 ;291.11
        call      _for_write_seq_lis                            ;291.11
                                ; LOE esi edi
.B1.443:                        ; Preds .B1.321                 ; Infreq
        add       esp, 24                                       ;291.11
                                ; LOE esi edi
.B1.322:                        ; Preds .B1.443                 ; Infreq
        push      32                                            ;292.11
        mov       DWORD PTR [-440+ebp], 0                       ;292.11
        lea       eax, DWORD PTR [-1032+ebp]                    ;292.11
        push      eax                                           ;292.11
        push      OFFSET FLAT: __STRLITPACK_62.0.1              ;292.11
        push      -2088435968                                   ;292.11
        push      666                                           ;292.11
        push      edi                                           ;292.11
        mov       DWORD PTR [-1032+ebp], 51                     ;292.11
        mov       DWORD PTR [-1028+ebp], OFFSET FLAT: __STRLITPACK_28 ;292.11
        call      _for_write_seq_lis                            ;292.11
                                ; LOE esi edi
.B1.444:                        ; Preds .B1.322                 ; Infreq
        add       esp, 24                                       ;292.11
                                ; LOE esi edi
.B1.323:                        ; Preds .B1.444                 ; Infreq
        push      32                                            ;293.11
        mov       DWORD PTR [-440+ebp], 0                       ;293.11
        lea       eax, DWORD PTR [-1024+ebp]                    ;293.11
        push      eax                                           ;293.11
        push      OFFSET FLAT: __STRLITPACK_63.0.1              ;293.11
        push      -2088435968                                   ;293.11
        push      666                                           ;293.11
        push      edi                                           ;293.11
        mov       DWORD PTR [-1024+ebp], 50                     ;293.11
        mov       DWORD PTR [-1020+ebp], OFFSET FLAT: __STRLITPACK_26 ;293.11
        call      _for_write_seq_lis                            ;293.11
                                ; LOE esi edi
.B1.445:                        ; Preds .B1.323                 ; Infreq
        add       esp, 24                                       ;293.11
                                ; LOE esi edi
.B1.324:                        ; Preds .B1.445                 ; Infreq
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ALLOUT], -1 ;294.11
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ENDTIME] ;367.7
        jmp       .B1.247       ; Prob 100%                     ;367.7
                                ; LOE edx esi edi
.B1.325:                        ; Preds .B1.207 .B1.206         ; Infreq
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NINTS] ;284.10
                                ; LOE edx ecx edi
.B1.326:                        ; Preds .B1.325                 ; Infreq
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_BEGINT+32] ;289.60
        neg       eax                                           ;289.90
        add       eax, edi                                      ;289.90
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_BEGINT] ;289.60
        mov       esi, DWORD PTR [-188+ebp]                     ;289.60
        cvtsi2ss  xmm0, esi                                     ;289.60
        movss     xmm1, DWORD PTR [edi+eax*4]                   ;289.64
        divss     xmm1, DWORD PTR [_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT] ;289.75
        comiss    xmm0, xmm1                                    ;289.62
        ja        .B1.319       ; Prob 20%                      ;289.62
        jmp       .B1.209       ; Prob 100%                     ;289.62
                                ; LOE edx ecx esi
.B1.328:                        ; Preds .B1.227                 ; Infreq
        mov       DWORD PTR [-1048+ebp], eax                    ;
        lea       eax, DWORD PTR [-744+ebp]                     ;321.7
        push      32                                            ;321.7
        push      eax                                           ;321.7
        push      OFFSET FLAT: __STRLITPACK_64.0.1              ;321.7
        push      -2088435968                                   ;321.7
        push      666                                           ;321.7
        mov       DWORD PTR [-440+ebp], 0                       ;321.7
        lea       edi, DWORD PTR [-440+ebp]                     ;
        push      edi                                           ;321.7
        mov       DWORD PTR [-744+ebp], 50                      ;321.7
        mov       DWORD PTR [-740+ebp], OFFSET FLAT: __STRLITPACK_24 ;321.7
        call      _for_write_seq_lis                            ;321.7
                                ; LOE esi edi
.B1.446:                        ; Preds .B1.328                 ; Infreq
        add       esp, 24                                       ;321.7
                                ; LOE esi edi
.B1.329:                        ; Preds .B1.446                 ; Infreq
        push      32                                            ;322.7
        mov       DWORD PTR [-440+ebp], 0                       ;322.7
        lea       eax, DWORD PTR [-736+ebp]                     ;322.7
        push      eax                                           ;322.7
        push      OFFSET FLAT: __STRLITPACK_65.0.1              ;322.7
        push      -2088435968                                   ;322.7
        push      666                                           ;322.7
        push      edi                                           ;322.7
        mov       DWORD PTR [-736+ebp], 50                      ;322.7
        mov       DWORD PTR [-732+ebp], OFFSET FLAT: __STRLITPACK_22 ;322.7
        call      _for_write_seq_lis                            ;322.7
                                ; LOE esi edi
.B1.447:                        ; Preds .B1.329                 ; Infreq
        add       esp, 24                                       ;322.7
                                ; LOE esi edi
.B1.330:                        ; Preds .B1.447                 ; Infreq
        push      32                                            ;323.7
        mov       DWORD PTR [-440+ebp], 0                       ;323.7
        lea       eax, DWORD PTR [-728+ebp]                     ;323.7
        push      eax                                           ;323.7
        push      OFFSET FLAT: __STRLITPACK_66.0.1              ;323.7
        push      -2088435968                                   ;323.7
        push      666                                           ;323.7
        push      edi                                           ;323.7
        mov       DWORD PTR [-728+ebp], 51                      ;323.7
        mov       DWORD PTR [-724+ebp], OFFSET FLAT: __STRLITPACK_20 ;323.7
        call      _for_write_seq_lis                            ;323.7
                                ; LOE esi edi
.B1.448:                        ; Preds .B1.330                 ; Infreq
        add       esp, 24                                       ;323.7
                                ; LOE esi edi
.B1.331:                        ; Preds .B1.448                 ; Infreq
        push      32                                            ;324.7
        mov       DWORD PTR [-440+ebp], 0                       ;324.7
        lea       eax, DWORD PTR [-720+ebp]                     ;324.7
        push      eax                                           ;324.7
        push      OFFSET FLAT: __STRLITPACK_67.0.1              ;324.7
        push      -2088435968                                   ;324.7
        push      666                                           ;324.7
        push      edi                                           ;324.7
        mov       DWORD PTR [-720+ebp], 3                       ;324.7
        mov       DWORD PTR [-716+ebp], OFFSET FLAT: __STRLITPACK_17 ;324.7
        call      _for_write_seq_lis                            ;324.7
                                ; LOE esi edi
.B1.449:                        ; Preds .B1.331                 ; Infreq
        add       esp, 24                                       ;324.7
                                ; LOE esi edi
.B1.332:                        ; Preds .B1.449                 ; Infreq
        mov       eax, DWORD PTR [-1048+ebp]                    ;324.7
        lea       edx, DWORD PTR [-648+ebp]                     ;324.7
        push      edx                                           ;324.7
        push      OFFSET FLAT: __STRLITPACK_68.0.1              ;324.7
        push      edi                                           ;324.7
        mov       DWORD PTR [-648+ebp], eax                     ;324.7
        call      _for_write_seq_lis_xmit                       ;324.7
                                ; LOE esi edi
.B1.450:                        ; Preds .B1.332                 ; Infreq
        add       esp, 12                                       ;324.7
                                ; LOE esi edi
.B1.333:                        ; Preds .B1.450                 ; Infreq
        mov       DWORD PTR [-712+ebp], 7                       ;324.7
        lea       eax, DWORD PTR [-712+ebp]                     ;324.7
        push      eax                                           ;324.7
        push      OFFSET FLAT: __STRLITPACK_69.0.1              ;324.7
        push      edi                                           ;324.7
        mov       DWORD PTR [-708+ebp], OFFSET FLAT: __STRLITPACK_16 ;324.7
        call      _for_write_seq_lis_xmit                       ;324.7
                                ; LOE esi edi
.B1.451:                        ; Preds .B1.333                 ; Infreq
        add       esp, 12                                       ;324.7
                                ; LOE esi edi
.B1.334:                        ; Preds .B1.451                 ; Infreq
        push      32                                            ;325.7
        mov       DWORD PTR [-440+ebp], 0                       ;325.7
        lea       eax, DWORD PTR [-704+ebp]                     ;325.7
        push      eax                                           ;325.7
        push      OFFSET FLAT: __STRLITPACK_70.0.1              ;325.7
        push      -2088435968                                   ;325.7
        push      666                                           ;325.7
        push      edi                                           ;325.7
        mov       DWORD PTR [-704+ebp], 50                      ;325.7
        mov       DWORD PTR [-700+ebp], OFFSET FLAT: __STRLITPACK_14 ;325.7
        call      _for_write_seq_lis                            ;325.7
                                ; LOE esi edi
.B1.452:                        ; Preds .B1.334                 ; Infreq
        add       esp, 24                                       ;325.7
                                ; LOE esi edi
.B1.335:                        ; Preds .B1.452                 ; Infreq
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ALLOUT], 0  ;326.7
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ENDTIME] ;367.7
        jmp       .B1.247       ; Prob 100%                     ;367.7
                                ; LOE edx esi edi
.B1.336:                        ; Preds .B1.267                 ; Infreq
        mov       eax, DWORD PTR [-628+ebp]                     ;379.1
        mov       edx, eax                                      ;379.1
        shr       edx, 1                                        ;379.1
        and       eax, 1                                        ;379.1
        and       edx, 1                                        ;379.1
        add       eax, eax                                      ;379.1
        shl       edx, 2                                        ;379.1
        or        edx, eax                                      ;379.1
        or        edx, 262144                                   ;379.1
        push      edx                                           ;379.1
        push      DWORD PTR [-920+ebp]                          ;379.1
        call      _for_dealloc_allocatable                      ;379.1
                                ; LOE
.B1.453:                        ; Preds .B1.336                 ; Infreq
        add       esp, 8                                        ;379.1
                                ; LOE
.B1.337:                        ; Preds .B1.453                 ; Infreq
        mov       eax, DWORD PTR [-628+ebp]                     ;379.1
        and       eax, -2                                       ;379.1
        mov       DWORD PTR [-920+ebp], 0                       ;379.1
        mov       DWORD PTR [-908+ebp], eax                     ;379.1
        jmp       .B1.268       ; Prob 100%                     ;379.1
                                ; LOE
.B1.338:                        ; Preds .B1.266                 ; Infreq
        mov       edx, esi                                      ;379.1
        mov       eax, esi                                      ;379.1
        shr       edx, 1                                        ;379.1
        and       eax, 1                                        ;379.1
        and       edx, 1                                        ;379.1
        add       eax, eax                                      ;379.1
        shl       edx, 2                                        ;379.1
        or        edx, eax                                      ;379.1
        or        edx, 262144                                   ;379.1
        push      edx                                           ;379.1
        push      DWORD PTR [-824+ebp]                          ;379.1
        call      _for_dealloc_allocatable                      ;379.1
                                ; LOE esi
.B1.454:                        ; Preds .B1.338                 ; Infreq
        add       esp, 8                                        ;379.1
                                ; LOE esi
.B1.339:                        ; Preds .B1.454                 ; Infreq
        and       esi, -2                                       ;379.1
        mov       DWORD PTR [-824+ebp], 0                       ;379.1
        mov       DWORD PTR [-812+ebp], esi                     ;379.1
        jmp       .B1.267       ; Prob 100%                     ;379.1
                                ; LOE
.B1.340:                        ; Preds .B1.265                 ; Infreq
        mov       edx, edi                                      ;379.1
        mov       eax, edi                                      ;379.1
        shr       edx, 1                                        ;379.1
        and       eax, 1                                        ;379.1
        and       edx, 1                                        ;379.1
        add       eax, eax                                      ;379.1
        shl       edx, 2                                        ;379.1
        or        edx, eax                                      ;379.1
        or        edx, 262144                                   ;379.1
        push      edx                                           ;379.1
        push      DWORD PTR [-872+ebp]                          ;379.1
        call      _for_dealloc_allocatable                      ;379.1
                                ; LOE esi edi
.B1.455:                        ; Preds .B1.340                 ; Infreq
        add       esp, 8                                        ;379.1
                                ; LOE esi edi
.B1.341:                        ; Preds .B1.455                 ; Infreq
        and       edi, -2                                       ;379.1
        mov       DWORD PTR [-872+ebp], 0                       ;379.1
        mov       DWORD PTR [-860+ebp], edi                     ;379.1
        jmp       .B1.266       ; Prob 100%                     ;379.1
                                ; LOE esi
.B1.342:                        ; Preds .B1.264                 ; Infreq
        mov       ecx, edx                                      ;379.1
        mov       eax, edx                                      ;379.1
        shr       ecx, 1                                        ;379.1
        and       eax, 1                                        ;379.1
        and       ecx, 1                                        ;379.1
        add       eax, eax                                      ;379.1
        shl       ecx, 2                                        ;379.1
        or        ecx, eax                                      ;379.1
        or        ecx, 262144                                   ;379.1
        push      ecx                                           ;379.1
        push      DWORD PTR [-784+ebp]                          ;379.1
        mov       DWORD PTR [-1016+ebp], edx                    ;379.1
        call      _for_dealloc_allocatable                      ;379.1
                                ; LOE esi edi
.B1.456:                        ; Preds .B1.342                 ; Infreq
        mov       edx, DWORD PTR [-1016+ebp]                    ;
        add       esp, 8                                        ;379.1
                                ; LOE edx esi edi dl dh
.B1.343:                        ; Preds .B1.456                 ; Infreq
        and       edx, -2                                       ;379.1
        mov       DWORD PTR [-784+ebp], 0                       ;379.1
        mov       DWORD PTR [-772+ebp], edx                     ;379.1
        jmp       .B1.265       ; Prob 100%                     ;379.1
                                ; LOE esi edi
.B1.346:                        ; Preds .B1.152                 ; Infreq
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_IER_OK], 1  ;176.33
        je        .B1.153       ; Prob 16%                      ;176.33
        jmp       .B1.169       ; Prob 100%                     ;176.33
                                ; LOE esi
.B1.348:                        ; Preds .B1.150 .B1.151         ; Infreq
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ITERATION], 0 ;175.109
        jne       .B1.152       ; Prob 50%                      ;175.109
                                ; LOE esi
.B1.349:                        ; Preds .B1.149 .B1.348         ; Infreq
        mov       eax, DWORD PTR [-188+ebp]                     ;175.43
        dec       eax                                           ;175.125
        cdq                                                     ;175.120
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TDSPSTEP] ;175.125
        idiv      ecx                                           ;175.120
        test      edx, edx                                      ;175.138
        je        .B1.156       ; Prob 50%                      ;175.138
        jmp       .B1.152       ; Prob 100%                     ;175.138
                                ; LOE esi
.B1.350:                        ; Preds .B1.146 .B1.147         ; Infreq
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ITERATION], 0 ;169.70
        jne       .B1.148       ; Prob 50%                      ;169.70
                                ; LOE esi
.B1.351:                        ; Preds .B1.350                 ; Infreq
        mov       ecx, DWORD PTR [-188+ebp]                     ;169.55
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TDSPSTEP] ;169.86
        lea       eax, DWORD PTR [-1+ecx]                       ;169.86
        cdq                                                     ;169.81
        idiv      edi                                           ;169.81
        test      edx, edx                                      ;169.99
        jne       .B1.148       ; Prob 50%                      ;169.99
                                ; LOE ecx esi
.B1.352:                        ; Preds .B1.351                 ; Infreq
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NINTS] ;169.108
        sub       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_BEGINT+32] ;169.113
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_BEGINT] ;169.108
        cvtsi2ss  xmm1, ecx                                     ;169.108
        movss     xmm0, DWORD PTR [4+eax+edx*4]                 ;169.113
        divss     xmm0, DWORD PTR [_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT] ;169.128
        comiss    xmm0, xmm1                                    ;169.110
        jb        .B1.148       ; Prob 78%                      ;169.110
                                ; LOE esi
.B1.353:                        ; Preds .B1.352                 ; Infreq
        lea       eax, DWORD PTR [-188+ebp]                     ;170.14
        push      eax                                           ;170.14
        call      _GENERATE_DEMAND                              ;170.14
                                ; LOE
.B1.457:                        ; Preds .B1.353                 ; Infreq
        add       esp, 4                                        ;170.14
                                ; LOE
.B1.354:                        ; Preds .B1.457                 ; Infreq
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_RUNMODE] ;175.22
        jmp       .B1.148       ; Prob 100%                     ;175.22
                                ; LOE esi
.B1.356:                        ; Preds .B1.274                 ; Infreq
        inc       edx                                           ;154.13
        cmp       edx, DWORD PTR [-240+ebp]                     ;154.13
        jb        .B1.270       ; Prob 82%                      ;154.13
        jmp       .B1.136       ; Prob 100%                     ;154.13
                                ; LOE edx
.B1.358:                        ; Preds .B1.270                 ; Infreq
        mov       edi, 1                                        ;
        jmp       .B1.274       ; Prob 100%                     ;
                                ; LOE edx edi
.B1.362:                        ; Preds .B1.102                 ; Infreq
        mov       eax, 1                                        ;
        mov       DWORD PTR [-408+ebp], eax                     ;
        jmp       .B1.113       ; Prob 100%                     ;
                                ; LOE
.B1.364:                        ; Preds .B1.277                 ; Infreq
        cmp       DWORD PTR [-452+ebp], 8                       ;136.17
        jl        .B1.375       ; Prob 10%                      ;136.17
                                ; LOE eax edx esi edi
.B1.365:                        ; Preds .B1.364                 ; Infreq
        mov       ecx, DWORD PTR [-488+ebp]                     ;136.17
        mov       DWORD PTR [-456+ebp], ecx                     ;136.17
        pxor      xmm0, xmm0                                    ;
        mov       ecx, DWORD PTR [-484+ebp]                     ;
        mov       DWORD PTR [-492+ebp], 0                       ;136.17
        mov       DWORD PTR [-516+ebp], eax                     ;
        mov       DWORD PTR [-512+ebp], edx                     ;
        add       ecx, esi                                      ;
        mov       DWORD PTR [-496+ebp], ecx                     ;
        mov       ecx, DWORD PTR [-500+ebp]                     ;
        mov       DWORD PTR [-508+ebp], edi                     ;
        mov       edx, DWORD PTR [-496+ebp]                     ;
        mov       edi, DWORD PTR [-488+ebp]                     ;
        add       ecx, esi                                      ;
        mov       eax, ecx                                      ;
        mov       ecx, DWORD PTR [-492+ebp]                     ;
                                ; LOE eax edx ecx esi edi xmm0
.B1.366:                        ; Preds .B1.366 .B1.365         ; Infreq
        movups    XMMWORD PTR [eax+ecx*4], xmm0                 ;136.17
        movups    XMMWORD PTR [16+edx+ecx*4], xmm0              ;136.17
        add       ecx, 8                                        ;136.17
        cmp       ecx, edi                                      ;136.17
        jb        .B1.366       ; Prob 82%                      ;136.17
                                ; LOE eax edx ecx esi edi xmm0
.B1.367:                        ; Preds .B1.366                 ; Infreq
        mov       eax, DWORD PTR [-516+ebp]                     ;
        mov       edx, DWORD PTR [-512+ebp]                     ;
        mov       edi, DWORD PTR [-508+ebp]                     ;
                                ; LOE eax edx esi edi
.B1.368:                        ; Preds .B1.367 .B1.375         ; Infreq
        mov       ecx, DWORD PTR [-456+ebp]                     ;136.17
        cmp       ecx, DWORD PTR [-452+ebp]                     ;136.17
        jae       .B1.279       ; Prob 10%                      ;136.17
                                ; LOE eax edx ecx esi edi cl ch
.B1.369:                        ; Preds .B1.368                 ; Infreq
        mov       DWORD PTR [-516+ebp], eax                     ;
        mov       DWORD PTR [-508+ebp], edi                     ;
        xor       edi, edi                                      ;
        mov       eax, ecx                                      ;
        mov       ecx, DWORD PTR [-452+ebp]                     ;
                                ; LOE eax edx ecx esi edi
.B1.370:                        ; Preds .B1.370 .B1.369         ; Infreq
        mov       DWORD PTR [edx+eax*4], edi                    ;136.17
        inc       eax                                           ;136.17
        cmp       eax, ecx                                      ;136.17
        jb        .B1.370       ; Prob 82%                      ;136.17
                                ; LOE eax edx ecx esi edi
.B1.371:                        ; Preds .B1.370                 ; Infreq
        mov       edi, DWORD PTR [-508+ebp]                     ;
        inc       edi                                           ;136.17
        mov       eax, DWORD PTR [-516+ebp]                     ;
        mov       ecx, DWORD PTR [-480+ebp]                     ;136.17
        add       eax, ecx                                      ;136.17
        add       edx, ecx                                      ;136.17
        add       esi, ecx                                      ;136.17
        cmp       edi, DWORD PTR [-476+ebp]                     ;136.17
        jb        .B1.277       ; Prob 82%                      ;136.17
        jmp       .B1.102       ; Prob 100%                     ;136.17
                                ; LOE eax edx esi edi
.B1.375:                        ; Preds .B1.364                 ; Infreq
        xor       ecx, ecx                                      ;136.17
        mov       DWORD PTR [-456+ebp], ecx                     ;136.17
        jmp       .B1.368       ; Prob 100%                     ;136.17
                                ; LOE eax edx esi edi
.B1.376:                        ; Preds .B1.81                  ; Infreq
        inc       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ISIGCOUNT]  ;116.14
        jmp       .B1.84        ; Prob 100%                     ;116.14
                                ; LOE xmm2
.B1.377:                        ; Preds .B1.10                  ; Infreq
        mov       eax, DWORD PTR [-152+ebp]                     ;68.10
        cdq                                                     ;68.10
        mov       ecx, DWORD PTR [-72+ebp]                      ;68.10
        idiv      ecx                                           ;68.10
        test      edx, edx                                      ;68.29
        je        .B1.14        ; Prob 50%                      ;68.29
                                ; LOE esi edi
.B1.378:                        ; Preds .B1.71 .B1.377          ; Infreq
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ICALLKSP], 0 ;95.7
        jmp       .B1.74        ; Prob 100%                     ;95.7
                                ; LOE esi
.B1.458:                        ; Preds .B1.3                   ; Infreq
        lea       edi, DWORD PTR [-440+ebp]                     ;62.22
        jmp       .B1.247       ; Prob 100%                     ;62.22
        ALIGN     16
                                ; LOE edx esi edi
; mark_end;
_SIM_MAIN_LOOP ENDP
_TEXT	ENDS
_BSS	SEGMENT  DWORD PUBLIC FLAT  'BSS'
	ALIGN 004H
_SIM_MAIN_LOOP$PRINTSTR1.0.1	DB ?	; pad
	ORG $+78	; pad
	DB ?	; pad
	DD 4 DUP (0H)	; pad
_SIM_MAIN_LOOP$PRINTSTR2.0.1	DB ?	; pad
	ORG $+78	; pad
	DB ?	; pad
_SIM_MAIN_LOOP$ICOUNT_STOP.0.1	DD 1 DUP (0H)	; pad
_BSS	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
SIM_MAIN_LOOP$format_pack.0.1	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	2
	DB	0
	DB	84
	DB	58
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	1
	DB	1
	DB	0
	DB	0
	DB	0
	DB	6
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	10
	DB	0
	DB	32
	DB	84
	DB	111
	DB	116
	DB	32
	DB	86
	DB	101
	DB	104
	DB	58
	DB	32
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	9
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	6
	DB	0
	DB	32
	DB	71
	DB	101
	DB	110
	DB	58
	DB	32
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	8
	DB	0
	DB	32
	DB	79
	DB	117
	DB	116
	DB	95
	DB	110
	DB	58
	DB	32
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	7
	DB	0
	DB	32
	DB	79
	DB	117
	DB	116
	DB	95
	DB	116
	DB	58
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	6
	DB	0
	DB	32
	DB	73
	DB	110
	DB	95
	DB	118
	DB	58
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	2
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	1
	DB	1
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
_DATA	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
__NLITPACK_3.0.1	DD	1
__NLITPACK_2.0.1	DD	500
__NLITPACK_0.0.1	DD	20
__NLITPACK_1.0.1	DD	395
__STRLITPACK_47.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_48.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_49.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_50.0.1	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_51.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_52.0.1	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_53.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_54.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_55.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_56.0.1	DB	7
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
..1..TPKT.5_0	DD	OFFSET FLAT: ..1.5_0.TAG.0
	DD	OFFSET FLAT: ..1.5_0.TAG.1
	DD	OFFSET FLAT: ..1.5_0.TAG.2
	DD	OFFSET FLAT: ..1.5_0.TAG.3
	DD	OFFSET FLAT: ..1.5_0.TAG.4
	DD	OFFSET FLAT: ..1.5_0.TAG.5
	DD	OFFSET FLAT: ..1.5_0.TAG.6
__STRLITPACK_57.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_59.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_58.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_60.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_61.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_62.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_63.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__NLITPACK_4.0.1	DD	16
__STRLITPACK_64.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_65.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_66.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_67.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_68.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_69.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_70.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_71.0.1	DB	56
	DB	4
	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__NLITPACK_8.0.1	DD	80
__NLITPACK_7.0.1	DD	400
__NLITPACK_6.0.1	DD	5
__NLITPACK_5.0.1	DD	300
__NLITPACK_10.0.1	DD	0
__NLITPACK_11.0.1	DD	800
__NLITPACK_9.0.1	DD	445
__STRLITPACK_72.0.1	DB	56
	DB	4
	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__NLITPACK_14.0.1	DD	200
__NLITPACK_13.0.1	DD	450
__NLITPACK_12.0.1	DD	370
__STRLITPACK_73.0.1	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_74.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_75.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_76.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_77.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_78.0.1	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_79.0.1	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	26
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_80.0.1	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_81.0.1	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_82.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _SIM_MAIN_LOOP
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _PRINTSCREEN
; mark_begin;
       ALIGN     16
	PUBLIC _PRINTSCREEN
_PRINTSCREEN	PROC NEAR 
; parameter 1: eax
; parameter 2: edx
; parameter 3: ecx
; parameter 4: 36 + esp
; parameter 5: 40 + esp
; parameter 6: 44 + esp
; parameter 7: 48 + esp
; parameter 8: 52 + esp
; parameter 9: 56 + esp
.B2.1:                          ; Preds .B2.0
        mov       eax, DWORD PTR [4+esp]                        ;383.12
        mov       edx, DWORD PTR [8+esp]                        ;383.12
        mov       ecx, DWORD PTR [12+esp]                       ;383.12
	PUBLIC _PRINTSCREEN.
_PRINTSCREEN.::
        push      esi                                           ;383.12
        push      edi                                           ;383.12
        push      ebx                                           ;383.12
        push      ebp                                           ;383.12
        push      esi                                           ;383.12
        mov       esi, eax                                      ;383.12
        mov       eax, DWORD PTR [52+esp]                       ;383.12
        mov       ebp, ecx                                      ;383.12
        mov       ebx, edx                                      ;383.12
        cmp       DWORD PTR [eax], 0                            ;392.12
        jle       .B2.4         ; Prob 41%                      ;392.12
                                ; LOE ebx ebp esi
.B2.2:                          ; Preds .B2.1
        push      10485760                                      ;393.12
        call      __setcolorrgb                                 ;393.12
                                ; LOE ebx ebp esi
.B2.3:                          ; Preds .B2.2
        mov       edi, DWORD PTR [40+esp]                       ;383.12
        mov       eax, DWORD PTR [44+esp]                       ;383.12
        mov       ecx, DWORD PTR [ebx]                          ;394.12
        mov       edx, DWORD PTR [edi]                          ;394.12
        mov       edi, DWORD PTR [48+esp]                       ;394.12
        neg       edx                                           ;394.12
        add       edx, ecx                                      ;394.12
        mov       DWORD PTR [4+esp], esi                        ;
        add       ecx, DWORD PTR [edi]                          ;394.12
        mov       edi, DWORD PTR [52+esp]                       ;394.12
        mov       eax, DWORD PTR [eax]                          ;394.12
        mov       esi, DWORD PTR [ebp]                          ;394.12
        neg       eax                                           ;394.12
        add       eax, esi                                      ;394.12
        add       esi, DWORD PTR [edi]                          ;394.12
        push      esi                                           ;394.12
        push      ecx                                           ;394.12
        push      eax                                           ;394.12
        push      edx                                           ;394.12
        push      3                                             ;394.12
        mov       esi, DWORD PTR [24+esp]                       ;394.12
        call      __rectangle                                   ;394.12
                                ; LOE ebx ebp esi
.B2.11:                         ; Preds .B2.3
        add       esp, 24                                       ;394.12
                                ; LOE ebx ebp esi
.B2.4:                          ; Preds .B2.11 .B2.1
        push      14                                            ;397.10
        push      OFFSET FLAT: __STRLITPACK_92                  ;397.10
        call      __f_setfont                                   ;397.10
                                ; LOE ebx ebp esi
.B2.5:                          ; Preds .B2.4
        push      7                                             ;398.8
        call      __setcolor                                    ;398.8
                                ; LOE ebx ebp esi
.B2.6:                          ; Preds .B2.5
        mov       eax, DWORD PTR [ebx]                          ;400.13
        lea       ebx, DWORD PTR [12+esp]                       ;400.6
        mov       edx, DWORD PTR [ebp]                          ;400.24
        lea       ecx, DWORD PTR [14+esp]                       ;400.6
        mov       WORD PTR [12+esp], ax                         ;400.6
        mov       WORD PTR [14+esp], dx                         ;400.6
        push      OFFSET FLAT: _PRINTSCREEN$POS.0.2             ;400.6
        push      ecx                                           ;400.6
        push      ebx                                           ;400.6
        call      __f_moveto                                    ;400.6
                                ; LOE esi
.B2.7:                          ; Preds .B2.6
        push      80                                            ;402.6
        push      esi                                           ;402.6
        call      __f_outgtext                                  ;402.6
                                ; LOE
.B2.8:                          ; Preds .B2.7
        add       esp, 36                                       ;404.1
        pop       ebp                                           ;404.1
        pop       ebx                                           ;404.1
        pop       edi                                           ;404.1
        pop       esi                                           ;404.1
        ret                                                     ;404.1
        ALIGN     16
                                ; LOE
; mark_end;
_PRINTSCREEN ENDP
_TEXT	ENDS
_BSS	SEGMENT  DWORD PUBLIC FLAT  'BSS'
	DD 1 DUP (0H)	; pad
_PRINTSCREEN$POS.0.2	DD 1 DUP (0H)	; pad
_BSS	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _PRINTSCREEN
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_40	DB	105
	DB	115
	DB	111
	DB	44
	DB	97
	DB	103
	DB	103
	DB	116
	DB	105
	DB	109
	DB	101
	DB	44
	DB	109
	DB	95
	DB	100
	DB	121
	DB	110
	DB	117
	DB	115
	DB	116
	DB	95
	DB	110
	DB	101
	DB	116
	DB	119
	DB	111
	DB	114
	DB	107
	DB	95
	DB	97
	DB	114
	DB	99
	DB	95
	DB	100
	DB	101
	DB	40
	DB	109
	DB	95
	DB	100
	DB	121
	DB	110
	DB	117
	DB	115
	DB	116
	DB	95
	DB	110
	DB	101
	DB	116
	DB	119
	DB	111
	DB	114
	DB	107
	DB	95
	DB	97
	DB	114
	DB	99
	DB	95
	DB	110
	DB	100
	DB	101
	DB	40
	DB	105
	DB	115
	DB	111
	DB	41
	DB	37
	DB	102
	DB	111
	DB	114
	DB	116
	DB	111
	DB	98
	DB	97
	DB	99
	DB	107
	DB	108
	DB	105
	DB	110
	DB	107
	DB	41
	DB	37
	DB	108
	DB	105
	DB	110
	DB	107
	DB	95
	DB	113
	DB	117
	DB	101
	DB	117
	DB	101
	DB	40
	DB	97
	DB	103
	DB	103
	DB	116
	DB	105
	DB	109
	DB	101
	DB	41
	DB	37
	DB	112
	DB	115
	DB	105
	DB	122
	DB	101
	DB	0
	DD 1 DUP (0H)	; pad
	DB 1 DUP ( 0H)	; pad
_2il0floatpacket.22	DD	042c80000H,042c80000H,042c80000H,042c80000H
_2il0floatpacket.29	DD	07fffffffH,000000000H,000000000H,000000000H
__STRLITPACK_46	DB	83
	DB	105
	DB	109
	DB	117
	DB	108
	DB	97
	DB	116
	DB	105
	DB	111
	DB	110
	DB	32
	DB	78
	DB	111
	DB	119
	DB	32
	DB	82
	DB	117
	DB	110
	DB	110
	DB	105
	DB	110
	DB	103
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_44	DB	108
	DB	111
	DB	111
	DB	112
	DB	32
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_42	DB	97
	DB	103
	DB	103
	DB	116
	DB	105
	DB	109
	DB	101
	DB	0
__STRLITPACK_38	DB	97
	DB	102
	DB	116
	DB	101
	DB	114
	DB	32
	DB	108
	DB	105
	DB	110
	DB	107
	DB	95
	DB	115
	DB	112
	DB	101
	DB	101
	DB	100
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_34	DB	67
	DB	65
	DB	76
	DB	76
	DB	32
	DB	100
	DB	101
	DB	109
	DB	97
	DB	110
	DB	100
	DB	32
	DB	103
	DB	101
	DB	110
	DB	101
	DB	114
	DB	97
	DB	116
	DB	105
	DB	111
	DB	110
	DB	44
	DB	32
	DB	112
	DB	101
	DB	110
	DB	97
	DB	108
	DB	116
	DB	121
	DB	32
	DB	97
	DB	110
	DB	100
	DB	32
	DB	107
	DB	101
	DB	112
	DB	0
__STRLITPACK_36	DB	97
	DB	108
	DB	108
	DB	111
	DB	99
	DB	97
	DB	116
	DB	101
	DB	32
	DB	97
	DB	99
	DB	99
	DB	117
	DB	118
	DB	111
	DB	108
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_32	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_30	DB	84
	DB	104
	DB	101
	DB	32
	DB	112
	DB	114
	DB	111
	DB	103
	DB	114
	DB	97
	DB	109
	DB	32
	DB	114
	DB	101
	DB	97
	DB	99
	DB	104
	DB	101
	DB	100
	DB	32
	DB	116
	DB	104
	DB	101
	DB	32
	DB	101
	DB	110
	DB	100
	DB	32
	DB	111
	DB	102
	DB	32
	DB	115
	DB	105
	DB	109
	DB	117
	DB	108
	DB	97
	DB	116
	DB	105
	DB	111
	DB	110
	DB	32
	DB	98
	DB	101
	DB	99
	DB	97
	DB	117
	DB	115
	DB	101
	DB	58
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_28	DB	97
	DB	108
	DB	108
	DB	32
	DB	116
	DB	97
	DB	114
	DB	103
	DB	101
	DB	116
	DB	32
	DB	118
	DB	101
	DB	104
	DB	105
	DB	99
	DB	108
	DB	101
	DB	115
	DB	32
	DB	104
	DB	97
	DB	118
	DB	101
	DB	32
	DB	114
	DB	101
	DB	97
	DB	99
	DB	104
	DB	101
	DB	100
	DB	32
	DB	116
	DB	104
	DB	101
	DB	105
	DB	114
	DB	32
	DB	100
	DB	101
	DB	115
	DB	116
	DB	105
	DB	110
	DB	97
	DB	116
	DB	105
	DB	111
	DB	110
	DB	115
	DB	0
__STRLITPACK_26	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_24	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_22	DB	84
	DB	104
	DB	101
	DB	32
	DB	112
	DB	114
	DB	111
	DB	103
	DB	114
	DB	97
	DB	109
	DB	32
	DB	114
	DB	101
	DB	97
	DB	99
	DB	104
	DB	101
	DB	100
	DB	32
	DB	116
	DB	104
	DB	101
	DB	32
	DB	101
	DB	110
	DB	100
	DB	32
	DB	111
	DB	102
	DB	32
	DB	115
	DB	105
	DB	109
	DB	117
	DB	108
	DB	97
	DB	116
	DB	105
	DB	111
	DB	110
	DB	32
	DB	98
	DB	101
	DB	99
	DB	97
	DB	117
	DB	115
	DB	101
	DB	58
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_20	DB	116
	DB	104
	DB	101
	DB	114
	DB	101
	DB	32
	DB	97
	DB	114
	DB	101
	DB	32
	DB	110
	DB	111
	DB	32
	DB	116
	DB	97
	DB	114
	DB	103
	DB	101
	DB	116
	DB	32
	DB	118
	DB	101
	DB	104
	DB	105
	DB	99
	DB	108
	DB	101
	DB	115
	DB	32
	DB	103
	DB	101
	DB	116
	DB	116
	DB	105
	DB	110
	DB	103
	DB	32
	DB	111
	DB	117
	DB	116
	DB	32
	DB	111
	DB	102
	DB	32
	DB	110
	DB	101
	DB	116
	DB	119
	DB	111
	DB	114
	DB	107
	DB	0
__STRLITPACK_17	DB	102
	DB	111
	DB	114
	DB	0
__STRLITPACK_16	DB	109
	DB	105
	DB	110
	DB	117
	DB	116
	DB	101
	DB	115
	DB	0
__STRLITPACK_14	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_11	DB	73
	DB	116
	DB	101
	DB	114
	DB	97
	DB	116
	DB	105
	DB	111
	DB	110
	DB	32
	DB	32
	DB	0
__STRLITPACK_10	DB	82
	DB	117
	DB	110
	DB	110
	DB	105
	DB	110
	DB	103
	DB	32
	DB	83
	DB	105
	DB	109
	DB	117
	DB	108
	DB	97
	DB	116
	DB	105
	DB	111
	DB	110
	DB	46
	DB	46
	DB	46
	DB	32
	DB	67
	DB	117
	DB	114
	DB	114
	DB	101
	DB	110
	DB	116
	DB	32
	DB	84
	DB	105
	DB	109
	DB	101
	DB	32
	DB	40
	DB	109
	DB	105
	DB	110
	DB	41
	DB	61
	DB	32
	DB	32
	DB	0
__STRLITPACK_1	DB	115
	DB	105
	DB	109
	DB	95
	DB	116
	DB	101
	DB	114
	DB	109
	DB	105
	DB	110
	DB	97
	DB	116
	DB	101
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_0	DB	117
	DB	110
	DB	107
	DB	110
	DB	111
	DB	119
	DB	110
	DB	0
_2il0floatpacket.19	DD	042700000H
_2il0floatpacket.20	DD	03c23d70aH
_2il0floatpacket.21	DD	042c80000H
_2il0floatpacket.23	DD	03dcccccdH
_2il0floatpacket.24	DD	080000000H
_2il0floatpacket.25	DD	04b000000H
_2il0floatpacket.26	DD	03f000000H
_2il0floatpacket.27	DD	0bf000000H
_2il0floatpacket.28	DD	03f800000H
__STRLITPACK_92	DB	116
	DB	39
	DB	65
	DB	114
	DB	105
	DB	97
	DB	108
	DB	39
	DB	104
	DB	50
	DB	48
	DB	119
	DB	55
	DB	118
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
EXTRN	_DYNUST_MAIN_MODULE_mp_SIMPERIOD:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NOUT_TAG_I:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NOUT_TAG:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NOUT_NONTAG_I:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NOUT_NONTAG:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_JUSTVEH_I:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_STOP_COUNT_THRESHOLD:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_OLDNUMCARS:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_DETECTOR_LENGTH:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_OCCUP:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_VKFUNC:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_RAMP_PAR:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_RAMP_END:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_RAMP_START:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_DETECTOR_RAMP:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_DET_LINK:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NOOFFLOWMODEL:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_DETECTOR:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_DLLFLAGRM:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NRATE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ALLOUT:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_MAXVEHICLESFILE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_JUSTVEH:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NUMNTAG:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NUMCARS:BYTE
EXTRN	_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ENDTM:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_STARTTM:BYTE
EXTRN	_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_JTOTAL:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_JRESTORE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_IPINIT:BYTE
EXTRN	_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND:BYTE
EXTRN	_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHAIN:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_BACKPOINTR:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NOOFNODES:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_MASTERDEST:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NZONES:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NOOF_MASTER_DESTINATIONS:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_CALLKSPREADVEH:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_IER_OK:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_VMS_NUM:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_READHISTARR:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NINTS:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_BEGINT:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_RUNMODE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_UETRAVELPENALTY:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_UETRAVELTIME:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_INCIL:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TRAVELTIME:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_INCI_NUM:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ITI_NUTMP:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_UETIMEFLAGR:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_WORKZONENUM:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ISIGCOUNT:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_STRTSIG:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ISIG:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_CALLAVG:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ITERATION:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TIME_NOW:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ICALLKSP:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TDSPSTEP:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NOOFARCS:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_SIMPERAGG:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_LOGOUT:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ENDTIME:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_STARTTIME:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_DEC_NUM:BYTE
_DATA	ENDS
EXTRN	__f_outgtext:PROC
EXTRN	__f_moveto:PROC
EXTRN	__setcolor:PROC
EXTRN	__f_setfont:PROC
EXTRN	__rectangle:PROC
EXTRN	__setcolorrgb:PROC
EXTRN	_for_dealloc_allocatable:PROC
EXTRN	_for_close:PROC
EXTRN	_for_open:PROC
EXTRN	_for_write_seq_fmt_xmit:PROC
EXTRN	_for_write_seq_fmt:PROC
EXTRN	_for_trim:PROC
EXTRN	_for_concat:PROC
EXTRN	_for_write_int_fmt:PROC
EXTRN	_for_alloc_allocatable:PROC
EXTRN	_for_check_mult_overflow:PROC
EXTRN	_for_write_seq_lis_xmit:PROC
EXTRN	_for_write_seq_lis:PROC
EXTRN	_for_cpystr:PROC
EXTRN	_MEMORY_MODULE_mp_MEMALLOCATE_ASSIGNMENT:PROC
EXTRN	_WORKZONE_MODULE_mp_WZ_SCAN:PROC
EXTRN	_INCIDENT_MODULE_mp_INCI_SCAN:PROC
EXTRN	_VARIABLE_MESSAGE_SIGN_MODULE_mp_VMS_MAIN:PROC
EXTRN	_DYNUST_NETWORK_MODULE_mp_GETFLINKFROMNODE:PROC
EXTRN	_NEXTA_STAT:PROC
EXTRN	_CAL_PENALTY:PROC
EXTRN	_GENERATE_DEMAND:PROC
EXTRN	_TDSP_MAIN:PROC
EXTRN	_RETRIEVE_VEH_PATH_ASTAR:PROC
EXTRN	_RETRIEVE_NEXT_LINK:PROC
EXTRN	_READ_VEHICLES:PROC
EXTRN	_SIM_MASTER:PROC
EXTRN	_RAMP_METERING:PROC
EXTRN	_RAMP_METER_DLL:PROC
EXTRN	__intel_fast_memset:PROC
EXTRN	__alloca_probe:PROC
EXTRN	__fltused:BYTE
	INCLUDELIB <ifconsol>
	INCLUDELIB <libifcoremt>
	INCLUDELIB <libifport>
	INCLUDELIB <libmmt>
	INCLUDELIB <LIBCMT>
	INCLUDELIB <libirc>
	INCLUDELIB <svml_dispmt>
	INCLUDELIB <OLDNAMES>
	END
