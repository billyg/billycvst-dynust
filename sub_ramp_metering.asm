; mark_description "Intel(R) Visual Fortran Compiler XE for applications running on IA-32, Version 13.1.1.171 Build 20130313";
; mark_description "-S";
	.686P
 	.387
	OPTION DOTNAME
	ASSUME	CS:FLAT,DS:FLAT,SS:FLAT
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
TXTST0:
; -- Begin  _RAMP_METERING
; mark_begin;
IF @Version GE 800
  .MMX
ELSEIF @Version GE 612
  .MMX
  MMWORD TEXTEQU <QWORD>
ENDIF
IF @Version GE 800
  .XMM
ELSEIF @Version GE 614
  .XMM
  XMMWORD TEXTEQU <OWORD>
ENDIF
       ALIGN     16
	PUBLIC _RAMP_METERING
_RAMP_METERING	PROC NEAR 
; parameter 1: 8 + ebp
.B1.1:                          ; Preds .B1.0
        push      ebp                                           ;1.12
        mov       ebp, esp                                      ;1.12
        and       esp, -16                                      ;1.12
        sub       esp, 128                                      ;1.12
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DEC_NUM] ;29.7
        test      ecx, ecx                                      ;29.7
        jle       .B1.13        ; Prob 2%                       ;29.7
                                ; LOE ecx ebx esi edi
.B1.2:                          ; Preds .B1.1
        mov       eax, 1                                        ;
        pxor      xmm2, xmm2                                    ;
        movss     xmm0, DWORD PTR [_2il0floatpacket.7]          ;44.29
        movss     DWORD PTR [96+esp], xmm2                      ;44.29
        mov       DWORD PTR [116+esp], ecx                      ;44.29
        mov       DWORD PTR [92+esp], esi                       ;44.29
        mov       DWORD PTR [88+esp], edi                       ;44.29
        mov       DWORD PTR [76+esp], ebx                       ;44.29
        mov       ebx, eax                                      ;44.29
                                ; LOE ebx
.B1.3:                          ; Preds .B1.11 .B1.2
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DET_LINK+32] ;30.8
        neg       ecx                                           ;30.8
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DETECTOR_RAMP+32] ;31.5
        add       ecx, ebx                                      ;30.8
        shl       edi, 2                                        ;31.5
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DETECTOR_RAMP] ;31.5
        neg       edi                                           ;31.5
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DET_LINK] ;30.8
        mov       eax, DWORD PTR [edx+ecx*4]                    ;30.8
        lea       edx, DWORD PTR [esi+ebx*4]                    ;31.5
        mov       ecx, DWORD PTR [edi+edx]                      ;31.5
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_RAMP_START+32] ;33.8
        neg       edx                                           ;33.29
        mov       esi, DWORD PTR [8+ebp]                        ;33.8
        add       edx, ebx                                      ;33.29
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_RAMP_START] ;33.8
        mov       DWORD PTR [120+esp], ecx                      ;31.5
        movss     xmm1, DWORD PTR [esi]                         ;33.8
        comiss    xmm1, DWORD PTR [edi+edx*4]                   ;33.12
        jb        .B1.10        ; Prob 50%                      ;33.12
                                ; LOE eax ebx xmm1
.B1.4:                          ; Preds .B1.3
        mov       ecx, ebx                                      ;33.29
        sub       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_RAMP_END+32] ;33.29
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_RAMP_END] ;33.12
        movss     xmm0, DWORD PTR [edx+ecx*4]                   ;33.39
        comiss    xmm0, xmm1                                    ;33.35
        jb        .B1.10        ; Prob 50%                      ;33.35
                                ; LOE eax ebx
.B1.5:                          ; Preds .B1.4
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_RAMP_PAR+40] ;34.9
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_RAMP_PAR+44] ;34.9
        imul      ecx, esi                                      ;34.22
        mov       DWORD PTR [104+esp], esi                      ;34.9
        shl       esi, 2                                        ;34.9
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_RAMP_PAR] ;34.6
        neg       esi                                           ;34.22
        add       esi, ecx                                      ;34.22
        neg       esi                                           ;34.22
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_RAMP_PAR+32] ;34.6
        lea       edx, DWORD PTR [edx+ebx*4]                    ;34.22
        shl       edi, 2                                        ;34.22
        add       esi, edx                                      ;34.22
        sub       esi, edi                                      ;34.22
        mov       DWORD PTR [100+esp], edx                      ;34.22
        mov       DWORD PTR [112+esp], ecx                      ;34.22
        mov       DWORD PTR [108+esp], edi                      ;34.22
        movss     xmm0, DWORD PTR [esi]                         ;34.9
        ucomiss   xmm0, DWORD PTR [_2il0floatpacket.1]          ;34.22
        jp        .B1.6         ; Prob 0%                       ;34.22
        je        .B1.34        ; Prob 16%                      ;34.22
                                ; LOE eax ebx xmm0
.B1.6:                          ; Preds .B1.5
        ucomiss   xmm0, DWORD PTR [_2il0floatpacket.7]          ;44.29
        jp        .B1.7         ; Prob 0%                       ;44.29
        je        .B1.33        ; Prob 16%                      ;44.29
                                ; LOE ebx xmm0
.B1.7:                          ; Preds .B1.6
        movss     xmm1, DWORD PTR [_2il0floatpacket.2]          ;48.29
        ucomiss   xmm0, xmm1                                    ;48.29
        jp        .B1.8         ; Prob 0%                       ;48.29
        je        .B1.14        ; Prob 16%                      ;48.29
                                ; LOE ebx
.B1.8:                          ; Preds .B1.7
        mov       DWORD PTR [32+esp], 0                         ;75.11
        lea       ecx, DWORD PTR [32+esp]                       ;75.11
        mov       DWORD PTR [80+esp], 22                        ;75.11
        lea       edx, DWORD PTR [80+esp]                       ;75.11
        mov       DWORD PTR [84+esp], OFFSET FLAT: __STRLITPACK_0 ;75.11
        push      32                                            ;75.11
        push      edx                                           ;75.11
        push      OFFSET FLAT: __STRLITPACK_9.0.1               ;75.11
        push      -2088435968                                   ;75.11
        push      -1                                            ;75.11
        push      ecx                                           ;75.11
        call      _for_write_seq_lis                            ;75.11
                                ; LOE ebx
.B1.37:                         ; Preds .B1.8
        add       esp, 24                                       ;75.11
                                ; LOE ebx
.B1.9:                          ; Preds .B1.37
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_OCCUP] ;81.10
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_OCCUP+32] ;81.10
        lea       edx, DWORD PTR [edx+ebx*4]                    ;
        jmp       .B1.11        ; Prob 100%                     ;
                                ; LOE eax edx ebx
.B1.10:                         ; Preds .B1.3 .B1.4
        mov       edx, DWORD PTR [120+esp]                      ;79.11
        sub       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;79.11
        imul      edi, edx, 900                                 ;79.11
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;79.11
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_OCCUP] ;81.10
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_OCCUP+32] ;81.10
        mov       ecx, DWORD PTR [436+esi+edi]                  ;79.11
        mov       DWORD PTR [432+esi+edi], ecx                  ;79.11
        lea       edx, DWORD PTR [edx+ebx*4]                    ;
                                ; LOE eax edx ebx
.B1.11:                         ; Preds .B1.10 .B1.34 .B1.33 .B1.27 .B1.9
                                ;      
        shl       eax, 2                                        ;81.10
        inc       ebx                                           ;82.7
        neg       eax                                           ;81.10
        cmp       ebx, DWORD PTR [116+esp]                      ;82.7
        mov       DWORD PTR [eax+edx], 0                        ;81.10
        jle       .B1.3         ; Prob 82%                      ;82.7
                                ; LOE ebx
.B1.12:                         ; Preds .B1.11
        mov       esi, DWORD PTR [92+esp]                       ;
        mov       edi, DWORD PTR [88+esp]                       ;
        mov       ebx, DWORD PTR [76+esp]                       ;
                                ; LOE ebx esi edi
.B1.13:                         ; Preds .B1.12 .B1.1
        mov       esp, ebp                                      ;84.1
        pop       ebp                                           ;84.1
        ret                                                     ;84.1
                                ; LOE
.B1.14:                         ; Preds .B1.7                   ; Infreq
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DETECTOR+40] ;50.39
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DETECTOR+44] ;50.39
        imul      edi, esi                                      ;50.22
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DETECTOR] ;50.22
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DETECTOR+32] ;50.22
        sub       ecx, edi                                      ;50.22
        shl       edx, 2                                        ;50.22
        lea       edi, DWORD PTR [esi+esi]                      ;50.22
        sub       edi, edx                                      ;50.22
        lea       esi, DWORD PTR [esi+esi*2]                    ;50.22
        sub       esi, edx                                      ;50.22
        add       edi, ecx                                      ;50.22
        add       ecx, esi                                      ;50.22
        push      OFFSET FLAT: __NLITPACK_0.0.1                 ;50.22
        lea       eax, DWORD PTR [edi+ebx*4]                    ;50.22
        lea       edx, DWORD PTR [ecx+ebx*4]                    ;50.22
        push      edx                                           ;50.22
        push      eax                                           ;50.22
        call      _DYNUST_NETWORK_MODULE_mp_GETFLINKFROMNODE    ;50.22
                                ; LOE eax ebx
.B1.38:                         ; Preds .B1.14                  ; Infreq
        add       esp, 12                                       ;50.22
        mov       DWORD PTR [20+esp], eax                       ;50.22
                                ; LOE ebx
.B1.15:                         ; Preds .B1.38                  ; Infreq
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DETECTOR] ;52.11
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DETECTOR+44] ;52.11
        neg       eax                                           ;52.11
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DETECTOR+32] ;52.11
        add       eax, 2                                        ;52.11
        shl       esi, 2                                        ;52.11
        lea       edi, DWORD PTR [ecx+ebx*4]                    ;52.11
        neg       esi                                           ;52.11
        imul      eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DETECTOR+40] ;52.11
        add       edi, esi                                      ;52.11
        mov       ecx, DWORD PTR [edi+eax]                      ;52.19
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_BACKPOINTR] ;52.11
        sub       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_BACKPOINTR+32] ;52.11
        mov       esi, DWORD PTR [4+edx+ecx*4]                  ;52.45
        dec       esi                                           ;52.11
        mov       eax, DWORD PTR [edx+ecx*4]                    ;52.11
        mov       edx, esi                                      ;52.11
        sub       edx, eax                                      ;52.11
        inc       edx                                           ;52.11
        mov       DWORD PTR [28+esp], edx                       ;52.11
        cmp       esi, eax                                      ;52.11
        jl        .B1.32        ; Prob 11%                      ;52.11
                                ; LOE eax ebx
.B1.16:                         ; Preds .B1.15                  ; Infreq
        imul      edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32], 152 ;
        xor       esi, esi                                      ;
        imul      eax, eax, 152                                 ;
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;53.11
        mov       edi, ecx                                      ;
        sub       edi, edx                                      ;
        add       eax, ecx                                      ;
        sub       eax, edx                                      ;
        mov       DWORD PTR [68+esp], ebx                       ;
        mov       ecx, edi                                      ;
        mov       edx, esi                                      ;
        mov       ebx, DWORD PTR [28+esp]                       ;
                                ; LOE eax edx ecx ebx esi
.B1.17:                         ; Preds .B1.20 .B1.16           ; Infreq
        imul      edi, DWORD PTR [16+esi+eax], 152              ;53.14
        movsx     edi, WORD PTR [148+ecx+edi]                   ;53.14
        cmp       edi, 5                                        ;53.92
        je        .B1.20        ; Prob 16%                      ;53.92
                                ; LOE eax edx ecx ebx esi edi
.B1.18:                         ; Preds .B1.17                  ; Infreq
        cmp       edi, 3                                        ;53.180
        je        .B1.20        ; Prob 16%                      ;53.180
                                ; LOE eax edx ecx ebx esi edi
.B1.19:                         ; Preds .B1.18                  ; Infreq
        cmp       edi, 4                                        ;53.268
        jne       .B1.31        ; Prob 20%                      ;53.268
                                ; LOE eax edx ecx ebx esi
.B1.20:                         ; Preds .B1.19 .B1.18 .B1.17    ; Infreq
        inc       edx                                           ;52.11
        add       esi, 152                                      ;52.11
        cmp       edx, ebx                                      ;52.11
        jb        .B1.17        ; Prob 82%                      ;52.11
                                ; LOE eax edx ecx ebx esi
.B1.21:                         ; Preds .B1.20                  ; Infreq
        mov       ebx, DWORD PTR [68+esp]                       ;
        mov       DWORD PTR [24+esp], 0                         ;
                                ; LOE ebx
.B1.22:                         ; Preds .B1.21 .B1.32 .B1.31    ; Infreq
        mov       DWORD PTR [32+esp], 0                         ;59.13
        lea       esi, DWORD PTR [32+esp]                       ;59.13
        mov       DWORD PTR [esp], 29                           ;59.13
        lea       edx, DWORD PTR [esp]                          ;59.13
        mov       DWORD PTR [4+esp], OFFSET FLAT: __STRLITPACK_2 ;59.13
        push      32                                            ;59.13
        push      edx                                           ;59.13
        push      OFFSET FLAT: __STRLITPACK_5.0.1               ;59.13
        push      -2088435968                                   ;59.13
        push      911                                           ;59.13
        push      esi                                           ;59.13
        call      _for_write_seq_lis                            ;59.13
                                ; LOE ebx esi
.B1.23:                         ; Preds .B1.22                  ; Infreq
        mov       DWORD PTR [40+esp], ebx                       ;59.13
        lea       edx, DWORD PTR [40+esp]                       ;59.13
        push      edx                                           ;59.13
        push      OFFSET FLAT: __STRLITPACK_6.0.1               ;59.13
        push      esi                                           ;59.13
        call      _for_write_seq_lis_xmit                       ;59.13
                                ; LOE ebx esi
.B1.24:                         ; Preds .B1.23                  ; Infreq
        mov       DWORD PTR [44+esp], 13                        ;59.13
        lea       edx, DWORD PTR [44+esp]                       ;59.13
        mov       DWORD PTR [48+esp], OFFSET FLAT: __STRLITPACK_1 ;59.13
        push      edx                                           ;59.13
        push      OFFSET FLAT: __STRLITPACK_7.0.1               ;59.13
        push      esi                                           ;59.13
        call      _for_write_seq_lis_xmit                       ;59.13
                                ; LOE ebx
.B1.25:                         ; Preds .B1.24                  ; Infreq
        push      32                                            ;60.13
        xor       edx, edx                                      ;60.13
        push      edx                                           ;60.13
        push      edx                                           ;60.13
        push      -2088435968                                   ;60.13
        push      edx                                           ;60.13
        push      OFFSET FLAT: __STRLITPACK_8                   ;60.13
        call      _for_stop_core                                ;60.13
                                ; LOE ebx
.B1.39:                         ; Preds .B1.25                  ; Infreq
        add       esp, 72                                       ;60.13
                                ; LOE ebx
.B1.26:                         ; Preds .B1.39 .B1.31           ; Infreq
        imul      ecx, DWORD PTR [20+esp], 900                  ;50.11
        imul      edi, DWORD PTR [24+esp], 900                  ;62.11
        imul      edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32], 900 ;62.11
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;62.11
        add       ecx, esi                                      ;62.11
        add       edi, esi                                      ;62.11
        sub       ecx, edx                                      ;62.11
        sub       edi, edx                                      ;62.11
        mov       DWORD PTR [64+esp], edx                       ;62.11
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC] ;64.11
        movss     xmm0, DWORD PTR [648+ecx]                     ;62.66
        movsx     ecx, BYTE PTR [680+edi]                       ;63.16
        sub       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC+32] ;65.16
        movss     xmm1, DWORD PTR [648+edi]                     ;62.24
        shl       ecx, 5                                        ;65.16
        mulss     xmm1, DWORD PTR [_2il0floatpacket.3]          ;62.23
        mulss     xmm0, DWORD PTR [_2il0floatpacket.4]          ;62.65
        cmp       DWORD PTR [24+edx+ecx], 1                     ;65.16
        addss     xmm1, xmm0                                    ;62.11
        je        .B1.28        ; Prob 16%                      ;65.16
                                ; LOE edx ecx ebx esi xmm1
.B1.27:                         ; Preds .B1.29 .B1.30 .B1.26    ; Infreq
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_RAMP_PAR] ;72.11
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_RAMP_PAR+44] ;72.64
        pxor      xmm3, xmm3                                    ;72.11
        neg       eax                                           ;72.11
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_RAMP_PAR+32] ;72.11
        add       eax, 3                                        ;72.11
        shl       edx, 2                                        ;72.11
        lea       ecx, DWORD PTR [ecx+ebx*4]                    ;72.11
        neg       edx                                           ;72.11
        imul      eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_RAMP_PAR+40] ;72.11
        add       ecx, edx                                      ;72.11
        imul      edi, DWORD PTR [120+esp], 900                 ;72.11
        movss     xmm0, DWORD PTR [ecx+eax]                     ;72.64
        divss     xmm0, DWORD PTR [_2il0floatpacket.5]          ;72.263
        add       esi, edi                                      ;72.11
        sub       esi, DWORD PTR [64+esp]                       ;72.11
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_OCCUP+32] ;81.10
        movsx     edi, BYTE PTR [668+esi]                       ;72.271
        cvtsi2ss  xmm1, edi                                     ;72.85
        mulss     xmm1, xmm0                                    ;72.84
        movss     xmm2, DWORD PTR [436+esi]                     ;72.199
        movaps    xmm4, xmm1                                    ;72.11
        minss     xmm4, xmm2                                    ;72.11
        subss     xmm2, xmm1                                    ;72.249
        maxss     xmm3, xmm2                                    ;72.11
        mulss     xmm3, DWORD PTR [96+esp]                      ;72.188
        addss     xmm4, xmm3                                    ;72.11
        movss     DWORD PTR [432+esi], xmm4                     ;72.11
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_OCCUP] ;81.10
        lea       edx, DWORD PTR [esi+ebx*4]                    ;
        jmp       .B1.11        ; Prob 100%                     ;
                                ; LOE eax edx ebx
.B1.28:                         ; Preds .B1.26                  ; Infreq
        mov       edi, DWORD PTR [edx+ecx]                      ;66.26
        cvtsi2ss  xmm0, edi                                     ;66.26
        comiss    xmm0, xmm1                                    ;66.22
        jb        .B1.30        ; Prob 50%                      ;66.22
                                ; LOE edx ecx ebx esi edi xmm0 xmm1
.B1.29:                         ; Preds .B1.28                  ; Infreq
        movss     xmm0, DWORD PTR [_2il0floatpacket.7]          ;67.16
        movss     DWORD PTR [96+esp], xmm0                      ;67.16
        jmp       .B1.27        ; Prob 100%                     ;67.16
                                ; LOE ebx esi
.B1.30:                         ; Preds .B1.28                  ; Infreq
        mov       edx, DWORD PTR [12+edx+ecx]                   ;69.54
        subss     xmm1, xmm0                                    ;69.35
        sub       edx, edi                                      ;69.54
        cvtsi2ss  xmm0, edx                                     ;69.54
        divss     xmm1, xmm0                                    ;69.52
        sqrtss    xmm2, xmm1                                    ;69.88
        mulss     xmm2, xmm1                                    ;69.88
        movss     xmm1, DWORD PTR [_2il0floatpacket.7]          ;69.16
        subss     xmm1, xmm2                                    ;69.16
        movss     DWORD PTR [96+esp], xmm1                      ;69.16
        jmp       .B1.27        ; Prob 100%                     ;69.16
                                ; LOE ebx esi
.B1.31:                         ; Preds .B1.19                  ; Infreq
        mov       edx, DWORD PTR [16+esi+eax]                   ;54.13
        test      edx, edx                                      ;58.24
        mov       ebx, DWORD PTR [68+esp]                       ;
        mov       DWORD PTR [24+esp], edx                       ;54.13
        jle       .B1.22        ; Prob 1%                       ;58.24
        jmp       .B1.26        ; Prob 100%                     ;58.24
                                ; LOE ebx
.B1.32:                         ; Preds .B1.15                  ; Infreq
        mov       DWORD PTR [24+esp], 0                         ;
        jmp       .B1.22        ; Prob 100%                     ;
                                ; LOE ebx
.B1.33:                         ; Preds .B1.6                   ; Infreq
        mov       esi, DWORD PTR [104+esp]                      ;46.64
        mov       ecx, DWORD PTR [120+esp]                      ;46.11
        sub       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;46.11
        imul      edx, ecx, 900                                 ;46.11
        lea       edi, DWORD PTR [esi+esi*2]                    ;46.64
        neg       edi                                           ;46.64
        mov       ecx, DWORD PTR [112+esp]                      ;46.11
        add       ecx, edi                                      ;46.11
        mov       esi, DWORD PTR [100+esp]                      ;46.11
        sub       esi, ecx                                      ;46.11
        sub       esi, DWORD PTR [108+esp]                      ;46.11
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;46.77
        movss     xmm1, DWORD PTR [esi]                         ;46.64
        divss     xmm1, DWORD PTR [_2il0floatpacket.5]          ;46.77
        movsx     edi, BYTE PTR [668+eax+edx]                   ;46.85
        cvtsi2ss  xmm0, edi                                     ;46.85
        mulss     xmm1, xmm0                                    ;46.84
        minss     xmm1, DWORD PTR [436+eax+edx]                 ;46.11
        movss     DWORD PTR [432+eax+edx], xmm1                 ;46.11
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_OCCUP] ;81.10
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_OCCUP+32] ;81.10
        lea       edx, DWORD PTR [edx+ebx*4]                    ;
        jmp       .B1.11        ; Prob 100%                     ;
                                ; LOE eax edx ebx
.B1.34:                         ; Preds .B1.5                   ; Infreq
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_OCCUP+32] ;35.10
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_OCCUP] ;35.10
        pxor      xmm0, xmm0                                    ;36.78
        mov       DWORD PTR [68+esp], ebx                       ;
        imul      eax, eax, 900                                 ;30.8
        lea       edx, DWORD PTR [esi*4]                        ;35.10
        movss     xmm7, DWORD PTR [_2il0floatpacket.6]          ;42.10
        neg       edx                                           ;35.10
        lea       edi, DWORD PTR [ecx+ebx*4]                    ;35.10
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DETECTOR_LENGTH+32] ;35.19
        add       edx, edi                                      ;35.10
        neg       ecx                                           ;35.10
        add       ecx, ebx                                      ;35.10
        mov       ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DETECTOR_LENGTH] ;35.19
        movss     xmm2, DWORD PTR [edx]                         ;35.19
        mov       DWORD PTR [72+esp], edi                       ;35.10
        divss     xmm2, DWORD PTR [ebx+ecx*4]                   ;35.10
        imul      ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32], 900 ;36.10
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;36.19
        add       eax, edi                                      ;36.10
        sub       eax, ebx                                      ;36.10
        mov       esi, DWORD PTR [100+esp]                      ;42.10
        movsx     ecx, BYTE PTR [668+eax]                       ;36.29
        imul      ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NRATE] ;36.72
        imul      eax, DWORD PTR [120+esp], 900                 ;37.7
        cvtsi2ss  xmm1, ecx                                     ;36.72
        divss     xmm1, xmm0                                    ;36.78
        divss     xmm2, xmm1                                    ;36.10
        add       eax, edi                                      ;37.7
        sub       eax, ebx                                      ;37.7
        movss     DWORD PTR [edx], xmm2                         ;36.10
        mov       ecx, DWORD PTR [112+esp]                      ;42.10
        mov       ebx, DWORD PTR [108+esp]                      ;42.10
        movsx     edx, BYTE PTR [668+eax]                       ;37.102
        cvtsi2ss  xmm5, edx                                     ;37.102
        movss     xmm4, DWORD PTR [432+eax]                     ;37.55
        divss     xmm4, xmm5                                    ;37.101
        mov       edx, DWORD PTR [104+esp]                      ;41.63
        lea       edi, DWORD PTR [edx+edx*2]                    ;41.63
        neg       edi                                           ;41.63
        add       edi, ecx                                      ;42.10
        neg       edi                                           ;42.10
        add       edi, esi                                      ;42.10
        sub       edi, ebx                                      ;42.10
        movss     xmm6, DWORD PTR [edi]                         ;41.63
        mov       edi, ecx                                      ;42.10
        sub       edi, edx                                      ;42.10
        add       edx, edx                                      ;37.160
        neg       edi                                           ;42.10
        sub       ecx, edx                                      ;42.10
        add       edi, esi                                      ;42.10
        sub       esi, ecx                                      ;42.10
        sub       esi, ebx                                      ;42.10
        sub       edi, ebx                                      ;42.10
        mulss     xmm6, xmm5                                    ;41.76
        movss     xmm3, DWORD PTR [esi]                         ;37.160
        mov       edx, DWORD PTR [72+esp]                       ;42.10
        subss     xmm3, xmm2                                    ;37.173
        mulss     xmm3, DWORD PTR [edi]                         ;37.158
        mov       ebx, DWORD PTR [68+esp]                       ;42.10
        addss     xmm4, xmm3                                    ;37.7
        mulss     xmm5, xmm4                                    ;38.10
        minss     xmm6, xmm5                                    ;42.10
        maxss     xmm7, xmm6                                    ;42.10
        movss     DWORD PTR [432+eax], xmm7                     ;42.10
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_OCCUP+32] ;42.10
        jmp       .B1.11        ; Prob 100%                     ;42.10
        ALIGN     16
                                ; LOE eax edx ebx
; mark_end;
_RAMP_METERING ENDP
_TEXT	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
__STRLITPACK_9.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__NLITPACK_0.0.1	DD	18
__STRLITPACK_5.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_6.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_7.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _RAMP_METERING
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_0	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	114
	DB	97
	DB	109
	DB	112
	DB	32
	DB	109
	DB	101
	DB	116
	DB	101
	DB	114
	DB	105
	DB	110
	DB	103
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_2	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	114
	DB	97
	DB	109
	DB	112
	DB	32
	DB	99
	DB	111
	DB	110
	DB	116
	DB	114
	DB	111
	DB	108
	DB	32
	DB	110
	DB	117
	DB	109
	DB	98
	DB	101
	DB	114
	DB	32
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_1	DB	32
	DB	80
	DB	108
	DB	101
	DB	97
	DB	115
	DB	101
	DB	32
	DB	99
	DB	104
	DB	101
	DB	99
	DB	107
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_8	DB	0
	DB 3 DUP ( 0H)	; pad
_2il0floatpacket.1	DD	040000000H
_2il0floatpacket.2	DD	040400000H
_2il0floatpacket.3	DD	03f666666H
_2il0floatpacket.4	DD	03dcccccdH
_2il0floatpacket.5	DD	045610000H
_2il0floatpacket.6	DD	03da3d70aH
_2il0floatpacket.7	DD	03f800000H
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
EXTRN	_DYNUST_NETWORK_MODULE_mp_VKFUNC:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_BACKPOINTR:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_DETECTOR:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NRATE:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_DETECTOR_LENGTH:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_OCCUP:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_RAMP_PAR:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_RAMP_END:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_RAMP_START:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_DETECTOR_RAMP:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_DET_LINK:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_DEC_NUM:BYTE
_DATA	ENDS
EXTRN	_for_stop_core:PROC
EXTRN	_for_write_seq_lis_xmit:PROC
EXTRN	_for_write_seq_lis:PROC
EXTRN	_DYNUST_NETWORK_MODULE_mp_GETFLINKFROMNODE:PROC
EXTRN	__fltused:BYTE
	INCLUDELIB <ifconsol>
	INCLUDELIB <libifcoremt>
	INCLUDELIB <libifport>
	INCLUDELIB <libmmt>
	INCLUDELIB <LIBCMT>
	INCLUDELIB <libirc>
	INCLUDELIB <svml_dispmt>
	INCLUDELIB <OLDNAMES>
	END
