; mark_description "Intel(R) Visual Fortran Compiler XE for applications running on IA-32, Version 13.1.1.171 Build 20130313";
; mark_description "-S";
	.686P
 	.387
	OPTION DOTNAME
	ASSUME	CS:FLAT,DS:FLAT,SS:FLAT
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
TXTST0:
; -- Begin  _TRANSLINK_ADD_MODULE$
; mark_begin;
IF @Version GE 800
  .MMX
ELSEIF @Version GE 612
  .MMX
  MMWORD TEXTEQU <QWORD>
ENDIF
IF @Version GE 800
  .XMM
ELSEIF @Version GE 614
  .XMM
  XMMWORD TEXTEQU <OWORD>
ENDIF
       ALIGN     16
	PUBLIC _TRANSLINK_ADD_MODULE$
_TRANSLINK_ADD_MODULE$	PROC NEAR 
.B1.1:                          ; Preds .B1.0
        ret                                                     ;1.8
        ALIGN     16
                                ; LOE
; mark_end;
_TRANSLINK_ADD_MODULE$ ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _TRANSLINK_ADD_MODULE$
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _TRANSLINK_ADD_MODULE_mp_TRANLINK_CONSOLIDATE
; mark_begin;
       ALIGN     16
	PUBLIC _TRANSLINK_ADD_MODULE_mp_TRANLINK_CONSOLIDATE
_TRANSLINK_ADD_MODULE_mp_TRANLINK_CONSOLIDATE	PROC NEAR 
; parameter 1: 8 + ebp
.B2.1:                          ; Preds .B2.0
        push      ebp                                           ;24.12
        mov       ebp, esp                                      ;24.12
        and       esp, -16                                      ;24.12
        push      esi                                           ;24.12
        push      edi                                           ;24.12
        push      ebx                                           ;24.12
        sub       esp, 148                                      ;24.12
        mov       ebx, DWORD PTR [8+ebp]                        ;24.12
        mov       edx, DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY] ;36.13
        mov       eax, DWORD PTR [ebx]                          ;36.17
        sub       eax, DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY+32] ;36.42
        lea       ecx, DWORD PTR [eax+eax*4]                    ;36.42
        movsx     edx, WORD PTR [36+edx+ecx*8]                  ;36.17
        test      edx, edx                                      ;36.42
        jle       .B2.54        ; Prob 79%                      ;36.42
                                ; LOE edx ebx
.B2.2:                          ; Preds .B2.1
        mov       eax, 0                                        ;38.17
        lea       ecx, DWORD PTR [84+esp]                       ;38.17
        push      12                                            ;38.17
        cmovl     edx, eax                                      ;38.17
        push      edx                                           ;38.17
        push      2                                             ;38.17
        push      ecx                                           ;38.17
        call      _for_check_mult_overflow                      ;38.17
                                ; LOE eax ebx
.B2.3:                          ; Preds .B2.2
        and       eax, 1                                        ;38.17
        lea       edx, DWORD PTR [64+esp]                       ;38.17
        shl       eax, 4                                        ;38.17
        or        eax, 262145                                   ;38.17
        push      eax                                           ;38.17
        push      edx                                           ;38.17
        push      DWORD PTR [108+esp]                           ;38.17
        call      _for_allocate                                 ;38.17
                                ; LOE eax ebx
.B2.79:                         ; Preds .B2.3
        add       esp, 28                                       ;38.17
                                ; LOE eax ebx
.B2.4:                          ; Preds .B2.79
        test      eax, eax                                      ;38.17
        je        .B2.7         ; Prob 50%                      ;38.17
                                ; LOE ebx
.B2.5:                          ; Preds .B2.4
        mov       DWORD PTR [esp], 0                            ;40.18
        lea       edx, DWORD PTR [esp]                          ;40.18
        mov       DWORD PTR [32+esp], 54                        ;40.18
        lea       eax, DWORD PTR [32+esp]                       ;40.18
        mov       DWORD PTR [36+esp], OFFSET FLAT: __STRLITPACK_0 ;40.18
        push      32                                            ;40.18
        push      eax                                           ;40.18
        push      OFFSET FLAT: __STRLITPACK_2.0.2               ;40.18
        push      -2088435968                                   ;40.18
        push      911                                           ;40.18
        push      edx                                           ;40.18
        call      _for_write_seq_lis                            ;40.18
                                ; LOE ebx
.B2.6:                          ; Preds .B2.5
        push      32                                            ;41.18
        xor       eax, eax                                      ;41.18
        push      eax                                           ;41.18
        push      eax                                           ;41.18
        push      -2088435968                                   ;41.18
        push      eax                                           ;41.18
        push      OFFSET FLAT: __STRLITPACK_3                   ;41.18
        call      _for_stop_core                                ;41.18
                                ; LOE ebx
.B2.80:                         ; Preds .B2.6
        add       esp, 48                                       ;41.18
        jmp       .B2.8         ; Prob 100%                     ;41.18
                                ; LOE ebx
.B2.7:                          ; Preds .B2.4
        mov       esi, DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY+32] ;38.33
        mov       ecx, 1                                        ;38.17
        neg       esi                                           ;38.17
        xor       edx, edx                                      ;38.17
        add       esi, DWORD PTR [ebx]                          ;38.17
        mov       eax, 12                                       ;38.17
        mov       edi, DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY] ;38.17
        mov       DWORD PTR [64+esp], ecx                       ;38.17
        mov       DWORD PTR [80+esp], ecx                       ;38.17
        lea       ecx, DWORD PTR [esi+esi*4]                    ;38.17
        movsx     esi, WORD PTR [36+edi+ecx*8]                  ;38.17
        test      esi, esi                                      ;38.17
        mov       DWORD PTR [60+esp], 5                         ;38.17
        cmovl     esi, edx                                      ;38.17
        mov       DWORD PTR [52+esp], eax                       ;38.17
        mov       DWORD PTR [56+esp], edx                       ;38.17
        lea       edx, DWORD PTR [40+esp]                       ;38.17
        mov       DWORD PTR [72+esp], esi                       ;38.17
        mov       DWORD PTR [76+esp], eax                       ;38.17
        push      eax                                           ;38.17
        push      esi                                           ;38.17
        push      2                                             ;38.17
        push      edx                                           ;38.17
        call      _for_check_mult_overflow                      ;38.17
                                ; LOE ebx
.B2.81:                         ; Preds .B2.7
        add       esp, 16                                       ;38.17
                                ; LOE ebx
.B2.8:                          ; Preds .B2.80 .B2.81
        mov       ecx, DWORD PTR [ebx]                          ;45.28
        mov       edi, DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY+32] ;45.28
        mov       edx, DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY] ;45.17
        lea       eax, DWORD PTR [ecx+ecx*4]                    ;45.17
        lea       edi, DWORD PTR [edi+edi*4]                    ;45.17
        shl       edi, 3                                        ;45.17
        lea       esi, DWORD PTR [edx+eax*8]                    ;45.17
        sub       esi, edi                                      ;45.17
        mov       DWORD PTR [104+esp], esi                      ;45.17
        mov       DWORD PTR [96+esp], edi                       ;45.17
        xor       edi, edi                                      ;
        mov       DWORD PTR [112+esp], edi                      ;
        movsx     eax, WORD PTR [38+esi]                        ;45.28
        test      eax, eax                                      ;45.17
        mov       esi, DWORD PTR [32+esi]                       ;62.21
        mov       DWORD PTR [132+esp], eax                      ;45.28
        mov       DWORD PTR [100+esp], esi                      ;62.21
        jle       .B2.64        ; Prob 2%                       ;45.17
                                ; LOE edx ecx ebx esi
.B2.9:                          ; Preds .B2.8
        mov       edi, DWORD PTR [104+esp]                      ;46.21
        mov       DWORD PTR [92+esp], edx                       ;
        mov       edx, esi                                      ;
        mov       esi, DWORD PTR [28+edi]                       ;46.21
        imul      edx, esi                                      ;
        mov       DWORD PTR [128+esp], esi                      ;46.21
        mov       DWORD PTR [44+esp], esi                       ;
        mov       esi, DWORD PTR [edi]                          ;46.21
        mov       eax, DWORD PTR [76+esp]                       ;57.21
        sub       esi, edx                                      ;
        mov       edx, DWORD PTR [80+esp]                       ;
        imul      edx, eax                                      ;
        mov       DWORD PTR [120+esp], eax                      ;57.21
        mov       eax, DWORD PTR [48+esp]                       ;
        mov       DWORD PTR [88+esp], 1                         ;
        sub       eax, edx                                      ;
        mov       DWORD PTR [108+esp], ecx                      ;
        mov       DWORD PTR [124+esp], esi                      ;
        xor       esi, esi                                      ;
        mov       DWORD PTR [116+esp], eax                      ;
        mov       edx, DWORD PTR [44+esp]                       ;
        mov       ecx, DWORD PTR [88+esp]                       ;
                                ; LOE edx ecx esi
.B2.10:                         ; Preds .B2.12 .B2.9
        mov       eax, DWORD PTR [124+esp]                      ;46.21
        mov       ebx, DWORD PTR [edx+eax]                      ;46.21
        test      ebx, ebx                                      ;46.52
        jle       .B2.12        ; Prob 16%                      ;46.52
                                ; LOE edx ecx ebx esi
.B2.11:                         ; Preds .B2.10
        add       esi, DWORD PTR [120+esp]                      ;47.22
        mov       eax, DWORD PTR [116+esp]                      ;48.22
        mov       edi, DWORD PTR [124+esp]                      ;49.22
        inc       DWORD PTR [112+esp]                           ;47.22
        mov       DWORD PTR [esi+eax], ebx                      ;48.22
        mov       ebx, DWORD PTR [4+edx+edi]                    ;49.22
        mov       DWORD PTR [4+esi+eax], ebx                    ;49.22
        mov       ebx, DWORD PTR [8+edx+edi]                    ;50.22
        mov       DWORD PTR [8+esi+eax], ebx                    ;50.22
                                ; LOE edx ecx esi
.B2.12:                         ; Preds .B2.11 .B2.10
        inc       ecx                                           ;52.14
        add       edx, DWORD PTR [128+esp]                      ;52.14
        cmp       ecx, DWORD PTR [132+esp]                      ;52.14
        jle       .B2.10        ; Prob 82%                      ;52.14
                                ; LOE edx ecx esi
.B2.13:                         ; Preds .B2.12
        mov       DWORD PTR [88+esp], ecx                       ;
        mov       edx, DWORD PTR [92+esp]                       ;
        mov       ecx, DWORD PTR [108+esp]                      ;
        mov       eax, DWORD PTR [116+esp]                      ;
        mov       ebx, DWORD PTR [8+ebp]                        ;
        cmp       DWORD PTR [112+esp], 0                        ;54.23
        jle       .B2.64        ; Prob 16%                      ;54.23
                                ; LOE eax edx ecx ebx al dl cl ah dh ch
.B2.14:                         ; Preds .B2.13
        mov       esi, DWORD PTR [112+esp]                      ;55.21
        mov       edi, esi                                      ;55.21
        shr       edi, 31                                       ;55.21
        add       edi, esi                                      ;55.21
        sar       edi, 1                                        ;55.21
        sub       edx, DWORD PTR [96+esp]                       ;
        mov       DWORD PTR [44+esp], edi                       ;55.21
        test      edi, edi                                      ;55.21
        jbe       .B2.63        ; Prob 10%                      ;55.21
                                ; LOE eax edx ecx ebx al cl ah ch
.B2.15:                         ; Preds .B2.14
        mov       edi, DWORD PTR [120+esp]                      ;
        xor       esi, esi                                      ;
        mov       DWORD PTR [92+esp], edx                       ;
        mov       DWORD PTR [116+esp], eax                      ;
        lea       edx, DWORD PTR [eax+edi]                      ;
        mov       DWORD PTR [100+esp], edx                      ;
        lea       edi, DWORD PTR [eax+edi*2]                    ;
        mov       edx, DWORD PTR [92+esp]                       ;
        mov       eax, esi                                      ;
        mov       DWORD PTR [96+esp], edi                       ;
                                ; LOE eax edx ecx esi
.B2.16:                         ; Preds .B2.88 .B2.15
        lea       ecx, DWORD PTR [ecx+ecx*4]                    ;55.21
        mov       edi, DWORD PTR [edx+ecx*8]                    ;55.21
        lea       ebx, DWORD PTR [1+esi+esi]                    ;55.21
        sub       ebx, DWORD PTR [32+edx+ecx*8]                 ;55.21
        imul      ebx, DWORD PTR [28+edx+ecx*8]                 ;55.21
        mov       ecx, DWORD PTR [100+esp]                      ;55.21
        mov       ecx, DWORD PTR [ecx+eax*2]                    ;55.21
        mov       DWORD PTR [edi+ebx], ecx                      ;55.21
        mov       edi, DWORD PTR [8+ebp]                        ;55.21
        mov       ebx, DWORD PTR [edi]                          ;55.21
        mov       edi, DWORD PTR [96+esp]                       ;55.21
        lea       ecx, DWORD PTR [ebx+ebx*4]                    ;55.21
        mov       edi, DWORD PTR [edi+eax*2]                    ;55.21
        lea       ebx, DWORD PTR [2+esi+esi]                    ;55.21
        sub       ebx, DWORD PTR [32+edx+ecx*8]                 ;55.21
        inc       esi                                           ;55.21
        imul      ebx, DWORD PTR [28+edx+ecx*8]                 ;55.21
        mov       ecx, DWORD PTR [edx+ecx*8]                    ;55.21
        add       eax, DWORD PTR [120+esp]                      ;55.21
        cmp       esi, DWORD PTR [44+esp]                       ;55.21
        mov       DWORD PTR [ecx+ebx], edi                      ;55.21
        jae       .B2.17        ; Prob 36%                      ;55.21
                                ; LOE eax edx esi
.B2.88:                         ; Preds .B2.16
        mov       ecx, DWORD PTR [8+ebp]                        ;36.17
        mov       ecx, DWORD PTR [ecx]                          ;36.17
        jmp       .B2.16        ; Prob 100%                     ;36.17
                                ; LOE eax edx ecx esi
.B2.17:                         ; Preds .B2.16
        mov       ebx, DWORD PTR [8+ebp]                        ;
        lea       esi, DWORD PTR [1+esi+esi]                    ;55.21
        mov       eax, DWORD PTR [116+esp]                      ;
        mov       ecx, DWORD PTR [ebx]                          ;55.21
                                ; LOE eax edx ecx ebx esi al ah
.B2.18:                         ; Preds .B2.17 .B2.63
        lea       edi, DWORD PTR [-1+esi]                       ;55.21
        cmp       edi, DWORD PTR [112+esp]                      ;55.21
        jae       .B2.20        ; Prob 10%                      ;55.21
                                ; LOE eax edx ecx ebx esi al ah
.B2.19:                         ; Preds .B2.18
        mov       edi, esi                                      ;55.21
        lea       ecx, DWORD PTR [ecx+ecx*4]                    ;55.21
        imul      edi, DWORD PTR [120+esp]                      ;55.21
        sub       esi, DWORD PTR [32+edx+ecx*8]                 ;55.21
        imul      esi, DWORD PTR [28+edx+ecx*8]                 ;55.21
        mov       ecx, DWORD PTR [edx+ecx*8]                    ;55.21
        mov       edi, DWORD PTR [edi+eax]                      ;55.21
        mov       DWORD PTR [ecx+esi], edi                      ;55.21
        mov       ecx, DWORD PTR [ebx]                          ;56.21
                                ; LOE eax edx ecx ebx al ah
.B2.20:                         ; Preds .B2.19 .B2.18
        cmp       DWORD PTR [44+esp], 0                         ;56.21
        jbe       .B2.62        ; Prob 10%                      ;56.21
                                ; LOE eax edx ecx ebx al ah
.B2.21:                         ; Preds .B2.20
        mov       esi, DWORD PTR [120+esp]                      ;
        xor       edi, edi                                      ;
        mov       DWORD PTR [92+esp], edx                       ;
        mov       DWORD PTR [116+esp], eax                      ;
        lea       edx, DWORD PTR [eax+esi]                      ;
        mov       DWORD PTR [96+esp], edx                       ;
        lea       edx, DWORD PTR [eax+esi*2]                    ;
        mov       DWORD PTR [100+esp], edx                      ;
        xor       esi, esi                                      ;
        mov       edx, DWORD PTR [92+esp]                       ;
                                ; LOE edx ecx esi edi
.B2.22:                         ; Preds .B2.87 .B2.21
        lea       eax, DWORD PTR [ecx+ecx*4]                    ;56.21
        mov       ebx, DWORD PTR [edx+eax*8]                    ;56.21
        lea       ecx, DWORD PTR [1+edi+edi]                    ;56.21
        sub       ecx, DWORD PTR [32+edx+eax*8]                 ;56.21
        imul      ecx, DWORD PTR [28+edx+eax*8]                 ;56.21
        mov       eax, DWORD PTR [96+esp]                       ;56.21
        mov       eax, DWORD PTR [4+eax+esi*2]                  ;56.21
        mov       DWORD PTR [4+ebx+ecx], eax                    ;56.21
        mov       ebx, DWORD PTR [8+ebp]                        ;56.21
        mov       ecx, DWORD PTR [ebx]                          ;56.21
        mov       ebx, DWORD PTR [100+esp]                      ;56.21
        lea       eax, DWORD PTR [ecx+ecx*4]                    ;56.21
        mov       ebx, DWORD PTR [4+ebx+esi*2]                  ;56.21
        lea       ecx, DWORD PTR [2+edi+edi]                    ;56.21
        sub       ecx, DWORD PTR [32+edx+eax*8]                 ;56.21
        inc       edi                                           ;56.21
        imul      ecx, DWORD PTR [28+edx+eax*8]                 ;56.21
        mov       eax, DWORD PTR [edx+eax*8]                    ;56.21
        add       esi, DWORD PTR [120+esp]                      ;56.21
        cmp       edi, DWORD PTR [44+esp]                       ;56.21
        mov       DWORD PTR [4+eax+ecx], ebx                    ;56.21
        jae       .B2.23        ; Prob 36%                      ;56.21
                                ; LOE edx esi edi
.B2.87:                         ; Preds .B2.22
        mov       eax, DWORD PTR [8+ebp]                        ;36.17
        mov       ecx, DWORD PTR [eax]                          ;36.17
        jmp       .B2.22        ; Prob 100%                     ;36.17
                                ; LOE edx ecx esi edi
.B2.23:                         ; Preds .B2.22
        mov       ebx, DWORD PTR [8+ebp]                        ;
        lea       esi, DWORD PTR [1+edi+edi]                    ;56.21
        mov       eax, DWORD PTR [116+esp]                      ;
        mov       ecx, DWORD PTR [ebx]                          ;56.21
                                ; LOE eax edx ecx ebx esi al ah
.B2.24:                         ; Preds .B2.23 .B2.62
        lea       edi, DWORD PTR [-1+esi]                       ;56.21
        cmp       edi, DWORD PTR [112+esp]                      ;56.21
        jae       .B2.26        ; Prob 10%                      ;56.21
                                ; LOE eax edx ecx ebx esi al ah
.B2.25:                         ; Preds .B2.24
        mov       edi, esi                                      ;56.21
        lea       ecx, DWORD PTR [ecx+ecx*4]                    ;56.21
        imul      edi, DWORD PTR [120+esp]                      ;56.21
        sub       esi, DWORD PTR [32+edx+ecx*8]                 ;56.21
        imul      esi, DWORD PTR [28+edx+ecx*8]                 ;56.21
        mov       ecx, DWORD PTR [edx+ecx*8]                    ;56.21
        mov       edi, DWORD PTR [4+edi+eax]                    ;56.21
        mov       DWORD PTR [4+ecx+esi], edi                    ;56.21
        mov       ecx, DWORD PTR [ebx]                          ;57.21
                                ; LOE eax edx ecx ebx al ah
.B2.26:                         ; Preds .B2.25 .B2.24
        cmp       DWORD PTR [44+esp], 0                         ;57.21
        jbe       .B2.61        ; Prob 10%                      ;57.21
                                ; LOE eax edx ecx ebx al ah
.B2.27:                         ; Preds .B2.26
        mov       edi, DWORD PTR [120+esp]                      ;
        xor       esi, esi                                      ;
        mov       DWORD PTR [92+esp], edx                       ;
        mov       DWORD PTR [116+esp], eax                      ;
        lea       edx, DWORD PTR [eax+edi]                      ;
        mov       DWORD PTR [100+esp], edx                      ;
        lea       edx, DWORD PTR [eax+edi*2]                    ;
        mov       DWORD PTR [96+esp], edx                       ;
        mov       edx, DWORD PTR [92+esp]                       ;
        mov       eax, esi                                      ;
                                ; LOE eax edx ecx esi
.B2.28:                         ; Preds .B2.86 .B2.27
        lea       ecx, DWORD PTR [ecx+ecx*4]                    ;57.21
        mov       edi, DWORD PTR [edx+ecx*8]                    ;57.21
        lea       ebx, DWORD PTR [1+esi+esi]                    ;57.21
        sub       ebx, DWORD PTR [32+edx+ecx*8]                 ;57.21
        imul      ebx, DWORD PTR [28+edx+ecx*8]                 ;57.21
        mov       ecx, DWORD PTR [100+esp]                      ;57.21
        mov       ecx, DWORD PTR [8+ecx+eax*2]                  ;57.21
        mov       DWORD PTR [8+edi+ebx], ecx                    ;57.21
        mov       edi, DWORD PTR [8+ebp]                        ;57.21
        mov       ebx, DWORD PTR [edi]                          ;57.21
        mov       edi, DWORD PTR [96+esp]                       ;57.21
        lea       ecx, DWORD PTR [ebx+ebx*4]                    ;57.21
        mov       edi, DWORD PTR [8+edi+eax*2]                  ;57.21
        lea       ebx, DWORD PTR [2+esi+esi]                    ;57.21
        sub       ebx, DWORD PTR [32+edx+ecx*8]                 ;57.21
        inc       esi                                           ;57.21
        imul      ebx, DWORD PTR [28+edx+ecx*8]                 ;57.21
        mov       ecx, DWORD PTR [edx+ecx*8]                    ;57.21
        add       eax, DWORD PTR [120+esp]                      ;57.21
        cmp       esi, DWORD PTR [44+esp]                       ;57.21
        mov       DWORD PTR [8+ecx+ebx], edi                    ;57.21
        jae       .B2.29        ; Prob 36%                      ;57.21
                                ; LOE eax edx esi
.B2.86:                         ; Preds .B2.28
        mov       ecx, DWORD PTR [8+ebp]                        ;36.17
        mov       ecx, DWORD PTR [ecx]                          ;36.17
        jmp       .B2.28        ; Prob 100%                     ;36.17
                                ; LOE eax edx ecx esi
.B2.29:                         ; Preds .B2.28
        mov       ebx, DWORD PTR [8+ebp]                        ;
        lea       esi, DWORD PTR [1+esi+esi]                    ;57.21
        mov       eax, DWORD PTR [116+esp]                      ;
        mov       ecx, DWORD PTR [ebx]                          ;57.21
                                ; LOE eax edx ecx ebx esi al ah
.B2.30:                         ; Preds .B2.29 .B2.61
        lea       edi, DWORD PTR [-1+esi]                       ;57.21
        cmp       edi, DWORD PTR [112+esp]                      ;57.21
        jae       .B2.32        ; Prob 10%                      ;57.21
                                ; LOE eax edx ecx ebx esi al ah
.B2.31:                         ; Preds .B2.30
        mov       edi, DWORD PTR [120+esp]                      ;57.21
        lea       ecx, DWORD PTR [ecx+ecx*4]                    ;57.21
        imul      edi, esi                                      ;57.21
        sub       esi, DWORD PTR [32+edx+ecx*8]                 ;57.21
        imul      esi, DWORD PTR [28+edx+ecx*8]                 ;57.21
        mov       ecx, DWORD PTR [edx+ecx*8]                    ;57.21
        mov       eax, DWORD PTR [8+edi+eax]                    ;57.21
        mov       DWORD PTR [8+ecx+esi], eax                    ;57.21
        mov       ecx, DWORD PTR [ebx]                          ;60.21
                                ; LOE edx ecx ebx
.B2.32:                         ; Preds .B2.31 .B2.30
        mov       esi, DWORD PTR [112+esp]                      ;58.21
        neg       esi                                           ;58.21
        mov       eax, DWORD PTR [88+esp]                       ;58.21
        lea       esi, DWORD PTR [-1+eax+esi]                   ;58.21
        test      esi, esi                                      ;58.21
        jle       .B2.52        ; Prob 50%                      ;58.21
                                ; LOE edx ecx ebx esi
.B2.33:                         ; Preds .B2.32
        mov       eax, esi                                      ;58.21
        shr       eax, 31                                       ;58.21
        add       eax, esi                                      ;58.21
        sar       eax, 1                                        ;58.21
        test      eax, eax                                      ;58.21
        jbe       .B2.60        ; Prob 10%                      ;58.21
                                ; LOE eax edx ecx ebx esi
.B2.34:                         ; Preds .B2.33
        mov       DWORD PTR [88+esp], eax                       ;
        xor       edi, edi                                      ;
        mov       DWORD PTR [44+esp], esi                       ;
                                ; LOE edx ecx ebx edi
.B2.35:                         ; Preds .B2.85 .B2.34
        mov       esi, DWORD PTR [112+esp]                      ;58.21
        mov       DWORD PTR [92+esp], edi                       ;
        lea       esi, DWORD PTR [1+esi+edi*2]                  ;58.21
        lea       edi, DWORD PTR [ecx+ecx*4]                    ;58.21
        xor       ecx, ecx                                      ;58.21
        mov       eax, DWORD PTR [32+edx+edi*8]                 ;58.21
        neg       eax                                           ;58.21
        add       eax, esi                                      ;58.21
        inc       esi                                           ;58.21
        imul      eax, DWORD PTR [28+edx+edi*8]                 ;58.21
        mov       edi, DWORD PTR [edx+edi*8]                    ;58.21
        mov       DWORD PTR [edi+eax], ecx                      ;58.21
        mov       eax, DWORD PTR [ebx]                          ;58.21
        lea       eax, DWORD PTR [eax+eax*4]                    ;58.21
        sub       esi, DWORD PTR [32+edx+eax*8]                 ;58.21
        imul      esi, DWORD PTR [28+edx+eax*8]                 ;58.21
        mov       edi, DWORD PTR [edx+eax*8]                    ;58.21
        mov       DWORD PTR [edi+esi], ecx                      ;58.21
        mov       edi, DWORD PTR [92+esp]                       ;58.21
        inc       edi                                           ;58.21
        cmp       edi, DWORD PTR [88+esp]                       ;58.21
        jae       .B2.36        ; Prob 36%                      ;58.21
                                ; LOE edx ebx edi
.B2.85:                         ; Preds .B2.35
        mov       ecx, DWORD PTR [ebx]                          ;36.17
        jmp       .B2.35        ; Prob 100%                     ;36.17
                                ; LOE edx ecx ebx edi
.B2.36:                         ; Preds .B2.35
        mov       esi, DWORD PTR [44+esp]                       ;
        lea       edi, DWORD PTR [1+edi+edi]                    ;58.21
        mov       ecx, DWORD PTR [ebx]                          ;58.21
                                ; LOE edx ecx ebx esi edi
.B2.37:                         ; Preds .B2.36 .B2.60
        lea       eax, DWORD PTR [-1+edi]                       ;58.21
        cmp       esi, eax                                      ;58.21
        jbe       .B2.39        ; Prob 10%                      ;58.21
                                ; LOE edx ecx ebx esi edi
.B2.38:                         ; Preds .B2.37
        lea       eax, DWORD PTR [ecx+ecx*4]                    ;58.21
        mov       ecx, DWORD PTR [112+esp]                      ;58.21
        add       ecx, edi                                      ;58.21
        mov       edi, DWORD PTR [edx+eax*8]                    ;58.21
        sub       ecx, DWORD PTR [32+edx+eax*8]                 ;58.21
        imul      ecx, DWORD PTR [28+edx+eax*8]                 ;58.21
        mov       DWORD PTR [edi+ecx], 0                        ;58.21
        mov       ecx, DWORD PTR [ebx]                          ;60.21
                                ; LOE edx ecx ebx esi
.B2.39:                         ; Preds .B2.38 .B2.37
        mov       eax, esi                                      ;59.21
        shr       eax, 31                                       ;59.21
        add       eax, esi                                      ;59.21
        sar       eax, 1                                        ;59.21
        test      eax, eax                                      ;59.21
        jbe       .B2.59        ; Prob 10%                      ;59.21
                                ; LOE eax edx ecx ebx esi
.B2.40:                         ; Preds .B2.39
        mov       DWORD PTR [88+esp], eax                       ;
        xor       edi, edi                                      ;
        mov       DWORD PTR [44+esp], esi                       ;
                                ; LOE edx ecx ebx edi
.B2.41:                         ; Preds .B2.84 .B2.40
        mov       esi, DWORD PTR [112+esp]                      ;59.21
        mov       DWORD PTR [92+esp], edi                       ;
        lea       esi, DWORD PTR [1+esi+edi*2]                  ;59.21
        lea       edi, DWORD PTR [ecx+ecx*4]                    ;59.21
        xor       ecx, ecx                                      ;59.21
        mov       eax, DWORD PTR [32+edx+edi*8]                 ;59.21
        neg       eax                                           ;59.21
        add       eax, esi                                      ;59.21
        inc       esi                                           ;59.21
        imul      eax, DWORD PTR [28+edx+edi*8]                 ;59.21
        mov       edi, DWORD PTR [edx+edi*8]                    ;59.21
        mov       DWORD PTR [4+edi+eax], ecx                    ;59.21
        mov       eax, DWORD PTR [ebx]                          ;59.21
        lea       eax, DWORD PTR [eax+eax*4]                    ;59.21
        sub       esi, DWORD PTR [32+edx+eax*8]                 ;59.21
        imul      esi, DWORD PTR [28+edx+eax*8]                 ;59.21
        mov       edi, DWORD PTR [edx+eax*8]                    ;59.21
        mov       DWORD PTR [4+edi+esi], ecx                    ;59.21
        mov       edi, DWORD PTR [92+esp]                       ;59.21
        inc       edi                                           ;59.21
        cmp       edi, DWORD PTR [88+esp]                       ;59.21
        jae       .B2.42        ; Prob 36%                      ;59.21
                                ; LOE edx ebx edi
.B2.84:                         ; Preds .B2.41
        mov       ecx, DWORD PTR [ebx]                          ;36.17
        jmp       .B2.41        ; Prob 100%                     ;36.17
                                ; LOE edx ecx ebx edi
.B2.42:                         ; Preds .B2.41
        mov       esi, DWORD PTR [44+esp]                       ;
        lea       edi, DWORD PTR [1+edi+edi]                    ;59.21
        mov       ecx, DWORD PTR [ebx]                          ;59.21
                                ; LOE edx ecx ebx esi edi
.B2.43:                         ; Preds .B2.42 .B2.59
        lea       eax, DWORD PTR [-1+edi]                       ;59.21
        cmp       esi, eax                                      ;59.21
        jbe       .B2.45        ; Prob 10%                      ;59.21
                                ; LOE edx ecx ebx esi edi
.B2.44:                         ; Preds .B2.43
        lea       eax, DWORD PTR [ecx+ecx*4]                    ;59.21
        mov       ecx, DWORD PTR [112+esp]                      ;59.21
        add       ecx, edi                                      ;59.21
        mov       edi, DWORD PTR [edx+eax*8]                    ;59.21
        sub       ecx, DWORD PTR [32+edx+eax*8]                 ;59.21
        imul      ecx, DWORD PTR [28+edx+eax*8]                 ;59.21
        mov       DWORD PTR [4+edi+ecx], 0                      ;59.21
        mov       ecx, DWORD PTR [ebx]                          ;60.21
                                ; LOE edx ecx ebx esi
.B2.45:                         ; Preds .B2.44 .B2.43
        mov       edi, esi                                      ;60.21
        shr       edi, 31                                       ;60.21
        add       edi, esi                                      ;60.21
        sar       edi, 1                                        ;60.21
        test      edi, edi                                      ;60.21
        jbe       .B2.58        ; Prob 10%                      ;60.21
                                ; LOE edx ecx ebx esi edi
.B2.46:                         ; Preds .B2.45
        mov       DWORD PTR [88+esp], edi                       ;
        xor       eax, eax                                      ;
        mov       DWORD PTR [44+esp], esi                       ;
                                ; LOE eax edx ecx ebx
.B2.47:                         ; Preds .B2.83 .B2.46
        mov       DWORD PTR [92+esp], eax                       ;
        lea       edi, DWORD PTR [ecx+ecx*4]                    ;60.21
        mov       ecx, DWORD PTR [112+esp]                      ;60.21
        mov       esi, DWORD PTR [32+edx+edi*8]                 ;60.21
        neg       esi                                           ;60.21
        lea       ecx, DWORD PTR [1+ecx+eax*2]                  ;60.21
        xor       eax, eax                                      ;60.21
        add       esi, ecx                                      ;60.21
        inc       ecx                                           ;60.21
        imul      esi, DWORD PTR [28+edx+edi*8]                 ;60.21
        mov       edi, DWORD PTR [edx+edi*8]                    ;60.21
        mov       DWORD PTR [8+edi+esi], eax                    ;60.21
        mov       esi, DWORD PTR [ebx]                          ;60.21
        lea       esi, DWORD PTR [esi+esi*4]                    ;60.21
        sub       ecx, DWORD PTR [32+edx+esi*8]                 ;60.21
        imul      ecx, DWORD PTR [28+edx+esi*8]                 ;60.21
        mov       edi, DWORD PTR [edx+esi*8]                    ;60.21
        mov       DWORD PTR [8+edi+ecx], eax                    ;60.21
        mov       eax, DWORD PTR [92+esp]                       ;60.21
        inc       eax                                           ;60.21
        cmp       eax, DWORD PTR [88+esp]                       ;60.21
        jae       .B2.48        ; Prob 36%                      ;60.21
                                ; LOE eax edx ebx
.B2.83:                         ; Preds .B2.47
        mov       ecx, DWORD PTR [ebx]                          ;36.17
        jmp       .B2.47        ; Prob 100%                     ;36.17
                                ; LOE eax edx ecx ebx
.B2.48:                         ; Preds .B2.47
        mov       esi, DWORD PTR [44+esp]                       ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;60.21
        mov       ecx, DWORD PTR [ebx]                          ;60.21
                                ; LOE eax edx ecx ebx esi
.B2.49:                         ; Preds .B2.48 .B2.58
        lea       edi, DWORD PTR [-1+eax]                       ;60.21
        cmp       esi, edi                                      ;60.21
        jbe       .B2.52        ; Prob 10%                      ;60.21
                                ; LOE eax edx ecx ebx
.B2.50:                         ; Preds .B2.49
        lea       esi, DWORD PTR [ecx+ecx*4]                    ;60.21
        mov       ecx, DWORD PTR [112+esp]                      ;60.21
        lea       edi, DWORD PTR [eax+ecx]                      ;60.21
        mov       eax, DWORD PTR [edx+esi*8]                    ;60.21
        sub       edi, DWORD PTR [32+edx+esi*8]                 ;60.21
        imul      edi, DWORD PTR [28+edx+esi*8]                 ;60.21
        mov       DWORD PTR [8+eax+edi], 0                      ;60.21
        mov       ecx, DWORD PTR [ebx]                          ;67.17
                                ; LOE edx ecx
.B2.52:                         ; Preds .B2.49 .B2.50 .B2.32
        lea       eax, DWORD PTR [ecx+ecx*4]                    ;
        lea       esi, DWORD PTR [edx+eax*8]                    ;
                                ; LOE ecx esi
.B2.53:                         ; Preds .B2.52 .B2.75 .B2.72
        mov       eax, DWORD PTR [112+esp]                      ;67.17
        mov       WORD PTR [38+esi], ax                         ;67.17
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;68.17
        neg       esi                                           ;68.17
        add       esi, ecx                                      ;68.17
        imul      ecx, esi, 900                                 ;68.17
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;68.17
        mov       DWORD PTR [700+edx+ecx], eax                  ;68.17
                                ; LOE
.B2.54:                         ; Preds .B2.1 .B2.53
        mov       ebx, DWORD PTR [60+esp]                       ;71.13
        test      bl, 1                                         ;71.16
        je        .B2.57        ; Prob 60%                      ;71.16
                                ; LOE ebx
.B2.55:                         ; Preds .B2.54
        mov       edx, ebx                                      ;71.36
        mov       eax, ebx                                      ;71.36
        shr       edx, 1                                        ;71.36
        and       eax, 1                                        ;71.36
        and       edx, 1                                        ;71.36
        add       eax, eax                                      ;71.36
        shl       edx, 2                                        ;71.36
        or        edx, eax                                      ;71.36
        or        edx, 262144                                   ;71.36
        push      edx                                           ;71.36
        push      DWORD PTR [52+esp]                            ;71.36
        call      _for_dealloc_allocatable                      ;71.36
                                ; LOE ebx
.B2.82:                         ; Preds .B2.55
        add       esp, 8                                        ;71.36
                                ; LOE ebx
.B2.56:                         ; Preds .B2.82
        and       ebx, -2                                       ;71.36
        mov       DWORD PTR [48+esp], 0                         ;71.36
        mov       DWORD PTR [60+esp], ebx                       ;71.36
                                ; LOE
.B2.57:                         ; Preds .B2.54 .B2.56
        add       esp, 148                                      ;73.2
        pop       ebx                                           ;73.2
        pop       edi                                           ;73.2
        pop       esi                                           ;73.2
        mov       esp, ebp                                      ;73.2
        pop       ebp                                           ;73.2
        ret                                                     ;73.2
                                ; LOE
.B2.58:                         ; Preds .B2.45                  ; Infreq
        mov       eax, 1                                        ;
        jmp       .B2.49        ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B2.59:                         ; Preds .B2.39                  ; Infreq
        mov       edi, 1                                        ;
        jmp       .B2.43        ; Prob 100%                     ;
                                ; LOE edx ecx ebx esi edi
.B2.60:                         ; Preds .B2.33                  ; Infreq
        mov       edi, 1                                        ;
        jmp       .B2.37        ; Prob 100%                     ;
                                ; LOE edx ecx ebx esi edi
.B2.61:                         ; Preds .B2.26                  ; Infreq
        mov       esi, 1                                        ;
        jmp       .B2.30        ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi al ah
.B2.62:                         ; Preds .B2.20                  ; Infreq
        mov       esi, 1                                        ;
        jmp       .B2.24        ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi al ah
.B2.63:                         ; Preds .B2.14                  ; Infreq
        mov       esi, 1                                        ;
        jmp       .B2.18        ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi al ah
.B2.64:                         ; Preds .B2.8 .B2.13            ; Infreq
        mov       eax, DWORD PTR [104+esp]                      ;62.21
        sub       edx, DWORD PTR [96+esp]                       ;
        cmp       DWORD PTR [24+eax], 0                         ;62.21
        jle       .B2.68        ; Prob 10%                      ;62.21
                                ; LOE edx ecx ebx
.B2.65:                         ; Preds .B2.64                  ; Infreq
        mov       esi, DWORD PTR [100+esp]                      ;
        mov       eax, 1                                        ;
                                ; LOE eax edx ecx ebx esi
.B2.66:                         ; Preds .B2.66 .B2.65           ; Infreq
        inc       eax                                           ;62.21
        lea       edi, DWORD PTR [ecx+ecx*4]                    ;62.21
        mov       ecx, DWORD PTR [32+edx+edi*8]                 ;62.21
        neg       ecx                                           ;62.21
        add       ecx, esi                                      ;62.21
        inc       esi                                           ;62.21
        imul      ecx, DWORD PTR [28+edx+edi*8]                 ;62.21
        mov       edi, DWORD PTR [edx+edi*8]                    ;62.21
        mov       DWORD PTR [edi+ecx], 0                        ;62.21
        mov       ecx, DWORD PTR [ebx]                          ;62.21
        lea       edi, DWORD PTR [ecx+ecx*4]                    ;62.21
        cmp       eax, DWORD PTR [24+edx+edi*8]                 ;62.21
        jle       .B2.66        ; Prob 82%                      ;62.21
                                ; LOE eax edx ecx ebx esi
.B2.68:                         ; Preds .B2.66 .B2.64           ; Infreq
        lea       eax, DWORD PTR [ecx+ecx*4]                    ;63.21
        lea       esi, DWORD PTR [edx+eax*8]                    ;63.21
        mov       edi, DWORD PTR [24+esi]                       ;63.21
        test      edi, edi                                      ;63.21
        mov       eax, DWORD PTR [32+esi]                       ;63.21
        jle       .B2.72        ; Prob 10%                      ;63.21
                                ; LOE eax edx ecx ebx esi edi
.B2.69:                         ; Preds .B2.68                  ; Infreq
        mov       esi, 1                                        ;
                                ; LOE eax edx ecx ebx esi
.B2.70:                         ; Preds .B2.70 .B2.69           ; Infreq
        inc       esi                                           ;63.21
        lea       edi, DWORD PTR [ecx+ecx*4]                    ;63.21
        mov       ecx, DWORD PTR [32+edx+edi*8]                 ;63.21
        neg       ecx                                           ;63.21
        add       ecx, eax                                      ;63.21
        inc       eax                                           ;63.21
        imul      ecx, DWORD PTR [28+edx+edi*8]                 ;63.21
        mov       edi, DWORD PTR [edx+edi*8]                    ;63.21
        mov       DWORD PTR [4+edi+ecx], 0                      ;63.21
        mov       ecx, DWORD PTR [ebx]                          ;63.21
        lea       edi, DWORD PTR [ecx+ecx*4]                    ;63.21
        cmp       esi, DWORD PTR [24+edx+edi*8]                 ;63.21
        jle       .B2.70        ; Prob 82%                      ;63.21
                                ; LOE eax edx ecx ebx esi edi
.B2.71:                         ; Preds .B2.70                  ; Infreq
        lea       esi, DWORD PTR [edx+edi*8]                    ;
        mov       edi, DWORD PTR [24+esi]                       ;64.21
        mov       eax, DWORD PTR [32+esi]                       ;64.21
                                ; LOE eax edx ecx ebx esi edi
.B2.72:                         ; Preds .B2.71 .B2.68           ; Infreq
        test      edi, edi                                      ;64.21
        jle       .B2.53        ; Prob 10%                      ;64.21
                                ; LOE eax edx ecx ebx esi
.B2.73:                         ; Preds .B2.72                  ; Infreq
        mov       esi, 1                                        ;
                                ; LOE eax edx ecx ebx esi
.B2.74:                         ; Preds .B2.74 .B2.73           ; Infreq
        inc       esi                                           ;64.21
        lea       edi, DWORD PTR [ecx+ecx*4]                    ;64.21
        mov       ecx, DWORD PTR [32+edx+edi*8]                 ;64.21
        neg       ecx                                           ;64.21
        add       ecx, eax                                      ;64.21
        inc       eax                                           ;64.21
        imul      ecx, DWORD PTR [28+edx+edi*8]                 ;64.21
        mov       edi, DWORD PTR [edx+edi*8]                    ;64.21
        mov       DWORD PTR [8+edi+ecx], 0                      ;64.21
        mov       ecx, DWORD PTR [ebx]                          ;64.21
        lea       edi, DWORD PTR [ecx+ecx*4]                    ;64.21
        cmp       esi, DWORD PTR [24+edx+edi*8]                 ;64.21
        jle       .B2.74        ; Prob 82%                      ;64.21
                                ; LOE eax edx ecx ebx esi edi
.B2.75:                         ; Preds .B2.74                  ; Infreq
        lea       esi, DWORD PTR [edx+edi*8]                    ;
        jmp       .B2.53        ; Prob 100%                     ;
        ALIGN     16
                                ; LOE ecx esi
; mark_end;
_TRANSLINK_ADD_MODULE_mp_TRANLINK_CONSOLIDATE ENDP
_TEXT	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
__STRLITPACK_2.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _TRANSLINK_ADD_MODULE_mp_TRANLINK_CONSOLIDATE
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _TRANSLINK_ADD_MODULE_mp_TRANLINK_CONSOLIDATE_NEW
; mark_begin;
       ALIGN     16
	PUBLIC _TRANSLINK_ADD_MODULE_mp_TRANLINK_CONSOLIDATE_NEW
_TRANSLINK_ADD_MODULE_mp_TRANLINK_CONSOLIDATE_NEW	PROC NEAR 
; parameter 1: 8 + ebx
.B3.1:                          ; Preds .B3.0
        push      ebx                                           ;76.13
        mov       ebx, esp                                      ;76.13
        and       esp, -16                                      ;76.13
        push      ebp                                           ;76.13
        push      ebp                                           ;76.13
        mov       ebp, DWORD PTR [4+ebx]                        ;76.13
        mov       DWORD PTR [4+esp], ebp                        ;76.13
        mov       ebp, esp                                      ;76.13
        sub       esp, 360                                      ;76.13
        mov       eax, 128                                      ;85.49
        mov       edx, DWORD PTR [8+ebx]                        ;76.13
        xor       ecx, ecx                                      ;85.49
        mov       DWORD PTR [-36+ebp], edi                      ;76.13
        mov       edi, DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY+32] ;87.26
        neg       edi                                           ;87.8
        add       edi, DWORD PTR [edx]                          ;87.8
        mov       edx, DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY] ;87.8
        mov       DWORD PTR [-40+ebp], esi                      ;76.13
        mov       esi, 1                                        ;85.49
        mov       DWORD PTR [-336+ebp], esi                     ;85.49
        mov       DWORD PTR [-296+ebp], esi                     ;85.39
        mov       DWORD PTR [-256+ebp], esi                     ;85.29
        mov       DWORD PTR [-216+ebp], esi                     ;84.49
        mov       DWORD PTR [-176+ebp], esi                     ;84.39
        mov       DWORD PTR [-136+ebp], esi                     ;84.29
        mov       DWORD PTR [-96+ebp], esi                      ;87.8
        mov       DWORD PTR [-80+ebp], esi                      ;87.8
        lea       esi, DWORD PTR [edi+edi*4]                    ;87.8
        movsx     edi, WORD PTR [36+edx+esi*8]                  ;87.8
        test      edi, edi                                      ;87.8
        mov       DWORD PTR [-340+ebp], eax                     ;85.49
        mov       DWORD PTR [-300+ebp], eax                     ;85.39
        cmovl     edi, ecx                                      ;87.8
        mov       DWORD PTR [-260+ebp], eax                     ;85.29
        mov       DWORD PTR [-220+ebp], eax                     ;84.49
        mov       DWORD PTR [-180+ebp], eax                     ;84.39
        mov       DWORD PTR [-140+ebp], eax                     ;84.29
        mov       eax, 4                                        ;87.8
        push      eax                                           ;87.8
        push      edi                                           ;87.8
        mov       DWORD PTR [-352+ebp], ecx                     ;85.49
        mov       DWORD PTR [-348+ebp], ecx                     ;85.49
        mov       DWORD PTR [-344+ebp], ecx                     ;85.49
        mov       DWORD PTR [-332+ebp], ecx                     ;85.49
        mov       DWORD PTR [-328+ebp], ecx                     ;85.49
        mov       DWORD PTR [-324+ebp], ecx                     ;85.49
        mov       DWORD PTR [-320+ebp], ecx                     ;85.49
        mov       DWORD PTR [-312+ebp], ecx                     ;85.39
        mov       DWORD PTR [-308+ebp], ecx                     ;85.39
        mov       DWORD PTR [-304+ebp], ecx                     ;85.39
        mov       DWORD PTR [-292+ebp], ecx                     ;85.39
        mov       DWORD PTR [-288+ebp], ecx                     ;85.39
        mov       DWORD PTR [-284+ebp], ecx                     ;85.39
        mov       DWORD PTR [-280+ebp], ecx                     ;85.39
        mov       DWORD PTR [-272+ebp], ecx                     ;85.29
        mov       DWORD PTR [-268+ebp], ecx                     ;85.29
        mov       DWORD PTR [-264+ebp], ecx                     ;85.29
        mov       DWORD PTR [-252+ebp], ecx                     ;85.29
        mov       DWORD PTR [-248+ebp], ecx                     ;85.29
        mov       DWORD PTR [-244+ebp], ecx                     ;85.29
        mov       DWORD PTR [-240+ebp], ecx                     ;85.29
        mov       DWORD PTR [-232+ebp], ecx                     ;84.49
        mov       DWORD PTR [-228+ebp], ecx                     ;84.49
        mov       DWORD PTR [-224+ebp], ecx                     ;84.49
        mov       DWORD PTR [-212+ebp], ecx                     ;84.49
        mov       DWORD PTR [-208+ebp], ecx                     ;84.49
        mov       DWORD PTR [-204+ebp], ecx                     ;84.49
        mov       DWORD PTR [-200+ebp], ecx                     ;84.49
        mov       DWORD PTR [-192+ebp], ecx                     ;84.39
        mov       DWORD PTR [-188+ebp], ecx                     ;84.39
        mov       DWORD PTR [-184+ebp], ecx                     ;84.39
        mov       DWORD PTR [-172+ebp], ecx                     ;84.39
        mov       DWORD PTR [-168+ebp], ecx                     ;84.39
        mov       DWORD PTR [-164+ebp], ecx                     ;84.39
        mov       DWORD PTR [-160+ebp], ecx                     ;84.39
        mov       DWORD PTR [-152+ebp], ecx                     ;84.29
        mov       DWORD PTR [-148+ebp], ecx                     ;84.29
        mov       DWORD PTR [-144+ebp], ecx                     ;84.29
        mov       DWORD PTR [-132+ebp], ecx                     ;84.29
        mov       DWORD PTR [-128+ebp], ecx                     ;84.29
        mov       DWORD PTR [-124+ebp], ecx                     ;84.29
        mov       DWORD PTR [-120+ebp], ecx                     ;84.29
        mov       DWORD PTR [-112+ebp], ecx                     ;83.29
        mov       DWORD PTR [-92+ebp], ecx                      ;83.29
        mov       DWORD PTR [-104+ebp], ecx                     ;87.8
        lea       ecx, DWORD PTR [-56+ebp]                      ;87.8
        push      2                                             ;87.8
        push      ecx                                           ;87.8
        mov       DWORD PTR [-100+ebp], 133                     ;87.8
        mov       DWORD PTR [-108+ebp], eax                     ;87.8
        mov       DWORD PTR [-88+ebp], edi                      ;87.8
        mov       DWORD PTR [-84+ebp], eax                      ;87.8
        call      _for_check_mult_overflow                      ;87.8
                                ; LOE eax
.B3.524:                        ; Preds .B3.1
        add       esp, 16                                       ;87.8
                                ; LOE eax
.B3.2:                          ; Preds .B3.524
        mov       edx, DWORD PTR [-100+ebp]                     ;87.8
        and       eax, 1                                        ;87.8
        and       edx, 1                                        ;87.8
        lea       ecx, DWORD PTR [-112+ebp]                     ;87.8
        shl       eax, 4                                        ;87.8
        add       edx, edx                                      ;87.8
        or        edx, eax                                      ;87.8
        or        edx, 262144                                   ;87.8
        push      edx                                           ;87.8
        push      ecx                                           ;87.8
        push      DWORD PTR [-56+ebp]                           ;87.8
        call      _for_alloc_allocatable                        ;87.8
                                ; LOE
.B3.525:                        ; Preds .B3.2
        add       esp, 12                                       ;87.8
                                ; LOE
.B3.3:                          ; Preds .B3.525
        mov       esi, DWORD PTR [-88+ebp]                      ;88.8
        test      esi, esi                                      ;88.8
        jle       .B3.6         ; Prob 50%                      ;88.8
                                ; LOE esi
.B3.4:                          ; Preds .B3.3
        mov       ecx, DWORD PTR [-112+ebp]                     ;88.8
        cmp       esi, 24                                       ;88.8
        jle       .B3.317       ; Prob 0%                       ;88.8
                                ; LOE ecx esi
.B3.5:                          ; Preds .B3.4
        shl       esi, 2                                        ;88.8
        push      esi                                           ;88.8
        push      0                                             ;88.8
        push      ecx                                           ;88.8
        call      __intel_fast_memset                           ;88.8
                                ; LOE
.B3.526:                        ; Preds .B3.5
        add       esp, 12                                       ;88.8
                                ; LOE
.B3.6:                          ; Preds .B3.331 .B3.526 .B3.3 .B3.329
        mov       esi, DWORD PTR [8+ebx]                        ;89.8
        mov       ecx, 1                                        ;89.8
        mov       edi, DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY+32] ;89.24
        mov       edx, 4                                        ;89.8
        neg       edi                                           ;89.8
        xor       eax, eax                                      ;89.8
        add       edi, DWORD PTR [esi]                          ;89.8
        mov       DWORD PTR [-136+ebp], ecx                     ;89.8
        mov       DWORD PTR [-120+ebp], ecx                     ;89.8
        mov       ecx, DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY] ;89.8
        lea       esi, DWORD PTR [edi+edi*4]                    ;89.8
        push      edx                                           ;89.8
        mov       DWORD PTR [-144+ebp], eax                     ;89.8
        movsx     ecx, WORD PTR [36+ecx+esi*8]                  ;89.8
        test      ecx, ecx                                      ;89.8
        mov       DWORD PTR [-140+ebp], 133                     ;89.8
        cmovl     ecx, eax                                      ;89.8
        lea       eax, DWORD PTR [-52+ebp]                      ;89.8
        push      ecx                                           ;89.8
        push      2                                             ;89.8
        push      eax                                           ;89.8
        mov       DWORD PTR [-148+ebp], edx                     ;89.8
        mov       DWORD PTR [-128+ebp], ecx                     ;89.8
        mov       DWORD PTR [-124+ebp], edx                     ;89.8
        call      _for_check_mult_overflow                      ;89.8
                                ; LOE eax
.B3.527:                        ; Preds .B3.6
        add       esp, 16                                       ;89.8
                                ; LOE eax
.B3.7:                          ; Preds .B3.527
        mov       edx, DWORD PTR [-140+ebp]                     ;89.8
        and       eax, 1                                        ;89.8
        and       edx, 1                                        ;89.8
        lea       ecx, DWORD PTR [-152+ebp]                     ;89.8
        shl       eax, 4                                        ;89.8
        add       edx, edx                                      ;89.8
        or        edx, eax                                      ;89.8
        or        edx, 262144                                   ;89.8
        push      edx                                           ;89.8
        push      ecx                                           ;89.8
        push      DWORD PTR [-52+ebp]                           ;89.8
        call      _for_alloc_allocatable                        ;89.8
                                ; LOE
.B3.528:                        ; Preds .B3.7
        add       esp, 12                                       ;89.8
                                ; LOE
.B3.8:                          ; Preds .B3.528
        mov       esi, DWORD PTR [8+ebx]                        ;89.8
        mov       ecx, 1                                        ;89.8
        mov       edi, DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY+32] ;89.57
        mov       edx, 4                                        ;89.8
        neg       edi                                           ;89.8
        xor       eax, eax                                      ;89.8
        add       edi, DWORD PTR [esi]                          ;89.8
        mov       DWORD PTR [-176+ebp], ecx                     ;89.8
        mov       DWORD PTR [-160+ebp], ecx                     ;89.8
        mov       ecx, DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY] ;89.8
        lea       esi, DWORD PTR [edi+edi*4]                    ;89.8
        push      edx                                           ;89.8
        mov       DWORD PTR [-184+ebp], eax                     ;89.8
        movsx     ecx, WORD PTR [36+ecx+esi*8]                  ;89.8
        test      ecx, ecx                                      ;89.8
        mov       DWORD PTR [-180+ebp], 133                     ;89.8
        cmovl     ecx, eax                                      ;89.8
        lea       eax, DWORD PTR [-48+ebp]                      ;89.8
        push      ecx                                           ;89.8
        push      2                                             ;89.8
        push      eax                                           ;89.8
        mov       DWORD PTR [-188+ebp], edx                     ;89.8
        mov       DWORD PTR [-168+ebp], ecx                     ;89.8
        mov       DWORD PTR [-164+ebp], edx                     ;89.8
        call      _for_check_mult_overflow                      ;89.8
                                ; LOE eax
.B3.529:                        ; Preds .B3.8
        add       esp, 16                                       ;89.8
                                ; LOE eax
.B3.9:                          ; Preds .B3.529
        mov       edx, DWORD PTR [-180+ebp]                     ;89.8
        and       eax, 1                                        ;89.8
        and       edx, 1                                        ;89.8
        lea       ecx, DWORD PTR [-192+ebp]                     ;89.8
        shl       eax, 4                                        ;89.8
        add       edx, edx                                      ;89.8
        or        edx, eax                                      ;89.8
        or        edx, 262144                                   ;89.8
        push      edx                                           ;89.8
        push      ecx                                           ;89.8
        push      DWORD PTR [-48+ebp]                           ;89.8
        call      _for_alloc_allocatable                        ;89.8
                                ; LOE
.B3.530:                        ; Preds .B3.9
        add       esp, 12                                       ;89.8
                                ; LOE
.B3.10:                         ; Preds .B3.530
        mov       esi, DWORD PTR [8+ebx]                        ;89.8
        mov       ecx, 1                                        ;89.8
        mov       edi, DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY+32] ;89.90
        mov       edx, 4                                        ;89.8
        neg       edi                                           ;89.8
        xor       eax, eax                                      ;89.8
        add       edi, DWORD PTR [esi]                          ;89.8
        mov       DWORD PTR [-216+ebp], ecx                     ;89.8
        mov       DWORD PTR [-200+ebp], ecx                     ;89.8
        mov       ecx, DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY] ;89.8
        lea       esi, DWORD PTR [edi+edi*4]                    ;89.8
        push      edx                                           ;89.8
        mov       DWORD PTR [-224+ebp], eax                     ;89.8
        movsx     ecx, WORD PTR [36+ecx+esi*8]                  ;89.8
        test      ecx, ecx                                      ;89.8
        mov       DWORD PTR [-220+ebp], 133                     ;89.8
        cmovl     ecx, eax                                      ;89.8
        lea       eax, DWORD PTR [-44+ebp]                      ;89.8
        push      ecx                                           ;89.8
        push      2                                             ;89.8
        push      eax                                           ;89.8
        mov       DWORD PTR [-228+ebp], edx                     ;89.8
        mov       DWORD PTR [-208+ebp], ecx                     ;89.8
        mov       DWORD PTR [-204+ebp], edx                     ;89.8
        call      _for_check_mult_overflow                      ;89.8
                                ; LOE eax
.B3.531:                        ; Preds .B3.10
        add       esp, 16                                       ;89.8
                                ; LOE eax
.B3.11:                         ; Preds .B3.531
        mov       edx, DWORD PTR [-220+ebp]                     ;89.8
        and       eax, 1                                        ;89.8
        and       edx, 1                                        ;89.8
        lea       ecx, DWORD PTR [-232+ebp]                     ;89.8
        shl       eax, 4                                        ;89.8
        add       edx, edx                                      ;89.8
        or        edx, eax                                      ;89.8
        or        edx, 262144                                   ;89.8
        push      edx                                           ;89.8
        push      ecx                                           ;89.8
        push      DWORD PTR [-44+ebp]                           ;89.8
        call      _for_alloc_allocatable                        ;89.8
                                ; LOE
.B3.532:                        ; Preds .B3.11
        add       esp, 12                                       ;89.8
                                ; LOE
.B3.12:                         ; Preds .B3.532
        mov       eax, DWORD PTR [-120+ebp]                     ;90.8
        mov       edx, DWORD PTR [-128+ebp]                     ;90.8
        test      edx, edx                                      ;90.8
        mov       DWORD PTR [-72+ebp], eax                      ;90.8
        mov       DWORD PTR [-20+ebp], edx                      ;90.8
        jle       .B3.15        ; Prob 50%                      ;90.8
                                ; LOE edx dl dh
.B3.13:                         ; Preds .B3.12
        mov       ecx, DWORD PTR [-152+ebp]                     ;90.8
        cmp       DWORD PTR [-20+ebp], 24                       ;90.8
        jle       .B3.336       ; Prob 0%                       ;90.8
                                ; LOE edx ecx dl dh
.B3.14:                         ; Preds .B3.13
        mov       eax, edx                                      ;90.8
        lea       edx, DWORD PTR [eax*4]                        ;90.8
        push      edx                                           ;90.8
        push      0                                             ;90.8
        push      ecx                                           ;90.8
        call      __intel_fast_memset                           ;90.8
                                ; LOE
.B3.533:                        ; Preds .B3.14
        add       esp, 12                                       ;90.8
                                ; LOE
.B3.15:                         ; Preds .B3.350 .B3.533 .B3.12 .B3.348
        mov       eax, DWORD PTR [-160+ebp]                     ;91.8
        mov       esi, DWORD PTR [-168+ebp]                     ;91.8
        test      esi, esi                                      ;91.8
        mov       DWORD PTR [-76+ebp], eax                      ;91.8
        jle       .B3.18        ; Prob 50%                      ;91.8
                                ; LOE esi
.B3.16:                         ; Preds .B3.15
        mov       ecx, DWORD PTR [-192+ebp]                     ;91.8
        cmp       esi, 24                                       ;91.8
        jle       .B3.355       ; Prob 0%                       ;91.8
                                ; LOE ecx esi
.B3.17:                         ; Preds .B3.16
        shl       esi, 2                                        ;91.8
        push      esi                                           ;91.8
        push      0                                             ;91.8
        push      ecx                                           ;91.8
        call      __intel_fast_memset                           ;91.8
                                ; LOE
.B3.534:                        ; Preds .B3.17
        add       esp, 12                                       ;91.8
                                ; LOE
.B3.18:                         ; Preds .B3.369 .B3.534 .B3.15 .B3.367
        mov       eax, DWORD PTR [-200+ebp]                     ;92.8
        mov       esi, DWORD PTR [-208+ebp]                     ;92.8
        test      esi, esi                                      ;92.8
        mov       DWORD PTR [-116+ebp], eax                     ;92.8
        jle       .B3.21        ; Prob 50%                      ;92.8
                                ; LOE esi
.B3.19:                         ; Preds .B3.18
        mov       ecx, DWORD PTR [-232+ebp]                     ;92.8
        cmp       esi, 24                                       ;92.8
        jle       .B3.374       ; Prob 0%                       ;92.8
                                ; LOE ecx esi
.B3.20:                         ; Preds .B3.19
        shl       esi, 2                                        ;92.8
        push      esi                                           ;92.8
        push      0                                             ;92.8
        push      ecx                                           ;92.8
        call      __intel_fast_memset                           ;92.8
                                ; LOE
.B3.535:                        ; Preds .B3.20
        add       esp, 12                                       ;92.8
                                ; LOE
.B3.21:                         ; Preds .B3.388 .B3.386 .B3.535 .B3.18
        mov       eax, DWORD PTR [8+ebx]                        ;93.21
        mov       edx, DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY+32] ;93.21
        mov       esi, DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY] ;93.8
        mov       ecx, DWORD PTR [eax]                          ;93.21
        mov       DWORD PTR [-28+ebp], esi                      ;93.8
        lea       eax, DWORD PTR [edx+edx*4]                    ;93.15
        shl       eax, 3                                        ;93.15
        mov       DWORD PTR [-24+ebp], ecx                      ;93.21
        lea       edi, DWORD PTR [ecx+ecx*4]                    ;93.15
        mov       DWORD PTR [-32+ebp], edx                      ;93.21
        lea       esi, DWORD PTR [esi+edi*8]                    ;93.15
        sub       esi, eax                                      ;93.15
        mov       DWORD PTR [-156+ebp], eax                     ;93.15
        movsx     ecx, WORD PTR [36+esi]                        ;93.21
        test      ecx, ecx                                      ;94.10
        jle       .B3.521       ; Prob 0%                       ;94.10
                                ; LOE ecx esi
.B3.22:                         ; Preds .B3.21
        mov       edx, DWORD PTR [28+esi]                       ;94.20
        cmp       edx, 4                                        ;94.10
        mov       eax, DWORD PTR [-152+ebp]                     ;94.20
        mov       DWORD PTR [-68+ebp], edx                      ;94.20
        jne       .B3.43        ; Prob 50%                      ;94.10
                                ; LOE eax ecx esi
.B3.23:                         ; Preds .B3.22
        cmp       ecx, 4                                        ;94.10
        jl        .B3.393       ; Prob 10%                      ;94.10
                                ; LOE eax ecx esi
.B3.24:                         ; Preds .B3.23
        mov       edx, DWORD PTR [-72+ebp]                      ;94.10
        lea       edi, DWORD PTR [edx*4]                        ;94.10
        neg       edi                                           ;94.10
        add       edi, eax                                      ;94.10
        mov       DWORD PTR [-360+ebp], edi                     ;94.10
        lea       edx, DWORD PTR [4+edi]                        ;94.10
        and       edx, 15                                       ;94.10
        je        .B3.27        ; Prob 50%                      ;94.10
                                ; LOE eax edx ecx esi
.B3.25:                         ; Preds .B3.24
        test      dl, 3                                         ;94.10
        jne       .B3.393       ; Prob 10%                      ;94.10
                                ; LOE eax edx ecx esi
.B3.26:                         ; Preds .B3.25
        neg       edx                                           ;94.10
        add       edx, 16                                       ;94.10
        shr       edx, 2                                        ;94.10
                                ; LOE eax edx ecx esi
.B3.27:                         ; Preds .B3.26 .B3.24
        lea       edi, DWORD PTR [4+edx]                        ;94.10
        cmp       ecx, edi                                      ;94.10
        jl        .B3.393       ; Prob 10%                      ;94.10
                                ; LOE eax edx ecx esi
.B3.28:                         ; Preds .B3.27
        mov       edi, ecx                                      ;94.10
        sub       edi, edx                                      ;94.10
        and       edi, 3                                        ;94.10
        neg       edi                                           ;94.10
        add       edi, ecx                                      ;94.10
        mov       DWORD PTR [-356+ebp], edi                     ;94.10
        mov       edi, DWORD PTR [32+esi]                       ;94.20
        mov       DWORD PTR [-196+ebp], edi                     ;94.20
        mov       DWORD PTR [-276+ebp], eax                     ;
        mov       eax, DWORD PTR [esi]                          ;94.20
        shl       edi, 2                                        ;
        sub       eax, edi                                      ;
        mov       DWORD PTR [-316+ebp], eax                     ;
        test      edx, edx                                      ;94.10
        mov       eax, DWORD PTR [-276+ebp]                     ;94.10
        jbe       .B3.32        ; Prob 3%                       ;94.10
                                ; LOE eax edx ecx esi al ah
.B3.29:                         ; Preds .B3.28
        mov       DWORD PTR [-60+ebp], ecx                      ;
        xor       edi, edi                                      ;
        mov       DWORD PTR [-236+ebp], esi                     ;
        mov       DWORD PTR [-276+ebp], eax                     ;
        mov       ecx, DWORD PTR [-316+ebp]                     ;
        mov       esi, DWORD PTR [-360+ebp]                     ;
                                ; LOE edx ecx esi edi
.B3.30:                         ; Preds .B3.30 .B3.29
        mov       eax, DWORD PTR [4+ecx+edi*4]                  ;94.10
        mov       DWORD PTR [4+esi+edi*4], eax                  ;94.10
        inc       edi                                           ;94.10
        cmp       edi, edx                                      ;94.10
        jb        .B3.30        ; Prob 82%                      ;94.10
                                ; LOE edx ecx esi edi
.B3.31:                         ; Preds .B3.30
        mov       eax, DWORD PTR [-276+ebp]                     ;
        mov       ecx, DWORD PTR [-60+ebp]                      ;
        mov       esi, DWORD PTR [-236+ebp]                     ;
                                ; LOE eax edx ecx esi al ah
.B3.32:                         ; Preds .B3.28 .B3.31
        mov       edi, DWORD PTR [-316+ebp]                     ;94.10
        lea       edi, DWORD PTR [4+edi+edx*4]                  ;94.10
        test      edi, 15                                       ;94.10
        je        .B3.36        ; Prob 60%                      ;94.10
                                ; LOE eax edx ecx esi al ah
.B3.33:                         ; Preds .B3.32
        mov       DWORD PTR [-60+ebp], ecx                      ;
        mov       DWORD PTR [-236+ebp], esi                     ;
        mov       ecx, DWORD PTR [-356+ebp]                     ;
        mov       esi, DWORD PTR [-316+ebp]                     ;
        mov       edi, DWORD PTR [-360+ebp]                     ;
                                ; LOE eax edx ecx esi edi
.B3.34:                         ; Preds .B3.34 .B3.33
        movdqu    xmm0, XMMWORD PTR [4+esi+edx*4]               ;94.10
        movdqa    XMMWORD PTR [4+edi+edx*4], xmm0               ;94.10
        add       edx, 4                                        ;94.10
        cmp       edx, ecx                                      ;94.10
        jb        .B3.34        ; Prob 82%                      ;94.10
        jmp       .B3.38        ; Prob 100%                     ;94.10
                                ; LOE eax edx ecx esi edi
.B3.36:                         ; Preds .B3.32
        mov       DWORD PTR [-60+ebp], ecx                      ;
        mov       DWORD PTR [-236+ebp], esi                     ;
        mov       ecx, DWORD PTR [-356+ebp]                     ;
        mov       esi, DWORD PTR [-316+ebp]                     ;
        mov       edi, DWORD PTR [-360+ebp]                     ;
                                ; LOE eax edx ecx esi edi
.B3.37:                         ; Preds .B3.37 .B3.36
        movdqa    xmm0, XMMWORD PTR [4+esi+edx*4]               ;94.10
        movdqa    XMMWORD PTR [4+edi+edx*4], xmm0               ;94.10
        add       edx, 4                                        ;94.10
        cmp       edx, ecx                                      ;94.10
        jb        .B3.37        ; Prob 82%                      ;94.10
                                ; LOE eax edx ecx esi edi
.B3.38:                         ; Preds .B3.34 .B3.37
        mov       DWORD PTR [-356+ebp], ecx                     ;
        mov       ecx, DWORD PTR [-60+ebp]                      ;
        mov       esi, DWORD PTR [-236+ebp]                     ;
                                ; LOE eax ecx esi
.B3.39:                         ; Preds .B3.38 .B3.393
        cmp       ecx, DWORD PTR [-356+ebp]                     ;94.10
        jbe       .B3.49        ; Prob 3%                       ;94.10
                                ; LOE eax ecx esi
.B3.40:                         ; Preds .B3.39
        mov       edx, DWORD PTR [-196+ebp]                     ;
        mov       edi, DWORD PTR [-72+ebp]                      ;
        shl       edi, 2                                        ;
        mov       DWORD PTR [-236+ebp], esi                     ;
        shl       edx, 2                                        ;
        neg       edx                                           ;
        sub       eax, edi                                      ;
        add       edx, DWORD PTR [esi]                          ;
        mov       edi, DWORD PTR [-356+ebp]                     ;
                                ; LOE eax edx ecx edi
.B3.41:                         ; Preds .B3.41 .B3.40
        mov       esi, DWORD PTR [4+edx+edi*4]                  ;94.10
        mov       DWORD PTR [4+eax+edi*4], esi                  ;94.10
        inc       edi                                           ;94.10
        cmp       edi, ecx                                      ;94.10
        jb        .B3.41        ; Prob 82%                      ;94.10
                                ; LOE eax edx ecx edi
.B3.42:                         ; Preds .B3.41
        mov       esi, DWORD PTR [-236+ebp]                     ;
        jmp       .B3.49        ; Prob 100%                     ;
                                ; LOE ecx esi
.B3.43:                         ; Preds .B3.22
        mov       edi, ecx                                      ;94.10
        shr       edi, 31                                       ;94.10
        add       edi, ecx                                      ;94.10
        sar       edi, 1                                        ;94.10
        mov       edx, DWORD PTR [32+esi]                       ;94.20
        test      edi, edi                                      ;94.10
        mov       DWORD PTR [-196+ebp], edx                     ;94.20
        jbe       .B3.520       ; Prob 3%                       ;94.10
                                ; LOE eax ecx esi edi
.B3.44:                         ; Preds .B3.43
        mov       DWORD PTR [-64+ebp], edi                      ;
        xor       edx, edx                                      ;
        mov       edi, DWORD PTR [-68+ebp]                      ;
        imul      edi, DWORD PTR [-196+ebp]                     ;
        mov       DWORD PTR [-316+ebp], edx                     ;
        mov       edx, DWORD PTR [esi]                          ;94.20
        sub       edx, edi                                      ;
        mov       edi, DWORD PTR [-72+ebp]                      ;
        mov       DWORD PTR [-360+ebp], edx                     ;
        mov       DWORD PTR [-276+ebp], eax                     ;
        mov       DWORD PTR [-236+ebp], esi                     ;
        lea       edx, DWORD PTR [edi*4]                        ;
        neg       edx                                           ;
        add       edx, eax                                      ;
        mov       DWORD PTR [-356+ebp], edx                     ;
        mov       edx, DWORD PTR [-316+ebp]                     ;
        mov       eax, DWORD PTR [-356+ebp]                     ;
        mov       esi, DWORD PTR [-360+ebp]                     ;
        mov       DWORD PTR [-60+ebp], ecx                      ;
                                ; LOE eax edx esi
.B3.45:                         ; Preds .B3.45 .B3.44
        mov       ecx, DWORD PTR [-68+ebp]                      ;94.10
        lea       edi, DWORD PTR [1+edx+edx]                    ;94.10
        imul      edi, ecx                                      ;94.10
        mov       edi, DWORD PTR [esi+edi]                      ;94.10
        mov       DWORD PTR [4+eax+edx*8], edi                  ;94.10
        lea       edi, DWORD PTR [2+edx+edx]                    ;94.10
        imul      edi, ecx                                      ;94.10
        mov       ecx, DWORD PTR [esi+edi]                      ;94.10
        mov       DWORD PTR [8+eax+edx*8], ecx                  ;94.10
        inc       edx                                           ;94.10
        cmp       edx, DWORD PTR [-64+ebp]                      ;94.10
        jb        .B3.45        ; Prob 63%                      ;94.10
                                ; LOE eax edx esi
.B3.46:                         ; Preds .B3.45
        mov       eax, DWORD PTR [-276+ebp]                     ;
        lea       edx, DWORD PTR [1+edx+edx]                    ;94.10
        mov       ecx, DWORD PTR [-60+ebp]                      ;
        mov       esi, DWORD PTR [-236+ebp]                     ;
                                ; LOE eax edx ecx esi
.B3.47:                         ; Preds .B3.46 .B3.520
        lea       edi, DWORD PTR [-1+edx]                       ;94.10
        cmp       ecx, edi                                      ;94.10
        jbe       .B3.49        ; Prob 3%                       ;94.10
                                ; LOE eax edx ecx esi
.B3.48:                         ; Preds .B3.47
        mov       DWORD PTR [-60+ebp], ecx                      ;
        mov       ecx, DWORD PTR [-196+ebp]                     ;94.10
        neg       ecx                                           ;94.10
        add       ecx, edx                                      ;94.10
        imul      ecx, DWORD PTR [-68+ebp]                      ;94.10
        mov       DWORD PTR [-236+ebp], esi                     ;
        mov       edi, DWORD PTR [esi]                          ;94.20
        mov       esi, DWORD PTR [-72+ebp]                      ;94.10
        neg       esi                                           ;94.10
        add       esi, edx                                      ;94.10
        mov       edx, DWORD PTR [edi+ecx]                      ;94.10
        mov       ecx, DWORD PTR [-60+ebp]                      ;94.10
        mov       DWORD PTR [eax+esi*4], edx                    ;94.10
        mov       esi, DWORD PTR [-236+ebp]                     ;94.10
                                ; LOE ecx esi
.B3.49:                         ; Preds .B3.42 .B3.39 .B3.48 .B3.47
        mov       edx, DWORD PTR [-192+ebp]                     ;95.20
        cmp       DWORD PTR [-68+ebp], 4                        ;95.10
        jne       .B3.70        ; Prob 50%                      ;95.10
                                ; LOE edx ecx esi
.B3.50:                         ; Preds .B3.49
        cmp       ecx, 4                                        ;95.10
        jl        .B3.396       ; Prob 10%                      ;95.10
                                ; LOE edx ecx esi
.B3.51:                         ; Preds .B3.50
        mov       eax, DWORD PTR [-76+ebp]                      ;95.10
        lea       edi, DWORD PTR [eax*4]                        ;95.10
        neg       edi                                           ;95.10
        add       edi, edx                                      ;95.10
        mov       DWORD PTR [-356+ebp], edi                     ;95.10
        lea       edi, DWORD PTR [4+edi]                        ;95.10
        and       edi, 15                                       ;95.10
        je        .B3.54        ; Prob 50%                      ;95.10
                                ; LOE edx ecx esi edi
.B3.52:                         ; Preds .B3.51
        test      edi, 3                                        ;95.10
        jne       .B3.396       ; Prob 10%                      ;95.10
                                ; LOE edx ecx esi edi
.B3.53:                         ; Preds .B3.52
        neg       edi                                           ;95.10
        add       edi, 16                                       ;95.10
        shr       edi, 2                                        ;95.10
                                ; LOE edx ecx esi edi
.B3.54:                         ; Preds .B3.53 .B3.51
        lea       eax, DWORD PTR [4+edi]                        ;95.10
        cmp       ecx, eax                                      ;95.10
        jl        .B3.396       ; Prob 10%                      ;95.10
                                ; LOE edx ecx esi edi
.B3.55:                         ; Preds .B3.54
        mov       eax, ecx                                      ;95.10
        sub       eax, edi                                      ;95.10
        and       eax, 3                                        ;95.10
        neg       eax                                           ;95.10
        add       eax, ecx                                      ;95.10
        mov       DWORD PTR [-316+ebp], eax                     ;95.10
        mov       eax, DWORD PTR [-196+ebp]                     ;
        shl       eax, 2                                        ;
        neg       eax                                           ;
        add       eax, DWORD PTR [esi]                          ;
        mov       DWORD PTR [-276+ebp], eax                     ;
        test      edi, edi                                      ;95.10
        jbe       .B3.59        ; Prob 3%                       ;95.10
                                ; LOE edx ecx esi edi
.B3.56:                         ; Preds .B3.55
        mov       DWORD PTR [-60+ebp], ecx                      ;
        xor       eax, eax                                      ;
        mov       DWORD PTR [-236+ebp], esi                     ;
        mov       DWORD PTR [-360+ebp], edx                     ;
        mov       ecx, DWORD PTR [-276+ebp]                     ;
        mov       esi, DWORD PTR [-356+ebp]                     ;
                                ; LOE eax ecx esi edi
.B3.57:                         ; Preds .B3.57 .B3.56
        mov       edx, DWORD PTR [8+ecx+eax*4]                  ;95.10
        mov       DWORD PTR [4+esi+eax*4], edx                  ;95.10
        inc       eax                                           ;95.10
        cmp       eax, edi                                      ;95.10
        jb        .B3.57        ; Prob 82%                      ;95.10
                                ; LOE eax ecx esi edi
.B3.58:                         ; Preds .B3.57
        mov       edx, DWORD PTR [-360+ebp]                     ;
        mov       ecx, DWORD PTR [-60+ebp]                      ;
        mov       esi, DWORD PTR [-236+ebp]                     ;
                                ; LOE edx ecx esi edi
.B3.59:                         ; Preds .B3.55 .B3.58
        mov       eax, DWORD PTR [-276+ebp]                     ;95.10
        lea       eax, DWORD PTR [8+eax+edi*4]                  ;95.10
        test      al, 15                                        ;95.10
        je        .B3.63        ; Prob 60%                      ;95.10
                                ; LOE edx ecx esi edi
.B3.60:                         ; Preds .B3.59
        mov       DWORD PTR [-60+ebp], ecx                      ;
        mov       DWORD PTR [-236+ebp], esi                     ;
        mov       eax, DWORD PTR [-316+ebp]                     ;
        mov       ecx, DWORD PTR [-276+ebp]                     ;
        mov       esi, DWORD PTR [-356+ebp]                     ;
                                ; LOE eax edx ecx esi edi
.B3.61:                         ; Preds .B3.61 .B3.60
        movdqu    xmm0, XMMWORD PTR [8+ecx+edi*4]               ;95.10
        movdqa    XMMWORD PTR [4+esi+edi*4], xmm0               ;95.10
        add       edi, 4                                        ;95.10
        cmp       edi, eax                                      ;95.10
        jb        .B3.61        ; Prob 82%                      ;95.10
        jmp       .B3.65        ; Prob 100%                     ;95.10
                                ; LOE eax edx ecx esi edi
.B3.63:                         ; Preds .B3.59
        mov       DWORD PTR [-60+ebp], ecx                      ;
        mov       DWORD PTR [-236+ebp], esi                     ;
        mov       eax, DWORD PTR [-316+ebp]                     ;
        mov       ecx, DWORD PTR [-276+ebp]                     ;
        mov       esi, DWORD PTR [-356+ebp]                     ;
                                ; LOE eax edx ecx esi edi
.B3.64:                         ; Preds .B3.64 .B3.63
        movdqa    xmm0, XMMWORD PTR [8+ecx+edi*4]               ;95.10
        movdqa    XMMWORD PTR [4+esi+edi*4], xmm0               ;95.10
        add       edi, 4                                        ;95.10
        cmp       edi, eax                                      ;95.10
        jb        .B3.64        ; Prob 82%                      ;95.10
                                ; LOE eax edx ecx esi edi
.B3.65:                         ; Preds .B3.61 .B3.64
        mov       DWORD PTR [-316+ebp], eax                     ;
        mov       ecx, DWORD PTR [-60+ebp]                      ;
        mov       esi, DWORD PTR [-236+ebp]                     ;
                                ; LOE edx ecx esi
.B3.66:                         ; Preds .B3.65 .B3.396
        cmp       ecx, DWORD PTR [-316+ebp]                     ;95.10
        jbe       .B3.76        ; Prob 3%                       ;95.10
                                ; LOE edx ecx esi
.B3.67:                         ; Preds .B3.66
        mov       eax, DWORD PTR [-196+ebp]                     ;
        mov       edi, DWORD PTR [-76+ebp]                      ;
        shl       edi, 2                                        ;
        mov       DWORD PTR [-236+ebp], esi                     ;
        shl       eax, 2                                        ;
        neg       eax                                           ;
        sub       edx, edi                                      ;
        add       eax, DWORD PTR [esi]                          ;
        mov       edi, DWORD PTR [-316+ebp]                     ;
                                ; LOE eax edx ecx edi
.B3.68:                         ; Preds .B3.68 .B3.67
        mov       esi, DWORD PTR [8+eax+edi*4]                  ;95.10
        mov       DWORD PTR [4+edx+edi*4], esi                  ;95.10
        inc       edi                                           ;95.10
        cmp       edi, ecx                                      ;95.10
        jb        .B3.68        ; Prob 82%                      ;95.10
                                ; LOE eax edx ecx edi
.B3.69:                         ; Preds .B3.68
        mov       esi, DWORD PTR [-236+ebp]                     ;
        jmp       .B3.76        ; Prob 100%                     ;
                                ; LOE ecx esi
.B3.70:                         ; Preds .B3.49
        mov       edi, ecx                                      ;95.10
        shr       edi, 31                                       ;95.10
        add       edi, ecx                                      ;95.10
        sar       edi, 1                                        ;95.10
        test      edi, edi                                      ;95.10
        jbe       .B3.519       ; Prob 3%                       ;95.10
                                ; LOE edx ecx esi edi
.B3.71:                         ; Preds .B3.70
        mov       DWORD PTR [-72+ebp], edi                      ;
        xor       eax, eax                                      ;
        mov       edi, DWORD PTR [-68+ebp]                      ;
        imul      edi, DWORD PTR [-196+ebp]                     ;
        mov       DWORD PTR [-276+ebp], eax                     ;
        mov       eax, DWORD PTR [esi]                          ;95.20
        sub       eax, edi                                      ;
        mov       edi, DWORD PTR [-76+ebp]                      ;
        mov       DWORD PTR [-356+ebp], eax                     ;
        mov       DWORD PTR [-360+ebp], edx                     ;
        mov       DWORD PTR [-236+ebp], esi                     ;
        lea       eax, DWORD PTR [edi*4]                        ;
        neg       eax                                           ;
        add       eax, edx                                      ;
        mov       DWORD PTR [-316+ebp], eax                     ;
        mov       eax, DWORD PTR [-276+ebp]                     ;
        mov       edx, DWORD PTR [-316+ebp]                     ;
        mov       esi, DWORD PTR [-356+ebp]                     ;
        mov       DWORD PTR [-60+ebp], ecx                      ;
                                ; LOE eax edx esi
.B3.72:                         ; Preds .B3.72 .B3.71
        mov       ecx, DWORD PTR [-68+ebp]                      ;95.10
        lea       edi, DWORD PTR [1+eax+eax]                    ;95.10
        imul      edi, ecx                                      ;95.10
        mov       edi, DWORD PTR [4+esi+edi]                    ;95.10
        mov       DWORD PTR [4+edx+eax*8], edi                  ;95.10
        lea       edi, DWORD PTR [2+eax+eax]                    ;95.10
        imul      edi, ecx                                      ;95.10
        mov       ecx, DWORD PTR [4+esi+edi]                    ;95.10
        mov       DWORD PTR [8+edx+eax*8], ecx                  ;95.10
        inc       eax                                           ;95.10
        cmp       eax, DWORD PTR [-72+ebp]                      ;95.10
        jb        .B3.72        ; Prob 63%                      ;95.10
                                ; LOE eax edx esi
.B3.73:                         ; Preds .B3.72
        mov       edx, DWORD PTR [-360+ebp]                     ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;95.10
        mov       ecx, DWORD PTR [-60+ebp]                      ;
        mov       esi, DWORD PTR [-236+ebp]                     ;
                                ; LOE eax edx ecx esi
.B3.74:                         ; Preds .B3.73 .B3.519
        lea       edi, DWORD PTR [-1+eax]                       ;95.10
        cmp       ecx, edi                                      ;95.10
        jbe       .B3.76        ; Prob 3%                       ;95.10
                                ; LOE eax edx ecx esi
.B3.75:                         ; Preds .B3.74
        mov       DWORD PTR [-60+ebp], ecx                      ;
        mov       ecx, DWORD PTR [-196+ebp]                     ;95.10
        neg       ecx                                           ;95.10
        add       ecx, eax                                      ;95.10
        imul      ecx, DWORD PTR [-68+ebp]                      ;95.10
        mov       DWORD PTR [-236+ebp], esi                     ;
        mov       edi, DWORD PTR [esi]                          ;95.20
        mov       esi, DWORD PTR [-76+ebp]                      ;95.10
        neg       esi                                           ;95.10
        add       esi, eax                                      ;95.10
        mov       eax, DWORD PTR [4+edi+ecx]                    ;95.10
        mov       ecx, DWORD PTR [-60+ebp]                      ;95.10
        mov       DWORD PTR [edx+esi*4], eax                    ;95.10
        mov       esi, DWORD PTR [-236+ebp]                     ;95.10
                                ; LOE ecx esi
.B3.76:                         ; Preds .B3.66 .B3.69 .B3.74 .B3.75
        mov       edx, DWORD PTR [-232+ebp]                     ;96.20
        cmp       DWORD PTR [-68+ebp], 4                        ;96.10
        jne       .B3.97        ; Prob 50%                      ;96.10
                                ; LOE edx ecx esi
.B3.77:                         ; Preds .B3.76
        cmp       ecx, 4                                        ;96.10
        jl        .B3.399       ; Prob 10%                      ;96.10
                                ; LOE edx ecx esi
.B3.78:                         ; Preds .B3.77
        mov       eax, DWORD PTR [-116+ebp]                     ;96.10
        lea       edi, DWORD PTR [eax*4]                        ;96.10
        neg       edi                                           ;96.10
        add       edi, edx                                      ;96.10
        mov       DWORD PTR [-356+ebp], edi                     ;96.10
        lea       edi, DWORD PTR [4+edi]                        ;96.10
        and       edi, 15                                       ;96.10
        je        .B3.81        ; Prob 50%                      ;96.10
                                ; LOE edx ecx esi edi
.B3.79:                         ; Preds .B3.78
        test      edi, 3                                        ;96.10
        jne       .B3.399       ; Prob 10%                      ;96.10
                                ; LOE edx ecx esi edi
.B3.80:                         ; Preds .B3.79
        neg       edi                                           ;96.10
        add       edi, 16                                       ;96.10
        shr       edi, 2                                        ;96.10
                                ; LOE edx ecx esi edi
.B3.81:                         ; Preds .B3.80 .B3.78
        lea       eax, DWORD PTR [4+edi]                        ;96.10
        cmp       ecx, eax                                      ;96.10
        jl        .B3.399       ; Prob 10%                      ;96.10
                                ; LOE edx ecx esi edi
.B3.82:                         ; Preds .B3.81
        mov       eax, ecx                                      ;96.10
        sub       eax, edi                                      ;96.10
        and       eax, 3                                        ;96.10
        neg       eax                                           ;96.10
        add       eax, ecx                                      ;96.10
        mov       DWORD PTR [-316+ebp], eax                     ;96.10
        mov       eax, DWORD PTR [-196+ebp]                     ;
        shl       eax, 2                                        ;
        neg       eax                                           ;
        add       eax, DWORD PTR [esi]                          ;
        mov       DWORD PTR [-276+ebp], eax                     ;
        test      edi, edi                                      ;96.10
        jbe       .B3.86        ; Prob 3%                       ;96.10
                                ; LOE edx ecx esi edi
.B3.83:                         ; Preds .B3.82
        mov       DWORD PTR [-60+ebp], ecx                      ;
        xor       eax, eax                                      ;
        mov       DWORD PTR [-236+ebp], esi                     ;
        mov       DWORD PTR [-360+ebp], edx                     ;
        mov       ecx, DWORD PTR [-276+ebp]                     ;
        mov       esi, DWORD PTR [-356+ebp]                     ;
                                ; LOE eax ecx esi edi
.B3.84:                         ; Preds .B3.84 .B3.83
        cvttss2si edx, DWORD PTR [12+ecx+eax*4]                 ;96.10
        mov       DWORD PTR [4+esi+eax*4], edx                  ;96.10
        inc       eax                                           ;96.10
        cmp       eax, edi                                      ;96.10
        jb        .B3.84        ; Prob 82%                      ;96.10
                                ; LOE eax ecx esi edi
.B3.85:                         ; Preds .B3.84
        mov       edx, DWORD PTR [-360+ebp]                     ;
        mov       ecx, DWORD PTR [-60+ebp]                      ;
        mov       esi, DWORD PTR [-236+ebp]                     ;
                                ; LOE edx ecx esi edi
.B3.86:                         ; Preds .B3.82 .B3.85
        mov       eax, DWORD PTR [-276+ebp]                     ;96.10
        lea       eax, DWORD PTR [12+eax+edi*4]                 ;96.10
        test      al, 15                                        ;96.10
        je        .B3.90        ; Prob 60%                      ;96.10
                                ; LOE edx ecx esi edi
.B3.87:                         ; Preds .B3.86
        mov       DWORD PTR [-60+ebp], ecx                      ;
        mov       DWORD PTR [-236+ebp], esi                     ;
        mov       eax, DWORD PTR [-316+ebp]                     ;
        mov       ecx, DWORD PTR [-276+ebp]                     ;
        mov       esi, DWORD PTR [-356+ebp]                     ;
                                ; LOE eax edx ecx esi edi
.B3.88:                         ; Preds .B3.88 .B3.87
        movups    xmm0, XMMWORD PTR [12+ecx+edi*4]              ;96.20
        cvttps2dq xmm1, xmm0                                    ;96.10
        movdqa    XMMWORD PTR [4+esi+edi*4], xmm1               ;96.10
        add       edi, 4                                        ;96.10
        cmp       edi, eax                                      ;96.10
        jb        .B3.88        ; Prob 82%                      ;96.10
        jmp       .B3.92        ; Prob 100%                     ;96.10
                                ; LOE eax edx ecx esi edi
.B3.90:                         ; Preds .B3.86
        mov       DWORD PTR [-60+ebp], ecx                      ;
        mov       DWORD PTR [-236+ebp], esi                     ;
        mov       eax, DWORD PTR [-316+ebp]                     ;
        mov       ecx, DWORD PTR [-276+ebp]                     ;
        mov       esi, DWORD PTR [-356+ebp]                     ;
                                ; LOE eax edx ecx esi edi
.B3.91:                         ; Preds .B3.91 .B3.90
        cvttps2dq xmm0, XMMWORD PTR [12+ecx+edi*4]              ;96.10
        movdqa    XMMWORD PTR [4+esi+edi*4], xmm0               ;96.10
        add       edi, 4                                        ;96.10
        cmp       edi, eax                                      ;96.10
        jb        .B3.91        ; Prob 82%                      ;96.10
                                ; LOE eax edx ecx esi edi
.B3.92:                         ; Preds .B3.88 .B3.91
        mov       DWORD PTR [-316+ebp], eax                     ;
        mov       ecx, DWORD PTR [-60+ebp]                      ;
        mov       esi, DWORD PTR [-236+ebp]                     ;
                                ; LOE edx ecx esi
.B3.93:                         ; Preds .B3.92 .B3.399
        cmp       ecx, DWORD PTR [-316+ebp]                     ;96.10
        jbe       .B3.564       ; Prob 3%                       ;96.10
                                ; LOE edx ecx esi
.B3.94:                         ; Preds .B3.93
        mov       eax, DWORD PTR [-196+ebp]                     ;
        mov       edi, DWORD PTR [-116+ebp]                     ;
        shl       edi, 2                                        ;
        mov       DWORD PTR [-236+ebp], esi                     ;
        shl       eax, 2                                        ;
        neg       eax                                           ;
        sub       edx, edi                                      ;
        add       eax, DWORD PTR [esi]                          ;
        mov       edi, DWORD PTR [-316+ebp]                     ;
                                ; LOE eax edx ecx edi
.B3.95:                         ; Preds .B3.95 .B3.94
        cvttss2si esi, DWORD PTR [12+eax+edi*4]                 ;96.10
        mov       DWORD PTR [4+edx+edi*4], esi                  ;96.10
        inc       edi                                           ;96.10
        cmp       edi, ecx                                      ;96.10
        jb        .B3.95        ; Prob 82%                      ;96.10
                                ; LOE eax edx ecx edi
.B3.96:                         ; Preds .B3.95
        mov       esi, DWORD PTR [-236+ebp]                     ;
        test      ecx, ecx                                      ;94.10
        jmp       .B3.103       ; Prob 100%                     ;94.10
                                ; LOE ecx esi
.B3.97:                         ; Preds .B3.76
        mov       edi, ecx                                      ;96.10
        shr       edi, 31                                       ;96.10
        add       edi, ecx                                      ;96.10
        sar       edi, 1                                        ;96.10
        test      edi, edi                                      ;96.10
        jbe       .B3.518       ; Prob 3%                       ;96.10
                                ; LOE edx ecx esi edi
.B3.98:                         ; Preds .B3.97
        mov       DWORD PTR [-76+ebp], edi                      ;
        xor       eax, eax                                      ;
        mov       edi, DWORD PTR [-68+ebp]                      ;
        imul      edi, DWORD PTR [-196+ebp]                     ;
        mov       DWORD PTR [-276+ebp], eax                     ;
        mov       eax, DWORD PTR [esi]                          ;96.20
        sub       eax, edi                                      ;
        mov       edi, DWORD PTR [-116+ebp]                     ;
        mov       DWORD PTR [-356+ebp], eax                     ;
        mov       DWORD PTR [-360+ebp], edx                     ;
        mov       DWORD PTR [-236+ebp], esi                     ;
        lea       eax, DWORD PTR [edi*4]                        ;
        neg       eax                                           ;
        add       eax, edx                                      ;
        mov       DWORD PTR [-316+ebp], eax                     ;
        mov       eax, DWORD PTR [-276+ebp]                     ;
        mov       edx, DWORD PTR [-316+ebp]                     ;
        mov       esi, DWORD PTR [-356+ebp]                     ;
        mov       DWORD PTR [-60+ebp], ecx                      ;
                                ; LOE eax edx esi
.B3.99:                         ; Preds .B3.99 .B3.98
        mov       ecx, DWORD PTR [-68+ebp]                      ;96.10
        lea       edi, DWORD PTR [1+eax+eax]                    ;96.10
        imul      edi, ecx                                      ;96.10
        cvttss2si edi, DWORD PTR [8+esi+edi]                    ;96.10
        mov       DWORD PTR [4+edx+eax*8], edi                  ;96.10
        lea       edi, DWORD PTR [2+eax+eax]                    ;96.10
        imul      edi, ecx                                      ;96.10
        cvttss2si ecx, DWORD PTR [8+esi+edi]                    ;96.10
        mov       DWORD PTR [8+edx+eax*8], ecx                  ;96.10
        inc       eax                                           ;96.10
        cmp       eax, DWORD PTR [-76+ebp]                      ;96.10
        jb        .B3.99        ; Prob 63%                      ;96.10
                                ; LOE eax edx esi
.B3.100:                        ; Preds .B3.99
        mov       edx, DWORD PTR [-360+ebp]                     ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;96.10
        mov       ecx, DWORD PTR [-60+ebp]                      ;
        mov       esi, DWORD PTR [-236+ebp]                     ;
                                ; LOE eax edx ecx esi al ah
.B3.101:                        ; Preds .B3.100 .B3.518
        lea       edi, DWORD PTR [-1+eax]                       ;96.10
        cmp       ecx, edi                                      ;96.10
        jbe       .B3.564       ; Prob 3%                       ;96.10
                                ; LOE eax edx ecx esi al ah
.B3.102:                        ; Preds .B3.101
        mov       edi, DWORD PTR [-196+ebp]                     ;96.10
        neg       edi                                           ;96.10
        add       edi, eax                                      ;96.10
        imul      edi, DWORD PTR [-68+ebp]                      ;96.10
        mov       DWORD PTR [-236+ebp], esi                     ;
        mov       esi, DWORD PTR [esi]                          ;96.20
        cvttss2si esi, DWORD PTR [8+esi+edi]                    ;96.10
        mov       edi, DWORD PTR [-116+ebp]                     ;96.10
        neg       edi                                           ;96.10
        add       edi, eax                                      ;96.10
        test      ecx, ecx                                      ;96.10
        mov       DWORD PTR [edx+edi*4], esi                    ;96.10
        mov       esi, DWORD PTR [-236+ebp]                     ;96.10
                                ; LOE ecx esi
.B3.103:                        ; Preds .B3.521 .B3.96 .B3.102 .B3.564
        mov       eax, DWORD PTR [-20+ebp]                      ;98.8
        mov       edx, 0                                        ;98.8
        cmovg     edx, ecx                                      ;98.8
        mov       edi, esp                                      ;99.28
        shl       eax, 2                                        ;98.8
        imul      eax, edx                                      ;98.8
        call      __alloca_probe                                ;98.8
        and       esp, -16                                      ;98.8
        mov       eax, esp                                      ;98.8
                                ; LOE eax ecx esi edi
.B3.536:                        ; Preds .B3.103
        mov       DWORD PTR [-276+ebp], eax                     ;98.8
        test      ecx, ecx                                      ;99.23
        jle       .B3.105       ; Prob 3%                       ;99.23
                                ; LOE ecx esi edi
.B3.104:                        ; Preds .B3.536
        mov       eax, DWORD PTR [-152+ebp]                     ;118.8
        cmp       DWORD PTR [-20+ebp], 0                        ;99.23
        jg        .B3.294       ; Prob 50%                      ;99.23
        jmp       .B3.563       ; Prob 100%                     ;99.23
                                ; LOE eax ecx esi edi
.B3.105:                        ; Preds .B3.536 .B3.563 .B3.510
        mov       eax, DWORD PTR [-112+ebp]                     ;105.22
        mov       edx, DWORD PTR [-88+ebp]                      ;105.22
        mov       DWORD PTR [-12+ebp], eax                      ;105.22
        jle       .B3.107       ; Prob 2%                       ;99.28
                                ; LOE edx ecx esi edi
.B3.106:                        ; Preds .B3.105
        test      edx, edx                                      ;99.28
        jg        .B3.280       ; Prob 50%                      ;99.28
                                ; LOE edx ecx esi edi
.B3.107:                        ; Preds .B3.105 .B3.106 .B3.505
        mov       eax, edi                                      ;99.28
        mov       esp, eax                                      ;99.28
                                ; LOE edx esi
.B3.537:                        ; Preds .B3.107
        mov       eax, DWORD PTR [-156+ebp]                     ;
        neg       eax                                           ;
        add       eax, DWORD PTR [-28+ebp]                      ;
        mov       DWORD PTR [-156+ebp], eax                     ;
        cmp       DWORD PTR [24+esi], 0                         ;101.8
        jle       .B3.111       ; Prob 10%                      ;101.8
                                ; LOE eax edx al ah
.B3.108:                        ; Preds .B3.537
        mov       DWORD PTR [-76+ebp], edx                      ;
        mov       esi, 1                                        ;
        mov       ecx, DWORD PTR [-196+ebp]                     ;
        mov       edi, DWORD PTR [-24+ebp]                      ;
                                ; LOE eax ecx esi edi
.B3.109:                        ; Preds .B3.109 .B3.108
        inc       esi                                           ;101.8
        lea       edi, DWORD PTR [edi+edi*4]                    ;101.8
        mov       edx, DWORD PTR [32+eax+edi*8]                 ;101.8
        neg       edx                                           ;101.8
        add       edx, ecx                                      ;101.8
        inc       ecx                                           ;101.8
        imul      edx, DWORD PTR [28+eax+edi*8]                 ;101.8
        mov       edi, DWORD PTR [eax+edi*8]                    ;101.8
        mov       DWORD PTR [edi+edx], 0                        ;101.8
        mov       edx, DWORD PTR [8+ebx]                        ;101.8
        mov       edi, DWORD PTR [edx]                          ;101.8
        lea       edx, DWORD PTR [edi+edi*4]                    ;101.8
        cmp       esi, DWORD PTR [24+eax+edx*8]                 ;101.8
        jle       .B3.109       ; Prob 82%                      ;101.8
                                ; LOE eax ecx esi edi
.B3.110:                        ; Preds .B3.109
        mov       DWORD PTR [-24+ebp], edi                      ;
        mov       edx, DWORD PTR [-76+ebp]                      ;
                                ; LOE edx
.B3.111:                        ; Preds .B3.537 .B3.110
        mov       ecx, DWORD PTR [-24+ebp]                      ;102.8
        mov       eax, DWORD PTR [-156+ebp]                     ;102.8
        lea       esi, DWORD PTR [ecx+ecx*4]                    ;102.8
        mov       edi, DWORD PTR [24+eax+esi*8]                 ;102.8
        test      edi, edi                                      ;102.8
        mov       ecx, DWORD PTR [32+eax+esi*8]                 ;102.8
        jle       .B3.115       ; Prob 10%                      ;102.8
                                ; LOE eax edx ecx edi al ah
.B3.112:                        ; Preds .B3.111
        mov       DWORD PTR [-76+ebp], edx                      ;
        mov       eax, 1                                        ;
        mov       edi, DWORD PTR [-156+ebp]                     ;
        mov       edx, DWORD PTR [-24+ebp]                      ;
                                ; LOE eax edx ecx edi
.B3.113:                        ; Preds .B3.113 .B3.112
        inc       eax                                           ;102.8
        lea       esi, DWORD PTR [edx+edx*4]                    ;102.8
        mov       edx, DWORD PTR [32+edi+esi*8]                 ;102.8
        neg       edx                                           ;102.8
        add       edx, ecx                                      ;102.8
        inc       ecx                                           ;102.8
        imul      edx, DWORD PTR [28+edi+esi*8]                 ;102.8
        mov       esi, DWORD PTR [edi+esi*8]                    ;102.8
        mov       DWORD PTR [4+esi+edx], 0                      ;102.8
        mov       edx, DWORD PTR [8+ebx]                        ;102.8
        mov       edx, DWORD PTR [edx]                          ;102.8
        lea       esi, DWORD PTR [edx+edx*4]                    ;102.8
        cmp       eax, DWORD PTR [24+edi+esi*8]                 ;102.8
        jle       .B3.113       ; Prob 82%                      ;102.8
                                ; LOE eax edx ecx esi edi
.B3.114:                        ; Preds .B3.113
        mov       eax, DWORD PTR [-156+ebp]                     ;103.8
        mov       DWORD PTR [-24+ebp], edx                      ;
        mov       edx, DWORD PTR [-76+ebp]                      ;
        mov       edi, DWORD PTR [24+eax+esi*8]                 ;103.8
        mov       ecx, DWORD PTR [32+eax+esi*8]                 ;103.8
                                ; LOE eax edx ecx edi al ah
.B3.115:                        ; Preds .B3.114 .B3.111
        test      edi, edi                                      ;103.8
        jle       .B3.119       ; Prob 10%                      ;103.8
                                ; LOE eax edx ecx al ah
.B3.116:                        ; Preds .B3.115
        mov       DWORD PTR [-76+ebp], edx                      ;
        mov       esi, 1                                        ;
        mov       edi, DWORD PTR [-24+ebp]                      ;
                                ; LOE eax ecx esi edi
.B3.117:                        ; Preds .B3.117 .B3.116
        inc       esi                                           ;103.8
        lea       edi, DWORD PTR [edi+edi*4]                    ;103.8
        mov       edx, DWORD PTR [32+eax+edi*8]                 ;103.8
        neg       edx                                           ;103.8
        add       edx, ecx                                      ;103.8
        inc       ecx                                           ;103.8
        imul      edx, DWORD PTR [28+eax+edi*8]                 ;103.8
        mov       edi, DWORD PTR [eax+edi*8]                    ;103.8
        mov       DWORD PTR [8+edi+edx], 0                      ;103.8
        mov       edx, DWORD PTR [8+ebx]                        ;103.8
        mov       edi, DWORD PTR [edx]                          ;103.8
        lea       edx, DWORD PTR [edi+edi*4]                    ;103.8
        cmp       esi, DWORD PTR [24+eax+edx*8]                 ;103.8
        jle       .B3.117       ; Prob 82%                      ;103.8
                                ; LOE eax ecx esi edi
.B3.118:                        ; Preds .B3.117
        mov       DWORD PTR [-24+ebp], edi                      ;
        mov       edx, DWORD PTR [-76+ebp]                      ;
                                ; LOE edx
.B3.119:                        ; Preds .B3.118 .B3.115
        test      edx, edx                                      ;105.22
        jle       .B3.504       ; Prob 0%                       ;105.22
                                ; LOE edx
.B3.120:                        ; Preds .B3.119
        cmp       edx, 4                                        ;105.22
        jl        .B3.402       ; Prob 10%                      ;105.22
                                ; LOE edx
.B3.121:                        ; Preds .B3.120
        mov       esi, DWORD PTR [-12+ebp]                      ;105.22
        and       esi, 15                                       ;105.22
        je        .B3.124       ; Prob 50%                      ;105.22
                                ; LOE edx esi
.B3.122:                        ; Preds .B3.121
        test      esi, 3                                        ;105.22
        jne       .B3.402       ; Prob 10%                      ;105.22
                                ; LOE edx esi
.B3.123:                        ; Preds .B3.122
        neg       esi                                           ;105.22
        add       esi, 16                                       ;105.22
        shr       esi, 2                                        ;105.22
                                ; LOE edx esi
.B3.124:                        ; Preds .B3.123 .B3.121
        lea       eax, DWORD PTR [4+esi]                        ;105.22
        cmp       edx, eax                                      ;105.22
        jl        .B3.402       ; Prob 10%                      ;105.22
                                ; LOE edx esi
.B3.125:                        ; Preds .B3.124
        mov       eax, edx                                      ;105.22
        xor       edi, edi                                      ;
        sub       eax, esi                                      ;105.22
        and       eax, 3                                        ;105.22
        neg       eax                                           ;105.22
        add       eax, edx                                      ;105.22
        test      esi, esi                                      ;105.22
        jbe       .B3.129       ; Prob 10%                      ;105.22
                                ; LOE eax edx esi edi
.B3.126:                        ; Preds .B3.125
        mov       DWORD PTR [-76+ebp], edx                      ;
        xor       ecx, ecx                                      ;
        mov       DWORD PTR [-360+ebp], eax                     ;
        mov       edx, DWORD PTR [-12+ebp]                      ;
                                ; LOE edx ecx esi edi
.B3.127:                        ; Preds .B3.127 .B3.126
        mov       eax, DWORD PTR [edx+ecx*4]                    ;105.22
        inc       ecx                                           ;105.22
        and       eax, 1                                        ;105.22
        add       edi, eax                                      ;105.22
        cmp       ecx, esi                                      ;105.22
        jb        .B3.127       ; Prob 82%                      ;105.22
                                ; LOE edx ecx esi edi
.B3.128:                        ; Preds .B3.127
        mov       eax, DWORD PTR [-360+ebp]                     ;
        mov       edx, DWORD PTR [-76+ebp]                      ;
                                ; LOE eax edx esi edi
.B3.129:                        ; Preds .B3.125 .B3.128
        movdqa    xmm0, XMMWORD PTR [_2il0floatpacket.23]       ;105.22
        movd      xmm2, edi                                     ;105.8
        mov       ecx, DWORD PTR [-12+ebp]                      ;105.22
        pcmpeqd   xmm1, xmm1                                    ;105.22
                                ; LOE eax edx ecx esi xmm0 xmm1 xmm2
.B3.130:                        ; Preds .B3.130 .B3.129
        movdqa    xmm4, XMMWORD PTR [ecx+esi*4]                 ;105.22
        add       esi, 4                                        ;105.22
        pand      xmm4, xmm0                                    ;105.22
        pxor      xmm3, xmm3                                    ;105.22
        pcmpeqd   xmm4, xmm3                                    ;105.22
        pxor      xmm5, xmm5                                    ;105.22
        pxor      xmm4, xmm1                                    ;105.22
        cmp       esi, eax                                      ;105.22
        psubd     xmm5, xmm4                                    ;105.22
        paddd     xmm2, xmm5                                    ;105.22
        jb        .B3.130       ; Prob 82%                      ;105.22
                                ; LOE eax edx ecx esi xmm0 xmm1 xmm2
.B3.131:                        ; Preds .B3.130
        movdqa    xmm0, xmm2                                    ;105.8
        psrldq    xmm0, 8                                       ;105.8
        paddd     xmm2, xmm0                                    ;105.8
        movdqa    xmm1, xmm2                                    ;105.8
        psrldq    xmm1, 4                                       ;105.8
        paddd     xmm2, xmm1                                    ;105.8
        movd      esi, xmm2                                     ;105.8
                                ; LOE eax edx esi
.B3.132:                        ; Preds .B3.131 .B3.402
        cmp       eax, edx                                      ;105.22
        jae       .B3.136       ; Prob 10%                      ;105.22
                                ; LOE eax edx esi
.B3.133:                        ; Preds .B3.132
        mov       edi, DWORD PTR [-12+ebp]                      ;
                                ; LOE eax edx esi edi
.B3.134:                        ; Preds .B3.134 .B3.133
        mov       ecx, DWORD PTR [edi+eax*4]                    ;105.22
        inc       eax                                           ;105.22
        and       ecx, 1                                        ;105.22
        add       esi, ecx                                      ;105.22
        cmp       eax, edx                                      ;105.22
        jb        .B3.134       ; Prob 82%                      ;105.22
                                ; LOE eax edx esi edi
.B3.136:                        ; Preds .B3.134 .B3.132
        test      esi, esi                                      ;106.23
        jg        .B3.138       ; Prob 21%                      ;106.23
                                ; LOE esi
.B3.137:                        ; Preds .B3.504 .B3.136
        mov       eax, DWORD PTR [-192+ebp]                     ;118.8
        mov       edx, DWORD PTR [-152+ebp]                     ;118.8
        mov       edi, DWORD PTR [-232+ebp]                     ;118.8
        mov       DWORD PTR [-16+ebp], eax                      ;118.8
        mov       DWORD PTR [-20+ebp], edx                      ;118.8
        jmp       .B3.262       ; Prob 100%                     ;118.8
                                ; LOE esi edi
.B3.138:                        ; Preds .B3.136
        mov       edi, 0                                        ;107.10
        lea       ecx, DWORD PTR [-276+ebp]                     ;107.10
        mov       DWORD PTR [-264+ebp], edi                     ;107.10
        cmovg     edi, esi                                      ;107.10
        mov       eax, 4                                        ;107.10
        push      eax                                           ;107.10
        push      edi                                           ;107.10
        push      2                                             ;107.10
        push      ecx                                           ;107.10
        mov       edx, 1                                        ;107.10
        mov       DWORD PTR [-260+ebp], 133                     ;107.10
        mov       DWORD PTR [-268+ebp], eax                     ;107.10
        mov       DWORD PTR [-256+ebp], edx                     ;107.10
        mov       DWORD PTR [-240+ebp], edx                     ;107.10
        mov       DWORD PTR [-248+ebp], edi                     ;107.10
        mov       DWORD PTR [-244+ebp], eax                     ;107.10
        call      _for_check_mult_overflow                      ;107.10
                                ; LOE eax esi edi
.B3.538:                        ; Preds .B3.138
        add       esp, 16                                       ;107.10
                                ; LOE eax esi edi
.B3.139:                        ; Preds .B3.538
        mov       edx, DWORD PTR [-260+ebp]                     ;107.10
        and       eax, 1                                        ;107.10
        and       edx, 1                                        ;107.10
        lea       ecx, DWORD PTR [-272+ebp]                     ;107.10
        shl       eax, 4                                        ;107.10
        add       edx, edx                                      ;107.10
        or        edx, eax                                      ;107.10
        or        edx, 262144                                   ;107.10
        push      edx                                           ;107.10
        push      ecx                                           ;107.10
        push      DWORD PTR [-276+ebp]                          ;107.10
        call      _for_alloc_allocatable                        ;107.10
                                ; LOE esi edi
.B3.539:                        ; Preds .B3.139
        add       esp, 12                                       ;107.10
                                ; LOE esi edi
.B3.140:                        ; Preds .B3.539
        mov       edx, 4                                        ;107.10
        lea       ecx, DWORD PTR [-236+ebp]                     ;107.10
        push      edx                                           ;107.10
        push      edi                                           ;107.10
        push      2                                             ;107.10
        push      ecx                                           ;107.10
        mov       eax, 1                                        ;107.10
        mov       DWORD PTR [-300+ebp], 133                     ;107.10
        mov       DWORD PTR [-308+ebp], edx                     ;107.10
        mov       DWORD PTR [-296+ebp], eax                     ;107.10
        mov       DWORD PTR [-304+ebp], 0                       ;107.10
        mov       DWORD PTR [-280+ebp], eax                     ;107.10
        mov       DWORD PTR [-288+ebp], edi                     ;107.10
        mov       DWORD PTR [-284+ebp], edx                     ;107.10
        call      _for_check_mult_overflow                      ;107.10
                                ; LOE eax esi edi
.B3.540:                        ; Preds .B3.140
        add       esp, 16                                       ;107.10
                                ; LOE eax esi edi
.B3.141:                        ; Preds .B3.540
        mov       edx, DWORD PTR [-300+ebp]                     ;107.10
        and       eax, 1                                        ;107.10
        and       edx, 1                                        ;107.10
        lea       ecx, DWORD PTR [-312+ebp]                     ;107.10
        shl       eax, 4                                        ;107.10
        add       edx, edx                                      ;107.10
        or        edx, eax                                      ;107.10
        or        edx, 262144                                   ;107.10
        push      edx                                           ;107.10
        push      ecx                                           ;107.10
        push      DWORD PTR [-236+ebp]                          ;107.10
        call      _for_alloc_allocatable                        ;107.10
                                ; LOE esi edi
.B3.541:                        ; Preds .B3.141
        add       esp, 12                                       ;107.10
                                ; LOE esi edi
.B3.142:                        ; Preds .B3.541
        mov       edx, 4                                        ;107.10
        lea       ecx, DWORD PTR [-196+ebp]                     ;107.10
        push      edx                                           ;107.10
        push      edi                                           ;107.10
        push      2                                             ;107.10
        push      ecx                                           ;107.10
        mov       eax, 1                                        ;107.10
        mov       DWORD PTR [-340+ebp], 133                     ;107.10
        mov       DWORD PTR [-348+ebp], edx                     ;107.10
        mov       DWORD PTR [-336+ebp], eax                     ;107.10
        mov       DWORD PTR [-344+ebp], 0                       ;107.10
        mov       DWORD PTR [-320+ebp], eax                     ;107.10
        mov       DWORD PTR [-328+ebp], edi                     ;107.10
        mov       DWORD PTR [-324+ebp], edx                     ;107.10
        call      _for_check_mult_overflow                      ;107.10
                                ; LOE eax esi
.B3.542:                        ; Preds .B3.142
        add       esp, 16                                       ;107.10
                                ; LOE eax esi
.B3.143:                        ; Preds .B3.542
        mov       edx, DWORD PTR [-340+ebp]                     ;107.10
        and       eax, 1                                        ;107.10
        and       edx, 1                                        ;107.10
        lea       ecx, DWORD PTR [-352+ebp]                     ;107.10
        shl       eax, 4                                        ;107.10
        add       edx, edx                                      ;107.10
        or        edx, eax                                      ;107.10
        or        edx, 262144                                   ;107.10
        push      edx                                           ;107.10
        push      ecx                                           ;107.10
        push      DWORD PTR [-196+ebp]                          ;107.10
        call      _for_alloc_allocatable                        ;107.10
                                ; LOE esi
.B3.543:                        ; Preds .B3.143
        add       esp, 12                                       ;107.10
                                ; LOE esi
.B3.144:                        ; Preds .B3.543
        mov       eax, DWORD PTR [-112+ebp]                     ;110.19
        mov       ecx, DWORD PTR [-128+ebp]                     ;108.19
        test      ecx, ecx                                      ;108.19
        mov       DWORD PTR [-76+ebp], esp                      ;108.10
        mov       DWORD PTR [-12+ebp], eax                      ;110.19
        jle       .B3.501       ; Prob 10%                      ;108.19
                                ; LOE eax ecx esi al ah
.B3.145:                        ; Preds .B3.144
        cmp       ecx, 4                                        ;108.19
        jl        .B3.403       ; Prob 10%                      ;108.19
                                ; LOE eax ecx esi al ah
.B3.146:                        ; Preds .B3.145
        and       eax, 15                                       ;108.19
        je        .B3.149       ; Prob 50%                      ;108.19
                                ; LOE eax ecx esi
.B3.147:                        ; Preds .B3.146
        test      al, 3                                         ;108.19
        jne       .B3.403       ; Prob 10%                      ;108.19
                                ; LOE eax ecx esi
.B3.148:                        ; Preds .B3.147
        neg       eax                                           ;108.19
        add       eax, 16                                       ;108.19
        shr       eax, 2                                        ;108.19
                                ; LOE eax ecx esi
.B3.149:                        ; Preds .B3.148 .B3.146
        lea       edx, DWORD PTR [4+eax]                        ;108.19
        cmp       ecx, edx                                      ;108.19
        jl        .B3.403       ; Prob 10%                      ;108.19
                                ; LOE eax ecx esi
.B3.150:                        ; Preds .B3.149
        mov       edx, ecx                                      ;108.19
        xor       edi, edi                                      ;
        sub       edx, eax                                      ;108.19
        and       edx, 3                                        ;108.19
        neg       edx                                           ;108.19
        add       edx, ecx                                      ;108.19
        mov       DWORD PTR [-316+ebp], edi                     ;
        test      eax, eax                                      ;108.19
        jbe       .B3.154       ; Prob 0%                       ;108.19
                                ; LOE eax edx ecx esi
.B3.151:                        ; Preds .B3.150
        xor       edi, edi                                      ;
        mov       DWORD PTR [-156+ebp], esi                     ;
        mov       DWORD PTR [-360+ebp], ecx                     ;
        mov       DWORD PTR [-356+ebp], edx                     ;
        mov       ecx, edi                                      ;
        mov       esi, DWORD PTR [-316+ebp]                     ;
        mov       edi, DWORD PTR [-12+ebp]                      ;
                                ; LOE eax ecx esi edi
.B3.152:                        ; Preds .B3.152 .B3.151
        mov       edx, DWORD PTR [edi+ecx*4]                    ;108.19
        inc       ecx                                           ;108.19
        and       edx, 1                                        ;108.19
        add       esi, edx                                      ;108.19
        cmp       ecx, eax                                      ;108.19
        jb        .B3.152       ; Prob 82%                      ;108.19
                                ; LOE eax ecx esi edi
.B3.153:                        ; Preds .B3.152
        mov       DWORD PTR [-316+ebp], esi                     ;
        mov       esi, DWORD PTR [-156+ebp]                     ;
        mov       edx, DWORD PTR [-356+ebp]                     ;
        mov       ecx, DWORD PTR [-360+ebp]                     ;
                                ; LOE eax edx ecx esi
.B3.154:                        ; Preds .B3.150 .B3.153
        movd      xmm2, DWORD PTR [-316+ebp]                    ;108.19
        mov       edi, DWORD PTR [-12+ebp]                      ;108.19
        pcmpeqd   xmm1, xmm1                                    ;108.19
        movdqa    xmm0, XMMWORD PTR [_2il0floatpacket.23]       ;108.19
                                ; LOE eax edx ecx esi edi xmm0 xmm1 xmm2
.B3.155:                        ; Preds .B3.155 .B3.154
        movdqa    xmm4, XMMWORD PTR [edi+eax*4]                 ;108.19
        add       eax, 4                                        ;108.19
        pand      xmm4, xmm0                                    ;108.19
        pxor      xmm3, xmm3                                    ;108.19
        pcmpeqd   xmm4, xmm3                                    ;108.19
        pxor      xmm5, xmm5                                    ;108.19
        pxor      xmm4, xmm1                                    ;108.19
        cmp       eax, edx                                      ;108.19
        psubd     xmm5, xmm4                                    ;108.19
        paddd     xmm2, xmm5                                    ;108.19
        jb        .B3.155       ; Prob 82%                      ;108.19
                                ; LOE eax edx ecx esi edi xmm0 xmm1 xmm2
.B3.156:                        ; Preds .B3.155
        movdqa    xmm0, xmm2                                    ;108.19
        psrldq    xmm0, 8                                       ;108.19
        paddd     xmm2, xmm0                                    ;108.19
        movdqa    xmm1, xmm2                                    ;108.19
        psrldq    xmm1, 4                                       ;108.19
        paddd     xmm2, xmm1                                    ;108.19
        movd      eax, xmm2                                     ;108.19
                                ; LOE eax edx ecx esi
.B3.157:                        ; Preds .B3.156 .B3.403
        cmp       edx, ecx                                      ;108.19
        jae       .B3.161       ; Prob 0%                       ;108.19
                                ; LOE eax edx ecx esi
.B3.158:                        ; Preds .B3.157
        mov       DWORD PTR [-156+ebp], esi                     ;
        mov       edi, DWORD PTR [-12+ebp]                      ;
                                ; LOE eax edx ecx edi
.B3.159:                        ; Preds .B3.159 .B3.158
        mov       esi, DWORD PTR [edi+edx*4]                    ;108.19
        inc       edx                                           ;108.19
        and       esi, 1                                        ;108.19
        add       eax, esi                                      ;108.19
        cmp       edx, ecx                                      ;108.19
        jb        .B3.159       ; Prob 82%                      ;108.19
                                ; LOE eax edx ecx edi
.B3.160:                        ; Preds .B3.159
        mov       esi, DWORD PTR [-156+ebp]                     ;
                                ; LOE eax ecx esi
.B3.161:                        ; Preds .B3.160 .B3.157 .B3.501
        shl       eax, 2                                        ;108.19
        call      __alloca_probe                                ;108.19
        and       esp, -16                                      ;108.19
        mov       eax, esp                                      ;108.19
                                ; LOE eax ecx esi
.B3.544:                        ; Preds .B3.161
        mov       DWORD PTR [-72+ebp], eax                      ;108.19
        test      ecx, ecx                                      ;108.19
        mov       eax, DWORD PTR [-152+ebp]                     ;118.8
        mov       edx, DWORD PTR [-120+ebp]                     ;108.19
        mov       DWORD PTR [-20+ebp], eax                      ;118.8
        jle       .B3.173       ; Prob 10%                      ;108.19
                                ; LOE edx ecx esi
.B3.162:                        ; Preds .B3.544
        mov       edi, ecx                                      ;108.19
        xor       eax, eax                                      ;
        shr       edi, 31                                       ;108.19
        add       edi, ecx                                      ;108.19
        sar       edi, 1                                        ;108.19
        mov       DWORD PTR [-316+ebp], eax                     ;
        test      edi, edi                                      ;108.19
        jbe       .B3.498       ; Prob 0%                       ;108.19
                                ; LOE edx ecx esi edi
.B3.163:                        ; Preds .B3.162
        mov       DWORD PTR [-356+ebp], edx                     ;
        xor       eax, eax                                      ;
        mov       DWORD PTR [-360+ebp], ecx                     ;
        mov       DWORD PTR [-156+ebp], esi                     ;
        mov       DWORD PTR [-116+ebp], edi                     ;
        mov       ecx, DWORD PTR [-316+ebp]                     ;
        mov       edx, DWORD PTR [-12+ebp]                      ;
                                ; LOE eax edx ecx
.B3.164:                        ; Preds .B3.168 .B3.163
        test      BYTE PTR [edx+eax*8], 1                       ;108.19
        je        .B3.166       ; Prob 60%                      ;108.19
                                ; LOE eax edx ecx
.B3.165:                        ; Preds .B3.164
        mov       esi, DWORD PTR [-20+ebp]                      ;108.19
        mov       edi, DWORD PTR [esi+eax*8]                    ;108.19
        mov       esi, DWORD PTR [-72+ebp]                      ;108.19
        mov       DWORD PTR [esi+ecx*4], edi                    ;108.19
        inc       ecx                                           ;108.19
                                ; LOE eax edx ecx
.B3.166:                        ; Preds .B3.165 .B3.164
        test      BYTE PTR [4+edx+eax*8], 1                     ;108.19
        je        .B3.168       ; Prob 60%                      ;108.19
                                ; LOE eax edx ecx
.B3.167:                        ; Preds .B3.166
        mov       esi, DWORD PTR [-20+ebp]                      ;108.19
        mov       edi, DWORD PTR [4+esi+eax*8]                  ;108.19
        mov       esi, DWORD PTR [-72+ebp]                      ;108.19
        mov       DWORD PTR [esi+ecx*4], edi                    ;108.19
        inc       ecx                                           ;108.19
                                ; LOE eax edx ecx
.B3.168:                        ; Preds .B3.167 .B3.166
        inc       eax                                           ;108.19
        cmp       eax, DWORD PTR [-116+ebp]                     ;108.19
        jb        .B3.164       ; Prob 64%                      ;108.19
                                ; LOE eax edx ecx
.B3.169:                        ; Preds .B3.168
        mov       DWORD PTR [-316+ebp], ecx                     ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;108.19
        mov       esi, DWORD PTR [-156+ebp]                     ;
        mov       edx, DWORD PTR [-356+ebp]                     ;
        mov       ecx, DWORD PTR [-360+ebp]                     ;
                                ; LOE eax edx ecx esi
.B3.170:                        ; Preds .B3.169 .B3.498
        lea       edi, DWORD PTR [-1+eax]                       ;108.19
        cmp       ecx, edi                                      ;108.19
        jbe       .B3.173       ; Prob 0%                       ;108.19
                                ; LOE eax edx esi
.B3.171:                        ; Preds .B3.170
        mov       ecx, DWORD PTR [-12+ebp]                      ;108.19
        test      BYTE PTR [-4+ecx+eax*4], 1                    ;108.19
        je        .B3.173       ; Prob 60%                      ;108.19
                                ; LOE eax edx esi
.B3.172:                        ; Preds .B3.171
        mov       ecx, edx                                      ;108.19
        add       eax, edx                                      ;108.19
        neg       ecx                                           ;108.19
        add       ecx, eax                                      ;108.19
        mov       eax, DWORD PTR [-20+ebp]                      ;108.19
        mov       edi, DWORD PTR [-72+ebp]                      ;108.19
        mov       edx, DWORD PTR [-4+eax+ecx*4]                 ;108.19
        mov       eax, DWORD PTR [-316+ebp]                     ;108.19
        mov       DWORD PTR [edi+eax*4], edx                    ;108.19
                                ; LOE esi
.B3.173:                        ; Preds .B3.544 .B3.170 .B3.171 .B3.172
        mov       eax, DWORD PTR [-240+ebp]                     ;108.10
        mov       DWORD PTR [-116+ebp], eax                     ;108.10
        mov       eax, DWORD PTR [-248+ebp]                     ;108.10
        test      eax, eax                                      ;108.10
        jle       .B3.176       ; Prob 50%                      ;108.10
                                ; LOE eax esi
.B3.174:                        ; Preds .B3.173
        mov       edx, DWORD PTR [-272+ebp]                     ;108.10
        cmp       eax, 24                                       ;108.10
        mov       DWORD PTR [-316+ebp], edx                     ;108.10
        jle       .B3.404       ; Prob 0%                       ;108.10
                                ; LOE eax edx esi dl dh
.B3.175:                        ; Preds .B3.174
        shl       eax, 2                                        ;108.10
        push      eax                                           ;108.10
        push      DWORD PTR [-72+ebp]                           ;108.10
        push      DWORD PTR [-316+ebp]                          ;108.10
        call      __intel_fast_memcpy                           ;108.10
                                ; LOE esi
.B3.545:                        ; Preds .B3.175
        add       esp, 12                                       ;108.10
                                ; LOE esi
.B3.176:                        ; Preds .B3.545 .B3.173 .B3.420 .B3.423
        mov       eax, DWORD PTR [-76+ebp]                      ;108.10
        mov       esp, eax                                      ;108.10
                                ; LOE esi
.B3.546:                        ; Preds .B3.176
        mov       ecx, DWORD PTR [-168+ebp]                     ;109.19
        test      ecx, ecx                                      ;109.19
        mov       DWORD PTR [-76+ebp], esp                      ;109.10
        jle       .B3.193       ; Prob 50%                      ;109.19
                                ; LOE ecx esi
.B3.177:                        ; Preds .B3.546
        cmp       ecx, 4                                        ;109.19
        jl        .B3.427       ; Prob 10%                      ;109.19
                                ; LOE ecx esi
.B3.178:                        ; Preds .B3.177
        mov       eax, DWORD PTR [-12+ebp]                      ;109.19
        and       eax, 15                                       ;109.19
        je        .B3.181       ; Prob 50%                      ;109.19
                                ; LOE eax ecx esi
.B3.179:                        ; Preds .B3.178
        test      al, 3                                         ;109.19
        jne       .B3.427       ; Prob 10%                      ;109.19
                                ; LOE eax ecx esi
.B3.180:                        ; Preds .B3.179
        neg       eax                                           ;109.19
        add       eax, 16                                       ;109.19
        shr       eax, 2                                        ;109.19
                                ; LOE eax ecx esi
.B3.181:                        ; Preds .B3.180 .B3.178
        lea       edx, DWORD PTR [4+eax]                        ;109.19
        cmp       ecx, edx                                      ;109.19
        jl        .B3.427       ; Prob 10%                      ;109.19
                                ; LOE eax ecx esi
.B3.182:                        ; Preds .B3.181
        mov       edx, ecx                                      ;109.19
        xor       edi, edi                                      ;
        sub       edx, eax                                      ;109.19
        and       edx, 3                                        ;109.19
        neg       edx                                           ;109.19
        add       edx, ecx                                      ;109.19
        mov       DWORD PTR [-316+ebp], edi                     ;
        test      eax, eax                                      ;109.19
        jbe       .B3.186       ; Prob 11%                      ;109.19
                                ; LOE eax edx ecx esi
.B3.183:                        ; Preds .B3.182
        xor       edi, edi                                      ;
        mov       DWORD PTR [-156+ebp], esi                     ;
        mov       DWORD PTR [-360+ebp], ecx                     ;
        mov       DWORD PTR [-356+ebp], edx                     ;
        mov       ecx, edi                                      ;
        mov       esi, DWORD PTR [-316+ebp]                     ;
        mov       edi, DWORD PTR [-12+ebp]                      ;
                                ; LOE eax ecx esi edi
.B3.184:                        ; Preds .B3.184 .B3.183
        mov       edx, DWORD PTR [edi+ecx*4]                    ;109.19
        inc       ecx                                           ;109.19
        and       edx, 1                                        ;109.19
        add       esi, edx                                      ;109.19
        cmp       ecx, eax                                      ;109.19
        jb        .B3.184       ; Prob 82%                      ;109.19
                                ; LOE eax ecx esi edi
.B3.185:                        ; Preds .B3.184
        mov       DWORD PTR [-316+ebp], esi                     ;
        mov       esi, DWORD PTR [-156+ebp]                     ;
        mov       edx, DWORD PTR [-356+ebp]                     ;
        mov       ecx, DWORD PTR [-360+ebp]                     ;
                                ; LOE eax edx ecx esi
.B3.186:                        ; Preds .B3.182 .B3.185
        movd      xmm2, DWORD PTR [-316+ebp]                    ;109.19
        mov       edi, DWORD PTR [-12+ebp]                      ;109.19
        pcmpeqd   xmm1, xmm1                                    ;109.19
        movdqa    xmm0, XMMWORD PTR [_2il0floatpacket.23]       ;109.19
                                ; LOE eax edx ecx esi edi xmm0 xmm1 xmm2
.B3.187:                        ; Preds .B3.187 .B3.186
        movdqa    xmm4, XMMWORD PTR [edi+eax*4]                 ;109.19
        add       eax, 4                                        ;109.19
        pand      xmm4, xmm0                                    ;109.19
        pxor      xmm3, xmm3                                    ;109.19
        pcmpeqd   xmm4, xmm3                                    ;109.19
        pxor      xmm5, xmm5                                    ;109.19
        pxor      xmm4, xmm1                                    ;109.19
        cmp       eax, edx                                      ;109.19
        psubd     xmm5, xmm4                                    ;109.19
        paddd     xmm2, xmm5                                    ;109.19
        jb        .B3.187       ; Prob 82%                      ;109.19
                                ; LOE eax edx ecx esi edi xmm0 xmm1 xmm2
.B3.188:                        ; Preds .B3.187
        movdqa    xmm0, xmm2                                    ;109.19
        psrldq    xmm0, 8                                       ;109.19
        paddd     xmm2, xmm0                                    ;109.19
        movdqa    xmm1, xmm2                                    ;109.19
        psrldq    xmm1, 4                                       ;109.19
        paddd     xmm2, xmm1                                    ;109.19
        movd      eax, xmm2                                     ;109.19
                                ; LOE eax edx ecx esi
.B3.189:                        ; Preds .B3.188 .B3.427
        cmp       edx, ecx                                      ;109.19
        jae       .B3.194       ; Prob 11%                      ;109.19
                                ; LOE eax edx ecx esi
.B3.190:                        ; Preds .B3.189
        mov       DWORD PTR [-156+ebp], esi                     ;
        mov       edi, DWORD PTR [-12+ebp]                      ;
                                ; LOE eax edx ecx edi
.B3.191:                        ; Preds .B3.191 .B3.190
        mov       esi, DWORD PTR [edi+edx*4]                    ;109.19
        inc       edx                                           ;109.19
        and       esi, 1                                        ;109.19
        add       eax, esi                                      ;109.19
        cmp       edx, ecx                                      ;109.19
        jb        .B3.191       ; Prob 82%                      ;109.19
                                ; LOE eax edx ecx edi
.B3.192:                        ; Preds .B3.191
        mov       esi, DWORD PTR [-156+ebp]                     ;
        jmp       .B3.194       ; Prob 100%                     ;
                                ; LOE eax ecx esi
.B3.193:                        ; Preds .B3.546
        xor       eax, eax                                      ;
                                ; LOE eax ecx esi
.B3.194:                        ; Preds .B3.192 .B3.189 .B3.193
        shl       eax, 2                                        ;109.19
        call      __alloca_probe                                ;109.19
        and       esp, -16                                      ;109.19
        mov       eax, esp                                      ;109.19
                                ; LOE eax ecx esi
.B3.547:                        ; Preds .B3.194
        mov       DWORD PTR [-72+ebp], eax                      ;109.19
        test      ecx, ecx                                      ;109.19
        mov       eax, DWORD PTR [-192+ebp]                     ;118.8
        mov       edx, DWORD PTR [-160+ebp]                     ;109.19
        mov       DWORD PTR [-16+ebp], eax                      ;118.8
        jle       .B3.206       ; Prob 10%                      ;109.19
                                ; LOE edx ecx esi
.B3.195:                        ; Preds .B3.547
        mov       edi, ecx                                      ;109.19
        xor       eax, eax                                      ;
        shr       edi, 31                                       ;109.19
        add       edi, ecx                                      ;109.19
        sar       edi, 1                                        ;109.19
        mov       DWORD PTR [-316+ebp], eax                     ;
        test      edi, edi                                      ;109.19
        jbe       .B3.497       ; Prob 0%                       ;109.19
                                ; LOE edx ecx esi edi
.B3.196:                        ; Preds .B3.195
        mov       DWORD PTR [-356+ebp], edx                     ;
        xor       eax, eax                                      ;
        mov       DWORD PTR [-360+ebp], ecx                     ;
        mov       DWORD PTR [-156+ebp], esi                     ;
        mov       DWORD PTR [-68+ebp], edi                      ;
        mov       ecx, DWORD PTR [-316+ebp]                     ;
        mov       edx, DWORD PTR [-12+ebp]                      ;
                                ; LOE eax edx ecx
.B3.197:                        ; Preds .B3.201 .B3.196
        test      BYTE PTR [edx+eax*8], 1                       ;109.19
        je        .B3.199       ; Prob 60%                      ;109.19
                                ; LOE eax edx ecx
.B3.198:                        ; Preds .B3.197
        mov       esi, DWORD PTR [-16+ebp]                      ;109.19
        mov       edi, DWORD PTR [esi+eax*8]                    ;109.19
        mov       esi, DWORD PTR [-72+ebp]                      ;109.19
        mov       DWORD PTR [esi+ecx*4], edi                    ;109.19
        inc       ecx                                           ;109.19
                                ; LOE eax edx ecx
.B3.199:                        ; Preds .B3.198 .B3.197
        test      BYTE PTR [4+edx+eax*8], 1                     ;109.19
        je        .B3.201       ; Prob 60%                      ;109.19
                                ; LOE eax edx ecx
.B3.200:                        ; Preds .B3.199
        mov       esi, DWORD PTR [-16+ebp]                      ;109.19
        mov       edi, DWORD PTR [4+esi+eax*8]                  ;109.19
        mov       esi, DWORD PTR [-72+ebp]                      ;109.19
        mov       DWORD PTR [esi+ecx*4], edi                    ;109.19
        inc       ecx                                           ;109.19
                                ; LOE eax edx ecx
.B3.201:                        ; Preds .B3.200 .B3.199
        inc       eax                                           ;109.19
        cmp       eax, DWORD PTR [-68+ebp]                      ;109.19
        jb        .B3.197       ; Prob 64%                      ;109.19
                                ; LOE eax edx ecx
.B3.202:                        ; Preds .B3.201
        mov       DWORD PTR [-316+ebp], ecx                     ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;109.19
        mov       esi, DWORD PTR [-156+ebp]                     ;
        mov       edx, DWORD PTR [-356+ebp]                     ;
        mov       ecx, DWORD PTR [-360+ebp]                     ;
                                ; LOE eax edx ecx esi
.B3.203:                        ; Preds .B3.202 .B3.497
        lea       edi, DWORD PTR [-1+eax]                       ;109.19
        cmp       ecx, edi                                      ;109.19
        jbe       .B3.206       ; Prob 0%                       ;109.19
                                ; LOE eax edx esi
.B3.204:                        ; Preds .B3.203
        mov       ecx, DWORD PTR [-12+ebp]                      ;109.19
        test      BYTE PTR [-4+ecx+eax*4], 1                    ;109.19
        je        .B3.206       ; Prob 60%                      ;109.19
                                ; LOE eax edx esi
.B3.205:                        ; Preds .B3.204
        mov       ecx, edx                                      ;109.19
        add       eax, edx                                      ;109.19
        neg       ecx                                           ;109.19
        add       ecx, eax                                      ;109.19
        mov       eax, DWORD PTR [-16+ebp]                      ;109.19
        mov       edi, DWORD PTR [-72+ebp]                      ;109.19
        mov       edx, DWORD PTR [-4+eax+ecx*4]                 ;109.19
        mov       eax, DWORD PTR [-316+ebp]                     ;109.19
        mov       DWORD PTR [edi+eax*4], edx                    ;109.19
                                ; LOE esi
.B3.206:                        ; Preds .B3.547 .B3.203 .B3.204 .B3.205
        mov       eax, DWORD PTR [-280+ebp]                     ;109.10
        mov       DWORD PTR [-316+ebp], eax                     ;109.10
        mov       eax, DWORD PTR [-288+ebp]                     ;109.10
        test      eax, eax                                      ;109.10
        jle       .B3.209       ; Prob 50%                      ;109.10
                                ; LOE eax esi
.B3.207:                        ; Preds .B3.206
        mov       edx, DWORD PTR [-312+ebp]                     ;109.10
        cmp       eax, 24                                       ;109.10
        mov       DWORD PTR [-68+ebp], edx                      ;109.10
        jle       .B3.430       ; Prob 0%                       ;109.10
                                ; LOE eax edx esi dl dh
.B3.208:                        ; Preds .B3.207
        shl       eax, 2                                        ;109.10
        push      eax                                           ;109.10
        push      DWORD PTR [-72+ebp]                           ;109.10
        push      DWORD PTR [-68+ebp]                           ;109.10
        call      __intel_fast_memcpy                           ;109.10
                                ; LOE esi
.B3.548:                        ; Preds .B3.208
        add       esp, 12                                       ;109.10
                                ; LOE esi
.B3.209:                        ; Preds .B3.548 .B3.206 .B3.446 .B3.449
        mov       eax, DWORD PTR [-76+ebp]                      ;109.10
        mov       esp, eax                                      ;109.10
                                ; LOE esi
.B3.549:                        ; Preds .B3.209
        mov       edx, DWORD PTR [-208+ebp]                     ;110.19
        test      edx, edx                                      ;110.19
        mov       DWORD PTR [-68+ebp], esp                      ;110.10
        jle       .B3.226       ; Prob 50%                      ;110.19
                                ; LOE edx esi
.B3.210:                        ; Preds .B3.549
        cmp       edx, 4                                        ;110.19
        jl        .B3.453       ; Prob 10%                      ;110.19
                                ; LOE edx esi
.B3.211:                        ; Preds .B3.210
        mov       eax, DWORD PTR [-12+ebp]                      ;110.19
        and       eax, 15                                       ;110.19
        je        .B3.214       ; Prob 50%                      ;110.19
                                ; LOE eax edx esi
.B3.212:                        ; Preds .B3.211
        test      al, 3                                         ;110.19
        jne       .B3.453       ; Prob 10%                      ;110.19
                                ; LOE eax edx esi
.B3.213:                        ; Preds .B3.212
        neg       eax                                           ;110.19
        add       eax, 16                                       ;110.19
        shr       eax, 2                                        ;110.19
                                ; LOE eax edx esi
.B3.214:                        ; Preds .B3.213 .B3.211
        lea       ecx, DWORD PTR [4+eax]                        ;110.19
        cmp       edx, ecx                                      ;110.19
        jl        .B3.453       ; Prob 10%                      ;110.19
                                ; LOE eax edx esi
.B3.215:                        ; Preds .B3.214
        mov       ecx, edx                                      ;110.19
        xor       edi, edi                                      ;
        sub       ecx, eax                                      ;110.19
        and       ecx, 3                                        ;110.19
        neg       ecx                                           ;110.19
        add       ecx, edx                                      ;110.19
        mov       DWORD PTR [-76+ebp], edi                      ;
        test      eax, eax                                      ;110.19
        jbe       .B3.219       ; Prob 11%                      ;110.19
                                ; LOE eax edx ecx esi
.B3.216:                        ; Preds .B3.215
        xor       edi, edi                                      ;
        mov       DWORD PTR [-156+ebp], esi                     ;
        mov       DWORD PTR [-356+ebp], ecx                     ;
        mov       DWORD PTR [-360+ebp], edx                     ;
        mov       ecx, edi                                      ;
        mov       esi, DWORD PTR [-76+ebp]                      ;
        mov       edi, DWORD PTR [-12+ebp]                      ;
                                ; LOE eax ecx esi edi
.B3.217:                        ; Preds .B3.217 .B3.216
        mov       edx, DWORD PTR [edi+ecx*4]                    ;110.19
        inc       ecx                                           ;110.19
        and       edx, 1                                        ;110.19
        add       esi, edx                                      ;110.19
        cmp       ecx, eax                                      ;110.19
        jb        .B3.217       ; Prob 82%                      ;110.19
                                ; LOE eax ecx esi edi
.B3.218:                        ; Preds .B3.217
        mov       DWORD PTR [-76+ebp], esi                      ;
        mov       esi, DWORD PTR [-156+ebp]                     ;
        mov       ecx, DWORD PTR [-356+ebp]                     ;
        mov       edx, DWORD PTR [-360+ebp]                     ;
                                ; LOE eax edx ecx esi
.B3.219:                        ; Preds .B3.215 .B3.218
        movd      xmm2, DWORD PTR [-76+ebp]                     ;110.19
        mov       edi, DWORD PTR [-12+ebp]                      ;110.19
        pcmpeqd   xmm1, xmm1                                    ;110.19
        movdqa    xmm0, XMMWORD PTR [_2il0floatpacket.23]       ;110.19
                                ; LOE eax edx ecx esi edi xmm0 xmm1 xmm2
.B3.220:                        ; Preds .B3.220 .B3.219
        movdqa    xmm4, XMMWORD PTR [edi+eax*4]                 ;110.19
        add       eax, 4                                        ;110.19
        pand      xmm4, xmm0                                    ;110.19
        pxor      xmm3, xmm3                                    ;110.19
        pcmpeqd   xmm4, xmm3                                    ;110.19
        pxor      xmm5, xmm5                                    ;110.19
        pxor      xmm4, xmm1                                    ;110.19
        cmp       eax, ecx                                      ;110.19
        psubd     xmm5, xmm4                                    ;110.19
        paddd     xmm2, xmm5                                    ;110.19
        jb        .B3.220       ; Prob 82%                      ;110.19
                                ; LOE eax edx ecx esi edi xmm0 xmm1 xmm2
.B3.221:                        ; Preds .B3.220
        movdqa    xmm0, xmm2                                    ;110.19
        psrldq    xmm0, 8                                       ;110.19
        paddd     xmm2, xmm0                                    ;110.19
        movdqa    xmm1, xmm2                                    ;110.19
        psrldq    xmm1, 4                                       ;110.19
        paddd     xmm2, xmm1                                    ;110.19
        movd      eax, xmm2                                     ;110.19
                                ; LOE eax edx ecx esi
.B3.222:                        ; Preds .B3.221 .B3.453
        cmp       ecx, edx                                      ;110.19
        jae       .B3.227       ; Prob 11%                      ;110.19
                                ; LOE eax edx ecx esi
.B3.223:                        ; Preds .B3.222
        mov       DWORD PTR [-156+ebp], esi                     ;
        mov       edi, DWORD PTR [-12+ebp]                      ;
                                ; LOE eax edx ecx edi
.B3.224:                        ; Preds .B3.224 .B3.223
        mov       esi, DWORD PTR [edi+ecx*4]                    ;110.19
        inc       ecx                                           ;110.19
        and       esi, 1                                        ;110.19
        add       eax, esi                                      ;110.19
        cmp       ecx, edx                                      ;110.19
        jb        .B3.224       ; Prob 82%                      ;110.19
                                ; LOE eax edx ecx edi
.B3.225:                        ; Preds .B3.224
        mov       esi, DWORD PTR [-156+ebp]                     ;
        jmp       .B3.227       ; Prob 100%                     ;
                                ; LOE eax edx esi
.B3.226:                        ; Preds .B3.549
        xor       eax, eax                                      ;
                                ; LOE eax edx esi
.B3.227:                        ; Preds .B3.225 .B3.222 .B3.226
        shl       eax, 2                                        ;110.19
        call      __alloca_probe                                ;110.19
        and       esp, -16                                      ;110.19
        mov       eax, esp                                      ;110.19
                                ; LOE eax edx esi
.B3.550:                        ; Preds .B3.227
        mov       DWORD PTR [-64+ebp], eax                      ;110.19
        test      edx, edx                                      ;110.19
        mov       eax, DWORD PTR [-200+ebp]                     ;110.19
        mov       edi, DWORD PTR [-232+ebp]                     ;118.8
        mov       DWORD PTR [-72+ebp], eax                      ;110.19
        jle       .B3.239       ; Prob 10%                      ;110.19
                                ; LOE edx esi edi
.B3.228:                        ; Preds .B3.550
        mov       ecx, edx                                      ;110.19
        xor       eax, eax                                      ;
        shr       ecx, 31                                       ;110.19
        add       ecx, edx                                      ;110.19
        sar       ecx, 1                                        ;110.19
        mov       DWORD PTR [-356+ebp], eax                     ;
        test      ecx, ecx                                      ;110.19
        jbe       .B3.496       ; Prob 0%                       ;110.19
                                ; LOE edx ecx esi edi
.B3.229:                        ; Preds .B3.228
        mov       DWORD PTR [-60+ebp], ecx                      ;
        xor       eax, eax                                      ;
        mov       DWORD PTR [-360+ebp], edx                     ;
        mov       DWORD PTR [-156+ebp], esi                     ;
        mov       DWORD PTR [-76+ebp], edi                      ;
        mov       ecx, DWORD PTR [-356+ebp]                     ;
        mov       edx, DWORD PTR [-12+ebp]                      ;
                                ; LOE eax edx ecx
.B3.230:                        ; Preds .B3.234 .B3.229
        test      BYTE PTR [edx+eax*8], 1                       ;110.19
        je        .B3.232       ; Prob 60%                      ;110.19
                                ; LOE eax edx ecx
.B3.231:                        ; Preds .B3.230
        mov       esi, DWORD PTR [-76+ebp]                      ;110.19
        mov       edi, DWORD PTR [esi+eax*8]                    ;110.19
        mov       esi, DWORD PTR [-64+ebp]                      ;110.19
        mov       DWORD PTR [esi+ecx*4], edi                    ;110.19
        inc       ecx                                           ;110.19
                                ; LOE eax edx ecx
.B3.232:                        ; Preds .B3.231 .B3.230
        test      BYTE PTR [4+edx+eax*8], 1                     ;110.19
        je        .B3.234       ; Prob 60%                      ;110.19
                                ; LOE eax edx ecx
.B3.233:                        ; Preds .B3.232
        mov       esi, DWORD PTR [-76+ebp]                      ;110.19
        mov       edi, DWORD PTR [4+esi+eax*8]                  ;110.19
        mov       esi, DWORD PTR [-64+ebp]                      ;110.19
        mov       DWORD PTR [esi+ecx*4], edi                    ;110.19
        inc       ecx                                           ;110.19
                                ; LOE eax edx ecx
.B3.234:                        ; Preds .B3.233 .B3.232
        inc       eax                                           ;110.19
        cmp       eax, DWORD PTR [-60+ebp]                      ;110.19
        jb        .B3.230       ; Prob 64%                      ;110.19
                                ; LOE eax edx ecx
.B3.235:                        ; Preds .B3.234
        mov       DWORD PTR [-356+ebp], ecx                     ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;110.19
        mov       esi, DWORD PTR [-156+ebp]                     ;
        mov       edx, DWORD PTR [-360+ebp]                     ;
        mov       edi, DWORD PTR [-76+ebp]                      ;
                                ; LOE eax edx esi edi
.B3.236:                        ; Preds .B3.235 .B3.496
        lea       ecx, DWORD PTR [-1+eax]                       ;110.19
        cmp       edx, ecx                                      ;110.19
        jbe       .B3.239       ; Prob 0%                       ;110.19
                                ; LOE eax esi edi
.B3.237:                        ; Preds .B3.236
        mov       edx, DWORD PTR [-12+ebp]                      ;110.19
        test      BYTE PTR [-4+edx+eax*4], 1                    ;110.19
        je        .B3.239       ; Prob 60%                      ;110.19
                                ; LOE eax esi edi
.B3.238:                        ; Preds .B3.237
        mov       edx, DWORD PTR [-72+ebp]                      ;110.19
        mov       ecx, edx                                      ;110.19
        neg       ecx                                           ;110.19
        add       eax, edx                                      ;110.19
        add       ecx, eax                                      ;110.19
        mov       edx, DWORD PTR [-64+ebp]                      ;110.19
        mov       eax, DWORD PTR [-4+edi+ecx*4]                 ;110.19
        mov       ecx, DWORD PTR [-356+ebp]                     ;110.19
        mov       DWORD PTR [edx+ecx*4], eax                    ;110.19
                                ; LOE esi edi
.B3.239:                        ; Preds .B3.550 .B3.236 .B3.237 .B3.238
        mov       eax, DWORD PTR [-320+ebp]                     ;110.10
        mov       edx, DWORD PTR [-352+ebp]                     ;110.10
        mov       DWORD PTR [-72+ebp], eax                      ;110.10
        mov       eax, DWORD PTR [-328+ebp]                     ;110.10
        test      eax, eax                                      ;110.10
        mov       DWORD PTR [-356+ebp], edx                     ;110.10
        jle       .B3.243       ; Prob 50%                      ;110.10
                                ; LOE eax edx esi edi dl dh
.B3.240:                        ; Preds .B3.239
        cmp       eax, 24                                       ;110.10
        jle       .B3.456       ; Prob 0%                       ;110.10
                                ; LOE eax edx esi edi dl dh
.B3.241:                        ; Preds .B3.240
        shl       eax, 2                                        ;110.10
        push      eax                                           ;110.10
        push      DWORD PTR [-64+ebp]                           ;110.10
        push      DWORD PTR [-356+ebp]                          ;110.10
        call      __intel_fast_memcpy                           ;110.10
                                ; LOE esi edi
.B3.551:                        ; Preds .B3.241
        add       esp, 12                                       ;110.10
                                ; LOE esi edi
.B3.242:                        ; Preds .B3.551
        mov       eax, DWORD PTR [-352+ebp]                     ;113.10
        mov       DWORD PTR [-356+ebp], eax                     ;113.10
                                ; LOE esi edi
.B3.243:                        ; Preds .B3.239 .B3.242 .B3.472 .B3.475
        mov       eax, DWORD PTR [-68+ebp]                      ;110.10
        mov       esp, eax                                      ;110.10
                                ; LOE esi edi
.B3.552:                        ; Preds .B3.243
        mov       eax, DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY] ;111.10
        mov       DWORD PTR [-28+ebp], eax                      ;111.10
        mov       eax, esi                                      ;111.10
        shr       eax, 31                                       ;111.10
        add       eax, esi                                      ;111.10
        sar       eax, 1                                        ;111.10
        mov       ecx, DWORD PTR [_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY+32] ;111.10
        test      eax, eax                                      ;111.10
        mov       edx, DWORD PTR [-272+ebp]                     ;111.10
        mov       DWORD PTR [-32+ebp], ecx                      ;111.10
        mov       DWORD PTR [-68+ebp], eax                      ;111.10
        jbe       .B3.495       ; Prob 0%                       ;111.10
                                ; LOE edx esi edi
.B3.244:                        ; Preds .B3.552
        imul      ecx, DWORD PTR [-32+ebp], -40                 ;
        xor       eax, eax                                      ;
        add       ecx, DWORD PTR [-28+ebp]                      ;
        mov       DWORD PTR [-60+ebp], ecx                      ;
        mov       ecx, DWORD PTR [-116+ebp]                     ;
        mov       DWORD PTR [-156+ebp], esi                     ;
        mov       DWORD PTR [-360+ebp], edx                     ;
        mov       DWORD PTR [-76+ebp], edi                      ;
        shl       ecx, 2                                        ;
        neg       ecx                                           ;
        add       ecx, edx                                      ;
        mov       DWORD PTR [-64+ebp], ecx                      ;
        ALIGN     16
                                ; LOE eax
.B3.245:                        ; Preds .B3.245 .B3.244
        mov       edi, DWORD PTR [8+ebx]                        ;111.10
        lea       esi, DWORD PTR [1+eax+eax]                    ;111.10
        mov       ecx, DWORD PTR [-60+ebp]                      ;111.10
        mov       edx, DWORD PTR [edi]                          ;111.10
        lea       edx, DWORD PTR [edx+edx*4]                    ;111.10
        sub       esi, DWORD PTR [32+ecx+edx*8]                 ;111.10
        imul      esi, DWORD PTR [28+ecx+edx*8]                 ;111.10
        mov       ecx, DWORD PTR [ecx+edx*8]                    ;111.10
        mov       edx, DWORD PTR [-64+ebp]                      ;111.10
        mov       edx, DWORD PTR [4+edx+eax*8]                  ;111.10
        mov       DWORD PTR [ecx+esi], edx                      ;111.10
        lea       edx, DWORD PTR [2+eax+eax]                    ;111.10
        mov       esi, DWORD PTR [edi]                          ;111.10
        mov       edi, DWORD PTR [-60+ebp]                      ;111.10
        lea       ecx, DWORD PTR [esi+esi*4]                    ;111.10
        mov       esi, DWORD PTR [-64+ebp]                      ;111.10
        sub       edx, DWORD PTR [32+edi+ecx*8]                 ;111.10
        imul      edx, DWORD PTR [28+edi+ecx*8]                 ;111.10
        mov       ecx, DWORD PTR [edi+ecx*8]                    ;111.10
        mov       edi, DWORD PTR [8+esi+eax*8]                  ;111.10
        inc       eax                                           ;111.10
        cmp       eax, DWORD PTR [-68+ebp]                      ;111.10
        mov       DWORD PTR [ecx+edx], edi                      ;111.10
        jb        .B3.245       ; Prob 64%                      ;111.10
                                ; LOE eax
.B3.246:                        ; Preds .B3.245
        mov       esi, DWORD PTR [-156+ebp]                     ;
        lea       ecx, DWORD PTR [1+eax+eax]                    ;111.10
        mov       edx, DWORD PTR [-360+ebp]                     ;
        mov       edi, DWORD PTR [-76+ebp]                      ;
                                ; LOE edx ecx esi edi
.B3.247:                        ; Preds .B3.246 .B3.495
        lea       eax, DWORD PTR [-1+ecx]                       ;111.10
        cmp       esi, eax                                      ;111.10
        jbe       .B3.249       ; Prob 0%                       ;111.10
                                ; LOE edx ecx esi edi
.B3.248:                        ; Preds .B3.247
        mov       DWORD PTR [-156+ebp], esi                     ;
        mov       esi, DWORD PTR [8+ebx]                        ;111.10
        mov       DWORD PTR [-76+ebp], edi                      ;
        mov       edi, DWORD PTR [-32+ebp]                      ;111.10
        neg       edi                                           ;111.10
        add       edi, DWORD PTR [esi]                          ;111.10
        mov       esi, DWORD PTR [-28+ebp]                      ;111.10
        mov       eax, DWORD PTR [-116+ebp]                     ;111.10
        neg       eax                                           ;111.10
        lea       edi, DWORD PTR [edi+edi*4]                    ;111.10
        add       eax, ecx                                      ;111.10
        sub       ecx, DWORD PTR [32+esi+edi*8]                 ;111.10
        imul      ecx, DWORD PTR [28+esi+edi*8]                 ;111.10
        mov       esi, DWORD PTR [esi+edi*8]                    ;111.10
        mov       edx, DWORD PTR [edx+eax*4]                    ;111.10
        mov       edi, DWORD PTR [-76+ebp]                      ;111.10
        mov       DWORD PTR [esi+ecx], edx                      ;111.10
        mov       esi, DWORD PTR [-156+ebp]                     ;111.10
                                ; LOE esi edi
.B3.249:                        ; Preds .B3.247 .B3.248
        mov       edx, DWORD PTR [-312+ebp]                     ;112.10
        cmp       DWORD PTR [-68+ebp], 0                        ;112.10
        jbe       .B3.494       ; Prob 11%                      ;112.10
                                ; LOE edx esi edi
.B3.250:                        ; Preds .B3.249
        imul      ecx, DWORD PTR [-32+ebp], -40                 ;
        xor       eax, eax                                      ;
        add       ecx, DWORD PTR [-28+ebp]                      ;
        mov       DWORD PTR [-64+ebp], ecx                      ;
        mov       ecx, DWORD PTR [-316+ebp]                     ;
        mov       DWORD PTR [-156+ebp], esi                     ;
        mov       DWORD PTR [-360+ebp], edx                     ;
        mov       DWORD PTR [-76+ebp], edi                      ;
        shl       ecx, 2                                        ;
        neg       ecx                                           ;
        add       ecx, edx                                      ;
        mov       DWORD PTR [-116+ebp], ecx                     ;
        ALIGN     16
                                ; LOE eax
.B3.251:                        ; Preds .B3.251 .B3.250
        mov       edi, DWORD PTR [8+ebx]                        ;112.10
        lea       esi, DWORD PTR [1+eax+eax]                    ;112.10
        mov       ecx, DWORD PTR [-64+ebp]                      ;112.10
        mov       edx, DWORD PTR [edi]                          ;112.10
        lea       edx, DWORD PTR [edx+edx*4]                    ;112.10
        sub       esi, DWORD PTR [32+ecx+edx*8]                 ;112.10
        imul      esi, DWORD PTR [28+ecx+edx*8]                 ;112.10
        mov       ecx, DWORD PTR [ecx+edx*8]                    ;112.10
        mov       edx, DWORD PTR [-116+ebp]                     ;112.10
        mov       edx, DWORD PTR [4+edx+eax*8]                  ;112.10
        mov       DWORD PTR [4+ecx+esi], edx                    ;112.10
        lea       edx, DWORD PTR [2+eax+eax]                    ;112.10
        mov       esi, DWORD PTR [edi]                          ;112.10
        mov       edi, DWORD PTR [-64+ebp]                      ;112.10
        lea       ecx, DWORD PTR [esi+esi*4]                    ;112.10
        mov       esi, DWORD PTR [-116+ebp]                     ;112.10
        sub       edx, DWORD PTR [32+edi+ecx*8]                 ;112.10
        imul      edx, DWORD PTR [28+edi+ecx*8]                 ;112.10
        mov       ecx, DWORD PTR [edi+ecx*8]                    ;112.10
        mov       edi, DWORD PTR [8+esi+eax*8]                  ;112.10
        inc       eax                                           ;112.10
        cmp       eax, DWORD PTR [-68+ebp]                      ;112.10
        mov       DWORD PTR [4+ecx+edx], edi                    ;112.10
        jb        .B3.251       ; Prob 64%                      ;112.10
                                ; LOE eax
.B3.252:                        ; Preds .B3.251
        mov       esi, DWORD PTR [-156+ebp]                     ;
        lea       ecx, DWORD PTR [1+eax+eax]                    ;112.10
        mov       edx, DWORD PTR [-360+ebp]                     ;
        mov       edi, DWORD PTR [-76+ebp]                      ;
                                ; LOE edx ecx esi edi
.B3.253:                        ; Preds .B3.252 .B3.494
        lea       eax, DWORD PTR [-1+ecx]                       ;112.10
        cmp       esi, eax                                      ;112.10
        jbe       .B3.255       ; Prob 11%                      ;112.10
                                ; LOE edx ecx esi edi
.B3.254:                        ; Preds .B3.253
        mov       DWORD PTR [-156+ebp], esi                     ;
        mov       esi, DWORD PTR [8+ebx]                        ;112.10
        mov       DWORD PTR [-76+ebp], edi                      ;
        mov       edi, DWORD PTR [-32+ebp]                      ;112.10
        neg       edi                                           ;112.10
        add       edi, DWORD PTR [esi]                          ;112.10
        mov       esi, DWORD PTR [-28+ebp]                      ;112.10
        mov       eax, DWORD PTR [-316+ebp]                     ;112.10
        neg       eax                                           ;112.10
        lea       edi, DWORD PTR [edi+edi*4]                    ;112.10
        add       eax, ecx                                      ;112.10
        sub       ecx, DWORD PTR [32+esi+edi*8]                 ;112.10
        imul      ecx, DWORD PTR [28+esi+edi*8]                 ;112.10
        mov       esi, DWORD PTR [esi+edi*8]                    ;112.10
        mov       edx, DWORD PTR [edx+eax*4]                    ;112.10
        mov       edi, DWORD PTR [-76+ebp]                      ;112.10
        mov       DWORD PTR [4+esi+ecx], edx                    ;112.10
        mov       esi, DWORD PTR [-156+ebp]                     ;112.10
                                ; LOE esi edi
.B3.255:                        ; Preds .B3.253 .B3.254
        cmp       DWORD PTR [-68+ebp], 0                        ;113.10
        jbe       .B3.493       ; Prob 0%                       ;113.10
                                ; LOE esi edi
.B3.256:                        ; Preds .B3.255
        mov       ecx, DWORD PTR [-72+ebp]                      ;
        xor       edx, edx                                      ;
        imul      eax, DWORD PTR [-32+ebp], -40                 ;
        add       eax, DWORD PTR [-28+ebp]                      ;
        shl       ecx, 2                                        ;
        neg       ecx                                           ;
        add       ecx, DWORD PTR [-356+ebp]                     ;
        mov       DWORD PTR [-156+ebp], esi                     ;
        mov       DWORD PTR [-360+ebp], ecx                     ;
        mov       DWORD PTR [-76+ebp], edi                      ;
        ALIGN     16
                                ; LOE eax edx
.B3.257:                        ; Preds .B3.257 .B3.256
        mov       edi, DWORD PTR [8+ebx]                        ;113.10
        pxor      xmm0, xmm0                                    ;113.10
        pxor      xmm1, xmm1                                    ;113.10
        mov       ecx, DWORD PTR [edi]                          ;113.10
        lea       esi, DWORD PTR [ecx+ecx*4]                    ;113.10
        mov       ecx, DWORD PTR [-360+ebp]                     ;113.10
        cvtsi2ss  xmm0, DWORD PTR [4+ecx+edx*8]                 ;113.10
        lea       ecx, DWORD PTR [1+edx+edx]                    ;113.10
        sub       ecx, DWORD PTR [32+eax+esi*8]                 ;113.10
        imul      ecx, DWORD PTR [28+eax+esi*8]                 ;113.10
        mov       esi, DWORD PTR [eax+esi*8]                    ;113.10
        movss     DWORD PTR [8+esi+ecx], xmm0                   ;113.10
        mov       edi, DWORD PTR [edi]                          ;113.10
        mov       ecx, DWORD PTR [-360+ebp]                     ;113.10
        lea       esi, DWORD PTR [edi+edi*4]                    ;113.10
        cvtsi2ss  xmm1, DWORD PTR [8+ecx+edx*8]                 ;113.10
        mov       edi, DWORD PTR [eax+esi*8]                    ;113.10
        lea       ecx, DWORD PTR [2+edx+edx]                    ;113.10
        sub       ecx, DWORD PTR [32+eax+esi*8]                 ;113.10
        inc       edx                                           ;113.10
        imul      ecx, DWORD PTR [28+eax+esi*8]                 ;113.10
        movss     DWORD PTR [8+edi+ecx], xmm1                   ;113.10
        cmp       edx, DWORD PTR [-68+ebp]                      ;113.10
        jb        .B3.257       ; Prob 64%                      ;113.10
                                ; LOE eax edx
.B3.258:                        ; Preds .B3.257
        mov       esi, DWORD PTR [-156+ebp]                     ;
        lea       edx, DWORD PTR [1+edx+edx]                    ;113.10
        mov       edi, DWORD PTR [-76+ebp]                      ;
                                ; LOE edx esi edi
.B3.259:                        ; Preds .B3.258 .B3.493
        lea       eax, DWORD PTR [-1+edx]                       ;113.10
        cmp       esi, eax                                      ;113.10
        jbe       .B3.261       ; Prob 0%                       ;113.10
                                ; LOE edx esi edi
.B3.260:                        ; Preds .B3.259
        mov       ecx, DWORD PTR [8+ebx]                        ;113.10
        mov       DWORD PTR [-156+ebp], esi                     ;
        mov       esi, DWORD PTR [-32+ebp]                      ;113.10
        neg       esi                                           ;113.10
        add       esi, DWORD PTR [ecx]                          ;113.10
        mov       ecx, DWORD PTR [-356+ebp]                     ;113.10
        lea       eax, DWORD PTR [esi+esi*4]                    ;113.10
        mov       esi, DWORD PTR [-72+ebp]                      ;113.10
        neg       esi                                           ;113.10
        add       esi, edx                                      ;113.10
        cvtsi2ss  xmm0, DWORD PTR [ecx+esi*4]                   ;113.10
        mov       ecx, DWORD PTR [-28+ebp]                      ;113.10
        mov       esi, DWORD PTR [-156+ebp]                     ;113.10
        sub       edx, DWORD PTR [32+ecx+eax*8]                 ;113.10
        imul      edx, DWORD PTR [28+ecx+eax*8]                 ;113.10
        mov       eax, DWORD PTR [ecx+eax*8]                    ;113.10
        movss     DWORD PTR [8+eax+edx], xmm0                   ;113.10
                                ; LOE esi edi
.B3.261:                        ; Preds .B3.259 .B3.260
        mov       eax, DWORD PTR [8+ebx]                        ;115.8
        mov       edx, DWORD PTR [eax]                          ;115.8
        mov       DWORD PTR [-24+ebp], edx                      ;115.8
                                ; LOE esi edi
.B3.262:                        ; Preds .B3.261 .B3.137
        mov       edx, DWORD PTR [-32+ebp]                      ;115.8
        neg       edx                                           ;115.8
        mov       eax, DWORD PTR [-24+ebp]                      ;115.8
        add       edx, eax                                      ;115.8
        mov       ecx, DWORD PTR [-28+ebp]                      ;115.8
        lea       edx, DWORD PTR [edx+edx*4]                    ;115.8
        mov       WORD PTR [38+ecx+edx*8], si                   ;115.8
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;116.8
        neg       ecx                                           ;116.8
        add       ecx, eax                                      ;116.8
        imul      edx, ecx, 900                                 ;116.8
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;116.8
        mov       DWORD PTR [700+eax+edx], esi                  ;116.8
        mov       esi, DWORD PTR [-140+ebp]                     ;118.8
        mov       eax, esi                                      ;118.8
        shr       eax, 1                                        ;118.8
        mov       ecx, esi                                      ;118.8
        and       eax, 1                                        ;118.8
        and       ecx, 1                                        ;118.8
        shl       eax, 2                                        ;118.8
        add       ecx, ecx                                      ;118.8
        or        eax, ecx                                      ;118.8
        or        eax, 262144                                   ;118.8
        push      eax                                           ;118.8
        push      DWORD PTR [-20+ebp]                           ;118.8
        call      _for_dealloc_allocatable                      ;118.8
                                ; LOE esi edi
.B3.553:                        ; Preds .B3.262
        add       esp, 8                                        ;118.8
                                ; LOE esi edi
.B3.263:                        ; Preds .B3.553
        and       esi, -2                                       ;118.8
        mov       DWORD PTR [-140+ebp], esi                     ;118.8
        mov       esi, DWORD PTR [-180+ebp]                     ;118.8
        mov       edx, esi                                      ;118.8
        shr       edx, 1                                        ;118.8
        mov       eax, esi                                      ;118.8
        and       edx, 1                                        ;118.8
        and       eax, 1                                        ;118.8
        shl       edx, 2                                        ;118.8
        add       eax, eax                                      ;118.8
        or        edx, eax                                      ;118.8
        or        edx, 262144                                   ;118.8
        mov       DWORD PTR [-152+ebp], 0                       ;118.8
        push      edx                                           ;118.8
        push      DWORD PTR [-16+ebp]                           ;118.8
        call      _for_dealloc_allocatable                      ;118.8
                                ; LOE esi edi
.B3.554:                        ; Preds .B3.263
        add       esp, 8                                        ;118.8
                                ; LOE esi edi
.B3.264:                        ; Preds .B3.554
        and       esi, -2                                       ;118.8
        mov       DWORD PTR [-180+ebp], esi                     ;118.8
        mov       esi, DWORD PTR [-220+ebp]                     ;118.8
        mov       edx, esi                                      ;118.8
        shr       edx, 1                                        ;118.8
        mov       eax, esi                                      ;118.8
        and       edx, 1                                        ;118.8
        and       eax, 1                                        ;118.8
        shl       edx, 2                                        ;118.8
        add       eax, eax                                      ;118.8
        or        edx, eax                                      ;118.8
        or        edx, 262144                                   ;118.8
        push      edx                                           ;118.8
        push      edi                                           ;118.8
        mov       DWORD PTR [-192+ebp], 0                       ;118.8
        call      _for_dealloc_allocatable                      ;118.8
                                ; LOE esi
.B3.555:                        ; Preds .B3.264
        add       esp, 8                                        ;118.8
                                ; LOE esi
.B3.265:                        ; Preds .B3.555
        and       esi, -2                                       ;118.8
        mov       DWORD PTR [-220+ebp], esi                     ;118.8
        mov       esi, DWORD PTR [-100+ebp]                     ;118.8
        mov       edx, esi                                      ;118.8
        shr       edx, 1                                        ;118.8
        mov       eax, esi                                      ;118.8
        and       edx, 1                                        ;118.8
        and       eax, 1                                        ;118.8
        shl       edx, 2                                        ;118.8
        add       eax, eax                                      ;118.8
        or        edx, eax                                      ;118.8
        or        edx, 262144                                   ;118.8
        mov       DWORD PTR [-232+ebp], 0                       ;118.8
        push      edx                                           ;118.8
        push      DWORD PTR [-12+ebp]                           ;118.8
        call      _for_dealloc_allocatable                      ;118.8
                                ; LOE esi
.B3.556:                        ; Preds .B3.265
        add       esp, 8                                        ;118.8
                                ; LOE esi
.B3.266:                        ; Preds .B3.556
        mov       eax, DWORD PTR [-260+ebp]                     ;119.8
        and       esi, -2                                       ;118.8
        mov       DWORD PTR [-112+ebp], 0                       ;118.8
        test      al, 1                                         ;119.11
        mov       DWORD PTR [-100+ebp], esi                     ;118.8
        je        .B3.271       ; Prob 60%                      ;119.11
                                ; LOE eax
.B3.267:                        ; Preds .B3.266
        mov       ecx, eax                                      ;119.30
        mov       edx, eax                                      ;119.30
        shr       ecx, 1                                        ;119.30
        and       edx, 1                                        ;119.30
        and       ecx, 1                                        ;119.30
        add       edx, edx                                      ;119.30
        shl       ecx, 2                                        ;119.30
        or        ecx, edx                                      ;119.30
        or        ecx, 262144                                   ;119.30
        push      ecx                                           ;119.30
        push      DWORD PTR [-272+ebp]                          ;119.30
        mov       DWORD PTR [-360+ebp], eax                     ;119.30
        call      _for_dealloc_allocatable                      ;119.30
                                ; LOE
.B3.557:                        ; Preds .B3.267
        mov       eax, DWORD PTR [-360+ebp]                     ;
        add       esp, 8                                        ;119.30
                                ; LOE eax al ah
.B3.268:                        ; Preds .B3.557
        mov       esi, DWORD PTR [-300+ebp]                     ;119.30
        mov       ecx, esi                                      ;119.30
        shr       ecx, 1                                        ;119.30
        mov       edx, esi                                      ;119.30
        and       ecx, 1                                        ;119.30
        and       edx, 1                                        ;119.30
        shl       ecx, 2                                        ;119.30
        add       edx, edx                                      ;119.30
        or        ecx, edx                                      ;119.30
        and       eax, -2                                       ;119.30
        or        ecx, 262144                                   ;119.30
        push      ecx                                           ;119.30
        push      DWORD PTR [-312+ebp]                          ;119.30
        mov       DWORD PTR [-272+ebp], 0                       ;119.30
        mov       DWORD PTR [-260+ebp], eax                     ;119.30
        mov       DWORD PTR [-360+ebp], eax                     ;119.30
        call      _for_dealloc_allocatable                      ;119.30
                                ; LOE esi
.B3.558:                        ; Preds .B3.268
        mov       eax, DWORD PTR [-360+ebp]                     ;
        add       esp, 8                                        ;119.30
                                ; LOE eax esi al ah
.B3.269:                        ; Preds .B3.558
        mov       edi, DWORD PTR [-340+ebp]                     ;119.30
        mov       ecx, edi                                      ;119.30
        shr       ecx, 1                                        ;119.30
        mov       edx, edi                                      ;119.30
        and       ecx, 1                                        ;119.30
        and       edx, 1                                        ;119.30
        shl       ecx, 2                                        ;119.30
        add       edx, edx                                      ;119.30
        or        ecx, edx                                      ;119.30
        and       esi, -2                                       ;119.30
        or        ecx, 262144                                   ;119.30
        push      ecx                                           ;119.30
        push      DWORD PTR [-352+ebp]                          ;119.30
        mov       DWORD PTR [-312+ebp], 0                       ;119.30
        mov       DWORD PTR [-300+ebp], esi                     ;119.30
        mov       DWORD PTR [-360+ebp], eax                     ;119.30
        call      _for_dealloc_allocatable                      ;119.30
                                ; LOE esi edi
.B3.559:                        ; Preds .B3.269
        mov       eax, DWORD PTR [-360+ebp]                     ;
        add       esp, 8                                        ;119.30
                                ; LOE eax esi edi al ah
.B3.270:                        ; Preds .B3.559
        and       edi, -2                                       ;119.30
        mov       DWORD PTR [-352+ebp], 0                       ;119.30
        mov       DWORD PTR [-340+ebp], edi                     ;119.30
        jmp       .B3.276       ; Prob 100%                     ;119.30
                                ; LOE eax esi edi
.B3.271:                        ; Preds .B3.266
        mov       edi, DWORD PTR [-340+ebp]                     ;121.1
        mov       esi, DWORD PTR [-300+ebp]                     ;121.1
                                ; LOE eax esi edi
.B3.276:                        ; Preds .B3.270 .B3.271
        test      al, 1                                         ;121.1
        jne       .B3.483       ; Prob 3%                       ;121.1
                                ; LOE eax esi edi
.B3.277:                        ; Preds .B3.276 .B3.484
        test      esi, 1                                        ;121.1
        jne       .B3.481       ; Prob 3%                       ;121.1
                                ; LOE esi edi
.B3.278:                        ; Preds .B3.277 .B3.482
        test      edi, 1                                        ;121.1
        jne       .B3.479       ; Prob 3%                       ;121.1
                                ; LOE edi
.B3.279:                        ; Preds .B3.278 .B3.480
        mov       esi, DWORD PTR [-40+ebp]                      ;121.1
        mov       edi, DWORD PTR [-36+ebp]                      ;121.1
        mov       esp, ebp                                      ;121.1
        pop       ebp                                           ;121.1
        mov       esp, ebx                                      ;121.1
        pop       ebx                                           ;121.1
        ret                                                     ;121.1
                                ; LOE
.B3.280:                        ; Preds .B3.106
        xor       eax, eax                                      ;
        mov       DWORD PTR [-356+ebp], eax                     ;
        mov       DWORD PTR [-116+ebp], eax                     ;
        mov       DWORD PTR [-360+ebp], eax                     ;
        mov       eax, edx                                      ;
        shr       eax, 31                                       ;
        add       eax, edx                                      ;
        sar       eax, 1                                        ;
        mov       DWORD PTR [-72+ebp], eax                      ;
        mov       DWORD PTR [-76+ebp], edx                      ;
        mov       DWORD PTR [-60+ebp], ecx                      ;
        mov       DWORD PTR [-316+ebp], edi                     ;
        mov       DWORD PTR [-236+ebp], esi                     ;
        mov       edx, DWORD PTR [-360+ebp]                     ;
        mov       eax, DWORD PTR [-116+ebp]                     ;
        mov       ecx, DWORD PTR [-356+ebp]                     ;
                                ; LOE eax edx ecx
.B3.281:                        ; Preds .B3.293 .B3.508 .B3.280
        cmp       DWORD PTR [-72+ebp], 0                        ;99.28
        jbe       .B3.506       ; Prob 10%                      ;99.28
                                ; LOE eax edx ecx
.B3.282:                        ; Preds .B3.281
        mov       ecx, DWORD PTR [-276+ebp]                     ;
        xor       edi, edi                                      ;
        mov       DWORD PTR [-116+ebp], eax                     ;
        mov       eax, DWORD PTR [-72+ebp]                      ;
        mov       esi, DWORD PTR [-12+ebp]                      ;
        lea       ecx, DWORD PTR [ecx+edx*4]                    ;
                                ; LOE eax edx ecx esi edi
.B3.283:                        ; Preds .B3.287 .B3.282
        test      BYTE PTR [ecx+edi*8], 1                       ;99.28
        je        .B3.285       ; Prob 60%                      ;99.28
                                ; LOE eax edx ecx esi edi
.B3.284:                        ; Preds .B3.283
        mov       DWORD PTR [esi+edi*8], -1                     ;99.28
                                ; LOE eax edx ecx esi edi
.B3.285:                        ; Preds .B3.283 .B3.284
        test      BYTE PTR [4+ecx+edi*8], 1                     ;99.28
        je        .B3.287       ; Prob 60%                      ;99.28
                                ; LOE eax edx ecx esi edi
.B3.286:                        ; Preds .B3.285
        mov       DWORD PTR [4+esi+edi*8], -1                   ;99.28
                                ; LOE eax edx ecx esi edi
.B3.287:                        ; Preds .B3.285 .B3.286
        inc       edi                                           ;99.28
        cmp       edi, eax                                      ;99.28
        jb        .B3.283       ; Prob 63%                      ;99.28
                                ; LOE eax edx ecx esi edi
.B3.288:                        ; Preds .B3.287
        mov       eax, DWORD PTR [-116+ebp]                     ;
        lea       esi, DWORD PTR [1+edi+edi]                    ;99.28
        lea       ecx, DWORD PTR [edx+edi*2]                    ;99.28
                                ; LOE eax edx ecx esi
.B3.289:                        ; Preds .B3.288 .B3.506
        lea       edi, DWORD PTR [-1+esi]                       ;99.28
        cmp       edi, DWORD PTR [-76+ebp]                      ;99.28
        jae       .B3.507       ; Prob 10%                      ;99.28
                                ; LOE eax edx ecx esi
.B3.290:                        ; Preds .B3.289
        lea       ecx, DWORD PTR [esi+edx]                      ;99.28
        mov       edx, DWORD PTR [-276+ebp]                     ;99.28
        test      BYTE PTR [-4+edx+ecx*4], 1                    ;99.28
        je        .B3.292       ; Prob 60%                      ;99.28
                                ; LOE eax ecx esi
.B3.291:                        ; Preds .B3.290
        mov       edx, DWORD PTR [-12+ebp]                      ;99.28
        mov       DWORD PTR [-4+edx+esi*4], -1                  ;99.28
                                ; LOE eax ecx
.B3.292:                        ; Preds .B3.290 .B3.291
        inc       eax                                           ;99.28
        cmp       eax, DWORD PTR [-60+ebp]                      ;99.28
        jae       .B3.505       ; Prob 18%                      ;99.28
                                ; LOE eax ecx
.B3.293:                        ; Preds .B3.292
        mov       edx, ecx                                      ;99.28
        jmp       .B3.281       ; Prob 100%                     ;99.28
                                ; LOE eax edx ecx
.B3.294:                        ; Preds .B3.104
        xor       edx, edx                                      ;
        mov       DWORD PTR [-360+ebp], edx                     ;
        pxor      xmm0, xmm0                                    ;99.25
        mov       DWORD PTR [-356+ebp], edx                     ;
        mov       DWORD PTR [-76+ebp], edx                      ;
        mov       DWORD PTR [-316+ebp], edi                     ;99.25
        mov       DWORD PTR [-236+ebp], esi                     ;99.25
        mov       DWORD PTR [-116+ebp], eax                     ;99.25
        mov       DWORD PTR [-60+ebp], ecx                      ;99.25
        mov       esi, DWORD PTR [-356+ebp]                     ;99.25
        mov       edi, DWORD PTR [-360+ebp]                     ;99.25
        mov       edx, DWORD PTR [-20+ebp]                      ;99.25
                                ; LOE edx esi edi xmm0
.B3.295:                        ; Preds .B3.316 .B3.512 .B3.294
        cmp       edx, 4                                        ;99.23
        jl        .B3.515       ; Prob 10%                      ;99.23
                                ; LOE edx esi edi xmm0
.B3.296:                        ; Preds .B3.295
        mov       ecx, DWORD PTR [-276+ebp]                     ;99.23
        lea       ecx, DWORD PTR [ecx+esi*4]                    ;99.23
        mov       DWORD PTR [-360+ebp], ecx                     ;99.23
        and       ecx, 15                                       ;99.23
        mov       eax, ecx                                      ;99.23
        neg       eax                                           ;99.23
        add       eax, 16                                       ;99.23
        shr       eax, 2                                        ;99.23
        test      ecx, ecx                                      ;99.23
        cmovne    ecx, eax                                      ;99.23
        lea       eax, DWORD PTR [4+ecx]                        ;99.23
        cmp       edx, eax                                      ;99.23
        jl        .B3.515       ; Prob 10%                      ;99.23
                                ; LOE edx ecx esi edi xmm0
.B3.297:                        ; Preds .B3.296
        mov       eax, edx                                      ;99.23
        sub       eax, ecx                                      ;99.23
        and       eax, 3                                        ;99.23
        neg       eax                                           ;99.23
        add       eax, edx                                      ;99.23
        test      ecx, ecx                                      ;99.23
        jbe       .B3.303       ; Prob 10%                      ;99.23
                                ; LOE eax edx ecx esi xmm0
.B3.298:                        ; Preds .B3.297
        mov       DWORD PTR [-356+ebp], esi                     ;
        xor       edi, edi                                      ;
        mov       edx, DWORD PTR [-360+ebp]                     ;
        mov       esi, DWORD PTR [-116+ebp]                     ;
                                ; LOE eax edx ecx esi edi xmm0
.B3.299:                        ; Preds .B3.301 .B3.298
        cmp       DWORD PTR [esi+edi*4], 0                      ;99.23
        jle       .B3.514       ; Prob 16%                      ;99.23
                                ; LOE eax edx ecx esi edi xmm0
.B3.300:                        ; Preds .B3.299
        mov       DWORD PTR [edx+edi*4], -1                     ;99.23
                                ; LOE eax edx ecx esi edi xmm0
.B3.301:                        ; Preds .B3.300 .B3.514
        inc       edi                                           ;99.23
        cmp       edi, ecx                                      ;99.23
        jb        .B3.299       ; Prob 82%                      ;99.23
                                ; LOE eax edx ecx esi edi xmm0
.B3.302:                        ; Preds .B3.301
        mov       esi, DWORD PTR [-356+ebp]                     ;
        mov       edx, DWORD PTR [-20+ebp]                      ;
                                ; LOE eax edx ecx esi xmm0
.B3.303:                        ; Preds .B3.297 .B3.302
        mov       edi, DWORD PTR [-116+ebp]                     ;99.23
        lea       edi, DWORD PTR [edi+ecx*4]                    ;99.23
        test      edi, 15                                       ;99.23
        je        .B3.307       ; Prob 60%                      ;99.23
                                ; LOE eax edx ecx esi xmm0
.B3.304:                        ; Preds .B3.303
        mov       edx, DWORD PTR [-360+ebp]                     ;
        mov       edi, DWORD PTR [-116+ebp]                     ;
                                ; LOE eax edx ecx esi edi xmm0
.B3.305:                        ; Preds .B3.305 .B3.304
        movdqu    xmm1, XMMWORD PTR [edi+ecx*4]                 ;99.23
        pcmpgtd   xmm1, xmm0                                    ;99.23
        movdqa    XMMWORD PTR [edx+ecx*4], xmm1                 ;99.23
        add       ecx, 4                                        ;99.23
        cmp       ecx, eax                                      ;99.23
        jb        .B3.305       ; Prob 82%                      ;99.23
        jmp       .B3.309       ; Prob 100%                     ;99.23
                                ; LOE eax edx ecx esi edi xmm0
.B3.307:                        ; Preds .B3.303
        mov       edx, DWORD PTR [-360+ebp]                     ;
        mov       edi, DWORD PTR [-116+ebp]                     ;
                                ; LOE eax edx ecx esi edi xmm0
.B3.308:                        ; Preds .B3.308 .B3.307
        movdqa    xmm1, XMMWORD PTR [edi+ecx*4]                 ;99.23
        pcmpgtd   xmm1, xmm0                                    ;99.23
        movdqa    XMMWORD PTR [edx+ecx*4], xmm1                 ;99.23
        add       ecx, 4                                        ;99.23
        cmp       ecx, eax                                      ;99.23
        jb        .B3.308       ; Prob 82%                      ;99.23
                                ; LOE eax edx ecx esi edi xmm0
.B3.309:                        ; Preds .B3.305 .B3.308
        mov       edx, DWORD PTR [-20+ebp]                      ;
        lea       edi, DWORD PTR [ecx+esi]                      ;99.23
                                ; LOE eax edx esi edi xmm0
.B3.310:                        ; Preds .B3.309 .B3.515
        cmp       eax, edx                                      ;99.23
        jae       .B3.511       ; Prob 10%                      ;99.23
                                ; LOE eax edx esi edi xmm0
.B3.311:                        ; Preds .B3.310
        mov       ecx, DWORD PTR [-276+ebp]                     ;
        mov       edi, DWORD PTR [-116+ebp]                     ;
        lea       ecx, DWORD PTR [ecx+esi*4]                    ;
                                ; LOE eax edx ecx esi edi xmm0
.B3.312:                        ; Preds .B3.314 .B3.311
        cmp       DWORD PTR [edi+eax*4], 0                      ;99.23
        jle       .B3.517       ; Prob 16%                      ;99.23
                                ; LOE eax edx ecx esi edi xmm0
.B3.313:                        ; Preds .B3.312
        mov       DWORD PTR [ecx+eax*4], -1                     ;99.23
                                ; LOE eax edx ecx esi edi xmm0
.B3.314:                        ; Preds .B3.313 .B3.517
        inc       eax                                           ;99.23
        cmp       eax, edx                                      ;99.23
        jb        .B3.312       ; Prob 82%                      ;99.23
                                ; LOE eax edx ecx esi edi xmm0
.B3.315:                        ; Preds .B3.314
        lea       edi, DWORD PTR [eax+esi]                      ;99.23
        mov       eax, DWORD PTR [-76+ebp]                      ;99.23
        inc       eax                                           ;99.23
        mov       DWORD PTR [-76+ebp], eax                      ;99.23
        cmp       eax, DWORD PTR [-60+ebp]                      ;99.23
        jae       .B3.510       ; Prob 18%                      ;99.23
                                ; LOE edx edi xmm0
.B3.316:                        ; Preds .B3.315
        mov       esi, edi                                      ;99.23
        jmp       .B3.295       ; Prob 100%                     ;99.23
                                ; LOE edx esi edi xmm0
.B3.317:                        ; Preds .B3.4                   ; Infreq
        cmp       esi, 4                                        ;88.8
        jl        .B3.333       ; Prob 10%                      ;88.8
                                ; LOE ecx esi
.B3.318:                        ; Preds .B3.317                 ; Infreq
        mov       edx, ecx                                      ;88.8
        and       edx, 15                                       ;88.8
        je        .B3.321       ; Prob 50%                      ;88.8
                                ; LOE edx ecx esi
.B3.319:                        ; Preds .B3.318                 ; Infreq
        test      dl, 3                                         ;88.8
        jne       .B3.333       ; Prob 10%                      ;88.8
                                ; LOE edx ecx esi
.B3.320:                        ; Preds .B3.319                 ; Infreq
        neg       edx                                           ;88.8
        add       edx, 16                                       ;88.8
        shr       edx, 2                                        ;88.8
                                ; LOE edx ecx esi
.B3.321:                        ; Preds .B3.320 .B3.318         ; Infreq
        lea       eax, DWORD PTR [4+edx]                        ;88.8
        cmp       esi, eax                                      ;88.8
        jl        .B3.333       ; Prob 10%                      ;88.8
                                ; LOE edx ecx esi
.B3.322:                        ; Preds .B3.321                 ; Infreq
        mov       eax, esi                                      ;88.8
        sub       eax, edx                                      ;88.8
        and       eax, 3                                        ;88.8
        neg       eax                                           ;88.8
        add       eax, esi                                      ;88.8
        test      edx, edx                                      ;88.8
        jbe       .B3.326       ; Prob 10%                      ;88.8
                                ; LOE eax edx ecx esi
.B3.323:                        ; Preds .B3.322                 ; Infreq
        xor       edi, edi                                      ;
                                ; LOE eax edx ecx esi edi
.B3.324:                        ; Preds .B3.324 .B3.323         ; Infreq
        mov       DWORD PTR [ecx+edi*4], 0                      ;88.8
        inc       edi                                           ;88.8
        cmp       edi, edx                                      ;88.8
        jb        .B3.324       ; Prob 82%                      ;88.8
                                ; LOE eax edx ecx esi edi
.B3.326:                        ; Preds .B3.324 .B3.322         ; Infreq
        pxor      xmm0, xmm0                                    ;88.8
                                ; LOE eax edx ecx esi xmm0
.B3.327:                        ; Preds .B3.327 .B3.326         ; Infreq
        movdqa    XMMWORD PTR [ecx+edx*4], xmm0                 ;88.8
        add       edx, 4                                        ;88.8
        cmp       edx, eax                                      ;88.8
        jb        .B3.327       ; Prob 82%                      ;88.8
                                ; LOE eax edx ecx esi xmm0
.B3.329:                        ; Preds .B3.327 .B3.333         ; Infreq
        cmp       eax, esi                                      ;88.8
        jae       .B3.6         ; Prob 10%                      ;88.8
                                ; LOE eax ecx esi
.B3.331:                        ; Preds .B3.329 .B3.331         ; Infreq
        mov       DWORD PTR [ecx+eax*4], 0                      ;88.8
        inc       eax                                           ;88.8
        cmp       eax, esi                                      ;88.8
        jb        .B3.331       ; Prob 82%                      ;88.8
        jmp       .B3.6         ; Prob 100%                     ;88.8
                                ; LOE eax ecx esi
.B3.333:                        ; Preds .B3.317 .B3.321 .B3.319 ; Infreq
        xor       eax, eax                                      ;88.8
        jmp       .B3.329       ; Prob 100%                     ;88.8
                                ; LOE eax ecx esi
.B3.336:                        ; Preds .B3.13                  ; Infreq
        cmp       DWORD PTR [-20+ebp], 4                        ;90.8
        jl        .B3.352       ; Prob 10%                      ;90.8
                                ; LOE ecx
.B3.337:                        ; Preds .B3.336                 ; Infreq
        mov       eax, ecx                                      ;90.8
        and       eax, 15                                       ;90.8
        je        .B3.340       ; Prob 50%                      ;90.8
                                ; LOE eax ecx
.B3.338:                        ; Preds .B3.337                 ; Infreq
        test      al, 3                                         ;90.8
        jne       .B3.352       ; Prob 10%                      ;90.8
                                ; LOE eax ecx
.B3.339:                        ; Preds .B3.338                 ; Infreq
        neg       eax                                           ;90.8
        add       eax, 16                                       ;90.8
        shr       eax, 2                                        ;90.8
                                ; LOE eax ecx
.B3.340:                        ; Preds .B3.339 .B3.337         ; Infreq
        lea       edx, DWORD PTR [4+eax]                        ;90.8
        cmp       edx, DWORD PTR [-20+ebp]                      ;90.8
        jg        .B3.352       ; Prob 10%                      ;90.8
                                ; LOE eax ecx
.B3.341:                        ; Preds .B3.340                 ; Infreq
        mov       esi, DWORD PTR [-20+ebp]                      ;90.8
        mov       edx, esi                                      ;90.8
        sub       edx, eax                                      ;90.8
        and       edx, 3                                        ;90.8
        neg       edx                                           ;90.8
        add       edx, esi                                      ;90.8
        test      eax, eax                                      ;90.8
        jbe       .B3.345       ; Prob 10%                      ;90.8
                                ; LOE eax edx ecx
.B3.342:                        ; Preds .B3.341                 ; Infreq
        xor       esi, esi                                      ;
                                ; LOE eax edx ecx esi
.B3.343:                        ; Preds .B3.343 .B3.342         ; Infreq
        mov       DWORD PTR [ecx+esi*4], 0                      ;90.8
        inc       esi                                           ;90.8
        cmp       esi, eax                                      ;90.8
        jb        .B3.343       ; Prob 82%                      ;90.8
                                ; LOE eax edx ecx esi
.B3.345:                        ; Preds .B3.343 .B3.341         ; Infreq
        pxor      xmm0, xmm0                                    ;90.8
                                ; LOE eax edx ecx xmm0
.B3.346:                        ; Preds .B3.346 .B3.345         ; Infreq
        movdqa    XMMWORD PTR [ecx+eax*4], xmm0                 ;90.8
        add       eax, 4                                        ;90.8
        cmp       eax, edx                                      ;90.8
        jb        .B3.346       ; Prob 82%                      ;90.8
                                ; LOE eax edx ecx xmm0
.B3.348:                        ; Preds .B3.346 .B3.352         ; Infreq
        cmp       edx, DWORD PTR [-20+ebp]                      ;90.8
        jae       .B3.15        ; Prob 10%                      ;90.8
                                ; LOE edx ecx
.B3.349:                        ; Preds .B3.348                 ; Infreq
        mov       eax, DWORD PTR [-20+ebp]                      ;
                                ; LOE eax edx ecx
.B3.350:                        ; Preds .B3.350 .B3.349         ; Infreq
        mov       DWORD PTR [ecx+edx*4], 0                      ;90.8
        inc       edx                                           ;90.8
        cmp       edx, eax                                      ;90.8
        jb        .B3.350       ; Prob 82%                      ;90.8
        jmp       .B3.15        ; Prob 100%                     ;90.8
                                ; LOE eax edx ecx
.B3.352:                        ; Preds .B3.336 .B3.340 .B3.338 ; Infreq
        xor       edx, edx                                      ;90.8
        jmp       .B3.348       ; Prob 100%                     ;90.8
                                ; LOE edx ecx
.B3.355:                        ; Preds .B3.16                  ; Infreq
        cmp       esi, 4                                        ;91.8
        jl        .B3.371       ; Prob 10%                      ;91.8
                                ; LOE ecx esi
.B3.356:                        ; Preds .B3.355                 ; Infreq
        mov       edx, ecx                                      ;91.8
        and       edx, 15                                       ;91.8
        je        .B3.359       ; Prob 50%                      ;91.8
                                ; LOE edx ecx esi
.B3.357:                        ; Preds .B3.356                 ; Infreq
        test      dl, 3                                         ;91.8
        jne       .B3.371       ; Prob 10%                      ;91.8
                                ; LOE edx ecx esi
.B3.358:                        ; Preds .B3.357                 ; Infreq
        neg       edx                                           ;91.8
        add       edx, 16                                       ;91.8
        shr       edx, 2                                        ;91.8
                                ; LOE edx ecx esi
.B3.359:                        ; Preds .B3.358 .B3.356         ; Infreq
        lea       eax, DWORD PTR [4+edx]                        ;91.8
        cmp       esi, eax                                      ;91.8
        jl        .B3.371       ; Prob 10%                      ;91.8
                                ; LOE edx ecx esi
.B3.360:                        ; Preds .B3.359                 ; Infreq
        mov       eax, esi                                      ;91.8
        sub       eax, edx                                      ;91.8
        and       eax, 3                                        ;91.8
        neg       eax                                           ;91.8
        add       eax, esi                                      ;91.8
        test      edx, edx                                      ;91.8
        jbe       .B3.364       ; Prob 10%                      ;91.8
                                ; LOE eax edx ecx esi
.B3.361:                        ; Preds .B3.360                 ; Infreq
        xor       edi, edi                                      ;
                                ; LOE eax edx ecx esi edi
.B3.362:                        ; Preds .B3.362 .B3.361         ; Infreq
        mov       DWORD PTR [ecx+edi*4], 0                      ;91.8
        inc       edi                                           ;91.8
        cmp       edi, edx                                      ;91.8
        jb        .B3.362       ; Prob 82%                      ;91.8
                                ; LOE eax edx ecx esi edi
.B3.364:                        ; Preds .B3.362 .B3.360         ; Infreq
        pxor      xmm0, xmm0                                    ;91.8
                                ; LOE eax edx ecx esi xmm0
.B3.365:                        ; Preds .B3.365 .B3.364         ; Infreq
        movdqa    XMMWORD PTR [ecx+edx*4], xmm0                 ;91.8
        add       edx, 4                                        ;91.8
        cmp       edx, eax                                      ;91.8
        jb        .B3.365       ; Prob 82%                      ;91.8
                                ; LOE eax edx ecx esi xmm0
.B3.367:                        ; Preds .B3.365 .B3.371         ; Infreq
        cmp       eax, esi                                      ;91.8
        jae       .B3.18        ; Prob 10%                      ;91.8
                                ; LOE eax ecx esi
.B3.369:                        ; Preds .B3.367 .B3.369         ; Infreq
        mov       DWORD PTR [ecx+eax*4], 0                      ;91.8
        inc       eax                                           ;91.8
        cmp       eax, esi                                      ;91.8
        jb        .B3.369       ; Prob 82%                      ;91.8
        jmp       .B3.18        ; Prob 100%                     ;91.8
                                ; LOE eax ecx esi
.B3.371:                        ; Preds .B3.355 .B3.359 .B3.357 ; Infreq
        xor       eax, eax                                      ;91.8
        jmp       .B3.367       ; Prob 100%                     ;91.8
                                ; LOE eax ecx esi
.B3.374:                        ; Preds .B3.19                  ; Infreq
        cmp       esi, 4                                        ;92.8
        jl        .B3.390       ; Prob 10%                      ;92.8
                                ; LOE ecx esi
.B3.375:                        ; Preds .B3.374                 ; Infreq
        mov       edx, ecx                                      ;92.8
        and       edx, 15                                       ;92.8
        je        .B3.378       ; Prob 50%                      ;92.8
                                ; LOE edx ecx esi
.B3.376:                        ; Preds .B3.375                 ; Infreq
        test      dl, 3                                         ;92.8
        jne       .B3.390       ; Prob 10%                      ;92.8
                                ; LOE edx ecx esi
.B3.377:                        ; Preds .B3.376                 ; Infreq
        neg       edx                                           ;92.8
        add       edx, 16                                       ;92.8
        shr       edx, 2                                        ;92.8
                                ; LOE edx ecx esi
.B3.378:                        ; Preds .B3.377 .B3.375         ; Infreq
        lea       eax, DWORD PTR [4+edx]                        ;92.8
        cmp       esi, eax                                      ;92.8
        jl        .B3.390       ; Prob 10%                      ;92.8
                                ; LOE edx ecx esi
.B3.379:                        ; Preds .B3.378                 ; Infreq
        mov       eax, esi                                      ;92.8
        sub       eax, edx                                      ;92.8
        and       eax, 3                                        ;92.8
        neg       eax                                           ;92.8
        add       eax, esi                                      ;92.8
        test      edx, edx                                      ;92.8
        jbe       .B3.383       ; Prob 10%                      ;92.8
                                ; LOE eax edx ecx esi
.B3.380:                        ; Preds .B3.379                 ; Infreq
        xor       edi, edi                                      ;
                                ; LOE eax edx ecx esi edi
.B3.381:                        ; Preds .B3.381 .B3.380         ; Infreq
        mov       DWORD PTR [ecx+edi*4], 0                      ;92.8
        inc       edi                                           ;92.8
        cmp       edi, edx                                      ;92.8
        jb        .B3.381       ; Prob 82%                      ;92.8
                                ; LOE eax edx ecx esi edi
.B3.383:                        ; Preds .B3.381 .B3.379         ; Infreq
        pxor      xmm0, xmm0                                    ;92.8
                                ; LOE eax edx ecx esi xmm0
.B3.384:                        ; Preds .B3.384 .B3.383         ; Infreq
        movdqa    XMMWORD PTR [ecx+edx*4], xmm0                 ;92.8
        add       edx, 4                                        ;92.8
        cmp       edx, eax                                      ;92.8
        jb        .B3.384       ; Prob 82%                      ;92.8
                                ; LOE eax edx ecx esi xmm0
.B3.386:                        ; Preds .B3.384 .B3.390         ; Infreq
        cmp       eax, esi                                      ;92.8
        jae       .B3.21        ; Prob 10%                      ;92.8
                                ; LOE eax ecx esi
.B3.388:                        ; Preds .B3.386 .B3.388         ; Infreq
        mov       DWORD PTR [ecx+eax*4], 0                      ;92.8
        inc       eax                                           ;92.8
        cmp       eax, esi                                      ;92.8
        jb        .B3.388       ; Prob 82%                      ;92.8
        jmp       .B3.21        ; Prob 100%                     ;92.8
                                ; LOE eax ecx esi
.B3.390:                        ; Preds .B3.374 .B3.378 .B3.376 ; Infreq
        xor       eax, eax                                      ;92.8
        jmp       .B3.386       ; Prob 100%                     ;92.8
                                ; LOE eax ecx esi
.B3.393:                        ; Preds .B3.23 .B3.27 .B3.25    ; Infreq
        mov       edi, DWORD PTR [32+esi]                       ;94.20
        xor       edx, edx                                      ;94.10
        mov       DWORD PTR [-356+ebp], edx                     ;94.10
        mov       DWORD PTR [-196+ebp], edi                     ;94.20
        jmp       .B3.39        ; Prob 100%                     ;94.20
                                ; LOE eax ecx esi
.B3.396:                        ; Preds .B3.50 .B3.54 .B3.52    ; Infreq
        xor       eax, eax                                      ;95.10
        mov       DWORD PTR [-316+ebp], eax                     ;95.10
        jmp       .B3.66        ; Prob 100%                     ;95.10
                                ; LOE edx ecx esi
.B3.399:                        ; Preds .B3.77 .B3.81 .B3.79    ; Infreq
        xor       eax, eax                                      ;96.10
        mov       DWORD PTR [-316+ebp], eax                     ;96.10
        jmp       .B3.93        ; Prob 100%                     ;96.10
                                ; LOE edx ecx esi
.B3.402:                        ; Preds .B3.120 .B3.124 .B3.122 ; Infreq
        xor       esi, esi                                      ;
        xor       eax, eax                                      ;
        jmp       .B3.132       ; Prob 100%                     ;
                                ; LOE eax edx esi
.B3.403:                        ; Preds .B3.145 .B3.149 .B3.147 ; Infreq
        xor       eax, eax                                      ;
        xor       edx, edx                                      ;
        jmp       .B3.157       ; Prob 100%                     ;
                                ; LOE eax edx ecx esi
.B3.404:                        ; Preds .B3.174                 ; Infreq
        cmp       eax, 4                                        ;108.10
        jl        .B3.424       ; Prob 10%                      ;108.10
                                ; LOE eax edx esi dl dh
.B3.405:                        ; Preds .B3.404                 ; Infreq
        mov       edi, edx                                      ;108.10
        and       edi, 15                                       ;108.10
        je        .B3.408       ; Prob 50%                      ;108.10
                                ; LOE eax esi edi
.B3.406:                        ; Preds .B3.405                 ; Infreq
        test      edi, 3                                        ;108.10
        jne       .B3.424       ; Prob 10%                      ;108.10
                                ; LOE eax esi edi
.B3.407:                        ; Preds .B3.406                 ; Infreq
        neg       edi                                           ;108.10
        add       edi, 16                                       ;108.10
        shr       edi, 2                                        ;108.10
                                ; LOE eax esi edi
.B3.408:                        ; Preds .B3.407 .B3.405         ; Infreq
        lea       edx, DWORD PTR [4+edi]                        ;108.10
        cmp       eax, edx                                      ;108.10
        jl        .B3.424       ; Prob 10%                      ;108.10
                                ; LOE eax esi edi
.B3.409:                        ; Preds .B3.408                 ; Infreq
        mov       edx, eax                                      ;108.10
        sub       edx, edi                                      ;108.10
        and       edx, 3                                        ;108.10
        neg       edx                                           ;108.10
        add       edx, eax                                      ;108.10
        test      edi, edi                                      ;108.10
        jbe       .B3.413       ; Prob 10%                      ;108.10
                                ; LOE eax edx esi edi
.B3.410:                        ; Preds .B3.409                 ; Infreq
        mov       DWORD PTR [-360+ebp], edx                     ;
        xor       ecx, ecx                                      ;
        mov       DWORD PTR [-156+ebp], esi                     ;
        mov       DWORD PTR [-356+ebp], eax                     ;
        mov       edx, DWORD PTR [-316+ebp]                     ;
        mov       esi, DWORD PTR [-72+ebp]                      ;
                                ; LOE edx ecx esi edi
.B3.411:                        ; Preds .B3.411 .B3.410         ; Infreq
        mov       eax, DWORD PTR [esi+ecx*4]                    ;108.10
        mov       DWORD PTR [edx+ecx*4], eax                    ;108.10
        inc       ecx                                           ;108.10
        cmp       ecx, edi                                      ;108.10
        jb        .B3.411       ; Prob 82%                      ;108.10
                                ; LOE edx ecx esi edi
.B3.412:                        ; Preds .B3.411                 ; Infreq
        mov       edx, DWORD PTR [-360+ebp]                     ;
        mov       esi, DWORD PTR [-156+ebp]                     ;
        mov       eax, DWORD PTR [-356+ebp]                     ;
                                ; LOE eax edx esi edi
.B3.413:                        ; Preds .B3.409 .B3.412         ; Infreq
        mov       ecx, DWORD PTR [-72+ebp]                      ;108.10
        lea       ecx, DWORD PTR [ecx+edi*4]                    ;108.10
        test      cl, 15                                        ;108.10
        je        .B3.417       ; Prob 60%                      ;108.10
                                ; LOE eax edx esi edi
.B3.414:                        ; Preds .B3.413                 ; Infreq
        mov       DWORD PTR [-356+ebp], eax                     ;
        mov       eax, DWORD PTR [-316+ebp]                     ;
        mov       ecx, DWORD PTR [-72+ebp]                      ;
                                ; LOE eax edx ecx esi edi
.B3.415:                        ; Preds .B3.415 .B3.414         ; Infreq
        movdqu    xmm0, XMMWORD PTR [ecx+edi*4]                 ;108.10
        movdqa    XMMWORD PTR [eax+edi*4], xmm0                 ;108.10
        add       edi, 4                                        ;108.10
        cmp       edi, edx                                      ;108.10
        jb        .B3.415       ; Prob 82%                      ;108.10
        jmp       .B3.419       ; Prob 100%                     ;108.10
                                ; LOE eax edx ecx esi edi
.B3.417:                        ; Preds .B3.413                 ; Infreq
        mov       DWORD PTR [-356+ebp], eax                     ;
        mov       eax, DWORD PTR [-316+ebp]                     ;
        mov       ecx, DWORD PTR [-72+ebp]                      ;
                                ; LOE eax edx ecx esi edi
.B3.418:                        ; Preds .B3.418 .B3.417         ; Infreq
        movdqa    xmm0, XMMWORD PTR [ecx+edi*4]                 ;108.10
        movdqa    XMMWORD PTR [eax+edi*4], xmm0                 ;108.10
        add       edi, 4                                        ;108.10
        cmp       edi, edx                                      ;108.10
        jb        .B3.418       ; Prob 82%                      ;108.10
                                ; LOE eax edx ecx esi edi
.B3.419:                        ; Preds .B3.415 .B3.418         ; Infreq
        mov       eax, DWORD PTR [-356+ebp]                     ;
                                ; LOE eax edx esi
.B3.420:                        ; Preds .B3.419 .B3.424         ; Infreq
        cmp       edx, eax                                      ;108.10
        jae       .B3.176       ; Prob 10%                      ;108.10
                                ; LOE eax edx esi
.B3.421:                        ; Preds .B3.420                 ; Infreq
        mov       DWORD PTR [-156+ebp], esi                     ;
        mov       esi, DWORD PTR [-316+ebp]                     ;
        mov       edi, DWORD PTR [-72+ebp]                      ;
                                ; LOE eax edx esi edi
.B3.422:                        ; Preds .B3.422 .B3.421         ; Infreq
        mov       ecx, DWORD PTR [edi+edx*4]                    ;108.10
        mov       DWORD PTR [esi+edx*4], ecx                    ;108.10
        inc       edx                                           ;108.10
        cmp       edx, eax                                      ;108.10
        jb        .B3.422       ; Prob 82%                      ;108.10
                                ; LOE eax edx esi edi
.B3.423:                        ; Preds .B3.422                 ; Infreq
        mov       esi, DWORD PTR [-156+ebp]                     ;
        jmp       .B3.176       ; Prob 100%                     ;
                                ; LOE esi
.B3.424:                        ; Preds .B3.404 .B3.408 .B3.406 ; Infreq
        xor       edx, edx                                      ;108.10
        jmp       .B3.420       ; Prob 100%                     ;108.10
                                ; LOE eax edx esi
.B3.427:                        ; Preds .B3.177 .B3.181 .B3.179 ; Infreq
        xor       eax, eax                                      ;
        xor       edx, edx                                      ;
        jmp       .B3.189       ; Prob 100%                     ;
                                ; LOE eax edx ecx esi
.B3.430:                        ; Preds .B3.207                 ; Infreq
        cmp       eax, 4                                        ;109.10
        jl        .B3.450       ; Prob 10%                      ;109.10
                                ; LOE eax edx esi dl dh
.B3.431:                        ; Preds .B3.430                 ; Infreq
        mov       edi, edx                                      ;109.10
        and       edi, 15                                       ;109.10
        je        .B3.434       ; Prob 50%                      ;109.10
                                ; LOE eax esi edi
.B3.432:                        ; Preds .B3.431                 ; Infreq
        test      edi, 3                                        ;109.10
        jne       .B3.450       ; Prob 10%                      ;109.10
                                ; LOE eax esi edi
.B3.433:                        ; Preds .B3.432                 ; Infreq
        neg       edi                                           ;109.10
        add       edi, 16                                       ;109.10
        shr       edi, 2                                        ;109.10
                                ; LOE eax esi edi
.B3.434:                        ; Preds .B3.433 .B3.431         ; Infreq
        lea       edx, DWORD PTR [4+edi]                        ;109.10
        cmp       eax, edx                                      ;109.10
        jl        .B3.450       ; Prob 10%                      ;109.10
                                ; LOE eax esi edi
.B3.435:                        ; Preds .B3.434                 ; Infreq
        mov       edx, eax                                      ;109.10
        sub       edx, edi                                      ;109.10
        and       edx, 3                                        ;109.10
        neg       edx                                           ;109.10
        add       edx, eax                                      ;109.10
        test      edi, edi                                      ;109.10
        jbe       .B3.439       ; Prob 10%                      ;109.10
                                ; LOE eax edx esi edi
.B3.436:                        ; Preds .B3.435                 ; Infreq
        mov       DWORD PTR [-360+ebp], edx                     ;
        xor       ecx, ecx                                      ;
        mov       DWORD PTR [-156+ebp], esi                     ;
        mov       DWORD PTR [-356+ebp], eax                     ;
        mov       edx, DWORD PTR [-68+ebp]                      ;
        mov       esi, DWORD PTR [-72+ebp]                      ;
                                ; LOE edx ecx esi edi
.B3.437:                        ; Preds .B3.437 .B3.436         ; Infreq
        mov       eax, DWORD PTR [esi+ecx*4]                    ;109.10
        mov       DWORD PTR [edx+ecx*4], eax                    ;109.10
        inc       ecx                                           ;109.10
        cmp       ecx, edi                                      ;109.10
        jb        .B3.437       ; Prob 82%                      ;109.10
                                ; LOE edx ecx esi edi
.B3.438:                        ; Preds .B3.437                 ; Infreq
        mov       edx, DWORD PTR [-360+ebp]                     ;
        mov       esi, DWORD PTR [-156+ebp]                     ;
        mov       eax, DWORD PTR [-356+ebp]                     ;
                                ; LOE eax edx esi edi
.B3.439:                        ; Preds .B3.435 .B3.438         ; Infreq
        mov       ecx, DWORD PTR [-72+ebp]                      ;109.10
        lea       ecx, DWORD PTR [ecx+edi*4]                    ;109.10
        test      cl, 15                                        ;109.10
        je        .B3.443       ; Prob 60%                      ;109.10
                                ; LOE eax edx esi edi
.B3.440:                        ; Preds .B3.439                 ; Infreq
        mov       DWORD PTR [-356+ebp], eax                     ;
        mov       eax, DWORD PTR [-68+ebp]                      ;
        mov       ecx, DWORD PTR [-72+ebp]                      ;
                                ; LOE eax edx ecx esi edi
.B3.441:                        ; Preds .B3.441 .B3.440         ; Infreq
        movdqu    xmm0, XMMWORD PTR [ecx+edi*4]                 ;109.10
        movdqa    XMMWORD PTR [eax+edi*4], xmm0                 ;109.10
        add       edi, 4                                        ;109.10
        cmp       edi, edx                                      ;109.10
        jb        .B3.441       ; Prob 82%                      ;109.10
        jmp       .B3.445       ; Prob 100%                     ;109.10
                                ; LOE eax edx ecx esi edi
.B3.443:                        ; Preds .B3.439                 ; Infreq
        mov       DWORD PTR [-356+ebp], eax                     ;
        mov       eax, DWORD PTR [-68+ebp]                      ;
        mov       ecx, DWORD PTR [-72+ebp]                      ;
                                ; LOE eax edx ecx esi edi
.B3.444:                        ; Preds .B3.444 .B3.443         ; Infreq
        movdqa    xmm0, XMMWORD PTR [ecx+edi*4]                 ;109.10
        movdqa    XMMWORD PTR [eax+edi*4], xmm0                 ;109.10
        add       edi, 4                                        ;109.10
        cmp       edi, edx                                      ;109.10
        jb        .B3.444       ; Prob 82%                      ;109.10
                                ; LOE eax edx ecx esi edi
.B3.445:                        ; Preds .B3.441 .B3.444         ; Infreq
        mov       eax, DWORD PTR [-356+ebp]                     ;
                                ; LOE eax edx esi
.B3.446:                        ; Preds .B3.445 .B3.450         ; Infreq
        cmp       edx, eax                                      ;109.10
        jae       .B3.209       ; Prob 10%                      ;109.10
                                ; LOE eax edx esi
.B3.447:                        ; Preds .B3.446                 ; Infreq
        mov       DWORD PTR [-156+ebp], esi                     ;
        mov       esi, DWORD PTR [-68+ebp]                      ;
        mov       edi, DWORD PTR [-72+ebp]                      ;
                                ; LOE eax edx esi edi
.B3.448:                        ; Preds .B3.448 .B3.447         ; Infreq
        mov       ecx, DWORD PTR [edi+edx*4]                    ;109.10
        mov       DWORD PTR [esi+edx*4], ecx                    ;109.10
        inc       edx                                           ;109.10
        cmp       edx, eax                                      ;109.10
        jb        .B3.448       ; Prob 82%                      ;109.10
                                ; LOE eax edx esi edi
.B3.449:                        ; Preds .B3.448                 ; Infreq
        mov       esi, DWORD PTR [-156+ebp]                     ;
        jmp       .B3.209       ; Prob 100%                     ;
                                ; LOE esi
.B3.450:                        ; Preds .B3.430 .B3.434 .B3.432 ; Infreq
        xor       edx, edx                                      ;109.10
        jmp       .B3.446       ; Prob 100%                     ;109.10
                                ; LOE eax edx esi
.B3.453:                        ; Preds .B3.210 .B3.214 .B3.212 ; Infreq
        xor       eax, eax                                      ;
        xor       ecx, ecx                                      ;
        jmp       .B3.222       ; Prob 100%                     ;
                                ; LOE eax edx ecx esi
.B3.456:                        ; Preds .B3.240                 ; Infreq
        cmp       eax, 4                                        ;110.10
        jl        .B3.476       ; Prob 10%                      ;110.10
                                ; LOE eax edx esi edi dl dh
.B3.457:                        ; Preds .B3.456                 ; Infreq
        mov       ecx, edx                                      ;110.10
        and       ecx, 15                                       ;110.10
        je        .B3.460       ; Prob 50%                      ;110.10
                                ; LOE eax ecx esi edi
.B3.458:                        ; Preds .B3.457                 ; Infreq
        test      cl, 3                                         ;110.10
        jne       .B3.476       ; Prob 10%                      ;110.10
                                ; LOE eax ecx esi edi
.B3.459:                        ; Preds .B3.458                 ; Infreq
        neg       ecx                                           ;110.10
        add       ecx, 16                                       ;110.10
        shr       ecx, 2                                        ;110.10
                                ; LOE eax ecx esi edi
.B3.460:                        ; Preds .B3.459 .B3.457         ; Infreq
        lea       edx, DWORD PTR [4+ecx]                        ;110.10
        cmp       eax, edx                                      ;110.10
        jl        .B3.476       ; Prob 10%                      ;110.10
                                ; LOE eax ecx esi edi
.B3.461:                        ; Preds .B3.460                 ; Infreq
        mov       edx, eax                                      ;110.10
        sub       edx, ecx                                      ;110.10
        and       edx, 3                                        ;110.10
        neg       edx                                           ;110.10
        add       edx, eax                                      ;110.10
        mov       DWORD PTR [-60+ebp], edx                      ;110.10
        test      ecx, ecx                                      ;110.10
        jbe       .B3.465       ; Prob 10%                      ;110.10
                                ; LOE eax ecx esi edi
.B3.462:                        ; Preds .B3.461                 ; Infreq
        mov       DWORD PTR [-156+ebp], esi                     ;
        xor       edx, edx                                      ;
        mov       DWORD PTR [-76+ebp], edi                      ;
        mov       DWORD PTR [-360+ebp], eax                     ;
        mov       esi, DWORD PTR [-356+ebp]                     ;
        mov       edi, DWORD PTR [-64+ebp]                      ;
                                ; LOE edx ecx esi edi
.B3.463:                        ; Preds .B3.463 .B3.462         ; Infreq
        mov       eax, DWORD PTR [edi+edx*4]                    ;110.10
        mov       DWORD PTR [esi+edx*4], eax                    ;110.10
        inc       edx                                           ;110.10
        cmp       edx, ecx                                      ;110.10
        jb        .B3.463       ; Prob 82%                      ;110.10
                                ; LOE edx ecx esi edi
.B3.464:                        ; Preds .B3.463                 ; Infreq
        mov       esi, DWORD PTR [-156+ebp]                     ;
        mov       eax, DWORD PTR [-360+ebp]                     ;
        mov       edi, DWORD PTR [-76+ebp]                      ;
                                ; LOE eax ecx esi edi
.B3.465:                        ; Preds .B3.461 .B3.464         ; Infreq
        mov       edx, DWORD PTR [-64+ebp]                      ;110.10
        lea       edx, DWORD PTR [edx+ecx*4]                    ;110.10
        test      dl, 15                                        ;110.10
        je        .B3.469       ; Prob 60%                      ;110.10
                                ; LOE eax ecx esi edi
.B3.466:                        ; Preds .B3.465                 ; Infreq
        mov       DWORD PTR [-360+ebp], eax                     ;
        mov       DWORD PTR [-76+ebp], edi                      ;
        mov       eax, DWORD PTR [-60+ebp]                      ;
        mov       edx, DWORD PTR [-356+ebp]                     ;
        mov       edi, DWORD PTR [-64+ebp]                      ;
                                ; LOE eax edx ecx esi edi
.B3.467:                        ; Preds .B3.467 .B3.466         ; Infreq
        movdqu    xmm0, XMMWORD PTR [edi+ecx*4]                 ;110.10
        movdqa    XMMWORD PTR [edx+ecx*4], xmm0                 ;110.10
        add       ecx, 4                                        ;110.10
        cmp       ecx, eax                                      ;110.10
        jb        .B3.467       ; Prob 82%                      ;110.10
        jmp       .B3.471       ; Prob 100%                     ;110.10
                                ; LOE eax edx ecx esi edi
.B3.469:                        ; Preds .B3.465                 ; Infreq
        mov       DWORD PTR [-360+ebp], eax                     ;
        mov       DWORD PTR [-76+ebp], edi                      ;
        mov       eax, DWORD PTR [-60+ebp]                      ;
        mov       edx, DWORD PTR [-356+ebp]                     ;
        mov       edi, DWORD PTR [-64+ebp]                      ;
                                ; LOE eax edx ecx esi edi
.B3.470:                        ; Preds .B3.470 .B3.469         ; Infreq
        movdqa    xmm0, XMMWORD PTR [edi+ecx*4]                 ;110.10
        movdqa    XMMWORD PTR [edx+ecx*4], xmm0                 ;110.10
        add       ecx, 4                                        ;110.10
        cmp       ecx, eax                                      ;110.10
        jb        .B3.470       ; Prob 82%                      ;110.10
                                ; LOE eax edx ecx esi edi
.B3.471:                        ; Preds .B3.467 .B3.470         ; Infreq
        mov       DWORD PTR [-60+ebp], eax                      ;
        mov       eax, DWORD PTR [-360+ebp]                     ;
        mov       edi, DWORD PTR [-76+ebp]                      ;
                                ; LOE eax esi edi
.B3.472:                        ; Preds .B3.471 .B3.476         ; Infreq
        cmp       eax, DWORD PTR [-60+ebp]                      ;110.10
        jbe       .B3.243       ; Prob 10%                      ;110.10
                                ; LOE eax esi edi
.B3.473:                        ; Preds .B3.472                 ; Infreq
        mov       DWORD PTR [-156+ebp], esi                     ;
        mov       DWORD PTR [-76+ebp], edi                      ;
        mov       ecx, DWORD PTR [-60+ebp]                      ;
        mov       esi, DWORD PTR [-356+ebp]                     ;
        mov       edi, DWORD PTR [-64+ebp]                      ;
                                ; LOE eax ecx esi edi
.B3.474:                        ; Preds .B3.474 .B3.473         ; Infreq
        mov       edx, DWORD PTR [edi+ecx*4]                    ;110.10
        mov       DWORD PTR [esi+ecx*4], edx                    ;110.10
        inc       ecx                                           ;110.10
        cmp       ecx, eax                                      ;110.10
        jb        .B3.474       ; Prob 82%                      ;110.10
                                ; LOE eax ecx esi edi
.B3.475:                        ; Preds .B3.474                 ; Infreq
        mov       esi, DWORD PTR [-156+ebp]                     ;
        mov       edi, DWORD PTR [-76+ebp]                      ;
        jmp       .B3.243       ; Prob 100%                     ;
                                ; LOE esi edi
.B3.476:                        ; Preds .B3.456 .B3.460 .B3.458 ; Infreq
        xor       edx, edx                                      ;110.10
        mov       DWORD PTR [-60+ebp], edx                      ;110.10
        jmp       .B3.472       ; Prob 100%                     ;110.10
                                ; LOE eax esi edi
.B3.479:                        ; Preds .B3.278                 ; Infreq
        mov       edx, edi                                      ;121.1
        mov       eax, edi                                      ;121.1
        shr       edx, 1                                        ;121.1
        and       eax, 1                                        ;121.1
        and       edx, 1                                        ;121.1
        add       eax, eax                                      ;121.1
        shl       edx, 2                                        ;121.1
        or        edx, eax                                      ;121.1
        or        edx, 262144                                   ;121.1
        push      edx                                           ;121.1
        push      DWORD PTR [-352+ebp]                          ;121.1
        call      _for_dealloc_allocatable                      ;121.1
                                ; LOE edi
.B3.560:                        ; Preds .B3.479                 ; Infreq
        add       esp, 8                                        ;121.1
                                ; LOE edi
.B3.480:                        ; Preds .B3.560                 ; Infreq
        and       edi, -2                                       ;121.1
        mov       DWORD PTR [-352+ebp], 0                       ;121.1
        mov       DWORD PTR [-340+ebp], edi                     ;121.1
        jmp       .B3.279       ; Prob 100%                     ;121.1
                                ; LOE
.B3.481:                        ; Preds .B3.277                 ; Infreq
        mov       edx, esi                                      ;121.1
        mov       eax, esi                                      ;121.1
        shr       edx, 1                                        ;121.1
        and       eax, 1                                        ;121.1
        and       edx, 1                                        ;121.1
        add       eax, eax                                      ;121.1
        shl       edx, 2                                        ;121.1
        or        edx, eax                                      ;121.1
        or        edx, 262144                                   ;121.1
        push      edx                                           ;121.1
        push      DWORD PTR [-312+ebp]                          ;121.1
        call      _for_dealloc_allocatable                      ;121.1
                                ; LOE esi edi
.B3.561:                        ; Preds .B3.481                 ; Infreq
        add       esp, 8                                        ;121.1
                                ; LOE esi edi
.B3.482:                        ; Preds .B3.561                 ; Infreq
        and       esi, -2                                       ;121.1
        mov       DWORD PTR [-312+ebp], 0                       ;121.1
        mov       DWORD PTR [-300+ebp], esi                     ;121.1
        jmp       .B3.278       ; Prob 100%                     ;121.1
                                ; LOE edi
.B3.483:                        ; Preds .B3.276                 ; Infreq
        mov       ecx, eax                                      ;121.1
        mov       edx, eax                                      ;121.1
        shr       ecx, 1                                        ;121.1
        and       edx, 1                                        ;121.1
        and       ecx, 1                                        ;121.1
        add       edx, edx                                      ;121.1
        shl       ecx, 2                                        ;121.1
        or        ecx, edx                                      ;121.1
        or        ecx, 262144                                   ;121.1
        push      ecx                                           ;121.1
        push      DWORD PTR [-272+ebp]                          ;121.1
        mov       DWORD PTR [-360+ebp], eax                     ;121.1
        call      _for_dealloc_allocatable                      ;121.1
                                ; LOE esi edi
.B3.562:                        ; Preds .B3.483                 ; Infreq
        mov       eax, DWORD PTR [-360+ebp]                     ;
        add       esp, 8                                        ;121.1
                                ; LOE eax esi edi al ah
.B3.484:                        ; Preds .B3.562                 ; Infreq
        and       eax, -2                                       ;121.1
        mov       DWORD PTR [-272+ebp], 0                       ;121.1
        mov       DWORD PTR [-260+ebp], eax                     ;121.1
        jmp       .B3.277       ; Prob 100%                     ;121.1
                                ; LOE esi edi
.B3.493:                        ; Preds .B3.255                 ; Infreq
        mov       edx, 1                                        ;
        jmp       .B3.259       ; Prob 100%                     ;
                                ; LOE edx esi edi
.B3.494:                        ; Preds .B3.249                 ; Infreq
        mov       ecx, 1                                        ;
        jmp       .B3.253       ; Prob 100%                     ;
                                ; LOE edx ecx esi edi
.B3.495:                        ; Preds .B3.552                 ; Infreq
        mov       ecx, 1                                        ;
        jmp       .B3.247       ; Prob 100%                     ;
                                ; LOE edx ecx esi edi
.B3.496:                        ; Preds .B3.228                 ; Infreq
        mov       eax, 1                                        ;
        jmp       .B3.236       ; Prob 100%                     ;
                                ; LOE eax edx esi edi
.B3.497:                        ; Preds .B3.195                 ; Infreq
        mov       eax, 1                                        ;
        jmp       .B3.203       ; Prob 100%                     ;
                                ; LOE eax edx ecx esi
.B3.498:                        ; Preds .B3.162                 ; Infreq
        mov       eax, 1                                        ;
        jmp       .B3.170       ; Prob 100%                     ;
                                ; LOE eax edx ecx esi
.B3.501:                        ; Preds .B3.144                 ; Infreq
        xor       eax, eax                                      ;
        jmp       .B3.161       ; Prob 100%                     ;
                                ; LOE eax ecx esi
.B3.504:                        ; Preds .B3.119                 ; Infreq
        xor       esi, esi                                      ;
        jmp       .B3.137       ; Prob 100%                     ;
                                ; LOE esi
.B3.505:                        ; Preds .B3.507 .B3.292         ; Infreq
        mov       edx, DWORD PTR [-76+ebp]                      ;
        mov       edi, DWORD PTR [-316+ebp]                     ;
        mov       esi, DWORD PTR [-236+ebp]                     ;
        jmp       .B3.107       ; Prob 100%                     ;
                                ; LOE edx esi edi
.B3.506:                        ; Preds .B3.281                 ; Infreq
        mov       esi, 1                                        ;
        jmp       .B3.289       ; Prob 100%                     ;
                                ; LOE eax edx ecx esi
.B3.507:                        ; Preds .B3.289                 ; Infreq
        inc       eax                                           ;99.28
        cmp       eax, DWORD PTR [-60+ebp]                      ;99.28
        jae       .B3.505       ; Prob 18%                      ;99.28
                                ; LOE eax ecx
.B3.508:                        ; Preds .B3.507                 ; Infreq
        mov       edx, ecx                                      ;99.28
        jmp       .B3.281       ; Prob 100%                     ;99.28
                                ; LOE eax edx ecx
.B3.510:                        ; Preds .B3.511 .B3.315         ; Infreq
        mov       ecx, DWORD PTR [-60+ebp]                      ;
        test      ecx, ecx                                      ;94.10
        mov       edi, DWORD PTR [-316+ebp]                     ;
        mov       esi, DWORD PTR [-236+ebp]                     ;
        jmp       .B3.105       ; Prob 100%                     ;
                                ; LOE ecx esi edi
.B3.511:                        ; Preds .B3.310                 ; Infreq
        mov       eax, DWORD PTR [-76+ebp]                      ;99.23
        inc       eax                                           ;99.23
        mov       DWORD PTR [-76+ebp], eax                      ;99.23
        cmp       eax, DWORD PTR [-60+ebp]                      ;99.23
        jae       .B3.510       ; Prob 18%                      ;99.23
                                ; LOE edx edi xmm0
.B3.512:                        ; Preds .B3.511                 ; Infreq
        mov       esi, edi                                      ;99.23
        jmp       .B3.295       ; Prob 100%                     ;99.23
                                ; LOE edx esi edi xmm0
.B3.514:                        ; Preds .B3.299                 ; Infreq
        mov       DWORD PTR [edx+edi*4], 0                      ;99.23
        jmp       .B3.301       ; Prob 100%                     ;99.23
                                ; LOE eax edx ecx esi edi xmm0
.B3.515:                        ; Preds .B3.295 .B3.296         ; Infreq
        xor       eax, eax                                      ;99.23
        jmp       .B3.310       ; Prob 100%                     ;99.23
                                ; LOE eax edx esi edi xmm0
.B3.517:                        ; Preds .B3.312                 ; Infreq
        mov       DWORD PTR [ecx+eax*4], 0                      ;99.23
        jmp       .B3.314       ; Prob 100%                     ;99.23
                                ; LOE eax edx ecx esi edi xmm0
.B3.518:                        ; Preds .B3.97                  ; Infreq
        mov       eax, 1                                        ;
        jmp       .B3.101       ; Prob 100%                     ;
                                ; LOE eax edx ecx esi al ah
.B3.519:                        ; Preds .B3.70                  ; Infreq
        mov       eax, 1                                        ;
        jmp       .B3.74        ; Prob 100%                     ;
                                ; LOE eax edx ecx esi
.B3.520:                        ; Preds .B3.43                  ; Infreq
        mov       edx, 1                                        ;
        jmp       .B3.47        ; Prob 100%                     ;
                                ; LOE eax edx ecx esi
.B3.521:                        ; Preds .B3.21                  ; Infreq
        mov       eax, DWORD PTR [32+esi]                       ;101.8
        mov       DWORD PTR [-196+ebp], eax                     ;101.8
        jmp       .B3.103       ; Prob 100%                     ;101.8
                                ; LOE ecx esi
.B3.563:                        ; Preds .B3.104                 ; Infreq
        test      ecx, ecx                                      ;94.10
        jmp       .B3.105       ; Prob 100%                     ;94.10
                                ; LOE ecx esi edi
.B3.564:                        ; Preds .B3.93 .B3.101          ; Infreq
        test      ecx, ecx                                      ;94.10
        jmp       .B3.103       ; Prob 100%                     ;94.10
        ALIGN     16
                                ; LOE ecx esi
; mark_end;
_TRANSLINK_ADD_MODULE_mp_TRANLINK_CONSOLIDATE_NEW ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _TRANSLINK_ADD_MODULE_mp_TRANLINK_CONSOLIDATE_NEW
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DD 2 DUP (0H)	; pad
	DB 3 DUP ( 0H)	; pad
_2il0floatpacket.23	DD	000000001H,000000001H,000000001H,000000001H
__STRLITPACK_0	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	97
	DB	108
	DB	108
	DB	111
	DB	99
	DB	97
	DB	116
	DB	105
	DB	110
	DB	103
	DB	32
	DB	116
	DB	109
	DB	112
	DB	80
	DB	32
	DB	118
	DB	101
	DB	99
	DB	116
	DB	111
	DB	114
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	84
	DB	114
	DB	97
	DB	110
	DB	76
	DB	105
	DB	110
	DB	107
	DB	95
	DB	83
	DB	101
	DB	116
	DB	117
	DB	112
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_3	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE:BYTE
EXTRN	_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_ARRAY:BYTE
_DATA	ENDS
EXTRN	_for_alloc_allocatable:PROC
EXTRN	_for_dealloc_allocatable:PROC
EXTRN	_for_stop_core:PROC
EXTRN	_for_write_seq_lis:PROC
EXTRN	_for_allocate:PROC
EXTRN	_for_check_mult_overflow:PROC
EXTRN	__intel_fast_memcpy:PROC
EXTRN	__intel_fast_memset:PROC
EXTRN	__alloca_probe:PROC
EXTRN	__fltused:BYTE
	INCLUDELIB <ifconsol>
	INCLUDELIB <libifcoremt>
	INCLUDELIB <libifport>
	INCLUDELIB <libmmt>
	INCLUDELIB <LIBCMT>
	INCLUDELIB <libirc>
	INCLUDELIB <svml_dispmt>
	INCLUDELIB <OLDNAMES>
	END
