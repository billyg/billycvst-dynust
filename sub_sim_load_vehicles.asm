; mark_description "Intel(R) Visual Fortran Compiler XE for applications running on IA-32, Version 13.1.1.171 Build 20130313";
; mark_description "-S";
	.686P
 	.387
	OPTION DOTNAME
	ASSUME	CS:FLAT,DS:FLAT,SS:FLAT
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
TXTST0:
; -- Begin  _SIM_LOAD_VEHICLES
; mark_begin;
IF @Version GE 800
  .MMX
ELSEIF @Version GE 612
  .MMX
  MMWORD TEXTEQU <QWORD>
ENDIF
IF @Version GE 800
  .XMM
ELSEIF @Version GE 614
  .XMM
  XMMWORD TEXTEQU <OWORD>
ENDIF
       ALIGN     16
	PUBLIC _SIM_LOAD_VEHICLES
_SIM_LOAD_VEHICLES	PROC NEAR 
; parameter 1: 8 + ebp
.B1.1:                          ; Preds .B1.0
        push      ebp                                           ;1.12
        mov       ebp, esp                                      ;1.12
        and       esp, -16                                      ;1.12
        push      esi                                           ;1.12
        push      edi                                           ;1.12
        push      ebx                                           ;1.12
        sub       esp, 244                                      ;1.12
        xor       eax, eax                                      ;38.23
        mov       edx, DWORD PTR [8+ebp]                        ;1.12
        movss     xmm0, DWORD PTR [_2il0floatpacket.7]          ;48.6
        mov       DWORD PTR [64+esp], eax                       ;38.23
        movss     xmm5, DWORD PTR [edx]                         ;48.1
        mov       DWORD PTR [68+esp], eax                       ;38.23
        mov       DWORD PTR [72+esp], eax                       ;38.23
        mov       DWORD PTR [76+esp], 128                       ;38.23
        mov       DWORD PTR [80+esp], 2                         ;38.23
        mov       DWORD PTR [84+esp], eax                       ;38.23
        mov       DWORD PTR [88+esp], eax                       ;38.23
        mov       DWORD PTR [92+esp], eax                       ;38.23
        mov       DWORD PTR [96+esp], eax                       ;38.23
        mov       DWORD PTR [100+esp], eax                      ;38.23
        mov       DWORD PTR [104+esp], eax                      ;38.23
        mov       DWORD PTR [108+esp], eax                      ;38.23
        comiss    xmm0, xmm5                                    ;48.6
        jbe       .B1.3         ; Prob 50%                      ;48.6
                                ; LOE xmm5
.B1.2:                          ; Preds .B1.1
        push      40000                                         ;50.3
        xor       eax, eax                                      ;49.3
        push      eax                                           ;50.3
        push      OFFSET FLAT: _SIM_LOAD_VEHICLES$PATHIN.0.1    ;50.3
        mov       DWORD PTR [_SIM_LOAD_VEHICLES$JFO.0.1], eax   ;49.3
        movss     DWORD PTR [12+esp], xmm5                      ;50.3
        call      __intel_fast_memset                           ;50.3
                                ; LOE
.B1.404:                        ; Preds .B1.2
        movss     xmm5, DWORD PTR [12+esp]                      ;
        add       esp, 12                                       ;50.3
                                ; LOE xmm5
.B1.3:                          ; Preds .B1.1 .B1.404
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_RUNMODE] ;53.1
        mov       eax, edx                                      ;53.17
        and       eax, -3                                       ;53.17
        cmp       eax, 1                                        ;53.17
        je        .B1.124       ; Prob 16%                      ;53.17
                                ; LOE edx xmm5
.B1.4:                          ; Preds .B1.3
        cmp       edx, 4                                        ;53.45
        je        .B1.124       ; Prob 16%                      ;53.45
                                ; LOE xmm5
.B1.5:                          ; Preds .B1.4
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ITERATION] ;86.1
                                ; LOE eax
.B1.6:                          ; Preds .B1.124 .B1.5
        test      eax, eax                                      ;86.18
        jle       .B1.47        ; Prob 16%                      ;86.18
                                ; LOE
.B1.7:                          ; Preds .B1.6
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_JUSTVEH] ;88.3
        cmp       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NUMGENVEH] ;88.20
        jge       .B1.47        ; Prob 10%                      ;88.20
                                ; LOE eax
.B1.8:                          ; Preds .B1.7
        cmp       DWORD PTR [_SIM_LOAD_VEHICLES$JFO.0.1], 1     ;90.11
        jne       .B1.30        ; Prob 77%                      ;90.11
                                ; LOE eax
.B1.10:                         ; Preds .B1.8
        inc       eax                                           ;92.7
        mov       edx, OFFSET FLAT: _SIM_LOAD_VEHICLES$PATHIN.0.1+4 ;
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_JUSTVEH], eax ;92.7
        mov       eax, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_MOP] ;115.33
        mov       DWORD PTR [_SIM_LOAD_VEHICLES$JFO.0.1], 0     ;91.7
        mov       DWORD PTR [8+esp], eax                        ;
                                ; LOE
.B1.11:                         ; Preds .B1.10 .B1.44
        mov       ebx, DWORD PTR [_SIM_LOAD_VEHICLES$PATHIN.0.1+4] ;109.7
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_MAXNU_PA] ;109.16
        lea       eax, DWORD PTR [3+ebx]                        ;109.16
        cmp       esi, eax                                      ;109.19
        jl        .B1.122       ; Prob 5%                       ;109.19
                                ; LOE ebx esi
.B1.12:                         ; Preds .B1.422 .B1.11
        sub       esi, ebx                                      ;114.4
        lea       edi, DWORD PTR [-2+esi]                       ;114.4
        test      edi, edi                                      ;114.4
        jle       .B1.15        ; Prob 50%                      ;114.4
                                ; LOE ebx esi edi
.B1.13:                         ; Preds .B1.12
        cmp       edi, 24                                       ;114.4
        jle       .B1.94        ; Prob 0%                       ;114.4
                                ; LOE ebx esi edi
.B1.14:                         ; Preds .B1.13
        lea       esi, DWORD PTR [-8+esi*4]                     ;114.4
        push      esi                                           ;114.4
        push      0                                             ;114.4
        lea       eax, DWORD PTR [_SIM_LOAD_VEHICLES$PATHIN.0.1+8+ebx*4] ;114.4
        push      eax                                           ;114.4
        call      __intel_fast_memset                           ;114.4
                                ; LOE
.B1.405:                        ; Preds .B1.14
        add       esp, 12                                       ;114.4
                                ; LOE
.B1.15:                         ; Preds .B1.105 .B1.405 .B1.12 .B1.103
        mov       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE+32] ;115.4
        neg       ecx                                           ;115.4
        mov       eax, DWORD PTR [8+esp]                        ;115.4
        add       ecx, eax                                      ;115.4
        mov       edi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;115.33
        neg       edi                                           ;115.4
        shl       ecx, 6                                        ;115.4
        add       edi, eax                                      ;115.4
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE] ;115.4
        shl       edi, 8                                        ;115.4
        mov       esi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;115.33
        push      OFFSET FLAT: _SIM_LOAD_VEHICLES$PATHIN.0.1+4  ;117.10
        mov       ebx, DWORD PTR [8+edx+ecx]                    ;115.4
        push      OFFSET FLAT: _DYNUST_VEH_PATH_ATT_MODULE_mp_MOP ;117.10
        mov       DWORD PTR [240+esi+edi], ebx                  ;115.4
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_P_SETUP ;117.10
                                ; LOE
.B1.16:                         ; Preds .B1.15
        push      OFFSET FLAT: _SIM_LOAD_VEHICLES$PATHIN.0.1+4  ;118.10
        push      OFFSET FLAT: _DYNUST_VEH_PATH_ATT_MODULE_mp_MOP ;118.10
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_A_SETUP ;118.10
                                ; LOE
.B1.406:                        ; Preds .B1.16
        add       esp, 16                                       ;118.10
                                ; LOE
.B1.17:                         ; Preds .B1.406
        movsx     edi, WORD PTR [_SIM_LOAD_VEHICLES$PATHIN.0.1+4] ;120.2
        mov       eax, 1                                        ;120.2
        mov       WORD PTR [204+esp], ax                        ;120.2
        test      edi, edi                                      ;120.2
        jle       .B1.22        ; Prob 16%                      ;120.2
                                ; LOE edi
.B1.18:                         ; Preds .B1.17
        mov       edx, 1                                        ;
        lea       esi, DWORD PTR [204+esp]                      ;
        lea       ebx, DWORD PTR [196+esp]                      ;
                                ; LOE edx ebx esi edi
.B1.19:                         ; Preds .B1.20 .B1.18
        movsx     edx, dx                                       ;121.5
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM+32] ;121.5
        neg       eax                                           ;121.5
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM] ;121.5
        add       eax, DWORD PTR [_SIM_LOAD_VEHICLES$PATHIN.0.1+4+edx*4] ;121.5
        cvtsi2ss  xmm0, DWORD PTR [ecx+eax*4]                   ;121.5
        movss     DWORD PTR [196+esp], xmm0                     ;121.5
        push      ebx                                           ;122.10
        push      OFFSET FLAT: __NLITPACK_2.0.1                 ;122.10
        push      esi                                           ;122.10
        push      OFFSET FLAT: _DYNUST_VEH_PATH_ATT_MODULE_mp_MOP ;122.10
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_INSERT  ;122.10
                                ; LOE ebx esi edi
.B1.407:                        ; Preds .B1.19
        add       esp, 16                                       ;122.10
                                ; LOE ebx esi edi
.B1.20:                         ; Preds .B1.407
        dec       edi                                           ;123.5
        movzx     edx, WORD PTR [204+esp]                       ;123.5
        inc       edx                                           ;123.5
        mov       WORD PTR [204+esp], dx                        ;123.5
        test      edi, edi                                      ;123.5
        jg        .B1.19        ; Prob 82%                      ;123.5
                                ; LOE edx ebx esi edi
.B1.22:                         ; Preds .B1.20 .B1.17
        push      OFFSET FLAT: _SIM_LOAD_VEHICLES$PATHIN.0.1+4  ;125.10
        push      OFFSET FLAT: _DYNUST_VEH_PATH_ATT_MODULE_mp_MOP ;125.10
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ACTIVE_SIZE ;125.10
                                ; LOE
.B1.408:                        ; Preds .B1.22
        add       esp, 8                                        ;125.10
                                ; LOE
.B1.23:                         ; Preds .B1.408
        mov       eax, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_MOP] ;127.8
        mov       ecx, eax                                      ;127.49
        shl       ecx, 5                                        ;127.49
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;127.5
        mov       ebx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;127.5
        shl       ebx, 5                                        ;127.49
        lea       esi, DWORD PTR [edx+ecx]                      ;127.49
        sub       esi, ebx                                      ;127.49
        movss     xmm1, DWORD PTR [12+esi]                      ;127.8
        comiss    xmm1, DWORD PTR [_DYNUST_MAIN_MODULE_mp_STARTTM] ;127.39
        jb        .B1.26        ; Prob 50%                      ;127.39
                                ; LOE eax edx ecx ebx xmm1
.B1.24:                         ; Preds .B1.23
        movss     xmm0, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ENDTM] ;127.54
        comiss    xmm0, xmm1                                    ;127.85
        jbe       .B1.26        ; Prob 50%                      ;127.85
                                ; LOE eax edx ecx ebx
.B1.25:                         ; Preds .B1.24
        mov       esi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;129.8
        neg       esi                                           ;129.8
        add       esi, eax                                      ;129.8
        shl       esi, 8                                        ;129.8
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;129.8
        inc       DWORD PTR [_DYNUST_MAIN_MODULE_mp_NUMCARS]    ;128.8
        mov       BYTE PTR [237+eax+esi], 1                     ;129.8
        jmp       .B1.27        ; Prob 100%                     ;129.8
                                ; LOE edx ecx ebx
.B1.26:                         ; Preds .B1.23 .B1.24
        mov       esi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;131.8
        neg       esi                                           ;131.8
        add       esi, eax                                      ;131.8
        shl       esi, 8                                        ;131.8
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;131.8
        inc       DWORD PTR [_DYNUST_MAIN_MODULE_mp_NUMNTAG]    ;132.8
        mov       BYTE PTR [237+eax+esi], 0                     ;131.8
                                ; LOE edx ecx ebx
.B1.27:                         ; Preds .B1.26 .B1.25
        add       ecx, edx                                      ;135.10
        add       ebx, -28                                      ;135.10
        push      OFFSET FLAT: _DYNUST_VEH_PATH_ATT_MODULE_mp_MOP ;135.10
        sub       ecx, ebx                                      ;135.10
        push      ecx                                           ;135.10
        call      _DYNUST_LINK_VEH_LIST_MODULE_mp_ENTRYQUEUE_INSERT ;135.10
                                ; LOE
.B1.28:                         ; Preds .B1.27
        mov       edx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_MOP] ;136.5
        sub       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;136.5
        shl       edx, 5                                        ;136.5
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;136.5
        push      OFFSET FLAT: __NLITPACK_3.0.1                 ;138.10
        push      OFFSET FLAT: __NLITPACK_2.0.1                 ;138.10
        mov       ecx, DWORD PTR [28+eax+edx]                   ;136.71
        sub       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;136.5
        imul      esi, ecx, 900                                 ;136.5
        push      OFFSET FLAT: __NLITPACK_1.0.1                 ;138.10
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;136.5
        push      OFFSET FLAT: _DYNUST_VEH_PATH_ATT_MODULE_mp_MOP ;138.10
        push      OFFSET FLAT: _DYNUST_MAIN_MODULE_mp_JUSTVEH   ;138.10
        push      OFFSET FLAT: _DYNUST_MAIN_MODULE_mp_TOTALVEH  ;138.10
        inc       WORD PTR [692+ebx+esi]                        ;136.136
        call      _WRITE_VEHICLES                               ;138.10
                                ; LOE
.B1.409:                        ; Preds .B1.28
        add       esp, 32                                       ;138.10
                                ; LOE
.B1.29:                         ; Preds .B1.409
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_JUSTVEH] ;88.3
        cmp       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NUMGENVEH] ;88.20
        jge       .B1.47        ; Prob 18%                      ;88.20
                                ; LOE
.B1.30:                         ; Preds .B1.8 .B1.29
        mov       DWORD PTR [112+esp], 0                        ;95.5
        lea       edi, DWORD PTR [112+esp]                      ;95.5
        mov       DWORD PTR [56+esp], 4                         ;95.5
        lea       eax, DWORD PTR [56+esp]                       ;95.5
        mov       DWORD PTR [60+esp], OFFSET FLAT: _SIM_LOAD_VEHICLES$PATHIN.0.1 ;95.5
        push      32                                            ;95.5
        push      OFFSET FLAT: SIM_LOAD_VEHICLES$format_pack.0.1 ;95.5
        push      eax                                           ;95.5
        push      OFFSET FLAT: __STRLITPACK_13.0.1              ;95.5
        push      -2088435968                                   ;95.5
        push      6099                                          ;95.5
        push      edi                                           ;95.5
        call      _for_read_seq_fmt                             ;95.5
                                ; LOE edi
.B1.31:                         ; Preds .B1.30
        mov       DWORD PTR [172+esp], 4                        ;95.5
        lea       eax, DWORD PTR [172+esp]                      ;95.5
        mov       DWORD PTR [176+esp], OFFSET FLAT: _SIM_LOAD_VEHICLES$PATHIN.0.1+4 ;95.5
        push      eax                                           ;95.5
        push      OFFSET FLAT: __STRLITPACK_14.0.1              ;95.5
        push      edi                                           ;95.5
        call      _for_read_seq_fmt_xmit                        ;95.5
                                ; LOE edi
.B1.32:                         ; Preds .B1.31
        push      0                                             ;95.5
        push      OFFSET FLAT: __STRLITPACK_15.0.1              ;95.5
        push      edi                                           ;95.5
        call      _for_read_seq_fmt_xmit                        ;95.5
                                ; LOE edi
.B1.410:                        ; Preds .B1.32
        add       esp, 52                                       ;95.5
                                ; LOE edi
.B1.33:                         ; Preds .B1.410
        mov       esi, DWORD PTR [_SIM_LOAD_VEHICLES$PATHIN.0.1+4] ;95.69
        mov       eax, 3                                        ;95.5
        add       esi, 2                                        ;95.78
        movsx     esi, si                                       ;95.5
        mov       WORD PTR [204+esp], ax                        ;95.5
        add       esi, -2                                       ;95.5
        jle       .B1.38        ; Prob 16%                      ;95.5
                                ; LOE eax esi edi
.B1.34:                         ; Preds .B1.33
        lea       ebx, DWORD PTR [176+esp]                      ;95.5
                                ; LOE eax ebx esi edi
.B1.35:                         ; Preds .B1.36 .B1.34
        movsx     eax, ax                                       ;95.5
        mov       DWORD PTR [176+esp], 4                        ;95.5
        lea       edx, DWORD PTR [_SIM_LOAD_VEHICLES$PATHIN.0.1-4+eax*4] ;95.5
        mov       DWORD PTR [180+esp], edx                      ;95.5
        push      ebx                                           ;95.5
        push      OFFSET FLAT: __STRLITPACK_16.0.1              ;95.5
        push      edi                                           ;95.5
        call      _for_read_seq_fmt_xmit                        ;95.5
                                ; LOE ebx esi edi
.B1.411:                        ; Preds .B1.35
        add       esp, 12                                       ;95.5
                                ; LOE ebx esi edi
.B1.36:                         ; Preds .B1.411
        dec       esi                                           ;95.5
        movzx     eax, WORD PTR [204+esp]                       ;95.5
        inc       eax                                           ;95.5
        mov       WORD PTR [204+esp], ax                        ;95.5
        test      esi, esi                                      ;95.5
        jg        .B1.35        ; Prob 82%                      ;95.5
                                ; LOE eax ebx esi edi
.B1.38:                         ; Preds .B1.36 .B1.33
        push      0                                             ;95.5
        push      OFFSET FLAT: __STRLITPACK_17.0.1              ;95.5
        push      edi                                           ;95.5
        call      _for_read_seq_fmt_xmit                        ;95.5
                                ; LOE
.B1.412:                        ; Preds .B1.38
        add       esp, 12                                       ;95.5
                                ; LOE
.B1.39:                         ; Preds .B1.412
        mov       edx, DWORD PTR [_SIM_LOAD_VEHICLES$PATHIN.0.1] ;97.6
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_JUSTVEH] ;96.6
        inc       eax                                           ;96.6
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_JUSTVEH], eax ;96.6
        mov       DWORD PTR [8+esp], edx                        ;97.6
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_MOP], edx ;97.6
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_FUELOUT], 0 ;98.17
        jle       .B1.44        ; Prob 16%                      ;98.17
                                ; LOE eax
.B1.40:                         ; Preds .B1.39
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ITERATION] ;98.17
        cmp       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ITEMAX] ;98.36
        jne       .B1.43        ; Prob 50%                      ;98.36
                                ; LOE eax
.B1.41:                         ; Preds .B1.40 .B1.43
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_RUNMODE] ;98.20
        mov       edx, ecx                                      ;98.91
        and       edx, -3                                       ;98.91
        cmp       edx, 1                                        ;98.91
        je        .B1.109       ; Prob 16%                      ;98.91
                                ; LOE eax ecx
.B1.42:                         ; Preds .B1.41
        cmp       ecx, 4                                        ;98.119
        je        .B1.109       ; Prob 5%                       ;98.119
        jmp       .B1.44        ; Prob 100%                     ;98.119
                                ; LOE eax
.B1.43:                         ; Preds .B1.40
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_REACH_CONVERG], -1 ;98.63
        je        .B1.41        ; Prob 16%                      ;98.63
                                ; LOE eax
.B1.44:                         ; Preds .B1.42 .B1.43 .B1.110 .B1.39
        mov       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;102.8
        neg       ecx                                           ;102.8
        add       ecx, DWORD PTR [8+esp]                        ;102.8
        shl       ecx, 5                                        ;102.8
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;102.8
        movss     xmm4, DWORD PTR [_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT] ;102.13
        movss     xmm5, DWORD PTR [_2il0floatpacket.10]         ;102.8
        movss     xmm2, DWORD PTR [12+edx+ecx]                  ;102.8
        divss     xmm2, xmm4                                    ;102.8
        andps     xmm5, xmm2                                    ;102.8
        pxor      xmm2, xmm5                                    ;102.8
        movss     xmm0, DWORD PTR [_2il0floatpacket.11]         ;102.8
        movaps    xmm1, xmm2                                    ;102.8
        movaps    xmm3, xmm2                                    ;102.8
        cmpltss   xmm1, xmm0                                    ;102.8
        andps     xmm1, xmm0                                    ;102.8
        mov       ebx, DWORD PTR [8+ebp]                        ;102.61
        addss     xmm3, xmm1                                    ;102.8
        subss     xmm3, xmm1                                    ;102.8
        movss     xmm1, DWORD PTR [_2il0floatpacket.12]         ;102.8
        movaps    xmm7, xmm3                                    ;102.8
        subss     xmm7, xmm2                                    ;102.8
        movaps    xmm2, xmm1                                    ;102.8
        addss     xmm2, xmm1                                    ;102.8
        movaps    xmm6, xmm7                                    ;102.8
        cmpless   xmm7, DWORD PTR [_2il0floatpacket.13]         ;102.8
        cmpnless  xmm6, xmm1                                    ;102.8
        andps     xmm6, xmm2                                    ;102.8
        andps     xmm7, xmm2                                    ;102.8
        subss     xmm3, xmm6                                    ;102.8
        movss     xmm6, DWORD PTR [ebx]                         ;102.61
        divss     xmm6, xmm4                                    ;102.61
        addss     xmm3, xmm7                                    ;102.8
        orps      xmm3, xmm5                                    ;102.8
        movss     xmm5, DWORD PTR [_2il0floatpacket.10]         ;102.61
        andps     xmm5, xmm6                                    ;102.61
        pxor      xmm6, xmm5                                    ;102.61
        movaps    xmm7, xmm6                                    ;102.61
        movaps    xmm4, xmm6                                    ;102.61
        cvtss2si  esi, xmm3                                     ;102.8
        cmpltss   xmm7, xmm0                                    ;102.61
        andps     xmm0, xmm7                                    ;102.61
        addss     xmm4, xmm0                                    ;102.61
        subss     xmm4, xmm0                                    ;102.61
        movaps    xmm7, xmm4                                    ;102.61
        subss     xmm7, xmm6                                    ;102.61
        movaps    xmm6, xmm7                                    ;102.61
        cmpless   xmm7, DWORD PTR [_2il0floatpacket.13]         ;102.61
        cmpnless  xmm6, xmm1                                    ;102.61
        andps     xmm6, xmm2                                    ;102.61
        andps     xmm7, xmm2                                    ;102.61
        subss     xmm4, xmm6                                    ;102.61
        addss     xmm4, xmm7                                    ;102.61
        orps      xmm4, xmm5                                    ;102.61
        cvtss2si  edi, xmm4                                     ;102.61
        cmp       esi, edi                                      ;102.59
        jle       .B1.11        ; Prob 80%                      ;102.59
                                ; LOE eax
.B1.45:                         ; Preds .B1.44
        dec       eax                                           ;103.6
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_JUSTVEH], eax ;103.6
        mov       DWORD PTR [_SIM_LOAD_VEHICLES$JFO.0.1], 1     ;104.6
                                ; LOE
.B1.47:                         ; Preds .B1.6 .B1.7 .B1.134 .B1.29 .B1.127
                                ;       .B1.45
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFARCS] ;148.4
        mov       eax, 1                                        ;148.4
        test      edx, edx                                      ;148.4
        jle       .B1.121       ; Prob 2%                       ;148.4
                                ; LOE eax edx
.B1.48:                         ; Preds .B1.47
        movss     xmm1, DWORD PTR [_2il0floatpacket.8]          ;203.53
        mov       ebx, 1                                        ;
        movss     xmm0, DWORD PTR [_2il0floatpacket.9]          ;237.103
        mov       DWORD PTR [188+esp], edx                      ;237.103
                                ; LOE eax ebx
.B1.49:                         ; Preds .B1.87 .B1.48
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;150.1
        neg       edx                                           ;150.42
        add       edx, ebx                                      ;150.42
        imul      esi, edx, 152                                 ;150.42
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;150.1
        movsx     edi, WORD PTR [148+ecx+esi]                   ;150.4
        cmp       edi, 99                                       ;150.42
        je        .B1.87        ; Prob 16%                      ;150.42
                                ; LOE eax ebx
.B1.50:                         ; Preds .B1.49
        imul      esi, ebx, 900                                 ;152.4
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;152.4
        imul      edx, edi, -900                                ;152.4
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;152.4
        mov       DWORD PTR [208+esp], eax                      ;148.4
        lea       eax, DWORD PTR [ecx+esi]                      ;152.4
        mov       DWORD PTR [636+edx+eax], 0                    ;152.4
        mov       edx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TRIPCHAINLIST+32] ;155.4
        neg       edx                                           ;155.28
        add       edx, ebx                                      ;155.28
        lea       eax, DWORD PTR [edx+edx*4]                    ;155.28
        mov       edx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TRIPCHAINLIST] ;155.4
        movsx     eax, WORD PTR [38+edx+eax*8]                  ;155.7
        test      eax, eax                                      ;155.28
        jle       .B1.68        ; Prob 16%                      ;155.28
                                ; LOE eax edx ecx ebx esi edi
.B1.51:                         ; Preds .B1.50
        mov       DWORD PTR [240+esp], eax                      ;
        mov       esi, 1                                        ;
                                ; LOE edx ebx esi
.B1.52:                         ; Preds .B1.441 .B1.51
        mov       eax, ebx                                      ;157.10
        mov       edi, esi                                      ;157.10
        sub       eax, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TRIPCHAINLIST+32] ;157.10
        movss     xmm0, DWORD PTR [_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT] ;158.13
        lea       ecx, DWORD PTR [eax+eax*4]                    ;157.10
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;158.10
        sub       edi, DWORD PTR [32+edx+ecx*8]                 ;157.10
        imul      edi, DWORD PTR [28+edx+ecx*8]                 ;157.10
        mov       edx, DWORD PTR [edx+ecx*8]                    ;157.12
        mov       ecx, DWORD PTR [edx+edi]                      ;157.10
        mov       edi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;158.10
        neg       edi                                           ;158.41
        add       edi, ecx                                      ;158.41
        shl       edi, 8                                        ;158.41
        mov       DWORD PTR [236+esp], ecx                      ;157.10
        movss     xmm1, DWORD PTR [32+eax+edi]                  ;158.13
        comiss    xmm0, xmm1                                    ;158.41
        jbe       .B1.65        ; Prob 50%                      ;158.41
                                ; LOE eax ecx ebx esi edi xmm0 xmm1
.B1.53:                         ; Preds .B1.52
        movzx     edx, BYTE PTR [40+eax+edi]                    ;160.39
        inc       edx                                           ;160.65
        mov       DWORD PTR [32+eax+edi], 0                     ;159.13
        mov       BYTE PTR [40+eax+edi], dl                     ;160.13
        mov       edi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE+32] ;161.13
        neg       edi                                           ;161.13
        add       edi, ecx                                      ;161.13
        shl       edi, 6                                        ;161.13
        mov       ebx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE] ;161.13
        movsx     eax, dl                                       ;161.42
        sub       eax, DWORD PTR [48+ebx+edi]                   ;161.13
        mov       edx, DWORD PTR [16+ebx+edi]                   ;161.42
        mov       edi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;161.42
        neg       edi                                           ;161.13
        add       edi, ecx                                      ;161.13
        shl       edi, 5                                        ;161.13
        mov       ebx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;161.42
        movzx     ecx, WORD PTR [edx+eax*2]                     ;161.13
        lea       eax, DWORD PTR [236+esp]                      ;162.9
        mov       WORD PTR [20+ebx+edi], cx                     ;161.13
        lea       edi, DWORD PTR [208+esp]                      ;162.9
        push      eax                                           ;162.9
        push      edi                                           ;162.9
        call      _DYNUST_LINK_VEH_LIST_MODULE_mp_TRIPCHAIN_REMOVE ;162.9
                                ; LOE esi edi
.B1.54:                         ; Preds .B1.53
        lea       eax, DWORD PTR [244+esp]                      ;163.9
        push      eax                                           ;163.9
        push      edi                                           ;163.9
        call      _DYNUST_LINK_VEH_LIST_MODULE_mp_ENTRYQUEUE_INSFRONT ;163.9
                                ; LOE esi edi
.B1.413:                        ; Preds .B1.54
        add       esp, 16                                       ;163.9
                                ; LOE esi edi
.B1.55:                         ; Preds .B1.413
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;164.4
        neg       eax                                           ;164.4
        mov       ebx, DWORD PTR [208+esp]                      ;164.42
        add       eax, ebx                                      ;164.4
        imul      ecx, eax, 900                                 ;164.4
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;164.4
        mov       eax, DWORD PTR [236+esp]                      ;165.4
        inc       WORD PTR [692+edx+ecx]                        ;164.79
        mov       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;165.4
        neg       ecx                                           ;165.4
        add       ecx, eax                                      ;165.4
        shl       ecx, 8                                        ;165.4
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;165.4
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ITERATION], 0 ;167.26
        mov       DWORD PTR [240+edx+ecx], 0                    ;165.4
        jne       .B1.63        ; Prob 50%                      ;167.26
                                ; LOE eax ebx esi edi
.B1.56:                         ; Preds .B1.55 .B1.64
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_RUNMODE] ;168.15
        test      eax, -2                                       ;168.30
        jne       .B1.62        ; Prob 40%                      ;168.30
                                ; LOE eax ebx esi edi
.B1.57:                         ; Preds .B1.56 .B1.62
        xor       eax, eax                                      ;169.18
        push      eax                                           ;169.18
        push      eax                                           ;169.18
        push      36                                            ;169.18
        push      OFFSET FLAT: __STRLITPACK_8                   ;169.18
        push      eax                                           ;169.18
        push      80                                            ;169.18
        push      OFFSET FLAT: _SIM_LOAD_VEHICLES$PRINTSTR1.0.1 ;169.18
        call      _for_cpystr                                   ;169.18
                                ; LOE esi edi
.B1.58:                         ; Preds .B1.57
        push      80                                            ;170.23
        mov       eax, OFFSET FLAT: __NLITPACK_6.0.1            ;170.23
        push      eax                                           ;170.23
        push      eax                                           ;170.23
        push      eax                                           ;170.23
        push      eax                                           ;170.23
        push      eax                                           ;170.23
        push      OFFSET FLAT: __NLITPACK_5.0.1                 ;170.23
        push      OFFSET FLAT: __NLITPACK_4.0.1                 ;170.23
        push      OFFSET FLAT: _SIM_LOAD_VEHICLES$PRINTSTR1.0.1 ;170.23
        call      _PRINTSCREEN                                  ;170.23
                                ; LOE esi edi
.B1.59:                         ; Preds .B1.58
        mov       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE+32] ;171.23
        neg       ecx                                           ;171.23
        mov       edx, DWORD PTR [300+esp]                      ;171.58
        add       ecx, edx                                      ;171.23
        shl       ecx, 6                                        ;171.23
        mov       ebx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE] ;171.23
        sub       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;171.23
        shl       edx, 5                                        ;171.23
        lea       eax, DWORD PTR [12+ecx+ebx]                   ;171.23
        mov       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;171.23
        lea       ebx, DWORD PTR [20+edx+ecx]                   ;171.23
        lea       edx, DWORD PTR [228+esp]                      ;171.23
        push      edx                                           ;171.23
        push      OFFSET FLAT: __NLITPACK_2.0.1                 ;171.23
        push      ebx                                           ;171.23
        push      eax                                           ;171.23
        push      OFFSET FLAT: _DYNUST_MAIN_MODULE_mp_IPINIT    ;171.23
        push      edi                                           ;171.23
        lea       eax, DWORD PTR [324+esp]                      ;171.23
        push      eax                                           ;171.23
        call      _RETRIEVE_VEH_PATH_ASTAR                      ;171.23
                                ; LOE esi edi
.B1.60:                         ; Preds .B1.59
        mov       eax, DWORD PTR [328+esp]                      ;174.23
        lea       ecx, DWORD PTR [264+esp]                      ;174.23
        sub       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE+32] ;174.23
        lea       ebx, DWORD PTR [260+esp]                      ;174.23
        shl       eax, 6                                        ;174.23
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE] ;174.23
        push      ecx                                           ;174.23
        push      ebx                                           ;174.23
        lea       eax, DWORD PTR [12+eax+edx]                   ;174.23
        push      eax                                           ;174.23
        push      edi                                           ;174.23
        lea       edi, DWORD PTR [344+esp]                      ;174.23
        push      edi                                           ;174.23
        push      DWORD PTR [8+ebp]                             ;174.23
        call      _RETRIEVE_NEXT_LINK                           ;174.23
                                ; LOE esi
.B1.414:                        ; Preds .B1.60
        add       esp, 116                                      ;174.23
                                ; LOE esi
.B1.61:                         ; Preds .B1.414
        mov       ebx, DWORD PTR [208+esp]                      ;157.12
        jmp       .B1.66        ; Prob 100%                     ;157.12
                                ; LOE ebx esi
.B1.62:                         ; Preds .B1.56
        cmp       eax, 3                                        ;168.58
        je        .B1.57        ; Prob 5%                       ;168.58
        jmp       .B1.66        ; Prob 100%                     ;168.58
                                ; LOE ebx esi edi
.B1.63:                         ; Preds .B1.55
        jle       .B1.66        ; Prob 16%                      ;167.45
                                ; LOE eax ebx esi edi
.B1.64:                         ; Preds .B1.63
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;167.45
        neg       edx                                           ;167.30
        add       edx, eax                                      ;167.30
        shl       edx, 5                                        ;167.30
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;167.45
        cmp       BYTE PTR [4+eax+edx], 4                       ;167.85
        je        .B1.56        ; Prob 16%                      ;167.85
        jmp       .B1.66        ; Prob 100%                     ;167.85
                                ; LOE ebx esi edi
.B1.65:                         ; Preds .B1.52
        subss     xmm1, xmm0                                    ;181.13
        movss     DWORD PTR [32+eax+edi], xmm1                  ;181.13
                                ; LOE ebx esi
.B1.66:                         ; Preds .B1.62 .B1.64 .B1.63 .B1.61 .B1.65
                                ;      
        inc       esi                                           ;183.4
        cmp       esi, DWORD PTR [240+esp]                      ;183.4
        jg        .B1.67        ; Prob 18%                      ;183.4
                                ; LOE ebx esi
.B1.441:                        ; Preds .B1.66
        mov       edx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_TRIPCHAINLIST] ;155.4
        jmp       .B1.52        ; Prob 100%                     ;155.4
                                ; LOE edx ebx esi
.B1.67:                         ; Preds .B1.66
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;189.4
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;189.4
        imul      esi, ebx, 900                                 ;
                                ; LOE ecx ebx esi edi
.B1.68:                         ; Preds .B1.67 .B1.50
        imul      edi, edi, -900                                ;204.6
        movss     xmm5, DWORD PTR [_2il0floatpacket.10]         ;204.70
        pxor      xmm7, xmm7                                    ;204.21
        movss     xmm3, DWORD PTR [_2il0floatpacket.11]         ;204.70
        add       esi, ecx                                      ;204.6
        mov       ecx, 1                                        ;204.6
        movss     xmm0, DWORD PTR [432+edi+esi]                 ;204.70
        mulss     xmm0, DWORD PTR [_2il0floatpacket.8]          ;204.70
        andps     xmm5, xmm0                                    ;204.70
        pxor      xmm0, xmm5                                    ;204.70
        movaps    xmm2, xmm0                                    ;204.70
        movaps    xmm1, xmm0                                    ;204.70
        movsx     eax, WORD PTR [692+edi+esi]                   ;204.32
        cmpltss   xmm2, xmm3                                    ;204.70
        andps     xmm2, xmm3                                    ;204.70
        addss     xmm1, xmm2                                    ;204.70
        subss     xmm1, xmm2                                    ;204.70
        movss     xmm2, DWORD PTR [_2il0floatpacket.12]         ;204.70
        movaps    xmm4, xmm1                                    ;204.70
        subss     xmm4, xmm0                                    ;204.70
        movaps    xmm0, xmm2                                    ;204.70
        addss     xmm0, xmm2                                    ;204.70
        movaps    xmm6, xmm4                                    ;204.70
        cmpless   xmm4, DWORD PTR [_2il0floatpacket.13]         ;204.70
        cmpnless  xmm6, xmm2                                    ;204.70
        andps     xmm6, xmm0                                    ;204.70
        andps     xmm4, xmm0                                    ;204.70
        subss     xmm1, xmm6                                    ;204.70
        addss     xmm1, xmm4                                    ;204.70
        orps      xmm1, xmm5                                    ;204.70
        cvtsi2ss  xmm5, DWORD PTR [8+edi+esi]                   ;204.21
        cvtss2si  edx, xmm1                                     ;204.70
        mulss     xmm5, DWORD PTR [672+edi+esi]                 ;204.21
        cmp       eax, edx                                      ;204.6
        subss     xmm5, DWORD PTR [688+edi+esi]                 ;204.21
        cmovl     edx, eax                                      ;204.6
        maxss     xmm7, xmm5                                    ;204.21
        movss     xmm5, DWORD PTR [_2il0floatpacket.10]         ;204.21
        andps     xmm5, xmm7                                    ;204.21
        pxor      xmm7, xmm5                                    ;204.21
        movaps    xmm4, xmm7                                    ;204.21
        movaps    xmm6, xmm7                                    ;204.21
        cmpltss   xmm4, xmm3                                    ;204.21
        andps     xmm4, xmm3                                    ;204.21
        addss     xmm6, xmm4                                    ;204.21
        subss     xmm6, xmm4                                    ;204.21
        movaps    xmm4, xmm6                                    ;204.21
        subss     xmm4, xmm7                                    ;204.21
        movaps    xmm7, xmm4                                    ;204.21
        cmpless   xmm4, DWORD PTR [_2il0floatpacket.13]         ;204.21
        cmpnless  xmm7, xmm2                                    ;204.21
        andps     xmm7, xmm0                                    ;204.21
        andps     xmm4, xmm0                                    ;204.21
        subss     xmm6, xmm7                                    ;204.21
        addss     xmm6, xmm4                                    ;204.21
        orps      xmm6, xmm5                                    ;204.21
        cvtss2si  eax, xmm6                                     ;204.21
        cmp       edx, eax                                      ;204.6
        cmovl     eax, edx                                      ;204.6
        test      eax, eax                                      ;204.6
        cmovle    eax, ecx                                      ;204.6
        test      eax, eax                                      ;207.12
        jl        .B1.117       ; Prob 1%                       ;207.12
                                ; LOE eax ebx esi edi xmm2 xmm3
.B1.69:                         ; Preds .B1.68
        mov       DWORD PTR [336+edi+esi], 0                    ;212.4
        jle       .B1.87        ; Prob 16%                      ;216.12
                                ; LOE eax ebx xmm2 xmm3
.B1.70:                         ; Preds .B1.69
        mov       ecx, ebx                                      ;216.15
        sub       ecx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_ENTQUEVEHLIST+32] ;216.15
        imul      ecx, ecx, 44                                  ;216.15
        mov       edx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_ENTQUEVEHLIST] ;216.12
        mov       DWORD PTR [184+esp], edx                      ;216.12
        mov       edi, DWORD PTR [40+edx+ecx]                   ;216.20
        test      edi, edi                                      ;216.41
        jle       .B1.87        ; Prob 16%                      ;216.41
                                ; LOE eax edx ecx ebx edi dl dh xmm2 xmm3
.B1.71:                         ; Preds .B1.70
        mov       esi, DWORD PTR [8+ebp]                        ;220.86
        movss     xmm0, DWORD PTR [_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT] ;220.17
        movss     xmm4, DWORD PTR [_2il0floatpacket.10]         ;220.86
        movss     xmm7, DWORD PTR [esi]                         ;220.86
        divss     xmm7, xmm0                                    ;220.86
        andps     xmm4, xmm7                                    ;220.86
        pxor      xmm7, xmm4                                    ;220.86
        movaps    xmm6, xmm7                                    ;220.86
        movaps    xmm5, xmm7                                    ;220.86
        mov       esi, edx                                      ;220.12
        cmpltss   xmm6, xmm3                                    ;220.86
        andps     xmm6, xmm3                                    ;220.86
        mov       DWORD PTR [152+esp], ebx                      ;
        mov       ebx, 1                                        ;
        mov       DWORD PTR [156+esp], ebx                      ;
        addss     xmm5, xmm6                                    ;220.86
        mov       ebx, DWORD PTR [28+esi+ecx]                   ;220.12
        subss     xmm5, xmm6                                    ;220.86
        movaps    xmm1, xmm5                                    ;220.86
        mov       edx, DWORD PTR [32+esi+ecx]                   ;220.12
        subss     xmm1, xmm7                                    ;220.86
        imul      edx, ebx                                      ;
        movaps    xmm7, xmm2                                    ;220.86
        movaps    xmm6, xmm1                                    ;220.86
        mov       ecx, DWORD PTR [esi+ecx]                      ;220.12
        addss     xmm7, xmm2                                    ;220.86
        cmpnless  xmm6, xmm2                                    ;220.86
        cmpless   xmm1, DWORD PTR [_2il0floatpacket.13]         ;220.86
        andps     xmm6, xmm7                                    ;220.86
        andps     xmm1, xmm7                                    ;220.86
        mov       esi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;
        sub       ecx, edx                                      ;
        shl       esi, 5                                        ;
        subss     xmm5, xmm6                                    ;220.86
        mov       DWORD PTR [36+esp], ebx                       ;
        neg       esi                                           ;
        mov       DWORD PTR [192+esp], ebx                      ;220.12
        addss     xmm5, xmm1                                    ;220.86
        orps      xmm5, xmm4                                    ;220.86
        mov       DWORD PTR [200+esp], ecx                      ;
        mov       DWORD PTR [216+esp], eax                      ;220.86
        add       esi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;
        cvtss2si  edx, xmm5                                     ;220.86
        mov       ecx, DWORD PTR [156+esp]                      ;220.86
        mov       eax, DWORD PTR [36+esp]                       ;220.86
                                ; LOE eax edx ecx esi edi xmm0 xmm2 xmm3
.B1.72:                         ; Preds .B1.73 .B1.71
        mov       ebx, DWORD PTR [200+esp]                      ;220.12
        movss     xmm4, DWORD PTR [_2il0floatpacket.10]         ;220.12
        mov       ebx, DWORD PTR [eax+ebx]                      ;220.12
        shl       ebx, 5                                        ;220.12
        movss     xmm7, DWORD PTR [12+esi+ebx]                  ;220.12
        divss     xmm7, xmm0                                    ;220.12
        andps     xmm4, xmm7                                    ;220.12
        pxor      xmm7, xmm4                                    ;220.12
        movaps    xmm6, xmm7                                    ;220.12
        movaps    xmm5, xmm7                                    ;220.12
        cmpltss   xmm6, xmm3                                    ;220.12
        andps     xmm6, xmm3                                    ;220.12
        addss     xmm5, xmm6                                    ;220.12
        subss     xmm5, xmm6                                    ;220.12
        movaps    xmm1, xmm5                                    ;220.12
        subss     xmm1, xmm7                                    ;220.12
        movaps    xmm7, xmm2                                    ;220.12
        addss     xmm7, xmm2                                    ;220.12
        movaps    xmm6, xmm1                                    ;220.12
        cmpless   xmm1, DWORD PTR [_2il0floatpacket.13]         ;220.12
        cmpnless  xmm6, xmm2                                    ;220.12
        andps     xmm6, xmm7                                    ;220.12
        andps     xmm1, xmm7                                    ;220.12
        subss     xmm5, xmm6                                    ;220.12
        addss     xmm5, xmm1                                    ;220.12
        orps      xmm5, xmm4                                    ;220.12
        cvtss2si  ebx, xmm5                                     ;220.12
        cmp       ebx, edx                                      ;220.84
        jg        .B1.74        ; Prob 20%                      ;220.84
                                ; LOE eax edx ecx esi edi xmm0 xmm2 xmm3
.B1.73:                         ; Preds .B1.72
        inc       ecx                                           ;221.7
        add       eax, DWORD PTR [192+esp]                      ;221.7
        cmp       ecx, edi                                      ;221.7
        jle       .B1.72        ; Prob 82%                      ;221.7
                                ; LOE eax edx ecx esi edi xmm0 xmm2 xmm3
.B1.74:                         ; Preds .B1.72 .B1.73
        mov       ebx, DWORD PTR [152+esp]                      ;
        mov       eax, DWORD PTR [216+esp]                      ;
                                ; LOE eax ecx ebx edi al bl ah bh
.B1.75:                         ; Preds .B1.74
        dec       ecx                                           ;222.7
        mov       esi, 1                                        ;
        cmp       edi, ecx                                      ;224.7
        cmovl     ecx, edi                                      ;224.7
        cmp       ecx, eax                                      ;224.7
        cmovl     eax, ecx                                      ;224.7
        test      eax, eax                                      ;224.7
        jle       .B1.85        ; Prob 2%                       ;224.7
                                ; LOE eax ebx esi bl bh
.B1.76:                         ; Preds .B1.75
        mov       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;234.14
        mov       edi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;234.14
        shl       ecx, 8                                        ;
        mov       DWORD PTR [216+esp], eax                      ;
        mov       DWORD PTR [220+esp], ecx                      ;
        mov       DWORD PTR [224+esp], edi                      ;
        mov       DWORD PTR [228+esp], esi                      ;
        mov       eax, DWORD PTR [184+esp]                      ;
                                ; LOE eax ebx
.B1.77:                         ; Preds .B1.443 .B1.76
        sub       ebx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_ENTQUEVEHLIST+32] ;226.6
        imul      edx, ebx, 44                                  ;226.6
        mov       ecx, DWORD PTR [228+esp]                      ;226.6
        sub       ecx, DWORD PTR [32+eax+edx]                   ;226.6
        imul      ecx, DWORD PTR [28+eax+edx]                   ;226.6
        mov       eax, DWORD PTR [eax+edx]                      ;226.9
        mov       ebx, DWORD PTR [eax+ecx]                      ;226.6
        test      ebx, ebx                                      ;228.9
        mov       DWORD PTR [212+esp], ebx                      ;226.6
        jle       .B1.113       ; Prob 1%                       ;228.9
                                ; LOE ebx
.B1.78:                         ; Preds .B1.420 .B1.77
        shl       ebx, 8                                        ;234.14
        mov       eax, DWORD PTR [224+esp]                      ;234.14
        add       eax, ebx                                      ;234.14
        mov       ebx, DWORD PTR [220+esp]                      ;234.14
        add       ebx, -240                                     ;234.14
        sub       eax, ebx                                      ;234.14
        push      eax                                           ;234.14
        lea       edx, DWORD PTR [216+esp]                      ;234.14
        push      edx                                           ;234.14
        lea       ecx, DWORD PTR [216+esp]                      ;234.14
        push      ecx                                           ;234.14
        call      _DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST_INSERT ;234.14
                                ; LOE
.B1.415:                        ; Preds .B1.78
        add       esp, 12                                       ;234.14
                                ; LOE
.B1.79:                         ; Preds .B1.415
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;235.9
        neg       esi                                           ;235.9
        mov       ebx, DWORD PTR [208+esp]                      ;235.41
        add       esi, ebx                                      ;235.9
        mov       edi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;237.45
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;237.45
        shl       edi, 8                                        ;237.9
        imul      eax, esi, 900                                 ;235.9
        mov       esi, DWORD PTR [212+esp]                      ;237.81
        mov       DWORD PTR [224+esp], edx                      ;237.45
        sub       edx, edi                                      ;237.9
        mov       DWORD PTR [220+esp], edi                      ;237.9
        mov       edi, esi                                      ;237.9
        shl       edi, 8                                        ;237.9
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;235.9
        mov       DWORD PTR [232+esp], eax                      ;235.9
        movss     xmm0, DWORD PTR [156+edi+edx]                 ;237.81
        divss     xmm0, DWORD PTR [_2il0floatpacket.9]          ;237.103
        inc       DWORD PTR [640+ecx+eax]                       ;235.9
        inc       DWORD PTR [396+ecx+eax]                       ;236.3
        addss     xmm0, DWORD PTR [688+ecx+eax]                 ;237.9
        movss     DWORD PTR [688+ecx+eax], xmm0                 ;237.9
        mov       eax, DWORD PTR [8+ebp]                        ;238.9
        movss     xmm0, DWORD PTR [eax]                         ;238.9
        mov       eax, DWORD PTR [eax]                          ;238.9
        mov       DWORD PTR [28+edi+edx], eax                   ;238.9
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;240.9
        neg       eax                                           ;240.88
        add       eax, esi                                      ;240.88
        shl       eax, 5                                        ;240.88
        mov       esi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;240.9
        mov       edi, DWORD PTR [232+esp]                      ;240.44
        movsx     edx, BYTE PTR [5+esi+eax]                     ;240.12
        cmp       edx, 2                                        ;240.44
        je        .B1.82        ; Prob 16%                      ;240.44
                                ; LOE eax edx ecx ebx esi edi xmm0
.B1.80:                         ; Preds .B1.79
        cmp       edx, 5                                        ;240.84
        je        .B1.82        ; Prob 16%                      ;240.84
                                ; LOE eax edx ecx ebx esi edi xmm0
.B1.81:                         ; Preds .B1.80
        cmp       edx, 7                                        ;240.124
        jne       .B1.83        ; Prob 84%                      ;240.124
                                ; LOE eax ecx ebx esi edi xmm0
.B1.82:                         ; Preds .B1.79 .B1.80 .B1.81
        inc       DWORD PTR [644+ecx+edi]                       ;241.5
                                ; LOE eax ecx ebx esi edi xmm0
.B1.83:                         ; Preds .B1.81 .B1.82
        subss     xmm0, DWORD PTR [12+esi+eax]                  ;244.97
        mov       eax, DWORD PTR [228+esp]                      ;245.6
        inc       eax                                           ;245.6
        addss     xmm0, DWORD PTR [336+ecx+edi]                 ;244.9
        movss     DWORD PTR [336+ecx+edi], xmm0                 ;244.9
        mov       DWORD PTR [228+esp], eax                      ;245.6
        cmp       eax, DWORD PTR [216+esp]                      ;245.6
        jg        .B1.84        ; Prob 18%                      ;245.6
                                ; LOE eax ebx al ah
.B1.443:                        ; Preds .B1.83
        mov       eax, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_ENTQUEVEHLIST] ;216.12
        jmp       .B1.77        ; Prob 100%                     ;216.12
                                ; LOE eax ebx
.B1.84:                         ; Preds .B1.83
        mov       esi, eax                                      ;
                                ; LOE esi
.B1.85:                         ; Preds .B1.75 .B1.84
        lea       eax, DWORD PTR [160+esp]                      ;247.8
        lea       edi, DWORD PTR [-1+esi]                       ;247.8
        mov       DWORD PTR [160+esp], edi                      ;247.8
        push      eax                                           ;247.8
        lea       edx, DWORD PTR [212+esp]                      ;247.8
        push      edx                                           ;247.8
        call      _DYNUST_LINK_VEH_LIST_MODULE_mp_ENTRYQUEUE_REMOVE ;247.8
                                ; LOE esi edi
.B1.416:                        ; Preds .B1.85
        add       esp, 8                                        ;247.8
                                ; LOE esi edi
.B1.86:                         ; Preds .B1.416
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;248.3
        neg       edx                                           ;248.3
        mov       ebx, DWORD PTR [208+esp]                      ;248.46
        add       edx, ebx                                      ;248.3
        imul      ecx, edx, 900                                 ;248.3
        cvtsi2ss  xmm0, edi                                     ;248.90
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;248.3
        movss     xmm1, DWORD PTR [336+eax+ecx]                 ;248.46
        divss     xmm1, xmm0                                    ;248.3
        movzx     edi, WORD PTR [692+eax+ecx]                   ;249.44
        sub       edi, esi                                      ;249.44
        inc       edi                                           ;249.44
        movss     DWORD PTR [336+eax+ecx], xmm1                 ;248.3
        mov       WORD PTR [692+eax+ecx], di                    ;249.6
                                ; LOE ebx
.B1.87:                         ; Preds .B1.120 .B1.86 .B1.70 .B1.69 .B1.49
                                ;      
        inc       ebx                                           ;148.4
        mov       eax, ebx                                      ;148.4
        cmp       ebx, DWORD PTR [188+esp]                      ;148.4
        jle       .B1.49        ; Prob 82%                      ;148.4
                                ; LOE eax ebx
.B1.88:                         ; Preds .B1.87
        mov       DWORD PTR [208+esp], ebx                      ;148.4
                                ; LOE
.B1.89:                         ; Preds .B1.88 .B1.121
        mov       ebx, DWORD PTR [76+esp]                       ;263.1
        test      bl, 1                                         ;263.4
        je        .B1.92        ; Prob 60%                      ;263.4
                                ; LOE ebx
.B1.90:                         ; Preds .B1.89
        mov       edx, ebx                                      ;263.23
        mov       eax, ebx                                      ;263.23
        shr       edx, 1                                        ;263.23
        and       eax, 1                                        ;263.23
        and       edx, 1                                        ;263.23
        add       eax, eax                                      ;263.23
        shl       edx, 2                                        ;263.23
        or        edx, eax                                      ;263.23
        or        edx, 262144                                   ;263.23
        push      edx                                           ;263.23
        push      DWORD PTR [68+esp]                            ;263.23
        call      _for_dealloc_allocatable                      ;263.23
                                ; LOE ebx
.B1.417:                        ; Preds .B1.90
        add       esp, 8                                        ;263.23
                                ; LOE ebx
.B1.91:                         ; Preds .B1.417
        and       ebx, -2                                       ;263.23
        mov       DWORD PTR [64+esp], 0                         ;263.23
        mov       DWORD PTR [76+esp], ebx                       ;263.23
                                ; LOE ebx
.B1.92:                         ; Preds .B1.91 .B1.89
        test      bl, 1                                         ;266.1
        jne       .B1.111       ; Prob 3%                       ;266.1
                                ; LOE ebx
.B1.93:                         ; Preds .B1.92
        add       esp, 244                                      ;266.1
        pop       ebx                                           ;266.1
        pop       edi                                           ;266.1
        pop       esi                                           ;266.1
        mov       esp, ebp                                      ;266.1
        pop       ebp                                           ;266.1
        ret                                                     ;266.1
                                ; LOE
.B1.94:                         ; Preds .B1.13                  ; Infreq
        cmp       edi, 4                                        ;114.4
        jl        .B1.107       ; Prob 10%                      ;114.4
                                ; LOE ebx edi
.B1.95:                         ; Preds .B1.94                  ; Infreq
        lea       ecx, DWORD PTR [_SIM_LOAD_VEHICLES$PATHIN.0.1+8+ebx*4] ;114.4
        and       ecx, 15                                       ;114.4
        mov       eax, ecx                                      ;114.4
        neg       eax                                           ;114.4
        add       eax, 16                                       ;114.4
        shr       eax, 2                                        ;114.4
        test      ecx, ecx                                      ;114.4
        cmovne    ecx, eax                                      ;114.4
        lea       edx, DWORD PTR [4+ecx]                        ;114.4
        cmp       edi, edx                                      ;114.4
        jl        .B1.107       ; Prob 10%                      ;114.4
                                ; LOE ecx ebx edi
.B1.96:                         ; Preds .B1.95                  ; Infreq
        mov       eax, edi                                      ;114.4
        lea       edx, DWORD PTR [ebx*4]                        ;
        sub       eax, ecx                                      ;114.4
        and       eax, 3                                        ;114.4
        neg       eax                                           ;114.4
        add       eax, edi                                      ;114.4
        test      ecx, ecx                                      ;114.4
        jbe       .B1.100       ; Prob 10%                      ;114.4
                                ; LOE eax edx ecx ebx edi
.B1.97:                         ; Preds .B1.96                  ; Infreq
        xor       esi, esi                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B1.98:                         ; Preds .B1.98 .B1.97           ; Infreq
        mov       DWORD PTR [_SIM_LOAD_VEHICLES$PATHIN.0.1+8+edx+esi*4], 0 ;114.4
        inc       esi                                           ;114.4
        cmp       esi, ecx                                      ;114.4
        jb        .B1.98        ; Prob 82%                      ;114.4
                                ; LOE eax edx ecx ebx esi edi
.B1.100:                        ; Preds .B1.98 .B1.96           ; Infreq
        pxor      xmm0, xmm0                                    ;114.4
                                ; LOE eax edx ecx ebx edi xmm0
.B1.101:                        ; Preds .B1.101 .B1.100         ; Infreq
        movdqa    XMMWORD PTR [_SIM_LOAD_VEHICLES$PATHIN.0.1+8+edx+ecx*4], xmm0 ;114.4
        add       ecx, 4                                        ;114.4
        cmp       ecx, eax                                      ;114.4
        jb        .B1.101       ; Prob 82%                      ;114.4
                                ; LOE eax edx ecx ebx edi xmm0
.B1.103:                        ; Preds .B1.101 .B1.107         ; Infreq
        cmp       eax, edi                                      ;114.4
        jae       .B1.15        ; Prob 10%                      ;114.4
                                ; LOE eax ebx edi
.B1.104:                        ; Preds .B1.103                 ; Infreq
        shl       ebx, 2                                        ;
                                ; LOE eax ebx edi
.B1.105:                        ; Preds .B1.105 .B1.104         ; Infreq
        mov       DWORD PTR [_SIM_LOAD_VEHICLES$PATHIN.0.1+8+ebx+eax*4], 0 ;114.4
        inc       eax                                           ;114.4
        cmp       eax, edi                                      ;114.4
        jb        .B1.105       ; Prob 82%                      ;114.4
        jmp       .B1.15        ; Prob 100%                     ;114.4
                                ; LOE eax ebx edi
.B1.107:                        ; Preds .B1.94 .B1.95           ; Infreq
        xor       eax, eax                                      ;114.4
        jmp       .B1.103       ; Prob 100%                     ;114.4
                                ; LOE eax ebx edi
.B1.109:                        ; Preds .B1.41 .B1.42           ; Infreq
        push      OFFSET FLAT: _DYNUST_MAIN_MODULE_mp_JUSTVEH   ;99.13
        call      _DYNUST_FUEL_MODULE_mp_SETFUELLEVEL           ;99.13
                                ; LOE
.B1.418:                        ; Preds .B1.109                 ; Infreq
        add       esp, 4                                        ;99.13
                                ; LOE
.B1.110:                        ; Preds .B1.418                 ; Infreq
        mov       edx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_MOP] ;102.13
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_JUSTVEH] ;103.6
        mov       DWORD PTR [8+esp], edx                        ;102.13
        jmp       .B1.44        ; Prob 100%                     ;102.13
                                ; LOE eax
.B1.111:                        ; Preds .B1.92                  ; Infreq
        mov       edx, ebx                                      ;266.1
        mov       eax, ebx                                      ;266.1
        shr       edx, 1                                        ;266.1
        and       eax, 1                                        ;266.1
        and       edx, 1                                        ;266.1
        add       eax, eax                                      ;266.1
        shl       edx, 2                                        ;266.1
        or        edx, eax                                      ;266.1
        or        edx, 262144                                   ;266.1
        push      edx                                           ;266.1
        push      DWORD PTR [68+esp]                            ;266.1
        call      _for_dealloc_allocatable                      ;266.1
                                ; LOE ebx
.B1.419:                        ; Preds .B1.111                 ; Infreq
        add       esp, 8                                        ;266.1
                                ; LOE ebx
.B1.112:                        ; Preds .B1.419                 ; Infreq
        and       ebx, -2                                       ;266.1
        mov       DWORD PTR [64+esp], 0                         ;266.1
        mov       DWORD PTR [76+esp], ebx                       ;266.1
        add       esp, 244                                      ;266.1
        pop       ebx                                           ;266.1
        pop       edi                                           ;266.1
        pop       esi                                           ;266.1
        mov       esp, ebp                                      ;266.1
        pop       ebp                                           ;266.1
        ret                                                     ;266.1
                                ; LOE
.B1.113:                        ; Preds .B1.77                  ; Infreq
        mov       DWORD PTR [112+esp], 0                        ;229.5
        lea       esi, DWORD PTR [112+esp]                      ;229.5
        mov       DWORD PTR [40+esp], 19                        ;229.5
        lea       eax, DWORD PTR [40+esp]                       ;229.5
        mov       DWORD PTR [44+esp], OFFSET FLAT: __STRLITPACK_4 ;229.5
        push      32                                            ;229.5
        push      eax                                           ;229.5
        push      OFFSET FLAT: __STRLITPACK_23.0.1              ;229.5
        push      -2088435968                                   ;229.5
        push      911                                           ;229.5
        push      esi                                           ;229.5
        call      _for_write_seq_lis                            ;229.5
                                ; LOE ebx esi
.B1.114:                        ; Preds .B1.113                 ; Infreq
        mov       DWORD PTR [136+esp], 0                        ;230.5
        lea       eax, DWORD PTR [72+esp]                       ;230.5
        mov       DWORD PTR [72+esp], 47                        ;230.5
        mov       DWORD PTR [76+esp], OFFSET FLAT: __STRLITPACK_2 ;230.5
        push      32                                            ;230.5
        push      eax                                           ;230.5
        push      OFFSET FLAT: __STRLITPACK_24.0.1              ;230.5
        push      -2088435968                                   ;230.5
        push      911                                           ;230.5
        push      esi                                           ;230.5
        call      _for_write_seq_lis                            ;230.5
                                ; LOE ebx
.B1.115:                        ; Preds .B1.114                 ; Infreq
        push      32                                            ;231.5
        xor       eax, eax                                      ;231.5
        push      eax                                           ;231.5
        push      eax                                           ;231.5
        push      -2088435968                                   ;231.5
        push      eax                                           ;231.5
        push      OFFSET FLAT: __STRLITPACK_25                  ;231.5
        call      _for_stop_core                                ;231.5
                                ; LOE ebx
.B1.420:                        ; Preds .B1.115                 ; Infreq
        add       esp, 72                                       ;231.5
        jmp       .B1.78        ; Prob 100%                     ;231.5
                                ; LOE ebx
.B1.117:                        ; Preds .B1.68                  ; Infreq
        mov       DWORD PTR [152+esp], ebx                      ;
        sub       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;208.7
        imul      ecx, ebx, 152                                 ;208.7
        imul      ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE+32], 44 ;208.7
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;208.7
        mov       DWORD PTR [20+esp], eax                       ;208.7
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE] ;208.7
        imul      eax, DWORD PTR [24+eax+ecx], 44               ;208.76
        add       eax, edx                                      ;208.7
        sub       eax, ebx                                      ;208.7
        mov       DWORD PTR [12+esp], edx                       ;208.7
        mov       DWORD PTR [112+esp], 0                        ;208.7
        mov       DWORD PTR [16+esp], ecx                       ;208.7
        lea       ecx, DWORD PTR [24+esp]                       ;208.7
        mov       edx, DWORD PTR [36+eax]                       ;208.7
        mov       DWORD PTR [8+esp], ebx                        ;208.7
        lea       ebx, DWORD PTR [112+esp]                      ;208.7
        mov       DWORD PTR [24+esp], edx                       ;208.7
        push      32                                            ;208.7
        push      OFFSET FLAT: SIM_LOAD_VEHICLES$format_pack.0.1+32 ;208.7
        push      ecx                                           ;208.7
        push      OFFSET FLAT: __STRLITPACK_20.0.1              ;208.7
        push      -2088435968                                   ;208.7
        push      911                                           ;208.7
        push      ebx                                           ;208.7
        mov       ebx, DWORD PTR [180+esp]                      ;208.7
        call      _for_write_seq_fmt                            ;208.7
                                ; LOE ebx esi edi bl bh
.B1.118:                        ; Preds .B1.117                 ; Infreq
        mov       edx, DWORD PTR [44+esp]                       ;208.152
        mov       eax, DWORD PTR [48+esp]                       ;208.152
        imul      ecx, DWORD PTR [28+eax+edx], 44               ;208.152
        lea       edx, DWORD PTR [60+esp]                       ;208.7
        add       ecx, DWORD PTR [40+esp]                       ;208.7
        sub       ecx, DWORD PTR [36+esp]                       ;208.7
        mov       eax, DWORD PTR [36+ecx]                       ;208.7
        mov       DWORD PTR [60+esp], eax                       ;208.7
        push      edx                                           ;208.7
        push      OFFSET FLAT: __STRLITPACK_21.0.1              ;208.7
        lea       ecx, DWORD PTR [148+esp]                      ;208.7
        push      ecx                                           ;208.7
        call      _for_write_seq_fmt_xmit                       ;208.7
                                ; LOE ebx esi edi bl bh
.B1.119:                        ; Preds .B1.118                 ; Infreq
        push      32                                            ;209.7
        xor       eax, eax                                      ;209.7
        push      eax                                           ;209.7
        push      eax                                           ;209.7
        push      -2088435968                                   ;209.7
        push      eax                                           ;209.7
        push      OFFSET FLAT: __STRLITPACK_22                  ;209.7
        call      _for_stop_core                                ;209.7
                                ; LOE ebx esi edi bl bh
.B1.421:                        ; Preds .B1.119                 ; Infreq
        add       esp, 64                                       ;209.7
                                ; LOE ebx esi edi bl bh
.B1.120:                        ; Preds .B1.421                 ; Infreq
        mov       DWORD PTR [336+edi+esi], 0                    ;212.4
        jmp       .B1.87        ; Prob 100%                     ;212.4
                                ; LOE ebx
.B1.121:                        ; Preds .B1.47                  ; Infreq
        mov       DWORD PTR [208+esp], 1                        ;148.4
        jmp       .B1.89        ; Prob 100%                     ;148.4
                                ; LOE
.B1.122:                        ; Preds .B1.11                  ; Infreq
        mov       DWORD PTR [112+esp], 0                        ;110.7
        lea       edx, DWORD PTR [112+esp]                      ;110.7
        mov       DWORD PTR [esp], 71                           ;110.7
        lea       eax, DWORD PTR [esp]                          ;110.7
        mov       DWORD PTR [4+esp], OFFSET FLAT: __STRLITPACK_9 ;110.7
        push      32                                            ;110.7
        push      eax                                           ;110.7
        push      OFFSET FLAT: __STRLITPACK_18.0.1              ;110.7
        push      -2088435968                                   ;110.7
        push      911                                           ;110.7
        push      edx                                           ;110.7
        call      _for_write_seq_lis                            ;110.7
                                ; LOE ebx esi
.B1.123:                        ; Preds .B1.122                 ; Infreq
        push      32                                            ;111.7
        xor       eax, eax                                      ;111.7
        push      eax                                           ;111.7
        push      eax                                           ;111.7
        push      -2088435968                                   ;111.7
        push      eax                                           ;111.7
        push      OFFSET FLAT: __STRLITPACK_19                  ;111.7
        call      _for_stop_core                                ;111.7
                                ; LOE ebx esi
.B1.422:                        ; Preds .B1.123                 ; Infreq
        add       esp, 48                                       ;111.7
        jmp       .B1.12        ; Prob 100%                     ;111.7
                                ; LOE ebx esi
.B1.124:                        ; Preds .B1.4 .B1.3             ; Infreq
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ITERATION] ;53.33
        test      eax, eax                                      ;53.65
        jne       .B1.6         ; Prob 50%                      ;53.65
                                ; LOE eax xmm5
.B1.125:                        ; Preds .B1.124                 ; Infreq
        movss     xmm4, DWORD PTR [_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT] ;55.10
        movaps    xmm7, xmm5                                    ;55.10
        divss     xmm7, xmm4                                    ;55.10
        movss     xmm3, DWORD PTR [_2il0floatpacket.10]         ;55.10
        andps     xmm3, xmm7                                    ;55.10
        pxor      xmm7, xmm3                                    ;55.10
        movss     xmm1, DWORD PTR [_2il0floatpacket.11]         ;55.10
        movaps    xmm6, xmm7                                    ;55.10
        movaps    xmm2, xmm7                                    ;55.10
        cmpltss   xmm6, xmm1                                    ;55.10
        andps     xmm6, xmm1                                    ;55.10
        movss     xmm1, DWORD PTR [_2il0floatpacket.12]         ;55.10
        addss     xmm2, xmm6                                    ;55.10
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TDSPSTEP] ;55.10
        subss     xmm2, xmm6                                    ;55.10
        movaps    xmm0, xmm2                                    ;55.10
        subss     xmm0, xmm7                                    ;55.10
        movaps    xmm7, xmm1                                    ;55.10
        addss     xmm7, xmm1                                    ;55.10
        movaps    xmm6, xmm0                                    ;55.10
        cmpless   xmm0, DWORD PTR [_2il0floatpacket.13]         ;55.10
        cmpnless  xmm6, xmm1                                    ;55.10
        andps     xmm6, xmm7                                    ;55.10
        andps     xmm0, xmm7                                    ;55.10
        subss     xmm2, xmm6                                    ;55.10
        addss     xmm2, xmm0                                    ;55.10
        orps      xmm2, xmm3                                    ;55.10
        cvtss2si  eax, xmm2                                     ;55.10
        cdq                                                     ;55.6
        idiv      ecx                                           ;55.6
        test      edx, edx                                      ;55.42
        je        .B1.136       ; Prob 5%                       ;55.42
                                ; LOE xmm1 xmm4 xmm5
.B1.126:                        ; Preds .B1.125                 ; Infreq
        mov       edi, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHAIN+44] ;74.11
        mov       esi, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHAIN+40] ;74.11
        mov       ebx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHAIN+32] ;74.1
        mov       edx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHAIN] ;74.1
        imul      edi, esi                                      ;
        shl       ebx, 2                                        ;
                                ; LOE edx ebx esi edi xmm1 xmm4 xmm5
.B1.127:                        ; Preds .B1.170 .B1.126         ; Infreq
        divss     xmm5, xmm4                                    ;74.30
        movss     xmm0, DWORD PTR [_2il0floatpacket.10]         ;74.30
        movaps    xmm4, xmm1                                    ;74.30
        andps     xmm0, xmm5                                    ;74.30
        add       esi, esi                                      ;74.11
        pxor      xmm5, xmm0                                    ;74.30
        sub       edi, esi                                      ;74.1
        movss     xmm2, DWORD PTR [_2il0floatpacket.11]         ;74.30
        movaps    xmm3, xmm5                                    ;74.30
        movaps    xmm7, xmm5                                    ;74.30
        cmpltss   xmm3, xmm2                                    ;74.30
        addss     xmm4, xmm1                                    ;74.30
        andps     xmm2, xmm3                                    ;74.30
        mov       eax, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_MOP] ;74.11
        addss     xmm7, xmm2                                    ;74.30
        subss     xmm7, xmm2                                    ;74.30
        movaps    xmm6, xmm7                                    ;74.30
        lea       edx, DWORD PTR [edx+eax*4]                    ;74.1
        sub       edx, edi                                      ;74.1
        subss     xmm6, xmm5                                    ;74.30
        movaps    xmm5, xmm6                                    ;74.30
        sub       edx, ebx                                      ;74.1
        cmpnless  xmm5, xmm1                                    ;74.30
        cmpless   xmm6, DWORD PTR [_2il0floatpacket.13]         ;74.30
        andps     xmm5, xmm4                                    ;74.30
        andps     xmm6, xmm4                                    ;74.30
        mov       ecx, DWORD PTR [edx]                          ;74.11
        subss     xmm7, xmm5                                    ;74.30
        dec       ecx                                           ;74.24
        addss     xmm7, xmm6                                    ;74.30
        orps      xmm7, xmm0                                    ;74.30
        cvtss2si  ebx, xmm7                                     ;74.30
        cmp       ecx, ebx                                      ;74.27
        jne       .B1.47        ; Prob 10%                      ;74.27
                                ; LOE eax
.B1.128:                        ; Preds .B1.127                 ; Infreq
        mov       edx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHAIN+44] ;76.31
        mov       ebx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHAIN+40] ;76.31
        imul      edx, ebx                                      ;
        mov       ecx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHAIN] ;76.23
        sub       ecx, edx                                      ;
        mov       edx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHAIN+32] ;76.23
        shl       edx, 2                                        ;
                                ; LOE eax edx ecx ebx
.B1.129:                        ; Preds .B1.128 .B1.134         ; Infreq
        add       ebx, eax                                      ;76.8
        sub       ecx, edx                                      ;76.8
        push      OFFSET FLAT: __NLITPACK_3.0.1                 ;76.8
        push      OFFSET FLAT: __NLITPACK_2.0.1                 ;76.8
        push      OFFSET FLAT: __NLITPACK_1.0.1                 ;76.8
        lea       eax, DWORD PTR [ecx+ebx*4]                    ;76.8
        push      eax                                           ;76.8
        push      eax                                           ;76.8
        push      OFFSET FLAT: _DYNUST_MAIN_MODULE_mp_JUSTVEH   ;76.8
        call      _WRITE_VEHICLES                               ;76.8
                                ; LOE
.B1.423:                        ; Preds .B1.129                 ; Infreq
        add       esp, 24                                       ;76.8
                                ; LOE
.B1.130:                        ; Preds .B1.423                 ; Infreq
        mov       eax, DWORD PTR [8+ebp]                        ;78.3
        movss     xmm1, DWORD PTR [eax]                         ;78.3
        comiss    xmm1, DWORD PTR [_DYNUST_MAIN_MODULE_mp_STARTTM] ;78.8
        jb        .B1.133       ; Prob 50%                      ;78.8
                                ; LOE xmm1
.B1.131:                        ; Preds .B1.130                 ; Infreq
        movss     xmm0, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ENDTM] ;78.8
        comiss    xmm0, xmm1                                    ;78.25
        jbe       .B1.133       ; Prob 50%                      ;78.25
                                ; LOE
.B1.132:                        ; Preds .B1.131                 ; Infreq
        inc       DWORD PTR [_DYNUST_MAIN_MODULE_mp_NUMCARS]    ;78.34
                                ; LOE
.B1.133:                        ; Preds .B1.130 .B1.131 .B1.132 ; Infreq
        mov       ebx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHAIN+40] ;80.26
        mov       edi, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHAIN+44] ;80.26
        imul      edi, ebx                                      ;80.8
        mov       esi, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_MOP] ;80.26
        mov       ecx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHAIN] ;80.8
        sub       ecx, edi                                      ;80.8
        lea       edi, DWORD PTR [ebx+ebx*2]                    ;80.8
        mov       eax, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHAIN+32] ;80.8
        lea       edx, DWORD PTR [esi*4]                        ;80.40
        shl       eax, 2                                        ;80.8
        lea       esi, DWORD PTR [edi+esi*4]                    ;80.8
        add       esi, ecx                                      ;80.8
        lea       edx, DWORD PTR [edx+ebx*4]                    ;80.8
        add       ecx, edx                                      ;80.8
        sub       esi, eax                                      ;80.8
        sub       ecx, eax                                      ;80.8
        push      ecx                                           ;80.8
        push      esi                                           ;80.8
        call      _DYNUST_LINK_VEH_LIST_MODULE_mp_ENTRYQUEUE_INSERT ;80.8
                                ; LOE
.B1.424:                        ; Preds .B1.133                 ; Infreq
        add       esp, 8                                        ;80.8
                                ; LOE
.B1.134:                        ; Preds .B1.424                 ; Infreq
        mov       ebx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHAIN+40] ;82.3
        mov       edx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHAIN+44] ;82.3
        imul      edx, ebx                                      ;82.3
        movss     xmm0, DWORD PTR [_2il0floatpacket.10]         ;74.30
        movss     xmm2, DWORD PTR [_2il0floatpacket.11]         ;74.30
        movss     xmm4, DWORD PTR [_2il0floatpacket.12]         ;74.30
        movaps    xmm6, xmm4                                    ;74.30
        mov       ecx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHAIN] ;82.3
        mov       edi, ecx                                      ;82.3
        mov       esi, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHAIN+32] ;82.3
        lea       eax, DWORD PTR [ebx+ebx*2]                    ;82.3
        shl       esi, 2                                        ;82.3
        sub       edi, edx                                      ;82.3
        sub       eax, esi                                      ;82.3
        addss     xmm6, xmm4                                    ;74.30
        add       eax, edi                                      ;82.3
        mov       DWORD PTR [esp], edi                          ;82.3
        mov       edi, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_MOP] ;82.53
        mov       DWORD PTR [4+esp], esi                        ;82.3
        mov       eax, DWORD PTR [eax+edi*4]                    ;82.53
        sub       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;82.3
        imul      esi, eax, 900                                 ;82.3
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;82.102
        inc       WORD PTR [692+eax+esi]                        ;82.102
        lea       eax, DWORD PTR [1+edi]                        ;83.3
        mov       esi, DWORD PTR [8+ebp]                        ;74.30
        mov       DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_MOP], eax ;83.3
        movss     xmm3, DWORD PTR [esi]                         ;74.30
        lea       esi, DWORD PTR [4+edi*4]                      ;74.1
        divss     xmm3, DWORD PTR [_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT] ;74.30
        andps     xmm0, xmm3                                    ;74.30
        add       esi, ecx                                      ;74.1
        pxor      xmm3, xmm0                                    ;74.30
        lea       ecx, DWORD PTR [ebx+ebx]                      ;74.11
        movaps    xmm1, xmm3                                    ;74.30
        sub       edx, ecx                                      ;74.1
        sub       esi, edx                                      ;74.1
        cmpltss   xmm1, xmm2                                    ;74.30
        andps     xmm2, xmm1                                    ;74.30
        movaps    xmm1, xmm3                                    ;74.30
        mov       edx, DWORD PTR [4+esp]                        ;74.1
        sub       esi, edx                                      ;74.1
        addss     xmm1, xmm2                                    ;74.30
        mov       esi, DWORD PTR [esi]                          ;74.11
        subss     xmm1, xmm2                                    ;74.30
        movaps    xmm7, xmm1                                    ;74.30
        dec       esi                                           ;74.24
        subss     xmm7, xmm3                                    ;74.30
        movaps    xmm5, xmm7                                    ;74.30
        cmpless   xmm7, DWORD PTR [_2il0floatpacket.13]         ;74.30
        cmpnless  xmm5, xmm4                                    ;74.30
        andps     xmm5, xmm6                                    ;74.30
        andps     xmm7, xmm6                                    ;74.30
        subss     xmm1, xmm5                                    ;74.30
        addss     xmm1, xmm7                                    ;74.30
        orps      xmm1, xmm0                                    ;74.30
        cvtss2si  ecx, xmm1                                     ;74.30
        cmp       esi, ecx                                      ;74.27
        mov       ecx, DWORD PTR [esp]                          ;74.27
        je        .B1.129       ; Prob 82%                      ;74.27
        jmp       .B1.47        ; Prob 100%                     ;74.27
                                ; LOE eax edx ecx ebx
.B1.136:                        ; Preds .B1.125                 ; Infreq
        mov       eax, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VLGCOUNTER] ;56.3
        xor       ecx, ecx                                      ;56.3
        test      eax, eax                                      ;56.3
        cmovle    eax, ecx                                      ;56.3
        mov       esi, 4                                        ;56.3
        mov       edx, 2                                        ;56.3
        mov       ebx, 1                                        ;56.3
        mov       DWORD PTR [76+esp], 133                       ;56.3
        mov       DWORD PTR [68+esp], esi                       ;56.3
        mov       DWORD PTR [80+esp], edx                       ;56.3
        lea       edi, DWORD PTR [eax*4]                        ;56.3
        mov       DWORD PTR [72+esp], ecx                       ;56.3
        lea       ecx, DWORD PTR [28+esp]                       ;56.3
        mov       DWORD PTR [96+esp], ebx                       ;56.3
        mov       DWORD PTR [88+esp], eax                       ;56.3
        mov       DWORD PTR [108+esp], ebx                      ;56.3
        mov       DWORD PTR [100+esp], 5                        ;56.3
        mov       DWORD PTR [92+esp], esi                       ;56.3
        mov       DWORD PTR [104+esp], edi                      ;56.3
        push      20                                            ;56.3
        push      eax                                           ;56.3
        push      edx                                           ;56.3
        push      ecx                                           ;56.3
        call      _for_check_mult_overflow                      ;56.3
                                ; LOE eax
.B1.137:                        ; Preds .B1.136                 ; Infreq
        mov       edx, DWORD PTR [92+esp]                       ;56.3
        and       eax, 1                                        ;56.3
        and       edx, 1                                        ;56.3
        lea       ecx, DWORD PTR [80+esp]                       ;56.3
        shl       eax, 4                                        ;56.3
        add       edx, edx                                      ;56.3
        or        edx, eax                                      ;56.3
        or        edx, 262144                                   ;56.3
        push      edx                                           ;56.3
        push      ecx                                           ;56.3
        push      DWORD PTR [52+esp]                            ;56.3
        call      _for_alloc_allocatable                        ;56.3
                                ; LOE
.B1.426:                        ; Preds .B1.137                 ; Infreq
        add       esp, 28                                       ;56.3
                                ; LOE
.B1.138:                        ; Preds .B1.426                 ; Infreq
        mov       edx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VLGCOUNTER] ;58.3
        test      edx, edx                                      ;58.3
        mov       eax, DWORD PTR [64+esp]                       ;64.8
        mov       DWORD PTR [44+esp], edx                       ;58.3
        mov       DWORD PTR [40+esp], eax                       ;64.8
        jle       .B1.153       ; Prob 10%                      ;58.3
                                ; LOE
.B1.139:                        ; Preds .B1.138                 ; Infreq
        mov       eax, DWORD PTR [104+esp]                      ;58.3
        mov       ecx, DWORD PTR [108+esp]                      ;58.3
        mov       ebx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHAIN] ;58.3
        mov       esi, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHAIN+40] ;58.3
        mov       DWORD PTR [4+esp], eax                        ;58.3
        mov       DWORD PTR [esp], ecx                          ;58.3
        mov       edx, DWORD PTR [96+esp]                       ;58.3
        mov       DWORD PTR [36+esp], ebx                       ;58.3
        mov       eax, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHAIN+44] ;58.3
        mov       ecx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHAIN+32] ;58.3
        mov       DWORD PTR [32+esp], esi                       ;58.3
        cmp       DWORD PTR [44+esp], 24                        ;58.3
        jle       .B1.378       ; Prob 0%                       ;58.3
                                ; LOE eax edx ecx ebx bl bh
.B1.140:                        ; Preds .B1.139                 ; Infreq
        neg       eax                                           ;58.3
        shl       ecx, 2                                        ;58.3
        add       eax, 2                                        ;58.3
        neg       ecx                                           ;58.3
        imul      eax, DWORD PTR [32+esp]                       ;58.3
        mov       edi, ebx                                      ;58.3
        mov       ebx, DWORD PTR [esp]                          ;58.3
        mov       esi, DWORD PTR [4+esp]                        ;58.3
        imul      ebx, esi                                      ;58.3
        lea       ecx, DWORD PTR [4+edi+ecx]                    ;58.3
        shl       edx, 2                                        ;58.3
        add       eax, ecx                                      ;58.3
        mov       ecx, DWORD PTR [44+esp]                       ;58.3
        neg       edx                                           ;58.3
        sub       esi, ebx                                      ;58.3
        add       edx, DWORD PTR [40+esp]                       ;58.3
        lea       ebx, DWORD PTR [ecx*4]                        ;58.3
        push      ebx                                           ;58.3
        push      eax                                           ;58.3
        lea       edx, DWORD PTR [4+edx+esi]                    ;58.3
        push      edx                                           ;58.3
        call      __intel_fast_memcpy                           ;58.3
                                ; LOE
.B1.427:                        ; Preds .B1.140                 ; Infreq
        add       esp, 12                                       ;58.3
                                ; LOE
.B1.141:                        ; Preds .B1.427                 ; Infreq
        mov       eax, DWORD PTR [104+esp]                      ;59.3
        mov       ecx, DWORD PTR [108+esp]                      ;59.3
        mov       ebx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHAIN] ;59.3
        mov       esi, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHAIN+40] ;59.3
        mov       DWORD PTR [4+esp], eax                        ;59.3
        mov       DWORD PTR [esp], ecx                          ;59.3
        mov       edx, DWORD PTR [96+esp]                       ;59.3
        mov       DWORD PTR [36+esp], ebx                       ;59.3
        mov       eax, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHAIN+44] ;59.3
        mov       ecx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHAIN+32] ;59.3
        mov       DWORD PTR [32+esp], esi                       ;59.3
                                ; LOE eax edx ecx
.B1.142:                        ; Preds .B1.141 .B1.397 .B1.394 ; Infreq
        cmp       DWORD PTR [44+esp], 24                        ;59.3
        jle       .B1.355       ; Prob 0%                       ;59.3
                                ; LOE eax edx ecx
.B1.143:                        ; Preds .B1.142                 ; Infreq
        neg       eax                                           ;59.3
        shl       ecx, 2                                        ;59.3
        add       eax, 3                                        ;59.3
        mov       ebx, DWORD PTR [esp]                          ;59.3
        neg       ecx                                           ;59.3
        imul      eax, DWORD PTR [32+esp]                       ;59.3
        neg       ebx                                           ;59.3
        mov       esi, DWORD PTR [36+esp]                       ;59.3
        add       ebx, 2                                        ;59.3
        mov       edi, DWORD PTR [44+esp]                       ;59.3
        shl       edx, 2                                        ;59.3
        imul      ebx, DWORD PTR [4+esp]                        ;59.3
        neg       edx                                           ;59.3
        add       edx, DWORD PTR [40+esp]                       ;59.3
        lea       ecx, DWORD PTR [4+esi+ecx]                    ;59.3
        add       eax, ecx                                      ;59.3
        lea       ecx, DWORD PTR [edi*4]                        ;59.3
        push      ecx                                           ;59.3
        push      eax                                           ;59.3
        lea       edx, DWORD PTR [4+ebx+edx]                    ;59.3
        push      edx                                           ;59.3
        call      __intel_fast_memcpy                           ;59.3
                                ; LOE
.B1.428:                        ; Preds .B1.143                 ; Infreq
        add       esp, 12                                       ;59.3
                                ; LOE
.B1.144:                        ; Preds .B1.428                 ; Infreq
        mov       eax, DWORD PTR [104+esp]                      ;60.3
        mov       ecx, DWORD PTR [108+esp]                      ;60.3
        mov       ebx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHAIN] ;60.3
        mov       esi, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHAIN+40] ;60.3
        mov       DWORD PTR [4+esp], eax                        ;60.3
        mov       DWORD PTR [esp], ecx                          ;60.3
        mov       edx, DWORD PTR [96+esp]                       ;60.3
        mov       DWORD PTR [36+esp], ebx                       ;60.3
        mov       eax, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHAIN+44] ;60.3
        mov       ecx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHAIN+32] ;60.3
        mov       DWORD PTR [32+esp], esi                       ;60.3
                                ; LOE eax edx ecx
.B1.145:                        ; Preds .B1.144 .B1.374 .B1.371 ; Infreq
        cmp       DWORD PTR [44+esp], 24                        ;60.3
        jle       .B1.332       ; Prob 0%                       ;60.3
                                ; LOE eax edx ecx
.B1.146:                        ; Preds .B1.145                 ; Infreq
        mov       edi, DWORD PTR [32+esp]                       ;60.3
        imul      eax, edi                                      ;60.3
        mov       ebx, DWORD PTR [esp]                          ;60.3
        neg       ebx                                           ;60.3
        add       ebx, 3                                        ;60.3
        shl       edx, 2                                        ;60.3
        shl       ecx, 2                                        ;60.3
        neg       edx                                           ;60.3
        imul      ebx, DWORD PTR [4+esp]                        ;60.3
        sub       edi, ecx                                      ;60.3
        mov       esi, DWORD PTR [36+esp]                       ;60.3
        sub       esi, eax                                      ;60.3
        mov       eax, DWORD PTR [44+esp]                       ;60.3
        add       edx, DWORD PTR [40+esp]                       ;60.3
        lea       ecx, DWORD PTR [eax*4]                        ;60.3
        push      ecx                                           ;60.3
        lea       edx, DWORD PTR [4+ebx+edx]                    ;60.3
        lea       ebx, DWORD PTR [4+esi+edi]                    ;60.3
        push      ebx                                           ;60.3
        push      edx                                           ;60.3
        call      __intel_fast_memcpy                           ;60.3
                                ; LOE
.B1.429:                        ; Preds .B1.146                 ; Infreq
        add       esp, 12                                       ;60.3
                                ; LOE
.B1.147:                        ; Preds .B1.429                 ; Infreq
        mov       eax, DWORD PTR [104+esp]                      ;61.3
        mov       ecx, DWORD PTR [108+esp]                      ;61.3
        mov       ebx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHAIN] ;61.3
        mov       esi, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHAIN+40] ;61.3
        mov       DWORD PTR [4+esp], eax                        ;61.3
        mov       DWORD PTR [esp], ecx                          ;61.3
        mov       edx, DWORD PTR [96+esp]                       ;61.3
        mov       DWORD PTR [36+esp], ebx                       ;61.3
        mov       eax, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHAIN+44] ;61.3
        mov       ecx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHAIN+32] ;61.3
        mov       DWORD PTR [32+esp], esi                       ;61.3
                                ; LOE eax edx ecx
.B1.148:                        ; Preds .B1.147 .B1.351 .B1.348 ; Infreq
        cmp       DWORD PTR [44+esp], 24                        ;61.3
        jle       .B1.309       ; Prob 0%                       ;61.3
                                ; LOE eax edx ecx
.B1.149:                        ; Preds .B1.148                 ; Infreq
        mov       esi, DWORD PTR [4+esp]                        ;61.3
        neg       edx                                           ;61.3
        add       edx, esi                                      ;61.3
        neg       eax                                           ;61.3
        imul      esi, DWORD PTR [esp]                          ;61.3
        add       eax, 4                                        ;61.3
        imul      eax, DWORD PTR [32+esp]                       ;61.3
        neg       esi                                           ;61.3
        mov       ebx, DWORD PTR [40+esp]                       ;61.3
        shl       ecx, 2                                        ;61.3
        neg       ecx                                           ;61.3
        lea       edi, DWORD PTR [4+ebx+esi]                    ;61.3
        mov       ebx, DWORD PTR [36+esp]                       ;61.3
        lea       edx, DWORD PTR [edi+edx*4]                    ;61.3
        lea       ecx, DWORD PTR [4+ebx+ecx]                    ;61.3
        add       eax, ecx                                      ;61.3
        mov       ecx, DWORD PTR [44+esp]                       ;61.3
        lea       ebx, DWORD PTR [ecx*4]                        ;61.3
        push      ebx                                           ;61.3
        push      eax                                           ;61.3
        push      edx                                           ;61.3
        call      __intel_fast_memcpy                           ;61.3
                                ; LOE
.B1.430:                        ; Preds .B1.149                 ; Infreq
        add       esp, 12                                       ;61.3
                                ; LOE
.B1.150:                        ; Preds .B1.430                 ; Infreq
        mov       eax, DWORD PTR [104+esp]                      ;62.3
        mov       ecx, DWORD PTR [108+esp]                      ;62.3
        mov       ebx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHAIN] ;62.3
        mov       esi, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHAIN+40] ;62.3
        mov       DWORD PTR [4+esp], eax                        ;62.3
        mov       DWORD PTR [esp], ecx                          ;62.3
        mov       edx, DWORD PTR [96+esp]                       ;62.3
        mov       DWORD PTR [36+esp], ebx                       ;62.3
        mov       eax, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHAIN+44] ;62.3
        mov       ecx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHAIN+32] ;62.3
        mov       DWORD PTR [32+esp], esi                       ;62.3
                                ; LOE eax edx ecx
.B1.151:                        ; Preds .B1.150 .B1.328 .B1.325 ; Infreq
        cmp       DWORD PTR [44+esp], 24                        ;62.3
        jle       .B1.287       ; Prob 0%                       ;62.3
                                ; LOE eax edx ecx
.B1.152:                        ; Preds .B1.151                 ; Infreq
        mov       ebx, DWORD PTR [esp]                          ;62.3
        neg       eax                                           ;62.3
        neg       ebx                                           ;62.3
        add       eax, 5                                        ;62.3
        add       ebx, 5                                        ;62.3
        shl       edx, 2                                        ;62.3
        imul      ebx, DWORD PTR [4+esp]                        ;62.3
        neg       edx                                           ;62.3
        imul      eax, DWORD PTR [32+esp]                       ;62.3
        add       edx, DWORD PTR [40+esp]                       ;62.3
        shl       ecx, 2                                        ;62.3
        neg       ecx                                           ;62.3
        mov       esi, DWORD PTR [44+esp]                       ;62.3
        lea       edi, DWORD PTR [4+ebx+edx]                    ;62.3
        mov       edx, DWORD PTR [36+esp]                       ;62.3
        shl       esi, 2                                        ;62.3
        push      esi                                           ;62.3
        lea       ecx, DWORD PTR [4+edx+ecx]                    ;62.3
        add       eax, ecx                                      ;62.3
        push      eax                                           ;62.3
        push      edi                                           ;62.3
        call      __intel_fast_memcpy                           ;62.3
                                ; LOE
.B1.431:                        ; Preds .B1.152                 ; Infreq
        add       esp, 12                                       ;62.3
                                ; LOE
.B1.153:                        ; Preds .B1.305 .B1.138 .B1.303 .B1.431 ; Infreq
        push      OFFSET FLAT: __NLITPACK_0.0.1                 ;64.8
        push      OFFSET FLAT: _DYNUST_VEH_PATH_ATT_MODULE_mp_VLGCOUNTER ;64.8
        push      DWORD PTR [48+esp]                            ;64.8
        call      _IMSLSORT                                     ;64.8
                                ; LOE
.B1.432:                        ; Preds .B1.153                 ; Infreq
        add       esp, 12                                       ;64.8
                                ; LOE
.B1.154:                        ; Preds .B1.432                 ; Infreq
        mov       edx, DWORD PTR [64+esp]                       ;71.3
        mov       eax, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VLGCOUNTER] ;66.3
        test      eax, eax                                      ;66.3
        mov       DWORD PTR [32+esp], edx                       ;71.3
        mov       DWORD PTR [52+esp], eax                       ;66.3
        mov       edx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHAIN] ;70.3
        mov       edi, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHAIN+44] ;70.3
        mov       esi, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHAIN+40] ;70.3
        mov       ebx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHAIN+32] ;70.3
        jle       .B1.286       ; Prob 10%                      ;66.3
                                ; LOE edx ebx esi edi
.B1.155:                        ; Preds .B1.154                 ; Infreq
        mov       eax, DWORD PTR [96+esp]                       ;66.3
        mov       DWORD PTR [48+esp], eax                       ;66.3
        mov       ecx, DWORD PTR [104+esp]                      ;66.3
        mov       eax, DWORD PTR [108+esp]                      ;66.3
        mov       DWORD PTR [44+esp], ecx                       ;66.3
        mov       DWORD PTR [40+esp], eax                       ;66.3
        cmp       DWORD PTR [52+esp], 24                        ;66.3
        jle       .B1.263       ; Prob 0%                       ;66.3
                                ; LOE edx ebx esi edi
.B1.156:                        ; Preds .B1.155                 ; Infreq
        mov       DWORD PTR [36+esp], edi                       ;
        neg       edi                                           ;66.3
        add       edi, 2                                        ;66.3
        lea       eax, DWORD PTR [ebx*4]                        ;66.3
        neg       eax                                           ;66.3
        imul      edi, esi                                      ;66.3
        mov       DWORD PTR [4+esp], ebx                        ;
        lea       ecx, DWORD PTR [4+edx+eax]                    ;66.3
        mov       ebx, DWORD PTR [40+esp]                       ;66.3
        add       edi, ecx                                      ;66.3
        mov       ecx, DWORD PTR [44+esp]                       ;66.3
        imul      ebx, ecx                                      ;66.3
        mov       eax, DWORD PTR [48+esp]                       ;66.3
        sub       ecx, ebx                                      ;66.3
        shl       eax, 2                                        ;66.3
        mov       ebx, DWORD PTR [52+esp]                       ;66.3
        neg       eax                                           ;66.3
        add       eax, DWORD PTR [32+esp]                       ;66.3
        shl       ebx, 2                                        ;66.3
        push      ebx                                           ;66.3
        lea       eax, DWORD PTR [4+eax+ecx]                    ;66.3
        push      eax                                           ;66.3
        push      edi                                           ;66.3
        mov       ebx, DWORD PTR [16+esp]                       ;66.3
        mov       edi, DWORD PTR [48+esp]                       ;66.3
        mov       DWORD PTR [36+esp], edx                       ;66.3
        call      __intel_fast_memcpy                           ;66.3
                                ; LOE ebx esi edi bl bh
.B1.433:                        ; Preds .B1.156                 ; Infreq
        mov       edx, DWORD PTR [36+esp]                       ;
        add       esp, 12                                       ;66.3
                                ; LOE edx ebx esi edi dl bl dh bh
.B1.157:                        ; Preds .B1.433                 ; Infreq
        mov       eax, DWORD PTR [96+esp]                       ;67.3
        mov       DWORD PTR [48+esp], eax                       ;67.3
        mov       ecx, DWORD PTR [104+esp]                      ;67.3
        mov       eax, DWORD PTR [108+esp]                      ;67.3
        mov       DWORD PTR [44+esp], ecx                       ;67.3
        mov       DWORD PTR [40+esp], eax                       ;67.3
                                ; LOE edx ebx esi edi
.B1.158:                        ; Preds .B1.157 .B1.282 .B1.279 ; Infreq
        cmp       DWORD PTR [52+esp], 24                        ;67.3
        jle       .B1.240       ; Prob 0%                       ;67.3
                                ; LOE edx ebx esi edi
.B1.159:                        ; Preds .B1.158                 ; Infreq
        imul      edi, esi                                      ;67.3
        mov       ecx, edx                                      ;67.3
        shl       ebx, 2                                        ;67.3
        lea       eax, DWORD PTR [esi+esi*2]                    ;67.3
        sub       ecx, edi                                      ;67.3
        sub       eax, ebx                                      ;67.3
        mov       DWORD PTR [4+esp], ebx                        ;67.3
        mov       ebx, DWORD PTR [40+esp]                       ;67.3
        neg       ebx                                           ;67.3
        add       ebx, 2                                        ;67.3
        lea       eax, DWORD PTR [4+ecx+eax]                    ;67.3
        mov       ecx, DWORD PTR [48+esp]                       ;67.3
        shl       ecx, 2                                        ;67.3
        imul      ebx, DWORD PTR [44+esp]                       ;67.3
        neg       ecx                                           ;67.3
        add       ecx, DWORD PTR [32+esp]                       ;67.3
        lea       ebx, DWORD PTR [4+ebx+ecx]                    ;67.3
        mov       ecx, DWORD PTR [52+esp]                       ;67.3
        shl       ecx, 2                                        ;67.3
        push      ecx                                           ;67.3
        push      ebx                                           ;67.3
        push      eax                                           ;67.3
        mov       ebx, DWORD PTR [16+esp]                       ;67.3
        mov       DWORD PTR [36+esp], edx                       ;67.3
        call      __intel_fast_memcpy                           ;67.3
                                ; LOE ebx esi edi bl bh
.B1.434:                        ; Preds .B1.159                 ; Infreq
        mov       edx, DWORD PTR [36+esp]                       ;
        add       esp, 12                                       ;67.3
                                ; LOE edx ebx esi edi dl bl dh bh
.B1.160:                        ; Preds .B1.434                 ; Infreq
        mov       eax, DWORD PTR [96+esp]                       ;68.3
        mov       DWORD PTR [48+esp], eax                       ;68.3
        mov       ecx, DWORD PTR [104+esp]                      ;68.3
        mov       eax, DWORD PTR [108+esp]                      ;68.3
        mov       DWORD PTR [44+esp], ecx                       ;68.3
        mov       DWORD PTR [40+esp], eax                       ;68.3
                                ; LOE edx ebx esi edi
.B1.161:                        ; Preds .B1.160 .B1.259 .B1.256 ; Infreq
        cmp       DWORD PTR [52+esp], 24                        ;68.3
        jle       .B1.217       ; Prob 0%                       ;68.3
                                ; LOE edx ebx esi edi
.B1.162:                        ; Preds .B1.161                 ; Infreq
        mov       ecx, edx                                      ;68.3
        mov       eax, esi                                      ;68.3
        sub       ecx, edi                                      ;68.3
        sub       eax, ebx                                      ;68.3
        mov       DWORD PTR [4+esp], ebx                        ;
        mov       ebx, DWORD PTR [40+esp]                       ;68.3
        neg       ebx                                           ;68.3
        add       ebx, 3                                        ;68.3
        lea       eax, DWORD PTR [4+ecx+eax]                    ;68.3
        mov       ecx, DWORD PTR [48+esp]                       ;68.3
        shl       ecx, 2                                        ;68.3
        imul      ebx, DWORD PTR [44+esp]                       ;68.3
        neg       ecx                                           ;68.3
        add       ecx, DWORD PTR [32+esp]                       ;68.3
        lea       ebx, DWORD PTR [4+ebx+ecx]                    ;68.3
        mov       ecx, DWORD PTR [52+esp]                       ;68.3
        shl       ecx, 2                                        ;68.3
        push      ecx                                           ;68.3
        push      ebx                                           ;68.3
        push      eax                                           ;68.3
        mov       ebx, DWORD PTR [16+esp]                       ;68.3
        mov       DWORD PTR [36+esp], edx                       ;68.3
        call      __intel_fast_memcpy                           ;68.3
                                ; LOE ebx esi edi bl bh
.B1.435:                        ; Preds .B1.162                 ; Infreq
        mov       edx, DWORD PTR [36+esp]                       ;
        add       esp, 12                                       ;68.3
                                ; LOE edx ebx esi edi dl bl dh bh
.B1.163:                        ; Preds .B1.435                 ; Infreq
        mov       eax, DWORD PTR [96+esp]                       ;69.3
        mov       DWORD PTR [48+esp], eax                       ;69.3
        mov       ecx, DWORD PTR [104+esp]                      ;69.3
        mov       eax, DWORD PTR [108+esp]                      ;69.3
        mov       DWORD PTR [44+esp], ecx                       ;69.3
        mov       DWORD PTR [40+esp], eax                       ;69.3
                                ; LOE edx ebx esi edi
.B1.164:                        ; Preds .B1.163 .B1.236 .B1.233 ; Infreq
        cmp       DWORD PTR [52+esp], 24                        ;69.3
        jle       .B1.194       ; Prob 0%                       ;69.3
                                ; LOE edx ebx esi edi
.B1.165:                        ; Preds .B1.164                 ; Infreq
        mov       eax, edx                                      ;69.3
        lea       ecx, DWORD PTR [esi*4]                        ;69.3
        sub       eax, edi                                      ;69.3
        sub       ecx, ebx                                      ;69.3
        mov       DWORD PTR [4+esp], ebx                        ;
        mov       ebx, DWORD PTR [48+esp]                       ;69.3
        mov       DWORD PTR [36+esp], edi                       ;
        neg       ebx                                           ;69.3
        lea       edi, DWORD PTR [4+eax+ecx]                    ;69.3
        mov       ecx, DWORD PTR [44+esp]                       ;69.3
        add       ebx, ecx                                      ;69.3
        imul      ecx, DWORD PTR [40+esp]                       ;69.3
        neg       ecx                                           ;69.3
        mov       eax, DWORD PTR [32+esp]                       ;69.3
        lea       eax, DWORD PTR [4+eax+ecx]                    ;69.3
        lea       eax, DWORD PTR [eax+ebx*4]                    ;69.3
        mov       ebx, DWORD PTR [52+esp]                       ;69.3
        shl       ebx, 2                                        ;69.3
        push      ebx                                           ;69.3
        push      eax                                           ;69.3
        push      edi                                           ;69.3
        mov       ebx, DWORD PTR [16+esp]                       ;69.3
        mov       edi, DWORD PTR [48+esp]                       ;69.3
        mov       DWORD PTR [36+esp], edx                       ;69.3
        call      __intel_fast_memcpy                           ;69.3
                                ; LOE ebx esi edi bl bh
.B1.436:                        ; Preds .B1.165                 ; Infreq
        mov       edx, DWORD PTR [36+esp]                       ;
        add       esp, 12                                       ;69.3
                                ; LOE edx ebx esi edi dl bl dh bh
.B1.166:                        ; Preds .B1.436                 ; Infreq
        mov       eax, DWORD PTR [96+esp]                       ;70.3
        mov       DWORD PTR [48+esp], eax                       ;70.3
        mov       ecx, DWORD PTR [104+esp]                      ;70.3
        mov       eax, DWORD PTR [108+esp]                      ;70.3
        mov       DWORD PTR [44+esp], ecx                       ;70.3
        mov       DWORD PTR [40+esp], eax                       ;70.3
                                ; LOE edx ebx esi edi
.B1.167:                        ; Preds .B1.166 .B1.213 .B1.210 ; Infreq
        cmp       DWORD PTR [52+esp], 24                        ;70.3
        jle       .B1.171       ; Prob 0%                       ;70.3
                                ; LOE edx ebx esi edi
.B1.168:                        ; Preds .B1.167                 ; Infreq
        mov       eax, edx                                      ;70.3
        lea       ecx, DWORD PTR [esi+esi*4]                    ;70.3
        sub       eax, edi                                      ;70.3
        sub       ecx, ebx                                      ;70.3
        mov       DWORD PTR [4+esp], ebx                        ;
        lea       ebx, DWORD PTR [4+eax+ecx]                    ;70.3
        mov       eax, DWORD PTR [40+esp]                       ;70.3
        neg       eax                                           ;70.3
        add       eax, 5                                        ;70.3
        mov       ecx, DWORD PTR [48+esp]                       ;70.3
        shl       ecx, 2                                        ;70.3
        imul      eax, DWORD PTR [44+esp]                       ;70.3
        neg       ecx                                           ;70.3
        add       ecx, DWORD PTR [32+esp]                       ;70.3
        lea       ecx, DWORD PTR [4+eax+ecx]                    ;70.3
        mov       eax, DWORD PTR [52+esp]                       ;70.3
        shl       eax, 2                                        ;70.3
        push      eax                                           ;70.3
        push      ecx                                           ;70.3
        push      ebx                                           ;70.3
        mov       ebx, DWORD PTR [16+esp]                       ;70.3
        mov       DWORD PTR [36+esp], edx                       ;70.3
        call      __intel_fast_memcpy                           ;70.3
                                ; LOE ebx esi edi bl bh
.B1.437:                        ; Preds .B1.168                 ; Infreq
        mov       edx, DWORD PTR [36+esp]                       ;
        add       esp, 12                                       ;70.3
                                ; LOE edx ebx esi edi
.B1.169:                        ; Preds .B1.286 .B1.437 .B1.187 .B1.190 ; Infreq
        mov       ecx, DWORD PTR [76+esp]                       ;71.3
        mov       eax, ecx                                      ;71.3
        shr       eax, 1                                        ;71.3
        mov       DWORD PTR [esp], ecx                          ;71.3
        and       eax, 1                                        ;71.3
        and       ecx, 1                                        ;71.3
        shl       eax, 2                                        ;71.3
        add       ecx, ecx                                      ;71.3
        or        eax, ecx                                      ;71.3
        or        eax, 262144                                   ;71.3
        push      eax                                           ;71.3
        push      DWORD PTR [36+esp]                            ;71.3
        mov       DWORD PTR [32+esp], edx                       ;71.3
        call      _for_dealloc_allocatable                      ;71.3
                                ; LOE ebx esi edi
.B1.438:                        ; Preds .B1.169                 ; Infreq
        mov       edx, DWORD PTR [32+esp]                       ;
        add       esp, 8                                        ;71.3
                                ; LOE edx ebx esi edi dl dh
.B1.170:                        ; Preds .B1.438                 ; Infreq
        mov       ecx, DWORD PTR [8+ebp]                        ;74.30
        mov       eax, DWORD PTR [esp]                          ;71.3
        and       eax, -2                                       ;71.3
        mov       DWORD PTR [64+esp], 0                         ;71.3
        mov       DWORD PTR [76+esp], eax                       ;71.3
        movss     xmm5, DWORD PTR [ecx]                         ;74.30
        movss     xmm4, DWORD PTR [_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT] ;74.30
        movss     xmm1, DWORD PTR [_2il0floatpacket.12]         ;74.30
        jmp       .B1.127       ; Prob 100%                     ;74.30
                                ; LOE edx ebx esi edi xmm1 xmm4 xmm5
.B1.171:                        ; Preds .B1.167                 ; Infreq
        cmp       DWORD PTR [52+esp], 4                         ;70.3
        jl        .B1.191       ; Prob 10%                      ;70.3
                                ; LOE edx ebx esi edi
.B1.172:                        ; Preds .B1.171                 ; Infreq
        mov       eax, edx                                      ;70.3
        lea       ecx, DWORD PTR [esi+esi*4]                    ;70.3
        sub       eax, edi                                      ;70.3
        mov       DWORD PTR [16+esp], eax                       ;70.3
        mov       DWORD PTR [12+esp], ecx                       ;70.3
        add       eax, ecx                                      ;70.3
        sub       eax, ebx                                      ;70.3
        add       eax, 4                                        ;70.3
        and       eax, 15                                       ;70.3
        mov       DWORD PTR [20+esp], eax                       ;70.3
        je        .B1.175       ; Prob 50%                      ;70.3
                                ; LOE eax edx ebx esi edi al ah
.B1.173:                        ; Preds .B1.172                 ; Infreq
        test      BYTE PTR [20+esp], 3                          ;70.3
        jne       .B1.191       ; Prob 10%                      ;70.3
                                ; LOE eax edx ebx esi edi al ah
.B1.174:                        ; Preds .B1.173                 ; Infreq
        neg       eax                                           ;70.3
        add       eax, 16                                       ;70.3
        shr       eax, 2                                        ;70.3
        mov       DWORD PTR [20+esp], eax                       ;70.3
                                ; LOE eax edx ebx esi edi al ah
.B1.175:                        ; Preds .B1.174 .B1.172         ; Infreq
        lea       ecx, DWORD PTR [4+eax]                        ;70.3
        cmp       ecx, DWORD PTR [52+esp]                       ;70.3
        jg        .B1.191       ; Prob 10%                      ;70.3
                                ; LOE edx ebx esi edi
.B1.176:                        ; Preds .B1.175                 ; Infreq
        mov       DWORD PTR [36+esp], edi                       ;
        mov       edi, DWORD PTR [52+esp]                       ;70.3
        mov       eax, edi                                      ;70.3
        mov       ecx, DWORD PTR [20+esp]                       ;70.3
        sub       eax, ecx                                      ;70.3
        and       eax, 3                                        ;70.3
        neg       eax                                           ;70.3
        add       eax, edi                                      ;70.3
        mov       DWORD PTR [4+esp], eax                        ;70.3
        mov       eax, DWORD PTR [40+esp]                       ;
        neg       eax                                           ;
        mov       edi, DWORD PTR [48+esp]                       ;
        add       eax, 5                                        ;
        imul      eax, DWORD PTR [44+esp]                       ;
        shl       edi, 2                                        ;
        neg       edi                                           ;
        add       edi, DWORD PTR [32+esp]                       ;
        add       eax, edi                                      ;
        mov       DWORD PTR [8+esp], eax                        ;
        mov       eax, DWORD PTR [12+esp]                       ;
        sub       eax, ebx                                      ;
        add       DWORD PTR [16+esp], eax                       ;
        mov       edi, DWORD PTR [36+esp]                       ;70.3
        test      ecx, ecx                                      ;70.3
        jbe       .B1.180       ; Prob 10%                      ;70.3
                                ; LOE edx ebx esi edi
.B1.177:                        ; Preds .B1.176                 ; Infreq
        mov       DWORD PTR [36+esp], edi                       ;
        xor       eax, eax                                      ;
        mov       DWORD PTR [esp], esi                          ;
        mov       DWORD PTR [24+esp], edx                       ;
        mov       ecx, DWORD PTR [16+esp]                       ;
        mov       esi, DWORD PTR [8+esp]                        ;
        mov       edi, DWORD PTR [20+esp]                       ;
                                ; LOE eax ecx ebx esi edi
.B1.178:                        ; Preds .B1.178 .B1.177         ; Infreq
        mov       edx, DWORD PTR [4+esi+eax*4]                  ;70.3
        mov       DWORD PTR [4+ecx+eax*4], edx                  ;70.3
        inc       eax                                           ;70.3
        cmp       eax, edi                                      ;70.3
        jb        .B1.178       ; Prob 82%                      ;70.3
                                ; LOE eax ecx ebx esi edi
.B1.179:                        ; Preds .B1.178                 ; Infreq
        mov       DWORD PTR [20+esp], edi                       ;
        mov       edi, DWORD PTR [36+esp]                       ;
        mov       esi, DWORD PTR [esp]                          ;
        mov       edx, DWORD PTR [24+esp]                       ;
                                ; LOE edx ebx esi edi
.B1.180:                        ; Preds .B1.176 .B1.179         ; Infreq
        mov       eax, DWORD PTR [8+esp]                        ;70.3
        mov       ecx, DWORD PTR [20+esp]                       ;70.3
        lea       eax, DWORD PTR [4+eax+ecx*4]                  ;70.3
        test      al, 15                                        ;70.3
        je        .B1.184       ; Prob 60%                      ;70.3
                                ; LOE edx ebx esi edi
.B1.181:                        ; Preds .B1.180                 ; Infreq
        mov       DWORD PTR [esp], esi                          ;
        mov       DWORD PTR [24+esp], edx                       ;
        mov       eax, DWORD PTR [4+esp]                        ;
        mov       edx, DWORD PTR [16+esp]                       ;
        mov       ecx, DWORD PTR [8+esp]                        ;
        mov       esi, DWORD PTR [20+esp]                       ;
                                ; LOE eax edx ecx ebx esi edi
.B1.182:                        ; Preds .B1.182 .B1.181         ; Infreq
        movdqu    xmm0, XMMWORD PTR [4+ecx+esi*4]               ;70.3
        movdqa    XMMWORD PTR [4+edx+esi*4], xmm0               ;70.3
        add       esi, 4                                        ;70.3
        cmp       esi, eax                                      ;70.3
        jb        .B1.182       ; Prob 82%                      ;70.3
        jmp       .B1.186       ; Prob 100%                     ;70.3
                                ; LOE eax edx ecx ebx esi edi
.B1.184:                        ; Preds .B1.180                 ; Infreq
        mov       DWORD PTR [esp], esi                          ;
        mov       DWORD PTR [24+esp], edx                       ;
        mov       eax, DWORD PTR [4+esp]                        ;
        mov       edx, DWORD PTR [16+esp]                       ;
        mov       ecx, DWORD PTR [8+esp]                        ;
        mov       esi, DWORD PTR [20+esp]                       ;
                                ; LOE eax edx ecx ebx esi edi
.B1.185:                        ; Preds .B1.185 .B1.184         ; Infreq
        movdqa    xmm0, XMMWORD PTR [4+ecx+esi*4]               ;70.3
        movdqa    XMMWORD PTR [4+edx+esi*4], xmm0               ;70.3
        add       esi, 4                                        ;70.3
        cmp       esi, eax                                      ;70.3
        jb        .B1.185       ; Prob 82%                      ;70.3
                                ; LOE eax edx ecx ebx esi edi
.B1.186:                        ; Preds .B1.182 .B1.185         ; Infreq
        mov       DWORD PTR [4+esp], eax                        ;
        mov       esi, DWORD PTR [esp]                          ;
        mov       edx, DWORD PTR [24+esp]                       ;
                                ; LOE edx ebx esi edi
.B1.187:                        ; Preds .B1.186 .B1.191         ; Infreq
        mov       eax, DWORD PTR [52+esp]                       ;70.3
        cmp       eax, DWORD PTR [4+esp]                        ;70.3
        jbe       .B1.169       ; Prob 10%                      ;70.3
                                ; LOE edx ebx esi edi
.B1.188:                        ; Preds .B1.187                 ; Infreq
        mov       ecx, DWORD PTR [40+esp]                       ;
        neg       ecx                                           ;
        add       ecx, 5                                        ;
        imul      ecx, DWORD PTR [44+esp]                       ;
        mov       eax, DWORD PTR [48+esp]                       ;
        shl       eax, 2                                        ;
        neg       eax                                           ;
        add       eax, DWORD PTR [32+esp]                       ;
        add       ecx, eax                                      ;
        mov       eax, edx                                      ;
        mov       DWORD PTR [40+esp], ecx                       ;
        lea       ecx, DWORD PTR [esi+esi*4]                    ;70.3
        sub       eax, edi                                      ;
        sub       ecx, ebx                                      ;
        mov       DWORD PTR [36+esp], edi                       ;
        add       eax, ecx                                      ;
        mov       DWORD PTR [esp], esi                          ;
        mov       DWORD PTR [24+esp], edx                       ;
        mov       ecx, DWORD PTR [40+esp]                       ;
        mov       esi, DWORD PTR [4+esp]                        ;
        mov       edi, DWORD PTR [52+esp]                       ;
                                ; LOE eax ecx ebx esi edi
.B1.189:                        ; Preds .B1.189 .B1.188         ; Infreq
        mov       edx, DWORD PTR [4+ecx+esi*4]                  ;70.3
        mov       DWORD PTR [4+eax+esi*4], edx                  ;70.3
        inc       esi                                           ;70.3
        cmp       esi, edi                                      ;70.3
        jb        .B1.189       ; Prob 82%                      ;70.3
                                ; LOE eax ecx ebx esi edi
.B1.190:                        ; Preds .B1.189                 ; Infreq
        mov       edi, DWORD PTR [36+esp]                       ;
        mov       esi, DWORD PTR [esp]                          ;
        mov       edx, DWORD PTR [24+esp]                       ;
        jmp       .B1.169       ; Prob 100%                     ;
                                ; LOE edx ebx esi edi
.B1.191:                        ; Preds .B1.171 .B1.175 .B1.173 ; Infreq
        xor       eax, eax                                      ;70.3
        mov       DWORD PTR [4+esp], eax                        ;70.3
        jmp       .B1.187       ; Prob 100%                     ;70.3
                                ; LOE edx ebx esi edi
.B1.194:                        ; Preds .B1.164                 ; Infreq
        cmp       DWORD PTR [52+esp], 4                         ;69.3
        jl        .B1.214       ; Prob 10%                      ;69.3
                                ; LOE edx ebx esi edi
.B1.195:                        ; Preds .B1.194                 ; Infreq
        mov       ecx, edx                                      ;69.3
        lea       eax, DWORD PTR [esi*4]                        ;69.3
        sub       ecx, edi                                      ;69.3
        mov       DWORD PTR [8+esp], eax                        ;69.3
        mov       DWORD PTR [16+esp], ecx                       ;69.3
        lea       eax, DWORD PTR [ecx+esi*4]                    ;69.3
        sub       eax, ebx                                      ;69.3
        add       eax, 4                                        ;69.3
        and       eax, 15                                       ;69.3
        mov       DWORD PTR [20+esp], eax                       ;69.3
        je        .B1.198       ; Prob 50%                      ;69.3
                                ; LOE eax edx ebx esi edi al ah
.B1.196:                        ; Preds .B1.195                 ; Infreq
        test      BYTE PTR [20+esp], 3                          ;69.3
        jne       .B1.214       ; Prob 10%                      ;69.3
                                ; LOE eax edx ebx esi edi al ah
.B1.197:                        ; Preds .B1.196                 ; Infreq
        neg       eax                                           ;69.3
        add       eax, 16                                       ;69.3
        shr       eax, 2                                        ;69.3
        mov       DWORD PTR [20+esp], eax                       ;69.3
                                ; LOE eax edx ebx esi edi al ah
.B1.198:                        ; Preds .B1.197 .B1.195         ; Infreq
        lea       ecx, DWORD PTR [4+eax]                        ;69.3
        cmp       ecx, DWORD PTR [52+esp]                       ;69.3
        jg        .B1.214       ; Prob 10%                      ;69.3
                                ; LOE edx ebx esi edi
.B1.199:                        ; Preds .B1.198                 ; Infreq
        mov       DWORD PTR [36+esp], edi                       ;
        mov       edi, DWORD PTR [52+esp]                       ;69.3
        mov       eax, edi                                      ;69.3
        mov       ecx, DWORD PTR [20+esp]                       ;69.3
        sub       eax, ecx                                      ;69.3
        and       eax, 3                                        ;69.3
        neg       eax                                           ;69.3
        add       eax, edi                                      ;69.3
        mov       DWORD PTR [12+esp], eax                       ;69.3
        mov       eax, DWORD PTR [48+esp]                       ;
        mov       edi, DWORD PTR [44+esp]                       ;
        neg       eax                                           ;
        add       eax, edi                                      ;
        imul      edi, DWORD PTR [40+esp]                       ;
        neg       edi                                           ;
        add       edi, DWORD PTR [32+esp]                       ;
        lea       eax, DWORD PTR [edi+eax*4]                    ;
        mov       edi, DWORD PTR [8+esp]                        ;
        sub       edi, ebx                                      ;
        add       DWORD PTR [16+esp], edi                       ;
        mov       edi, DWORD PTR [36+esp]                       ;69.3
        test      ecx, ecx                                      ;69.3
        jbe       .B1.203       ; Prob 10%                      ;69.3
                                ; LOE eax edx ebx esi edi
.B1.200:                        ; Preds .B1.199                 ; Infreq
        mov       DWORD PTR [36+esp], edi                       ;
        xor       ecx, ecx                                      ;
        mov       DWORD PTR [esp], esi                          ;
        mov       DWORD PTR [24+esp], edx                       ;
        mov       esi, DWORD PTR [16+esp]                       ;
        mov       edi, DWORD PTR [20+esp]                       ;
                                ; LOE eax ecx ebx esi edi
.B1.201:                        ; Preds .B1.201 .B1.200         ; Infreq
        mov       edx, DWORD PTR [4+eax+ecx*4]                  ;69.3
        mov       DWORD PTR [4+esi+ecx*4], edx                  ;69.3
        inc       ecx                                           ;69.3
        cmp       ecx, edi                                      ;69.3
        jb        .B1.201       ; Prob 82%                      ;69.3
                                ; LOE eax ecx ebx esi edi
.B1.202:                        ; Preds .B1.201                 ; Infreq
        mov       DWORD PTR [20+esp], edi                       ;
        mov       edi, DWORD PTR [36+esp]                       ;
        mov       esi, DWORD PTR [esp]                          ;
        mov       edx, DWORD PTR [24+esp]                       ;
                                ; LOE eax edx ebx esi edi
.B1.203:                        ; Preds .B1.199 .B1.202         ; Infreq
        mov       ecx, DWORD PTR [20+esp]                       ;69.3
        lea       ecx, DWORD PTR [4+eax+ecx*4]                  ;69.3
        test      cl, 15                                        ;69.3
        je        .B1.207       ; Prob 60%                      ;69.3
                                ; LOE eax edx ebx esi edi
.B1.204:                        ; Preds .B1.203                 ; Infreq
        mov       DWORD PTR [esp], esi                          ;
        mov       DWORD PTR [24+esp], edx                       ;
        mov       edx, DWORD PTR [12+esp]                       ;
        mov       ecx, DWORD PTR [16+esp]                       ;
        mov       esi, DWORD PTR [20+esp]                       ;
                                ; LOE eax edx ecx ebx esi edi
.B1.205:                        ; Preds .B1.205 .B1.204         ; Infreq
        movdqu    xmm0, XMMWORD PTR [4+eax+esi*4]               ;69.3
        movdqa    XMMWORD PTR [4+ecx+esi*4], xmm0               ;69.3
        add       esi, 4                                        ;69.3
        cmp       esi, edx                                      ;69.3
        jb        .B1.205       ; Prob 82%                      ;69.3
        jmp       .B1.209       ; Prob 100%                     ;69.3
                                ; LOE eax edx ecx ebx esi edi
.B1.207:                        ; Preds .B1.203                 ; Infreq
        mov       DWORD PTR [esp], esi                          ;
        mov       DWORD PTR [24+esp], edx                       ;
        mov       edx, DWORD PTR [12+esp]                       ;
        mov       ecx, DWORD PTR [16+esp]                       ;
        mov       esi, DWORD PTR [20+esp]                       ;
                                ; LOE eax edx ecx ebx esi edi
.B1.208:                        ; Preds .B1.208 .B1.207         ; Infreq
        movdqa    xmm0, XMMWORD PTR [4+eax+esi*4]               ;69.3
        movdqa    XMMWORD PTR [4+ecx+esi*4], xmm0               ;69.3
        add       esi, 4                                        ;69.3
        cmp       esi, edx                                      ;69.3
        jb        .B1.208       ; Prob 82%                      ;69.3
                                ; LOE eax edx ecx ebx esi edi
.B1.209:                        ; Preds .B1.205 .B1.208         ; Infreq
        mov       DWORD PTR [12+esp], edx                       ;
        mov       esi, DWORD PTR [esp]                          ;
        mov       edx, DWORD PTR [24+esp]                       ;
                                ; LOE edx ebx esi edi
.B1.210:                        ; Preds .B1.209 .B1.214         ; Infreq
        mov       eax, DWORD PTR [52+esp]                       ;69.3
        cmp       eax, DWORD PTR [12+esp]                       ;69.3
        jbe       .B1.167       ; Prob 10%                      ;69.3
                                ; LOE edx ebx esi edi
.B1.211:                        ; Preds .B1.210                 ; Infreq
        mov       eax, DWORD PTR [48+esp]                       ;
        mov       ecx, DWORD PTR [44+esp]                       ;
        neg       eax                                           ;
        add       eax, ecx                                      ;
        imul      ecx, DWORD PTR [40+esp]                       ;
        neg       ecx                                           ;
        add       ecx, DWORD PTR [32+esp]                       ;
        mov       DWORD PTR [36+esp], edi                       ;
        mov       DWORD PTR [esp], esi                          ;
        mov       DWORD PTR [24+esp], edx                       ;
        lea       eax, DWORD PTR [ecx+eax*4]                    ;
        mov       DWORD PTR [8+esp], eax                        ;
        mov       eax, edx                                      ;
        sub       eax, edi                                      ;
        lea       ecx, DWORD PTR [esi*4]                        ;69.3
        sub       ecx, ebx                                      ;
        mov       esi, DWORD PTR [12+esp]                       ;
        add       eax, ecx                                      ;
        mov       ecx, DWORD PTR [8+esp]                        ;
        mov       edi, DWORD PTR [52+esp]                       ;
                                ; LOE eax ecx ebx esi edi
.B1.212:                        ; Preds .B1.212 .B1.211         ; Infreq
        mov       edx, DWORD PTR [4+ecx+esi*4]                  ;69.3
        mov       DWORD PTR [4+eax+esi*4], edx                  ;69.3
        inc       esi                                           ;69.3
        cmp       esi, edi                                      ;69.3
        jb        .B1.212       ; Prob 82%                      ;69.3
                                ; LOE eax ecx ebx esi edi
.B1.213:                        ; Preds .B1.212                 ; Infreq
        mov       edi, DWORD PTR [36+esp]                       ;
        mov       esi, DWORD PTR [esp]                          ;
        mov       edx, DWORD PTR [24+esp]                       ;
        jmp       .B1.167       ; Prob 100%                     ;
                                ; LOE edx ebx esi edi
.B1.214:                        ; Preds .B1.194 .B1.198 .B1.196 ; Infreq
        xor       eax, eax                                      ;69.3
        mov       DWORD PTR [12+esp], eax                       ;69.3
        jmp       .B1.210       ; Prob 100%                     ;69.3
                                ; LOE edx ebx esi edi
.B1.217:                        ; Preds .B1.161                 ; Infreq
        cmp       DWORD PTR [52+esp], 4                         ;68.3
        jl        .B1.237       ; Prob 10%                      ;68.3
                                ; LOE edx ebx esi edi
.B1.218:                        ; Preds .B1.217                 ; Infreq
        mov       eax, edx                                      ;68.3
        sub       eax, edi                                      ;68.3
        mov       DWORD PTR [16+esp], eax                       ;68.3
        lea       ecx, DWORD PTR [esi+eax]                      ;68.3
        sub       ecx, ebx                                      ;68.3
        add       ecx, 4                                        ;68.3
        and       ecx, 15                                       ;68.3
        mov       DWORD PTR [20+esp], ecx                       ;68.3
        je        .B1.221       ; Prob 50%                      ;68.3
                                ; LOE edx ecx ebx esi edi cl ch
.B1.219:                        ; Preds .B1.218                 ; Infreq
        test      BYTE PTR [20+esp], 3                          ;68.3
        jne       .B1.237       ; Prob 10%                      ;68.3
                                ; LOE edx ecx ebx esi edi cl ch
.B1.220:                        ; Preds .B1.219                 ; Infreq
        mov       eax, ecx                                      ;68.3
        neg       eax                                           ;68.3
        add       eax, 16                                       ;68.3
        shr       eax, 2                                        ;68.3
        mov       DWORD PTR [20+esp], eax                       ;68.3
                                ; LOE edx ebx esi edi
.B1.221:                        ; Preds .B1.220 .B1.218         ; Infreq
        mov       eax, DWORD PTR [20+esp]                       ;68.3
        lea       ecx, DWORD PTR [4+eax]                        ;68.3
        cmp       ecx, DWORD PTR [52+esp]                       ;68.3
        jg        .B1.237       ; Prob 10%                      ;68.3
                                ; LOE edx ebx esi edi
.B1.222:                        ; Preds .B1.221                 ; Infreq
        mov       DWORD PTR [36+esp], edi                       ;
        mov       edi, DWORD PTR [52+esp]                       ;68.3
        mov       eax, edi                                      ;68.3
        mov       ecx, DWORD PTR [20+esp]                       ;68.3
        sub       eax, ecx                                      ;68.3
        and       eax, 3                                        ;68.3
        neg       eax                                           ;68.3
        add       eax, edi                                      ;68.3
        mov       edi, esi                                      ;
        sub       edi, ebx                                      ;
        mov       DWORD PTR [12+esp], eax                       ;68.3
        mov       eax, DWORD PTR [40+esp]                       ;
        neg       eax                                           ;
        add       DWORD PTR [16+esp], edi                       ;
        add       eax, 3                                        ;
        mov       edi, DWORD PTR [48+esp]                       ;
        imul      eax, DWORD PTR [44+esp]                       ;
        shl       edi, 2                                        ;
        neg       edi                                           ;
        add       edi, DWORD PTR [32+esp]                       ;
        add       eax, edi                                      ;
        mov       edi, DWORD PTR [36+esp]                       ;68.3
        test      ecx, ecx                                      ;68.3
        mov       DWORD PTR [8+esp], eax                        ;
        jbe       .B1.226       ; Prob 10%                      ;68.3
                                ; LOE edx ebx esi edi
.B1.223:                        ; Preds .B1.222                 ; Infreq
        mov       DWORD PTR [36+esp], edi                       ;
        xor       eax, eax                                      ;
        mov       DWORD PTR [esp], esi                          ;
        mov       DWORD PTR [24+esp], edx                       ;
        mov       ecx, DWORD PTR [8+esp]                        ;
        mov       esi, DWORD PTR [16+esp]                       ;
        mov       edi, DWORD PTR [20+esp]                       ;
                                ; LOE eax ecx ebx esi edi
.B1.224:                        ; Preds .B1.224 .B1.223         ; Infreq
        mov       edx, DWORD PTR [4+ecx+eax*4]                  ;68.3
        mov       DWORD PTR [4+esi+eax*4], edx                  ;68.3
        inc       eax                                           ;68.3
        cmp       eax, edi                                      ;68.3
        jb        .B1.224       ; Prob 82%                      ;68.3
                                ; LOE eax ecx ebx esi edi
.B1.225:                        ; Preds .B1.224                 ; Infreq
        mov       DWORD PTR [20+esp], edi                       ;
        mov       edi, DWORD PTR [36+esp]                       ;
        mov       esi, DWORD PTR [esp]                          ;
        mov       edx, DWORD PTR [24+esp]                       ;
                                ; LOE edx ebx esi edi
.B1.226:                        ; Preds .B1.222 .B1.225         ; Infreq
        mov       eax, DWORD PTR [8+esp]                        ;68.3
        mov       ecx, DWORD PTR [20+esp]                       ;68.3
        lea       eax, DWORD PTR [4+eax+ecx*4]                  ;68.3
        test      al, 15                                        ;68.3
        je        .B1.230       ; Prob 60%                      ;68.3
                                ; LOE edx ebx esi edi
.B1.227:                        ; Preds .B1.226                 ; Infreq
        mov       DWORD PTR [esp], esi                          ;
        mov       DWORD PTR [24+esp], edx                       ;
        mov       eax, DWORD PTR [12+esp]                       ;
        mov       edx, DWORD PTR [8+esp]                        ;
        mov       ecx, DWORD PTR [16+esp]                       ;
        mov       esi, DWORD PTR [20+esp]                       ;
                                ; LOE eax edx ecx ebx esi edi
.B1.228:                        ; Preds .B1.228 .B1.227         ; Infreq
        movdqu    xmm0, XMMWORD PTR [4+edx+esi*4]               ;68.3
        movdqa    XMMWORD PTR [4+ecx+esi*4], xmm0               ;68.3
        add       esi, 4                                        ;68.3
        cmp       esi, eax                                      ;68.3
        jb        .B1.228       ; Prob 82%                      ;68.3
        jmp       .B1.232       ; Prob 100%                     ;68.3
                                ; LOE eax edx ecx ebx esi edi
.B1.230:                        ; Preds .B1.226                 ; Infreq
        mov       DWORD PTR [esp], esi                          ;
        mov       DWORD PTR [24+esp], edx                       ;
        mov       eax, DWORD PTR [12+esp]                       ;
        mov       edx, DWORD PTR [8+esp]                        ;
        mov       ecx, DWORD PTR [16+esp]                       ;
        mov       esi, DWORD PTR [20+esp]                       ;
                                ; LOE eax edx ecx ebx esi edi
.B1.231:                        ; Preds .B1.231 .B1.230         ; Infreq
        movdqa    xmm0, XMMWORD PTR [4+edx+esi*4]               ;68.3
        movdqa    XMMWORD PTR [4+ecx+esi*4], xmm0               ;68.3
        add       esi, 4                                        ;68.3
        cmp       esi, eax                                      ;68.3
        jb        .B1.231       ; Prob 82%                      ;68.3
                                ; LOE eax edx ecx ebx esi edi
.B1.232:                        ; Preds .B1.228 .B1.231         ; Infreq
        mov       DWORD PTR [12+esp], eax                       ;
        mov       esi, DWORD PTR [esp]                          ;
        mov       edx, DWORD PTR [24+esp]                       ;
                                ; LOE edx ebx esi edi
.B1.233:                        ; Preds .B1.232 .B1.237         ; Infreq
        mov       eax, DWORD PTR [52+esp]                       ;68.3
        cmp       eax, DWORD PTR [12+esp]                       ;68.3
        jbe       .B1.164       ; Prob 10%                      ;68.3
                                ; LOE edx ebx esi edi
.B1.234:                        ; Preds .B1.233                 ; Infreq
        mov       ecx, edx                                      ;
        mov       eax, esi                                      ;
        sub       ecx, edi                                      ;
        sub       eax, ebx                                      ;
        add       ecx, eax                                      ;
        mov       eax, DWORD PTR [40+esp]                       ;
        neg       eax                                           ;
        mov       DWORD PTR [8+esp], ecx                        ;
        add       eax, 3                                        ;
        mov       ecx, DWORD PTR [48+esp]                       ;
        imul      eax, DWORD PTR [44+esp]                       ;
        mov       DWORD PTR [36+esp], edi                       ;
        shl       ecx, 2                                        ;
        neg       ecx                                           ;
        add       ecx, DWORD PTR [32+esp]                       ;
        mov       DWORD PTR [esp], esi                          ;
        add       eax, ecx                                      ;
        mov       DWORD PTR [24+esp], edx                       ;
        mov       ecx, DWORD PTR [8+esp]                        ;
        mov       esi, DWORD PTR [12+esp]                       ;
        mov       edi, DWORD PTR [52+esp]                       ;
                                ; LOE eax ecx ebx esi edi
.B1.235:                        ; Preds .B1.235 .B1.234         ; Infreq
        mov       edx, DWORD PTR [4+eax+esi*4]                  ;68.3
        mov       DWORD PTR [4+ecx+esi*4], edx                  ;68.3
        inc       esi                                           ;68.3
        cmp       esi, edi                                      ;68.3
        jb        .B1.235       ; Prob 82%                      ;68.3
                                ; LOE eax ecx ebx esi edi
.B1.236:                        ; Preds .B1.235                 ; Infreq
        mov       edi, DWORD PTR [36+esp]                       ;
        mov       esi, DWORD PTR [esp]                          ;
        mov       edx, DWORD PTR [24+esp]                       ;
        jmp       .B1.164       ; Prob 100%                     ;
                                ; LOE edx ebx esi edi
.B1.237:                        ; Preds .B1.217 .B1.221 .B1.219 ; Infreq
        xor       eax, eax                                      ;68.3
        mov       DWORD PTR [12+esp], eax                       ;68.3
        jmp       .B1.233       ; Prob 100%                     ;68.3
                                ; LOE edx ebx esi edi
.B1.240:                        ; Preds .B1.158                 ; Infreq
        imul      edi, esi                                      ;
        shl       ebx, 2                                        ;
        cmp       DWORD PTR [52+esp], 4                         ;67.3
        jl        .B1.260       ; Prob 10%                      ;67.3
                                ; LOE edx ebx esi edi
.B1.241:                        ; Preds .B1.240                 ; Infreq
        mov       eax, edx                                      ;67.3
        lea       ecx, DWORD PTR [esi+esi*2]                    ;67.3
        sub       eax, edi                                      ;67.3
        mov       DWORD PTR [20+esp], eax                       ;67.3
        mov       DWORD PTR [16+esp], ecx                       ;67.3
        add       eax, ecx                                      ;67.3
        sub       eax, ebx                                      ;67.3
        add       eax, 4                                        ;67.3
        and       eax, 15                                       ;67.3
        mov       DWORD PTR [56+esp], eax                       ;67.3
        je        .B1.244       ; Prob 50%                      ;67.3
                                ; LOE eax edx ebx esi edi al ah
.B1.242:                        ; Preds .B1.241                 ; Infreq
        test      BYTE PTR [56+esp], 3                          ;67.3
        jne       .B1.260       ; Prob 10%                      ;67.3
                                ; LOE eax edx ebx esi edi al ah
.B1.243:                        ; Preds .B1.242                 ; Infreq
        neg       eax                                           ;67.3
        add       eax, 16                                       ;67.3
        shr       eax, 2                                        ;67.3
        mov       DWORD PTR [56+esp], eax                       ;67.3
                                ; LOE eax edx ebx esi edi al ah
.B1.244:                        ; Preds .B1.243 .B1.241         ; Infreq
        lea       ecx, DWORD PTR [4+eax]                        ;67.3
        cmp       ecx, DWORD PTR [52+esp]                       ;67.3
        jg        .B1.260       ; Prob 10%                      ;67.3
                                ; LOE edx ebx esi edi
.B1.245:                        ; Preds .B1.244                 ; Infreq
        mov       DWORD PTR [36+esp], edi                       ;
        mov       edi, DWORD PTR [52+esp]                       ;67.3
        mov       eax, edi                                      ;67.3
        mov       ecx, DWORD PTR [56+esp]                       ;67.3
        sub       eax, ecx                                      ;67.3
        and       eax, 3                                        ;67.3
        neg       eax                                           ;67.3
        add       eax, edi                                      ;67.3
        mov       DWORD PTR [12+esp], eax                       ;67.3
        mov       eax, DWORD PTR [40+esp]                       ;
        neg       eax                                           ;
        mov       edi, DWORD PTR [48+esp]                       ;
        add       eax, 2                                        ;
        imul      eax, DWORD PTR [44+esp]                       ;
        shl       edi, 2                                        ;
        neg       edi                                           ;
        add       edi, DWORD PTR [32+esp]                       ;
        add       eax, edi                                      ;
        mov       DWORD PTR [8+esp], eax                        ;
        mov       eax, DWORD PTR [16+esp]                       ;
        sub       eax, ebx                                      ;
        add       DWORD PTR [20+esp], eax                       ;
        mov       edi, DWORD PTR [36+esp]                       ;67.3
        test      ecx, ecx                                      ;67.3
        jbe       .B1.249       ; Prob 10%                      ;67.3
                                ; LOE edx ebx esi edi
.B1.246:                        ; Preds .B1.245                 ; Infreq
        mov       DWORD PTR [36+esp], edi                       ;
        xor       eax, eax                                      ;
        mov       DWORD PTR [esp], esi                          ;
        mov       DWORD PTR [24+esp], edx                       ;
        mov       ecx, DWORD PTR [20+esp]                       ;
        mov       esi, DWORD PTR [8+esp]                        ;
        mov       edi, DWORD PTR [56+esp]                       ;
                                ; LOE eax ecx ebx esi edi
.B1.247:                        ; Preds .B1.247 .B1.246         ; Infreq
        mov       edx, DWORD PTR [4+esi+eax*4]                  ;67.3
        mov       DWORD PTR [4+ecx+eax*4], edx                  ;67.3
        inc       eax                                           ;67.3
        cmp       eax, edi                                      ;67.3
        jb        .B1.247       ; Prob 82%                      ;67.3
                                ; LOE eax ecx ebx esi edi
.B1.248:                        ; Preds .B1.247                 ; Infreq
        mov       DWORD PTR [56+esp], edi                       ;
        mov       edi, DWORD PTR [36+esp]                       ;
        mov       esi, DWORD PTR [esp]                          ;
        mov       edx, DWORD PTR [24+esp]                       ;
                                ; LOE edx ebx esi edi
.B1.249:                        ; Preds .B1.245 .B1.248         ; Infreq
        mov       eax, DWORD PTR [8+esp]                        ;67.3
        mov       ecx, DWORD PTR [56+esp]                       ;67.3
        lea       eax, DWORD PTR [4+eax+ecx*4]                  ;67.3
        test      al, 15                                        ;67.3
        je        .B1.253       ; Prob 60%                      ;67.3
                                ; LOE edx ebx esi edi
.B1.250:                        ; Preds .B1.249                 ; Infreq
        mov       DWORD PTR [esp], esi                          ;
        mov       DWORD PTR [24+esp], edx                       ;
        mov       eax, DWORD PTR [12+esp]                       ;
        mov       edx, DWORD PTR [20+esp]                       ;
        mov       ecx, DWORD PTR [8+esp]                        ;
        mov       esi, DWORD PTR [56+esp]                       ;
                                ; LOE eax edx ecx ebx esi edi
.B1.251:                        ; Preds .B1.251 .B1.250         ; Infreq
        movdqu    xmm0, XMMWORD PTR [4+ecx+esi*4]               ;67.3
        movdqa    XMMWORD PTR [4+edx+esi*4], xmm0               ;67.3
        add       esi, 4                                        ;67.3
        cmp       esi, eax                                      ;67.3
        jb        .B1.251       ; Prob 82%                      ;67.3
        jmp       .B1.255       ; Prob 100%                     ;67.3
                                ; LOE eax edx ecx ebx esi edi
.B1.253:                        ; Preds .B1.249                 ; Infreq
        mov       DWORD PTR [esp], esi                          ;
        mov       DWORD PTR [24+esp], edx                       ;
        mov       eax, DWORD PTR [12+esp]                       ;
        mov       edx, DWORD PTR [20+esp]                       ;
        mov       ecx, DWORD PTR [8+esp]                        ;
        mov       esi, DWORD PTR [56+esp]                       ;
                                ; LOE eax edx ecx ebx esi edi
.B1.254:                        ; Preds .B1.254 .B1.253         ; Infreq
        movdqa    xmm0, XMMWORD PTR [4+ecx+esi*4]               ;67.3
        movdqa    XMMWORD PTR [4+edx+esi*4], xmm0               ;67.3
        add       esi, 4                                        ;67.3
        cmp       esi, eax                                      ;67.3
        jb        .B1.254       ; Prob 82%                      ;67.3
                                ; LOE eax edx ecx ebx esi edi
.B1.255:                        ; Preds .B1.251 .B1.254         ; Infreq
        mov       DWORD PTR [12+esp], eax                       ;
        mov       esi, DWORD PTR [esp]                          ;
        mov       edx, DWORD PTR [24+esp]                       ;
                                ; LOE edx ebx esi edi
.B1.256:                        ; Preds .B1.255 .B1.260         ; Infreq
        mov       eax, DWORD PTR [52+esp]                       ;67.3
        cmp       eax, DWORD PTR [12+esp]                       ;67.3
        jbe       .B1.161       ; Prob 10%                      ;67.3
                                ; LOE edx ebx esi edi
.B1.257:                        ; Preds .B1.256                 ; Infreq
        mov       eax, DWORD PTR [40+esp]                       ;
        neg       eax                                           ;
        mov       ecx, DWORD PTR [48+esp]                       ;
        add       eax, 2                                        ;
        imul      eax, DWORD PTR [44+esp]                       ;
        mov       DWORD PTR [36+esp], edi                       ;
        shl       ecx, 2                                        ;
        neg       ecx                                           ;
        add       ecx, DWORD PTR [32+esp]                       ;
        add       eax, ecx                                      ;
        lea       ecx, DWORD PTR [esi+esi*2]                    ;67.3
        mov       DWORD PTR [8+esp], eax                        ;
        mov       eax, edx                                      ;
        sub       eax, edi                                      ;
        sub       ecx, ebx                                      ;
        mov       DWORD PTR [esp], esi                          ;
        add       eax, ecx                                      ;
        mov       DWORD PTR [24+esp], edx                       ;
        mov       ecx, DWORD PTR [8+esp]                        ;
        mov       esi, DWORD PTR [12+esp]                       ;
        mov       edi, DWORD PTR [52+esp]                       ;
                                ; LOE eax ecx ebx esi edi
.B1.258:                        ; Preds .B1.258 .B1.257         ; Infreq
        mov       edx, DWORD PTR [4+ecx+esi*4]                  ;67.3
        mov       DWORD PTR [4+eax+esi*4], edx                  ;67.3
        inc       esi                                           ;67.3
        cmp       esi, edi                                      ;67.3
        jb        .B1.258       ; Prob 82%                      ;67.3
                                ; LOE eax ecx ebx esi edi
.B1.259:                        ; Preds .B1.258                 ; Infreq
        mov       edi, DWORD PTR [36+esp]                       ;
        mov       esi, DWORD PTR [esp]                          ;
        mov       edx, DWORD PTR [24+esp]                       ;
        jmp       .B1.161       ; Prob 100%                     ;
                                ; LOE edx ebx esi edi
.B1.260:                        ; Preds .B1.240 .B1.244 .B1.242 ; Infreq
        xor       eax, eax                                      ;67.3
        mov       DWORD PTR [12+esp], eax                       ;67.3
        jmp       .B1.256       ; Prob 100%                     ;67.3
                                ; LOE edx ebx esi edi
.B1.263:                        ; Preds .B1.155                 ; Infreq
        cmp       DWORD PTR [52+esp], 4                         ;66.3
        jl        .B1.283       ; Prob 10%                      ;66.3
                                ; LOE edx ebx esi edi
.B1.264:                        ; Preds .B1.263                 ; Infreq
        mov       eax, edi                                      ;66.3
        lea       ecx, DWORD PTR [esi+esi]                      ;66.3
        imul      eax, esi                                      ;66.3
        neg       eax                                           ;66.3
        add       eax, edx                                      ;66.3
        mov       DWORD PTR [4+esp], ebx                        ;
        shl       ebx, 2                                        ;66.3
        mov       DWORD PTR [16+esp], eax                       ;66.3
        mov       DWORD PTR [24+esp], ebx                       ;66.3
        mov       DWORD PTR [esp], ecx                          ;66.3
        lea       eax, DWORD PTR [eax+esi*2]                    ;66.3
        sub       eax, ebx                                      ;66.3
        add       eax, 4                                        ;66.3
        mov       ebx, DWORD PTR [4+esp]                        ;66.3
        and       eax, 15                                       ;66.3
        mov       DWORD PTR [20+esp], eax                       ;66.3
        je        .B1.267       ; Prob 50%                      ;66.3
                                ; LOE eax edx ebx esi edi al bl ah bh
.B1.265:                        ; Preds .B1.264                 ; Infreq
        test      BYTE PTR [20+esp], 3                          ;66.3
        jne       .B1.283       ; Prob 10%                      ;66.3
                                ; LOE eax edx ebx esi edi al bl ah bh
.B1.266:                        ; Preds .B1.265                 ; Infreq
        neg       eax                                           ;66.3
        add       eax, 16                                       ;66.3
        shr       eax, 2                                        ;66.3
        mov       DWORD PTR [20+esp], eax                       ;66.3
                                ; LOE eax edx ebx esi edi al bl ah bh
.B1.267:                        ; Preds .B1.266 .B1.264         ; Infreq
        lea       ecx, DWORD PTR [4+eax]                        ;66.3
        cmp       ecx, DWORD PTR [52+esp]                       ;66.3
        jg        .B1.283       ; Prob 10%                      ;66.3
                                ; LOE eax edx ebx esi edi al bl ah bh
.B1.268:                        ; Preds .B1.267                 ; Infreq
        mov       ecx, DWORD PTR [52+esp]                       ;66.3
        mov       DWORD PTR [4+esp], ebx                        ;
        mov       ebx, ecx                                      ;66.3
        sub       ebx, eax                                      ;66.3
        and       ebx, 3                                        ;66.3
        neg       ebx                                           ;66.3
        add       ebx, ecx                                      ;66.3
        mov       ecx, DWORD PTR [48+esp]                       ;
        mov       DWORD PTR [36+esp], edi                       ;
        mov       edi, DWORD PTR [esp]                          ;
        sub       edi, DWORD PTR [24+esp]                       ;
        mov       DWORD PTR [8+esp], ebx                        ;66.3
        mov       ebx, DWORD PTR [44+esp]                       ;
        add       DWORD PTR [16+esp], edi                       ;
        lea       edi, DWORD PTR [ecx*4]                        ;
        mov       ecx, ebx                                      ;
        neg       edi                                           ;
        imul      ecx, DWORD PTR [40+esp]                       ;
        neg       ecx                                           ;
        add       edi, DWORD PTR [32+esp]                       ;
        add       ecx, ebx                                      ;
        add       edi, ecx                                      ;
        mov       DWORD PTR [12+esp], edi                       ;
        test      eax, eax                                      ;66.3
        mov       ebx, DWORD PTR [4+esp]                        ;66.3
        mov       edi, DWORD PTR [36+esp]                       ;66.3
        jbe       .B1.272       ; Prob 0%                       ;66.3
                                ; LOE edx ebx esi edi bl bh
.B1.269:                        ; Preds .B1.268                 ; Infreq
        mov       DWORD PTR [36+esp], edi                       ;
        xor       eax, eax                                      ;
        mov       DWORD PTR [esp], esi                          ;
        mov       DWORD PTR [24+esp], edx                       ;
        mov       ecx, DWORD PTR [12+esp]                       ;
        mov       esi, DWORD PTR [16+esp]                       ;
        mov       edi, DWORD PTR [20+esp]                       ;
                                ; LOE eax ecx ebx esi edi
.B1.270:                        ; Preds .B1.270 .B1.269         ; Infreq
        mov       edx, DWORD PTR [4+ecx+eax*4]                  ;66.3
        mov       DWORD PTR [4+esi+eax*4], edx                  ;66.3
        inc       eax                                           ;66.3
        cmp       eax, edi                                      ;66.3
        jb        .B1.270       ; Prob 82%                      ;66.3
                                ; LOE eax ecx ebx esi edi
.B1.271:                        ; Preds .B1.270                 ; Infreq
        mov       DWORD PTR [20+esp], edi                       ;
        mov       edi, DWORD PTR [36+esp]                       ;
        mov       esi, DWORD PTR [esp]                          ;
        mov       edx, DWORD PTR [24+esp]                       ;
                                ; LOE edx ebx esi edi
.B1.272:                        ; Preds .B1.268 .B1.271         ; Infreq
        mov       eax, DWORD PTR [12+esp]                       ;66.3
        mov       ecx, DWORD PTR [20+esp]                       ;66.3
        lea       eax, DWORD PTR [4+eax+ecx*4]                  ;66.3
        test      al, 15                                        ;66.3
        je        .B1.276       ; Prob 60%                      ;66.3
                                ; LOE edx ebx esi edi
.B1.273:                        ; Preds .B1.272                 ; Infreq
        mov       DWORD PTR [esp], esi                          ;
        mov       DWORD PTR [24+esp], edx                       ;
        mov       eax, DWORD PTR [8+esp]                        ;
        mov       edx, DWORD PTR [12+esp]                       ;
        mov       ecx, DWORD PTR [16+esp]                       ;
        mov       esi, DWORD PTR [20+esp]                       ;
                                ; LOE eax edx ecx ebx esi edi
.B1.274:                        ; Preds .B1.274 .B1.273         ; Infreq
        movdqu    xmm0, XMMWORD PTR [4+edx+esi*4]               ;66.3
        movdqa    XMMWORD PTR [4+ecx+esi*4], xmm0               ;66.3
        add       esi, 4                                        ;66.3
        cmp       esi, eax                                      ;66.3
        jb        .B1.274       ; Prob 82%                      ;66.3
        jmp       .B1.278       ; Prob 100%                     ;66.3
                                ; LOE eax edx ecx ebx esi edi
.B1.276:                        ; Preds .B1.272                 ; Infreq
        mov       DWORD PTR [esp], esi                          ;
        mov       DWORD PTR [24+esp], edx                       ;
        mov       eax, DWORD PTR [8+esp]                        ;
        mov       edx, DWORD PTR [12+esp]                       ;
        mov       ecx, DWORD PTR [16+esp]                       ;
        mov       esi, DWORD PTR [20+esp]                       ;
                                ; LOE eax edx ecx ebx esi edi
.B1.277:                        ; Preds .B1.277 .B1.276         ; Infreq
        movdqa    xmm0, XMMWORD PTR [4+edx+esi*4]               ;66.3
        movdqa    XMMWORD PTR [4+ecx+esi*4], xmm0               ;66.3
        add       esi, 4                                        ;66.3
        cmp       esi, eax                                      ;66.3
        jb        .B1.277       ; Prob 82%                      ;66.3
                                ; LOE eax edx ecx ebx esi edi
.B1.278:                        ; Preds .B1.274 .B1.277         ; Infreq
        mov       DWORD PTR [8+esp], eax                        ;
        mov       esi, DWORD PTR [esp]                          ;
        mov       edx, DWORD PTR [24+esp]                       ;
                                ; LOE edx ebx esi edi
.B1.279:                        ; Preds .B1.278 .B1.283         ; Infreq
        mov       eax, DWORD PTR [52+esp]                       ;66.3
        cmp       eax, DWORD PTR [8+esp]                        ;66.3
        jbe       .B1.158       ; Prob 0%                       ;66.3
                                ; LOE edx ebx esi edi
.B1.280:                        ; Preds .B1.279                 ; Infreq
        mov       eax, edi                                      ;
        neg       eax                                           ;
        add       eax, 2                                        ;
        imul      eax, esi                                      ;
        mov       DWORD PTR [4+esp], ebx                        ;
        shl       ebx, 2                                        ;
        neg       ebx                                           ;
        add       ebx, edx                                      ;
        add       eax, ebx                                      ;
        mov       DWORD PTR [12+esp], eax                       ;
        mov       eax, DWORD PTR [44+esp]                       ;
        mov       ebx, eax                                      ;
        imul      ebx, DWORD PTR [40+esp]                       ;
        mov       ecx, DWORD PTR [48+esp]                       ;
        neg       ebx                                           ;
        add       ebx, eax                                      ;
        mov       DWORD PTR [36+esp], edi                       ;
        mov       DWORD PTR [esp], esi                          ;
        shl       ecx, 2                                        ;
        neg       ecx                                           ;
        add       ecx, DWORD PTR [32+esp]                       ;
        add       ecx, ebx                                      ;
        mov       DWORD PTR [24+esp], edx                       ;
        mov       ebx, DWORD PTR [4+esp]                        ;
        mov       edx, ecx                                      ;
        mov       ecx, DWORD PTR [12+esp]                       ;
        mov       esi, DWORD PTR [8+esp]                        ;
        mov       edi, DWORD PTR [52+esp]                       ;
                                ; LOE edx ecx ebx esi edi
.B1.281:                        ; Preds .B1.281 .B1.280         ; Infreq
        mov       eax, DWORD PTR [4+edx+esi*4]                  ;66.3
        mov       DWORD PTR [4+ecx+esi*4], eax                  ;66.3
        inc       esi                                           ;66.3
        cmp       esi, edi                                      ;66.3
        jb        .B1.281       ; Prob 82%                      ;66.3
                                ; LOE edx ecx ebx esi edi
.B1.282:                        ; Preds .B1.281                 ; Infreq
        mov       edi, DWORD PTR [36+esp]                       ;
        mov       esi, DWORD PTR [esp]                          ;
        mov       edx, DWORD PTR [24+esp]                       ;
        jmp       .B1.158       ; Prob 100%                     ;
                                ; LOE edx ebx esi edi
.B1.283:                        ; Preds .B1.263 .B1.267 .B1.265 ; Infreq
        xor       eax, eax                                      ;66.3
        mov       DWORD PTR [8+esp], eax                        ;66.3
        jmp       .B1.279       ; Prob 100%                     ;66.3
                                ; LOE edx ebx esi edi
.B1.286:                        ; Preds .B1.154                 ; Infreq
        imul      edi, esi                                      ;
        shl       ebx, 2                                        ;
        jmp       .B1.169       ; Prob 100%                     ;
                                ; LOE edx ebx esi edi
.B1.287:                        ; Preds .B1.151                 ; Infreq
        cmp       DWORD PTR [44+esp], 4                         ;62.3
        jl        .B1.307       ; Prob 10%                      ;62.3
                                ; LOE eax edx ecx
.B1.288:                        ; Preds .B1.287                 ; Infreq
        mov       edi, DWORD PTR [4+esp]                        ;62.3
        lea       ebx, DWORD PTR [edx*4]                        ;62.3
        mov       esi, edi                                      ;62.3
        neg       ebx                                           ;62.3
        imul      esi, DWORD PTR [esp]                          ;62.3
        add       ebx, DWORD PTR [40+esp]                       ;62.3
        lea       edi, DWORD PTR [edi+edi*4]                    ;62.3
        mov       DWORD PTR [20+esp], edi                       ;62.3
        mov       DWORD PTR [52+esp], ebx                       ;62.3
        mov       DWORD PTR [48+esp], esi                       ;62.3
        add       edi, ebx                                      ;62.3
        sub       edi, esi                                      ;62.3
        add       edi, 4                                        ;62.3
        and       edi, 15                                       ;62.3
        je        .B1.291       ; Prob 50%                      ;62.3
                                ; LOE eax edx ecx edi
.B1.289:                        ; Preds .B1.288                 ; Infreq
        test      edi, 3                                        ;62.3
        jne       .B1.307       ; Prob 10%                      ;62.3
                                ; LOE eax edx ecx edi
.B1.290:                        ; Preds .B1.289                 ; Infreq
        neg       edi                                           ;62.3
        add       edi, 16                                       ;62.3
        shr       edi, 2                                        ;62.3
                                ; LOE eax edx ecx edi
.B1.291:                        ; Preds .B1.290 .B1.288         ; Infreq
        lea       ebx, DWORD PTR [4+edi]                        ;62.3
        cmp       ebx, DWORD PTR [44+esp]                       ;62.3
        jg        .B1.307       ; Prob 10%                      ;62.3
                                ; LOE eax edx ecx edi
.B1.292:                        ; Preds .B1.291                 ; Infreq
        mov       ebx, DWORD PTR [44+esp]                       ;62.3
        mov       esi, ebx                                      ;62.3
        sub       esi, edi                                      ;62.3
        and       esi, 3                                        ;62.3
        neg       esi                                           ;62.3
        add       esi, ebx                                      ;62.3
        mov       ebx, DWORD PTR [20+esp]                       ;
        sub       ebx, DWORD PTR [48+esp]                       ;
        add       DWORD PTR [52+esp], ebx                       ;
        mov       ebx, eax                                      ;
        neg       ebx                                           ;
        add       ebx, 5                                        ;
        imul      ebx, DWORD PTR [32+esp]                       ;
        mov       DWORD PTR [16+esp], esi                       ;62.3
        lea       esi, DWORD PTR [ecx*4]                        ;
        neg       esi                                           ;
        add       esi, DWORD PTR [36+esp]                       ;
        add       ebx, esi                                      ;
        mov       DWORD PTR [24+esp], ebx                       ;
        test      edi, edi                                      ;62.3
        jbe       .B1.296       ; Prob 10%                      ;62.3
                                ; LOE eax edx ecx edi
.B1.293:                        ; Preds .B1.292                 ; Infreq
        mov       DWORD PTR [8+esp], ecx                        ;
        xor       ebx, ebx                                      ;
        mov       DWORD PTR [12+esp], edx                       ;
        mov       ecx, DWORD PTR [24+esp]                       ;
        mov       esi, DWORD PTR [52+esp]                       ;
                                ; LOE eax ecx ebx esi edi
.B1.294:                        ; Preds .B1.294 .B1.293         ; Infreq
        mov       edx, DWORD PTR [4+ecx+ebx*4]                  ;62.3
        mov       DWORD PTR [4+esi+ebx*4], edx                  ;62.3
        inc       ebx                                           ;62.3
        cmp       ebx, edi                                      ;62.3
        jb        .B1.294       ; Prob 82%                      ;62.3
                                ; LOE eax ecx ebx esi edi
.B1.295:                        ; Preds .B1.294                 ; Infreq
        mov       ecx, DWORD PTR [8+esp]                        ;
        mov       edx, DWORD PTR [12+esp]                       ;
                                ; LOE eax edx ecx edi
.B1.296:                        ; Preds .B1.292 .B1.295         ; Infreq
        mov       ebx, DWORD PTR [24+esp]                       ;62.3
        lea       esi, DWORD PTR [4+ebx+edi*4]                  ;62.3
        test      esi, 15                                       ;62.3
        je        .B1.300       ; Prob 60%                      ;62.3
                                ; LOE eax edx ecx ebx edi bl bh
.B1.297:                        ; Preds .B1.296                 ; Infreq
        mov       DWORD PTR [12+esp], edx                       ;
        mov       edx, DWORD PTR [16+esp]                       ;
        mov       esi, DWORD PTR [52+esp]                       ;
                                ; LOE eax edx ecx ebx esi edi
.B1.298:                        ; Preds .B1.298 .B1.297         ; Infreq
        movdqu    xmm0, XMMWORD PTR [4+ebx+edi*4]               ;62.3
        movdqa    XMMWORD PTR [4+esi+edi*4], xmm0               ;62.3
        add       edi, 4                                        ;62.3
        cmp       edi, edx                                      ;62.3
        jb        .B1.298       ; Prob 82%                      ;62.3
        jmp       .B1.302       ; Prob 100%                     ;62.3
                                ; LOE eax edx ecx ebx esi edi
.B1.300:                        ; Preds .B1.296                 ; Infreq
        mov       DWORD PTR [12+esp], edx                       ;
        mov       edx, DWORD PTR [16+esp]                       ;
        mov       esi, DWORD PTR [52+esp]                       ;
                                ; LOE eax edx ecx ebx esi edi
.B1.301:                        ; Preds .B1.301 .B1.300         ; Infreq
        movdqa    xmm0, XMMWORD PTR [4+ebx+edi*4]               ;62.3
        movdqa    XMMWORD PTR [4+esi+edi*4], xmm0               ;62.3
        add       edi, 4                                        ;62.3
        cmp       edi, edx                                      ;62.3
        jb        .B1.301       ; Prob 82%                      ;62.3
                                ; LOE eax edx ecx ebx esi edi
.B1.302:                        ; Preds .B1.298 .B1.301         ; Infreq
        mov       DWORD PTR [16+esp], edx                       ;
        mov       edx, DWORD PTR [12+esp]                       ;
                                ; LOE eax edx ecx
.B1.303:                        ; Preds .B1.302 .B1.307         ; Infreq
        mov       ebx, DWORD PTR [44+esp]                       ;62.3
        cmp       ebx, DWORD PTR [16+esp]                       ;62.3
        jbe       .B1.153       ; Prob 10%                      ;62.3
                                ; LOE eax edx ecx
.B1.304:                        ; Preds .B1.303                 ; Infreq
        mov       ebx, DWORD PTR [esp]                          ;
        neg       eax                                           ;
        neg       ebx                                           ;
        add       eax, 5                                        ;
        add       ebx, 5                                        ;
        imul      ebx, DWORD PTR [4+esp]                        ;
        imul      eax, DWORD PTR [32+esp]                       ;
        shl       edx, 2                                        ;
        neg       edx                                           ;
        shl       ecx, 2                                        ;
        add       edx, DWORD PTR [40+esp]                       ;
        neg       ecx                                           ;
        add       ebx, edx                                      ;
        mov       edx, DWORD PTR [36+esp]                       ;
        mov       DWORD PTR [esp], ebx                          ;
        add       edx, ecx                                      ;
        mov       ecx, ebx                                      ;
        add       eax, edx                                      ;
        mov       ebx, DWORD PTR [16+esp]                       ;
        mov       esi, DWORD PTR [44+esp]                       ;
                                ; LOE eax ecx ebx esi
.B1.305:                        ; Preds .B1.305 .B1.304         ; Infreq
        mov       edx, DWORD PTR [4+eax+ebx*4]                  ;62.3
        mov       DWORD PTR [4+ecx+ebx*4], edx                  ;62.3
        inc       ebx                                           ;62.3
        cmp       ebx, esi                                      ;62.3
        jb        .B1.305       ; Prob 82%                      ;62.3
        jmp       .B1.153       ; Prob 100%                     ;62.3
                                ; LOE eax ecx ebx esi
.B1.307:                        ; Preds .B1.289 .B1.287 .B1.291 ; Infreq
        xor       ebx, ebx                                      ;62.3
        mov       DWORD PTR [16+esp], ebx                       ;62.3
        jmp       .B1.303       ; Prob 100%                     ;62.3
                                ; LOE eax edx ecx
.B1.309:                        ; Preds .B1.148                 ; Infreq
        cmp       DWORD PTR [44+esp], 4                         ;61.3
        jl        .B1.329       ; Prob 10%                      ;61.3
                                ; LOE eax edx ecx
.B1.310:                        ; Preds .B1.309                 ; Infreq
        mov       edi, DWORD PTR [4+esp]                        ;61.3
        lea       esi, DWORD PTR [edx*4]                        ;61.3
        mov       DWORD PTR [16+esp], eax                       ;
        mov       eax, edi                                      ;61.3
        neg       esi                                           ;61.3
        imul      eax, DWORD PTR [esp]                          ;61.3
        lea       ebx, DWORD PTR [edi*4]                        ;61.3
        add       esi, DWORD PTR [40+esp]                       ;61.3
        mov       DWORD PTR [48+esp], esi                       ;61.3
        mov       DWORD PTR [24+esp], eax                       ;61.3
        mov       DWORD PTR [20+esp], ebx                       ;61.3
        lea       esi, DWORD PTR [esi+edi*4]                    ;61.3
        sub       esi, eax                                      ;61.3
        add       esi, 4                                        ;61.3
        mov       eax, DWORD PTR [16+esp]                       ;61.3
        and       esi, 15                                       ;61.3
        je        .B1.313       ; Prob 50%                      ;61.3
                                ; LOE eax edx ecx esi al ah
.B1.311:                        ; Preds .B1.310                 ; Infreq
        test      esi, 3                                        ;61.3
        jne       .B1.329       ; Prob 10%                      ;61.3
                                ; LOE eax edx ecx esi al ah
.B1.312:                        ; Preds .B1.311                 ; Infreq
        neg       esi                                           ;61.3
        add       esi, 16                                       ;61.3
        shr       esi, 2                                        ;61.3
                                ; LOE eax edx ecx esi al ah
.B1.313:                        ; Preds .B1.312 .B1.310         ; Infreq
        lea       ebx, DWORD PTR [4+esi]                        ;61.3
        cmp       ebx, DWORD PTR [44+esp]                       ;61.3
        jg        .B1.329       ; Prob 10%                      ;61.3
                                ; LOE eax edx ecx esi al ah
.B1.314:                        ; Preds .B1.313                 ; Infreq
        mov       ebx, DWORD PTR [44+esp]                       ;61.3
        mov       edi, ebx                                      ;61.3
        sub       edi, esi                                      ;61.3
        and       edi, 3                                        ;61.3
        neg       edi                                           ;61.3
        add       edi, ebx                                      ;61.3
        lea       ebx, DWORD PTR [ecx*4]                        ;
        mov       DWORD PTR [16+esp], edi                       ;61.3
        mov       edi, eax                                      ;
        neg       edi                                           ;
        neg       ebx                                           ;
        add       edi, 4                                        ;
        imul      edi, DWORD PTR [32+esp]                       ;
        add       ebx, DWORD PTR [36+esp]                       ;
        add       edi, ebx                                      ;
        mov       ebx, DWORD PTR [20+esp]                       ;
        sub       ebx, DWORD PTR [24+esp]                       ;
        add       DWORD PTR [48+esp], ebx                       ;
        mov       DWORD PTR [52+esp], edi                       ;
        test      esi, esi                                      ;61.3
        jbe       .B1.318       ; Prob 10%                      ;61.3
                                ; LOE eax edx ecx esi edi al ah
.B1.315:                        ; Preds .B1.314                 ; Infreq
        mov       DWORD PTR [8+esp], ecx                        ;
        xor       ebx, ebx                                      ;
        mov       DWORD PTR [12+esp], edx                       ;
        mov       ecx, DWORD PTR [48+esp]                       ;
                                ; LOE eax ecx ebx esi edi
.B1.316:                        ; Preds .B1.316 .B1.315         ; Infreq
        mov       edx, DWORD PTR [4+edi+ebx*4]                  ;61.3
        mov       DWORD PTR [4+ecx+ebx*4], edx                  ;61.3
        inc       ebx                                           ;61.3
        cmp       ebx, esi                                      ;61.3
        jb        .B1.316       ; Prob 82%                      ;61.3
                                ; LOE eax ecx ebx esi edi
.B1.317:                        ; Preds .B1.316                 ; Infreq
        mov       ecx, DWORD PTR [8+esp]                        ;
        mov       edx, DWORD PTR [12+esp]                       ;
                                ; LOE eax edx ecx esi
.B1.318:                        ; Preds .B1.314 .B1.317         ; Infreq
        mov       ebx, DWORD PTR [52+esp]                       ;61.3
        lea       edi, DWORD PTR [4+ebx+esi*4]                  ;61.3
        test      edi, 15                                       ;61.3
        je        .B1.322       ; Prob 60%                      ;61.3
                                ; LOE eax edx ecx esi
.B1.319:                        ; Preds .B1.318                 ; Infreq
        mov       DWORD PTR [12+esp], edx                       ;
        mov       edx, DWORD PTR [16+esp]                       ;
        mov       ebx, DWORD PTR [48+esp]                       ;
        mov       edi, DWORD PTR [52+esp]                       ;
                                ; LOE eax edx ecx ebx esi edi
.B1.320:                        ; Preds .B1.320 .B1.319         ; Infreq
        movdqu    xmm0, XMMWORD PTR [4+edi+esi*4]               ;61.3
        movdqa    XMMWORD PTR [4+ebx+esi*4], xmm0               ;61.3
        add       esi, 4                                        ;61.3
        cmp       esi, edx                                      ;61.3
        jb        .B1.320       ; Prob 82%                      ;61.3
        jmp       .B1.324       ; Prob 100%                     ;61.3
                                ; LOE eax edx ecx ebx esi edi
.B1.322:                        ; Preds .B1.318                 ; Infreq
        mov       DWORD PTR [12+esp], edx                       ;
        mov       edx, DWORD PTR [16+esp]                       ;
        mov       ebx, DWORD PTR [48+esp]                       ;
        mov       edi, DWORD PTR [52+esp]                       ;
                                ; LOE eax edx ecx ebx esi edi
.B1.323:                        ; Preds .B1.323 .B1.322         ; Infreq
        movdqa    xmm0, XMMWORD PTR [4+edi+esi*4]               ;61.3
        movdqa    XMMWORD PTR [4+ebx+esi*4], xmm0               ;61.3
        add       esi, 4                                        ;61.3
        cmp       esi, edx                                      ;61.3
        jb        .B1.323       ; Prob 82%                      ;61.3
                                ; LOE eax edx ecx ebx esi edi
.B1.324:                        ; Preds .B1.320 .B1.323         ; Infreq
        mov       DWORD PTR [16+esp], edx                       ;
        mov       edx, DWORD PTR [12+esp]                       ;
                                ; LOE eax edx ecx
.B1.325:                        ; Preds .B1.324 .B1.329         ; Infreq
        mov       ebx, DWORD PTR [44+esp]                       ;61.3
        cmp       ebx, DWORD PTR [16+esp]                       ;61.3
        jbe       .B1.151       ; Prob 10%                      ;61.3
                                ; LOE eax edx ecx
.B1.326:                        ; Preds .B1.325                 ; Infreq
        mov       esi, eax                                      ;
        lea       ebx, DWORD PTR [ecx*4]                        ;
        neg       esi                                           ;
        neg       ebx                                           ;
        add       esi, 4                                        ;
        imul      esi, DWORD PTR [32+esp]                       ;
        add       ebx, DWORD PTR [36+esp]                       ;
        add       esi, ebx                                      ;
        mov       ebx, edx                                      ;
        mov       edi, DWORD PTR [4+esp]                        ;
        neg       ebx                                           ;
        add       ebx, edi                                      ;
        imul      edi, DWORD PTR [esp]                          ;
        neg       edi                                           ;
        add       edi, DWORD PTR [40+esp]                       ;
        mov       DWORD PTR [8+esp], ecx                        ;
        mov       DWORD PTR [12+esp], edx                       ;
        lea       ebx, DWORD PTR [edi+ebx*4]                    ;
        mov       ecx, esi                                      ;
        mov       esi, DWORD PTR [16+esp]                       ;
        mov       edi, DWORD PTR [44+esp]                       ;
                                ; LOE eax ecx ebx esi edi
.B1.327:                        ; Preds .B1.327 .B1.326         ; Infreq
        mov       edx, DWORD PTR [4+ecx+esi*4]                  ;61.3
        mov       DWORD PTR [4+ebx+esi*4], edx                  ;61.3
        inc       esi                                           ;61.3
        cmp       esi, edi                                      ;61.3
        jb        .B1.327       ; Prob 82%                      ;61.3
                                ; LOE eax ecx ebx esi edi
.B1.328:                        ; Preds .B1.327                 ; Infreq
        mov       ecx, DWORD PTR [8+esp]                        ;
        mov       edx, DWORD PTR [12+esp]                       ;
        jmp       .B1.151       ; Prob 100%                     ;
                                ; LOE eax edx ecx
.B1.329:                        ; Preds .B1.309 .B1.313 .B1.311 ; Infreq
        xor       ebx, ebx                                      ;61.3
        mov       DWORD PTR [16+esp], ebx                       ;61.3
        jmp       .B1.325       ; Prob 100%                     ;61.3
                                ; LOE eax edx ecx
.B1.332:                        ; Preds .B1.145                 ; Infreq
        cmp       DWORD PTR [44+esp], 4                         ;60.3
        jl        .B1.352       ; Prob 10%                      ;60.3
                                ; LOE eax edx ecx
.B1.333:                        ; Preds .B1.332                 ; Infreq
        mov       edi, DWORD PTR [4+esp]                        ;60.3
        lea       ebx, DWORD PTR [edx*4]                        ;60.3
        mov       esi, edi                                      ;60.3
        neg       ebx                                           ;60.3
        imul      esi, DWORD PTR [esp]                          ;60.3
        add       ebx, DWORD PTR [40+esp]                       ;60.3
        lea       edi, DWORD PTR [edi+edi*2]                    ;60.3
        mov       DWORD PTR [24+esp], edi                       ;60.3
        mov       DWORD PTR [56+esp], ebx                       ;60.3
        mov       DWORD PTR [52+esp], esi                       ;60.3
        add       edi, ebx                                      ;60.3
        sub       edi, esi                                      ;60.3
        add       edi, 4                                        ;60.3
        and       edi, 15                                       ;60.3
        je        .B1.336       ; Prob 50%                      ;60.3
                                ; LOE eax edx ecx edi
.B1.334:                        ; Preds .B1.333                 ; Infreq
        test      edi, 3                                        ;60.3
        jne       .B1.352       ; Prob 10%                      ;60.3
                                ; LOE eax edx ecx edi
.B1.335:                        ; Preds .B1.334                 ; Infreq
        neg       edi                                           ;60.3
        add       edi, 16                                       ;60.3
        shr       edi, 2                                        ;60.3
                                ; LOE eax edx ecx edi
.B1.336:                        ; Preds .B1.335 .B1.333         ; Infreq
        lea       ebx, DWORD PTR [4+edi]                        ;60.3
        cmp       ebx, DWORD PTR [44+esp]                       ;60.3
        jg        .B1.352       ; Prob 10%                      ;60.3
                                ; LOE eax edx ecx edi
.B1.337:                        ; Preds .B1.336                 ; Infreq
        mov       ebx, DWORD PTR [44+esp]                       ;60.3
        mov       esi, ebx                                      ;60.3
        sub       esi, edi                                      ;60.3
        and       esi, 3                                        ;60.3
        neg       esi                                           ;60.3
        add       esi, ebx                                      ;60.3
        mov       ebx, DWORD PTR [32+esp]                       ;
        mov       DWORD PTR [16+esp], eax                       ;
        imul      eax, ebx                                      ;
        mov       DWORD PTR [20+esp], esi                       ;60.3
        lea       esi, DWORD PTR [ecx*4]                        ;
        neg       esi                                           ;
        neg       eax                                           ;
        add       esi, ebx                                      ;
        mov       ebx, DWORD PTR [24+esp]                       ;
        add       eax, DWORD PTR [36+esp]                       ;
        sub       ebx, DWORD PTR [52+esp]                       ;
        add       eax, esi                                      ;
        add       DWORD PTR [56+esp], ebx                       ;
        mov       DWORD PTR [48+esp], eax                       ;
        test      edi, edi                                      ;60.3
        mov       eax, DWORD PTR [16+esp]                       ;60.3
        jbe       .B1.341       ; Prob 10%                      ;60.3
                                ; LOE eax edx ecx edi al ah
.B1.338:                        ; Preds .B1.337                 ; Infreq
        mov       DWORD PTR [8+esp], ecx                        ;
        xor       ebx, ebx                                      ;
        mov       DWORD PTR [12+esp], edx                       ;
        mov       ecx, DWORD PTR [56+esp]                       ;
        mov       esi, DWORD PTR [48+esp]                       ;
                                ; LOE eax ecx ebx esi edi
.B1.339:                        ; Preds .B1.339 .B1.338         ; Infreq
        mov       edx, DWORD PTR [4+esi+ebx*4]                  ;60.3
        mov       DWORD PTR [4+ecx+ebx*4], edx                  ;60.3
        inc       ebx                                           ;60.3
        cmp       ebx, edi                                      ;60.3
        jb        .B1.339       ; Prob 82%                      ;60.3
                                ; LOE eax ecx ebx esi edi
.B1.340:                        ; Preds .B1.339                 ; Infreq
        mov       ecx, DWORD PTR [8+esp]                        ;
        mov       edx, DWORD PTR [12+esp]                       ;
                                ; LOE eax edx ecx edi
.B1.341:                        ; Preds .B1.337 .B1.340         ; Infreq
        mov       ebx, DWORD PTR [48+esp]                       ;60.3
        lea       esi, DWORD PTR [4+ebx+edi*4]                  ;60.3
        test      esi, 15                                       ;60.3
        je        .B1.345       ; Prob 60%                      ;60.3
                                ; LOE eax edx ecx edi
.B1.342:                        ; Preds .B1.341                 ; Infreq
        mov       DWORD PTR [12+esp], edx                       ;
        mov       edx, DWORD PTR [20+esp]                       ;
        mov       ebx, DWORD PTR [56+esp]                       ;
        mov       esi, DWORD PTR [48+esp]                       ;
                                ; LOE eax edx ecx ebx esi edi
.B1.343:                        ; Preds .B1.343 .B1.342         ; Infreq
        movdqu    xmm0, XMMWORD PTR [4+esi+edi*4]               ;60.3
        movdqa    XMMWORD PTR [4+ebx+edi*4], xmm0               ;60.3
        add       edi, 4                                        ;60.3
        cmp       edi, edx                                      ;60.3
        jb        .B1.343       ; Prob 82%                      ;60.3
        jmp       .B1.347       ; Prob 100%                     ;60.3
                                ; LOE eax edx ecx ebx esi edi
.B1.345:                        ; Preds .B1.341                 ; Infreq
        mov       DWORD PTR [12+esp], edx                       ;
        mov       edx, DWORD PTR [20+esp]                       ;
        mov       ebx, DWORD PTR [56+esp]                       ;
        mov       esi, DWORD PTR [48+esp]                       ;
                                ; LOE eax edx ecx ebx esi edi
.B1.346:                        ; Preds .B1.346 .B1.345         ; Infreq
        movdqa    xmm0, XMMWORD PTR [4+esi+edi*4]               ;60.3
        movdqa    XMMWORD PTR [4+ebx+edi*4], xmm0               ;60.3
        add       edi, 4                                        ;60.3
        cmp       edi, edx                                      ;60.3
        jb        .B1.346       ; Prob 82%                      ;60.3
                                ; LOE eax edx ecx ebx esi edi
.B1.347:                        ; Preds .B1.343 .B1.346         ; Infreq
        mov       DWORD PTR [20+esp], edx                       ;
        mov       edx, DWORD PTR [12+esp]                       ;
                                ; LOE eax edx ecx
.B1.348:                        ; Preds .B1.347 .B1.352         ; Infreq
        mov       ebx, DWORD PTR [44+esp]                       ;60.3
        cmp       ebx, DWORD PTR [20+esp]                       ;60.3
        jbe       .B1.148       ; Prob 10%                      ;60.3
                                ; LOE eax edx ecx
.B1.349:                        ; Preds .B1.348                 ; Infreq
        mov       edi, eax                                      ;
        lea       esi, DWORD PTR [ecx*4]                        ;
        mov       ebx, DWORD PTR [32+esp]                       ;
        neg       esi                                           ;
        imul      edi, ebx                                      ;
        add       esi, ebx                                      ;
        mov       ebx, DWORD PTR [esp]                          ;
        neg       edi                                           ;
        neg       ebx                                           ;
        add       ebx, 3                                        ;
        imul      ebx, DWORD PTR [4+esp]                        ;
        add       edi, DWORD PTR [36+esp]                       ;
        add       edi, esi                                      ;
        lea       esi, DWORD PTR [edx*4]                        ;
        neg       esi                                           ;
        add       esi, DWORD PTR [40+esp]                       ;
        add       ebx, esi                                      ;
        mov       DWORD PTR [8+esp], ecx                        ;
        mov       DWORD PTR [12+esp], edx                       ;
        mov       ecx, edi                                      ;
        mov       esi, DWORD PTR [20+esp]                       ;
        mov       edi, DWORD PTR [44+esp]                       ;
                                ; LOE eax ecx ebx esi edi
.B1.350:                        ; Preds .B1.350 .B1.349         ; Infreq
        mov       edx, DWORD PTR [4+ecx+esi*4]                  ;60.3
        mov       DWORD PTR [4+ebx+esi*4], edx                  ;60.3
        inc       esi                                           ;60.3
        cmp       esi, edi                                      ;60.3
        jb        .B1.350       ; Prob 82%                      ;60.3
                                ; LOE eax ecx ebx esi edi
.B1.351:                        ; Preds .B1.350                 ; Infreq
        mov       ecx, DWORD PTR [8+esp]                        ;
        mov       edx, DWORD PTR [12+esp]                       ;
        jmp       .B1.148       ; Prob 100%                     ;
                                ; LOE eax edx ecx
.B1.352:                        ; Preds .B1.332 .B1.336 .B1.334 ; Infreq
        xor       ebx, ebx                                      ;60.3
        mov       DWORD PTR [20+esp], ebx                       ;60.3
        jmp       .B1.348       ; Prob 100%                     ;60.3
                                ; LOE eax edx ecx
.B1.355:                        ; Preds .B1.142                 ; Infreq
        cmp       DWORD PTR [44+esp], 4                         ;59.3
        jl        .B1.375       ; Prob 10%                      ;59.3
                                ; LOE eax edx ecx
.B1.356:                        ; Preds .B1.355                 ; Infreq
        mov       edi, DWORD PTR [4+esp]                        ;59.3
        lea       esi, DWORD PTR [edx*4]                        ;59.3
        mov       DWORD PTR [16+esp], eax                       ;
        mov       eax, edi                                      ;59.3
        neg       esi                                           ;59.3
        imul      eax, DWORD PTR [esp]                          ;59.3
        lea       ebx, DWORD PTR [edi+edi]                      ;59.3
        add       esi, DWORD PTR [40+esp]                       ;59.3
        mov       DWORD PTR [56+esp], esi                       ;59.3
        mov       DWORD PTR [52+esp], eax                       ;59.3
        mov       DWORD PTR [48+esp], ebx                       ;59.3
        lea       esi, DWORD PTR [esi+edi*2]                    ;59.3
        sub       esi, eax                                      ;59.3
        add       esi, 4                                        ;59.3
        mov       eax, DWORD PTR [16+esp]                       ;59.3
        and       esi, 15                                       ;59.3
        je        .B1.359       ; Prob 50%                      ;59.3
                                ; LOE eax edx ecx esi al ah
.B1.357:                        ; Preds .B1.356                 ; Infreq
        test      esi, 3                                        ;59.3
        jne       .B1.375       ; Prob 10%                      ;59.3
                                ; LOE eax edx ecx esi al ah
.B1.358:                        ; Preds .B1.357                 ; Infreq
        neg       esi                                           ;59.3
        add       esi, 16                                       ;59.3
        shr       esi, 2                                        ;59.3
                                ; LOE eax edx ecx esi al ah
.B1.359:                        ; Preds .B1.358 .B1.356         ; Infreq
        lea       ebx, DWORD PTR [4+esi]                        ;59.3
        cmp       ebx, DWORD PTR [44+esp]                       ;59.3
        jg        .B1.375       ; Prob 10%                      ;59.3
                                ; LOE eax edx ecx esi al ah
.B1.360:                        ; Preds .B1.359                 ; Infreq
        mov       ebx, DWORD PTR [44+esp]                       ;59.3
        mov       edi, ebx                                      ;59.3
        sub       edi, esi                                      ;59.3
        and       edi, 3                                        ;59.3
        neg       edi                                           ;59.3
        add       edi, ebx                                      ;59.3
        lea       ebx, DWORD PTR [ecx*4]                        ;
        mov       DWORD PTR [20+esp], edi                       ;59.3
        mov       edi, eax                                      ;
        neg       edi                                           ;
        neg       ebx                                           ;
        add       edi, 3                                        ;
        imul      edi, DWORD PTR [32+esp]                       ;
        add       ebx, DWORD PTR [36+esp]                       ;
        add       edi, ebx                                      ;
        mov       ebx, DWORD PTR [48+esp]                       ;
        sub       ebx, DWORD PTR [52+esp]                       ;
        add       DWORD PTR [56+esp], ebx                       ;
        mov       DWORD PTR [24+esp], edi                       ;
        test      esi, esi                                      ;59.3
        jbe       .B1.364       ; Prob 10%                      ;59.3
                                ; LOE eax edx ecx esi edi al ah
.B1.361:                        ; Preds .B1.360                 ; Infreq
        mov       DWORD PTR [8+esp], ecx                        ;
        xor       ebx, ebx                                      ;
        mov       DWORD PTR [12+esp], edx                       ;
        mov       ecx, DWORD PTR [56+esp]                       ;
                                ; LOE eax ecx ebx esi edi
.B1.362:                        ; Preds .B1.362 .B1.361         ; Infreq
        mov       edx, DWORD PTR [4+edi+ebx*4]                  ;59.3
        mov       DWORD PTR [4+ecx+ebx*4], edx                  ;59.3
        inc       ebx                                           ;59.3
        cmp       ebx, esi                                      ;59.3
        jb        .B1.362       ; Prob 82%                      ;59.3
                                ; LOE eax ecx ebx esi edi
.B1.363:                        ; Preds .B1.362                 ; Infreq
        mov       ecx, DWORD PTR [8+esp]                        ;
        mov       edx, DWORD PTR [12+esp]                       ;
                                ; LOE eax edx ecx esi
.B1.364:                        ; Preds .B1.360 .B1.363         ; Infreq
        mov       ebx, DWORD PTR [24+esp]                       ;59.3
        lea       edi, DWORD PTR [4+ebx+esi*4]                  ;59.3
        test      edi, 15                                       ;59.3
        je        .B1.368       ; Prob 60%                      ;59.3
                                ; LOE eax edx ecx esi
.B1.365:                        ; Preds .B1.364                 ; Infreq
        mov       DWORD PTR [12+esp], edx                       ;
        mov       edx, DWORD PTR [20+esp]                       ;
        mov       ebx, DWORD PTR [56+esp]                       ;
        mov       edi, DWORD PTR [24+esp]                       ;
                                ; LOE eax edx ecx ebx esi edi
.B1.366:                        ; Preds .B1.366 .B1.365         ; Infreq
        movdqu    xmm0, XMMWORD PTR [4+edi+esi*4]               ;59.3
        movdqa    XMMWORD PTR [4+ebx+esi*4], xmm0               ;59.3
        add       esi, 4                                        ;59.3
        cmp       esi, edx                                      ;59.3
        jb        .B1.366       ; Prob 82%                      ;59.3
        jmp       .B1.370       ; Prob 100%                     ;59.3
                                ; LOE eax edx ecx ebx esi edi
.B1.368:                        ; Preds .B1.364                 ; Infreq
        mov       DWORD PTR [12+esp], edx                       ;
        mov       edx, DWORD PTR [20+esp]                       ;
        mov       ebx, DWORD PTR [56+esp]                       ;
        mov       edi, DWORD PTR [24+esp]                       ;
                                ; LOE eax edx ecx ebx esi edi
.B1.369:                        ; Preds .B1.369 .B1.368         ; Infreq
        movdqa    xmm0, XMMWORD PTR [4+edi+esi*4]               ;59.3
        movdqa    XMMWORD PTR [4+ebx+esi*4], xmm0               ;59.3
        add       esi, 4                                        ;59.3
        cmp       esi, edx                                      ;59.3
        jb        .B1.369       ; Prob 82%                      ;59.3
                                ; LOE eax edx ecx ebx esi edi
.B1.370:                        ; Preds .B1.366 .B1.369         ; Infreq
        mov       DWORD PTR [20+esp], edx                       ;
        mov       edx, DWORD PTR [12+esp]                       ;
                                ; LOE eax edx ecx
.B1.371:                        ; Preds .B1.370 .B1.375         ; Infreq
        mov       ebx, DWORD PTR [44+esp]                       ;59.3
        cmp       ebx, DWORD PTR [20+esp]                       ;59.3
        jbe       .B1.145       ; Prob 10%                      ;59.3
                                ; LOE eax edx ecx
.B1.372:                        ; Preds .B1.371                 ; Infreq
        mov       esi, eax                                      ;
        lea       ebx, DWORD PTR [ecx*4]                        ;
        neg       esi                                           ;
        neg       ebx                                           ;
        add       esi, 3                                        ;
        lea       edi, DWORD PTR [edx*4]                        ;
        imul      esi, DWORD PTR [32+esp]                       ;
        neg       edi                                           ;
        add       ebx, DWORD PTR [36+esp]                       ;
        add       esi, ebx                                      ;
        mov       ebx, DWORD PTR [esp]                          ;
        neg       ebx                                           ;
        add       ebx, 2                                        ;
        imul      ebx, DWORD PTR [4+esp]                        ;
        add       edi, DWORD PTR [40+esp]                       ;
        add       ebx, edi                                      ;
        mov       DWORD PTR [8+esp], ecx                        ;
        mov       DWORD PTR [12+esp], edx                       ;
        mov       ecx, esi                                      ;
        mov       esi, DWORD PTR [20+esp]                       ;
        mov       edi, DWORD PTR [44+esp]                       ;
                                ; LOE eax ecx ebx esi edi
.B1.373:                        ; Preds .B1.373 .B1.372         ; Infreq
        mov       edx, DWORD PTR [4+ecx+esi*4]                  ;59.3
        mov       DWORD PTR [4+ebx+esi*4], edx                  ;59.3
        inc       esi                                           ;59.3
        cmp       esi, edi                                      ;59.3
        jb        .B1.373       ; Prob 82%                      ;59.3
                                ; LOE eax ecx ebx esi edi
.B1.374:                        ; Preds .B1.373                 ; Infreq
        mov       ecx, DWORD PTR [8+esp]                        ;
        mov       edx, DWORD PTR [12+esp]                       ;
        jmp       .B1.145       ; Prob 100%                     ;
                                ; LOE eax edx ecx
.B1.375:                        ; Preds .B1.355 .B1.359 .B1.357 ; Infreq
        xor       ebx, ebx                                      ;59.3
        mov       DWORD PTR [20+esp], ebx                       ;59.3
        jmp       .B1.371       ; Prob 100%                     ;59.3
                                ; LOE eax edx ecx
.B1.378:                        ; Preds .B1.139                 ; Infreq
        cmp       DWORD PTR [44+esp], 4                         ;58.3
        jl        .B1.398       ; Prob 10%                      ;58.3
                                ; LOE eax edx ecx
.B1.379:                        ; Preds .B1.378                 ; Infreq
        mov       esi, DWORD PTR [4+esp]                        ;58.3
        lea       edi, DWORD PTR [edx*4]                        ;58.3
        mov       ebx, esi                                      ;58.3
        neg       edi                                           ;58.3
        imul      ebx, DWORD PTR [esp]                          ;58.3
        add       edi, DWORD PTR [40+esp]                       ;58.3
        mov       DWORD PTR [24+esp], edi                       ;58.3
        mov       DWORD PTR [8+esp], ebx                        ;58.3
        add       esi, edi                                      ;58.3
        sub       esi, ebx                                      ;58.3
        add       esi, 4                                        ;58.3
        and       esi, 15                                       ;58.3
        je        .B1.382       ; Prob 50%                      ;58.3
                                ; LOE eax edx ecx esi
.B1.380:                        ; Preds .B1.379                 ; Infreq
        test      esi, 3                                        ;58.3
        jne       .B1.398       ; Prob 10%                      ;58.3
                                ; LOE eax edx ecx esi
.B1.381:                        ; Preds .B1.380                 ; Infreq
        neg       esi                                           ;58.3
        add       esi, 16                                       ;58.3
        shr       esi, 2                                        ;58.3
                                ; LOE eax edx ecx esi
.B1.382:                        ; Preds .B1.381 .B1.379         ; Infreq
        lea       ebx, DWORD PTR [4+esi]                        ;58.3
        cmp       ebx, DWORD PTR [44+esp]                       ;58.3
        jg        .B1.398       ; Prob 10%                      ;58.3
                                ; LOE eax edx ecx esi
.B1.383:                        ; Preds .B1.382                 ; Infreq
        mov       ebx, DWORD PTR [44+esp]                       ;58.3
        mov       edi, ebx                                      ;58.3
        sub       edi, esi                                      ;58.3
        and       edi, 3                                        ;58.3
        neg       edi                                           ;58.3
        add       edi, ebx                                      ;58.3
        lea       ebx, DWORD PTR [ecx*4]                        ;
        mov       DWORD PTR [16+esp], edi                       ;58.3
        mov       edi, eax                                      ;
        neg       edi                                           ;
        neg       ebx                                           ;
        add       edi, 2                                        ;
        imul      edi, DWORD PTR [32+esp]                       ;
        add       ebx, DWORD PTR [36+esp]                       ;
        add       edi, ebx                                      ;
        mov       ebx, DWORD PTR [8+esp]                        ;
        neg       ebx                                           ;
        add       ebx, DWORD PTR [4+esp]                        ;
        add       DWORD PTR [24+esp], ebx                       ;
        mov       DWORD PTR [20+esp], edi                       ;
        test      esi, esi                                      ;58.3
        jbe       .B1.387       ; Prob 0%                       ;58.3
                                ; LOE eax edx ecx esi edi
.B1.384:                        ; Preds .B1.383                 ; Infreq
        mov       DWORD PTR [8+esp], ecx                        ;
        xor       ebx, ebx                                      ;
        mov       DWORD PTR [12+esp], edx                       ;
        mov       ecx, DWORD PTR [24+esp]                       ;
                                ; LOE eax ecx ebx esi edi
.B1.385:                        ; Preds .B1.385 .B1.384         ; Infreq
        mov       edx, DWORD PTR [4+edi+ebx*4]                  ;58.3
        mov       DWORD PTR [4+ecx+ebx*4], edx                  ;58.3
        inc       ebx                                           ;58.3
        cmp       ebx, esi                                      ;58.3
        jb        .B1.385       ; Prob 82%                      ;58.3
                                ; LOE eax ecx ebx esi edi
.B1.386:                        ; Preds .B1.385                 ; Infreq
        mov       ecx, DWORD PTR [8+esp]                        ;
        mov       edx, DWORD PTR [12+esp]                       ;
                                ; LOE eax edx ecx esi
.B1.387:                        ; Preds .B1.383 .B1.386         ; Infreq
        mov       ebx, DWORD PTR [20+esp]                       ;58.3
        lea       edi, DWORD PTR [4+ebx+esi*4]                  ;58.3
        test      edi, 15                                       ;58.3
        je        .B1.391       ; Prob 60%                      ;58.3
                                ; LOE eax edx ecx esi
.B1.388:                        ; Preds .B1.387                 ; Infreq
        mov       DWORD PTR [12+esp], edx                       ;
        mov       edx, DWORD PTR [16+esp]                       ;
        mov       ebx, DWORD PTR [24+esp]                       ;
        mov       edi, DWORD PTR [20+esp]                       ;
                                ; LOE eax edx ecx ebx esi edi
.B1.389:                        ; Preds .B1.389 .B1.388         ; Infreq
        movdqu    xmm0, XMMWORD PTR [4+edi+esi*4]               ;58.3
        movdqa    XMMWORD PTR [4+ebx+esi*4], xmm0               ;58.3
        add       esi, 4                                        ;58.3
        cmp       esi, edx                                      ;58.3
        jb        .B1.389       ; Prob 82%                      ;58.3
        jmp       .B1.393       ; Prob 100%                     ;58.3
                                ; LOE eax edx ecx ebx esi edi
.B1.391:                        ; Preds .B1.387                 ; Infreq
        mov       DWORD PTR [12+esp], edx                       ;
        mov       edx, DWORD PTR [16+esp]                       ;
        mov       ebx, DWORD PTR [24+esp]                       ;
        mov       edi, DWORD PTR [20+esp]                       ;
                                ; LOE eax edx ecx ebx esi edi
.B1.392:                        ; Preds .B1.392 .B1.391         ; Infreq
        movdqa    xmm0, XMMWORD PTR [4+edi+esi*4]               ;58.3
        movdqa    XMMWORD PTR [4+ebx+esi*4], xmm0               ;58.3
        add       esi, 4                                        ;58.3
        cmp       esi, edx                                      ;58.3
        jb        .B1.392       ; Prob 82%                      ;58.3
                                ; LOE eax edx ecx ebx esi edi
.B1.393:                        ; Preds .B1.389 .B1.392         ; Infreq
        mov       DWORD PTR [16+esp], edx                       ;
        mov       edx, DWORD PTR [12+esp]                       ;
                                ; LOE eax edx ecx
.B1.394:                        ; Preds .B1.393 .B1.398         ; Infreq
        mov       ebx, DWORD PTR [44+esp]                       ;58.3
        cmp       ebx, DWORD PTR [16+esp]                       ;58.3
        jbe       .B1.142       ; Prob 0%                       ;58.3
                                ; LOE eax edx ecx
.B1.395:                        ; Preds .B1.394                 ; Infreq
        mov       esi, eax                                      ;
        lea       ebx, DWORD PTR [ecx*4]                        ;
        neg       esi                                           ;
        neg       ebx                                           ;
        add       esi, 2                                        ;
        imul      esi, DWORD PTR [32+esp]                       ;
        add       ebx, DWORD PTR [36+esp]                       ;
        mov       edi, DWORD PTR [4+esp]                        ;
        add       esi, ebx                                      ;
        mov       ebx, edi                                      ;
        imul      ebx, DWORD PTR [esp]                          ;
        mov       DWORD PTR [20+esp], esi                       ;
        lea       esi, DWORD PTR [edx*4]                        ;
        neg       esi                                           ;
        neg       ebx                                           ;
        add       esi, DWORD PTR [40+esp]                       ;
        add       ebx, edi                                      ;
        add       esi, ebx                                      ;
        mov       DWORD PTR [8+esp], ecx                        ;
        mov       DWORD PTR [12+esp], edx                       ;
        mov       ecx, esi                                      ;
        mov       ebx, DWORD PTR [20+esp]                       ;
        mov       esi, DWORD PTR [16+esp]                       ;
        mov       edi, DWORD PTR [44+esp]                       ;
                                ; LOE eax ecx ebx esi edi
.B1.396:                        ; Preds .B1.396 .B1.395         ; Infreq
        mov       edx, DWORD PTR [4+ebx+esi*4]                  ;58.3
        mov       DWORD PTR [4+ecx+esi*4], edx                  ;58.3
        inc       esi                                           ;58.3
        cmp       esi, edi                                      ;58.3
        jb        .B1.396       ; Prob 82%                      ;58.3
                                ; LOE eax ecx ebx esi edi
.B1.397:                        ; Preds .B1.396                 ; Infreq
        mov       ecx, DWORD PTR [8+esp]                        ;
        mov       edx, DWORD PTR [12+esp]                       ;
        jmp       .B1.142       ; Prob 100%                     ;
                                ; LOE eax edx ecx
.B1.398:                        ; Preds .B1.378 .B1.382 .B1.380 ; Infreq
        xor       ebx, ebx                                      ;58.3
        mov       DWORD PTR [16+esp], ebx                       ;58.3
        jmp       .B1.394       ; Prob 100%                     ;58.3
        ALIGN     16
                                ; LOE eax edx ecx
; mark_end;
_SIM_LOAD_VEHICLES ENDP
_TEXT	ENDS
_BSS	SEGMENT  DWORD PUBLIC FLAT  'BSS'
	ALIGN 004H
_SIM_LOAD_VEHICLES$PATHIN.0.1	DB ?	; pad
	ORG $+39998	; pad
	DB ?	; pad
_SIM_LOAD_VEHICLES$PRINTSTR1.0.1	DB ?	; pad
	ORG $+78	; pad
	DB ?	; pad
_SIM_LOAD_VEHICLES$JFO.0.1	DD 1 DUP (0H)	; pad
_BSS	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
SIM_LOAD_VEHICLES$format_pack.0.1	DB	54
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	9
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	-24
	DB	3
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	40
	DB	0
	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	44
	DB	32
	DB	111
	DB	118
	DB	101
	DB	114
	DB	115
	DB	97
	DB	116
	DB	117
	DB	114
	DB	97
	DB	116
	DB	105
	DB	111
	DB	110
	DB	32
	DB	111
	DB	110
	DB	32
	DB	103
	DB	101
	DB	110
	DB	101
	DB	114
	DB	97
	DB	116
	DB	105
	DB	111
	DB	110
	DB	32
	DB	108
	DB	105
	DB	110
	DB	107
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	2
	DB	0
	DB	45
	DB	62
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
_DATA	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
__NLITPACK_0.0.1	DD	5
__NLITPACK_3.0.1	DD	98
__NLITPACK_2.0.1	DD	1
__NLITPACK_1.0.1	DD	97
__STRLITPACK_13.0.1	DB	9
	DB	5
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_14.0.1	DB	9
	DB	5
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_15.0.1	DB	2
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_16.0.1	DB	9
	DB	5
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_17.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_18.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__NLITPACK_6.0.1	DD	0
__NLITPACK_5.0.1	DD	440
__NLITPACK_4.0.1	DD	20
__STRLITPACK_20.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_21.0.1	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_23.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_24.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _SIM_LOAD_VEHICLES
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DD 7 DUP (0H)	; pad
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_9	DB	109
	DB	97
	DB	120
	DB	110
	DB	117
	DB	95
	DB	112
	DB	97
	DB	32
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	44
	DB	32
	DB	112
	DB	108
	DB	101
	DB	97
	DB	115
	DB	101
	DB	32
	DB	105
	DB	110
	DB	99
	DB	114
	DB	101
	DB	97
	DB	115
	DB	101
	DB	32
	DB	116
	DB	104
	DB	101
	DB	32
	DB	109
	DB	97
	DB	120
	DB	110
	DB	117
	DB	95
	DB	112
	DB	97
	DB	116
	DB	104
	DB	32
	DB	115
	DB	101
	DB	116
	DB	116
	DB	105
	DB	110
	DB	103
	DB	32
	DB	105
	DB	110
	DB	32
	DB	80
	DB	65
	DB	82
	DB	65
	DB	77
	DB	69
	DB	84
	DB	69
	DB	82
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
__STRLITPACK_19	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_8	DB	67
	DB	65
	DB	76
	DB	76
	DB	32
	DB	103
	DB	101
	DB	116
	DB	32
	DB	118
	DB	101
	DB	104
	DB	32
	DB	105
	DB	110
	DB	32
	DB	118
	DB	101
	DB	104
	DB	32
	DB	108
	DB	111
	DB	97
	DB	100
	DB	44
	DB	32
	DB	82
	DB	117
	DB	110
	DB	77
	DB	111
	DB	100
	DB	101
	DB	32
	DB	61
	DB	32
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_22	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_4	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	102
	DB	105
	DB	110
	DB	100
	DB	105
	DB	110
	DB	103
	DB	32
	DB	106
	DB	49
	DB	0
__STRLITPACK_2	DB	112
	DB	108
	DB	101
	DB	97
	DB	115
	DB	101
	DB	32
	DB	99
	DB	111
	DB	110
	DB	116
	DB	97
	DB	99
	DB	116
	DB	32
	DB	100
	DB	101
	DB	118
	DB	101
	DB	108
	DB	111
	DB	112
	DB	101
	DB	114
	DB	115
	DB	32
	DB	116
	DB	111
	DB	32
	DB	114
	DB	101
	DB	115
	DB	111
	DB	108
	DB	118
	DB	101
	DB	32
	DB	116
	DB	104
	DB	105
	DB	115
	DB	32
	DB	105
	DB	115
	DB	115
	DB	117
	DB	101
	DB	0
__STRLITPACK_25	DB	0
	DB 3 DUP ( 0H)	; pad
_2il0floatpacket.7	DD	03a83126fH
_2il0floatpacket.8	DD	040c00000H
_2il0floatpacket.9	DD	042c80000H
_2il0floatpacket.10	DD	080000000H
_2il0floatpacket.11	DD	04b000000H
_2il0floatpacket.12	DD	03f000000H
_2il0floatpacket.13	DD	0bf000000H
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
EXTRN	_DYNUST_LINK_VEH_LIST_MODULE_mp_ENTQUEVEHLIST:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_IPINIT:BYTE
EXTRN	_DYNUST_LINK_VEH_LIST_MODULE_mp_TRIPCHAINLIST:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NOOFARCS:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TOTALVEH:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NUMNTAG:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM:BYTE
EXTRN	_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE:BYTE
EXTRN	_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_MAXNU_PA:BYTE
EXTRN	_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_REACH_CONVERG:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ITEMAX:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_FUELOUT:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NUMGENVEH:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NUMCARS:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ENDTM:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_STARTTM:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_JUSTVEH:BYTE
EXTRN	_DYNUST_VEH_PATH_ATT_MODULE_mp_MOP:BYTE
EXTRN	_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHAIN:BYTE
EXTRN	_DYNUST_VEH_PATH_ATT_MODULE_mp_VLGCOUNTER:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TDSPSTEP:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ITERATION:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_RUNMODE:BYTE
_DATA	ENDS
EXTRN	_for_write_seq_fmt_xmit:PROC
EXTRN	_for_write_seq_fmt:PROC
EXTRN	_for_cpystr:PROC
EXTRN	_for_stop_core:PROC
EXTRN	_for_write_seq_lis:PROC
EXTRN	_for_read_seq_fmt_xmit:PROC
EXTRN	_for_read_seq_fmt:PROC
EXTRN	_for_dealloc_allocatable:PROC
EXTRN	_for_alloc_allocatable:PROC
EXTRN	_for_check_mult_overflow:PROC
EXTRN	_DYNUST_LINK_VEH_LIST_MODULE_mp_ENTRYQUEUE_INSERT:PROC
EXTRN	_DYNUST_FUEL_MODULE_mp_SETFUELLEVEL:PROC
EXTRN	_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_P_SETUP:PROC
EXTRN	_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_A_SETUP:PROC
EXTRN	_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_INSERT:PROC
EXTRN	_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ACTIVE_SIZE:PROC
EXTRN	_DYNUST_LINK_VEH_LIST_MODULE_mp_TRIPCHAIN_REMOVE:PROC
EXTRN	_DYNUST_LINK_VEH_LIST_MODULE_mp_ENTRYQUEUE_INSFRONT:PROC
EXTRN	_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST_INSERT:PROC
EXTRN	_DYNUST_LINK_VEH_LIST_MODULE_mp_ENTRYQUEUE_REMOVE:PROC
EXTRN	_PRINTSCREEN:PROC
EXTRN	_IMSLSORT:PROC
EXTRN	_WRITE_VEHICLES:PROC
EXTRN	_RETRIEVE_VEH_PATH_ASTAR:PROC
EXTRN	_RETRIEVE_NEXT_LINK:PROC
EXTRN	__intel_fast_memcpy:PROC
EXTRN	__intel_fast_memset:PROC
EXTRN	__fltused:BYTE
	INCLUDELIB <ifconsol>
	INCLUDELIB <libifcoremt>
	INCLUDELIB <libifport>
	INCLUDELIB <libmmt>
	INCLUDELIB <LIBCMT>
	INCLUDELIB <libirc>
	INCLUDELIB <svml_dispmt>
	INCLUDELIB <OLDNAMES>
	END
