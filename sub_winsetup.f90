SUBROUTINE winsetup
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~ Copyright (C) 2002-2011 The Arizona Board of Regents on Behalf of The University of Arizona.  ~~!
!~~ Developed by Yi-Chang Chiu, Ph.D. as par to the DynusT project.                               ~~!
!~~ This program is free software: you can redistribute it and/or modify it                       ~~!
!~~ under the terms of the GNU General Public License version 2 as published by                   ~~!
!~~ the Free Software Foundation.                                                                 ~~!
!~~ This program is distributed in the hope that it will be useful,                               ~~!
!~~ but WITHOUT ANY WARRANTY; without even the implied warranty of                                ~~!
!~~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                                          ~~!
!~~ See the GNU General Public License for more details.                                          ~~!
!~~ You should have received a copy of the GNU General Public License                             ~~!
!~~ along with this program (in the file COPYING).                                                ~~!
!~~ IF not, see <http://www.gnu.org/licenses/>.                                                   ~~!
!~~ Please see http://www.dynust.net for contact information and names                            ~~!
!~~ of individual contributors.                                                                   ~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
USE IFQWIN
USE DYNUST_MAIN_MODULE

INTERFACE
   SUBROUTINE printscreen(printstr,intx,inty,dx1,dy1,dx2,dy2,refresh)
    INTEGER intx, inty, refresh,dx1,dy1,dx2,dy2
      CHARACTER (80) printstr
    END SUBROUTINE 
END INTERFACE  

LOGICAL(4) result, STATUS
INTEGER(2) numfonts,fontnum
TYPE (qwinfo) winfo
TYPE (xycoord) pos
TYPE (WINDOWCONFIG) WC
CHARACTER (11) YCST
CHARACTER(100) pathtest
CHARACTER(1) patharray(100)
CHARACTER(LEN = 200) strPath
INTEGER strLen
CHARACTER(Len=:),ALLOCATABLE :: strFinal(:) ! THE ONLY WAY TO SET VARIALBE-LENGTH STRING IS TO SEND IT AS A DYNAMIC ARRAY
INTEGER(2) dummy, x1, y1, x2, y2
LOGICAL EXT2
INTEGER ERRID,istat
CHARACTER(80) printstr1, printstr2
!> > > > > > > > > >                < < < < < < < < < < <
!>>>>>>>>>>>>>>>>>>>CODES START HERE<<<<<<<<<<<<<<<<<<<<<
!> > > > > > > > > >                < < < < < < < < < < <

result = initialsettings()
result = INITIALIZEFONTS()
istat = GetCWD(strPath)
strLen = len_trim(strPath)
strPath = TRIM(strPath)
!strPath = adjustr(strPath)
ALLOCATE(CHARACTER(strLen)::strFinal(1))
LEnd = max(1,StrLen-30)
strFinal(1) = strPath(Lend:strLen) ! fixed to print the rightmost 30 characters

STATUS = GETWINDOWCONFIG(WC)
WC%extendfontname = 'Times New Roman'
WC%MODE = QWIN$MIN
WC%numcolors = 256
WC%title = strFinal(1) ! ASSIGN THE DIRECTORY PATH TO THE WINDOW TITLE
result = SETWINDOWCONFIG(WC)

winfo%TYPE = QWIN$MAX
result = SETWSIZEQQ(QWIN$FRAMEWINDOW,winfo)

winfo%TYPE = QWIN$SET
winfo%X = 300
winfo%Y = 100
winfo%H = 500
winfo%W = 460
result = SETWSIZEQQ(QWIN$FRAMEWINDOW,winfo)
result = SETBKCOLORRGB(Z'a00000')

winfo%TYPE = QWIN$MAX
result = SETWSIZEQQ(0,winfo)
CALL CLEARSCREEN($GCLEARSCREEN)

! the following is to load the dynust.bmp
INQUIRE (FILE='workingdir.ini', EXIST = EXT2)

IF(EXT2) THEN
OPEN(file = "workingdir.ini", access = 'sequential',unit = 599)

READ(599,1000,ADVANCE='NO',EOR=765,ERR=765,IOSTAT=errid) (pathtest(i:i), i=1,100)
IF(errid /= 0) go to 234
close(599)
1000 FORMAT(100a1)
765 pathtest = trim(pathtest)
DO k = len(pathtest), 1, -1
 IF(pathtest(k:k) == '\') THEN
   Len_trim_result = k
   exit
 ENDIF
ENDDO

DO k = 1, Len_trim_result
 patharray(k) = pathtest(k:k)
ENDDO

CALL bmpload(len_trim_result,patharray)

ELSE ! the workingdir.ini does not exist

printstr1 = "No Image Available. Please run DynusT from NEXTA"
234  CALL printscreen(printstr1,20,200,0,0,0,0,0)

ENDIF

numfonts = INITIALIZEFONTS( )

END SUBROUTINE


!~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>
SUBROUTINE bmpload(n,patharray)
USE IFQWIN
INTEGER n
CHARACTER (n) path1
CHARACTER(n+10) path2
CHARACTER(1) patharray(100)

DO k = 1, n
  path1(k:k) = patharray(k)
ENDDO

path2=path1//'dynust.bmp'
result = LOADIMAGE (path2, 20, 30)
END SUBROUTINE

!~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>
PURE FUNCTION Copy_a2s(a)  RESULT (s)    ! copy char array to string
CHARACTER,INTENT(IN) :: a(:)
CHARACTER(SIZE(a)) :: s
INTEGER :: i
DO i = 1,SIZE(a)
   s(i:i) = a(i)
END DO
END FUNCTION Copy_a2s

!~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>
PURE FUNCTION Copy_s2a(s)  RESULT (a)   ! copy s(1:Clen(s)) to char array
CHARACTER(*),INTENT(IN) :: s
CHARACTER :: a(LEN(s))
INTEGER :: i
DO i = 1,LEN(s)
   a(i) = s(i:i)
END DO
END FUNCTION Copy_s2a
