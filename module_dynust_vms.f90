MODULE VARIABLE_MESSAGE_SIGN_MODULE
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~ Copyright (C) 2002-2011 The Arizona Board of Regents on Behalf of The University of Arizona.  ~~!
!~~ Developed by Yi-Chang Chiu, Ph.D. as par to the DynusT project.                               ~~!
!~~ This program is free software: you can redistribute it and/or modify it                       ~~!
!~~ under the terms of the GNU General Public License version 2 as published by                   ~~!
!~~ the Free Software Foundation.                                                                 ~~!
!~~ This program is distributed in the hope that it will be useful,                               ~~!
!~~ but WITHOUT ANY WARRANTY; without even the implied warranty of                                ~~!
!~~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                                          ~~!
!~~ See the GNU General Public License for more details.                                          ~~!
!~~ You should have received a copy of the GNU General Public License                             ~~!
!~~ along with this program (in the file COPYING).                                                ~~!
!~~ IF not, see <http://www.gnu.org/licenses/>.                                                   ~~!
!~~ Please see http://www.dynust.net for contact information and names                            ~~!
!~~ of individual contributors.                                                                   ~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
     USE DYNUST_MAIN_MODULE
     USE DYNUST_VEH_MODULE
     USE DYNUST_NETWORK_MODULE
	 USE DYNUST_VEH_PATH_ATT_MODULE
     USE DFPORT
     USE RAND_GEN_INT
      
CONTAINS

SUBROUTINE VMS_READINPUT

	DO i=1,vms_num
      READ(49,*) vmstype(i),i1,i2,vms(i,2),vms(i,3),vms_start(i),vms_end(i)
      IF(vmstype(i) /= 3.and.vmstype(i) /= 2.and.vmstype(i) /= 5) THEN
         WRITE(911,*) "Only Congestion Warning or mandatory detour are activated"
         WRITE(911,*) "Other features are under testing"
         WRITE(911,*) "Optional detour is permanently removed"
         STOP
      ENDIF

      vms(i,1)=GetFLinkFromNode(OutToInNodeNum(i1),OutToInNodeNum(i2),9)
      nodeWVMS(OutToInNodeNum(i2))= i ! mark the downstream node of the link with VMS number

      IF(vmstype(i) == 2) THEN
	   READ(49,*,iostat=error) (vmstypetwopath(i,1,k)%node,k=1,vms(i,3))
	   IF(vmstypetwopath(i,1,1)%node /= i2) THEN
	    WRITE(911,*) "Error in specifying VMS"
	    WRITE(911,'("Subpaths for VMS number", i3,"  is incorrect, please check")') i
	    STOP
	   ENDIF
	   DO is = 1, vms(i,3) !  convert to internal node numbers
	     vmstypetwopath(i,1,is)%node = OutToInNodeNum(vmstypetwopath(i,1,is)%node)
	   ENDDO      
       vms(i,2) = 100.0 ! full compliance
       DO mmp = 1, vms(i,3)-1
         vmstypetwopath(i,1,mmp)%link=GetFLinkFromNode(vmstypetwopath(i,1,mmp)%node,vmstypetwopath(i,1,mmp+1)%node,9)
       ENDDO
      ENDIF  

	 IF(vmstype(i) == 3) THEN
	  IF(vms(i,2) > 100.or.vms(i,2) < 0) THEN
	   WRITE(911,*) "Error in vms.dat type 3"
	   WRITE(911,*) "Check vms(i,2) for response percentage",i
	   STOP
	  ENDIF
	  IF(vms(i,3) /= 0.and.vms(i,3) /= 1) THEN
	     WRITE(911,*) "Error in vms.dat type 3"
         WRITE(911,*) "Diversion Mode shoule be either 1 or 0"
	     STOP
	  ENDIF
	  ENDIF
      IF(OutToInNodeNum(i1) < 1.or.OutToInNodeNum(i2) < 1) THEN
   	     WRITE(911,*) "error in vms.dat"
   	     WRITE(911,*) "please check number ", i
   	     STOP
   	  ENDIF

      IF(vmstype(i) == 5) THEN
	    DO ih = 1, vms(i,3) ! # of sub paths
	     READ(49,*,iostat=error) vmsnnode(i,ih), (vmstypetwopath(i,ih,k)%node,k=1,vmsnnode(i,ih)), ratte
	     IF(vmstypetwopath(i,ih,1)%node /= i2.or.vmsnnode(i,ih) < 1) THEN
	      WRITE(911,*) "Error in specifying VMS"
	      WRITE(911,'("Subpaths for VMS number", i3,"  is incorrect, please check")') i
	      STOP
	     ENDIF
	     DO is = 1, vmsnnode(i,ih) !  convert to internal node numbers
	       vmstypetwopath(i,ih,is)%node = OutToInNodeNum(vmstypetwopath(i,ih,is)%node)
	     ENDDO      
         vms(i,2) = 100.0 ! full compliance
         vmsrate(i,ih) = ratte
         DO mmp = 1, vmsnnode(i,ih)-1
           vmstypetwopath(i,ih,mmp)%link=GetFLinkFromNode(vmstypetwopath(i,ih,mmp)%node,vmstypetwopath(i,ih,mmp+1)%node,9)
         ENDDO
        ENDDO
        DO ih = 1, vms(i,3)-1 ! convert the rate to cumulative rate
          vmsrate(i,ih+1) = vmsrate(i,ih) + vmsrate(i,ih+1)
        ENDDO
        IF(vmsrate(i,vms(i,3)) > 1.0001.or.vmsrate(i,vms(i,3)) < 0.9999) THEN
          WRITE(911,*) "VMS Type 5 subpath prob incorrect"
          STOP
        ENDIF
      ENDIF  

       IF(vms(i,1) == 0) THEN 
         WRITE(911,*) 'INPUT ERROR : VMS data file'
         WRITE(911,*) 'check the VMS link' 
         WRITE(911,*) 'for VMS number',i
         STOP
       ENDIF

! --
        ENDDO   

END SUBROUTINE
SUBROUTINE VMS_MAIN(t_start)
      IF(vms_num > 0) THEN  
        DO i=1,vms_num
          IF(t_start >= vms_start(i).and.t_start < vms_end(i)) THEN
            IF(vmstype(i) == 1) THEN
		     CALL VMS_SPEED(i)
	      ENDIF
          ENDIF
        end do
      ENDIF
END SUBROUTINE

!~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>
SUBROUTINE VMS_DIVERSION(t,ilink,id_veh,kvms)

 USE DYNUST_ROUTE_SWITCH_MODULE	 
 INTEGER ipvms,Nlnkmv
 INTEGER jd4,jm4,NodeSum
 LOGICAL BottleneckAhead
 r5 = ranxy(29)          
     
 IF(r5 <= vms(kvms,2)*0.01) THEN ! IF the vehicle responds
   icu2 = ilink
   iculnk = m_dynust_veh_nde(id_veh)%icurrnt
   current_time = 0
   NodeSumOrg = 0
   BottleneckAhead = .false.
   jm = id_veh
   LL = nint(t/xminPerSimInt)
   CALL GET_AUTO_REMAIN_TIME(jm,ilink,iculnk,LL,current_time,NodeSumOrg,BottleneckAhead)
   IF( BottleneckAhead ) THEN
      IWill = 1
      !WRITE(8456,'("VMST Time: ",f7.1," Vehicle no",i7, " OD zone", 2i6," #Curn,UN,DN ",i4,2i6," VMS # ",I5)') (l-1)*xminPerSimInt,jm,m_dynust_last_stand(jm)%jorig,m_dynust_last_stand(jm)%jdest,m_dynust_veh_nde(jm)%icurrnt,m_dynust_network_node_nde(m_dynust_network_arc_nde(icu2)%iunod)%IntoOutNodeNum,&
!      m_dynust_network_node_nde(m_dynust_network_arc_nde(icu2)%idnod)%IntoOutNodeNum, KVMS
   ENDIF
   IF( BottleneckAhead ) THEN
     jd4 = m_dynust_last_stand(id_veh)%jdest
     jm4 = m_dynust_veh_nde(id_veh)%icurrnt
     !CALL RETRIEVE_VEH_PATH(id_veh,ilink,vms(kvms,3),jm4,jd4,0,NodeSum) 
     CALL RETRIEVE_VEH_PATH_AStar(id_veh,ilink,vms(kvms,3),jm4,jd4,0,NodeSum) 
     IF( vms(kvms,3 ) /= 0.and.vms(kvms,3) /= 1) THEN
       print *, "check vms"
     ENDIF
     CALL RETRIEVE_NEXT_LINK(t,id_veh,ilink,jm4,Nlnk,NlnkMv)        
   ENDIF
 ENDIF
END SUBROUTINE

!~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>
SUBROUTINE vms_detour(t,ilink,id_veh,kvms)

	  INTEGER ipvms
	  INTEGER(2) jd2
	  INTEGER jd4,NodeSum
	  REAL evalue

      DO is = 1, vms(kvms,3)
        evalue = float(vmstypetwopath(kvms,1,is)%node)
        jd2 = m_dynust_veh_nde(id_veh)%icurrnt-1+is ! this type of assignment is to enforce the KIND consistency with the called subprogram
	    CALL vehatt_Insert(id_veh,jd2,1,evalue)
	  ENDDO

      ict = m_dynust_veh_nde(id_veh)%icurrnt+vms(kvms,3)-1

      IF(vms(kvms,3) > 1) THEN
	    inewlink = vmstypetwopath(kvms,1,vms(kvms,3)-1)%link
	  ELSE
	    inewlink = ilink
	  ENDIF
      jd4 = m_dynust_last_stand(id_veh)%jdest
      !CALL RETRIEVE_VEH_PATH(id_veh,inewlink,ipinit,ict,jd4,0,NodeSum) 
      CALL RETRIEVE_VEH_PATH_AStar(id_veh,inewlink,ipinit,ict,jd4,0,NodeSum) 
      jd4 = m_dynust_veh_nde(id_veh)%icurrnt
      CALL RETRIEVE_NEXT_LINK(t,id_veh,ilink,jd4,Nlnk,NlnkMv)        

END SUBROUTINE
      
!~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>
SUBROUTINE vms_multi(t,ilink,id_veh,kvms)
	  INTEGER ipvms,NodeSum
	  REAL r3, eValue
	  INTEGER(2) jd2
 
      r3 = ranxy(30)       
      
      DO it = 1, vms(kvms,3)
        IF(vmsrate(kvms,it) >= r3) exit
      ENDDO
        
      DO is = 1, vmsnnode(kvms,it)
        evalue = float(vmstypetwopath(kvms,it,is)%node)
        jd2 = m_dynust_veh_nde(id_veh)%icurrnt-1+is
	    CALL vehatt_Insert(id_veh,jd2,1,evalue)
	  ENDDO

      ict = m_dynust_veh_nde(id_veh)%icurrnt + vmsnnode(kvms,it)-1

	  inewlink = vmstypetwopath(kvms,it,vmsnnode(kvms,it)-1)%link
	  jd4 = m_dynust_last_stand(id_veh)%jdest
      !CALL RETRIEVE_VEH_PATH(id_veh,inewlink,ipinit,ict,jd4,0,NodeSum) 
      CALL RETRIEVE_VEH_PATH_AStar(id_veh,inewlink,ipinit,ict,jd4,0,NodeSum) 
      jd4 = m_dynust_veh_nde(id_veh)%icurrnt
      CALL RETRIEVE_NEXT_LINK(t,id_veh,ilink,jd4,Nlnk,NlnkMv)! update next link

END SUBROUTINE

!~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>
SUBROUTINE VMS_PATH(ilink,id_veh,kvms)
        INTEGER jd4,jm4,NodeSum
        ipath=vms(kvms,2)
        jd4 = m_dynust_veh_nde(id_veh)%icurrnt
        jm4 = m_dynust_last_stand(id_veh)%jdest
        !CALL RETRIEVE_VEH_PATH(id_veh,ilink,ipath,jd4,jm4,0,NodeSum)
        CALL RETRIEVE_VEH_PATH_Astar(id_veh,ilink,ipath,jd4,jm4,0,NodeSum)
END SUBROUTINE

!~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>
SUBROUTINE VMS_SPEED(i)
      ilink=vms(i,1)
      IF(vms(i,2) > 0) THEN
         IF(m_dynust_network_arc_de(ilink)%v < vms(i,2)/60.0) THEN
           m_dynust_network_arc_de(ilink)%v=(1+(vms(i,3)/100.0))*m_dynust_network_arc_de(ilink)%v
         ENDIF
      ELSE 
           IF(m_dynust_network_arc_de(ilink)%v > vms(i,2)/(-60.0)) THEN
             IF(vms(i,3) >= 99) vms(i,3)=99 
		   m_dynust_network_arc_de(ilink)%v=(1-(vms(i,3)/100.0))*m_dynust_network_arc_de(ilink)%v
           ENDIF
      ENDIF

END SUBROUTINE



SUBROUTINE TESTRAN
	IMPLICIT NONE
	INTEGER, DIMENSION(2) :: SEED ! THIS PROGRAM ASSUMES K = 1
	INTEGER, DIMENSION(2) :: OLD  ! THIS PROGRAM ASSUMES K = 1	
	INTEGER :: I, K
	REAL, DIMENSION(3) :: HARVEST
	SEED(1) = 12345
	CALL RANDOM_SEED  ! SYSTEM RANDOMLY PICKS UP TEH SEED
	CALL RANDOM_SEED(SIZE=K) ! RETURN THE NUMBER OF SEEDS USED AND SET IT TO K
	WRITE(6767,*) ' Number of integers for starting value = ', K
	CALL RANDOM_SEED(GET=OLD(1:K)) ! PUT THESE SEEDS TO OLD ARRAY
	WRITE(6767,*) ' Old starting value = ', OLD
	CALL RANDOM_NUMBER(HARVEST) ! GENERATE TEH RANDOM NUMBERS ACCORDING TO TEH CURRENT SEED
	WRITE(6767,*) ' Random numbers : ', HARVEST
	CALL RANDOM_SEED(GET=OLD(1:K)) 
	WRITE(6767,*) ' Present starting value = ', OLD
	CALL RANDOM_SEED(PUT=SEED(1:K)) ! FORCE TO USE THE SEED SET IN SEED ARRAY
	CALL RANDOM_SEED(GET=OLD(1:K)) ! REPORT THE SEEDS TO TEH OLD ARRAY
	WRITE(6767,*) ' New starting value = ', OLD
	CALL RANDOM_NUMBER(HARVEST) ! PRINT THE RANDOM NUMBER
	WRITE(6767,*) ' Random numbers : ', HARVEST
	DO I = 1, 3
		CALL RANDOM_SEED(GET=OLD(1:K))
		WRITE(6767,*) ' Present starting value = ', OLD
		CALL RANDOM_NUMBER(HARVEST)
		WRITE(6767,*) ' Random numbers : ', HARVEST
		CALL RANDOM_NUMBER(HARVEST)
		WRITE(6767,*) ' Random numbers : ', HARVEST
	END DO
END SUBROUTINE
END MODULE

