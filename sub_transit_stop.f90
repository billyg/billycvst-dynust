      SUBROUTINE TRANSIT_STOP(i,j,xpos,imbus)     
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~ Copyright (C) 2002-2011 The Arizona Board of Regents on Behalf of The University of Arizona.  ~~!
!~~ Developed by Yi-Chang Chiu, Ph.D. as par to the DynusT project.                               ~~!
!~~ This program is free software: you can redistribute it and/or modify it                       ~~!
!~~ under the terms of the GNU General Public License version 2 as published by                   ~~!
!~~ the Free Software Foundation.                                                                 ~~!
!~~ This program is distributed in the hope that it will be useful,                               ~~!
!~~ but WITHOUT ANY WARRANTY; without even the implied warranty of                                ~~!
!~~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                                          ~~!
!~~ See the GNU General Public License for more details.                                          ~~!
!~~ You should have received a copy of the GNU General Public License                             ~~!
!~~ along with this program (in the file COPYING).                                                ~~!
!~~ IF not, see <http://www.gnu.org/licenses/>.                                                   ~~!
!~~ Please see http://www.dynust.net for contact information and names                            ~~!
!~~ of individual contributors.                                                                   ~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!

      USE DYNUST_MAIN_MODULE
      USE DYNUST_VEH_MODULE
      USE DYNUST_NETWORK_MODULE
      USE DYNUST_VEH_PATH_ATT_MODULE
      INTEGER::icasetmp = 0
      INTEGER::iSTOPtmp = 0
	  INTEGER iSTOP, icase
!> > > > > > > > > >                < < < < < < < < < < <
!>>>>>>>>>>>>>>>>>>>CODES START HERE<<<<<<<<<<<<<<<<<<<<<
!> > > > > > > > > >                < < < < < < < < < < <

      idbus=0
      DO ih=1,nubus
        IF(m_dynust_bus(ih)%busid == j) idbus=ih
      ENDDO

      IF(idbus == 0) go to 100

      iSTOP=m_dynust_veh_nde(j)%icurrnt+1

      icase=BusAtt_Value(idbus,iSTOP,2)

      IF(icase == 0) goto 100

      IF(icase == 1.and.xpos < 0.0075) THEN
         icasetmp=-1*icase
         CALL BusAtt_Insert(idbus,iSTOP,2,icasetmp)
         m_dynust_bus(idbus)%bustime=-(m_dynust_bus(idbus)%busdwell)
         tt=(0.0075-xpos)/m_dynust_network_arc_de(i)%v
         m_dynust_veh(j)%distans=m_dynust_veh(j)%distans+m_dynust_veh(j)%position-0.0075
         m_dynust_veh(j)%ttSTOP=m_dynust_veh(j)%ttSTOP+tt
         m_dynust_veh(j)%ttilnow=m_dynust_veh(j)%ttilnow+tt
         GuiTotalTime=GuiTotalTime+tt
         m_dynust_bus(idbus)%bustime=m_dynust_bus(idbus)%bustime+tt
         xpos=0.0075
         m_dynust_veh(j)%position=0.0075
         imbus=1
      ENDIF

      IF(busSTOP(idbus,iSTOP) == -1) THEN
         m_dynust_bus(idbus)%bustime=m_dynust_bus(idbus)%bustime+xminPerSimInt
        IF(m_dynust_bus(idbus)%bustime <= 0) THEN

             imbus=1
             xpos=0.0075
             m_dynust_veh(j)%position=0.0075
             m_dynust_veh(j)%ttSTOP=m_dynust_veh(j)%ttSTOP+xminPerSimInt
             m_dynust_veh(j)%ttilnow=m_dynust_veh(j)%ttilnow+xminPerSimInt
             GuiTotalTime=GuiTotalTime+xminPerSimInt
             xltemp = m_dynust_network_arc_nde(i)%s*(m_dynust_network_arc_de(i)%nlanes-0.5)
             m_dynust_network_arc_de(i)%xl=max(xltemp,m_dynust_network_arc_de(i)%pcetotal/m_dynust_network_arc_de(i)%maxden)
           DO k=1,m_dynust_network_arc_nde(i)%llink%no
             m_dynust_network_arcmove_de(i,k)%capacity=m_dynust_network_arcmove_de(i,k)%capacity-(m_dynust_network_arcmove_de(i,k)%capacity/m_dynust_network_arc_de(i)%nlanes)
           end do

        ELSE
           imbus=1
           xpos=m_dynust_veh(j)%position-m_dynust_network_arc_de(i)%v*xminPerSimInt
           IF(xpos < 0) xpos=0
           m_dynust_veh(j)%tocross=m_dynust_veh(j)%position/m_dynust_network_arc_de(i)%v
           m_dynust_veh(j)%tleft=xminPerSimInt-m_dynust_veh(j)%tocross
           xlold=m_dynust_network_arc_nde(i)%s*m_dynust_network_arc_de(i)%nlanes
           m_dynust_veh(j)%ttSTOP=m_dynust_veh(j)%ttSTOP+(xminPerSimInt-m_dynust_bus(idbus)%bustime)
           m_dynust_veh(j)%ttilnow=m_dynust_veh(j)%ttilnow+(xminPerSimInt-m_dynust_bus(idbus)%bustime)
           GuiTotalTime=GuiTotalTime+(xminPerSimInt-m_dynust_bus(idbus)%bustime)

           CALL BusAtt_Insert(idbus,iSTOP,2,99)

           xltemp=m_dynust_network_arc_de(i)%nlanes*m_dynust_network_arc_nde(i)%s-(m_dynust_network_arc_nde(i)%s/2)*(xminPerSimInt-m_dynust_bus(idbus)%bustime)/xminPerSimInt
	     m_dynust_network_arc_de(i)%xl=max(xltemp,m_dynust_network_arc_de(i)%pcetotal/m_dynust_network_arc_de(i)%maxden)
        ENDIF
      ENDIF

      IF(icase == 2.and.(xpos < m_dynust_network_arc_nde(i)%s/2) )THEN

         iSTOPtmp = (-1)*busSTOP(idbus,iSTOP)
         CALL BusAtt_Insert(idbus,iSTOP,2,iSTOPtmp)
         m_dynust_bus(idbus)%bustime=-(m_dynust_bus(idbus)%busdwell)
         tt=(m_dynust_network_arc_nde(i)%s/2-xpos)/m_dynust_network_arc_de(i)%v
         m_dynust_veh(j)%distans=m_dynust_veh(j)%distans+m_dynust_veh(j)%position-m_dynust_network_arc_nde(i)%s/2
         m_dynust_veh(j)%ttSTOP=m_dynust_veh(j)%ttSTOP+tt
         m_dynust_veh(j)%ttilnow=m_dynust_veh(j)%ttilnow+tt
         GuiTotalTime=GuiTotalTime+tt
         m_dynust_bus(idbus)%bustime=m_dynust_bus(idbus)%bustime+tt
         xpos=m_dynust_network_arc_nde(i)%s/2
         m_dynust_veh(j)%position=m_dynust_network_arc_nde(i)%s/2
         imbus=1
      ENDIF
      IF(busSTOP(idbus,iSTOP) == -2) THEN
         m_dynust_bus(idbus)%bustime=m_dynust_bus(idbus)%bustime+xminPerSimInt
        IF(m_dynust_bus(idbus)%bustime <= 0) THEN
           imbus=1
           xltemp=m_dynust_network_arc_nde(i)%s*(m_dynust_network_arc_de(i)%nlanes-0.5)
	     m_dynust_network_arc_de(i)%xl=max(xltemp,m_dynust_network_arc_de(i)%pcetotal/m_dynust_network_arc_de(i)%maxden)
           m_dynust_veh(j)%ttSTOP=m_dynust_veh(j)%ttSTOP+xminPerSimInt
           m_dynust_veh(j)%ttilnow=m_dynust_veh(j)%ttilnow+xminPerSimInt
           GuiTotalTime=GuiTotalTime+xminPerSimInt
           xpos=m_dynust_network_arc_nde(i)%s/2
           m_dynust_veh(j)%position=m_dynust_network_arc_nde(i)%s/2
        ELSE
            imbus=1
           xpos=m_dynust_veh(j)%position-m_dynust_network_arc_de(i)%v*xminPerSimInt
           IF(xpos < 0) xpos=0
           m_dynust_veh(j)%tocross=m_dynust_veh(j)%position/m_dynust_network_arc_de(i)%v
           m_dynust_veh(j)%tleft=xminPerSimInt-m_dynust_veh(j)%tocross
           xlold=m_dynust_network_arc_nde(i)%s*m_dynust_network_arc_de(i)%nlanes
           m_dynust_veh(j)%ttSTOP=xminPerSimInt-m_dynust_bus(idbus)%bustime+m_dynust_veh(j)%ttSTOP
           m_dynust_veh(j)%ttilnow=m_dynust_veh(j)%ttilnow+xminPerSimInt-m_dynust_bus(idbus)%bustime
           GuiTotalTime=GuiTotalTime+xminPerSimInt-m_dynust_bus(idbus)%bustime

           CALL BusAtt_Insert(idbus,iSTOP,2,99)
           xltemp=m_dynust_network_arc_de(i)%nlanes*m_dynust_network_arc_nde(i)%s-(m_dynust_network_arc_nde(i)%s/2)*(xminPerSimInt-m_dynust_bus(idbus)%bustime)/xminPerSimInt
		 m_dynust_network_arc_de(i)%xl=max(xltemp,m_dynust_network_arc_de(i)%pcetotal/m_dynust_network_arc_de(i)%maxden)
        ENDIF
       ENDIF

      IF(icase == 3.and.(xpos < m_dynust_network_arc_nde(i)%s/2)) THEN

         iSTOPtmp = (-1)*busSTOP(idbus,iSTOP)
         CALL BusAtt_Insert(idbus,iSTOP,2,iSTOPtmp)
         m_dynust_bus(idbus)%bustime=-(m_dynust_bus(idbus)%busdwell)
         tt=(m_dynust_network_arc_nde(i)%s/2-xpos)/m_dynust_network_arc_de(i)%v
         m_dynust_veh(j)%distans=m_dynust_veh(j)%distans+m_dynust_veh(j)%position-m_dynust_network_arc_nde(i)%s/2
         m_dynust_veh(j)%ttSTOP=m_dynust_veh(j)%ttSTOP+tt
         m_dynust_veh(j)%ttilnow=m_dynust_veh(j)%ttilnow+tt
         GuiTotalTime=GuiTotalTime+tt

         m_dynust_bus(idbus)%bustime=m_dynust_bus(idbus)%bustime+tt
         xltemp=m_dynust_network_arc_de(i)%nlanes*m_dynust_network_arc_nde(i)%s-((m_dynust_network_arc_nde(i)%s/2)*0.067/xminPerSimInt)
	   m_dynust_network_arc_de(i)%xl=max(xltemp,m_dynust_network_arc_de(i)%pcetotal/m_dynust_network_arc_de(i)%maxden)
         xpos=m_dynust_network_arc_nde(i)%s/2
         m_dynust_veh(j)%position=m_dynust_network_arc_nde(i)%s/2
         imbus=1
      ENDIF
      IF(busSTOP(idbus,iSTOP) == -3) THEN
         m_dynust_bus(idbus)%bustime=m_dynust_bus(idbus)%bustime+xminPerSimInt
       IF(m_dynust_bus(idbus)%bustime < 0) THEN
         imbus=1
         m_dynust_veh(j)%ttSTOP=m_dynust_veh(j)%ttSTOP+xminPerSimInt
         m_dynust_veh(j)%ttilnow=m_dynust_veh(j)%ttilnow+xminPerSimInt
         GuiTotalTime=GuiTotalTime+xminPerSimInt

         m_dynust_network_arc_de(i)%xl=m_dynust_network_arc_de(i)%nlanes*m_dynust_network_arc_nde(i)%s
         xpos=m_dynust_network_arc_nde(i)%s/2
         m_dynust_veh(j)%position=m_dynust_network_arc_nde(i)%s/2
       ELSEIF(m_dynust_bus(idbus)%bustime >= 0) THEN
         imbus=1
           xpos=m_dynust_veh(j)%position-m_dynust_network_arc_de(i)%v*xminPerSimInt
           IF(xpos < 0) xpos=0
           m_dynust_veh(j)%tocross=m_dynust_veh(j)%position/m_dynust_network_arc_de(i)%v
           m_dynust_veh(j)%tleft=xminPerSimInt-m_dynust_veh(j)%tocross
           xlold=m_dynust_network_arc_nde(i)%s*m_dynust_network_arc_de(i)%nlanes
           m_dynust_veh(j)%ttSTOP=m_dynust_veh(j)%ttSTOP+(xminPerSimInt-m_dynust_bus(idbus)%bustime)
           m_dynust_veh(j)%ttilnow=m_dynust_veh(j)%ttilnow+xminPerSimInt-m_dynust_bus(idbus)%bustime
           GuiTotalTime=GuiTotalTime+xminPerSimInt-m_dynust_bus(idbus)%bustime

           CALL BusAtt_Insert(idbus,iSTOP,2,99)
       ENDIF
      ENDIF
! --
100   CONTINUE
END SUBROUTINE
     

