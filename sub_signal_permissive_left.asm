; mark_description "Intel(R) Visual Fortran Compiler XE for applications running on IA-32, Version 13.1.1.171 Build 20130313";
; mark_description "-S";
	.686P
 	.387
	OPTION DOTNAME
	ASSUME	CS:FLAT,DS:FLAT,SS:FLAT
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
TXTST0:
; -- Begin  _ADJUST_LEFT_SATURATION
; mark_begin;
IF @Version GE 800
  .MMX
ELSEIF @Version GE 612
  .MMX
  MMWORD TEXTEQU <QWORD>
ENDIF
IF @Version GE 800
  .XMM
ELSEIF @Version GE 614
  .XMM
  XMMWORD TEXTEQU <OWORD>
ENDIF
       ALIGN     16
	PUBLIC _ADJUST_LEFT_SATURATION
_ADJUST_LEFT_SATURATION	PROC NEAR 
; parameter 1: 16 + esp
; parameter 2: 20 + esp
.B1.1:                          ; Preds .B1.0
        push      esi                                           ;1.12
        sub       esp, 8                                        ;1.12
        mov       eax, DWORD PTR [20+esp]                       ;1.12
        mov       eax, DWORD PTR [eax]                          ;35.5
        test      eax, eax                                      ;35.5
        jne       .B1.5         ; Prob 67%                      ;35.5
                                ; LOE eax ebx ebp edi
.B1.2:                          ; Preds .B1.1
        imul      edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32], -900 ;37.9
        mov       ecx, DWORD PTR [16+esp]                       ;37.141
        add       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;37.9
        push      OFFSET FLAT: __NLITPACK_0.0.1                 ;38.15
        imul      eax, DWORD PTR [ecx], 900                     ;37.141
        movsx     esi, BYTE PTR [668+eax+edx]                   ;37.97
        cvtsi2ss  xmm0, esi                                     ;37.97
        movss     xmm2, DWORD PTR [440+eax+edx]                 ;37.54
        mov       esi, 1                                        ;37.9
        divss     xmm2, xmm0                                    ;37.96
        movsx     ecx, BYTE PTR [44+eax+edx]                    ;37.135
        test      ecx, ecx                                      ;37.9
        cmovg     esi, ecx                                      ;37.9
        cvtsi2ss  xmm1, esi                                     ;37.135
        mulss     xmm2, xmm1                                    ;37.134
        mulss     xmm2, DWORD PTR [_2il0floatpacket.2]          ;37.9
        movss     DWORD PTR [416+eax+edx], xmm2                 ;37.9
        call      _RANXY                                        ;38.15
                                ; LOE ebx ebp edi f1
.B1.10:                         ; Preds .B1.2
        fstp      DWORD PTR [4+esp]                             ;38.15
        movss     xmm2, DWORD PTR [4+esp]                       ;38.15
        add       esp, 4                                        ;38.15
                                ; LOE ebx ebp edi xmm2
.B1.3:                          ; Preds .B1.10
        imul      ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32], -900 ;39.15
        mov       eax, DWORD PTR [16+esp]                       ;39.69
        add       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;39.15
        imul      eax, DWORD PTR [eax], 900                     ;39.69
        movss     xmm3, DWORD PTR [416+eax+ecx]                 ;39.19
        cvttss2si edx, xmm3                                     ;39.64
        cvtsi2ss  xmm0, edx                                     ;39.64
        movaps    xmm1, xmm3                                    ;39.63
        subss     xmm1, xmm0                                    ;39.63
        comiss    xmm1, xmm2                                    ;39.15
        jb        .B1.7         ; Prob 50%                      ;39.15
                                ; LOE eax ecx ebx ebp edi xmm3
.B1.4:                          ; Preds .B1.3
        movss     xmm0, DWORD PTR [_2il0floatpacket.3]          ;39.117
        addss     xmm0, xmm3                                    ;39.117
        movss     DWORD PTR [416+eax+ecx], xmm0                 ;39.117
        add       esp, 8                                        ;39.117
        pop       esi                                           ;39.117
        ret                                                     ;39.117
                                ; LOE ebx ebp edi
.B1.5:                          ; Preds .B1.1
        cmp       eax, 1                                        ;35.5
        je        .B1.7         ; Prob 50%                      ;35.5
                                ; LOE ebx ebp edi
.B1.6:                          ; Preds .B1.5
        imul      edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32], -900 ;52.6
        mov       ecx, DWORD PTR [16+esp]                       ;52.138
        add       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;52.6
        imul      eax, DWORD PTR [ecx], 900                     ;52.138
        movsx     esi, BYTE PTR [668+eax+edx]                   ;52.94
        cvtsi2ss  xmm0, esi                                     ;52.94
        movss     xmm2, DWORD PTR [440+eax+edx]                 ;52.51
        mov       esi, 1                                        ;52.6
        divss     xmm2, xmm0                                    ;52.93
        movsx     ecx, BYTE PTR [44+eax+edx]                    ;52.132
        test      ecx, ecx                                      ;52.6
        cmovg     esi, ecx                                      ;52.6
        cvtsi2ss  xmm1, esi                                     ;52.132
        mulss     xmm2, xmm1                                    ;52.131
        mulss     xmm2, DWORD PTR [_2il0floatpacket.2]          ;52.6
        movss     DWORD PTR [416+eax+edx], xmm2                 ;52.6
                                ; LOE ebx ebp edi
.B1.7:                          ; Preds .B1.3 .B1.5 .B1.6
        add       esp, 8                                        ;55.1
        pop       esi                                           ;55.1
        ret                                                     ;55.1
        ALIGN     16
                                ; LOE
; mark_end;
_ADJUST_LEFT_SATURATION ENDP
_TEXT	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
__NLITPACK_0.0.1	DD	99
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _ADJUST_LEFT_SATURATION
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
_2il0floatpacket.2	DD	040c00000H
_2il0floatpacket.3	DD	03f800000H
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE:BYTE
_DATA	ENDS
EXTRN	_RANXY:PROC
EXTRN	__fltused:BYTE
	INCLUDELIB <ifconsol>
	INCLUDELIB <libifcoremt>
	INCLUDELIB <libifport>
	INCLUDELIB <libmmt>
	INCLUDELIB <LIBCMT>
	INCLUDELIB <libirc>
	INCLUDELIB <svml_dispmt>
	INCLUDELIB <OLDNAMES>
	END
