MODULE DYNUST_SIGNAL_MODULE
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~ Copyright (C) 2002-2011 The Arizona Board of Regents on Behalf of The University of Arizona.  ~~!
!~~ Developed by Yi-Chang Chiu, Ph.D. as par to the DynusT project.                               ~~!
!~~ This program is free software: you can redistribute it and/or modify it                       ~~!
!~~ under the terms of the GNU General Public License version 2 as published by                   ~~!
!~~ the Free Software Foundation.                                                                 ~~!
!~~ This program is distributed in the hope that it will be useful,                               ~~!
!~~ but WITHOUT ANY WARRANTY; without even the implied warranty of                                ~~!
!~~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                                          ~~!
!~~ See the GNU General Public License for more details.                                          ~~!
!~~ You should have received a copy of the GNU General Public License                             ~~!
!~~ along with this program (in the file COPYING).                                                ~~!
!~~ IF not, see <http://www.gnu.org/licenses/>.                                                   ~~!
!~~ Please see http://www.dynust.net for contact information and names                            ~~!
!~~ of individual contributors.                                                                   ~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!

USE DYNUST_MAIN_MODULE

TYPE CONTROL ! define signal class
    INTEGER(1) ControlType
    INTEGER(1) NPhase
    INTEGER CycleLen
    INTEGER(1) ActivePhase
    TYPE (PHASEDATA),POINTER::F(:)
END TYPE CONTROL


TYPE PHASEDATA ! define phase class
    INTEGER(1) NIBAPP ! number of inbound approaches
    INTEGER(2) MAXG_OFFSET
    INTEGER(1) MING_G
    INTEGER(1) AMBER
    INTEGER(2) Offset
    INTEGER(2) UsedG
    INTEGER(1) Status ! 0: inactive, 1:within minG, 2, btw ming and MaxG
    TYPE (PHASEMOVEMENT), POINTER::MOV(:)
END TYPE

TYPE PHASEMOVEMENT ! define class of movements in each phase
    INTEGER InLinkNu ! inbound approach number
    INTEGER(1) NOBAppch ! number of outbound approach
    INTEGER, POINTER::OBAPPNU(:) ! outbound approach numbers
END TYPE

INTEGER(1) LostTime
INTEGER(1) NofActSig, NofPreSig
TYPE(control),ALLOCATABLE::Control_Array(:)

INTEGER(1)::GreenExt
REAL::ActuationThreshold = 0.05 


CONTAINS

!~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>
SUBROUTINE DeclareSignal(NSignal)
    INTEGER,INTENT(in)::NSignal
    INTEGER ERROR
    ALLOCATE (Control_Array(NSignal),stat=error)
    IF(error /= 0) THEN
        WRITE(911,*) 'allocate signal_array error'
        STOP
    ENDIF
    control_array(:)%ControlType = 0
    control_array(:)%NPhase = 0
    control_array(:)%CycleLen = 0
    control_array(:)%ActivePhase = 1
END SUBROUTINE

!~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>
SUBROUTINE InsertSignalPar(SigParTmp,Dim)
    USE DYNUST_MAIN_MODULE
    USE DYNUST_NETWORK_MODULE
    INTEGER,INTENT(in)::Dim,SigParTmp(Dim)
    INTEGER NodeID
    NodeID = OutToInNodeNum(SigParTmp(1))
    control_array(NodeID)%ControlType = SigParTmp(2)
    IF(SigParTmp(2) == 4.or.SigParTmp(2) == 5) THEN
        control_array(NodeID)%NPhase = SigParTmp(3)
        control_array(NodeID)%CycleLen = SigParTmp(4)
    ENDIF
END SUBROUTINE


!~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>
SUBROUTINE DeclarePhase(NodeID,NPhase)
    USE DYNUST_MAIN_MODULE
    USE DYNUST_NETWORK_MODULE
    INTEGER,INTENT(in)::NodeID,NPhase
    ALLOCATE(Control_Array(NodeID)%F(NPhase),stat=error)
    IF(error /= 0) THEN
        WRITE(911,*) 'allocate control_array phase allocation error'
        STOP
    ENDIF
    IF(NPhase < 1) THEN
        WRITE(911,*) 'Wrong number of phases for pre-time for actuated signal'
	    WRITE(911,*) 'check node ID', m_dynust_network_node_nde(NodeID)%IntoOutNodeNum
    ENDIF
    Control_Array(NodeID)%F(:)%NIBAPP = 0 ! number of inbound approaches
    Control_Array(NodeID)%F(:)%MAXG_OFFSET = 0
    Control_Array(NodeID)%F(:)%MING_G = 0
    Control_Array(NodeID)%F(:)%AMBER = 0
    Control_Array(NodeID)%F(:)%Offset = 0
    Control_Array(NodeID)%F(:)%UsedG = 0
    Control_Array(NodeID)%F(:)%Status = 0
    Control_Array(NodeID)%F(1)%Status = 1
END SUBROUTINE


!~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>
SUBROUTINE InsertSignalPhase(TmpSigData,Dim)
    USE DYNUST_MAIN_MODULE
    USE DYNUST_NETWORK_MODULE
  
    INTEGER,INTENT(in)::Dim,TmpSigData(Dim)
    INTEGER PhaseID,NodeID
    NodeID = OutToInNodeNum(TmpSigData(1))
    IF(TmpSigData(2) > control_array(NodeID)%NPhase) THEN
        WRITE(911,*) 'error in reading sig data in insertSignalPhase sub for node',TmpSigData(1)
        WRITE(911,*) "check # of phases for it"
	    STOP
    ENDIF
    PhaseID = TmpsigData(2)
    Control_array(NodeID)%F(PhaseID)%NIbApp =      TmpSigData(6)
    Control_Array(NodeID)%F(PhaseID)%maxg_offset = max(nint(60*xminPerSimInt),TmpSigData(3))
    Control_Array(NodeID)%F(PhaseID)%ming_g =      max(nint(60*xminPerSimInt),TmpSigData(4))
    Control_Array(NodeID)%F(PhaseID)%amber =       TmpSigData(5)
END SUBROUTINE

!~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>
SUBROUTINE DeclarePhaseMove(TmpSigData,Dim)
    USE DYNUST_MAIN_MODULE
    USE DYNUST_NETWORK_MODULE
  
    INTEGER,INTENT(in)::Dim,TmpSigData(Dim)
    INTEGER error,MovNu
    MovNu = TmpSigData(6)
    PhaseID = TmpSigData(2) 
    NodeID = OutToInNodeNum(TmpSigData(1))
    ALLOCATE(control_array(NodeID)%F(PhaseID)%MOV(MovNu),stat=error)
    IF(error /= 0) THEN
        WRITE(911,*) 'allocate control_array phase allocation error'
        STOP
    ENDIF
END SUBROUTINE


!~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>
SUBROUTINE InsertPhaseMove(TmpSigData,Dim,MovID)
    USE DYNUST_MAIN_MODULE
    USE DYNUST_NETWORK_MODULE
    INTEGER,INTENT(in)::Dim,TmpSigData(Dim),MOvID
    INTEGER PhaseID,NodeID,OBLink,GoP,UMOVE
    LOGICAL FindLK
    NodeID = OutToInNodeNum(TmpSigData(2))
    PhaseID = TmpSigData(3)
    IF(nodeid < 1.or.OutToInNodeNum(TmpSigData(1)) < 1) THEN
        WRITE(911,*) "Error in control.dat"
        WRITE(911,*) "Check phase movement for node", m_dynust_network_node_nde(NodeID)%IntoOutNodeNum
        WRITE(911,*) "Phase ", tmpSigData(3)
        STOP
    ENDIF
  
    IBL = GetFLinkFromNode(OutToInNodeNum(TmpSigData(1)),OutToInNodeNum(TmpSigData(2)),3)
    control_array(NodeID)%F(PhaseID)%MOV(MovID)%InLinkNu = IBL
    control_array(NodeID)%F(PhaseID)%MOV(MovID)%NOBAppch = TmpSigData(4)
    DO i = 1, TmpSigData(4) ! # of OB links
        OBLink = GETFLinkFromNode(OutToInNodeNum(TmpSigData(2)),OutToInNodeNum(TmpSigData(4+i)),3)
	    FindLK = .false.
        UMove = 0
	    DO isss = 1, m_dynust_network_arc_nde(IBL)%llink%no
            IF(m_dynust_network_arcmove_nde(IBL,isss)%move == 6) UMove = isss ! record u-turn
	        IF(m_dynust_network_arc_nde(IBL)%llink%p(isss) == OBLink) THEN
                FindLK = .true.
		        control_array(NodeID)%F(PhaseID)%MOV(MovID)%OBAppNu(i) = isss
                m_dynust_network_arcmove_de(IBL,isss)%SignalPreventFor= 0
	        ENDIF
	    ENDDO
	    IF(.not.FindLK.or.OBLink < 1) THEN
            print *, 'not finding movement in InsertPhaseMove'
	    ENDIF
    ENDDO
    ! Add U turn IF U-turn is allowed
    IF(UMove > maxmove) THEN
        WRITE(911,*) "error in phase-movement for node ", m_dynust_network_node_nde(OutToInNodeNum(TmpSigData(2)))%IntoOutNodeNum, "phase ", TmpSigData(3)
        WRITE(911,*) "Please check control.dat"
        STOP
    ENDIF
    IF(UMove > 0) THEN
        IF(m_dynust_network_arcmove_de(IBL,UMove)%GeoPreventFor < 1) THEN ! check IF U turn allowed
	        FindLK = .false.  
            control_array(NodeID)%F(PhaseID)%MOV(MovID)%NOBAppch=control_array(NodeID)%F(PhaseID)%MOV(MovID)%NOBAppch+1
            NewOB = control_array(NodeID)%F(PhaseID)%MOV(MovID)%NOBAppch
	        OBLink = GETFLinkFromNode(m_dynust_network_arc_nde(IBL)%idnod,m_dynust_network_arc_nde(IBL)%iunod,3)
		    control_array(NodeID)%F(PhaseID)%MOV(MovID)%OBAppNu(NewOB) = UMove
            m_dynust_network_arcmove_de(IBL,UMove)%SignalPreventFor= 0
        ENDIF
    ENDIF
END SUBROUTINE


!~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>
SUBROUTINE DeclareOBLink(TmpSigData,Dim,MovID)
    USE DYNUST_MAIN_MODULE
    USE DYNUST_NETWORK_MODULE
  
    INTEGER,INTENT(in)::Dim,TmpSigData(Dim),MovID
    INTEGER ERROR,PhaseID
    MovNu = TmpSigData(4)
    PhaseID = TmpSigData(3) 
    NodeID = OutToInNodeNum(TmpSigData(2))
    IF(NodeID < 1) THEN
        WRITE(911,*) 'error in reading phasing data for node'
	    WRITE(911,*) 'From => to', TmpSigData(1),TmpSigData(2)
	    WRITE(911,*) 'check number of phases matches the second block'
	    WRITE(911,*) 'also check node numbers for the movement downstream nodes'
	    STOP
    ENDIF
    ALLOCATE(control_array(NodeID)%F(PhaseID)%MOV(MovID)%ObAppNu(MovNu+1),stat=error)
    IF(error /= 0) THEN
        WRITE(911,*) 'allocate control_array phase allocation error'
        STOP
    ENDIF
END SUBROUTINE

!~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>
SUBROUTINE AssignGreen(IntoOutNodeNumber,CurrentActPhase,NIBlink,AssG)
    USE DYNUST_MAIN_MODULE
    USE DYNUST_NETWORK_MODULE

    INTEGER,INTENT(in)::IntoOutNodeNumber,CurrentActPhase,NIBlink
    INTEGER(1) AssG
    INTEGER IFF,oblNu
    LOGICAL ifound
    ifound = .false.
    DO iff = 1, NIBlink
        InlinkNu = control_array(IntoOutNodeNumber)%F(CurrentActPhase)%Mov(iff)%InlinkNu
        ObLNu    = control_array(IntoOutNodeNumber)%F(CurrentActPhase)%Mov(iff)%Nobappch
            DO nos = 1, ObLNu
                MovNu = control_array(IntoOutNodeNumber)%F(CurrentActPhase)%Mov(iff)%obappnu(nos)
                IF(m_dynust_network_arc_nde(InlinkNu)%llink%p(MovNu) == m_dynust_network_arc_de(inlinkNu)%RightLane) THEN 
                    m_dynust_network_arcmove_de(InlinkNu,MovNu)%green = 6.0 ! always assign 6 sec to righ-turn movement IF RT permitted on red note that green is set to zero
                    ifound=.true.
                ELSE
                    m_dynust_network_arcmove_de(InlinkNu,MovNu)%green = AssG ! give green time to all permissible movements
                ENDIF
            ENDDO
    ENDDO
END SUBROUTINE


!~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>
LOGICAL FUNCTION FindTimeNeeded(IntoOutNodeNumber,currentactphase) ! find time needed
    USE DYNUST_MAIN_MODULE
    USE DYNUST_NETWORK_MODULE
  
    INTEGER,INTENT(in)::IntoOutNodeNumber,CurrentActPhase
    INTEGER nibp,Qcounter
    REAL DETCAP
    FindTimeNeeded = .false.
    nibp = GETSIGNALPAR(IntoOutNodeNumber,CurrentActPhase,5)
    Qcounter = 0
    DetCap = 0.0
    DO iss = 1, nibp
        Nlinktmp = control_array(IntoOutNodeNumber)%F(CurrentActPhase)%Mov(iss)%inlinknu
        Qcounter = Qcounter + m_dynust_network_arc_de(nlinktmp)%vehicle_queue
        DetCap = DetCap + GreenExt*m_dynust_network_arc_de(nlinktmp)%SatFlowRate ! SatFlowRate unit is # Veh/Sec
    ENDDO
    IF(Qcounter/DetCap >= ActuationThreshold) THEN ! extend the green only IF there are sufficient number of vehicles in queue
	    FindTimeNeeded = .true.
    ENDIF
END FUNCTION

!~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>
INTEGER FUNCTION GETSIGNALPAR(IntoOutNodeNumber,CurrentActPhase,InCode)
    INTEGER,INTENT(in)::IntoOutNodeNumber,CurrentActPhase,InCode
    IF(InCode == 1) THEN
        GetSignalPar = control_array(IntoOutNodeNumber)%F(CurrentActPhase)%Status
    ELSEIF(InCode == 2) THEN 
        GetSignalPar = control_array(IntoOutNodeNumber)%F(CurrentActPhase)%UsedG
    ELSEIF(InCode == 3) THEN 
        GetSignalPar = control_array(IntoOutNodeNumber)%F(CurrentActPhase)%ming_g
    ELSEIF(InCode == 4) THEN
        GetSignalPar = control_array(IntoOutNodeNumber)%F(CurrentActPhase)%maxg_offset
    ELSEIF(InCode == 5) THEN 
        GetSignalPar = control_array(IntoOutNodeNumber)%F(CurrentActPhase)%nibapp 
    ENDIF
END FUNCTION

!~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>
SUBROUTINE DESTROY_CONTROL_ARRAY
    USE DYNUST_MAIN_MODULE
    DO i=1,noofnodes-noof_master_destinations
        IF(control_array(i)%ControlType == 4.or.control_array(i)%ControlType == 5) THEN
            DO j = 1, control_array(i)%NPhase
		        DO k = 1, control_array(i)%F(j)%nibapp
		            DEALLOCATE(control_array(i)%F(j)%Mov(k)%obappNu)
                ENDDO
		        DEALLOCATE(control_array(i)%F(j)%Mov)
		    ENDDO
		    DEALLOCATE(control_array(i)%F)
        ENDIF
    ENDDO
    DEALLOCATE(control_array)
END SUBROUTINE
     
END MODULE 
