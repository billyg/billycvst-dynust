SUBROUTINE SIGNAL_PERMISSIVE_LEFT_RATE(ilink,MFRtmp,t,typeid,moveno)
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~ Copyright (C) 2002-2011 The Arizona Board of Regents on Behalf of The University of Arizona.  ~~!
!~~ Developed by Yi-Chang Chiu, Ph.D. as par to the DynusT project.                               ~~!
!~~ This program is free software: you can redistribute it and/or modify it                       ~~!
!~~ under the terms of the GNU General Public License version 2 as published by                   ~~!
!~~ the Free Software Foundation.                                                                 ~~!
!~~ This program is distributed in the hope that it will be useful,                               ~~!
!~~ but WITHOUT ANY WARRANTY; without even the implied warranty of                                ~~!
!~~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                                          ~~!
!~~ See the GNU General Public License for more details.                                          ~~!
!~~ You should have received a copy of the GNU General Public License                             ~~!
!~~ along with this program (in the file COPYING).                                                ~~!
!~~ IF not, see <http://www.gnu.org/licenses/>.                                                   ~~!
!~~ Please see http://www.dynust.net for contact information and names                            ~~!
!~~ of individual contributors.                                                                   ~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!

	USE DYNUST_MAIN_MODULE
	USE DYNUST_VEH_MODULE
	USE DYNUST_NETWORK_MODULE
    USE DYNUST_LINK_VEH_LIST_MODULE
    USE RAND_GEN_INT
	
	INTEGER typeid,moveno
	REAL MFRtmp, arrivalveh,arrivalveh1, sana, temptraveltime,tempoffset,pid,s_pm
!> > > > > > > > > >                < < < < < < < < < < <
!>>>>>>>>>>>>>>>>>>>CODES START HERE<<<<<<<<<<<<<<<<<<<<<
!> > > > > > > > > >                < < < < < < < < < < <
   
    Select Case (typeid)

    case (0) !  in pcphpl unit
      MFRtmp=m_dynust_network_arc_de(ilink)%MaxFlowRate


	case (1)  ! signalized/signed arterials

      left_count=0 
      left_hv_count = 0
      DO iveh = 1, LinkVehList(ilink)%Fid
        idveh=LinkVehList(ilink)%P(iveh)

        IF(idveh > 0) THEN
          IF(m_dynust_veh(idveh)%qflag) THEN
             DO k=1,m_dynust_network_arc_nde(ilink)%llink%no
                IF(m_dynust_veh(idveh)%nexlink(1) == m_dynust_network_arc_nde(ilink)%llink%p(k)) THEN
                  IF(m_dynust_network_arcmove_nde(ilink,k)%move == 1) THEN
                     left_count=left_count+m_dynust_veh(idveh)%vhpce/AttScale
                     IF(m_dynust_veh(idveh)%vhpce/AttScale > 1.0) THEN
                       left_hv_count = left_hv_count + m_dynust_veh(idveh)%vhpce/AttScale
                     ENDIF
                  ENDIF
                  exit
                ENDIF
             end do
          ENDIF
        ENDIF

      ENDDO   
 
      baycap =   (m_dynust_network_arc_de(ilink)%bay*Baylength)/20.0   ! calclate # of veh can fit into the bay
      IF(left_count < 1.or.(left_count > 0.and.m_dynust_network_arc_de(ilink)%bay > 0.and.left_count <= baycap)) THEN

	      MFRtmp=m_dynust_network_arc_de(ilink)%SatFlowRate  ! sat() here is the total saturation rate for all lanes

	  ELSE ! not LT bay, shared T and L lane or with LT bay but spillover occurs

      IF(m_dynust_network_arc_de(ilink)%pcetotal < 1) THEN
           plt=0
      ELSE
           plt=(left_count-baycap)/m_dynust_network_arc_de(ilink)%pcetotal
      ENDIF

        flt=1.0/(1.0+0.05*plt)
        MFRtmp=m_dynust_network_arc_de(ilink)%SatFlowRate*flt
      ENDIF

    IF(left_count > 0) THEN
      left_hv_count = left_hv_count/float(left_count)
    ELSE
      left_hv_count = 0
    ENDIF
    IF(m_dynust_network_arc_de(ilink)%opp_linkS == 0) THEN ! no opposing link in the current phase i.e. protected left-turn
         m_dynust_network_arc_de(ilink)%left_capacity=MFRtmp/m_dynust_network_arc_de(ilink)%nlanes*max(1,m_dynust_network_arc_de(ilink)%bay)*6.0
         r3 = ranxy(1) 
        IF(r3 <= (m_dynust_network_arc_de(ilink)%left_capacity-ifix(m_dynust_network_arc_de(ilink)%left_capacity))) m_dynust_network_arc_de(ilink)%left_capacity = ifix(m_dynust_network_arc_de(ilink)%left_capacity) + 1


    ELSE  ! with opposing link in the current phase - i.e. permitted left
! -- Apply Chang, G.-L., Chen, C.-Y. and Perez, C (2007) "Hybrid Model for Estimating Permitted Left-Turn Saturation Flow Rate", TRR, 2007.      
       arrivalveh1 = 0
       ipc = LinkVehList(m_dynust_network_arc_de(ilink)%opp_linkS)%Fid
       ipc = 0
        DO iveh = 1, ipc
         idveh=LinkVehList(m_dynust_network_arc_de(ilink)%opp_linkS)%P(iveh)
         IF(idveh > 0) THEN
          IF(m_dynust_veh(idveh)%position-m_dynust_network_arc_de(m_dynust_network_arc_de(ilink)%opp_linkS)%v*xminPerSimInt < 0) THEN
            DO js = 1,m_dynust_network_arc_nde(m_dynust_network_arc_de(ilink)%opp_linkS)%llink%no
              moveturn = GetMoveTurn_LL(m_dynust_network_arc_de(ilink)%opp_linkS,m_dynust_network_arc_nde(m_dynust_network_arc_de(ilink)%opp_linkS)%llink%p(js))
	          IF(moveturn == 2.and.m_dynust_network_arc_nde(m_dynust_network_arc_de(ilink)%opp_linkS)%llink%p(js) == m_dynust_veh(idveh)%nexlink(1)) THEN
		        arrivalveh1 = arrivalveh1 + 1
                go to 2000
		      ENDIF            
            ENDDO
          ENDIF
         ENDIF
2000    CONTINUE
        ENDDO   
! this is too high, need to consider only the through traffic
        arrivalveh = min(m_dynust_network_arc_de(m_dynust_network_arc_de(ilink)%opp_linkS)%SatFlowRate*3600.0,arrivalveh1*600.0) ! convert vph value
        IF(arrivalveh > 0) THEN
            Sana = arrivalveh*exp(-t_cr*(arrivalveh/3600))/(1-exp(-(1.0/MFRtmp)*(arrivalveh/3600)))
        ELSE
            Sana = 1000 ! HCM value
        ENDIF
        temptraveltime = (m_dynust_network_arc_de(ilink)%xl/m_dynust_network_arc_de(ilink)%nlanes)/m_dynust_network_arc_de(ilink)%v/0.1 ! convert to seconds
        tempoffset = 0
        IF(float(m_dynust_network_node(m_dynust_network_arc_nde(ilink)%idnod)%node(4)) < 1) THEN
          print *, "error in adjust sat"
        ENDIF
        Pid = (mod(nint(temptraveltime+tempoffset),m_dynust_network_node(m_dynust_network_arc_nde(ilink)%idnod)%node(4)))/float(m_dynust_network_node(m_dynust_network_arc_nde(ilink)%idnod)%node(4))
        

        S_pm = min(3600.0*MFRtmp, 959+0.498*Sana - 88*(t_cr-5.0) - 264*(1.0/(MFRtmp/m_dynust_network_arc_de(ilink)%nlanes)-2.0) - 64*m_dynust_network_arc_de(ilink)%opp_lane - 0.37*(arrivalveh/m_dynust_network_arc_de(ilink)%opp_lane) + 139*Pid - 3.77*left_hv_count)
        s_pm = max(200.0,s_pm) ! set minimal left-cap to be 200 veh/hr/lane
        
!        IF(s_pm < 0) THEN
!          WRITE(511,*) "s_pm < 0, dumping variables values...."
!          WRITE(511,*) s_pm, ilink, mfrtmp, sana, t_cr, m_dynust_network_arc_de(ilink)%nlanes,m_dynust_network_arc_de(ilink)%opp_lane,arrivalveh, pid,left_hv_count
!          s_pm = 0 
!        ENDIF
        
       
        !! Sana: analytical estimation
        !! arrivalveh: Total opposing through flow rate
        !! t_cr: critical gap for left-turn vehicles (input)
        !! left_hv_count(ilink) : heavy vehicle percentage of left-turn flow
        !! LTSat : average queue discharge headway (input)
        !! opp_lane(ilink) : number of opposing lanes
        ! Pid : indication for signal progression

         m_dynust_network_arc_de(ilink)%left_capacity=s_pm/3600.0 ! per second per lane
         r3 = ranxy(2) 
        IF(r3 <= (m_dynust_network_arc_de(ilink)%left_capacity-ifix(m_dynust_network_arc_de(ilink)%left_capacity))) m_dynust_network_arc_de(ilink)%left_capacity = ifix(m_dynust_network_arc_de(ilink)%left_capacity) + 1

    ENDIF
    
   case (2) !    right turn capacity

      IF(m_dynust_network_arc_de(ilink)%bayR > 0) THEN
          m_dynust_network_arc_de(ilink)%right_capacity = RTSat/3600.0
      ELSE
          m_dynust_network_arc_de(ilink)%right_capacity = (RTSat*0.8)/3600.0 ! per second, only 80% of the total
      ENDIF

   case (4) !     right turn capacity
      MFRtmp=m_dynust_network_arc_de(ilink)%SatFlowRate   

	end select 

END SUBROUTINE
