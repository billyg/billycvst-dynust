SUBROUTINE GENERATE_DEMAND(l)
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~ Copyright (C) 2002-2011 The Arizona Board of Regents on Behalf of The University of Arizona.  ~~!
!~~ Developed by Yi-Chang Chiu, Ph.D. as par to the DynusT project.                               ~~!
!~~ This program is free software: you can redistribute it and/or modify it                       ~~!
!~~ under the terms of the GNU General Public License version 2 as published by                   ~~!
!~~ the Free Software Foundation.                                                                 ~~!
!~~ This program is distributed in the hope that it will be useful,                               ~~!
!~~ but WITHOUT ANY WARRANTY; without even the implied warranty of                                ~~!
!~~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                                          ~~!
!~~ See the GNU General Public License for more details.                                          ~~!
!~~ You should have received a copy of the GNU General Public License                             ~~!
!~~ along with this program (in the file COPYING).                                                ~~!
!~~ IF not, see <http://www.gnu.org/licenses/>.                                                   ~~!
!~~ Please see http://www.dynust.net for contact information and names                            ~~!
!~~ of individual contributors.                                                                   ~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
     USE DYNUST_MAIN_MODULE
     USE DYNUST_VEH_MODULE
     USE DYNUST_NETWORK_MODULE
	 USE DYNUST_VEH_PATH_ATT_MODULE
	 USE DYNUST_TRANSIT_MODULE
     USE DFPORT
     USE RAND_GEN_INT     
     !INTERFACE
     !  SUBROUTINE GENERATE_VEH_ATTRIBUTES(t,ilink,i,j,jdst)
     !    REAL ts
     !    INTEGER ilk,ii,justveh,jdst
     !  END SUBROUTINE
     !END INTERFACE
     !INTERFACE
     !  SUBROUTINE IMSLSort(Ain,ROWS,COLS)     
     !     INTEGER ROWS,COLS
     !     INTEGER, INTENT(inout), DIMENSION(ROWS,COLS) :: AIN
     !  END SUBROUTINE
     !END INTERFACE
     INTEGER, ALLOCATABLE::AinTemp(:,:)
	 INTEGER, ALLOCATABLE::VehAinTmp(:,:)
	 REAL vehtmp(3),vehtmpratio(3),vehtmpro(3)
     INTEGER vlglocal,genlk
     LOGICAL,save:: readflag, readflagT, readflagH,TransitExist
     REAL r

!> > > > > > > > > >                < < < < < < < < < < <
!>>>>>>>>>>>>>>>>>>>CODES START HERE<<<<<<<<<<<<<<<<<<<<<
!> > > > > > > > > >                < < < < < < < < < < <

     
     demandmidcounter = justveh
     
     IF(l == 1) THEN
	    ALLOCATE(VehAin(VehAinCounter,5))     
        ALLOCATE(DestDemandOK(nzones))
        readflag = .false.
        readflagT = .false.
        readflagH = .false.
     ENDIF
     VehAin(:,:) = 0
     DestDemandOK(:)=.false.
     vlgcounter = 0
	 readflag=.false.
	 readflagT=.false.

! GENERATE ALL VEHICLES FROM NOW L TO NEXT TDSP CALCULATION
DO 2345 linter = l, l + tdspstep-1
	 tend=linter*xminPerSimInt
     IF(CntDemTime < 1) tnext = begint(1)
     IF(classpro2(2)-classpro2(1) > 0.001) THEN
	   IF(CntDemTimeT < 1) tnextT = begintT(1)
     ENDIF
     IF(classpro2(3)-classpro2(2) > 0.001) THEN
	   IF(CntDemTimeH < 1) tnextH = begintH(1)
     ENDIF

  IF(tend > (tnext+0.005)) THEN
      readflag = .true.
	  DemOrig(:) = 0
	  DemDest(:) = 0
	  DemCell(:,:)= 0
	  DemOrigAcu(:,:)= 0
	  zoneDemandProfile(:,:,:) = 0
            
	  CntDemTime = CntDemTime + 1

      IF(CntDemTime <= nints) THEN
       misflag = .false.
       checkflag = .false.
	   READ(42,*)
	   IF(loadszdem.and.bg_num > 0) THEN 
	     READ(545,*)
	   ENDIF	     
       DO 223 iz=1,nzones
        IF(InputCode == 0) THEN
          READ(42,224,iostat=error) (DemCell(iz,izz),izz=1,nzones)
          IF(CntDemTime <= bg_num.and.nintsS > 0.and.superzoneswitch > 0.and.bg_num > 0) THEN
            READ(545,224,iostat=error)(DemCellS(iz,izz),izz=1,nzones)
          ENDIF  
        ELSEIF(InputCode > 0) THEN
          READ(42,*,iostat=error) (DemCell(iz,izz),izz=1,nzones)
          DemCell(:,:)=DemCell(:,:)*multi          
          IF(CntDemTime <= bg_num.and.nintsS > 0.and.superzoneswitch > 0.and.bg_num > 0) THEN
            READ(545,*,iostat=error) (DemCellS(iz,izz),izz=1,nzones)
          ENDIF
	    ELSE
          WRITE(911,*) 'error in reading demand.dat or super zone demand'
	      STOP
	    ENDIF
	   IF(error /= 0) THEN
         WRITE(911,*) 'Error when reading demand.dat'
	     STOP
	   ENDIF
223   CONTINUE
224   FORMAT(6f10.4)

      DemCell(:,:)=DemCell(:,:)*multi
      IF(ALLOCATED(DemCellS)) DemCellS(:,:)=DemCellS(:,:)*multiS
      IF(loadszdem) THEN
        DemCell(:,:) = bg_factor(CntDemTime)*DemCell(:,:)+DemCellS(:,:) ! combine bg demand and super zone demand
      ENDIF

      DO iz=1,nzones
         DO izz=1,nzones
           DemOrig(iz)=DemOrig(iz)+DemCell(iz,izz)
           DemDest(iz)=DemDest(iz)+DemCell(izz,iz)
         ENDDO
      ENDDO

      tnext=begint(CntDemTime+1)
      DO iz=1,nzones
        DemGenZ(iz)=DemOrig(iz)/((1/xminPerSimInt)*(tnext-begint(CntDemTime)))
      ENDDO

        DO iz=1,nzones
          IF(DemOrig(iz) > 0.00005) THEN
            DemOrigAcu(iz,1)=DemCell(iz,1)/DemOrig(iz)
            DO izz = 2, nzones
               DemOrigAcu(iz,izz)=DemOrigAcu(iz,izz-1)+DemCell(iz,izz)/DemOrig(iz)
	        ENDDO
          ELSE
            DemOrigAcu(iz,:)=0.0
          ENDIF
        ENDDO

        ELSE ! current time is greater than max number of demand duration
          DemGenZ(:)=0
        ENDIF

  IF(classpro2(2)-classpro2(1) > 0.001.and.vehprofile(2)%odorfile == 1) THEN ! reading truck from file
	 IF(nintsT < 1) THEN
	   WRITE(911,*) "truck demand file is specified to be read but this file is not ready"
	   WRITE(911,*) "please demand_truck.dat"
	   STOP
	 ENDIF
	 IF(tend > (tnextT+0.005).and..not.readflagT) THEN
	  readflagT=.true.
      DemOrigT(:) = 0.0  
      DemDestT(:) = 0.0
	  DemCellT(:,:)= 0
      DemOrigAcuT(:,:) = 0
            
	  CntDemTimeT = CntDemTimeT + 1

      IF(CntDemTimeT <= nintsT) THEN
	    READ(54,*)
       
	    DO 2233 iz=1,nzones

        IF(InputCode == 0) THEN
         READ(54,224,iostat=error) (DemCellT(iz,izz),izz=1,nzones)
        ELSEIF(InputCode > 0) THEN
         READ(54,*,iostat=error) (DemCellT(iz,izz),izz=1,nzones)
	    ELSE
         WRITE(911,*) 'error in reading demand_truck.dat'
	     STOP
	    ENDIF

	    IF(error /= 0) THEN
         WRITE(911,*) 'Error in gen when reading demand_truck.dat origin zone ', iz
	     STOP
	    ENDIF
2233    CONTINUE
2244    FORMAT(6f10.4)
        DemCellT(:,:)=DemCellT(:,:)*multiT
	    tnextT=begintT(CntDemTimeT+1)

        DO iz=1,nzones
          DO izz=1,nzones
            DemOrigT(iz)=DemOrigT(iz)+DemCellT(iz,izz)
            DemDestT(iz)=DemDestT(iz)+DemCellT(izz,iz)
          ENDDO

          DemGenZT(iz)=DemOrigT(iz)/((1/xminPerSimInt)*(tnextT-begintT(CntDemTimeT)))
	    ENDDO
        DO iz=1,nzones
          IF(DemOrigT(iz) > 0.00005) THEN
            DemOrigAcuT(iz,1)=DemCellT(iz,1)/DemOrigT(iz)
            DO izz = 2, nzones
               DemOrigAcuT(iz,izz)=DemOrigAcuT(iz,izz-1)+DemCellT(iz,izz)/DemOrigT(iz)
	        ENDDO
          ELSE
            DemOrigAcuT(iz,:)=0.0
          ENDIF
        ENDDO
      ELSE ! current time is greater than max number of demand duration
          DemGenZT(:)=0
      ENDIF
   ENDIF

  ELSE !with truck but the truck is from auto share
      DemOrigT(:) = DemOrig(:)*(classpro2(2)-classpro2(1))/classpro2(1)
      DemDestT(:) = DemDest(:)*multi*(classpro2(2)-classpro2(1))/classpro2(1)
	  DemCellT(:,:)=DemCell(:,:)*(classpro2(2)-classpro2(1))/classpro2(1)
      DemGenZT(:)=DemGenZ(:)*(classpro2(2)-classpro2(1))/classpro2(1)
      DemOrigAcuT(:,:)=DemOrigAcu(:,:)
 ENDIF

  IF(classpro2(3)-classpro2(2) > 0.001.and.vehprofile(3)%odorfile == 1) THEN ! reading hov from file
	 
	 IF(tend > (tnextH+0.005).and..not.readflagH) THEN
	  readflagH=.true.
      DemOrigH(:) = 0.0  
      DemDestH(:) = 0.0
	  DemCellH(:,:)= 0
      DemOrigAcuH(:,:) = 0
	  CntDemTimeH = CntDemTimeH + 1

      IF(CntDemTimeH <= nintsH) THEN
	    READ(544,*)

	    DO 22337 iz=1,nzones
        IF(InputCode == 0) THEN
         READ(544,224,iostat=error) (DemCellH(iz,izz),izz=1,nzones)
        ELSEIF(InputCode > 0) THEN
         READ(544,*,iostat=error) (DemCellH(iz,izz),izz=1,nzones)
	    ELSE
         WRITE(911,*) 'error in reading demand_hov.dat'
	     STOP
	    ENDIF
	    IF(error /= 0) THEN
         WRITE(911,*) 'Error in gen when reading demand_hov.dat'
	     STOP
	    ENDIF
22337   CONTINUE
        DemCellH(:,:)=DemCellH(:,:)*multiH
	    tnextH = begintH(CntDemTimeH+1)
        DO iz=1,nzones
          DO izz=1,nzones
           DemOrigH(iz)=DemOrigH(iz)+DemCellH(iz,izz)
           DemDestH(iz)=DemDestH(iz)+DemCellH(izz,iz)
          ENDDO
          DemGenZH(iz)=DemOrigH(iz)/((1/xminPerSimInt)*(tnextH-begintH(CntDemTimeH)))
	    ENDDO
        DO iz=1,nzones
          IF(DemOrigH(iz) > 0.00005) THEN
            DemOrigAcuH(iz,1)=DemCellH(iz,1)/DemOrigH(iz)
            DO izz = 2, nzones
               DemOrigAcuH(iz,izz)=DemOrigAcuH(iz,izz-1)+DemCellH(iz,izz)/DemOrigH(iz)
	        ENDDO
          ELSE
            DemOrigAcuH(iz,:)=0.0
          ENDIF
        ENDDO

      ELSE ! current time is greater than max number of demand duration
          DemGenZH(:)=0
      ENDIF
   ENDIF
  ELSE !with hov but the hov is from auto share
      DemOrigH(:) = DemOrig(:)*(classpro2(3)-classpro2(2))/classpro2(1)
      DemDestH(:) = DemDest(:)*(classpro2(3)-classpro2(2))/classpro2(1)      
	  DemCellH(:,:) = DemCell(:,:)*(classpro2(3)-classpro2(2))/classpro2(1)
      DemGenZH(:)=DemGenZ(:)*(classpro2(3)-classpro2(2))/classpro2(1)
      DemOrigAcuH(:,:)=DemOrigAcu(:,:)
 ENDIF
     iflag = 0
     seetmp = 0
 
     DO i  =1, nzones
     DO j = 1, nzones
     IF(vehprofile(2)%odorfile < 1.and.vehprofile(3)%odorfile < 1) THEN !demand.dat only
       seetmp = DemCell(i,j)/vehprofile(1)%vehfrac 
       iflag = 1
     ELSEIF(vehprofile(2)%odorfile > 0.and.vehprofile(3)%odorfile < 1) THEN !sov and truck
       seetmp = DemCell(i,j)/vehprofile(1)%vehfrac + DemCellT(i,j)
       iflag = 2
     ELSEIF(vehprofile(2)%odorfile == 0.and.vehprofile(3)%odorfile > 0) THEN !sov and hov
       seetmp = DemCell(i,j)/vehprofile(1)%vehfrac + DemCellH(i,j)
       iflag = 3
     ELSE ! all demand files
       seetmp = DemCell(i,j)+DemCellT(i,j)*multiT+DemCellH(i,j)
       iflag = 4
     ENDIF  
 
   IF(seetmp > 0.00001) THEN
     select case(iflag)
     case(1) !Demand.dat only
       zoneDemandProfile(1,i,j) = classpro2(1)
       zoneDemandProfile(2,i,j) = classpro2(2)
       zoneDemandProfile(3,i,j) = classpro2(3)
     case(2) !sov and truck
       zoneDemandProfile(1,i,j) = DemCell(i,j)/seetmp
       zoneDemandProfile(2,i,j) = DemCellT(i,j)/seetmp + zoneDemandProfile(1,i,j)
       zoneDemandProfile(3,i,j) = 1.0
     case(3) !sov and hov
       zoneDemandProfile(1,i,j) = DemCell(i,j)/seetmp
       zoneDemandProfile(2,i,j) = (vehprofile(2)%vehfrac/vehprofile(1)%vehfrac)*DemCell(i,j)*multi/seetmp+zoneDemandProfile(1,i,j)
       zoneDemandProfile(3,i,j) = 1.0
     case(4)
       zoneDemandProfile(1,i,j) = DemCell(i,j)/seetmp
       zoneDemandProfile(2,i,j) = DemCellT(i,j)/seetmp+zoneDemandProfile(1,i,j)
       zoneDemandProfile(3,i,j) = 1.0
     end select
   ELSE
       zoneDemandProfile(:,i,j)= 0
   ENDIF
     ENDDO
     ENDDO
ENDIF !IF(CntDemTime <= nints) THEN
    
     m_dynust_network_arc_de(:)%vlg=0.0
     m_dynust_network_arc_de(:)%expgen = 0
     vehtmp(:) = 0.0

      DO 68 iz=1,nzones
      
      DO 68 il=1,NoofGenLinksPerZone(iz)
        vlglocal = 0 
         IF(LoadWeightID(iz)) THEN
           m_dynust_network_arc_de(LinkNoInZone(iz,il))%expgen=DemGenZ(iz)*m_dynust_network_arc_de((LinkNoInZone(iz,il)))%LoadWeight
           vehtmp(1) = m_dynust_network_arc_de(LinkNoInZone(iz,il))%expgen
           IF(truckok) THEN
             m_dynust_network_arc_de(LinkNoInZone(iz,il))%expgen=m_dynust_network_arc_de(LinkNoInZone(iz,il))%expgen+ DemGenZT(iz)*m_dynust_network_arc_de((LinkNoInZone(iz,il)))%LoadWeight
             vehtmp(2) = DemGenZT(iz)*m_dynust_network_arc_de((LinkNoInZone(iz,il)))%LoadWeight/TotalLinkLenPerZone(iz)
           ENDIF
           IF(hovok) THEN
             m_dynust_network_arc_de(LinkNoInZone(iz,il))%expgen=m_dynust_network_arc_de(LinkNoInZone(iz,il))%expgen+ DemGenZH(iz)*m_dynust_network_arc_de((LinkNoInZone(iz,il)))%LoadWeight
             vehtmp(3) = DemGenZH(iz)*m_dynust_network_arc_de((LinkNoInZone(iz,il)))%LoadWeight/TotalLinkLenPerZone(iz)
           ENDIF
		 ELSE ! GENERATE DEMAND WITHOUT USING THE LOADWEIGHT
           m_dynust_network_arc_de(LinkNoInZone(iz,il))%expgen=DemGenZ(iz)*m_dynust_network_arc_de(LinkNoInZone(iz,il))%xl/TotalLinkLenPerZone(iz)
           vehtmp(1) = m_dynust_network_arc_de(LinkNoInZone(iz,il))%expgen
           IF(truckok) THEN
             m_dynust_network_arc_de(LinkNoInZone(iz,il))%expgen=m_dynust_network_arc_de(LinkNoInZone(iz,il))%expgen+DemGenZT(iz)*m_dynust_network_arc_de(LinkNoInZone(iz,il))%xl/TotalLinkLenPerZone(iz)
             vehtmp(2) = DemGenZT(iz)*m_dynust_network_arc_de(LinkNoInZone(iz,il))%xl/TotalLinkLenPerZone(iz)
           ENDIF             
           IF(hovok) THEN
             m_dynust_network_arc_de(LinkNoInZone(iz,il))%expgen=m_dynust_network_arc_de(LinkNoInZone(iz,il))%expgen+DemGenZH(iz)*m_dynust_network_arc_de(LinkNoInZone(iz,il))%xl/TotalLinkLenPerZone(iz)
             vehtmp(3) = DemGenZH(iz)*m_dynust_network_arc_de(LinkNoInZone(iz,il))%xl/TotalLinkLenPerZone(iz)
           ENDIF             
         ENDIF
         vehtmpratio(:)= vehtmp(:)/SUM(vehtmp(:))
         vehtmpro(1) = vehtmpratio(1)
         vehtmpro(2) = vehtmpratio(1)+vehtmpratio(2)
         vehtmpro(3) = 1.0
         iflag1 = 0 ! this block is to make sure no generation on those links with downstream not reaching any destinations
         DO jp=1, noof_master_destinations
	      IF(m_dynust_network_node_nde(m_dynust_network_arc_nde(LinkNoInZone(iz,il))%idnod)%connectivity(jp) > 0) THEN
	        iflag1 = 1
	        exit
	      ENDIF
	     ENDDO
         r = ranxy(3)
         
         ! DETERMINE HOW MANY VEHICLES TO BE GENERATED ON EACH LINK
	     IF(iflag1 > 0) THEN
            IF(r <= (m_dynust_network_arc_de(LinkNoInZone(iz,il))%expgen-ifix(m_dynust_network_arc_de(LinkNoInZone(iz,il))%expgen))) THEN
		     IF((m_dynust_network_arc_de(LinkNoInZone(iz,il))%LinkStatus%inci == 0).or.(m_dynust_network_arc_de(LinkNoInZone(iz,il))%Linkstatus%inci /= 0.and.m_dynust_network_arc_de(LinkNoInZone(iz,il))%LinkStatus%incisev < 0.95)) THEN
               vlglocal=ifix(m_dynust_network_arc_de(LinkNoInZone(iz,il))%expgen)+1
			 ENDIF
		   ELSE
		     IF((m_dynust_network_arc_de(LinkNoInZone(iz,il))%LinkStatus%inci == 0).or.(m_dynust_network_arc_de(LinkNoInZone(iz,il))%Linkstatus%inci /= 0.and.m_dynust_network_arc_de(LinkNoInZone(iz,il))%LinkStatus%incisev < 0.95)) THEN
               vlglocal=ifix(m_dynust_network_arc_de(LinkNoInZone(iz,il))%expgen)
			 ENDIF
	       ENDIF
	     ELSE
	       WRITE(911,*) 'Found invalid generation for zone',iz
		   WRITE(911,*) 'This link does not have proper connectivity', m_dynust_network_node_nde(m_dynust_network_arc_nde(LinkNoInZone(iz,il))%iunod)%IntoOutNodeNum,'->',m_dynust_network_node_nde(m_dynust_network_arc_nde(LinkNoInZone(iz,il))%idnod)%IntoOutNodeNum
		   WRITE(911,*) 'Please check origin.dat'
		   STOP
	     ENDIF
		 t = (linter-1)*xminPerSimInt
         iliid = LinkNoInZone(iz,il)

		 IF(iliid < 1) THEN
           WRITE(911,*) 'error in IID in demand generation'
           STOP
		 ENDIF

         ! ASSIGNMENT AN ID, TIME AND GEN LINK ID TO EACH VEH AND PUT IT INTO THE VEHAIN ARRAY
		 DO ih = 1, vlglocal
		   IF(vlgcounter+demandmidcounter >= NoofVeh) THEN
		     go to 100
           ENDIF
		   vlgcounter = vlgcounter + 1

		   IF(vlgcounter > VehAinCounter) THEN
             Allocate (VehAinTmp(VehAinCounter,5))
		     VehAinTmp(:,:) = VehAin(:,:)
             DEALLOCATE(VehAin)
			 ijr = VehAinCounter+VehAinIncre
             Allocate (VehAin(ijr,5))
             VehAin(1:VehAinCounter,:)=VehAinTmp(1:VehAinCounter,:)
			 VehAinCounter=VehAinCounter+VehAinIncre
			 DEALLOCATE(VehAinTmp)
		   ENDIF
		   VehAin(vlgcounter,4) = vlgcounter+demandmidcounter	! Veh ID
		   VehAin(vlgcounter,2) = linter						! time
		   VehAin(vlgcounter,3) = iliid							! link id
	       m_dynust_last_stand(vlgcounter+demandmidcounter)%jorig = iz ! origin zone for this vehicle
           rrs = ranxy(88)                                      ! assign vehicle type 

           DO imore = 1, 3
             IF(vehtmpro(imore) >= rrs) THEN
                m_dynust_last_stand(vlgcounter+demandmidcounter)%vehtype = imore
                exit
             ENDIF
           ENDDO
           IF(IMORE==0) THEN
              STOP
           ENDIF
		   VehAin(vlgcounter,5) = imore                         ! veh type         
!          OBTAIN DETAILED ATTRIBUTES
		   CALL GENERATE_VEH_ATTRIBUTES(t,iliid,iz,vlgcounter+demandmidcounter,jdst)
		   VehAin(vlgcounter,1)= MasterDest(jdst)   			! destination
		   DestDemandOK(jdst) = .true.
	       m_dynust_last_stand(vlgcounter+demandmidcounter)%isec = iliid            ! GENERATION LINKH
!9876
!	       WRITE(9091,*) "VEH IN DEMAND", vlgcounter+demandmidcounter, justveh-1
          
           justveh = justveh + 1
	       

		 ENDDO ! ih = 1, vlglocal
!        GENERATE TRANSIT
         IF(GenTransit > 0.AND.NumRoutes > 0) THEN ! WITH AT LEAST ONE ROUTE
             IF(Query_Transit_Vehicles(linter)) THEN ! QUERY TO SEE IF AT LEAST ONE TRANSIT IS TO BE GENERATED AT LINTER
               TransitExist = .True. 
               DO WHILE(TransitExist)
                 CALL GET_TRANSIT_VEH(linter,genlk,TransitExist,morig,mdest,iRouteID)
                 IF(TransitExist) THEN
                   vlgcounter = vlgcounter + 1
                   VehAin(vlgcounter,1) = mdest
                   VehAin(vlgcounter,4) = vlgcounter+demandmidcounter
                   VehAin(vlgcounter,2) = linter
                   VehAin(vlgcounter,3) = genlk
                   VehAin(vlgcounter,5) = 6
                   NoofBuses = NoofBuses + 1
                   readveh(6,1) = readveh(6,1) + 1
                   CALL GENERATE_TRANSIT_VEH_ATTRIBUTES(linter,vlgcounter+demandmidcounter,mdest,morig,genlk)      
                   CALL RETRIEVE_TRANSIT_PATH(vlgcounter+demandmidcounter,iRouteID)
                   tide = (linter-1)*xminPerSimInt
                   CALL RETRIEVE_NEXT_LINK(tide,vlgcounter+demandmidcounter,genlk,1,Nlnk,Nlnkmv)
                 ENDIF
               ENDDO
             ENDIF
         ENDIF

68    CONTINUE
      
      
2345  ENDDO !linter
           
!100  demandmidcounter = demandmidcounter + vlgcounter
100	  demandmidcounter = justveh

	  ALLOCATE(AinTemp(vlgcounter,5))
      AinTemp(1:vlgcounter,:) = VehAin(1:vlgcounter,:)

	  CALL IMSLSort(AinTemp,vlgcounter,5)
      VehAin(1:vlgcounter,:) = AinTemp(1:vlgcounter,:)
      VehAin(vlgcounter+1:VehAinCounter,:) = 0

	  IF(ALLOCATED(AinTemp)) DEALLOCATE(AinTemp)
	  IF(ALLOCATED(VehAinTmp)) DEALLOCATE(VehAinTmp)
  
	  mop = 1
END SUBROUTINE
