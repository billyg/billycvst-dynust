SUBROUTINE CAL_PENALTY(l)
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~ Copyright (C) 2002-2011 The Arizona Board of Regents on Behalf of The University of Arizona.  ~~!
!~~ Developed by Yi-Chang Chiu, Ph.D. as par to the DynusT project.                               ~~!
!~~ This program is free software: you can redistribute it and/or modify it                       ~~!
!~~ under the terms of the GNU General Public License version 2 as published by                   ~~!
!~~ the Free Software Foundation.                                                                 ~~!
!~~ This program is distributed in the hope that it will be useful,                               ~~!
!~~ but WITHOUT ANY WARRANTY; without even the implied warranty of                                ~~!
!~~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                                          ~~!
!~~ See the GNU General Public License for more details.                                          ~~!
!~~ You should have received a copy of the GNU General Public License                             ~~!
!~~ along with this program (in the file COPYING).                                                ~~!
!~~ IF not, see <http://www.gnu.org/licenses/>.                                                   ~~!
!~~ Please see http://www.dynust.net for contact information and names                            ~~!
!~~ of individual contributors.                                                                   ~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
   USE DYNUST_MAIN_MODULE
   USE DYNUST_NETWORK_MODULE
   USE DYNUST_VEH_PATH_ATT_MODULE
   USE DYNUST_LINK_VEH_LIST_MODULE

   REAL link_Q(noofarcs),link_S(noofarcs)
   INTEGER ips
   INTEGER Itp1,isnode, Nlnk,aggtime, ifd, InitP
   LOGICAL Itp2,idone,havefound
   REAL penrat,lookP

!> > > > > > > > > >                < < < < < < < < < < <
!>>>>>>>>>>>>>>>>>>>CODES START HERE<<<<<<<<<<<<<<<<<<<<<
!> > > > > > > > > >                < < < < < < < < < < <
    
    link_Q(:) = 0
    link_S(:) = 0

    penrat = 0.0
    LookP = 0.0
    initP = 0

!    aggtime = max(1,ifix((l-2)*1.0/simPerAgg)+1)
    aggtime = max(1,ifix(float(l-1)/simPerAgg)+1)
    ifd = max(InitP,nint(LookP/xminPerSimInt/simPerAgg))  
 

     DO i=1,noofarcs
          smps = 0
          qmps = 0
          ! estimate link speed and queue length

          smps = sum(m_dynust_network_arc_de(i)%link_smp(1:simPerAgg))/AttScale
          IF(smps < -0.00001) THEN
            print *, "error in smps"
          ENDIF
          link_S(i) = smps/simPerAgg

          ips = m_dynust_network_arc_de(m_dynust_network_arc_nde(i)%fortobacklink)%link_qmp(1)%psize
          DO isee = 1, max(simPerAgg,tdspstep)
          IF(ips > 0) THEN
            qmps = qmps + sum(m_dynust_network_arc_de(m_dynust_network_arc_nde(i)%fortobacklink)%link_qmp(isee)%p(1:ips))
          ENDIF
          ENDDO

          IF(qmps < 0) print *, "error in qmps"
          link_Q(m_dynust_network_arc_nde(i)%fortobacklink) = qmps/(max(simPerAgg,tdspstep)*ips)
        

          idone = .false.
          IF(m_dynust_network_arc_de(i)%link_count(aggtime) > 0) THEN
		    TravelTime(i,aggtime) = ((m_dynust_network_arc_de(i)%link_time(aggtime)/AttScale)/m_dynust_network_arc_de(i)%link_count(aggtime))*AttScale
          ELSE
			  DO in = 0, min(max(0,aggtime-1),ifd)
			    IF(aggtime-in > 0) THEN
			     IF(m_dynust_network_arc_de(i)%link_count(max(1,aggtime-in)) > 0) THEN ! check forward for non-zero count up to 5 agg intervals
 	  		      TravelTime(i,aggtime) = (m_dynust_network_arc_de(i)%link_time(aggtime-in)/AttScale)/m_dynust_network_arc_de(i)%link_count(max(1,aggtime-in))*AttScale
                  idone=.true.
				  exit
                 ENDIF
                ENDIF
              ENDDO

			IF(.not.idone) THEN ! IF not finding any
                IF(l == 1) THEN
                  speedtmp = m_dynust_network_arc_de(i)%v
                ELSE
                  speedtmp = link_S(i)
                ENDIF
                IF(m_dynust_network_arc_de(i)%LinkStatus%inci /= 2) THEN ! outside incident period
                  TravelTime(i,aggtime) = (m_dynust_network_arc_nde(i)%s/speedtmp)*AttScale
                ELSE ! during incident
                  IF(m_dynust_network_arc_de(i)%LinkStatus%incisev < 0.95) THEN
                    IH = m_dynust_network_arc_de(i)%FlowModelnum
!                    TravelTime(i,aggtime) = s(i)/VKFunc(IH)%V02*60.0 ! traveling at minimal speed
!                    link_time(i,aggtime) = s(i)/VKFunc(IH)%V02*60.0
                    TravelTime(i,aggtime) = (m_dynust_network_arc_nde(i)%s/(m_dynust_network_arc_de(i)%link_speed(aggtime)/AttScale))*AttScale
                    m_dynust_network_arc_de(i)%link_time(aggtime) = (m_dynust_network_arc_nde(i)%s/(m_dynust_network_arc_de(i)%link_speed(aggtime)/AttScale))*AttScale
                  ELSE
                    TravelTime(i,aggtime) = 101.0*AttScale
                    m_dynust_network_arc_de(i)%link_time(aggtime) = 101.0*AttScale
                  ENDIF
                  m_dynust_network_arc_de(i)%link_count(aggtime) = 1
                ENDIF 
			ENDIF   
          ENDIF

         IF(traveltime(i,aggtime) < 0) THEN
           WRITE(911,*) "error in travel time in penalty cal"
         ENDIF

       ENDDO  


      m_dynust_network_arcmove_nde(:,:)%penalty=PenForPreventMove

      DO i=1,noofarcs
      
                
        nodeup=m_dynust_network_arc_nde(i)%iunod
        ip=m_dynust_network_arc_nde(i)%ForToBackLink


         DO ig = 1, m_dynust_network_arc_nde(i)%inlink%no
          is = m_dynust_network_arc_nde(m_dynust_network_arc_nde(i)%inlink%p(ig))%fortobacklink
           mp = getBstMove(m_dynust_network_arc_nde(i)%inlink%p(ig),i)
         
  
          idone = .false.

          havefound=.false.
          
          IF(m_dynust_network_arcmove_de(ip,mp)%SignalPreventBack < 1.and.m_dynust_network_arcmove_de(ip,mp)%GeoPreventBack < 1) THEN
		  IF(m_dynust_network_arc_de(is)%link_outflow_count(aggtime)%psize > 0) THEN
             IF((m_dynust_network_arc_de(is)%link_outflow_count(aggtime)%p(mp)) > 0) THEN
                 m_dynust_network_arcmove_nde(ip,mp)%penalty=(m_dynust_network_arc_de(is)%link_outflow_delay(aggtime)%p(mp)/AttScale)/(m_dynust_network_arc_de(is)%link_outflow_count(aggtime)%p(mp))
                 havefound=.true.
                 idone = .true.
             ENDIF
          ENDIF
          ENDIF
                   IF(.not.idone) THEN
                     IF(m_dynust_network_arcmove_de(ip,mp)%SignalPreventBack < 1.and.m_dynust_network_arcmove_de(ip,mp)%GeoPreventBack < 1) THEN
                     IF(link_Q(is)/(m_dynust_network_arc_de(m_dynust_network_arc_nde(is)%backtoforlink)%xl*m_dynust_network_arc_de(m_dynust_network_arc_nde(is)%backtoforlink)%maxden) > penrat) THEN
                       m_dynust_network_arcmove_nde(ip,mp)%penalty = link_Q(is)/2.0 ! twice as long as the agg interval 
                     ELSE
                       m_dynust_network_arcmove_nde(ip,mp)%penalty = 0.05 ! 3 seconds
                     ENDIF
                     ENDIF
                   ENDIF 
     

                IF(m_dynust_network_arcmove_nde(ip,mp)%penalty < 0) THEN
                   WRITE(911,*) "error in penalty in penalty cal"
                   STOP
                ENDIF
                !IF(m_dynust_network_arcmove_de(ip,mp)%GeoPreventBack > 0.and.&
                !  m_dynust_network_node_nde(m_dynust_network_arc_nde(m_dynust_network_arc_nde(ip)%backtoforlink)%idnod)%IntoOutNodeNum < 900000) THEN

                IF(m_dynust_network_arcmove_de(ip,mp)%GeoPreventBack > 0) THEN
                   m_dynust_network_arcmove_nde(ip,mp)%penalty = PenForPreventMove
                ENDIF ! signalpreventback = 0
        ENDDO
      ENDDO


    IF(mod(l-1,max(simPerAgg,tdspstep)) == 0) THEN ! reset link_qmp
     DO i = 1, noofarcs
      DO j = 1, max(simPerAgg,tdspstep)
        ips = m_dynust_network_arc_de(i)%link_qmp(j)%psize      
        IF(ips > 0) m_dynust_network_arc_de(i)%link_qmp(j)%p(:) = 0
      ENDDO
     ENDDO
    ENDIF

!    IF(iteMax == 0) THEN ! one-shot, so these arrays are not setup for aggint

!    IF((m_dynust_network_arc_de(1)%link_outflow_count(aggtime)%Psize>0)) THEN ! CHECK IF MEMORY IS ALLOCATED BY CHECKING THE FIRST LINK
!      DO is = 1, noofarcs
!        m_dynust_network_arc_de(is)%link_outflow_count(aggtime)%p(:) = 0 
!      ENDDO
!    ENDIF
!    IF((m_dynust_network_arc_de(1)%link_outflow_delay(aggtime)%Psize>0)) THEN ! CHECK IF MEMORY IS ALLOCATED BY CHECKING THE FIRST LINK
!      DO is = 1, noofarcs
!	    m_dynust_network_arc_de(is)%link_outflow_delay(aggtime)%p(:) = 0
!      ENDDO
!    ENDIF
!
!    ENDIF
        
END SUBROUTINE

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
SUBROUTINE CAL_PENALTY_MIVA
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
   USE DYNUST_MAIN_MODULE
   USE DYNUST_NETWORK_MODULE
   USE DYNUST_VEH_PATH_ATT_MODULE
   USE DYNUST_LINK_VEH_LIST_MODULE
   INTEGER Itp1,isnode, Nlnk,aggtime,ifd,initP,LookP
   REAL penrat
   LOGICAL Itp2,havefound

!> > > > > > > > > >                < < < < < < < < < < <
!>>>>>>>>>>>>>>>>>>>CODES START HERE<<<<<<<<<<<<<<<<<<<<<
!> > > > > > > > > >                < < < < < < < < < < <
    
    penrat = 0.0
    LookP = 0.0
    initP = 0

    ifd = max(InitP,nint(LookP/xminPerSimInt/simPerAgg))  ! 100 means search forward and backward for 10 min     

    travelpenalty(:,:,:) = PenForPreventMove
    traveltime(:,:) = 0

      DO i=1,noofarcs
        DO it = 1, aggint

          IF(m_dynust_network_arc_de(i)%link_count(it) > 0) THEN
		    TravelTime(i,it) = ((m_dynust_network_arc_de(i)%link_time(it)/AttScale)/m_dynust_network_arc_de(i)%link_count(it))*AttScale !may have incident panelty already
          ELSE
            idone=.false.
            DO in = 0, min(max(0,it-1),ifd)
			  IF(it-in > 0) THEN
			   IF(m_dynust_network_arc_de(i)%link_count(max(1,it-in)) > 0) THEN ! check forward for non-zero count up to 5 agg intervals
 	  		     TravelTime(i,it) = (m_dynust_network_arc_de(i)%link_time(max(1,it-in))/AttScale)/m_dynust_network_arc_de(i)%link_count(max(1,it-in))*AttScale
                 idone=.true.
			     exit
               ENDIF
              ENDIF
              IF(it+in <= aggint) THEN
               IF(m_dynust_network_arc_de(i)%link_count(min(aggint,it+in)) > 0) THEN ! check backward for non-zero count
 	  		     TravelTime(i,it) = (m_dynust_network_arc_de(i)%link_time(min(aggint,it+in))/AttScale)/m_dynust_network_arc_de(i)%link_count(min(aggint,it+in))*AttScale
                 idone=.true.
			     exit
			   ENDIF
              ENDIF
            ENDDO

			IF(.not.idone) THEN ! IF not finding any
               TravelTime(i,it) = (m_dynust_network_arc_nde(i)%s/(m_dynust_network_arc_de(i)%link_speed(it)/AttScale))*AttScale
			ENDIF   
          ENDIF
        ENDDO
     ENDDO
     
       DO i=1,noofarcs
        nodeup=m_dynust_network_arc_nde(i)%iunod
        ip=m_dynust_network_arc_nde(i)%ForToBackLink


         DO ig = 1, m_dynust_network_arc_nde(i)%inlink%no
          is = m_dynust_network_arc_nde(m_dynust_network_arc_nde(i)%inlink%p(ig))%fortobacklink
          mp = getBstMove(m_dynust_network_arc_nde(i)%inlink%p(ig),i)          

        
          idone = .false.
          DO aggtime = 1, aggint

            idone=.false.
            
            IF(m_dynust_network_arcmove_de(ip,mp)%GeoPreventBack < 1) THEN
		    IF(m_dynust_network_arc_de(is)%link_outflow_count(aggtime)%psize > 0) THEN
				IF(m_dynust_network_arc_de(is)%link_outflow_count(aggtime)%p(mp) > 0) THEN
                  travelpenalty(ip,aggtime,mp)=(m_dynust_network_arc_de(is)%link_outflow_delay(aggtime)%p(mp)/AttScale)/(m_dynust_network_arc_de(is)%link_outflow_count(aggtime)%p(mp))
                  IF(travelpenalty(ip,aggtime,mp) < 0) THEN
                     print *,"error in travel penalty"
                  ENDIF
                  idone=.true.
                ENDIF
            ENDIF

            IF(.not.idone) THEN
 			       DO in = 0, min(max(0,aggtime-1),ifd) ! +- two agg intervals
			         IF(aggtime-in > 0) THEN
			         IF(m_dynust_network_arc_de(is)%link_outflow_count(max(1,aggtime-in))%psize > 0) THEN
			          IF(m_dynust_network_arc_de(is)%link_outflow_count(max(1,aggtime-in))%p(mp) > 0) THEN ! check forward for non-zero count up to 5 agg intervals
 	  		           travelpenalty(ip,aggtime,mp) = (m_dynust_network_arc_de(is)%link_outflow_delay(max(1,aggtime-in))%p(mp/AttScale)/m_dynust_network_arc_de(is)%link_outflow_count(max(1,aggtime-in))%p(mp))
                       idone=.true.
                       IF(travelpenalty(ip,aggtime,mp) < 0) THEN
                         print *,"error in travel penalty"
                       ENDIF
				       exit
                      ENDIF
                     ENDIF
                     ENDIF
                   ENDDO
            ENDIF
            IF(.not.idone) THEN
                IF(((m_dynust_network_arc_de(is)%link_queue(aggtime)%p(mp))/AttScale)/(m_dynust_network_arc_de(is)%xl*m_dynust_network_arc_de(is)%maxden) > penrat) THEN
                 travelpenalty(ip,aggtime,mp) = (m_dynust_network_arc_de(is)%link_queue(aggtime)%p(mp)/AttScale)/2.0
                  IF(travelpenalty(ip,aggtime,mp) < 0) THEN
                     print *,"error in travel penalty"
                  ENDIF
                ELSE
                  travelpenalty(ip,aggtime,mp) = 0.05 ! 3 sec
                ENDIF
            ENDIF 
            IF(travelpenalty(ip,aggtime,mp) < 0) THEN
                WRITE(911,*) "error in travelpenalty in penalty cal"
                STOP
            ENDIF
           ENDIF !            IF(GeoPreventBack(ip,mp) < 1) THEN
        
        ENDDO ! aggtime
        ENDDO !j
      ENDDO ! i
    
END SUBROUTINE