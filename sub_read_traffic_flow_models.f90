SUBROUTINE READ_TRAFFIC_FLOW_MODELS
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~ Copyright (C) 2002-2011 The Arizona Board of Regents on Behalf of The University of Arizona.  ~~!
!~~ Developed by Yi-Chang Chiu, Ph.D. as par to the DynusT project.                               ~~!
!~~ This program is free software: you can redistribute it and/or modify it                       ~~!
!~~ under the terms of the GNU General Public License version 2 as published by                   ~~!
!~~ the Free Software Foundation.                                                                 ~~!
!~~ This program is distributed in the hope that it will be useful,                               ~~!
!~~ but WITHOUT ANY WARRANTY; without even the implied warranty of                                ~~!
!~~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                                          ~~!
!~~ See the GNU General Public License for more details.                                          ~~!
!~~ You should have received a copy of the GNU General Public License                             ~~!
!~~ along with this program (in the file COPYING).                                                ~~!
!~~ IF not, see <http://www.gnu.org/licenses/>.                                                   ~~!
!~~ Please see http://www.dynust.net for contact information and names                            ~~!
!~~ of individual contributors.                                                                   ~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
   USE DYNUST_NETWORK_MODULE
   USE DYNUST_AMS_MODULE
   REAL,ALLOCATABLE::tmpmaxflow(:)

   OPEN(file='TrafficFlowModel.dat',unit=55,status='old',iostat=error) 
   IF(error /= 0) THEN
       WRITE(911,*) 'Error when opening TrfficFlowModel.dat'
	   STOP
   ENDIF

   OPEN(file='CheckTrafficFlowModel.dat',unit=5545,status='unknown',iostat=error) 
   IF(error /= 0) THEN
       WRITE(911,*) 'Error when opening CheckTrfficFlowModel.dat'
	   STOP
   ENDIF
   WRITE(5545,'("    DENSITY   SPEED     FLOW  ")')   

   MaxSpeedLimit = 0
   ISEE = 0
   DO IH = 1, NoofArcs
      IF(m_dynust_network_arc_de(ih)%SpeedLimit.GT.MaxSpeedLimit.AND.m_dynust_network_arc_nde(ih)%link_iden /= 99) THEN
         MaxSpeedLimit = m_dynust_network_arc_de(ih)%SpeedLimit
         Isee = IH
      ENDIF
   ENDDO
   ISEE = MIN(NoofArcs, ISEE)
   WRITE(5545,*) "USE LINK # ",ISEE, "SPEED LIMIT: ",m_dynust_network_arc_de(ISEE)%SpeedLimit

   READ(55,*,iostat=error) NoOfFlowModel
   ALLOCATE(tmpMaxFlow(NoOfFlowModel))
   
   DO i = 1, NoOfFlowModel
     flowmax = 0.0
     WRITE(5545,*) "TRAFFIC FLOW MODEL #: ",I    
     IF(Tflswitch == 1) THEN ! TRAFFIC FLOW MODEL IS TAKING THE NEW FORMAT WITH ONE ADDITIONAL NUMBER IN THE FIRST LINE OF EACH MODEL
       READ(55,*) MG, VKFunc(i)%regime,VKFunc(i)%form
     ELSE
       READ(55,*) MG, VKFunc(i)%regime
       VKFunc(i)%form = 1
     ENDIF 
     IF(VKFunc(i)%form < 1) VKFunc(i)%form = 1 ! IF THE EXTRA FORM INDICATOR IS NOT FOUND, THEN ASSIGN IT TO BE FORM 1 - ALPHA ONLY
     IF(VKFunc(i)%form == 1) THEN ! ALPHA ONLY FORM
      IF(VKFunc(i)%regime == 1) THEN ! FREEWAYS
	    READ(55,*) VKFunc(i)%Kcut, VKFunc(i)%Vf2,VKFunc(i)%V02,VKFunc(i)%Kjam2,VKFunc(i)%alpha2
        DO M = 1, VKFunc(i)%kjam2,5 ! PRINT OUT DENSITY,SPEED AND THEORETICAL FLOW RATE
          IH = i
          IQ = VKFunc(i)%regime
          IForm = VKFunc(i)%form
          densityentry = float(m)
	      CALL VehSpeed_update(isee,IH,IQ,IForm,densityentry,AdjSpeed) ! 1: 1ST LINK
	      WRITE(5545,'(3F10.3)') densityentry,AdjSpeed*60.0,densityentry*AdjSpeed*60.0
	      IF(densityentry*AdjSpeed*60.0 > flowmax) flowmax = densityentry*AdjSpeed*60.0
	    ENDDO
        densityentry = float(VKFunc(i)%kjam2)	    
        CALL VehSpeed_update(isee,IH,IQ,IForm,densityentry,AdjSpeed) ! 1: 1ST LINK
        WRITE(5545,'(3F10.3)') densityentry,AdjSpeed*60.0,densityentry*AdjSpeed*60.0
	    IF(densityentry*AdjSpeed*60.0 > flowmax) flowmax = densityentry*AdjSpeed*60.0
	    
	  ELSEIF(VKFunc(i)%regime == 2) THEN ! ARTERIALS
	    READ(55,*) MT1, MT2, VKFunc(i)%V02,VKFunc(i)%Kjam2,VKFunc(i)%alpha2
        VKFunc(i)%Kcut = 0	    

        DO M = 1, VKFunc(i)%kjam2,5 ! PRINT OUT DENSITY,SPEED AND THEORETICAL FLOW RATE
          IH = i
          IQ = VKFunc(i)%regime
          IForm = VKFunc(i)%form
          densityentry = float(m)          
	      CALL VehSpeed_update(isee,IH,IQ,IForm,densityentry,AdjSpeed) ! 1: 1ST LINK
	      WRITE(5545,'(3F10.3)') densityentry,AdjSpeed*60.0,densityentry*AdjSpeed*60.0
	      IF(densityentry*AdjSpeed*60.0 > flowmax) flowmax = densityentry*AdjSpeed*60.0	      
	    ENDDO
        densityentry = float(VKFunc(i)%kjam2)	    
        CALL VehSpeed_update(isee,IH,IQ,IForm,densityentry,AdjSpeed) ! 1: 1ST LINK
        WRITE(5545,'(3F10.3)') densityentry,AdjSpeed*60.0,densityentry*AdjSpeed*60.0
	    IF(densityentry*AdjSpeed*60.0 > flowmax) flowmax = densityentry*AdjSpeed*60.0
      ELSE
        WRITE(911,*) "Error in Traffic Flow Model Type"
	    WRITE(911,*) "Currenlty, only two types are supported"  
	    WRITE(911,*) "Please see user's manual for details"
	    STOP
	  EndIF
	 ELSEIF(VKFunc(i)%form == 2) THEN ! ALPHA AND BETA FORM
      IF(VKFunc(i)%regime == 1) THEN ! FREEWAYS
	    READ(55,*) VKFunc(i)%Kcut, VKFunc(i)%Vf2,VKFunc(i)%V02,VKFunc(i)%Kjam2,VKFunc(i)%alpha2,VKFunc(i)%beta2
        DO M = 1, VKFunc(i)%kjam2,5 ! PRINT OUT DENSITY,SPEED AND THEORETICAL FLOW RATE
          IH = i
          IQ = VKFunc(i)%regime
          IForm = VKFunc(i)%form
          densityentry = float(m)          
	      CALL VehSpeed_update(isee,IH,IQ,IForm,densityentry,AdjSpeed) ! 1: 1ST LINK
	      WRITE(5545,'(3F10.3)') densityentry,AdjSpeed*60.0,densityentry*AdjSpeed*60.0
	      IF(densityentry*AdjSpeed*60.0 > flowmax) flowmax = densityentry*AdjSpeed*60.0	      
	    ENDDO
        densityentry = float(VKFunc(i)%kjam2)	    
        CALL VehSpeed_update(isee,IH,IQ,IForm,densityentry,AdjSpeed) ! 1: 1ST LINK
        WRITE(5545,'(3F10.3)') densityentry,AdjSpeed*60.0,densityentry*AdjSpeed*60.0
	    IF(densityentry*AdjSpeed*60.0 > flowmax) flowmax = densityentry*AdjSpeed*60.0
	  ELSEIF(VKFunc(i)%regime == 2) THEN ! ARTERIALS
	    READ(55,*) MT1, MT2, VKFunc(i)%V02,VKFunc(i)%Kjam2,VKFunc(i)%alpha2,VKFunc(i)%beta2
        VKFunc(i)%Kcut = 0	    
        DO M = 1, VKFunc(i)%kjam2,5 ! PRINT OUT DENSITY,SPEED AND THEORETICAL FLOW RATE
          IH = i
          IQ = VKFunc(i)%regime
          IForm = VKFunc(i)%form
          densityentry = float(m)          
	      CALL VehSpeed_update(isee,IH,IQ,IForm,densityentry,AdjSpeed) ! 1: 1ST LINK
	      WRITE(5545,'(3F10.3)') densityentry,AdjSpeed*60.0,densityentry*AdjSpeed*60.0
	      IF(densityentry*AdjSpeed*60.0 > flowmax) flowmax = densityentry*AdjSpeed*60.0	      
	    ENDDO
        densityentry = float(VKFunc(i)%kjam2)	    
        CALL VehSpeed_update(isee,IH,IQ,IForm,densityentry,AdjSpeed) ! 1: 1ST LINK
        WRITE(5545,'(3F10.3)') densityentry,AdjSpeed*60.0,densityentry*AdjSpeed*60.0
	    IF(densityentry*AdjSpeed*60.0 > flowmax) flowmax = densityentry*AdjSpeed*60.0
      ELSE
        WRITE(911,*) "Error in Traffic Flow Model Type"
	    WRITE(911,*) "Currenlty, only two types are supported"  
	    WRITE(911,*) "Please see user's manual for details"
	    STOP
	  EndIF
	 ENDIF
	 tmpMaxFlow(i) = flowmax
	 IF(flowmax > 2400) THEN
	  WRITE(511,'("The traffic flow model setting for flow model ",i4," is likely to produce excessive service flow rate at: ",f5.1)') i,flowmax
	 ENDIF
   ENDDO !DO i = 1, NoOfFlowModel
     
   VKFunc(NoOfFlowModel+1)%regime = 1
   VKFunc(NoOfFlowModel+1)%Kcut = 300
   VKFunc(NoOfFlowModel+1)%Vf2 = 100
   VKFunc(NoOfFlowModel+1)%V02 = 100
   VKFunc(NoOfFlowModel+1)%Kjam2 = 300
   VKFunc(NoOfFlowModel+1)%alpha2 = 1	      
   VKFunc(NoOfFlowModel+1)%beta2 = 1

   DO is = 1, noofarcs
      IF(m_dynust_network_arc_de(is)%FlowModelnum > NoOfFlowModel+1) THEN
        WRITE(911,*) "The traffic flow model is higher than the max setup"
        WRITE(911,*) "Link", m_dynust_network_node_nde(m_dynust_network_arc_nde(is)%iunod)%IntoOutNodeNum, "===>", m_dynust_network_node_nde(m_dynust_network_arc_nde(is)%idnod)%IntoOutNodeNum
      ENDIF
     m_dynust_network_arc_de(is)%maxden = VKFunc(m_dynust_network_arc_de(is)%FlowModelnum)%Kjam2
!    m_dynust_network_arc_de(is)%maxden = MaxDenPar
   ENDDO    		 

   DO i=1,noofarcs
      m_dynust_network_arc_de(i)%xl=m_dynust_network_arc_de(i)%nlanes*m_dynust_network_arc_nde(i)%s
      m_dynust_network_arc_de(i)%v=(m_dynust_network_arc_de(i)%SpeedLimit+m_dynust_network_arc_de(i)%Vfadjust)/60.0 !VMS
      m_dynust_network_arc_de(i)%statmpt=m_dynust_network_arc_nde(i)%s/m_dynust_network_arc_de(i)%v !AMS
      m_dynust_network_arc_nde(m_dynust_network_arc_nde(i)%ForToBackLink)%TTimeOfBackLink=m_dynust_network_arc_de(i)%statmpt
      IH = m_dynust_network_arc_de(i)%FlowModelnum
      m_dynust_network_arc_de(i)%cmax = VKFunc(IH)%Kjam2
      IF(m_dynust_network_arc_nde(i)%link_iden /= 99 ) THEN
        m_dynust_network_arc_de(i)%MaxFlowRateOrig =  tmpMaxFlow(Ih)/3600.0*m_dynust_network_arc_de(i)%nlanes ! number of vehicles per second !i70
        m_dynust_network_arc_de(i)%MaxFlowRate =      tmpMaxFlow(Ih)/3600.0*m_dynust_network_arc_de(i)%nlanes ! number of vehicles per second !i70
      ENDIF
   ENDDO
   CLOSE(5545)
   
END SUBROUTINE