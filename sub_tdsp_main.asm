; mark_description "Intel(R) Visual Fortran Compiler XE for applications running on IA-32, Version 13.1.1.171 Build 20130313";
; mark_description "-S";
	.686P
 	.387
	OPTION DOTNAME
	ASSUME	CS:FLAT,DS:FLAT,SS:FLAT
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
TXTST0:
; -- Begin  _TDSP_MAIN
; mark_begin;
IF @Version GE 800
  .MMX
ELSEIF @Version GE 612
  .MMX
  MMWORD TEXTEQU <QWORD>
ENDIF
IF @Version GE 800
  .XMM
ELSEIF @Version GE 614
  .XMM
  XMMWORD TEXTEQU <OWORD>
ENDIF
       ALIGN     16
	PUBLIC _TDSP_MAIN
_TDSP_MAIN	PROC NEAR 
; parameter 1: 8 + ebp
; parameter 2: 12 + ebp
.B1.1:                          ; Preds .B1.0
        push      ebp                                           ;1.18
        mov       ebp, esp                                      ;1.18
        and       esp, -16                                      ;1.18
        push      esi                                           ;1.18
        push      edi                                           ;1.18
        push      ebx                                           ;1.18
        sub       esp, 100                                      ;1.18
        xor       ecx, ecx                                      ;39.5
        mov       eax, DWORD PTR [12+ebp]                       ;1.18
        cvtsi2ss  xmm0, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIMPERAGG] ;39.40
        mov       edx, DWORD PTR [eax]                          ;39.5
        add       edx, -2                                       ;39.28
        cvtsi2ss  xmm1, edx                                     ;39.28
        divss     xmm1, xmm0                                    ;39.22
        cvttss2si esi, xmm1                                     ;39.22
        test      esi, esi                                      ;39.5
        mov       ebx, DWORD PTR [8+ebp]                        ;1.18
        cmovl     esi, ecx                                      ;39.5
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_KAY]   ;42.9
        inc       esi                                           ;39.5
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFARCS] ;43.4
        mov       eax, DWORD PTR [ebx]                          ;41.2
        test      eax, eax                                      ;41.22
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_AGGINDEX], esi ;39.5
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_KPATHS], edx ;42.9
        mov       DWORD PTR [84+esp], ecx                       ;43.4
        jne       .B1.16        ; Prob 50%                      ;41.22
                                ; LOE eax esi
.B1.2:                          ; Preds .B1.1
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ITI_NU] ;44.10
        mov       DWORD PTR [56+esp], edx                       ;44.10
        cmp       DWORD PTR [84+esp], 0                         ;43.4
        jle       .B1.48        ; Prob 3%                       ;43.4
                                ; LOE eax esi
.B1.3:                          ; Preds .B1.2
        mov       edi, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTIME+40] ;47.7
        mov       DWORD PTR [52+esp], edi                       ;47.7
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELTIME] ;47.7
        mov       DWORD PTR [16+esp], edi                       ;47.7
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELTIME+44] ;47.27
        mov       DWORD PTR [esp], edi                          ;47.27
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELTIME+40] ;47.27
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;45.13
        mov       edx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTIME+44] ;47.7
        mov       DWORD PTR [4+esp], edi                        ;47.27
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELTIME+32] ;47.7
        mov       DWORD PTR [32+esp], ecx                       ;45.13
        mov       DWORD PTR [8+esp], edx                        ;47.7
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;45.13
        mov       ebx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTIME] ;47.62
        mov       edx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTIME+32] ;47.62
        mov       DWORD PTR [12+esp], edi                       ;47.7
        movss     xmm2, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELTIMEWEIGHT] ;47.53
        cmp       DWORD PTR [56+esp], 0                         ;44.10
        jle       .B1.28        ; Prob 50%                      ;44.10
                                ; LOE eax edx ecx ebx esi xmm2
.B1.4:                          ; Preds .B1.3
        movss     xmm1, DWORD PTR [_2il0floatpacket.2]          ;47.84
        divss     xmm2, xmm1                                    ;47.84
        mov       DWORD PTR [48+esp], eax                       ;
        mov       eax, DWORD PTR [52+esp]                       ;
        imul      ecx, ecx, -152                                ;
        movss     xmm0, DWORD PTR [_2il0floatpacket.3]          ;47.67
        subss     xmm0, xmm2                                    ;47.67
        mov       edi, DWORD PTR [8+esp]                        ;
        imul      edi, eax                                      ;
        shl       edx, 2                                        ;
        sub       ebx, edi                                      ;
        add       DWORD PTR [32+esp], ecx                       ;
        mov       ecx, edx                                      ;
        mov       edi, eax                                      ;
        neg       ecx                                           ;
        sub       edi, edx                                      ;
        add       ecx, ebx                                      ;
        sub       esi, DWORD PTR [esp]                          ;
        add       edi, ebx                                      ;
        mov       DWORD PTR [28+esp], ecx                       ;
        add       eax, eax                                      ;
        mov       ecx, DWORD PTR [56+esp]                       ;
        sub       eax, edx                                      ;
        mov       DWORD PTR [24+esp], edi                       ;
        mov       edi, ecx                                      ;
        imul      esi, DWORD PTR [4+esp]                        ;
        add       ebx, eax                                      ;
        shr       edi, 31                                       ;
        add       edi, ecx                                      ;
        mov       ecx, DWORD PTR [12+esp]                       ;
        shl       ecx, 2                                        ;
        sar       edi, 1                                        ;
        mov       DWORD PTR [44+esp], edi                       ;
        mov       edi, DWORD PTR [16+esp]                       ;
        sub       edi, ecx                                      ;
        mov       DWORD PTR [20+esp], 0                         ;
        add       esi, edi                                      ;
        mov       DWORD PTR [36+esp], ebx                       ;
        mov       ebx, DWORD PTR [20+esp]                       ;
        mov       DWORD PTR [40+esp], esi                       ;
                                ; LOE ebx xmm0 xmm1
.B1.5:                          ; Preds .B1.15 .B1.98 .B1.4
        cmp       DWORD PTR [44+esp], 0                         ;44.10
        jbe       .B1.47        ; Prob 3%                       ;44.10
                                ; LOE ebx xmm0 xmm1
.B1.6:                          ; Preds .B1.5
        imul      eax, ebx, 152                                 ;
        xor       edx, edx                                      ;
        movaps    xmm2, xmm0                                    ;47.7
        mov       edi, DWORD PTR [32+esp]                       ;46.14
        mov       DWORD PTR [20+esp], ebx                       ;
        movsx     ecx, WORD PTR [300+eax+edi]                   ;46.14
        mov       esi, DWORD PTR [164+eax+edi]                  ;45.13
        mov       edi, DWORD PTR [40+esp]                       ;47.27
        mov       eax, DWORD PTR [24+esp]                       ;
        cvtsi2ss  xmm3, DWORD PTR [4+edi+ebx*4]                 ;47.27
        divss     xmm3, xmm1                                    ;47.53
        mov       edi, DWORD PTR [36+esp]                       ;
        lea       eax, DWORD PTR [eax+esi*4]                    ;
        mov       ebx, DWORD PTR [44+esp]                       ;
        mulss     xmm2, xmm3                                    ;47.7
        lea       edi, DWORD PTR [edi+esi*4]                    ;
        xor       esi, esi                                      ;
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1 xmm2 xmm3
.B1.7:                          ; Preds .B1.9 .B1.6
        cmp       ecx, 1                                        ;46.56
        je        .B1.100       ; Prob 16%                      ;46.56
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1 xmm2 xmm3
.B1.8:                          ; Preds .B1.7
        movss     DWORD PTR [eax+esi*2], xmm3                   ;49.7
        movss     DWORD PTR [edi+esi*2], xmm3                   ;49.7
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1 xmm2 xmm3
.B1.9:                          ; Preds .B1.100 .B1.8
        inc       edx                                           ;44.10
        add       esi, DWORD PTR [52+esp]                       ;44.10
        cmp       edx, ebx                                      ;44.10
        jb        .B1.7         ; Prob 64%                      ;44.10
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1 xmm2 xmm3
.B1.10:                         ; Preds .B1.9
        mov       ebx, DWORD PTR [20+esp]                       ;
        lea       edx, DWORD PTR [1+edx+edx]                    ;44.10
                                ; LOE edx ebx xmm0 xmm1
.B1.11:                         ; Preds .B1.10 .B1.47
        lea       eax, DWORD PTR [-1+edx]                       ;44.10
        cmp       eax, DWORD PTR [56+esp]                       ;44.10
        jae       .B1.98        ; Prob 3%                       ;44.10
                                ; LOE edx ebx xmm0 xmm1
.B1.12:                         ; Preds .B1.11
        imul      edi, ebx, 152                                 ;45.13
        imul      edx, DWORD PTR [52+esp]                       ;
        mov       ecx, DWORD PTR [40+esp]                       ;47.27
        mov       esi, DWORD PTR [32+esp]                       ;45.13
        add       edx, DWORD PTR [28+esp]                       ;
        cvtsi2ss  xmm2, DWORD PTR [4+ecx+ebx*4]                 ;47.27
        movsx     ecx, WORD PTR [300+esi+edi]                   ;46.14
        cmp       ecx, 1                                        ;46.56
        mov       eax, DWORD PTR [164+esi+edi]                  ;45.13
        je        .B1.14        ; Prob 16%                      ;46.56
                                ; LOE eax edx ebx xmm0 xmm1 xmm2
.B1.13:                         ; Preds .B1.12
        divss     xmm2, xmm1                                    ;49.7
        movss     DWORD PTR [edx+eax*4], xmm2                   ;49.7
        jmp       .B1.15        ; Prob 100%                     ;49.7
                                ; LOE ebx xmm0 xmm1
.B1.14:                         ; Preds .B1.12
        divss     xmm2, xmm1                                    ;47.53
        mulss     xmm2, xmm0                                    ;47.7
        movss     DWORD PTR [edx+eax*4], xmm2                   ;47.7
                                ; LOE ebx xmm0 xmm1
.B1.15:                         ; Preds .B1.14 .B1.13
        inc       ebx                                           ;43.4
        cmp       ebx, DWORD PTR [84+esp]                       ;43.4
        jb        .B1.5         ; Prob 82%                      ;43.4
        jmp       .B1.99        ; Prob 100%                     ;43.4
                                ; LOE ebx xmm0 xmm1
.B1.16:                         ; Preds .B1.1
        cmp       DWORD PTR [84+esp], 0                         ;55.4
        jle       .B1.25        ; Prob 0%                       ;55.4
                                ; LOE eax
.B1.17:                         ; Preds .B1.16
        mov       edi, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTIME+40] ;81.40
        mov       DWORD PTR [36+esp], edi                       ;81.40
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELTIME+44] ;58.68
        mov       ecx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTIME+44] ;81.40
        mov       DWORD PTR [4+esp], edi                        ;58.68
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELTIME+40] ;58.68
        mov       DWORD PTR [esp], ecx                          ;81.40
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;57.6
        mov       ecx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTIME+32] ;81.12
        mov       DWORD PTR [32+esp], edi                       ;58.68
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ITI_NU] ;56.10
        test      edx, edx                                      ;56.10
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELTIME+32] ;58.11
        mov       DWORD PTR [12+esp], ebx                       ;57.6
        mov       DWORD PTR [16+esp], ecx                       ;81.12
        mov       DWORD PTR [24+esp], edx                       ;56.10
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;57.6
        mov       ebx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTIME] ;81.12
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELTIME] ;58.11
        mov       DWORD PTR [8+esp], edi                        ;58.11
        movss     xmm2, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELTIMEWEIGHT] ;58.89
        jle       .B1.25        ; Prob 3%                       ;56.10
                                ; LOE eax ecx ebx esi xmm2
.B1.18:                         ; Preds .B1.17
        movss     xmm1, DWORD PTR [_2il0floatpacket.2]          ;58.118
        divss     xmm2, xmm1                                    ;58.118
        imul      edi, DWORD PTR [12+esp], -152                 ;
        movss     xmm0, DWORD PTR [_2il0floatpacket.3]          ;58.101
        subss     xmm0, xmm2                                    ;58.101
        mov       edx, DWORD PTR [36+esp]                       ;
        add       esi, edi                                      ;
        mov       edi, DWORD PTR [esp]                          ;
        imul      edi, edx                                      ;
        sub       ebx, edi                                      ;
        mov       edi, DWORD PTR [16+esp]                       ;
        shl       edi, 2                                        ;
        neg       edi                                           ;
        add       edi, edx                                      ;
        mov       edx, DWORD PTR [4+esp]                        ;
        add       ebx, edi                                      ;
        mov       edi, DWORD PTR [32+esp]                       ;
        imul      edx, edi                                      ;
        sub       ecx, edx                                      ;
        mov       edx, DWORD PTR [8+esp]                        ;
        add       ecx, edi                                      ;
        mov       DWORD PTR [28+esp], 0                         ;
        shl       edx, 2                                        ;
        mov       DWORD PTR [20+esp], esi                       ;58.101
        sub       ecx, edx                                      ;
        mov       DWORD PTR [48+esp], eax                       ;58.101
        mov       edi, DWORD PTR [28+esp]                       ;58.101
        mov       esi, DWORD PTR [24+esp]                       ;58.101
                                ; LOE ecx ebx esi edi xmm0 xmm1
.B1.19:                         ; Preds .B1.23 .B1.18
        xor       eax, eax                                      ;
        mov       DWORD PTR [esp], ebx                          ;
        xor       edx, edx                                      ;
        mov       DWORD PTR [28+esp], edi                       ;
        mov       ebx, DWORD PTR [20+esp]                       ;
                                ; LOE eax edx ecx ebx xmm0 xmm1
.B1.20:                         ; Preds .B1.19 .B1.22
        movsx     edi, WORD PTR [300+edx+ebx]                   ;57.9
        cmp       edi, 1                                        ;57.51
        mov       esi, DWORD PTR [164+edx+ebx]                  ;58.11
        cvtsi2ss  xmm2, DWORD PTR [4+ecx+eax*4]                 ;58.68
        je        .B1.101       ; Prob 16%                      ;57.51
                                ; LOE eax edx ecx ebx esi xmm0 xmm1 xmm2
.B1.21:                         ; Preds .B1.20
        divss     xmm2, xmm1                                    ;60.8
        mov       edi, DWORD PTR [esp]                          ;60.8
        movss     DWORD PTR [edi+esi*4], xmm2                   ;60.8
                                ; LOE eax edx ecx ebx edi xmm0 xmm1
.B1.22:                         ; Preds .B1.101 .B1.21
        inc       eax                                           ;55.4
        add       edx, 152                                      ;55.4
        cmp       eax, DWORD PTR [84+esp]                       ;55.4
        jb        .B1.20        ; Prob 82%                      ;55.4
                                ; LOE eax edx ecx ebx edi xmm0 xmm1
.B1.23:                         ; Preds .B1.22
        mov       ebx, edi                                      ;
        mov       edi, DWORD PTR [28+esp]                       ;
        inc       edi                                           ;56.10
        mov       esi, DWORD PTR [24+esp]                       ;
        add       ebx, DWORD PTR [36+esp]                       ;56.10
        add       ecx, DWORD PTR [32+esp]                       ;56.10
        cmp       edi, esi                                      ;56.10
        jb        .B1.19        ; Prob 82%                      ;56.10
                                ; LOE ecx ebx esi edi xmm0 xmm1
.B1.24:                         ; Preds .B1.23
        mov       eax, DWORD PTR [48+esp]                       ;
                                ; LOE eax
.B1.25:                         ; Preds .B1.24 .B1.17 .B1.16
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELPENALTY+56] ;72.10
        mov       ebx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTPENALTY+48] ;72.10
        test      ebx, ebx                                      ;72.10
        mov       DWORD PTR [12+esp], edx                       ;72.10
        mov       edx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTPENALTY+56] ;72.10
        jle       .B1.28        ; Prob 10%                      ;72.10
                                ; LOE eax edx ebx
.B1.26:                         ; Preds .B1.25
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELPENALTY+32] ;72.10
        mov       DWORD PTR [4+esp], edi                        ;72.10
        mov       edi, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTPENALTY] ;81.40
        mov       DWORD PTR [44+esp], edi                       ;81.40
        mov       edi, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTPENALTY+52] ;81.57
        mov       DWORD PTR [40+esp], edi                       ;81.57
        mov       edi, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTPENALTY+40] ;81.57
        mov       DWORD PTR [52+esp], edi                       ;81.57
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELPENALTY] ;72.10
        mov       esi, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTPENALTY+44] ;72.10
        mov       DWORD PTR [36+esp], edi                       ;72.10
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELPENALTY+52] ;72.10
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELPENALTY+44] ;72.10
        mov       DWORD PTR [56+esp], esi                       ;72.10
        mov       esi, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTPENALTY+32] ;81.40
        mov       DWORD PTR [68+esp], edi                       ;72.10
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELPENALTY+40] ;72.10
        mov       DWORD PTR [esp], ecx                          ;72.10
        mov       ecx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTPENALTY+36] ;72.10
        test      ecx, ecx                                      ;72.10
        mov       DWORD PTR [8+esp], esi                        ;81.40
        mov       esi, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTPENALTY+24] ;72.10
        mov       DWORD PTR [72+esp], edi                       ;72.10
        mov       DWORD PTR [76+esp], 0                         ;
        jle       .B1.28        ; Prob 10%                      ;72.10
                                ; LOE eax edx ecx ebx esi
.B1.27:                         ; Preds .B1.26
        test      esi, esi                                      ;72.10
        jg        .B1.40        ; Prob 50%                      ;72.10
                                ; LOE eax edx ecx ebx esi
.B1.28:                         ; Preds .B1.50 .B1.27 .B1.26 .B1.3 .B1.25
                                ;       .B1.48 .B1.49 .B1.60 .B1.63
        cmp       DWORD PTR [84+esp], 0                         ;75.8
        jle       .B1.68        ; Prob 0%                       ;75.8
                                ; LOE eax
.B1.29:                         ; Preds .B1.28
        mov       ebx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTIME] ;81.12
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ITI_NU] ;76.9
        mov       DWORD PTR [76+esp], ebx                       ;81.12
        mov       DWORD PTR [64+esp], ecx                       ;76.9
        mov       ebx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTPENALTY+44] ;81.57
        mov       ecx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTPENALTY+56] ;81.57
        mov       esi, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTIME+44] ;81.40
        mov       edi, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTIME+40] ;81.40
        mov       edx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTIME+32] ;81.12
        mov       DWORD PTR [16+esp], ebx                       ;81.57
        mov       DWORD PTR [esp], esi                          ;81.40
        mov       DWORD PTR [68+esp], edi                       ;81.40
        mov       DWORD PTR [12+esp], edx                       ;81.12
        mov       DWORD PTR [20+esp], ecx                       ;81.57
        mov       ebx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTMARGINAL] ;81.56
        mov       esi, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTPENALTY+32] ;81.40
        mov       edi, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTPENALTY] ;81.40
        mov       edx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTPENALTY+52] ;81.57
        mov       ecx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTPENALTY+40] ;81.57
        mov       DWORD PTR [60+esp], ebx                       ;81.56
        mov       DWORD PTR [4+esp], esi                        ;81.40
        mov       DWORD PTR [56+esp], edi                       ;81.40
        mov       DWORD PTR [28+esp], edx                       ;81.57
        mov       DWORD PTR [72+esp], ecx                       ;81.57
        mov       ebx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTMARGINAL+40] ;81.12
        mov       esi, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTMARGINAL+56] ;81.12
        mov       edi, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTMARGINAL+52] ;81.12
        mov       ecx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTMARGINAL+44] ;81.12
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_MAXMOVE] ;77.10
        test      edx, edx                                      ;77.10
        mov       DWORD PTR [88+esp], ebx                       ;81.12
        mov       DWORD PTR [32+esp], edx                       ;77.10
        mov       DWORD PTR [8+esp], esi                        ;81.12
        mov       DWORD PTR [36+esp], edi                       ;81.12
        mov       DWORD PTR [24+esp], ecx                       ;81.12
        mov       ebx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTMARGINAL+32] ;81.56
        jle       .B1.68        ; Prob 3%                       ;77.10
                                ; LOE eax ebx
.B1.30:                         ; Preds .B1.29
        mov       ecx, DWORD PTR [68+esp]                       ;
        xor       esi, esi                                      ;
        mov       edi, DWORD PTR [esp]                          ;
        imul      edi, ecx                                      ;
        mov       DWORD PTR [48+esp], eax                       ;
        mov       eax, DWORD PTR [76+esp]                       ;
        sub       eax, edi                                      ;
        mov       edx, DWORD PTR [12+esp]                       ;
        add       eax, ecx                                      ;
        shl       edx, 2                                        ;
        mov       edi, DWORD PTR [36+esp]                       ;
        sub       eax, edx                                      ;
        mov       edx, DWORD PTR [8+esp]                        ;
        imul      edx, edi                                      ;
        mov       DWORD PTR [40+esp], esi                       ;
        mov       esi, DWORD PTR [84+esp]                       ;
        mov       ecx, esi                                      ;
        shr       ecx, 31                                       ;
        add       ecx, esi                                      ;
        sar       ecx, 1                                        ;
        mov       DWORD PTR [80+esp], ecx                       ;
        mov       ecx, DWORD PTR [60+esp]                       ;
        sub       ecx, edx                                      ;
        mov       edx, DWORD PTR [88+esp]                       ;
        mov       DWORD PTR [76+esp], eax                       ;
        mov       eax, DWORD PTR [24+esp]                       ;
        imul      eax, edx                                      ;
        shl       ebx, 2                                        ;
        neg       eax                                           ;
        add       eax, edi                                      ;
        mov       esi, ebx                                      ;
        add       ecx, eax                                      ;
        neg       esi                                           ;
        mov       edi, edx                                      ;
        add       esi, ecx                                      ;
        mov       DWORD PTR [44+esp], esi                       ;
        sub       edi, ebx                                      ;
        mov       esi, DWORD PTR [4+esp]                        ;
        add       edi, ecx                                      ;
        shl       esi, 2                                        ;
        add       edx, edx                                      ;
        mov       eax, DWORD PTR [56+esp]                       ;
        sub       edx, ebx                                      ;
        mov       DWORD PTR [52+esp], edi                       ;
        sub       eax, esi                                      ;
        mov       edi, DWORD PTR [28+esp]                       ;
        add       ecx, edx                                      ;
        mov       esi, DWORD PTR [20+esp]                       ;
        imul      esi, edi                                      ;
        sub       esi, edi                                      ;
        mov       edi, DWORD PTR [72+esp]                       ;
        sub       eax, esi                                      ;
        mov       esi, DWORD PTR [16+esp]                       ;
        imul      esi, edi                                      ;
        sub       esi, edi                                      ;
        sub       eax, esi                                      ;
        mov       DWORD PTR [56+esp], eax                       ;
        mov       DWORD PTR [60+esp], ecx                       ;
        mov       esi, DWORD PTR [40+esp]                       ;76.9
        mov       eax, DWORD PTR [48+esp]                       ;76.9
        cmp       DWORD PTR [64+esp], 0                         ;76.9
        jle       .B1.68        ; Prob 0%                       ;76.9
                                ; LOE eax esi al ah
.B1.32:                         ; Preds .B1.30 .B1.66
        cmp       eax, 2                                        ;78.26
        je        .B1.66        ; Prob 16%                      ;78.26
                                ; LOE eax esi
.B1.33:                         ; Preds .B1.32
        mov       ebx, DWORD PTR [36+esp]                       ;
        xor       edx, edx                                      ;
        imul      ebx, esi                                      ;
        mov       edi, DWORD PTR [44+esp]                       ;
        mov       ecx, DWORD PTR [52+esp]                       ;
        mov       DWORD PTR [40+esp], esi                       ;
        mov       DWORD PTR [48+esp], eax                       ;
        add       edi, ebx                                      ;
        mov       DWORD PTR [16+esp], edi                       ;
        add       ecx, ebx                                      ;
        mov       edi, DWORD PTR [28+esp]                       ;
        imul      edi, esi                                      ;
        add       edi, DWORD PTR [56+esp]                       ;
        add       ebx, DWORD PTR [60+esp]                       ;
        mov       DWORD PTR [12+esp], ebx                       ;
        mov       DWORD PTR [8+esp], edi                        ;
        mov       DWORD PTR [4+esp], ecx                        ;
                                ; LOE edx
.B1.34:                         ; Preds .B1.39 .B1.70 .B1.33
        cmp       DWORD PTR [80+esp], 0                         ;75.8
        jbe       .B1.72        ; Prob 0%                       ;75.8
                                ; LOE edx
.B1.35:                         ; Preds .B1.34
        mov       ecx, DWORD PTR [68+esp]                       ;
        xor       edi, edi                                      ;
        mov       ebx, DWORD PTR [72+esp]                       ;
        imul      ecx, edx                                      ;
        imul      ebx, edx                                      ;
        mov       esi, DWORD PTR [4+esp]                        ;
        mov       eax, DWORD PTR [12+esp]                       ;
        mov       DWORD PTR [esp], edx                          ;
        lea       esi, DWORD PTR [esi+edx*4]                    ;
        add       ecx, DWORD PTR [76+esp]                       ;
        lea       eax, DWORD PTR [eax+edx*4]                    ;
        add       ebx, DWORD PTR [8+esp]                        ;
        mov       edx, edi                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B1.36:                         ; Preds .B1.36 .B1.35
        movss     xmm0, DWORD PTR [4+ecx+edi*8]                 ;81.40
        movss     xmm1, DWORD PTR [8+ecx+edi*8]                 ;81.40
        addss     xmm0, DWORD PTR [4+ebx+edi*8]                 ;81.12
        addss     xmm1, DWORD PTR [8+ebx+edi*8]                 ;81.12
        inc       edi                                           ;75.8
        movss     DWORD PTR [4+esi+edx*2], xmm0                 ;81.12
        movss     DWORD PTR [4+eax+edx*2], xmm1                 ;81.12
        add       edx, DWORD PTR [88+esp]                       ;75.8
        cmp       edi, DWORD PTR [80+esp]                       ;75.8
        jb        .B1.36        ; Prob 63%                      ;75.8
                                ; LOE eax edx ecx ebx esi edi
.B1.37:                         ; Preds .B1.36
        mov       edx, DWORD PTR [esp]                          ;
        lea       esi, DWORD PTR [1+edi+edi]                    ;75.8
                                ; LOE edx esi
.B1.38:                         ; Preds .B1.37 .B1.72
        lea       eax, DWORD PTR [-1+esi]                       ;75.8
        cmp       eax, DWORD PTR [84+esp]                       ;75.8
        jae       .B1.70        ; Prob 0%                       ;75.8
                                ; LOE edx esi
.B1.39:                         ; Preds .B1.38
        mov       edi, DWORD PTR [76+esp]                       ;81.12
        mov       ebx, DWORD PTR [68+esp]                       ;81.12
        imul      ebx, edx                                      ;81.12
        mov       eax, DWORD PTR [72+esp]                       ;81.12
        lea       ecx, DWORD PTR [edi+esi*4]                    ;81.12
        mov       edi, DWORD PTR [8+esp]                        ;81.12
        imul      eax, edx                                      ;81.12
        movss     xmm0, DWORD PTR [ecx+ebx]                     ;81.40
        lea       edi, DWORD PTR [edi+esi*4]                    ;81.12
        imul      esi, DWORD PTR [88+esp]                       ;81.12
        addss     xmm0, DWORD PTR [edi+eax]                     ;81.12
        add       esi, DWORD PTR [16+esp]                       ;81.12
        movss     DWORD PTR [4+esi+edx*4], xmm0                 ;81.12
        inc       edx                                           ;76.9
        cmp       edx, DWORD PTR [64+esp]                       ;76.9
        jb        .B1.34        ; Prob 82%                      ;76.9
        jmp       .B1.65        ; Prob 100%                     ;76.9
                                ; LOE edx
.B1.40:                         ; Preds .B1.27
        imul      edx, DWORD PTR [40+esp]                       ;
        mov       edi, DWORD PTR [56+esp]                       ;
        imul      edi, DWORD PTR [52+esp]                       ;
        mov       DWORD PTR [48+esp], eax                       ;
        mov       DWORD PTR [16+esp], ebx                       ;
        mov       eax, DWORD PTR [8+esp]                        ;
        mov       ebx, DWORD PTR [44+esp]                       ;
        mov       DWORD PTR [32+esp], ecx                       ;
        mov       ecx, ebx                                      ;
        shl       eax, 2                                        ;
        mov       DWORD PTR [24+esp], esi                       ;
        sub       ecx, eax                                      ;
        mov       esi, edx                                      ;
        sub       eax, edx                                      ;
        sub       esi, edi                                      ;
        add       ecx, eax                                      ;
        add       ecx, esi                                      ;
        neg       edx                                           ;
        add       ecx, edi                                      ;
        add       edx, ebx                                      ;
        mov       DWORD PTR [64+esp], ecx                       ;
        add       edx, esi                                      ;
        mov       ecx, DWORD PTR [12+esp]                       ;
        add       edi, edx                                      ;
        imul      ecx, DWORD PTR [68+esp]                       ;
        mov       ebx, DWORD PTR [esp]                          ;
        imul      ebx, DWORD PTR [72+esp]                       ;
        mov       eax, DWORD PTR [4+esp]                        ;
        shl       eax, 2                                        ;
        mov       edx, DWORD PTR [36+esp]                       ;
        sub       edx, eax                                      ;
        sub       eax, ecx                                      ;
        mov       esi, DWORD PTR [24+esp]                       ;
        add       edx, eax                                      ;
        sub       ecx, ebx                                      ;
        add       edx, ecx                                      ;
        mov       DWORD PTR [20+esp], 0                         ;
        add       edx, ebx                                      ;
        mov       DWORD PTR [56+esp], edi                       ;
        lea       edi, DWORD PTR [esi*4]                        ;
        mov       ecx, DWORD PTR [32+esp]                       ;
        mov       eax, DWORD PTR [48+esp]                       ;
        mov       ebx, DWORD PTR [20+esp]                       ;
        mov       DWORD PTR [28+esp], edi                       ;
        mov       DWORD PTR [60+esp], edx                       ;
                                ; LOE ebx esi
.B1.41:                         ; Preds .B1.45 .B1.43 .B1.40
        cmp       esi, 24                                       ;72.10
        jle       .B1.74        ; Prob 0%                       ;72.10
                                ; LOE ebx esi
.B1.42:                         ; Preds .B1.41
        mov       edx, DWORD PTR [52+esp]                       ;72.10
        mov       edi, ebx                                      ;72.10
        imul      edx, ebx                                      ;72.10
        imul      edi, DWORD PTR [72+esp]                       ;72.10
        mov       eax, DWORD PTR [40+esp]                       ;72.10
        mov       ecx, DWORD PTR [76+esp]                       ;72.10
        imul      eax, ecx                                      ;72.10
        add       edx, DWORD PTR [44+esp]                       ;72.10
        add       eax, edx                                      ;72.10
        mov       edx, DWORD PTR [68+esp]                       ;72.10
        imul      edx, ecx                                      ;72.10
        add       edi, DWORD PTR [36+esp]                       ;72.10
        push      DWORD PTR [28+esp]                            ;72.10
        add       edx, edi                                      ;72.10
        push      edx                                           ;72.10
        push      eax                                           ;72.10
        call      __intel_fast_memcpy                           ;72.10
                                ; LOE ebx esi
.B1.104:                        ; Preds .B1.42
        add       esp, 12                                       ;72.10
                                ; LOE ebx esi
.B1.43:                         ; Preds .B1.92 .B1.90 .B1.104
        inc       ebx                                           ;72.10
        cmp       ebx, DWORD PTR [32+esp]                       ;72.10
        jb        .B1.41        ; Prob 82%                      ;72.10
                                ; LOE ebx esi
.B1.44:                         ; Preds .B1.43
        mov       eax, DWORD PTR [76+esp]                       ;72.10
        inc       eax                                           ;72.10
        mov       DWORD PTR [76+esp], eax                       ;72.10
        cmp       eax, DWORD PTR [16+esp]                       ;72.10
        jae       .B1.60        ; Prob 18%                      ;72.10
                                ; LOE esi
.B1.45:                         ; Preds .B1.44
        xor       ebx, ebx                                      ;
        jmp       .B1.41        ; Prob 100%                     ;
                                ; LOE ebx esi
.B1.47:                         ; Preds .B1.5                   ; Infreq
        mov       edx, 1                                        ;
        jmp       .B1.11        ; Prob 100%                     ;
                                ; LOE edx ebx xmm0 xmm1
.B1.48:                         ; Preds .B1.2                   ; Infreq
        cmp       DWORD PTR [56+esp], 0                         ;68.10
        jle       .B1.28        ; Prob 0%                       ;68.10
                                ; LOE eax
.B1.49:                         ; Preds .B1.99 .B1.48           ; Infreq
        mov       edi, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTPENALTY+40] ;69.11
        mov       esi, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTPENALTY+52] ;69.11
        mov       DWORD PTR [40+esp], edi                       ;69.11
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE] ;69.11
        mov       edx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTPENALTY+56] ;69.11
        mov       DWORD PTR [28+esp], esi                       ;69.11
        mov       ecx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTPENALTY+44] ;69.11
        mov       ebx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTPENALTY+24] ;69.11
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+44] ;69.11
        mov       DWORD PTR [36+esp], edi                       ;69.11
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+40] ;69.11
        mov       DWORD PTR [8+esp], edx                        ;69.11
        mov       edx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTPENALTY+48] ;69.11
        test      edx, edx                                      ;69.11
        mov       DWORD PTR [12+esp], ecx                       ;69.11
        mov       DWORD PTR [44+esp], ebx                       ;69.11
        mov       DWORD PTR [4+esp], esi                        ;69.11
        mov       ecx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTPENALTY+32] ;69.11
        mov       ebx, DWORD PTR [_DYNUST_TDSP_ASTAR_MODULE_mp_TTPENALTY] ;69.11
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+32] ;69.11
        mov       DWORD PTR [32+esp], edi                       ;69.11
        jle       .B1.28        ; Prob 3%                       ;69.11
                                ; LOE eax edx ecx ebx esi
.B1.50:                         ; Preds .B1.49                  ; Infreq
        mov       DWORD PTR [48+esp], eax                       ;
        mov       eax, DWORD PTR [8+esp]                        ;
        imul      eax, DWORD PTR [28+esp]                       ;
        mov       DWORD PTR [16+esp], edx                       ;
        mov       edi, DWORD PTR [12+esp]                       ;
        mov       edx, DWORD PTR [40+esp]                       ;
        imul      edi, edx                                      ;
        mov       DWORD PTR [esp], esi                          ;
        mov       esi, ebx                                      ;
        shl       ecx, 2                                        ;
        mov       DWORD PTR [12+esp], edi                       ;
        sub       esi, ecx                                      ;
        sub       ecx, eax                                      ;
        sub       edi, edx                                      ;
        sub       eax, DWORD PTR [12+esp]                       ;
        add       esi, ecx                                      ;
        add       esi, eax                                      ;
        sub       ebx, edi                                      ;
        add       esi, edx                                      ;
        mov       edx, DWORD PTR [4+esp]                        ;
        imul      edx, DWORD PTR [32+esp]                       ;
        mov       edi, DWORD PTR [44+esp]                       ;
        mov       ecx, edi                                      ;
        mov       eax, DWORD PTR [esp]                          ;
        shl       eax, 3                                        ;
        mov       DWORD PTR [24+esp], esi                       ;
        mov       esi, DWORD PTR [36+esp]                       ;
        shr       ecx, 31                                       ;
        sub       esi, eax                                      ;
        sub       eax, edx                                      ;
        add       ecx, edi                                      ;
        add       esi, eax                                      ;
        sar       ecx, 1                                        ;
        add       esi, edx                                      ;
        mov       edx, DWORD PTR [16+esp]                       ;69.11
        test      edi, edi                                      ;69.11
        mov       eax, DWORD PTR [48+esp]                       ;69.11
        mov       DWORD PTR [20+esp], 0                         ;
        jle       .B1.28        ; Prob 50%                      ;69.11
                                ; LOE eax edx ecx ebx esi al dl ah dh
.B1.51:                         ; Preds .B1.50                  ; Infreq
        mov       DWORD PTR [64+esp], ecx                       ;
        mov       DWORD PTR [4+esp], esi                        ;
        mov       DWORD PTR [esp], ebx                          ;
        mov       DWORD PTR [16+esp], edx                       ;
        mov       DWORD PTR [48+esp], eax                       ;
        mov       ecx, DWORD PTR [20+esp]                       ;
                                ; LOE ecx
.B1.52:                         ; Preds .B1.59 .B1.62 .B1.51    ; Infreq
        mov       esi, DWORD PTR [32+esp]                       ;
        xor       edi, edi                                      ;
        mov       edx, DWORD PTR [28+esp]                       ;
        imul      esi, ecx                                      ;
        imul      edx, ecx                                      ;
        mov       ebx, DWORD PTR [36+esp]                       ;
        mov       eax, DWORD PTR [24+esp]                       ;
        mov       DWORD PTR [60+esp], edi                       ;
        mov       DWORD PTR [20+esp], ecx                       ;
        add       ebx, esi                                      ;
        mov       DWORD PTR [68+esp], ebx                       ;
        add       eax, edx                                      ;
        add       edx, DWORD PTR [esp]                          ;
        add       esi, DWORD PTR [4+esp]                        ;
        mov       ebx, edi                                      ;
        mov       DWORD PTR [52+esp], edx                       ;
        mov       DWORD PTR [12+esp], eax                       ;
                                ; LOE ebx esi edi
.B1.53:                         ; Preds .B1.58 .B1.61 .B1.52    ; Infreq
        cmp       DWORD PTR [64+esp], 0                         ;69.11
        jbe       .B1.64        ; Prob 0%                       ;69.11
                                ; LOE ebx esi edi
.B1.54:                         ; Preds .B1.53                  ; Infreq
        mov       eax, DWORD PTR [52+esp]                       ;
        xor       edx, edx                                      ;
        mov       ecx, DWORD PTR [12+esp]                       ;
        mov       DWORD PTR [60+esp], ebx                       ;
        mov       DWORD PTR [8+esp], edi                        ;
        add       eax, ebx                                      ;
        add       ecx, ebx                                      ;
                                ; LOE eax edx ecx esi
.B1.55:                         ; Preds .B1.55 .B1.54           ; Infreq
        mov       ebx, edx                                      ;69.11
        shl       ebx, 4                                        ;69.11
        mov       edi, DWORD PTR [68+esp]                       ;69.11
        mov       edi, DWORD PTR [edi+ebx]                      ;69.11
        mov       ebx, DWORD PTR [8+esi+ebx]                    ;69.11
        mov       DWORD PTR [ecx+edx*8], edi                    ;69.11
        mov       DWORD PTR [4+eax+edx*8], ebx                  ;69.11
        inc       edx                                           ;69.11
        cmp       edx, DWORD PTR [64+esp]                       ;69.11
        jb        .B1.55        ; Prob 64%                      ;69.11
                                ; LOE eax edx ecx esi
.B1.56:                         ; Preds .B1.55                  ; Infreq
        mov       ebx, DWORD PTR [60+esp]                       ;
        lea       eax, DWORD PTR [1+edx+edx]                    ;69.11
        mov       edi, DWORD PTR [8+esp]                        ;
                                ; LOE eax ebx esi edi
.B1.57:                         ; Preds .B1.56 .B1.64           ; Infreq
        lea       edx, DWORD PTR [-1+eax]                       ;69.11
        cmp       edx, DWORD PTR [44+esp]                       ;69.11
        jae       .B1.61        ; Prob 0%                       ;69.11
                                ; LOE eax ebx esi edi
.B1.58:                         ; Preds .B1.57                  ; Infreq
        mov       edx, DWORD PTR [52+esp]                       ;69.11
        inc       edi                                           ;68.10
        lea       ecx, DWORD PTR [edx+eax*4]                    ;69.11
        mov       eax, DWORD PTR [-8+esi+eax*8]                 ;69.11
        mov       DWORD PTR [-4+ecx+ebx], eax                   ;69.11
        add       ebx, DWORD PTR [40+esp]                       ;68.10
        cmp       edi, DWORD PTR [56+esp]                       ;68.10
        jb        .B1.53        ; Prob 82%                      ;68.10
                                ; LOE ebx esi edi
.B1.59:                         ; Preds .B1.58                  ; Infreq
        DB        15                                            ;
        DB        31                                            ;
        DB        68                                            ;
        DB        0                                             ;
        DB        0                                             ;
        mov       ecx, DWORD PTR [20+esp]                       ;
        inc       ecx                                           ;69.11
        cmp       ecx, DWORD PTR [16+esp]                       ;69.11
        jb        .B1.52        ; Prob 82%                      ;69.11
                                ; LOE ecx
.B1.60:                         ; Preds .B1.44 .B1.59           ; Infreq
        mov       eax, DWORD PTR [48+esp]                       ;
        jmp       .B1.28        ; Prob 100%                     ;
                                ; LOE eax
.B1.61:                         ; Preds .B1.57                  ; Infreq
        inc       edi                                           ;68.10
        add       ebx, DWORD PTR [40+esp]                       ;68.10
        cmp       edi, DWORD PTR [56+esp]                       ;68.10
        jb        .B1.53        ; Prob 82%                      ;68.10
                                ; LOE ebx esi edi
.B1.62:                         ; Preds .B1.61                  ; Infreq
        DB        15                                            ;
        DB        31                                            ;
        DB        68                                            ;
        DB        0                                             ;
        DB        0                                             ;
        mov       ecx, DWORD PTR [20+esp]                       ;
        inc       ecx                                           ;69.11
        cmp       ecx, DWORD PTR [16+esp]                       ;69.11
        jb        .B1.52        ; Prob 82%                      ;69.11
                                ; LOE ecx
.B1.63:                         ; Preds .B1.62                  ; Infreq
        mov       eax, DWORD PTR [48+esp]                       ;
        jmp       .B1.28        ; Prob 100%                     ;
                                ; LOE eax
.B1.64:                         ; Preds .B1.53                  ; Infreq
        mov       eax, 1                                        ;
        jmp       .B1.57        ; Prob 100%                     ;
                                ; LOE eax ebx esi edi
.B1.65:                         ; Preds .B1.70 .B1.39           ; Infreq
        mov       esi, DWORD PTR [40+esp]                       ;
        mov       eax, DWORD PTR [48+esp]                       ;
                                ; LOE eax esi
.B1.66:                         ; Preds .B1.65 .B1.32           ; Infreq
        inc       esi                                           ;77.10
        cmp       esi, DWORD PTR [32+esp]                       ;77.10
        jb        .B1.32        ; Prob 81%                      ;77.10
                                ; LOE eax esi
.B1.68:                         ; Preds .B1.66 .B1.30 .B1.28 .B1.29 ; Infreq
        push      DWORD PTR [12+ebp]                            ;87.9
        push      OFFSET FLAT: __NLITPACK_1.0.1                 ;87.9
        push      DWORD PTR [8+ebp]                             ;87.9
        push      OFFSET FLAT: __NLITPACK_0.0.1                 ;87.9
        call      _TOLL_LINK_PRICING                            ;87.9
                                ; LOE
.B1.69:                         ; Preds .B1.68                  ; Infreq
        add       esp, 116                                      ;89.1
        pop       ebx                                           ;89.1
        pop       edi                                           ;89.1
        pop       esi                                           ;89.1
        mov       esp, ebp                                      ;89.1
        pop       ebp                                           ;89.1
        ret                                                     ;89.1
                                ; LOE
.B1.70:                         ; Preds .B1.38                  ; Infreq
        inc       edx                                           ;76.9
        cmp       edx, DWORD PTR [64+esp]                       ;76.9
        jb        .B1.34        ; Prob 82%                      ;76.9
        jmp       .B1.65        ; Prob 100%                     ;76.9
                                ; LOE edx
.B1.72:                         ; Preds .B1.34                  ; Infreq
        mov       esi, 1                                        ;
        jmp       .B1.38        ; Prob 100%                     ;
                                ; LOE edx esi
.B1.74:                         ; Preds .B1.41                  ; Infreq
        cmp       esi, 4                                        ;72.10
        jl        .B1.94        ; Prob 10%                      ;72.10
                                ; LOE ebx esi
.B1.75:                         ; Preds .B1.74                  ; Infreq
        mov       ecx, DWORD PTR [40+esp]                       ;72.10
        imul      ecx, DWORD PTR [76+esp]                       ;72.10
        mov       edx, DWORD PTR [52+esp]                       ;72.10
        imul      edx, ebx                                      ;72.10
        mov       eax, DWORD PTR [56+esp]                       ;72.10
        add       eax, ecx                                      ;72.10
        add       eax, edx                                      ;72.10
        and       eax, 15                                       ;72.10
        je        .B1.78        ; Prob 50%                      ;72.10
                                ; LOE eax edx ecx ebx esi
.B1.76:                         ; Preds .B1.75                  ; Infreq
        test      al, 3                                         ;72.10
        jne       .B1.94        ; Prob 10%                      ;72.10
                                ; LOE eax edx ecx ebx esi
.B1.77:                         ; Preds .B1.76                  ; Infreq
        neg       eax                                           ;72.10
        add       eax, 16                                       ;72.10
        shr       eax, 2                                        ;72.10
                                ; LOE eax edx ecx ebx esi
.B1.78:                         ; Preds .B1.77 .B1.75           ; Infreq
        lea       edi, DWORD PTR [4+eax]                        ;72.10
        cmp       esi, edi                                      ;72.10
        jl        .B1.94        ; Prob 10%                      ;72.10
                                ; LOE eax edx ecx ebx esi
.B1.79:                         ; Preds .B1.78                  ; Infreq
        mov       edi, esi                                      ;72.10
        sub       edi, eax                                      ;72.10
        and       edi, 3                                        ;72.10
        neg       edi                                           ;72.10
        add       edi, esi                                      ;72.10
        mov       DWORD PTR [12+esp], edi                       ;72.10
        add       ecx, DWORD PTR [64+esp]                       ;
        mov       edi, DWORD PTR [68+esp]                       ;
        add       ecx, edx                                      ;
        imul      edi, DWORD PTR [76+esp]                       ;
        mov       edx, ebx                                      ;
        imul      edx, DWORD PTR [72+esp]                       ;
        mov       DWORD PTR [80+esp], ecx                       ;
        mov       ecx, DWORD PTR [60+esp]                       ;
        mov       DWORD PTR [esp], edx                          ;
        mov       DWORD PTR [4+esp], edi                        ;
        add       ecx, edi                                      ;
        add       ecx, edx                                      ;
        mov       DWORD PTR [8+esp], ecx                        ;
        test      eax, eax                                      ;72.10
        mov       ecx, DWORD PTR [80+esp]                       ;72.10
        mov       edx, DWORD PTR [12+esp]                       ;72.10
        jbe       .B1.83        ; Prob 10%                      ;72.10
                                ; LOE eax edx ecx ebx esi dl cl dh ch
.B1.80:                         ; Preds .B1.79                  ; Infreq
        xor       edi, edi                                      ;
        mov       DWORD PTR [24+esp], esi                       ;
        mov       DWORD PTR [20+esp], ebx                       ;
        mov       esi, edi                                      ;
        mov       edi, DWORD PTR [8+esp]                        ;
                                ; LOE eax edx ecx esi edi
.B1.81:                         ; Preds .B1.81 .B1.80           ; Infreq
        mov       ebx, DWORD PTR [edi+esi*4]                    ;72.10
        mov       DWORD PTR [ecx+esi*4], ebx                    ;72.10
        inc       esi                                           ;72.10
        cmp       esi, eax                                      ;72.10
        jb        .B1.81        ; Prob 82%                      ;72.10
                                ; LOE eax edx ecx esi edi
.B1.82:                         ; Preds .B1.81                  ; Infreq
        mov       ebx, DWORD PTR [20+esp]                       ;
        mov       esi, DWORD PTR [24+esp]                       ;
                                ; LOE eax edx ecx ebx esi
.B1.83:                         ; Preds .B1.79 .B1.82           ; Infreq
        mov       edi, DWORD PTR [4+esp]                        ;72.10
        add       edi, DWORD PTR [36+esp]                       ;72.10
        add       edi, DWORD PTR [esp]                          ;72.10
        lea       edi, DWORD PTR [edi+eax*4]                    ;72.10
        test      edi, 15                                       ;72.10
        je        .B1.87        ; Prob 60%                      ;72.10
                                ; LOE eax edx ecx ebx esi
.B1.84:                         ; Preds .B1.83                  ; Infreq
        mov       edi, DWORD PTR [8+esp]                        ;
                                ; LOE eax edx ecx ebx esi edi
.B1.85:                         ; Preds .B1.85 .B1.84           ; Infreq
        movups    xmm0, XMMWORD PTR [edi+eax*4]                 ;72.10
        movaps    XMMWORD PTR [ecx+eax*4], xmm0                 ;72.10
        add       eax, 4                                        ;72.10
        cmp       eax, edx                                      ;72.10
        jb        .B1.85        ; Prob 82%                      ;72.10
        jmp       .B1.90        ; Prob 100%                     ;72.10
                                ; LOE eax edx ecx ebx esi edi
.B1.87:                         ; Preds .B1.83                  ; Infreq
        mov       edi, DWORD PTR [8+esp]                        ;
                                ; LOE eax edx ecx ebx esi edi
.B1.88:                         ; Preds .B1.88 .B1.87           ; Infreq
        movaps    xmm0, XMMWORD PTR [edi+eax*4]                 ;72.10
        movaps    XMMWORD PTR [ecx+eax*4], xmm0                 ;72.10
        add       eax, 4                                        ;72.10
        cmp       eax, edx                                      ;72.10
        jb        .B1.88        ; Prob 82%                      ;72.10
                                ; LOE eax edx ecx ebx esi edi
.B1.90:                         ; Preds .B1.88 .B1.85 .B1.94    ; Infreq
        cmp       edx, esi                                      ;72.10
        jae       .B1.43        ; Prob 10%                      ;72.10
                                ; LOE edx ebx esi
.B1.91:                         ; Preds .B1.90                  ; Infreq
        mov       edi, DWORD PTR [40+esp]                       ;
        mov       eax, DWORD PTR [76+esp]                       ;
        imul      edi, eax                                      ;
        mov       ecx, DWORD PTR [52+esp]                       ;
        imul      ecx, ebx                                      ;
        add       edi, DWORD PTR [64+esp]                       ;
        add       edi, ecx                                      ;
        mov       ecx, DWORD PTR [68+esp]                       ;
        imul      ecx, eax                                      ;
        mov       eax, ebx                                      ;
        imul      eax, DWORD PTR [72+esp]                       ;
        add       ecx, DWORD PTR [60+esp]                       ;
        add       ecx, eax                                      ;
                                ; LOE edx ecx ebx esi edi
.B1.92:                         ; Preds .B1.92 .B1.91           ; Infreq
        mov       eax, DWORD PTR [ecx+edx*4]                    ;72.10
        mov       DWORD PTR [edi+edx*4], eax                    ;72.10
        inc       edx                                           ;72.10
        cmp       edx, esi                                      ;72.10
        jb        .B1.92        ; Prob 82%                      ;72.10
        jmp       .B1.43        ; Prob 100%                     ;72.10
                                ; LOE edx ecx ebx esi edi
.B1.94:                         ; Preds .B1.74 .B1.78 .B1.76    ; Infreq
        xor       edx, edx                                      ;72.10
        jmp       .B1.90        ; Prob 100%                     ;72.10
                                ; LOE edx ebx esi
.B1.98:                         ; Preds .B1.11                  ; Infreq
        inc       ebx                                           ;43.4
        cmp       ebx, DWORD PTR [84+esp]                       ;43.4
        jb        .B1.5         ; Prob 82%                      ;43.4
                                ; LOE ebx xmm0 xmm1
.B1.99:                         ; Preds .B1.15 .B1.98           ; Infreq
        mov       eax, DWORD PTR [48+esp]                       ;
        jmp       .B1.49        ; Prob 100%                     ;
                                ; LOE eax
.B1.100:                        ; Preds .B1.7                   ; Infreq
        movss     DWORD PTR [eax+esi*2], xmm2                   ;47.7
        movss     DWORD PTR [edi+esi*2], xmm2                   ;47.7
        jmp       .B1.9         ; Prob 100%                     ;47.7
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1 xmm2 xmm3
.B1.101:                        ; Preds .B1.20                  ; Infreq
        divss     xmm2, xmm1                                    ;58.89
        mulss     xmm2, xmm0                                    ;58.11
        mov       edi, DWORD PTR [esp]                          ;58.11
        movss     DWORD PTR [edi+esi*4], xmm2                   ;58.11
        jmp       .B1.22        ; Prob 100%                     ;58.11
        ALIGN     16
                                ; LOE eax edx ecx ebx edi xmm0 xmm1
; mark_end;
_TDSP_MAIN ENDP
_TEXT	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
__NLITPACK_1.0.1	DD	1
__NLITPACK_0.0.1	DD	2
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _TDSP_MAIN
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _TDSP_CORE
; mark_begin;
       ALIGN     16
	PUBLIC _TDSP_CORE
_TDSP_CORE	PROC NEAR 
; parameter 1: 8 + ebp
; parameter 2: 12 + ebp
.B2.1:                          ; Preds .B2.0
        push      ebp                                           ;92.12
        mov       ebp, esp                                      ;92.12
        and       esp, -16                                      ;92.12
        push      esi                                           ;92.12
        push      ebx                                           ;92.12
        sub       esp, 136                                      ;92.12
        mov       ebx, DWORD PTR [12+ebp]                       ;92.12
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DESTINATION+32] ;105.11
        neg       edx                                           ;105.11
        mov       ecx, DWORD PTR [ebx]                          ;105.18
        add       edx, ecx                                      ;105.11
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DESTINATION] ;105.11
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_IDES], 1    ;103.11
        mov       eax, DWORD PTR [eax+edx*4]                    ;105.11
        test      eax, eax                                      ;107.19
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_DESTIN], eax ;105.11
        je        .B2.15        ; Prob 50%                      ;107.19
                                ; LOE ecx ebx edi
.B2.2:                          ; Preds .B2.1
        mov       esi, DWORD PTR [8+ebp]                        ;92.12
        cmp       DWORD PTR [esi], 0                            ;108.33
        jne       .B2.15        ; Prob 50%                      ;108.33
                                ; LOE ecx ebx esi edi
.B2.3:                          ; Preds .B2.2
        mov       eax, 1374389535                               ;109.17
        imul      ecx                                           ;109.17
        mov       eax, ecx                                      ;109.17
        sar       edx, 5                                        ;109.17
        sar       eax, 31                                       ;109.17
        sub       edx, eax                                      ;109.17
        imul      eax, edx, 100                                 ;109.17
        mov       edx, ecx                                      ;109.17
        sub       edx, eax                                      ;109.17
        je        .B2.5         ; Prob 50%                      ;109.32
                                ; LOE ecx ebx esi edi
.B2.4:                          ; Preds .B2.3
        cmp       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOF_MASTER_DESTINATIONS] ;109.46
        jne       .B2.9         ; Prob 50%                      ;109.46
                                ; LOE ecx ebx esi edi
.B2.5:                          ; Preds .B2.4 .B2.3
        mov       DWORD PTR [80+esp], 0                         ;110.16
        lea       edx, DWORD PTR [80+esp]                       ;110.16
        mov       DWORD PTR [112+esp], 80                       ;110.16
        lea       eax, DWORD PTR [112+esp]                      ;110.16
        mov       DWORD PTR [116+esp], OFFSET FLAT: _TDSP_CORE$PRINTSTR1.0.2 ;110.16
        mov       DWORD PTR [120+esp], ecx                      ;110.16
        push      32                                            ;110.16
        push      OFFSET FLAT: TDSP_CORE$format_pack.0.2        ;110.16
        push      eax                                           ;110.16
        push      OFFSET FLAT: __STRLITPACK_6.0.2               ;110.16
        push      -2088435968                                   ;110.16
        push      edx                                           ;110.16
        call      _for_write_int_fmt                            ;110.16
                                ; LOE ebx esi edi
.B2.6:                          ; Preds .B2.5
        xor       ecx, ecx                                      ;111.54
        mov       edx, OFFSET FLAT: _TDSP_CORE$PRINTSTR1.0.2    ;111.54
        mov       eax, 80                                       ;111.54
        lea       esi, DWORD PTR [72+esp]                       ;111.16
        mov       DWORD PTR [72+esp], OFFSET FLAT: __STRLITPACK_3 ;111.54
        mov       DWORD PTR [80+esp], 24                        ;111.54
        mov       DWORD PTR [84+esp], ecx                       ;111.54
        mov       DWORD PTR [88+esp], edx                       ;111.54
        mov       DWORD PTR [96+esp], eax                       ;111.54
        mov       DWORD PTR [100+esp], ecx                      ;111.54
        push      ecx                                           ;111.16
        push      eax                                           ;111.16
        push      edx                                           ;111.16
        push      ecx                                           ;111.16
        push      2                                             ;111.16
        push      esi                                           ;111.16
        mov       esi, DWORD PTR [8+ebp]                        ;111.16
        call      _for_concat                                   ;111.16
                                ; LOE ebx esi edi
.B2.7:                          ; Preds .B2.6
        push      80                                            ;112.21
        push      OFFSET FLAT: __NLITPACK_6.0.2                 ;112.21
        push      OFFSET FLAT: __NLITPACK_5.0.2                 ;112.21
        push      OFFSET FLAT: __NLITPACK_4.0.2                 ;112.21
        mov       eax, OFFSET FLAT: __NLITPACK_2.0.2            ;112.21
        push      eax                                           ;112.21
        push      eax                                           ;112.21
        push      OFFSET FLAT: __NLITPACK_3.0.2                 ;112.21
        push      eax                                           ;112.21
        push      OFFSET FLAT: _TDSP_CORE$PRINTSTR1.0.2         ;112.21
        call      _PRINTSCREEN                                  ;112.21
                                ; LOE ebx esi edi
.B2.18:                         ; Preds .B2.7
        add       esp, 84                                       ;112.21
                                ; LOE ebx esi edi
.B2.8:                          ; Preds .B2.18
        cmp       DWORD PTR [esi], 0                            ;118.25
        jne       .B2.15        ; Prob 50%                      ;118.25
                                ; LOE ebx edi
.B2.9:                          ; Preds .B2.4 .B2.8
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ITERATION], 0 ;118.44
        jne       .B2.15        ; Prob 50%                      ;118.44
                                ; LOE ebx edi
.B2.10:                         ; Preds .B2.9
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_NETCHECK], 0 ;119.28
        jle       .B2.15        ; Prob 16%                      ;119.28
                                ; LOE ebx edi
.B2.11:                         ; Preds .B2.10
        movss     xmm0, DWORD PTR [_2il0floatpacket.6]          ;120.30
        comiss    xmm0, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TIME_NOW] ;120.30
        jb        .B2.15        ; Prob 50%                      ;120.30
                                ; LOE ebx edi
.B2.12:                         ; Preds .B2.11
        mov       eax, DWORD PTR [ebx]                          ;122.19
        lea       ecx, DWORD PTR [80+esp]                       ;122.19
        mov       DWORD PTR [80+esp], 0                         ;122.19
        lea       edx, DWORD PTR [32+esp]                       ;122.19
        mov       DWORD PTR [32+esp], 80                        ;122.19
        mov       DWORD PTR [36+esp], OFFSET FLAT: _TDSP_CORE$PRINTSTR1.0.2 ;122.19
        mov       DWORD PTR [40+esp], eax                       ;122.19
        push      32                                            ;122.19
        push      OFFSET FLAT: TDSP_CORE$format_pack.0.2+20     ;122.19
        push      edx                                           ;122.19
        push      OFFSET FLAT: __STRLITPACK_7.0.2               ;122.19
        push      -2088435968                                   ;122.19
        push      ecx                                           ;122.19
        call      _for_write_int_fmt                            ;122.19
                                ; LOE edi
.B2.13:                         ; Preds .B2.12
        xor       ecx, ecx                                      ;123.64
        mov       edx, OFFSET FLAT: _TDSP_CORE$PRINTSTR1.0.2    ;123.64
        mov       eax, 80                                       ;123.64
        lea       ebx, DWORD PTR [24+esp]                       ;123.19
        mov       DWORD PTR [24+esp], OFFSET FLAT: __STRLITPACK_0 ;123.64
        mov       DWORD PTR [32+esp], 31                        ;123.64
        mov       DWORD PTR [36+esp], ecx                       ;123.64
        mov       DWORD PTR [40+esp], edx                       ;123.64
        mov       DWORD PTR [48+esp], eax                       ;123.64
        mov       DWORD PTR [52+esp], ecx                       ;123.64
        push      ecx                                           ;123.19
        push      eax                                           ;123.19
        push      edx                                           ;123.19
        push      ecx                                           ;123.19
        push      2                                             ;123.19
        push      ebx                                           ;123.19
        call      _for_concat                                   ;123.19
                                ; LOE edi
.B2.14:                         ; Preds .B2.13
        push      80                                            ;124.24
        push      OFFSET FLAT: __NLITPACK_6.0.2                 ;124.24
        mov       eax, OFFSET FLAT: __NLITPACK_7.0.2            ;124.24
        mov       edx, OFFSET FLAT: __NLITPACK_2.0.2            ;124.24
        push      eax                                           ;124.24
        push      eax                                           ;124.24
        push      edx                                           ;124.24
        push      edx                                           ;124.24
        push      OFFSET FLAT: __NLITPACK_3.0.2                 ;124.24
        push      edx                                           ;124.24
        push      OFFSET FLAT: _TDSP_CORE$PRINTSTR1.0.2         ;124.24
        call      _PRINTSCREEN                                  ;124.24
                                ; LOE edi
.B2.19:                         ; Preds .B2.14
        add       esp, 84                                       ;124.24
                                ; LOE edi
.B2.15:                         ; Preds .B2.2 .B2.19 .B2.11 .B2.10 .B2.9
                                ;       .B2.8 .B2.1
        add       esp, 136                                      ;132.1
        pop       ebx                                           ;132.1
        pop       esi                                           ;132.1
        mov       esp, ebp                                      ;132.1
        pop       ebp                                           ;132.1
        ret                                                     ;132.1
        ALIGN     16
                                ; LOE
; mark_end;
_TDSP_CORE ENDP
_TEXT	ENDS
_BSS	SEGMENT  DWORD PUBLIC FLAT  'BSS'
	ALIGN 004H
_TDSP_CORE$PRINTSTR1.0.2	DB ?	; pad
	ORG $+78	; pad
	DB ?	; pad
_BSS	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
TDSP_CORE$format_pack.0.2	DB	54
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	6
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	6
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
_DATA	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
__STRLITPACK_6.0.2	DB	56
	DB	4
	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__NLITPACK_6.0.2	DD	1
__NLITPACK_5.0.2	DD	40
__NLITPACK_4.0.2	DD	200
__NLITPACK_2.0.2	DD	20
__NLITPACK_3.0.2	DD	395
__STRLITPACK_7.0.2	DB	56
	DB	4
	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__NLITPACK_7.0.2	DD	500
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _TDSP_CORE
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
_2il0floatpacket.2	DD	042c80000H
_2il0floatpacket.3	DD	03f800000H
__STRLITPACK_3	DB	78
	DB	111
	DB	119
	DB	32
	DB	67
	DB	97
	DB	108
	DB	108
	DB	105
	DB	110
	DB	103
	DB	32
	DB	83
	DB	80
	DB	44
	DB	32
	DB	68
	DB	101
	DB	115
	DB	116
	DB	32
	DB	61
	DB	32
	DB	32
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_0	DB	67
	DB	104
	DB	101
	DB	99
	DB	107
	DB	105
	DB	110
	DB	103
	DB	32
	DB	67
	DB	111
	DB	110
	DB	110
	DB	101
	DB	99
	DB	116
	DB	105
	DB	118
	DB	105
	DB	116
	DB	121
	DB	46
	DB	32
	DB	68
	DB	101
	DB	115
	DB	116
	DB	32
	DB	61
	DB	32
	DB	32
	DB	0
_2il0floatpacket.6	DD	040c00000H
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
EXTRN	_DYNUST_TDSP_ASTAR_MODULE_mp_TTMARGINAL:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_MAXMOVE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TRAVELPENALTY:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE:BYTE
EXTRN	_DYNUST_TDSP_ASTAR_MODULE_mp_TTPENALTY:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TRAVELTIMEWEIGHT:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TRAVELTIME:BYTE
EXTRN	_DYNUST_TDSP_ASTAR_MODULE_mp_TTIME:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ITI_NU:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NOOFARCS:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_KAY:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_KPATHS:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_SIMPERAGG:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_AGGINDEX:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TIME_NOW:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NETCHECK:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ITERATION:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NOOF_MASTER_DESTINATIONS:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_DESTINATION:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_DESTIN:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_IDES:BYTE
_DATA	ENDS
EXTRN	_for_concat:PROC
EXTRN	_for_write_int_fmt:PROC
EXTRN	_PRINTSCREEN:PROC
EXTRN	_TOLL_LINK_PRICING:PROC
EXTRN	__intel_fast_memcpy:PROC
EXTRN	__fltused:BYTE
	INCLUDELIB <ifconsol>
	INCLUDELIB <libifcoremt>
	INCLUDELIB <libifport>
	INCLUDELIB <libmmt>
	INCLUDELIB <LIBCMT>
	INCLUDELIB <libirc>
	INCLUDELIB <svml_dispmt>
	INCLUDELIB <OLDNAMES>
	END
