SUBROUTINE adjust_left_saturation(ilink,oppomode)       
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~ Copyright (C) 2002-2011 The Arizona Board of Regents on Behalf of The University of Arizona.  ~~!
!~~ Developed by Yi-Chang Chiu, Ph.D. as par to the DynusT project.                               ~~!
!~~ This program is free software: you can redistribute it and/or modify it                       ~~!
!~~ under the terms of the GNU General Public License version 2 as published by                   ~~!
!~~ the Free Software Foundation.                                                                 ~~!
!~~ This program is distributed in the hope that it will be useful,                               ~~!
!~~ but WITHOUT ANY WARRANTY; without even the implied warranty of                                ~~!
!~~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                                          ~~!
!~~ See the GNU General Public License for more details.                                          ~~!
!~~ You should have received a copy of the GNU General Public License                             ~~!
!~~ along with this program (in the file COPYING).                                                ~~!
!~~ IF not, see <http://www.gnu.org/licenses/>.                                                   ~~!
!~~ Please see http://www.dynust.net for contact information and names                            ~~!
!~~ of individual contributors.                                                                   ~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!

      USE DYNUST_MAIN_MODULE 
      USE DYNUST_NETWORK_MODULE
      USE RAND_GEN_INT


	  INTEGER oppomode
	  INTEGER OPPOMODE11
!> > > > > > > > > >                < < < < < < < < < < <
!>>>>>>>>>>>>>>>>>>>CODES START HERE<<<<<<<<<<<<<<<<<<<<<
!> > > > > > > > > >                < < < < < < < < < < <

! -- Apply Chang, G.-L., Chen, C.-Y. and Perez, C (2007) "Hybrid Model for Estimating Permitted Left-Turn Saturation Flow Rate", TRR, 2007.


    IF(oppomode == 0) THEN ! no opposing traffic in the same phase (protected)

        m_dynust_network_arc_de(ilink)%left_capacity=m_dynust_network_arc_de(ilink)%SatFlowRate/m_dynust_network_arc_de(ilink)%nlanes*max(1,m_dynust_network_arc_de(ilink)%bay)*6.0
         r3 = ranxy(99) 
        IF(r3 <= (m_dynust_network_arc_de(ilink)%left_capacity-ifix(m_dynust_network_arc_de(ilink)%left_capacity))) m_dynust_network_arc_de(ilink)%left_capacity = m_dynust_network_arc_de(ilink)%left_capacity + 1
     	
	  ELSEIF(oppomode == 1) THEN 
        ! S_pm = 959 + 0.498*Sana - 88(t_cr-5.0) - 264(hwy-2.0) - 64*N_o - 0.37*(F_ot/N_o) + 139*Pid - 3.77*H_l
        ! Sana: analytical estimation
        ! f_ot: Total opposing through flow rate
        ! t_cr: critical gap for left-turn vehicles (input)
        ! H_l : heavy vehicle percentage of left-turn flow
        ! hwy : average queue discharge headway (input)
        ! N_o : number of opposing lanes
        ! Pid : indication for signal progression
      
      ELSE 
	    m_dynust_network_arc_de(ilink)%left_capacity=m_dynust_network_arc_de(ilink)%SatFlowRate/m_dynust_network_arc_de(ilink)%nlanes*max(1,m_dynust_network_arc_de(ilink)%bay)*6.0 ! unit of Satflow rate is #/sec
      
	  ENDIF
END SUBROUTINE
