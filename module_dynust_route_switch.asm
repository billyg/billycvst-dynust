; mark_description "Intel(R) Visual Fortran Compiler XE for applications running on IA-32, Version 13.1.1.171 Build 20130313";
; mark_description "-S";
	.686P
 	.387
	OPTION DOTNAME
	ASSUME	CS:FLAT,DS:FLAT,SS:FLAT
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
TXTST0:
; -- Begin  _DYNUST_ROUTE_SWITCH_MODULE$
; mark_begin;
IF @Version GE 800
  .MMX
ELSEIF @Version GE 612
  .MMX
  MMWORD TEXTEQU <QWORD>
ENDIF
IF @Version GE 800
  .XMM
ELSEIF @Version GE 614
  .XMM
  XMMWORD TEXTEQU <OWORD>
ENDIF
       ALIGN     16
	PUBLIC _DYNUST_ROUTE_SWITCH_MODULE$
_DYNUST_ROUTE_SWITCH_MODULE$	PROC NEAR 
.B1.1:                          ; Preds .B1.0
        ret                                                     ;1.8
        ALIGN     16
                                ; LOE
; mark_end;
_DYNUST_ROUTE_SWITCH_MODULE$ ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_ROUTE_SWITCH_MODULE$
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_ROUTE_SWITCH_MODULE_mp_DELAY_SWITCH_AUTO
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_ROUTE_SWITCH_MODULE_mp_DELAY_SWITCH_AUTO
_DYNUST_ROUTE_SWITCH_MODULE_mp_DELAY_SWITCH_AUTO	PROC NEAR 
; parameter 1: 8 + ebp
; parameter 2: 12 + ebp
; parameter 3: 16 + ebp
; parameter 4: 20 + ebp
.B2.1:                          ; Preds .B2.0
        push      ebp                                           ;30.12
        mov       ebp, esp                                      ;30.12
        and       esp, -16                                      ;30.12
        push      esi                                           ;30.12
        push      edi                                           ;30.12
        push      ebx                                           ;30.12
        sub       esp, 132                                      ;30.12
        mov       eax, DWORD PTR [20+ebp]                       ;30.12
        lea       edx, DWORD PTR [116+esp]                      ;60.6
        mov       ebx, DWORD PTR [8+ebp]                        ;30.12
        lea       ecx, DWORD PTR [124+esp]                      ;60.6
        mov       esi, DWORD PTR [16+ebp]                       ;30.12
        mov       DWORD PTR [12+esp], eax                       ;60.6
        mov       eax, ebx                                      ;60.6
        mov       DWORD PTR [16+esp], edx                       ;60.6
        mov       DWORD PTR [20+esp], ecx                       ;60.6
        mov       ecx, esi                                      ;60.6
        mov       edx, DWORD PTR [12+ebp]                       ;60.6
        mov       DWORD PTR [24+esp], OFFSET FLAT: _DYNUST_ROUTE_SWITCH_MODULE_mp_DELAY_SWITCH_AUTO$BOTTLENECKAHEAD.0.2 ;60.6
        call      _DYNUST_ROUTE_SWITCH_MODULE_mp_GET_AUTO_REMAIN_TIME. ;60.6
                                ; LOE ebx esi
.B2.2:                          ; Preds .B2.1
        push      OFFSET FLAT: __NLITPACK_1.0.2                 ;113.17
        push      esi                                           ;113.17
        push      ebx                                           ;113.17
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_VALUE   ;113.17
                                ; LOE ebx esi f1
.B2.26:                         ; Preds .B2.2
        fstp      DWORD PTR [44+esp]                            ;113.17
        movss     xmm0, DWORD PTR [44+esp]                      ;113.17
        add       esp, 12                                       ;113.17
                                ; LOE ebx esi xmm0
.B2.3:                          ; Preds .B2.26
        cvttss2si edi, xmm0                                     ;113.8
        sub       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE+32] ;115.53
        imul      edx, edi, 84                                  ;115.53
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE] ;115.8
        mov       edi, DWORD PTR [72+eax+edx]                   ;115.11
        add       edi, edi                                      ;115.53
        neg       edi                                           ;115.53
        add       edi, DWORD PTR [40+eax+edx]                   ;115.53
        movsx     ecx, WORD PTR [2+edi]                         ;115.11
        test      ecx, ecx                                      ;115.53
        jle       .B2.8         ; Prob 16%                      ;115.53
                                ; LOE ecx ebx esi edi
.B2.4:                          ; Preds .B2.3
        mov       edx, DWORD PTR [ebx]                          ;117.61
        sub       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;117.58
        shl       edx, 5                                        ;117.58
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;117.13
        movsx     edx, WORD PTR [20+eax+edx]                    ;117.58
        xor       eax, eax                                      ;
                                ; LOE eax edx ecx esi edi
.B2.5:                          ; Preds .B2.6 .B2.4
        movsx     ebx, WORD PTR [4+edi+eax*2]                   ;117.58
        cmp       ebx, edx                                      ;117.58
        je        .B2.22        ; Prob 20%                      ;117.58
                                ; LOE eax edx ecx esi edi
.B2.6:                          ; Preds .B2.5
        inc       eax                                           ;116.9
        cmp       eax, ecx                                      ;116.9
        jb        .B2.5         ; Prob 82%                      ;116.9
                                ; LOE eax edx ecx esi edi
.B2.7:                          ; Preds .B2.6
        mov       ebx, DWORD PTR [8+ebp]                        ;
                                ; LOE ebx esi
.B2.8:                          ; Preds .B2.7 .B2.3
        test      BYTE PTR [_DYNUST_ROUTE_SWITCH_MODULE_mp_DELAY_SWITCH_AUTO$BOTTLENECKAHEAD.0.2], 1 ;123.24
        je        .B2.22        ; Prob 60%                      ;123.24
                                ; LOE ebx esi
.B2.9:                          ; Preds .B2.8
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;124.9
        lea       edi, DWORD PTR [84+esp]                       ;126.15
        neg       edx                                           ;124.9
        add       edx, DWORD PTR [ebx]                          ;124.9
        shl       edx, 5                                        ;124.9
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;124.9
        movsx     ecx, WORD PTR [20+eax+edx]                    ;124.19
        mov       edx, OFFSET FLAT: __NLITPACK_1.0.2            ;126.15
        mov       DWORD PTR [76+esp], ecx                       ;124.9
        lea       eax, DWORD PTR [76+esp]                       ;126.15
        push      edi                                           ;126.15
        push      edx                                           ;126.15
        push      eax                                           ;126.15
        push      esi                                           ;126.15
        push      edx                                           ;126.15
        mov       esi, DWORD PTR [12+ebp]                       ;126.15
        push      esi                                           ;126.15
        push      ebx                                           ;126.15
        call      _RETRIEVE_VEH_PATH_ASTAR                      ;126.15
                                ; LOE ebx
.B2.27:                         ; Preds .B2.9
        add       esp, 28                                       ;126.15
                                ; LOE ebx
.B2.10:                         ; Preds .B2.27
        mov       esi, DWORD PTR [ebx]                          ;128.11
        mov       edx, esi                                      ;
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;128.11
        mov       edi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;128.11
        shl       eax, 8                                        ;
        mov       ecx, DWORD PTR [84+esp]                       ;127.9
        mov       DWORD PTR [92+esp], edi                       ;128.11
        sub       edi, eax                                      ;
        shl       edx, 8                                        ;
        mov       DWORD PTR [100+esp], eax                      ;
        mov       DWORD PTR [108+esp], edx                      ;
        cmp       ecx, DWORD PTR [124+esp]                      ;127.23
        je        .B2.20        ; Prob 50%                      ;127.23
                                ; LOE ebx esi edi
.B2.11:                         ; Preds .B2.10
        mov       edx, DWORD PTR [20+ebp]                       ;129.11
        mov       eax, DWORD PTR [108+esp]                      ;128.11
        mov       DWORD PTR [32+esp], 0                         ;129.11
        mov       ecx, DWORD PTR [edx]                          ;129.11
        lea       edx, DWORD PTR [32+esp]                       ;129.11
        dec       ecx                                           ;129.152
        cvtsi2ss  xmm0, ecx                                     ;129.152
        mulss     xmm0, DWORD PTR [_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT] ;129.11
        mov       BYTE PTR [252+eax+edi], 2                     ;128.11
        lea       eax, DWORD PTR [80+esp]                       ;129.11
        movss     DWORD PTR [80+esp], xmm0                      ;129.11
        push      32                                            ;129.11
        push      OFFSET FLAT: DYNUST_ROUTE_SWITCH_MODULE_mp_DELAY_SWITCH_AUTO$format_pack.0.2 ;129.11
        push      eax                                           ;129.11
        push      OFFSET FLAT: __STRLITPACK_34.0.2              ;129.11
        push      -2088435968                                   ;129.11
        push      8456                                          ;129.11
        push      edx                                           ;129.11
        call      _for_write_seq_fmt                            ;129.11
                                ; LOE ebx esi edi
.B2.12:                         ; Preds .B2.11
        mov       DWORD PTR [116+esp], esi                      ;129.11
        lea       eax, DWORD PTR [116+esp]                      ;129.11
        push      eax                                           ;129.11
        push      OFFSET FLAT: __STRLITPACK_35.0.2              ;129.11
        lea       edx, DWORD PTR [68+esp]                       ;129.11
        push      edx                                           ;129.11
        call      _for_write_seq_fmt_xmit                       ;129.11
                                ; LOE ebx esi edi
.B2.13:                         ; Preds .B2.12
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;129.11
        neg       edx                                           ;129.11
        add       edx, esi                                      ;129.11
        shl       edx, 5                                        ;129.11
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;129.11
        mov       DWORD PTR [104+esp], eax                      ;129.11
        mov       DWORD PTR [108+esp], edx                      ;129.11
        movzx     ecx, WORD PTR [22+eax+edx]                    ;129.11
        lea       eax, DWORD PTR [136+esp]                      ;129.11
        mov       WORD PTR [136+esp], cx                        ;129.11
        push      eax                                           ;129.11
        push      OFFSET FLAT: __STRLITPACK_36.0.2              ;129.11
        lea       edx, DWORD PTR [80+esp]                       ;129.11
        push      edx                                           ;129.11
        call      _for_write_seq_fmt_xmit                       ;129.11
                                ; LOE ebx esi edi
.B2.14:                         ; Preds .B2.13
        mov       eax, DWORD PTR [116+esp]                      ;129.11
        mov       edx, DWORD PTR [120+esp]                      ;129.11
        movzx     ecx, WORD PTR [20+eax+edx]                    ;129.11
        lea       eax, DWORD PTR [156+esp]                      ;129.11
        mov       WORD PTR [156+esp], cx                        ;129.11
        push      eax                                           ;129.11
        push      OFFSET FLAT: __STRLITPACK_37.0.2              ;129.11
        lea       edx, DWORD PTR [92+esp]                       ;129.11
        push      edx                                           ;129.11
        call      _for_write_seq_fmt_xmit                       ;129.11
                                ; LOE ebx esi edi
.B2.15:                         ; Preds .B2.14
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE+32] ;129.11
        lea       ecx, DWORD PTR [176+esp]                      ;129.11
        neg       eax                                           ;129.11
        add       eax, esi                                      ;129.11
        shl       eax, 6                                        ;129.11
        mov       esi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE] ;129.11
        movzx     edx, WORD PTR [12+esi+eax]                    ;129.11
        mov       WORD PTR [176+esp], dx                        ;129.11
        push      ecx                                           ;129.11
        push      OFFSET FLAT: __STRLITPACK_38.0.2              ;129.11
        lea       eax, DWORD PTR [104+esp]                      ;129.11
        push      eax                                           ;129.11
        call      _for_write_seq_fmt_xmit                       ;129.11
                                ; LOE ebx edi
.B2.16:                         ; Preds .B2.15
        mov       ecx, DWORD PTR [12+ebp]                       ;129.11
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;129.11
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE] ;129.11
        mov       eax, DWORD PTR [ecx]                          ;129.11
        sub       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;129.11
        imul      ecx, eax, 152                                 ;129.11
        imul      eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE+32], 44 ;129.11
        mov       DWORD PTR [144+esp], ecx                      ;129.11
        imul      ecx, DWORD PTR [28+esi+ecx], 44               ;129.259
        add       ecx, edx                                      ;129.11
        sub       ecx, eax                                      ;129.11
        mov       DWORD PTR [148+esp], edx                      ;129.11
        mov       DWORD PTR [140+esp], eax                      ;129.11
        lea       eax, DWORD PTR [196+esp]                      ;129.11
        mov       edx, DWORD PTR [36+ecx]                       ;129.11
        mov       DWORD PTR [196+esp], edx                      ;129.11
        push      eax                                           ;129.11
        push      OFFSET FLAT: __STRLITPACK_39.0.2              ;129.11
        lea       edx, DWORD PTR [116+esp]                      ;129.11
        push      edx                                           ;129.11
        call      _for_write_seq_fmt_xmit                       ;129.11
                                ; LOE ebx esi edi
.B2.17:                         ; Preds .B2.16
        mov       eax, DWORD PTR [156+esp]                      ;130.24
        mov       ecx, DWORD PTR [160+esp]                      ;129.11
        imul      edx, DWORD PTR [24+esi+eax], 44               ;130.24
        lea       eax, DWORD PTR [216+esp]                      ;129.11
        add       ecx, edx                                      ;129.11
        sub       ecx, DWORD PTR [152+esp]                      ;129.11
        mov       esi, DWORD PTR [36+ecx]                       ;129.11
        mov       DWORD PTR [216+esp], esi                      ;129.11
        push      eax                                           ;129.11
        push      OFFSET FLAT: __STRLITPACK_40.0.2              ;129.11
        lea       edx, DWORD PTR [128+esp]                      ;129.11
        push      edx                                           ;129.11
        call      _for_write_seq_fmt_xmit                       ;129.11
                                ; LOE ebx edi
.B2.18:                         ; Preds .B2.17
        mov       eax, DWORD PTR [208+esp]                      ;130.103
        lea       edx, DWORD PTR [164+esp]                      ;129.11
        cvtsi2ss  xmm0, DWORD PTR [244+eax+edi]                 ;130.103
        divss     xmm0, DWORD PTR [_2il0floatpacket.4]          ;129.11
        movss     DWORD PTR [164+esp], xmm0                     ;129.11
        push      edx                                           ;129.11
        push      OFFSET FLAT: __STRLITPACK_41.0.2              ;129.11
        lea       ecx, DWORD PTR [140+esp]                      ;129.11
        push      ecx                                           ;129.11
        call      _for_write_seq_fmt_xmit                       ;129.11
                                ; LOE ebx edi
.B2.19:                         ; Preds .B2.18
        mov       eax, DWORD PTR [220+esp]                      ;130.139
        lea       edx, DWORD PTR [184+esp]                      ;129.11
        cvtsi2ss  xmm0, DWORD PTR [248+eax+edi]                 ;130.139
        divss     xmm0, DWORD PTR [_2il0floatpacket.4]          ;129.11
        movss     DWORD PTR [184+esp], xmm0                     ;129.11
        push      edx                                           ;129.11
        push      OFFSET FLAT: __STRLITPACK_42.0.2              ;129.11
        lea       ecx, DWORD PTR [152+esp]                      ;129.11
        push      ecx                                           ;129.11
        call      _for_write_seq_fmt_xmit                       ;129.11
                                ; LOE ebx edi
.B2.28:                         ; Preds .B2.19
        add       esp, 124                                      ;129.11
                                ; LOE ebx edi
.B2.20:                         ; Preds .B2.10 .B2.28
        mov       eax, DWORD PTR [108+esp]                      ;132.37
        mov       edx, DWORD PTR [44+eax+edi]                   ;132.37
        sub       edx, DWORD PTR [76+eax+edi]                   ;132.9
        inc       BYTE PTR [1+edx]                              ;132.64
        mov       edx, DWORD PTR [ebx]                          ;133.12
        shl       edx, 8                                        ;133.35
        add       edx, DWORD PTR [92+esp]                       ;133.35
        sub       edx, DWORD PTR [100+esp]                      ;133.35
        movsx     eax, BYTE PTR [80+edx]                        ;133.12
        test      eax, eax                                      ;133.35
        jle       .B2.22        ; Prob 16%                      ;133.35
                                ; LOE eax edx
.B2.21:                         ; Preds .B2.20
        neg       eax                                           ;133.67
        mov       BYTE PTR [80+edx], al                         ;133.40
                                ; LOE
.B2.22:                         ; Preds .B2.5 .B2.20 .B2.8 .B2.21
        add       esp, 132                                      ;139.1
        pop       ebx                                           ;139.1
        pop       edi                                           ;139.1
        pop       esi                                           ;139.1
        mov       esp, ebp                                      ;139.1
        pop       ebp                                           ;139.1
        ret                                                     ;139.1
        ALIGN     16
                                ; LOE
; mark_end;
_DYNUST_ROUTE_SWITCH_MODULE_mp_DELAY_SWITCH_AUTO ENDP
_TEXT	ENDS
_BSS	SEGMENT  DWORD PUBLIC FLAT  'BSS'
	ALIGN 004H
_DYNUST_ROUTE_SWITCH_MODULE_mp_DELAY_SWITCH_AUTO$BOTTLENECKAHEAD.0.2	DD 1 DUP (0H)	; pad
_BSS	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
DYNUST_ROUTE_SWITCH_MODULE_mp_DELAY_SWITCH_AUTO$format_pack.0.2	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	6
	DB	0
	DB	84
	DB	105
	DB	109
	DB	101
	DB	58
	DB	32
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	1
	DB	1
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	11
	DB	0
	DB	32
	DB	86
	DB	101
	DB	104
	DB	105
	DB	99
	DB	108
	DB	101
	DB	32
	DB	110
	DB	111
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	8
	DB	0
	DB	32
	DB	79
	DB	68
	DB	32
	DB	122
	DB	111
	DB	110
	DB	101
	DB	36
	DB	0
	DB	0
	DB	0
	DB	2
	DB	0
	DB	0
	DB	0
	DB	6
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	13
	DB	0
	DB	32
	DB	35
	DB	67
	DB	117
	DB	114
	DB	110
	DB	44
	DB	85
	DB	78
	DB	44
	DB	68
	DB	78
	DB	32
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	4
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	2
	DB	0
	DB	0
	DB	0
	DB	6
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	18
	DB	0
	DB	32
	DB	100
	DB	101
	DB	108
	DB	97
	DB	121
	DB	32
	DB	116
	DB	111
	DB	108
	DB	101
	DB	58
	DB	40
	DB	109
	DB	105
	DB	110
	DB	41
	DB	32
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	1
	DB	1
	DB	0
	DB	0
	DB	0
	DB	6
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	18
	DB	0
	DB	32
	DB	99
	DB	117
	DB	114
	DB	110
	DB	32
	DB	100
	DB	101
	DB	108
	DB	97
	DB	121
	DB	58
	DB	40
	DB	109
	DB	105
	DB	110
	DB	41
	DB	32
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	1
	DB	1
	DB	0
	DB	0
	DB	0
	DB	6
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
_DATA	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
__NLITPACK_1.0.2	DD	1
__STRLITPACK_34.0.2	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_35.0.2	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_36.0.2	DB	7
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_37.0.2	DB	7
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_38.0.2	DB	7
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_39.0.2	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_40.0.2	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_41.0.2	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_42.0.2	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_ROUTE_SWITCH_MODULE_mp_DELAY_SWITCH_AUTO
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_ROUTE_SWITCH_MODULE_mp_GET_AUTO_REMAIN_TIME
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_ROUTE_SWITCH_MODULE_mp_GET_AUTO_REMAIN_TIME
_DYNUST_ROUTE_SWITCH_MODULE_mp_GET_AUTO_REMAIN_TIME	PROC NEAR 
; parameter 1: eax
; parameter 2: edx
; parameter 3: ecx
; parameter 4: 20 + ebp
; parameter 5: 24 + ebp
; parameter 6: 28 + ebp
; parameter 7: 32 + ebp
.B3.1:                          ; Preds .B3.0
        mov       eax, DWORD PTR [4+esp]                        ;451.12
        mov       edx, DWORD PTR [8+esp]                        ;451.12
        mov       ecx, DWORD PTR [12+esp]                       ;451.12
	PUBLIC _DYNUST_ROUTE_SWITCH_MODULE_mp_GET_AUTO_REMAIN_TIME.
_DYNUST_ROUTE_SWITCH_MODULE_mp_GET_AUTO_REMAIN_TIME.::
        push      ebp                                           ;451.12
        mov       ebp, esp                                      ;451.12
        and       esp, -16                                      ;451.12
        push      esi                                           ;451.12
        push      edi                                           ;451.12
        push      ebx                                           ;451.12
        sub       esp, 132                                      ;451.12
        mov       ebx, ecx                                      ;451.12
        mov       esi, DWORD PTR [32+ebp]                       ;451.12
        xor       ecx, ecx                                      ;463.5
        mov       edi, eax                                      ;451.12
        mov       eax, DWORD PTR [28+ebp]                       ;451.12
        mov       DWORD PTR [esi], ecx                          ;463.5
        mov       esi, DWORD PTR [20+ebp]                       ;465.5
        mov       DWORD PTR [eax], ecx                          ;464.5
        cvtsi2ss  xmm0, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIMPERAGG] ;465.37
        mov       eax, DWORD PTR [esi]                          ;465.5
        dec       eax                                           ;465.26
        cvtsi2ss  xmm1, eax                                     ;465.26
        divss     xmm1, xmm0                                    ;465.21
        cvttss2si esi, xmm1                                     ;465.21
        mov       eax, DWORD PTR [24+ebp]                       ;466.5
        test      esi, esi                                      ;465.5
        mov       edx, DWORD PTR [edx]                          ;467.5
        mov       DWORD PTR [68+esp], edx                       ;467.5
        cmovl     esi, ecx                                      ;465.5
        mov       DWORD PTR [80+esp], edx                       ;467.5
        push      edi                                           ;468.18
        mov       DWORD PTR [eax], ecx                          ;466.5
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_P_SIZE  ;468.18
                                ; LOE eax ebx esi edi
.B3.23:                         ; Preds .B3.1
        add       esp, 4                                        ;468.18
                                ; LOE eax ebx esi edi
.B3.2:                          ; Preds .B3.23
        dec       eax                                           ;468.8
        mov       edx, DWORD PTR [ebx]                          ;468.8
        cmp       eax, edx                                      ;468.8
        jl        .B3.14        ; Prob 10%                      ;468.8
                                ; LOE eax edx esi edi
.B3.3:                          ; Preds .B3.2
        mov       ecx, DWORD PTR [edi]                          ;484.12
        lea       ebx, DWORD PTR [4+esi*4]                      ;465.5
        mov       DWORD PTR [84+esp], edx                       ;468.8
        mov       edx, ecx                                      ;
        mov       DWORD PTR [104+esp], ebx                      ;465.5
        mov       ebx, ecx                                      ;
        shl       edx, 5                                        ;
        shl       ebx, 6                                        ;
        mov       DWORD PTR [116+esp], esi                      ;
        mov       DWORD PTR [52+esp], ebx                       ;
        mov       DWORD PTR [108+esp], edx                      ;
        mov       DWORD PTR [60+esp], ecx                       ;
        mov       DWORD PTR [112+esp], eax                      ;
        mov       DWORD PTR [120+esp], edi                      ;
        mov       esi, DWORD PTR [24+ebp]                       ;
        jmp       .B3.4         ; Prob 100%                     ;
                                ; LOE esi
.B3.12:                         ; Preds .B3.11
        mov       DWORD PTR [84+esp], eax                       ;468.8
                                ; LOE esi
.B3.4:                          ; Preds .B3.12 .B3.3
        push      OFFSET FLAT: __NLITPACK_7.0.7                 ;469.22
        lea       eax, DWORD PTR [88+esp]                       ;469.22
        push      eax                                           ;469.22
        push      DWORD PTR [128+esp]                           ;469.22
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_VALUE   ;469.22
                                ; LOE esi f1
.B3.24:                         ; Preds .B3.4
        fstp      DWORD PTR [84+esp]                            ;469.22
        movss     xmm3, DWORD PTR [84+esp]                      ;469.22
                                ; LOE esi xmm3
.B3.5:                          ; Preds .B3.24
        movss     xmm0, DWORD PTR [_2il0floatpacket.13]         ;469.17
        andps     xmm0, xmm3                                    ;469.17
        pxor      xmm3, xmm0                                    ;469.17
        movss     xmm1, DWORD PTR [_2il0floatpacket.14]         ;469.17
        movaps    xmm2, xmm3                                    ;469.17
        movaps    xmm7, xmm3                                    ;469.17
        cmpltss   xmm2, xmm1                                    ;469.17
        andps     xmm1, xmm2                                    ;469.17
        mov       edx, DWORD PTR [28+ebp]                       ;470.10
        addss     xmm7, xmm1                                    ;469.17
        mov       ecx, DWORD PTR [96+esp]                       ;471.22
        subss     xmm7, xmm1                                    ;469.17
        movaps    xmm6, xmm7                                    ;469.17
        inc       ecx                                           ;471.22
        mov       DWORD PTR [104+esp], ecx                      ;471.22
        subss     xmm6, xmm3                                    ;469.17
        movss     xmm3, DWORD PTR [_2il0floatpacket.15]         ;469.17
        movaps    xmm4, xmm6                                    ;469.17
        movaps    xmm5, xmm3                                    ;469.17
        cmpnless  xmm4, xmm3                                    ;469.17
        addss     xmm5, xmm3                                    ;469.17
        cmpless   xmm6, DWORD PTR [_2il0floatpacket.16]         ;469.17
        andps     xmm4, xmm5                                    ;469.17
        andps     xmm6, xmm5                                    ;469.17
        subss     xmm7, xmm4                                    ;469.17
        addss     xmm7, xmm6                                    ;469.17
        orps      xmm7, xmm0                                    ;469.17
        cvtss2si  eax, xmm7                                     ;469.10
        mov       DWORD PTR [100+esp], eax                      ;469.10
        push      OFFSET FLAT: __NLITPACK_7.0.7                 ;471.22
        add       DWORD PTR [edx], eax                          ;470.10
        lea       ebx, DWORD PTR [108+esp]                      ;471.22
        push      ebx                                           ;471.22
        push      DWORD PTR [140+esp]                           ;471.22
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_VALUE   ;471.22
                                ; LOE esi f1
.B3.25:                         ; Preds .B3.5
        fstp      DWORD PTR [96+esp]                            ;471.22
        movss     xmm3, DWORD PTR [96+esp]                      ;471.22
                                ; LOE esi xmm3
.B3.6:                          ; Preds .B3.25
        movss     xmm0, DWORD PTR [_2il0floatpacket.13]         ;471.17
        andps     xmm0, xmm3                                    ;471.17
        pxor      xmm3, xmm0                                    ;471.17
        movss     xmm1, DWORD PTR [_2il0floatpacket.14]         ;471.17
        movaps    xmm2, xmm3                                    ;471.17
        movaps    xmm7, xmm3                                    ;471.17
        cmpltss   xmm2, xmm1                                    ;471.17
        andps     xmm1, xmm2                                    ;471.17
        addss     xmm7, xmm1                                    ;471.17
        subss     xmm7, xmm1                                    ;471.17
        movaps    xmm6, xmm7                                    ;471.17
        subss     xmm6, xmm3                                    ;471.17
        movss     xmm3, DWORD PTR [_2il0floatpacket.15]         ;471.17
        movaps    xmm5, xmm3                                    ;471.17
        movaps    xmm4, xmm6                                    ;471.17
        addss     xmm5, xmm3                                    ;471.17
        cmpnless  xmm4, xmm3                                    ;471.17
        cmpless   xmm6, DWORD PTR [_2il0floatpacket.16]         ;471.17
        andps     xmm4, xmm5                                    ;471.17
        andps     xmm6, xmm5                                    ;471.17
        subss     xmm7, xmm4                                    ;471.17
        addss     xmm7, xmm6                                    ;471.17
        orps      xmm7, xmm0                                    ;471.17
        cvtss2si  eax, xmm7                                     ;471.10
        mov       DWORD PTR [120+esp], eax                      ;471.10
        push      OFFSET FLAT: __NLITPACK_8.0.7                 ;476.17
        lea       edx, DWORD PTR [124+esp]                      ;476.17
        push      edx                                           ;476.17
        lea       ecx, DWORD PTR [120+esp]                      ;476.17
        push      ecx                                           ;476.17
        call      _DYNUST_NETWORK_MODULE_mp_GETFLINKFROMNODE    ;476.17
                                ; LOE eax esi
.B3.26:                         ; Preds .B3.6
        add       esp, 36                                       ;476.17
                                ; LOE eax esi
.B3.7:                          ; Preds .B3.26
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;477.10
        neg       edx                                           ;477.59
        add       edx, eax                                      ;477.59
        imul      ecx, edx, 900                                 ;477.59
        mov       DWORD PTR [100+esp], eax                      ;476.10
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;477.10
        cmp       BYTE PTR [12+eax+ecx], 2                      ;477.59
        jne       .B3.9         ; Prob 84%                      ;477.59
                                ; LOE esi
.B3.8:                          ; Preds .B3.7
        mov       eax, DWORD PTR [32+ebp]                       ;478.12
        mov       DWORD PTR [eax], -1                           ;478.12
                                ; LOE esi
.B3.9:                          ; Preds .B3.7 .B3.8
        lea       eax, DWORD PTR [100+esp]                      ;480.16
        push      eax                                           ;480.16
        lea       edx, DWORD PTR [84+esp]                       ;480.16
        push      edx                                           ;480.16
        call      _DYNUST_NETWORK_MODULE_mp_GETBSTMOVE          ;480.16
                                ; LOE eax esi
.B3.27:                         ; Preds .B3.9
        add       esp, 8                                        ;480.16
        mov       ebx, eax                                      ;480.16
                                ; LOE ebx esi
.B3.10:                         ; Preds .B3.27
        mov       edi, DWORD PTR [100+esp]                      ;481.10
        test      ebx, ebx                                      ;482.17
        mov       DWORD PTR [80+esp], edi                       ;481.10
        jle       .B3.15        ; Prob 16%                      ;482.17
                                ; LOE ebx esi edi
.B3.11:                         ; Preds .B3.28 .B3.10
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELTIME+44] ;487.40
        neg       edx                                           ;487.10
        mov       eax, DWORD PTR [116+esp]                      ;487.10
        lea       ecx, DWORD PTR [1+edx+eax]                    ;487.10
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELTIME+32] ;487.10
        neg       eax                                           ;487.10
        add       eax, edi                                      ;487.10
        imul      ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELTIME+40] ;487.10
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAVELTIME] ;487.10
        lea       edx, DWORD PTR [edx+eax*4]                    ;487.10
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+32] ;487.38
        cvtsi2ss  xmm0, DWORD PTR [edx+ecx]                     ;487.40
        divss     xmm0, DWORD PTR [_2il0floatpacket.12]         ;487.64
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+44] ;487.74
        neg       eax                                           ;487.10
        neg       edx                                           ;487.10
        add       edx, ebx                                      ;487.10
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;487.38
        neg       ebx                                           ;487.10
        add       ebx, edi                                      ;487.10
        imul      ebx, ebx, 152                                 ;487.10
        imul      edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+40] ;487.10
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;487.38
        sub       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;491.10
        add       eax, DWORD PTR [12+ecx+ebx]                   ;487.10
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE] ;487.38
        mov       ebx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;490.10
        add       ebx, DWORD PTR [108+esp]                      ;490.44
        lea       eax, DWORD PTR [ecx+eax*8]                    ;487.10
        addss     xmm0, DWORD PTR [eax+edx]                     ;487.38
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;490.10
        shl       edx, 5                                        ;490.44
        addss     xmm0, DWORD PTR [esi]                         ;487.10
        neg       edx                                           ;490.44
        add       ebx, edx                                      ;490.44
        imul      edx, edi, 900                                 ;491.10
        mov       edi, 3                                        ;491.10
        movss     DWORD PTR [esi], xmm0                         ;487.10
        movsx     ebx, BYTE PTR [5+ebx]                         ;490.13
        cmp       ebx, 6                                        ;491.10
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;491.10
        cmove     ebx, edi                                      ;491.10
        mov       ecx, DWORD PTR [840+eax+edx]                  ;491.40
        mov       edi, DWORD PTR [808+eax+edx]                  ;491.40
        shl       ecx, 2                                        ;491.10
        sub       ebx, DWORD PTR [852+eax+edx]                  ;491.10
        neg       ecx                                           ;491.10
        add       edi, DWORD PTR [104+esp]                      ;491.10
        imul      ebx, DWORD PTR [848+eax+edx]                  ;491.10
        add       edi, ecx                                      ;491.10
        mov       eax, DWORD PTR [84+esp]                       ;493.1
        inc       eax                                           ;468.8
        addss     xmm0, DWORD PTR [edi+ebx]                     ;491.10
        movss     DWORD PTR [esi], xmm0                         ;491.10
        cmp       eax, DWORD PTR [112+esp]                      ;468.8
        jle       .B3.12        ; Prob 82%                      ;468.8
                                ; LOE eax esi
.B3.14:                         ; Preds .B3.11 .B3.2            ; Infreq
        add       esp, 132                                      ;494.1
        pop       ebx                                           ;494.1
        pop       edi                                           ;494.1
        pop       esi                                           ;494.1
        mov       esp, ebp                                      ;494.1
        pop       ebp                                           ;494.1
        ret                                                     ;494.1
                                ; LOE
.B3.15:                         ; Preds .B3.10                  ; Infreq
        mov       DWORD PTR [esp], 0                            ;483.12
        lea       edx, DWORD PTR [esp]                          ;483.12
        mov       DWORD PTR [32+esp], 20                        ;483.12
        lea       eax, DWORD PTR [32+esp]                       ;483.12
        mov       DWORD PTR [36+esp], OFFSET FLAT: __STRLITPACK_12 ;483.12
        push      32                                            ;483.12
        push      eax                                           ;483.12
        push      OFFSET FLAT: __STRLITPACK_57.0.7              ;483.12
        push      -2088435968                                   ;483.12
        push      911                                           ;483.12
        push      edx                                           ;483.12
        call      _for_write_seq_lis                            ;483.12
                                ; LOE ebx esi edi
.B3.16:                         ; Preds .B3.15                  ; Infreq
        mov       DWORD PTR [24+esp], 0                         ;484.12
        lea       eax, DWORD PTR [64+esp]                       ;484.12
        mov       DWORD PTR [64+esp], 14                        ;484.12
        mov       DWORD PTR [68+esp], OFFSET FLAT: __STRLITPACK_10 ;484.12
        push      32                                            ;484.12
        push      eax                                           ;484.12
        push      OFFSET FLAT: __STRLITPACK_58.0.7              ;484.12
        push      -2088435968                                   ;484.12
        push      911                                           ;484.12
        lea       edx, DWORD PTR [44+esp]                       ;484.12
        push      edx                                           ;484.12
        call      _for_write_seq_lis                            ;484.12
                                ; LOE ebx esi edi
.B3.17:                         ; Preds .B3.16                  ; Infreq
        mov       eax, DWORD PTR [108+esp]                      ;484.12
        lea       edx, DWORD PTR [96+esp]                       ;484.12
        mov       DWORD PTR [96+esp], eax                       ;484.12
        push      edx                                           ;484.12
        push      OFFSET FLAT: __STRLITPACK_59.0.7              ;484.12
        lea       ecx, DWORD PTR [56+esp]                       ;484.12
        push      ecx                                           ;484.12
        call      _for_write_seq_lis_xmit                       ;484.12
                                ; LOE ebx esi edi
.B3.18:                         ; Preds .B3.17                  ; Infreq
        mov       eax, DWORD PTR [128+esp]                      ;484.12
        lea       edx, DWORD PTR [116+esp]                      ;484.12
        mov       DWORD PTR [116+esp], eax                      ;484.12
        push      edx                                           ;484.12
        push      OFFSET FLAT: __STRLITPACK_60.0.7              ;484.12
        lea       ecx, DWORD PTR [68+esp]                       ;484.12
        push      ecx                                           ;484.12
        call      _for_write_seq_lis_xmit                       ;484.12
                                ; LOE ebx esi edi
.B3.19:                         ; Preds .B3.18                  ; Infreq
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE+32] ;484.12
        shl       edx, 6                                        ;484.12
        neg       edx                                           ;484.12
        add       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE] ;484.12
        mov       eax, DWORD PTR [124+esp]                      ;484.12
        movzx     ecx, WORD PTR [12+eax+edx]                    ;484.12
        lea       eax, DWORD PTR [136+esp]                      ;484.12
        mov       WORD PTR [136+esp], cx                        ;484.12
        push      eax                                           ;484.12
        push      OFFSET FLAT: __STRLITPACK_61.0.7              ;484.12
        lea       edx, DWORD PTR [80+esp]                       ;484.12
        push      edx                                           ;484.12
        call      _for_write_seq_lis_xmit                       ;484.12
                                ; LOE ebx esi edi
.B3.20:                         ; Preds .B3.19                  ; Infreq
        push      32                                            ;485.12
        xor       eax, eax                                      ;485.12
        push      eax                                           ;485.12
        push      eax                                           ;485.12
        push      -2088435968                                   ;485.12
        push      eax                                           ;485.12
        push      OFFSET FLAT: __STRLITPACK_62                  ;485.12
        call      _for_stop_core                                ;485.12
                                ; LOE ebx esi edi
.B3.28:                         ; Preds .B3.20                  ; Infreq
        add       esp, 108                                      ;485.12
        jmp       .B3.11        ; Prob 100%                     ;485.12
        ALIGN     16
                                ; LOE ebx esi edi
; mark_end;
_DYNUST_ROUTE_SWITCH_MODULE_mp_GET_AUTO_REMAIN_TIME ENDP
_TEXT	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DB 3 DUP ( 0H)	; pad
__NLITPACK_7.0.7	DD	1
__NLITPACK_8.0.7	DD	2
__STRLITPACK_57.0.7	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_58.0.7	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_59.0.7	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_60.0.7	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_61.0.7	DB	7
	DB	1
	DB	1
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_ROUTE_SWITCH_MODULE_mp_GET_AUTO_REMAIN_TIME
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_ROUTE_SWITCH_MODULE_mp_DELAY_SWITCH_MULT
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_ROUTE_SWITCH_MODULE_mp_DELAY_SWITCH_MULT
_DYNUST_ROUTE_SWITCH_MODULE_mp_DELAY_SWITCH_MULT	PROC NEAR 
; parameter 1: 8 + ebp
; parameter 2: 12 + ebp
; parameter 3: 16 + ebp
; parameter 4: 20 + ebp
; parameter 5: 24 + ebp
; parameter 6: 28 + ebp
; parameter 7: 32 + ebp
; parameter 8: 36 + ebp
; parameter 9: 40 + ebp
.B4.1:                          ; Preds .B4.0
        push      ebp                                           ;143.12
        mov       ebp, esp                                      ;143.12
        and       esp, -16                                      ;143.12
        push      esi                                           ;143.12
        push      edi                                           ;143.12
        push      ebx                                           ;143.12
        sub       esp, 84                                       ;143.12
        mov       eax, DWORD PTR [12+ebp]                       ;143.12
        mov       ecx, DWORD PTR [8+ebp]                        ;143.12
        mov       ebx, DWORD PTR [24+ebp]                       ;143.12
        mov       edx, DWORD PTR [eax]                          ;161.5
        mov       eax, DWORD PTR [ecx]                          ;163.5
        imul      ecx, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE+32], 176 ;163.5
        mov       DWORD PTR [esp], edx                          ;161.5
        neg       ecx                                           ;163.5
        imul      edx, DWORD PTR [ebx], 176                     ;163.5
        mov       esi, DWORD PTR [32+ebp]                       ;143.12
        add       edx, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE] ;163.5
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE+32] ;163.5
        mov       esi, DWORD PTR [esi]                          ;162.15
        neg       edi                                           ;163.5
        sub       esi, DWORD PTR [52+ecx+edx]                   ;163.5
        imul      esi, DWORD PTR [48+ecx+edx]                   ;163.5
        mov       edx, DWORD PTR [20+ecx+edx]                   ;162.15
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE] ;163.5
        mov       ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_MASTERDEST+32] ;163.5
        add       edi, DWORD PTR [edx+esi]                      ;163.5
        neg       ebx                                           ;163.5
        imul      esi, edi, 84                                  ;163.5
        movsx     edi, WORD PTR [36+ecx+esi]                    ;163.5
        add       ebx, edi                                      ;163.5
        sub       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;163.5
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_MASTERDEST] ;163.5
        shl       eax, 5                                        ;163.5
        mov       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;163.5
        movsx     edx, WORD PTR [edx+ebx*2]                     ;163.5
        test      BYTE PTR [_DYNUST_MAIN_MODULE_mp_CAPKNOW], 1  ;166.8
        mov       WORD PTR [20+ecx+eax], dx                     ;163.5
        je        .B4.3         ; Prob 60%                      ;166.8
                                ; LOE edx
.B4.2:                          ; Preds .B4.1
        mov       eax, DWORD PTR [40+ebp]                       ;166.20
        test      BYTE PTR [eax], 1                             ;166.20
        jne       .B4.13        ; Prob 15%                      ;166.20
        jmp       .B4.12        ; Prob 100%                     ;166.20
                                ; LOE edx
.B4.3:                          ; Preds .B4.1
        mov       eax, DWORD PTR [40+ebp]                       ;190.34
        test      BYTE PTR [eax], 1                             ;190.34
        jne       .B4.12        ; Prob 71%                      ;190.34
                                ; LOE edx
.B4.4:                          ; Preds .B4.3
        mov       DWORD PTR [60+esp], edx                       ;164.5
        lea       edx, DWORD PTR [68+esp]                       ;192.12
        mov       eax, DWORD PTR [esp]                          ;161.5
        lea       esi, DWORD PTR [64+esp]                       ;192.12
        mov       DWORD PTR [64+esp], eax                       ;161.5
        mov       ebx, OFFSET FLAT: __NLITPACK_2.0.3            ;192.12
        lea       ecx, DWORD PTR [60+esp]                       ;192.12
        push      edx                                           ;192.12
        push      ebx                                           ;192.12
        push      ecx                                           ;192.12
        push      DWORD PTR [16+ebp]                            ;192.12
        push      ebx                                           ;192.12
        push      esi                                           ;192.12
        push      DWORD PTR [8+ebp]                             ;192.12
        call      _RETRIEVE_VEH_PATH_ASTAR                      ;192.12
                                ; LOE
.B4.5:                          ; Preds .B4.4
        push      DWORD PTR [8+ebp]                             ;193.15
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_P_SIZE  ;193.15
                                ; LOE eax
.B4.6:                          ; Preds .B4.5
        mov       edx, DWORD PTR [16+ebp]                       ;193.7
        dec       eax                                           ;193.7
        push      DWORD PTR [8+ebp]                             ;194.33
        mov       DWORD PTR [edx], eax                          ;193.7
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_P_SIZE  ;194.33
                                ; LOE eax
.B4.7:                          ; Preds .B4.6
        dec       eax                                           ;194.18
        mov       DWORD PTR [36+esp], eax                       ;194.18
        lea       eax, DWORD PTR [36+esp]                       ;194.18
        push      OFFSET FLAT: __NLITPACK_2.0.3                 ;194.18
        push      eax                                           ;194.18
        push      DWORD PTR [8+ebp]                             ;194.18
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_VALUE   ;194.18
                                ; LOE f1
.B4.30:                         ; Preds .B4.7
        fstp      DWORD PTR [56+esp]                            ;194.18
        movss     xmm0, DWORD PTR [56+esp]                      ;194.18
                                ; LOE xmm0
.B4.8:                          ; Preds .B4.30
        cvttss2si eax, xmm0                                     ;194.7
        mov       DWORD PTR [52+esp], eax                       ;194.7
        push      DWORD PTR [8+ebp]                             ;195.36
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_P_SIZE  ;195.36
                                ; LOE eax
.B4.9:                          ; Preds .B4.8
        add       eax, -2                                       ;195.21
        mov       DWORD PTR [68+esp], eax                       ;195.21
        lea       eax, DWORD PTR [68+esp]                       ;195.21
        push      OFFSET FLAT: __NLITPACK_2.0.3                 ;195.21
        push      eax                                           ;195.21
        push      DWORD PTR [8+ebp]                             ;195.21
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_VALUE   ;195.21
                                ; LOE f1
.B4.32:                         ; Preds .B4.9
        fstp      DWORD PTR [72+esp]                            ;195.21
        movss     xmm0, DWORD PTR [72+esp]                      ;195.21
                                ; LOE xmm0
.B4.10:                         ; Preds .B4.32
        cvttss2si eax, xmm0                                     ;195.7
        mov       DWORD PTR [72+esp], eax                       ;195.7
        lea       ecx, DWORD PTR [72+esp]                       ;196.14
        lea       edx, DWORD PTR [68+esp]                       ;196.14
        push      OFFSET FLAT: __NLITPACK_3.0.3                 ;196.14
        push      edx                                           ;196.14
        push      ecx                                           ;196.14
        call      _DYNUST_NETWORK_MODULE_mp_GETFLINKFROMNODE    ;196.14
                                ; LOE eax
.B4.11:                         ; Preds .B4.10
        mov       esi, DWORD PTR [24+ebp]                       ;197.17
        mov       edi, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE] ;197.7
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_MASTERDEST+32] ;198.7
        mov       ecx, DWORD PTR [esi]                          ;197.17
        neg       edx                                           ;198.7
        sub       ecx, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE+32] ;198.7
        imul      ebx, ecx, 176                                 ;198.7
        mov       ecx, DWORD PTR [36+ebp]                       ;197.17
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE+32] ;198.7
        neg       esi                                           ;198.7
        mov       ecx, DWORD PTR [ecx]                          ;197.17
        sub       ecx, DWORD PTR [52+edi+ebx]                   ;198.7
        imul      ecx, DWORD PTR [48+edi+ebx]                   ;198.7
        mov       ebx, DWORD PTR [20+edi+ebx]                   ;197.17
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE] ;198.7
        mov       DWORD PTR [140+esp], eax                      ;196.7
        add       esi, DWORD PTR [ebx+ecx]                      ;198.7
        imul      ecx, esi, 84                                  ;198.7
        movsx     ebx, WORD PTR [36+edi+ecx]                    ;198.7
        add       edx, ebx                                      ;198.7
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_MASTERDEST] ;198.7
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;198.38
        neg       eax                                           ;198.7
        movsx     ecx, WORD PTR [esi+edx*2]                     ;198.7
        mov       edx, DWORD PTR [8+ebp]                        ;198.7
        mov       edi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;198.7
        mov       DWORD PTR [136+esp], ecx                      ;199.7
        add       eax, DWORD PTR [edx]                          ;198.7
        shl       eax, 5                                        ;198.7
        mov       WORD PTR [20+edi+eax], cx                     ;198.7
        lea       eax, DWORD PTR [144+esp]                      ;201.12
        push      eax                                           ;201.12
        mov       ecx, OFFSET FLAT: __NLITPACK_2.0.3            ;201.12
        push      ecx                                           ;201.12
        lea       eax, DWORD PTR [144+esp]                      ;201.12
        push      eax                                           ;201.12
        push      DWORD PTR [16+ebp]                            ;201.12
        push      ecx                                           ;201.12
        lea       ebx, DWORD PTR [160+esp]                      ;201.12
        push      ebx                                           ;201.12
        push      edx                                           ;201.12
        call      _RETRIEVE_VEH_PATH_ASTAR                      ;201.12
                                ; LOE
.B4.34:                         ; Preds .B4.11
        add       esp, 104                                      ;201.12
                                ; LOE
.B4.12:                         ; Preds .B4.2 .B4.34 .B4.3
        add       esp, 84                                       ;204.1
        pop       ebx                                           ;204.1
        pop       edi                                           ;204.1
        pop       esi                                           ;204.1
        mov       esp, ebp                                      ;204.1
        pop       ebp                                           ;204.1
        ret                                                     ;204.1
                                ; LOE
.B4.13:                         ; Preds .B4.2                   ; Infreq
        mov       DWORD PTR [60+esp], edx                       ;164.5
        lea       edx, DWORD PTR [68+esp]                       ;168.12
        mov       eax, DWORD PTR [esp]                          ;161.5
        lea       esi, DWORD PTR [64+esp]                       ;168.12
        mov       DWORD PTR [64+esp], eax                       ;161.5
        mov       ebx, OFFSET FLAT: __NLITPACK_2.0.3            ;168.12
        lea       ecx, DWORD PTR [60+esp]                       ;168.12
        push      edx                                           ;168.12
        push      ebx                                           ;168.12
        push      ecx                                           ;168.12
        push      DWORD PTR [16+ebp]                            ;168.12
        push      ebx                                           ;168.12
        push      esi                                           ;168.12
        push      DWORD PTR [8+ebp]                             ;168.12
        call      _RETRIEVE_VEH_PATH_ASTAR                      ;168.12
                                ; LOE
.B4.14:                         ; Preds .B4.13                  ; Infreq
        mov       eax, DWORD PTR [16+ebp]                       ;170.7
        mov       ecx, DWORD PTR [32+ebp]                       ;171.7
        mov       edx, DWORD PTR [eax]                          ;170.7
        mov       ebx, DWORD PTR [ecx]                          ;171.7
        mov       DWORD PTR [76+esp], edx                       ;170.7
        mov       DWORD PTR [28+esp], ebx                       ;171.7
        push      DWORD PTR [8+ebp]                             ;172.33
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_P_SIZE  ;172.33
                                ; LOE eax
.B4.15:                         ; Preds .B4.14                  ; Infreq
        dec       eax                                           ;172.18
        mov       DWORD PTR [76+esp], eax                       ;172.18
        lea       eax, DWORD PTR [76+esp]                       ;172.18
        push      OFFSET FLAT: __NLITPACK_2.0.3                 ;172.18
        push      eax                                           ;172.18
        push      DWORD PTR [8+ebp]                             ;172.18
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_VALUE   ;172.18
                                ; LOE f1
.B4.36:                         ; Preds .B4.15                  ; Infreq
        fstp      DWORD PTR [52+esp]                            ;172.18
        movss     xmm0, DWORD PTR [52+esp]                      ;172.18
        add       esp, 44                                       ;172.18
                                ; LOE xmm0
.B4.16:                         ; Preds .B4.36                  ; Infreq
        mov       esi, DWORD PTR [28+ebp]                       ;143.12
        mov       edi, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE+32] ;173.22
        mov       DWORD PTR [52+esp], edi                       ;173.22
        neg       edi                                           ;173.19
        add       edi, DWORD PTR [esi]                          ;173.19
        imul      esi, edi, 176                                 ;173.19
        cvttss2si ecx, xmm0                                     ;172.7
        mov       ebx, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE] ;173.7
        mov       eax, DWORD PTR [32+ebp]                       ;173.22
        mov       DWORD PTR [4+esp], ecx                        ;172.7
        mov       edi, DWORD PTR [52+ebx+esi]                   ;173.22
        neg       edi                                           ;173.19
        mov       eax, DWORD PTR [eax]                          ;173.22
        add       edi, eax                                      ;173.19
        imul      edi, DWORD PTR [48+ebx+esi]                   ;173.19
        mov       esi, DWORD PTR [20+ebx+esi]                   ;173.22
        cmp       ecx, DWORD PTR [esi+edi]                      ;173.19
        je        .B4.18        ; Prob 50%                      ;173.19
                                ; LOE eax ebx
.B4.17:                         ; Preds .B4.16                  ; Infreq
        mov       DWORD PTR [esp], 0                            ;174.10
        lea       ecx, DWORD PTR [esp]                          ;174.10
        mov       DWORD PTR [32+esp], 31                        ;174.10
        lea       eax, DWORD PTR [32+esp]                       ;174.10
        mov       DWORD PTR [36+esp], OFFSET FLAT: __STRLITPACK_22 ;174.10
        push      32                                            ;174.10
        push      eax                                           ;174.10
        push      OFFSET FLAT: __STRLITPACK_43.0.3              ;174.10
        push      -2088435968                                   ;174.10
        push      911                                           ;174.10
        push      ecx                                           ;174.10
        call      _for_write_seq_lis                            ;174.10
                                ; LOE ebx
.B4.37:                         ; Preds .B4.17                  ; Infreq
        add       esp, 24                                       ;174.10
        jmp       .B4.25        ; Prob 100%                     ;174.10
                                ; LOE ebx
.B4.18:                         ; Preds .B4.16                  ; Infreq
        mov       ecx, DWORD PTR [36+ebp]                       ;176.10
        mov       esi, DWORD PTR [ecx]                          ;176.10
        cmp       esi, eax                                      ;176.10
        jl        .B4.23        ; Prob 10%                      ;176.10
                                ; LOE eax ebx esi
.B4.19:                         ; Preds .B4.18                  ; Infreq
        mov       DWORD PTR [4+esp], esi                        ;180.17
        mov       edi, DWORD PTR [48+esp]                       ;180.17
        mov       edx, ebx                                      ;180.17
        mov       esi, DWORD PTR [esp]                          ;180.17
        mov       ebx, eax                                      ;180.17
                                ; LOE edx ebx esi edi
.B4.20:                         ; Preds .B4.40 .B4.19           ; Infreq
        mov       ecx, DWORD PTR [24+ebp]                       ;179.27
        inc       esi                                           ;178.12
        inc       edi                                           ;177.12
        mov       WORD PTR [42+esp], di                         ;180.17
        mov       ecx, DWORD PTR [ecx]                          ;179.27
        sub       ecx, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE+32] ;179.12
        imul      ecx, ecx, 176                                 ;179.12
        mov       eax, DWORD PTR [52+edx+ecx]                   ;179.27
        neg       eax                                           ;179.12
        add       eax, esi                                      ;179.12
        imul      eax, DWORD PTR [48+edx+ecx]                   ;179.12
        mov       edx, DWORD PTR [20+edx+ecx]                   ;179.27
        cvtsi2ss  xmm0, DWORD PTR [edx+eax]                     ;179.12
        movss     DWORD PTR [56+esp], xmm0                      ;179.12
        lea       edx, DWORD PTR [56+esp]                       ;180.17
        push      edx                                           ;180.17
        push      OFFSET FLAT: __NLITPACK_2.0.3                 ;180.17
        lea       ecx, DWORD PTR [50+esp]                       ;180.17
        push      ecx                                           ;180.17
        push      DWORD PTR [8+ebp]                             ;180.17
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_INSERT  ;180.17
                                ; LOE ebx esi edi
.B4.38:                         ; Preds .B4.20                  ; Infreq
        add       esp, 16                                       ;180.17
                                ; LOE ebx esi edi
.B4.21:                         ; Preds .B4.38                  ; Infreq
        inc       ebx                                           ;181.10
        cmp       ebx, DWORD PTR [4+esp]                        ;181.10
        jg        .B4.22        ; Prob 18%                      ;181.10
                                ; LOE ebx esi edi
.B4.40:                         ; Preds .B4.21                  ; Infreq
        mov       edx, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE] ;197.7
        jmp       .B4.20        ; Prob 100%                     ;197.7
                                ; LOE edx ebx esi edi
.B4.22:                         ; Preds .B4.21                  ; Infreq
        mov       DWORD PTR [48+esp], edi                       ;
                                ; LOE
.B4.23:                         ; Preds .B4.18 .B4.22           ; Infreq
        mov       ebx, DWORD PTR [8+ebp]                        ;183.10
        mov       edi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;183.10
        neg       edi                                           ;183.10
        add       edi, DWORD PTR [ebx]                          ;183.10
        shl       edi, 5                                        ;183.10
        mov       esi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;183.10
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_MASTERDEST+32] ;183.10
        neg       eax                                           ;183.10
        movsx     esi, WORD PTR [20+esi+edi]                    ;183.10
        add       eax, esi                                      ;183.10
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_MASTERDEST] ;183.10
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DESTINATION+32] ;183.10
        neg       edx                                           ;183.10
        movsx     eax, WORD PTR [edi+eax*2]                     ;183.10
        add       edx, eax                                      ;183.10
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DESTINATION] ;183.10
        mov       ecx, DWORD PTR [48+esp]                       ;182.10
        inc       ecx                                           ;182.10
        cvtsi2ss  xmm0, DWORD PTR [eax+edx*4]                   ;183.10
        mov       DWORD PTR [48+esp], ecx                       ;182.10
        lea       edx, DWORD PTR [56+esp]                       ;184.15
        movss     DWORD PTR [56+esp], xmm0                      ;183.10
        mov       WORD PTR [40+esp], cx                         ;184.15
        lea       ecx, DWORD PTR [40+esp]                       ;184.15
        push      edx                                           ;184.15
        push      OFFSET FLAT: __NLITPACK_2.0.3                 ;184.15
        push      ecx                                           ;184.15
        push      ebx                                           ;184.15
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_INSERT  ;184.15
                                ; LOE
.B4.39:                         ; Preds .B4.23                  ; Infreq
        add       esp, 16                                       ;184.15
                                ; LOE
.B4.24:                         ; Preds .B4.39                  ; Infreq
        mov       eax, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE+32] ;186.17
        mov       ebx, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE] ;186.7
        mov       DWORD PTR [52+esp], eax                       ;186.17
                                ; LOE ebx
.B4.25:                         ; Preds .B4.37 .B4.24           ; Infreq
        mov       ecx, DWORD PTR [24+ebp]                       ;186.17
        mov       edi, DWORD PTR [8+ebp]                        ;187.7
        mov       eax, DWORD PTR [ecx]                          ;186.17
        sub       eax, DWORD PTR [52+esp]                       ;187.7
        imul      esi, eax, 176                                 ;187.7
        mov       eax, DWORD PTR [36+ebp]                       ;186.17
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE+32] ;187.7
        neg       ecx                                           ;187.7
        mov       eax, DWORD PTR [eax]                          ;186.17
        sub       eax, DWORD PTR [52+ebx+esi]                   ;187.7
        imul      eax, DWORD PTR [48+ebx+esi]                   ;187.7
        mov       ebx, DWORD PTR [20+ebx+esi]                   ;186.17
        mov       edx, DWORD PTR [edi]                          ;187.7
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_MASTERDEST+32] ;187.7
        add       ecx, DWORD PTR [ebx+eax]                      ;187.7
        neg       edi                                           ;187.7
        imul      ecx, ecx, 84                                  ;187.7
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE] ;187.7
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_MASTERDEST] ;187.7
        movsx     ebx, WORD PTR [36+eax+ecx]                    ;187.7
        add       edi, ebx                                      ;187.7
        mov       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;187.38
        neg       ecx                                           ;187.7
        add       ecx, edx                                      ;187.7
        shl       ecx, 5                                        ;187.7
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;187.38
        movzx     edi, WORD PTR [esi+edi*2]                     ;187.7
        sub       edx, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY+32] ;188.7
        mov       WORD PTR [20+eax+ecx], di                     ;187.7
        imul      ecx, edx, 76                                  ;188.7
        mov       eax, DWORD PTR [_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY] ;188.7
        mov       edx, DWORD PTR [48+esp]                       ;188.53
        add       WORD PTR [72+eax+ecx], dx                     ;188.53
        add       WORD PTR [74+eax+ecx], dx                     ;189.55
        add       esp, 84                                       ;189.55
        pop       ebx                                           ;189.55
        pop       edi                                           ;189.55
        pop       esi                                           ;189.55
        mov       esp, ebp                                      ;189.55
        pop       ebp                                           ;189.55
        ret                                                     ;189.55
        ALIGN     16
                                ; LOE
; mark_end;
_DYNUST_ROUTE_SWITCH_MODULE_mp_DELAY_SWITCH_MULT ENDP
_TEXT	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DB 3 DUP ( 0H)	; pad
__NLITPACK_2.0.3	DD	1
__STRLITPACK_43.0.3	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__NLITPACK_3.0.3	DD	2
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_ROUTE_SWITCH_MODULE_mp_DELAY_SWITCH_MULT
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_ROUTE_SWITCH_MODULE_mp_INFO_SWITCH
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_ROUTE_SWITCH_MODULE_mp_INFO_SWITCH
_DYNUST_ROUTE_SWITCH_MODULE_mp_INFO_SWITCH	PROC NEAR 
; parameter 1: 8 + ebp
; parameter 2: 12 + ebp
; parameter 3: 16 + ebp
; parameter 4: 20 + ebp
.B5.1:                          ; Preds .B5.0
        push      ebp                                           ;207.12
        mov       ebp, esp                                      ;207.12
        and       esp, -16                                      ;207.12
        push      esi                                           ;207.12
        push      edi                                           ;207.12
        push      ebx                                           ;207.12
        sub       esp, 52                                       ;207.12
        mov       eax, DWORD PTR [20+ebp]                       ;207.12
        lea       edx, DWORD PTR [40+esp]                       ;260.6
        mov       ebx, DWORD PTR [8+ebp]                        ;207.12
        lea       ecx, DWORD PTR [44+esp]                       ;260.6
        lea       esi, DWORD PTR [48+esp]                       ;260.6
        mov       DWORD PTR [12+esp], eax                       ;260.6
        mov       eax, ebx                                      ;260.6
        mov       DWORD PTR [16+esp], edx                       ;260.6
        mov       DWORD PTR [20+esp], ecx                       ;260.6
        mov       edx, DWORD PTR [12+ebp]                       ;260.6
        mov       ecx, DWORD PTR [16+ebp]                       ;260.6
        mov       DWORD PTR [24+esp], esi                       ;260.6
        call      _DYNUST_ROUTE_SWITCH_MODULE_mp_GET_AUTO_REMAIN_TIME. ;260.6
                                ; LOE ebx
.B5.2:                          ; Preds .B5.1
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE+32] ;313.6
        neg       edx                                           ;313.59
        pxor      xmm2, xmm2                                    ;313.59
        add       edx, DWORD PTR [ebx]                          ;313.59
        shl       edx, 6                                        ;313.59
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE] ;313.6
        movss     xmm0, DWORD PTR [_2il0floatpacket.30]         ;313.59
        movss     xmm1, DWORD PTR [40+esp]                      ;313.6
        subss     xmm0, DWORD PTR [60+eax+edx]                  ;313.32
        mulss     xmm0, xmm1                                    ;313.29
        comiss    xmm0, xmm2                                    ;313.15
        jbe       .B5.13        ; Prob 50%                      ;313.15
                                ; LOE ebx xmm1 xmm2
.B5.3:                          ; Preds .B5.2
        subss     xmm1, DWORD PTR [_DYNUST_MAIN_MODULE_mp_BOUND] ;313.84
        comiss    xmm1, xmm2                                    ;313.70
        jbe       .B5.13        ; Prob 78%                      ;313.70
                                ; LOE ebx
.B5.4:                          ; Preds .B5.3
        push      OFFSET FLAT: __NLITPACK_5.0.4                 ;314.17
        mov       eax, DWORD PTR [16+ebp]                       ;314.17
        push      eax                                           ;314.17
        push      ebx                                           ;314.17
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_VALUE   ;314.17
                                ; LOE ebx f1
.B5.17:                         ; Preds .B5.4
        fstp      DWORD PTR [44+esp]                            ;314.17
        movss     xmm0, DWORD PTR [44+esp]                      ;314.17
        add       esp, 12                                       ;314.17
                                ; LOE ebx xmm0
.B5.5:                          ; Preds .B5.17
        cvttss2si edx, xmm0                                     ;314.8
        sub       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE+32] ;316.53
        imul      ecx, edx, 84                                  ;316.53
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE] ;316.8
        mov       edi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;318.13
        neg       edi                                           ;318.58
        mov       edx, DWORD PTR [72+eax+ecx]                   ;316.11
        add       edx, edx                                      ;316.53
        add       edi, DWORD PTR [ebx]                          ;318.58
        neg       edx                                           ;316.53
        shl       edi, 5                                        ;318.58
        add       edx, DWORD PTR [40+eax+ecx]                   ;316.53
        mov       esi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;318.13
        movsx     eax, WORD PTR [2+edx]                         ;316.11
        test      eax, eax                                      ;316.53
        movsx     esi, WORD PTR [20+esi+edi]                    ;318.58
        jle       .B5.10        ; Prob 16%                      ;316.53
                                ; LOE eax edx ebx esi
.B5.6:                          ; Preds .B5.5
        xor       ecx, ecx                                      ;
                                ; LOE eax edx ecx ebx esi
.B5.7:                          ; Preds .B5.8 .B5.6
        movsx     edi, WORD PTR [4+edx+ecx*2]                   ;318.58
        cmp       edi, esi                                      ;318.58
        je        .B5.13        ; Prob 20%                      ;318.58
                                ; LOE eax edx ecx ebx esi
.B5.8:                          ; Preds .B5.7
        inc       ecx                                           ;317.9
        cmp       ecx, eax                                      ;317.9
        jb        .B5.7         ; Prob 82%                      ;317.9
                                ; LOE eax edx ecx ebx esi
.B5.10:                         ; Preds .B5.8 .B5.5
        mov       DWORD PTR [32+esp], esi                       ;325.9
        lea       eax, DWORD PTR [36+esp]                       ;327.14
        mov       esi, OFFSET FLAT: __NLITPACK_5.0.4            ;327.14
        lea       edx, DWORD PTR [32+esp]                       ;327.14
        push      eax                                           ;327.14
        push      esi                                           ;327.14
        push      edx                                           ;327.14
        mov       ecx, DWORD PTR [16+ebp]                       ;327.14
        push      ecx                                           ;327.14
        push      esi                                           ;327.14
        mov       edi, DWORD PTR [12+ebp]                       ;327.14
        push      edi                                           ;327.14
        push      ebx                                           ;327.14
        call      _RETRIEVE_VEH_PATH_ASTAR                      ;327.14
                                ; LOE ebx
.B5.18:                         ; Preds .B5.10
        add       esp, 28                                       ;327.14
                                ; LOE ebx
.B5.11:                         ; Preds .B5.18
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;328.9
        mov       esi, eax                                      ;328.9
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;328.9
        shl       edx, 8                                        ;328.9
        mov       ecx, DWORD PTR [ebx]                          ;328.37
        sub       esi, edx                                      ;328.9
        shl       ecx, 8                                        ;328.9
        mov       edi, DWORD PTR [44+ecx+esi]                   ;328.37
        sub       edi, DWORD PTR [76+ecx+esi]                   ;328.9
        inc       BYTE PTR [1+edi]                              ;328.64
        mov       ecx, DWORD PTR [ebx]                          ;329.12
        shl       ecx, 8                                        ;329.35
        add       ecx, eax                                      ;329.35
        sub       ecx, edx                                      ;329.35
        movsx     eax, BYTE PTR [80+ecx]                        ;329.12
        test      eax, eax                                      ;329.35
        jle       .B5.13        ; Prob 16%                      ;329.35
                                ; LOE eax ecx
.B5.12:                         ; Preds .B5.11
        neg       eax                                           ;329.67
        mov       BYTE PTR [80+ecx], al                         ;329.40
                                ; LOE
.B5.13:                         ; Preds .B5.7 .B5.11 .B5.3 .B5.2 .B5.12
                                ;      
        add       esp, 52                                       ;335.1
        pop       ebx                                           ;335.1
        pop       edi                                           ;335.1
        pop       esi                                           ;335.1
        mov       esp, ebp                                      ;335.1
        pop       ebp                                           ;335.1
        ret                                                     ;335.1
        ALIGN     16
                                ; LOE
; mark_end;
_DYNUST_ROUTE_SWITCH_MODULE_mp_INFO_SWITCH ENDP
_TEXT	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
__NLITPACK_5.0.4	DD	1
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_ROUTE_SWITCH_MODULE_mp_INFO_SWITCH
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_ROUTE_SWITCH_MODULE_mp_CHECK_IF_MULTM
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_ROUTE_SWITCH_MODULE_mp_CHECK_IF_MULTM
_DYNUST_ROUTE_SWITCH_MODULE_mp_CHECK_IF_MULTM	PROC NEAR 
; parameter 1: 8 + ebp
; parameter 2: 12 + ebp
; parameter 3: 16 + ebp
; parameter 4: 20 + ebp
; parameter 5: 24 + ebp
; parameter 6: 28 + ebp
; parameter 7: 32 + ebp
; parameter 8: 36 + ebp
; parameter 9: 40 + ebp
; parameter 10: 44 + ebp
.B6.1:                          ; Preds .B6.0
        push      ebp                                           ;339.12
        mov       ebp, esp                                      ;339.12
        and       esp, -16                                      ;339.12
        push      esi                                           ;339.12
        push      edi                                           ;339.12
        push      ebx                                           ;339.12
        sub       esp, 196                                      ;339.12
        xor       esi, esi                                      ;347.3
        mov       ebx, DWORD PTR [40+ebp]                       ;339.12
        mov       eax, DWORD PTR [28+ebp]                       ;339.12
        mov       edx, DWORD PTR [32+ebp]                       ;339.12
        mov       ecx, DWORD PTR [36+ebp]                       ;339.12
        mov       edi, DWORD PTR [44+ebp]                       ;339.12
        mov       DWORD PTR [ebx], esi                          ;356.8
        mov       ebx, DWORD PTR [12+ebp]                       ;356.8
        push      ebx                                           ;356.8
        mov       DWORD PTR [eax], esi                          ;347.3
        mov       DWORD PTR [edx], esi                          ;356.8
        mov       DWORD PTR [ecx], esi                          ;356.8
        mov       DWORD PTR [edi], esi                          ;356.8
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_P_SIZE  ;356.8
                                ; LOE eax ebx
.B6.2:                          ; Preds .B6.1
        dec       eax                                           ;356.8
        mov       DWORD PTR [72+esp], eax                       ;356.8
        lea       eax, DWORD PTR [72+esp]                       ;356.8
        push      OFFSET FLAT: __NLITPACK_6.0.6                 ;356.8
        push      eax                                           ;356.8
        push      ebx                                           ;356.8
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_VALUE   ;356.8
                                ; LOE ebx f1
.B6.55:                         ; Preds .B6.2
        fstp      DWORD PTR [48+esp]                            ;356.8
        movss     xmm2, DWORD PTR [48+esp]                      ;356.8
        add       esp, 16                                       ;356.8
                                ; LOE ebx xmm2
.B6.3:                          ; Preds .B6.55
        mov       ecx, DWORD PTR [32+ebp]                       ;356.8
        mov       eax, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_NUMROUTES] ;356.8
        test      eax, eax                                      ;356.8
        mov       esi, DWORD PTR [24+ebp]                       ;339.12
        mov       DWORD PTR [40+esp], eax                       ;356.8
        mov       ecx, DWORD PTR [ecx]                          ;356.8
        movss     xmm3, DWORD PTR [_2il0floatpacket.63]         ;356.8
        jle       .B6.32        ; Prob 2%                       ;356.8
                                ; LOE ecx ebx esi xmm2 xmm3
.B6.4:                          ; Preds .B6.3
        mov       edx, DWORD PTR [8+ebp]                        ;339.12
        imul      eax, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_SCHEDULE+32], -44 ;
        imul      edi, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE+32], -176 ;
        movss     xmm0, DWORD PTR [_2il0floatpacket.64]         ;356.8
        mulss     xmm0, DWORD PTR [edx]                         ;356.8
        imul      edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE+32], 84 ;
        cvtsi2ss  xmm1, DWORD PTR [esi]                         ;356.8
        add       eax, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_SCHEDULE] ;
        add       edi, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE] ;
        mov       DWORD PTR [48+esp], eax                       ;
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE] ;
        mov       DWORD PTR [96+esp], edi                       ;
        mov       edi, eax                                      ;
        sub       edi, edx                                      ;
        mov       DWORD PTR [92+esp], edi                       ;
        cvttss2si edi, xmm2                                     ;356.8
        mulss     xmm1, DWORD PTR [_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT] ;356.8
        imul      edi, edi, 84                                  ;356.8
        movss     xmm4, DWORD PTR [_2il0floatpacket.65]         ;356.8
        pxor      xmm2, xmm2                                    ;356.8
        add       edi, eax                                      ;
        mov       DWORD PTR [64+esp], 1                         ;
        sub       edi, edx                                      ;
        mov       DWORD PTR [76+esp], edi                       ;
        mov       eax, DWORD PTR [64+esp]                       ;356.8
        mov       DWORD PTR [60+esp], ecx                       ;356.8
                                ; LOE eax xmm0 xmm1 xmm2 xmm3 xmm4
.B6.5:                          ; Preds .B6.30 .B6.4
        imul      ebx, eax, 176                                 ;356.8
        mov       edx, DWORD PTR [96+esp]                       ;356.8
        mov       ecx, DWORD PTR [16+edx+ebx]                   ;356.8
        test      ecx, ecx                                      ;356.8
        mov       DWORD PTR [56+esp], ecx                       ;356.8
        jle       .B6.30        ; Prob 2%                       ;356.8
                                ; LOE eax edx ebx dl dh xmm0 xmm1 xmm2 xmm3 xmm4
.B6.6:                          ; Preds .B6.5
        mov       edi, edx                                      ;356.8
        mov       ecx, 1                                        ;
        mov       DWORD PTR [100+esp], ebx                      ;
        mov       DWORD PTR [64+esp], eax                       ;
        mov       esi, DWORD PTR [84+ebx+edi]                   ;356.8
        mov       edx, DWORD PTR [88+ebx+edi]                   ;356.8
        imul      edx, esi                                      ;
        mov       DWORD PTR [108+esp], esi                      ;356.8
        mov       esi, DWORD PTR [56+ebx+edi]                   ;356.8
        sub       esi, edx                                      ;
        imul      edx, eax, 44                                  ;
        mov       DWORD PTR [104+esp], esi                      ;
        mov       DWORD PTR [52+esp], edx                       ;
                                ; LOE ecx xmm0 xmm1 xmm2 xmm3 xmm4
.B6.7:                          ; Preds .B6.28 .B6.6
        mov       edx, DWORD PTR [108+esp]                      ;356.8
        imul      edx, ecx                                      ;356.8
        mov       eax, DWORD PTR [104+esp]                      ;356.8
        cmp       BYTE PTR [eax+edx], 0                         ;356.8
        jle       .B6.51        ; Prob 16%                      ;356.8
                                ; LOE ecx xmm0 xmm1 xmm2 xmm3 xmm4
.B6.8:                          ; Preds .B6.7
        mov       edi, DWORD PTR [48+esp]                       ;356.8
        mov       eax, DWORD PTR [52+esp]                       ;356.8
        mov       edx, DWORD PTR [4+eax+edi]                    ;356.8
        mov       ebx, DWORD PTR [36+eax+edi]                   ;356.8
        mov       DWORD PTR [172+esp], edx                      ;356.8
        mov       edx, DWORD PTR [40+eax+edi]                   ;356.8
        mov       esi, DWORD PTR [eax+edi]                      ;356.8
        mov       DWORD PTR [168+esp], ebx                      ;356.8
        imul      edx, ebx                                      ;356.8
        imul      ebx, esi                                      ;356.8
        mov       eax, DWORD PTR [8+eax+edi]                    ;356.8
        add       ebx, eax                                      ;356.8
        sub       ebx, edx                                      ;356.8
        mov       DWORD PTR [164+esp], esi                      ;356.8
        lea       esi, DWORD PTR [1+ecx]                        ;356.8
        mov       edi, DWORD PTR [32+ebx]                       ;356.8
        neg       edi                                           ;356.8
        add       edi, DWORD PTR [172+esp]                      ;356.8
        imul      edi, DWORD PTR [28+ebx]                       ;356.8
        mov       ebx, DWORD PTR [ebx]                          ;356.8
        movss     xmm5, DWORD PTR [ebx+edi]                     ;356.8
        comiss    xmm5, xmm1                                    ;356.8
        jbe       .B6.28        ; Prob 50%                      ;356.8
                                ; LOE eax edx ecx esi xmm0 xmm1 xmm2 xmm3 xmm4
.B6.9:                          ; Preds .B6.8
        mov       edi, DWORD PTR [96+esp]                       ;356.8
        mov       ebx, DWORD PTR [100+esp]                      ;356.8
        mov       edi, DWORD PTR [16+ebx+edi]                   ;356.8
        mov       ebx, esi                                      ;356.8
        cmp       edi, ecx                                      ;356.8
        jle       .B6.28        ; Prob 10%                      ;356.8
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1 xmm2 xmm3 xmm4
.B6.10:                         ; Preds .B6.9
        sub       eax, edx                                      ;
        mov       DWORD PTR [72+esp], ecx                       ;
        movss     DWORD PTR [116+esp], xmm1                     ;
        mov       DWORD PTR [44+esp], esi                       ;
        mov       DWORD PTR [160+esp], eax                      ;
        mov       DWORD PTR [112+esp], edi                      ;
        mov       ecx, DWORD PTR [76+esp]                       ;
                                ; LOE ecx ebx xmm0 xmm2 xmm3 xmm4
.B6.11:                         ; Preds .B6.26 .B6.10
        mov       edx, DWORD PTR [108+esp]                      ;356.8
        imul      edx, ebx                                      ;356.8
        mov       eax, DWORD PTR [104+esp]                      ;356.8
        cmp       BYTE PTR [eax+edx], 0                         ;356.8
        jle       .B6.26        ; Prob 16%                      ;356.8
                                ; LOE ecx ebx xmm0 xmm2 xmm3 xmm4
.B6.12:                         ; Preds .B6.11
        mov       edx, DWORD PTR [96+esp]                       ;356.8
        mov       edi, ebx                                      ;356.8
        mov       eax, DWORD PTR [100+esp]                      ;356.8
        sub       edi, DWORD PTR [52+eax+edx]                   ;356.8
        imul      edi, DWORD PTR [48+eax+edx]                   ;356.8
        mov       esi, DWORD PTR [20+eax+edx]                   ;356.8
        mov       eax, DWORD PTR [92+esp]                       ;356.8
        imul      edx, DWORD PTR [esi+edi], 84                  ;356.8
        movss     xmm1, DWORD PTR [76+eax+edx]                  ;356.8
        movss     xmm5, DWORD PTR [80+eax+edx]                  ;356.8
        cmp       DWORD PTR [164+esp], 0                        ;356.8
        subss     xmm1, DWORD PTR [76+ecx]                      ;356.8
        subss     xmm5, DWORD PTR [80+ecx]                      ;356.8
        andps     xmm1, XMMWORD PTR [_2il0floatpacket.67]       ;356.8
        andps     xmm5, XMMWORD PTR [_2il0floatpacket.67]       ;356.8
        addss     xmm1, xmm5                                    ;356.8
        mulss     xmm1, xmm0                                    ;356.8
        divss     xmm1, xmm4                                    ;356.8
        jle       .B6.26        ; Prob 2%                       ;356.8
                                ; LOE ecx ebx xmm0 xmm1 xmm2 xmm3 xmm4
.B6.13:                         ; Preds .B6.12
        mov       edi, DWORD PTR [96+esp]                       ;356.8
        mov       eax, 1                                        ;
        mov       esi, DWORD PTR [100+esp]                      ;356.8
        movss     DWORD PTR [84+esp], xmm0                      ;
        mov       DWORD PTR [184+esp], eax                      ;
        mov       edx, DWORD PTR [160+esi+edi]                  ;356.8
        neg       edx                                           ;
        add       edx, ebx                                      ;
        imul      edx, DWORD PTR [156+esi+edi]                  ;
        mov       edi, DWORD PTR [128+esi+edi]                  ;356.8
        mov       esi, DWORD PTR [168+esp]                      ;
        mov       DWORD PTR [180+esp], edi                      ;
        mov       DWORD PTR [176+esp], edx                      ;
        mov       DWORD PTR [88+esp], ebx                       ;
                                ; LOE esi xmm1 xmm2 xmm3
.B6.14:                         ; Preds .B6.24 .B6.13
        mov       eax, 1                                        ;
        cmp       DWORD PTR [172+esp], 0                        ;356.8
        jle       .B6.18        ; Prob 2%                       ;356.8
                                ; LOE eax esi xmm1 xmm2 xmm3
.B6.15:                         ; Preds .B6.14
        mov       edx, DWORD PTR [160+esp]                      ;356.8
        movss     xmm4, DWORD PTR [116+esp]                     ;
        mov       edi, DWORD PTR [32+esi+edx]                   ;356.8
        mov       ecx, DWORD PTR [28+esi+edx]                   ;356.8
        imul      edi, ecx                                      ;
        mov       ebx, DWORD PTR [esi+edx]                      ;356.8
        mov       edx, ecx                                      ;
        sub       ebx, edi                                      ;
        mov       edi, DWORD PTR [172+esp]                      ;
                                ; LOE eax edx ecx ebx esi edi xmm1 xmm2 xmm3 xmm4
.B6.16:                         ; Preds .B6.17 .B6.15
        movss     xmm0, DWORD PTR [edx+ebx]                     ;356.8
        comiss    xmm0, xmm4                                    ;356.8
        jae       .B6.49        ; Prob 20%                      ;356.8
                                ; LOE eax edx ecx ebx esi edi xmm1 xmm2 xmm3 xmm4
.B6.17:                         ; Preds .B6.16
        inc       eax                                           ;356.8
        add       edx, ecx                                      ;356.8
        cmp       eax, edi                                      ;356.8
        jle       .B6.16        ; Prob 82%                      ;356.8
                                ; LOE eax edx ecx ebx esi edi xmm1 xmm2 xmm3 xmm4
.B6.18:                         ; Preds .B6.14 .B6.17
        xor       edx, edx                                      ;
                                ; LOE eax edx esi xmm1 xmm2 xmm3
.B6.19:                         ; Preds .B6.49 .B6.18
        mov       ecx, DWORD PTR [176+esp]                      ;356.8
        mov       ebx, DWORD PTR [180+esp]                      ;356.8
        mov       edi, DWORD PTR [172+esp]                      ;356.8
        cmp       edi, eax                                      ;356.8
        movsx     ebx, BYTE PTR [ebx+ecx]                       ;356.8
        cmovl     eax, edi                                      ;356.8
        test      dl, 1                                         ;356.8
        je        .B6.24        ; Prob 60%                      ;356.8
                                ; LOE eax ebx esi xmm1 xmm2 xmm3
.B6.20:                         ; Preds .B6.19
        cmp       ebx, eax                                      ;356.8
        jle       .B6.24        ; Prob 50%                      ;356.8
                                ; LOE eax ebx esi xmm1 xmm2 xmm3
.B6.21:                         ; Preds .B6.20
        mov       edi, DWORD PTR [160+esp]                      ;356.8
        mov       ecx, DWORD PTR [28+esi+edi]                   ;356.8
        mov       edx, DWORD PTR [32+esi+edi]                   ;356.8
        imul      edx, ecx                                      ;356.8
        imul      ebx, ecx                                      ;356.8
        imul      ecx, eax                                      ;356.8
        mov       edi, DWORD PTR [esi+edi]                      ;356.8
        sub       edi, edx                                      ;356.8
        movss     xmm0, DWORD PTR [edi+ebx]                     ;356.8
        subss     xmm0, DWORD PTR [edi+ecx]                     ;356.8
        comiss    xmm2, xmm0                                    ;356.8
        ja        .B6.47        ; Prob 5%                       ;356.8
                                ; LOE esi xmm0 xmm1 xmm2 xmm3
.B6.22:                         ; Preds .B6.57 .B6.21
        addss     xmm0, xmm1                                    ;356.8
        comiss    xmm3, xmm0                                    ;356.8
        jbe       .B6.24        ; Prob 38%                      ;356.8
                                ; LOE esi xmm0 xmm1 xmm2 xmm3
.B6.23:                         ; Preds .B6.22
        mov       edx, DWORD PTR [32+ebp]                       ;356.8
        movaps    xmm3, xmm0                                    ;356.8
        mov       ebx, DWORD PTR [36+ebp]                       ;356.8
        mov       eax, DWORD PTR [64+esp]                       ;356.8
        mov       ecx, DWORD PTR [184+esp]                      ;356.8
        mov       DWORD PTR [60+esp], eax                       ;356.8
        mov       DWORD PTR [edx], eax                          ;356.8
        mov       DWORD PTR [ebx], ecx                          ;356.8
        mov       eax, DWORD PTR [40+ebp]                       ;356.8
        mov       ecx, DWORD PTR [44+ebp]                       ;356.8
        mov       edi, DWORD PTR [72+esp]                       ;356.8
        mov       edx, DWORD PTR [88+esp]                       ;356.8
        mov       DWORD PTR [eax], edi                          ;356.8
        mov       DWORD PTR [ecx], edx                          ;356.8
                                ; LOE esi xmm1 xmm2 xmm3
.B6.24:                         ; Preds .B6.23 .B6.22 .B6.20 .B6.19
        mov       eax, DWORD PTR [184+esp]                      ;356.8
        inc       eax                                           ;356.8
        add       esi, DWORD PTR [168+esp]                      ;356.8
        mov       DWORD PTR [184+esp], eax                      ;356.8
        cmp       eax, DWORD PTR [164+esp]                      ;356.8
        jle       .B6.14        ; Prob 82%                      ;356.8
                                ; LOE esi xmm1 xmm2 xmm3
.B6.25:                         ; Preds .B6.24
        movss     xmm4, DWORD PTR [_2il0floatpacket.65]         ;
        movss     xmm0, DWORD PTR [84+esp]                      ;
        mov       ebx, DWORD PTR [88+esp]                       ;
        mov       ecx, DWORD PTR [76+esp]                       ;
                                ; LOE ecx ebx xmm0 xmm2 xmm3 xmm4
.B6.26:                         ; Preds .B6.25 .B6.12 .B6.11
        inc       ebx                                           ;356.8
        cmp       ebx, DWORD PTR [112+esp]                      ;356.8
        jle       .B6.11        ; Prob 82%                      ;356.8
                                ; LOE ecx ebx xmm0 xmm2 xmm3 xmm4
.B6.27:                         ; Preds .B6.26
        movss     xmm1, DWORD PTR [116+esp]                     ;
        mov       esi, DWORD PTR [44+esp]                       ;
                                ; LOE esi xmm0 xmm1 xmm2 xmm3 xmm4
.B6.28:                         ; Preds .B6.8 .B6.27 .B6.9 .B6.51
        mov       ecx, esi                                      ;356.8
        cmp       esi, DWORD PTR [56+esp]                       ;356.8
        jle       .B6.7         ; Prob 82%                      ;356.8
                                ; LOE ecx xmm0 xmm1 xmm2 xmm3 xmm4
.B6.29:                         ; Preds .B6.28
        DB        15                                            ;
        DB        31                                            ;
        DB        68                                            ;
        DB        0                                             ;
        DB        0                                             ;
        mov       eax, DWORD PTR [64+esp]                       ;
                                ; LOE eax xmm0 xmm1 xmm2 xmm3 xmm4
.B6.30:                         ; Preds .B6.29 .B6.5
        inc       eax                                           ;356.8
        cmp       eax, DWORD PTR [40+esp]                       ;356.8
        jle       .B6.5         ; Prob 82%                      ;356.8
                                ; LOE eax xmm0 xmm1 xmm2 xmm3 xmm4
.B6.31:                         ; Preds .B6.30
        mov       ecx, DWORD PTR [60+esp]                       ;
        mov       esi, DWORD PTR [24+ebp]                       ;
        mov       ebx, DWORD PTR [12+ebp]                       ;
                                ; LOE ecx ebx esi xmm3
.B6.32:                         ; Preds .B6.3 .B6.31
        test      ecx, ecx                                      ;356.8
        jle       .B6.35        ; Prob 16%                      ;356.8
                                ; LOE ecx ebx esi xmm3
.B6.33:                         ; Preds .B6.32
        test      BYTE PTR [_DYNUST_MAIN_MODULE_mp_CAPKNOW], 1  ;356.8
        je        .B6.35        ; Prob 60%                      ;356.8
                                ; LOE ecx ebx esi xmm3
.B6.34:                         ; Preds .B6.33
        sub       ecx, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE+32] ;356.8
        imul      ecx, ecx, 176                                 ;356.8
        movss     xmm0, DWORD PTR [_2il0floatpacket.66]         ;356.8
        addss     xmm0, xmm3                                    ;356.8
        mov       eax, DWORD PTR [44+ebp]                       ;356.8
        mov       edx, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE] ;356.8
        mov       eax, DWORD PTR [eax]                          ;356.8
        sub       eax, DWORD PTR [124+edx+ecx]                  ;356.8
        imul      eax, DWORD PTR [120+edx+ecx]                  ;356.8
        mov       edi, DWORD PTR [92+edx+ecx]                   ;356.8
        movsx     edx, WORD PTR [edi+eax]                       ;356.8
        test      edx, edx                                      ;356.8
        jg        L7            ; Prob 50%                      ;356.8
        movaps    xmm3, xmm0                                    ;356.8
L7:                                                             ;
                                ; LOE ebx esi xmm3
.B6.35:                         ; Preds .B6.34 .B6.33 .B6.32
        mov       DWORD PTR [12+esp], esi                       ;358.8
        lea       eax, DWORD PTR [92+esp]                       ;358.8
        mov       edx, DWORD PTR [16+ebp]                       ;358.8
        lea       esi, DWORD PTR [76+esp]                       ;358.8
        mov       DWORD PTR [16+esp], esi                       ;358.8
        lea       edi, DWORD PTR [84+esp]                       ;358.8
        mov       DWORD PTR [20+esp], edi                       ;358.8
        mov       DWORD PTR [24+esp], eax                       ;358.8
        mov       eax, ebx                                      ;358.8
        mov       ecx, DWORD PTR [20+ebp]                       ;358.8
        movss     DWORD PTR [80+esp], xmm3                      ;358.8
        call      _DYNUST_ROUTE_SWITCH_MODULE_mp_GET_AUTO_REMAIN_TIME. ;358.8
                                ; LOE ebx
.B6.36:                         ; Preds .B6.35
        movss     xmm3, DWORD PTR [80+esp]                      ;
        movss     xmm0, DWORD PTR [76+esp]                      ;360.3
        mov       esi, DWORD PTR [76+esp]                       ;360.3
        comiss    xmm0, xmm3                                    ;360.25
        jbe       .B6.46        ; Prob 50%                      ;360.25
                                ; LOE ebx esi xmm3
.B6.37:                         ; Preds .B6.36
        test      BYTE PTR [92+esp], 1                          ;360.49
        je        .B6.46        ; Prob 60%                      ;360.49
                                ; LOE ebx esi xmm3
.B6.38:                         ; Preds .B6.37
        mov       ebx, DWORD PTR [ebx]                          ;361.5
        lea       eax, DWORD PTR [64+esp]                       ;361.5
        mov       DWORD PTR [32+esp], 0                         ;361.5
        mov       DWORD PTR [64+esp], ebx                       ;361.5
        lea       ebx, DWORD PTR [32+esp]                       ;361.5
        push      32                                            ;361.5
        push      OFFSET FLAT: DYNUST_ROUTE_SWITCH_MODULE_mp_CHECK_IF_MULTM$format_pack.0.5 ;361.5
        push      eax                                           ;361.5
        push      OFFSET FLAT: __STRLITPACK_48.0.5              ;361.5
        push      -2088435968                                   ;361.5
        push      8456                                          ;361.5
        push      ebx                                           ;361.5
        movss     DWORD PTR [108+esp], xmm3                     ;361.5
        call      _for_write_seq_fmt                            ;361.5
                                ; LOE ebx esi
.B6.39:                         ; Preds .B6.38
        movss     xmm3, DWORD PTR [108+esp]                     ;
        lea       eax, DWORD PTR [100+esp]                      ;361.5
        movss     DWORD PTR [100+esp], xmm3                     ;361.5
        push      eax                                           ;361.5
        push      OFFSET FLAT: __STRLITPACK_49.0.5              ;361.5
        push      ebx                                           ;361.5
        call      _for_write_seq_fmt_xmit                       ;361.5
                                ; LOE ebx esi
.B6.40:                         ; Preds .B6.39
        mov       DWORD PTR [120+esp], esi                      ;361.5
        lea       eax, DWORD PTR [120+esp]                      ;361.5
        push      eax                                           ;361.5
        push      OFFSET FLAT: __STRLITPACK_50.0.5              ;361.5
        push      ebx                                           ;361.5
        call      _for_write_seq_fmt_xmit                       ;361.5
                                ; LOE ebx
.B6.41:                         ; Preds .B6.40
        mov       eax, DWORD PTR [32+ebp]                       ;361.5
        lea       ecx, DWORD PTR [140+esp]                      ;361.5
        mov       edx, DWORD PTR [eax]                          ;361.5
        mov       DWORD PTR [140+esp], edx                      ;361.5
        push      ecx                                           ;361.5
        push      OFFSET FLAT: __STRLITPACK_51.0.5              ;361.5
        push      ebx                                           ;361.5
        call      _for_write_seq_fmt_xmit                       ;361.5
                                ; LOE ebx
.B6.42:                         ; Preds .B6.41
        mov       eax, DWORD PTR [36+ebp]                       ;361.5
        lea       ecx, DWORD PTR [160+esp]                      ;361.5
        mov       edx, DWORD PTR [eax]                          ;361.5
        mov       DWORD PTR [160+esp], edx                      ;361.5
        push      ecx                                           ;361.5
        push      OFFSET FLAT: __STRLITPACK_52.0.5              ;361.5
        push      ebx                                           ;361.5
        call      _for_write_seq_fmt_xmit                       ;361.5
                                ; LOE ebx
.B6.43:                         ; Preds .B6.42
        mov       eax, DWORD PTR [40+ebp]                       ;361.5
        lea       ecx, DWORD PTR [180+esp]                      ;361.5
        mov       edx, DWORD PTR [eax]                          ;361.5
        mov       DWORD PTR [180+esp], edx                      ;361.5
        push      ecx                                           ;361.5
        push      OFFSET FLAT: __STRLITPACK_53.0.5              ;361.5
        push      ebx                                           ;361.5
        call      _for_write_seq_fmt_xmit                       ;361.5
                                ; LOE ebx
.B6.44:                         ; Preds .B6.43
        mov       eax, DWORD PTR [44+ebp]                       ;361.5
        lea       ecx, DWORD PTR [200+esp]                      ;361.5
        mov       edx, DWORD PTR [eax]                          ;361.5
        mov       DWORD PTR [200+esp], edx                      ;361.5
        push      ecx                                           ;361.5
        push      OFFSET FLAT: __STRLITPACK_54.0.5              ;361.5
        push      ebx                                           ;361.5
        call      _for_write_seq_fmt_xmit                       ;361.5
                                ; LOE
.B6.56:                         ; Preds .B6.44
        add       esp, 100                                      ;361.5
                                ; LOE
.B6.45:                         ; Preds .B6.56
        mov       eax, DWORD PTR [28+ebp]                       ;362.5
        mov       DWORD PTR [eax], -1                           ;362.5
                                ; LOE
.B6.46:                         ; Preds .B6.37 .B6.36 .B6.45
        add       esp, 196                                      ;365.1
        pop       ebx                                           ;365.1
        pop       edi                                           ;365.1
        pop       esi                                           ;365.1
        mov       esp, ebp                                      ;365.1
        pop       ebp                                           ;365.1
        ret                                                     ;365.1
                                ; LOE
.B6.47:                         ; Preds .B6.21                  ; Infreq
        mov       DWORD PTR [128+esp], 0                        ;356.8
        lea       edx, DWORD PTR [128+esp]                      ;356.8
        mov       DWORD PTR [120+esp], 38                       ;356.8
        lea       eax, DWORD PTR [120+esp]                      ;356.8
        mov       DWORD PTR [124+esp], OFFSET FLAT: __STRLITPACK_14 ;356.8
        push      32                                            ;356.8
        push      eax                                           ;356.8
        push      OFFSET FLAT: __STRLITPACK_55.0.6              ;356.8
        push      -2088435968                                   ;356.8
        push      911                                           ;356.8
        push      edx                                           ;356.8
        movss     DWORD PTR [56+esp], xmm0                      ;356.8
        movss     DWORD PTR [60+esp], xmm1                      ;356.8
        movss     DWORD PTR [104+esp], xmm3                     ;356.8
        call      _for_write_seq_lis                            ;356.8
                                ; LOE esi
.B6.48:                         ; Preds .B6.47                  ; Infreq
        xor       eax, eax                                      ;356.8
        pxor      xmm2, xmm2                                    ;
        push      32                                            ;356.8
        push      eax                                           ;356.8
        push      eax                                           ;356.8
        push      -2088435968                                   ;356.8
        push      eax                                           ;356.8
        push      OFFSET FLAT: __STRLITPACK_56                  ;356.8
        call      _for_stop_core                                ;356.8
                                ; LOE esi
.B6.57:                         ; Preds .B6.48                  ; Infreq
        movss     xmm3, DWORD PTR [128+esp]                     ;
        movss     xmm1, DWORD PTR [84+esp]                      ;
        pxor      xmm2, xmm2                                    ;
        movss     xmm0, DWORD PTR [80+esp]                      ;
        add       esp, 48                                       ;356.8
        jmp       .B6.22        ; Prob 100%                     ;356.8
                                ; LOE esi xmm0 xmm1 xmm2 xmm3
.B6.49:                         ; Preds .B6.16                  ; Infreq
        mov       edx, -1                                       ;356.8
        jmp       .B6.19        ; Prob 100%                     ;356.8
                                ; LOE eax edx esi xmm1 xmm2 xmm3
.B6.51:                         ; Preds .B6.7                   ; Infreq
        lea       esi, DWORD PTR [1+ecx]                        ;356.8
        jmp       .B6.28        ; Prob 100%                     ;356.8
        ALIGN     16
                                ; LOE esi xmm0 xmm1 xmm2 xmm3 xmm4
; mark_end;
_DYNUST_ROUTE_SWITCH_MODULE_mp_CHECK_IF_MULTM ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
	DD 6 DUP (0H)	; pad
DYNUST_ROUTE_SWITCH_MODULE_mp_CHECK_IF_MULTM$format_pack.0.5	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	6
	DB	0
	DB	86
	DB	101
	DB	104
	DB	32
	DB	35
	DB	32
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	13
	DB	0
	DB	32
	DB	84
	DB	111
	DB	32
	DB	84
	DB	114
	DB	97
	DB	110
	DB	115
	DB	105
	DB	116
	DB	58
	DB	32
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	11
	DB	0
	DB	84
	DB	114
	DB	97
	DB	110
	DB	115
	DB	105
	DB	116
	DB	32
	DB	84
	DB	84
	DB	32
	DB	0
	DB	33
	DB	0
	DB	0
	DB	1
	DB	1
	DB	0
	DB	0
	DB	0
	DB	8
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	9
	DB	0
	DB	32
	DB	65
	DB	117
	DB	116
	DB	111
	DB	32
	DB	84
	DB	84
	DB	32
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	1
	DB	1
	DB	0
	DB	0
	DB	0
	DB	8
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	4
	DB	0
	DB	0
	DB	0
	DB	5
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
_DATA	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
__STRLITPACK_48.0.5	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_49.0.5	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_50.0.5	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_51.0.5	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_52.0.5	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_53.0.5	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_54.0.5	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_ROUTE_SWITCH_MODULE_mp_CHECK_IF_MULTM
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_ROUTE_SWITCH_MODULE_mp_GET_MULT_REMAIN_TIME
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_ROUTE_SWITCH_MODULE_mp_GET_MULT_REMAIN_TIME
_DYNUST_ROUTE_SWITCH_MODULE_mp_GET_MULT_REMAIN_TIME	PROC NEAR 
; parameter 1: 8 + ebp
; parameter 2: 12 + ebp
; parameter 3: 16 + ebp
; parameter 4: 20 + ebp
; parameter 5: 24 + ebp
; parameter 6: 28 + ebp
; parameter 7: 32 + ebp
; parameter 8: 36 + ebp
; parameter 9: 40 + ebp
; parameter 10: 44 + ebp
; parameter 11: 48 + ebp
.B7.1:                          ; Preds .B7.0
        push      ebp                                           ;368.12
        mov       ebp, esp                                      ;368.12
        and       esp, -16                                      ;368.12
        push      esi                                           ;368.12
        push      edi                                           ;368.12
        push      ebx                                           ;368.12
        sub       esp, 164                                      ;368.12
        xor       ecx, ecx                                      ;385.3
        mov       esi, DWORD PTR [32+ebp]                       ;368.12
        mov       eax, DWORD PTR [36+ebp]                       ;368.12
        mov       edx, DWORD PTR [40+ebp]                       ;368.12
        mov       ebx, DWORD PTR [44+ebp]                       ;368.12
        mov       edi, DWORD PTR [12+ebp]                       ;368.12
        push      edi                                           ;390.33
        mov       DWORD PTR [esi], ecx                          ;385.3
        mov       DWORD PTR [eax], ecx                          ;386.3
        mov       DWORD PTR [edx], ecx                          ;387.3
        mov       DWORD PTR [ebx], ecx                          ;388.3
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_P_SIZE  ;390.33
                                ; LOE eax ebx esi edi
.B7.2:                          ; Preds .B7.1
        dec       eax                                           ;390.14
        mov       DWORD PTR [36+esp], eax                       ;390.14
        lea       eax, DWORD PTR [36+esp]                       ;390.14
        push      OFFSET FLAT: __NLITPACK_6.0.6                 ;390.14
        push      eax                                           ;390.14
        push      edi                                           ;390.14
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_VALUE   ;390.14
                                ; LOE ebx esi f1
.B7.46:                         ; Preds .B7.2
        fstp      DWORD PTR [16+esp]                            ;390.14
        movss     xmm2, DWORD PTR [16+esp]                      ;390.14
        add       esp, 16                                       ;390.14
                                ; LOE ebx esi xmm2
.B7.3:                          ; Preds .B7.46
        mov       edx, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_NUMROUTES] ;393.3
        test      edx, edx                                      ;393.3
        mov       DWORD PTR [56+esp], edx                       ;393.3
        mov       eax, DWORD PTR [esi]                          ;439.1
        movss     xmm3, DWORD PTR [_2il0floatpacket.79]         ;384.3
        jle       .B7.32        ; Prob 2%                       ;393.3
                                ; LOE eax ebx esi xmm2 xmm3
.B7.4:                          ; Preds .B7.3
        imul      ecx, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_SCHEDULE+32], -44 ;
        imul      edi, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE+32], -176 ;
        movss     xmm0, DWORD PTR [_2il0floatpacket.80]         ;404.43
        movss     xmm4, DWORD PTR [_2il0floatpacket.81]         ;404.52
        add       ecx, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_SCHEDULE] ;
        add       edi, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE] ;
        mov       DWORD PTR [64+esp], ecx                       ;
        imul      ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE+32], 84 ;
        mov       edx, DWORD PTR [24+ebp]                       ;368.12
        mov       DWORD PTR [104+esp], edi                      ;
        mov       edi, DWORD PTR [8+ebp]                        ;404.43
        cvtsi2ss  xmm1, DWORD PTR [edx]                         ;398.25
        mulss     xmm0, DWORD PTR [edi]                         ;404.43
        mulss     xmm1, DWORD PTR [_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT] ;398.31
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE] ;
        mov       edi, edx                                      ;
        sub       edi, ecx                                      ;
        mov       DWORD PTR [100+esp], edi                      ;
        cvttss2si edi, xmm2                                     ;390.3
        imul      edi, edi, 84                                  ;390.3
        pxor      xmm2, xmm2                                    ;418.27
        add       edi, edx                                      ;
        mov       DWORD PTR [80+esp], 1                         ;
        sub       edi, ecx                                      ;
        mov       DWORD PTR [88+esp], edi                       ;
        mov       edx, DWORD PTR [80+esp]                       ;418.27
        mov       DWORD PTR [76+esp], eax                       ;418.27
                                ; LOE edx xmm0 xmm1 xmm2 xmm3 xmm4
.B7.5:                          ; Preds .B7.30 .B7.4
        imul      ebx, edx, 176                                 ;394.4
        mov       eax, DWORD PTR [104+esp]                      ;394.4
        mov       ecx, DWORD PTR [16+eax+ebx]                   ;394.4
        test      ecx, ecx                                      ;394.4
        mov       DWORD PTR [72+esp], ecx                       ;394.4
        jle       .B7.30        ; Prob 2%                       ;394.4
                                ; LOE eax edx ebx al ah xmm0 xmm1 xmm2 xmm3 xmm4
.B7.6:                          ; Preds .B7.5
        mov       edi, eax                                      ;395.9
        mov       ecx, 1                                        ;
        mov       DWORD PTR [108+esp], ebx                      ;
        mov       DWORD PTR [80+esp], edx                       ;
        mov       esi, DWORD PTR [84+ebx+edi]                   ;395.9
        mov       eax, DWORD PTR [88+ebx+edi]                   ;395.9
        imul      eax, esi                                      ;
        mov       DWORD PTR [116+esp], esi                      ;395.9
        mov       esi, DWORD PTR [56+ebx+edi]                   ;395.9
        sub       esi, eax                                      ;
        imul      eax, edx, 44                                  ;
        mov       DWORD PTR [112+esp], esi                      ;
        mov       DWORD PTR [68+esp], eax                       ;
                                ; LOE ecx xmm0 xmm1 xmm2 xmm3 xmm4
.B7.7:                          ; Preds .B7.28 .B7.6
        mov       edx, DWORD PTR [116+esp]                      ;395.32
        imul      edx, ecx                                      ;395.32
        mov       eax, DWORD PTR [112+esp]                      ;395.32
        cmp       BYTE PTR [eax+edx], 0                         ;395.32
        jle       .B7.42        ; Prob 16%                      ;395.32
                                ; LOE ecx xmm0 xmm1 xmm2 xmm3 xmm4
.B7.8:                          ; Preds .B7.7
        mov       edi, DWORD PTR [64+esp]                       ;399.28
        mov       eax, DWORD PTR [68+esp]                       ;399.28
        mov       edx, DWORD PTR [4+eax+edi]                    ;399.28
        mov       ebx, DWORD PTR [36+eax+edi]                   ;399.28
        mov       DWORD PTR [140+esp], edx                      ;399.28
        mov       edx, DWORD PTR [40+eax+edi]                   ;399.28
        mov       esi, DWORD PTR [eax+edi]                      ;399.28
        mov       DWORD PTR [136+esp], ebx                      ;399.28
        imul      edx, ebx                                      ;399.26
        imul      ebx, esi                                      ;399.28
        mov       eax, DWORD PTR [8+eax+edi]                    ;399.28
        add       ebx, eax                                      ;399.26
        sub       ebx, edx                                      ;399.26
        mov       DWORD PTR [132+esp], esi                      ;399.28
        lea       esi, DWORD PTR [1+ecx]                        ;400.10
        mov       edi, DWORD PTR [32+ebx]                       ;399.28
        neg       edi                                           ;399.26
        add       edi, DWORD PTR [140+esp]                      ;399.26
        imul      edi, DWORD PTR [28+ebx]                       ;399.26
        mov       ebx, DWORD PTR [ebx]                          ;399.28
        movss     xmm5, DWORD PTR [ebx+edi]                     ;399.28
        comiss    xmm5, xmm1                                    ;399.26
        jbe       .B7.28        ; Prob 50%                      ;399.26
                                ; LOE eax edx ecx esi xmm0 xmm1 xmm2 xmm3 xmm4
.B7.9:                          ; Preds .B7.8
        mov       edi, DWORD PTR [104+esp]                      ;400.10
        mov       ebx, DWORD PTR [108+esp]                      ;400.10
        mov       edi, DWORD PTR [16+ebx+edi]                   ;400.10
        mov       ebx, esi                                      ;400.10
        cmp       edi, ecx                                      ;400.10
        jle       .B7.28        ; Prob 10%                      ;400.10
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1 xmm2 xmm3 xmm4
.B7.10:                         ; Preds .B7.9
        sub       eax, edx                                      ;
        mov       DWORD PTR [84+esp], ecx                       ;
        movss     DWORD PTR [124+esp], xmm1                     ;
        mov       DWORD PTR [60+esp], esi                       ;
        mov       DWORD PTR [128+esp], eax                      ;
        mov       DWORD PTR [120+esp], edi                      ;
        mov       ecx, DWORD PTR [88+esp]                       ;
                                ; LOE ecx ebx xmm0 xmm2 xmm3 xmm4
.B7.11:                         ; Preds .B7.26 .B7.10
        mov       edx, DWORD PTR [116+esp]                      ;401.38
        imul      edx, ebx                                      ;401.38
        mov       eax, DWORD PTR [112+esp]                      ;401.38
        cmp       BYTE PTR [eax+edx], 0                         ;401.38
        jle       .B7.26        ; Prob 16%                      ;401.38
                                ; LOE ecx ebx xmm0 xmm2 xmm3 xmm4
.B7.12:                         ; Preds .B7.11
        mov       edx, DWORD PTR [104+esp]                      ;404.14
        mov       edi, ebx                                      ;404.14
        mov       eax, DWORD PTR [108+esp]                      ;404.14
        sub       edi, DWORD PTR [52+eax+edx]                   ;404.14
        imul      edi, DWORD PTR [48+eax+edx]                   ;404.14
        mov       esi, DWORD PTR [20+eax+edx]                   ;402.14
        mov       eax, DWORD PTR [100+esp]                      ;402.30
        imul      edx, DWORD PTR [esi+edi], 84                  ;403.30
        movss     xmm1, DWORD PTR [76+eax+edx]                  ;402.30
        movss     xmm5, DWORD PTR [80+eax+edx]                  ;403.30
        cmp       DWORD PTR [132+esp], 0                        ;406.14
        subss     xmm1, DWORD PTR [76+ecx]                      ;402.76
        subss     xmm5, DWORD PTR [80+ecx]                      ;403.76
        andps     xmm1, XMMWORD PTR [_2il0floatpacket.83]       ;402.26
        andps     xmm5, XMMWORD PTR [_2il0floatpacket.83]       ;403.26
        addss     xmm1, xmm5                                    ;402.14
        mulss     xmm1, xmm0                                    ;404.48
        divss     xmm1, xmm4                                    ;404.14
        jle       .B7.26        ; Prob 2%                       ;406.14
                                ; LOE ecx ebx xmm0 xmm1 xmm2 xmm3 xmm4
.B7.13:                         ; Preds .B7.12
        mov       edi, DWORD PTR [104+esp]                      ;414.22
        mov       eax, 1                                        ;
        mov       esi, DWORD PTR [108+esp]                      ;414.22
        movss     DWORD PTR [92+esp], xmm0                      ;
        mov       DWORD PTR [152+esp], eax                      ;
        mov       edx, DWORD PTR [160+esi+edi]                  ;414.22
        neg       edx                                           ;
        add       edx, ebx                                      ;
        imul      edx, DWORD PTR [156+esi+edi]                  ;
        mov       edi, DWORD PTR [128+esi+edi]                  ;414.22
        mov       esi, DWORD PTR [136+esp]                      ;
        mov       DWORD PTR [148+esp], edi                      ;
        mov       DWORD PTR [144+esp], edx                      ;
        mov       DWORD PTR [96+esp], ebx                       ;
                                ; LOE esi xmm1 xmm2 xmm3
.B7.14:                         ; Preds .B7.24 .B7.13
        mov       eax, 1                                        ;
        cmp       DWORD PTR [140+esp], 0                        ;408.16
        jle       .B7.18        ; Prob 2%                       ;408.16
                                ; LOE eax esi xmm1 xmm2 xmm3
.B7.15:                         ; Preds .B7.14
        mov       edx, DWORD PTR [128+esp]                      ;409.21
        movss     xmm4, DWORD PTR [124+esp]                     ;
        mov       edi, DWORD PTR [32+esi+edx]                   ;409.21
        mov       ecx, DWORD PTR [28+esi+edx]                   ;409.21
        imul      edi, ecx                                      ;
        mov       ebx, DWORD PTR [esi+edx]                      ;409.21
        mov       edx, ecx                                      ;
        sub       ebx, edi                                      ;
        mov       edi, DWORD PTR [140+esp]                      ;
                                ; LOE eax edx ecx ebx esi edi xmm1 xmm2 xmm3 xmm4
.B7.16:                         ; Preds .B7.17 .B7.15
        movss     xmm0, DWORD PTR [edx+ebx]                     ;409.21
        comiss    xmm0, xmm4                                    ;409.60
        jae       .B7.39        ; Prob 20%                      ;409.60
                                ; LOE eax edx ecx ebx esi edi xmm1 xmm2 xmm3 xmm4
.B7.17:                         ; Preds .B7.16
        inc       eax                                           ;413.16
        add       edx, ecx                                      ;413.16
        cmp       eax, edi                                      ;413.16
        jle       .B7.16        ; Prob 82%                      ;413.16
                                ; LOE eax edx ecx ebx esi edi xmm1 xmm2 xmm3 xmm4
.B7.18:                         ; Preds .B7.14 .B7.17
        xor       edx, edx                                      ;
                                ; LOE eax edx esi xmm1 xmm2 xmm3
.B7.19:                         ; Preds .B7.39 .B7.18
        mov       ecx, DWORD PTR [144+esp]                      ;414.22
        mov       ebx, DWORD PTR [148+esp]                      ;414.22
        mov       edi, DWORD PTR [140+esp]                      ;415.16
        cmp       edi, eax                                      ;415.16
        movsx     ebx, BYTE PTR [ebx+ecx]                       ;414.22
        cmovl     eax, edi                                      ;415.16
        test      dl, 1                                         ;416.19
        je        .B7.24        ; Prob 60%                      ;416.19
                                ; LOE eax ebx esi xmm1 xmm2 xmm3
.B7.20:                         ; Preds .B7.19
        cmp       ebx, eax                                      ;416.34
        jle       .B7.24        ; Prob 50%                      ;416.34
                                ; LOE eax ebx esi xmm1 xmm2 xmm3
.B7.21:                         ; Preds .B7.20
        mov       edi, DWORD PTR [128+esp]                      ;417.26
        mov       ecx, DWORD PTR [28+esi+edi]                   ;417.26
        mov       edx, DWORD PTR [32+esi+edi]                   ;417.26
        imul      edx, ecx                                      ;417.18
        imul      ebx, ecx                                      ;414.16
        imul      ecx, eax                                      ;408.16
        mov       edi, DWORD PTR [esi+edi]                      ;417.26
        sub       edi, edx                                      ;417.18
        movss     xmm0, DWORD PTR [edi+ebx]                     ;417.26
        subss     xmm0, DWORD PTR [edi+ecx]                     ;417.18
        comiss    xmm2, xmm0                                    ;418.27
        ja        .B7.37        ; Prob 5%                       ;418.27
                                ; LOE esi xmm0 xmm1 xmm2 xmm3
.B7.22:                         ; Preds .B7.47 .B7.21
        addss     xmm0, xmm1                                    ;422.32
        comiss    xmm3, xmm0                                    ;422.39
        jbe       .B7.24        ; Prob 38%                      ;422.39
                                ; LOE esi xmm0 xmm1 xmm2 xmm3
.B7.23:                         ; Preds .B7.22
        mov       edx, DWORD PTR [32+ebp]                       ;424.20
        movaps    xmm3, xmm0                                    ;423.20
        mov       ebx, DWORD PTR [36+ebp]                       ;425.20
        mov       eax, DWORD PTR [80+esp]                       ;424.20
        mov       ecx, DWORD PTR [152+esp]                      ;425.20
        mov       DWORD PTR [76+esp], eax                       ;424.20
        mov       DWORD PTR [edx], eax                          ;424.20
        mov       DWORD PTR [ebx], ecx                          ;425.20
        mov       eax, DWORD PTR [40+ebp]                       ;426.20
        mov       ecx, DWORD PTR [44+ebp]                       ;427.20
        mov       edi, DWORD PTR [84+esp]                       ;426.20
        mov       edx, DWORD PTR [96+esp]                       ;427.20
        mov       DWORD PTR [eax], edi                          ;426.20
        mov       DWORD PTR [ecx], edx                          ;427.20
                                ; LOE esi xmm1 xmm2 xmm3
.B7.24:                         ; Preds .B7.23 .B7.22 .B7.20 .B7.19
        mov       eax, DWORD PTR [152+esp]                      ;430.14
        inc       eax                                           ;430.14
        add       esi, DWORD PTR [136+esp]                      ;430.14
        mov       DWORD PTR [152+esp], eax                      ;430.14
        cmp       eax, DWORD PTR [132+esp]                      ;430.14
        jle       .B7.14        ; Prob 82%                      ;430.14
                                ; LOE esi xmm1 xmm2 xmm3
.B7.25:                         ; Preds .B7.24
        movss     xmm4, DWORD PTR [_2il0floatpacket.81]         ;
        movss     xmm0, DWORD PTR [92+esp]                      ;
        mov       ebx, DWORD PTR [96+esp]                       ;
        mov       ecx, DWORD PTR [88+esp]                       ;
                                ; LOE ecx ebx xmm0 xmm2 xmm3 xmm4
.B7.26:                         ; Preds .B7.25 .B7.12 .B7.11
        inc       ebx                                           ;432.10
        cmp       ebx, DWORD PTR [120+esp]                      ;432.10
        jle       .B7.11        ; Prob 82%                      ;432.10
                                ; LOE ecx ebx xmm0 xmm2 xmm3 xmm4
.B7.27:                         ; Preds .B7.26
        movss     xmm1, DWORD PTR [124+esp]                     ;
        mov       esi, DWORD PTR [60+esp]                       ;
                                ; LOE esi xmm0 xmm1 xmm2 xmm3 xmm4
.B7.28:                         ; Preds .B7.8 .B7.27 .B7.9 .B7.42
        mov       ecx, esi                                      ;435.4
        cmp       esi, DWORD PTR [72+esp]                       ;435.4
        jle       .B7.7         ; Prob 82%                      ;435.4
                                ; LOE ecx xmm0 xmm1 xmm2 xmm3 xmm4
.B7.29:                         ; Preds .B7.28
        DB        15                                            ;
        DB        31                                            ;
        DB        68                                            ;
        DB        0                                             ;
        DB        0                                             ;
        mov       edx, DWORD PTR [80+esp]                       ;
                                ; LOE edx xmm0 xmm1 xmm2 xmm3 xmm4
.B7.30:                         ; Preds .B7.29 .B7.5
        inc       edx                                           ;436.3
        cmp       edx, DWORD PTR [56+esp]                       ;436.3
        jle       .B7.5         ; Prob 82%                      ;436.3
                                ; LOE edx xmm0 xmm1 xmm2 xmm3 xmm4
.B7.31:                         ; Preds .B7.30
        mov       eax, DWORD PTR [76+esp]                       ;
        mov       ebx, DWORD PTR [44+ebp]                       ;
                                ; LOE eax ebx xmm3
.B7.32:                         ; Preds .B7.3 .B7.31
        mov       edx, DWORD PTR [28+ebp]                       ;368.12
        test      eax, eax                                      ;439.12
        movss     DWORD PTR [edx], xmm3                         ;438.1
        jle       .B7.41        ; Prob 16%                      ;439.12
                                ; LOE eax edx ebx xmm3
.B7.33:                         ; Preds .B7.32
        test      BYTE PTR [_DYNUST_MAIN_MODULE_mp_CAPKNOW], 1  ;440.4
        je        .B7.36        ; Prob 60%                      ;440.4
                                ; LOE eax edx ebx xmm3
.B7.34:                         ; Preds .B7.33
        sub       eax, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE+32] ;440.11
        imul      ecx, eax, 176                                 ;440.11
        mov       eax, DWORD PTR [_DYNUST_TRANSIT_MODULE_mp_M_ROUTE] ;440.4
        mov       esi, DWORD PTR [ebx]                          ;440.16
        sub       esi, DWORD PTR [124+eax+ecx]                  ;440.11
        imul      esi, DWORD PTR [120+eax+ecx]                  ;440.11
        mov       ebx, DWORD PTR [92+eax+ecx]                   ;440.16
        movsx     edi, WORD PTR [ebx+esi]                       ;440.16
        test      edi, edi                                      ;440.61
        jg        .B7.36        ; Prob 84%                      ;440.61
                                ; LOE edx xmm3
.B7.35:                         ; Preds .B7.34
        mov       eax, DWORD PTR [48+ebp]                       ;442.4
        addss     xmm3, DWORD PTR [_2il0floatpacket.82]         ;441.4
        movss     DWORD PTR [edx], xmm3                         ;441.4
        mov       DWORD PTR [eax], 0                            ;442.4
                                ; LOE
.B7.36:                         ; Preds .B7.34 .B7.33 .B7.35
        add       esp, 164                                      ;448.1
        pop       ebx                                           ;448.1
        pop       edi                                           ;448.1
        pop       esi                                           ;448.1
        mov       esp, ebp                                      ;448.1
        pop       ebp                                           ;448.1
        ret                                                     ;448.1
                                ; LOE
.B7.37:                         ; Preds .B7.21                  ; Infreq
        mov       DWORD PTR [esp], 0                            ;419.20
        lea       edx, DWORD PTR [esp]                          ;419.20
        mov       DWORD PTR [40+esp], 38                        ;419.20
        lea       eax, DWORD PTR [40+esp]                       ;419.20
        mov       DWORD PTR [44+esp], OFFSET FLAT: __STRLITPACK_14 ;419.20
        push      32                                            ;419.20
        push      eax                                           ;419.20
        push      OFFSET FLAT: __STRLITPACK_55.0.6              ;419.20
        push      -2088435968                                   ;419.20
        push      911                                           ;419.20
        push      edx                                           ;419.20
        movss     DWORD PTR [60+esp], xmm0                      ;419.20
        movss     DWORD PTR [72+esp], xmm1                      ;419.20
        movss     DWORD PTR [76+esp], xmm3                      ;419.20
        call      _for_write_seq_lis                            ;419.20
                                ; LOE esi
.B7.38:                         ; Preds .B7.37                  ; Infreq
        xor       eax, eax                                      ;420.20
        pxor      xmm2, xmm2                                    ;
        push      32                                            ;420.20
        push      eax                                           ;420.20
        push      eax                                           ;420.20
        push      -2088435968                                   ;420.20
        push      eax                                           ;420.20
        push      OFFSET FLAT: __STRLITPACK_56                  ;420.20
        call      _for_stop_core                                ;420.20
                                ; LOE esi
.B7.47:                         ; Preds .B7.38                  ; Infreq
        movss     xmm3, DWORD PTR [100+esp]                     ;
        movss     xmm1, DWORD PTR [96+esp]                      ;
        pxor      xmm2, xmm2                                    ;
        movss     xmm0, DWORD PTR [84+esp]                      ;
        add       esp, 48                                       ;420.20
        jmp       .B7.22        ; Prob 100%                     ;420.20
                                ; LOE esi xmm0 xmm1 xmm2 xmm3
.B7.39:                         ; Preds .B7.16                  ; Infreq
        mov       edx, -1                                       ;410.20
        jmp       .B7.19        ; Prob 100%                     ;410.20
                                ; LOE eax edx esi xmm1 xmm2 xmm3
.B7.41:                         ; Preds .B7.32                  ; Infreq
        mov       eax, DWORD PTR [48+ebp]                       ;445.4
        mov       DWORD PTR [eax], 0                            ;445.4
        add       esp, 164                                      ;445.4
        pop       ebx                                           ;445.4
        pop       edi                                           ;445.4
        pop       esi                                           ;445.4
        mov       esp, ebp                                      ;445.4
        pop       ebp                                           ;445.4
        ret                                                     ;445.4
                                ; LOE
.B7.42:                         ; Preds .B7.7                   ; Infreq
        lea       esi, DWORD PTR [1+ecx]                        ;435.4
        jmp       .B7.28        ; Prob 100%                     ;435.4
        ALIGN     16
                                ; LOE esi xmm0 xmm1 xmm2 xmm3 xmm4
; mark_end;
_DYNUST_ROUTE_SWITCH_MODULE_mp_GET_MULT_REMAIN_TIME ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_ROUTE_SWITCH_MODULE_mp_GET_MULT_REMAIN_TIME
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DD 2 DUP (0H)	; pad
	DB 3 DUP ( 0H)	; pad
_2il0floatpacket.67	DD	07fffffffH,000000000H,000000000H,000000000H
_2il0floatpacket.83	DD	07fffffffH,000000000H,000000000H,000000000H
_2il0floatpacket.4	DD	042c80000H
__STRLITPACK_12	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	73
	DB	78
	DB	70
	DB	79
	DB	95
	DB	83
	DB	87
	DB	73
	DB	84
	DB	67
	DB	72
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_10	DB	106
	DB	44
	DB	32
	DB	105
	DB	44
	DB	32
	DB	105
	DB	99
	DB	99
	DB	117
	DB	114
	DB	114
	DB	110
	DB	116
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_62	DB	0
	DB 3 DUP ( 0H)	; pad
_2il0floatpacket.12	DD	042c80000H
_2il0floatpacket.13	DD	080000000H
_2il0floatpacket.14	DD	04b000000H
_2il0floatpacket.15	DD	03f000000H
_2il0floatpacket.16	DD	0bf000000H
__STRLITPACK_22	DB	69
	DB	82
	DB	82
	DB	79
	DB	82
	DB	32
	DB	73
	DB	78
	DB	32
	DB	65
	DB	83
	DB	83
	DB	73
	DB	71
	DB	78
	DB	73
	DB	78
	DB	71
	DB	32
	DB	84
	DB	82
	DB	65
	DB	78
	DB	83
	DB	73
	DB	84
	DB	32
	DB	80
	DB	65
	DB	84
	DB	72
	DB	0
_2il0floatpacket.30	DD	03f800000H
__NLITPACK_6.0.6	DD	1
__STRLITPACK_14	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	116
	DB	105
	DB	109
	DB	101
	DB	50
	DB	32
	DB	105
	DB	110
	DB	32
	DB	71
	DB	101
	DB	116
	DB	95
	DB	77
	DB	117
	DB	108
	DB	116
	DB	95
	DB	82
	DB	101
	DB	109
	DB	97
	DB	105
	DB	110
	DB	95
	DB	84
	DB	105
	DB	109
	DB	101
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_55.0.6	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_56	DB	0
	DB 3 DUP ( 0H)	; pad
_2il0floatpacket.63	DD	049742400H
_2il0floatpacket.64	DD	044a50000H
_2il0floatpacket.65	DD	042700000H
_2il0floatpacket.66	DD	0447a0000H
_2il0floatpacket.79	DD	049742400H
_2il0floatpacket.80	DD	044a50000H
_2il0floatpacket.81	DD	042700000H
_2il0floatpacket.82	DD	0447a0000H
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TRAVELTIME:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_SIMPERAGG:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_BOUND:BYTE
EXTRN	_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_ARRAY:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_DESTINATION:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_CAPKNOW:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_MASTERDEST:BYTE
EXTRN	_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE:BYTE
EXTRN	_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE:BYTE
EXTRN	_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND:BYTE
EXTRN	_DYNUST_TRANSIT_MODULE_mp_M_SCHEDULE:BYTE
EXTRN	_DYNUST_TRANSIT_MODULE_mp_NUMROUTES:BYTE
EXTRN	_DYNUST_TRANSIT_MODULE_mp_M_ROUTE:BYTE
_DATA	ENDS
EXTRN	_for_write_seq_fmt_xmit:PROC
EXTRN	_for_write_seq_fmt:PROC
EXTRN	_for_write_seq_lis_xmit:PROC
EXTRN	_for_stop_core:PROC
EXTRN	_for_write_seq_lis:PROC
EXTRN	_RETRIEVE_VEH_PATH_ASTAR:PROC
EXTRN	_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_VALUE:PROC
EXTRN	_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_P_SIZE:PROC
EXTRN	_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_INSERT:PROC
EXTRN	_DYNUST_NETWORK_MODULE_mp_GETFLINKFROMNODE:PROC
EXTRN	_DYNUST_NETWORK_MODULE_mp_GETBSTMOVE:PROC
EXTRN	__fltused:BYTE
	INCLUDELIB <ifconsol>
	INCLUDELIB <libifcoremt>
	INCLUDELIB <libifport>
	INCLUDELIB <libmmt>
	INCLUDELIB <LIBCMT>
	INCLUDELIB <libirc>
	INCLUDELIB <svml_dispmt>
	INCLUDELIB <OLDNAMES>
	END
