MODULE MEMORY_MODULE
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~ Copyright (C) 2002-2011 The Arizona Board of Regents on Behalf of The University of Arizona.  ~~!
!~~ Developed by Yi-Chang Chiu, Ph.D. as par to the DynusT project.                               ~~!
!~~ This program is free software: you can redistribute it and/or modify it                       ~~!
!~~ under the terms of the GNU General Public License version 2 as published by                   ~~!
!~~ the Free Software Foundation.                                                                 ~~!
!~~ This program is distributed in the hope that it will be useful,                               ~~!
!~~ but WITHOUT ANY WARRANTY; without even the implied warranty of                                ~~!
!~~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                                          ~~!
!~~ See the GNU General Public License for more details.                                          ~~!
!~~ You should have received a copy of the GNU General Public License                             ~~!
!~~ along with this program (in the file COPYING).                                                ~~!
!~~ IF not, see <http://www.gnu.org/licenses/>.                                                   ~~!
!~~ Please see http://www.dynust.net for contact information and names                            ~~!
!~~ of individual contributors.                                                                   ~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
    USE DYNUST_MAIN_MODULE
    USE DYNUST_VEH_MODULE
    USE DYNUST_NETWORK_MODULE
	USE DYNUST_VEH_PATH_ATT_MODULE
    USE DYNUST_LINK_VEH_LIST_MODULE
    USE DYNUST_SIGNAL_MODULE
    USE DYNUST_AMS_MODULE
    USE DYNUST_FUEL_MODULE
    USE DYNUST_TDSP_ASTAR_MODULE
    
	
CONTAINS

SUBROUTINE  MEMALLOCATE_SIM

	allocate (NoofGenLinksPerZone(nzones),stat=error)
	IF(error /= 0) THEN
	  WRITE(911,*) 'allocate NoofGenLinksPerZone error'
	  STOP
	ENDIF
	NoofGenLinksPerZone(:)=0

	allocate (zoneDemandProfile(nu_types,nzones,nzones),stat=error)
	IF(error /= 0) THEN
	  WRITE(911,*) 'allocate zoneDemandProfile error - insufficient memory'
	  STOP
	ENDIF
	zoneDemandProfile(:,:,:)=0

	IF(iteration == 0) THEN
      allocate (readveh(nu_types,5),stat=error)
	   IF(error /= 0) THEN
	     WRITE(911,*) 'allocate readveh error - insufficient memory'
	     STOP
	   ENDIF
	   readveh(:,:)=0
    ENDIF
    
	allocate (DemCell(nzones,nzones),stat=error)
	IF(error /= 0) THEN
	  WRITE(911,*) 'allocate zdem error - insufficient memory'
	  STOP
	ENDIF
	DemCell(:,:)=0

	allocate (numActZone(3,nzones),stat=error) ! 3 vehicle types
	IF(error /= 0) THEN
	  WRITE(911,*) 'allocate numActZone error - insufficient memory'
	  STOP
	ENDIF
	numActZone(:,:)=nzones
	
	allocate (DemOrigAcu(nzones,nzones),stat=error)
	IF(error /= 0) THEN
	  WRITE(911,*) 'allocate zfdem error - insufficient memory'
	  STOP
	ENDIF
	DemOrigAcu(:,:)=0

	allocate (DemDest(nzones),stat=error)
	IF(error /= 0) THEN
	  WRITE(911,*) 'allocate ztdem error - insufficient memory'
	  STOP
	ENDIF
	DemDest(:)=0

	allocate (DemOrig(nzones),stat=error)
	IF(error /= 0) THEN
	  WRITE(911,*) 'allocate ztdemGen error - insufficient memory'
	  STOP
	ENDIF
	DemOrig(:)=0

	allocate (DemOrigT(nzones),stat=error)
	IF(error /= 0) THEN
	  WRITE(911,*) 'allocate DemOrigT error - insufficient memory'
	  STOP
	ENDIF
	DemOrigT(:)=0

	allocate (DemGenZT(nzones),stat=error)
	IF(error /= 0) THEN
	  WRITE(911,*) 'allocate DemGenZT error - insufficient memory'
	  STOP
	ENDIF
	DemGenZT(:)=0

	allocate (DemCellT(nzones,nzones),stat=error)
	IF(error /= 0) THEN
	  WRITE(911,*) 'allocate DemCellT error - insufficient memory'
	  STOP
	ENDIF
	DemCellT(:,:)=0

	allocate (DemOrigAcuT(nzones,nzones),stat=error)
	IF(error /= 0) THEN
	  WRITE(911,*) 'allocate zfdemT error - insufficient memory'
	  STOP
	ENDIF
	DemOrigAcuT(:,:)=0
    

	allocate (DemDestT(nzones),stat=error)
	IF(error /= 0) THEN
	  WRITE(911,*) 'allocate ztdemT error - insufficient memory'
	  STOP
	ENDIF
	DemDestT(:)=0


	allocate (DemOrigH(nzones),stat=error)
	IF(error /= 0) THEN
	  WRITE(911,*) 'allocate DemOrigH error - insufficient memory'
	  STOP
	ENDIF
	DemOrigH(:)=0

	allocate (DemGenZH(nzones),stat=error)
	IF(error /= 0) THEN
	  WRITE(911,*) 'allocate DemGenZH error - insufficient memory'
	  STOP
	ENDIF
	DemGenZH(:)=0

	allocate (DemCellH(nzones,nzones),stat=error)
	IF(error /= 0) THEN
	  WRITE(911,*) 'allocate DemCellH error - insufficient memory'
	  STOP
	ENDIF
	DemCellH(:,:)=0


	allocate (DemOrigAcuH(nzones,nzones),stat=error)
	IF(error /= 0) THEN
	  WRITE(911,*) 'allocate DemOrigAcuH error - insufficient memory'
	  STOP
	ENDIF
	DemOrigAcuH(:,:)=0
    
	allocate (DemDestH(nzones),stat=error)
	IF(error /= 0) THEN
	  WRITE(911,*) 'allocate ztdemH error - insufficient memory'
	  STOP
	ENDIF
	DemDestH(:)=0

	allocate (TotalLinkLenPerZone(nzones),stat=error)
	IF(error /= 0) THEN
	  WRITE(911,*) 'allocate totlmz error - insufficient memory'
	  STOP
	ENDIF
	TotalLinkLenPerZone(:)=0

	allocate (DemGenZ(nzones),stat=error)
	IF(error /= 0) THEN
	  WRITE(911,*) 'allocate DemGenZ error - insufficient memory'
	  STOP
	ENDIF
	DemGenZ(:)=0


    IF(vms_num > 0) THEN

    ALLOCATE(nodeWVMS(noofnodes),stat=error)
	IF(error /= 0) THEN 
	  WRITE(911,*) 'allocate nodeWVMS error - insufficient memory'
	  STOP
	ENDIF
	nodeWVMS(:) = 0
      
    ALLOCATE(vmstypetwopath(vms_num,10,100),stat=error) ! each vms has up to 10 subpaths and each subpath has up to 100 nodes
	IF(error /= 0) THEN 
	  WRITE(911,*) 'allocate vmstypetwopath error - insufficient memory'
	  STOP
	ENDIF
	vmstypetwopath(:,:,:)%node = 0 
	vmstypetwopath(:,:,:)%link = 0

    ALLOCATE(vmsrate(vms_num,10),stat=error) ! each vms has up to 10 subpaths and each subpath has up to 100 nodes
	IF(error /= 0) THEN 
	  WRITE(911,*) 'allocate vmsrate error - insufficient memory'
	  STOP
	ENDIF
	vmsrate(:,:) = 0 

    ALLOCATE(vmsnnode(vms_num,10),stat=error) ! each vms has up to 10 subpaths and each subpath has up to 100 nodes
	IF(error /= 0) THEN 
	  WRITE(911,*) 'allocate vmsnnode error - insufficient memory'
	  STOP
	ENDIF
	vmsnnode(:,:) = 0 
	
    ALLOCATE(vmstype(vms_num),stat=error)
	IF(error /= 0) THEN 
	  WRITE(911,*) 'allocate vmstype error - insufficient memory'
	  STOP
	ENDIF
	vmstype(:) = 0 
	
    ALLOCATE(vms(vms_num,4),stat=error)
	IF(error /= 0) THEN
	  WRITE(911,*) 'allocate vms error - insufficient memory'
	  STOP
	ENDIF
	vms(:,:) = 0
	
    ALLOCATE(vms_start(vms_num),stat=error)
	IF(error /= 0) THEN
	  WRITE(911,*) 'allocate vms_start error - insufficient memory'
	  STOP
	ENDIF
	vms_start(:) = 0
	
    ALLOCATE(vms_end(vms_num),stat=error)
	IF(error /= 0) THEN
	  WRITE(911,*) 'allocate vms_end error - insufficient memory'
	  STOP
	ENDIF
	vms_end(:) = 0
      
	ELSE

    ALLOCATE(vmstypetwopath(1,1,1),stat=error)
	IF(error /= 0) THEN 
	  WRITE(911,*) 'allocate vmstypetwopath error - insufficient memory'
	  STOP
	ENDIF
	vmstypetwopath(:,:,:)%node = 0 
	vmstypetwopath(:,:,:)%link = 0

	
    ALLOCATE(vmstype(1),stat=error)
 	IF(error /= 0) THEN
	  WRITE(911,*) 'allocate vmstype error - insufficient memory'
	  STOP
	ENDIF
	vmstype(:) = 0 
	
    ALLOCATE(vms(1,4),stat=error)
	IF(error /= 0) THEN
	  WRITE(911,*) 'allocate vms error - insufficient memory'
	  STOP
	ENDIF
	vms(:,:) = 0
	
    ALLOCATE(vms_start(1),stat=error)
	IF(error /= 0) THEN
	  WRITE(911,*) 'allocate vms_start error - insufficient memory'
	  STOP
	ENDIF
	vms_start(:) = 0
	
    ALLOCATE(vms_end(1),stat=error)
	IF(error /= 0) THEN
	  WRITE(911,*) 'allocate vms_end error - insufficient memory'
	  STOP
	ENDIF
	vms_end(:) = 0

	ENDIF

	IF(dec_num > 0) THEN

	ALLOCATE(ramp_par(dec_num,4),stat=error)
	IF(error /= 0) THEN
	  WRITE(911,*) 'allocate ramp_par error - insufficient memory'
	  STOP
	ENDIF
	ramp_par(:,:)=0.0
	
      ALLOCATE(ramp_start(dec_num),stat=error)
	IF(error /= 0) THEN
	  WRITE(911,*) 'allocate ramp_start error - insufficient memory'
	  STOP
	ENDIF
	ramp_start(:)=0.0
	
      ALLOCATE(ramp_end(dec_num),stat=error)
	IF(error /= 0) THEN
	  WRITE(911,*) 'allocate ramp_end error - insufficient memory'
	  STOP
	ENDIF
	ramp_end(:)=0.0
	
      ALLOCATE(detector(dec_num,7),stat=error)
	IF(error /= 0) THEN
	  WRITE(911,*) 'allocate detector error - insufficient memory'
	  STOP
	ENDIF
	detector(:,:)=0
	
      ALLOCATE(detector_length(dec_num),stat=error)
	IF(error /= 0) THEN
	  WRITE(911,*) 'allocate detector error - insufficient memory'
	  STOP
	ENDIF
	detector_length(:)=0
	
      ALLOCATE(detector_ramp(dec_num),stat=error)
	IF(error /= 0) THEN
	  WRITE(911,*) 'allocate detector_ramp error - insufficient memory'
	  STOP
	ENDIF
      detector_ramp(:)=0
     	
	ALLOCATE(det_link(dec_num),stat=error)
	IF(error /= 0) THEN
	  WRITE(911,*) 'allocate det_link error - insufficient memory'
	  STOP
	ENDIF
	det_link(:)=0
	
      ALLOCATE(occup(dec_num),stat=error)
	IF(error /= 0) THEN
	  WRITE(911,*) 'allocate occup error - insufficient memory'
	  STOP
	ENDIF
	occup(:)=0
      
	ELSE
	
      ALLOCATE(ramp_par(1,3),stat=error)
	IF(error /= 0) THEN
	  WRITE(911,*) 'allocate ramp_par error - insufficient memory'
	  STOP
	ENDIF
	ramp_par(:,:) = 0
	
      ALLOCATE(ramp_start(1),stat=error)
	IF(error /= 0) THEN
	  WRITE(911,*) 'allocate ramp_start error - insufficient memory'
	  STOP
	ENDIF
	ramp_start(:)=0.0

	ALLOCATE(ramp_end(1),stat=error)
	IF(error /= 0) THEN
	  WRITE(911,*) 'allocate ramp_end error - insufficient memory'
	  STOP
	ENDIF
	ramp_end(:)=0.0
	
      ALLOCATE(detector(1,7),stat=error)
	IF(error /= 0) THEN
	  WRITE(911,*) 'allocate detector error - insufficient memory'
	  STOP
	ENDIF
	detector(:,:)=0
	
      ALLOCATE(detector_length(1),stat=error)
	IF(error /= 0) THEN
	  WRITE(911,*) 'allocate detector_length error - insufficient memory'
	  STOP
	ENDIF
	detector_length(:)=0
	
      ALLOCATE(detector_ramp(1),stat=error)
	IF(error /= 0) THEN
	  WRITE(911,*) 'allocate detector_ramp error - insufficient memory'
	  STOP
	ENDIF
      detector_ramp(:)=0
     	
	ALLOCATE(det_link(1),stat=error)
	IF(error /= 0) THEN
	  WRITE(911,*) 'allocate det_link error - insufficient memory'
	  STOP
	ENDIF
	det_link(:)=0
	
      ALLOCATE(occup(1),stat=error)
	IF(error /= 0) THEN
	  WRITE(911,*) 'allocate occup error - insufficient memory'
	  STOP
	ENDIF
	occup(:)=0
	
      ENDIF
    
    IF(iteration == 0) THEN
	IF(inci_num > 0) THEN
  	ALLOCATE(incistartflag(inci_num),stat=error)
	IF(error /= 0) THEN
	  WRITE(911,*) 'allocate incistartflag error - insufficient memory'
	  STOP
	ENDIF
 	incistartflag(:) = .False.
      
    ALLOCATE(inci(inci_num,3),stat=error)
	IF(error /= 0) THEN
	  WRITE(911,*) 'allocate inci error - insufficient memory'
	  STOP
	ENDIF
	inci(:,:)=0
	
      ALLOCATE(incil(inci_num),stat=error)
	IF(error /= 0) THEN
	  WRITE(911,*) 'allocate incil error - insufficient memory'
	  STOP
	ENDIF
	incil(:)=0
	
    ALLOCATE(incilist(inci_num),stat=error)
	IF(error /= 0) THEN
	  WRITE(911,*) 'allocate incilist error - insufficient memory'
	  STOP
	ENDIF
	incilist(:)=0
	
    ALLOCATE(itp(inci_num),stat=error)
	IF(error /= 0) THEN
	  WRITE(911,*) 'allocate itp error - insufficient memory'
	  STOP
	ENDIF
	itp(:)=0
	ENDIF
	
      ELSE
    
    IF(iteration == 0) THEN
	ALLOCATE(incistartflag(1),stat=error)
	IF(error /= 0) THEN
	  WRITE(911,*) 'allocate incistartflag error - insufficient memory'
	  STOP
	ENDIF
 	incistartflag(:) = .False.
      
	ALLOCATE(inci(1,3),stat=error)
	IF(error /= 0) THEN
	  WRITE(911,*) 'allocate inci error - insufficient memory'
	  STOP
	ENDIF
	inci(:,:)=0
	
      ALLOCATE(incil(1),stat=error)
	IF(error /= 0) THEN
	  WRITE(911,*) 'allocate incil error - insufficient memory'
	  STOP
	ENDIF
	incil(:)=0
	
      ALLOCATE(incilist(1),stat=error)
	IF(error /= 0) THEN
	  WRITE(911,*) 'allocate incilist error - insufficient memory'
	  STOP
	ENDIF
	incilist(:)=0
	
      ALLOCATE(itp(1),stat=error)
	IF(error /= 0) THEN
	  WRITE(911,*) 'allocate itp error - insufficient memory'
	  STOP
	ENDIF
	itp(:)=0
	ENDIF
 	
      ENDIF

    IF(WorkZoneNum > 0) THEN
      ALLOCATE(WorkZone(WorkZoneNum),stat=error)
	IF(error /= 0) THEN
	  WRITE(911,*) 'allocate WorkZone error - insufficient memory'
	  STOP
	ENDIF
    WorkZone(:)%FNode=0
	WorkZone(:)%TNode=0
    WorkZone(:)%ST=0          ! starting time
	WorkZone(:)%ET=0          ! ending time
	WorkZone(:)%CapRed=0      ! percentage of capacity reduction
    WorkZone(:)%SpeedLmt=0    ! speed limit in work zone
	WorkZone(:)%Discharge=0   ! discharge rate at work zone
	WorkZone(:)%OrigDisChg=0

    allocate (wzstartflag(WorkZoneNum),stat=error)
	IF(error /= 0) THEN
	  WRITE(911,*) 'allocate wzstartflag error - insufficient memory'
	  STOP
	ENDIF
	wzstartflag(:) = .False.

      ELSE

      ALLOCATE(WorkZone(1),stat=error)
	IF(error /= 0) THEN
	  WRITE(911,*) 'allocate WorkZone error - insufficient memory'
	  STOP
	ENDIF
    WorkZone(:)%FNode=0
	WorkZone(:)%TNode=0
    WorkZone(:)%ST=0          ! starting time
	WorkZone(:)%ET=0          ! ending time
	WorkZone(:)%CapRed=0      ! percentage of capacity reduction
    WorkZone(:)%SpeedLmt=0    ! speed limit in work zone
	WorkZone(:)%Discharge=0   ! discharge rate at work zone
	WorkZone(:)%OrigDisChg=0

      allocate (wzstartflag(1),stat=error)
	IF(error /= 0) THEN
	  WRITE(911,*) 'allocate wzstartflag error - insufficient memory'
	  STOP
	ENDIF
	wzstartflag(:) = .False.

	ENDIF


 IF(iteration == 0) THEN

    allocate (destination(noof_master_destinations),stat=error)
	IF(error /= 0) THEN
	  WRITE(911,*) 'allocate destination error - insufficient memory'
	  STOP
	ENDIF
      destination(:)=0

      allocate (MasterDest(nzones),stat=error)
 	IF(error /= 0) THEN
	  WRITE(911,*) 'allocate destination error - insufficient memory'
	  STOP
	ENDIF
      MasterDest(:)=0
 ENDIF

	ALLOCATE(begint(nints+1),stat=error)
	IF(error /= 0) THEN
	  WRITE(911,*) 'allocate begint error - insufficient memory'
	  STOP
	ENDIF
	begint(:) = 0

	ALLOCATE(begintT(nintsT+1),stat=error)
	IF(error /= 0) THEN
	  WRITE(911,*) 'allocate begintT error - insufficient memory'
	  STOP
	ENDIF
	begintT(:) = 0

	ALLOCATE(begintH(nintsH+1),stat=error)
	IF(error /= 0) THEN
	  WRITE(911,*) 'allocate begintH error - insufficient memory'
	  STOP
	ENDIF
	begintH(:) = 0
	
      ALLOCATE(strtsig(isig+1),stat=error)
	IF(error /= 0) THEN
	  WRITE(911,*) 'allocate strtsig error - insufficient memory'
	  STOP
	ENDIF
	strtsig(:) = 0
      
	ALLOCATE(decisionnum(nu_switch+1),stat=error)
	IF(error /= 0) THEN
	  WRITE(911,*) 'allcoate decisionum error - insufficient memory'
	  STOP
	ENDIF
	decisionnum(:) = 0
	
      ALLOCATE(switchnum(nu_switch+1),stat=error)
	IF(error /= 0) THEN
	  WRITE(911,*) 'allocate switchnum error - insufficient memory'
	  STOP
	ENDIF
	switchnum(:) = 0

	ALLOCATE(LinkNoInZone(nzones,2000),stat=error)
	IF(error /= 0) THEN 
	  WRITE(911,*) 'allocate LinkNoInZone error - insufficient memory'
	  STOP
	ENDIF
	LinkNoInZone(:,:)=0

	ALLOCATE(NoofConsPerZone(nzones),stat=error)
	IF(error /= 0) THEN 
	  WRITE(911,*) 'allocate NoofConsPerZone error - insufficient memory'
	  STOP
	ENDIF
	NoofConsPerZone(:)=0

	ALLOCATE(ConNoInZone(nzones,1000),stat=error)
	IF(error /= 0) THEN 
	  WRITE(911,*) 'allocate ConNoInZone error - insufficient memory'
	  STOP
	ENDIF
	ConNoInZone(:,:)=0


      ALLOCATE(GradeBPnt(GradeNum),stat=error)
	IF(error /= 0) THEN 
	  WRITE(911,*) 'allocate GradeBPnt error - insufficient memory'
	  STOP
	ENDIF
      GradeBPnt(:)=0

      ALLOCATE(LengthBPnt(GradeNum,LenNum),stat=error)
	IF(error /= 0) THEN 
	  WRITE(911,*) 'allocate LengthBPnt error - insufficient memory'
	  STOP
	ENDIF
      LengthBPnt(:,:)=0

	ALLOCATE(TruckBPnt(TruckNum),stat=error)
	IF(error /= 0) THEN 
	  WRITE(911,*) 'allocate TruckBPnt error - insufficient memory'
	  STOP
	ENDIF
      TruckBPnt(:)=0

      ALLOCATE(PCE(GradeNum,LenNum,TruckNum),stat=error)      
	IF(error /= 0) THEN 
	  WRITE(911,*) 'allocate PCE error - insufficient memory'
	  STOP
	ENDIF
      PCE(:,:,:)=0

END SUBROUTINE

!~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>
SUBROUTINE MEMALLOCATE_ASSIGNMENT()
	ALLOCATE(totaltime(0:iteMax),stat=error)
	IF(error /= 0) THEN
	  WRITE(911,*) "allocate totaltime error - insufficient memory"
	  STOP
	ENDIF
	totaltime(:) = 0 
      
	ALLOCATE(TravelTime(noofarcs,aggint),stat=error)
	IF(error /= 0) THEN
	  WRITE(911,*) "allocate TravelTime error - insufficient memory"
	  STOP
	ENDIF
	TravelTime(:,:) = 0 
	
	ALLOCATE(TravelPenalty(noofarcs,aggint,maxmove),stat=error)
	IF(error /= 0) THEN
	  WRITE(911,*) "allocate TravelPenalty error - insufficient memory"
	  STOP
	ENDIF
	TravelPenalty(:,:,:) = 0

END SUBROUTINE

!~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>
 SUBROUTINE MEMALLOCATE_SP(dynust_mode_flag)
   INTEGER ksptime,dynust_mode_flag

	IF(dynust_mode_flag == 0) THEN !called from SIM_DYNUST
	 iti_nu = 1
	 ksptime = iti_nu
     noof_master_destinations_alc = 1
	ELSE ! called from UE
!	 iti_nu = nint((ProjPeriod/xminPerSimInt)/simPerAgg)
	 ksptime = iti_nu	 !called from mtc
	 noof_master_destinations_alc = 1
	ENDIF

    IF(logout > 0) WRITE(711,*) "MaxMove = ", MaxMove
	ALLOCATE(ttmarginal(ksptime,noofarcs,MaxMove),stat=error)
	IF(error /= 0) THEN
	  WRITE(911,*) "allocate ttmarginal error - insufficient memory"
	  WRITE(911,*) "ksptime, noofarcs, maxmove", ksptime, noofarcs, maxmove
	  STOP
	ENDIF
	ttmarginal(:,:,:) = 0

	ALLOCATE(TTime(noofarcs,ksptime),stat=error)
	IF(error /= 0) THEN
	  WRITE(911,*) "allocate TTime error - insufficient memory"
	  STOP
	ENDIF
	TTime(:,:) = 0
	
    ALLOCATE(TTpenalty(noofarcs,ksptime,MaxMove),stat=error)
	IF(error /= 0) THEN
	  WRITE(911,*) "allocate TTpenalty error - insufficient memory"
	  STOP
	ENDIF
	TTpenalty(:,:,:) = 0

END SUBROUTINE

!~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>
SUBROUTINE  DEALLOCATE_SIM

    CALL DEALLOCATE_dynust_veh()
    CALL DEALLOCATE_dynust_network_arcmove()
    CALL DEALLOCATE_dynust_network_arc()
    CALL DEALLOCATE_dynust_network_node()

	DEALLOCATE (ramp_par,stat=error)
	DEALLOCATE (ramp_start,stat=error)
	DEALLOCATE (ramp_end,stat=error)
	DEALLOCATE (detector,stat=error)
	DEALLOCATE (detector_length,stat=error)
	DEALLOCATE (detector_ramp,stat=error)
	DEALLOCATE (det_link,stat=error)
	DEALLOCATE (occup,stat=error)
	DEALLOCATE (p, stat=error)
    DEALLOCATE (rgtilnow,stat=error) 
	DEALLOCATE (NoofGenLinksPerZone,stat=error)
	DEALLOCATE (LinkNoInZone,stat=error)
	DEALLOCATE (NoofConsPerZone,stat=error)
    DEALLOCATE (ConNoInZone,stat=error)
	DEALLOCATE (zoneDemandProfile,stat=error)	
	DEALLOCATE (DemCell,stat=error)
	DEALLOCATE (DemCellT,stat=error)
	DEALLOCATE (DemCellH,stat=error)	
	DEALLOCATE (DemCellS,stat=error)		
	DEALLOCATE (bg_factor,stat=error)	
	DEALLOCATE (DemOrigAcu,stat=error)
	DEALLOCATE (DemOrigAcuT,stat=error)
	DEALLOCATE (DemOrigAcuH,stat=error)	
	DEALLOCATE (DemOrig,stat=error)
	DEALLOCATE (DemOrigT,stat=error)
	DEALLOCATE (DemOrigH,stat=error)	
	DEALLOCATE (DemDest,stat=error)
	DEALLOCATE (DemDestT,stat=error)
	DEALLOCATE (DemDestH,stat=error)	
	DEALLOCATE (TotalLinkLenPerZone,stat=error)
	DEALLOCATE (DemGenZ,stat=error)
	DEALLOCATE (DemGenZT,stat=error)
	DEALLOCATE (DemGenZH,stat=error)	
    DEALLOCATE (nsign,stat=error)
	DEALLOCATE (kgpoint,stat=error)
    DEALLOCATE (begint,stat=error)
    DEALLOCATE (begintT,stat=error)
    DEALLOCATE (begintH,stat=error)
	DEALLOCATE (strtsig,stat=error)
	DEALLOCATE (vmstype,stat=error)
	DEALLOCATE (vmstypetwopath,stat=error)
	DEALLOCATE (vms,stat=error)
	DEALLOCATE (vms_start,stat=error)
	DEALLOCATE (vms_end,stat=error)
    DEALLOCATE (decisionnum,stat=error)
	DEALLOCATE (switchnum,stat=error)
    DEALLOCATE (WorkZone,stat=error)
    DEALLOCATE (wzstartflag,stat=error)
	DEALLOCATE (STOPcap4w,stat=error)
	DEALLOCATE (STOPcap2w,stat=error)
    DEALLOCATE (STOPcap2wIND,stat=error)	
    DEALLOCATE (YieldCapIND,stat=error)	
	DEALLOCATE (YieldCap,stat=error)
    DEALLOCATE (VKFunc,stat=error)
    DEALLOCATE (VKFunc,stat=error)
    DEALLOCATE (GradeBPnt,stat=error)
    DEALLOCATE (LengthBPnt,stat=error)
	DEALLOCATE (TruckBPnt,stat=error)
    DEALLOCATE (PCE,stat=error)      
    DEALLOCATE (STOPcap2wIND,stat=error)	
    DEALLOCATE (YieldCapIND,stat=error)	
    DEALLOCATE(VehProfile)
    
    CALL TranLink_2DRemove
    CALL LinkAtt_2DRemove
    CALL LinkTemp_2DRemove    
    CALL EnQAtt_2DRemove
	CALL TChainAtt_2DRemove
!   CALL BusAtt_2DRemove
	CALL DESTROY_CONTROL_ARRAY
	CALL vehatt_2DRemove()
!	CALL BusAtt_2DRemove()

    IF(ALLOCATED(DestDemandOK)) DEALLOCATE(DestDemandOK)
    IF(ALLOCATED(SignData)) DEALLOCATE(SignData)
    IF(ALLOCATED(SignApprh)) DEALLOCATE(SignApprh) 
    IF(ALLOCATED(VehAin)) DEALLOCATE(VehAin)
    IF(ALLOCATED(VehLod)) DEALLOCATE(VehLod)    
    IF(ALLOCATED(VehLodtmp)) DEALLOCATE(VehLodtmp)        
    IF(ALLOCATED(sir)) DEALLOCATE(sir)
    IF(ALLOCATED(SigOptNid)) DEALLOCATE(SigOptNid)
    IF(ALLOCATED(SigOut)) DEALLOCATE(SigOut)    
    IF(ALLOCATED(nodeWVMS)) DEALLOCATE(nodeWVMS)
    IF(ALLOCATED(dstmp2)) DEALLOCATE(dstmp2)            
    IF(ALLOCATED(numActZone)) DEALLOCATE(numActZone)                
    IF(ALLOCATED(shelter)) DEALLOCATE(shelter)
    IF(ALLOCATED(vmsrate)) DEALLOCATE(vmsrate)
    IF(ALLOCATED(vmsnnode)) DEALLOCATE(vmsnnode)  
    
    IF(FuelOut == 1.and.(iteration == iteMax.or.reach_converg == .true.)) CALL DEALLOCATEFuel    
END SUBROUTINE

!~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>        
SUBROUTINE DEALLOCATE_SP1
 
      DEALLOCATE(TTime,stat=error)
      IF(error /= 0) THEN
        WRITE(911,*) "DEALLOCATE TTime error"
        STOP
      ENDIF
 
      DEALLOCATE(TTpenalty,stat=error)
      IF(error /= 0) THEN
        WRITE(911,*) "DEALLOCATE TTpenalty error"
        STOP
      ENDIF

      DEALLOCATE(ttmarginal,stat=error)
      IF(error /= 0) THEN
        WRITE(911,*) "DEALLOCATE ttmarginal error"
        STOP
      ENDIF

END SUBROUTINE


END MODULE