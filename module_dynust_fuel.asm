; mark_description "Intel(R) Visual Fortran Compiler XE for applications running on IA-32, Version 13.1.1.171 Build 20130313";
; mark_description "-S";
	.686P
 	.387
	OPTION DOTNAME
	ASSUME	CS:FLAT,DS:FLAT,SS:FLAT
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
TXTST0:
; -- Begin  _DYNUST_FUEL_MODULE$
; mark_begin;
IF @Version GE 800
  .MMX
ELSEIF @Version GE 612
  .MMX
  MMWORD TEXTEQU <QWORD>
ENDIF
IF @Version GE 800
  .XMM
ELSEIF @Version GE 614
  .XMM
  XMMWORD TEXTEQU <OWORD>
ENDIF
       ALIGN     16
	PUBLIC _DYNUST_FUEL_MODULE$
_DYNUST_FUEL_MODULE$	PROC NEAR 
.B1.1:                          ; Preds .B1.0
        ret                                                     ;1.8
        ALIGN     16
                                ; LOE
; mark_end;
_DYNUST_FUEL_MODULE$ ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_FUEL_MODULE$
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_FUEL_MODULE_mp_READNALLOCATE_FUEL
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_FUEL_MODULE_mp_READNALLOCATE_FUEL
_DYNUST_FUEL_MODULE_mp_READNALLOCATE_FUEL	PROC NEAR 
; parameter 1: 8 + ebp
.B2.1:                          ; Preds .B2.0
        push      ebp                                           ;67.12
        mov       ebp, esp                                      ;67.12
        and       esp, -16                                      ;67.12
        push      edi                                           ;67.12
        push      ebx                                           ;67.12
        sub       esp, 232                                      ;67.12
        mov       DWORD PTR [48+esp], 0                         ;77.12
        lea       eax, DWORD PTR [148+esp]                      ;77.12
        mov       DWORD PTR [80+esp], 12                        ;77.12
        lea       edi, DWORD PTR [48+esp]                       ;77.12
        mov       DWORD PTR [84+esp], OFFSET FLAT: __STRLITPACK_67 ;77.12
        lea       edx, DWORD PTR [80+esp]                       ;77.12
        mov       DWORD PTR [88+esp], eax                       ;77.12
        push      32                                            ;77.12
        push      edx                                           ;77.12
        push      OFFSET FLAT: __STRLITPACK_68.0.2              ;77.12
        push      -2088435968                                   ;77.12
        push      -1                                            ;77.12
        push      edi                                           ;77.12
        call      _for_inquire                                  ;77.12
                                ; LOE esi edi
.B2.69:                         ; Preds .B2.1
        add       esp, 24                                       ;77.12
                                ; LOE esi edi
.B2.2:                          ; Preds .B2.69
        test      BYTE PTR [148+esp], 1                         ;78.6
        je        .B2.51        ; Prob 60%                      ;78.6
                                ; LOE esi edi
.B2.3:                          ; Preds .B2.2
        mov       DWORD PTR [48+esp], 0                         ;80.5
        lea       eax, DWORD PTR [8+esp]                        ;80.5
        mov       DWORD PTR [8+esp], 12                         ;80.5
        mov       DWORD PTR [12+esp], OFFSET FLAT: __STRLITPACK_66 ;80.5
        mov       DWORD PTR [16+esp], 7                         ;80.5
        mov       DWORD PTR [20+esp], OFFSET FLAT: __STRLITPACK_65 ;80.5
        push      32                                            ;80.5
        push      eax                                           ;80.5
        push      OFFSET FLAT: __STRLITPACK_69.0.2              ;80.5
        push      -2088435968                                   ;80.5
        push      6058                                          ;80.5
        push      edi                                           ;80.5
        call      _for_open                                     ;80.5
                                ; LOE esi edi
.B2.4:                          ; Preds .B2.3
        mov       DWORD PTR [72+esp], 0                         ;82.5
        lea       eax, DWORD PTR [48+esp]                       ;82.5
        mov       DWORD PTR [48+esp], 12                        ;82.5
        mov       DWORD PTR [52+esp], OFFSET FLAT: __STRLITPACK_64 ;82.5
        mov       DWORD PTR [56+esp], 7                         ;82.5
        mov       DWORD PTR [60+esp], OFFSET FLAT: __STRLITPACK_63 ;82.5
        push      32                                            ;82.5
        push      eax                                           ;82.5
        push      OFFSET FLAT: __STRLITPACK_70.0.2              ;82.5
        push      -2088435968                                   ;82.5
        push      6060                                          ;82.5
        push      edi                                           ;82.5
        call      _for_open                                     ;82.5
                                ; LOE esi edi
.B2.5:                          ; Preds .B2.4
        mov       DWORD PTR [96+esp], 0                         ;85.5
        lea       eax, DWORD PTR [88+esp]                       ;85.5
        mov       DWORD PTR [88+esp], OFFSET FLAT: _DYNUST_FUEL_MODULE_mp_NUMFUEL ;85.5
        push      32                                            ;85.5
        push      eax                                           ;85.5
        push      OFFSET FLAT: __STRLITPACK_71.0.2              ;85.5
        push      -2088435968                                   ;85.5
        push      6058                                          ;85.5
        push      edi                                           ;85.5
        call      _for_read_seq_lis                             ;85.5
                                ; LOE esi edi
.B2.70:                         ; Preds .B2.5
        add       esp, 72                                       ;85.5
                                ; LOE esi edi
.B2.6:                          ; Preds .B2.70
        mov       eax, DWORD PTR [_DYNUST_FUEL_MODULE_mp_NUMFUEL] ;86.8
        test      eax, eax                                      ;86.19
        jle       .B2.54        ; Prob 79%                      ;86.19
                                ; LOE eax esi edi
.B2.7:                          ; Preds .B2.6
        mov       ebx, 0                                        ;87.7
        cmovle    eax, ebx                                      ;87.7
        mov       edx, 100                                      ;87.7
        mov       ecx, 1                                        ;87.7
        mov       DWORD PTR [_DYNUST_FUEL_MODULE_mp_FUELSET+16], ecx ;87.7
        mov       DWORD PTR [_DYNUST_FUEL_MODULE_mp_FUELSET+32], ecx ;87.7
        lea       ecx, DWORD PTR [4+esp]                        ;87.7
        push      edx                                           ;87.7
        push      eax                                           ;87.7
        push      2                                             ;87.7
        push      ecx                                           ;87.7
        mov       DWORD PTR [_DYNUST_FUEL_MODULE_mp_FUELSET+12], 133 ;87.7
        mov       DWORD PTR [_DYNUST_FUEL_MODULE_mp_FUELSET+4], edx ;87.7
        mov       DWORD PTR [_DYNUST_FUEL_MODULE_mp_FUELSET+8], ebx ;87.7
        mov       DWORD PTR [_DYNUST_FUEL_MODULE_mp_FUELSET+24], eax ;87.7
        mov       DWORD PTR [_DYNUST_FUEL_MODULE_mp_FUELSET+28], edx ;87.7
        call      _for_check_mult_overflow                      ;87.7
                                ; LOE eax esi edi
.B2.8:                          ; Preds .B2.7
        mov       edx, DWORD PTR [_DYNUST_FUEL_MODULE_mp_FUELSET+12] ;87.7
        and       eax, 1                                        ;87.7
        and       edx, 1                                        ;87.7
        shl       eax, 4                                        ;87.7
        add       edx, edx                                      ;87.7
        or        edx, eax                                      ;87.7
        or        edx, 262144                                   ;87.7
        push      edx                                           ;87.7
        push      OFFSET FLAT: _DYNUST_FUEL_MODULE_mp_FUELSET   ;87.7
        push      DWORD PTR [28+esp]                            ;87.7
        call      _for_alloc_allocatable                        ;87.7
                                ; LOE esi edi
.B2.72:                         ; Preds .B2.8
        add       esp, 28                                       ;87.7
                                ; LOE esi edi
.B2.9:                          ; Preds .B2.72
        mov       eax, DWORD PTR [_DYNUST_FUEL_MODULE_mp_FUELSET] ;87.7
        mov       edx, eax                                      ;87.7
        mov       ecx, DWORD PTR [4+esp]                        ;87.7
        add       ecx, eax                                      ;87.7
        cmp       eax, ecx                                      ;87.7
        jae       .B2.14        ; Prob 67%                      ;87.7
                                ; LOE eax edx esi edi
.B2.10:                         ; Preds .B2.9
        mov       DWORD PTR [esp], esi                          ;
        xor       ecx, ecx                                      ;
                                ; LOE eax edx ecx edi
.B2.11:                         ; Preds .B2.12 .B2.10
        add       eax, ecx                                      ;87.7
        mov       ebx, DWORD PTR [_DYNUST_FUEL_MODULE_mp_READNALLOCATE_FUEL$BLK..T1219_+96] ;87.7
        mov       DWORD PTR [96+eax], ebx                       ;87.7
        mov       ebx, 96                                       ;87.7
                                ; LOE eax edx ecx ebx edi
.B2.68:                         ; Preds .B2.68 .B2.11
        movsd     xmm0, QWORD PTR [_DYNUST_FUEL_MODULE_mp_READNALLOCATE_FUEL$BLK..T1219_-8+ebx] ;87.7
        movsd     QWORD PTR [-8+eax+ebx], xmm0                  ;87.7
        movsd     xmm1, QWORD PTR [_DYNUST_FUEL_MODULE_mp_READNALLOCATE_FUEL$BLK..T1219_-16+ebx] ;87.7
        movsd     QWORD PTR [-16+eax+ebx], xmm1                 ;87.7
        movsd     xmm2, QWORD PTR [_DYNUST_FUEL_MODULE_mp_READNALLOCATE_FUEL$BLK..T1219_-24+ebx] ;87.7
        movsd     QWORD PTR [-24+eax+ebx], xmm2                 ;87.7
        movsd     xmm3, QWORD PTR [_DYNUST_FUEL_MODULE_mp_READNALLOCATE_FUEL$BLK..T1219_-32+ebx] ;87.7
        movsd     QWORD PTR [-32+eax+ebx], xmm3                 ;87.7
        sub       ebx, 32                                       ;87.7
        jne       .B2.68        ; Prob 66%                      ;87.7
                                ; LOE eax edx ecx ebx edi
.B2.12:                         ; Preds .B2.68
        mov       ebx, DWORD PTR [4+esp]                        ;87.7
        add       edx, 100                                      ;87.7
        mov       eax, DWORD PTR [_DYNUST_FUEL_MODULE_mp_FUELSET] ;87.7
        add       ebx, eax                                      ;87.7
        add       ecx, 100                                      ;87.7
        cmp       edx, ebx                                      ;87.7
        jb        .B2.11        ; Prob 82%                      ;87.7
                                ; LOE eax edx ecx edi
.B2.13:                         ; Preds .B2.12
        mov       esi, DWORD PTR [esp]                          ;
                                ; LOE esi edi
.B2.14:                         ; Preds .B2.9 .B2.13
        mov       edx, DWORD PTR [_DYNUST_FUEL_MODULE_mp_NUMFUEL] ;88.7
        test      edx, edx                                      ;88.7
        jle       .B2.54        ; Prob 2%                       ;88.7
                                ; LOE edx esi edi
.B2.15:                         ; Preds .B2.14
        xor       eax, eax                                      ;88.7
        mov       DWORD PTR [132+esp], eax                      ;
        xor       ebx, ebx                                      ;
        mov       DWORD PTR [136+esp], edx                      ;
        pxor      xmm0, xmm0                                    ;95.9
        mov       DWORD PTR [esp], esi                          ;
                                ; LOE ebx
.B2.16:                         ; Preds .B2.49 .B2.15
        mov       DWORD PTR [48+esp], 0                         ;89.9
        lea       eax, DWORD PTR [116+esp]                      ;89.9
        mov       DWORD PTR [152+esp], eax                      ;89.9
        push      32                                            ;89.9
        lea       edx, DWORD PTR [156+esp]                      ;89.9
        push      edx                                           ;89.9
        push      OFFSET FLAT: __STRLITPACK_72.0.2              ;89.9
        push      -2088435968                                   ;89.9
        push      6058                                          ;89.9
        lea       ecx, DWORD PTR [68+esp]                       ;89.9
        push      ecx                                           ;89.9
        call      _for_read_seq_lis                             ;89.9
                                ; LOE ebx
.B2.17:                         ; Preds .B2.16
        imul      edx, DWORD PTR [_DYNUST_FUEL_MODULE_mp_FUELSET+32], -100 ;89.9
        lea       esi, DWORD PTR [184+esp]                      ;89.9
        mov       eax, DWORD PTR [_DYNUST_FUEL_MODULE_mp_FUELSET] ;89.9
        lea       ecx, DWORD PTR [100+eax+edx]                  ;89.9
        add       ecx, ebx                                      ;89.9
        mov       DWORD PTR [184+esp], ecx                      ;89.9
        push      esi                                           ;89.9
        push      OFFSET FLAT: __STRLITPACK_73.0.2              ;89.9
        lea       edi, DWORD PTR [80+esp]                       ;89.9
        push      edi                                           ;89.9
        call      _for_read_seq_lis_xmit                        ;89.9
                                ; LOE ebx
.B2.18:                         ; Preds .B2.17
        imul      edx, DWORD PTR [_DYNUST_FUEL_MODULE_mp_FUELSET+32], -100 ;89.9
        lea       esi, DWORD PTR [204+esp]                      ;89.9
        mov       eax, DWORD PTR [_DYNUST_FUEL_MODULE_mp_FUELSET] ;89.9
        lea       ecx, DWORD PTR [104+eax+edx]                  ;89.9
        add       ecx, ebx                                      ;89.9
        mov       DWORD PTR [204+esp], ecx                      ;89.9
        push      esi                                           ;89.9
        push      OFFSET FLAT: __STRLITPACK_74.0.2              ;89.9
        lea       edi, DWORD PTR [92+esp]                       ;89.9
        push      edi                                           ;89.9
        call      _for_read_seq_lis_xmit                        ;89.9
                                ; LOE ebx
.B2.19:                         ; Preds .B2.18
        imul      edx, DWORD PTR [_DYNUST_FUEL_MODULE_mp_FUELSET+32], -100 ;89.9
        lea       esi, DWORD PTR [224+esp]                      ;89.9
        mov       eax, DWORD PTR [_DYNUST_FUEL_MODULE_mp_FUELSET] ;89.9
        lea       ecx, DWORD PTR [108+eax+edx]                  ;89.9
        add       ecx, ebx                                      ;89.9
        mov       DWORD PTR [224+esp], ecx                      ;89.9
        push      esi                                           ;89.9
        push      OFFSET FLAT: __STRLITPACK_75.0.2              ;89.9
        lea       edi, DWORD PTR [104+esp]                      ;89.9
        push      edi                                           ;89.9
        call      _for_read_seq_lis_xmit                        ;89.9
                                ; LOE ebx
.B2.20:                         ; Preds .B2.19
        imul      edx, DWORD PTR [_DYNUST_FUEL_MODULE_mp_FUELSET+32], -100 ;89.9
        lea       esi, DWORD PTR [244+esp]                      ;89.9
        mov       eax, DWORD PTR [_DYNUST_FUEL_MODULE_mp_FUELSET] ;89.9
        lea       ecx, DWORD PTR [112+eax+edx]                  ;89.9
        add       ecx, ebx                                      ;89.9
        mov       DWORD PTR [244+esp], ecx                      ;89.9
        push      esi                                           ;89.9
        push      OFFSET FLAT: __STRLITPACK_76.0.2              ;89.9
        lea       edi, DWORD PTR [116+esp]                      ;89.9
        push      edi                                           ;89.9
        call      _for_read_seq_lis_xmit                        ;89.9
                                ; LOE ebx
.B2.21:                         ; Preds .B2.20
        imul      edx, DWORD PTR [_DYNUST_FUEL_MODULE_mp_FUELSET+32], -100 ;89.9
        lea       esi, DWORD PTR [264+esp]                      ;89.9
        mov       eax, DWORD PTR [_DYNUST_FUEL_MODULE_mp_FUELSET] ;89.9
        lea       ecx, DWORD PTR [116+eax+edx]                  ;89.9
        add       ecx, ebx                                      ;89.9
        mov       DWORD PTR [264+esp], ecx                      ;89.9
        push      esi                                           ;89.9
        push      OFFSET FLAT: __STRLITPACK_77.0.2              ;89.9
        lea       edi, DWORD PTR [128+esp]                      ;89.9
        push      edi                                           ;89.9
        call      _for_read_seq_lis_xmit                        ;89.9
                                ; LOE ebx
.B2.22:                         ; Preds .B2.21
        imul      edx, DWORD PTR [_DYNUST_FUEL_MODULE_mp_FUELSET+32], -100 ;90.9
        mov       eax, DWORD PTR [_DYNUST_FUEL_MODULE_mp_FUELSET] ;90.9
        mov       DWORD PTR [132+esp], 0                        ;90.9
        lea       ecx, DWORD PTR [120+eax+edx]                  ;90.9
        add       ecx, ebx                                      ;90.9
        mov       DWORD PTR [284+esp], ecx                      ;90.9
        push      32                                            ;90.9
        lea       esi, DWORD PTR [288+esp]                      ;90.9
        push      esi                                           ;90.9
        push      OFFSET FLAT: __STRLITPACK_78.0.2              ;90.9
        push      -2088435968                                   ;90.9
        push      6058                                          ;90.9
        lea       edi, DWORD PTR [152+esp]                      ;90.9
        push      edi                                           ;90.9
        call      _for_read_seq_lis                             ;90.9
                                ; LOE ebx
.B2.23:                         ; Preds .B2.22
        imul      edx, DWORD PTR [_DYNUST_FUEL_MODULE_mp_FUELSET+32], -100 ;91.9
        mov       edi, 1                                        ;91.9
        add       edx, ebx                                      ;91.9
        mov       eax, 8                                        ;91.9
        mov       ecx, DWORD PTR [_DYNUST_FUEL_MODULE_mp_FUELSET] ;91.9
        xor       esi, esi                                      ;91.9
        push      eax                                           ;91.9
        mov       DWORD PTR [180+ecx+edx], edi                  ;91.9
        mov       DWORD PTR [196+ecx+edx], edi                  ;91.9
        mov       edi, DWORD PTR [120+ecx+edx]                  ;91.9
        test      edi, edi                                      ;91.9
        mov       DWORD PTR [168+ecx+edx], eax                  ;91.9
        cmovl     edi, esi                                      ;91.9
        push      edi                                           ;91.9
        push      2                                             ;91.9
        mov       DWORD PTR [192+ecx+edx], eax                  ;91.9
        lea       eax, DWORD PTR [240+esp]                      ;91.9
        push      eax                                           ;91.9
        mov       DWORD PTR [176+ecx+edx], 5                    ;91.9
        mov       DWORD PTR [172+ecx+edx], esi                  ;91.9
        mov       DWORD PTR [188+ecx+edx], edi                  ;91.9
        call      _for_check_mult_overflow                      ;91.9
                                ; LOE eax ebx
.B2.73:                         ; Preds .B2.23
        add       esp, 124                                      ;91.9
                                ; LOE eax ebx
.B2.24:                         ; Preds .B2.73
        imul      ecx, DWORD PTR [_DYNUST_FUEL_MODULE_mp_FUELSET+32], -100 ;91.9
        and       eax, 1                                        ;91.9
        mov       edx, DWORD PTR [_DYNUST_FUEL_MODULE_mp_FUELSET] ;91.9
        shl       eax, 4                                        ;91.9
        or        eax, 262144                                   ;91.9
        push      eax                                           ;91.9
        lea       esi, DWORD PTR [164+edx+ecx]                  ;91.9
        add       esi, ebx                                      ;91.9
        push      esi                                           ;91.9
        push      DWORD PTR [128+esp]                           ;91.9
        call      _for_allocate                                 ;91.9
                                ; LOE ebx
.B2.74:                         ; Preds .B2.24
        add       esp, 12                                       ;91.9
                                ; LOE ebx
.B2.25:                         ; Preds .B2.74
        imul      eax, DWORD PTR [_DYNUST_FUEL_MODULE_mp_FUELSET+32], 100 ;92.9
        mov       ecx, ebx                                      ;92.9
        sub       ecx, eax                                      ;92.9
        mov       edx, DWORD PTR [_DYNUST_FUEL_MODULE_mp_FUELSET] ;92.9
        mov       DWORD PTR [128+esp], eax                      ;92.9
        mov       esi, DWORD PTR [196+edx+ecx]                  ;92.9
        mov       DWORD PTR [156+esp], esi                      ;92.9
        mov       esi, DWORD PTR [188+edx+ecx]                  ;92.9
        test      esi, esi                                      ;92.9
        jle       .B2.38        ; Prob 50%                      ;92.9
                                ; LOE edx ecx ebx esi
.B2.26:                         ; Preds .B2.25
        mov       eax, esi                                      ;92.9
        shr       eax, 31                                       ;92.9
        add       eax, esi                                      ;92.9
        sar       eax, 1                                        ;92.9
        mov       DWORD PTR [172+esp], eax                      ;92.9
        test      eax, eax                                      ;92.9
        jbe       .B2.56        ; Prob 10%                      ;92.9
                                ; LOE edx ecx ebx esi
.B2.27:                         ; Preds .B2.26
        mov       DWORD PTR [44+esp], esi                       ;
        mov       eax, DWORD PTR [192+edx+ecx]                  ;92.9
        mov       esi, DWORD PTR [156+esp]                      ;
        imul      esi, eax                                      ;
        mov       edi, DWORD PTR [164+edx+ecx]                  ;92.9
        sub       edi, esi                                      ;
        mov       DWORD PTR [112+esp], 0                        ;
        mov       DWORD PTR [104+esp], eax                      ;92.9
        xor       eax, eax                                      ;
        mov       DWORD PTR [92+esp], ecx                       ;
        mov       DWORD PTR [96+esp], edx                       ;
        mov       DWORD PTR [124+esp], ebx                      ;
        mov       edx, edi                                      ;
        mov       ebx, DWORD PTR [104+esp]                      ;
        mov       ecx, DWORD PTR [112+esp]                      ;
                                ; LOE eax edx ecx ebx
.B2.28:                         ; Preds .B2.28 .B2.27
        mov       esi, DWORD PTR [156+esp]                      ;92.9
        mov       edi, ebx                                      ;92.9
        lea       esi, DWORD PTR [esi+ecx*2]                    ;92.9
        inc       ecx                                           ;92.9
        imul      edi, esi                                      ;92.9
        inc       esi                                           ;92.9
        imul      esi, ebx                                      ;92.9
        mov       DWORD PTR [edx+edi], eax                      ;92.9
        mov       DWORD PTR [edx+esi], eax                      ;92.9
        cmp       ecx, DWORD PTR [172+esp]                      ;92.9
        jb        .B2.28        ; Prob 63%                      ;92.9
                                ; LOE eax edx ecx ebx
.B2.29:                         ; Preds .B2.28
        mov       DWORD PTR [112+esp], ecx                      ;
        mov       eax, ecx                                      ;92.9
        mov       esi, DWORD PTR [44+esp]                       ;
        mov       ecx, DWORD PTR [92+esp]                       ;
        mov       edx, DWORD PTR [96+esp]                       ;
        lea       edi, DWORD PTR [1+eax+eax]                    ;92.9
        mov       ebx, DWORD PTR [124+esp]                      ;
                                ; LOE edx ecx ebx esi edi
.B2.30:                         ; Preds .B2.29 .B2.56
        lea       eax, DWORD PTR [-1+edi]                       ;92.9
        cmp       esi, eax                                      ;92.9
        jbe       .B2.32        ; Prob 10%                      ;92.9
                                ; LOE edx ecx ebx esi edi
.B2.31:                         ; Preds .B2.30
        mov       eax, DWORD PTR [156+esp]                      ;92.9
        lea       edi, DWORD PTR [-1+edi+eax]                   ;92.9
        sub       edi, eax                                      ;92.9
        imul      edi, DWORD PTR [192+edx+ecx]                  ;92.9
        mov       eax, DWORD PTR [164+edx+ecx]                  ;92.9
        mov       DWORD PTR [eax+edi], 0                        ;92.9
                                ; LOE edx ecx ebx esi
.B2.32:                         ; Preds .B2.31 .B2.30
        mov       eax, esi                                      ;93.9
        shr       eax, 31                                       ;93.9
        add       eax, esi                                      ;93.9
        sar       eax, 1                                        ;93.9
        mov       DWORD PTR [164+esp], eax                      ;93.9
        test      eax, eax                                      ;93.9
        jbe       .B2.55        ; Prob 10%                      ;93.9
                                ; LOE edx ecx ebx esi
.B2.33:                         ; Preds .B2.32
        mov       DWORD PTR [44+esp], esi                       ;
        mov       eax, DWORD PTR [192+edx+ecx]                  ;93.9
        mov       esi, DWORD PTR [156+esp]                      ;
        imul      esi, eax                                      ;
        mov       edi, DWORD PTR [164+edx+ecx]                  ;93.9
        sub       edi, esi                                      ;
        mov       DWORD PTR [108+esp], 0                        ;
        mov       DWORD PTR [100+esp], eax                      ;93.9
        xor       eax, eax                                      ;
        mov       DWORD PTR [92+esp], ecx                       ;
        mov       DWORD PTR [96+esp], edx                       ;
        mov       DWORD PTR [124+esp], ebx                      ;
        mov       edx, edi                                      ;
        mov       ebx, DWORD PTR [100+esp]                      ;
        mov       ecx, DWORD PTR [108+esp]                      ;
                                ; LOE eax edx ecx ebx
.B2.34:                         ; Preds .B2.34 .B2.33
        mov       esi, DWORD PTR [156+esp]                      ;93.9
        mov       edi, ebx                                      ;93.9
        lea       esi, DWORD PTR [esi+ecx*2]                    ;93.9
        inc       ecx                                           ;93.9
        imul      edi, esi                                      ;93.9
        inc       esi                                           ;93.9
        imul      esi, ebx                                      ;93.9
        mov       DWORD PTR [4+edx+edi], eax                    ;93.9
        mov       DWORD PTR [4+edx+esi], eax                    ;93.9
        cmp       ecx, DWORD PTR [164+esp]                      ;93.9
        jb        .B2.34        ; Prob 63%                      ;93.9
                                ; LOE eax edx ecx ebx
.B2.35:                         ; Preds .B2.34
        mov       DWORD PTR [108+esp], ecx                      ;
        mov       eax, ecx                                      ;93.9
        mov       esi, DWORD PTR [44+esp]                       ;
        mov       ecx, DWORD PTR [92+esp]                       ;
        mov       edx, DWORD PTR [96+esp]                       ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;93.9
        mov       ebx, DWORD PTR [124+esp]                      ;
                                ; LOE eax edx ecx ebx esi
.B2.36:                         ; Preds .B2.35 .B2.55
        lea       edi, DWORD PTR [-1+eax]                       ;93.9
        cmp       esi, edi                                      ;93.9
        jbe       .B2.38        ; Prob 10%                      ;93.9
                                ; LOE eax edx ecx ebx
.B2.37:                         ; Preds .B2.36
        mov       esi, DWORD PTR [156+esp]                      ;93.9
        lea       edi, DWORD PTR [-1+eax+esi]                   ;93.9
        mov       eax, DWORD PTR [164+edx+ecx]                  ;93.9
        sub       edi, esi                                      ;93.9
        imul      edi, DWORD PTR [192+edx+ecx]                  ;93.9
        mov       DWORD PTR [4+eax+edi], 0                      ;93.9
                                ; LOE edx ecx ebx
.B2.38:                         ; Preds .B2.25 .B2.36 .B2.37
        mov       esi, 4                                        ;94.9
        mov       eax, 1                                        ;94.9
        mov       DWORD PTR [140+edx+ecx], 133                  ;94.9
        mov       DWORD PTR [132+edx+ecx], esi                  ;94.9
        mov       DWORD PTR [144+edx+ecx], eax                  ;94.9
        mov       DWORD PTR [136+edx+ecx], 0                    ;94.9
        mov       DWORD PTR [160+edx+ecx], eax                  ;94.9
        mov       DWORD PTR [152+edx+ecx], 1000000              ;94.9
        mov       DWORD PTR [156+edx+ecx], esi                  ;94.9
        sub       edx, DWORD PTR [128+esp]                      ;94.9
        push      262146                                        ;94.9
        lea       edx, DWORD PTR [128+edx+ebx]                  ;94.9
        push      edx                                           ;94.9
        push      4000000                                       ;94.9
        call      _for_alloc_allocatable                        ;94.9
                                ; LOE ebx
.B2.75:                         ; Preds .B2.38
        add       esp, 12                                       ;94.9
                                ; LOE ebx
.B2.39:                         ; Preds .B2.75
        mov       eax, DWORD PTR [_DYNUST_FUEL_MODULE_mp_FUELSET+32] ;95.9
        imul      edx, eax, -100                                ;95.9
        add       edx, ebx                                      ;95.9
        mov       esi, DWORD PTR [_DYNUST_FUEL_MODULE_mp_FUELSET] ;95.9
        mov       DWORD PTR [140+esp], eax                      ;95.9
        mov       edi, DWORD PTR [152+esi+edx]                  ;95.9
        test      edi, edi                                      ;95.9
        jle       .B2.42        ; Prob 50%                      ;95.9
                                ; LOE edx ebx esi edi
.B2.40:                         ; Preds .B2.39
        cmp       edi, 24                                       ;95.9
        jle       .B2.57        ; Prob 0%                       ;95.9
                                ; LOE edx ebx esi edi
.B2.41:                         ; Preds .B2.40
        shl       edi, 2                                        ;95.9
        push      edi                                           ;95.9
        push      0                                             ;95.9
        push      DWORD PTR [128+esi+edx]                       ;95.9
        call      __intel_fast_memset                           ;95.9
                                ; LOE ebx esi
.B2.76:                         ; Preds .B2.41
        add       esp, 12                                       ;95.9
                                ; LOE ebx esi
.B2.42:                         ; Preds .B2.63 .B2.76 .B2.39 .B2.61
        imul      edx, DWORD PTR [140+esp], -100                ;96.9
        add       edx, ebx                                      ;96.9
        add       edx, esi                                      ;96.9
        mov       eax, DWORD PTR [120+edx]                      ;96.9
        test      eax, eax                                      ;96.9
        jle       .B2.49        ; Prob 2%                       ;96.9
                                ; LOE eax edx ebx
.B2.43:                         ; Preds .B2.42
        mov       DWORD PTR [188+esp], eax                      ;96.9
        xor       esi, esi                                      ;96.9
        lea       edi, DWORD PTR [48+esp]                       ;96.9
                                ; LOE ebx esi edi
.B2.44:                         ; Preds .B2.47 .B2.43
        mov       DWORD PTR [48+esp], 0                         ;97.11
        lea       eax, DWORD PTR [180+esp]                      ;97.11
        mov       DWORD PTR [208+esp], eax                      ;97.11
        push      32                                            ;97.11
        lea       edx, DWORD PTR [212+esp]                      ;97.11
        push      edx                                           ;97.11
        push      OFFSET FLAT: __STRLITPACK_79.0.2              ;97.11
        push      -2088435968                                   ;97.11
        push      6058                                          ;97.11
        push      edi                                           ;97.11
        call      _for_read_seq_lis                             ;97.11
                                ; LOE ebx esi edi
.B2.45:                         ; Preds .B2.44
        imul      eax, DWORD PTR [_DYNUST_FUEL_MODULE_mp_FUELSET+32], -100 ;97.11
        inc       esi                                           ;97.11
        add       eax, DWORD PTR [_DYNUST_FUEL_MODULE_mp_FUELSET] ;97.11
        lea       ecx, DWORD PTR [240+esp]                      ;97.11
        mov       edx, DWORD PTR [196+ebx+eax]                  ;97.29
        neg       edx                                           ;97.11
        add       edx, esi                                      ;97.11
        imul      edx, DWORD PTR [192+ebx+eax]                  ;97.11
        add       edx, DWORD PTR [164+ebx+eax]                  ;97.11
        mov       DWORD PTR [240+esp], edx                      ;97.11
        push      ecx                                           ;97.11
        push      OFFSET FLAT: __STRLITPACK_80.0.2              ;97.11
        push      edi                                           ;97.11
        call      _for_read_seq_lis_xmit                        ;97.11
                                ; LOE ebx esi edi
.B2.46:                         ; Preds .B2.45
        imul      eax, DWORD PTR [_DYNUST_FUEL_MODULE_mp_FUELSET+32], -100 ;97.11
        add       eax, DWORD PTR [_DYNUST_FUEL_MODULE_mp_FUELSET] ;97.11
        mov       edx, DWORD PTR [196+ebx+eax]                  ;97.57
        neg       edx                                           ;97.11
        add       edx, esi                                      ;97.11
        imul      edx, DWORD PTR [192+ebx+eax]                  ;97.11
        mov       ecx, DWORD PTR [164+ebx+eax]                  ;97.57
        lea       eax, DWORD PTR [4+edx+ecx]                    ;97.11
        mov       DWORD PTR [260+esp], eax                      ;97.11
        lea       edx, DWORD PTR [260+esp]                      ;97.11
        push      edx                                           ;97.11
        push      OFFSET FLAT: __STRLITPACK_81.0.2              ;97.11
        push      edi                                           ;97.11
        call      _for_read_seq_lis_xmit                        ;97.11
                                ; LOE ebx esi edi
.B2.77:                         ; Preds .B2.46
        add       esp, 48                                       ;97.11
                                ; LOE ebx esi edi
.B2.47:                         ; Preds .B2.77
        cmp       esi, DWORD PTR [188+esp]                      ;96.9
        jb        .B2.44        ; Prob 82%                      ;96.9
                                ; LOE ebx esi edi
.B2.48:                         ; Preds .B2.47
        imul      edx, DWORD PTR [_DYNUST_FUEL_MODULE_mp_FUELSET+32], -100 ;
        add       edx, ebx                                      ;
        add       edx, DWORD PTR [_DYNUST_FUEL_MODULE_mp_FUELSET] ;
                                ; LOE edx ebx
.B2.49:                         ; Preds .B2.48 .B2.42
        mov       eax, DWORD PTR [132+esp]                      ;88.7
        add       ebx, 100                                      ;88.7
        inc       eax                                           ;88.7
        mov       DWORD PTR [124+edx], 1000000                  ;99.9
        mov       DWORD PTR [132+esp], eax                      ;88.7
        cmp       eax, DWORD PTR [136+esp]                      ;88.7
        jb        .B2.16        ; Prob 82%                      ;88.7
                                ; LOE ebx
.B2.50:                         ; Preds .B2.49
        mov       esi, DWORD PTR [esp]                          ;
        jmp       .B2.54        ; Prob 100%                     ;
                                ; LOE esi
.B2.51:                         ; Preds .B2.2
        mov       DWORD PTR [48+esp], 0                         ;127.6
        lea       eax, DWORD PTR [esp]                          ;127.6
        mov       DWORD PTR [esp], 71                           ;127.6
        mov       DWORD PTR [4+esp], OFFSET FLAT: __STRLITPACK_61 ;127.6
        push      32                                            ;127.6
        push      eax                                           ;127.6
        push      OFFSET FLAT: __STRLITPACK_82.0.2              ;127.6
        push      -2088435968                                   ;127.6
        push      911                                           ;127.6
        push      edi                                           ;127.6
        call      _for_write_seq_lis                            ;127.6
                                ; LOE esi edi
.B2.52:                         ; Preds .B2.51
        mov       DWORD PTR [72+esp], 0                         ;128.6
        lea       eax, DWORD PTR [32+esp]                       ;128.6
        mov       DWORD PTR [32+esp], 43                        ;128.6
        mov       DWORD PTR [36+esp], OFFSET FLAT: __STRLITPACK_59 ;128.6
        push      32                                            ;128.6
        push      eax                                           ;128.6
        push      OFFSET FLAT: __STRLITPACK_83.0.2              ;128.6
        push      -2088435968                                   ;128.6
        push      911                                           ;128.6
        push      edi                                           ;128.6
        call      _for_write_seq_lis                            ;128.6
                                ; LOE esi edi
.B2.53:                         ; Preds .B2.52
        mov       DWORD PTR [64+esp], 0                         ;128.6
        lea       eax, DWORD PTR [64+esp]                       ;128.6
        push      eax                                           ;128.6
        push      OFFSET FLAT: __STRLITPACK_84.0.2              ;128.6
        push      edi                                           ;128.6
        call      _for_write_seq_lis_xmit                       ;128.6
                                ; LOE esi
.B2.78:                         ; Preds .B2.53
        add       esp, 60                                       ;128.6
                                ; LOE esi
.B2.54:                         ; Preds .B2.6 .B2.14 .B2.50 .B2.78
        add       esp, 232                                      ;131.1
        pop       ebx                                           ;131.1
        pop       edi                                           ;131.1
        mov       esp, ebp                                      ;131.1
        pop       ebp                                           ;131.1
        ret                                                     ;131.1
                                ; LOE
.B2.55:                         ; Preds .B2.32                  ; Infreq
        mov       eax, 1                                        ;
        jmp       .B2.36        ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B2.56:                         ; Preds .B2.26                  ; Infreq
        mov       edi, 1                                        ;
        jmp       .B2.30        ; Prob 100%                     ;
                                ; LOE edx ecx ebx esi edi
.B2.57:                         ; Preds .B2.40                  ; Infreq
        cmp       edi, 8                                        ;95.9
        jl        .B2.65        ; Prob 10%                      ;95.9
                                ; LOE edx ebx esi edi
.B2.58:                         ; Preds .B2.57                  ; Infreq
        xor       eax, eax                                      ;95.9
        mov       ecx, edi                                      ;95.9
        mov       DWORD PTR [144+esp], eax                      ;95.9
        and       ecx, -8                                       ;95.9
        mov       DWORD PTR [124+esp], ebx                      ;95.9
        pxor      xmm0, xmm0                                    ;95.9
        mov       eax, DWORD PTR [128+esi+edx]                  ;95.9
        mov       ebx, DWORD PTR [144+esp]                      ;95.9
                                ; LOE eax edx ecx ebx esi edi xmm0
.B2.59:                         ; Preds .B2.59 .B2.58           ; Infreq
        movups    XMMWORD PTR [eax+ebx*4], xmm0                 ;95.9
        movups    XMMWORD PTR [16+eax+ebx*4], xmm0              ;95.9
        add       ebx, 8                                        ;95.9
        cmp       ebx, ecx                                      ;95.9
        jb        .B2.59        ; Prob 81%                      ;95.9
                                ; LOE eax edx ecx ebx esi edi xmm0
.B2.60:                         ; Preds .B2.59                  ; Infreq
        mov       ebx, DWORD PTR [124+esp]                      ;
                                ; LOE edx ecx ebx esi edi
.B2.61:                         ; Preds .B2.60 .B2.65           ; Infreq
        cmp       ecx, edi                                      ;95.9
        jae       .B2.42        ; Prob 10%                      ;95.9
                                ; LOE edx ecx ebx esi edi
.B2.62:                         ; Preds .B2.61                  ; Infreq
        mov       eax, DWORD PTR [128+esi+edx]                  ;95.9
        xor       edx, edx                                      ;95.9
                                ; LOE eax edx ecx ebx esi edi
.B2.63:                         ; Preds .B2.63 .B2.62           ; Infreq
        mov       DWORD PTR [eax+ecx*4], edx                    ;95.9
        inc       ecx                                           ;95.9
        cmp       ecx, edi                                      ;95.9
        jb        .B2.63        ; Prob 81%                      ;95.9
        jmp       .B2.42        ; Prob 100%                     ;95.9
                                ; LOE eax edx ecx ebx esi edi
.B2.65:                         ; Preds .B2.57                  ; Infreq
        xor       ecx, ecx                                      ;95.9
        jmp       .B2.61        ; Prob 100%                     ;95.9
        ALIGN     16
                                ; LOE edx ecx ebx esi edi
; mark_end;
_DYNUST_FUEL_MODULE_mp_READNALLOCATE_FUEL ENDP
_TEXT	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
__STRLITPACK_68.0.2	DB	56
	DB	4
	DB	13
	DB	0
	DB	16
	DB	3
	DB	27
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_82.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_83.0.2	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_84.0.2	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_69.0.2	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	26
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_70.0.2	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	26
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_71.0.2	DB	9
	DB	3
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_72.0.2	DB	9
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_73.0.2	DB	26
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_74.0.2	DB	26
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_75.0.2	DB	26
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_76.0.2	DB	26
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_77.0.2	DB	26
	DB	3
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_78.0.2	DB	9
	DB	3
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_79.0.2	DB	9
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_80.0.2	DB	26
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_81.0.2	DB	26
	DB	3
	DB	1
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_FUEL_MODULE_mp_READNALLOCATE_FUEL
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_FUEL_MODULE_mp_SERVEDDEMAND
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_FUEL_MODULE_mp_SERVEDDEMAND
_DYNUST_FUEL_MODULE_mp_SERVEDDEMAND	PROC NEAR 
.B3.1:                          ; Preds .B3.0
        push      ebp                                           ;134.12
        mov       ebp, esp                                      ;134.12
        and       esp, -16                                      ;134.12
        push      esi                                           ;134.12
        push      edi                                           ;134.12
        push      ebx                                           ;134.12
        sub       esp, 260                                      ;134.12
        xor       edi, edi                                      ;143.8
        mov       DWORD PTR [96+esp], edi                       ;143.8
        lea       ebx, DWORD PTR [96+esp]                       ;143.8
        push      32                                            ;143.8
        push      edi                                           ;143.8
        push      OFFSET FLAT: __STRLITPACK_85.0.3              ;143.8
        push      -2088435968                                   ;143.8
        push      6059                                          ;143.8
        push      ebx                                           ;143.8
        call      _for_close                                    ;143.8
                                ; LOE ebx edi
.B3.2:                          ; Preds .B3.1
        mov       DWORD PTR [120+esp], 0                        ;144.2
        lea       edx, DWORD PTR [24+esp]                       ;144.2
        mov       DWORD PTR [24+esp], 12                        ;144.2
        mov       DWORD PTR [28+esp], OFFSET FLAT: __STRLITPACK_58 ;144.2
        mov       DWORD PTR [32+esp], 3                         ;144.2
        mov       DWORD PTR [36+esp], OFFSET FLAT: __STRLITPACK_57 ;144.2
        push      32                                            ;144.2
        push      edx                                           ;144.2
        push      OFFSET FLAT: __STRLITPACK_86.0.3              ;144.2
        push      -2088435968                                   ;144.2
        push      6059                                          ;144.2
        push      ebx                                           ;144.2
        call      _for_open                                     ;144.2
                                ; LOE ebx edi
.B3.3:                          ; Preds .B3.2
        mov       DWORD PTR [144+esp], 0                        ;145.2
        lea       edx, DWORD PTR [64+esp]                       ;145.2
        mov       DWORD PTR [64+esp], 19                        ;145.2
        mov       DWORD PTR [68+esp], OFFSET FLAT: __STRLITPACK_56 ;145.2
        mov       DWORD PTR [72+esp], 7                         ;145.2
        mov       DWORD PTR [76+esp], OFFSET FLAT: __STRLITPACK_55 ;145.2
        push      32                                            ;145.2
        push      edx                                           ;145.2
        push      OFFSET FLAT: __STRLITPACK_87.0.3              ;145.2
        push      -2088435968                                   ;145.2
        push      60599                                         ;145.2
        push      ebx                                           ;145.2
        call      _for_open                                     ;145.2
                                ; LOE ebx edi
.B3.4:                          ; Preds .B3.3
        mov       DWORD PTR [168+esp], 0                        ;146.2
        lea       edx, DWORD PTR [104+esp]                      ;146.2
        mov       DWORD PTR [104+esp], 13                       ;146.2
        mov       DWORD PTR [108+esp], OFFSET FLAT: __STRLITPACK_54 ;146.2
        mov       DWORD PTR [112+esp], 7                        ;146.2
        mov       DWORD PTR [116+esp], OFFSET FLAT: __STRLITPACK_53 ;146.2
        push      32                                            ;146.2
        push      edx                                           ;146.2
        push      OFFSET FLAT: __STRLITPACK_88.0.3              ;146.2
        push      -2088435968                                   ;146.2
        push      6080                                          ;146.2
        push      ebx                                           ;146.2
        call      _for_open                                     ;146.2
                                ; LOE ebx edi
.B3.5:                          ; Preds .B3.4
        mov       DWORD PTR [192+esp], 0                        ;147.2
        lea       edx, DWORD PTR [144+esp]                      ;147.2
        mov       DWORD PTR [144+esp], 20                       ;147.2
        mov       DWORD PTR [148+esp], OFFSET FLAT: __STRLITPACK_52 ;147.2
        mov       DWORD PTR [152+esp], 7                        ;147.2
        mov       DWORD PTR [156+esp], OFFSET FLAT: __STRLITPACK_51 ;147.2
        push      32                                            ;147.2
        push      edx                                           ;147.2
        push      OFFSET FLAT: __STRLITPACK_89.0.3              ;147.2
        push      -2088435968                                   ;147.2
        push      6081                                          ;147.2
        push      ebx                                           ;147.2
        call      _for_open                                     ;147.2
                                ; LOE ebx edi
.B3.54:                         ; Preds .B3.5
        add       esp, 120                                      ;147.2
                                ; LOE ebx edi
.B3.6:                          ; Preds .B3.54
        mov       DWORD PTR [64+esp], edi                       ;149.16
        lea       edx, DWORD PTR [64+esp]                       ;149.16
        push      edi                                           ;149.16
        push      6059                                          ;149.16
        push      edx                                           ;149.16
        call      _for_eof                                      ;149.16
                                ; LOE eax ebx edi
.B3.55:                         ; Preds .B3.6
        add       esp, 12                                       ;149.16
                                ; LOE eax ebx edi
.B3.7:                          ; Preds .B3.55
        test      al, 1                                         ;149.16
        jne       .B3.33        ; Prob 6%                       ;149.16
                                ; LOE ebx edi
.B3.8:                          ; Preds .B3.7
        pxor      xmm0, xmm0                                    ;165.27
        lea       ecx, DWORD PTR [184+esp]                      ;165.27
                                ; LOE ebx
.B3.9:                          ; Preds .B3.31 .B3.8
        mov       DWORD PTR [96+esp], 0                         ;150.3
        lea       eax, DWORD PTR [140+esp]                      ;150.3
        mov       DWORD PTR [168+esp], eax                      ;150.3
        push      32                                            ;150.3
        lea       edx, DWORD PTR [172+esp]                      ;150.3
        push      edx                                           ;150.3
        push      OFFSET FLAT: __STRLITPACK_90.0.3              ;150.3
        push      -2088435968                                   ;150.3
        push      6059                                          ;150.3
        push      ebx                                           ;150.3
        call      _for_read_seq_lis                             ;150.3
                                ; LOE ebx
.B3.10:                         ; Preds .B3.9
        lea       edx, DWORD PTR [200+esp]                      ;150.3
        lea       eax, DWORD PTR [168+esp]                      ;150.3
        mov       DWORD PTR [200+esp], eax                      ;150.3
        push      edx                                           ;150.3
        push      OFFSET FLAT: __STRLITPACK_91.0.3              ;150.3
        push      ebx                                           ;150.3
        call      _for_read_seq_lis_xmit                        ;150.3
                                ; LOE ebx
.B3.11:                         ; Preds .B3.10
        lea       edx, DWORD PTR [220+esp]                      ;150.3
        lea       eax, DWORD PTR [184+esp]                      ;150.3
        mov       DWORD PTR [220+esp], eax                      ;150.3
        push      edx                                           ;150.3
        push      OFFSET FLAT: __STRLITPACK_92.0.3              ;150.3
        push      ebx                                           ;150.3
        call      _for_read_seq_lis_xmit                        ;150.3
                                ; LOE ebx
.B3.12:                         ; Preds .B3.11
        lea       edx, DWORD PTR [240+esp]                      ;150.3
        lea       eax, DWORD PTR [200+esp]                      ;150.3
        mov       DWORD PTR [240+esp], eax                      ;150.3
        push      edx                                           ;150.3
        push      OFFSET FLAT: __STRLITPACK_93.0.3              ;150.3
        push      ebx                                           ;150.3
        call      _for_read_seq_lis_xmit                        ;150.3
                                ; LOE ebx
.B3.13:                         ; Preds .B3.12
        lea       edx, DWORD PTR [260+esp]                      ;150.3
        lea       eax, DWORD PTR [216+esp]                      ;150.3
        mov       DWORD PTR [260+esp], eax                      ;150.3
        push      edx                                           ;150.3
        push      OFFSET FLAT: __STRLITPACK_94.0.3              ;150.3
        push      ebx                                           ;150.3
        call      _for_read_seq_lis_xmit                        ;150.3
                                ; LOE ebx
.B3.56:                         ; Preds .B3.13
        add       esp, 72                                       ;150.3
                                ; LOE ebx
.B3.14:                         ; Preds .B3.56
        mov       edx, DWORD PTR [_DYNUST_FUEL_MODULE_mp_NUMGASST] ;154.3
        test      edx, edx                                      ;154.3
        mov       eax, DWORD PTR [144+esp]                      ;174.3
        mov       DWORD PTR [172+esp], edx                      ;154.3
        mov       DWORD PTR [160+esp], eax                      ;174.3
        jle       .B3.51        ; Prob 2%                       ;154.3
                                ; LOE ebx
.B3.15:                         ; Preds .B3.14
        mov       edi, DWORD PTR [_DYNUST_FUEL_MODULE_mp_GASLOC+32] ;156.5
        xor       eax, eax                                      ;
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM+32] ;155.5
        neg       edx                                           ;155.5
        mov       ecx, DWORD PTR [_DYNUST_FUEL_MODULE_mp_GASLOC] ;156.5
        lea       esi, DWORD PTR [edi*4]                        ;
        shl       edi, 5                                        ;
        add       edx, DWORD PTR [160+esp]                      ;155.5
        sub       edi, esi                                      ;
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM] ;
        mov       DWORD PTR [128+esp], ecx                      ;156.5
        sub       ecx, edi                                      ;
        mov       DWORD PTR [136+esp], ecx                      ;
        mov       ecx, DWORD PTR [_DYNUST_FUEL_MODULE_mp_COORD+32] ;
        neg       ecx                                           ;
        add       ecx, DWORD PTR [esi+edx*4]                    ;
        xor       edx, edx                                      ;
        mov       esi, DWORD PTR [_DYNUST_FUEL_MODULE_mp_COORD] ;156.23
        mov       DWORD PTR [132+esp], edi                      ;
        lea       edi, DWORD PTR [ecx*8]                        ;
        mov       DWORD PTR [164+esp], esi                      ;
        lea       ecx, DWORD PTR [edi+ecx*4]                    ;
        movss     xmm0, DWORD PTR [4+esi+ecx]                   ;156.23
        movss     xmm1, DWORD PTR [_2il0floatpacket.1]          ;
        mov       esi, DWORD PTR [136+esp]                      ;
        mov       edi, eax                                      ;
                                ; LOE eax edx ecx esi edi xmm0 xmm1
.B3.16:                         ; Preds .B3.18 .B3.15
        movss     xmm2, DWORD PTR [36+edx+esi]                  ;156.8
        inc       edi                                           ;
        comiss    xmm2, xmm0                                    ;156.20
        jb        .B3.18        ; Prob 50%                      ;156.20
                                ; LOE eax edx ecx esi edi xmm0 xmm1 xmm2
.B3.17:                         ; Preds .B3.16
        mov       ebx, DWORD PTR [164+esp]                      ;157.48
        movaps    xmm4, xmm1                                    ;156.20
        movss     xmm3, DWORD PTR [32+edx+esi]                  ;157.20
        subss     xmm2, xmm0                                    ;157.81
        subss     xmm3, DWORD PTR [ebx+ecx]                     ;157.48
        mulss     xmm2, xmm2                                    ;157.81
        mulss     xmm3, xmm3                                    ;157.48
        addss     xmm3, xmm2                                    ;157.51
        movaps    xmm2, xmm1                                    ;159.9
        sqrtss    xmm3, xmm3                                    ;157.14
        comiss    xmm4, xmm3                                    ;160.9
        movaps    xmm1, xmm3                                    ;159.9
        cmova     eax, edi                                      ;160.9
        minss     xmm1, xmm2                                    ;159.9
                                ; LOE eax edx ecx esi edi xmm0 xmm1
.B3.18:                         ; Preds .B3.16 .B3.17
        add       edx, 28                                       ;156.23
        cmp       edi, DWORD PTR [172+esp]                      ;156.23
        jb        .B3.16        ; Prob 81%                      ;156.23
                                ; LOE eax edx ecx esi edi xmm0 xmm1
.B3.19:                         ; Preds .B3.18
        mov       edx, DWORD PTR [140+esp]                      ;166.51
        lea       ebx, DWORD PTR [96+esp]                       ;
        test      eax, eax                                      ;164.15
        jle       .B3.50        ; Prob 16%                      ;164.15
                                ; LOE eax edx ebx
.B3.20:                         ; Preds .B3.19
        lea       ecx, DWORD PTR [eax*4]                        ;165.27
        shl       eax, 5                                        ;165.27
        sub       eax, ecx                                      ;165.27
        pxor      xmm0, xmm0                                    ;165.27
        add       eax, DWORD PTR [128+esp]                      ;165.27
        sub       eax, DWORD PTR [132+esp]                      ;165.27
        movss     xmm1, DWORD PTR [16+eax]                      ;165.6
        comiss    xmm1, xmm0                                    ;165.27
        jbe       .B3.22        ; Prob 50%                      ;165.27
                                ; LOE eax edx ebx xmm1
.B3.21:                         ; Preds .B3.20
        mov       esi, DWORD PTR [_DYNUST_FUEL_MODULE_mp_VEHFUEL+32] ;166.28
        neg       esi                                           ;166.5
        add       esi, edx                                      ;166.5
        mov       edi, DWORD PTR [_DYNUST_FUEL_MODULE_mp_VEHFUEL] ;166.28
        inc       DWORD PTR [24+eax]                            ;168.5
        lea       ecx, DWORD PTR [esi*8]                        ;166.5
        lea       ecx, DWORD PTR [ecx+esi*4]                    ;166.5
        mov       esi, 1                                        ;
        subss     xmm1, DWORD PTR [edi+ecx]                     ;166.5
        movss     DWORD PTR [16+eax], xmm1                      ;166.5
        jmp       .B3.23        ; Prob 100%                     ;166.5
                                ; LOE eax edx ebx esi
.B3.22:                         ; Preds .B3.20
        xor       esi, esi                                      ;
                                ; LOE eax edx ebx esi
.B3.23:                         ; Preds .B3.21 .B3.22
        inc       DWORD PTR [20+eax]                            ;172.3
                                ; LOE edx ebx esi
.B3.24:                         ; Preds .B3.51 .B3.23 .B3.50
        mov       DWORD PTR [96+esp], 0                         ;174.3
        mov       DWORD PTR [208+esp], edx                      ;174.3
        push      32                                            ;174.3
        push      OFFSET FLAT: DYNUST_FUEL_MODULE_mp_SERVEDDEMAND$format_pack.0.3 ;174.3
        lea       eax, DWORD PTR [216+esp]                      ;174.3
        push      eax                                           ;174.3
        push      OFFSET FLAT: __STRLITPACK_95.0.3              ;174.3
        push      -2088435968                                   ;174.3
        push      60599                                         ;174.3
        push      ebx                                           ;174.3
        call      _for_write_seq_fmt                            ;174.3
                                ; LOE ebx esi
.B3.25:                         ; Preds .B3.24
        mov       eax, DWORD PTR [188+esp]                      ;174.3
        lea       edx, DWORD PTR [244+esp]                      ;174.3
        mov       DWORD PTR [244+esp], eax                      ;174.3
        push      edx                                           ;174.3
        push      OFFSET FLAT: __STRLITPACK_96.0.3              ;174.3
        push      ebx                                           ;174.3
        call      _for_write_seq_fmt_xmit                       ;174.3
                                ; LOE ebx esi
.B3.26:                         ; Preds .B3.25
        mov       eax, DWORD PTR [188+esp]                      ;174.3
        lea       edx, DWORD PTR [264+esp]                      ;174.3
        mov       DWORD PTR [264+esp], eax                      ;174.3
        push      edx                                           ;174.3
        push      OFFSET FLAT: __STRLITPACK_97.0.3              ;174.3
        push      ebx                                           ;174.3
        call      _for_write_seq_fmt_xmit                       ;174.3
                                ; LOE ebx esi
.B3.27:                         ; Preds .B3.26
        mov       eax, DWORD PTR [204+esp]                      ;174.3
        lea       edx, DWORD PTR [284+esp]                      ;174.3
        mov       DWORD PTR [284+esp], eax                      ;174.3
        push      edx                                           ;174.3
        push      OFFSET FLAT: __STRLITPACK_98.0.3              ;174.3
        push      ebx                                           ;174.3
        call      _for_write_seq_fmt_xmit                       ;174.3
                                ; LOE ebx esi
.B3.28:                         ; Preds .B3.27
        mov       eax, DWORD PTR [220+esp]                      ;174.3
        lea       edx, DWORD PTR [304+esp]                      ;174.3
        mov       DWORD PTR [304+esp], eax                      ;174.3
        push      edx                                           ;174.3
        push      OFFSET FLAT: __STRLITPACK_99.0.3              ;174.3
        push      ebx                                           ;174.3
        call      _for_write_seq_fmt_xmit                       ;174.3
                                ; LOE ebx esi
.B3.29:                         ; Preds .B3.28
        mov       DWORD PTR [324+esp], esi                      ;174.3
        lea       eax, DWORD PTR [324+esp]                      ;174.3
        push      eax                                           ;174.3
        push      OFFSET FLAT: __STRLITPACK_100.0.3             ;174.3
        push      ebx                                           ;174.3
        call      _for_write_seq_fmt_xmit                       ;174.3
                                ; LOE ebx
.B3.30:                         ; Preds .B3.29
        xor       eax, eax                                      ;149.16
        mov       DWORD PTR [152+esp], eax                      ;149.16
        push      eax                                           ;149.16
        push      6059                                          ;149.16
        lea       edx, DWORD PTR [160+esp]                      ;149.16
        push      edx                                           ;149.16
        call      _for_eof                                      ;149.16
                                ; LOE eax ebx
.B3.57:                         ; Preds .B3.30
        add       esp, 100                                      ;149.16
                                ; LOE eax ebx
.B3.31:                         ; Preds .B3.57
        test      al, 1                                         ;149.16
        je        .B3.9         ; Prob 82%                      ;149.16
                                ; LOE ebx
.B3.32:                         ; Preds .B3.31
        xor       edi, edi                                      ;
                                ; LOE ebx edi
.B3.33:                         ; Preds .B3.7 .B3.32
        mov       DWORD PTR [96+esp], edi                       ;177.2
        push      32                                            ;177.2
        push      OFFSET FLAT: DYNUST_FUEL_MODULE_mp_SERVEDDEMAND$format_pack.0.3+56 ;177.2
        push      edi                                           ;177.2
        push      OFFSET FLAT: __STRLITPACK_101.0.3             ;177.2
        push      -2088435968                                   ;177.2
        push      6080                                          ;177.2
        push      ebx                                           ;177.2
        call      _for_write_seq_fmt                            ;177.2
                                ; LOE ebx edi
.B3.58:                         ; Preds .B3.33
        add       esp, 28                                       ;177.2
                                ; LOE ebx edi
.B3.34:                         ; Preds .B3.58
        mov       esi, DWORD PTR [_DYNUST_FUEL_MODULE_mp_NUMGASST] ;177.2
        test      esi, esi                                      ;177.2
        jle       .B3.39        ; Prob 2%                       ;177.2
                                ; LOE ebx esi edi
.B3.35:                         ; Preds .B3.34
        imul      edx, DWORD PTR [_DYNUST_FUEL_MODULE_mp_GASLOC+32], -28 ;
        mov       ecx, 1                                        ;
        add       edx, DWORD PTR [_DYNUST_FUEL_MODULE_mp_GASLOC] ;
        mov       eax, 28                                       ;
        mov       DWORD PTR [128+esp], esi                      ;
        mov       esi, eax                                      ;
        mov       edi, edx                                      ;
        mov       ebx, ecx                                      ;
                                ; LOE ebx esi edi
.B3.36:                         ; Preds .B3.37 .B3.35
        mov       eax, DWORD PTR [20+esi+edi]                   ;177.2
        lea       edx, DWORD PTR [136+esp]                      ;177.2
        mov       DWORD PTR [136+esp], eax                      ;177.2
        push      edx                                           ;177.2
        push      OFFSET FLAT: __STRLITPACK_102.0.3             ;177.2
        lea       ecx, DWORD PTR [104+esp]                      ;177.2
        push      ecx                                           ;177.2
        call      _for_write_seq_fmt_xmit                       ;177.2
                                ; LOE ebx esi edi
.B3.59:                         ; Preds .B3.36
        add       esp, 12                                       ;177.2
                                ; LOE ebx esi edi
.B3.37:                         ; Preds .B3.59
        inc       ebx                                           ;177.2
        add       esi, 28                                       ;177.2
        cmp       ebx, DWORD PTR [128+esp]                      ;177.2
        jle       .B3.36        ; Prob 82%                      ;177.2
                                ; LOE ebx esi edi
.B3.38:                         ; Preds .B3.37
        mov       esi, DWORD PTR [128+esp]                      ;
        lea       ebx, DWORD PTR [96+esp]                       ;
        xor       edi, edi                                      ;
                                ; LOE ebx esi edi
.B3.39:                         ; Preds .B3.38 .B3.34
        push      0                                             ;177.2
        push      OFFSET FLAT: __STRLITPACK_103.0.3             ;177.2
        push      ebx                                           ;177.2
        call      _for_write_seq_fmt_xmit                       ;177.2
                                ; LOE ebx esi edi
.B3.40:                         ; Preds .B3.39
        mov       DWORD PTR [108+esp], edi                      ;178.2
        push      32                                            ;178.2
        push      OFFSET FLAT: DYNUST_FUEL_MODULE_mp_SERVEDDEMAND$format_pack.0.3+76 ;178.2
        push      edi                                           ;178.2
        push      OFFSET FLAT: __STRLITPACK_104.0.3             ;178.2
        push      -2088435968                                   ;178.2
        push      6081                                          ;178.2
        push      ebx                                           ;178.2
        call      _for_write_seq_fmt                            ;178.2
                                ; LOE ebx esi edi
.B3.60:                         ; Preds .B3.40
        add       esp, 40                                       ;178.2
                                ; LOE ebx esi edi
.B3.41:                         ; Preds .B3.60
        test      esi, esi                                      ;178.2
        jle       .B3.46        ; Prob 2%                       ;178.2
                                ; LOE ebx esi edi
.B3.42:                         ; Preds .B3.41
        imul      edx, DWORD PTR [_DYNUST_FUEL_MODULE_mp_GASLOC+32], -28 ;
        mov       ecx, 1                                        ;
        add       edx, DWORD PTR [_DYNUST_FUEL_MODULE_mp_GASLOC] ;
        mov       eax, 28                                       ;
        mov       DWORD PTR [128+esp], esi                      ;
        mov       esi, eax                                      ;
        mov       eax, ebx                                      ;
        mov       ebx, ecx                                      ;
        mov       edi, edx                                      ;
                                ; LOE eax ebx esi edi
.B3.43:                         ; Preds .B3.44 .B3.42
        mov       edx, DWORD PTR [24+esi+edi]                   ;178.2
        lea       ecx, DWORD PTR [160+esp]                      ;178.2
        mov       DWORD PTR [160+esp], edx                      ;178.2
        push      ecx                                           ;178.2
        push      OFFSET FLAT: __STRLITPACK_105.0.3             ;178.2
        push      eax                                           ;178.2
        call      _for_write_seq_fmt_xmit                       ;178.2
                                ; LOE ebx esi edi
.B3.61:                         ; Preds .B3.43
        lea       eax, DWORD PTR [108+esp]                      ;
        add       esp, 12                                       ;178.2
                                ; LOE eax ebx esi edi
.B3.44:                         ; Preds .B3.61
        inc       ebx                                           ;178.2
        add       esi, 28                                       ;178.2
        cmp       ebx, DWORD PTR [128+esp]                      ;178.2
        jle       .B3.43        ; Prob 82%                      ;178.2
                                ; LOE eax ebx esi edi
.B3.45:                         ; Preds .B3.44
        mov       ebx, eax                                      ;
        xor       edi, edi                                      ;
                                ; LOE ebx edi
.B3.46:                         ; Preds .B3.45 .B3.41
        push      0                                             ;178.2
        push      OFFSET FLAT: __STRLITPACK_106.0.3             ;178.2
        push      ebx                                           ;178.2
        call      _for_write_seq_fmt_xmit                       ;178.2
                                ; LOE ebx edi
.B3.47:                         ; Preds .B3.46
        mov       DWORD PTR [108+esp], edi                      ;180.8
        push      32                                            ;180.8
        push      edi                                           ;180.8
        push      OFFSET FLAT: __STRLITPACK_107.0.3             ;180.8
        push      -2088435968                                   ;180.8
        push      6059                                          ;180.8
        push      ebx                                           ;180.8
        call      _for_close                                    ;180.8
                                ; LOE ebx edi
.B3.48:                         ; Preds .B3.47
        mov       DWORD PTR [132+esp], edi                      ;181.8
        push      32                                            ;181.8
        push      edi                                           ;181.8
        push      OFFSET FLAT: __STRLITPACK_108.0.3             ;181.8
        push      -2088435968                                   ;181.8
        push      60599                                         ;181.8
        push      ebx                                           ;181.8
        call      _for_close                                    ;181.8
                                ; LOE
.B3.49:                         ; Preds .B3.48
        add       esp, 320                                      ;184.1
        pop       ebx                                           ;184.1
        pop       edi                                           ;184.1
        pop       esi                                           ;184.1
        mov       esp, ebp                                      ;184.1
        pop       ebp                                           ;184.1
        ret                                                     ;184.1
                                ; LOE
.B3.50:                         ; Preds .B3.19                  ; Infreq
        xor       esi, esi                                      ;
        jmp       .B3.24        ; Prob 100%                     ;
                                ; LOE edx ebx esi
.B3.51:                         ; Preds .B3.14                  ; Infreq
        mov       edx, DWORD PTR [140+esp]                      ;174.3
        xor       esi, esi                                      ;
        jmp       .B3.24        ; Prob 100%                     ;
        ALIGN     16
                                ; LOE edx ebx esi
; mark_end;
_DYNUST_FUEL_MODULE_mp_SERVEDDEMAND ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
DYNUST_FUEL_MODULE_mp_SERVEDDEMAND$format_pack.0.3	DB	54
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	2
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	3
	DB	2
	DB	0
	DB	0
	DB	0
	DB	10
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	1
	DB	1
	DB	0
	DB	0
	DB	0
	DB	8
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	5
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	-56
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	-56
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
_DATA	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_85.0.3	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_86.0.3	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	26
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_87.0.3	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	26
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_88.0.3	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	26
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_89.0.3	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	26
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_90.0.3	DB	9
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_91.0.3	DB	9
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_92.0.3	DB	26
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_93.0.3	DB	26
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_94.0.3	DB	26
	DB	3
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_95.0.3	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_96.0.3	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_97.0.3	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_98.0.3	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_99.0.3	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_100.0.3	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_101.0.3	DB	2
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_102.0.3	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_103.0.3	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_104.0.3	DB	2
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_105.0.3	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_106.0.3	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_107.0.3	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_108.0.3	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_FUEL_MODULE_mp_SERVEDDEMAND
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_FUEL_MODULE_mp_OUTPUTFUELDEMAND
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_FUEL_MODULE_mp_OUTPUTFUELDEMAND
_DYNUST_FUEL_MODULE_mp_OUTPUTFUELDEMAND	PROC NEAR 
; parameter 1: 48 + esp
; parameter 2: 52 + esp
; parameter 3: 56 + esp
.B4.1:                          ; Preds .B4.0
        push      esi                                           ;187.12
        push      edi                                           ;187.12
        push      ebp                                           ;187.12
        sub       esp, 32                                       ;187.12
        fld       DWORD PTR [_2il0floatpacket.12]               ;198.10
        mov       ecx, DWORD PTR [52+esp]                       ;187.12
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;197.14
        shl       eax, 8                                        ;197.5
        mov       ecx, DWORD PTR [ecx]                          ;197.14
        mov       edx, ecx                                      ;197.39
        neg       eax                                           ;197.5
        shl       edx, 8                                        ;197.39
        add       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;197.5
        mov       ebp, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;198.10
        neg       ebp                                           ;198.10
        add       ebp, ecx                                      ;198.10
        movss     xmm1, DWORD PTR [12+edx+eax]                  ;197.39
        movss     xmm0, DWORD PTR [_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT] ;197.39
        shl       ebp, 5                                        ;198.10
        movss     xmm2, DWORD PTR [240+edx+eax]                 ;197.14
        mov       eax, 1                                        ;198.10
        mulss     xmm0, xmm1                                    ;197.63
        mulss     xmm1, DWORD PTR [_2il0floatpacket.11]         ;198.10
        minss     xmm2, xmm0                                    ;197.5
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;198.10
        movss     DWORD PTR [12+esp], xmm1                      ;198.10
        fld       DWORD PTR [12+esp]                            ;198.10
        movsx     edx, BYTE PTR [5+edx+ebp]                     ;198.10
        cmp       edx, 3                                        ;198.10
        fst       DWORD PTR [28+esp]                            ;198.10
        cmove     edx, eax                                      ;198.10
L5:                                                             ;198.10
        fprem                                                   ;198.10
        fnstsw    ax                                            ;198.10
        sahf                                                    ;198.10
        jp        L5            ; Prob 90%                      ;198.10
        fstp      st(1)                                         ;198.10
                                ; LOE edx ecx ebx f1 xmm2
.B4.4:                          ; Preds .B4.1
        fld       DWORD PTR [_2il0floatpacket.12]               ;198.10
        fxch      st(1)                                         ;198.10
        fstp      DWORD PTR [12+esp]                            ;198.10
        fld       DWORD PTR [12+esp]                            ;198.10
        fdiv      st, st(1)                                     ;198.10
        fld       DWORD PTR [28+esp]                            ;198.10
        fdivrp    st(2), st                                     ;198.10
        fcom      DWORD PTR [_2il0floatpacket.15]               ;198.10
        fnstsw    ax                                            ;198.10
        fld       DWORD PTR [_2il0floatpacket.14]               ;198.10
        fld       DWORD PTR [_2il0floatpacket.13]               ;198.10
        sahf                                                    ;198.10
        mov       edi, DWORD PTR [_DYNUST_FUEL_MODULE_mp_VEHFUEL+32] ;198.10
        fxch      st(1)                                         ;198.10
        ja        L6            ; Prob 50%                      ;198.10
        fst       st(1)                                         ;198.10
L6:                                                             ;
        fstp      st(0)                                         ;198.10
        neg       edi                                           ;198.10
        faddp     st(1), st                                     ;198.10
        fnstcw    [esp]                                         ;198.10
        movzx     eax, WORD PTR [esp]                           ;198.10
        or        eax, 3072                                     ;198.10
        mov       DWORD PTR [8+esp], eax                        ;198.10
        fldcw     [8+esp]                                       ;198.10
        frndint                                                 ;198.10
        fldcw     [esp]                                         ;198.10
        add       edi, ecx                                      ;198.10
        fstp      DWORD PTR [20+esp]                            ;198.10
        movss     xmm0, DWORD PTR [20+esp]                      ;198.10
        fstp      DWORD PTR [20+esp]                            ;198.10
        mov       eax, DWORD PTR [_DYNUST_FUEL_MODULE_mp_FUELSET+32] ;198.10
        lea       esi, DWORD PTR [edi*8]                        ;198.10
        movss     xmm1, DWORD PTR [20+esp]                      ;198.10
        neg       eax                                           ;198.10
        add       eax, edx                                      ;198.10
        lea       ebp, DWORD PTR [esi+edi*4]                    ;198.10
        cvtss2si  edx, xmm0                                     ;198.10
        cvttss2si esi, xmm1                                     ;198.10
        imul      eax, eax, 100                                 ;198.10
        mov       edi, DWORD PTR [_DYNUST_FUEL_MODULE_mp_FUELSET] ;198.10
        add       edx, esi                                      ;198.10
        mov       ecx, DWORD PTR [_DYNUST_FUEL_MODULE_mp_VEHFUEL] ;198.10
        sub       edx, DWORD PTR [96+edi+eax]                   ;198.10
        imul      edx, DWORD PTR [92+edi+eax]                   ;198.10
        movss     xmm3, DWORD PTR [4+ecx+ebp]                   ;198.10
        mov       esi, DWORD PTR [64+edi+eax]                   ;198.10
        divss     xmm2, DWORD PTR [4+esi+edx]                   ;198.10
        subss     xmm3, xmm2                                    ;198.10
        movss     DWORD PTR [4+ecx+ebp], xmm3                   ;198.10
        add       esp, 32                                       ;199.1
        pop       ebp                                           ;199.1
        pop       edi                                           ;199.1
        pop       esi                                           ;199.1
        ret                                                     ;199.1
        ALIGN     16
                                ; LOE
; mark_end;
_DYNUST_FUEL_MODULE_mp_OUTPUTFUELDEMAND ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_FUEL_MODULE_mp_OUTPUTFUELDEMAND
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_FUEL_MODULE_mp_RANNOR
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_FUEL_MODULE_mp_RANNOR
_DYNUST_FUEL_MODULE_mp_RANNOR	PROC NEAR 
; parameter 1: 84 + esp
.B5.1:                          ; Preds .B5.0
        push      esi                                           ;202.15
        push      edi                                           ;202.15
        push      ebx                                           ;202.15
        push      ebp                                           ;202.15
        sub       esp, 64                                       ;202.15
        mov       ebx, DWORD PTR [84+esp]                       ;202.15
        mov       edx, DWORD PTR [_DYNUST_FUEL_MODULE_mp_FUELSET+32] ;207.2
        mov       eax, DWORD PTR [.T1401_.0.5+32]               ;205.21
        imul      ebx, DWORD PTR [ebx], 100                     ;207.23
        mov       DWORD PTR [56+esp], edx                       ;207.2
        imul      edx, edx, -100                                ;207.23
        mov       DWORD PTR [esp], eax                          ;205.21
        mov       eax, DWORD PTR [_DYNUST_FUEL_MODULE_mp_FUELSET] ;207.2
        mov       ecx, DWORD PTR [.T1401_.0.5]                  ;205.21
        mov       DWORD PTR [48+esp], ecx                       ;205.21
        mov       edi, DWORD PTR [.T1401_.0.5+4]                ;205.21
        mov       esi, DWORD PTR [.T1401_.0.5+12]               ;205.21
        mov       ecx, DWORD PTR [.T1401_.0.5+20]               ;205.21
        mov       DWORD PTR [60+esp], eax                       ;207.2
        add       eax, ebx                                      ;207.23
        mov       DWORD PTR [4+esp], edi                        ;205.21
        mov       DWORD PTR [52+esp], esi                       ;205.21
        mov       DWORD PTR [32+esp], ecx                       ;205.21
        mov       edi, DWORD PTR [.T1401_.0.5+8]                ;205.21
        mov       esi, DWORD PTR [.T1401_.0.5+16]               ;205.21
        mov       ebp, DWORD PTR [.T1401_.0.5+24]               ;205.21
        mov       ecx, DWORD PTR [.T1401_.0.5+28]               ;205.21
        cmp       DWORD PTR [24+edx+eax], 1000000               ;207.23
        je        .B5.7         ; Prob 1%                       ;207.23
                                ; LOE ecx ebx ebp esi edi
.B5.2:                          ; Preds .B5.1
        mov       eax, DWORD PTR [esp]                          ;208.4
        mov       edx, DWORD PTR [4+esp]                        ;205.21
        mov       DWORD PTR [44+esp], eax                       ;208.4
        mov       DWORD PTR [40+esp], ecx                       ;208.4
        mov       DWORD PTR [36+esp], ebp                       ;208.4
        mov       DWORD PTR [28+esp], esi                       ;205.21
        mov       DWORD PTR [20+esp], edi                       ;205.21
        mov       DWORD PTR [16+esp], edx                       ;205.21
                                ; LOE ebx
.B5.3:                          ; Preds .B5.17 .B5.2
        imul      eax, DWORD PTR [56+esp], -100                 ;218.2
        add       ebx, DWORD PTR [60+esp]                       ;218.2
        mov       edx, DWORD PTR [24+eax+ebx]                   ;218.22
        inc       edx                                           ;218.2
        mov       DWORD PTR [24+eax+ebx], edx                   ;218.2
        sub       edx, DWORD PTR [60+eax+ebx]                   ;219.2
        mov       ebx, DWORD PTR [28+eax+ebx]                   ;219.11
        test      BYTE PTR [52+esp], 1                          ;220.1
        fld       DWORD PTR [ebx+edx*4]                         ;219.2
        jne       .B5.5         ; Prob 3%                       ;220.1
                                ; LOE f1
.B5.4:                          ; Preds .B5.3
        add       esp, 64                                       ;220.1
        pop       ebp                                           ;220.1
        pop       ebx                                           ;220.1
        pop       edi                                           ;220.1
        pop       esi                                           ;220.1
        ret                                                     ;220.1
                                ; LOE
.B5.5:                          ; Preds .B5.3                   ; Infreq
        mov       eax, DWORD PTR [52+esp]                       ;205.21
        mov       edx, eax                                      ;220.1
        shr       edx, 1                                        ;220.1
        mov       DWORD PTR [24+esp], eax                       ;205.21
        and       edx, 1                                        ;220.1
        and       eax, 1                                        ;220.1
        shl       edx, 2                                        ;220.1
        add       eax, eax                                      ;220.1
        or        edx, eax                                      ;220.1
        mov       ecx, DWORD PTR [48+esp]                       ;205.21
        or        edx, 262144                                   ;220.1
        mov       DWORD PTR [12+esp], ecx                       ;205.21
        push      edx                                           ;220.1
        push      ecx                                           ;220.1
        fstp      DWORD PTR [8+esp]                             ;220.1
        call      _for_dealloc_allocatable                      ;220.1
                                ; LOE
.B5.20:                         ; Preds .B5.5                   ; Infreq
        fld       DWORD PTR [8+esp]                             ;
        add       esp, 8                                        ;220.1
                                ; LOE f1
.B5.6:                          ; Preds .B5.20                  ; Infreq
        mov       eax, DWORD PTR [52+esp]                       ;220.1
        and       eax, -2                                       ;220.1
        mov       DWORD PTR [12+esp], 0                         ;220.1
        mov       DWORD PTR [24+esp], eax                       ;220.1
        add       esp, 64                                       ;220.1
        pop       ebp                                           ;220.1
        pop       ebx                                           ;220.1
        pop       edi                                           ;220.1
        pop       esi                                           ;220.1
        ret                                                     ;220.1
                                ; LOE f1
.B5.7:                          ; Preds .B5.1                   ; Infreq
        mov       eax, DWORD PTR [48+esp]                       ;205.21
        mov       ecx, 4                                        ;208.4
        mov       edx, 1                                        ;208.4
        lea       ebx, DWORD PTR [12+esp]                       ;208.4
        mov       DWORD PTR [12+esp], eax                       ;205.21
        mov       DWORD PTR [24+esp], 133                       ;208.4
        mov       DWORD PTR [16+esp], ecx                       ;208.4
        mov       DWORD PTR [28+esp], edx                       ;208.4
        mov       DWORD PTR [20+esp], 0                         ;208.4
        mov       DWORD PTR [44+esp], edx                       ;208.4
        mov       DWORD PTR [36+esp], 1000000                   ;208.4
        mov       DWORD PTR [40+esp], ecx                       ;208.4
        push      262146                                        ;208.4
        push      ebx                                           ;208.4
        push      4000000                                       ;208.4
        call      _for_alloc_allocatable                        ;208.4
                                ; LOE
.B5.8:                          ; Preds .B5.7                   ; Infreq
        push      OFFSET FLAT: _DYNUST_MAIN_MODULE_mp_ISTRM     ;210.9
        inc       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ISTRM]      ;209.4
        call      _RNSET                                        ;210.9
                                ; LOE
.B5.9:                          ; Preds .B5.8                   ; Infreq
        push      DWORD PTR [28+esp]                            ;211.9
        push      OFFSET FLAT: __NLITPACK_0.0.5                 ;211.9
        call      _RNNOA                                        ;211.9
                                ; LOE
.B5.10:                         ; Preds .B5.9                   ; Infreq
        mov       eax, DWORD PTR [108+esp]                      ;212.9
        mov       edx, DWORD PTR [_DYNUST_FUEL_MODULE_mp_FUELSET+32] ;212.9
        neg       edx                                           ;212.9
        add       edx, DWORD PTR [eax]                          ;212.9
        imul      ecx, edx, 100                                 ;212.9
        mov       ebx, DWORD PTR [_DYNUST_FUEL_MODULE_mp_FUELSET] ;212.9
        push      OFFSET FLAT: __NLITPACK_1.0.5                 ;212.9
        push      DWORD PTR [40+esp]                            ;212.9
        lea       ebp, DWORD PTR [4+ecx+ebx]                    ;212.9
        push      ebp                                           ;212.9
        push      OFFSET FLAT: __NLITPACK_0.0.5                 ;212.9
        call      _SSCAL                                        ;212.9
                                ; LOE
.B5.11:                         ; Preds .B5.10                  ; Infreq
        mov       eax, DWORD PTR [124+esp]                      ;213.9
        mov       edx, DWORD PTR [_DYNUST_FUEL_MODULE_mp_FUELSET+32] ;213.9
        neg       edx                                           ;213.9
        add       edx, DWORD PTR [eax]                          ;213.9
        imul      ecx, edx, 100                                 ;213.9
        push      OFFSET FLAT: __NLITPACK_1.0.5                 ;213.9
        add       ecx, DWORD PTR [_DYNUST_FUEL_MODULE_mp_FUELSET] ;213.9
        push      DWORD PTR [56+esp]                            ;213.9
        push      ecx                                           ;213.9
        push      OFFSET FLAT: __NLITPACK_0.0.5                 ;213.9
        call      _SADD                                         ;213.9
                                ; LOE
.B5.21:                         ; Preds .B5.11                  ; Infreq
        add       esp, 56                                       ;213.9
                                ; LOE
.B5.12:                         ; Preds .B5.21                  ; Infreq
        mov       ebx, DWORD PTR [84+esp]                       ;214.4
        mov       edx, DWORD PTR [_DYNUST_FUEL_MODULE_mp_FUELSET+32] ;214.4
        imul      edi, edx, 100                                 ;214.4
        imul      ebx, DWORD PTR [ebx], 100                     ;214.4
        mov       ecx, DWORD PTR [_DYNUST_FUEL_MODULE_mp_FUELSET] ;214.4
        mov       DWORD PTR [56+esp], edx                       ;214.4
        mov       esi, DWORD PTR [44+esp]                       ;214.4
        mov       edx, DWORD PTR [12+esp]                       ;216.4
        lea       eax, DWORD PTR [ecx+ebx]                      ;214.4
        sub       eax, edi                                      ;214.4
        neg       edi                                           ;
        add       edi, ecx                                      ;
        mov       DWORD PTR [60+esp], ecx                       ;214.4
        mov       ebp, DWORD PTR [60+eax]                       ;214.4
        mov       DWORD PTR [4+esp], ebp                        ;214.4
        lea       ebp, DWORD PTR [edi+ebx]                      ;
        cmp       DWORD PTR [52+eax], 0                         ;214.4
        jle       .B5.16        ; Prob 10%                      ;214.4
                                ; LOE edx ebx ebp esi edi
.B5.13:                         ; Preds .B5.12                  ; Infreq
        mov       DWORD PTR [esp], edx                          ;
        lea       eax, DWORD PTR [esi*4]                        ;
        neg       eax                                           ;
        mov       ecx, 1                                        ;
        add       eax, edx                                      ;
        mov       DWORD PTR [8+esp], eax                        ;
        mov       eax, edi                                      ;
        mov       edx, DWORD PTR [4+esp]                        ;
                                ; LOE eax edx ecx ebp esi
.B5.14:                         ; Preds .B5.14 .B5.13           ; Infreq
        mov       ebx, DWORD PTR [60+ebp]                       ;214.4
        inc       ecx                                           ;214.4
        neg       ebx                                           ;214.4
        mov       edi, DWORD PTR [8+esp]                        ;214.4
        add       ebx, edx                                      ;214.4
        mov       ebp, DWORD PTR [28+ebp]                       ;214.4
        inc       edx                                           ;214.4
        mov       edi, DWORD PTR [edi+esi*4]                    ;214.4
        inc       esi                                           ;214.4
        mov       DWORD PTR [ebp+ebx*4], edi                    ;214.4
        mov       ebx, DWORD PTR [84+esp]                       ;214.4
        imul      ebx, DWORD PTR [ebx], 100                     ;214.4
        lea       ebp, DWORD PTR [eax+ebx]                      ;214.4
        cmp       ecx, DWORD PTR [52+ebp]                       ;214.4
        jle       .B5.14        ; Prob 82%                      ;214.4
                                ; LOE eax edx ecx ebx ebp esi
.B5.15:                         ; Preds .B5.14                  ; Infreq
        mov       edx, DWORD PTR [esp]                          ;
                                ; LOE edx ebx ebp
.B5.16:                         ; Preds .B5.12 .B5.15           ; Infreq
        mov       eax, DWORD PTR [24+esp]                       ;216.4
        mov       ecx, eax                                      ;216.4
        shr       ecx, 1                                        ;216.4
        mov       DWORD PTR [52+esp], eax                       ;216.4
        and       ecx, 1                                        ;216.4
        and       eax, 1                                        ;216.4
        shl       ecx, 2                                        ;216.4
        add       eax, eax                                      ;216.4
        or        ecx, eax                                      ;216.4
        or        ecx, 262144                                   ;216.4
        push      ecx                                           ;216.4
        push      edx                                           ;216.4
        mov       DWORD PTR [24+ebp], 0                         ;215.4
        call      _for_dealloc_allocatable                      ;216.4
                                ; LOE ebx
.B5.22:                         ; Preds .B5.16                  ; Infreq
        add       esp, 8                                        ;216.4
                                ; LOE ebx
.B5.17:                         ; Preds .B5.22                  ; Infreq
        mov       eax, DWORD PTR [52+esp]                       ;216.4
        and       eax, -2                                       ;216.4
        mov       DWORD PTR [48+esp], 0                         ;216.4
        mov       DWORD PTR [52+esp], eax                       ;216.4
        jmp       .B5.3         ; Prob 100%                     ;216.4
        ALIGN     16
                                ; LOE ebx
; mark_end;
_DYNUST_FUEL_MODULE_mp_RANNOR ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_FUEL_MODULE_mp_RANNOR
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_FUEL_MODULE_mp_SETFUELLEVEL
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_FUEL_MODULE_mp_SETFUELLEVEL
_DYNUST_FUEL_MODULE_mp_SETFUELLEVEL	PROC NEAR 
; parameter 1: 8 + ebp
.B6.1:                          ; Preds .B6.0
        push      ebp                                           ;223.12
        mov       ebp, esp                                      ;223.12
        and       esp, -16                                      ;223.12
        push      esi                                           ;223.12
        push      edi                                           ;223.12
        push      ebx                                           ;223.12
        sub       esp, 84                                       ;223.12
        mov       esi, 1                                        ;228.15
        mov       ecx, DWORD PTR [8+ebp]                        ;223.12
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;227.2
        mov       ebx, DWORD PTR [.T1401_.0.5+16]               ;229.49
        mov       eax, DWORD PTR [ecx]                          ;227.8
        mov       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;227.8
        neg       ecx                                           ;227.2
        add       ecx, eax                                      ;227.2
        shl       ecx, 5                                        ;227.2
        mov       DWORD PTR [esp], ebx                          ;229.49
        mov       ebx, DWORD PTR [_DYNUST_FUEL_MODULE_mp_FUELSET] ;229.49
        mov       DWORD PTR [60+esp], eax                       ;227.8
        movsx     edi, BYTE PTR [5+edx+ecx]                     ;227.8
        cmp       edi, 3                                        ;228.15
        mov       eax, DWORD PTR [.T1401_.0.5]                  ;229.49
        cmove     edi, esi                                      ;228.15
        imul      edi, edi, 100                                 ;229.49
        mov       DWORD PTR [68+esp], edi                       ;229.49
        add       ebx, edi                                      ;229.49
        imul      edi, DWORD PTR [_DYNUST_FUEL_MODULE_mp_FUELSET+32], -100 ;229.49
        add       ebx, edi                                      ;229.49
        mov       edx, DWORD PTR [.T1401_.0.5+12]               ;229.49
        mov       DWORD PTR [20+esp], eax                       ;229.49
        mov       DWORD PTR [64+esp], edx                       ;229.49
        mov       eax, DWORD PTR [.T1401_.0.5+20]               ;229.49
        mov       edx, DWORD PTR [.T1401_.0.5+24]               ;229.49
        mov       edi, DWORD PTR [24+ebx]                       ;229.49
        cmp       edi, 1000000                                  ;229.49
        mov       DWORD PTR [44+esp], eax                       ;229.49
        mov       DWORD PTR [4+esp], edx                        ;229.49
        mov       esi, DWORD PTR [.T1401_.0.5+4]                ;229.49
        mov       ecx, DWORD PTR [.T1401_.0.5+8]                ;229.49
        mov       edx, DWORD PTR [.T1401_.0.5+28]               ;229.49
        mov       eax, DWORD PTR [.T1401_.0.5+32]               ;229.49
        mov       DWORD PTR [72+esp], edi                       ;229.49
        je        .B6.7         ; Prob 1%                       ;229.49
                                ; LOE eax edx ecx ebx esi
.B6.2:                          ; Preds .B6.1
        mov       DWORD PTR [56+esp], eax                       ;229.49
        mov       DWORD PTR [52+esp], edx                       ;229.49
        mov       eax, DWORD PTR [4+esp]                        ;229.49
        mov       edx, DWORD PTR [esp]                          ;229.49
        mov       DWORD PTR [48+esp], eax                       ;229.49
        mov       DWORD PTR [40+esp], edx                       ;229.49
        mov       DWORD PTR [32+esp], ecx                       ;229.49
        mov       DWORD PTR [28+esp], esi                       ;229.49
                                ; LOE ebx
.B6.3:                          ; Preds .B6.16 .B6.2
        mov       edx, DWORD PTR [72+esp]                       ;229.49
        inc       edx                                           ;229.49
        mov       DWORD PTR [24+ebx], edx                       ;229.49
        sub       edx, DWORD PTR [60+ebx]                       ;229.49
        mov       eax, DWORD PTR [28+ebx]                       ;229.49
        test      BYTE PTR [64+esp], 1                          ;229.49
        movss     xmm2, DWORD PTR [eax+edx*4]                   ;229.49
        jne       .B6.5         ; Prob 3%                       ;229.49
                                ; LOE ebx xmm2
.B6.4:                          ; Preds .B6.3 .B6.6
        movss     xmm0, DWORD PTR [8+ebx]                       ;229.30
        mov       edx, DWORD PTR [60+esp]                       ;229.2
        maxss     xmm0, xmm2                                    ;230.2
        sub       edx, DWORD PTR [_DYNUST_FUEL_MODULE_mp_VEHFUEL+32] ;229.2
        movss     xmm1, DWORD PTR [12+ebx]                      ;230.30
        mov       ecx, DWORD PTR [_DYNUST_FUEL_MODULE_mp_VEHFUEL] ;229.26
        minss     xmm1, xmm0                                    ;230.2
        lea       eax, DWORD PTR [edx*8]                        ;229.2
        lea       esi, DWORD PTR [eax+edx*4]                    ;229.2
        movss     DWORD PTR [ecx+esi], xmm1                     ;230.2
        movss     DWORD PTR [4+ecx+esi], xmm1                   ;231.2
        add       esp, 84                                       ;232.1
        pop       ebx                                           ;232.1
        pop       edi                                           ;232.1
        pop       esi                                           ;232.1
        mov       esp, ebp                                      ;232.1
        pop       ebp                                           ;232.1
        ret                                                     ;232.1
                                ; LOE
.B6.5:                          ; Preds .B6.3                   ; Infreq
        mov       eax, DWORD PTR [64+esp]                       ;229.49
        mov       edx, eax                                      ;229.49
        shr       edx, 1                                        ;229.49
        mov       DWORD PTR [36+esp], eax                       ;229.49
        and       edx, 1                                        ;229.49
        and       eax, 1                                        ;229.49
        shl       edx, 2                                        ;229.49
        add       eax, eax                                      ;229.49
        or        edx, eax                                      ;229.49
        mov       ecx, DWORD PTR [20+esp]                       ;229.49
        or        edx, 262144                                   ;229.49
        mov       DWORD PTR [24+esp], ecx                       ;229.49
        push      edx                                           ;229.49
        push      ecx                                           ;229.49
        movss     DWORD PTR [8+esp], xmm2                       ;229.49
        call      _for_dealloc_allocatable                      ;229.49
                                ; LOE ebx
.B6.28:                         ; Preds .B6.5                   ; Infreq
        movss     xmm2, DWORD PTR [8+esp]                       ;
        add       esp, 8                                        ;229.49
                                ; LOE ebx xmm2
.B6.6:                          ; Preds .B6.28                  ; Infreq
        mov       eax, DWORD PTR [64+esp]                       ;229.49
        and       eax, -2                                       ;229.49
        mov       DWORD PTR [24+esp], 0                         ;229.49
        mov       DWORD PTR [36+esp], eax                       ;229.49
        jmp       .B6.4         ; Prob 100%                     ;229.49
                                ; LOE ebx xmm2
.B6.7:                          ; Preds .B6.1                   ; Infreq
        mov       eax, DWORD PTR [20+esp]                       ;229.49
        mov       ecx, 4                                        ;229.49
        mov       edx, 1                                        ;229.49
        lea       ebx, DWORD PTR [24+esp]                       ;229.49
        mov       DWORD PTR [24+esp], eax                       ;229.49
        mov       DWORD PTR [36+esp], 133                       ;229.49
        mov       DWORD PTR [28+esp], ecx                       ;229.49
        mov       DWORD PTR [40+esp], edx                       ;229.49
        mov       DWORD PTR [32+esp], 0                         ;229.49
        mov       DWORD PTR [56+esp], edx                       ;229.49
        mov       DWORD PTR [48+esp], 1000000                   ;229.49
        mov       DWORD PTR [52+esp], ecx                       ;229.49
        push      262146                                        ;229.49
        push      ebx                                           ;229.49
        push      4000000                                       ;229.49
        call      _for_alloc_allocatable                        ;229.49
                                ; LOE
.B6.8:                          ; Preds .B6.7                   ; Infreq
        push      OFFSET FLAT: _DYNUST_MAIN_MODULE_mp_ISTRM     ;229.49
        inc       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ISTRM]      ;229.49
        call      _RNSET                                        ;229.49
                                ; LOE
.B6.9:                          ; Preds .B6.8                   ; Infreq
        push      DWORD PTR [40+esp]                            ;229.49
        push      OFFSET FLAT: __NLITPACK_0.0.5                 ;229.49
        call      _RNNOA                                        ;229.49
                                ; LOE
.B6.10:                         ; Preds .B6.9                   ; Infreq
        imul      eax, DWORD PTR [_DYNUST_FUEL_MODULE_mp_FUELSET+32], -100 ;229.49
        add       eax, DWORD PTR [_DYNUST_FUEL_MODULE_mp_FUELSET] ;229.49
        mov       edx, DWORD PTR [92+esp]                       ;229.49
        push      OFFSET FLAT: __NLITPACK_1.0.5                 ;229.49
        push      DWORD PTR [52+esp]                            ;229.49
        lea       ecx, DWORD PTR [4+eax+edx]                    ;229.49
        push      ecx                                           ;229.49
        push      OFFSET FLAT: __NLITPACK_0.0.5                 ;229.49
        call      _SSCAL                                        ;229.49
                                ; LOE
.B6.11:                         ; Preds .B6.10                  ; Infreq
        imul      eax, DWORD PTR [_DYNUST_FUEL_MODULE_mp_FUELSET+32], -100 ;229.49
        add       eax, DWORD PTR [_DYNUST_FUEL_MODULE_mp_FUELSET] ;229.49
        add       eax, DWORD PTR [108+esp]                      ;229.49
        push      OFFSET FLAT: __NLITPACK_1.0.5                 ;229.49
        push      DWORD PTR [68+esp]                            ;229.49
        push      eax                                           ;229.49
        push      OFFSET FLAT: __NLITPACK_0.0.5                 ;229.49
        call      _SADD                                         ;229.49
                                ; LOE
.B6.29:                         ; Preds .B6.11                  ; Infreq
        add       esp, 56                                       ;229.49
                                ; LOE
.B6.12:                         ; Preds .B6.29                  ; Infreq
        mov       ecx, DWORD PTR [_DYNUST_FUEL_MODULE_mp_FUELSET] ;229.49
        mov       eax, DWORD PTR [68+esp]                       ;229.49
        mov       ebx, DWORD PTR [_DYNUST_FUEL_MODULE_mp_FUELSET+32] ;229.49
        mov       DWORD PTR [12+esp], ecx                       ;229.49
        mov       edx, DWORD PTR [24+esp]                       ;229.49
        add       eax, ecx                                      ;229.49
        imul      ecx, ebx, -100                                ;229.49
        mov       esi, DWORD PTR [52+ecx+eax]                   ;229.49
        test      esi, esi                                      ;229.49
        mov       DWORD PTR [16+esp], edx                       ;229.49
        jle       .B6.15        ; Prob 10%                      ;229.49
                                ; LOE eax ecx ebx esi
.B6.13:                         ; Preds .B6.12                  ; Infreq
        cmp       esi, 24                                       ;229.49
        jle       .B6.17        ; Prob 0%                       ;229.49
                                ; LOE eax ecx ebx esi
.B6.14:                         ; Preds .B6.13                  ; Infreq
        shl       esi, 2                                        ;229.49
        push      esi                                           ;229.49
        push      DWORD PTR [20+esp]                            ;229.49
        push      DWORD PTR [28+ecx+eax]                        ;229.49
        call      __intel_fast_memcpy                           ;229.49
                                ; LOE ebx
.B6.30:                         ; Preds .B6.14                  ; Infreq
        add       esp, 12                                       ;229.49
                                ; LOE ebx
.B6.15:                         ; Preds .B6.12 .B6.23 .B6.21 .B6.30 ; Infreq
        imul      ebx, ebx, -100                                ;229.49
        mov       edx, DWORD PTR [36+esp]                       ;229.49
        mov       ecx, edx                                      ;229.49
        mov       eax, DWORD PTR [68+esp]                       ;229.49
        shr       ecx, 1                                        ;229.49
        add       eax, DWORD PTR [12+esp]                       ;229.49
        and       ecx, 1                                        ;229.49
        mov       DWORD PTR [64+esp], edx                       ;229.49
        and       edx, 1                                        ;229.49
        add       ebx, eax                                      ;229.49
        add       edx, edx                                      ;229.49
        shl       ecx, 2                                        ;229.49
        or        ecx, edx                                      ;229.49
        or        ecx, 262144                                   ;229.49
        mov       DWORD PTR [72+esp], 0                         ;229.49
        mov       DWORD PTR [24+ebx], 0                         ;229.49
        push      ecx                                           ;229.49
        push      DWORD PTR [20+esp]                            ;229.49
        call      _for_dealloc_allocatable                      ;229.49
                                ; LOE ebx
.B6.31:                         ; Preds .B6.15                  ; Infreq
        add       esp, 8                                        ;229.49
                                ; LOE ebx
.B6.16:                         ; Preds .B6.31                  ; Infreq
        mov       edx, DWORD PTR [8+ebp]                        ;229.2
        mov       eax, DWORD PTR [64+esp]                       ;229.49
        and       eax, -2                                       ;229.49
        mov       ecx, DWORD PTR [edx]                          ;229.2
        mov       DWORD PTR [20+esp], 0                         ;229.49
        mov       DWORD PTR [64+esp], eax                       ;229.49
        mov       DWORD PTR [60+esp], ecx                       ;229.2
        jmp       .B6.3         ; Prob 100%                     ;229.2
                                ; LOE ebx
.B6.17:                         ; Preds .B6.13                  ; Infreq
        cmp       esi, 4                                        ;229.49
        jl        .B6.25        ; Prob 10%                      ;229.49
                                ; LOE eax ecx ebx esi
.B6.18:                         ; Preds .B6.17                  ; Infreq
        mov       edi, DWORD PTR [28+ecx+eax]                   ;229.49
        mov       edx, esi                                      ;229.49
        mov       DWORD PTR [8+esp], 0                          ;229.49
        and       edx, -4                                       ;229.49
        mov       DWORD PTR [esp], eax                          ;229.49
        mov       DWORD PTR [4+esp], ebx                        ;229.49
        mov       eax, edi                                      ;229.49
        mov       ebx, DWORD PTR [8+esp]                        ;229.49
        mov       edi, DWORD PTR [16+esp]                       ;229.49
                                ; LOE eax edx ecx ebx esi edi
.B6.19:                         ; Preds .B6.19 .B6.18           ; Infreq
        movups    xmm0, XMMWORD PTR [edi+ebx*4]                 ;229.49
        movups    XMMWORD PTR [eax+ebx*4], xmm0                 ;229.49
        add       ebx, 4                                        ;229.49
        cmp       ebx, edx                                      ;229.49
        jb        .B6.19        ; Prob 82%                      ;229.49
                                ; LOE eax edx ecx ebx esi edi
.B6.20:                         ; Preds .B6.19                  ; Infreq
        mov       eax, DWORD PTR [esp]                          ;
        mov       ebx, DWORD PTR [4+esp]                        ;
                                ; LOE eax edx ecx ebx esi
.B6.21:                         ; Preds .B6.20 .B6.25           ; Infreq
        cmp       edx, esi                                      ;229.49
        jae       .B6.15        ; Prob 0%                       ;229.49
                                ; LOE eax edx ecx ebx esi
.B6.22:                         ; Preds .B6.21                  ; Infreq
        mov       eax, DWORD PTR [28+ecx+eax]                   ;229.49
        mov       edi, DWORD PTR [16+esp]                       ;229.49
                                ; LOE eax edx ebx esi edi
.B6.23:                         ; Preds .B6.23 .B6.22           ; Infreq
        mov       ecx, DWORD PTR [edi+edx*4]                    ;229.49
        mov       DWORD PTR [eax+edx*4], ecx                    ;229.49
        inc       edx                                           ;229.49
        cmp       edx, esi                                      ;229.49
        jb        .B6.23        ; Prob 82%                      ;229.49
        jmp       .B6.15        ; Prob 100%                     ;229.49
                                ; LOE eax edx ebx esi edi
.B6.25:                         ; Preds .B6.17                  ; Infreq
        xor       edx, edx                                      ;229.49
        jmp       .B6.21        ; Prob 100%                     ;229.49
        ALIGN     16
                                ; LOE eax edx ecx ebx esi
; mark_end;
_DYNUST_FUEL_MODULE_mp_SETFUELLEVEL ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_FUEL_MODULE_mp_SETFUELLEVEL
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_FUEL_MODULE_mp_UPDATEFUELLEVEL
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_FUEL_MODULE_mp_UPDATEFUELLEVEL
_DYNUST_FUEL_MODULE_mp_UPDATEFUELLEVEL	PROC NEAR 
; parameter 1: 52 + esp
; parameter 2: 56 + esp
; parameter 3: 60 + esp
.B7.1:                          ; Preds .B7.0
        push      esi                                           ;235.12
        push      edi                                           ;235.12
        push      ebx                                           ;235.12
        push      ebp                                           ;235.12
        sub       esp, 32                                       ;235.12
        fld       DWORD PTR [_2il0floatpacket.24]               ;248.12
        mov       edx, DWORD PTR [52+esp]                       ;235.12
        mov       ebx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;246.11
        neg       ebx                                           ;246.2
        add       ebx, DWORD PTR [edx]                          ;246.2
        shl       ebx, 5                                        ;246.2
        mov       esi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;248.16
        neg       esi                                           ;248.7
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;246.2
        add       esi, DWORD PTR [edx]                          ;248.7
        mov       ecx, DWORD PTR [60+esp]                       ;235.12
        shl       esi, 8                                        ;248.7
        mov       edi, DWORD PTR [_DYNUST_FUEL_MODULE_mp_VEHFUEL+32] ;249.2
        neg       edi                                           ;249.2
        mov       ebp, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;248.12
        movsx     eax, BYTE PTR [5+eax+ebx]                     ;246.11
        cmp       eax, 3                                        ;247.18
        mov       DWORD PTR [ecx], 0                            ;245.2
        mov       ecx, 1                                        ;247.18
        mov       ebx, DWORD PTR [_DYNUST_FUEL_MODULE_mp_FUELSET+32] ;249.26
        cmove     eax, ecx                                      ;247.18
        add       edi, DWORD PTR [edx]                          ;249.2
        neg       ebx                                           ;249.2
        movss     xmm0, DWORD PTR [12+ebp+esi]                  ;248.16
        add       ebx, eax                                      ;249.2
        mulss     xmm0, DWORD PTR [_2il0floatpacket.23]         ;248.44
        mov       esi, DWORD PTR [_DYNUST_FUEL_MODULE_mp_FUELSET] ;249.26
        lea       edx, DWORD PTR [edi*8]                        ;249.2
        movss     DWORD PTR [12+esp], xmm0                      ;248.7
        lea       edx, DWORD PTR [edx+edi*4]                    ;249.2
        fld       DWORD PTR [12+esp]                            ;248.7
        imul      edi, ebx, 100                                 ;249.2
        fst       DWORD PTR [28+esp]                            ;248.7
        mov       ecx, DWORD PTR [_DYNUST_FUEL_MODULE_mp_VEHFUEL] ;249.2
        mov       ebp, DWORD PTR [92+esi+edi]                   ;249.53
L10:                                                            ;248.12
        fprem                                                   ;248.12
        fnstsw    ax                                            ;248.12
        sahf                                                    ;248.12
        jp        L10           ; Prob 90%                      ;248.12
        fstp      st(1)                                         ;248.12
                                ; LOE edx ecx ebp esi edi f1
.B7.6:                          ; Preds .B7.1
        fld       DWORD PTR [_2il0floatpacket.24]               ;248.7
        fxch      st(1)                                         ;248.12
        fstp      DWORD PTR [12+esp]                            ;248.12
        fld       DWORD PTR [12+esp]                            ;248.12
        fdiv      st, st(1)                                     ;248.7
        fld       DWORD PTR [28+esp]                            ;248.60
        fdivrp    st(2), st                                     ;248.60
        fcom      DWORD PTR [_2il0floatpacket.27]               ;248.7
        fnstsw    ax                                            ;248.7
        fld       DWORD PTR [_2il0floatpacket.26]               ;248.7
        fld       DWORD PTR [_2il0floatpacket.25]               ;248.7
        sahf                                                    ;248.7
        mov       ebx, DWORD PTR [56+esp]                       ;249.26
        fxch      st(1)                                         ;248.7
        ja        L11           ; Prob 50%                      ;248.7
        fst       st(1)                                         ;248.7
L11:                                                            ;
        fstp      st(0)                                         ;248.7
        movss     xmm4, DWORD PTR [4+ecx+edx]                   ;249.26
        faddp     st(1), st                                     ;248.7
        fnstcw    [esp]                                         ;248.7
        movzx     eax, WORD PTR [esp]                           ;248.7
        or        eax, 3072                                     ;248.7
        mov       DWORD PTR [8+esp], eax                        ;248.7
        fldcw     [8+esp]                                       ;248.7
        frndint                                                 ;248.7
        fldcw     [esp]                                         ;248.7
        fstp      DWORD PTR [20+esp]                            ;249.2
        movss     xmm0, DWORD PTR [20+esp]                      ;249.2
        fstp      DWORD PTR [20+esp]                            ;249.2
        movss     xmm1, DWORD PTR [20+esp]                      ;249.2
        movss     xmm2, DWORD PTR [ebx]                         ;249.26
        cvtss2si  ebx, xmm0                                     ;248.7
        cvttss2si eax, xmm1                                     ;248.60
        add       ebx, eax                                      ;249.2
        sub       ebx, DWORD PTR [96+esi+edi]                   ;249.2
        imul      ebx, ebp                                      ;249.2
        movss     xmm3, DWORD PTR [16+esi+edi]                  ;250.29
        mov       ebp, DWORD PTR [64+esi+edi]                   ;249.53
        divss     xmm2, DWORD PTR [4+ebp+ebx]                   ;249.52
        subss     xmm4, xmm2                                    ;249.2
        movss     DWORD PTR [4+ecx+edx], xmm4                   ;249.2
        comiss    xmm3, xmm4                                    ;250.27
        jbe       .B7.3         ; Prob 50%                      ;250.27
                                ; LOE
.B7.2:                          ; Preds .B7.6
        mov       eax, DWORD PTR [60+esp]                       ;251.4
        mov       DWORD PTR [eax], -1                           ;251.4
                                ; LOE
.B7.3:                          ; Preds .B7.6 .B7.2
        add       esp, 32                                       ;253.1
        pop       ebp                                           ;253.1
        pop       ebx                                           ;253.1
        pop       edi                                           ;253.1
        pop       esi                                           ;253.1
        ret                                                     ;253.1
        ALIGN     16
                                ; LOE
; mark_end;
_DYNUST_FUEL_MODULE_mp_UPDATEFUELLEVEL ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_FUEL_MODULE_mp_UPDATEFUELLEVEL
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_FUEL_MODULE_mp_INIVEHFUEL
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_FUEL_MODULE_mp_INIVEHFUEL
_DYNUST_FUEL_MODULE_mp_INIVEHFUEL	PROC NEAR 
.B8.1:                          ; Preds .B8.0
        push      ebp                                           ;256.12
        mov       ebp, esp                                      ;256.12
        and       esp, -16                                      ;256.12
        push      esi                                           ;256.12
        push      edi                                           ;256.12
        push      ebx                                           ;256.12
        sub       esp, 68                                       ;256.12
        xor       eax, eax                                      ;258.5
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFVEH] ;258.5
        test      edx, edx                                      ;258.5
        lea       ecx, DWORD PTR [52+esp]                       ;258.5
        push      12                                            ;258.5
        cmovle    edx, eax                                      ;258.5
        push      edx                                           ;258.5
        push      2                                             ;258.5
        push      ecx                                           ;258.5
        call      _for_check_mult_overflow                      ;258.5
                                ; LOE eax
.B8.2:                          ; Preds .B8.1
        mov       edx, DWORD PTR [_DYNUST_FUEL_MODULE_mp_VEHFUEL+12] ;258.5
        and       eax, 1                                        ;258.5
        and       edx, 1                                        ;258.5
        add       edx, edx                                      ;258.5
        shl       eax, 4                                        ;258.5
        or        edx, 1                                        ;258.5
        or        edx, eax                                      ;258.5
        or        edx, 262144                                   ;258.5
        push      edx                                           ;258.5
        push      OFFSET FLAT: _DYNUST_FUEL_MODULE_mp_VEHFUEL   ;258.5
        push      DWORD PTR [76+esp]                            ;258.5
        call      _for_alloc_allocatable                        ;258.5
                                ; LOE eax
.B8.33:                         ; Preds .B8.2
        add       esp, 28                                       ;258.5
                                ; LOE eax
.B8.3:                          ; Preds .B8.33
        test      eax, eax                                      ;258.5
        jne       .B8.5         ; Prob 50%                      ;258.5
                                ; LOE
.B8.4:                          ; Preds .B8.3
        mov       ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFVEH] ;258.5
        xor       edx, edx                                      ;258.5
        test      ebx, ebx                                      ;258.5
        cmovle    ebx, edx                                      ;258.5
        mov       ecx, 12                                       ;258.5
        lea       esi, DWORD PTR [40+esp]                       ;258.5
        push      ecx                                           ;258.5
        push      ebx                                           ;258.5
        push      2                                             ;258.5
        push      esi                                           ;258.5
        mov       eax, 1                                        ;258.5
        mov       DWORD PTR [_DYNUST_FUEL_MODULE_mp_VEHFUEL+12], 133 ;258.5
        mov       DWORD PTR [_DYNUST_FUEL_MODULE_mp_VEHFUEL+4], ecx ;258.5
        mov       DWORD PTR [_DYNUST_FUEL_MODULE_mp_VEHFUEL+16], eax ;258.5
        mov       DWORD PTR [_DYNUST_FUEL_MODULE_mp_VEHFUEL+8], edx ;258.5
        mov       DWORD PTR [_DYNUST_FUEL_MODULE_mp_VEHFUEL+32], eax ;258.5
        mov       DWORD PTR [_DYNUST_FUEL_MODULE_mp_VEHFUEL+24], ebx ;258.5
        mov       DWORD PTR [_DYNUST_FUEL_MODULE_mp_VEHFUEL+28], ecx ;258.5
        call      _for_check_mult_overflow                      ;258.5
                                ; LOE
.B8.34:                         ; Preds .B8.4
        add       esp, 16                                       ;258.5
        jmp       .B8.7         ; Prob 100%                     ;258.5
                                ; LOE
.B8.5:                          ; Preds .B8.3
        mov       DWORD PTR [esp], 0                            ;259.21
        lea       ebx, DWORD PTR [esp]                          ;259.21
        mov       DWORD PTR [32+esp], 44                        ;259.21
        lea       eax, DWORD PTR [32+esp]                       ;259.21
        mov       DWORD PTR [36+esp], OFFSET FLAT: __STRLITPACK_43 ;259.21
        push      32                                            ;259.21
        push      eax                                           ;259.21
        push      OFFSET FLAT: __STRLITPACK_109.0.8             ;259.21
        push      -2088435968                                   ;259.21
        push      911                                           ;259.21
        push      ebx                                           ;259.21
        call      _for_write_seq_lis                            ;259.21
                                ; LOE ebx
.B8.6:                          ; Preds .B8.5
        mov       DWORD PTR [72+esp], 0                         ;259.21
        lea       eax, DWORD PTR [72+esp]                       ;259.21
        push      eax                                           ;259.21
        push      OFFSET FLAT: __STRLITPACK_110.0.8             ;259.21
        push      ebx                                           ;259.21
        call      _for_write_seq_lis_xmit                       ;259.21
                                ; LOE
.B8.35:                         ; Preds .B8.6
        add       esp, 36                                       ;259.21
                                ; LOE
.B8.7:                          ; Preds .B8.34 .B8.35
        mov       edx, DWORD PTR [_DYNUST_FUEL_MODULE_mp_VEHFUEL+24] ;260.5
        test      edx, edx                                      ;260.5
        mov       ebx, DWORD PTR [_DYNUST_FUEL_MODULE_mp_VEHFUEL+32] ;260.5
        jle       .B8.26        ; Prob 50%                      ;260.5
                                ; LOE edx ebx
.B8.8:                          ; Preds .B8.7
        mov       eax, edx                                      ;260.5
        shr       eax, 31                                       ;260.5
        add       eax, edx                                      ;260.5
        sar       eax, 1                                        ;260.5
        mov       ecx, DWORD PTR [_DYNUST_FUEL_MODULE_mp_VEHFUEL] ;260.5
        test      eax, eax                                      ;260.5
        jbe       .B8.29        ; Prob 10%                      ;260.5
                                ; LOE eax edx ecx ebx
.B8.9:                          ; Preds .B8.8
        xor       edi, edi                                      ;
        mov       DWORD PTR [44+esp], ebx                       ;
        xor       esi, esi                                      ;
        xor       ebx, ebx                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B8.10:                         ; Preds .B8.10 .B8.9
        inc       edi                                           ;260.5
        mov       DWORD PTR [esi+ecx], ebx                      ;260.5
        mov       DWORD PTR [12+esi+ecx], ebx                   ;260.5
        add       esi, 24                                       ;260.5
        cmp       edi, eax                                      ;260.5
        jb        .B8.10        ; Prob 63%                      ;260.5
                                ; LOE eax edx ecx ebx esi edi
.B8.11:                         ; Preds .B8.10
        mov       ebx, DWORD PTR [44+esp]                       ;
        lea       edi, DWORD PTR [1+edi+edi]                    ;260.5
                                ; LOE eax edx ecx ebx edi
.B8.12:                         ; Preds .B8.11 .B8.29
        lea       esi, DWORD PTR [-1+edi]                       ;260.5
        cmp       edx, esi                                      ;260.5
        jbe       .B8.14        ; Prob 10%                      ;260.5
                                ; LOE eax edx ecx ebx edi
.B8.13:                         ; Preds .B8.12
        mov       esi, ebx                                      ;260.5
        add       edi, ebx                                      ;260.5
        neg       esi                                           ;260.5
        add       esi, edi                                      ;260.5
        lea       edi, DWORD PTR [esi*8]                        ;260.5
        lea       esi, DWORD PTR [edi+esi*4]                    ;260.5
        mov       DWORD PTR [-12+ecx+esi], 0                    ;260.5
                                ; LOE eax edx ecx ebx
.B8.14:                         ; Preds .B8.12 .B8.13
        test      eax, eax                                      ;261.5
        jbe       .B8.28        ; Prob 10%                      ;261.5
                                ; LOE eax edx ecx ebx
.B8.15:                         ; Preds .B8.14
        xor       edi, edi                                      ;
        mov       DWORD PTR [44+esp], ebx                       ;
        xor       esi, esi                                      ;
        xor       ebx, ebx                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B8.16:                         ; Preds .B8.16 .B8.15
        inc       edi                                           ;261.5
        mov       DWORD PTR [4+esi+ecx], ebx                    ;261.5
        mov       DWORD PTR [16+esi+ecx], ebx                   ;261.5
        add       esi, 24                                       ;261.5
        cmp       edi, eax                                      ;261.5
        jb        .B8.16        ; Prob 63%                      ;261.5
                                ; LOE eax edx ecx ebx esi edi
.B8.17:                         ; Preds .B8.16
        mov       ebx, DWORD PTR [44+esp]                       ;
        lea       edi, DWORD PTR [1+edi+edi]                    ;261.5
                                ; LOE eax edx ecx ebx edi
.B8.18:                         ; Preds .B8.17 .B8.28
        lea       esi, DWORD PTR [-1+edi]                       ;261.5
        cmp       edx, esi                                      ;261.5
        jbe       .B8.20        ; Prob 10%                      ;261.5
                                ; LOE eax edx ecx ebx edi
.B8.19:                         ; Preds .B8.18
        mov       esi, ebx                                      ;261.5
        add       edi, ebx                                      ;261.5
        neg       esi                                           ;261.5
        add       esi, edi                                      ;261.5
        lea       edi, DWORD PTR [esi*8]                        ;261.5
        lea       esi, DWORD PTR [edi+esi*4]                    ;261.5
        mov       DWORD PTR [-8+ecx+esi], 0                     ;261.5
                                ; LOE eax edx ecx ebx
.B8.20:                         ; Preds .B8.18 .B8.19
        test      eax, eax                                      ;262.5
        jbe       .B8.27        ; Prob 10%                      ;262.5
                                ; LOE eax edx ecx ebx
.B8.21:                         ; Preds .B8.20
        xor       esi, esi                                      ;
        mov       DWORD PTR [44+esp], ebx                       ;
        xor       edi, edi                                      ;
                                ; LOE eax edx ecx esi edi
.B8.22:                         ; Preds .B8.22 .B8.21
        lea       ebx, DWORD PTR [esi+esi*2]                    ;262.5
        inc       esi                                           ;262.5
        mov       DWORD PTR [8+ecx+ebx*8], edi                  ;262.5
        cmp       esi, eax                                      ;262.5
        mov       DWORD PTR [20+ecx+ebx*8], edi                 ;262.5
        jb        .B8.22        ; Prob 63%                      ;262.5
                                ; LOE eax edx ecx esi edi
.B8.23:                         ; Preds .B8.22
        mov       ebx, DWORD PTR [44+esp]                       ;
        lea       esi, DWORD PTR [1+esi+esi]                    ;262.5
                                ; LOE edx ecx ebx esi
.B8.24:                         ; Preds .B8.23 .B8.27
        lea       eax, DWORD PTR [-1+esi]                       ;262.5
        cmp       edx, eax                                      ;262.5
        jbe       .B8.26        ; Prob 10%                      ;262.5
                                ; LOE ecx ebx esi
.B8.25:                         ; Preds .B8.24
        mov       eax, ebx                                      ;262.5
        add       esi, ebx                                      ;262.5
        neg       eax                                           ;262.5
        add       eax, esi                                      ;262.5
        lea       ebx, DWORD PTR [eax*8]                        ;262.5
        lea       edx, DWORD PTR [ebx+eax*4]                    ;262.5
        mov       DWORD PTR [-4+ecx+edx], 0                     ;262.5
                                ; LOE
.B8.26:                         ; Preds .B8.7 .B8.24 .B8.25
        add       esp, 68                                       ;263.1
        pop       ebx                                           ;263.1
        pop       edi                                           ;263.1
        pop       esi                                           ;263.1
        mov       esp, ebp                                      ;263.1
        pop       ebp                                           ;263.1
        ret                                                     ;263.1
                                ; LOE
.B8.27:                         ; Preds .B8.20                  ; Infreq
        mov       esi, 1                                        ;
        jmp       .B8.24        ; Prob 100%                     ;
                                ; LOE edx ecx ebx esi
.B8.28:                         ; Preds .B8.14                  ; Infreq
        mov       edi, 1                                        ;
        jmp       .B8.18        ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx edi
.B8.29:                         ; Preds .B8.8                   ; Infreq
        mov       edi, 1                                        ;
        jmp       .B8.12        ; Prob 100%                     ;
        ALIGN     16
                                ; LOE eax edx ecx ebx edi
; mark_end;
_DYNUST_FUEL_MODULE_mp_INIVEHFUEL ENDP
_TEXT	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_109.0.8	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_110.0.8	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_FUEL_MODULE_mp_INIVEHFUEL
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_FUEL_MODULE_mp_DEALLOCATEFUEL
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_FUEL_MODULE_mp_DEALLOCATEFUEL
_DYNUST_FUEL_MODULE_mp_DEALLOCATEFUEL	PROC NEAR 
.B9.1:                          ; Preds .B9.0
        push      ebp                                           ;266.12
        mov       ebp, esp                                      ;266.12
        and       esp, -16                                      ;266.12
        push      edi                                           ;266.12
        push      ebx                                           ;266.12
        sub       esp, 40                                       ;266.12
        mov       ecx, DWORD PTR [_DYNUST_FUEL_MODULE_mp_NUMFUEL] ;267.5
        test      ecx, ecx                                      ;267.16
        jle       .B9.14        ; Prob 79%                      ;267.16
                                ; LOE ecx esi
.B9.2:                          ; Preds .B9.1
        imul      edi, DWORD PTR [_DYNUST_FUEL_MODULE_mp_FUELSET+32], -100 ;
        mov       edx, 1                                        ;
        mov       eax, DWORD PTR [_DYNUST_FUEL_MODULE_mp_FUELSET] ;269.9
        add       edi, eax                                      ;
        mov       DWORD PTR [4+esp], eax                        ;
        mov       ebx, 100                                      ;
        mov       DWORD PTR [esp], ecx                          ;
        mov       DWORD PTR [8+esp], esi                        ;
        mov       esi, edx                                      ;
                                ; LOE ebx esi edi
.B9.3:                          ; Preds .B9.5 .B9.2
        mov       eax, DWORD PTR [76+ebx+edi]                   ;269.20
        mov       ecx, eax                                      ;269.9
        shr       ecx, 1                                        ;269.9
        and       eax, 1                                        ;269.9
        and       ecx, 1                                        ;269.9
        add       eax, eax                                      ;269.9
        shl       ecx, 2                                        ;269.9
        or        ecx, eax                                      ;269.9
        or        ecx, 262144                                   ;269.9
        push      ecx                                           ;269.9
        push      DWORD PTR [64+ebx+edi]                        ;269.9
        call      _for_dealloc_allocatable                      ;269.9
                                ; LOE ebx esi edi
.B9.4:                          ; Preds .B9.3
        mov       eax, DWORD PTR [40+ebx+edi]                   ;270.20
        mov       ecx, eax                                      ;270.9
        shr       ecx, 1                                        ;270.9
        and       eax, 1                                        ;270.9
        and       ecx, 1                                        ;270.9
        add       eax, eax                                      ;270.9
        shl       ecx, 2                                        ;270.9
        or        ecx, eax                                      ;270.9
        or        ecx, 262144                                   ;270.9
        push      ecx                                           ;270.9
        push      DWORD PTR [28+ebx+edi]                        ;270.9
        mov       DWORD PTR [64+ebx+edi], 0                     ;269.9
        and       DWORD PTR [76+ebx+edi], -2                    ;269.9
        call      _for_dealloc_allocatable                      ;270.9
                                ; LOE ebx esi edi
.B9.20:                         ; Preds .B9.4
        add       esp, 16                                       ;270.9
                                ; LOE ebx esi edi
.B9.5:                          ; Preds .B9.20
        inc       esi                                           ;271.7
        mov       DWORD PTR [28+ebx+edi], 0                     ;270.9
        and       DWORD PTR [40+ebx+edi], -2                    ;270.9
        add       ebx, 100                                      ;271.7
        cmp       esi, DWORD PTR [esp]                          ;271.7
        jle       .B9.3         ; Prob 82%                      ;271.7
                                ; LOE ebx esi edi
.B9.6:                          ; Preds .B9.5
        mov       edi, DWORD PTR [_DYNUST_FUEL_MODULE_mp_FUELSET+12] ;272.7
        test      edi, 1                                        ;272.7
        mov       eax, DWORD PTR [4+esp]                        ;
        mov       esi, DWORD PTR [8+esp]                        ;
        je        .B9.12        ; Prob 60%                      ;272.7
                                ; LOE eax esi edi al ah
.B9.7:                          ; Preds .B9.6
        imul      ebx, DWORD PTR [_DYNUST_FUEL_MODULE_mp_FUELSET+24], 100 ;272.7
        mov       edx, eax                                      ;272.7
        add       ebx, eax                                      ;272.7
        cmp       eax, ebx                                      ;272.7
        jae       .B9.12        ; Prob 10%                      ;272.7
                                ; LOE eax edx ebx esi edi al dl ah dh
.B9.8:                          ; Preds .B9.7
        mov       DWORD PTR [esp], edi                          ;
        mov       DWORD PTR [4+esp], eax                        ;
        mov       DWORD PTR [8+esp], esi                        ;
        mov       esi, edx                                      ;
                                ; LOE ebx esi
.B9.9:                          ; Preds .B9.10 .B9.8
        mov       edi, DWORD PTR [40+esi]                       ;272.7
        test      edi, 1                                        ;272.7
        jne       .B9.16        ; Prob 4%                       ;272.7
                                ; LOE ebx esi edi
.B9.10:                         ; Preds .B9.9 .B9.17
        add       esi, 100                                      ;272.7
        cmp       esi, ebx                                      ;272.7
        jb        .B9.9         ; Prob 82%                      ;272.7
                                ; LOE ebx esi
.B9.11:                         ; Preds .B9.10
        mov       edi, DWORD PTR [esp]                          ;
        mov       eax, DWORD PTR [4+esp]                        ;
        mov       esi, DWORD PTR [8+esp]                        ;
                                ; LOE eax esi edi al ah
.B9.12:                         ; Preds .B9.11 .B9.6 .B9.7
        mov       ecx, edi                                      ;272.7
        mov       edx, edi                                      ;272.7
        shr       ecx, 1                                        ;272.7
        and       edx, 1                                        ;272.7
        and       ecx, 1                                        ;272.7
        add       edx, edx                                      ;272.7
        shl       ecx, 2                                        ;272.7
        or        ecx, edx                                      ;272.7
        or        ecx, 262144                                   ;272.7
        push      ecx                                           ;272.7
        push      eax                                           ;272.7
        call      _for_dealloc_allocatable                      ;272.7
                                ; LOE esi edi
.B9.21:                         ; Preds .B9.12
        add       esp, 8                                        ;272.7
                                ; LOE esi edi
.B9.13:                         ; Preds .B9.21
        and       edi, -2                                       ;272.7
        mov       DWORD PTR [_DYNUST_FUEL_MODULE_mp_FUELSET], 0 ;272.7
        mov       DWORD PTR [_DYNUST_FUEL_MODULE_mp_FUELSET+12], edi ;272.7
                                ; LOE esi
.B9.14:                         ; Preds .B9.1 .B9.13
        xor       eax, eax                                      ;275.11
        lea       edx, DWORD PTR [esp]                          ;275.11
        mov       DWORD PTR [esp], eax                          ;275.11
        push      32                                            ;275.11
        push      eax                                           ;275.11
        push      OFFSET FLAT: __STRLITPACK_111.0.9             ;275.11
        push      -2088435968                                   ;275.11
        push      6056                                          ;275.11
        push      edx                                           ;275.11
        call      _for_close                                    ;275.11
                                ; LOE esi
.B9.15:                         ; Preds .B9.14
        add       esp, 64                                       ;276.1
        pop       ebx                                           ;276.1
        pop       edi                                           ;276.1
        mov       esp, ebp                                      ;276.1
        pop       ebp                                           ;276.1
        ret                                                     ;276.1
                                ; LOE
.B9.16:                         ; Preds .B9.9                   ; Infreq
        push      262145                                        ;272.7
        push      DWORD PTR [28+esi]                            ;272.7
        call      _for_deallocate                               ;272.7
                                ; LOE ebx esi edi
.B9.23:                         ; Preds .B9.16                  ; Infreq
        add       esp, 8                                        ;272.7
                                ; LOE ebx esi edi
.B9.17:                         ; Preds .B9.23                  ; Infreq
        and       edi, -2                                       ;272.7
        mov       DWORD PTR [28+esi], 0                         ;272.7
        mov       DWORD PTR [40+esi], edi                       ;272.7
        jmp       .B9.10        ; Prob 100%                     ;272.7
        ALIGN     16
                                ; LOE ebx esi
; mark_end;
_DYNUST_FUEL_MODULE_mp_DEALLOCATEFUEL ENDP
_TEXT	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_111.0.9	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_FUEL_MODULE_mp_DEALLOCATEFUEL
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_FUEL_MODULE_mp_WRITEOUTFUELCONSUM
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_FUEL_MODULE_mp_WRITEOUTFUELCONSUM
_DYNUST_FUEL_MODULE_mp_WRITEOUTFUELCONSUM	PROC NEAR 
.B10.1:                         ; Preds .B10.0
        push      ebp                                           ;279.12
        mov       ebp, esp                                      ;279.12
        and       esp, -16                                      ;279.12
        push      esi                                           ;279.12
        push      edi                                           ;279.12
        push      ebx                                           ;279.12
        sub       esp, 244                                      ;279.12
        xor       edx, edx                                      ;284.22
        movss     xmm3, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIMPERIOD] ;286.25
        mov       ebx, 8                                        ;286.3
        movss     xmm0, DWORD PTR [_2il0floatpacket.40]         ;286.25
        mov       edi, 2                                        ;284.22
        andps     xmm0, xmm3                                    ;286.25
        mov       esi, 4                                        ;286.3
        pxor      xmm3, xmm0                                    ;286.25
        mov       eax, 1                                        ;286.3
        movss     xmm1, DWORD PTR [_2il0floatpacket.41]         ;286.25
        movaps    xmm2, xmm3                                    ;286.25
        movss     xmm4, DWORD PTR [_2il0floatpacket.42]         ;286.25
        cmpltss   xmm2, xmm1                                    ;286.25
        andps     xmm1, xmm2                                    ;286.25
        movaps    xmm2, xmm3                                    ;286.25
        movaps    xmm6, xmm4                                    ;286.25
        addss     xmm2, xmm1                                    ;286.25
        addss     xmm6, xmm4                                    ;286.25
        subss     xmm2, xmm1                                    ;286.25
        movaps    xmm7, xmm2                                    ;286.25
        mov       DWORD PTR [esp], edx                          ;284.22
        subss     xmm7, xmm3                                    ;286.25
        movaps    xmm5, xmm7                                    ;286.25
        cmpless   xmm7, DWORD PTR [_2il0floatpacket.43]         ;286.25
        cmpnless  xmm5, xmm4                                    ;286.25
        andps     xmm5, xmm6                                    ;286.25
        andps     xmm7, xmm6                                    ;286.25
        mov       DWORD PTR [4+esp], edx                        ;284.22
        subss     xmm2, xmm5                                    ;286.25
        mov       DWORD PTR [8+esp], edx                        ;284.22
        addss     xmm2, xmm7                                    ;286.25
        orps      xmm2, xmm0                                    ;286.25
        cvtss2si  ecx, xmm2                                     ;286.3
        test      ecx, ecx                                      ;286.3
        mov       DWORD PTR [12+esp], 128                       ;284.22
        cmovl     ecx, edx                                      ;286.3
        mov       DWORD PTR [16+esp], edi                       ;284.22
        mov       DWORD PTR [20+esp], edx                       ;284.22
        mov       DWORD PTR [24+esp], edx                       ;284.22
        mov       DWORD PTR [28+esp], edx                       ;284.22
        mov       DWORD PTR [32+esp], edx                       ;284.22
        mov       DWORD PTR [36+esp], edx                       ;284.22
        mov       DWORD PTR [40+esp], edx                       ;284.22
        mov       DWORD PTR [44+esp], edx                       ;284.22
        mov       DWORD PTR [48+esp], edx                       ;283.25
        mov       DWORD PTR [68+esp], edx                       ;283.25
        mov       DWORD PTR [60+esp], 133                       ;286.3
        mov       DWORD PTR [52+esp], esi                       ;286.3
        mov       DWORD PTR [64+esp], edi                       ;286.3
        mov       DWORD PTR [56+esp], edx                       ;286.3
        mov       DWORD PTR [80+esp], eax                       ;286.3
        mov       DWORD PTR [72+esp], edi                       ;286.3
        mov       DWORD PTR [92+esp], eax                       ;286.3
        lea       eax, DWORD PTR [148+esp]                      ;286.3
        mov       DWORD PTR [84+esp], ecx                       ;286.3
        mov       DWORD PTR [76+esp], esi                       ;286.3
        mov       DWORD PTR [88+esp], ebx                       ;286.3
        push      ebx                                           ;286.3
        push      ecx                                           ;286.3
        push      edi                                           ;286.3
        push      eax                                           ;286.3
        call      _for_check_mult_overflow                      ;286.3
                                ; LOE eax ebx esi edi
.B10.2:                         ; Preds .B10.1
        mov       edx, DWORD PTR [76+esp]                       ;286.3
        and       eax, 1                                        ;286.3
        and       edx, 1                                        ;286.3
        lea       ecx, DWORD PTR [64+esp]                       ;286.3
        shl       eax, 4                                        ;286.3
        add       edx, edx                                      ;286.3
        or        edx, eax                                      ;286.3
        or        edx, 262144                                   ;286.3
        push      edx                                           ;286.3
        push      ecx                                           ;286.3
        push      DWORD PTR [172+esp]                           ;286.3
        call      _for_alloc_allocatable                        ;286.3
                                ; LOE ebx esi edi
.B10.3:                         ; Preds .B10.2
        movss     xmm3, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIMPERIOD] ;287.25
        xor       edx, edx                                      ;287.3
        movss     xmm0, DWORD PTR [_2il0floatpacket.40]         ;287.25
        mov       eax, 1                                        ;287.3
        andps     xmm0, xmm3                                    ;287.25
        pxor      xmm3, xmm0                                    ;287.25
        movss     xmm1, DWORD PTR [_2il0floatpacket.41]         ;287.25
        movaps    xmm2, xmm3                                    ;287.25
        movss     xmm4, DWORD PTR [_2il0floatpacket.42]         ;287.25
        cmpltss   xmm2, xmm1                                    ;287.25
        andps     xmm1, xmm2                                    ;287.25
        movaps    xmm2, xmm3                                    ;287.25
        movaps    xmm6, xmm4                                    ;287.25
        addss     xmm2, xmm1                                    ;287.25
        addss     xmm6, xmm4                                    ;287.25
        subss     xmm2, xmm1                                    ;287.25
        movaps    xmm7, xmm2                                    ;287.25
        mov       DWORD PTR [40+esp], 133                       ;287.3
        subss     xmm7, xmm3                                    ;287.25
        movaps    xmm5, xmm7                                    ;287.25
        cmpless   xmm7, DWORD PTR [_2il0floatpacket.43]         ;287.25
        cmpnless  xmm5, xmm4                                    ;287.25
        andps     xmm5, xmm6                                    ;287.25
        andps     xmm7, xmm6                                    ;287.25
        mov       DWORD PTR [32+esp], esi                       ;287.3
        subss     xmm2, xmm5                                    ;287.25
        mov       DWORD PTR [44+esp], edi                       ;287.3
        addss     xmm2, xmm7                                    ;287.25
        orps      xmm2, xmm0                                    ;287.25
        cvtss2si  ecx, xmm2                                     ;287.3
        test      ecx, ecx                                      ;287.3
        mov       DWORD PTR [36+esp], edx                       ;287.3
        cmovl     ecx, edx                                      ;287.3
        mov       DWORD PTR [60+esp], eax                       ;287.3
        mov       DWORD PTR [52+esp], edi                       ;287.3
        mov       DWORD PTR [72+esp], eax                       ;287.3
        mov       DWORD PTR [64+esp], ecx                       ;287.3
        mov       DWORD PTR [56+esp], esi                       ;287.3
        lea       esi, DWORD PTR [180+esp]                      ;287.3
        mov       DWORD PTR [68+esp], ebx                       ;287.3
        push      ebx                                           ;287.3
        push      ecx                                           ;287.3
        push      edi                                           ;287.3
        push      esi                                           ;287.3
        call      _for_check_mult_overflow                      ;287.3
                                ; LOE eax
.B10.4:                         ; Preds .B10.3
        mov       edx, DWORD PTR [56+esp]                       ;287.3
        and       eax, 1                                        ;287.3
        and       edx, 1                                        ;287.3
        lea       ecx, DWORD PTR [44+esp]                       ;287.3
        shl       eax, 4                                        ;287.3
        add       edx, edx                                      ;287.3
        or        edx, eax                                      ;287.3
        or        edx, 262144                                   ;287.3
        push      edx                                           ;287.3
        push      ecx                                           ;287.3
        push      DWORD PTR [204+esp]                           ;287.3
        call      _for_alloc_allocatable                        ;287.3
                                ; LOE
.B10.135:                       ; Preds .B10.4
        add       esp, 56                                       ;287.3
                                ; LOE
.B10.5:                         ; Preds .B10.135
        mov       eax, DWORD PTR [92+esp]                       ;288.3
        mov       ecx, DWORD PTR [84+esp]                       ;288.3
        test      ecx, ecx                                      ;288.3
        mov       edx, DWORD PTR [48+esp]                       ;315.4
        mov       DWORD PTR [156+esp], eax                      ;288.3
        mov       DWORD PTR [180+esp], ecx                      ;288.3
        mov       DWORD PTR [196+esp], edx                      ;315.4
        jle       .B10.7        ; Prob 10%                      ;288.3
                                ; LOE
.B10.6:                         ; Preds .B10.5
        mov       ecx, DWORD PTR [72+esp]                       ;288.3
        test      ecx, ecx                                      ;288.3
        mov       eax, DWORD PTR [80+esp]                       ;288.3
        mov       edx, DWORD PTR [88+esp]                       ;313.82
        jg        .B10.82       ; Prob 50%                      ;288.3
                                ; LOE eax edx ecx
.B10.7:                         ; Preds .B10.123 .B10.85 .B10.6 .B10.5
        mov       eax, DWORD PTR [44+esp]                       ;289.3
        mov       ecx, DWORD PTR [36+esp]                       ;289.3
        test      ecx, ecx                                      ;289.3
        mov       edx, DWORD PTR [esp]                          ;314.4
        mov       DWORD PTR [160+esp], eax                      ;289.3
        mov       DWORD PTR [188+esp], ecx                      ;289.3
        mov       DWORD PTR [184+esp], edx                      ;314.4
        jle       .B10.107      ; Prob 10%                      ;289.3
                                ; LOE
.B10.8:                         ; Preds .B10.7
        mov       ebx, DWORD PTR [24+esp]                       ;289.3
        test      ebx, ebx                                      ;289.3
        pxor      xmm0, xmm0                                    ;306.104
        mov       eax, DWORD PTR [32+esp]                       ;289.3
        mov       ecx, DWORD PTR [40+esp]                       ;313.104
        movss     DWORD PTR [192+esp], xmm0                     ;306.104
        jg        .B10.78       ; Prob 50%                      ;289.3
                                ; LOE eax ecx ebx
.B10.9:                         ; Preds .B10.102 .B10.81 .B10.8 .B10.107
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFVEH] ;291.3
        test      eax, eax                                      ;291.3
        mov       DWORD PTR [124+esp], eax                      ;291.3
        jle       .B10.15       ; Prob 2%                       ;291.3
                                ; LOE
.B10.10:                        ; Preds .B10.9
        mov       edi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;292.10
        shl       edi, 5                                        ;
        mov       ebx, DWORD PTR [_DYNUST_FUEL_MODULE_mp_VEHFUEL+32] ;296.32
        neg       edi                                           ;
        add       edi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;
        mov       eax, DWORD PTR [40+esp]                       ;313.104
        mov       DWORD PTR [120+esp], edi                      ;
        mov       edi, DWORD PTR [160+esp]                      ;
        mov       DWORD PTR [116+esp], eax                      ;313.104
        imul      edi, eax                                      ;
        lea       eax, DWORD PTR [ebx*8]                        ;
        movss     xmm2, DWORD PTR [192+esp]                     ;
        mov       esi, DWORD PTR [88+esp]                       ;313.82
        lea       eax, DWORD PTR [eax+ebx*4]                    ;
        mov       ebx, DWORD PTR [156+esp]                      ;
        neg       eax                                           ;
        imul      ebx, esi                                      ;
        mov       edx, DWORD PTR [32+esp]                       ;313.104
        mov       ecx, DWORD PTR [80+esp]                       ;313.82
        shl       edx, 2                                        ;
        shl       ecx, 2                                        ;
        neg       edx                                           ;
        neg       ecx                                           ;
        add       edx, DWORD PTR [184+esp]                      ;
        add       ecx, DWORD PTR [196+esp]                      ;
        sub       edx, edi                                      ;
        mov       DWORD PTR [112+esp], 1                        ;
        sub       ecx, ebx                                      ;
        add       eax, DWORD PTR [_DYNUST_FUEL_MODULE_mp_VEHFUEL] ;
        mov       DWORD PTR [104+esp], ecx                      ;
        mov       DWORD PTR [108+esp], eax                      ;
        mov       DWORD PTR [100+esp], edx                      ;
        mov       DWORD PTR [96+esp], esi                       ;
        mov       ebx, DWORD PTR [112+esp]                      ;
                                ; LOE ebx xmm2
.B10.11:                        ; Preds .B10.13 .B10.10
        mov       ecx, ebx                                      ;292.4
        mov       eax, 1                                        ;293.17
        shl       ecx, 5                                        ;292.4
        mov       edx, DWORD PTR [120+esp]                      ;292.10
        movsx     esi, BYTE PTR [5+edx+ecx]                     ;292.10
        cmp       esi, 3                                        ;293.17
        movss     xmm0, DWORD PTR [12+edx+ecx]                  ;294.7
        cmove     esi, eax                                      ;293.17
        comiss    xmm0, xmm2                                    ;294.36
        jbe       .B10.13       ; Prob 50%                      ;294.36
                                ; LOE ebx esi xmm0 xmm2
.B10.12:                        ; Preds .B10.11
        mov       edi, DWORD PTR [100+esp]                      ;296.6
        movaps    xmm1, xmm2                                    ;296.6
        mov       DWORD PTR [112+esp], ebx                      ;
        cvttss2si ecx, xmm0                                     ;295.16
        inc       ecx                                           ;295.6
        lea       eax, DWORD PTR [edi+esi*4]                    ;296.6
        mov       edx, DWORD PTR [116+esp]                      ;295.6
        lea       edi, DWORD PTR [ebx*8]                        ;296.6
        imul      edx, ecx                                      ;295.6
        lea       edi, DWORD PTR [edi+ebx*4]                    ;296.6
        imul      ecx, DWORD PTR [96+esp]                       ;295.6
        mov       ebx, DWORD PTR [108+esp]                      ;296.66
        movss     xmm0, DWORD PTR [ebx+edi]                     ;296.66
        subss     xmm0, DWORD PTR [4+ebx+edi]                   ;296.84
        mov       ebx, DWORD PTR [112+esp]                      ;297.6
        maxss     xmm1, xmm0                                    ;296.6
        addss     xmm1, DWORD PTR [edx+eax]                     ;296.6
        movss     DWORD PTR [edx+eax], xmm1                     ;296.6
        mov       eax, DWORD PTR [104+esp]                      ;297.6
        lea       esi, DWORD PTR [eax+esi*4]                    ;297.6
        inc       DWORD PTR [ecx+esi]                           ;297.6
                                ; LOE ebx xmm2
.B10.13:                        ; Preds .B10.11 .B10.12
        inc       ebx                                           ;299.3
        cmp       ebx, DWORD PTR [124+esp]                      ;299.3
        jle       .B10.11       ; Prob 82%                      ;299.3
                                ; LOE ebx xmm2
.B10.15:                        ; Preds .B10.13 .B10.9
        xor       eax, eax                                      ;300.3
        lea       edx, DWORD PTR [96+esp]                       ;300.3
        mov       DWORD PTR [96+esp], eax                       ;300.3
        push      32                                            ;300.3
        push      OFFSET FLAT: DYNUST_FUEL_MODULE_mp_WRITEOUTFUELCONSUM$format_pack.0.10 ;300.3
        push      eax                                           ;300.3
        push      OFFSET FLAT: __STRLITPACK_112.0.10            ;300.3
        push      -2088435968                                   ;300.3
        push      6060                                          ;300.3
        push      edx                                           ;300.3
        call      _for_write_seq_fmt                            ;300.3
                                ; LOE
.B10.16:                        ; Preds .B10.15
        xor       eax, eax                                      ;301.3
        mov       DWORD PTR [124+esp], eax                      ;301.3
        push      32                                            ;301.3
        push      OFFSET FLAT: DYNUST_FUEL_MODULE_mp_WRITEOUTFUELCONSUM$format_pack.0.10+52 ;301.3
        push      eax                                           ;301.3
        push      OFFSET FLAT: __STRLITPACK_113.0.10            ;301.3
        push      -2088435968                                   ;301.3
        push      6060                                          ;301.3
        lea       edx, DWORD PTR [148+esp]                      ;301.3
        push      edx                                           ;301.3
        call      _for_write_seq_fmt                            ;301.3
                                ; LOE
.B10.136:                       ; Preds .B10.16
        add       esp, 56                                       ;301.3
                                ; LOE
.B10.17:                        ; Preds .B10.136
        cvttss2si eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIMPERIOD] ;302.4
        mov       DWORD PTR [172+esp], eax                      ;302.4
        test      eax, eax                                      ;302.4
        jle       .B10.24       ; Prob 2%                       ;302.4
                                ; LOE
.B10.18:                        ; Preds .B10.17
        mov       esi, DWORD PTR [40+esp]                       ;303.116
        mov       ebx, 1                                        ;
        mov       edi, DWORD PTR [160+esp]                      ;
        imul      edi, esi                                      ;
        mov       edx, DWORD PTR [32+esp]                       ;303.116
        shl       edx, 2                                        ;
        neg       edx                                           ;
        add       edx, DWORD PTR [184+esp]                      ;
        mov       ecx, DWORD PTR [88+esp]                       ;303.99
        sub       edx, edi                                      ;
        mov       edi, DWORD PTR [156+esp]                      ;
        imul      edi, ecx                                      ;
        mov       eax, DWORD PTR [80+esp]                       ;303.99
        shl       eax, 2                                        ;
        neg       eax                                           ;
        add       eax, DWORD PTR [196+esp]                      ;
        mov       DWORD PTR [132+esp], esi                      ;
        sub       eax, edi                                      ;
        mov       DWORD PTR [128+esp], ecx                      ;
        mov       DWORD PTR [136+esp], esi                      ;
        mov       DWORD PTR [164+esp], eax                      ;
        mov       DWORD PTR [144+esp], edx                      ;
        mov       DWORD PTR [140+esp], ecx                      ;
        mov       esi, DWORD PTR [128+esp]                      ;
        mov       edi, DWORD PTR [132+esp]                      ;
                                ; LOE ebx esi edi
.B10.19:                        ; Preds .B10.22 .B10.18
        cvtsi2ss  xmm0, ebx                                     ;303.6
        mov       DWORD PTR [96+esp], 0                         ;303.6
        movss     DWORD PTR [200+esp], xmm0                     ;303.6
        push      32                                            ;303.6
        push      OFFSET FLAT: DYNUST_FUEL_MODULE_mp_WRITEOUTFUELCONSUM$format_pack.0.10+80 ;303.6
        lea       eax, DWORD PTR [208+esp]                      ;303.6
        push      eax                                           ;303.6
        push      OFFSET FLAT: __STRLITPACK_114.0.10            ;303.6
        push      -2088435968                                   ;303.6
        push      6060                                          ;303.6
        lea       edx, DWORD PTR [120+esp]                      ;303.6
        push      edx                                           ;303.6
        call      _for_write_seq_fmt                            ;303.6
                                ; LOE ebx esi edi
.B10.20:                        ; Preds .B10.19
        mov       eax, DWORD PTR [192+esp]                      ;303.6
        lea       ecx, DWORD PTR [236+esp]                      ;303.6
        mov       edx, DWORD PTR [4+esi+eax]                    ;303.6
        mov       DWORD PTR [236+esp], edx                      ;303.6
        push      ecx                                           ;303.6
        push      OFFSET FLAT: __STRLITPACK_115.0.10            ;303.6
        lea       eax, DWORD PTR [132+esp]                      ;303.6
        push      eax                                           ;303.6
        call      _for_write_seq_fmt_xmit                       ;303.6
                                ; LOE ebx esi edi
.B10.21:                        ; Preds .B10.20
        mov       eax, DWORD PTR [184+esp]                      ;303.6
        lea       ecx, DWORD PTR [256+esp]                      ;303.6
        mov       edx, DWORD PTR [4+edi+eax]                    ;303.6
        mov       DWORD PTR [256+esp], edx                      ;303.6
        push      ecx                                           ;303.6
        push      OFFSET FLAT: __STRLITPACK_116.0.10            ;303.6
        lea       eax, DWORD PTR [144+esp]                      ;303.6
        push      eax                                           ;303.6
        call      _for_write_seq_fmt_xmit                       ;303.6
                                ; LOE ebx esi edi
.B10.137:                       ; Preds .B10.21
        add       esp, 52                                       ;303.6
                                ; LOE ebx esi edi
.B10.22:                        ; Preds .B10.137
        inc       ebx                                           ;304.4
        add       esi, DWORD PTR [140+esp]                      ;304.4
        add       edi, DWORD PTR [136+esp]                      ;304.4
        cmp       ebx, DWORD PTR [172+esp]                      ;304.4
        jle       .B10.19       ; Prob 82%                      ;304.4
                                ; LOE ebx esi edi
.B10.24:                        ; Preds .B10.22 .B10.17
        xor       eax, eax                                      ;305.4
        mov       DWORD PTR [96+esp], eax                       ;305.4
        push      32                                            ;305.4
        push      eax                                           ;305.4
        push      OFFSET FLAT: __STRLITPACK_117.0.10            ;305.4
        push      -2088435968                                   ;305.4
        push      6060                                          ;305.4
        lea       edx, DWORD PTR [116+esp]                      ;305.4
        push      edx                                           ;305.4
        call      _for_write_seq_lis                            ;305.4
                                ; LOE
.B10.138:                       ; Preds .B10.24
        add       esp, 24                                       ;305.4
                                ; LOE
.B10.25:                        ; Preds .B10.138
        cmp       DWORD PTR [180+esp], 0                        ;306.82
        jle       .B10.32       ; Prob 50%                      ;306.82
                                ; LOE
.B10.26:                        ; Preds .B10.25
        mov       edx, DWORD PTR [180+esp]                      ;306.82
        mov       eax, edx                                      ;306.82
        shr       eax, 31                                       ;306.82
        xor       ecx, ecx                                      ;
        add       eax, edx                                      ;306.82
        sar       eax, 1                                        ;306.82
        mov       esi, DWORD PTR [80+esp]                       ;306.82
        test      eax, eax                                      ;306.82
        mov       ebx, DWORD PTR [88+esp]                       ;306.82
        jbe       .B10.130      ; Prob 10%                      ;306.82
                                ; LOE eax ecx ebx esi
.B10.27:                        ; Preds .B10.26
        mov       DWORD PTR [140+esp], ecx                      ;
        lea       edi, DWORD PTR [esi*4]                        ;
        mov       ecx, DWORD PTR [156+esp]                      ;
        neg       edi                                           ;
        imul      ecx, ebx                                      ;
        xor       edx, edx                                      ;
        add       edi, DWORD PTR [196+esp]                      ;
        mov       DWORD PTR [136+esp], edi                      ;
        sub       edi, ecx                                      ;
        add       ecx, ebx                                      ;
        add       edi, ecx                                      ;
        mov       DWORD PTR [144+esp], edx                      ;
        mov       DWORD PTR [132+esp], edi                      ;
        mov       DWORD PTR [164+esp], eax                      ;
        mov       DWORD PTR [168+esp], ebx                      ;
        mov       DWORD PTR [128+esp], esi                      ;
        mov       edi, DWORD PTR [144+esp]                      ;
        mov       ecx, DWORD PTR [140+esp]                      ;
        mov       eax, edx                                      ;
        mov       ebx, DWORD PTR [132+esp]                      ;
        mov       esi, DWORD PTR [136+esp]                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.28:                        ; Preds .B10.28 .B10.27
        inc       edi                                           ;306.82
        add       ecx, DWORD PTR [4+esi+eax*2]                  ;306.82
        add       edx, DWORD PTR [4+ebx+eax*2]                  ;306.82
        add       eax, DWORD PTR [168+esp]                      ;306.82
        cmp       edi, DWORD PTR [164+esp]                      ;306.82
        jb        .B10.28       ; Prob 63%                      ;306.82
                                ; LOE eax edx ecx ebx esi edi
.B10.29:                        ; Preds .B10.28
        mov       ebx, DWORD PTR [168+esp]                      ;
        add       ecx, edx                                      ;306.82
        mov       esi, DWORD PTR [128+esp]                      ;
        lea       edx, DWORD PTR [1+edi+edi]                    ;306.82
                                ; LOE edx ecx ebx esi
.B10.30:                        ; Preds .B10.29 .B10.130
        lea       eax, DWORD PTR [-1+edx]                       ;306.82
        cmp       eax, DWORD PTR [180+esp]                      ;306.82
        jae       .B10.33       ; Prob 10%                      ;306.82
                                ; LOE edx ecx ebx esi
.B10.31:                        ; Preds .B10.30
        mov       edi, DWORD PTR [156+esp]                      ;306.82
        mov       eax, edi                                      ;306.82
        neg       eax                                           ;306.82
        shl       esi, 2                                        ;306.82
        lea       edx, DWORD PTR [-1+edi+edx]                   ;306.82
        add       eax, edx                                      ;306.82
        neg       esi                                           ;306.82
        imul      eax, ebx                                      ;306.82
        add       esi, DWORD PTR [196+esp]                      ;306.82
        add       ecx, DWORD PTR [4+esi+eax]                    ;306.82
        jmp       .B10.33       ; Prob 100%                     ;306.82
                                ; LOE ecx
.B10.32:                        ; Preds .B10.25
        xor       ecx, ecx                                      ;
                                ; LOE ecx
.B10.33:                        ; Preds .B10.31 .B10.30 .B10.32
        mov       DWORD PTR [96+esp], 0                         ;306.4
        lea       eax, DWORD PTR [168+esp]                      ;306.4
        mov       DWORD PTR [168+esp], ecx                      ;306.4
        push      32                                            ;306.4
        push      OFFSET FLAT: DYNUST_FUEL_MODULE_mp_WRITEOUTFUELCONSUM$format_pack.0.10+180 ;306.4
        push      eax                                           ;306.4
        push      OFFSET FLAT: __STRLITPACK_118.0.10            ;306.4
        push      -2088435968                                   ;306.4
        push      6060                                          ;306.4
        lea       edx, DWORD PTR [120+esp]                      ;306.4
        push      edx                                           ;306.4
        call      _for_write_seq_fmt                            ;306.4
                                ; LOE
.B10.139:                       ; Preds .B10.33
        add       esp, 28                                       ;306.4
                                ; LOE
.B10.34:                        ; Preds .B10.139
        cmp       DWORD PTR [188+esp], 0                        ;306.104
        jle       .B10.41       ; Prob 50%                      ;306.104
                                ; LOE
.B10.35:                        ; Preds .B10.34
        mov       eax, DWORD PTR [188+esp]                      ;306.104
        mov       ebx, eax                                      ;306.104
        shr       ebx, 31                                       ;306.104
        add       ebx, eax                                      ;306.104
        sar       ebx, 1                                        ;306.104
        mov       edx, DWORD PTR [32+esp]                       ;306.104
        test      ebx, ebx                                      ;306.104
        mov       esi, DWORD PTR [40+esp]                       ;306.104
        movss     xmm0, DWORD PTR [192+esp]                     ;
        jbe       .B10.87       ; Prob 10%                      ;306.104
                                ; LOE edx ebx esi xmm0
.B10.36:                        ; Preds .B10.35
        mov       eax, DWORD PTR [160+esp]                      ;
        lea       edi, DWORD PTR [edx*4]                        ;
        imul      eax, esi                                      ;
        neg       edi                                           ;
        movss     xmm1, DWORD PTR [192+esp]                     ;
        add       edi, DWORD PTR [184+esp]                      ;
        xor       ecx, ecx                                      ;
        mov       DWORD PTR [132+esp], edi                      ;
        sub       edi, eax                                      ;
        add       eax, esi                                      ;
        add       edi, eax                                      ;
        xor       eax, eax                                      ;
        mov       DWORD PTR [128+esp], edx                      ;
        mov       edx, edi                                      ;
        mov       edi, DWORD PTR [132+esp]                      ;
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1
.B10.37:                        ; Preds .B10.37 .B10.36
        inc       ecx                                           ;306.104
        addss     xmm0, DWORD PTR [4+edi+eax*2]                 ;306.104
        addss     xmm1, DWORD PTR [4+edx+eax*2]                 ;306.104
        add       eax, esi                                      ;306.104
        cmp       ecx, ebx                                      ;306.104
        jb        .B10.37       ; Prob 63%                      ;306.104
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1
.B10.38:                        ; Preds .B10.37
        mov       edx, DWORD PTR [128+esp]                      ;
        lea       ecx, DWORD PTR [1+ecx+ecx]                    ;306.104
        addss     xmm0, xmm1                                    ;306.104
                                ; LOE edx ecx esi xmm0
.B10.39:                        ; Preds .B10.38 .B10.87
        lea       eax, DWORD PTR [-1+ecx]                       ;306.104
        cmp       eax, DWORD PTR [188+esp]                      ;306.104
        jae       .B10.42       ; Prob 10%                      ;306.104
                                ; LOE edx ecx esi xmm0
.B10.40:                        ; Preds .B10.39
        mov       eax, DWORD PTR [160+esp]                      ;306.104
        mov       ebx, eax                                      ;306.104
        neg       ebx                                           ;306.104
        shl       edx, 2                                        ;306.104
        lea       ecx, DWORD PTR [-1+eax+ecx]                   ;306.104
        add       ebx, ecx                                      ;306.104
        neg       edx                                           ;306.104
        imul      ebx, esi                                      ;306.104
        add       edx, DWORD PTR [184+esp]                      ;306.104
        addss     xmm0, DWORD PTR [4+edx+ebx]                   ;306.104
        jmp       .B10.42       ; Prob 100%                     ;306.104
                                ; LOE xmm0
.B10.41:                        ; Preds .B10.34
        movss     xmm0, DWORD PTR [192+esp]                     ;
                                ; LOE xmm0
.B10.42:                        ; Preds .B10.40 .B10.39 .B10.41
        movss     DWORD PTR [176+esp], xmm0                     ;306.4
        lea       eax, DWORD PTR [176+esp]                      ;306.4
        push      eax                                           ;306.4
        push      OFFSET FLAT: __STRLITPACK_119.0.10            ;306.4
        lea       edx, DWORD PTR [104+esp]                      ;306.4
        push      edx                                           ;306.4
        call      _for_write_seq_fmt_xmit                       ;306.4
                                ; LOE
.B10.43:                        ; Preds .B10.42
        xor       eax, eax                                      ;307.4
        mov       DWORD PTR [108+esp], eax                      ;307.4
        push      32                                            ;307.4
        push      eax                                           ;307.4
        push      OFFSET FLAT: __STRLITPACK_120.0.10            ;307.4
        push      -2088435968                                   ;307.4
        push      6060                                          ;307.4
        lea       edx, DWORD PTR [128+esp]                      ;307.4
        push      edx                                           ;307.4
        call      _for_write_seq_lis                            ;307.4
                                ; LOE
.B10.44:                        ; Preds .B10.43
        xor       eax, eax                                      ;308.4
        mov       DWORD PTR [132+esp], eax                      ;308.4
        push      32                                            ;308.4
        push      OFFSET FLAT: DYNUST_FUEL_MODULE_mp_WRITEOUTFUELCONSUM$format_pack.0.10+272 ;308.4
        push      eax                                           ;308.4
        push      OFFSET FLAT: __STRLITPACK_121.0.10            ;308.4
        push      -2088435968                                   ;308.4
        push      6060                                          ;308.4
        lea       edx, DWORD PTR [156+esp]                      ;308.4
        push      edx                                           ;308.4
        call      _for_write_seq_fmt                            ;308.4
                                ; LOE
.B10.140:                       ; Preds .B10.44
        add       esp, 64                                       ;308.4
                                ; LOE
.B10.45:                        ; Preds .B10.140
        cmp       DWORD PTR [172+esp], 0                        ;309.4
        jle       .B10.52       ; Prob 2%                       ;309.4
                                ; LOE
.B10.46:                        ; Preds .B10.45
        mov       esi, DWORD PTR [40+esp]                       ;310.116
        mov       ebx, 1                                        ;
        mov       edi, DWORD PTR [160+esp]                      ;
        imul      edi, esi                                      ;
        mov       edx, DWORD PTR [32+esp]                       ;310.116
        shl       edx, 2                                        ;
        neg       edx                                           ;
        add       edx, DWORD PTR [184+esp]                      ;
        mov       ecx, DWORD PTR [88+esp]                       ;310.99
        sub       edx, edi                                      ;
        mov       edi, DWORD PTR [156+esp]                      ;
        imul      edi, ecx                                      ;
        mov       eax, DWORD PTR [80+esp]                       ;310.99
        shl       eax, 2                                        ;
        neg       eax                                           ;
        add       eax, DWORD PTR [196+esp]                      ;
        mov       DWORD PTR [132+esp], esi                      ;
        sub       eax, edi                                      ;
        mov       DWORD PTR [128+esp], ecx                      ;
        mov       DWORD PTR [136+esp], esi                      ;
        mov       DWORD PTR [164+esp], eax                      ;
        mov       DWORD PTR [144+esp], edx                      ;
        mov       DWORD PTR [140+esp], ecx                      ;
        mov       esi, DWORD PTR [128+esp]                      ;
        mov       edi, DWORD PTR [132+esp]                      ;
                                ; LOE ebx esi edi
.B10.47:                        ; Preds .B10.50 .B10.46
        cvtsi2ss  xmm0, ebx                                     ;310.6
        mov       DWORD PTR [96+esp], 0                         ;310.6
        movss     DWORD PTR [224+esp], xmm0                     ;310.6
        push      32                                            ;310.6
        push      OFFSET FLAT: DYNUST_FUEL_MODULE_mp_WRITEOUTFUELCONSUM$format_pack.0.10+300 ;310.6
        lea       eax, DWORD PTR [232+esp]                      ;310.6
        push      eax                                           ;310.6
        push      OFFSET FLAT: __STRLITPACK_122.0.10            ;310.6
        push      -2088435968                                   ;310.6
        push      6060                                          ;310.6
        lea       edx, DWORD PTR [120+esp]                      ;310.6
        push      edx                                           ;310.6
        call      _for_write_seq_fmt                            ;310.6
                                ; LOE ebx esi edi
.B10.48:                        ; Preds .B10.47
        mov       eax, DWORD PTR [192+esp]                      ;310.6
        lea       ecx, DWORD PTR [260+esp]                      ;310.6
        mov       edx, DWORD PTR [8+esi+eax]                    ;310.6
        mov       DWORD PTR [260+esp], edx                      ;310.6
        push      ecx                                           ;310.6
        push      OFFSET FLAT: __STRLITPACK_123.0.10            ;310.6
        lea       eax, DWORD PTR [132+esp]                      ;310.6
        push      eax                                           ;310.6
        call      _for_write_seq_fmt_xmit                       ;310.6
                                ; LOE ebx esi edi
.B10.49:                        ; Preds .B10.48
        mov       eax, DWORD PTR [184+esp]                      ;310.6
        lea       ecx, DWORD PTR [280+esp]                      ;310.6
        mov       edx, DWORD PTR [8+edi+eax]                    ;310.6
        mov       DWORD PTR [280+esp], edx                      ;310.6
        push      ecx                                           ;310.6
        push      OFFSET FLAT: __STRLITPACK_124.0.10            ;310.6
        lea       eax, DWORD PTR [144+esp]                      ;310.6
        push      eax                                           ;310.6
        call      _for_write_seq_fmt_xmit                       ;310.6
                                ; LOE ebx esi edi
.B10.141:                       ; Preds .B10.49
        add       esp, 52                                       ;310.6
                                ; LOE ebx esi edi
.B10.50:                        ; Preds .B10.141
        inc       ebx                                           ;311.4
        add       esi, DWORD PTR [140+esp]                      ;311.4
        add       edi, DWORD PTR [136+esp]                      ;311.4
        cmp       ebx, DWORD PTR [172+esp]                      ;311.4
        jle       .B10.47       ; Prob 82%                      ;311.4
                                ; LOE ebx esi edi
.B10.52:                        ; Preds .B10.50 .B10.45
        xor       eax, eax                                      ;312.4
        mov       DWORD PTR [96+esp], eax                       ;312.4
        push      32                                            ;312.4
        push      eax                                           ;312.4
        push      OFFSET FLAT: __STRLITPACK_125.0.10            ;312.4
        push      -2088435968                                   ;312.4
        push      6060                                          ;312.4
        lea       edx, DWORD PTR [116+esp]                      ;312.4
        push      edx                                           ;312.4
        call      _for_write_seq_lis                            ;312.4
                                ; LOE
.B10.142:                       ; Preds .B10.52
        add       esp, 24                                       ;312.4
                                ; LOE
.B10.53:                        ; Preds .B10.142
        cmp       DWORD PTR [180+esp], 0                        ;313.82
        jle       .B10.60       ; Prob 50%                      ;313.82
                                ; LOE
.B10.54:                        ; Preds .B10.53
        mov       edx, DWORD PTR [180+esp]                      ;313.82
        mov       eax, edx                                      ;313.82
        shr       eax, 31                                       ;313.82
        xor       ecx, ecx                                      ;
        add       eax, edx                                      ;313.82
        sar       eax, 1                                        ;313.82
        mov       esi, DWORD PTR [80+esp]                       ;313.82
        test      eax, eax                                      ;313.82
        mov       ebx, DWORD PTR [88+esp]                       ;313.82
        jbe       .B10.88       ; Prob 10%                      ;313.82
                                ; LOE eax ecx ebx esi
.B10.55:                        ; Preds .B10.54
        mov       DWORD PTR [140+esp], ecx                      ;
        lea       edi, DWORD PTR [esi*4]                        ;
        mov       ecx, DWORD PTR [156+esp]                      ;
        neg       edi                                           ;
        imul      ecx, ebx                                      ;
        xor       edx, edx                                      ;
        add       edi, DWORD PTR [196+esp]                      ;
        mov       DWORD PTR [136+esp], edi                      ;
        sub       edi, ecx                                      ;
        add       ecx, ebx                                      ;
        add       edi, ecx                                      ;
        mov       DWORD PTR [144+esp], edx                      ;
        mov       DWORD PTR [132+esp], edi                      ;
        mov       DWORD PTR [164+esp], eax                      ;
        mov       DWORD PTR [172+esp], ebx                      ;
        mov       DWORD PTR [128+esp], esi                      ;
        mov       edi, DWORD PTR [144+esp]                      ;
        mov       ecx, DWORD PTR [140+esp]                      ;
        mov       eax, edx                                      ;
        mov       ebx, DWORD PTR [132+esp]                      ;
        mov       esi, DWORD PTR [136+esp]                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.56:                        ; Preds .B10.56 .B10.55
        inc       edi                                           ;313.82
        add       ecx, DWORD PTR [8+esi+eax*2]                  ;313.82
        add       edx, DWORD PTR [8+ebx+eax*2]                  ;313.82
        add       eax, DWORD PTR [172+esp]                      ;313.82
        cmp       edi, DWORD PTR [164+esp]                      ;313.82
        jb        .B10.56       ; Prob 63%                      ;313.82
                                ; LOE eax edx ecx ebx esi edi
.B10.57:                        ; Preds .B10.56
        mov       ebx, DWORD PTR [172+esp]                      ;
        add       ecx, edx                                      ;313.82
        mov       esi, DWORD PTR [128+esp]                      ;
        lea       edx, DWORD PTR [1+edi+edi]                    ;313.82
                                ; LOE edx ecx ebx esi
.B10.58:                        ; Preds .B10.57 .B10.88
        lea       eax, DWORD PTR [-1+edx]                       ;313.82
        cmp       eax, DWORD PTR [180+esp]                      ;313.82
        jae       .B10.61       ; Prob 10%                      ;313.82
                                ; LOE edx ecx ebx esi
.B10.59:                        ; Preds .B10.58
        mov       edi, DWORD PTR [156+esp]                      ;313.82
        mov       eax, edi                                      ;313.82
        neg       eax                                           ;313.82
        shl       esi, 2                                        ;313.82
        lea       edx, DWORD PTR [-1+edi+edx]                   ;313.82
        add       eax, edx                                      ;313.82
        neg       esi                                           ;313.82
        imul      eax, ebx                                      ;313.82
        add       esi, DWORD PTR [196+esp]                      ;313.82
        add       ecx, DWORD PTR [8+esi+eax]                    ;313.82
        jmp       .B10.61       ; Prob 100%                     ;313.82
                                ; LOE ecx
.B10.60:                        ; Preds .B10.53
        xor       ecx, ecx                                      ;
                                ; LOE ecx
.B10.61:                        ; Preds .B10.59 .B10.58 .B10.60
        mov       DWORD PTR [96+esp], 0                         ;313.4
        lea       eax, DWORD PTR [136+esp]                      ;313.4
        mov       DWORD PTR [136+esp], ecx                      ;313.4
        push      32                                            ;313.4
        push      OFFSET FLAT: DYNUST_FUEL_MODULE_mp_WRITEOUTFUELCONSUM$format_pack.0.10+400 ;313.4
        push      eax                                           ;313.4
        push      OFFSET FLAT: __STRLITPACK_126.0.10            ;313.4
        push      -2088435968                                   ;313.4
        push      6060                                          ;313.4
        lea       edx, DWORD PTR [120+esp]                      ;313.4
        push      edx                                           ;313.4
        call      _for_write_seq_fmt                            ;313.4
                                ; LOE
.B10.143:                       ; Preds .B10.61
        add       esp, 28                                       ;313.4
                                ; LOE
.B10.62:                        ; Preds .B10.143
        cmp       DWORD PTR [188+esp], 0                        ;313.104
        jle       .B10.69       ; Prob 50%                      ;313.104
                                ; LOE
.B10.63:                        ; Preds .B10.62
        mov       eax, DWORD PTR [188+esp]                      ;313.104
        mov       ebx, eax                                      ;313.104
        shr       ebx, 31                                       ;313.104
        add       ebx, eax                                      ;313.104
        sar       ebx, 1                                        ;313.104
        mov       edx, DWORD PTR [32+esp]                       ;313.104
        test      ebx, ebx                                      ;313.104
        mov       esi, DWORD PTR [40+esp]                       ;313.104
        movss     xmm0, DWORD PTR [192+esp]                     ;
        jbe       .B10.89       ; Prob 10%                      ;313.104
                                ; LOE edx ebx esi xmm0
.B10.64:                        ; Preds .B10.63
        mov       eax, DWORD PTR [160+esp]                      ;
        lea       edi, DWORD PTR [edx*4]                        ;
        imul      eax, esi                                      ;
        neg       edi                                           ;
        movss     xmm1, DWORD PTR [192+esp]                     ;
        add       edi, DWORD PTR [184+esp]                      ;
        xor       ecx, ecx                                      ;
        mov       DWORD PTR [132+esp], edi                      ;
        sub       edi, eax                                      ;
        add       eax, esi                                      ;
        add       edi, eax                                      ;
        xor       eax, eax                                      ;
        mov       DWORD PTR [128+esp], edx                      ;
        mov       edx, edi                                      ;
        mov       edi, DWORD PTR [132+esp]                      ;
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1
.B10.65:                        ; Preds .B10.65 .B10.64
        inc       ecx                                           ;313.104
        addss     xmm0, DWORD PTR [8+edi+eax*2]                 ;313.104
        addss     xmm1, DWORD PTR [8+edx+eax*2]                 ;313.104
        add       eax, esi                                      ;313.104
        cmp       ecx, ebx                                      ;313.104
        jb        .B10.65       ; Prob 63%                      ;313.104
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1
.B10.66:                        ; Preds .B10.65
        movss     DWORD PTR [192+esp], xmm1                     ;
        lea       ecx, DWORD PTR [1+ecx+ecx]                    ;313.104
        mov       edx, DWORD PTR [128+esp]                      ;
        addss     xmm0, DWORD PTR [192+esp]                     ;313.104
                                ; LOE edx ecx esi xmm0
.B10.67:                        ; Preds .B10.66 .B10.89
        lea       eax, DWORD PTR [-1+ecx]                       ;313.104
        cmp       eax, DWORD PTR [188+esp]                      ;313.104
        jae       .B10.70       ; Prob 10%                      ;313.104
                                ; LOE edx ecx esi xmm0
.B10.68:                        ; Preds .B10.67
        mov       eax, DWORD PTR [160+esp]                      ;313.104
        mov       ebx, eax                                      ;313.104
        neg       ebx                                           ;313.104
        shl       edx, 2                                        ;313.104
        lea       ecx, DWORD PTR [-1+eax+ecx]                   ;313.104
        add       ebx, ecx                                      ;313.104
        neg       edx                                           ;313.104
        imul      ebx, esi                                      ;313.104
        add       edx, DWORD PTR [184+esp]                      ;313.104
        addss     xmm0, DWORD PTR [8+edx+ebx]                   ;313.104
        jmp       .B10.70       ; Prob 100%                     ;313.104
                                ; LOE xmm0
.B10.69:                        ; Preds .B10.62
        movss     xmm0, DWORD PTR [192+esp]                     ;
                                ; LOE xmm0
.B10.70:                        ; Preds .B10.68 .B10.67 .B10.69
        movss     DWORD PTR [128+esp], xmm0                     ;313.4
        lea       eax, DWORD PTR [128+esp]                      ;313.4
        push      eax                                           ;313.4
        push      OFFSET FLAT: __STRLITPACK_127.0.10            ;313.4
        lea       edx, DWORD PTR [104+esp]                      ;313.4
        push      edx                                           ;313.4
        call      _for_write_seq_fmt_xmit                       ;313.4
                                ; LOE
.B10.71:                        ; Preds .B10.70
        mov       ebx, DWORD PTR [24+esp]                       ;314.4
        mov       edx, ebx                                      ;314.4
        shr       edx, 1                                        ;314.4
        mov       eax, ebx                                      ;314.4
        and       edx, 1                                        ;314.4
        and       eax, 1                                        ;314.4
        shl       edx, 2                                        ;314.4
        add       eax, eax                                      ;314.4
        or        edx, eax                                      ;314.4
        or        edx, 262144                                   ;314.4
        push      edx                                           ;314.4
        push      DWORD PTR [200+esp]                           ;314.4
        call      _for_dealloc_allocatable                      ;314.4
                                ; LOE ebx
.B10.72:                        ; Preds .B10.71
        and       ebx, -2                                       ;314.4
        mov       DWORD PTR [32+esp], ebx                       ;314.4
        mov       ebx, DWORD PTR [80+esp]                       ;315.4
        mov       edx, ebx                                      ;315.4
        shr       edx, 1                                        ;315.4
        mov       eax, ebx                                      ;315.4
        and       edx, 1                                        ;315.4
        and       eax, 1                                        ;315.4
        shl       edx, 2                                        ;315.4
        add       eax, eax                                      ;315.4
        or        edx, eax                                      ;315.4
        or        edx, 262144                                   ;315.4
        mov       DWORD PTR [20+esp], 0                         ;314.4
        push      edx                                           ;315.4
        push      DWORD PTR [220+esp]                           ;315.4
        call      _for_dealloc_allocatable                      ;315.4
                                ; LOE ebx
.B10.73:                        ; Preds .B10.72
        xor       eax, eax                                      ;315.4
        and       ebx, -2                                       ;315.4
        mov       DWORD PTR [76+esp], eax                       ;315.4
        mov       DWORD PTR [88+esp], ebx                       ;315.4
        mov       DWORD PTR [124+esp], eax                      ;316.10
        push      32                                            ;316.10
        push      eax                                           ;316.10
        push      OFFSET FLAT: __STRLITPACK_128.0.10            ;316.10
        push      -2088435968                                   ;316.10
        push      6060                                          ;316.10
        lea       edx, DWORD PTR [144+esp]                      ;316.10
        push      edx                                           ;316.10
        call      _for_close                                    ;316.10
                                ; LOE
.B10.74:                        ; Preds .B10.73
        xor       eax, eax                                      ;318.10
        mov       DWORD PTR [148+esp], eax                      ;318.10
        push      32                                            ;318.10
        push      eax                                           ;318.10
        push      OFFSET FLAT: __STRLITPACK_129.0.10            ;318.10
        push      -2088435968                                   ;318.10
        push      6058                                          ;318.10
        lea       edx, DWORD PTR [168+esp]                      ;318.10
        push      edx                                           ;318.10
        call      _for_close                                    ;318.10
                                ; LOE
.B10.144:                       ; Preds .B10.74
        add       esp, 76                                       ;318.10
                                ; LOE
.B10.75:                        ; Preds .B10.144
        mov       ebx, DWORD PTR [60+esp]                       ;319.1
        test      bl, 1                                         ;319.1
        jne       .B10.92       ; Prob 3%                       ;319.1
                                ; LOE ebx
.B10.76:                        ; Preds .B10.75 .B10.93
        mov       ebx, DWORD PTR [12+esp]                       ;319.1
        test      bl, 1                                         ;319.1
        jne       .B10.90       ; Prob 3%                       ;319.1
                                ; LOE ebx
.B10.77:                        ; Preds .B10.76
        add       esp, 244                                      ;319.1
        pop       ebx                                           ;319.1
        pop       edi                                           ;319.1
        pop       esi                                           ;319.1
        mov       esp, ebp                                      ;319.1
        pop       ebp                                           ;319.1
        ret                                                     ;319.1
                                ; LOE
.B10.78:                        ; Preds .B10.8
        mov       esi, ebx                                      ;289.3
        xor       edx, edx                                      ;
        and       esi, -8                                       ;289.3
        mov       DWORD PTR [120+esp], esi                      ;289.3
        pxor      xmm0, xmm0                                    ;289.3
        mov       esi, DWORD PTR [160+esp]                      ;
        imul      esi, ecx                                      ;
        mov       edi, DWORD PTR [184+esp]                      ;
        mov       DWORD PTR [104+esp], edx                      ;
        mov       edx, edi                                      ;
        shl       eax, 2                                        ;
        sub       edx, eax                                      ;
        sub       eax, esi                                      ;
        add       edx, eax                                      ;
        add       edx, esi                                      ;
        lea       esi, DWORD PTR [ebx*4]                        ;
        mov       DWORD PTR [100+esp], edi                      ;
        mov       DWORD PTR [96+esp], edx                       ;
        mov       eax, DWORD PTR [104+esp]                      ;
        mov       DWORD PTR [136+esp], ebx                      ;
        mov       DWORD PTR [108+esp], esi                      ;
        mov       esi, eax                                      ;
        mov       DWORD PTR [112+esp], edx                      ;
        mov       DWORD PTR [116+esp], ecx                      ;
        mov       ebx, DWORD PTR [96+esp]                       ;
        mov       edi, DWORD PTR [100+esp]                      ;
                                ; LOE eax ebx esi edi
.B10.79:                        ; Preds .B10.81 .B10.102 .B10.78
        cmp       DWORD PTR [136+esp], 24                       ;289.3
        jle       .B10.95       ; Prob 0%                       ;289.3
                                ; LOE eax ebx esi edi
.B10.80:                        ; Preds .B10.79
        push      DWORD PTR [108+esp]                           ;289.3
        push      0                                             ;289.3
        push      ebx                                           ;289.3
        mov       DWORD PTR [144+esp], eax                      ;289.3
        call      __intel_fast_memset                           ;289.3
                                ; LOE ebx esi edi
.B10.145:                       ; Preds .B10.80
        mov       eax, DWORD PTR [144+esp]                      ;
        add       esp, 12                                       ;289.3
                                ; LOE eax ebx esi edi al ah
.B10.81:                        ; Preds .B10.99 .B10.145
        inc       esi                                           ;289.3
        mov       edx, DWORD PTR [116+esp]                      ;289.3
        add       edi, edx                                      ;289.3
        add       ebx, edx                                      ;289.3
        add       eax, edx                                      ;289.3
        cmp       esi, DWORD PTR [188+esp]                      ;289.3
        jb        .B10.79       ; Prob 82%                      ;289.3
        jmp       .B10.9        ; Prob 100%                     ;289.3
                                ; LOE eax ebx esi edi
.B10.82:                        ; Preds .B10.6
        mov       esi, DWORD PTR [156+esp]                      ;
        xor       edi, edi                                      ;
        imul      esi, edx                                      ;
        pxor      xmm0, xmm0                                    ;288.3
        mov       ebx, DWORD PTR [196+esp]                      ;
        mov       DWORD PTR [104+esp], edi                      ;
        mov       edi, ebx                                      ;
        shl       eax, 2                                        ;
        sub       edi, eax                                      ;
        sub       eax, esi                                      ;
        add       edi, eax                                      ;
        mov       DWORD PTR [100+esp], edx                      ;
        lea       eax, DWORD PTR [ecx*4]                        ;
        mov       DWORD PTR [96+esp], eax                       ;
        add       edi, esi                                      ;
        mov       esi, DWORD PTR [104+esp]                      ;
        mov       DWORD PTR [112+esp], ecx                      ;
                                ; LOE ebx esi edi
.B10.83:                        ; Preds .B10.85 .B10.123 .B10.82
        cmp       DWORD PTR [112+esp], 24                       ;288.3
        jle       .B10.108      ; Prob 0%                       ;288.3
                                ; LOE ebx esi edi
.B10.84:                        ; Preds .B10.83
        push      DWORD PTR [96+esp]                            ;288.3
        push      0                                             ;288.3
        push      edi                                           ;288.3
        call      __intel_fast_memset                           ;288.3
                                ; LOE ebx esi edi
.B10.146:                       ; Preds .B10.84
        add       esp, 12                                       ;288.3
                                ; LOE ebx esi edi
.B10.85:                        ; Preds .B10.120 .B10.146
        mov       edx, DWORD PTR [104+esp]                      ;288.3
        inc       edx                                           ;288.3
        mov       eax, DWORD PTR [100+esp]                      ;288.3
        add       ebx, eax                                      ;288.3
        add       edi, eax                                      ;288.3
        add       esi, eax                                      ;288.3
        mov       DWORD PTR [104+esp], edx                      ;288.3
        cmp       edx, DWORD PTR [180+esp]                      ;288.3
        jb        .B10.83       ; Prob 82%                      ;288.3
        jmp       .B10.7        ; Prob 100%                     ;288.3
                                ; LOE ebx esi edi
.B10.87:                        ; Preds .B10.35                 ; Infreq
        mov       ecx, 1                                        ;
        jmp       .B10.39       ; Prob 100%                     ;
                                ; LOE edx ecx esi xmm0
.B10.88:                        ; Preds .B10.54                 ; Infreq
        mov       edx, 1                                        ;
        jmp       .B10.58       ; Prob 100%                     ;
                                ; LOE edx ecx ebx esi
.B10.89:                        ; Preds .B10.63                 ; Infreq
        mov       ecx, 1                                        ;
        jmp       .B10.67       ; Prob 100%                     ;
                                ; LOE edx ecx esi xmm0
.B10.90:                        ; Preds .B10.76                 ; Infreq
        mov       edx, ebx                                      ;319.1
        mov       eax, ebx                                      ;319.1
        shr       edx, 1                                        ;319.1
        and       eax, 1                                        ;319.1
        and       edx, 1                                        ;319.1
        add       eax, eax                                      ;319.1
        shl       edx, 2                                        ;319.1
        or        edx, eax                                      ;319.1
        or        edx, 262144                                   ;319.1
        push      edx                                           ;319.1
        push      DWORD PTR [4+esp]                             ;319.1
        call      _for_dealloc_allocatable                      ;319.1
                                ; LOE ebx
.B10.147:                       ; Preds .B10.90                 ; Infreq
        add       esp, 8                                        ;319.1
                                ; LOE ebx
.B10.91:                        ; Preds .B10.147                ; Infreq
        and       ebx, -2                                       ;319.1
        mov       DWORD PTR [esp], 0                            ;319.1
        mov       DWORD PTR [12+esp], ebx                       ;319.1
        add       esp, 244                                      ;319.1
        pop       ebx                                           ;319.1
        pop       edi                                           ;319.1
        pop       esi                                           ;319.1
        mov       esp, ebp                                      ;319.1
        pop       ebp                                           ;319.1
        ret                                                     ;319.1
                                ; LOE
.B10.92:                        ; Preds .B10.75                 ; Infreq
        mov       edx, ebx                                      ;319.1
        mov       eax, ebx                                      ;319.1
        shr       edx, 1                                        ;319.1
        and       eax, 1                                        ;319.1
        and       edx, 1                                        ;319.1
        add       eax, eax                                      ;319.1
        shl       edx, 2                                        ;319.1
        or        edx, eax                                      ;319.1
        or        edx, 262144                                   ;319.1
        push      edx                                           ;319.1
        push      DWORD PTR [52+esp]                            ;319.1
        call      _for_dealloc_allocatable                      ;319.1
                                ; LOE ebx
.B10.148:                       ; Preds .B10.92                 ; Infreq
        add       esp, 8                                        ;319.1
                                ; LOE ebx
.B10.93:                        ; Preds .B10.148                ; Infreq
        and       ebx, -2                                       ;319.1
        mov       DWORD PTR [48+esp], 0                         ;319.1
        mov       DWORD PTR [60+esp], ebx                       ;319.1
        jmp       .B10.76       ; Prob 100%                     ;319.1
                                ; LOE
.B10.95:                        ; Preds .B10.79                 ; Infreq
        cmp       DWORD PTR [136+esp], 8                        ;289.3
        jl        .B10.106      ; Prob 10%                      ;289.3
                                ; LOE eax ebx esi edi
.B10.96:                        ; Preds .B10.95                 ; Infreq
        xor       ecx, ecx                                      ;289.3
        mov       DWORD PTR [128+esp], ecx                      ;289.3
        pxor      xmm0, xmm0                                    ;
        mov       ecx, DWORD PTR [112+esp]                      ;
        mov       edx, DWORD PTR [120+esp]                      ;289.3
        mov       DWORD PTR [96+esp], ebx                       ;
        mov       DWORD PTR [104+esp], esi                      ;
        add       ecx, eax                                      ;
        mov       DWORD PTR [124+esp], ecx                      ;
        mov       ecx, DWORD PTR [184+esp]                      ;
        mov       ebx, DWORD PTR [124+esp]                      ;
        mov       esi, DWORD PTR [128+esp]                      ;
        mov       DWORD PTR [100+esp], edi                      ;
        add       ecx, eax                                      ;
        mov       edi, edx                                      ;
                                ; LOE eax edx ecx ebx esi edi xmm0
.B10.97:                        ; Preds .B10.97 .B10.96         ; Infreq
        movups    XMMWORD PTR [ecx+esi*4], xmm0                 ;289.3
        movups    XMMWORD PTR [16+ebx+esi*4], xmm0              ;289.3
        add       esi, 8                                        ;289.3
        cmp       esi, edi                                      ;289.3
        jb        .B10.97       ; Prob 82%                      ;289.3
                                ; LOE eax edx ecx ebx esi edi xmm0
.B10.98:                        ; Preds .B10.97                 ; Infreq
        mov       ebx, DWORD PTR [96+esp]                       ;
        mov       edi, DWORD PTR [100+esp]                      ;
        mov       esi, DWORD PTR [104+esp]                      ;
                                ; LOE eax edx ebx esi edi
.B10.99:                        ; Preds .B10.98 .B10.106        ; Infreq
        cmp       edx, DWORD PTR [136+esp]                      ;289.3
        jae       .B10.81       ; Prob 10%                      ;289.3
                                ; LOE eax edx ebx esi edi
.B10.100:                       ; Preds .B10.99                 ; Infreq
        movss     xmm0, DWORD PTR [192+esp]                     ;
        mov       ecx, DWORD PTR [136+esp]                      ;
                                ; LOE eax edx ecx ebx esi edi xmm0
.B10.101:                       ; Preds .B10.101 .B10.100       ; Infreq
        movss     DWORD PTR [edi+edx*4], xmm0                   ;289.3
        inc       edx                                           ;289.3
        cmp       edx, ecx                                      ;289.3
        jb        .B10.101      ; Prob 82%                      ;289.3
                                ; LOE eax edx ecx ebx esi edi xmm0
.B10.102:                       ; Preds .B10.101                ; Infreq
        inc       esi                                           ;289.3
        mov       edx, DWORD PTR [116+esp]                      ;289.3
        add       edi, edx                                      ;289.3
        add       ebx, edx                                      ;289.3
        add       eax, edx                                      ;289.3
        cmp       esi, DWORD PTR [188+esp]                      ;289.3
        jb        .B10.79       ; Prob 82%                      ;289.3
        jmp       .B10.9        ; Prob 100%                     ;289.3
                                ; LOE eax ebx esi edi
.B10.106:                       ; Preds .B10.95                 ; Infreq
        xor       edx, edx                                      ;289.3
        jmp       .B10.99       ; Prob 100%                     ;289.3
                                ; LOE eax edx ebx esi edi
.B10.107:                       ; Preds .B10.7                  ; Infreq
        pxor      xmm0, xmm0                                    ;306.104
        movss     DWORD PTR [192+esp], xmm0                     ;306.104
        jmp       .B10.9        ; Prob 100%                     ;306.104
                                ; LOE
.B10.108:                       ; Preds .B10.83                 ; Infreq
        cmp       DWORD PTR [112+esp], 4                        ;288.3
        jl        .B10.125      ; Prob 10%                      ;288.3
                                ; LOE ebx esi edi
.B10.109:                       ; Preds .B10.108                ; Infreq
        mov       eax, DWORD PTR [196+esp]                      ;288.3
        add       eax, esi                                      ;288.3
        and       eax, 15                                       ;288.3
        je        .B10.112      ; Prob 50%                      ;288.3
                                ; LOE eax ebx esi edi
.B10.110:                       ; Preds .B10.109                ; Infreq
        test      al, 3                                         ;288.3
        jne       .B10.125      ; Prob 10%                      ;288.3
                                ; LOE eax ebx esi edi
.B10.111:                       ; Preds .B10.110                ; Infreq
        neg       eax                                           ;288.3
        add       eax, 16                                       ;288.3
        shr       eax, 2                                        ;288.3
                                ; LOE eax ebx esi edi
.B10.112:                       ; Preds .B10.111 .B10.109       ; Infreq
        lea       edx, DWORD PTR [4+eax]                        ;288.3
        cmp       edx, DWORD PTR [112+esp]                      ;288.3
        jg        .B10.125      ; Prob 10%                      ;288.3
                                ; LOE eax ebx esi edi
.B10.113:                       ; Preds .B10.112                ; Infreq
        mov       edx, DWORD PTR [112+esp]                      ;288.3
        mov       ecx, edx                                      ;288.3
        sub       ecx, eax                                      ;288.3
        and       ecx, 3                                        ;288.3
        neg       ecx                                           ;288.3
        add       ecx, edx                                      ;288.3
        mov       edx, DWORD PTR [196+esp]                      ;
        test      eax, eax                                      ;288.3
        mov       DWORD PTR [108+esp], ecx                      ;288.3
        lea       ecx, DWORD PTR [edx+esi]                      ;
        mov       DWORD PTR [116+esp], ecx                      ;
        jbe       .B10.117      ; Prob 10%                      ;288.3
                                ; LOE eax ecx ebx esi edi cl ch
.B10.114:                       ; Preds .B10.113                ; Infreq
        xor       edx, edx                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.115:                       ; Preds .B10.115 .B10.114       ; Infreq
        mov       DWORD PTR [ecx+edx*4], 0                      ;288.3
        inc       edx                                           ;288.3
        cmp       edx, eax                                      ;288.3
        jb        .B10.115      ; Prob 82%                      ;288.3
                                ; LOE eax edx ecx ebx esi edi
.B10.117:                       ; Preds .B10.115 .B10.113       ; Infreq
        mov       edx, DWORD PTR [108+esp]                      ;
        mov       ecx, DWORD PTR [116+esp]                      ;
        pxor      xmm0, xmm0                                    ;
                                ; LOE eax edx ecx ebx esi edi xmm0
.B10.118:                       ; Preds .B10.118 .B10.117       ; Infreq
        movdqa    XMMWORD PTR [ecx+eax*4], xmm0                 ;288.3
        add       eax, 4                                        ;288.3
        cmp       eax, edx                                      ;288.3
        jb        .B10.118      ; Prob 82%                      ;288.3
                                ; LOE eax edx ecx ebx esi edi xmm0
.B10.119:                       ; Preds .B10.118                ; Infreq
        mov       DWORD PTR [108+esp], edx                      ;
                                ; LOE ebx esi edi
.B10.120:                       ; Preds .B10.119 .B10.125       ; Infreq
        mov       eax, DWORD PTR [108+esp]                      ;288.3
        cmp       eax, DWORD PTR [112+esp]                      ;288.3
        jae       .B10.85       ; Prob 10%                      ;288.3
                                ; LOE eax ebx esi edi al ah
.B10.121:                       ; Preds .B10.120                ; Infreq
        mov       edx, DWORD PTR [112+esp]                      ;
                                ; LOE eax edx ebx esi edi
.B10.122:                       ; Preds .B10.122 .B10.121       ; Infreq
        mov       DWORD PTR [ebx+eax*4], 0                      ;288.3
        inc       eax                                           ;288.3
        cmp       eax, edx                                      ;288.3
        jb        .B10.122      ; Prob 82%                      ;288.3
                                ; LOE eax edx ebx esi edi
.B10.123:                       ; Preds .B10.122                ; Infreq
        mov       edx, DWORD PTR [104+esp]                      ;288.3
        inc       edx                                           ;288.3
        mov       eax, DWORD PTR [100+esp]                      ;288.3
        add       ebx, eax                                      ;288.3
        add       edi, eax                                      ;288.3
        add       esi, eax                                      ;288.3
        mov       DWORD PTR [104+esp], edx                      ;288.3
        cmp       edx, DWORD PTR [180+esp]                      ;288.3
        jb        .B10.83       ; Prob 82%                      ;288.3
        jmp       .B10.7        ; Prob 100%                     ;288.3
                                ; LOE ebx esi edi
.B10.125:                       ; Preds .B10.108 .B10.112 .B10.110 ; Infreq
        xor       eax, eax                                      ;288.3
        mov       DWORD PTR [108+esp], eax                      ;288.3
        jmp       .B10.120      ; Prob 100%                     ;288.3
                                ; LOE ebx esi edi
.B10.130:                       ; Preds .B10.26                 ; Infreq
        mov       edx, 1                                        ;
        jmp       .B10.30       ; Prob 100%                     ;
        ALIGN     16
                                ; LOE edx ecx ebx esi
; mark_end;
_DYNUST_FUEL_MODULE_mp_WRITEOUTFUELCONSUM ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
DYNUST_FUEL_MODULE_mp_WRITEOUTFUELCONSUM$format_pack.0.10	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	38
	DB	0
	DB	32
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	32
	DB	70
	DB	85
	DB	69
	DB	76
	DB	32
	DB	67
	DB	79
	DB	78
	DB	83
	DB	85
	DB	77
	DB	80
	DB	84
	DB	73
	DB	79
	DB	78
	DB	32
	DB	82
	DB	69
	DB	80
	DB	79
	DB	82
	DB	84
	DB	32
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	32
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	14
	DB	0
	DB	45
	DB	45
	DB	45
	DB	45
	DB	32
	DB	65
	DB	117
	DB	116
	DB	111
	DB	32
	DB	45
	DB	45
	DB	45
	DB	45
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	12
	DB	0
	DB	84
	DB	105
	DB	109
	DB	101
	DB	32
	DB	40
	DB	109
	DB	105
	DB	110
	DB	41
	DB	58
	DB	32
	DB	33
	DB	0
	DB	0
	DB	1
	DB	1
	DB	0
	DB	0
	DB	0
	DB	5
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	9
	DB	0
	DB	32
	DB	35
	DB	32
	DB	111
	DB	102
	DB	32
	DB	86
	DB	101
	DB	104
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	20
	DB	0
	DB	32
	DB	32
	DB	70
	DB	117
	DB	101
	DB	108
	DB	32
	DB	67
	DB	111
	DB	110
	DB	115
	DB	117
	DB	109
	DB	101
	DB	100
	DB	40
	DB	71
	DB	41
	DB	58
	DB	32
	DB	33
	DB	0
	DB	0
	DB	3
	DB	1
	DB	0
	DB	0
	DB	0
	DB	12
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	22
	DB	0
	DB	84
	DB	111
	DB	116
	DB	97
	DB	108
	DB	32
	DB	35
	DB	32
	DB	111
	DB	102
	DB	32
	DB	97
	DB	117
	DB	116
	DB	111
	DB	115
	DB	58
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	9
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	25
	DB	0
	DB	32
	DB	84
	DB	111
	DB	116
	DB	97
	DB	108
	DB	32
	DB	70
	DB	117
	DB	101
	DB	108
	DB	32
	DB	67
	DB	111
	DB	110
	DB	115
	DB	117
	DB	109
	DB	112
	DB	116
	DB	105
	DB	111
	DB	110
	DB	58
	DB	32
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	1
	DB	1
	DB	0
	DB	0
	DB	0
	DB	12
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	15
	DB	0
	DB	45
	DB	45
	DB	45
	DB	45
	DB	32
	DB	84
	DB	114
	DB	117
	DB	99
	DB	107
	DB	32
	DB	45
	DB	45
	DB	45
	DB	45
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	12
	DB	0
	DB	84
	DB	105
	DB	109
	DB	101
	DB	32
	DB	40
	DB	109
	DB	105
	DB	110
	DB	41
	DB	58
	DB	32
	DB	33
	DB	0
	DB	0
	DB	1
	DB	1
	DB	0
	DB	0
	DB	0
	DB	5
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	9
	DB	0
	DB	32
	DB	35
	DB	32
	DB	111
	DB	102
	DB	32
	DB	86
	DB	101
	DB	104
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	20
	DB	0
	DB	32
	DB	32
	DB	70
	DB	117
	DB	101
	DB	108
	DB	32
	DB	67
	DB	111
	DB	110
	DB	115
	DB	117
	DB	109
	DB	101
	DB	100
	DB	40
	DB	71
	DB	41
	DB	58
	DB	32
	DB	33
	DB	0
	DB	0
	DB	3
	DB	1
	DB	0
	DB	0
	DB	0
	DB	12
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	22
	DB	0
	DB	84
	DB	111
	DB	116
	DB	97
	DB	108
	DB	32
	DB	35
	DB	32
	DB	111
	DB	102
	DB	32
	DB	116
	DB	114
	DB	117
	DB	99
	DB	107
	DB	115
	DB	58
	DB	32
	DB	32
	DB	32
	DB	32
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	9
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	25
	DB	0
	DB	32
	DB	84
	DB	111
	DB	116
	DB	97
	DB	108
	DB	32
	DB	70
	DB	117
	DB	101
	DB	108
	DB	32
	DB	67
	DB	111
	DB	110
	DB	115
	DB	117
	DB	109
	DB	112
	DB	116
	DB	105
	DB	111
	DB	110
	DB	58
	DB	32
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	1
	DB	1
	DB	0
	DB	0
	DB	0
	DB	12
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
_DATA	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_112.0.10	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_113.0.10	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_114.0.10	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_115.0.10	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_116.0.10	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_117.0.10	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_118.0.10	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_119.0.10	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_120.0.10	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_121.0.10	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_122.0.10	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_123.0.10	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_124.0.10	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_125.0.10	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_126.0.10	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_127.0.10	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_128.0.10	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_129.0.10	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_FUEL_MODULE_mp_WRITEOUTFUELCONSUM
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_FUEL_MODULE_mp_WRITEOUTFUELCONSUM_CITY
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_FUEL_MODULE_mp_WRITEOUTFUELCONSUM_CITY
_DYNUST_FUEL_MODULE_mp_WRITEOUTFUELCONSUM_CITY	PROC NEAR 
.B11.1:                         ; Preds .B11.0
        push      ebx                                           ;322.12
        mov       ebx, esp                                      ;322.12
        and       esp, -16                                      ;322.12
        push      ebp                                           ;322.12
        push      ebp                                           ;322.12
        mov       ebp, DWORD PTR [4+ebx]                        ;322.12
        mov       DWORD PTR [4+esp], ebp                        ;322.12
        mov       ebp, esp                                      ;322.12
        sub       esp, 504                                      ;322.12
        mov       edx, 1                                        ;334.43
        mov       DWORD PTR [-304+ebp], edx                     ;334.43
        xor       ecx, ecx                                      ;335.22
        mov       DWORD PTR [-264+ebp], edx                     ;330.31
        lea       edx, DWORD PTR [-240+ebp]                     ;337.12
        push      32                                            ;337.12
        push      edx                                           ;337.12
        push      OFFSET FLAT: __STRLITPACK_130.0.11            ;337.12
        push      -2088435968                                   ;337.12
        mov       DWORD PTR [-496+ebp], ecx                     ;335.22
        mov       eax, 128                                      ;335.22
        mov       DWORD PTR [-492+ebp], ecx                     ;335.22
        mov       DWORD PTR [-488+ebp], ecx                     ;335.22
        mov       DWORD PTR [-476+ebp], ecx                     ;335.22
        mov       DWORD PTR [-472+ebp], ecx                     ;335.22
        mov       DWORD PTR [-468+ebp], ecx                     ;335.22
        mov       DWORD PTR [-464+ebp], ecx                     ;335.22
        mov       DWORD PTR [-460+ebp], ecx                     ;335.22
        mov       DWORD PTR [-456+ebp], ecx                     ;335.22
        mov       DWORD PTR [-452+ebp], ecx                     ;335.22
        mov       DWORD PTR [-448+ebp], ecx                     ;335.22
        mov       DWORD PTR [-444+ebp], ecx                     ;335.22
        mov       DWORD PTR [-440+ebp], ecx                     ;335.22
        mov       DWORD PTR [-436+ebp], ecx                     ;335.22
        mov       DWORD PTR [-432+ebp], ecx                     ;335.22
        mov       DWORD PTR [-428+ebp], ecx                     ;335.22
        mov       DWORD PTR [-320+ebp], ecx                     ;334.43
        mov       DWORD PTR [-316+ebp], ecx                     ;334.43
        mov       DWORD PTR [-312+ebp], ecx                     ;334.43
        mov       DWORD PTR [-300+ebp], ecx                     ;334.43
        mov       DWORD PTR [-296+ebp], ecx                     ;334.43
        mov       DWORD PTR [-292+ebp], ecx                     ;334.43
        mov       DWORD PTR [-288+ebp], ecx                     ;334.43
        mov       DWORD PTR [-424+ebp], ecx                     ;334.25
        mov       DWORD PTR [-420+ebp], ecx                     ;334.25
        mov       DWORD PTR [-416+ebp], ecx                     ;334.25
        mov       DWORD PTR [-404+ebp], ecx                     ;334.25
        mov       DWORD PTR [-400+ebp], ecx                     ;334.25
        mov       DWORD PTR [-396+ebp], ecx                     ;334.25
        mov       DWORD PTR [-392+ebp], ecx                     ;334.25
        mov       DWORD PTR [-388+ebp], ecx                     ;334.25
        mov       DWORD PTR [-384+ebp], ecx                     ;334.25
        mov       DWORD PTR [-380+ebp], ecx                     ;334.25
        mov       DWORD PTR [-376+ebp], ecx                     ;334.25
        mov       DWORD PTR [-372+ebp], ecx                     ;334.25
        mov       DWORD PTR [-368+ebp], ecx                     ;334.25
        mov       DWORD PTR [-364+ebp], ecx                     ;334.25
        mov       DWORD PTR [-360+ebp], ecx                     ;334.25
        mov       DWORD PTR [-356+ebp], ecx                     ;334.25
        mov       DWORD PTR [-280+ebp], ecx                     ;330.31
        mov       DWORD PTR [-276+ebp], ecx                     ;330.31
        mov       DWORD PTR [-272+ebp], ecx                     ;330.31
        mov       DWORD PTR [-260+ebp], ecx                     ;330.31
        mov       DWORD PTR [-256+ebp], ecx                     ;330.31
        mov       DWORD PTR [-252+ebp], ecx                     ;330.31
        mov       DWORD PTR [-248+ebp], ecx                     ;330.31
        mov       DWORD PTR [-40+ebp], ecx                      ;337.12
        lea       ecx, DWORD PTR [-40+ebp]                      ;337.12
        push      -1                                            ;337.12
        mov       DWORD PTR [-184+ebp], edi                     ;322.12
        mov       edi, 4                                        ;335.22
        mov       DWORD PTR [-484+ebp], eax                     ;335.22
        mov       DWORD PTR [-308+ebp], eax                     ;334.43
        mov       DWORD PTR [-412+ebp], eax                     ;334.25
        mov       DWORD PTR [-268+ebp], eax                     ;330.31
        lea       eax, DWORD PTR [-192+ebp]                     ;337.12
        push      ecx                                           ;337.12
        mov       DWORD PTR [-188+ebp], esi                     ;322.12
        mov       DWORD PTR [-480+ebp], edi                     ;335.22
        mov       DWORD PTR [-408+ebp], edi                     ;334.25
        mov       DWORD PTR [-240+ebp], 28                      ;337.12
        mov       DWORD PTR [-236+ebp], OFFSET FLAT: __STRLITPACK_28 ;337.12
        mov       DWORD PTR [-232+ebp], eax                     ;337.12
        call      _for_inquire                                  ;337.12
                                ; LOE edi
.B11.214:                       ; Preds .B11.1
        add       esp, 24                                       ;337.12
                                ; LOE edi
.B11.2:                         ; Preds .B11.214
        test      BYTE PTR [-192+ebp], 1                        ;338.6
        je        .B11.62       ; Prob 60%                      ;338.6
                                ; LOE edi
.B11.3:                         ; Preds .B11.2
        push      32                                            ;339.5
        mov       DWORD PTR [-40+ebp], 0                        ;339.5
        lea       eax, DWORD PTR [-352+ebp]                     ;339.5
        push      eax                                           ;339.5
        push      OFFSET FLAT: __STRLITPACK_131.0.11            ;339.5
        push      -2088435968                                   ;339.5
        push      6058                                          ;339.5
        mov       DWORD PTR [-352+ebp], 28                      ;339.5
        lea       edx, DWORD PTR [-40+ebp]                      ;339.5
        push      edx                                           ;339.5
        mov       DWORD PTR [-348+ebp], OFFSET FLAT: __STRLITPACK_27 ;339.5
        mov       DWORD PTR [-344+ebp], 7                       ;339.5
        mov       DWORD PTR [-340+ebp], OFFSET FLAT: __STRLITPACK_26 ;339.5
        call      _for_open                                     ;339.5
                                ; LOE edi
.B11.215:                       ; Preds .B11.3
        add       esp, 24                                       ;339.5
                                ; LOE edi
.B11.4:                         ; Preds .B11.215
        push      32                                            ;340.5
        mov       DWORD PTR [-40+ebp], 0                        ;340.5
        lea       eax, DWORD PTR [-336+ebp]                     ;340.5
        push      eax                                           ;340.5
        push      OFFSET FLAT: __STRLITPACK_132.0.11            ;340.5
        push      -2088435968                                   ;340.5
        push      6059                                          ;340.5
        mov       DWORD PTR [-336+ebp], 29                      ;340.5
        lea       edx, DWORD PTR [-40+ebp]                      ;340.5
        push      edx                                           ;340.5
        mov       DWORD PTR [-332+ebp], OFFSET FLAT: __STRLITPACK_25 ;340.5
        mov       DWORD PTR [-328+ebp], 7                       ;340.5
        mov       DWORD PTR [-324+ebp], OFFSET FLAT: __STRLITPACK_24 ;340.5
        call      _for_open                                     ;340.5
                                ; LOE edi
.B11.216:                       ; Preds .B11.4
        add       esp, 24                                       ;340.5
                                ; LOE edi
.B11.5:                         ; Preds .B11.216
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NZONES] ;342.8
        xor       edx, edx                                      ;342.8
        test      ecx, ecx                                      ;342.8
        lea       esi, DWORD PTR [-500+ebp]                     ;342.8
        push      edi                                           ;342.8
        cmovle    ecx, edx                                      ;342.8
        mov       eax, 1                                        ;342.8
        push      ecx                                           ;342.8
        push      2                                             ;342.8
        push      esi                                           ;342.8
        mov       DWORD PTR [-308+ebp], 133                     ;342.8
        mov       DWORD PTR [-316+ebp], edi                     ;342.8
        mov       DWORD PTR [-304+ebp], eax                     ;342.8
        mov       DWORD PTR [-312+ebp], edx                     ;342.8
        mov       DWORD PTR [-288+ebp], eax                     ;342.8
        mov       DWORD PTR [-296+ebp], ecx                     ;342.8
        mov       DWORD PTR [-292+ebp], edi                     ;342.8
        call      _for_check_mult_overflow                      ;342.8
                                ; LOE eax edi
.B11.217:                       ; Preds .B11.5
        add       esp, 16                                       ;342.8
                                ; LOE eax edi
.B11.6:                         ; Preds .B11.217
        mov       edx, DWORD PTR [-308+ebp]                     ;342.8
        and       eax, 1                                        ;342.8
        and       edx, 1                                        ;342.8
        lea       ecx, DWORD PTR [-320+ebp]                     ;342.8
        shl       eax, 4                                        ;342.8
        add       edx, edx                                      ;342.8
        or        edx, eax                                      ;342.8
        or        edx, 262144                                   ;342.8
        push      edx                                           ;342.8
        push      ecx                                           ;342.8
        push      DWORD PTR [-500+ebp]                          ;342.8
        call      _for_alloc_allocatable                        ;342.8
                                ; LOE edi
.B11.218:                       ; Preds .B11.6
        add       esp, 12                                       ;342.8
                                ; LOE edi
.B11.7:                         ; Preds .B11.218
        mov       edx, DWORD PTR [-296+ebp]                     ;343.8
        test      edx, edx                                      ;343.8
        jle       .B11.10       ; Prob 50%                      ;343.8
                                ; LOE edx edi
.B11.8:                         ; Preds .B11.7
        mov       eax, DWORD PTR [-320+ebp]                     ;343.8
        cmp       edx, 24                                       ;343.8
        mov       DWORD PTR [-504+ebp], eax                     ;343.8
        jle       .B11.173      ; Prob 0%                       ;343.8
                                ; LOE eax edx edi al ah
.B11.9:                         ; Preds .B11.8
        shl       edx, 2                                        ;343.8
        push      edx                                           ;343.8
        push      0                                             ;343.8
        push      DWORD PTR [-504+ebp]                          ;343.8
        call      __intel_fast_memset                           ;343.8
                                ; LOE edi
.B11.219:                       ; Preds .B11.9
        add       esp, 12                                       ;343.8
                                ; LOE edi
.B11.10:                        ; Preds .B11.187 .B11.219 .B11.185 .B11.7
        push      32                                            ;344.8
        mov       DWORD PTR [-40+ebp], 0                        ;344.8
        lea       edx, DWORD PTR [-224+ebp]                     ;344.8
        push      edx                                           ;344.8
        push      OFFSET FLAT: __STRLITPACK_133.0.11            ;344.8
        push      -2088435968                                   ;344.8
        push      6058                                          ;344.8
        lea       ecx, DWORD PTR [-40+ebp]                      ;344.8
        push      ecx                                           ;344.8
        lea       eax, DWORD PTR [-180+ebp]                     ;344.8
        mov       DWORD PTR [-224+ebp], eax                     ;344.8
        call      _for_read_seq_lis                             ;344.8
                                ; LOE edi
.B11.220:                       ; Preds .B11.10
        add       esp, 24                                       ;344.8
                                ; LOE edi
.B11.11:                        ; Preds .B11.220
        mov       esi, DWORD PTR [-180+ebp]                     ;345.8
        mov       eax, 40                                       ;345.8
        xor       ecx, ecx                                      ;345.8
        test      esi, esi                                      ;345.8
        push      eax                                           ;345.8
        cmovle    esi, ecx                                      ;345.8
        mov       edx, 1                                        ;345.8
        push      esi                                           ;345.8
        mov       DWORD PTR [-264+ebp], edx                     ;345.8
        mov       DWORD PTR [-248+ebp], edx                     ;345.8
        lea       edx, DWORD PTR [-284+ebp]                     ;345.8
        push      2                                             ;345.8
        push      edx                                           ;345.8
        mov       DWORD PTR [-268+ebp], 133                     ;345.8
        mov       DWORD PTR [-276+ebp], eax                     ;345.8
        mov       DWORD PTR [-272+ebp], ecx                     ;345.8
        mov       DWORD PTR [-256+ebp], esi                     ;345.8
        mov       DWORD PTR [-252+ebp], eax                     ;345.8
        call      _for_check_mult_overflow                      ;345.8
                                ; LOE eax edi
.B11.221:                       ; Preds .B11.11
        add       esp, 16                                       ;345.8
                                ; LOE eax edi
.B11.12:                        ; Preds .B11.221
        mov       edx, DWORD PTR [-268+ebp]                     ;345.8
        and       eax, 1                                        ;345.8
        and       edx, 1                                        ;345.8
        lea       ecx, DWORD PTR [-280+ebp]                     ;345.8
        shl       eax, 4                                        ;345.8
        add       edx, edx                                      ;345.8
        or        edx, eax                                      ;345.8
        or        edx, 262144                                   ;345.8
        push      edx                                           ;345.8
        push      ecx                                           ;345.8
        push      DWORD PTR [-284+ebp]                          ;345.8
        call      _for_alloc_allocatable                        ;345.8
                                ; LOE edi
.B11.222:                       ; Preds .B11.12
        add       esp, 12                                       ;345.8
                                ; LOE edi
.B11.13:                        ; Preds .B11.222
        mov       eax, DWORD PTR [-180+ebp]                     ;346.8
        test      eax, eax                                      ;346.8
        jle       .B11.18       ; Prob 2%                       ;346.8
                                ; LOE eax edi
.B11.14:                        ; Preds .B11.13
        mov       esi, 1                                        ;
        mov       edi, eax                                      ;
                                ; LOE esi edi
.B11.15:                        ; Preds .B11.16 .B11.14
        mov       edx, esi                                      ;347.10
        sub       edx, DWORD PTR [-248+ebp]                     ;347.10
        mov       ecx, DWORD PTR [-280+ebp]                     ;347.10
        push      32                                            ;347.10
        mov       DWORD PTR [-40+ebp], 0                        ;347.10
        lea       edx, DWORD PTR [edx+edx*4]                    ;347.10
        lea       edx, DWORD PTR [ecx+edx*8]                    ;347.10
        mov       DWORD PTR [-80+ebp], edx                      ;347.10
        lea       edx, DWORD PTR [-80+ebp]                      ;347.10
        push      edx                                           ;347.10
        push      OFFSET FLAT: __STRLITPACK_134.0.11            ;347.10
        push      -2088435968                                   ;347.10
        push      6058                                          ;347.10
        lea       edx, DWORD PTR [-40+ebp]                      ;347.10
        push      edx                                           ;347.10
        call      _for_read_seq_lis                             ;347.10
                                ; LOE esi edi
.B11.223:                       ; Preds .B11.15
        add       esp, 24                                       ;347.10
                                ; LOE esi edi
.B11.16:                        ; Preds .B11.223
        inc       esi                                           ;348.8
        cmp       esi, edi                                      ;348.8
        jle       .B11.15       ; Prob 82%                      ;348.8
                                ; LOE esi edi
.B11.17:                        ; Preds .B11.16
        mov       edi, 4                                        ;
                                ; LOE edi
.B11.18:                        ; Preds .B11.13 .B11.17
        push      32                                            ;349.8
        push      -2088435968                                   ;349.8
        push      6058                                          ;349.8
        mov       DWORD PTR [-40+ebp], 0                        ;349.8
        lea       eax, DWORD PTR [-40+ebp]                      ;349.8
        push      eax                                           ;349.8
        call      _for_rewind                                   ;349.8
                                ; LOE edi
.B11.224:                       ; Preds .B11.18
        add       esp, 16                                       ;349.8
                                ; LOE edi
.B11.19:                        ; Preds .B11.224
        push      32                                            ;350.8
        xor       eax, eax                                      ;350.8
        lea       edx, DWORD PTR [-40+ebp]                      ;350.8
        push      eax                                           ;350.8
        push      OFFSET FLAT: __STRLITPACK_135.0.11            ;350.8
        push      -2088435968                                   ;350.8
        push      6058                                          ;350.8
        push      edx                                           ;350.8
        mov       DWORD PTR [-40+ebp], eax                      ;350.8
        call      _for_read_seq_lis                             ;350.8
                                ; LOE edi
.B11.225:                       ; Preds .B11.19
        add       esp, 24                                       ;350.8
                                ; LOE edi
.B11.20:                        ; Preds .B11.225
        mov       eax, DWORD PTR [-180+ebp]                     ;351.8
        test      eax, eax                                      ;351.8
        jle       .B11.63       ; Prob 29%                      ;351.8
                                ; LOE eax edi
.B11.21:                        ; Preds .B11.20
        mov       edx, DWORD PTR [-280+ebp]                     ;352.10
        xor       esi, esi                                      ;
        mov       ecx, DWORD PTR [-248+ebp]                     ;352.19
        mov       DWORD PTR [-88+ebp], esi                      ;
        mov       DWORD PTR [-140+ebp], eax                     ;
        mov       DWORD PTR [-64+ebp], esi                      ;
        mov       DWORD PTR [-128+ebp], edx                     ;
                                ; LOE ecx
.B11.22:                        ; Preds .B11.60 .B11.21
        imul      edi, ecx, -40                                 ;352.10
        mov       edx, 4                                        ;352.10
        mov       esi, DWORD PTR [-128+ebp]                     ;352.10
        xor       ecx, ecx                                      ;352.10
        mov       eax, DWORD PTR [-64+ebp]                      ;352.10
        add       esi, edi                                      ;352.10
        mov       edi, 1                                        ;352.10
        push      edx                                           ;352.10
        lea       eax, DWORD PTR [eax+eax*4]                    ;352.10
        mov       DWORD PTR [-132+ebp], eax                     ;352.10
        mov       DWORD PTR [60+esi+eax*8], edi                 ;352.10
        mov       DWORD PTR [76+esi+eax*8], edi                 ;352.10
        mov       edi, DWORD PTR [40+esi+eax*8]                 ;352.10
        test      edi, edi                                      ;352.10
        mov       DWORD PTR [48+esi+eax*8], edx                 ;352.10
        cmovl     edi, ecx                                      ;352.10
        push      edi                                           ;352.10
        mov       DWORD PTR [72+esi+eax*8], edx                 ;352.10
        lea       edx, DWORD PTR [-148+ebp]                     ;352.10
        push      2                                             ;352.10
        push      edx                                           ;352.10
        mov       DWORD PTR [56+esi+eax*8], 5                   ;352.10
        mov       DWORD PTR [52+esi+eax*8], ecx                 ;352.10
        mov       DWORD PTR [68+esi+eax*8], edi                 ;352.10
        call      _for_check_mult_overflow                      ;352.10
                                ; LOE eax
.B11.226:                       ; Preds .B11.22
        add       esp, 16                                       ;352.10
                                ; LOE eax
.B11.23:                        ; Preds .B11.226
        imul      edx, DWORD PTR [-248+ebp], -40                ;352.10
        and       eax, 1                                        ;352.10
        add       edx, DWORD PTR [-280+ebp]                     ;352.10
        mov       ecx, DWORD PTR [-132+ebp]                     ;352.10
        shl       eax, 4                                        ;352.10
        or        eax, 262144                                   ;352.10
        push      eax                                           ;352.10
        lea       esi, DWORD PTR [44+edx+ecx*8]                 ;352.10
        push      esi                                           ;352.10
        push      DWORD PTR [-148+ebp]                          ;352.10
        call      _for_allocate                                 ;352.10
                                ; LOE
.B11.227:                       ; Preds .B11.23
        add       esp, 12                                       ;352.10
                                ; LOE
.B11.24:                        ; Preds .B11.227
        mov       ecx, DWORD PTR [-280+ebp]                     ;353.10
        mov       esi, DWORD PTR [-132+ebp]                     ;353.10
        lea       ecx, DWORD PTR [ecx+esi*8]                    ;353.10
        imul      esi, DWORD PTR [-248+ebp], -40                ;353.10
        mov       eax, DWORD PTR [76+esi+ecx]                   ;353.10
        mov       edx, DWORD PTR [68+esi+ecx]                   ;353.10
        test      edx, edx                                      ;353.10
        mov       DWORD PTR [-84+ebp], eax                      ;353.10
        jle       .B11.31       ; Prob 50%                      ;353.10
                                ; LOE edx ecx esi
.B11.25:                        ; Preds .B11.24
        mov       eax, edx                                      ;353.10
        shr       eax, 31                                       ;353.10
        add       eax, edx                                      ;353.10
        sar       eax, 1                                        ;353.10
        mov       DWORD PTR [-72+ebp], eax                      ;353.10
        test      eax, eax                                      ;353.10
        jbe       .B11.163      ; Prob 10%                      ;353.10
                                ; LOE edx ecx esi
.B11.26:                        ; Preds .B11.25
        mov       DWORD PTR [-176+ebp], edx                     ;
        mov       eax, DWORD PTR [72+esi+ecx]                   ;353.10
        mov       edx, DWORD PTR [-84+ebp]                      ;
        imul      edx, eax                                      ;
        mov       edi, DWORD PTR [44+esi+ecx]                   ;353.10
        sub       edi, edx                                      ;
        mov       DWORD PTR [-68+ebp], 0                        ;
        mov       DWORD PTR [-76+ebp], eax                      ;353.10
        mov       DWORD PTR [-168+ebp], ecx                     ;
        mov       eax, edi                                      ;
        mov       ecx, DWORD PTR [-68+ebp]                      ;
        mov       DWORD PTR [-172+ebp], esi                     ;
                                ; LOE eax ecx
.B11.27:                        ; Preds .B11.27 .B11.26
        mov       edx, DWORD PTR [-84+ebp]                      ;353.10
        xor       edi, edi                                      ;353.10
        mov       esi, DWORD PTR [-76+ebp]                      ;353.10
        mov       DWORD PTR [-68+ebp], ecx                      ;
        lea       edx, DWORD PTR [edx+ecx*2]                    ;353.10
        mov       ecx, esi                                      ;353.10
        imul      ecx, edx                                      ;353.10
        inc       edx                                           ;353.10
        imul      edx, esi                                      ;353.10
        mov       DWORD PTR [eax+ecx], edi                      ;353.10
        mov       ecx, DWORD PTR [-68+ebp]                      ;353.10
        inc       ecx                                           ;353.10
        mov       DWORD PTR [eax+edx], edi                      ;353.10
        cmp       ecx, DWORD PTR [-72+ebp]                      ;353.10
        jb        .B11.27       ; Prob 63%                      ;353.10
                                ; LOE eax ecx
.B11.28:                        ; Preds .B11.27
        mov       DWORD PTR [-68+ebp], ecx                      ;
        mov       eax, ecx                                      ;353.10
        mov       edx, DWORD PTR [-176+ebp]                     ;
        mov       esi, DWORD PTR [-172+ebp]                     ;
        mov       ecx, DWORD PTR [-168+ebp]                     ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;353.10
                                ; LOE eax edx ecx esi
.B11.29:                        ; Preds .B11.28 .B11.163
        lea       edi, DWORD PTR [-1+eax]                       ;353.10
        cmp       edx, edi                                      ;353.10
        jbe       .B11.31       ; Prob 10%                      ;353.10
                                ; LOE eax ecx esi
.B11.30:                        ; Preds .B11.29
        mov       edx, DWORD PTR [-84+ebp]                      ;353.10
        lea       eax, DWORD PTR [-1+eax+edx]                   ;353.10
        sub       eax, edx                                      ;353.10
        imul      eax, DWORD PTR [72+esi+ecx]                   ;353.10
        mov       ecx, DWORD PTR [44+esi+ecx]                   ;353.10
        mov       DWORD PTR [ecx+eax], 0                        ;353.10
                                ; LOE
.B11.31:                        ; Preds .B11.24 .B11.29 .B11.30
        mov       DWORD PTR [-136+ebp], esp                     ;354.10
        lea       edx, DWORD PTR [-112+ebp]                     ;354.10
        push      32                                            ;354.10
        push      edx                                           ;354.10
        push      OFFSET FLAT: __STRLITPACK_136.0.11            ;354.10
        push      -2088435968                                   ;354.10
        push      6058                                          ;354.10
        mov       DWORD PTR [-40+ebp], 0                        ;354.10
        lea       ecx, DWORD PTR [-40+ebp]                      ;354.10
        push      ecx                                           ;354.10
        lea       eax, DWORD PTR [-144+ebp]                     ;354.10
        mov       DWORD PTR [-112+ebp], eax                     ;354.10
        call      _for_read_seq_lis                             ;354.10
                                ; LOE
.B11.228:                       ; Preds .B11.31
        add       esp, 24                                       ;354.10
                                ; LOE
.B11.32:                        ; Preds .B11.228
        mov       edx, DWORD PTR [-280+ebp]                     ;354.10
        xor       edi, edi                                      ;354.10
        mov       ecx, DWORD PTR [-132+ebp]                     ;354.10
        lea       edx, DWORD PTR [edx+ecx*8]                    ;354.10
        imul      ecx, DWORD PTR [-248+ebp], -40                ;354.10
        mov       esi, DWORD PTR [40+ecx+edx]                   ;354.10
        test      esi, esi                                      ;354.10
        mov       eax, DWORD PTR [72+ecx+edx]                   ;354.10
        cmovge    edi, esi                                      ;354.10
        shl       edi, 2                                        ;354.10
        mov       DWORD PTR [-204+ebp], esi                     ;354.10
        mov       DWORD PTR [-108+ebp], eax                     ;354.10
        cmp       eax, DWORD PTR [48+ecx+edx]                   ;354.10
        je        .B11.41       ; Prob 50%                      ;354.10
                                ; LOE edx ecx esi edi
.B11.33:                        ; Preds .B11.32
        mov       eax, edi                                      ;354.10
        call      __alloca_probe                                ;354.10
        and       esp, -16                                      ;354.10
        mov       eax, esp                                      ;354.10
                                ; LOE eax edx ecx esi edi
.B11.229:                       ; Preds .B11.33
        mov       DWORD PTR [-88+ebp], eax                      ;354.10
        cmp       DWORD PTR [-204+ebp], 0                       ;354.10
        jle       .B11.40       ; Prob 50%                      ;354.10
                                ; LOE edx ecx esi edi
.B11.34:                        ; Preds .B11.229
        mov       eax, esi                                      ;354.10
        mov       esi, eax                                      ;354.10
        shr       esi, 31                                       ;354.10
        add       esi, eax                                      ;354.10
        sar       esi, 1                                        ;354.10
        mov       DWORD PTR [-92+ebp], esi                      ;354.10
        test      esi, esi                                      ;354.10
        jbe       .B11.164      ; Prob 10%                      ;354.10
                                ; LOE edx ecx edi
.B11.35:                        ; Preds .B11.34
        mov       esi, DWORD PTR [76+ecx+edx]                   ;354.10
        xor       eax, eax                                      ;
        imul      esi, DWORD PTR [-108+ebp]                     ;
        mov       DWORD PTR [-208+ebp], eax                     ;
        mov       eax, DWORD PTR [44+ecx+edx]                   ;354.10
        sub       eax, esi                                      ;
        mov       DWORD PTR [-228+ebp], edx                     ;
        mov       DWORD PTR [-504+ebp], edi                     ;
        mov       DWORD PTR [-244+ebp], ecx                     ;
        mov       edx, eax                                      ;
        mov       esi, DWORD PTR [-208+ebp]                     ;
        mov       eax, DWORD PTR [-88+ebp]                      ;
                                ; LOE eax edx esi
.B11.36:                        ; Preds .B11.36 .B11.35
        mov       ecx, DWORD PTR [-108+ebp]                     ;354.10
        lea       edi, DWORD PTR [1+esi+esi]                    ;354.10
        imul      edi, ecx                                      ;354.10
        mov       edi, DWORD PTR [edx+edi]                      ;354.28
        mov       DWORD PTR [eax+esi*8], edi                    ;354.28
        lea       edi, DWORD PTR [2+esi+esi]                    ;354.10
        imul      edi, ecx                                      ;354.10
        mov       ecx, DWORD PTR [edx+edi]                      ;354.28
        mov       DWORD PTR [4+eax+esi*8], ecx                  ;354.28
        inc       esi                                           ;354.10
        cmp       esi, DWORD PTR [-92+ebp]                      ;354.10
        jb        .B11.36       ; Prob 63%                      ;354.10
                                ; LOE eax edx esi
.B11.37:                        ; Preds .B11.36
        mov       DWORD PTR [-208+ebp], esi                     ;
        mov       eax, esi                                      ;354.10
        mov       edi, DWORD PTR [-504+ebp]                     ;
        mov       ecx, DWORD PTR [-244+ebp]                     ;
        mov       edx, DWORD PTR [-228+ebp]                     ;
        lea       esi, DWORD PTR [1+eax+eax]                    ;354.10
                                ; LOE edx ecx esi edi
.B11.38:                        ; Preds .B11.37 .B11.164
        lea       eax, DWORD PTR [-1+esi]                       ;354.10
        cmp       eax, DWORD PTR [-204+ebp]                     ;354.10
        jae       .B11.40       ; Prob 10%                      ;354.10
                                ; LOE edx ecx esi edi
.B11.39:                        ; Preds .B11.38
        mov       eax, DWORD PTR [76+ecx+edx]                   ;354.10
        neg       eax                                           ;354.10
        add       eax, esi                                      ;354.10
        imul      eax, DWORD PTR [-108+ebp]                     ;354.10
        mov       edx, DWORD PTR [44+ecx+edx]                   ;354.10
        mov       ecx, DWORD PTR [edx+eax]                      ;354.28
        mov       eax, DWORD PTR [-88+ebp]                      ;354.28
        mov       DWORD PTR [-4+eax+esi*4], ecx                 ;354.28
                                ; LOE edi
.B11.40:                        ; Preds .B11.229 .B11.38 .B11.39
        mov       eax, DWORD PTR [-88+ebp]                      ;354.10
        mov       esi, 1                                        ;
        jmp       .B11.42       ; Prob 100%                     ;
                                ; LOE eax esi edi
.B11.41:                        ; Preds .B11.32
        mov       eax, DWORD PTR [76+ecx+edx]                   ;354.10
        xor       esi, esi                                      ;
        neg       eax                                           ;354.10
        inc       eax                                           ;354.10
        imul      eax, DWORD PTR [-108+ebp]                     ;354.10
        add       eax, DWORD PTR [44+ecx+edx]                   ;354.10
                                ; LOE eax esi edi
.B11.42:                        ; Preds .B11.41 .B11.40
        mov       DWORD PTR [-196+ebp], eax                     ;354.10
        lea       eax, DWORD PTR [-200+ebp]                     ;354.10
        push      eax                                           ;354.10
        push      OFFSET FLAT: __STRLITPACK_137.0.11            ;354.10
        mov       DWORD PTR [-200+ebp], edi                     ;354.10
        lea       edx, DWORD PTR [-40+ebp]                      ;354.10
        push      edx                                           ;354.10
        call      _for_read_seq_lis_xmit                        ;354.10
                                ; LOE esi
.B11.230:                       ; Preds .B11.42
        add       esp, 12                                       ;354.10
                                ; LOE esi
.B11.43:                        ; Preds .B11.230
        test      esi, esi                                      ;354.10
        je        .B11.51       ; Prob 50%                      ;354.10
                                ; LOE
.B11.44:                        ; Preds .B11.43
        cmp       DWORD PTR [-204+ebp], 0                       ;354.28
        jle       .B11.51       ; Prob 50%                      ;354.28
                                ; LOE
.B11.45:                        ; Preds .B11.44
        mov       edx, DWORD PTR [-204+ebp]                     ;354.28
        mov       ecx, edx                                      ;354.28
        shr       ecx, 31                                       ;354.28
        add       ecx, edx                                      ;354.28
        sar       ecx, 1                                        ;354.28
        mov       eax, DWORD PTR [-280+ebp]                     ;354.28
        test      ecx, ecx                                      ;354.28
        mov       edi, DWORD PTR [-248+ebp]                     ;354.28
        mov       DWORD PTR [-96+ebp], ecx                      ;354.28
        jbe       .B11.165      ; Prob 10%                      ;354.28
                                ; LOE eax edi
.B11.46:                        ; Preds .B11.45
        mov       DWORD PTR [-220+ebp], edi                     ;
        imul      edi, edi, -40                                 ;
        mov       esi, DWORD PTR [-132+ebp]                     ;
        mov       DWORD PTR [-212+ebp], 0                       ;
        mov       DWORD PTR [-216+ebp], eax                     ;
        lea       ecx, DWORD PTR [eax+esi*8]                    ;
        mov       esi, DWORD PTR [44+edi+ecx]                   ;354.28
        mov       edx, DWORD PTR [76+edi+ecx]                   ;354.28
        mov       ecx, DWORD PTR [72+edi+ecx]                   ;354.28
        imul      edx, ecx                                      ;
        sub       esi, edx                                      ;
        mov       DWORD PTR [-100+ebp], esi                     ;
        mov       esi, DWORD PTR [-212+ebp]                     ;
        mov       DWORD PTR [-104+ebp], ecx                     ;
                                ; LOE esi
.B11.47:                        ; Preds .B11.47 .B11.46
        mov       ecx, DWORD PTR [-88+ebp]                      ;354.28
        lea       edx, DWORD PTR [1+esi+esi]                    ;354.28
        mov       edi, DWORD PTR [-104+ebp]                     ;354.28
        imul      edx, edi                                      ;354.28
        mov       eax, DWORD PTR [ecx+esi*8]                    ;354.28
        mov       ecx, DWORD PTR [-100+ebp]                     ;354.28
        mov       DWORD PTR [ecx+edx], eax                      ;354.28
        lea       eax, DWORD PTR [2+esi+esi]                    ;354.28
        imul      eax, edi                                      ;354.28
        mov       edx, DWORD PTR [-88+ebp]                      ;354.28
        mov       edi, DWORD PTR [4+edx+esi*8]                  ;354.28
        inc       esi                                           ;354.28
        mov       DWORD PTR [ecx+eax], edi                      ;354.28
        cmp       esi, DWORD PTR [-96+ebp]                      ;354.28
        jb        .B11.47       ; Prob 63%                      ;354.28
                                ; LOE esi
.B11.48:                        ; Preds .B11.47
        mov       DWORD PTR [-212+ebp], esi                     ;
        mov       edx, esi                                      ;354.28
        mov       edi, DWORD PTR [-220+ebp]                     ;
        mov       eax, DWORD PTR [-216+ebp]                     ;
        lea       edx, DWORD PTR [1+edx+edx]                    ;354.28
                                ; LOE eax edx edi
.B11.49:                        ; Preds .B11.48 .B11.165
        lea       ecx, DWORD PTR [-1+edx]                       ;354.28
        cmp       ecx, DWORD PTR [-204+ebp]                     ;354.28
        jae       .B11.51       ; Prob 10%                      ;354.28
                                ; LOE eax edx edi
.B11.50:                        ; Preds .B11.49
        imul      edi, edi, -40                                 ;354.28
        mov       ecx, DWORD PTR [-132+ebp]                     ;354.28
        lea       esi, DWORD PTR [eax+ecx*8]                    ;354.28
        mov       ecx, DWORD PTR [-88+ebp]                      ;354.28
        mov       eax, DWORD PTR [76+edi+esi]                   ;354.28
        neg       eax                                           ;354.28
        add       eax, edx                                      ;354.28
        imul      eax, DWORD PTR [72+edi+esi]                   ;354.28
        mov       esi, DWORD PTR [44+edi+esi]                   ;354.28
        mov       edx, DWORD PTR [-4+ecx+edx*4]                 ;354.28
        mov       DWORD PTR [esi+eax], edx                      ;354.28
                                ; LOE
.B11.51:                        ; Preds .B11.43 .B11.44 .B11.49 .B11.50
        push      0                                             ;354.10
        push      OFFSET FLAT: __STRLITPACK_138.0.11            ;354.10
        lea       eax, DWORD PTR [-40+ebp]                      ;354.10
        push      eax                                           ;354.10
        call      _for_read_seq_lis_xmit                        ;354.10
                                ; LOE
.B11.231:                       ; Preds .B11.51
        add       esp, 12                                       ;354.10
                                ; LOE
.B11.52:                        ; Preds .B11.231
        mov       eax, DWORD PTR [-136+ebp]                     ;354.10
        mov       esp, eax                                      ;354.10
                                ; LOE
.B11.232:                       ; Preds .B11.52
        mov       ecx, DWORD PTR [-248+ebp]                     ;355.20
        imul      esi, ecx, -40                                 ;355.10
        mov       edx, DWORD PTR [-280+ebp]                     ;355.10
        mov       eax, DWORD PTR [-132+ebp]                     ;355.10
        mov       DWORD PTR [-128+ebp], edx                     ;355.10
        mov       DWORD PTR [-124+ebp], esi                     ;355.10
        lea       edx, DWORD PTR [edx+eax*8]                    ;355.10
        mov       esi, DWORD PTR [40+esi+edx]                   ;355.10
        test      esi, esi                                      ;355.10
        jle       .B11.59       ; Prob 50%                      ;355.10
                                ; LOE edx ecx esi
.B11.53:                        ; Preds .B11.232
        mov       eax, DWORD PTR [-320+ebp]                     ;356.12
        mov       DWORD PTR [-120+ebp], eax                     ;356.12
        mov       eax, esi                                      ;355.10
        shr       eax, 31                                       ;355.10
        add       eax, esi                                      ;355.10
        sar       eax, 1                                        ;355.10
        mov       edi, DWORD PTR [-288+ebp]                     ;356.12
        inc       DWORD PTR [-64+ebp]                           ;
        mov       DWORD PTR [-116+ebp], edi                     ;356.12
        test      eax, eax                                      ;355.10
        mov       DWORD PTR [-48+ebp], eax                      ;355.10
        jbe       .B11.166      ; Prob 3%                       ;355.10
                                ; LOE edx ecx esi edi
.B11.54:                        ; Preds .B11.53
        mov       eax, edi                                      ;
        xor       edi, edi                                      ;
        mov       DWORD PTR [-156+ebp], esi                     ;
        mov       DWORD PTR [-152+ebp], edi                     ;
        mov       edi, DWORD PTR [-124+ebp]                     ;356.12
        lea       esi, DWORD PTR [eax*4]                        ;
        neg       esi                                           ;
        add       esi, DWORD PTR [-120+ebp]                     ;
        mov       DWORD PTR [-56+ebp], esi                      ;
        mov       eax, DWORD PTR [44+edi+edx]                   ;356.12
        mov       esi, DWORD PTR [76+edi+edx]                   ;356.12
        mov       edi, DWORD PTR [72+edi+edx]                   ;356.12
        imul      esi, edi                                      ;
        sub       eax, esi                                      ;
        mov       DWORD PTR [-52+ebp], eax                      ;
        mov       DWORD PTR [-60+ebp], edi                      ;356.12
        mov       eax, DWORD PTR [-152+ebp]                     ;
        mov       DWORD PTR [-160+ebp], edx                     ;
        mov       DWORD PTR [-164+ebp], ecx                     ;
                                ; LOE eax
.B11.55:                        ; Preds .B11.55 .B11.54
        mov       edi, DWORD PTR [-60+ebp]                      ;356.12
        lea       esi, DWORD PTR [1+eax+eax]                    ;356.12
        imul      esi, edi                                      ;356.12
        mov       ecx, DWORD PTR [-52+ebp]                      ;356.12
        mov       edx, DWORD PTR [ecx+esi]                      ;356.12
        mov       ecx, DWORD PTR [-56+ebp]                      ;356.12
        mov       esi, DWORD PTR [-64+ebp]                      ;356.12
        mov       DWORD PTR [ecx+edx*4], esi                    ;356.12
        lea       edx, DWORD PTR [2+eax+eax]                    ;356.12
        imul      edx, edi                                      ;356.12
        inc       eax                                           ;355.10
        mov       edi, DWORD PTR [-52+ebp]                      ;356.12
        cmp       eax, DWORD PTR [-48+ebp]                      ;355.10
        mov       edx, DWORD PTR [edi+edx]                      ;356.12
        mov       DWORD PTR [ecx+edx*4], esi                    ;356.12
        jb        .B11.55       ; Prob 64%                      ;355.10
                                ; LOE eax
.B11.56:                        ; Preds .B11.55
        mov       esi, DWORD PTR [-156+ebp]                     ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;355.10
        mov       edx, DWORD PTR [-160+ebp]                     ;
        mov       ecx, DWORD PTR [-164+ebp]                     ;
                                ; LOE eax edx ecx esi
.B11.57:                        ; Preds .B11.56 .B11.166
        lea       edi, DWORD PTR [-1+eax]                       ;355.10
        cmp       esi, edi                                      ;355.10
        jbe       .B11.60       ; Prob 3%                       ;355.10
                                ; LOE eax edx ecx
.B11.58:                        ; Preds .B11.57
        mov       esi, DWORD PTR [-124+ebp]                     ;356.12
        mov       edi, DWORD PTR [-120+ebp]                     ;356.12
        sub       eax, DWORD PTR [76+esi+edx]                   ;356.12
        imul      eax, DWORD PTR [72+esi+edx]                   ;356.12
        mov       edx, DWORD PTR [44+esi+edx]                   ;356.12
        mov       edx, DWORD PTR [edx+eax]                      ;356.12
        sub       edx, DWORD PTR [-116+ebp]                     ;356.12
        mov       eax, DWORD PTR [-64+ebp]                      ;356.12
        mov       DWORD PTR [edi+edx*4], eax                    ;356.12
        jmp       .B11.60       ; Prob 100%                     ;356.12
                                ; LOE ecx
.B11.59:                        ; Preds .B11.232
        inc       DWORD PTR [-64+ebp]                           ;
                                ; LOE ecx
.B11.60:                        ; Preds .B11.58 .B11.57 .B11.59
        mov       eax, DWORD PTR [-64+ebp]                      ;351.8
        cmp       eax, DWORD PTR [-140+ebp]                     ;351.8
        jb        .B11.22       ; Prob 82%                      ;351.8
                                ; LOE ecx
.B11.61:                        ; Preds .B11.60
        mov       eax, DWORD PTR [-180+ebp]                     ;361.5
        mov       edi, 4                                        ;
        jmp       .B11.63       ; Prob 100%                     ;
                                ; LOE eax edi
.B11.62:                        ; Preds .B11.2
        mov       eax, DWORD PTR [-180+ebp]                     ;361.5
                                ; LOE eax edi
.B11.63:                        ; Preds .B11.61 .B11.20 .B11.62
        movss     xmm3, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIMPERIOD] ;361.41
        xor       ecx, ecx                                      ;361.5
        test      eax, eax                                      ;361.5
        movss     xmm0, DWORD PTR [_2il0floatpacket.74]         ;361.41
        andps     xmm0, xmm3                                    ;361.41
        cmovle    eax, ecx                                      ;361.5
        pxor      xmm3, xmm0                                    ;361.41
        mov       esi, 1                                        ;361.5
        movss     xmm1, DWORD PTR [_2il0floatpacket.75]         ;361.41
        movaps    xmm2, xmm3                                    ;361.41
        movss     xmm4, DWORD PTR [_2il0floatpacket.76]         ;361.41
        cmpltss   xmm2, xmm1                                    ;361.41
        andps     xmm1, xmm2                                    ;361.41
        movaps    xmm2, xmm3                                    ;361.41
        movaps    xmm6, xmm4                                    ;361.41
        addss     xmm2, xmm1                                    ;361.41
        addss     xmm6, xmm4                                    ;361.41
        subss     xmm2, xmm1                                    ;361.41
        movaps    xmm7, xmm2                                    ;361.41
        mov       DWORD PTR [-392+ebp], esi                     ;361.5
        subss     xmm7, xmm3                                    ;361.41
        movaps    xmm5, xmm7                                    ;361.41
        cmpless   xmm7, DWORD PTR [_2il0floatpacket.77]         ;361.41
        cmpnless  xmm5, xmm4                                    ;361.41
        andps     xmm5, xmm6                                    ;361.41
        andps     xmm7, xmm6                                    ;361.41
        mov       DWORD PTR [-380+ebp], esi                     ;361.5
        subss     xmm2, xmm5                                    ;361.41
        mov       DWORD PTR [-368+ebp], esi                     ;361.5
        addss     xmm2, xmm7                                    ;361.41
        orps      xmm2, xmm0                                    ;361.41
        cvtss2si  edx, xmm2                                     ;361.5
        test      edx, edx                                      ;361.5
        mov       DWORD PTR [-356+ebp], esi                     ;361.5
        mov       DWORD PTR [-416+ebp], ecx                     ;361.5
        cmovl     edx, ecx                                      ;361.5
        mov       esi, 8                                        ;361.5
        push      esi                                           ;361.5
        push      edx                                           ;361.5
        push      eax                                           ;361.5
        push      eax                                           ;361.5
        push      edi                                           ;361.5
        mov       DWORD PTR [-412+ebp], 133                     ;361.5
        lea       ecx, DWORD PTR [eax*8]                        ;361.5
        mov       DWORD PTR [-372+ebp], ecx                     ;361.5
        imul      ecx, eax                                      ;361.5
        mov       DWORD PTR [-360+ebp], ecx                     ;361.5
        lea       ecx, DWORD PTR [-216+ebp]                     ;361.5
        push      ecx                                           ;361.5
        mov       DWORD PTR [-420+ebp], edi                     ;361.5
        mov       DWORD PTR [-408+ebp], edi                     ;361.5
        mov       DWORD PTR [-400+ebp], 2                       ;361.5
        mov       DWORD PTR [-388+ebp], eax                     ;361.5
        mov       DWORD PTR [-376+ebp], eax                     ;361.5
        mov       DWORD PTR [-364+ebp], edx                     ;361.5
        mov       DWORD PTR [-396+ebp], edi                     ;361.5
        mov       DWORD PTR [-384+ebp], esi                     ;361.5
        call      _for_check_mult_overflow                      ;361.5
                                ; LOE eax esi edi
.B11.233:                       ; Preds .B11.63
        add       esp, 24                                       ;361.5
                                ; LOE eax esi edi
.B11.64:                        ; Preds .B11.233
        mov       edx, DWORD PTR [-412+ebp]                     ;361.5
        and       eax, 1                                        ;361.5
        and       edx, 1                                        ;361.5
        lea       ecx, DWORD PTR [-424+ebp]                     ;361.5
        shl       eax, 4                                        ;361.5
        add       edx, edx                                      ;361.5
        or        edx, eax                                      ;361.5
        or        edx, 262144                                   ;361.5
        push      edx                                           ;361.5
        push      ecx                                           ;361.5
        push      DWORD PTR [-216+ebp]                          ;361.5
        call      _for_alloc_allocatable                        ;361.5
                                ; LOE esi edi
.B11.234:                       ; Preds .B11.64
        add       esp, 12                                       ;361.5
                                ; LOE esi edi
.B11.65:                        ; Preds .B11.234
        movss     xmm3, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIMPERIOD] ;362.44
        mov       eax, 1                                        ;362.8
        movss     xmm0, DWORD PTR [_2il0floatpacket.74]         ;362.44
        xor       edx, edx                                      ;362.8
        andps     xmm0, xmm3                                    ;362.44
        pxor      xmm3, xmm0                                    ;362.44
        movss     xmm1, DWORD PTR [_2il0floatpacket.75]         ;362.44
        movaps    xmm2, xmm3                                    ;362.44
        movss     xmm4, DWORD PTR [_2il0floatpacket.76]         ;362.44
        cmpltss   xmm2, xmm1                                    ;362.44
        andps     xmm1, xmm2                                    ;362.44
        movaps    xmm2, xmm3                                    ;362.44
        movaps    xmm6, xmm4                                    ;362.44
        addss     xmm2, xmm1                                    ;362.44
        addss     xmm6, xmm4                                    ;362.44
        subss     xmm2, xmm1                                    ;362.44
        movaps    xmm7, xmm2                                    ;362.44
        mov       DWORD PTR [-464+ebp], eax                     ;362.8
        subss     xmm7, xmm3                                    ;362.44
        movaps    xmm5, xmm7                                    ;362.44
        cmpless   xmm7, DWORD PTR [_2il0floatpacket.77]         ;362.44
        cmpnless  xmm5, xmm4                                    ;362.44
        andps     xmm5, xmm6                                    ;362.44
        andps     xmm7, xmm6                                    ;362.44
        mov       DWORD PTR [-452+ebp], eax                     ;362.8
        subss     xmm2, xmm5                                    ;362.44
        mov       DWORD PTR [-440+ebp], eax                     ;362.8
        addss     xmm2, xmm7                                    ;362.44
        orps      xmm2, xmm0                                    ;362.44
        mov       DWORD PTR [-428+ebp], eax                     ;362.8
        cvtss2si  eax, xmm2                                     ;362.8
        mov       ecx, DWORD PTR [-180+ebp]                     ;362.8
        test      ecx, ecx                                      ;362.8
        mov       DWORD PTR [-488+ebp], edx                     ;362.8
        cmovle    ecx, edx                                      ;362.8
        test      eax, eax                                      ;362.8
        push      esi                                           ;362.8
        cmovl     eax, edx                                      ;362.8
        push      eax                                           ;362.8
        push      ecx                                           ;362.8
        push      ecx                                           ;362.8
        push      edi                                           ;362.8
        mov       DWORD PTR [-484+ebp], 133                     ;362.8
        lea       edx, DWORD PTR [ecx*8]                        ;362.8
        mov       DWORD PTR [-444+ebp], edx                     ;362.8
        imul      edx, ecx                                      ;362.8
        mov       DWORD PTR [-432+ebp], edx                     ;362.8
        lea       edx, DWORD PTR [-212+ebp]                     ;362.8
        push      edx                                           ;362.8
        mov       DWORD PTR [-492+ebp], edi                     ;362.8
        mov       DWORD PTR [-480+ebp], edi                     ;362.8
        mov       DWORD PTR [-472+ebp], 2                       ;362.8
        mov       DWORD PTR [-460+ebp], ecx                     ;362.8
        mov       DWORD PTR [-448+ebp], ecx                     ;362.8
        mov       DWORD PTR [-436+ebp], eax                     ;362.8
        mov       DWORD PTR [-468+ebp], edi                     ;362.8
        mov       DWORD PTR [-456+ebp], esi                     ;362.8
        call      _for_check_mult_overflow                      ;362.8
                                ; LOE eax
.B11.235:                       ; Preds .B11.65
        add       esp, 24                                       ;362.8
                                ; LOE eax
.B11.66:                        ; Preds .B11.235
        mov       edx, DWORD PTR [-484+ebp]                     ;362.8
        and       eax, 1                                        ;362.8
        and       edx, 1                                        ;362.8
        lea       ecx, DWORD PTR [-496+ebp]                     ;362.8
        shl       eax, 4                                        ;362.8
        add       edx, edx                                      ;362.8
        or        edx, eax                                      ;362.8
        or        edx, 262144                                   ;362.8
        push      edx                                           ;362.8
        push      ecx                                           ;362.8
        push      DWORD PTR [-212+ebp]                          ;362.8
        call      _for_alloc_allocatable                        ;362.8
                                ; LOE
.B11.236:                       ; Preds .B11.66
        add       esp, 12                                       ;362.8
                                ; LOE
.B11.67:                        ; Preds .B11.236
        mov       eax, DWORD PTR [-356+ebp]                     ;363.8
        mov       edx, DWORD PTR [-364+ebp]                     ;363.8
        test      edx, edx                                      ;363.8
        mov       DWORD PTR [-208+ebp], eax                     ;363.8
        mov       DWORD PTR [-204+ebp], edx                     ;363.8
        jle       .B11.84       ; Prob 10%                      ;363.8
                                ; LOE
.B11.68:                        ; Preds .B11.67
        mov       esi, DWORD PTR [-388+ebp]                     ;363.8
        mov       ecx, DWORD PTR [-400+ebp]                     ;363.8
        pxor      xmm0, xmm0                                    ;363.8
        mov       DWORD PTR [-156+ebp], esi                     ;363.8
        mov       esi, DWORD PTR [-392+ebp]                     ;
        mov       edi, DWORD PTR [-424+ebp]                     ;384.4
        shl       esi, 2                                        ;
        mov       eax, DWORD PTR [-384+ebp]                     ;384.141
        mov       DWORD PTR [-136+ebp], ecx                     ;363.8
        mov       ecx, DWORD PTR [-376+ebp]                     ;363.8
        mov       edx, DWORD PTR [-372+ebp]                     ;384.141
        mov       DWORD PTR [-220+ebp], edi                     ;384.4
        sub       edi, esi                                      ;
        mov       DWORD PTR [-244+ebp], edi                     ;
        mov       DWORD PTR [-140+ebp], eax                     ;384.141
        mov       edi, DWORD PTR [-380+ebp]                     ;
        mov       DWORD PTR [-172+ebp], ecx                     ;363.8
        mov       DWORD PTR [-152+ebp], edx                     ;384.141
        imul      edi, eax                                      ;
        mov       eax, DWORD PTR [-368+ebp]                     ;
        sub       esi, edi                                      ;
        mov       ecx, DWORD PTR [-360+ebp]                     ;384.141
        imul      eax, edx                                      ;
        mov       edx, DWORD PTR [-208+ebp]                     ;
        sub       edi, eax                                      ;
        imul      edx, ecx                                      ;
        sub       eax, edx                                      ;
        mov       DWORD PTR [-228+ebp], ecx                     ;384.141
        add       edi, eax                                      ;
        mov       ecx, DWORD PTR [-244+ebp]                     ;
        mov       eax, DWORD PTR [-400+ebp]                     ;
        add       ecx, esi                                      ;
        add       ecx, edi                                      ;
        add       ecx, edx                                      ;
        mov       DWORD PTR [-176+ebp], 0                       ;
        shl       eax, 2                                        ;
        cmp       DWORD PTR [-376+ebp], 0                       ;363.8
        jle       .B11.84       ; Prob 10%                      ;363.8
                                ; LOE eax ecx xmm0
.B11.69:                        ; Preds .B11.68
        mov       DWORD PTR [-168+ebp], eax                     ;
        mov       DWORD PTR [-244+ebp], ecx                     ;
        mov       edx, DWORD PTR [-176+ebp]                     ;
        mov       esi, DWORD PTR [-204+ebp]                     ;
                                ; LOE edx esi
.B11.70:                        ; Preds .B11.82 .B11.69
        mov       esi, DWORD PTR [-172+ebp]                     ;
        xor       eax, eax                                      ;
        mov       ecx, DWORD PTR [-156+ebp]                     ;
        mov       edi, DWORD PTR [-136+ebp]                     ;
                                ; LOE eax edx ecx esi edi
.B11.71:                        ; Preds .B11.170 .B11.70
        test      ecx, ecx                                      ;363.8
        jle       .B11.170      ; Prob 10%                      ;363.8
                                ; LOE eax edx ecx esi edi
.B11.72:                        ; Preds .B11.71
        test      edi, edi                                      ;363.8
        jg        .B11.75       ; Prob 50%                      ;363.8
                                ; LOE eax edx ecx esi edi
.B11.73:                        ; Preds .B11.72
        xor       edi, edi                                      ;
                                ; LOE eax edx ecx esi edi
.B11.74:                        ; Preds .B11.168 .B11.74 .B11.73
        inc       eax                                           ;363.8
        cmp       eax, esi                                      ;363.8
        jae       .B11.167      ; Prob 18%                      ;363.8
        jmp       .B11.74       ; Prob 100%                     ;363.8
                                ; LOE eax edx ecx esi edi
.B11.75:                        ; Preds .B11.72
        mov       ecx, DWORD PTR [-228+ebp]                     ;
        imul      ecx, edx                                      ;
        mov       edi, DWORD PTR [-220+ebp]                     ;
        mov       DWORD PTR [-504+ebp], 0                       ;
        mov       esi, DWORD PTR [-504+ebp]                     ;
        add       edi, ecx                                      ;
        add       ecx, DWORD PTR [-244+ebp]                     ;
        mov       DWORD PTR [-128+ebp], edi                     ;
        mov       DWORD PTR [-164+ebp], ecx                     ;
        mov       DWORD PTR [-160+ebp], eax                     ;
        mov       DWORD PTR [-176+ebp], edx                     ;
                                ; LOE esi
.B11.76:                        ; Preds .B11.80 .B11.78 .B11.75
        cmp       DWORD PTR [-136+ebp], 24                      ;363.8
        jle       .B11.192      ; Prob 0%                       ;363.8
                                ; LOE esi
.B11.77:                        ; Preds .B11.76
        mov       edx, DWORD PTR [-160+ebp]                     ;363.8
        mov       eax, esi                                      ;363.8
        imul      edx, DWORD PTR [-152+ebp]                     ;363.8
        imul      eax, DWORD PTR [-140+ebp]                     ;363.8
        add       edx, DWORD PTR [-164+ebp]                     ;363.8
        push      DWORD PTR [-168+ebp]                          ;363.8
        push      0                                             ;363.8
        add       edx, eax                                      ;363.8
        push      edx                                           ;363.8
        call      __intel_fast_memset                           ;363.8
                                ; LOE esi
.B11.237:                       ; Preds .B11.77
        add       esp, 12                                       ;363.8
                                ; LOE esi
.B11.78:                        ; Preds .B11.204 .B11.206 .B11.237
        inc       esi                                           ;363.8
        cmp       esi, DWORD PTR [-156+ebp]                     ;363.8
        jb        .B11.76       ; Prob 82%                      ;363.8
                                ; LOE esi
.B11.79:                        ; Preds .B11.78
        mov       eax, DWORD PTR [-160+ebp]                     ;363.8
        inc       eax                                           ;363.8
        mov       DWORD PTR [-160+ebp], eax                     ;363.8
        cmp       eax, DWORD PTR [-172+ebp]                     ;363.8
        jae       .B11.81       ; Prob 18%                      ;363.8
                                ; LOE
.B11.80:                        ; Preds .B11.79
        xor       esi, esi                                      ;
        jmp       .B11.76       ; Prob 100%                     ;
                                ; LOE esi
.B11.81:                        ; Preds .B11.79                 ; Infreq
        mov       edx, DWORD PTR [-176+ebp]                     ;
        mov       esi, DWORD PTR [-204+ebp]                     ;
                                ; LOE edx esi
.B11.82:                        ; Preds .B11.171 .B11.81        ; Infreq
        inc       edx                                           ;363.8
        cmp       edx, esi                                      ;363.8
        jb        .B11.70       ; Prob 82%                      ;363.8
                                ; LOE edx esi
.B11.84:                        ; Preds .B11.167 .B11.82 .B11.68 .B11.67 ; Infreq
        mov       eax, DWORD PTR [-428+ebp]                     ;364.8
        mov       edx, DWORD PTR [-436+ebp]                     ;364.8
        test      edx, edx                                      ;364.8
        mov       DWORD PTR [-220+ebp], eax                     ;364.8
        mov       DWORD PTR [-204+ebp], edx                     ;364.8
        jle       .B11.101      ; Prob 10%                      ;364.8
                                ; LOE
.B11.85:                        ; Preds .B11.84                 ; Infreq
        mov       esi, DWORD PTR [-448+ebp]                     ;364.8
        mov       DWORD PTR [-172+ebp], esi                     ;364.8
        pxor      xmm0, xmm0                                    ;364.8
        mov       esi, DWORD PTR [-472+ebp]                     ;364.8
        and       esi, -8                                       ;364.8
        mov       DWORD PTR [-128+ebp], esi                     ;364.8
        mov       esi, DWORD PTR [-464+ebp]                     ;
        mov       edi, DWORD PTR [-496+ebp]                     ;384.4
        shl       esi, 2                                        ;
        mov       eax, DWORD PTR [-456+ebp]                     ;384.122
        mov       ecx, DWORD PTR [-460+ebp]                     ;364.8
        mov       edx, DWORD PTR [-444+ebp]                     ;384.122
        mov       DWORD PTR [-228+ebp], edi                     ;384.4
        sub       edi, esi                                      ;
        mov       DWORD PTR [-504+ebp], edi                     ;
        mov       DWORD PTR [-120+ebp], eax                     ;384.122
        mov       edi, DWORD PTR [-452+ebp]                     ;
        mov       DWORD PTR [-132+ebp], ecx                     ;364.8
        mov       DWORD PTR [-124+ebp], edx                     ;384.122
        imul      edi, eax                                      ;
        mov       eax, DWORD PTR [-440+ebp]                     ;
        sub       esi, edi                                      ;
        mov       ecx, DWORD PTR [-432+ebp]                     ;384.122
        imul      eax, edx                                      ;
        mov       edx, DWORD PTR [-220+ebp]                     ;
        sub       edi, eax                                      ;
        imul      edx, ecx                                      ;
        sub       eax, edx                                      ;
        mov       DWORD PTR [-244+ebp], ecx                     ;384.122
        add       edi, eax                                      ;
        mov       ecx, DWORD PTR [-504+ebp]                     ;
        add       ecx, esi                                      ;
        add       ecx, edi                                      ;
        add       ecx, edx                                      ;
        mov       edx, DWORD PTR [-472+ebp]                     ;
        mov       DWORD PTR [-176+ebp], 0                       ;
        cmp       DWORD PTR [-448+ebp], 0                       ;364.8
        lea       eax, DWORD PTR [edx*4]                        ;
        jle       .B11.101      ; Prob 10%                      ;364.8
                                ; LOE eax edx ecx xmm0
.B11.86:                        ; Preds .B11.85                 ; Infreq
        mov       DWORD PTR [-116+ebp], edx                     ;
        mov       DWORD PTR [-168+ebp], eax                     ;
        mov       DWORD PTR [-504+ebp], ecx                     ;
        mov       edx, DWORD PTR [-176+ebp]                     ;
        mov       esi, DWORD PTR [-204+ebp]                     ;
                                ; LOE edx esi
.B11.87:                        ; Preds .B11.99 .B11.86         ; Infreq
        mov       esi, DWORD PTR [-172+ebp]                     ;
        xor       eax, eax                                      ;
        mov       ecx, DWORD PTR [-132+ebp]                     ;
        mov       edi, DWORD PTR [-116+ebp]                     ;
                                ; LOE eax edx ecx esi edi
.B11.88:                        ; Preds .B11.150 .B11.87        ; Infreq
        test      ecx, ecx                                      ;364.8
        jle       .B11.150      ; Prob 10%                      ;364.8
                                ; LOE eax edx ecx esi edi
.B11.89:                        ; Preds .B11.88                 ; Infreq
        test      edi, edi                                      ;364.8
        jg        .B11.92       ; Prob 50%                      ;364.8
                                ; LOE eax edx ecx esi edi
.B11.90:                        ; Preds .B11.89                 ; Infreq
        xor       edi, edi                                      ;
                                ; LOE eax edx ecx esi edi
.B11.91:                        ; Preds .B11.148 .B11.91 .B11.90 ; Infreq
        inc       eax                                           ;364.8
        cmp       eax, esi                                      ;364.8
        jae       .B11.147      ; Prob 18%                      ;364.8
        jmp       .B11.91       ; Prob 100%                     ;364.8
                                ; LOE eax edx ecx esi edi
.B11.92:                        ; Preds .B11.89                 ; Infreq
        mov       ecx, DWORD PTR [-244+ebp]                     ;
        imul      ecx, edx                                      ;
        mov       edi, DWORD PTR [-504+ebp]                     ;
        mov       DWORD PTR [-164+ebp], 0                       ;
        mov       esi, DWORD PTR [-164+ebp]                     ;
        add       edi, ecx                                      ;
        add       ecx, DWORD PTR [-228+ebp]                     ;
        mov       DWORD PTR [-136+ebp], edi                     ;
        mov       DWORD PTR [-160+ebp], ecx                     ;
        mov       DWORD PTR [-156+ebp], eax                     ;
        mov       DWORD PTR [-176+ebp], edx                     ;
                                ; LOE esi
.B11.93:                        ; Preds .B11.97 .B11.95 .B11.92 ; Infreq
        cmp       DWORD PTR [-116+ebp], 24                      ;364.8
        jle       .B11.153      ; Prob 0%                       ;364.8
                                ; LOE esi
.B11.94:                        ; Preds .B11.93                 ; Infreq
        mov       edx, DWORD PTR [-156+ebp]                     ;364.8
        imul      edx, DWORD PTR [-124+ebp]                     ;364.8
        mov       eax, DWORD PTR [-120+ebp]                     ;364.8
        imul      eax, esi                                      ;364.8
        add       edx, DWORD PTR [-136+ebp]                     ;364.8
        push      DWORD PTR [-168+ebp]                          ;364.8
        push      0                                             ;364.8
        add       edx, eax                                      ;364.8
        push      edx                                           ;364.8
        call      __intel_fast_memset                           ;364.8
                                ; LOE esi
.B11.238:                       ; Preds .B11.94                 ; Infreq
        add       esp, 12                                       ;364.8
                                ; LOE esi
.B11.95:                        ; Preds .B11.159 .B11.157 .B11.238 ; Infreq
        inc       esi                                           ;364.8
        cmp       esi, DWORD PTR [-132+ebp]                     ;364.8
        jb        .B11.93       ; Prob 82%                      ;364.8
                                ; LOE esi
.B11.96:                        ; Preds .B11.95                 ; Infreq
        mov       eax, DWORD PTR [-156+ebp]                     ;364.8
        inc       eax                                           ;364.8
        mov       DWORD PTR [-156+ebp], eax                     ;364.8
        cmp       eax, DWORD PTR [-172+ebp]                     ;364.8
        jae       .B11.98       ; Prob 18%                      ;364.8
                                ; LOE
.B11.97:                        ; Preds .B11.96                 ; Infreq
        xor       esi, esi                                      ;
        jmp       .B11.93       ; Prob 100%                     ;
                                ; LOE esi
.B11.98:                        ; Preds .B11.96                 ; Infreq
        mov       edx, DWORD PTR [-176+ebp]                     ;
        mov       esi, DWORD PTR [-204+ebp]                     ;
                                ; LOE edx esi
.B11.99:                        ; Preds .B11.98 .B11.151        ; Infreq
        inc       edx                                           ;364.8
        cmp       edx, esi                                      ;364.8
        jb        .B11.87       ; Prob 82%                      ;364.8
                                ; LOE edx esi
.B11.101:                       ; Preds .B11.147 .B11.99 .B11.85 .B11.84 ; Infreq
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFVEH] ;366.2
        test      eax, eax                                      ;366.2
        mov       DWORD PTR [-120+ebp], eax                     ;366.2
        jle       .B11.109      ; Prob 2%                       ;366.2
                                ; LOE
.B11.102:                       ; Preds .B11.101                ; Infreq
        mov       eax, DWORD PTR [-384+ebp]                     ;384.141
        mov       edi, DWORD PTR [-372+ebp]                     ;384.141
        pxor      xmm0, xmm0                                    ;369.36
        mov       DWORD PTR [-160+ebp], eax                     ;384.141
        mov       DWORD PTR [-172+ebp], edi                     ;384.141
        mov       eax, DWORD PTR [-360+ebp]                     ;384.141
        mov       edi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;
        shl       edi, 5                                        ;
        mov       DWORD PTR [-176+ebp], eax                     ;384.141
        neg       edi                                           ;
        mov       eax, DWORD PTR [-288+ebp]                     ;
        shl       eax, 2                                        ;
        add       edi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;
        neg       eax                                           ;
        mov       DWORD PTR [-124+ebp], edi                     ;
        add       eax, DWORD PTR [-320+ebp]                     ;
        mov       edi, DWORD PTR [-464+ebp]                     ;
        shl       edi, 2                                        ;
        mov       DWORD PTR [-152+ebp], eax                     ;
        mov       eax, DWORD PTR [-496+ebp]                     ;
        mov       edx, DWORD PTR [-456+ebp]                     ;384.122
        sub       eax, edi                                      ;
        mov       edi, DWORD PTR [-452+ebp]                     ;
        imul      edi, edx                                      ;
        mov       esi, DWORD PTR [-444+ebp]                     ;384.122
        sub       eax, edi                                      ;
        mov       edi, DWORD PTR [-440+ebp]                     ;
        mov       DWORD PTR [-168+ebp], esi                     ;384.122
        imul      edi, esi                                      ;
        mov       esi, DWORD PTR [_DYNUST_FUEL_MODULE_mp_VEHFUEL+32] ;
        mov       DWORD PTR [-156+ebp], edx                     ;384.122
        mov       ecx, DWORD PTR [-432+ebp]                     ;384.122
        mov       edx, DWORD PTR [-220+ebp]                     ;
        imul      edx, ecx                                      ;
        mov       DWORD PTR [-164+ebp], ecx                     ;384.122
        lea       ecx, DWORD PTR [esi*8]                        ;
        add       edi, edx                                      ;
        lea       ecx, DWORD PTR [ecx+esi*4]                    ;
        mov       esi, DWORD PTR [-380+ebp]                     ;
        sub       eax, edi                                      ;
        imul      esi, DWORD PTR [-384+ebp]                     ;
        neg       ecx                                           ;
        mov       edi, DWORD PTR [-392+ebp]                     ;
        shl       edi, 2                                        ;
        mov       edx, DWORD PTR [-424+ebp]                     ;
        sub       edx, edi                                      ;
        mov       edi, DWORD PTR [-208+ebp]                     ;
        sub       edx, esi                                      ;
        mov       esi, DWORD PTR [-368+ebp]                     ;
        imul      esi, DWORD PTR [-372+ebp]                     ;
        imul      edi, DWORD PTR [-360+ebp]                     ;
        add       esi, edi                                      ;
        sub       edx, esi                                      ;
        mov       DWORD PTR [-504+ebp], 1                       ;
        add       ecx, DWORD PTR [_DYNUST_FUEL_MODULE_mp_VEHFUEL] ;
        mov       DWORD PTR [-244+ebp], edx                     ;
        mov       DWORD PTR [-204+ebp], ecx                     ;
        mov       DWORD PTR [-228+ebp], eax                     ;
        mov       edx, DWORD PTR [-504+ebp]                     ;
                                ; LOE edx xmm0
.B11.103:                       ; Preds .B11.107 .B11.102       ; Infreq
        mov       eax, edx                                      ;367.4
        mov       esi, 1                                        ;368.17
        shl       eax, 5                                        ;367.4
        mov       edi, DWORD PTR [-124+ebp]                     ;367.10
        movsx     ecx, BYTE PTR [5+edi+eax]                     ;367.10
        cmp       ecx, 3                                        ;368.17
        movss     xmm1, DWORD PTR [12+edi+eax]                  ;369.7
        cmove     ecx, esi                                      ;368.17
        comiss    xmm1, xmm0                                    ;369.36
        jbe       .B11.107      ; Prob 50%                      ;369.36
                                ; LOE eax edx ecx edi xmm0 xmm1
.B11.104:                       ; Preds .B11.103                ; Infreq
        mov       DWORD PTR [-132+ebp], ecx                     ;
        cvttss2si ecx, xmm1                                     ;370.16
        mov       esi, edi                                      ;371.6
        inc       ecx                                           ;370.6
        mov       DWORD PTR [-128+ebp], ecx                     ;370.6
        mov       edi, DWORD PTR [-152+ebp]                     ;371.6
        movsx     ecx, WORD PTR [22+esi+eax]                    ;371.6
        movsx     eax, WORD PTR [20+esi+eax]                    ;372.6
        mov       ecx, DWORD PTR [edi+ecx*4]                    ;371.6
        test      ecx, ecx                                      ;373.12
        mov       eax, DWORD PTR [edi+eax*4]                    ;372.6
        mov       DWORD PTR [-136+ebp], ecx                     ;371.6
        mov       DWORD PTR [-140+ebp], eax                     ;372.6
        mov       eax, DWORD PTR [-128+ebp]                     ;373.12
        mov       ecx, DWORD PTR [-132+ebp]                     ;373.12
        jle       .B11.107      ; Prob 16%                      ;373.12
                                ; LOE eax edx ecx al cl ah ch xmm0
.B11.105:                       ; Preds .B11.104                ; Infreq
        cmp       DWORD PTR [-140+ebp], 0                       ;373.23
        jle       .B11.107      ; Prob 16%                      ;373.23
                                ; LOE eax edx ecx al cl ah ch xmm0
.B11.106:                       ; Preds .B11.105                ; Infreq
        mov       esi, DWORD PTR [-228+ebp]                     ;374.8
        movaps    xmm2, xmm0                                    ;374.8
        mov       DWORD PTR [-132+ebp], ecx                     ;
        mov       DWORD PTR [-128+ebp], eax                     ;
        lea       edi, DWORD PTR [esi+ecx*4]                    ;374.8
        mov       DWORD PTR [-504+ebp], edi                     ;374.8
        mov       edi, DWORD PTR [-156+ebp]                     ;371.6
        mov       ecx, DWORD PTR [-164+ebp]                     ;370.6
        mov       esi, DWORD PTR [-168+ebp]                     ;372.6
        imul      ecx, eax                                      ;370.6
        imul      edi, DWORD PTR [-136+ebp]                     ;371.6
        mov       eax, DWORD PTR [-140+ebp]                     ;372.6
        imul      esi, eax                                      ;372.6
        imul      eax, DWORD PTR [-172+ebp]                     ;372.6
        add       edi, esi                                      ;374.8
        lea       esi, DWORD PTR [edx*8]                        ;374.8
        add       ecx, edi                                      ;374.8
        lea       esi, DWORD PTR [esi+edx*4]                    ;374.8
        mov       edi, DWORD PTR [-204+ebp]                     ;374.76
        movss     xmm1, DWORD PTR [edi+esi]                     ;374.76
        subss     xmm1, DWORD PTR [4+edi+esi]                   ;374.94
        mov       edi, DWORD PTR [-504+ebp]                     ;374.8
        maxss     xmm2, xmm1                                    ;374.8
        mov       esi, DWORD PTR [-132+ebp]                     ;375.8
        addss     xmm2, DWORD PTR [ecx+edi]                     ;374.8
        movss     DWORD PTR [ecx+edi], xmm2                     ;374.8
        mov       ecx, DWORD PTR [-244+ebp]                     ;375.8
        mov       edi, DWORD PTR [-136+ebp]                     ;371.6
        imul      edi, DWORD PTR [-160+ebp]                     ;371.6
        add       edi, eax                                      ;375.8
        lea       esi, DWORD PTR [ecx+esi*4]                    ;375.8
        mov       ecx, DWORD PTR [-128+ebp]                     ;370.6
        imul      ecx, DWORD PTR [-176+ebp]                     ;370.6
        add       ecx, edi                                      ;375.8
        inc       DWORD PTR [ecx+esi]                           ;375.8
                                ; LOE edx xmm0
.B11.107:                       ; Preds .B11.105 .B11.104 .B11.103 .B11.106 ; Infreq
        inc       edx                                           ;378.3
        cmp       edx, DWORD PTR [-120+ebp]                     ;378.3
        jle       .B11.103      ; Prob 82%                      ;378.3
                                ; LOE edx xmm0
.B11.109:                       ; Preds .B11.107 .B11.101       ; Infreq
        mov       eax, DWORD PTR [-180+ebp]                     ;380.1
        test      eax, eax                                      ;380.1
        mov       DWORD PTR [-156+ebp], eax                     ;380.1
        jle       .B11.126      ; Prob 2%                       ;380.1
                                ; LOE
.B11.110:                       ; Preds .B11.109                ; Infreq
        movss     xmm3, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIMPERIOD] ;383.13
        movss     xmm0, DWORD PTR [_2il0floatpacket.74]         ;383.13
        andps     xmm0, xmm3                                    ;383.13
        pxor      xmm3, xmm0                                    ;383.13
        movss     xmm1, DWORD PTR [_2il0floatpacket.75]         ;383.13
        movaps    xmm2, xmm3                                    ;383.13
        mov       edx, DWORD PTR [-384+ebp]                     ;384.141
        cmpltss   xmm2, xmm1                                    ;383.13
        andps     xmm1, xmm2                                    ;383.13
        movaps    xmm2, xmm3                                    ;383.13
        mov       edi, DWORD PTR [-372+ebp]                     ;384.141
        addss     xmm2, xmm1                                    ;383.13
        mov       DWORD PTR [-228+ebp], edx                     ;384.141
        subss     xmm2, xmm1                                    ;383.13
        movaps    xmm7, xmm2                                    ;383.13
        mov       DWORD PTR [-176+ebp], edi                     ;384.141
        subss     xmm7, xmm3                                    ;383.13
        mov       edx, DWORD PTR [-360+ebp]                     ;384.141
        movaps    xmm5, xmm7                                    ;383.13
        mov       edi, DWORD PTR [-464+ebp]                     ;
        shl       edi, 2                                        ;
        cmpless   xmm7, DWORD PTR [_2il0floatpacket.77]         ;383.13
        movss     xmm4, DWORD PTR [_2il0floatpacket.76]         ;383.13
        mov       DWORD PTR [-128+ebp], edx                     ;384.141
        movaps    xmm6, xmm4                                    ;383.13
        mov       edx, DWORD PTR [-496+ebp]                     ;
        addss     xmm6, xmm4                                    ;383.13
        cmpnless  xmm5, xmm4                                    ;383.13
        mov       eax, DWORD PTR [-456+ebp]                     ;384.122
        sub       edx, edi                                      ;
        mov       edi, DWORD PTR [-452+ebp]                     ;
        andps     xmm5, xmm6                                    ;383.13
        imul      edi, eax                                      ;
        subss     xmm2, xmm5                                    ;383.13
        mov       DWORD PTR [-204+ebp], eax                     ;384.122
        sub       edx, edi                                      ;
        mov       esi, DWORD PTR [-444+ebp]                     ;384.122
        andps     xmm7, xmm6                                    ;383.13
        mov       ecx, DWORD PTR [-432+ebp]                     ;384.122
        addss     xmm2, xmm7                                    ;383.13
        mov       edi, DWORD PTR [-440+ebp]                     ;
        orps      xmm2, xmm0                                    ;383.13
        mov       eax, DWORD PTR [-220+ebp]                     ;
        imul      edi, esi                                      ;
        imul      eax, ecx                                      ;
        mov       DWORD PTR [-132+ebp], ecx                     ;384.122
        add       edi, eax                                      ;
        mov       ecx, DWORD PTR [-392+ebp]                     ;
        sub       edx, edi                                      ;
        shl       ecx, 2                                        ;
        mov       eax, DWORD PTR [-424+ebp]                     ;
        mov       DWORD PTR [-172+ebp], esi                     ;384.122
        sub       eax, ecx                                      ;
        mov       esi, DWORD PTR [-380+ebp]                     ;
        mov       edi, DWORD PTR [-368+ebp]                     ;
        mov       ecx, DWORD PTR [-208+ebp]                     ;
        imul      esi, DWORD PTR [-384+ebp]                     ;
        imul      edi, DWORD PTR [-372+ebp]                     ;
        imul      ecx, DWORD PTR [-360+ebp]                     ;
        sub       eax, esi                                      ;
        add       edi, ecx                                      ;
        sub       eax, edi                                      ;
        cvtss2si  edi, xmm2                                     ;383.2
        mov       DWORD PTR [-160+ebp], 1                       ;
        mov       DWORD PTR [-140+ebp], edi                     ;383.2
        mov       edi, 1                                        ;383.2
        mov       DWORD PTR [-504+ebp], eax                     ;383.2
        mov       DWORD PTR [-244+ebp], edx                     ;383.2
        mov       ecx, DWORD PTR [-160+ebp]                     ;383.2
                                ; LOE ecx edi
.B11.111:                       ; Preds .B11.124 .B11.110       ; Infreq
        mov       edx, DWORD PTR [-204+ebp]                     ;
        mov       esi, edi                                      ;381.2
        mov       eax, DWORD PTR [-228+ebp]                     ;
        imul      edx, ecx                                      ;
        imul      eax, ecx                                      ;
        add       edx, DWORD PTR [-244+ebp]                     ;
        add       eax, DWORD PTR [-504+ebp]                     ;
        mov       DWORD PTR [-208+ebp], eax                     ;
        mov       DWORD PTR [-220+ebp], edx                     ;
        mov       DWORD PTR [-160+ebp], ecx                     ;
                                ; LOE esi
.B11.112:                       ; Preds .B11.111 .B11.123       ; Infreq
        push      32                                            ;382.2
        push      OFFSET FLAT: DYNUST_FUEL_MODULE_mp_WRITEOUTFUELCONSUM_CITY$format_pack.0.11 ;382.2
        mov       eax, DWORD PTR [-160+ebp]                     ;382.2
        lea       edx, DWORD PTR [-152+ebp]                     ;382.2
        push      edx                                           ;382.2
        push      OFFSET FLAT: __STRLITPACK_139.0.11            ;382.2
        push      -2088435968                                   ;382.2
        push      6059                                          ;382.2
        mov       DWORD PTR [-40+ebp], 0                        ;382.2
        lea       ecx, DWORD PTR [-40+ebp]                      ;382.2
        push      ecx                                           ;382.2
        mov       DWORD PTR [-152+ebp], eax                     ;382.2
        call      _for_write_seq_fmt                            ;382.2
                                ; LOE esi
.B11.239:                       ; Preds .B11.112                ; Infreq
        add       esp, 28                                       ;382.2
                                ; LOE esi
.B11.113:                       ; Preds .B11.239                ; Infreq
        mov       DWORD PTR [-136+ebp], esi                     ;382.2
        lea       eax, DWORD PTR [-136+ebp]                     ;382.2
        push      eax                                           ;382.2
        push      OFFSET FLAT: __STRLITPACK_140.0.11            ;382.2
        lea       edx, DWORD PTR [-40+ebp]                      ;382.2
        push      edx                                           ;382.2
        call      _for_write_seq_fmt_xmit                       ;382.2
                                ; LOE esi
.B11.240:                       ; Preds .B11.113                ; Infreq
        add       esp, 12                                       ;382.2
                                ; LOE esi
.B11.114:                       ; Preds .B11.240                ; Infreq
        cmp       DWORD PTR [-140+ebp], 0                       ;383.2
        jle       .B11.123      ; Prob 2%                       ;383.2
                                ; LOE esi
.B11.115:                       ; Preds .B11.114                ; Infreq
        mov       edx, DWORD PTR [-172+ebp]                     ;
        mov       eax, 1                                        ;
        mov       ecx, DWORD PTR [-176+ebp]                     ;
        imul      edx, esi                                      ;
        imul      ecx, esi                                      ;
        mov       edi, DWORD PTR [-132+ebp]                     ;
        add       edx, DWORD PTR [-220+ebp]                     ;
        add       ecx, DWORD PTR [-208+ebp]                     ;
        mov       DWORD PTR [-164+ebp], edi                     ;
        mov       DWORD PTR [-168+ebp], esi                     ;
        mov       edi, DWORD PTR [-128+ebp]                     ;
        mov       esi, DWORD PTR [-164+ebp]                     ;
        mov       DWORD PTR [-120+ebp], ecx                     ;
        mov       DWORD PTR [-124+ebp], edx                     ;
        mov       DWORD PTR [-116+ebp], eax                     ;
                                ; LOE esi edi
.B11.116:                       ; Preds .B11.121 .B11.115       ; Infreq
        push      32                                            ;384.4
        push      OFFSET FLAT: DYNUST_FUEL_MODULE_mp_WRITEOUTFUELCONSUM_CITY$format_pack.0.11+72 ;384.4
        cvtsi2ss  xmm0, DWORD PTR [-116+ebp]                    ;384.4
        mov       DWORD PTR [-40+ebp], 0                        ;384.4
        lea       eax, DWORD PTR [-104+ebp]                     ;384.4
        push      eax                                           ;384.4
        push      OFFSET FLAT: __STRLITPACK_141.0.11            ;384.4
        push      -2088435968                                   ;384.4
        push      6059                                          ;384.4
        movss     DWORD PTR [-104+ebp], xmm0                    ;384.4
        lea       edx, DWORD PTR [-40+ebp]                      ;384.4
        push      edx                                           ;384.4
        call      _for_write_seq_fmt                            ;384.4
                                ; LOE esi edi
.B11.241:                       ; Preds .B11.116                ; Infreq
        add       esp, 28                                       ;384.4
                                ; LOE esi edi
.B11.117:                       ; Preds .B11.241                ; Infreq
        mov       eax, DWORD PTR [-124+ebp]                     ;384.4
        lea       ecx, DWORD PTR [-96+ebp]                      ;384.4
        push      ecx                                           ;384.4
        push      OFFSET FLAT: __STRLITPACK_142.0.11            ;384.4
        mov       edx, DWORD PTR [4+esi+eax]                    ;384.4
        lea       eax, DWORD PTR [-40+ebp]                      ;384.4
        push      eax                                           ;384.4
        mov       DWORD PTR [-96+ebp], edx                      ;384.4
        call      _for_write_seq_fmt_xmit                       ;384.4
                                ; LOE esi edi
.B11.242:                       ; Preds .B11.117                ; Infreq
        add       esp, 12                                       ;384.4
                                ; LOE esi edi
.B11.118:                       ; Preds .B11.242                ; Infreq
        mov       eax, DWORD PTR [-120+ebp]                     ;384.4
        lea       ecx, DWORD PTR [-88+ebp]                      ;384.4
        push      ecx                                           ;384.4
        push      OFFSET FLAT: __STRLITPACK_143.0.11            ;384.4
        mov       edx, DWORD PTR [4+edi+eax]                    ;384.4
        lea       eax, DWORD PTR [-40+ebp]                      ;384.4
        push      eax                                           ;384.4
        mov       DWORD PTR [-88+ebp], edx                      ;384.4
        call      _for_write_seq_fmt_xmit                       ;384.4
                                ; LOE esi edi
.B11.243:                       ; Preds .B11.118                ; Infreq
        add       esp, 12                                       ;384.4
                                ; LOE esi edi
.B11.119:                       ; Preds .B11.243                ; Infreq
        mov       eax, DWORD PTR [-124+ebp]                     ;384.4
        lea       ecx, DWORD PTR [-72+ebp]                      ;384.4
        push      ecx                                           ;384.4
        push      OFFSET FLAT: __STRLITPACK_144.0.11            ;384.4
        mov       edx, DWORD PTR [8+esi+eax]                    ;384.4
        lea       eax, DWORD PTR [-40+ebp]                      ;384.4
        push      eax                                           ;384.4
        mov       DWORD PTR [-72+ebp], edx                      ;384.4
        call      _for_write_seq_fmt_xmit                       ;384.4
                                ; LOE esi edi
.B11.244:                       ; Preds .B11.119                ; Infreq
        add       esp, 12                                       ;384.4
                                ; LOE esi edi
.B11.120:                       ; Preds .B11.244                ; Infreq
        mov       eax, DWORD PTR [-120+ebp]                     ;384.4
        lea       ecx, DWORD PTR [-64+ebp]                      ;384.4
        push      ecx                                           ;384.4
        push      OFFSET FLAT: __STRLITPACK_145.0.11            ;384.4
        mov       edx, DWORD PTR [8+edi+eax]                    ;384.4
        lea       eax, DWORD PTR [-40+ebp]                      ;384.4
        push      eax                                           ;384.4
        mov       DWORD PTR [-64+ebp], edx                      ;384.4
        call      _for_write_seq_fmt_xmit                       ;384.4
                                ; LOE esi edi
.B11.245:                       ; Preds .B11.120                ; Infreq
        add       esp, 12                                       ;384.4
                                ; LOE esi edi
.B11.121:                       ; Preds .B11.245                ; Infreq
        mov       eax, DWORD PTR [-116+ebp]                     ;385.2
        inc       eax                                           ;385.2
        add       esi, DWORD PTR [-132+ebp]                     ;385.2
        add       edi, DWORD PTR [-128+ebp]                     ;385.2
        mov       DWORD PTR [-116+ebp], eax                     ;385.2
        cmp       eax, DWORD PTR [-140+ebp]                     ;385.2
        jle       .B11.116      ; Prob 82%                      ;385.2
                                ; LOE esi edi
.B11.122:                       ; Preds .B11.121                ; Infreq
        mov       esi, DWORD PTR [-168+ebp]                     ;
                                ; LOE esi
.B11.123:                       ; Preds .B11.122 .B11.114       ; Infreq
        inc       esi                                           ;386.2
        cmp       esi, DWORD PTR [-156+ebp]                     ;386.2
        jle       .B11.112      ; Prob 82%                      ;386.2
                                ; LOE esi
.B11.124:                       ; Preds .B11.123                ; Infreq
        mov       ecx, DWORD PTR [-160+ebp]                     ;
        mov       edi, 1                                        ;
        inc       ecx                                           ;387.1
        cmp       ecx, DWORD PTR [-156+ebp]                     ;387.1
        jle       .B11.111      ; Prob 82%                      ;387.1
                                ; LOE ecx edi
.B11.126:                       ; Preds .B11.124 .B11.109       ; Infreq
        push      32                                            ;389.7
        xor       eax, eax                                      ;389.7
        lea       edx, DWORD PTR [-40+ebp]                      ;389.7
        push      eax                                           ;389.7
        push      OFFSET FLAT: __STRLITPACK_146.0.11            ;389.7
        push      -2088435968                                   ;389.7
        push      6058                                          ;389.7
        push      edx                                           ;389.7
        mov       DWORD PTR [-40+ebp], eax                      ;389.7
        call      _for_close                                    ;389.7
                                ; LOE
.B11.246:                       ; Preds .B11.126                ; Infreq
        add       esp, 24                                       ;389.7
                                ; LOE
.B11.127:                       ; Preds .B11.246                ; Infreq
        push      32                                            ;390.7
        xor       eax, eax                                      ;390.7
        lea       edx, DWORD PTR [-40+ebp]                      ;390.7
        push      eax                                           ;390.7
        push      OFFSET FLAT: __STRLITPACK_147.0.11            ;390.7
        push      -2088435968                                   ;390.7
        push      6059                                          ;390.7
        push      edx                                           ;390.7
        mov       DWORD PTR [-40+ebp], eax                      ;390.7
        call      _for_close                                    ;390.7
                                ; LOE
.B11.247:                       ; Preds .B11.127                ; Infreq
        add       esp, 24                                       ;390.7
                                ; LOE
.B11.128:                       ; Preds .B11.247                ; Infreq
        mov       ecx, DWORD PTR [-180+ebp]                     ;391.1
        test      ecx, ecx                                      ;391.1
        mov       eax, DWORD PTR [-280+ebp]                     ;394.1
        jle       .B11.133      ; Prob 28%                      ;391.1
                                ; LOE eax ecx
.B11.129:                       ; Preds .B11.128                ; Infreq
        imul      edx, DWORD PTR [-248+ebp], -40                ;
        mov       esi, 1                                        ;
        add       edx, eax                                      ;
        mov       DWORD PTR [-228+ebp], edx                     ;
        mov       DWORD PTR [-504+ebp], eax                     ;
        mov       DWORD PTR [-244+ebp], ecx                     ;
                                ; LOE esi
.B11.130:                       ; Preds .B11.131 .B11.129       ; Infreq
        mov       ecx, DWORD PTR [-228+ebp]                     ;392.14
        lea       edi, DWORD PTR [esi+esi*4]                    ;392.3
        mov       eax, DWORD PTR [16+ecx+edi*8]                 ;392.14
        mov       edx, eax                                      ;392.3
        shr       edx, 1                                        ;392.3
        and       eax, 1                                        ;392.3
        and       edx, 1                                        ;392.3
        add       eax, eax                                      ;392.3
        shl       edx, 2                                        ;392.3
        or        edx, eax                                      ;392.3
        or        edx, 262144                                   ;392.3
        push      edx                                           ;392.3
        push      DWORD PTR [4+ecx+edi*8]                       ;392.3
        call      _for_dealloc_allocatable                      ;392.3
                                ; LOE esi edi
.B11.248:                       ; Preds .B11.130                ; Infreq
        add       esp, 8                                        ;392.3
                                ; LOE esi edi
.B11.131:                       ; Preds .B11.248                ; Infreq
        mov       eax, DWORD PTR [-228+ebp]                     ;392.3
        inc       esi                                           ;393.1
        and       DWORD PTR [16+eax+edi*8], -2                  ;392.3
        mov       DWORD PTR [4+eax+edi*8], 0                    ;392.3
        cmp       esi, DWORD PTR [-244+ebp]                     ;393.1
        jle       .B11.130      ; Prob 82%                      ;393.1
                                ; LOE esi
.B11.132:                       ; Preds .B11.131                ; Infreq
        mov       eax, DWORD PTR [-504+ebp]                     ;
                                ; LOE eax
.B11.133:                       ; Preds .B11.132 .B11.128       ; Infreq
        mov       esi, DWORD PTR [-268+ebp]                     ;394.1
        mov       ecx, esi                                      ;394.1
        shr       ecx, 1                                        ;394.1
        mov       edx, esi                                      ;394.1
        and       ecx, 1                                        ;394.1
        and       edx, 1                                        ;394.1
        shl       ecx, 2                                        ;394.1
        add       edx, edx                                      ;394.1
        or        ecx, edx                                      ;394.1
        or        ecx, 262144                                   ;394.1
        push      ecx                                           ;394.1
        push      eax                                           ;394.1
        call      _for_dealloc_allocatable                      ;394.1
                                ; LOE esi
.B11.249:                       ; Preds .B11.133                ; Infreq
        add       esp, 8                                        ;394.1
                                ; LOE esi
.B11.134:                       ; Preds .B11.249                ; Infreq
        and       esi, -2                                       ;394.1
        mov       DWORD PTR [-280+ebp], 0                       ;394.1
        mov       DWORD PTR [-268+ebp], esi                     ;394.1
                                ; LOE
.B11.135:                       ; Preds .B11.134                ; Infreq
        mov       esi, DWORD PTR [-412+ebp]                     ;396.1
        test      esi, 1                                        ;396.1
        jne       .B11.143      ; Prob 3%                       ;396.1
                                ; LOE esi
.B11.136:                       ; Preds .B11.135 .B11.144       ; Infreq
        mov       esi, DWORD PTR [-308+ebp]                     ;396.1
        test      esi, 1                                        ;396.1
        jne       .B11.141      ; Prob 3%                       ;396.1
                                ; LOE esi
.B11.137:                       ; Preds .B11.136 .B11.142       ; Infreq
        mov       esi, DWORD PTR [-484+ebp]                     ;396.1
        test      esi, 1                                        ;396.1
        jne       .B11.139      ; Prob 3%                       ;396.1
                                ; LOE esi
.B11.138:                       ; Preds .B11.137 .B11.140       ; Infreq
        mov       esi, DWORD PTR [-188+ebp]                     ;396.1
        mov       edi, DWORD PTR [-184+ebp]                     ;396.1
        mov       esp, ebp                                      ;396.1
        pop       ebp                                           ;396.1
        mov       esp, ebx                                      ;396.1
        pop       ebx                                           ;396.1
        ret                                                     ;396.1
                                ; LOE
.B11.139:                       ; Preds .B11.137                ; Infreq
        mov       edx, esi                                      ;396.1
        mov       eax, esi                                      ;396.1
        shr       edx, 1                                        ;396.1
        and       eax, 1                                        ;396.1
        and       edx, 1                                        ;396.1
        add       eax, eax                                      ;396.1
        shl       edx, 2                                        ;396.1
        or        edx, eax                                      ;396.1
        or        edx, 262144                                   ;396.1
        push      edx                                           ;396.1
        push      DWORD PTR [-496+ebp]                          ;396.1
        call      _for_dealloc_allocatable                      ;396.1
                                ; LOE esi
.B11.250:                       ; Preds .B11.139                ; Infreq
        add       esp, 8                                        ;396.1
                                ; LOE esi
.B11.140:                       ; Preds .B11.250                ; Infreq
        and       esi, -2                                       ;396.1
        mov       DWORD PTR [-496+ebp], 0                       ;396.1
        mov       DWORD PTR [-484+ebp], esi                     ;396.1
        jmp       .B11.138      ; Prob 100%                     ;396.1
                                ; LOE
.B11.141:                       ; Preds .B11.136                ; Infreq
        mov       edx, esi                                      ;396.1
        mov       eax, esi                                      ;396.1
        shr       edx, 1                                        ;396.1
        and       eax, 1                                        ;396.1
        and       edx, 1                                        ;396.1
        add       eax, eax                                      ;396.1
        shl       edx, 2                                        ;396.1
        or        edx, eax                                      ;396.1
        or        edx, 262144                                   ;396.1
        push      edx                                           ;396.1
        push      DWORD PTR [-320+ebp]                          ;396.1
        call      _for_dealloc_allocatable                      ;396.1
                                ; LOE esi
.B11.251:                       ; Preds .B11.141                ; Infreq
        add       esp, 8                                        ;396.1
                                ; LOE esi
.B11.142:                       ; Preds .B11.251                ; Infreq
        and       esi, -2                                       ;396.1
        mov       DWORD PTR [-320+ebp], 0                       ;396.1
        mov       DWORD PTR [-308+ebp], esi                     ;396.1
        jmp       .B11.137      ; Prob 100%                     ;396.1
                                ; LOE
.B11.143:                       ; Preds .B11.135                ; Infreq
        mov       edx, esi                                      ;396.1
        mov       eax, esi                                      ;396.1
        shr       edx, 1                                        ;396.1
        and       eax, 1                                        ;396.1
        and       edx, 1                                        ;396.1
        add       eax, eax                                      ;396.1
        shl       edx, 2                                        ;396.1
        or        edx, eax                                      ;396.1
        or        edx, 262144                                   ;396.1
        push      edx                                           ;396.1
        push      DWORD PTR [-424+ebp]                          ;396.1
        call      _for_dealloc_allocatable                      ;396.1
                                ; LOE esi
.B11.252:                       ; Preds .B11.143                ; Infreq
        add       esp, 8                                        ;396.1
                                ; LOE esi
.B11.144:                       ; Preds .B11.252                ; Infreq
        and       esi, -2                                       ;396.1
        mov       DWORD PTR [-424+ebp], 0                       ;396.1
        mov       DWORD PTR [-412+ebp], esi                     ;396.1
        jmp       .B11.136      ; Prob 100%                     ;396.1
                                ; LOE
.B11.147:                       ; Preds .B11.91                 ; Infreq
        inc       edx                                           ;364.8
        cmp       edx, DWORD PTR [-204+ebp]                     ;364.8
        jae       .B11.101      ; Prob 18%                      ;364.8
                                ; LOE edx ecx esi edi
.B11.148:                       ; Preds .B11.147                ; Infreq
        mov       eax, edi                                      ;
        test      ecx, ecx                                      ;364.8
        jg        .B11.91       ; Prob 90%                      ;364.8
                                ; LOE eax edx ecx esi edi
.B11.149:                       ; Preds .B11.148                ; Infreq
        DB        15                                            ;
        DB        31                                            ;
        DB        132                                           ;
        DB        0                                             ;
        DB        0                                             ;
        DB        0                                             ;
        DB        0                                             ;
        DB        0                                             ;
        mov       edi, DWORD PTR [-116+ebp]                     ;
                                ; LOE eax edx ecx esi edi
.B11.150:                       ; Preds .B11.149 .B11.88        ; Infreq
        inc       eax                                           ;364.8
        cmp       eax, esi                                      ;364.8
        jb        .B11.88       ; Prob 82%                      ;364.8
                                ; LOE eax edx ecx esi edi
.B11.151:                       ; Preds .B11.150                ; Infreq
        mov       esi, DWORD PTR [-204+ebp]                     ;
        jmp       .B11.99       ; Prob 100%                     ;
                                ; LOE edx esi
.B11.153:                       ; Preds .B11.93                 ; Infreq
        cmp       DWORD PTR [-116+ebp], 8                       ;364.8
        jl        .B11.162      ; Prob 10%                      ;364.8
                                ; LOE esi
.B11.154:                       ; Preds .B11.153                ; Infreq
        mov       edx, DWORD PTR [-156+ebp]                     ;
        imul      edx, DWORD PTR [-124+ebp]                     ;
        pxor      xmm0, xmm0                                    ;
        mov       eax, DWORD PTR [-120+ebp]                     ;
        imul      eax, esi                                      ;
        mov       edi, DWORD PTR [-136+ebp]                     ;
        mov       DWORD PTR [-152+ebp], 0                       ;364.8
        mov       ecx, DWORD PTR [-128+ebp]                     ;364.8
        mov       DWORD PTR [-164+ebp], esi                     ;
        add       edi, edx                                      ;
        add       edi, eax                                      ;
        mov       DWORD PTR [-140+ebp], edi                     ;
        mov       edi, ecx                                      ;
        add       edx, DWORD PTR [-160+ebp]                     ;
        mov       esi, DWORD PTR [-152+ebp]                     ;
        add       edx, eax                                      ;
        mov       eax, DWORD PTR [-140+ebp]                     ;
                                ; LOE eax edx ecx esi edi xmm0
.B11.155:                       ; Preds .B11.155 .B11.154       ; Infreq
        movups    XMMWORD PTR [edx+esi*4], xmm0                 ;364.8
        movups    XMMWORD PTR [16+eax+esi*4], xmm0              ;364.8
        add       esi, 8                                        ;364.8
        cmp       esi, edi                                      ;364.8
        jb        .B11.155      ; Prob 82%                      ;364.8
                                ; LOE eax edx ecx esi edi xmm0
.B11.156:                       ; Preds .B11.155                ; Infreq
        mov       esi, DWORD PTR [-164+ebp]                     ;
                                ; LOE ecx esi
.B11.157:                       ; Preds .B11.156 .B11.162       ; Infreq
        cmp       ecx, DWORD PTR [-116+ebp]                     ;364.8
        jae       .B11.95       ; Prob 10%                      ;364.8
                                ; LOE ecx esi
.B11.158:                       ; Preds .B11.157                ; Infreq
        mov       eax, DWORD PTR [-156+ebp]                     ;
        xor       edi, edi                                      ;
        imul      eax, DWORD PTR [-124+ebp]                     ;
        mov       edx, DWORD PTR [-120+ebp]                     ;
        imul      edx, esi                                      ;
        add       eax, DWORD PTR [-160+ebp]                     ;
        add       eax, edx                                      ;
        mov       edx, DWORD PTR [-116+ebp]                     ;
                                ; LOE eax edx ecx esi edi
.B11.159:                       ; Preds .B11.159 .B11.158       ; Infreq
        mov       DWORD PTR [eax+ecx*4], edi                    ;364.8
        inc       ecx                                           ;364.8
        cmp       ecx, edx                                      ;364.8
        jb        .B11.159      ; Prob 82%                      ;364.8
        jmp       .B11.95       ; Prob 100%                     ;364.8
                                ; LOE eax edx ecx esi edi
.B11.162:                       ; Preds .B11.153                ; Infreq
        xor       ecx, ecx                                      ;364.8
        jmp       .B11.157      ; Prob 100%                     ;364.8
                                ; LOE ecx esi
.B11.163:                       ; Preds .B11.25                 ; Infreq
        mov       eax, 1                                        ;
        jmp       .B11.29       ; Prob 100%                     ;
                                ; LOE eax edx ecx esi
.B11.164:                       ; Preds .B11.34                 ; Infreq
        mov       esi, 1                                        ;
        jmp       .B11.38       ; Prob 100%                     ;
                                ; LOE edx ecx esi edi
.B11.165:                       ; Preds .B11.45                 ; Infreq
        mov       edx, 1                                        ;
        jmp       .B11.49       ; Prob 100%                     ;
                                ; LOE eax edx edi
.B11.166:                       ; Preds .B11.53                 ; Infreq
        mov       eax, 1                                        ;
        jmp       .B11.57       ; Prob 100%                     ;
                                ; LOE eax edx ecx esi
.B11.167:                       ; Preds .B11.74                 ; Infreq
        inc       edx                                           ;363.8
        cmp       edx, DWORD PTR [-204+ebp]                     ;363.8
        jae       .B11.84       ; Prob 18%                      ;363.8
                                ; LOE edx ecx esi edi
.B11.168:                       ; Preds .B11.167                ; Infreq
        mov       eax, edi                                      ;
        test      ecx, ecx                                      ;363.8
        jg        .B11.74       ; Prob 90%                      ;363.8
                                ; LOE eax edx ecx esi edi
.B11.169:                       ; Preds .B11.168                ; Infreq
        DB        15                                            ;
        DB        31                                            ;
        DB        68                                            ;
        DB        0                                             ;
        DB        0                                             ;
        mov       edi, DWORD PTR [-136+ebp]                     ;
                                ; LOE eax edx ecx esi edi
.B11.170:                       ; Preds .B11.169 .B11.71        ; Infreq
        inc       eax                                           ;363.8
        cmp       eax, esi                                      ;363.8
        jb        .B11.71       ; Prob 82%                      ;363.8
                                ; LOE eax edx ecx esi edi
.B11.171:                       ; Preds .B11.170                ; Infreq
        mov       esi, DWORD PTR [-204+ebp]                     ;
        jmp       .B11.82       ; Prob 100%                     ;
                                ; LOE edx esi
.B11.173:                       ; Preds .B11.8                  ; Infreq
        cmp       edx, 4                                        ;343.8
        jl        .B11.189      ; Prob 10%                      ;343.8
                                ; LOE eax edx edi al ah
.B11.174:                       ; Preds .B11.173                ; Infreq
        mov       ecx, eax                                      ;343.8
        and       ecx, 15                                       ;343.8
        je        .B11.177      ; Prob 50%                      ;343.8
                                ; LOE edx ecx edi
.B11.175:                       ; Preds .B11.174                ; Infreq
        test      cl, 3                                         ;343.8
        jne       .B11.189      ; Prob 10%                      ;343.8
                                ; LOE edx ecx edi
.B11.176:                       ; Preds .B11.175                ; Infreq
        neg       ecx                                           ;343.8
        add       ecx, 16                                       ;343.8
        shr       ecx, 2                                        ;343.8
                                ; LOE edx ecx edi
.B11.177:                       ; Preds .B11.176 .B11.174       ; Infreq
        lea       eax, DWORD PTR [4+ecx]                        ;343.8
        cmp       edx, eax                                      ;343.8
        jl        .B11.189      ; Prob 10%                      ;343.8
                                ; LOE edx ecx edi
.B11.178:                       ; Preds .B11.177                ; Infreq
        mov       esi, edx                                      ;343.8
        sub       esi, ecx                                      ;343.8
        and       esi, 3                                        ;343.8
        neg       esi                                           ;343.8
        add       esi, edx                                      ;343.8
        test      ecx, ecx                                      ;343.8
        jbe       .B11.182      ; Prob 10%                      ;343.8
                                ; LOE edx ecx esi edi
.B11.179:                       ; Preds .B11.178                ; Infreq
        mov       edi, DWORD PTR [-504+ebp]                     ;
        xor       eax, eax                                      ;
                                ; LOE eax edx ecx esi edi
.B11.180:                       ; Preds .B11.180 .B11.179       ; Infreq
        mov       DWORD PTR [edi+eax*4], 0                      ;343.8
        inc       eax                                           ;343.8
        cmp       eax, ecx                                      ;343.8
        jb        .B11.180      ; Prob 82%                      ;343.8
                                ; LOE eax edx ecx esi edi
.B11.181:                       ; Preds .B11.180                ; Infreq
        mov       edi, 4                                        ;
                                ; LOE edx ecx esi edi
.B11.182:                       ; Preds .B11.178 .B11.181       ; Infreq
        mov       eax, DWORD PTR [-504+ebp]                     ;343.8
        pxor      xmm0, xmm0                                    ;343.8
                                ; LOE eax edx ecx esi edi xmm0
.B11.183:                       ; Preds .B11.183 .B11.182       ; Infreq
        movdqa    XMMWORD PTR [eax+ecx*4], xmm0                 ;343.8
        add       ecx, 4                                        ;343.8
        cmp       ecx, esi                                      ;343.8
        jb        .B11.183      ; Prob 82%                      ;343.8
                                ; LOE eax edx ecx esi edi xmm0
.B11.185:                       ; Preds .B11.183 .B11.189       ; Infreq
        cmp       esi, edx                                      ;343.8
        jae       .B11.10       ; Prob 10%                      ;343.8
                                ; LOE edx esi edi
.B11.186:                       ; Preds .B11.185                ; Infreq
        mov       eax, DWORD PTR [-504+ebp]                     ;
                                ; LOE eax edx esi edi
.B11.187:                       ; Preds .B11.187 .B11.186       ; Infreq
        mov       DWORD PTR [eax+esi*4], 0                      ;343.8
        inc       esi                                           ;343.8
        cmp       esi, edx                                      ;343.8
        jb        .B11.187      ; Prob 82%                      ;343.8
        jmp       .B11.10       ; Prob 100%                     ;343.8
                                ; LOE eax edx esi edi
.B11.189:                       ; Preds .B11.173 .B11.177 .B11.175 ; Infreq
        xor       esi, esi                                      ;343.8
        jmp       .B11.185      ; Prob 100%                     ;343.8
                                ; LOE edx esi edi
.B11.192:                       ; Preds .B11.76                 ; Infreq
        cmp       DWORD PTR [-136+ebp], 4                       ;363.8
        jl        .B11.208      ; Prob 10%                      ;363.8
                                ; LOE esi
.B11.193:                       ; Preds .B11.192                ; Infreq
        mov       edx, DWORD PTR [-160+ebp]                     ;363.8
        mov       ecx, esi                                      ;363.8
        imul      edx, DWORD PTR [-152+ebp]                     ;363.8
        imul      ecx, DWORD PTR [-140+ebp]                     ;363.8
        mov       eax, DWORD PTR [-128+ebp]                     ;363.8
        mov       DWORD PTR [-132+ebp], ecx                     ;363.8
        add       eax, edx                                      ;363.8
        add       eax, ecx                                      ;363.8
        and       eax, 15                                       ;363.8
        je        .B11.196      ; Prob 50%                      ;363.8
                                ; LOE eax edx esi
.B11.194:                       ; Preds .B11.193                ; Infreq
        test      al, 3                                         ;363.8
        jne       .B11.208      ; Prob 10%                      ;363.8
                                ; LOE eax edx esi
.B11.195:                       ; Preds .B11.194                ; Infreq
        neg       eax                                           ;363.8
        add       eax, 16                                       ;363.8
        shr       eax, 2                                        ;363.8
                                ; LOE eax edx esi
.B11.196:                       ; Preds .B11.195 .B11.193       ; Infreq
        lea       ecx, DWORD PTR [4+eax]                        ;363.8
        cmp       ecx, DWORD PTR [-136+ebp]                     ;363.8
        jg        .B11.208      ; Prob 10%                      ;363.8
                                ; LOE eax edx esi
.B11.197:                       ; Preds .B11.196                ; Infreq
        mov       edi, DWORD PTR [-136+ebp]                     ;363.8
        mov       ecx, edi                                      ;363.8
        sub       ecx, eax                                      ;363.8
        and       ecx, 3                                        ;363.8
        add       edx, DWORD PTR [-128+ebp]                     ;
        neg       ecx                                           ;363.8
        add       edx, DWORD PTR [-132+ebp]                     ;
        add       ecx, edi                                      ;363.8
        test      eax, eax                                      ;363.8
        jbe       .B11.201      ; Prob 10%                      ;363.8
                                ; LOE eax edx ecx esi
.B11.198:                       ; Preds .B11.197                ; Infreq
        xor       edi, edi                                      ;
                                ; LOE eax edx ecx esi edi
.B11.199:                       ; Preds .B11.199 .B11.198       ; Infreq
        mov       DWORD PTR [edx+edi*4], 0                      ;363.8
        inc       edi                                           ;363.8
        cmp       edi, eax                                      ;363.8
        jb        .B11.199      ; Prob 82%                      ;363.8
                                ; LOE eax edx ecx esi edi
.B11.201:                       ; Preds .B11.199 .B11.197       ; Infreq
        pxor      xmm0, xmm0                                    ;
                                ; LOE eax edx ecx esi xmm0
.B11.202:                       ; Preds .B11.202 .B11.201       ; Infreq
        movdqa    XMMWORD PTR [edx+eax*4], xmm0                 ;363.8
        add       eax, 4                                        ;363.8
        cmp       eax, ecx                                      ;363.8
        jb        .B11.202      ; Prob 82%                      ;363.8
                                ; LOE eax edx ecx esi xmm0
.B11.204:                       ; Preds .B11.202 .B11.208       ; Infreq
        cmp       ecx, DWORD PTR [-136+ebp]                     ;363.8
        jae       .B11.78       ; Prob 10%                      ;363.8
                                ; LOE ecx esi
.B11.205:                       ; Preds .B11.204                ; Infreq
        mov       eax, DWORD PTR [-160+ebp]                     ;
        mov       edx, esi                                      ;
        imul      eax, DWORD PTR [-152+ebp]                     ;
        imul      edx, DWORD PTR [-140+ebp]                     ;
        add       eax, DWORD PTR [-128+ebp]                     ;
        add       eax, edx                                      ;
        mov       edx, DWORD PTR [-136+ebp]                     ;
                                ; LOE eax edx ecx esi
.B11.206:                       ; Preds .B11.206 .B11.205       ; Infreq
        mov       DWORD PTR [eax+ecx*4], 0                      ;363.8
        inc       ecx                                           ;363.8
        cmp       ecx, edx                                      ;363.8
        jb        .B11.206      ; Prob 82%                      ;363.8
        jmp       .B11.78       ; Prob 100%                     ;363.8
                                ; LOE eax edx ecx esi
.B11.208:                       ; Preds .B11.192 .B11.196 .B11.194 ; Infreq
        xor       ecx, ecx                                      ;363.8
        jmp       .B11.204      ; Prob 100%                     ;363.8
        ALIGN     16
                                ; LOE ecx esi
; mark_end;
_DYNUST_FUEL_MODULE_mp_WRITEOUTFUELCONSUM_CITY ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
	DD 5 DUP (0H)	; pad
DYNUST_FUEL_MODULE_mp_WRITEOUTFUELCONSUM_CITY$format_pack.0.11	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	14
	DB	0
	DB	61
	DB	61
	DB	61
	DB	32
	DB	70
	DB	114
	DB	111
	DB	109
	DB	32
	DB	90
	DB	111
	DB	110
	DB	101
	DB	32
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	4
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	13
	DB	0
	DB	32
	DB	116
	DB	111
	DB	32
	DB	122
	DB	111
	DB	110
	DB	101
	DB	32
	DB	61
	DB	61
	DB	61
	DB	32
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	4
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	11
	DB	0
	DB	84
	DB	105
	DB	109
	DB	101
	DB	40
	DB	109
	DB	105
	DB	110
	DB	41
	DB	58
	DB	32
	DB	0
	DB	33
	DB	0
	DB	0
	DB	1
	DB	1
	DB	0
	DB	0
	DB	0
	DB	6
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	13
	DB	0
	DB	32
	DB	65
	DB	117
	DB	116
	DB	111
	DB	32
	DB	84
	DB	111
	DB	116
	DB	97
	DB	108
	DB	58
	DB	32
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	1
	DB	1
	DB	0
	DB	0
	DB	0
	DB	8
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	9
	DB	0
	DB	32
	DB	65
	DB	117
	DB	116
	DB	111
	DB	32
	DB	35
	DB	58
	DB	32
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	14
	DB	0
	DB	32
	DB	84
	DB	114
	DB	117
	DB	99
	DB	107
	DB	32
	DB	84
	DB	111
	DB	116
	DB	97
	DB	108
	DB	58
	DB	32
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	1
	DB	1
	DB	0
	DB	0
	DB	0
	DB	8
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	10
	DB	0
	DB	32
	DB	84
	DB	114
	DB	117
	DB	99
	DB	107
	DB	32
	DB	35
	DB	58
	DB	32
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
_DATA	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_130.0.11	DB	56
	DB	4
	DB	13
	DB	0
	DB	16
	DB	3
	DB	27
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_131.0.11	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	26
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_132.0.11	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	26
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_133.0.11	DB	9
	DB	3
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_134.0.11	DB	9
	DB	3
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_135.0.11	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_136.0.11	DB	9
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_137.0.11	DB	9
	DB	5
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_138.0.11	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_139.0.11	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_140.0.11	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_141.0.11	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_142.0.11	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_143.0.11	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_144.0.11	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_145.0.11	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_146.0.11	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_147.0.11	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_FUEL_MODULE_mp_WRITEOUTFUELCONSUM_CITY
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
	DD 1 DUP (0H)	; pad
	PUBLIC _DYNUST_FUEL_MODULE_mp_COORD
_DYNUST_FUEL_MODULE_mp_COORD	DD	0
	DD	0
	DD	0
	DD	128
	DD	1
	DD	0
	DD	0
	DD	0
	DD	0
	DD 1 DUP (0H)	; pad
	PUBLIC _DYNUST_FUEL_MODULE_mp_GASLOC
_DYNUST_FUEL_MODULE_mp_GASLOC	DD	0
	DD	0
	DD	0
	DD	128
	DD	1
	DD	0
	DD	0
	DD	0
	DD	0
	DD 1 DUP (0H)	; pad
	PUBLIC _DYNUST_FUEL_MODULE_mp_VEHFUEL
_DYNUST_FUEL_MODULE_mp_VEHFUEL	DD	0
	DD	0
	DD	0
	DD	128
	DD	1
	DD	0
	DD	0
	DD	0
	DD	0
	DD 1 DUP (0H)	; pad
	PUBLIC _DYNUST_FUEL_MODULE_mp_FUELSET
_DYNUST_FUEL_MODULE_mp_FUELSET	DD	0
	DD	0
	DD	0
	DD	128
	DD	1
	DD	0
	DD	0
	DD	0
	DD	0
	DD 7 DUP (0H)	; pad
_DYNUST_FUEL_MODULE_mp_READNALLOCATE_FUEL$BLK..T1219_	DD 7 DUP (0H)	; pad
	DD	0
	DD	0
	DD	0
	DD	128
	DD	1
	DD	0
	DD	0
	DD	0
	DD	0
	DD 9 DUP (0H)	; pad
	DD 1 DUP (0H)	; pad
.T1401_.0.5	DD	0
	DD	0
	DD	0
	DD	128
	DD	1
	DD	0
	DD	0
	DD	0
	DD	0
_DATA	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_61	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	33
	DB	32
	DB	70
	DB	117
	DB	101
	DB	108
	DB	32
	DB	99
	DB	111
	DB	110
	DB	115
	DB	117
	DB	109
	DB	112
	DB	116
	DB	105
	DB	111
	DB	110
	DB	32
	DB	99
	DB	97
	DB	108
	DB	99
	DB	117
	DB	97
	DB	108
	DB	116
	DB	105
	DB	111
	DB	110
	DB	32
	DB	105
	DB	115
	DB	32
	DB	98
	DB	101
	DB	105
	DB	110
	DB	103
	DB	32
	DB	115
	DB	112
	DB	101
	DB	99
	DB	105
	DB	102
	DB	105
	DB	101
	DB	100
	DB	32
	DB	105
	DB	110
	DB	32
	DB	80
	DB	65
	DB	82
	DB	65
	DB	77
	DB	69
	DB	84
	DB	69
	DB	82
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
__STRLITPACK_67	DB	102
	DB	117
	DB	101
	DB	108
	DB	95
	DB	112
	DB	97
	DB	114
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_59	DB	66
	DB	117
	DB	116
	DB	32
	DB	110
	DB	111
	DB	32
	DB	102
	DB	117
	DB	101
	DB	108
	DB	95
	DB	112
	DB	97
	DB	114
	DB	46
	DB	100
	DB	97
	DB	116
	DB	32
	DB	105
	DB	110
	DB	112
	DB	117
	DB	116
	DB	32
	DB	102
	DB	105
	DB	108
	DB	101
	DB	32
	DB	105
	DB	115
	DB	32
	DB	97
	DB	118
	DB	97
	DB	105
	DB	108
	DB	97
	DB	98
	DB	108
	DB	101
	DB	0
__STRLITPACK_66	DB	102
	DB	117
	DB	101
	DB	108
	DB	95
	DB	112
	DB	97
	DB	114
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_65	DB	117
	DB	110
	DB	107
	DB	110
	DB	111
	DB	119
	DB	110
	DB	0
__STRLITPACK_64	DB	102
	DB	117
	DB	101
	DB	108
	DB	95
	DB	111
	DB	117
	DB	116
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_63	DB	117
	DB	110
	DB	107
	DB	110
	DB	111
	DB	119
	DB	110
	DB	0
__STRLITPACK_58	DB	97
	DB	108
	DB	108
	DB	95
	DB	102
	DB	117
	DB	101
	DB	108
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_57	DB	79
	DB	76
	DB	68
	DB	0
__STRLITPACK_56	DB	97
	DB	108
	DB	108
	DB	95
	DB	102
	DB	117
	DB	101
	DB	108
	DB	95
	DB	83
	DB	101
	DB	114
	DB	118
	DB	101
	DB	100
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
__STRLITPACK_55	DB	85
	DB	78
	DB	75
	DB	78
	DB	79
	DB	87
	DB	78
	DB	0
__STRLITPACK_54	DB	71
	DB	97
	DB	115
	DB	68
	DB	101
	DB	109
	DB	97
	DB	110
	DB	100
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_53	DB	85
	DB	78
	DB	75
	DB	78
	DB	79
	DB	87
	DB	78
	DB	0
__STRLITPACK_52	DB	71
	DB	97
	DB	115
	DB	68
	DB	101
	DB	109
	DB	97
	DB	110
	DB	100
	DB	95
	DB	115
	DB	101
	DB	114
	DB	118
	DB	101
	DB	100
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_51	DB	85
	DB	78
	DB	75
	DB	78
	DB	79
	DB	87
	DB	78
	DB	0
_2il0floatpacket.1	DD	04cbebc20H
_2il0floatpacket.11	DD	042700000H
_2il0floatpacket.12	DD	040a00000H
_2il0floatpacket.13	DD	03f000000H
_2il0floatpacket.14	DD	0bf000000H
_2il0floatpacket.15	DD	000000000H
__NLITPACK_0.0.5	DD	1000000
__NLITPACK_1.0.5	DD	1
_2il0floatpacket.23	DD	042700000H
_2il0floatpacket.24	DD	040a00000H
_2il0floatpacket.25	DD	03f000000H
_2il0floatpacket.26	DD	0bf000000H
_2il0floatpacket.27	DD	000000000H
__STRLITPACK_43	DB	97
	DB	108
	DB	108
	DB	111
	DB	99
	DB	97
	DB	116
	DB	101
	DB	32
	DB	86
	DB	101
	DB	104
	DB	70
	DB	117
	DB	101
	DB	108
	DB	32
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	45
	DB	32
	DB	105
	DB	110
	DB	115
	DB	117
	DB	102
	DB	102
	DB	105
	DB	99
	DB	105
	DB	101
	DB	110
	DB	116
	DB	32
	DB	109
	DB	101
	DB	109
	DB	111
	DB	114
	DB	121
	DB	0
	DB 3 DUP ( 0H)	; pad
_2il0floatpacket.40	DD	080000000H
_2il0floatpacket.41	DD	04b000000H
_2il0floatpacket.42	DD	03f000000H
_2il0floatpacket.43	DD	0bf000000H
__STRLITPACK_28	DB	105
	DB	110
	DB	112
	DB	117
	DB	116
	DB	95
	DB	80
	DB	114
	DB	111
	DB	99
	DB	101
	DB	115
	DB	115
	DB	84
	DB	114
	DB	97
	DB	118
	DB	101
	DB	108
	DB	84
	DB	105
	DB	109
	DB	101
	DB	115
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_27	DB	105
	DB	110
	DB	112
	DB	117
	DB	116
	DB	95
	DB	80
	DB	114
	DB	111
	DB	99
	DB	101
	DB	115
	DB	115
	DB	84
	DB	114
	DB	97
	DB	118
	DB	101
	DB	108
	DB	84
	DB	105
	DB	109
	DB	101
	DB	115
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_26	DB	117
	DB	110
	DB	107
	DB	110
	DB	111
	DB	119
	DB	110
	DB	0
__STRLITPACK_25	DB	111
	DB	117
	DB	116
	DB	112
	DB	117
	DB	116
	DB	95
	DB	80
	DB	114
	DB	111
	DB	99
	DB	101
	DB	115
	DB	115
	DB	84
	DB	114
	DB	97
	DB	118
	DB	101
	DB	108
	DB	84
	DB	105
	DB	109
	DB	101
	DB	115
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_24	DB	117
	DB	110
	DB	107
	DB	110
	DB	111
	DB	119
	DB	110
	DB	0
_2il0floatpacket.74	DD	080000000H
_2il0floatpacket.75	DD	04b000000H
_2il0floatpacket.76	DD	03f000000H
_2il0floatpacket.77	DD	0bf000000H
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
EXTRN	_DYNUST_MAIN_MODULE_mp_NZONES:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_SIMPERIOD:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ISTRM:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NOOFVEH:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM:BYTE
EXTRN	_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND:BYTE
EXTRN	_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH:BYTE
	COMM _DYNUST_FUEL_MODULE_mp_NUMGASST:BYTE:4
	COMM _DYNUST_FUEL_MODULE_mp_NUMFUEL:BYTE:4
_DATA	ENDS
EXTRN	_for_rewind:PROC
EXTRN	_for_deallocate:PROC
EXTRN	_for_dealloc_allocatable:PROC
EXTRN	_RNNOA:PROC
EXTRN	_SSCAL:PROC
EXTRN	_SADD:PROC
EXTRN	_for_write_seq_fmt_xmit:PROC
EXTRN	_for_write_seq_fmt:PROC
EXTRN	_for_eof:PROC
EXTRN	_for_close:PROC
EXTRN	_for_write_seq_lis_xmit:PROC
EXTRN	_for_write_seq_lis:PROC
EXTRN	_for_allocate:PROC
EXTRN	_for_read_seq_lis_xmit:PROC
EXTRN	_for_alloc_allocatable:PROC
EXTRN	_for_check_mult_overflow:PROC
EXTRN	_for_read_seq_lis:PROC
EXTRN	_for_open:PROC
EXTRN	_for_inquire:PROC
EXTRN	_RNSET:PROC
EXTRN	__intel_fast_memcpy:PROC
EXTRN	__intel_fast_memset:PROC
EXTRN	__alloca_probe:PROC
EXTRN	__fltused:BYTE
	INCLUDELIB <ifconsol>
	INCLUDELIB <libifcoremt>
	INCLUDELIB <libifport>
	INCLUDELIB <libmmt>
	INCLUDELIB <LIBCMT>
	INCLUDELIB <libirc>
	INCLUDELIB <svml_dispmt>
	INCLUDELIB <OLDNAMES>
	END
