; mark_description "Intel(R) Visual Fortran Compiler XE for applications running on IA-32, Version 13.1.1.171 Build 20130313";
; mark_description "-S";
	.686P
 	.387
	OPTION DOTNAME
	ASSUME	CS:FLAT,DS:FLAT,SS:FLAT
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
TXTST0:
; -- Begin  _CAL_ACCUMU_STAT
; mark_begin;
IF @Version GE 800
  .MMX
ELSEIF @Version GE 612
  .MMX
  MMWORD TEXTEQU <QWORD>
ENDIF
IF @Version GE 800
  .XMM
ELSEIF @Version GE 614
  .XMM
  XMMWORD TEXTEQU <OWORD>
ENDIF
       ALIGN     16
	PUBLIC _CAL_ACCUMU_STAT
_CAL_ACCUMU_STAT	PROC NEAR 
; parameter 1: 8 + ebp
; parameter 2: 12 + ebp
; parameter 3: 16 + ebp
.B1.1:                          ; Preds .B1.0
        push      ebp                                           ;1.12
        mov       ebp, esp                                      ;1.12
        and       esp, -16                                      ;1.12
        push      esi                                           ;1.12
        push      edi                                           ;1.12
        push      ebx                                           ;1.12
        sub       esp, 100                                      ;1.12
        mov       ebx, DWORD PTR [12+ebp]                       ;1.12
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;57.4
        shl       edx, 5                                        ;57.38
        mov       esi, DWORD PTR [ebx]                          ;57.7
        mov       ecx, esi                                      ;57.38
        mov       eax, edx                                      ;57.38
        shl       ecx, 5                                        ;57.38
        neg       eax                                           ;57.38
        add       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;57.38
        cmp       BYTE PTR [5+eax+ecx], 7                       ;57.38
        je        .B1.3         ; Prob 16%                      ;57.38
                                ; LOE edx ecx ebx esi
.B1.2:                          ; Preds .B1.1
        inc       DWORD PTR [_DYNUST_MAIN_MODULE_mp_KTOTAL_OUT] ;57.44
                                ; LOE edx ecx ebx esi
.B1.3:                          ; Preds .B1.1 .B1.2
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;59.4
        neg       edx                                           ;59.4
        shl       eax, 8                                        ;59.4
        mov       edi, DWORD PTR [16+ebp]                       ;59.4
        neg       eax                                           ;59.4
        mov       DWORD PTR [8+esp], esi                        ;
        shl       esi, 8                                        ;59.4
        add       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;59.4
        movss     xmm1, DWORD PTR [edi]                         ;59.4
        movss     xmm0, DWORD PTR [12+edx+ecx]                  ;61.38
        mov       edi, DWORD PTR [12+edx+ecx]                   ;61.38
        mov       DWORD PTR [esp], eax                          ;59.4
        addss     xmm1, DWORD PTR [152+esi+eax]                 ;59.4
        movss     DWORD PTR [4+esp], xmm1                       ;59.4
        movss     DWORD PTR [16+edx+ecx], xmm1                  ;59.4
        mov       esi, DWORD PTR [8+esp]                        ;61.36
        comiss    xmm0, xmm1                                    ;61.36
        jbe       .B1.11        ; Prob 50%                      ;61.36
                                ; LOE ebx esi edi
.B1.4:                          ; Preds .B1.3
        mov       DWORD PTR [32+esp], 0                         ;62.6
        lea       edx, DWORD PTR [32+esp]                       ;62.6
        mov       DWORD PTR [24+esp], 34                        ;62.6
        lea       eax, DWORD PTR [24+esp]                       ;62.6
        mov       DWORD PTR [28+esp], OFFSET FLAT: __STRLITPACK_4 ;62.6
        push      32                                            ;62.6
        push      eax                                           ;62.6
        push      OFFSET FLAT: __STRLITPACK_6.0.1               ;62.6
        push      -2088435968                                   ;62.6
        push      911                                           ;62.6
        push      edx                                           ;62.6
        call      _for_write_seq_lis                            ;62.6
                                ; LOE ebx esi edi
.B1.5:                          ; Preds .B1.4
        mov       DWORD PTR [104+esp], esi                      ;62.6
        lea       eax, DWORD PTR [104+esp]                      ;62.6
        push      eax                                           ;62.6
        push      OFFSET FLAT: __STRLITPACK_7.0.1               ;62.6
        lea       edx, DWORD PTR [64+esp]                       ;62.6
        push      edx                                           ;62.6
        call      _for_write_seq_lis_xmit                       ;62.6
                                ; LOE ebx edi
.B1.6:                          ; Preds .B1.5
        mov       DWORD PTR [68+esp], 0                         ;63.6
        lea       eax, DWORD PTR [100+esp]                      ;63.6
        mov       DWORD PTR [100+esp], 14                       ;63.6
        mov       DWORD PTR [104+esp], OFFSET FLAT: __STRLITPACK_1 ;63.6
        push      32                                            ;63.6
        push      eax                                           ;63.6
        push      OFFSET FLAT: __STRLITPACK_8.0.1               ;63.6
        push      -2088435968                                   ;63.6
        push      911                                           ;63.6
        lea       edx, DWORD PTR [88+esp]                       ;63.6
        push      edx                                           ;63.6
        call      _for_write_seq_lis                            ;63.6
                                ; LOE ebx edi
.B1.7:                          ; Preds .B1.6
        movss     xmm0, DWORD PTR [64+esp]                      ;63.6
        lea       eax, DWORD PTR [148+esp]                      ;63.6
        movss     DWORD PTR [148+esp], xmm0                     ;63.6
        push      eax                                           ;63.6
        push      OFFSET FLAT: __STRLITPACK_9.0.1               ;63.6
        lea       edx, DWORD PTR [100+esp]                      ;63.6
        push      edx                                           ;63.6
        call      _for_write_seq_lis_xmit                       ;63.6
                                ; LOE ebx edi
.B1.8:                          ; Preds .B1.7
        mov       DWORD PTR [144+esp], 13                       ;63.6
        lea       eax, DWORD PTR [144+esp]                      ;63.6
        mov       DWORD PTR [148+esp], OFFSET FLAT: __STRLITPACK_0 ;63.6
        push      eax                                           ;63.6
        push      OFFSET FLAT: __STRLITPACK_10.0.1              ;63.6
        lea       edx, DWORD PTR [112+esp]                      ;63.6
        push      edx                                           ;63.6
        call      _for_write_seq_lis_xmit                       ;63.6
                                ; LOE ebx edi
.B1.9:                          ; Preds .B1.8
        mov       DWORD PTR [180+esp], edi                      ;63.6
        lea       eax, DWORD PTR [180+esp]                      ;63.6
        push      eax                                           ;63.6
        push      OFFSET FLAT: __STRLITPACK_11.0.1              ;63.6
        lea       edx, DWORD PTR [124+esp]                      ;63.6
        push      edx                                           ;63.6
        call      _for_write_seq_lis_xmit                       ;63.6
                                ; LOE ebx
.B1.10:                         ; Preds .B1.9
        push      32                                            ;64.6
        xor       eax, eax                                      ;64.6
        push      eax                                           ;64.6
        push      eax                                           ;64.6
        push      -2088435968                                   ;64.6
        push      eax                                           ;64.6
        push      OFFSET FLAT: __STRLITPACK_12                  ;64.6
        call      _for_stop_core                                ;64.6
                                ; LOE ebx
.B1.99:                         ; Preds .B1.10
        mov       esi, DWORD PTR [ebx]                          ;64.6
        add       esp, 120                                      ;64.6
                                ; LOE ebx esi
.B1.11:                         ; Preds .B1.99 .B1.3
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE+32] ;67.9
        neg       eax                                           ;67.9
        add       eax, esi                                      ;67.9
        shl       esi, 8                                        ;67.9
        mov       ecx, DWORD PTR [esp]                          ;67.9
        shl       eax, 6                                        ;67.9
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE] ;67.9
        lea       esi, DWORD PTR [88+ecx+esi]                   ;67.9
        push      esi                                           ;67.9
        push      OFFSET FLAT: __NLITPACK_0.0.1                 ;67.9
        lea       edi, DWORD PTR [12+eax+edx]                   ;67.9
        push      edi                                           ;67.9
        push      ebx                                           ;67.9
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_INSERT  ;67.9
                                ; LOE ebx
.B1.12:                         ; Preds .B1.11
        mov       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;68.9
        neg       ecx                                           ;68.9
        add       ecx, DWORD PTR [ebx]                          ;68.9
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE+32] ;68.9
        shl       ecx, 8                                        ;68.9
        neg       eax                                           ;68.9
        mov       esi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;68.9
        add       eax, DWORD PTR [ebx]                          ;68.9
        shl       eax, 6                                        ;68.9
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE] ;68.9
        lea       edi, DWORD PTR [84+ecx+esi]                   ;68.9
        push      edi                                           ;68.9
        push      OFFSET FLAT: __NLITPACK_1.0.1                 ;68.9
        lea       eax, DWORD PTR [12+eax+edx]                   ;68.9
        push      eax                                           ;68.9
        push      ebx                                           ;68.9
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_INSERT  ;68.9
                                ; LOE ebx
.B1.100:                        ; Preds .B1.12
        add       esp, 32                                       ;68.9
                                ; LOE ebx
.B1.13:                         ; Preds .B1.100
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ITEMAX] ;71.4
        test      eax, eax                                      ;71.14
        je        .B1.17        ; Prob 50%                      ;71.14
                                ; LOE eax ebx
.B1.14:                         ; Preds .B1.13
        jle       .B1.16        ; Prob 16%                      ;71.30
                                ; LOE eax ebx
.B1.15:                         ; Preds .B1.14
        cmp       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ITERATION] ;71.48
        je        .B1.17        ; Prob 50%                      ;71.48
                                ; LOE ebx
.B1.16:                         ; Preds .B1.15 .B1.14
        test      BYTE PTR [_DYNUST_MAIN_MODULE_mp_REACH_CONVERG], 1 ;71.62
        je        .B1.18        ; Prob 85%                      ;71.62
                                ; LOE ebx
.B1.17:                         ; Preds .B1.16 .B1.15 .B1.13
        push      DWORD PTR [16+ebp]                            ;72.11
        push      OFFSET FLAT: __NLITPACK_3.0.1                 ;72.11
        push      OFFSET FLAT: __NLITPACK_2.0.1                 ;72.11
        push      ebx                                           ;72.11
        call      _WRITE_PRINT_TRAJECTORY                       ;72.11
                                ; LOE ebx
.B1.101:                        ; Preds .B1.17
        add       esp, 16                                       ;72.11
                                ; LOE ebx
.B1.18:                         ; Preds .B1.101 .B1.16
        push      DWORD PTR [16+ebp]                            ;74.9
        push      ebx                                           ;74.9
        call      _PRINTALT                                     ;74.9
                                ; LOE ebx
.B1.102:                        ; Preds .B1.18
        add       esp, 8                                        ;74.9
                                ; LOE ebx
.B1.19:                         ; Preds .B1.102
        mov       esi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;76.4
        neg       esi                                           ;76.30
        mov       eax, DWORD PTR [ebx]                          ;76.7
        add       esi, eax                                      ;76.30
        shl       esi, 8                                        ;76.30
        mov       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;76.4
        test      BYTE PTR [36+ecx+esi], 1                      ;76.7
        je        .B1.23        ; Prob 60%                      ;76.7
                                ; LOE eax ecx ebx esi
.B1.20:                         ; Preds .B1.19
        cmp       BYTE PTR [236+ecx+esi], 1                     ;76.55
        je        .B1.32        ; Prob 16%                      ;76.55
                                ; LOE eax ecx ebx esi
.B1.21:                         ; Preds .B1.20
        cmp       BYTE PTR [236+ecx+esi], 2                     ;82.59
        jne       .B1.26        ; Prob 84%                      ;82.59
                                ; LOE eax ecx ebx esi
.B1.22:                         ; Preds .B1.21
        movss     xmm0, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TIME_HOV_HOT] ;84.7
        inc       DWORD PTR [_DYNUST_MAIN_MODULE_mp_IACTUAL_HOV_HOT] ;83.7
        addss     xmm0, DWORD PTR [88+ecx+esi]                  ;84.7
        movss     DWORD PTR [_DYNUST_MAIN_MODULE_mp_TIME_HOV_HOT], xmm0 ;84.7
        jmp       .B1.26        ; Prob 100%                     ;84.7
                                ; LOE eax ecx ebx esi
.B1.23:                         ; Preds .B1.19
        cmp       BYTE PTR [236+ecx+esi], 1                     ;79.65
        je        .B1.96        ; Prob 16%                      ;79.65
                                ; LOE eax ecx ebx esi
.B1.24:                         ; Preds .B1.23
        cmp       BYTE PTR [236+ecx+esi], 2                     ;85.64
        jne       .B1.26        ; Prob 84%                      ;85.64
                                ; LOE eax ecx ebx esi
.B1.25:                         ; Preds .B1.24
        movss     xmm0, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TIME_HOV_OHOT] ;87.7
        inc       DWORD PTR [_DYNUST_MAIN_MODULE_mp_IACTUAL_HOV_OHOT] ;86.7
        addss     xmm0, DWORD PTR [88+ecx+esi]                  ;87.7
        movss     DWORD PTR [_DYNUST_MAIN_MODULE_mp_TIME_HOV_OHOT], xmm0 ;87.7
                                ; LOE eax ecx ebx esi
.B1.26:                         ; Preds .B1.21 .B1.24 .B1.25 .B1.22 .B1.96
                                ;       .B1.32
        movsx     edx, BYTE PTR [237+ecx+esi]                   ;92.7
        cmp       edx, 2                                        ;92.28
        je        .B1.33        ; Prob 16%                      ;92.28
                                ; LOE eax edx ecx ebx esi
.B1.27:                         ; Preds .B1.26
        test      edx, edx                                      ;178.30
        jne       .B1.29        ; Prob 50%                      ;178.30
                                ; LOE ebx
.B1.28:                         ; Preds .B1.27
        inc       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ITAG0]      ;179.8
                                ; LOE ebx
.B1.29:                         ; Preds .B1.89 .B1.53 .B1.50 .B1.27 .B1.90
                                ;       .B1.58 .B1.57 .B1.54 .B1.28
        mov       DWORD PTR [esp], 0                            ;182.5
        lea       esi, DWORD PTR [esp]                          ;183.10
        push      DWORD PTR [16+ebp]                            ;183.10
        push      esi                                           ;183.10
        push      ebx                                           ;183.10
        call      _WRITE_SUMMARY                                ;183.10
                                ; LOE ebx esi
.B1.30:                         ; Preds .B1.29
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;184.10
        neg       eax                                           ;184.10
        add       eax, DWORD PTR [ebx]                          ;184.10
        shl       eax, 5                                        ;184.10
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;184.10
        lea       ecx, DWORD PTR [12+eax+edx]                   ;184.10
        push      ecx                                           ;184.10
        push      esi                                           ;184.10
        push      ebx                                           ;184.10
        call      _WRITE_SUMMARY_TYPEBASED                      ;184.10
                                ; LOE
.B1.31:                         ; Preds .B1.30
        add       esp, 124                                      ;185.1
        pop       ebx                                           ;185.1
        pop       edi                                           ;185.1
        pop       esi                                           ;185.1
        mov       esp, ebp                                      ;185.1
        pop       ebp                                           ;185.1
        ret                                                     ;185.1
                                ; LOE
.B1.32:                         ; Preds .B1.20                  ; Infreq
        movss     xmm0, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TIME_LOV_HOT] ;78.7
        inc       DWORD PTR [_DYNUST_MAIN_MODULE_mp_IACTUAL_LOV_HOT] ;77.7
        addss     xmm0, DWORD PTR [88+ecx+esi]                  ;78.7
        movss     DWORD PTR [_DYNUST_MAIN_MODULE_mp_TIME_LOV_HOT], xmm0 ;78.7
        jmp       .B1.26        ; Prob 100%                     ;78.7
                                ; LOE eax ecx ebx esi
.B1.33:                         ; Preds .B1.26                  ; Infreq
        movsx     edx, BYTE PTR [160+ecx+esi]                   ;93.9
        cmp       edx, 1                                        ;93.36
        mov       DWORD PTR [16+esp], edx                       ;93.9
        jle       .B1.48        ; Prob 16%                      ;93.36
                                ; LOE eax edx ecx ebx esi dl dh
.B1.34:                         ; Preds .B1.33                  ; Infreq
        dec       edx                                           ;94.6
        mov       DWORD PTR [4+esp], edx                        ;94.6
        jle       .B1.48        ; Prob 50%                      ;94.6
                                ; LOE eax edx ecx ebx esi dl dh
.B1.35:                         ; Preds .B1.34                  ; Infreq
        cmp       DWORD PTR [4+esp], 16                         ;94.6
        jl        .B1.56        ; Prob 10%                      ;94.6
                                ; LOE eax edx ecx ebx esi dl dh
.B1.36:                         ; Preds .B1.35                  ; Infreq
        xor       edi, edi                                      ;
        and       edx, -16                                      ;94.6
        pxor      xmm0, xmm0                                    ;
        mov       DWORD PTR [8+esp], edx                        ;94.6
        movaps    xmm5, xmm0                                    ;
        mov       edx, DWORD PTR [232+ecx+esi]                  ;95.23
        movaps    xmm3, xmm0                                    ;
        add       edx, edx                                      ;
        movaps    xmm4, xmm0                                    ;
        movaps    xmm1, XMMWORD PTR [_2il0floatpacket.3]        ;95.55
        neg       edx                                           ;
        add       edx, DWORD PTR [200+ecx+esi]                  ;
        rcpps     xmm2, xmm1                                    ;95.55
        mov       ebx, DWORD PTR [8+esp]                        ;95.55
        ALIGN     16
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm2 xmm3 xmm4 xmm5
.B1.37:                         ; Preds .B1.37 .B1.36           ; Infreq
        movaps    xmm7, xmm2                                    ;95.55
        movaps    xmm1, xmm2                                    ;95.55
        mulps     xmm7, XMMWORD PTR [_2il0floatpacket.3]        ;95.55
        addps     xmm1, xmm2                                    ;95.55
        mulps     xmm7, xmm2                                    ;95.55
        movq      xmm6, QWORD PTR [2+edx+edi*2]                 ;95.23
        subps     xmm1, xmm7                                    ;95.55
        punpcklwd xmm6, xmm6                                    ;95.23
        psrad     xmm6, 16                                      ;95.23
        cvtdq2ps  xmm6, xmm6                                    ;95.23
        mulps     xmm6, xmm1                                    ;95.55
        movq      xmm7, QWORD PTR [10+edx+edi*2]                ;95.23
        addps     xmm0, xmm6                                    ;95.11
        punpcklwd xmm7, xmm7                                    ;95.23
        psrad     xmm7, 16                                      ;95.23
        cvtdq2ps  xmm6, xmm7                                    ;95.23
        mulps     xmm6, xmm1                                    ;95.55
        movq      xmm7, QWORD PTR [18+edx+edi*2]                ;95.23
        addps     xmm5, xmm6                                    ;95.11
        punpcklwd xmm7, xmm7                                    ;95.23
        psrad     xmm7, 16                                      ;95.23
        cvtdq2ps  xmm6, xmm7                                    ;95.23
        mulps     xmm6, xmm1                                    ;95.55
        movq      xmm7, QWORD PTR [26+edx+edi*2]                ;95.23
        add       edi, 16                                       ;94.6
        punpcklwd xmm7, xmm7                                    ;95.23
        cmp       edi, ebx                                      ;94.6
        psrad     xmm7, 16                                      ;95.23
        addps     xmm3, xmm6                                    ;95.11
        cvtdq2ps  xmm6, xmm7                                    ;95.23
        mulps     xmm6, xmm1                                    ;95.55
        addps     xmm4, xmm6                                    ;95.11
        jb        .B1.37        ; Prob 81%                      ;94.6
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm2 xmm3 xmm4 xmm5
.B1.38:                         ; Preds .B1.37                  ; Infreq
        mov       ebx, DWORD PTR [12+ebp]                       ;
        addps     xmm0, xmm5                                    ;91.4
        addps     xmm3, xmm4                                    ;91.4
        addps     xmm0, xmm3                                    ;91.4
        movaps    xmm1, xmm0                                    ;91.4
        movhlps   xmm1, xmm0                                    ;91.4
        addps     xmm0, xmm1                                    ;91.4
        movaps    xmm2, xmm0                                    ;91.4
        shufps    xmm2, xmm0, 245                               ;91.4
        addss     xmm0, xmm2                                    ;91.4
                                ; LOE eax ecx ebx esi xmm0
.B1.39:                         ; Preds .B1.38 .B1.56           ; Infreq
        mov       edx, DWORD PTR [8+esp]                        ;94.6
        lea       edi, DWORD PTR [1+edx]                        ;94.6
        cmp       edi, DWORD PTR [4+esp]                        ;94.6
        ja        .B1.49        ; Prob 50%                      ;94.6
                                ; LOE eax ecx ebx esi xmm0
.B1.40:                         ; Preds .B1.39                  ; Infreq
        mov       edx, DWORD PTR [4+esp]                        ;94.6
        sub       edx, DWORD PTR [8+esp]                        ;94.6
        mov       DWORD PTR [4+esp], edx                        ;94.6
        cmp       edx, 4                                        ;94.6
        jl        .B1.55        ; Prob 10%                      ;94.6
                                ; LOE eax edx ecx ebx esi dl dh xmm0
.B1.41:                         ; Preds .B1.40                  ; Infreq
        and       edx, -4                                       ;94.6
        pxor      xmm1, xmm1                                    ;91.4
        mov       DWORD PTR [12+esp], edx                       ;94.6
        movss     xmm1, xmm0                                    ;91.4
        mov       edx, DWORD PTR [232+ecx+esi]                  ;95.23
        neg       edx                                           ;
        mov       edi, DWORD PTR [200+ecx+esi]                  ;95.23
        add       edx, DWORD PTR [8+esp]                        ;
        mov       DWORD PTR [esp], 0                            ;94.6
        movaps    xmm2, XMMWORD PTR [_2il0floatpacket.3]        ;95.55
        rcpps     xmm0, xmm2                                    ;
        mov       ebx, DWORD PTR [12+esp]                       ;
        lea       edx, DWORD PTR [edi+edx*2]                    ;
        mov       edi, DWORD PTR [esp]                          ;
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1 xmm2
.B1.42:                         ; Preds .B1.42 .B1.41           ; Infreq
        movaps    xmm4, xmm0                                    ;95.55
        movaps    xmm5, xmm0                                    ;95.55
        mulps     xmm4, xmm2                                    ;95.55
        addps     xmm5, xmm0                                    ;95.55
        mulps     xmm4, xmm0                                    ;95.55
        movq      xmm3, QWORD PTR [2+edx+edi*2]                 ;95.23
        add       edi, 4                                        ;94.6
        punpcklwd xmm3, xmm3                                    ;95.23
        cmp       edi, ebx                                      ;94.6
        psrad     xmm3, 16                                      ;95.23
        subps     xmm5, xmm4                                    ;95.55
        cvtdq2ps  xmm6, xmm3                                    ;95.23
        mulps     xmm6, xmm5                                    ;95.55
        addps     xmm1, xmm6                                    ;95.11
        jb        .B1.42        ; Prob 81%                      ;94.6
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1 xmm2
.B1.43:                         ; Preds .B1.42                  ; Infreq
        movaps    xmm0, xmm1                                    ;91.4
        movhlps   xmm0, xmm1                                    ;91.4
        mov       DWORD PTR [12+esp], ebx                       ;
        addps     xmm1, xmm0                                    ;91.4
        movaps    xmm2, xmm1                                    ;91.4
        shufps    xmm2, xmm1, 245                               ;91.4
        mov       ebx, DWORD PTR [12+ebp]                       ;
        addss     xmm1, xmm2                                    ;91.4
        movaps    xmm0, xmm1                                    ;91.4
                                ; LOE eax ecx ebx esi xmm0
.B1.44:                         ; Preds .B1.43 .B1.55           ; Infreq
        mov       edx, DWORD PTR [4+esp]                        ;94.6
        cmp       edx, DWORD PTR [12+esp]                       ;94.6
        jbe       .B1.49        ; Prob 3%                       ;94.6
                                ; LOE eax ecx ebx esi xmm0
.B1.45:                         ; Preds .B1.44                  ; Infreq
        mov       edi, DWORD PTR [232+ecx+esi]                  ;95.23
        neg       edi                                           ;
        mov       edx, DWORD PTR [200+ecx+esi]                  ;95.23
        add       edi, DWORD PTR [8+esp]                        ;
        mov       ebx, DWORD PTR [12+esp]                       ;95.55
        movss     xmm1, DWORD PTR [_2il0floatpacket.4]          ;95.55
        mov       DWORD PTR [esp], eax                          ;95.55
        lea       edx, DWORD PTR [edx+edi*2]                    ;
        mov       edi, DWORD PTR [4+esp]                        ;95.55
                                ; LOE edx ecx ebx esi edi xmm0 xmm1
.B1.46:                         ; Preds .B1.46 .B1.45           ; Infreq
        pxor      xmm2, xmm2                                    ;95.23
        movsx     eax, WORD PTR [2+edx+ebx*2]                   ;95.23
        inc       ebx                                           ;94.6
        cvtsi2ss  xmm2, eax                                     ;95.23
        divss     xmm2, xmm1                                    ;95.55
        cmp       ebx, edi                                      ;94.6
        addss     xmm0, xmm2                                    ;95.11
        jb        .B1.46        ; Prob 81%                      ;94.6
                                ; LOE edx ecx ebx esi edi xmm0 xmm1
.B1.47:                         ; Preds .B1.46                  ; Infreq
        mov       eax, DWORD PTR [esp]                          ;
        mov       ebx, DWORD PTR [12+ebp]                       ;
        jmp       .B1.49        ; Prob 100%                     ;
                                ; LOE eax ecx ebx esi xmm0
.B1.48:                         ; Preds .B1.33 .B1.34           ; Infreq
        pxor      xmm0, xmm0                                    ;
                                ; LOE eax ecx ebx esi xmm0
.B1.49:                         ; Preds .B1.47 .B1.44 .B1.39 .B1.48 ; Infreq
        mov       edi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;100.6
        neg       edi                                           ;100.6
        pxor      xmm2, xmm2                                    ;100.6
        add       edi, eax                                      ;100.6
        movss     xmm4, DWORD PTR [84+ecx+esi]                  ;98.6
        movss     xmm1, DWORD PTR [_DYNUST_MAIN_MODULE_mp_STOPTIME] ;99.6
        shl       edi, 5                                        ;100.6
        addss     xmm1, xmm4                                    ;99.6
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;100.6
        movss     DWORD PTR [_DYNUST_MAIN_MODULE_mp_STOPTIME], xmm1 ;99.6
        movss     xmm1, DWORD PTR [88+ecx+esi]                  ;100.76
        movss     xmm3, DWORD PTR [16+edx+edi]                  ;100.18
        movaps    xmm5, xmm3                                    ;100.75
        subss     xmm3, xmm0                                    ;102.6
        subss     xmm5, xmm1                                    ;100.75
        movss     xmm7, DWORD PTR [12+edx+edi]                  ;100.47
        subss     xmm5, xmm0                                    ;100.99
        subss     xmm3, xmm7                                    ;102.37
        subss     xmm5, xmm7                                    ;100.46
        movss     xmm6, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ENTRY_QUEUE1] ;101.6
        maxss     xmm2, xmm5                                    ;100.6
        movss     xmm0, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRIPTIME1] ;103.6
        addss     xmm6, xmm2                                    ;101.6
        addss     xmm0, xmm3                                    ;103.6
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE+32] ;107.6
        sub       eax, edx                                      ;107.34
        movss     DWORD PTR [_DYNUST_MAIN_MODULE_mp_ENTRY_QUEUE1], xmm6 ;101.6
        movss     DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRIPTIME1], xmm0 ;103.6
        movss     xmm0, DWORD PTR [108+ecx+esi]                 ;104.22
        movss     xmm5, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DTOTAL1] ;104.6
        movss     xmm6, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VTOTHR1] ;105.6
        addss     xmm5, xmm0                                    ;104.6
        addss     xmm6, xmm1                                    ;105.6
        shl       eax, 6                                        ;107.34
        mov       edi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE] ;107.6
        inc       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ITAG2]      ;106.6
        movss     DWORD PTR [_DYNUST_MAIN_MODULE_mp_DTOTAL1], xmm5 ;104.6
        movsx     eax, BYTE PTR [56+edi+eax]                    ;107.9
        cmp       eax, 1                                        ;107.34
        movss     DWORD PTR [_DYNUST_MAIN_MODULE_mp_VTOTHR1], xmm6 ;105.6
        je        .B1.59        ; Prob 16%                      ;107.34
                                ; LOE eax ecx ebx esi xmm0 xmm1 xmm2 xmm3 xmm4
.B1.50:                         ; Preds .B1.49                  ; Infreq
        test      eax, eax                                      ;148.38
        jne       .B1.29        ; Prob 50%                      ;148.38
                                ; LOE ebx xmm0 xmm1 xmm2 xmm3 xmm4
.B1.51:                         ; Preds .B1.50                  ; Infreq
        movss     xmm5, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ENTRY_QUEUE3] ;149.5
        movss     xmm6, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRIPTIME3] ;150.8
        addss     xmm5, xmm2                                    ;149.5
        addss     xmm6, xmm3                                    ;150.8
        movss     DWORD PTR [_DYNUST_MAIN_MODULE_mp_ENTRY_QUEUE3], xmm5 ;149.5
        movss     DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRIPTIME3], xmm6 ;150.8
        movss     xmm7, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VTOTHR3] ;152.8
        movss     xmm5, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DTOTAL3] ;153.8
        addss     xmm7, xmm1                                    ;152.8
        addss     xmm5, xmm0                                    ;153.8
        movss     xmm6, DWORD PTR [_DYNUST_MAIN_MODULE_mp_STOPNOINFO] ;154.8
        inc       DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOINFORMATION] ;151.8
        addss     xmm6, xmm4                                    ;154.8
        movss     DWORD PTR [_DYNUST_MAIN_MODULE_mp_VTOTHR3], xmm7 ;152.8
        movss     DWORD PTR [_DYNUST_MAIN_MODULE_mp_DTOTAL3], xmm5 ;153.8
        movss     DWORD PTR [_DYNUST_MAIN_MODULE_mp_STOPNOINFO], xmm6 ;154.8
        cmp       DWORD PTR [16+esp], 1                         ;155.38
        je        .B1.58        ; Prob 16%                      ;155.38
                                ; LOE ebx xmm0 xmm1 xmm2 xmm3 xmm4
.B1.52:                         ; Preds .B1.51                  ; Infreq
        cmp       DWORD PTR [16+esp], 2                         ;162.42
        je        .B1.57        ; Prob 16%                      ;162.42
                                ; LOE ebx xmm0 xmm1 xmm2 xmm3 xmm4
.B1.53:                         ; Preds .B1.52                  ; Infreq
        cmp       DWORD PTR [16+esp], 3                         ;169.42
        jne       .B1.29        ; Prob 84%                      ;169.42
                                ; LOE ebx xmm0 xmm1 xmm2 xmm3 xmm4
.B1.54:                         ; Preds .B1.53                  ; Infreq
        movss     xmm5, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ENTRY_QUEUE3_3] ;170.11
        inc       DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOINFORMATION_3] ;172.11
        addss     xmm5, xmm2                                    ;170.11
        movss     xmm2, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRIPTIME3_3] ;171.11
        movss     DWORD PTR [_DYNUST_MAIN_MODULE_mp_ENTRY_QUEUE3_3], xmm5 ;170.11
        addss     xmm2, xmm3                                    ;171.11
        movss     xmm3, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VTOTHR3_3] ;173.11
        movss     DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRIPTIME3_3], xmm2 ;171.11
        addss     xmm3, xmm1                                    ;173.11
        movss     xmm1, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DTOTAL3_3] ;174.11
        movss     DWORD PTR [_DYNUST_MAIN_MODULE_mp_VTOTHR3_3], xmm3 ;173.11
        addss     xmm1, xmm0                                    ;174.11
        movss     xmm0, DWORD PTR [_DYNUST_MAIN_MODULE_mp_STOPNOINFO_2] ;175.11
        movss     DWORD PTR [_DYNUST_MAIN_MODULE_mp_DTOTAL3_3], xmm1 ;174.11
        addss     xmm0, xmm4                                    ;175.11
        movss     DWORD PTR [_DYNUST_MAIN_MODULE_mp_STOPNOINFO_2], xmm0 ;175.11
        jmp       .B1.29        ; Prob 100%                     ;175.11
                                ; LOE ebx
.B1.55:                         ; Preds .B1.40                  ; Infreq
        mov       DWORD PTR [12+esp], 0                         ;94.6
        jmp       .B1.44        ; Prob 100%                     ;94.6
                                ; LOE eax ecx ebx esi xmm0
.B1.56:                         ; Preds .B1.35                  ; Infreq
        mov       DWORD PTR [8+esp], 0                          ;
        pxor      xmm0, xmm0                                    ;
        jmp       .B1.39        ; Prob 100%                     ;
                                ; LOE eax ecx ebx esi xmm0
.B1.57:                         ; Preds .B1.52                  ; Infreq
        movss     xmm5, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ENTRY_QUEUE3_2] ;163.11
        inc       DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOINFORMATION_2] ;165.11
        addss     xmm5, xmm2                                    ;163.11
        movss     xmm2, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRIPTIME3_2] ;164.11
        movss     DWORD PTR [_DYNUST_MAIN_MODULE_mp_ENTRY_QUEUE3_2], xmm5 ;163.11
        addss     xmm2, xmm3                                    ;164.11
        movss     xmm3, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VTOTHR3_2] ;166.11
        movss     DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRIPTIME3_2], xmm2 ;164.11
        addss     xmm3, xmm1                                    ;166.11
        movss     xmm1, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DTOTAL3_2] ;167.11
        movss     DWORD PTR [_DYNUST_MAIN_MODULE_mp_VTOTHR3_2], xmm3 ;166.11
        addss     xmm1, xmm0                                    ;167.11
        movss     xmm0, DWORD PTR [_DYNUST_MAIN_MODULE_mp_STOPNOINFO_2] ;168.11
        movss     DWORD PTR [_DYNUST_MAIN_MODULE_mp_DTOTAL3_2], xmm1 ;167.11
        addss     xmm0, xmm4                                    ;168.11
        movss     DWORD PTR [_DYNUST_MAIN_MODULE_mp_STOPNOINFO_2], xmm0 ;168.11
        jmp       .B1.29        ; Prob 100%                     ;168.11
                                ; LOE ebx
.B1.58:                         ; Preds .B1.51                  ; Infreq
        movss     xmm5, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ENTRY_QUEUE3_1] ;156.11
        inc       DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOINFORMATION_1] ;158.11
        addss     xmm5, xmm2                                    ;156.11
        movss     xmm2, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRIPTIME3_1] ;157.11
        movss     DWORD PTR [_DYNUST_MAIN_MODULE_mp_ENTRY_QUEUE3_1], xmm5 ;156.11
        addss     xmm2, xmm3                                    ;157.11
        movss     xmm3, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VTOTHR3_1] ;159.11
        movss     DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRIPTIME3_1], xmm2 ;157.11
        addss     xmm3, xmm1                                    ;159.11
        movss     xmm1, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DTOTAL3_1] ;160.11
        movss     DWORD PTR [_DYNUST_MAIN_MODULE_mp_VTOTHR3_1], xmm3 ;159.11
        addss     xmm1, xmm0                                    ;160.11
        movss     xmm0, DWORD PTR [_DYNUST_MAIN_MODULE_mp_STOPNOINFO_1] ;161.11
        movss     DWORD PTR [_DYNUST_MAIN_MODULE_mp_DTOTAL3_1], xmm1 ;160.11
        addss     xmm0, xmm4                                    ;161.11
        movss     DWORD PTR [_DYNUST_MAIN_MODULE_mp_STOPNOINFO_1], xmm0 ;161.11
        jmp       .B1.29        ; Prob 100%                     ;161.11
                                ; LOE ebx
.B1.59:                         ; Preds .B1.49                  ; Infreq
        movss     xmm5, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ENTRY_QUEUE2] ;108.8
        movss     xmm6, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRIPTIME2] ;109.8
        addss     xmm5, xmm2                                    ;108.8
        addss     xmm6, xmm3                                    ;109.8
        movsx     eax, BYTE PTR [80+ecx+esi]                    ;114.31
        cdq                                                     ;114.31
        mov       edi, DWORD PTR [44+ecx+esi]                   ;115.22
        xor       eax, edx                                      ;114.31
        movss     DWORD PTR [_DYNUST_MAIN_MODULE_mp_ENTRY_QUEUE2], xmm5 ;108.8
        sub       eax, edx                                      ;114.31
        movss     DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRIPTIME2], xmm6 ;109.8
        dec       eax                                           ;114.59
        movss     xmm7, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VTOTHR2] ;111.8
        movss     xmm5, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DTOTAL2] ;112.8
        addss     xmm7, xmm1                                    ;111.8
        addss     xmm5, xmm0                                    ;112.8
        movss     xmm6, DWORD PTR [_DYNUST_MAIN_MODULE_mp_STOPINFO] ;113.8
        sub       edi, DWORD PTR [76+ecx+esi]                   ;115.8
        addss     xmm6, xmm4                                    ;113.8
        mov       BYTE PTR [80+ecx+esi], al                     ;114.8
        movsx     ecx, BYTE PTR [1+edi]                         ;115.22
        inc       DWORD PTR [_DYNUST_MAIN_MODULE_mp_INFORMATION] ;110.8
        add       DWORD PTR [_DYNUST_MAIN_MODULE_mp_TOTALDECISION], ecx ;115.8
        movss     DWORD PTR [_DYNUST_MAIN_MODULE_mp_VTOTHR2], xmm7 ;111.8
        movss     DWORD PTR [_DYNUST_MAIN_MODULE_mp_DTOTAL2], xmm5 ;112.8
        movss     DWORD PTR [_DYNUST_MAIN_MODULE_mp_STOPINFO], xmm6 ;113.8
        mov       DWORD PTR [esp], eax                          ;114.59
        mov       DWORD PTR [8+esp], ecx                        ;115.22
        cmp       DWORD PTR [16+esp], 1                         ;117.38
        je        .B1.94        ; Prob 16%                      ;117.38
                                ; LOE ebx xmm0 xmm1 xmm2 xmm3 xmm4
.B1.60:                         ; Preds .B1.59                  ; Infreq
        cmp       DWORD PTR [16+esp], 2                         ;124.42
        je        .B1.93        ; Prob 16%                      ;124.42
                                ; LOE ebx xmm0 xmm1 xmm2 xmm3 xmm4
.B1.61:                         ; Preds .B1.60                  ; Infreq
        cmp       DWORD PTR [16+esp], 3                         ;131.42
        jne       .B1.63        ; Prob 84%                      ;131.42
                                ; LOE ebx xmm0 xmm1 xmm2 xmm3 xmm4
.B1.62:                         ; Preds .B1.61                  ; Infreq
        movss     xmm5, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ENTRY_QUEUE2_3] ;132.11
        inc       DWORD PTR [_DYNUST_MAIN_MODULE_mp_INFORMATION_3] ;134.11
        addss     xmm5, xmm2                                    ;132.11
        movss     xmm2, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRIPTIME2_3] ;133.11
        movss     DWORD PTR [_DYNUST_MAIN_MODULE_mp_ENTRY_QUEUE2_3], xmm5 ;132.11
        addss     xmm2, xmm3                                    ;133.11
        movss     xmm3, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VTOTHR2_3] ;135.11
        movss     DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRIPTIME2_3], xmm2 ;133.11
        addss     xmm3, xmm1                                    ;135.11
        movss     xmm1, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DTOTAL2_3] ;136.11
        movss     DWORD PTR [_DYNUST_MAIN_MODULE_mp_VTOTHR2_3], xmm3 ;135.11
        addss     xmm1, xmm0                                    ;136.11
        movss     xmm0, DWORD PTR [_DYNUST_MAIN_MODULE_mp_STOPINFO_3] ;137.11
        movss     DWORD PTR [_DYNUST_MAIN_MODULE_mp_DTOTAL2_3], xmm1 ;136.11
        addss     xmm0, xmm4                                    ;137.11
        movss     DWORD PTR [_DYNUST_MAIN_MODULE_mp_STOPINFO_3], xmm0 ;137.11
                                ; LOE ebx
.B1.63:                         ; Preds .B1.61 .B1.94 .B1.93 .B1.62 ; Infreq
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NU_SWITCH] ;139.8
        mov       DWORD PTR [12+esp], eax                       ;139.8
        lea       edx, DWORD PTR [1+eax]                        ;139.8
        test      edx, edx                                      ;139.8
        jle       .B1.86        ; Prob 50%                      ;139.8
                                ; LOE edx ebx
.B1.64:                         ; Preds .B1.63                  ; Infreq
        mov       edi, edx                                      ;139.8
        shr       edi, 31                                       ;139.8
        add       edi, edx                                      ;139.8
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SWITCHNUM] ;145.48
        sar       edi, 1                                        ;139.8
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SWITCHNUM+32] ;145.48
        test      edi, edi                                      ;139.8
        mov       DWORD PTR [16+esp], eax                       ;145.48
        mov       DWORD PTR [4+esp], ecx                        ;145.48
        movsx     eax, BYTE PTR [esp]                           ;140.14
        jbe       .B1.92        ; Prob 3%                       ;139.8
                                ; LOE eax edx ecx ebx edi cl ch
.B1.65:                         ; Preds .B1.64                  ; Infreq
        xor       esi, esi                                      ;
        add       ecx, ecx                                      ;
        neg       ecx                                           ;
        add       ecx, DWORD PTR [16+esp]                       ;
                                ; LOE eax edx ecx esi edi
.B1.66:                         ; Preds .B1.70 .B1.65           ; Infreq
        lea       ebx, DWORD PTR [esi+esi]                      ;140.37
        cmp       eax, ebx                                      ;140.37
        jne       .B1.68        ; Prob 50%                      ;140.37
                                ; LOE eax edx ecx esi edi
.B1.67:                         ; Preds .B1.66                  ; Infreq
        inc       WORD PTR [2+ecx+esi*4]                        ;140.73
                                ; LOE eax edx ecx esi edi
.B1.68:                         ; Preds .B1.66 .B1.67           ; Infreq
        lea       ebx, DWORD PTR [1+esi+esi]                    ;140.42
        cmp       eax, ebx                                      ;140.37
        jne       .B1.70        ; Prob 50%                      ;140.37
                                ; LOE eax edx ecx esi edi
.B1.69:                         ; Preds .B1.68                  ; Infreq
        inc       WORD PTR [4+ecx+esi*4]                        ;140.73
                                ; LOE eax edx ecx esi edi
.B1.70:                         ; Preds .B1.68 .B1.69           ; Infreq
        inc       esi                                           ;139.8
        cmp       esi, edi                                      ;139.8
        jb        .B1.66        ; Prob 64%                      ;139.8
                                ; LOE eax edx ecx esi edi
.B1.71:                         ; Preds .B1.70                  ; Infreq
        mov       ebx, DWORD PTR [12+ebp]                       ;
        lea       edi, DWORD PTR [1+esi+esi]                    ;139.8
                                ; LOE eax edx ebx edi
.B1.72:                         ; Preds .B1.71 .B1.92           ; Infreq
        lea       ecx, DWORD PTR [-1+edi]                       ;139.8
        cmp       edx, ecx                                      ;139.8
        jbe       .B1.75        ; Prob 3%                       ;139.8
                                ; LOE eax edx ecx ebx edi
.B1.73:                         ; Preds .B1.72                  ; Infreq
        cmp       eax, ecx                                      ;140.37
        jne       .B1.75        ; Prob 50%                      ;140.37
                                ; LOE eax edx ebx edi
.B1.74:                         ; Preds .B1.73                  ; Infreq
        mov       esi, DWORD PTR [4+esp]                        ;140.46
        neg       esi                                           ;140.46
        add       esi, edi                                      ;140.46
        mov       ecx, DWORD PTR [16+esp]                       ;140.73
        inc       WORD PTR [ecx+esi*2]                          ;140.73
                                ; LOE eax edx ebx
.B1.75:                         ; Preds .B1.74 .B1.73 .B1.72    ; Infreq
        mov       ecx, edx                                      ;142.8
        shr       ecx, 31                                       ;142.8
        add       ecx, edx                                      ;142.8
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DECISIONNUM] ;146.52
        sar       ecx, 1                                        ;142.8
        mov       DWORD PTR [16+esp], esi                       ;146.52
        test      ecx, ecx                                      ;142.8
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DECISIONNUM+32] ;146.52
        mov       DWORD PTR [20+esp], ecx                       ;142.8
        jbe       .B1.91        ; Prob 3%                       ;142.8
                                ; LOE eax edx ebx esi
.B1.76:                         ; Preds .B1.75                  ; Infreq
        mov       DWORD PTR [esp], eax                          ;
        lea       edi, DWORD PTR [esi+esi]                      ;
        neg       edi                                           ;
        xor       ecx, ecx                                      ;
        add       edi, DWORD PTR [16+esp]                       ;
        mov       DWORD PTR [4+esp], edx                        ;
        mov       edx, edi                                      ;
        mov       ebx, DWORD PTR [20+esp]                       ;
        mov       eax, DWORD PTR [8+esp]                        ;
                                ; LOE eax edx ecx ebx esi
.B1.77:                         ; Preds .B1.81 .B1.76           ; Infreq
        lea       edi, DWORD PTR [ecx+ecx]                      ;143.42
        cmp       eax, edi                                      ;143.42
        jne       .B1.79        ; Prob 50%                      ;143.42
                                ; LOE eax edx ecx ebx esi
.B1.78:                         ; Preds .B1.77                  ; Infreq
        inc       WORD PTR [2+edx+ecx*4]                        ;143.82
                                ; LOE eax edx ecx ebx esi
.B1.79:                         ; Preds .B1.77 .B1.78           ; Infreq
        lea       edi, DWORD PTR [1+ecx+ecx]                    ;143.47
        cmp       eax, edi                                      ;143.42
        jne       .B1.81        ; Prob 50%                      ;143.42
                                ; LOE eax edx ecx ebx esi
.B1.80:                         ; Preds .B1.79                  ; Infreq
        inc       WORD PTR [4+edx+ecx*4]                        ;143.82
                                ; LOE eax edx ecx ebx esi
.B1.81:                         ; Preds .B1.79 .B1.80           ; Infreq
        inc       ecx                                           ;142.8
        cmp       ecx, ebx                                      ;142.8
        jb        .B1.77        ; Prob 64%                      ;142.8
                                ; LOE eax edx ecx ebx esi
.B1.82:                         ; Preds .B1.81                  ; Infreq
        mov       eax, DWORD PTR [esp]                          ;
        lea       edi, DWORD PTR [1+ecx+ecx]                    ;142.8
        mov       edx, DWORD PTR [4+esp]                        ;
        mov       ebx, DWORD PTR [12+ebp]                       ;
                                ; LOE eax edx ebx esi edi
.B1.83:                         ; Preds .B1.82 .B1.91           ; Infreq
        lea       ecx, DWORD PTR [-1+edi]                       ;142.8
        cmp       edx, ecx                                      ;142.8
        jbe       .B1.87        ; Prob 3%                       ;142.8
                                ; LOE eax ecx ebx esi edi
.B1.84:                         ; Preds .B1.83                  ; Infreq
        cmp       ecx, DWORD PTR [8+esp]                        ;143.42
        jne       .B1.87        ; Prob 50%                      ;143.42
                                ; LOE eax ebx esi edi
.B1.85:                         ; Preds .B1.84                  ; Infreq
        neg       esi                                           ;143.51
        add       esi, edi                                      ;143.51
        mov       edx, DWORD PTR [16+esp]                       ;143.82
        inc       WORD PTR [edx+esi*2]                          ;143.82
        jmp       .B1.87        ; Prob 100%                     ;143.82
                                ; LOE eax ebx
.B1.86:                         ; Preds .B1.63                  ; Infreq
        movsx     eax, BYTE PTR [esp]                           ;145.11
                                ; LOE eax ebx
.B1.87:                         ; Preds .B1.86 .B1.84 .B1.83 .B1.85 ; Infreq
        cmp       eax, DWORD PTR [12+esp]                       ;145.34
        jle       .B1.89        ; Prob 50%                      ;145.34
                                ; LOE ebx
.B1.88:                         ; Preds .B1.87                  ; Infreq
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SWITCHNUM+32] ;145.48
        neg       edx                                           ;145.48
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SWITCHNUM] ;145.48
        add       edx, DWORD PTR [12+esp]                       ;145.48
        inc       WORD PTR [2+eax+edx*2]                        ;145.93
                                ; LOE ebx
.B1.89:                         ; Preds .B1.87 .B1.88           ; Infreq
        mov       eax, DWORD PTR [8+esp]                        ;146.39
        cmp       eax, DWORD PTR [12+esp]                       ;146.39
        jle       .B1.29        ; Prob 50%                      ;146.39
                                ; LOE ebx
.B1.90:                         ; Preds .B1.89                  ; Infreq
        mov       edx, DWORD PTR [12+esp]                       ;146.52
        sub       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DECISIONNUM+32] ;146.52
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DECISIONNUM] ;146.52
        inc       WORD PTR [2+eax+edx*2]                        ;146.101
        jmp       .B1.29        ; Prob 100%                     ;146.101
                                ; LOE ebx
.B1.91:                         ; Preds .B1.75                  ; Infreq
        mov       edi, 1                                        ;
        jmp       .B1.83        ; Prob 100%                     ;
                                ; LOE eax edx ebx esi edi
.B1.92:                         ; Preds .B1.64                  ; Infreq
        mov       edi, 1                                        ;
        jmp       .B1.72        ; Prob 100%                     ;
                                ; LOE eax edx ebx edi
.B1.93:                         ; Preds .B1.60                  ; Infreq
        movss     xmm5, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ENTRY_QUEUE2_2] ;125.11
        inc       DWORD PTR [_DYNUST_MAIN_MODULE_mp_INFORMATION_2] ;127.11
        addss     xmm5, xmm2                                    ;125.11
        movss     xmm2, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRIPTIME2_2] ;126.11
        movss     DWORD PTR [_DYNUST_MAIN_MODULE_mp_ENTRY_QUEUE2_2], xmm5 ;125.11
        addss     xmm2, xmm3                                    ;126.11
        movss     xmm3, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VTOTHR2_2] ;128.11
        movss     DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRIPTIME2_2], xmm2 ;126.11
        addss     xmm3, xmm1                                    ;128.11
        movss     xmm1, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DTOTAL2_2] ;129.11
        movss     DWORD PTR [_DYNUST_MAIN_MODULE_mp_VTOTHR2_2], xmm3 ;128.11
        addss     xmm1, xmm0                                    ;129.11
        movss     xmm0, DWORD PTR [_DYNUST_MAIN_MODULE_mp_STOPINFO_2] ;130.11
        movss     DWORD PTR [_DYNUST_MAIN_MODULE_mp_DTOTAL2_2], xmm1 ;129.11
        addss     xmm0, xmm4                                    ;130.11
        movss     DWORD PTR [_DYNUST_MAIN_MODULE_mp_STOPINFO_2], xmm0 ;130.11
        jmp       .B1.63        ; Prob 100%                     ;130.11
                                ; LOE ebx
.B1.94:                         ; Preds .B1.59                  ; Infreq
        movss     xmm5, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ENTRY_QUEUE2_1] ;118.11
        inc       DWORD PTR [_DYNUST_MAIN_MODULE_mp_INFORMATION_1] ;120.11
        addss     xmm5, xmm2                                    ;118.11
        movss     xmm2, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRIPTIME2_1] ;119.11
        movss     DWORD PTR [_DYNUST_MAIN_MODULE_mp_ENTRY_QUEUE2_1], xmm5 ;118.11
        addss     xmm2, xmm3                                    ;119.11
        movss     xmm3, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VTOTHR2_1] ;121.11
        movss     DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRIPTIME2_1], xmm2 ;119.11
        addss     xmm3, xmm1                                    ;121.11
        movss     xmm1, DWORD PTR [_DYNUST_MAIN_MODULE_mp_DTOTAL2_1] ;122.11
        movss     DWORD PTR [_DYNUST_MAIN_MODULE_mp_VTOTHR2_1], xmm3 ;121.11
        addss     xmm1, xmm0                                    ;122.11
        movss     xmm0, DWORD PTR [_DYNUST_MAIN_MODULE_mp_STOPINFO_1] ;123.11
        movss     DWORD PTR [_DYNUST_MAIN_MODULE_mp_DTOTAL2_1], xmm1 ;122.11
        addss     xmm0, xmm4                                    ;123.11
        movss     DWORD PTR [_DYNUST_MAIN_MODULE_mp_STOPINFO_1], xmm0 ;123.11
        jmp       .B1.63        ; Prob 100%                     ;123.11
                                ; LOE ebx
.B1.96:                         ; Preds .B1.23                  ; Infreq
        movss     xmm0, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TIME_LOV_OHOT] ;81.7
        inc       DWORD PTR [_DYNUST_MAIN_MODULE_mp_IACTUAL_LOV_OHOT] ;80.7
        addss     xmm0, DWORD PTR [88+ecx+esi]                  ;81.7
        movss     DWORD PTR [_DYNUST_MAIN_MODULE_mp_TIME_LOV_OHOT], xmm0 ;81.7
        jmp       .B1.26        ; Prob 100%                     ;81.7
        ALIGN     16
                                ; LOE eax ecx ebx esi
; mark_end;
_CAL_ACCUMU_STAT ENDP
_TEXT	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
__STRLITPACK_6.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_7.0.1	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_8.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_9.0.1	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_10.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_11.0.1	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__NLITPACK_0.0.1	DD	3
__NLITPACK_1.0.1	DD	2
__NLITPACK_3.0.1	DD	18
__NLITPACK_2.0.1	DD	1
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _CAL_ACCUMU_STAT
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
_2il0floatpacket.3	DD	042700000H,042700000H,042700000H,042700000H
__STRLITPACK_4	DB	69
	DB	82
	DB	82
	DB	79
	DB	82
	DB	32
	DB	111
	DB	110
	DB	32
	DB	97
	DB	114
	DB	114
	DB	105
	DB	118
	DB	97
	DB	108
	DB	32
	DB	116
	DB	105
	DB	109
	DB	101
	DB	32
	DB	102
	DB	111
	DB	114
	DB	32
	DB	118
	DB	101
	DB	104
	DB	105
	DB	99
	DB	108
	DB	101
	DB	32
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_1	DB	97
	DB	114
	DB	114
	DB	105
	DB	118
	DB	97
	DB	108
	DB	32
	DB	116
	DB	105
	DB	109
	DB	101
	DB	61
	DB	32
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_0	DB	32
	DB	115
	DB	116
	DB	97
	DB	114
	DB	116
	DB	32
	DB	116
	DB	105
	DB	109
	DB	101
	DB	61
	DB	32
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_12	DB	0
	DB 3 DUP ( 0H)	; pad
_2il0floatpacket.4	DD	042700000H
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
EXTRN	_DYNUST_MAIN_MODULE_mp_ITAG0:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_DTOTAL3_3:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_VTOTHR3_3:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NOINFORMATION_3:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TRIPTIME3_3:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ENTRY_QUEUE3_3:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_STOPNOINFO_2:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_DTOTAL3_2:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_VTOTHR3_2:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NOINFORMATION_2:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TRIPTIME3_2:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ENTRY_QUEUE3_2:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_STOPNOINFO_1:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_DTOTAL3_1:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_VTOTHR3_1:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NOINFORMATION_1:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TRIPTIME3_1:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ENTRY_QUEUE3_1:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_STOPNOINFO:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_DTOTAL3:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_VTOTHR3:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NOINFORMATION:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TRIPTIME3:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ENTRY_QUEUE3:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_DECISIONNUM:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_SWITCHNUM:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NU_SWITCH:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_STOPINFO_3:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_DTOTAL2_3:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_VTOTHR2_3:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_INFORMATION_3:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TRIPTIME2_3:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ENTRY_QUEUE2_3:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_STOPINFO_2:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_DTOTAL2_2:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_VTOTHR2_2:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_INFORMATION_2:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TRIPTIME2_2:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ENTRY_QUEUE2_2:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_STOPINFO_1:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_DTOTAL2_1:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_VTOTHR2_1:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_INFORMATION_1:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TRIPTIME2_1:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ENTRY_QUEUE2_1:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TOTALDECISION:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_STOPINFO:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_DTOTAL2:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_VTOTHR2:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_INFORMATION:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TRIPTIME2:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ENTRY_QUEUE2:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ITAG2:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_VTOTHR1:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_DTOTAL1:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TRIPTIME1:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ENTRY_QUEUE1:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_STOPTIME:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TIME_HOV_OHOT:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_IACTUAL_HOV_OHOT:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TIME_HOV_HOT:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_IACTUAL_HOV_HOT:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TIME_LOV_OHOT:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_IACTUAL_LOV_OHOT:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TIME_LOV_HOT:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_IACTUAL_LOV_HOT:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_REACH_CONVERG:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ITERATION:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ITEMAX:BYTE
EXTRN	_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE:BYTE
EXTRN	_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_KTOTAL_OUT:BYTE
EXTRN	_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND:BYTE
_DATA	ENDS
EXTRN	_for_stop_core:PROC
EXTRN	_for_write_seq_lis_xmit:PROC
EXTRN	_for_write_seq_lis:PROC
EXTRN	_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_INSERT:PROC
EXTRN	_WRITE_PRINT_TRAJECTORY:PROC
EXTRN	_PRINTALT:PROC
EXTRN	_WRITE_SUMMARY:PROC
EXTRN	_WRITE_SUMMARY_TYPEBASED:PROC
EXTRN	__fltused:BYTE
	INCLUDELIB <ifconsol>
	INCLUDELIB <libifcoremt>
	INCLUDELIB <libifport>
	INCLUDELIB <libmmt>
	INCLUDELIB <LIBCMT>
	INCLUDELIB <libirc>
	INCLUDELIB <svml_dispmt>
	INCLUDELIB <OLDNAMES>
	END
