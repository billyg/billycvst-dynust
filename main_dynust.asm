; mark_description "Intel(R) Visual Fortran Compiler XE for applications running on IA-32, Version 13.1.1.171 Build 20130313";
; mark_description "-S";
	.686P
 	.387
	OPTION DOTNAME
	ASSUME	CS:FLAT,DS:FLAT,SS:FLAT
;ident "-defaultlib:IFWIN.LIB"
;ident "-defaultlib:USER32.LIB"
;ident "-defaultlib:iflogm.lib"
;ident "-defaultlib:user32.lib"
;ident "-defaultlib:comctl32.lib"
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
TXTST0:
; -- Begin  _MAIN__
; mark_begin;
IF @Version GE 800
  .MMX
ELSEIF @Version GE 612
  .MMX
  MMWORD TEXTEQU <QWORD>
ENDIF
IF @Version GE 800
  .XMM
ELSEIF @Version GE 614
  .XMM
  XMMWORD TEXTEQU <OWORD>
ENDIF
       ALIGN     16
	PUBLIC _MAIN__
_MAIN__	PROC NEAR 
.B1.1:                          ; Preds .B1.0
        push      ebp                                           ;1.9
        mov       ebp, esp                                      ;1.9
        and       esp, -128                                     ;1.9
        push      esi                                           ;1.9
        push      edi                                           ;1.9
        sub       esp, 248                                      ;1.9
        push      3                                             ;1.9
        call      ___intel_new_proc_init                        ;1.9
                                ; LOE ebx
.B1.74:                         ; Preds .B1.1
        stmxcsr   DWORD PTR [4+esp]                             ;1.9
        or        DWORD PTR [4+esp], 32768                      ;1.9
        ldmxcsr   DWORD PTR [4+esp]                             ;1.9
        push      OFFSET FLAT: __NLITPACK_0.0.1                 ;1.9
        call      _for_set_reentrancy                           ;1.9
                                ; LOE ebx
.B1.2:                          ; Preds .B1.74
        mov       DWORD PTR [124+esp], 0                        ;62.3
        lea       eax, DWORD PTR [92+esp]                       ;64.8
        push      eax                                           ;64.8
        call      _for_cpusec                                   ;64.8
                                ; LOE ebx
.B1.75:                         ; Preds .B1.2
        add       esp, 12                                       ;64.8
                                ; LOE ebx
.B1.3:                          ; Preds .B1.75
        call      _WINSETUP                                     ;66.8
                                ; LOE ebx
.B1.4:                          ; Preds .B1.3
        xor       edi, edi                                      ;70.3
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ITERATION], edi ;70.3
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_SOINT], edi ;72.3
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_LOGOUT], 0  ;75.13
        jle       .B1.91        ; Prob 16%                      ;75.13
                                ; LOE ebx edi
.B1.5:                          ; Preds .B1.4
        mov       DWORD PTR [esp], 0                            ;75.18
        lea       esi, DWORD PTR [esp]                          ;75.18
        mov       DWORD PTR [56+esp], 11                        ;75.18
        lea       eax, DWORD PTR [56+esp]                       ;75.18
        mov       DWORD PTR [60+esp], OFFSET FLAT: __STRLITPACK_51 ;75.18
        push      32                                            ;75.18
        push      eax                                           ;75.18
        push      OFFSET FLAT: __STRLITPACK_53.0.1              ;75.18
        push      -2088435968                                   ;75.18
        push      711                                           ;75.18
        push      esi                                           ;75.18
        call      _for_write_seq_lis                            ;75.18
                                ; LOE ebx esi edi
.B1.76:                         ; Preds .B1.5
        add       esp, 24                                       ;75.18
                                ; LOE ebx esi edi
.B1.6:                          ; Preds .B1.76 .B1.91
        lea       eax, DWORD PTR [88+esp]                       ;76.8
        push      eax                                           ;76.8
        call      _for_cpusec                                   ;76.8
                                ; LOE ebx esi edi
.B1.7:                          ; Preds .B1.6
        push      OFFSET FLAT: _DYNUSTMAIN$MAXINTERVALS.0.1     ;77.8
        call      _SIM_DYNUST                                   ;77.8
                                ; LOE ebx esi edi
.B1.8:                          ; Preds .B1.7
        lea       eax, DWORD PTR [100+esp]                      ;78.8
        push      eax                                           ;78.8
        call      _for_cpusec                                   ;78.8
                                ; LOE ebx esi edi
.B1.77:                         ; Preds .B1.8
        add       esp, 12                                       ;78.8
                                ; LOE ebx esi edi
.B1.9:                          ; Preds .B1.77
        movss     xmm0, DWORD PTR [92+esp]                      ;79.3
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ITERATION] ;82.6
        pxor      xmm1, xmm1                                    ;68.3
        movss     DWORD PTR [132+esp], xmm1                     ;68.3
        cmp       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ITEMAX] ;82.25
        subss     xmm0, DWORD PTR [88+esp]                      ;79.3
        movss     DWORD PTR [156+esp], xmm0                     ;79.3
        jge       .B1.40        ; Prob 10%                      ;82.25
                                ; LOE ebx esi edi
.B1.10:                         ; Preds .B1.9
        movss     xmm0, DWORD PTR [_2il0floatpacket.0]          ;126.23
        mov       DWORD PTR [80+esp], ebx                       ;126.23
                                ; LOE esi edi
.B1.11:                         ; Preds .B1.38 .B1.10
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ISO_OK], 1  ;82.46
        je        .B1.13        ; Prob 16%                      ;82.46
                                ; LOE esi edi
.B1.12:                         ; Preds .B1.11
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_IUE_OK], 1  ;82.61
        jne       .B1.39        ; Prob 20%                      ;82.61
                                ; LOE esi edi
.B1.13:                         ; Preds .B1.11 .B1.12
        call      _CLOSEFILE                                    ;83.12
                                ; LOE esi edi
.B1.14:                         ; Preds .B1.13
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ITERATION] ;86.7
        inc       eax                                           ;86.7
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_TOTALVIOLATION], edi ;85.7
        cmp       eax, 1                                        ;87.20
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ITERATION], eax ;86.7
        je        .B1.64        ; Prob 16%                      ;87.20
                                ; LOE esi edi
.B1.15:                         ; Preds .B1.14 .B1.65 .B1.69 .B1.64
        test      BYTE PTR [_DYNUST_MAIN_MODULE_mp_CALLKSP1], 1 ;97.10
        jne       .B1.62        ; Prob 3%                       ;97.10
                                ; LOE esi edi
.B1.16:                         ; Preds .B1.15 .B1.63
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_IUE_OK], 0  ;103.17
        jle       .B1.92        ; Prob 16%                      ;103.17
                                ; LOE esi edi
.B1.17:                         ; Preds .B1.16
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_LOGOUT], 0  ;104.16
        jle       .B1.19        ; Prob 16%                      ;104.16
                                ; LOE esi edi
.B1.18:                         ; Preds .B1.17
        mov       DWORD PTR [esp], 0                            ;104.21
        lea       eax, DWORD PTR [64+esp]                       ;104.21
        mov       DWORD PTR [64+esp], 10                        ;104.21
        mov       DWORD PTR [68+esp], OFFSET FLAT: __STRLITPACK_49 ;104.21
        push      32                                            ;104.21
        push      eax                                           ;104.21
        push      OFFSET FLAT: __STRLITPACK_54.0.1              ;104.21
        push      -2088435968                                   ;104.21
        push      711                                           ;104.21
        push      esi                                           ;104.21
        call      _for_write_seq_lis                            ;104.21
                                ; LOE esi edi
.B1.78:                         ; Preds .B1.18
        add       esp, 24                                       ;104.21
                                ; LOE esi edi
.B1.19:                         ; Preds .B1.78 .B1.17
        push      10485760                                      ;105.18
        call      __setcolorrgb                                 ;105.18
                                ; LOE esi edi
.B1.20:                         ; Preds .B1.19
        push      500                                           ;106.18
        push      550                                           ;106.18
        push      395                                           ;106.18
        push      0                                             ;106.18
        push      3                                             ;106.18
        call      __rectangle                                   ;106.18
                                ; LOE esi edi
.B1.79:                         ; Preds .B1.20
        add       esp, 24                                       ;106.18
                                ; LOE esi edi
.B1.21:                         ; Preds .B1.79
        mov       DWORD PTR [116+esp], 3                        ;107.6
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_LOGOUT], 0  ;108.14
        jle       .B1.23        ; Prob 16%                      ;108.14
                                ; LOE esi edi
.B1.22:                         ; Preds .B1.21
        mov       DWORD PTR [esp], 0                            ;108.19
        lea       eax, DWORD PTR [72+esp]                       ;108.19
        mov       DWORD PTR [72+esp], 21                        ;108.19
        mov       DWORD PTR [76+esp], OFFSET FLAT: __STRLITPACK_47 ;108.19
        push      32                                            ;108.19
        push      eax                                           ;108.19
        push      OFFSET FLAT: __STRLITPACK_55.0.1              ;108.19
        push      -2088435968                                   ;108.19
        push      711                                           ;108.19
        push      esi                                           ;108.19
        call      _for_write_seq_lis                            ;108.19
                                ; LOE esi edi
.B1.80:                         ; Preds .B1.22
        add       esp, 24                                       ;108.19
                                ; LOE esi edi
.B1.23:                         ; Preds .B1.80 .B1.21
        push      edi                                           ;109.10
        push      edi                                           ;109.10
        push      21                                            ;109.10
        push      OFFSET FLAT: __STRLITPACK_46                  ;109.10
        push      edi                                           ;109.10
        push      80                                            ;109.10
        push      OFFSET FLAT: _DYNUSTMAIN$PRINTSTR1.0.1        ;109.10
        call      _for_cpystr                                   ;109.10
                                ; LOE esi edi
.B1.24:                         ; Preds .B1.23
        push      80                                            ;110.15
        mov       eax, OFFSET FLAT: __NLITPACK_0.0.1            ;110.15
        push      eax                                           ;110.15
        push      eax                                           ;110.15
        push      eax                                           ;110.15
        push      eax                                           ;110.15
        push      eax                                           ;110.15
        push      OFFSET FLAT: __NLITPACK_3.0.1                 ;110.15
        push      OFFSET FLAT: __NLITPACK_2.0.1                 ;110.15
        push      OFFSET FLAT: _DYNUSTMAIN$PRINTSTR1.0.1        ;110.15
        call      _PRINTSCREEN                                  ;110.15
                                ; LOE esi edi
.B1.25:                         ; Preds .B1.24
        lea       eax, DWORD PTR [188+esp]                      ;112.15
        push      eax                                           ;112.15
        call      _for_cpusec                                   ;112.15
                                ; LOE esi edi
.B1.81:                         ; Preds .B1.25
        add       esp, 68                                       ;112.15
                                ; LOE esi edi
.B1.26:                         ; Preds .B1.81
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFPS] ;114.9
        test      ecx, ecx                                      ;114.19
        jle       .B1.28        ; Prob 6%                       ;114.19
                                ; LOE ecx esi edi
.B1.27:                         ; Preds .B1.26
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ITEMAX] ;115.11
        cdq                                                     ;115.34
        idiv      ecx                                           ;115.34
        mov       ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ITERATION] ;115.11
        mov       ecx, eax                                      ;115.34
        mov       eax, ebx                                      ;115.14
        cdq                                                     ;115.14
        idiv      ecx                                           ;115.14
        test      edx, edx                                      ;115.43
        je        .B1.29        ; Prob 78%                      ;115.43
                                ; LOE esi edi
.B1.28:                         ; Preds .B1.26 .B1.27
        lea       eax, DWORD PTR [116+esp]                      ;118.18
        push      OFFSET FLAT: _DYNUSTMAIN$MAXINTERVALS.0.1     ;118.18
        push      eax                                           ;118.18
        call      _MIVAGFV                                      ;118.18
                                ; LOE esi edi
.B1.82:                         ; Preds .B1.28
        add       esp, 8                                        ;118.18
                                ; LOE esi edi
.B1.29:                         ; Preds .B1.82 .B1.27
        lea       ebx, DWORD PTR [140+esp]                      ;124.14
        push      ebx                                           ;124.14
        call      _for_cpusec                                   ;124.14
                                ; LOE ebx esi edi
.B1.83:                         ; Preds .B1.29
        add       esp, 4                                        ;124.14
                                ; LOE ebx esi edi
.B1.30:                         ; Preds .B1.83
        movss     xmm0, DWORD PTR [140+esp]                     ;125.10
        movss     xmm1, DWORD PTR [132+esp]                     ;125.34
        movss     xmm2, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TOTAL_HOV] ;126.10
        comiss    xmm2, DWORD PTR [_2il0floatpacket.0]          ;126.23
        subss     xmm0, DWORD PTR [124+esp]                     ;125.10
        addss     xmm1, xmm0                                    ;125.34
        movss     DWORD PTR [132+esp], xmm1                     ;125.34
        jbe       .B1.33        ; Prob 50%                      ;126.23
                                ; LOE ebx esi edi
.B1.31:                         ; Preds .B1.30
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_LOGOUT], 0  ;127.19
        jle       .B1.33        ; Prob 16%                      ;127.19
                                ; LOE ebx esi edi
.B1.32:                         ; Preds .B1.31
        mov       DWORD PTR [esp], 0                            ;127.24
        lea       eax, DWORD PTR [48+esp]                       ;127.24
        mov       DWORD PTR [48+esp], 21                        ;127.24
        mov       DWORD PTR [52+esp], OFFSET FLAT: __STRLITPACK_44 ;127.24
        push      32                                            ;127.24
        push      eax                                           ;127.24
        push      OFFSET FLAT: __STRLITPACK_56.0.1              ;127.24
        push      -2088435968                                   ;127.24
        push      711                                           ;127.24
        push      esi                                           ;127.24
        call      _for_write_seq_lis                            ;127.24
                                ; LOE ebx esi edi
.B1.84:                         ; Preds .B1.32
        add       esp, 24                                       ;127.24
                                ; LOE ebx esi edi
.B1.33:                         ; Preds .B1.84 .B1.31 .B1.30 .B1.92
        call      _OPENFILE                                     ;136.12
                                ; LOE ebx esi edi
.B1.34:                         ; Preds .B1.33
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_JUSTVEH] ;138.7
        push      ebx                                           ;140.11
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_JTOTAL], eax ;138.7
        call      _for_cpusec                                   ;140.11
                                ; LOE esi edi
.B1.35:                         ; Preds .B1.34
        push      OFFSET FLAT: _DYNUSTMAIN$MAXINTERVALS.0.1     ;141.11
        call      _SIM_DYNUST                                   ;141.11
                                ; LOE esi edi
.B1.36:                         ; Preds .B1.35
        lea       eax, DWORD PTR [156+esp]                      ;142.11
        push      eax                                           ;142.11
        call      _for_cpusec                                   ;142.11
                                ; LOE esi edi
.B1.85:                         ; Preds .B1.36
        add       esp, 12                                       ;142.11
                                ; LOE esi edi
.B1.37:                         ; Preds .B1.85
        movss     xmm0, DWORD PTR [148+esp]                     ;143.6
        movss     xmm1, DWORD PTR [156+esp]                     ;143.30
        test      BYTE PTR [_DYNUST_MAIN_MODULE_mp_REACH_CONVERG], 1 ;145.9
        subss     xmm0, DWORD PTR [140+esp]                     ;143.6
        addss     xmm1, xmm0                                    ;143.30
        movss     DWORD PTR [156+esp], xmm1                     ;143.30
        jne       .B1.39        ; Prob 20%                      ;145.9
                                ; LOE esi edi
.B1.38:                         ; Preds .B1.37
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ITERATION] ;82.6
        cmp       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ITEMAX] ;82.25
        jl        .B1.11        ; Prob 82%                      ;82.25
                                ; LOE esi edi
.B1.39:                         ; Preds .B1.12 .B1.37 .B1.38
        mov       ebx, DWORD PTR [80+esp]                       ;
                                ; LOE ebx esi edi
.B1.40:                         ; Preds .B1.9 .B1.39
        call      _CLOSEFILE                                    ;153.11
                                ; LOE ebx esi edi
.B1.41:                         ; Preds .B1.40
        mov       DWORD PTR [esp], edi                          ;154.12
        push      32                                            ;154.12
        push      edi                                           ;154.12
        push      OFFSET FLAT: __STRLITPACK_57.0.1              ;154.12
        push      -2088435968                                   ;154.12
        push      912                                           ;154.12
        push      esi                                           ;154.12
        call      _for_close                                    ;154.12
                                ; LOE ebx esi edi
.B1.42:                         ; Preds .B1.41
        mov       DWORD PTR [24+esp], 0                         ;156.6
        lea       eax, DWORD PTR [56+esp]                       ;156.6
        mov       DWORD PTR [56+esp], 13                        ;156.6
        mov       DWORD PTR [60+esp], OFFSET FLAT: __STRLITPACK_43 ;156.6
        mov       DWORD PTR [64+esp], 7                         ;156.6
        mov       DWORD PTR [68+esp], OFFSET FLAT: __STRLITPACK_42 ;156.6
        push      32                                            ;156.6
        push      eax                                           ;156.6
        push      OFFSET FLAT: __STRLITPACK_58.0.1              ;156.6
        push      -2088435968                                   ;156.6
        push      9999                                          ;156.6
        push      esi                                           ;156.6
        call      _for_open                                     ;156.6
                                ; LOE ebx esi edi
.B1.43:                         ; Preds .B1.42
        cvtsi2ss  xmm0, DWORD PTR [_DYNUSTMAIN$MAXINTERVALS.0.1] ;157.24
        mulss     xmm0, DWORD PTR [_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT] ;157.3
        lea       eax, DWORD PTR [160+esp]                      ;157.3
        mov       DWORD PTR [48+esp], 0                         ;157.3
        movss     DWORD PTR [160+esp], xmm0                     ;157.3
        push      32                                            ;157.3
        push      OFFSET FLAT: DYNUSTMAIN$format_pack.0.1       ;157.3
        push      eax                                           ;157.3
        push      OFFSET FLAT: __STRLITPACK_59.0.1              ;157.3
        push      -2088435968                                   ;157.3
        push      9999                                          ;157.3
        push      esi                                           ;157.3
        call      _for_write_seq_fmt                            ;157.3
                                ; LOE ebx esi edi
.B1.44:                         ; Preds .B1.43
        mov       DWORD PTR [76+esp], edi                       ;158.9
        push      32                                            ;158.9
        push      edi                                           ;158.9
        push      OFFSET FLAT: __STRLITPACK_60.0.1              ;158.9
        push      -2088435968                                   ;158.9
        push      9999                                          ;158.9
        push      esi                                           ;158.9
        call      _for_close                                    ;158.9
                                ; LOE ebx esi edi
.B1.45:                         ; Preds .B1.44
        push      60                                            ;161.11
        push      OFFSET FLAT: __STRLITPACK_39                  ;161.11
        call      _SYSTEM                                       ;161.11
                                ; LOE ebx esi edi
.B1.46:                         ; Preds .B1.45
        push      2                                             ;163.15
        call      __QWINSetExit                                 ;163.15
                                ; LOE ebx esi edi
.B1.47:                         ; Preds .B1.46
        lea       eax, DWORD PTR [208+esp]                      ;165.11
        push      eax                                           ;165.11
        call      _for_cpusec                                   ;165.11
                                ; LOE ebx esi edi
.B1.48:                         ; Preds .B1.47
        push      OFFSET FLAT: _DYNUSTMAIN$TA.0.1               ;167.16
        call      _DTIME                                        ;167.16
                                ; LOE ebx esi edi f1
.B1.86:                         ; Preds .B1.48
        add       esp, 120                                      ;167.16
        fstp      DWORD PTR [80+esp]                            ;167.16
                                ; LOE ebx esi edi
.B1.49:                         ; Preds .B1.86
        mov       DWORD PTR [esp], edi                          ;169.6
        push      32                                            ;169.6
        push      edi                                           ;169.6
        push      OFFSET FLAT: __STRLITPACK_61.0.1              ;169.6
        push      -2088435968                                   ;169.6
        push      180                                           ;169.6
        push      esi                                           ;169.6
        call      _for_write_seq_lis                            ;169.6
                                ; LOE ebx esi
.B1.50:                         ; Preds .B1.49
        movss     xmm0, DWORD PTR [120+esp]                     ;170.6
        lea       eax, DWORD PTR [144+esp]                      ;170.6
        movss     xmm1, DWORD PTR [108+esp]                     ;170.6
        movss     DWORD PTR [128+esp], xmm0                     ;170.6
        subss     xmm0, xmm1                                    ;170.6
        mov       DWORD PTR [24+esp], 0                         ;170.6
        movss     DWORD PTR [132+esp], xmm1                     ;170.6
        movss     DWORD PTR [124+esp], xmm0                     ;170.6
        movss     DWORD PTR [144+esp], xmm0                     ;170.6
        push      32                                            ;170.6
        push      OFFSET FLAT: DYNUSTMAIN$format_pack.0.1+20    ;170.6
        push      eax                                           ;170.6
        push      OFFSET FLAT: __STRLITPACK_62.0.1              ;170.6
        push      -2088435968                                   ;170.6
        push      180                                           ;170.6
        push      esi                                           ;170.6
        call      _for_write_seq_fmt                            ;170.6
                                ; LOE ebx esi
.B1.51:                         ; Preds .B1.50
        movss     xmm0, DWORD PTR [208+esp]                     ;171.6
        lea       eax, DWORD PTR [180+esp]                      ;171.6
        mov       DWORD PTR [52+esp], 0                         ;171.6
        movss     DWORD PTR [180+esp], xmm0                     ;171.6
        push      32                                            ;171.6
        push      OFFSET FLAT: DYNUSTMAIN$format_pack.0.1+92    ;171.6
        push      eax                                           ;171.6
        push      OFFSET FLAT: __STRLITPACK_63.0.1              ;171.6
        push      -2088435968                                   ;171.6
        push      180                                           ;171.6
        push      esi                                           ;171.6
        call      _for_write_seq_fmt                            ;171.6
                                ; LOE ebx esi
.B1.52:                         ; Preds .B1.51
        movss     xmm0, DWORD PTR [236+esp]                     ;171.119
        lea       eax, DWORD PTR [216+esp]                      ;171.6
        divss     xmm0, DWORD PTR [180+esp]                     ;171.119
        mulss     xmm0, DWORD PTR [_2il0floatpacket.1]          ;171.6
        movss     DWORD PTR [216+esp], xmm0                     ;171.6
        push      eax                                           ;171.6
        push      OFFSET FLAT: __STRLITPACK_64.0.1              ;171.6
        push      esi                                           ;171.6
        call      _for_write_seq_fmt_xmit                       ;171.6
                                ; LOE ebx esi
.B1.53:                         ; Preds .B1.52
        movss     xmm0, DWORD PTR [224+esp]                     ;172.6
        lea       eax, DWORD PTR [236+esp]                      ;172.6
        mov       DWORD PTR [92+esp], 0                         ;172.6
        movss     DWORD PTR [236+esp], xmm0                     ;172.6
        push      32                                            ;172.6
        push      OFFSET FLAT: DYNUSTMAIN$format_pack.0.1+192   ;172.6
        push      eax                                           ;172.6
        push      OFFSET FLAT: __STRLITPACK_65.0.1              ;172.6
        push      -2088435968                                   ;172.6
        push      180                                           ;172.6
        push      esi                                           ;172.6
        call      _for_write_seq_fmt                            ;172.6
                                ; LOE ebx esi
.B1.87:                         ; Preds .B1.53
        add       esp, 120                                      ;172.6
                                ; LOE ebx esi
.B1.54:                         ; Preds .B1.87
        movss     xmm0, DWORD PTR [132+esp]                     ;172.119
        lea       eax, DWORD PTR [152+esp]                      ;172.6
        divss     xmm0, DWORD PTR [100+esp]                     ;172.119
        mulss     xmm0, DWORD PTR [_2il0floatpacket.1]          ;172.6
        movss     DWORD PTR [152+esp], xmm0                     ;172.6
        push      eax                                           ;172.6
        push      OFFSET FLAT: __STRLITPACK_66.0.1              ;172.6
        push      esi                                           ;172.6
        call      _for_write_seq_fmt_xmit                       ;172.6
                                ; LOE ebx esi
.B1.55:                         ; Preds .B1.54
        movss     xmm0, DWORD PTR [116+esp]                     ;173.6
        lea       eax, DWORD PTR [172+esp]                      ;173.6
        mov       DWORD PTR [12+esp], 0                         ;173.6
        subss     xmm0, DWORD PTR [168+esp]                     ;173.6
        subss     xmm0, DWORD PTR [144+esp]                     ;173.6
        subss     xmm0, DWORD PTR [120+esp]                     ;173.105
        movss     DWORD PTR [116+esp], xmm0                     ;173.105
        movss     DWORD PTR [172+esp], xmm0                     ;173.6
        push      32                                            ;173.6
        push      OFFSET FLAT: DYNUSTMAIN$format_pack.0.1+292   ;173.6
        push      eax                                           ;173.6
        push      OFFSET FLAT: __STRLITPACK_67.0.1              ;173.6
        push      -2088435968                                   ;173.6
        push      180                                           ;173.6
        push      esi                                           ;173.6
        call      _for_write_seq_fmt                            ;173.6
                                ; LOE ebx esi
.B1.56:                         ; Preds .B1.55
        movss     xmm1, DWORD PTR [144+esp]                     ;173.182
        lea       eax, DWORD PTR [208+esp]                      ;173.6
        divss     xmm1, DWORD PTR [140+esp]                     ;173.182
        movss     xmm0, DWORD PTR [_2il0floatpacket.1]          ;173.6
        mulss     xmm0, xmm1                                    ;173.6
        movss     DWORD PTR [208+esp], xmm0                     ;173.6
        push      eax                                           ;173.6
        push      OFFSET FLAT: __STRLITPACK_68.0.1              ;173.6
        push      esi                                           ;173.6
        call      _for_write_seq_fmt_xmit                       ;173.6
                                ; LOE ebx esi
.B1.57:                         ; Preds .B1.56
        fld       DWORD PTR [132+esp]                           ;174.6
        lea       eax, DWORD PTR [156+esp]                      ;174.6
        mov       DWORD PTR [52+esp], 0                         ;174.6
        fstp      DWORD PTR [156+esp]                           ;174.6
        push      32                                            ;174.6
        push      OFFSET FLAT: DYNUSTMAIN$format_pack.0.1+392   ;174.6
        push      eax                                           ;174.6
        push      OFFSET FLAT: __STRLITPACK_69.0.1              ;174.6
        push      -2088435968                                   ;174.6
        push      180                                           ;174.6
        push      esi                                           ;174.6
        call      _for_write_seq_fmt                            ;174.6
                                ; LOE ebx esi
.B1.58:                         ; Preds .B1.57
        mov       eax, DWORD PTR [_DYNUSTMAIN$TA.0.1]           ;175.6
        lea       edx, DWORD PTR [160+esp]                      ;175.6
        mov       DWORD PTR [80+esp], 0                         ;175.6
        mov       DWORD PTR [160+esp], eax                      ;175.6
        push      32                                            ;175.6
        push      OFFSET FLAT: DYNUSTMAIN$format_pack.0.1+480   ;175.6
        push      edx                                           ;175.6
        push      OFFSET FLAT: __STRLITPACK_70.0.1              ;175.6
        push      -2088435968                                   ;175.6
        push      180                                           ;175.6
        push      esi                                           ;175.6
        call      _for_write_seq_fmt                            ;175.6
                                ; LOE ebx esi
.B1.59:                         ; Preds .B1.58
        mov       eax, DWORD PTR [_DYNUSTMAIN$TA.0.1+4]         ;175.6
        lea       edx, DWORD PTR [284+esp]                      ;175.6
        mov       DWORD PTR [284+esp], eax                      ;175.6
        push      edx                                           ;175.6
        push      OFFSET FLAT: __STRLITPACK_71.0.1              ;175.6
        push      esi                                           ;175.6
        call      _for_write_seq_fmt_xmit                       ;175.6
                                ; LOE ebx
.B1.60:                         ; Preds .B1.59
        mov       eax, 1                                        ;176.6
        add       esp, 368                                      ;176.6
        pop       edi                                           ;176.6
        pop       esi                                           ;176.6
        mov       esp, ebp                                      ;176.6
        pop       ebp                                           ;176.6
        ret                                                     ;176.6
                                ; LOE
.B1.62:                         ; Preds .B1.15                  ; Infreq
        call      _MEMORY_MODULE_mp_DEALLOCATE_SP1              ;98.14
                                ; LOE esi edi
.B1.63:                         ; Preds .B1.62                  ; Infreq
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_CALLKSP1], 0 ;100.9
        jmp       .B1.16        ; Prob 100%                     ;100.9
                                ; LOE esi edi
.B1.64:                         ; Preds .B1.14                  ; Infreq
        movss     xmm0, DWORD PTR [_2il0floatpacket.2]          ;87.41
        comiss    xmm0, DWORD PTR [_DYNUST_MAIN_MODULE_mp_VHPCEFACTOR] ;87.41
        jbe       .B1.15        ; Prob 50%                      ;87.41
                                ; LOE esi edi
.B1.65:                         ; Preds .B1.64                  ; Infreq
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_JUSTVEH] ;88.8
        test      eax, eax                                      ;88.8
        jle       .B1.15        ; Prob 2%                       ;88.8
                                ; LOE eax esi edi
.B1.66:                         ; Preds .B1.65                  ; Infreq
        mov       ebx, 1                                        ;
        mov       edi, eax                                      ;
                                ; LOE ebx esi edi
.B1.67:                         ; Preds .B1.68 .B1.66           ; Infreq
        push      OFFSET FLAT: __NLITPACK_1.0.1                 ;89.14
        call      _RANXY                                        ;89.14
                                ; LOE ebx esi edi f1
.B1.89:                         ; Preds .B1.67                  ; Infreq
        fstp      st(0)                                         ;
        add       esp, 4                                        ;89.14
                                ; LOE ebx esi edi
.B1.68:                         ; Preds .B1.89                  ; Infreq
        inc       ebx                                           ;93.8
        cmp       ebx, edi                                      ;93.8
        jle       .B1.67        ; Prob 82%                      ;93.8
                                ; LOE ebx esi edi
.B1.69:                         ; Preds .B1.68                  ; Infreq
        xor       edi, edi                                      ;
        jmp       .B1.15        ; Prob 100%                     ;
                                ; LOE esi edi
.B1.91:                         ; Preds .B1.4                   ; Infreq
        lea       esi, DWORD PTR [esp]                          ;75.18
        jmp       .B1.6         ; Prob 100%                     ;75.18
                                ; LOE ebx esi edi
.B1.92:                         ; Preds .B1.16                  ; Infreq
        lea       ebx, DWORD PTR [140+esp]                      ;124.14
        jmp       .B1.33        ; Prob 100%                     ;124.14
        ALIGN     16
                                ; LOE ebx esi edi
; mark_end;
_MAIN__ ENDP
_TEXT	ENDS
_BSS	SEGMENT  DWORD PUBLIC FLAT  'BSS'
	ALIGN 004H
_DYNUSTMAIN$PRINTSTR1.0.1	DB ?	; pad
	ORG $+78	; pad
	DB ?	; pad
_DYNUSTMAIN$TA.0.1	DD 2 DUP (0H)	; pad
_DYNUSTMAIN$MAXINTERVALS.0.1	DD 1 DUP (0H)	; pad
_BSS	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
DYNUSTMAIN$format_pack.0.1	DB	54
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	1
	DB	1
	DB	0
	DB	0
	DB	0
	DB	8
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	31
	DB	0
	DB	32
	DB	67
	DB	80
	DB	85
	DB	32
	DB	84
	DB	105
	DB	109
	DB	101
	DB	115
	DB	32
	DB	111
	DB	102
	DB	32
	DB	111
	DB	112
	DB	101
	DB	114
	DB	97
	DB	116
	DB	105
	DB	111
	DB	110
	DB	32
	DB	119
	DB	101
	DB	114
	DB	101
	DB	32
	DB	32
	DB	32
	DB	0
	DB	33
	DB	0
	DB	0
	DB	2
	DB	1
	DB	0
	DB	0
	DB	0
	DB	12
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	10
	DB	0
	DB	32
	DB	115
	DB	101
	DB	99
	DB	111
	DB	110
	DB	100
	DB	115
	DB	32
	DB	32
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	31
	DB	0
	DB	32
	DB	67
	DB	80
	DB	85
	DB	32
	DB	84
	DB	105
	DB	109
	DB	101
	DB	115
	DB	32
	DB	111
	DB	102
	DB	32
	DB	115
	DB	105
	DB	109
	DB	117
	DB	108
	DB	97
	DB	116
	DB	105
	DB	111
	DB	110
	DB	32
	DB	119
	DB	101
	DB	114
	DB	101
	DB	32
	DB	32
	DB	0
	DB	33
	DB	0
	DB	0
	DB	2
	DB	1
	DB	0
	DB	0
	DB	0
	DB	12
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	10
	DB	0
	DB	32
	DB	115
	DB	101
	DB	99
	DB	111
	DB	110
	DB	100
	DB	115
	DB	32
	DB	32
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	2
	DB	1
	DB	0
	DB	0
	DB	0
	DB	6
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	12
	DB	0
	DB	32
	DB	37
	DB	32
	DB	111
	DB	102
	DB	32
	DB	116
	DB	111
	DB	116
	DB	97
	DB	108
	DB	41
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	31
	DB	0
	DB	32
	DB	67
	DB	80
	DB	85
	DB	32
	DB	84
	DB	105
	DB	109
	DB	101
	DB	115
	DB	32
	DB	111
	DB	102
	DB	32
	DB	97
	DB	115
	DB	115
	DB	105
	DB	103
	DB	110
	DB	109
	DB	101
	DB	110
	DB	116
	DB	32
	DB	119
	DB	101
	DB	114
	DB	101
	DB	32
	DB	32
	DB	0
	DB	33
	DB	0
	DB	0
	DB	2
	DB	1
	DB	0
	DB	0
	DB	0
	DB	12
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	10
	DB	0
	DB	32
	DB	115
	DB	101
	DB	99
	DB	111
	DB	110
	DB	100
	DB	115
	DB	32
	DB	32
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	2
	DB	1
	DB	0
	DB	0
	DB	0
	DB	6
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	12
	DB	0
	DB	32
	DB	37
	DB	32
	DB	111
	DB	102
	DB	32
	DB	116
	DB	111
	DB	116
	DB	97
	DB	108
	DB	41
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	31
	DB	0
	DB	32
	DB	67
	DB	80
	DB	85
	DB	32
	DB	84
	DB	105
	DB	109
	DB	101
	DB	115
	DB	32
	DB	111
	DB	102
	DB	32
	DB	111
	DB	116
	DB	104
	DB	101
	DB	114
	DB	115
	DB	32
	DB	119
	DB	101
	DB	114
	DB	101
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	0
	DB	33
	DB	0
	DB	0
	DB	2
	DB	1
	DB	0
	DB	0
	DB	0
	DB	12
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	10
	DB	0
	DB	32
	DB	115
	DB	101
	DB	99
	DB	111
	DB	110
	DB	100
	DB	115
	DB	32
	DB	32
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	2
	DB	1
	DB	0
	DB	0
	DB	0
	DB	6
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	12
	DB	0
	DB	32
	DB	37
	DB	32
	DB	111
	DB	102
	DB	32
	DB	116
	DB	111
	DB	116
	DB	97
	DB	108
	DB	41
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	39
	DB	0
	DB	32
	DB	67
	DB	80
	DB	85
	DB	32
	DB	84
	DB	105
	DB	109
	DB	101
	DB	32
	DB	111
	DB	102
	DB	32
	DB	111
	DB	112
	DB	101
	DB	114
	DB	97
	DB	116
	DB	105
	DB	111
	DB	110
	DB	32
	DB	119
	DB	97
	DB	115
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	0
	DB	33
	DB	0
	DB	0
	DB	2
	DB	1
	DB	0
	DB	0
	DB	0
	DB	12
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	17
	DB	0
	DB	32
	DB	115
	DB	101
	DB	99
	DB	111
	DB	110
	DB	100
	DB	115
	DB	32
	DB	98
	DB	121
	DB	32
	DB	68
	DB	84
	DB	105
	DB	109
	DB	101
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	39
	DB	0
	DB	32
	DB	85
	DB	115
	DB	101
	DB	114
	DB	32
	DB	97
	DB	110
	DB	100
	DB	32
	DB	83
	DB	121
	DB	115
	DB	116
	DB	101
	DB	109
	DB	32
	DB	84
	DB	105
	DB	109
	DB	101
	DB	32
	DB	111
	DB	102
	DB	32
	DB	111
	DB	112
	DB	101
	DB	114
	DB	97
	DB	116
	DB	105
	DB	111
	DB	110
	DB	32
	DB	119
	DB	97
	DB	115
	DB	32
	DB	0
	DB	33
	DB	0
	DB	0
	DB	2
	DB	2
	DB	0
	DB	0
	DB	0
	DB	12
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	17
	DB	0
	DB	32
	DB	115
	DB	101
	DB	99
	DB	111
	DB	110
	DB	100
	DB	115
	DB	32
	DB	98
	DB	121
	DB	32
	DB	68
	DB	84
	DB	105
	DB	109
	DB	101
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
_DATA	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
__NLITPACK_0.0.1	DD	0
__STRLITPACK_53.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__NLITPACK_1.0.1	DD	16
__STRLITPACK_54.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_55.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__NLITPACK_3.0.1	DD	395
__NLITPACK_2.0.1	DD	20
__STRLITPACK_56.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_57.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_58.0.1	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	26
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_59.0.1	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_60.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_61.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_62.0.1	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_63.0.1	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_64.0.1	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_65.0.1	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_66.0.1	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_67.0.1	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_68.0.1	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_69.0.1	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_70.0.1	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_71.0.1	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _MAIN__
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_51	DB	67
	DB	65
	DB	76
	DB	76
	DB	32
	DB	68
	DB	121
	DB	110
	DB	117
	DB	115
	DB	84
	DB	0
__STRLITPACK_49	DB	85
	DB	69
	DB	45
	DB	75
	DB	83
	DB	80
	DB	32
	DB	46
	DB	46
	DB	46
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_47	DB	85
	DB	69
	DB	95
	DB	108
	DB	111
	DB	118
	DB	32
	DB	97
	DB	115
	DB	115
	DB	105
	DB	103
	DB	110
	DB	109
	DB	101
	DB	110
	DB	116
	DB	32
	DB	46
	DB	46
	DB	46
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_46	DB	68
	DB	121
	DB	110
	DB	97
	DB	109
	DB	105
	DB	99
	DB	32
	DB	85
	DB	69
	DB	32
	DB	65
	DB	115
	DB	115
	DB	105
	DB	103
	DB	110
	DB	109
	DB	101
	DB	110
	DB	116
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_44	DB	85
	DB	69
	DB	95
	DB	104
	DB	111
	DB	118
	DB	32
	DB	97
	DB	115
	DB	115
	DB	105
	DB	103
	DB	110
	DB	109
	DB	101
	DB	110
	DB	116
	DB	32
	DB	46
	DB	46
	DB	46
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_43	DB	83
	DB	105
	DB	109
	DB	80
	DB	101
	DB	114
	DB	105
	DB	111
	DB	100
	DB	46
	DB	111
	DB	112
	DB	116
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_42	DB	117
	DB	110
	DB	107
	DB	110
	DB	111
	DB	119
	DB	110
	DB	0
__STRLITPACK_39	DB	100
	DB	101
	DB	108
	DB	32
	DB	46
	DB	92
	DB	101
	DB	120
	DB	101
	DB	99
	DB	117
	DB	116
	DB	105
	DB	110
	DB	103
	DB	32
	DB	98
	DB	105
	DB	110
	DB	112
	DB	97
	DB	116
	DB	104
	DB	46
	DB	98
	DB	105
	DB	112
	DB	32
	DB	98
	DB	105
	DB	110
	DB	108
	DB	97
	DB	98
	DB	101
	DB	108
	DB	46
	DB	98
	DB	105
	DB	112
	DB	32
	DB	102
	DB	111
	DB	114
	DB	116
	DB	46
	DB	54
	DB	54
	DB	54
	DB	54
	DB	32
	DB	102
	DB	111
	DB	114
	DB	116
	DB	46
	DB	54
	DB	54
	DB	54
	DB	55
	DB	0
	DB 3 DUP ( 0H)	; pad
_2il0floatpacket.0	DD	038e6afcdH
_2il0floatpacket.1	DD	042c80000H
_2il0floatpacket.2	DD	03f800000H
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
EXTRN	_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_REACH_CONVERG:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_JTOTAL:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TOTAL_HOV:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NOOFPS:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_CALLKSP1:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_JUSTVEH:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_VHPCEFACTOR:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TOTALVIOLATION:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_IUE_OK:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ISO_OK:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ITEMAX:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_LOGOUT:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_SOINT:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ITERATION:BYTE
_DATA	ENDS
EXTRN	_for_write_seq_fmt_xmit:PROC
EXTRN	__QWINSetExit:PROC
EXTRN	_for_write_seq_fmt:PROC
EXTRN	_for_open:PROC
EXTRN	_for_close:PROC
EXTRN	_for_cpystr:PROC
EXTRN	__rectangle:PROC
EXTRN	__setcolorrgb:PROC
EXTRN	_for_write_seq_lis:PROC
EXTRN	_for_cpusec:PROC
EXTRN	_for_set_reentrancy:PROC
EXTRN	_MEMORY_MODULE_mp_DEALLOCATE_SP1:PROC
EXTRN	_PRINTSCREEN:PROC
EXTRN	_SIM_DYNUST:PROC
EXTRN	_MIVAGFV:PROC
EXTRN	_WINSETUP:PROC
EXTRN	_CLOSEFILE:PROC
EXTRN	_RANXY:PROC
EXTRN	_OPENFILE:PROC
EXTRN	_SYSTEM:PROC
EXTRN	_DTIME:PROC
EXTRN	___intel_new_proc_init:PROC
EXTRN	__fltused:BYTE
	INCLUDELIB <ifconsol>
	INCLUDELIB <libifcoremt>
	INCLUDELIB <libifport>
	INCLUDELIB <libmmt>
	INCLUDELIB <LIBCMT>
	INCLUDELIB <libirc>
	INCLUDELIB <svml_dispmt>
	INCLUDELIB <OLDNAMES>
	END
