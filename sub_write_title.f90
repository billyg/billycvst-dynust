      SUBROUTINE WRITE_TITLE()
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~ Copyright (C) 2002-2011 The Arizona Board of Regents on Behalf of The University of Arizona.  ~~!
!~~ Developed by Yi-Chang Chiu, Ph.D. as par to the DynusT project.                               ~~!
!~~ This program is free software: you can redistribute it and/or modify it                       ~~!
!~~ under the terms of the GNU General Public License version 2 as published by                   ~~!
!~~ the Free Software Foundation.                                                                 ~~!
!~~ This program is distributed in the hope that it will be useful,                               ~~!
!~~ but WITHOUT ANY WARRANTY; without even the implied warranty of                                ~~!
!~~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                                          ~~!
!~~ See the GNU General Public License for more details.                                          ~~!
!~~ You should have received a copy of the GNU General Public License                             ~~!
!~~ along with this program (in the file COPYING).                                                ~~!
!~~ IF not, see <http://www.gnu.org/licenses/>.                                                   ~~!
!~~ Please see http://www.dynust.net for contact information and names                            ~~!
!~~ of individual contributors.                                                                   ~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
    USE DYNUST_MAIN_MODULE
!> > > > > > > > > >                < < < < < < < < < < <
!>>>>>>>>>>>>>>>>>>>CODES START HERE<<<<<<<<<<<<<<<<<<<<<
!> > > > > > > > > >                < < < < < < < < < < <
    
      WRITE(666,*)
      WRITE(666,'(11x,"==========================================================")')
      WRITE(666,'(11x,"==========================================================")')
      WRITE(666,'(11x,"H                        DynusT                          H")')
	  WRITE(666,'(11x,"H                                                        H")')
	  WRITE(666,'(11x,"H       Dynamic Urban Systems for Transportation         H")')
	  WRITE(666,'(11x,"H                                                        H")')
	  WRITE(666,'(11x,"H                  Version (2.0.1 Beta)                  H")')
	  WRITE(666,'(11x,"H                                                        H")')
	  WRITE(666,'(11x,"H                                                        H")')
	  WRITE(666,'(11x,"H      Released by: Federal Highway Administration       H")')
	  WRITE(666,'(11x,"H                Copyright: Yi-Chang Chiu                H")')	  
	  WRITE(666,'(11x,"H                                                        H")')
	  WRITE(666,'(11x,"H         Scheduled Release Date: October, 2009          H")')
	  WRITE(666,'(11x,"H                                                        H")')
      WRITE(666,'(11x,"==========================================================")')
      WRITE(666,'(11x,"==========================================================")')

      IF(i18 == 1.or.reach_converg) THEN
      WRITE(18,181)
      WRITE(18,182)
      WRITE(18,183)
181   FORMAT(/,'****  Output file for vehicles tranjectories ****')
182   FORMAT(  '=================================================')
183   FORMAT(  'This file provides all the vehicles trajectories')
      WRITE(18,*)
      ENDIF

      IF(i30 == 1) THEN
      WRITE(30,*) '******  Output file for vehicle generation ******'
      WRITE(30,*) '================================================='
      WRITE(30,*) 'This file provides the average number of vehicles'
      WRITE(30,*) 'per sim. int., averaged over',i30_t,'sim. int.'
      WRITE(30,*)
      ENDIF

      IF(i31 == 1) THEN
      WRITE(31,*) '******  Output file for link volumes       ******'
      WRITE(31,*) '================================================='
      WRITE(31,*) 'This file provides the average number of vehicles'
      WRITE(31,*) 'per sim. int. averaged over',i31_t, 'sim. int.'
      WRITE(31,*)
      ENDIF

      IF(i32 == 1) THEN
      WRITE(32,*) '******  Output file for vehicle queue      ******'
      WRITE(32,*) '================================================='
      WRITE(32,*) 'This file provides the average number of vehicles'
      WRITE(32,*) 'in the queues on links per sim. int. averaged over',i32_t ,'sim. int.'
      WRITE(32,*)
      ENDIF

      IF(i33 == 1) THEN
      WRITE(33,*) '******  Output file for link speed         ******'
      WRITE(33,*) '================================================='
      WRITE(33,*) 'This file provides the average speed  '
      WRITE(33,*) 'on links per sim. int. averaged over ',i33_t,'sim. int.'
      WRITE(33,*)
      ENDIF

      IF(i34 == 1) THEN
      WRITE(34,*) '******  Output file for link density      ******'
      WRITE(34,*) '================================================='
      WRITE(34,*) 'This file provides the average density  '
      WRITE(34,*) 'on links per sim. int. averaged over',i34_t, 'sim. int.'
      WRITE(34,*)
      ENDIF

      IF(i35 == 1) THEN
      WRITE(35,*) 'Speed on the queue-free portion of the link'
      WRITE(35,*) '================================================='
      WRITE(35,*) 'This file provides the average speed on' 
      WRITE(35,*) 'the queue-free portion on links every'
      WRITE(35,*) 'sim. int. averaged over', i35_t, 'sim. int.'
      WRITE(35,*)
      ENDIF

      IF(i36 == 1) THEN
      WRITE(36,*) 'Density on the queue-free portion of the link'
      WRITE(36,*) '================================================='
      WRITE(36,*) 'This file provides the average density on'
      WRITE(36,*) 'the queue-free portion on links per'
      WRITE(36,*) 'sim. int. averaged over',i36_t,'sim. int.'
      WRITE(36,*)
      ENDIF

      IF(i37 == 1) THEN
      WRITE(37,*) 'Output file for left turning out flow'
      WRITE(37,*) '================================================='
      WRITE(37,*) 'This file provides the average number of'
      WRITE(37,*) 'left turning vehicles on links per'
      WRITE(37,*) 'sim. int. averaged over',i37_t, 'sim. int.'
      WRITE(37,*)
      ENDIF

      IF(i38 == 1) THEN
      WRITE(38,*) 'Output file for green time '
      WRITE(38,*) '================================================='
      WRITE(38,*) 'This file provides the average green time'
      WRITE(38,*) 'for each link every per sim. int. averated over',i38_t, 'sim. int.'
      WRITE(38,*)
      ENDIF

      IF(i39 == 1) THEN
      WRITE(39,*) 'Output file for out flow '
      WRITE(39,*) '================================================='
      WRITE(39,*) 'This file provides the average number of vehicles'
      WRITE(39,*) 'out of each link per sim. int. averaged over',i39_t, 'sims ints'
      WRITE(39,*)
      ENDIF

      IF(i40 == 1) THEN
      IF((iteMax == 0.or.(iteMax > 0.and.(iteration == iteMax.or.reach_converg)))) THEN
      WRITE(400,*) 'Output file for accumulated volume '
      WRITE(400,*) '================================================='
      WRITE(400,*) 'This file provides the accummulated number of veh.'
      WRITE(400,*) 'on of each link           10 every sims ints'
      WRITE(400,*)

      WRITE(401,*) 'Output file for accumulated volume '
      WRITE(401,*) '================================================='
      WRITE(401,*) 'This file provides the accummulated number of veh.'
      WRITE(401,*) 'on of each link           10 every sims ints'
      WRITE(401,*)

      ENDIF
      ENDIF
      
END SUBROUTINE