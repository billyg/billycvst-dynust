; mark_description "Intel(R) Visual Fortran Compiler XE for applications running on IA-32, Version 13.1.1.171 Build 20130313";
; mark_description "-S";
	.686P
 	.387
	OPTION DOTNAME
	ASSUME	CS:FLAT,DS:FLAT,SS:FLAT
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
TXTST0:
; -- Begin  _SIGNAL_STOP
; mark_begin;
IF @Version GE 800
  .MMX
ELSEIF @Version GE 612
  .MMX
  MMWORD TEXTEQU <QWORD>
ENDIF
IF @Version GE 800
  .XMM
ELSEIF @Version GE 614
  .XMM
  XMMWORD TEXTEQU <OWORD>
ENDIF
       ALIGN     16
	PUBLIC _SIGNAL_STOP
_SIGNAL_STOP	PROC NEAR 
; parameter 1: 100 + esp
.B1.1:                          ; Preds .B1.0
        push      esi                                           ;1.18
        push      edi                                           ;1.18
        push      ebx                                           ;1.18
        push      ebp                                           ;1.18
        sub       esp, 80                                       ;1.18
        mov       ecx, DWORD PTR [100+esp]                      ;1.18
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_BACKPOINTR] ;29.7
        mov       edx, DWORD PTR [ecx]                          ;29.10
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_BACKPOINTR+32] ;29.7
        shl       ecx, 2                                        ;29.7
        lea       eax, DWORD PTR [eax+edx*4]                    ;29.7
        mov       ebx, eax                                      ;29.7
        sub       ebx, ecx                                      ;29.7
        neg       ecx                                           ;30.10
        mov       ebp, DWORD PTR [4+ecx+eax]                    ;30.10
        dec       ebp                                           ;30.7
        mov       ecx, ebp                                      ;30.7
        mov       ebx, DWORD PTR [ebx]                          ;29.7
        sub       ecx, ebx                                      ;30.7
        inc       ecx                                           ;30.7
        cmp       ebp, ebx                                      ;31.9
        jl        .B1.26        ; Prob 10%                      ;31.9
                                ; LOE ecx ebx
.B1.2:                          ; Preds .B1.1
        movss     xmm0, DWORD PTR [_2il0floatpacket.1]          ;34.102
        mov       ebp, ecx                                      ;
        mulss     xmm0, DWORD PTR [_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT] ;34.102
        cvttss2si eax, xmm0                                     ;34.11
        shr       ebp, 31                                       ;
        add       ebp, ecx                                      ;
        sar       ebp, 1                                        ;
        mov       DWORD PTR [76+esp], ebp                       ;
        imul      ebp, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32], 152 ;
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;32.5
        mov       DWORD PTR [36+esp], eax                       ;34.11
        mov       eax, esi                                      ;
        sub       eax, ebp                                      ;
        mov       DWORD PTR [56+esp], eax                       ;
        imul      eax, ebx, 152                                 ;
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+40] ;34.11
        add       eax, esi                                      ;
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+44] ;34.11
        sub       eax, ebp                                      ;
        imul      edi, edx                                      ;
        imul      esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32], -900 ;
        mov       DWORD PTR [60+esp], eax                       ;
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE] ;
        sub       eax, edi                                      ;
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+32] ;
        add       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;
        mov       DWORD PTR [40+esp], esi                       ;
        mov       esi, eax                                      ;
        mov       DWORD PTR [64+esp], 0                         ;
        lea       ebp, DWORD PTR [edi*4]                        ;
        shl       edi, 5                                        ;
        sub       edi, ebp                                      ;
        lea       ebp, DWORD PTR [eax+edx]                      ;
        sub       esi, edi                                      ;
        lea       eax, DWORD PTR [eax+edx*2]                    ;
        sub       ebp, edi                                      ;
        sub       eax, edi                                      ;
        mov       DWORD PTR [28+esp], edx                       ;
        mov       DWORD PTR [16+esp], eax                       ;
        mov       DWORD PTR [20+esp], ebp                       ;
        mov       DWORD PTR [24+esp], esi                       ;
        mov       DWORD PTR [48+esp], ecx                       ;
        mov       DWORD PTR [68+esp], ebx                       ;
        mov       edx, DWORD PTR [64+esp]                       ;
                                ; LOE edx
.B1.3:                          ; Preds .B1.24 .B1.2
        imul      ecx, edx, 152                                 ;32.5
        mov       eax, DWORD PTR [60+esp]                       ;32.5
        mov       esi, DWORD PTR [56+esp]                       ;34.11
        mov       eax, DWORD PTR [16+eax+ecx]                   ;32.5
        xor       ecx, ecx                                      ;33.11
        imul      ebp, eax, 152                                 ;32.5
        imul      ebx, eax, 900                                 ;32.5
        movsx     esi, BYTE PTR [32+ebp+esi]                    ;34.11
        test      esi, esi                                      ;34.11
        mov       DWORD PTR [44+esp], ebx                       ;32.5
        mov       ebx, ecx                                      ;33.11
        jle       .B1.10        ; Prob 50%                      ;34.11
                                ; LOE eax edx ecx ebx esi
.B1.4:                          ; Preds .B1.3
        mov       ebp, esi                                      ;34.11
        shr       ebp, 31                                       ;34.11
        add       ebp, esi                                      ;34.11
        sar       ebp, 1                                        ;34.11
        mov       DWORD PTR [52+esp], ebp                       ;34.11
        test      ebp, ebp                                      ;34.11
        jbe       .B1.27        ; Prob 10%                      ;34.11
                                ; LOE eax edx ecx ebx esi
.B1.5:                          ; Preds .B1.4
        mov       ebp, eax                                      ;32.5
        xor       edi, edi                                      ;
        mov       DWORD PTR [esp], esi                          ;
        lea       esi, DWORD PTR [eax*4]                        ;32.5
        shl       ebp, 5                                        ;32.5
        sub       ebp, esi                                      ;32.5
        mov       esi, DWORD PTR [20+esp]                       ;
        mov       DWORD PTR [4+esp], ebx                        ;
        mov       DWORD PTR [8+esp], ecx                        ;
        add       esi, ebp                                      ;
        add       ebp, DWORD PTR [16+esp]                       ;
        mov       DWORD PTR [32+esp], esi                       ;
        mov       DWORD PTR [12+esp], eax                       ;
        mov       DWORD PTR [64+esp], edx                       ;
        mov       eax, edi                                      ;
        mov       edx, ebp                                      ;
        mov       ecx, DWORD PTR [32+esp]                       ;
        mov       ebx, DWORD PTR [52+esp]                       ;
        mov       ebp, DWORD PTR [36+esp]                       ;
        mov       esi, DWORD PTR [28+esp]                       ;
                                ; LOE eax edx ecx ebx ebp esi edi
.B1.6:                          ; Preds .B1.6 .B1.5
        inc       edi                                           ;34.11
        mov       DWORD PTR [4+ecx+eax*2], ebp                  ;34.11
        mov       DWORD PTR [4+edx+eax*2], ebp                  ;34.11
        add       eax, esi                                      ;34.11
        cmp       edi, ebx                                      ;34.11
        jb        .B1.6         ; Prob 64%                      ;34.11
                                ; LOE eax edx ecx ebx ebp esi edi
.B1.7:                          ; Preds .B1.6
        mov       esi, DWORD PTR [esp]                          ;
        lea       ebp, DWORD PTR [1+edi+edi]                    ;34.11
        mov       ebx, DWORD PTR [4+esp]                        ;
        mov       ecx, DWORD PTR [8+esp]                        ;
        mov       eax, DWORD PTR [12+esp]                       ;
        mov       edx, DWORD PTR [64+esp]                       ;
                                ; LOE eax edx ecx ebx ebp esi
.B1.8:                          ; Preds .B1.7 .B1.27
        lea       edi, DWORD PTR [-1+ebp]                       ;34.11
        cmp       esi, edi                                      ;34.11
        jbe       .B1.10        ; Prob 10%                      ;34.11
                                ; LOE eax edx ecx ebx ebp
.B1.9:                          ; Preds .B1.8
        imul      ebp, DWORD PTR [28+esp]                       ;34.11
        lea       esi, DWORD PTR [eax*4]                        ;32.5
        shl       eax, 5                                        ;32.5
        sub       eax, esi                                      ;32.5
        add       eax, DWORD PTR [24+esp]                       ;34.11
        mov       edi, DWORD PTR [36+esp]                       ;34.11
        mov       DWORD PTR [4+ebp+eax], edi                    ;34.11
                                ; LOE edx ecx ebx
.B1.10:                         ; Preds .B1.3 .B1.8 .B1.9
        cmp       DWORD PTR [76+esp], 0                         ;35.4
        jbe       .B1.29        ; Prob 10%                      ;35.4
                                ; LOE edx ecx ebx
.B1.11:                         ; Preds .B1.10
        mov       eax, DWORD PTR [68+esp]                       ;
        xor       ebp, ebp                                      ;
        mov       DWORD PTR [64+esp], edx                       ;
        lea       esi, DWORD PTR [eax+edx]                      ;
        xor       eax, eax                                      ;
        mov       DWORD PTR [72+esp], esi                       ;
                                ; LOE eax ecx ebx ebp
.B1.12:                         ; Preds .B1.16 .B1.11
        lea       edx, DWORD PTR [ebp+ebp]                      ;36.15
        cmp       edx, DWORD PTR [64+esp]                       ;36.11
        je        .B1.14        ; Prob 62%                      ;36.11
                                ; LOE eax ecx ebx ebp
.B1.13:                         ; Preds .B1.12
        mov       edx, DWORD PTR [60+esp]                       ;36.20
        mov       esi, DWORD PTR [40+esp]                       ;36.106
        imul      edi, DWORD PTR [16+eax+edx], 900              ;36.20
        lea       edx, DWORD PTR [1+ecx]                        ;36.106
        cmp       DWORD PTR [400+esi+edi], 0                    ;36.106
        cmovg     ecx, edx                                      ;36.106
        cmovg     ebx, edx                                      ;36.106
                                ; LOE eax ecx ebx ebp
.B1.14:                         ; Preds .B1.13 .B1.12
        mov       edx, DWORD PTR [68+esp]                       ;36.15
        lea       esi, DWORD PTR [1+edx+ebp*2]                  ;36.15
        cmp       esi, DWORD PTR [72+esp]                       ;36.11
        je        .B1.16        ; Prob 62%                      ;36.11
                                ; LOE eax ecx ebx ebp
.B1.15:                         ; Preds .B1.14
        mov       edx, DWORD PTR [60+esp]                       ;36.20
        mov       esi, DWORD PTR [40+esp]                       ;36.106
        imul      edi, DWORD PTR [168+eax+edx], 900             ;36.20
        lea       edx, DWORD PTR [1+ecx]                        ;36.106
        cmp       DWORD PTR [400+esi+edi], 0                    ;36.106
        cmovg     ecx, edx                                      ;36.106
        cmovg     ebx, edx                                      ;36.106
                                ; LOE eax ecx ebx ebp
.B1.16:                         ; Preds .B1.15 .B1.14
        inc       ebp                                           ;35.4
        add       eax, 304                                      ;35.4
        cmp       ebp, DWORD PTR [76+esp]                       ;35.4
        jb        .B1.12        ; Prob 64%                      ;35.4
                                ; LOE eax ecx ebx ebp
.B1.17:                         ; Preds .B1.16
        mov       edx, DWORD PTR [64+esp]                       ;
        lea       ebp, DWORD PTR [1+ebp+ebp]                    ;35.4
                                ; LOE edx ecx ebx ebp
.B1.18:                         ; Preds .B1.17 .B1.29
        lea       eax, DWORD PTR [-1+ebp]                       ;35.4
        cmp       eax, DWORD PTR [48+esp]                       ;35.4
        jae       .B1.23        ; Prob 10%                      ;35.4
                                ; LOE eax edx ecx ebx ebp
.B1.19:                         ; Preds .B1.18
        cmp       edx, eax                                      ;36.11
        je        .B1.23        ; Prob 62%                      ;36.11
                                ; LOE edx ecx ebx ebp
.B1.20:                         ; Preds .B1.19
        imul      eax, ebp, 152                                 ;36.15
        mov       ebp, DWORD PTR [60+esp]                       ;36.20
        mov       edi, DWORD PTR [40+esp]                       ;36.101
        imul      esi, DWORD PTR [-136+eax+ebp], 900            ;36.20
        cmp       DWORD PTR [400+esi+edi], 0                    ;36.101
        jle       .B1.22        ; Prob 16%                      ;36.101
                                ; LOE edx ecx ebx edi
.B1.21:                         ; Preds .B1.20
        mov       ebx, edi                                      ;36.106
        inc       ecx                                           ;36.106
        mov       eax, DWORD PTR [44+esp]                       ;36.106
        mov       DWORD PTR [424+eax+ebx], ecx                  ;36.106
        jmp       .B1.24        ; Prob 100%                     ;36.106
                                ; LOE edx
.B1.22:                         ; Preds .B1.20
        mov       ecx, edi                                      ;36.106
        mov       eax, DWORD PTR [44+esp]                       ;36.106
        mov       DWORD PTR [424+eax+ecx], ebx                  ;36.106
        jmp       .B1.24        ; Prob 100%                     ;36.106
                                ; LOE edx
.B1.23:                         ; Preds .B1.18 .B1.19
        mov       ecx, DWORD PTR [40+esp]                       ;36.106
        mov       eax, DWORD PTR [44+esp]                       ;36.106
        mov       DWORD PTR [424+eax+ecx], ebx                  ;36.106
                                ; LOE edx
.B1.24:                         ; Preds .B1.21 .B1.22 .B1.23
        inc       edx                                           ;31.9
        cmp       edx, DWORD PTR [48+esp]                       ;31.9
        jb        .B1.3         ; Prob 82%                      ;31.9
                                ; LOE edx
.B1.26:                         ; Preds .B1.24 .B1.1
        add       esp, 80                                       ;40.7
        pop       ebp                                           ;40.7
        pop       ebx                                           ;40.7
        pop       edi                                           ;40.7
        pop       esi                                           ;40.7
        ret                                                     ;40.7
                                ; LOE
.B1.27:                         ; Preds .B1.4                   ; Infreq
        mov       ebp, 1                                        ;
        jmp       .B1.8         ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx ebp esi
.B1.29:                         ; Preds .B1.10                  ; Infreq
        mov       ebp, 1                                        ;
        jmp       .B1.18        ; Prob 100%                     ;
        ALIGN     16
                                ; LOE edx ecx ebx ebp
; mark_end;
_SIGNAL_STOP ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _SIGNAL_STOP
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
_2il0floatpacket.1	DD	042700000H
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
EXTRN	_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_BACKPOINTR:BYTE
_DATA	ENDS
EXTRN	__fltused:BYTE
	INCLUDELIB <ifconsol>
	INCLUDELIB <libifcoremt>
	INCLUDELIB <libifport>
	INCLUDELIB <libmmt>
	INCLUDELIB <LIBCMT>
	INCLUDELIB <libirc>
	INCLUDELIB <svml_dispmt>
	INCLUDELIB <OLDNAMES>
	END
