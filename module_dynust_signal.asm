; mark_description "Intel(R) Visual Fortran Compiler XE for applications running on IA-32, Version 13.1.1.171 Build 20130313";
; mark_description "-S";
	.686P
 	.387
	OPTION DOTNAME
	ASSUME	CS:FLAT,DS:FLAT,SS:FLAT
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
TXTST0:
; -- Begin  _DYNUST_SIGNAL_MODULE$
; mark_begin;
IF @Version GE 800
  .MMX
ELSEIF @Version GE 612
  .MMX
  MMWORD TEXTEQU <QWORD>
ENDIF
IF @Version GE 800
  .XMM
ELSEIF @Version GE 614
  .XMM
  XMMWORD TEXTEQU <OWORD>
ENDIF
       ALIGN     16
	PUBLIC _DYNUST_SIGNAL_MODULE$
_DYNUST_SIGNAL_MODULE$	PROC NEAR 
.B1.1:                          ; Preds .B1.0
        ret                                                     ;1.8
        ALIGN     16
                                ; LOE
; mark_end;
_DYNUST_SIGNAL_MODULE$ ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_SIGNAL_MODULE$
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_SIGNAL_MODULE_mp_DECLARESIGNAL
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_SIGNAL_MODULE_mp_DECLARESIGNAL
_DYNUST_SIGNAL_MODULE_mp_DECLARESIGNAL	PROC NEAR 
; parameter 1: 8 + ebp
.B2.1:                          ; Preds .B2.0
        push      ebp                                           ;60.12
        mov       ebp, esp                                      ;60.12
        and       esp, -16                                      ;60.12
        push      esi                                           ;60.12
        push      edi                                           ;60.12
        push      ebx                                           ;60.12
        sub       esp, 52                                       ;60.12
        xor       edx, edx                                      ;63.5
        mov       eax, DWORD PTR [8+ebp]                        ;60.12
        lea       ecx, DWORD PTR [48+esp]                       ;63.5
        push      48                                            ;63.5
        mov       ebx, DWORD PTR [eax]                          ;63.5
        test      ebx, ebx                                      ;63.5
        cmovl     ebx, edx                                      ;63.5
        push      ebx                                           ;63.5
        push      2                                             ;63.5
        push      ecx                                           ;63.5
        call      _for_check_mult_overflow                      ;63.5
                                ; LOE eax ebx
.B2.2:                          ; Preds .B2.1
        mov       edx, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY+12] ;63.5
        and       eax, 1                                        ;63.5
        and       edx, 1                                        ;63.5
        add       edx, edx                                      ;63.5
        shl       eax, 4                                        ;63.5
        or        edx, 1                                        ;63.5
        or        edx, eax                                      ;63.5
        or        edx, 262144                                   ;63.5
        push      edx                                           ;63.5
        push      OFFSET FLAT: _DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY ;63.5
        push      DWORD PTR [72+esp]                            ;63.5
        call      _for_alloc_allocatable                        ;63.5
                                ; LOE eax ebx
.B2.40:                         ; Preds .B2.2
        add       esp, 28                                       ;63.5
                                ; LOE eax ebx
.B2.3:                          ; Preds .B2.40
        test      eax, eax                                      ;63.5
        jne       .B2.5         ; Prob 50%                      ;63.5
                                ; LOE ebx
.B2.4:                          ; Preds .B2.3
        mov       edx, 48                                       ;63.5
        lea       ecx, DWORD PTR [40+esp]                       ;63.5
        push      edx                                           ;63.5
        push      ebx                                           ;63.5
        push      2                                             ;63.5
        push      ecx                                           ;63.5
        mov       eax, 1                                        ;63.5
        mov       DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY+12], 133 ;63.5
        mov       DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY+4], edx ;63.5
        mov       DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY+16], eax ;63.5
        mov       DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY+8], 0 ;63.5
        mov       DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY+32], eax ;63.5
        mov       DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY+24], ebx ;63.5
        mov       DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY+28], edx ;63.5
        call      _for_check_mult_overflow                      ;63.5
                                ; LOE
.B2.41:                         ; Preds .B2.4
        add       esp, 16                                       ;63.5
        jmp       .B2.7         ; Prob 100%                     ;63.5
                                ; LOE
.B2.5:                          ; Preds .B2.3
        mov       DWORD PTR [esp], 0                            ;65.9
        lea       edx, DWORD PTR [esp]                          ;65.9
        mov       DWORD PTR [32+esp], 27                        ;65.9
        lea       eax, DWORD PTR [32+esp]                       ;65.9
        mov       DWORD PTR [36+esp], OFFSET FLAT: __STRLITPACK_35 ;65.9
        push      32                                            ;65.9
        push      eax                                           ;65.9
        push      OFFSET FLAT: __STRLITPACK_37.0.2              ;65.9
        push      -2088435968                                   ;65.9
        push      911                                           ;65.9
        push      edx                                           ;65.9
        call      _for_write_seq_lis                            ;65.9
                                ; LOE
.B2.6:                          ; Preds .B2.5
        push      32                                            ;66.9
        xor       eax, eax                                      ;66.9
        push      eax                                           ;66.9
        push      eax                                           ;66.9
        push      -2088435968                                   ;66.9
        push      eax                                           ;66.9
        push      OFFSET FLAT: __STRLITPACK_38                  ;66.9
        call      _for_stop_core                                ;66.9
                                ; LOE
.B2.42:                         ; Preds .B2.6
        add       esp, 48                                       ;66.9
                                ; LOE
.B2.7:                          ; Preds .B2.41 .B2.42
        mov       ecx, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY+24] ;68.5
        test      ecx, ecx                                      ;68.5
        mov       esi, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY+32] ;68.5
        jle       .B2.32        ; Prob 50%                      ;68.5
                                ; LOE ecx esi
.B2.8:                          ; Preds .B2.7
        mov       edx, ecx                                      ;68.5
        shr       edx, 31                                       ;68.5
        add       edx, ecx                                      ;68.5
        sar       edx, 1                                        ;68.5
        mov       ebx, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY] ;68.5
        test      edx, edx                                      ;68.5
        jbe       .B2.36        ; Prob 10%                      ;68.5
                                ; LOE edx ecx ebx esi
.B2.9:                          ; Preds .B2.8
        xor       eax, eax                                      ;
        mov       DWORD PTR [44+esp], esi                       ;
        xor       edi, edi                                      ;
                                ; LOE eax edx ecx ebx edi
.B2.10:                         ; Preds .B2.10 .B2.9
        lea       esi, DWORD PTR [edi+edi*2]                    ;68.5
        inc       edi                                           ;68.5
        shl       esi, 5                                        ;68.5
        cmp       edi, edx                                      ;68.5
        mov       BYTE PTR [ebx+esi], al                        ;68.5
        mov       BYTE PTR [48+ebx+esi], al                     ;68.5
        jb        .B2.10        ; Prob 63%                      ;68.5
                                ; LOE eax edx ecx ebx edi
.B2.11:                         ; Preds .B2.10
        mov       esi, DWORD PTR [44+esp]                       ;
        lea       eax, DWORD PTR [1+edi+edi]                    ;68.5
                                ; LOE eax edx ecx ebx esi
.B2.12:                         ; Preds .B2.11 .B2.36
        lea       edi, DWORD PTR [-1+eax]                       ;68.5
        cmp       ecx, edi                                      ;68.5
        jbe       .B2.14        ; Prob 10%                      ;68.5
                                ; LOE eax edx ecx ebx esi
.B2.13:                         ; Preds .B2.12
        mov       edi, esi                                      ;68.5
        add       eax, esi                                      ;68.5
        neg       edi                                           ;68.5
        add       edi, eax                                      ;68.5
        lea       eax, DWORD PTR [edi+edi*2]                    ;68.5
        shl       eax, 4                                        ;68.5
        mov       BYTE PTR [-48+ebx+eax], 0                     ;68.5
                                ; LOE edx ecx ebx esi
.B2.14:                         ; Preds .B2.12 .B2.13
        test      edx, edx                                      ;69.5
        jbe       .B2.35        ; Prob 10%                      ;69.5
                                ; LOE edx ecx ebx esi
.B2.15:                         ; Preds .B2.14
        xor       eax, eax                                      ;
        mov       DWORD PTR [44+esp], esi                       ;
        xor       edi, edi                                      ;
                                ; LOE eax edx ecx ebx edi
.B2.16:                         ; Preds .B2.16 .B2.15
        lea       esi, DWORD PTR [edi+edi*2]                    ;69.5
        inc       edi                                           ;69.5
        shl       esi, 5                                        ;69.5
        cmp       edi, edx                                      ;69.5
        mov       BYTE PTR [1+ebx+esi], al                      ;69.5
        mov       BYTE PTR [49+ebx+esi], al                     ;69.5
        jb        .B2.16        ; Prob 63%                      ;69.5
                                ; LOE eax edx ecx ebx edi
.B2.17:                         ; Preds .B2.16
        mov       esi, DWORD PTR [44+esp]                       ;
        lea       eax, DWORD PTR [1+edi+edi]                    ;69.5
                                ; LOE eax edx ecx ebx esi
.B2.18:                         ; Preds .B2.17 .B2.35
        lea       edi, DWORD PTR [-1+eax]                       ;69.5
        cmp       ecx, edi                                      ;69.5
        jbe       .B2.20        ; Prob 10%                      ;69.5
                                ; LOE eax edx ecx ebx esi
.B2.19:                         ; Preds .B2.18
        mov       edi, esi                                      ;69.5
        add       eax, esi                                      ;69.5
        neg       edi                                           ;69.5
        add       edi, eax                                      ;69.5
        lea       eax, DWORD PTR [edi+edi*2]                    ;69.5
        shl       eax, 4                                        ;69.5
        mov       BYTE PTR [-47+ebx+eax], 0                     ;69.5
                                ; LOE edx ecx ebx esi
.B2.20:                         ; Preds .B2.18 .B2.19
        test      edx, edx                                      ;70.5
        jbe       .B2.34        ; Prob 10%                      ;70.5
                                ; LOE edx ecx ebx esi
.B2.21:                         ; Preds .B2.20
        xor       eax, eax                                      ;
        mov       DWORD PTR [44+esp], esi                       ;
        xor       edi, edi                                      ;
                                ; LOE eax edx ecx ebx edi
.B2.22:                         ; Preds .B2.22 .B2.21
        lea       esi, DWORD PTR [edi+edi*2]                    ;70.5
        inc       edi                                           ;70.5
        shl       esi, 5                                        ;70.5
        cmp       edi, edx                                      ;70.5
        mov       DWORD PTR [4+ebx+esi], eax                    ;70.5
        mov       DWORD PTR [52+ebx+esi], eax                   ;70.5
        jb        .B2.22        ; Prob 63%                      ;70.5
                                ; LOE eax edx ecx ebx edi
.B2.23:                         ; Preds .B2.22
        mov       esi, DWORD PTR [44+esp]                       ;
        lea       eax, DWORD PTR [1+edi+edi]                    ;70.5
                                ; LOE eax edx ecx ebx esi
.B2.24:                         ; Preds .B2.23 .B2.34
        lea       edi, DWORD PTR [-1+eax]                       ;70.5
        cmp       ecx, edi                                      ;70.5
        jbe       .B2.26        ; Prob 10%                      ;70.5
                                ; LOE eax edx ecx ebx esi
.B2.25:                         ; Preds .B2.24
        mov       edi, esi                                      ;70.5
        add       eax, esi                                      ;70.5
        neg       edi                                           ;70.5
        add       edi, eax                                      ;70.5
        lea       eax, DWORD PTR [edi+edi*2]                    ;70.5
        shl       eax, 4                                        ;70.5
        mov       DWORD PTR [-44+ebx+eax], 0                    ;70.5
                                ; LOE edx ecx ebx esi
.B2.26:                         ; Preds .B2.24 .B2.25
        test      edx, edx                                      ;71.5
        jbe       .B2.33        ; Prob 10%                      ;71.5
                                ; LOE edx ecx ebx esi
.B2.27:                         ; Preds .B2.26
        xor       eax, eax                                      ;
        mov       DWORD PTR [44+esp], esi                       ;
        xor       edi, edi                                      ;
        mov       eax, 1                                        ;
                                ; LOE eax edx ecx ebx edi
.B2.28:                         ; Preds .B2.28 .B2.27
        lea       esi, DWORD PTR [edi+edi*2]                    ;71.5
        inc       edi                                           ;71.5
        shl       esi, 5                                        ;71.5
        cmp       edi, edx                                      ;71.5
        mov       BYTE PTR [8+ebx+esi], al                      ;71.5
        mov       BYTE PTR [56+ebx+esi], al                     ;71.5
        jb        .B2.28        ; Prob 63%                      ;71.5
                                ; LOE eax edx ecx ebx edi
.B2.29:                         ; Preds .B2.28
        mov       esi, DWORD PTR [44+esp]                       ;
        lea       edx, DWORD PTR [1+edi+edi]                    ;71.5
                                ; LOE edx ecx ebx esi
.B2.30:                         ; Preds .B2.29 .B2.33
        lea       eax, DWORD PTR [-1+edx]                       ;71.5
        cmp       ecx, eax                                      ;71.5
        jbe       .B2.32        ; Prob 10%                      ;71.5
                                ; LOE edx ebx esi
.B2.31:                         ; Preds .B2.30
        mov       eax, esi                                      ;71.5
        add       edx, esi                                      ;71.5
        neg       eax                                           ;71.5
        add       eax, edx                                      ;71.5
        lea       edx, DWORD PTR [eax+eax*2]                    ;71.5
        shl       edx, 4                                        ;71.5
        mov       BYTE PTR [-40+ebx+edx], 1                     ;71.5
                                ; LOE
.B2.32:                         ; Preds .B2.7 .B2.30 .B2.31
        add       esp, 52                                       ;72.1
        pop       ebx                                           ;72.1
        pop       edi                                           ;72.1
        pop       esi                                           ;72.1
        mov       esp, ebp                                      ;72.1
        pop       ebp                                           ;72.1
        ret                                                     ;72.1
                                ; LOE
.B2.33:                         ; Preds .B2.26                  ; Infreq
        mov       edx, 1                                        ;
        jmp       .B2.30        ; Prob 100%                     ;
                                ; LOE edx ecx ebx esi
.B2.34:                         ; Preds .B2.20                  ; Infreq
        mov       eax, 1                                        ;
        jmp       .B2.24        ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B2.35:                         ; Preds .B2.14                  ; Infreq
        mov       eax, 1                                        ;
        jmp       .B2.18        ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B2.36:                         ; Preds .B2.8                   ; Infreq
        mov       eax, 1                                        ;
        jmp       .B2.12        ; Prob 100%                     ;
        ALIGN     16
                                ; LOE eax edx ecx ebx esi
; mark_end;
_DYNUST_SIGNAL_MODULE_mp_DECLARESIGNAL ENDP
_TEXT	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
__STRLITPACK_37.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_SIGNAL_MODULE_mp_DECLARESIGNAL
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_SIGNAL_MODULE_mp_INSERTSIGNALPAR
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_SIGNAL_MODULE_mp_INSERTSIGNALPAR
_DYNUST_SIGNAL_MODULE_mp_INSERTSIGNALPAR	PROC NEAR 
; parameter 1: 8 + esp
; parameter 2: 12 + esp
.B3.1:                          ; Preds .B3.0
        push      ebx                                           ;75.12
        mov       ecx, DWORD PTR [8+esp]                        ;75.12
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM+32] ;80.14
        neg       eax                                           ;80.5
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM] ;80.5
        add       eax, DWORD PTR [ecx]                          ;80.5
        mov       ebx, DWORD PTR [4+ecx]                        ;81.41
        mov       eax, DWORD PTR [edx+eax*4]                    ;80.5
        imul      edx, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY+32], -48 ;81.5
        add       edx, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY] ;81.5
        lea       eax, DWORD PTR [eax+eax*2]                    ;80.5
        shl       eax, 4                                        ;80.5
        mov       BYTE PTR [edx+eax], bl                        ;81.5
        and       ebx, -2                                       ;82.25
        cmp       ebx, 4                                        ;82.25
        jne       .B3.3         ; Prob 84%                      ;82.25
                                ; LOE eax edx ecx ebp esi edi
.B3.2:                          ; Preds .B3.1
        mov       ebx, DWORD PTR [8+ecx]                        ;83.40
        mov       ecx, DWORD PTR [12+ecx]                       ;84.9
        mov       BYTE PTR [1+eax+edx], bl                      ;83.9
        mov       DWORD PTR [4+eax+edx], ecx                    ;84.9
                                ; LOE ebp esi edi
.B3.3:                          ; Preds .B3.1 .B3.2
        pop       ebx                                           ;86.1
        ret                                                     ;86.1
        ALIGN     16
                                ; LOE
; mark_end;
_DYNUST_SIGNAL_MODULE_mp_INSERTSIGNALPAR ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_SIGNAL_MODULE_mp_INSERTSIGNALPAR
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_SIGNAL_MODULE_mp_DECLAREPHASE
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_SIGNAL_MODULE_mp_DECLAREPHASE
_DYNUST_SIGNAL_MODULE_mp_DECLAREPHASE	PROC NEAR 
; parameter 1: 8 + ebp
; parameter 2: 12 + ebp
.B4.1:                          ; Preds .B4.0
        push      ebp                                           ;90.12
        mov       ebp, esp                                      ;90.12
        and       esp, -16                                      ;90.12
        push      esi                                           ;90.12
        push      edi                                           ;90.12
        push      ebx                                           ;90.12
        sub       esp, 100                                      ;90.12
        xor       ebx, ebx                                      ;94.5
        mov       eax, DWORD PTR [12+ebp]                       ;90.12
        lea       edx, DWORD PTR [72+esp]                       ;94.5
        push      48                                            ;94.5
        mov       esi, DWORD PTR [eax]                          ;94.5
        test      esi, esi                                      ;94.5
        cmovge    ebx, esi                                      ;94.5
        push      ebx                                           ;94.5
        push      2                                             ;94.5
        push      edx                                           ;94.5
        call      _for_check_mult_overflow                      ;94.5
                                ; LOE eax ebx esi
.B4.2:                          ; Preds .B4.1
        mov       edx, DWORD PTR [8+ebp]                        ;90.12
        and       eax, 1                                        ;94.5
        imul      edi, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY+32], -48 ;94.5
        mov       ecx, DWORD PTR [edx]                          ;94.14
        add       edi, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY] ;94.5
        shl       eax, 4                                        ;94.5
        or        eax, 262145                                   ;94.5
        lea       edx, DWORD PTR [ecx+ecx*2]                    ;94.14
        shl       edx, 4                                        ;94.14
        mov       DWORD PTR [80+esp], ecx                       ;94.14
        mov       DWORD PTR [68+esp], edx                       ;94.14
        push      eax                                           ;94.5
        lea       ecx, DWORD PTR [12+edi+edx]                   ;94.5
        push      ecx                                           ;94.5
        push      DWORD PTR [96+esp]                            ;94.5
        call      _for_allocate                                 ;94.5
                                ; LOE eax ebx esi
.B4.200:                        ; Preds .B4.2
        add       esp, 28                                       ;94.5
        mov       edi, eax                                      ;94.5
                                ; LOE ebx esi edi
.B4.3:                          ; Preds .B4.200
        test      edi, edi                                      ;94.5
        jne       .B4.6         ; Prob 50%                      ;94.5
                                ; LOE ebx esi edi
.B4.4:                          ; Preds .B4.3
        imul      ecx, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY+32], -48 ;94.5
        mov       eax, 48                                       ;94.5
        mov       DWORD PTR [esp], edi                          ;
        mov       edx, 1                                        ;94.5
        mov       edi, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY] ;94.5
        add       edi, DWORD PTR [52+esp]                       ;94.5
        mov       DWORD PTR [28+ecx+edi], edx                   ;94.5
        mov       DWORD PTR [44+ecx+edi], edx                   ;94.5
        lea       edx, DWORD PTR [68+esp]                       ;94.5
        push      eax                                           ;94.5
        push      ebx                                           ;94.5
        push      2                                             ;94.5
        push      edx                                           ;94.5
        mov       DWORD PTR [24+ecx+edi], 5                     ;94.5
        mov       DWORD PTR [16+ecx+edi], eax                   ;94.5
        mov       DWORD PTR [20+ecx+edi], 0                     ;94.5
        mov       DWORD PTR [36+ecx+edi], ebx                   ;94.5
        mov       DWORD PTR [40+ecx+edi], eax                   ;94.5
        mov       edi, DWORD PTR [16+esp]                       ;94.5
        call      _for_check_mult_overflow                      ;94.5
                                ; LOE esi edi
.B4.201:                        ; Preds .B4.4
        add       esp, 16                                       ;94.5
                                ; LOE esi edi
.B4.5:                          ; Preds .B4.201
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], edi ;94.5
        jmp       .B4.8         ; Prob 100%                     ;94.5
                                ; LOE esi
.B4.6:                          ; Preds .B4.3
        mov       DWORD PTR [esp], 0                            ;96.9
        lea       edx, DWORD PTR [esp]                          ;96.9
        mov       DWORD PTR [56+esp], 45                        ;96.9
        lea       eax, DWORD PTR [56+esp]                       ;96.9
        mov       DWORD PTR [60+esp], OFFSET FLAT: __STRLITPACK_33 ;96.9
        push      32                                            ;96.9
        push      eax                                           ;96.9
        push      OFFSET FLAT: __STRLITPACK_39.0.4              ;96.9
        push      -2088435968                                   ;96.9
        push      911                                           ;96.9
        push      edx                                           ;96.9
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], edi ;94.5
        call      _for_write_seq_lis                            ;96.9
                                ; LOE esi
.B4.7:                          ; Preds .B4.6
        push      32                                            ;97.9
        xor       eax, eax                                      ;97.9
        push      eax                                           ;97.9
        push      eax                                           ;97.9
        push      -2088435968                                   ;97.9
        push      eax                                           ;97.9
        push      OFFSET FLAT: __STRLITPACK_40                  ;97.9
        call      _for_stop_core                                ;97.9
                                ; LOE esi
.B4.202:                        ; Preds .B4.7
        add       esp, 48                                       ;97.9
                                ; LOE esi
.B4.8:                          ; Preds .B4.202 .B4.5
        test      esi, esi                                      ;99.15
        jle       .B4.194       ; Prob 16%                      ;99.15
                                ; LOE
.B4.9:                          ; Preds .B4.8 .B4.203
        imul      eax, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY+32], -48 ;103.5
        mov       ecx, DWORD PTR [52+esp]                       ;103.5
        add       ecx, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY] ;103.5
        mov       DWORD PTR [52+esp], ecx                       ;103.5
        mov       edx, DWORD PTR [44+eax+ecx]                   ;103.5
        mov       ebx, DWORD PTR [40+eax+ecx]                   ;103.5
        cmp       ebx, 1                                        ;103.5
        mov       DWORD PTR [80+esp], edx                       ;103.5
        mov       edx, DWORD PTR [36+eax+ecx]                   ;103.5
        mov       DWORD PTR [76+esp], ebx                       ;103.5
        jne       .B4.25        ; Prob 50%                      ;103.5
                                ; LOE eax edx ecx cl ch
.B4.10:                         ; Preds .B4.9
        test      edx, edx                                      ;103.5
        mov       eax, DWORD PTR [12+eax+ecx]                   ;103.5
        jle       .B4.24        ; Prob 50%                      ;103.5
                                ; LOE eax edx
.B4.11:                         ; Preds .B4.10
        cmp       edx, 16                                       ;103.5
        jl        .B4.170       ; Prob 10%                      ;103.5
                                ; LOE eax edx
.B4.12:                         ; Preds .B4.11
        mov       ecx, eax                                      ;103.5
        and       ecx, 15                                       ;103.5
        mov       ebx, ecx                                      ;103.5
        neg       ebx                                           ;103.5
        add       ebx, 16                                       ;103.5
        test      ecx, ecx                                      ;103.5
        cmovne    ecx, ebx                                      ;103.5
        lea       esi, DWORD PTR [16+ecx]                       ;103.5
        cmp       edx, esi                                      ;103.5
        jl        .B4.170       ; Prob 10%                      ;103.5
                                ; LOE eax edx ecx
.B4.13:                         ; Preds .B4.12
        mov       esi, edx                                      ;103.5
        sub       esi, ecx                                      ;103.5
        and       esi, 15                                       ;103.5
        neg       esi                                           ;103.5
        add       esi, edx                                      ;103.5
        test      ecx, ecx                                      ;103.5
        jbe       .B4.17        ; Prob 10%                      ;103.5
                                ; LOE eax edx ecx esi
.B4.14:                         ; Preds .B4.13
        xor       ebx, ebx                                      ;
                                ; LOE eax edx ecx ebx esi
.B4.15:                         ; Preds .B4.15 .B4.14
        mov       BYTE PTR [ebx+eax], 0                         ;103.5
        inc       ebx                                           ;103.5
        cmp       ebx, ecx                                      ;103.5
        jb        .B4.15        ; Prob 82%                      ;103.5
                                ; LOE eax edx ecx ebx esi
.B4.17:                         ; Preds .B4.15 .B4.13
        pxor      xmm0, xmm0                                    ;103.5
                                ; LOE eax edx ecx esi xmm0
.B4.18:                         ; Preds .B4.18 .B4.17
        movdqa    XMMWORD PTR [ecx+eax], xmm0                   ;103.5
        add       ecx, 16                                       ;103.5
        cmp       ecx, esi                                      ;103.5
        jb        .B4.18        ; Prob 82%                      ;103.5
                                ; LOE eax edx ecx esi xmm0
.B4.20:                         ; Preds .B4.18 .B4.170
        cmp       esi, edx                                      ;103.5
        jae       .B4.23        ; Prob 10%                      ;103.5
                                ; LOE eax edx esi
.B4.22:                         ; Preds .B4.20 .B4.22
        mov       BYTE PTR [esi+eax], 0                         ;103.5
        inc       esi                                           ;103.5
        cmp       esi, edx                                      ;103.5
        jb        .B4.22        ; Prob 82%                      ;103.5
                                ; LOE eax edx esi
.B4.23:                         ; Preds .B4.20 .B4.22
        mov       ecx, DWORD PTR [80+esp]                       ;
        imul      ecx, DWORD PTR [76+esp]                       ;
        neg       ecx                                           ;
        add       ecx, eax                                      ;
        jmp       .B4.52        ; Prob 100%                     ;
                                ; LOE eax edx ecx
.B4.24:                         ; Preds .B4.10
        mov       edx, DWORD PTR [80+esp]                       ;
        mov       ecx, eax                                      ;
        imul      edx, DWORD PTR [76+esp]                       ;
        sub       ecx, edx                                      ;
        jmp       .B4.168       ; Prob 100%                     ;
                                ; LOE ecx
.B4.25:                         ; Preds .B4.9
        test      edx, edx                                      ;103.5
        jle       .B4.32        ; Prob 50%                      ;103.5
                                ; LOE eax edx ecx cl ch
.B4.26:                         ; Preds .B4.25
        mov       esi, edx                                      ;103.5
        shr       esi, 31                                       ;103.5
        add       esi, edx                                      ;103.5
        mov       eax, DWORD PTR [12+eax+ecx]                   ;103.5
        mov       ecx, DWORD PTR [80+esp]                       ;
        imul      ecx, DWORD PTR [76+esp]                       ;
        neg       ecx                                           ;
        sar       esi, 1                                        ;103.5
        add       ecx, eax                                      ;
        test      esi, esi                                      ;103.5
        jbe       .B4.172       ; Prob 10%                      ;103.5
                                ; LOE eax edx ecx esi
.B4.27:                         ; Preds .B4.26
        xor       ebx, ebx                                      ;
        mov       DWORD PTR [84+esp], esi                       ;
        mov       DWORD PTR [64+esp], eax                       ;
        xor       eax, eax                                      ;
        mov       DWORD PTR [52+esp], edx                       ;
                                ; LOE eax ecx ebx
.B4.28:                         ; Preds .B4.28 .B4.27
        mov       edx, DWORD PTR [80+esp]                       ;103.5
        mov       edi, DWORD PTR [76+esp]                       ;103.5
        mov       esi, edi                                      ;103.5
        lea       edx, DWORD PTR [edx+ebx*2]                    ;103.5
        inc       ebx                                           ;103.5
        imul      esi, edx                                      ;103.5
        inc       edx                                           ;103.5
        imul      edx, edi                                      ;103.5
        mov       BYTE PTR [ecx+esi], al                        ;103.5
        mov       BYTE PTR [ecx+edx], al                        ;103.5
        cmp       ebx, DWORD PTR [84+esp]                       ;103.5
        jb        .B4.28        ; Prob 63%                      ;103.5
                                ; LOE eax ecx ebx
.B4.29:                         ; Preds .B4.28
        mov       eax, DWORD PTR [64+esp]                       ;
        lea       esi, DWORD PTR [1+ebx+ebx]                    ;103.5
        mov       edx, DWORD PTR [52+esp]                       ;
                                ; LOE eax edx ecx esi
.B4.30:                         ; Preds .B4.29 .B4.172
        lea       ebx, DWORD PTR [-1+esi]                       ;103.5
        cmp       edx, ebx                                      ;103.5
        jbe       .B4.33        ; Prob 10%                      ;103.5
                                ; LOE eax edx ecx esi
.B4.31:                         ; Preds .B4.30
        mov       ebx, DWORD PTR [80+esp]                       ;103.5
        lea       esi, DWORD PTR [-1+esi+ebx]                   ;103.5
        imul      esi, DWORD PTR [76+esp]                       ;103.5
        mov       BYTE PTR [esi+ecx], 0                         ;103.5
        jmp       .B4.33        ; Prob 100%                     ;103.5
                                ; LOE eax edx ecx
.B4.32:                         ; Preds .B4.25
        mov       eax, DWORD PTR [12+eax+ecx]                   ;104.5
        mov       ecx, DWORD PTR [80+esp]                       ;
        imul      ecx, DWORD PTR [76+esp]                       ;
        neg       ecx                                           ;
        add       ecx, eax                                      ;
                                ; LOE eax edx ecx
.B4.33:                         ; Preds .B4.31 .B4.30 .B4.32
        cmp       DWORD PTR [76+esp], 2                         ;104.5
        jne       .B4.51        ; Prob 50%                      ;104.5
                                ; LOE eax edx ecx
.B4.34:                         ; Preds .B4.33
        test      edx, edx                                      ;104.5
        jle       .B4.168       ; Prob 50%                      ;104.5
                                ; LOE eax edx ecx
.B4.35:                         ; Preds .B4.34
        cmp       edx, 8                                        ;104.5
        jl        .B4.173       ; Prob 10%                      ;104.5
                                ; LOE eax edx ecx
.B4.36:                         ; Preds .B4.35
        lea       edi, DWORD PTR [2+eax]                        ;104.5
        and       edi, 15                                       ;104.5
        je        .B4.39        ; Prob 50%                      ;104.5
                                ; LOE eax edx ecx edi
.B4.37:                         ; Preds .B4.36
        test      edi, 1                                        ;104.5
        jne       .B4.173       ; Prob 10%                      ;104.5
                                ; LOE eax edx ecx edi
.B4.38:                         ; Preds .B4.37
        neg       edi                                           ;104.5
        add       edi, 16                                       ;104.5
        shr       edi, 1                                        ;104.5
                                ; LOE eax edx ecx edi
.B4.39:                         ; Preds .B4.38 .B4.36
        lea       ebx, DWORD PTR [8+edi]                        ;104.5
        cmp       edx, ebx                                      ;104.5
        jl        .B4.173       ; Prob 10%                      ;104.5
                                ; LOE eax edx ecx edi
.B4.40:                         ; Preds .B4.39
        mov       esi, edx                                      ;104.5
        sub       esi, edi                                      ;104.5
        and       esi, 7                                        ;104.5
        neg       esi                                           ;104.5
        add       esi, edx                                      ;104.5
        test      edi, edi                                      ;104.5
        jbe       .B4.44        ; Prob 10%                      ;104.5
                                ; LOE eax edx ecx esi edi
.B4.41:                         ; Preds .B4.40
        xor       ebx, ebx                                      ;
        mov       DWORD PTR [52+esp], edx                       ;
        xor       edx, edx                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B4.42:                         ; Preds .B4.42 .B4.41
        mov       WORD PTR [2+eax+ebx*2], dx                    ;104.5
        inc       ebx                                           ;104.5
        cmp       ebx, edi                                      ;104.5
        jb        .B4.42        ; Prob 82%                      ;104.5
                                ; LOE eax edx ecx ebx esi edi
.B4.43:                         ; Preds .B4.42
        mov       edx, DWORD PTR [52+esp]                       ;
                                ; LOE eax edx ecx esi edi
.B4.44:                         ; Preds .B4.40 .B4.43
        pxor      xmm0, xmm0                                    ;104.5
                                ; LOE eax edx ecx esi edi xmm0
.B4.45:                         ; Preds .B4.45 .B4.44
        movdqa    XMMWORD PTR [2+eax+edi*2], xmm0               ;104.5
        add       edi, 8                                        ;104.5
        cmp       edi, esi                                      ;104.5
        jb        .B4.45        ; Prob 82%                      ;104.5
                                ; LOE eax edx ecx esi edi xmm0
.B4.47:                         ; Preds .B4.45 .B4.173
        cmp       esi, edx                                      ;104.5
        jae       .B4.87        ; Prob 10%                      ;104.5
                                ; LOE eax edx ecx esi
.B4.48:                         ; Preds .B4.47
        xor       ebx, ebx                                      ;
                                ; LOE eax edx ecx ebx esi
.B4.49:                         ; Preds .B4.49 .B4.48
        mov       WORD PTR [2+eax+esi*2], bx                    ;104.5
        inc       esi                                           ;104.5
        cmp       esi, edx                                      ;104.5
        jb        .B4.49        ; Prob 82%                      ;104.5
        jmp       .B4.87        ; Prob 100%                     ;104.5
                                ; LOE eax edx ecx ebx esi
.B4.51:                         ; Preds .B4.33
        test      edx, edx                                      ;104.5
        jle       .B4.58        ; Prob 50%                      ;104.5
                                ; LOE eax edx ecx
.B4.52:                         ; Preds .B4.23 .B4.51
        mov       esi, edx                                      ;104.5
        shr       esi, 31                                       ;104.5
        add       esi, edx                                      ;104.5
        sar       esi, 1                                        ;104.5
        test      esi, esi                                      ;104.5
        jbe       .B4.176       ; Prob 10%                      ;104.5
                                ; LOE eax edx ecx esi
.B4.53:                         ; Preds .B4.52
        xor       ebx, ebx                                      ;
        mov       DWORD PTR [84+esp], esi                       ;
        mov       DWORD PTR [64+esp], eax                       ;
        xor       eax, eax                                      ;
        mov       DWORD PTR [52+esp], edx                       ;
                                ; LOE eax ecx ebx
.B4.54:                         ; Preds .B4.54 .B4.53
        mov       edx, DWORD PTR [80+esp]                       ;104.5
        mov       edi, DWORD PTR [76+esp]                       ;104.5
        mov       esi, edi                                      ;104.5
        lea       edx, DWORD PTR [edx+ebx*2]                    ;104.5
        inc       ebx                                           ;104.5
        imul      esi, edx                                      ;104.5
        inc       edx                                           ;104.5
        imul      edx, edi                                      ;104.5
        mov       WORD PTR [2+ecx+esi], ax                      ;104.5
        mov       WORD PTR [2+ecx+edx], ax                      ;104.5
        cmp       ebx, DWORD PTR [84+esp]                       ;104.5
        jb        .B4.54        ; Prob 63%                      ;104.5
                                ; LOE eax ecx ebx
.B4.55:                         ; Preds .B4.54
        mov       eax, DWORD PTR [64+esp]                       ;
        lea       edi, DWORD PTR [1+ebx+ebx]                    ;104.5
        mov       edx, DWORD PTR [52+esp]                       ;
                                ; LOE eax edx ecx edi
.B4.56:                         ; Preds .B4.55 .B4.176
        lea       ebx, DWORD PTR [-1+edi]                       ;104.5
        cmp       edx, ebx                                      ;104.5
        jbe       .B4.58        ; Prob 10%                      ;104.5
                                ; LOE eax edx ecx edi
.B4.57:                         ; Preds .B4.56
        mov       esi, DWORD PTR [80+esp]                       ;104.5
        xor       ebx, ebx                                      ;104.5
        lea       edi, DWORD PTR [-1+edi+esi]                   ;104.5
        imul      edi, DWORD PTR [76+esp]                       ;104.5
        mov       WORD PTR [2+edi+ecx], bx                      ;104.5
                                ; LOE eax edx ecx
.B4.58:                         ; Preds .B4.51 .B4.56 .B4.57
        cmp       DWORD PTR [76+esp], 1                         ;105.5
        jne       .B4.86        ; Prob 50%                      ;105.5
                                ; LOE eax edx ecx
.B4.59:                         ; Preds .B4.58
        test      edx, edx                                      ;105.5
        jle       .B4.168       ; Prob 50%                      ;105.5
                                ; LOE eax edx ecx
.B4.60:                         ; Preds .B4.59
        cmp       edx, 16                                       ;105.5
        jl        .B4.179       ; Prob 10%                      ;105.5
                                ; LOE eax edx ecx
.B4.61:                         ; Preds .B4.60
        lea       esi, DWORD PTR [4+eax]                        ;105.5
        and       esi, 15                                       ;105.5
        mov       ebx, esi                                      ;105.5
        neg       ebx                                           ;105.5
        add       ebx, 16                                       ;105.5
        test      esi, esi                                      ;105.5
        cmovne    esi, ebx                                      ;105.5
        lea       edi, DWORD PTR [16+esi]                       ;105.5
        cmp       edx, edi                                      ;105.5
        jl        .B4.179       ; Prob 10%                      ;105.5
                                ; LOE eax edx ecx esi
.B4.62:                         ; Preds .B4.61
        mov       edi, edx                                      ;105.5
        sub       edi, esi                                      ;105.5
        and       edi, 15                                       ;105.5
        neg       edi                                           ;105.5
        add       edi, edx                                      ;105.5
        test      esi, esi                                      ;105.5
        jbe       .B4.66        ; Prob 10%                      ;105.5
                                ; LOE eax edx ecx esi edi
.B4.63:                         ; Preds .B4.62
        xor       ebx, ebx                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B4.64:                         ; Preds .B4.64 .B4.63
        mov       BYTE PTR [4+ebx+eax], 0                       ;105.5
        inc       ebx                                           ;105.5
        cmp       ebx, esi                                      ;105.5
        jb        .B4.64        ; Prob 82%                      ;105.5
                                ; LOE eax edx ecx ebx esi edi
.B4.66:                         ; Preds .B4.64 .B4.62
        pxor      xmm0, xmm0                                    ;105.5
                                ; LOE eax edx ecx esi edi xmm0
.B4.67:                         ; Preds .B4.67 .B4.66
        movdqa    XMMWORD PTR [4+esi+eax], xmm0                 ;105.5
        add       esi, 16                                       ;105.5
        cmp       esi, edi                                      ;105.5
        jb        .B4.67        ; Prob 82%                      ;105.5
                                ; LOE eax edx ecx esi edi xmm0
.B4.69:                         ; Preds .B4.67 .B4.179
        cmp       edi, edx                                      ;105.5
        jae       .B4.73        ; Prob 10%                      ;105.5
                                ; LOE eax edx ecx edi
.B4.71:                         ; Preds .B4.69 .B4.71
        mov       BYTE PTR [4+edi+eax], 0                       ;105.5
        inc       edi                                           ;105.5
        cmp       edi, edx                                      ;105.5
        jb        .B4.71        ; Prob 82%                      ;105.5
                                ; LOE eax edx ecx edi
.B4.73:                         ; Preds .B4.71 .B4.69
        cmp       edx, 16                                       ;106.5
        jl        .B4.177       ; Prob 10%                      ;106.5
                                ; LOE eax edx ecx
.B4.74:                         ; Preds .B4.73
        lea       esi, DWORD PTR [5+eax]                        ;106.5
        and       esi, 15                                       ;106.5
        mov       ebx, esi                                      ;106.5
        neg       ebx                                           ;106.5
        add       ebx, 16                                       ;106.5
        test      esi, esi                                      ;106.5
        cmovne    esi, ebx                                      ;106.5
        lea       edi, DWORD PTR [16+esi]                       ;106.5
        cmp       edx, edi                                      ;106.5
        jl        .B4.177       ; Prob 10%                      ;106.5
                                ; LOE eax edx ecx esi
.B4.75:                         ; Preds .B4.74
        mov       edi, edx                                      ;106.5
        sub       edi, esi                                      ;106.5
        and       edi, 15                                       ;106.5
        neg       edi                                           ;106.5
        add       edi, edx                                      ;106.5
        test      esi, esi                                      ;106.5
        jbe       .B4.79        ; Prob 10%                      ;106.5
                                ; LOE eax edx ecx esi edi
.B4.76:                         ; Preds .B4.75
        xor       ebx, ebx                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B4.77:                         ; Preds .B4.77 .B4.76
        mov       BYTE PTR [5+ebx+eax], 0                       ;106.5
        inc       ebx                                           ;106.5
        cmp       ebx, esi                                      ;106.5
        jb        .B4.77        ; Prob 82%                      ;106.5
                                ; LOE eax edx ecx ebx esi edi
.B4.79:                         ; Preds .B4.77 .B4.75
        pxor      xmm0, xmm0                                    ;106.5
                                ; LOE eax edx ecx esi edi xmm0
.B4.80:                         ; Preds .B4.80 .B4.79
        movdqa    XMMWORD PTR [5+esi+eax], xmm0                 ;106.5
        add       esi, 16                                       ;106.5
        cmp       esi, edi                                      ;106.5
        jb        .B4.80        ; Prob 82%                      ;106.5
                                ; LOE eax edx ecx esi edi xmm0
.B4.82:                         ; Preds .B4.80 .B4.177
        cmp       edi, edx                                      ;106.5
        jae       .B4.134       ; Prob 10%                      ;106.5
                                ; LOE eax edx ecx edi
.B4.84:                         ; Preds .B4.82 .B4.84
        mov       BYTE PTR [5+edi+eax], 0                       ;106.5
        inc       edi                                           ;106.5
        cmp       edi, edx                                      ;106.5
        jb        .B4.84        ; Prob 82%                      ;106.5
        jmp       .B4.134       ; Prob 100%                     ;106.5
                                ; LOE eax edx ecx edi
.B4.86:                         ; Preds .B4.58
        test      edx, edx                                      ;105.5
        jle       .B4.99        ; Prob 50%                      ;105.5
                                ; LOE eax edx ecx
.B4.87:                         ; Preds .B4.49 .B4.47 .B4.86
        mov       esi, edx                                      ;105.5
        shr       esi, 31                                       ;105.5
        add       esi, edx                                      ;105.5
        sar       esi, 1                                        ;105.5
        test      esi, esi                                      ;105.5
        jbe       .B4.182       ; Prob 10%                      ;105.5
                                ; LOE eax edx ecx esi
.B4.88:                         ; Preds .B4.87
        xor       ebx, ebx                                      ;
        mov       DWORD PTR [84+esp], esi                       ;
        mov       DWORD PTR [64+esp], eax                       ;
        xor       eax, eax                                      ;
        mov       DWORD PTR [52+esp], edx                       ;
                                ; LOE eax ecx ebx
.B4.89:                         ; Preds .B4.89 .B4.88
        mov       edx, DWORD PTR [80+esp]                       ;105.5
        mov       edi, DWORD PTR [76+esp]                       ;105.5
        mov       esi, edi                                      ;105.5
        lea       edx, DWORD PTR [edx+ebx*2]                    ;105.5
        inc       ebx                                           ;105.5
        imul      esi, edx                                      ;105.5
        inc       edx                                           ;105.5
        imul      edx, edi                                      ;105.5
        mov       BYTE PTR [4+ecx+esi], al                      ;105.5
        mov       BYTE PTR [4+ecx+edx], al                      ;105.5
        cmp       ebx, DWORD PTR [84+esp]                       ;105.5
        jb        .B4.89        ; Prob 63%                      ;105.5
                                ; LOE eax ecx ebx
.B4.90:                         ; Preds .B4.89
        mov       eax, DWORD PTR [64+esp]                       ;
        lea       esi, DWORD PTR [1+ebx+ebx]                    ;105.5
        mov       edx, DWORD PTR [52+esp]                       ;
                                ; LOE eax edx ecx esi
.B4.91:                         ; Preds .B4.90 .B4.182
        lea       ebx, DWORD PTR [-1+esi]                       ;105.5
        cmp       edx, ebx                                      ;105.5
        jbe       .B4.93        ; Prob 10%                      ;105.5
                                ; LOE eax edx ecx esi
.B4.92:                         ; Preds .B4.91
        mov       ebx, DWORD PTR [80+esp]                       ;105.5
        lea       esi, DWORD PTR [-1+esi+ebx]                   ;105.5
        imul      esi, DWORD PTR [76+esp]                       ;105.5
        mov       BYTE PTR [4+esi+ecx], 0                       ;105.5
                                ; LOE eax edx ecx
.B4.93:                         ; Preds .B4.92 .B4.91
        mov       esi, edx                                      ;106.5
        shr       esi, 31                                       ;106.5
        add       esi, edx                                      ;106.5
        sar       esi, 1                                        ;106.5
        test      esi, esi                                      ;106.5
        jbe       .B4.181       ; Prob 10%                      ;106.5
                                ; LOE eax edx ecx esi
.B4.94:                         ; Preds .B4.93
        xor       ebx, ebx                                      ;
        mov       DWORD PTR [84+esp], esi                       ;
        mov       DWORD PTR [64+esp], eax                       ;
        xor       eax, eax                                      ;
        mov       DWORD PTR [52+esp], edx                       ;
                                ; LOE eax ecx ebx
.B4.95:                         ; Preds .B4.95 .B4.94
        mov       edx, DWORD PTR [80+esp]                       ;106.5
        mov       edi, DWORD PTR [76+esp]                       ;106.5
        mov       esi, edi                                      ;106.5
        lea       edx, DWORD PTR [edx+ebx*2]                    ;106.5
        inc       ebx                                           ;106.5
        imul      esi, edx                                      ;106.5
        inc       edx                                           ;106.5
        imul      edx, edi                                      ;106.5
        mov       BYTE PTR [5+ecx+esi], al                      ;106.5
        mov       BYTE PTR [5+ecx+edx], al                      ;106.5
        cmp       ebx, DWORD PTR [84+esp]                       ;106.5
        jb        .B4.95        ; Prob 63%                      ;106.5
                                ; LOE eax ecx ebx
.B4.96:                         ; Preds .B4.95
        mov       eax, DWORD PTR [64+esp]                       ;
        lea       esi, DWORD PTR [1+ebx+ebx]                    ;106.5
        mov       edx, DWORD PTR [52+esp]                       ;
                                ; LOE eax edx ecx esi
.B4.97:                         ; Preds .B4.96 .B4.181
        lea       ebx, DWORD PTR [-1+esi]                       ;106.5
        cmp       edx, ebx                                      ;106.5
        jbe       .B4.99        ; Prob 10%                      ;106.5
                                ; LOE eax edx ecx esi
.B4.98:                         ; Preds .B4.97
        mov       ebx, DWORD PTR [80+esp]                       ;106.5
        lea       esi, DWORD PTR [-1+esi+ebx]                   ;106.5
        imul      esi, DWORD PTR [76+esp]                       ;106.5
        mov       BYTE PTR [5+esi+ecx], 0                       ;106.5
                                ; LOE eax edx ecx
.B4.99:                         ; Preds .B4.86 .B4.97 .B4.98
        cmp       DWORD PTR [76+esp], 2                         ;107.5
        jne       .B4.133       ; Prob 50%                      ;107.5
                                ; LOE eax edx ecx
.B4.100:                        ; Preds .B4.99
        test      edx, edx                                      ;107.5
        jle       .B4.168       ; Prob 50%                      ;107.5
                                ; LOE eax edx ecx
.B4.101:                        ; Preds .B4.100
        cmp       edx, 8                                        ;107.5
        jl        .B4.183       ; Prob 10%                      ;107.5
                                ; LOE eax edx ecx
.B4.102:                        ; Preds .B4.101
        lea       edi, DWORD PTR [6+eax]                        ;107.5
        and       edi, 15                                       ;107.5
        je        .B4.105       ; Prob 50%                      ;107.5
                                ; LOE eax edx ecx edi
.B4.103:                        ; Preds .B4.102
        test      edi, 1                                        ;107.5
        jne       .B4.183       ; Prob 10%                      ;107.5
                                ; LOE eax edx ecx edi
.B4.104:                        ; Preds .B4.103
        neg       edi                                           ;107.5
        add       edi, 16                                       ;107.5
        shr       edi, 1                                        ;107.5
                                ; LOE eax edx ecx edi
.B4.105:                        ; Preds .B4.104 .B4.102
        lea       ebx, DWORD PTR [8+edi]                        ;107.5
        cmp       edx, ebx                                      ;107.5
        jl        .B4.183       ; Prob 10%                      ;107.5
                                ; LOE eax edx ecx edi
.B4.106:                        ; Preds .B4.105
        mov       esi, edx                                      ;107.5
        sub       esi, edi                                      ;107.5
        and       esi, 7                                        ;107.5
        neg       esi                                           ;107.5
        add       esi, edx                                      ;107.5
        test      edi, edi                                      ;107.5
        jbe       .B4.110       ; Prob 10%                      ;107.5
                                ; LOE eax edx ecx esi edi
.B4.107:                        ; Preds .B4.106
        xor       ebx, ebx                                      ;
        mov       DWORD PTR [52+esp], edx                       ;
        xor       edx, edx                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B4.108:                        ; Preds .B4.108 .B4.107
        mov       WORD PTR [6+eax+ebx*2], dx                    ;107.5
        inc       ebx                                           ;107.5
        cmp       ebx, edi                                      ;107.5
        jb        .B4.108       ; Prob 82%                      ;107.5
                                ; LOE eax edx ecx ebx esi edi
.B4.109:                        ; Preds .B4.108
        mov       edx, DWORD PTR [52+esp]                       ;
                                ; LOE eax edx ecx esi edi
.B4.110:                        ; Preds .B4.106 .B4.109
        pxor      xmm0, xmm0                                    ;107.5
                                ; LOE eax edx ecx esi edi xmm0
.B4.111:                        ; Preds .B4.111 .B4.110
        movdqa    XMMWORD PTR [6+eax+edi*2], xmm0               ;107.5
        add       edi, 8                                        ;107.5
        cmp       edi, esi                                      ;107.5
        jb        .B4.111       ; Prob 82%                      ;107.5
                                ; LOE eax edx ecx esi edi xmm0
.B4.113:                        ; Preds .B4.111 .B4.183
        cmp       esi, edx                                      ;107.5
        jae       .B4.117       ; Prob 10%                      ;107.5
                                ; LOE eax edx ecx esi
.B4.114:                        ; Preds .B4.113
        xor       ebx, ebx                                      ;
                                ; LOE eax edx ecx ebx esi
.B4.115:                        ; Preds .B4.115 .B4.114
        mov       WORD PTR [6+eax+esi*2], bx                    ;107.5
        inc       esi                                           ;107.5
        cmp       esi, edx                                      ;107.5
        jb        .B4.115       ; Prob 82%                      ;107.5
                                ; LOE eax edx ecx ebx esi
.B4.117:                        ; Preds .B4.115 .B4.113
        cmp       edx, 8                                        ;108.5
        jl        .B4.184       ; Prob 10%                      ;108.5
                                ; LOE eax edx ecx
.B4.118:                        ; Preds .B4.117
        lea       edi, DWORD PTR [8+eax]                        ;108.5
        and       edi, 15                                       ;108.5
        je        .B4.121       ; Prob 50%                      ;108.5
                                ; LOE eax edx ecx edi
.B4.119:                        ; Preds .B4.118
        test      edi, 1                                        ;108.5
        jne       .B4.184       ; Prob 10%                      ;108.5
                                ; LOE eax edx ecx edi
.B4.120:                        ; Preds .B4.119
        neg       edi                                           ;108.5
        add       edi, 16                                       ;108.5
        shr       edi, 1                                        ;108.5
                                ; LOE eax edx ecx edi
.B4.121:                        ; Preds .B4.120 .B4.118
        lea       ebx, DWORD PTR [8+edi]                        ;108.5
        cmp       edx, ebx                                      ;108.5
        jl        .B4.184       ; Prob 10%                      ;108.5
                                ; LOE eax edx ecx edi
.B4.122:                        ; Preds .B4.121
        mov       esi, edx                                      ;108.5
        sub       esi, edi                                      ;108.5
        and       esi, 7                                        ;108.5
        neg       esi                                           ;108.5
        add       esi, edx                                      ;108.5
        test      edi, edi                                      ;108.5
        jbe       .B4.126       ; Prob 10%                      ;108.5
                                ; LOE eax edx ecx esi edi
.B4.123:                        ; Preds .B4.122
        xor       ebx, ebx                                      ;
        mov       DWORD PTR [52+esp], edx                       ;
        xor       edx, edx                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B4.124:                        ; Preds .B4.124 .B4.123
        mov       WORD PTR [8+eax+ebx*2], dx                    ;108.5
        inc       ebx                                           ;108.5
        cmp       ebx, edi                                      ;108.5
        jb        .B4.124       ; Prob 82%                      ;108.5
                                ; LOE eax edx ecx ebx esi edi
.B4.125:                        ; Preds .B4.124
        mov       edx, DWORD PTR [52+esp]                       ;
                                ; LOE eax edx ecx esi edi
.B4.126:                        ; Preds .B4.125 .B4.122
        pxor      xmm0, xmm0                                    ;108.5
                                ; LOE eax edx ecx esi edi xmm0
.B4.127:                        ; Preds .B4.127 .B4.126
        movdqa    XMMWORD PTR [8+eax+edi*2], xmm0               ;108.5
        add       edi, 8                                        ;108.5
        cmp       edi, esi                                      ;108.5
        jb        .B4.127       ; Prob 82%                      ;108.5
                                ; LOE eax edx ecx esi edi xmm0
.B4.129:                        ; Preds .B4.127 .B4.184
        cmp       esi, edx                                      ;108.5
        jae       .B4.162       ; Prob 10%                      ;108.5
                                ; LOE eax edx ecx esi
.B4.130:                        ; Preds .B4.129
        xor       ebx, ebx                                      ;
                                ; LOE eax edx ecx ebx esi
.B4.131:                        ; Preds .B4.131 .B4.130
        mov       WORD PTR [8+eax+esi*2], bx                    ;108.5
        inc       esi                                           ;108.5
        cmp       esi, edx                                      ;108.5
        jb        .B4.131       ; Prob 82%                      ;108.5
        jmp       .B4.162       ; Prob 100%                     ;108.5
                                ; LOE eax edx ecx ebx esi
.B4.133:                        ; Preds .B4.99
        test      edx, edx                                      ;107.5
        jle       .B4.146       ; Prob 50%                      ;107.5
                                ; LOE eax edx ecx
.B4.134:                        ; Preds .B4.84 .B4.82 .B4.133
        mov       esi, edx                                      ;107.5
        shr       esi, 31                                       ;107.5
        add       esi, edx                                      ;107.5
        sar       esi, 1                                        ;107.5
        test      esi, esi                                      ;107.5
        jbe       .B4.190       ; Prob 10%                      ;107.5
                                ; LOE eax edx ecx esi
.B4.135:                        ; Preds .B4.134
        xor       ebx, ebx                                      ;
        mov       DWORD PTR [84+esp], esi                       ;
        mov       DWORD PTR [64+esp], eax                       ;
        xor       eax, eax                                      ;
        mov       DWORD PTR [52+esp], edx                       ;
                                ; LOE eax ecx ebx
.B4.136:                        ; Preds .B4.136 .B4.135
        mov       edx, DWORD PTR [80+esp]                       ;107.5
        mov       edi, DWORD PTR [76+esp]                       ;107.5
        mov       esi, edi                                      ;107.5
        lea       edx, DWORD PTR [edx+ebx*2]                    ;107.5
        inc       ebx                                           ;107.5
        imul      esi, edx                                      ;107.5
        inc       edx                                           ;107.5
        imul      edx, edi                                      ;107.5
        mov       WORD PTR [6+ecx+esi], ax                      ;107.5
        mov       WORD PTR [6+ecx+edx], ax                      ;107.5
        cmp       ebx, DWORD PTR [84+esp]                       ;107.5
        jb        .B4.136       ; Prob 63%                      ;107.5
                                ; LOE eax ecx ebx
.B4.137:                        ; Preds .B4.136
        mov       eax, DWORD PTR [64+esp]                       ;
        lea       edi, DWORD PTR [1+ebx+ebx]                    ;107.5
        mov       edx, DWORD PTR [52+esp]                       ;
                                ; LOE eax edx ecx edi
.B4.138:                        ; Preds .B4.137 .B4.190
        lea       ebx, DWORD PTR [-1+edi]                       ;107.5
        cmp       edx, ebx                                      ;107.5
        jbe       .B4.140       ; Prob 10%                      ;107.5
                                ; LOE eax edx ecx edi
.B4.139:                        ; Preds .B4.138
        mov       esi, DWORD PTR [80+esp]                       ;107.5
        xor       ebx, ebx                                      ;107.5
        lea       edi, DWORD PTR [-1+edi+esi]                   ;107.5
        imul      edi, DWORD PTR [76+esp]                       ;107.5
        mov       WORD PTR [6+edi+ecx], bx                      ;107.5
                                ; LOE eax edx ecx
.B4.140:                        ; Preds .B4.139 .B4.138
        mov       esi, edx                                      ;108.5
        shr       esi, 31                                       ;108.5
        add       esi, edx                                      ;108.5
        sar       esi, 1                                        ;108.5
        test      esi, esi                                      ;108.5
        jbe       .B4.189       ; Prob 10%                      ;108.5
                                ; LOE eax edx ecx esi
.B4.141:                        ; Preds .B4.140
        xor       ebx, ebx                                      ;
        mov       DWORD PTR [84+esp], esi                       ;
        mov       DWORD PTR [64+esp], eax                       ;
        xor       eax, eax                                      ;
        mov       DWORD PTR [52+esp], edx                       ;
                                ; LOE eax ecx ebx
.B4.142:                        ; Preds .B4.142 .B4.141
        mov       edx, DWORD PTR [80+esp]                       ;108.5
        mov       edi, DWORD PTR [76+esp]                       ;108.5
        mov       esi, edi                                      ;108.5
        lea       edx, DWORD PTR [edx+ebx*2]                    ;108.5
        inc       ebx                                           ;108.5
        imul      esi, edx                                      ;108.5
        inc       edx                                           ;108.5
        imul      edx, edi                                      ;108.5
        mov       WORD PTR [8+ecx+esi], ax                      ;108.5
        mov       WORD PTR [8+ecx+edx], ax                      ;108.5
        cmp       ebx, DWORD PTR [84+esp]                       ;108.5
        jb        .B4.142       ; Prob 63%                      ;108.5
                                ; LOE eax ecx ebx
.B4.143:                        ; Preds .B4.142
        mov       eax, DWORD PTR [64+esp]                       ;
        lea       edi, DWORD PTR [1+ebx+ebx]                    ;108.5
        mov       edx, DWORD PTR [52+esp]                       ;
                                ; LOE eax edx ecx edi
.B4.144:                        ; Preds .B4.143 .B4.189
        lea       ebx, DWORD PTR [-1+edi]                       ;108.5
        cmp       edx, ebx                                      ;108.5
        jbe       .B4.146       ; Prob 10%                      ;108.5
                                ; LOE eax edx ecx edi
.B4.145:                        ; Preds .B4.144
        mov       esi, DWORD PTR [80+esp]                       ;108.5
        xor       ebx, ebx                                      ;108.5
        lea       edi, DWORD PTR [-1+edi+esi]                   ;108.5
        imul      edi, DWORD PTR [76+esp]                       ;108.5
        mov       WORD PTR [8+edi+ecx], bx                      ;108.5
                                ; LOE eax edx ecx
.B4.146:                        ; Preds .B4.133 .B4.144 .B4.145
        cmp       DWORD PTR [76+esp], 1                         ;109.5
        jne       .B4.161       ; Prob 50%                      ;109.5
                                ; LOE eax edx ecx
.B4.147:                        ; Preds .B4.146
        test      edx, edx                                      ;109.5
        jle       .B4.168       ; Prob 50%                      ;109.5
                                ; LOE eax edx ecx
.B4.148:                        ; Preds .B4.147
        cmp       edx, 16                                       ;109.5
        jl        .B4.191       ; Prob 10%                      ;109.5
                                ; LOE eax edx ecx
.B4.149:                        ; Preds .B4.148
        lea       esi, DWORD PTR [10+eax]                       ;109.5
        and       esi, 15                                       ;109.5
        mov       ebx, esi                                      ;109.5
        neg       ebx                                           ;109.5
        add       ebx, 16                                       ;109.5
        test      esi, esi                                      ;109.5
        cmovne    esi, ebx                                      ;109.5
        lea       edi, DWORD PTR [16+esi]                       ;109.5
        cmp       edx, edi                                      ;109.5
        jl        .B4.191       ; Prob 10%                      ;109.5
                                ; LOE eax edx ecx esi
.B4.150:                        ; Preds .B4.149
        mov       edi, edx                                      ;109.5
        sub       edi, esi                                      ;109.5
        and       edi, 15                                       ;109.5
        neg       edi                                           ;109.5
        add       edi, edx                                      ;109.5
        test      esi, esi                                      ;109.5
        jbe       .B4.154       ; Prob 10%                      ;109.5
                                ; LOE eax edx ecx esi edi
.B4.151:                        ; Preds .B4.150
        xor       ebx, ebx                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B4.152:                        ; Preds .B4.152 .B4.151
        mov       BYTE PTR [10+ebx+eax], 0                      ;109.5
        inc       ebx                                           ;109.5
        cmp       ebx, esi                                      ;109.5
        jb        .B4.152       ; Prob 82%                      ;109.5
                                ; LOE eax edx ecx ebx esi edi
.B4.154:                        ; Preds .B4.152 .B4.150
        pxor      xmm0, xmm0                                    ;109.5
                                ; LOE eax edx ecx esi edi xmm0
.B4.155:                        ; Preds .B4.155 .B4.154
        movdqa    XMMWORD PTR [10+esi+eax], xmm0                ;109.5
        add       esi, 16                                       ;109.5
        cmp       esi, edi                                      ;109.5
        jb        .B4.155       ; Prob 82%                      ;109.5
                                ; LOE eax edx ecx esi edi xmm0
.B4.157:                        ; Preds .B4.155 .B4.191
        cmp       edi, edx                                      ;109.5
        jae       .B4.168       ; Prob 10%                      ;109.5
                                ; LOE eax edx ecx edi
.B4.159:                        ; Preds .B4.157 .B4.159
        mov       BYTE PTR [10+edi+eax], 0                      ;109.5
        inc       edi                                           ;109.5
        cmp       edi, edx                                      ;109.5
        jb        .B4.159       ; Prob 82%                      ;109.5
        jmp       .B4.168       ; Prob 100%                     ;109.5
                                ; LOE eax edx ecx edi
.B4.161:                        ; Preds .B4.146
        test      edx, edx                                      ;109.5
        jle       .B4.168       ; Prob 50%                      ;109.5
                                ; LOE edx ecx
.B4.162:                        ; Preds .B4.131 .B4.129 .B4.161
        mov       ebx, edx                                      ;109.5
        shr       ebx, 31                                       ;109.5
        add       ebx, edx                                      ;109.5
        sar       ebx, 1                                        ;109.5
        test      ebx, ebx                                      ;109.5
        jbe       .B4.193       ; Prob 10%                      ;109.5
                                ; LOE edx ecx ebx
.B4.163:                        ; Preds .B4.162
        mov       DWORD PTR [64+esp], ebx                       ;
        xor       eax, eax                                      ;
        mov       DWORD PTR [52+esp], edx                       ;
                                ; LOE eax ecx
.B4.164:                        ; Preds .B4.164 .B4.163
        mov       ebx, DWORD PTR [80+esp]                       ;109.5
        xor       edx, edx                                      ;109.5
        mov       edi, DWORD PTR [76+esp]                       ;109.5
        mov       esi, edi                                      ;109.5
        lea       ebx, DWORD PTR [ebx+eax*2]                    ;109.5
        inc       eax                                           ;109.5
        imul      esi, ebx                                      ;109.5
        inc       ebx                                           ;109.5
        imul      ebx, edi                                      ;109.5
        mov       BYTE PTR [10+ecx+esi], dl                     ;109.5
        mov       BYTE PTR [10+ecx+ebx], dl                     ;109.5
        cmp       eax, DWORD PTR [64+esp]                       ;109.5
        jb        .B4.164       ; Prob 63%                      ;109.5
                                ; LOE eax ecx
.B4.165:                        ; Preds .B4.164
        mov       edx, DWORD PTR [52+esp]                       ;
        lea       ebx, DWORD PTR [1+eax+eax]                    ;109.5
                                ; LOE edx ecx ebx
.B4.166:                        ; Preds .B4.165 .B4.193
        lea       eax, DWORD PTR [-1+ebx]                       ;109.5
        cmp       edx, eax                                      ;109.5
        jbe       .B4.168       ; Prob 10%                      ;109.5
                                ; LOE ecx ebx
.B4.167:                        ; Preds .B4.166
        mov       eax, DWORD PTR [80+esp]                       ;109.5
        lea       edx, DWORD PTR [-1+ebx+eax]                   ;109.5
        imul      edx, DWORD PTR [76+esp]                       ;109.5
        mov       BYTE PTR [10+edx+ecx], 0                      ;109.5
                                ; LOE ecx
.B4.168:                        ; Preds .B4.159 .B4.100 .B4.59 .B4.34 .B4.24
                                ;       .B4.147 .B4.161 .B4.157 .B4.166 .B4.167
                                ;      
        mov       eax, DWORD PTR [76+esp]                       ;110.5
        mov       BYTE PTR [10+ecx+eax], 1                      ;110.5
        add       esp, 100                                      ;111.1
        pop       ebx                                           ;111.1
        pop       edi                                           ;111.1
        pop       esi                                           ;111.1
        mov       esp, ebp                                      ;111.1
        pop       ebp                                           ;111.1
        ret                                                     ;111.1
                                ; LOE
.B4.170:                        ; Preds .B4.11 .B4.12           ; Infreq
        xor       ecx, ecx                                      ;103.5
        xor       esi, esi                                      ;103.5
        jmp       .B4.20        ; Prob 100%                     ;103.5
                                ; LOE eax edx esi
.B4.172:                        ; Preds .B4.26                  ; Infreq
        mov       esi, 1                                        ;
        jmp       .B4.30        ; Prob 100%                     ;
                                ; LOE eax edx ecx esi
.B4.173:                        ; Preds .B4.35 .B4.39 .B4.37    ; Infreq
        xor       ebx, ebx                                      ;104.5
        xor       esi, esi                                      ;104.5
        jmp       .B4.47        ; Prob 100%                     ;104.5
                                ; LOE eax edx ecx esi
.B4.176:                        ; Preds .B4.52                  ; Infreq
        mov       edi, 1                                        ;
        jmp       .B4.56        ; Prob 100%                     ;
                                ; LOE eax edx ecx edi
.B4.177:                        ; Preds .B4.73 .B4.74           ; Infreq
        xor       ebx, ebx                                      ;106.5
        xor       edi, edi                                      ;106.5
        jmp       .B4.82        ; Prob 100%                     ;106.5
                                ; LOE eax edx ecx edi
.B4.179:                        ; Preds .B4.60 .B4.61           ; Infreq
        xor       ebx, ebx                                      ;105.5
        xor       edi, edi                                      ;105.5
        jmp       .B4.69        ; Prob 100%                     ;105.5
                                ; LOE eax edx ecx edi
.B4.181:                        ; Preds .B4.93                  ; Infreq
        mov       esi, 1                                        ;
        jmp       .B4.97        ; Prob 100%                     ;
                                ; LOE eax edx ecx esi
.B4.182:                        ; Preds .B4.87                  ; Infreq
        mov       esi, 1                                        ;
        jmp       .B4.91        ; Prob 100%                     ;
                                ; LOE eax edx ecx esi
.B4.183:                        ; Preds .B4.101 .B4.105 .B4.103 ; Infreq
        xor       ebx, ebx                                      ;107.5
        xor       esi, esi                                      ;107.5
        jmp       .B4.113       ; Prob 100%                     ;107.5
                                ; LOE eax edx ecx esi
.B4.184:                        ; Preds .B4.117 .B4.121 .B4.119 ; Infreq
        xor       ebx, ebx                                      ;108.5
        xor       esi, esi                                      ;108.5
        jmp       .B4.129       ; Prob 100%                     ;108.5
                                ; LOE eax edx ecx esi
.B4.189:                        ; Preds .B4.140                 ; Infreq
        mov       edi, 1                                        ;
        jmp       .B4.144       ; Prob 100%                     ;
                                ; LOE eax edx ecx edi
.B4.190:                        ; Preds .B4.134                 ; Infreq
        mov       edi, 1                                        ;
        jmp       .B4.138       ; Prob 100%                     ;
                                ; LOE eax edx ecx edi
.B4.191:                        ; Preds .B4.148 .B4.149         ; Infreq
        xor       ebx, ebx                                      ;109.5
        xor       edi, edi                                      ;109.5
        jmp       .B4.157       ; Prob 100%                     ;109.5
                                ; LOE eax edx ecx edi
.B4.193:                        ; Preds .B4.162                 ; Infreq
        mov       ebx, 1                                        ;
        jmp       .B4.166       ; Prob 100%                     ;
                                ; LOE edx ecx ebx
.B4.194:                        ; Preds .B4.8                   ; Infreq
        mov       DWORD PTR [esp], 0                            ;100.9
        lea       ebx, DWORD PTR [esp]                          ;100.9
        mov       DWORD PTR [32+esp], 55                        ;100.9
        lea       eax, DWORD PTR [32+esp]                       ;100.9
        mov       DWORD PTR [36+esp], OFFSET FLAT: __STRLITPACK_31 ;100.9
        push      32                                            ;100.9
        push      eax                                           ;100.9
        push      OFFSET FLAT: __STRLITPACK_41.0.4              ;100.9
        push      -2088435968                                   ;100.9
        push      911                                           ;100.9
        push      ebx                                           ;100.9
        call      _for_write_seq_lis                            ;100.9
                                ; LOE ebx
.B4.195:                        ; Preds .B4.194                 ; Infreq
        mov       DWORD PTR [24+esp], 0                         ;101.6
        lea       eax, DWORD PTR [64+esp]                       ;101.6
        mov       DWORD PTR [64+esp], 13                        ;101.6
        mov       DWORD PTR [68+esp], OFFSET FLAT: __STRLITPACK_29 ;101.6
        push      32                                            ;101.6
        push      eax                                           ;101.6
        push      OFFSET FLAT: __STRLITPACK_42.0.4              ;101.6
        push      -2088435968                                   ;101.6
        push      911                                           ;101.6
        push      ebx                                           ;101.6
        call      _for_write_seq_lis                            ;101.6
                                ; LOE ebx
.B4.196:                        ; Preds .B4.195                 ; Infreq
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE+32] ;101.36
        lea       edi, DWORD PTR [96+esp]                       ;101.6
        neg       eax                                           ;101.6
        add       eax, DWORD PTR [112+esp]                      ;101.6
        imul      ecx, eax, 44                                  ;101.6
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE] ;101.6
        mov       esi, DWORD PTR [36+edx+ecx]                   ;101.6
        mov       DWORD PTR [96+esp], esi                       ;101.6
        push      edi                                           ;101.6
        push      OFFSET FLAT: __STRLITPACK_43.0.4              ;101.6
        push      ebx                                           ;101.6
        call      _for_write_seq_lis_xmit                       ;101.6
                                ; LOE
.B4.203:                        ; Preds .B4.196                 ; Infreq
        add       esp, 60                                       ;101.6
        jmp       .B4.9         ; Prob 100%                     ;101.6
        ALIGN     16
                                ; LOE
; mark_end;
_DYNUST_SIGNAL_MODULE_mp_DECLAREPHASE ENDP
_TEXT	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_39.0.4	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_41.0.4	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_42.0.4	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_43.0.4	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_SIGNAL_MODULE_mp_DECLAREPHASE
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_SIGNAL_MODULE_mp_INSERTSIGNALPHASE
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_SIGNAL_MODULE_mp_INSERTSIGNALPHASE
_DYNUST_SIGNAL_MODULE_mp_INSERTSIGNALPHASE	PROC NEAR 
; parameter 1: 8 + ebp
; parameter 2: 12 + ebp
.B5.1:                          ; Preds .B5.0
        push      ebp                                           ;115.12
        mov       ebp, esp                                      ;115.12
        and       esp, -16                                      ;115.12
        push      esi                                           ;115.12
        push      edi                                           ;115.12
        push      ebx                                           ;115.12
        sub       esp, 52                                       ;115.12
        mov       eax, DWORD PTR [8+ebp]                        ;115.12
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM+32] ;121.14
        neg       ebx                                           ;121.5
        mov       edx, DWORD PTR [eax]                          ;121.14
        add       ebx, edx                                      ;121.5
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM] ;121.5
        imul      esi, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY+32], -48 ;122.22
        mov       edi, DWORD PTR [ecx+ebx*4]                    ;121.5
        add       esi, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY] ;122.22
        mov       ebx, DWORD PTR [4+eax]                        ;122.8
        lea       edi, DWORD PTR [edi+edi*2]                    ;121.5
        shl       edi, 4                                        ;121.5
        movsx     ecx, BYTE PTR [1+edi+esi]                     ;122.24
        cmp       ebx, ecx                                      ;122.22
        jle       .B5.6         ; Prob 50%                      ;122.22
                                ; LOE eax edx ebx esi edi
.B5.2:                          ; Preds .B5.1
        mov       DWORD PTR [esp], 0                            ;123.9
        lea       ecx, DWORD PTR [32+esp]                       ;123.9
        mov       DWORD PTR [32+esp], 59                        ;123.9
        mov       DWORD PTR [36+esp], OFFSET FLAT: __STRLITPACK_27 ;123.9
        push      32                                            ;123.9
        push      ecx                                           ;123.9
        push      OFFSET FLAT: __STRLITPACK_44.0.5              ;123.9
        push      -2088435968                                   ;123.9
        push      911                                           ;123.9
        lea       ecx, DWORD PTR [20+esp]                       ;123.9
        push      ecx                                           ;123.9
        mov       DWORD PTR [64+esp], edx                       ;123.9
        call      _for_write_seq_lis                            ;123.9
                                ; LOE ebx esi edi
.B5.3:                          ; Preds .B5.2
        mov       edx, DWORD PTR [64+esp]                       ;
        mov       DWORD PTR [72+esp], edx                       ;123.9
        lea       edx, DWORD PTR [72+esp]                       ;123.9
        push      edx                                           ;123.9
        push      OFFSET FLAT: __STRLITPACK_45.0.5              ;123.9
        mov       eax, DWORD PTR [8+ebp]                        ;
        lea       ecx, DWORD PTR [32+esp]                       ;123.9
        push      ecx                                           ;123.9
        call      _for_write_seq_lis_xmit                       ;123.9
                                ; LOE ebx esi edi
.B5.4:                          ; Preds .B5.3
        mov       DWORD PTR [36+esp], 0                         ;124.9
        lea       edx, DWORD PTR [76+esp]                       ;124.9
        mov       DWORD PTR [76+esp], 24                        ;124.9
        mov       DWORD PTR [80+esp], OFFSET FLAT: __STRLITPACK_25 ;124.9
        push      32                                            ;124.9
        push      edx                                           ;124.9
        push      OFFSET FLAT: __STRLITPACK_46.0.5              ;124.9
        push      -2088435968                                   ;124.9
        push      911                                           ;124.9
        mov       eax, DWORD PTR [8+ebp]                        ;
        lea       ecx, DWORD PTR [56+esp]                       ;124.9
        push      ecx                                           ;124.9
        call      _for_write_seq_lis                            ;124.9
                                ; LOE ebx esi edi
.B5.5:                          ; Preds .B5.4
        push      32                                            ;125.6
        xor       edx, edx                                      ;125.6
        push      edx                                           ;125.6
        push      edx                                           ;125.6
        push      -2088435968                                   ;125.6
        push      edx                                           ;125.6
        mov       eax, DWORD PTR [8+ebp]                        ;
        push      OFFSET FLAT: __STRLITPACK_47                  ;125.6
        call      _for_stop_core                                ;125.6
                                ; LOE ebx esi edi
.B5.9:                          ; Preds .B5.5
        mov       eax, DWORD PTR [8+ebp]                        ;
        add       esp, 84                                       ;125.6
                                ; LOE eax ebx esi edi
.B5.6:                          ; Preds .B5.9 .B5.1
        movss     xmm3, DWORD PTR [_2il0floatpacket.7]          ;129.56
        mulss     xmm3, DWORD PTR [_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT] ;129.56
        movss     xmm0, DWORD PTR [_2il0floatpacket.8]          ;129.56
        andps     xmm0, xmm3                                    ;129.56
        pxor      xmm3, xmm0                                    ;129.56
        movss     xmm1, DWORD PTR [_2il0floatpacket.9]          ;129.56
        movaps    xmm2, xmm3                                    ;129.56
        movss     xmm4, DWORD PTR [_2il0floatpacket.10]         ;129.56
        cmpltss   xmm2, xmm1                                    ;129.56
        andps     xmm1, xmm2                                    ;129.56
        movaps    xmm2, xmm3                                    ;129.56
        movaps    xmm6, xmm4                                    ;129.56
        addss     xmm2, xmm1                                    ;129.56
        addss     xmm6, xmm4                                    ;129.56
        subss     xmm2, xmm1                                    ;129.56
        movaps    xmm7, xmm2                                    ;129.56
        mov       ecx, DWORD PTR [44+edi+esi]                   ;128.5
        subss     xmm7, xmm3                                    ;129.56
        movaps    xmm5, xmm7                                    ;129.56
        neg       ecx                                           ;128.5
        add       ecx, ebx                                      ;128.5
        cmpnless  xmm5, xmm4                                    ;129.56
        imul      ecx, DWORD PTR [40+edi+esi]                   ;128.5
        cmpless   xmm7, DWORD PTR [_2il0floatpacket.11]         ;129.56
        andps     xmm5, xmm6                                    ;129.56
        andps     xmm7, xmm6                                    ;129.56
        mov       edx, DWORD PTR [12+edi+esi]                   ;128.5
        subss     xmm2, xmm5                                    ;129.56
        mov       ebx, DWORD PTR [20+eax]                       ;128.52
        addss     xmm2, xmm7                                    ;129.56
        orps      xmm2, xmm0                                    ;129.56
        mov       BYTE PTR [edx+ecx], bl                        ;128.5
        cvtss2si  ebx, xmm2                                     ;129.79
        mov       esi, DWORD PTR [8+eax]                        ;129.79
        cmp       esi, ebx                                      ;129.5
        mov       edi, DWORD PTR [12+eax]                       ;130.79
        cmovl     esi, ebx                                      ;129.5
        cmp       edi, ebx                                      ;130.5
        mov       eax, DWORD PTR [16+eax]                       ;131.52
        cmovge    ebx, edi                                      ;130.5
        mov       WORD PTR [2+edx+ecx], si                      ;129.5
        mov       BYTE PTR [4+edx+ecx], bl                      ;130.5
        mov       BYTE PTR [5+edx+ecx], al                      ;131.5
        add       esp, 52                                       ;132.1
        pop       ebx                                           ;132.1
        pop       edi                                           ;132.1
        pop       esi                                           ;132.1
        mov       esp, ebp                                      ;132.1
        pop       ebp                                           ;132.1
        ret                                                     ;132.1
        ALIGN     16
                                ; LOE
; mark_end;
_DYNUST_SIGNAL_MODULE_mp_INSERTSIGNALPHASE ENDP
_TEXT	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_44.0.5	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_45.0.5	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_46.0.5	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_SIGNAL_MODULE_mp_INSERTSIGNALPHASE
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_SIGNAL_MODULE_mp_DECLAREPHASEMOVE
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_SIGNAL_MODULE_mp_DECLAREPHASEMOVE
_DYNUST_SIGNAL_MODULE_mp_DECLAREPHASEMOVE	PROC NEAR 
; parameter 1: 8 + ebp
; parameter 2: 12 + ebp
.B6.1:                          ; Preds .B6.0
        push      ebp                                           ;135.12
        mov       ebp, esp                                      ;135.12
        and       esp, -16                                      ;135.12
        push      esi                                           ;135.12
        push      edi                                           ;135.12
        push      ebx                                           ;135.12
        sub       esp, 52                                       ;135.12
        mov       eax, DWORD PTR [8+ebp]                        ;135.12
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM+32] ;143.14
        neg       ecx                                           ;143.5
        cvtsi2ss  xmm0, DWORD PTR [4+eax]                       ;142.5
        add       ecx, DWORD PTR [eax]                          ;143.5
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM] ;143.5
        mov       ebx, DWORD PTR [20+eax]                       ;141.5
        xor       eax, eax                                      ;144.5
        test      ebx, ebx                                      ;144.5
        movss     DWORD PTR [esp], xmm0                         ;142.5
        cmovle    ebx, eax                                      ;144.5
        mov       esi, DWORD PTR [edx+ecx*4]                    ;143.5
        lea       edx, DWORD PTR [40+esp]                       ;144.5
        push      44                                            ;144.5
        push      ebx                                           ;144.5
        push      2                                             ;144.5
        push      edx                                           ;144.5
        call      _for_check_mult_overflow                      ;144.5
                                ; LOE eax ebx esi
.B6.2:                          ; Preds .B6.1
        imul      edx, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY+32], -48 ;144.5
        lea       esi, DWORD PTR [esi+esi*2]                    ;143.5
        cvttss2si edi, DWORD PTR [16+esp]                       ;144.14
        shl       esi, 4                                        ;143.5
        and       eax, 1                                        ;144.5
        add       edx, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY] ;144.5
        shl       eax, 4                                        ;144.5
        or        eax, 262145                                   ;144.5
        mov       ecx, DWORD PTR [44+esi+edx]                   ;144.5
        neg       ecx                                           ;144.5
        add       ecx, edi                                      ;144.5
        imul      ecx, DWORD PTR [40+esi+edx]                   ;144.5
        mov       edx, DWORD PTR [12+esi+edx]                   ;144.5
        push      eax                                           ;144.5
        lea       ecx, DWORD PTR [12+ecx+edx]                   ;144.5
        push      ecx                                           ;144.5
        push      DWORD PTR [64+esp]                            ;144.5
        call      _for_allocate                                 ;144.5
                                ; LOE eax ebx esi edi
.B6.11:                         ; Preds .B6.2
        add       esp, 28                                       ;144.5
                                ; LOE eax ebx esi edi
.B6.3:                          ; Preds .B6.11
        test      eax, eax                                      ;144.5
        jne       .B6.5         ; Prob 50%                      ;144.5
                                ; LOE ebx esi edi
.B6.4:                          ; Preds .B6.3
        imul      ecx, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY+32], -48 ;144.5
        add       ecx, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY] ;144.5
        mov       eax, DWORD PTR [44+esi+ecx]                   ;144.14
        mov       edx, DWORD PTR [40+esi+ecx]                   ;144.14
        imul      eax, edx                                      ;144.5
        imul      edx, edi                                      ;144.14
        neg       eax                                           ;144.5
        mov       edi, 44                                       ;144.5
        add       edx, DWORD PTR [12+esi+ecx]                   ;144.5
        mov       esi, 1                                        ;144.5
        mov       DWORD PTR [24+eax+edx], 5                     ;144.5
        mov       DWORD PTR [16+eax+edx], edi                   ;144.5
        mov       DWORD PTR [28+eax+edx], esi                   ;144.5
        mov       DWORD PTR [20+eax+edx], 0                     ;144.5
        mov       DWORD PTR [44+eax+edx], esi                   ;144.5
        mov       DWORD PTR [36+eax+edx], ebx                   ;144.5
        mov       DWORD PTR [40+eax+edx], edi                   ;144.5
        lea       eax, DWORD PTR [esp]                          ;144.5
        push      edi                                           ;144.5
        push      ebx                                           ;144.5
        push      2                                             ;144.5
        push      eax                                           ;144.5
        call      _for_check_mult_overflow                      ;144.5
                                ; LOE
.B6.12:                         ; Preds .B6.4
        add       esp, 68                                       ;144.5
        pop       ebx                                           ;144.5
        pop       edi                                           ;144.5
        pop       esi                                           ;144.5
        mov       esp, ebp                                      ;144.5
        pop       ebp                                           ;144.5
        ret                                                     ;144.5
                                ; LOE
.B6.5:                          ; Preds .B6.3
        mov       DWORD PTR [esp], 0                            ;146.9
        lea       edx, DWORD PTR [esp]                          ;146.9
        mov       DWORD PTR [32+esp], 45                        ;146.9
        lea       eax, DWORD PTR [32+esp]                       ;146.9
        mov       DWORD PTR [36+esp], OFFSET FLAT: __STRLITPACK_23 ;146.9
        push      32                                            ;146.9
        push      eax                                           ;146.9
        push      OFFSET FLAT: __STRLITPACK_48.0.6              ;146.9
        push      -2088435968                                   ;146.9
        push      911                                           ;146.9
        push      edx                                           ;146.9
        call      _for_write_seq_lis                            ;146.9
                                ; LOE
.B6.6:                          ; Preds .B6.5
        push      32                                            ;147.9
        xor       eax, eax                                      ;147.9
        push      eax                                           ;147.9
        push      eax                                           ;147.9
        push      -2088435968                                   ;147.9
        push      eax                                           ;147.9
        push      OFFSET FLAT: __STRLITPACK_49                  ;147.9
        call      _for_stop_core                                ;147.9
                                ; LOE
.B6.7:                          ; Preds .B6.6
        add       esp, 100                                      ;149.1
        pop       ebx                                           ;149.1
        pop       edi                                           ;149.1
        pop       esi                                           ;149.1
        mov       esp, ebp                                      ;149.1
        pop       ebp                                           ;149.1
        ret                                                     ;149.1
        ALIGN     16
                                ; LOE
; mark_end;
_DYNUST_SIGNAL_MODULE_mp_DECLAREPHASEMOVE ENDP
_TEXT	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_48.0.6	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_SIGNAL_MODULE_mp_DECLAREPHASEMOVE
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_SIGNAL_MODULE_mp_INSERTPHASEMOVE
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_SIGNAL_MODULE_mp_INSERTPHASEMOVE
_DYNUST_SIGNAL_MODULE_mp_INSERTPHASEMOVE	PROC NEAR 
; parameter 1: 8 + ebp
; parameter 2: 12 + ebp
; parameter 3: 16 + ebp
.B7.1:                          ; Preds .B7.0
        push      ebp                                           ;153.12
        mov       ebp, esp                                      ;153.12
        and       esp, -16                                      ;153.12
        push      esi                                           ;153.12
        push      edi                                           ;153.12
        push      ebx                                           ;153.12
        sub       esp, 196                                      ;153.12
        mov       eax, DWORD PTR [8+ebp]                        ;153.12
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM] ;159.5
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM+32] ;159.14
        mov       edx, DWORD PTR [4+eax]                        ;159.14
        shl       ecx, 2                                        ;159.5
        mov       DWORD PTR [128+esp], edx                      ;159.14
        mov       DWORD PTR [esp], ecx                          ;159.5
        lea       ebx, DWORD PTR [esi+edx*4]                    ;159.5
        sub       ebx, ecx                                      ;159.5
        mov       edx, DWORD PTR [8+eax]                        ;160.5
        mov       edi, DWORD PTR [eax]                          ;168.28
        mov       DWORD PTR [140+esp], edx                      ;160.5
        mov       ebx, DWORD PTR [ebx]                          ;159.5
        test      ebx, ebx                                      ;161.15
        jle       .B7.40        ; Prob 16%                      ;161.15
                                ; LOE ebx esi edi
.B7.2:                          ; Preds .B7.1
        lea       eax, DWORD PTR [esi+edi*4]                    ;161.18
        sub       eax, DWORD PTR [esp]                          ;161.18
        cmp       DWORD PTR [eax], 0                            ;161.52
        jle       .B7.40        ; Prob 16%                      ;161.52
                                ; LOE ebx esi edi
.B7.3:                          ; Preds .B7.2 .B7.53
        sub       esi, DWORD PTR [esp]                          ;168.11
        mov       eax, DWORD PTR [128+esp]                      ;168.11
        push      OFFSET FLAT: __NLITPACK_0.0.7                 ;168.11
        lea       edx, DWORD PTR [esi+edi*4]                    ;168.11
        lea       esi, DWORD PTR [esi+eax*4]                    ;168.11
        push      esi                                           ;168.11
        push      edx                                           ;168.11
        call      _DYNUST_NETWORK_MODULE_mp_GETFLINKFROMNODE    ;168.11
                                ; LOE eax ebx
.B7.48:                         ; Preds .B7.3
        add       esp, 12                                       ;168.11
        mov       esi, eax                                      ;168.11
                                ; LOE ebx esi
.B7.4:                          ; Preds .B7.48
        mov       eax, DWORD PTR [16+ebp]                       ;153.12
        lea       ebx, DWORD PTR [ebx+ebx*2]                    ;159.5
        shl       ebx, 4                                        ;159.5
        mov       ecx, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY] ;169.5
        mov       edi, DWORD PTR [eax]                          ;169.5
        add       ecx, ebx                                      ;169.5
        imul      eax, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY+32], -48 ;169.5
        mov       edx, DWORD PTR [44+eax+ecx]                   ;169.5
        neg       edx                                           ;169.5
        add       edx, DWORD PTR [140+esp]                      ;169.5
        imul      edx, DWORD PTR [40+eax+ecx]                   ;169.5
        mov       ecx, DWORD PTR [12+eax+ecx]                   ;169.5
        mov       DWORD PTR [60+esp], ebx                       ;159.5
        mov       DWORD PTR [144+esp], edi                      ;169.5
        mov       ebx, DWORD PTR [44+ecx+edx]                   ;169.5
        neg       ebx                                           ;169.5
        add       ebx, edi                                      ;169.5
        imul      ebx, DWORD PTR [40+ecx+edx]                   ;169.5
        mov       edi, DWORD PTR [8+ebp]                        ;170.60
        mov       eax, DWORD PTR [12+ecx+edx]                   ;169.5
        mov       edx, DWORD PTR [12+edi]                       ;170.60
        test      edx, edx                                      ;171.5
        mov       DWORD PTR [eax+ebx], esi                      ;169.5
        mov       DWORD PTR [136+esp], edx                      ;170.60
        mov       BYTE PTR [4+eax+ebx], dl                      ;170.5
        jle       .B7.39        ; Prob 2%                       ;171.5
                                ; LOE esi
.B7.5:                          ; Preds .B7.4
        imul      ebx, esi, 152                                 ;168.5
        xor       ecx, ecx                                      ;
        mov       DWORD PTR [112+esp], ebx                      ;168.5
        mov       ebx, esi                                      ;168.5
        shl       ebx, 5                                        ;168.5
        lea       edi, DWORD PTR [esi*4]                        ;168.5
        sub       ebx, edi                                      ;168.5
        mov       DWORD PTR [172+esp], 1                        ;
        mov       DWORD PTR [116+esp], ecx                      ;
        mov       DWORD PTR [192+esp], ecx                      ;
        mov       DWORD PTR [76+esp], ecx                       ;
        mov       DWORD PTR [esp], ebx                          ;168.5
        mov       DWORD PTR [32+esp], ecx                       ;168.5
        mov       DWORD PTR [40+esp], ecx                       ;168.5
        mov       DWORD PTR [36+esp], ecx                       ;168.5
        mov       DWORD PTR [4+esp], esi                        ;168.5
                                ; LOE
.B7.6:                          ; Preds .B7.23 .B7.5
        mov       ebx, DWORD PTR [8+ebp]                        ;172.18
        mov       esi, DWORD PTR [172+esp]                      ;172.18
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM+32] ;172.35
        shl       edx, 2                                        ;172.18
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM] ;172.18
        sub       eax, edx                                      ;172.18
        mov       ecx, DWORD PTR [128+esp]                      ;172.18
        mov       edi, DWORD PTR [12+ebx+esi*4]                 ;172.18
        push      OFFSET FLAT: __NLITPACK_0.0.7                 ;172.18
        lea       edx, DWORD PTR [eax+ecx*4]                    ;172.18
        lea       eax, DWORD PTR [eax+edi*4]                    ;172.18
        push      eax                                           ;172.18
        push      edx                                           ;172.18
        call      _DYNUST_NETWORK_MODULE_mp_GETFLINKFROMNODE    ;172.18
                                ; LOE eax
.B7.49:                         ; Preds .B7.6
        add       esp, 12                                       ;172.18
        mov       DWORD PTR [180+esp], eax                      ;172.18
                                ; LOE
.B7.7:                          ; Preds .B7.49
        imul      eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32], -152 ;175.6
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;175.6
        add       edx, DWORD PTR [112+esp]                      ;175.6
        mov       DWORD PTR [48+esp], edx                       ;175.6
        mov       DWORD PTR [44+esp], eax                       ;175.6
        movsx     ecx, BYTE PTR [32+eax+edx]                    ;175.19
        test      ecx, ecx                                      ;175.6
        mov       DWORD PTR [184+esp], ecx                      ;175.19
        jle       .B7.38        ; Prob 0%                       ;175.6
                                ; LOE ecx cl ch
.B7.8:                          ; Preds .B7.7
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE] ;194.9
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+44] ;194.12
        mov       DWORD PTR [76+esp], ebx                       ;194.9
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+32] ;194.12
        mov       DWORD PTR [32+esp], esi                       ;194.12
        mov       esi, ecx                                      ;175.6
        mov       DWORD PTR [40+esp], ebx                       ;194.12
        mov       ebx, esi                                      ;175.6
        shr       ebx, 31                                       ;175.6
        add       ebx, esi                                      ;175.6
        mov       eax, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY] ;196.13
        mov       edx, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY+32] ;196.13
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE] ;176.13
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+40] ;194.12
        sar       ebx, 1                                        ;175.6
        mov       DWORD PTR [36+esp], eax                       ;196.13
        test      ebx, ebx                                      ;175.6
        mov       DWORD PTR [116+esp], edx                      ;196.13
        mov       DWORD PTR [52+esp], ecx                       ;176.13
        mov       DWORD PTR [192+esp], edi                      ;194.12
        mov       edi, 0                                        ;
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+44] ;176.16
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+40] ;176.16
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+32] ;176.16
        mov       DWORD PTR [152+esp], ebx                      ;175.6
        mov       DWORD PTR [132+esp], edi                      ;
        jbe       .B7.37        ; Prob 3%                       ;175.6
                                ; LOE eax edx ecx
.B7.9:                          ; Preds .B7.8
        mov       edi, DWORD PTR [4+esp]                        ;
        xor       ebx, ebx                                      ;
        mov       esi, DWORD PTR [52+esp]                       ;
        mov       DWORD PTR [68+esp], eax                       ;
        shl       eax, 3                                        ;
        mov       DWORD PTR [72+esp], edx                       ;
        lea       esi, DWORD PTR [esi+edi*8]                    ;
        mov       edi, edx                                      ;
        imul      edi, ecx                                      ;
        sub       esi, edi                                      ;
        mov       edi, ecx                                      ;
        sub       edi, eax                                      ;
        add       edi, esi                                      ;
        mov       DWORD PTR [156+esp], edi                      ;
        lea       edi, DWORD PTR [ecx+ecx]                      ;
        sub       edi, eax                                      ;
        add       esi, edi                                      ;
        mov       edx, DWORD PTR [132+esp]                      ;
        mov       eax, ebx                                      ;
        mov       DWORD PTR [168+esp], ecx                      ;
                                ; LOE eax edx ebx esi
.B7.10:                         ; Preds .B7.10 .B7.9
        mov       ecx, DWORD PTR [156+esp]                      ;176.66
        lea       edi, DWORD PTR [1+eax+eax]                    ;176.66
        cmp       BYTE PTR [5+ecx+ebx*2], 6                     ;176.66
        lea       ecx, DWORD PTR [2+eax+eax]                    ;176.66
        jne       L8            ; Prob 50%                      ;176.66
        mov       edx, edi                                      ;176.66
L8:                                                             ;
        cmp       BYTE PTR [5+esi+ebx*2], 6                     ;176.66
        cmove     edx, ecx                                      ;176.66
        inc       eax                                           ;175.6
        add       ebx, DWORD PTR [168+esp]                      ;175.6
        cmp       eax, DWORD PTR [152+esp]                      ;175.6
        jb        .B7.10        ; Prob 64%                      ;175.6
                                ; LOE eax edx ebx esi
.B7.11:                         ; Preds .B7.10
        mov       ebx, eax                                      ;175.6
        mov       DWORD PTR [132+esp], edx                      ;
        mov       eax, DWORD PTR [68+esp]                       ;
        mov       ecx, DWORD PTR [168+esp]                      ;
        lea       ebx, DWORD PTR [1+ebx+ebx]                    ;175.6
        mov       edx, DWORD PTR [72+esp]                       ;
                                ; LOE eax edx ecx ebx
.B7.12:                         ; Preds .B7.11 .B7.37
        lea       esi, DWORD PTR [-1+ebx]                       ;175.6
        cmp       esi, DWORD PTR [184+esp]                      ;175.6
        jae       .B7.55        ; Prob 3%                       ;175.6
                                ; LOE eax edx ecx ebx
.B7.13:                         ; Preds .B7.12
        neg       edx                                           ;176.16
        add       edx, ebx                                      ;176.16
        imul      edx, ecx                                      ;176.16
        mov       esi, DWORD PTR [4+esp]                        ;176.16
        mov       ecx, DWORD PTR [52+esp]                       ;176.16
        shl       eax, 3                                        ;176.16
        lea       edi, DWORD PTR [ecx+esi*8]                    ;176.16
        sub       edi, eax                                      ;176.16
        mov       eax, DWORD PTR [132+esp]                      ;176.66
        cmp       BYTE PTR [5+edi+edx], 6                       ;176.66
        cmove     eax, ebx                                      ;176.66
        mov       DWORD PTR [132+esp], eax                      ;176.66
        cmp       DWORD PTR [184+esp], 0                        ;176.66
                                ; LOE
.B7.14:                         ; Preds .B7.13 .B7.38 .B7.55
        mov       eax, 0                                        ;
        jle       .B7.20        ; Prob 3%                       ;175.6
                                ; LOE eax
.B7.15:                         ; Preds .B7.14
        mov       edi, DWORD PTR [44+esp]                       ;177.13
        xor       ebx, ebx                                      ;
        mov       ecx, DWORD PTR [48+esp]                       ;177.13
        mov       edx, DWORD PTR [36+edi+ecx]                   ;177.13
        mov       esi, DWORD PTR [68+edi+ecx]                   ;177.13
        mov       edi, DWORD PTR [64+edi+ecx]                   ;177.13
        imul      esi, edi                                      ;
        mov       ecx, DWORD PTR [36+esp]                       ;
        sub       edx, esi                                      ;
        mov       esi, DWORD PTR [60+esp]                       ;
        mov       DWORD PTR [176+esp], edi                      ;177.13
        imul      edi, DWORD PTR [116+esp], -48                 ;
        mov       DWORD PTR [148+esp], edi                      ;
        add       esi, ecx                                      ;
        mov       DWORD PTR [160+esp], esi                      ;
        mov       ecx, DWORD PTR [esp]                          ;
        mov       esi, DWORD PTR [76+esp]                       ;
        mov       edi, DWORD PTR [32+esp]                       ;
        mov       DWORD PTR [188+esp], edx                      ;
        add       esi, ecx                                      ;
        mov       ecx, DWORD PTR [192+esp]                      ;
        imul      edi, ecx                                      ;
        sub       esi, edi                                      ;
        imul      edi, DWORD PTR [40+esp], -28                  ;
        add       edi, ecx                                      ;
        xor       ecx, ecx                                      ;
        add       esi, edi                                      ;
        mov       DWORD PTR [164+esp], esi                      ;
                                ; LOE eax ecx ebx
.B7.16:                         ; Preds .B7.18 .B7.15
        inc       ebx                                           ;177.57
        mov       edi, DWORD PTR [176+esp]                      ;177.57
        imul      edi, ebx                                      ;177.57
        mov       esi, DWORD PTR [188+esp]                      ;177.57
        mov       edx, DWORD PTR [180+esp]                      ;177.57
        cmp       edx, DWORD PTR [esi+edi]                      ;177.57
        jne       .B7.18        ; Prob 50%                      ;177.57
                                ; LOE eax ecx ebx
.B7.17:                         ; Preds .B7.16
        mov       edi, DWORD PTR [148+esp]                      ;179.11
        mov       eax, DWORD PTR [160+esp]                      ;179.11
        mov       esi, DWORD PTR [44+edi+eax]                   ;179.11
        neg       esi                                           ;179.11
        add       esi, DWORD PTR [140+esp]                      ;179.11
        imul      esi, DWORD PTR [40+edi+eax]                   ;179.11
        mov       edi, DWORD PTR [12+edi+eax]                   ;179.11
        mov       eax, DWORD PTR [44+edi+esi]                   ;179.11
        neg       eax                                           ;179.11
        add       eax, DWORD PTR [144+esp]                      ;179.11
        imul      eax, DWORD PTR [40+edi+esi]                   ;179.11
        mov       edx, DWORD PTR [12+edi+esi]                   ;179.11
        mov       esi, DWORD PTR [172+esp]                      ;179.11
        sub       esi, DWORD PTR [40+edx+eax]                   ;179.11
        imul      esi, DWORD PTR [36+edx+eax]                   ;179.11
        mov       edx, DWORD PTR [8+edx+eax]                    ;179.11
        mov       eax, DWORD PTR [164+esp]                      ;180.17
        mov       DWORD PTR [edx+esi], ebx                      ;179.11
        mov       BYTE PTR [9+ecx+eax], 0                       ;180.17
        mov       eax, -1                                       ;
                                ; LOE eax ecx ebx
.B7.18:                         ; Preds .B7.17 .B7.16
        add       ecx, DWORD PTR [192+esp]                      ;175.6
        cmp       ebx, DWORD PTR [184+esp]                      ;175.6
        jb        .B7.16        ; Prob 82%                      ;175.6
                                ; LOE eax ecx ebx
.B7.20:                         ; Preds .B7.18 .B7.14
        test      al, 1                                         ;183.14
        je        .B7.22        ; Prob 60%                      ;183.14
                                ; LOE
.B7.21:                         ; Preds .B7.20
        cmp       DWORD PTR [180+esp], 0                        ;183.31
        jg        .B7.23        ; Prob 84%                      ;183.31
                                ; LOE
.B7.22:                         ; Preds .B7.20 .B7.21
        mov       DWORD PTR [80+esp], 0                         ;184.13
        lea       edx, DWORD PTR [80+esp]                       ;184.13
        mov       DWORD PTR [120+esp], 39                       ;184.13
        lea       eax, DWORD PTR [120+esp]                      ;184.13
        mov       DWORD PTR [124+esp], OFFSET FLAT: __STRLITPACK_16 ;184.13
        push      32                                            ;184.13
        push      eax                                           ;184.13
        push      OFFSET FLAT: __STRLITPACK_56.0.7              ;184.13
        push      -2088435968                                   ;184.13
        push      -1                                            ;184.13
        push      edx                                           ;184.13
        call      _for_write_seq_lis                            ;184.13
                                ; LOE
.B7.50:                         ; Preds .B7.22
        add       esp, 24                                       ;184.13
                                ; LOE
.B7.23:                         ; Preds .B7.50 .B7.21
        mov       eax, DWORD PTR [172+esp]                      ;186.5
        inc       eax                                           ;186.5
        mov       DWORD PTR [172+esp], eax                      ;186.5
        cmp       eax, DWORD PTR [136+esp]                      ;186.5
        jle       .B7.6         ; Prob 82%                      ;186.5
                                ; LOE
.B7.24:                         ; Preds .B7.23
        mov       ebx, DWORD PTR [132+esp]                      ;
        mov       esi, DWORD PTR [4+esp]                        ;
                                ; LOE ebx esi
.B7.25:                         ; Preds .B7.24 .B7.39
        cmp       ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_MAXMOVE] ;188.14
        jle       .B7.32        ; Prob 50%                      ;188.14
                                ; LOE ebx esi
.B7.26:                         ; Preds .B7.25
        mov       DWORD PTR [80+esp], 0                         ;189.9
        lea       edi, DWORD PTR [80+esp]                       ;189.9
        mov       DWORD PTR [32+esp], 33                        ;189.9
        lea       eax, DWORD PTR [32+esp]                       ;189.9
        mov       DWORD PTR [36+esp], OFFSET FLAT: __STRLITPACK_13 ;189.9
        push      32                                            ;189.9
        push      eax                                           ;189.9
        push      OFFSET FLAT: __STRLITPACK_57.0.7              ;189.9
        push      -2088435968                                   ;189.9
        push      911                                           ;189.9
        push      edi                                           ;189.9
        call      _for_write_seq_lis                            ;189.9
                                ; LOE ebx esi edi
.B7.27:                         ; Preds .B7.26
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM+32] ;189.9
        shl       eax, 2                                        ;189.9
        neg       eax                                           ;189.9
        add       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM] ;189.9
        mov       edx, DWORD PTR [152+esp]                      ;189.9
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE+32] ;189.59
        neg       ecx                                           ;189.9
        add       ecx, DWORD PTR [eax+edx*4]                    ;189.9
        imul      edx, ecx, 44                                  ;189.9
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE] ;189.9
        mov       ecx, DWORD PTR [36+eax+edx]                   ;189.9
        lea       eax, DWORD PTR [96+esp]                       ;189.9
        mov       DWORD PTR [96+esp], ecx                       ;189.9
        push      eax                                           ;189.9
        push      OFFSET FLAT: __STRLITPACK_58.0.7              ;189.9
        push      edi                                           ;189.9
        call      _for_write_seq_lis_xmit                       ;189.9
                                ; LOE ebx esi edi
.B7.28:                         ; Preds .B7.27
        mov       DWORD PTR [76+esp], 6                         ;189.9
        lea       eax, DWORD PTR [76+esp]                       ;189.9
        mov       DWORD PTR [80+esp], OFFSET FLAT: __STRLITPACK_12 ;189.9
        push      eax                                           ;189.9
        push      OFFSET FLAT: __STRLITPACK_59.0.7              ;189.9
        push      edi                                           ;189.9
        call      _for_write_seq_lis_xmit                       ;189.9
                                ; LOE ebx esi edi
.B7.29:                         ; Preds .B7.28
        mov       eax, DWORD PTR [188+esp]                      ;189.9
        lea       edx, DWORD PTR [160+esp]                      ;189.9
        mov       DWORD PTR [160+esp], eax                      ;189.9
        push      edx                                           ;189.9
        push      OFFSET FLAT: __STRLITPACK_60.0.7              ;189.9
        push      edi                                           ;189.9
        call      _for_write_seq_lis_xmit                       ;189.9
                                ; LOE ebx esi edi
.B7.30:                         ; Preds .B7.29
        mov       DWORD PTR [140+esp], 0                        ;190.9
        lea       eax, DWORD PTR [108+esp]                      ;190.9
        mov       DWORD PTR [108+esp], 24                       ;190.9
        mov       DWORD PTR [112+esp], OFFSET FLAT: __STRLITPACK_10 ;190.9
        push      32                                            ;190.9
        push      eax                                           ;190.9
        push      OFFSET FLAT: __STRLITPACK_61.0.7              ;190.9
        push      -2088435968                                   ;190.9
        push      911                                           ;190.9
        push      edi                                           ;190.9
        call      _for_write_seq_lis                            ;190.9
                                ; LOE ebx esi
.B7.31:                         ; Preds .B7.30
        push      32                                            ;191.9
        xor       eax, eax                                      ;191.9
        push      eax                                           ;191.9
        push      eax                                           ;191.9
        push      -2088435968                                   ;191.9
        push      eax                                           ;191.9
        push      OFFSET FLAT: __STRLITPACK_62                  ;191.9
        call      _for_stop_core                                ;191.9
                                ; LOE ebx esi
.B7.51:                         ; Preds .B7.31
        add       esp, 108                                      ;191.9
                                ; LOE ebx esi
.B7.32:                         ; Preds .B7.51 .B7.25
        test      ebx, ebx                                      ;193.14
        jle       .B7.34        ; Prob 16%                      ;193.14
                                ; LOE ebx esi
.B7.33:                         ; Preds .B7.32
        imul      edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+32], -28 ;194.65
        mov       edi, esi                                      ;168.5
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+44] ;194.12
        lea       eax, DWORD PTR [esi*4]                        ;168.5
        neg       ecx                                           ;194.65
        shl       edi, 5                                        ;168.5
        add       ecx, ebx                                      ;194.65
        add       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE] ;194.65
        sub       edi, eax                                      ;168.5
        imul      ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+40] ;194.65
        add       edx, edi                                      ;194.65
        cmp       BYTE PTR [10+edx+ecx], 0                      ;194.65
        jle       .B7.35        ; Prob 16%                      ;194.65
                                ; LOE ebx esi edi
.B7.34:                         ; Preds .B7.33 .B7.32
        add       esp, 196                                      ;203.1
        pop       ebx                                           ;203.1
        pop       edi                                           ;203.1
        pop       esi                                           ;203.1
        mov       esp, ebp                                      ;203.1
        pop       ebp                                           ;203.1
        ret                                                     ;203.1
                                ; LOE
.B7.35:                         ; Preds .B7.33                  ; Infreq
        imul      edx, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY+32], -48 ;196.13
        mov       ecx, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY] ;196.13
        add       ecx, DWORD PTR [60+esp]                       ;196.13
        mov       DWORD PTR [4+esp], edi                        ;
        mov       edi, DWORD PTR [44+edx+ecx]                   ;196.66
        neg       edi                                           ;196.13
        add       edi, DWORD PTR [140+esp]                      ;196.13
        imul      edi, DWORD PTR [40+edx+ecx]                   ;196.13
        mov       eax, DWORD PTR [12+edx+ecx]                   ;196.66
        mov       ecx, DWORD PTR [44+eax+edi]                   ;196.66
        neg       ecx                                           ;196.13
        add       ecx, DWORD PTR [144+esp]                      ;196.13
        imul      ecx, DWORD PTR [40+eax+edi]                   ;196.13
        mov       edx, DWORD PTR [12+eax+edi]                   ;196.66
        movzx     eax, BYTE PTR [4+edx+ecx]                     ;196.66
        inc       eax                                           ;196.118
        mov       BYTE PTR [4+edx+ecx], al                      ;196.13
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;198.36
        neg       edx                                           ;198.19
        add       edx, esi                                      ;198.19
        imul      esi, edx, 152                                 ;198.19
        mov       DWORD PTR [esp], eax                          ;196.118
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;198.19
        push      OFFSET FLAT: __NLITPACK_0.0.7                 ;198.19
        lea       edx, DWORD PTR [24+esi+eax]                   ;198.19
        lea       esi, DWORD PTR [28+esi+eax]                   ;198.19
        push      esi                                           ;198.19
        push      edx                                           ;198.19
        mov       edi, DWORD PTR [16+esp]                       ;198.19
        call      _DYNUST_NETWORK_MODULE_mp_GETFLINKFROMNODE    ;198.19
                                ; LOE ebx edi
.B7.52:                         ; Preds .B7.35                  ; Infreq
        add       esp, 12                                       ;198.19
                                ; LOE ebx edi
.B7.36:                         ; Preds .B7.52                  ; Infreq
        imul      esi, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY+32], -48 ;199.7
        mov       eax, DWORD PTR [60+esp]                       ;199.7
        add       eax, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY] ;199.7
        mov       ecx, DWORD PTR [140+esp]                      ;199.7
        mov       edx, DWORD PTR [144+esp]                      ;199.7
        sub       ecx, DWORD PTR [44+esi+eax]                   ;199.7
        imul      ecx, DWORD PTR [40+esi+eax]                   ;199.7
        mov       esi, DWORD PTR [12+esi+eax]                   ;199.7
        sub       edx, DWORD PTR [44+esi+ecx]                   ;199.7
        imul      edx, DWORD PTR [40+esi+ecx]                   ;199.7
        mov       eax, DWORD PTR [12+esi+ecx]                   ;199.7
        movsx     ecx, BYTE PTR [esp]                           ;197.13
        sub       ecx, DWORD PTR [40+eax+edx]                   ;199.7
        imul      ecx, DWORD PTR [36+eax+edx]                   ;199.7
        mov       edx, DWORD PTR [8+eax+edx]                    ;199.7
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+44] ;200.13
        neg       eax                                           ;200.13
        mov       DWORD PTR [edx+ecx], ebx                      ;199.7
        add       eax, ebx                                      ;200.13
        imul      ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+32], -28 ;200.13
        imul      eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+40] ;200.13
        add       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE] ;200.13
        add       ebx, edi                                      ;200.13
        mov       BYTE PTR [9+ebx+eax], 0                       ;200.13
        add       esp, 196                                      ;200.13
        pop       ebx                                           ;200.13
        pop       edi                                           ;200.13
        pop       esi                                           ;200.13
        mov       esp, ebp                                      ;200.13
        pop       ebp                                           ;200.13
        ret                                                     ;200.13
                                ; LOE
.B7.37:                         ; Preds .B7.8                   ; Infreq
        mov       ebx, 1                                        ;
        jmp       .B7.12        ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx
.B7.38:                         ; Preds .B7.7                   ; Infreq
        mov       eax, 0                                        ;
        mov       DWORD PTR [132+esp], eax                      ;
        jmp       .B7.14        ; Prob 100%                     ;
                                ; LOE
.B7.39:                         ; Preds .B7.4                   ; Infreq
        xor       ebx, ebx                                      ;
        jmp       .B7.25        ; Prob 100%                     ;
                                ; LOE ebx esi
.B7.40:                         ; Preds .B7.1 .B7.2             ; Infreq
        mov       DWORD PTR [80+esp], 0                         ;162.9
        lea       edx, DWORD PTR [80+esp]                       ;162.9
        mov       DWORD PTR [8+esp], 20                         ;162.9
        lea       eax, DWORD PTR [8+esp]                        ;162.9
        mov       DWORD PTR [12+esp], OFFSET FLAT: __STRLITPACK_21 ;162.9
        push      32                                            ;162.9
        push      eax                                           ;162.9
        push      OFFSET FLAT: __STRLITPACK_50.0.7              ;162.9
        push      -2088435968                                   ;162.9
        push      911                                           ;162.9
        push      edx                                           ;162.9
        call      _for_write_seq_lis                            ;162.9
                                ; LOE ebx esi edi
.B7.41:                         ; Preds .B7.40                  ; Infreq
        mov       DWORD PTR [104+esp], 0                        ;163.9
        lea       eax, DWORD PTR [40+esp]                       ;163.9
        mov       DWORD PTR [40+esp], 29                        ;163.9
        mov       DWORD PTR [44+esp], OFFSET FLAT: __STRLITPACK_19 ;163.9
        push      32                                            ;163.9
        push      eax                                           ;163.9
        push      OFFSET FLAT: __STRLITPACK_51.0.7              ;163.9
        push      -2088435968                                   ;163.9
        push      911                                           ;163.9
        lea       edx, DWORD PTR [124+esp]                      ;163.9
        push      edx                                           ;163.9
        call      _for_write_seq_lis                            ;163.9
                                ; LOE ebx esi edi
.B7.42:                         ; Preds .B7.41                  ; Infreq
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE+32] ;163.55
        neg       eax                                           ;163.9
        add       eax, ebx                                      ;163.9
        imul      ecx, eax, 44                                  ;163.9
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE] ;163.9
        mov       eax, DWORD PTR [36+edx+ecx]                   ;163.9
        lea       edx, DWORD PTR [104+esp]                      ;163.9
        mov       DWORD PTR [104+esp], eax                      ;163.9
        push      edx                                           ;163.9
        push      OFFSET FLAT: __STRLITPACK_52.0.7              ;163.9
        lea       ecx, DWORD PTR [136+esp]                      ;163.9
        push      ecx                                           ;163.9
        call      _for_write_seq_lis_xmit                       ;163.9
                                ; LOE ebx esi edi
.B7.43:                         ; Preds .B7.42                  ; Infreq
        mov       DWORD PTR [140+esp], 0                        ;164.9
        lea       eax, DWORD PTR [84+esp]                       ;164.9
        mov       DWORD PTR [84+esp], 6                         ;164.9
        mov       DWORD PTR [88+esp], OFFSET FLAT: __STRLITPACK_17 ;164.9
        push      32                                            ;164.9
        push      eax                                           ;164.9
        push      OFFSET FLAT: __STRLITPACK_53.0.7              ;164.9
        push      -2088435968                                   ;164.9
        push      911                                           ;164.9
        lea       edx, DWORD PTR [160+esp]                      ;164.9
        push      edx                                           ;164.9
        call      _for_write_seq_lis                            ;164.9
                                ; LOE ebx esi edi
.B7.44:                         ; Preds .B7.43                  ; Infreq
        mov       eax, DWORD PTR [224+esp]                      ;164.9
        lea       edx, DWORD PTR [148+esp]                      ;164.9
        mov       DWORD PTR [148+esp], eax                      ;164.9
        push      edx                                           ;164.9
        push      OFFSET FLAT: __STRLITPACK_54.0.7              ;164.9
        lea       ecx, DWORD PTR [172+esp]                      ;164.9
        push      ecx                                           ;164.9
        call      _for_write_seq_lis_xmit                       ;164.9
                                ; LOE ebx esi edi
.B7.45:                         ; Preds .B7.44                  ; Infreq
        push      32                                            ;165.9
        xor       eax, eax                                      ;165.9
        push      eax                                           ;165.9
        push      eax                                           ;165.9
        push      -2088435968                                   ;165.9
        push      eax                                           ;165.9
        push      OFFSET FLAT: __STRLITPACK_55                  ;165.9
        call      _for_stop_core                                ;165.9
                                ; LOE ebx esi edi
.B7.53:                         ; Preds .B7.45                  ; Infreq
        add       esp, 120                                      ;165.9
        jmp       .B7.3         ; Prob 100%                     ;165.9
                                ; LOE ebx esi edi
.B7.55:                         ; Preds .B7.12                  ; Infreq
        cmp       DWORD PTR [184+esp], 0                        ;175.6
        jmp       .B7.14        ; Prob 100%                     ;175.6
        ALIGN     16
                                ; LOE
; mark_end;
_DYNUST_SIGNAL_MODULE_mp_INSERTPHASEMOVE ENDP
_TEXT	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_50.0.7	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_51.0.7	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_52.0.7	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_53.0.7	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_54.0.7	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__NLITPACK_0.0.7	DD	3
__STRLITPACK_56.0.7	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_57.0.7	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_58.0.7	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_59.0.7	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_60.0.7	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_61.0.7	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_SIGNAL_MODULE_mp_INSERTPHASEMOVE
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_SIGNAL_MODULE_mp_DECLAREOBLINK
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_SIGNAL_MODULE_mp_DECLAREOBLINK
_DYNUST_SIGNAL_MODULE_mp_DECLAREOBLINK	PROC NEAR 
; parameter 1: 8 + ebp
; parameter 2: 12 + ebp
; parameter 3: 16 + ebp
.B8.1:                          ; Preds .B8.0
        push      ebp                                           ;207.12
        mov       ebp, esp                                      ;207.12
        and       esp, -16                                      ;207.12
        push      esi                                           ;207.12
        push      edi                                           ;207.12
        push      ebx                                           ;207.12
        sub       esp, 100                                      ;207.12
        mov       edx, DWORD PTR [8+ebp]                        ;207.12
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM] ;215.5
        mov       eax, DWORD PTR [12+edx]                       ;213.5
        mov       DWORD PTR [88+esp], eax                       ;213.5
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM+32] ;215.14
        neg       eax                                           ;215.5
        mov       edi, DWORD PTR [4+edx]                        ;215.14
        add       eax, edi                                      ;215.5
        mov       esi, DWORD PTR [8+edx]                        ;214.5
        mov       ebx, DWORD PTR [ecx+eax*4]                    ;215.5
        test      ebx, ebx                                      ;216.15
        jle       .B8.9         ; Prob 16%                      ;216.15
                                ; LOE ebx esi edi
.B8.2:                          ; Preds .B8.1 .B8.23
        mov       eax, DWORD PTR [88+esp]                       ;223.5
        xor       edx, edx                                      ;223.5
        test      eax, eax                                      ;223.5
        lea       edi, DWORD PTR [76+esp]                       ;223.5
        push      4                                             ;223.5
        lea       ecx, DWORD PTR [1+eax]                        ;223.5
        cmovl     ecx, edx                                      ;223.5
        push      ecx                                           ;223.5
        push      2                                             ;223.5
        push      edi                                           ;223.5
        call      _for_check_mult_overflow                      ;223.5
                                ; LOE eax ebx esi
.B8.3:                          ; Preds .B8.2
        imul      edi, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY+32], -48 ;223.5
        lea       ebx, DWORD PTR [ebx+ebx*2]                    ;215.5
        shl       ebx, 4                                        ;215.5
        and       eax, 1                                        ;223.5
        add       edi, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY] ;223.5
        mov       DWORD PTR [100+esp], ebx                      ;215.5
        shl       eax, 4                                        ;223.5
        mov       edx, DWORD PTR [44+ebx+edi]                   ;223.14
        or        eax, 262145                                   ;223.5
        mov       ecx, DWORD PTR [40+ebx+edi]                   ;223.14
        imul      edx, ecx                                      ;223.5
        imul      ecx, esi                                      ;214.5
        neg       edx                                           ;223.5
        add       ecx, DWORD PTR [12+ebx+edi]                   ;223.5
        mov       ebx, DWORD PTR [16+ebp]                       ;223.14
        push      eax                                           ;223.5
        mov       edi, DWORD PTR [44+edx+ecx]                   ;223.5
        neg       edi                                           ;223.5
        mov       ebx, DWORD PTR [ebx]                          ;223.14
        add       edi, ebx                                      ;223.5
        imul      edi, DWORD PTR [40+edx+ecx]                   ;223.5
        mov       edx, DWORD PTR [12+edx+ecx]                   ;223.5
        lea       ecx, DWORD PTR [8+edi+edx]                    ;223.5
        push      ecx                                           ;223.5
        push      DWORD PTR [100+esp]                           ;223.5
        call      _for_allocate                                 ;223.5
                                ; LOE eax ebx esi
.B8.19:                         ; Preds .B8.3
        add       esp, 28                                       ;223.5
                                ; LOE eax ebx esi
.B8.4:                          ; Preds .B8.19
        test      eax, eax                                      ;223.5
        je        .B8.7         ; Prob 50%                      ;223.5
                                ; LOE ebx esi
.B8.5:                          ; Preds .B8.4
        mov       DWORD PTR [esp], 0                            ;225.9
        lea       edx, DWORD PTR [esp]                          ;225.9
        mov       DWORD PTR [32+esp], 45                        ;225.9
        lea       eax, DWORD PTR [32+esp]                       ;225.9
        mov       DWORD PTR [36+esp], OFFSET FLAT: __STRLITPACK_0 ;225.9
        push      32                                            ;225.9
        push      eax                                           ;225.9
        push      OFFSET FLAT: __STRLITPACK_70.0.8              ;225.9
        push      -2088435968                                   ;225.9
        push      911                                           ;225.9
        push      edx                                           ;225.9
        call      _for_write_seq_lis                            ;225.9
                                ; LOE
.B8.6:                          ; Preds .B8.5
        push      32                                            ;226.9
        xor       eax, eax                                      ;226.9
        push      eax                                           ;226.9
        push      eax                                           ;226.9
        push      -2088435968                                   ;226.9
        push      eax                                           ;226.9
        push      OFFSET FLAT: __STRLITPACK_71                  ;226.9
        call      _for_stop_core                                ;226.9
                                ; LOE
.B8.20:                         ; Preds .B8.6
        add       esp, 148                                      ;226.9
        pop       ebx                                           ;226.9
        pop       edi                                           ;226.9
        pop       esi                                           ;226.9
        mov       esp, ebp                                      ;226.9
        pop       ebp                                           ;226.9
        ret                                                     ;226.9
                                ; LOE
.B8.7:                          ; Preds .B8.4
        imul      eax, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY+32], -48 ;223.5
        add       eax, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY] ;223.5
        mov       edi, DWORD PTR [84+esp]                       ;223.14
        mov       DWORD PTR [36+esp], ebx                       ;
        mov       edx, DWORD PTR [44+edi+eax]                   ;223.14
        mov       ecx, DWORD PTR [40+edi+eax]                   ;223.14
        imul      edx, ecx                                      ;223.5
        imul      esi, ecx                                      ;214.5
        neg       edx                                           ;223.5
        add       esi, DWORD PTR [12+edi+eax]                   ;223.5
        mov       eax, DWORD PTR [44+edx+esi]                   ;223.14
        neg       eax                                           ;223.5
        add       eax, ebx                                      ;223.5
        imul      eax, DWORD PTR [40+edx+esi]                   ;223.5
        mov       ecx, DWORD PTR [12+edx+esi]                   ;223.14
        mov       DWORD PTR [20+ecx+eax], 5                     ;223.5
        mov       eax, DWORD PTR [44+edx+esi]                   ;223.14
        neg       eax                                           ;223.5
        add       eax, ebx                                      ;223.5
        imul      eax, DWORD PTR [40+edx+esi]                   ;223.5
        mov       edi, DWORD PTR [12+edx+esi]                   ;223.14
        mov       DWORD PTR [12+edi+eax], 4                     ;223.5
        mov       eax, DWORD PTR [44+edx+esi]                   ;223.14
        neg       eax                                           ;223.5
        add       eax, ebx                                      ;223.5
        imul      eax, DWORD PTR [40+edx+esi]                   ;223.5
        mov       ecx, DWORD PTR [12+edx+esi]                   ;223.14
        mov       DWORD PTR [24+ecx+eax], 1                     ;223.5
        xor       eax, eax                                      ;223.5
        mov       ecx, DWORD PTR [44+edx+esi]                   ;223.14
        neg       ecx                                           ;223.5
        add       ecx, ebx                                      ;223.5
        imul      ecx, DWORD PTR [40+edx+esi]                   ;223.5
        mov       edi, DWORD PTR [12+edx+esi]                   ;223.14
        mov       DWORD PTR [16+edi+ecx], eax                   ;223.5
        mov       edi, DWORD PTR [44+edx+esi]                   ;223.14
        neg       edi                                           ;223.5
        add       edi, ebx                                      ;223.5
        imul      edi, DWORD PTR [40+edx+esi]                   ;223.5
        mov       ebx, DWORD PTR [12+edx+esi]                   ;223.14
        mov       DWORD PTR [40+ebx+edi], 1                     ;223.5
        mov       ebx, DWORD PTR [88+esp]                       ;223.5
        test      ebx, ebx                                      ;223.5
        mov       edi, DWORD PTR [12+edx+esi]                   ;223.14
        lea       ecx, DWORD PTR [1+ebx]                        ;223.5
        mov       ebx, DWORD PTR [36+esp]                       ;223.5
        cmovge    eax, ecx                                      ;223.5
        mov       ecx, DWORD PTR [44+edx+esi]                   ;223.14
        neg       ecx                                           ;223.5
        add       ecx, ebx                                      ;223.5
        imul      ecx, DWORD PTR [40+edx+esi]                   ;223.5
        mov       DWORD PTR [32+edi+ecx], eax                   ;223.5
        lea       ecx, DWORD PTR [32+esp]                       ;223.5
        sub       ebx, DWORD PTR [44+edx+esi]                   ;223.5
        imul      ebx, DWORD PTR [40+edx+esi]                   ;223.5
        mov       esi, DWORD PTR [12+edx+esi]                   ;223.14
        mov       edx, 4                                        ;223.5
        push      edx                                           ;223.5
        push      eax                                           ;223.5
        push      2                                             ;223.5
        push      ecx                                           ;223.5
        mov       DWORD PTR [36+esi+ebx], edx                   ;223.5
        call      _for_check_mult_overflow                      ;223.5
                                ; LOE
.B8.8:                          ; Preds .B8.7
        add       esp, 116                                      ;228.1
        pop       ebx                                           ;228.1
        pop       edi                                           ;228.1
        pop       esi                                           ;228.1
        mov       esp, ebp                                      ;228.1
        pop       ebp                                           ;228.1
        ret                                                     ;228.1
                                ; LOE
.B8.9:                          ; Preds .B8.1                   ; Infreq
        mov       DWORD PTR [esp], 0                            ;217.9
        lea       edx, DWORD PTR [esp]                          ;217.9
        mov       DWORD PTR [40+esp], 38                        ;217.9
        lea       eax, DWORD PTR [40+esp]                       ;217.9
        mov       DWORD PTR [44+esp], OFFSET FLAT: __STRLITPACK_8 ;217.9
        push      32                                            ;217.9
        push      eax                                           ;217.9
        push      OFFSET FLAT: __STRLITPACK_63.0.8              ;217.9
        push      -2088435968                                   ;217.9
        push      911                                           ;217.9
        push      edx                                           ;217.9
        call      _for_write_seq_lis                            ;217.9
                                ; LOE ebx esi edi
.B8.10:                         ; Preds .B8.9                   ; Infreq
        mov       DWORD PTR [24+esp], 0                         ;218.6
        lea       eax, DWORD PTR [72+esp]                       ;218.6
        mov       DWORD PTR [72+esp], 10                        ;218.6
        mov       DWORD PTR [76+esp], OFFSET FLAT: __STRLITPACK_6 ;218.6
        push      32                                            ;218.6
        push      eax                                           ;218.6
        push      OFFSET FLAT: __STRLITPACK_64.0.8              ;218.6
        push      -2088435968                                   ;218.6
        push      911                                           ;218.6
        lea       edx, DWORD PTR [44+esp]                       ;218.6
        push      edx                                           ;218.6
        call      _for_write_seq_lis                            ;218.6
                                ; LOE ebx esi edi
.B8.11:                         ; Preds .B8.10                  ; Infreq
        mov       eax, DWORD PTR [8+ebp]                        ;218.6
        lea       ecx, DWORD PTR [120+esp]                      ;218.6
        mov       edx, DWORD PTR [eax]                          ;218.6
        mov       DWORD PTR [120+esp], edx                      ;218.6
        push      ecx                                           ;218.6
        push      OFFSET FLAT: __STRLITPACK_65.0.8              ;218.6
        lea       eax, DWORD PTR [56+esp]                       ;218.6
        push      eax                                           ;218.6
        call      _for_write_seq_lis_xmit                       ;218.6
                                ; LOE ebx esi edi
.B8.12:                         ; Preds .B8.11                  ; Infreq
        mov       DWORD PTR [140+esp], edi                      ;218.6
        lea       eax, DWORD PTR [140+esp]                      ;218.6
        push      eax                                           ;218.6
        push      OFFSET FLAT: __STRLITPACK_66.0.8              ;218.6
        lea       edx, DWORD PTR [68+esp]                       ;218.6
        push      edx                                           ;218.6
        call      _for_write_seq_lis_xmit                       ;218.6
                                ; LOE ebx esi
.B8.13:                         ; Preds .B8.12                  ; Infreq
        mov       DWORD PTR [72+esp], 0                         ;219.6
        lea       eax, DWORD PTR [128+esp]                      ;219.6
        mov       DWORD PTR [128+esp], 47                       ;219.6
        mov       DWORD PTR [132+esp], OFFSET FLAT: __STRLITPACK_4 ;219.6
        push      32                                            ;219.6
        push      eax                                           ;219.6
        push      OFFSET FLAT: __STRLITPACK_67.0.8              ;219.6
        push      -2088435968                                   ;219.6
        push      911                                           ;219.6
        lea       edx, DWORD PTR [92+esp]                       ;219.6
        push      edx                                           ;219.6
        call      _for_write_seq_lis                            ;219.6
                                ; LOE ebx esi
.B8.14:                         ; Preds .B8.13                  ; Infreq
        mov       DWORD PTR [96+esp], 0                         ;220.6
        lea       eax, DWORD PTR [160+esp]                      ;220.6
        mov       DWORD PTR [160+esp], 57                       ;220.6
        mov       DWORD PTR [164+esp], OFFSET FLAT: __STRLITPACK_2 ;220.6
        push      32                                            ;220.6
        push      eax                                           ;220.6
        push      OFFSET FLAT: __STRLITPACK_68.0.8              ;220.6
        push      -2088435968                                   ;220.6
        push      911                                           ;220.6
        lea       edx, DWORD PTR [116+esp]                      ;220.6
        push      edx                                           ;220.6
        call      _for_write_seq_lis                            ;220.6
                                ; LOE ebx esi
.B8.22:                         ; Preds .B8.14                  ; Infreq
        add       esp, 120                                      ;220.6
                                ; LOE ebx esi
.B8.15:                         ; Preds .B8.22                  ; Infreq
        push      32                                            ;221.6
        xor       eax, eax                                      ;221.6
        push      eax                                           ;221.6
        push      eax                                           ;221.6
        push      -2088435968                                   ;221.6
        push      eax                                           ;221.6
        push      OFFSET FLAT: __STRLITPACK_69                  ;221.6
        call      _for_stop_core                                ;221.6
                                ; LOE ebx esi
.B8.23:                         ; Preds .B8.15                  ; Infreq
        add       esp, 24                                       ;221.6
        jmp       .B8.2         ; Prob 100%                     ;221.6
        ALIGN     16
                                ; LOE ebx esi
; mark_end;
_DYNUST_SIGNAL_MODULE_mp_DECLAREOBLINK ENDP
_TEXT	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_63.0.8	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_64.0.8	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_65.0.8	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_66.0.8	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_67.0.8	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_68.0.8	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_70.0.8	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_SIGNAL_MODULE_mp_DECLAREOBLINK
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_SIGNAL_MODULE_mp_ASSIGNGREEN
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_SIGNAL_MODULE_mp_ASSIGNGREEN
_DYNUST_SIGNAL_MODULE_mp_ASSIGNGREEN	PROC NEAR 
; parameter 1: 84 + esp
; parameter 2: 88 + esp
; parameter 3: 92 + esp
; parameter 4: 96 + esp
.B9.1:                          ; Preds .B9.0
        push      esi                                           ;231.12
        push      edi                                           ;231.12
        push      ebx                                           ;231.12
        push      ebp                                           ;231.12
        sub       esp, 64                                       ;231.12
        mov       eax, DWORD PTR [92+esp]                       ;231.12
        mov       edx, DWORD PTR [eax]                          ;240.5
        test      edx, edx                                      ;240.5
        mov       DWORD PTR [28+esp], edx                       ;240.5
        jle       .B9.12        ; Prob 2%                       ;240.5
                                ; LOE
.B9.2:                          ; Preds .B9.1
        mov       edx, DWORD PTR [84+esp]                       ;231.12
        mov       esi, DWORD PTR [96+esp]                       ;231.12
        mov       ebp, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY] ;241.9
        mov       edi, DWORD PTR [edx]                          ;241.20
        sub       edi, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY+32] ;
        movsx     ebx, BYTE PTR [esi]                           ;249.73
        mov       ecx, DWORD PTR [88+esp]                       ;231.12
        mov       DWORD PTR [40+esp], ebx                       ;249.73
        lea       esi, DWORD PTR [edi+edi*2]                    ;
        shl       esi, 4                                        ;
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+44] ;246.21
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+40] ;246.21
        imul      eax, edx                                      ;
        mov       ebx, DWORD PTR [44+ebp+esi]                   ;241.20
        neg       ebx                                           ;
        add       ebx, DWORD PTR [ecx]                          ;
        imul      ebx, DWORD PTR [40+ebp+esi]                   ;
        mov       edi, DWORD PTR [12+ebp+esi]                   ;241.20
        imul      esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32], -152 ;
        mov       ecx, DWORD PTR [12+edi+ebx]                   ;241.20
        mov       ebp, DWORD PTR [44+edi+ebx]                   ;241.20
        mov       edi, DWORD PTR [40+edi+ebx]                   ;241.20
        imul      ebp, edi                                      ;
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE] ;
        sub       ecx, ebp                                      ;
        imul      ebp, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32], -900 ;
        sub       ebx, eax                                      ;
        imul      eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+32], -28 ;
        add       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;
        add       ebx, eax                                      ;
        add       ebp, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;
        mov       DWORD PTR [24+esp], 1                         ;
        mov       DWORD PTR [32+esp], edi                       ;
        mov       DWORD PTR [esp], ebx                          ;
        mov       DWORD PTR [4+esp], ebp                        ;
        mov       DWORD PTR [8+esp], esi                        ;
        mov       DWORD PTR [20+esp], ecx                       ;
        mov       DWORD PTR [16+esp], edi                       ;
        mov       DWORD PTR [44+esp], edx                       ;
                                ; LOE
.B9.3:                          ; Preds .B9.10 .B9.2
        mov       ecx, DWORD PTR [20+esp]                       ;241.9
        mov       edx, DWORD PTR [32+esp]                       ;241.9
        movsx     ebx, BYTE PTR [4+edx+ecx]                     ;242.20
        test      ebx, ebx                                      ;243.13
        mov       eax, DWORD PTR [edx+ecx]                      ;241.9
        mov       DWORD PTR [60+esp], ebx                       ;242.20
        jle       .B9.10        ; Prob 2%                       ;243.13
                                ; LOE eax edx ecx dl cl dh ch
.B9.4:                          ; Preds .B9.3
        mov       edi, ecx                                      ;244.25
        mov       ecx, 1                                        ;
        mov       esi, DWORD PTR [8+edx+edi]                    ;244.25
        mov       ebp, DWORD PTR [40+edx+edi]                   ;244.25
        mov       edx, DWORD PTR [36+edx+edi]                   ;244.25
        imul      ebp, edx                                      ;
        sub       esi, ebp                                      ;
        mov       DWORD PTR [12+esp], esi                       ;
        imul      esi, eax, 152                                 ;241.9
        mov       edi, DWORD PTR [8+esp]                        ;245.20
        mov       DWORD PTR [56+esp], edx                       ;244.25
        mov       ebp, DWORD PTR [64+esi+edi]                   ;245.20
        mov       ebx, DWORD PTR [68+esi+edi]                   ;245.20
        imul      ebx, ebp                                      ;
        mov       DWORD PTR [52+esp], ebp                       ;245.20
        mov       ebp, DWORD PTR [36+esi+edi]                   ;245.20
        lea       edi, DWORD PTR [eax*4]                        ;241.9
        sub       ebp, ebx                                      ;
        imul      ebx, eax, 900                                 ;241.9
        mov       esi, DWORD PTR [4+esp]                        ;245.73
        shl       eax, 5                                        ;241.9
        sub       eax, edi                                      ;241.9
        movsx     ebx, BYTE PTR [428+ebx+esi]                   ;245.73
        add       eax, DWORD PTR [esp]                          ;
        mov       DWORD PTR [48+esp], ebx                       ;245.73
        mov       ebx, DWORD PTR [12+esp]                       ;
        mov       DWORD PTR [36+esp], eax                       ;
                                ; LOE edx ecx ebx ebp
.B9.5:                          ; Preds .B9.8 .B9.4
        mov       edi, DWORD PTR [edx+ebx]                      ;244.17
        mov       eax, DWORD PTR [44+esp]                       ;244.17
        imul      eax, edi                                      ;244.17
        imul      edi, DWORD PTR [52+esp]                       ;244.17
        mov       esi, DWORD PTR [48+esp]                       ;245.70
        cmp       esi, DWORD PTR [edi+ebp]                      ;245.70
        jne       .B9.7         ; Prob 50%                      ;245.70
                                ; LOE eax edx ecx ebx ebp
.B9.6:                          ; Preds .B9.5
        mov       esi, DWORD PTR [36+esp]                       ;246.21
        mov       DWORD PTR [4+eax+esi], 6                      ;246.21
        jmp       .B9.8         ; Prob 100%                     ;246.21
                                ; LOE edx ecx ebx ebp
.B9.7:                          ; Preds .B9.5
        mov       edi, DWORD PTR [36+esp]                       ;249.21
        mov       esi, DWORD PTR [40+esp]                       ;249.21
        mov       DWORD PTR [4+eax+edi], esi                    ;249.21
                                ; LOE edx ecx ebx ebp
.B9.8:                          ; Preds .B9.6 .B9.7
        inc       ecx                                           ;251.13
        add       edx, DWORD PTR [56+esp]                       ;251.13
        cmp       ecx, DWORD PTR [60+esp]                       ;251.13
        jle       .B9.5         ; Prob 82%                      ;251.13
                                ; LOE edx ecx ebx ebp
.B9.10:                         ; Preds .B9.8 .B9.3
        mov       eax, DWORD PTR [32+esp]                       ;252.5
        mov       edx, DWORD PTR [24+esp]                       ;252.5
        inc       edx                                           ;252.5
        add       eax, DWORD PTR [16+esp]                       ;252.5
        mov       DWORD PTR [32+esp], eax                       ;252.5
        mov       DWORD PTR [24+esp], edx                       ;252.5
        cmp       edx, DWORD PTR [28+esp]                       ;252.5
        jle       .B9.3         ; Prob 82%                      ;252.5
                                ; LOE
.B9.12:                         ; Preds .B9.10 .B9.1
        add       esp, 64                                       ;253.1
        pop       ebp                                           ;253.1
        pop       ebx                                           ;253.1
        pop       edi                                           ;253.1
        pop       esi                                           ;253.1
        ret                                                     ;253.1
        ALIGN     16
                                ; LOE
; mark_end;
_DYNUST_SIGNAL_MODULE_mp_ASSIGNGREEN ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_SIGNAL_MODULE_mp_ASSIGNGREEN
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_SIGNAL_MODULE_mp_FINDTIMENEEDED
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_SIGNAL_MODULE_mp_FINDTIMENEEDED
_DYNUST_SIGNAL_MODULE_mp_FINDTIMENEEDED	PROC NEAR 
; parameter 1: 8 + ebp
; parameter 2: 12 + ebp
.B10.1:                         ; Preds .B10.0
        push      ebp                                           ;257.18
        mov       ebp, esp                                      ;257.18
        and       esp, -16                                      ;257.18
        push      esi                                           ;257.18
        push      edi                                           ;257.18
        push      ebx                                           ;257.18
        sub       esp, 116                                      ;257.18
        mov       edx, DWORD PTR [8+ebp]                        ;257.18
        mov       eax, DWORD PTR [12+ebp]                       ;257.18
        mov       ecx, DWORD PTR [edx]                          ;265.12
        mov       edx, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY+32] ;265.12
        lea       esi, DWORD PTR [ecx+ecx*2]                    ;265.12
        shl       esi, 4                                        ;265.12
        lea       ebx, DWORD PTR [edx+edx*2]                    ;265.12
        shl       ebx, 4                                        ;265.12
        neg       ebx                                           ;265.12
        add       esi, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY] ;265.12
        mov       edi, DWORD PTR [44+ebx+esi]                   ;265.12
        neg       edi                                           ;265.12
        add       edi, DWORD PTR [eax]                          ;265.12
        imul      edi, DWORD PTR [40+ebx+esi]                   ;265.12
        mov       edx, DWORD PTR [12+ebx+esi]                   ;265.12
        mov       DWORD PTR [100+esp], edi                      ;265.12
        movsx     eax, BYTE PTR [edx+edi]                       ;265.12
        test      eax, eax                                      ;268.5
        mov       DWORD PTR [96+esp], eax                       ;265.12
        jle       .B10.30       ; Prob 0%                       ;268.5
                                ; LOE edx edi
.B10.2:                         ; Preds .B10.1
        mov       ebx, edi                                      ;269.20
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;270.9
        movsx     ecx, BYTE PTR [_DYNUST_SIGNAL_MODULE_mp_GREENEXT] ;271.27
        mov       esi, DWORD PTR [40+edx+ebx]                   ;269.20
        cmp       esi, 4                                        ;268.5
        mov       DWORD PTR [24+esp], eax                       ;270.9
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;270.31
        mov       DWORD PTR [28+esp], ecx                       ;271.27
        mov       DWORD PTR [16+esp], esi                       ;269.20
        jne       .B10.19       ; Prob 50%                      ;268.5
                                ; LOE eax edx ebx edi bl bh
.B10.3:                         ; Preds .B10.2
        cmp       DWORD PTR [96+esp], 4                         ;268.5
        jl        .B10.26       ; Prob 10%                      ;268.5
                                ; LOE eax edx ebx bl bh
.B10.4:                         ; Preds .B10.3
        mov       ecx, ebx                                      ;269.20
        mov       edi, DWORD PTR [44+edx+ecx]                   ;269.20
        shl       edi, 2                                        ;268.5
        neg       edi                                           ;268.5
        mov       esi, DWORD PTR [12+edx+ecx]                   ;269.20
        lea       ebx, DWORD PTR [esi+edi]                      ;268.5
        mov       DWORD PTR [12+esp], ebx                       ;268.5
        lea       esi, DWORD PTR [4+esi+edi]                    ;268.5
        and       esi, 15                                       ;268.5
        je        .B10.7        ; Prob 50%                      ;268.5
                                ; LOE eax edx esi
.B10.5:                         ; Preds .B10.4
        test      esi, 3                                        ;268.5
        jne       .B10.26       ; Prob 10%                      ;268.5
                                ; LOE eax edx esi
.B10.6:                         ; Preds .B10.5
        neg       esi                                           ;268.5
        add       esi, 16                                       ;268.5
        shr       esi, 2                                        ;268.5
                                ; LOE eax edx esi
.B10.7:                         ; Preds .B10.6 .B10.4
        lea       ecx, DWORD PTR [4+esi]                        ;268.5
        cmp       ecx, DWORD PTR [96+esp]                       ;268.5
        jg        .B10.26       ; Prob 10%                      ;268.5
                                ; LOE eax edx esi
.B10.8:                         ; Preds .B10.7
        mov       ecx, DWORD PTR [96+esp]                       ;268.5
        mov       ebx, ecx                                      ;268.5
        sub       ebx, esi                                      ;268.5
        and       ebx, 3                                        ;268.5
        pxor      xmm0, xmm0                                    ;
        neg       ebx                                           ;268.5
        add       ebx, ecx                                      ;268.5
        cvtsi2ss  xmm7, DWORD PTR [28+esp]                      ;271.27
        mov       DWORD PTR [20+esp], 0                         ;
        test      esi, esi                                      ;268.5
        jbe       .B10.12       ; Prob 3%                       ;268.5
                                ; LOE eax edx ebx esi xmm0 xmm7
.B10.9:                         ; Preds .B10.8
        imul      ecx, eax, -900                                ;
        add       ecx, DWORD PTR [24+esp]                       ;
        mov       DWORD PTR [16+esp], 0                         ;
        mov       DWORD PTR [8+esp], ebx                        ;
        mov       DWORD PTR [4+esp], edx                        ;
        mov       DWORD PTR [esp], eax                          ;
        mov       edx, ecx                                      ;
        mov       ecx, DWORD PTR [16+esp]                       ;
        mov       ebx, DWORD PTR [20+esp]                       ;
        mov       edi, DWORD PTR [12+esp]                       ;
                                ; LOE edx ecx ebx esi edi xmm0 xmm7
.B10.10:                        ; Preds .B10.10 .B10.9
        imul      eax, DWORD PTR [4+edi+ecx*4], 900             ;269.9
        inc       ecx                                           ;268.5
        movss     xmm1, DWORD PTR [440+eax+edx]                 ;271.36
        mulss     xmm1, xmm7                                    ;271.35
        add       ebx, DWORD PTR [400+eax+edx]                  ;270.9
        addss     xmm0, xmm1                                    ;271.9
        cmp       ecx, esi                                      ;268.5
        jb        .B10.10       ; Prob 82%                      ;268.5
                                ; LOE edx ecx ebx esi edi xmm0 xmm7
.B10.11:                        ; Preds .B10.10
        mov       DWORD PTR [20+esp], ebx                       ;
        mov       ebx, DWORD PTR [8+esp]                        ;
        mov       eax, DWORD PTR [esp]                          ;
        mov       edx, DWORD PTR [4+esp]                        ;
                                ; LOE eax edx ebx esi xmm0 xmm7
.B10.12:                        ; Preds .B10.8 .B10.11
        mov       DWORD PTR [8+esp], ebx                        ;
        movd      xmm6, eax                                     ;270.31
        mov       ebx, DWORD PTR [24+esp]                       ;270.31
        pxor      xmm2, xmm2                                    ;267.5
        movss     xmm2, xmm0                                    ;267.5
        pshufd    xmm6, xmm6, 0                                 ;270.31
        shufps    xmm7, xmm7, 0                                 ;271.27
        lea       ecx, DWORD PTR [400+ebx]                      ;270.31
        movd      xmm4, DWORD PTR [20+esp]                      ;266.5
        lea       edi, DWORD PTR [440+ebx]                      ;271.36
        mov       ebx, DWORD PTR [8+esp]                        ;271.36
        movdqa    xmm3, XMMWORD PTR [_2il0floatpacket.33]       ;270.31
        movd      xmm0, ecx                                     ;270.31
        pshufd    xmm5, xmm0, 0                                 ;270.31
        movd      xmm1, edi                                     ;271.36
        movdqa    xmm0, XMMWORD PTR [_2il0floatpacket.32]       ;270.31
        psrlq     xmm0, 32                                      ;270.31
        pshufd    xmm1, xmm1, 0                                 ;271.36
        movaps    XMMWORD PTR [48+esp], xmm7                    ;271.36
        movdqa    XMMWORD PTR [32+esp], xmm0                    ;271.36
        movdqa    XMMWORD PTR [64+esp], xmm5                    ;271.36
        movdqa    XMMWORD PTR [80+esp], xmm6                    ;271.36
        mov       ecx, DWORD PTR [12+esp]                       ;271.36
                                ; LOE eax edx ecx ebx esi xmm1 xmm2 xmm3 xmm4
.B10.13:                        ; Preds .B10.13 .B10.12
        movdqa    xmm6, XMMWORD PTR [4+ecx+esi*4]               ;270.31
        add       esi, 4                                        ;268.5
        psubd     xmm6, XMMWORD PTR [80+esp]                    ;270.31
        cmp       esi, ebx                                      ;268.5
        movdqa    xmm0, XMMWORD PTR [_2il0floatpacket.32]       ;270.31
        pmuludq   xmm0, xmm6                                    ;270.31
        psrlq     xmm6, 32                                      ;270.31
        pmuludq   xmm6, XMMWORD PTR [32+esp]                    ;270.31
        pand      xmm0, xmm3                                    ;270.31
        psllq     xmm6, 32                                      ;270.31
        por       xmm0, xmm6                                    ;270.31
        movdqa    xmm6, XMMWORD PTR [64+esp]                    ;270.31
        paddd     xmm6, xmm0                                    ;270.31
        paddd     xmm0, xmm1                                    ;271.36
        movd      edi, xmm6                                     ;270.31
        pshuflw   xmm7, xmm6, 238                               ;270.31
        punpckhqdq xmm6, xmm6                                   ;270.31
        movd      xmm5, DWORD PTR [edi]                         ;270.31
        movd      edi, xmm7                                     ;270.31
        movd      xmm7, DWORD PTR [edi]                         ;270.31
        movd      edi, xmm6                                     ;270.31
        pshuflw   xmm6, xmm6, 238                               ;270.31
        punpcklqdq xmm5, xmm7                                   ;270.31
        movd      xmm7, DWORD PTR [edi]                         ;270.31
        movd      edi, xmm6                                     ;270.31
        movd      xmm6, DWORD PTR [edi]                         ;270.31
        movd      edi, xmm0                                     ;271.36
        punpcklqdq xmm7, xmm6                                   ;270.31
        shufps    xmm5, xmm7, 136                               ;270.31
        paddd     xmm4, xmm5                                    ;270.9
        pshuflw   xmm5, xmm0, 238                               ;271.36
        movd      xmm6, DWORD PTR [edi]                         ;271.36
        movd      edi, xmm5                                     ;271.36
        punpckhqdq xmm0, xmm0                                   ;271.36
        movd      xmm5, DWORD PTR [edi]                         ;271.36
        movd      edi, xmm0                                     ;271.36
        pshuflw   xmm0, xmm0, 238                               ;271.36
        punpcklqdq xmm6, xmm5                                   ;271.36
        movd      xmm5, DWORD PTR [edi]                         ;271.36
        movd      edi, xmm0                                     ;271.36
        movd      xmm0, DWORD PTR [edi]                         ;271.36
        punpcklqdq xmm5, xmm0                                   ;271.36
        shufps    xmm6, xmm5, 136                               ;271.36
        mulps     xmm6, XMMWORD PTR [48+esp]                    ;271.35
        addps     xmm2, xmm6                                    ;271.9
        jb        .B10.13       ; Prob 82%                      ;268.5
                                ; LOE eax edx ecx ebx esi xmm1 xmm2 xmm3 xmm4
.B10.14:                        ; Preds .B10.13
        movaps    xmm0, xmm2                                    ;267.5
        movdqa    xmm3, xmm4                                    ;266.5
        movhlps   xmm0, xmm2                                    ;267.5
        psrldq    xmm3, 8                                       ;266.5
        addps     xmm2, xmm0                                    ;267.5
        movaps    xmm1, xmm2                                    ;267.5
        paddd     xmm4, xmm3                                    ;266.5
        shufps    xmm1, xmm2, 245                               ;267.5
        movdqa    xmm5, xmm4                                    ;266.5
        psrldq    xmm5, 4                                       ;266.5
        addss     xmm2, xmm1                                    ;267.5
        paddd     xmm4, xmm5                                    ;266.5
        movd      ecx, xmm4                                     ;266.5
                                ; LOE eax edx ecx ebx xmm2
.B10.15:                        ; Preds .B10.14 .B10.26
        cmp       ebx, DWORD PTR [96+esp]                       ;268.5
        jae       .B10.25       ; Prob 3%                       ;268.5
                                ; LOE eax edx ecx ebx xmm2
.B10.16:                        ; Preds .B10.15
        mov       esi, DWORD PTR [100+esp]                      ;269.20
        imul      eax, eax, -900                                ;
        cvtsi2ss  xmm0, DWORD PTR [28+esp]                      ;271.27
        mov       edi, DWORD PTR [44+edx+esi]                   ;269.20
        shl       edi, 2                                        ;
        neg       edi                                           ;
        add       edi, DWORD PTR [12+edx+esi]                   ;
        add       DWORD PTR [24+esp], eax                       ;
        mov       edx, DWORD PTR [24+esp]                       ;271.27
        mov       esi, edi                                      ;271.27
        mov       edi, DWORD PTR [96+esp]                       ;271.27
                                ; LOE edx ecx ebx esi edi xmm0 xmm2
.B10.17:                        ; Preds .B10.17 .B10.16
        imul      eax, DWORD PTR [4+esi+ebx*4], 900             ;269.9
        inc       ebx                                           ;268.5
        movss     xmm1, DWORD PTR [440+eax+edx]                 ;271.36
        mulss     xmm1, xmm0                                    ;271.35
        add       ecx, DWORD PTR [400+eax+edx]                  ;270.9
        addss     xmm2, xmm1                                    ;271.9
        cmp       ebx, edi                                      ;268.5
        jb        .B10.17       ; Prob 82%                      ;268.5
        jmp       .B10.25       ; Prob 100%                     ;268.5
                                ; LOE edx ecx ebx esi edi xmm0 xmm2
.B10.19:                        ; Preds .B10.2
        mov       ecx, DWORD PTR [96+esp]                       ;268.5
        mov       ebx, ecx                                      ;268.5
        shr       ebx, 31                                       ;268.5
        add       ebx, ecx                                      ;268.5
        xor       ecx, ecx                                      ;
        sar       ebx, 1                                        ;268.5
        pxor      xmm2, xmm2                                    ;267.5
        mov       DWORD PTR [40+esp], ebx                       ;268.5
        test      ebx, ebx                                      ;268.5
        jbe       .B10.29       ; Prob 3%                       ;268.5
                                ; LOE eax edx ecx edi xmm2
.B10.20:                        ; Preds .B10.19
        mov       ebx, edi                                      ;269.20
        movaps    xmm1, xmm2                                    ;
        mov       DWORD PTR [8+esp], ecx                        ;
        xor       ecx, ecx                                      ;
        mov       DWORD PTR [12+esp], ecx                       ;
        mov       DWORD PTR [20+esp], ecx                       ;
        mov       esi, DWORD PTR [44+edx+ebx]                   ;269.20
        imul      ecx, eax, -900                                ;
        imul      esi, DWORD PTR [16+esp]                       ;
        cvtsi2ss  xmm0, DWORD PTR [28+esp]                      ;271.27
        add       ecx, DWORD PTR [24+esp]                       ;
        mov       edi, DWORD PTR [12+edx+ebx]                   ;269.20
        mov       DWORD PTR [36+esp], ecx                       ;
        sub       edi, esi                                      ;
        mov       DWORD PTR [32+esp], edi                       ;
        mov       ebx, DWORD PTR [12+esp]                       ;271.27
        mov       ecx, DWORD PTR [8+esp]                        ;271.27
        mov       esi, DWORD PTR [20+esp]                       ;271.27
        mov       DWORD PTR [esp], eax                          ;271.27
        mov       DWORD PTR [4+esp], edx                        ;271.27
        ALIGN     16
                                ; LOE ecx ebx esi xmm0 xmm1 xmm2
.B10.21:                        ; Preds .B10.21 .B10.20
        mov       DWORD PTR [20+esp], esi                       ;
        lea       edi, DWORD PTR [1+ebx+ebx]                    ;270.9
        mov       esi, DWORD PTR [16+esp]                       ;270.9
        imul      edi, esi                                      ;270.9
        mov       edx, DWORD PTR [32+esp]                       ;269.9
        mov       eax, DWORD PTR [36+esp]                       ;270.9
        imul      edi, DWORD PTR [edx+edi], 900                 ;269.9
        add       ecx, DWORD PTR [400+edi+eax]                  ;270.9
        movss     xmm3, DWORD PTR [440+edi+eax]                 ;271.36
        lea       edi, DWORD PTR [2+ebx+ebx]                    ;270.9
        imul      edi, esi                                      ;270.9
        inc       ebx                                           ;268.5
        mulss     xmm3, xmm0                                    ;271.35
        imul      edx, DWORD PTR [edx+edi], 900                 ;269.9
        addss     xmm2, xmm3                                    ;271.9
        movss     xmm4, DWORD PTR [440+edx+eax]                 ;271.36
        mulss     xmm4, xmm0                                    ;271.35
        mov       esi, DWORD PTR [20+esp]                       ;270.9
        addss     xmm1, xmm4                                    ;271.9
        add       esi, DWORD PTR [400+edx+eax]                  ;270.9
        cmp       ebx, DWORD PTR [40+esp]                       ;268.5
        jb        .B10.21       ; Prob 63%                      ;268.5
                                ; LOE ecx ebx esi xmm0 xmm1 xmm2
.B10.22:                        ; Preds .B10.21
        mov       DWORD PTR [20+esp], esi                       ;
        lea       ebx, DWORD PTR [1+ebx+ebx]                    ;268.5
        mov       eax, DWORD PTR [esp]                          ;
        addss     xmm2, xmm1                                    ;268.5
        mov       edx, DWORD PTR [4+esp]                        ;
        add       ecx, DWORD PTR [20+esp]                       ;268.5
        mov       DWORD PTR [8+esp], ebx                        ;268.5
                                ; LOE eax edx ecx xmm2
.B10.23:                        ; Preds .B10.22 .B10.29
        mov       ebx, DWORD PTR [8+esp]                        ;268.5
        lea       esi, DWORD PTR [-1+ebx]                       ;268.5
        cmp       esi, DWORD PTR [96+esp]                       ;268.5
        jae       .B10.25       ; Prob 3%                       ;268.5
                                ; LOE eax edx ecx xmm2
.B10.24:                        ; Preds .B10.23
        mov       ebx, DWORD PTR [100+esp]                      ;269.20
        neg       eax                                           ;270.9
        cvtsi2ss  xmm0, DWORD PTR [28+esp]                      ;271.27
        mov       esi, DWORD PTR [44+edx+ebx]                   ;269.20
        neg       esi                                           ;270.9
        add       esi, DWORD PTR [8+esp]                        ;270.9
        imul      esi, DWORD PTR [16+esp]                       ;270.9
        mov       edx, DWORD PTR [12+edx+ebx]                   ;269.20
        add       eax, DWORD PTR [edx+esi]                      ;270.9
        imul      edi, eax, 900                                 ;270.9
        mov       eax, DWORD PTR [24+esp]                       ;270.9
        mulss     xmm0, DWORD PTR [440+eax+edi]                 ;271.35
        add       ecx, DWORD PTR [400+eax+edi]                  ;270.9
        addss     xmm2, xmm0                                    ;271.9
                                ; LOE ecx xmm2
.B10.25:                        ; Preds .B10.17 .B10.15 .B10.30 .B10.24 .B10.23
                                ;      
        cvtsi2ss  xmm0, ecx                                     ;273.8
        divss     xmm0, xmm2                                    ;273.16
        movss     xmm1, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_ACTUATIONTHRESHOLD] ;273.16
        cmpless   xmm1, xmm0                                    ;276.1
        movd      eax, xmm1                                     ;276.1
        add       esp, 116                                      ;276.1
        pop       ebx                                           ;276.1
        pop       edi                                           ;276.1
        pop       esi                                           ;276.1
        mov       esp, ebp                                      ;276.1
        pop       ebp                                           ;276.1
        ret                                                     ;276.1
                                ; LOE
.B10.26:                        ; Preds .B10.3 .B10.7 .B10.5    ; Infreq
        xor       ecx, ecx                                      ;
        pxor      xmm2, xmm2                                    ;
        xor       ebx, ebx                                      ;
        jmp       .B10.15       ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx xmm2
.B10.29:                        ; Preds .B10.19                 ; Infreq
        mov       DWORD PTR [8+esp], 1                          ;
        jmp       .B10.23       ; Prob 100%                     ;
                                ; LOE eax edx ecx xmm2
.B10.30:                        ; Preds .B10.1                  ; Infreq
        xor       ecx, ecx                                      ;
        pxor      xmm2, xmm2                                    ;
        jmp       .B10.25       ; Prob 100%                     ;
        ALIGN     16
                                ; LOE ecx xmm2
; mark_end;
_DYNUST_SIGNAL_MODULE_mp_FINDTIMENEEDED ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_SIGNAL_MODULE_mp_FINDTIMENEEDED
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_SIGNAL_MODULE_mp_GETSIGNALPAR
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_SIGNAL_MODULE_mp_GETSIGNALPAR
_DYNUST_SIGNAL_MODULE_mp_GETSIGNALPAR	PROC NEAR 
; parameter 1: 8 + esp
; parameter 2: 12 + esp
; parameter 3: 16 + esp
.B11.1:                         ; Preds .B11.0
        push      esi                                           ;279.18
        mov       eax, DWORD PTR [16+esp]                       ;279.18
        mov       eax, DWORD PTR [eax]                          ;281.5
        dec       eax                                           ;281.15
        cmp       eax, 4                                        ;281.15
        ja        .B11.13       ; Prob 50%                      ;281.15
                                ; LOE eax ebx ebp edi
.B11.2:                         ; Preds .B11.1
        jmp       DWORD PTR [..1..TPKT.11_0.0.11+eax*4]         ;281.15
                                ; LOE ebx ebp edi
..1.11_0.TAG.05.0.11::
.B11.4:                         ; Preds .B11.2
        mov       ecx, DWORD PTR [8+esp]                        ;290.24
        imul      edx, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY+32], -48 ;290.9
        mov       esi, DWORD PTR [ecx]                          ;290.24
        add       edx, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY] ;290.9
        mov       ecx, DWORD PTR [12+esp]                       ;290.9
        lea       esi, DWORD PTR [esi+esi*2]                    ;290.24
        shl       esi, 4                                        ;290.24
        mov       eax, DWORD PTR [44+esi+edx]                   ;290.24
        neg       eax                                           ;290.9
        add       eax, DWORD PTR [ecx]                          ;290.9
        imul      eax, DWORD PTR [40+esi+edx]                   ;290.9
        mov       edx, DWORD PTR [12+esi+edx]                   ;290.24
        movsx     eax, BYTE PTR [edx+eax]                       ;290.24
        jmp       .B11.14       ; Prob 100%                     ;290.24
                                ; LOE eax ebx ebp edi
..1.11_0.TAG.04.0.11::
.B11.6:                         ; Preds .B11.2
        mov       ecx, DWORD PTR [8+esp]                        ;288.24
        imul      edx, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY+32], -48 ;288.9
        mov       esi, DWORD PTR [ecx]                          ;288.24
        add       edx, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY] ;288.9
        mov       ecx, DWORD PTR [12+esp]                       ;288.9
        lea       esi, DWORD PTR [esi+esi*2]                    ;288.24
        shl       esi, 4                                        ;288.24
        mov       eax, DWORD PTR [44+esi+edx]                   ;288.24
        neg       eax                                           ;288.9
        add       eax, DWORD PTR [ecx]                          ;288.9
        imul      eax, DWORD PTR [40+esi+edx]                   ;288.9
        mov       edx, DWORD PTR [12+esi+edx]                   ;288.24
        movsx     eax, WORD PTR [2+edx+eax]                     ;288.24
        jmp       .B11.14       ; Prob 100%                     ;288.24
                                ; LOE eax ebx ebp edi
..1.11_0.TAG.03.0.11::
.B11.8:                         ; Preds .B11.2
        mov       ecx, DWORD PTR [8+esp]                        ;286.24
        imul      edx, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY+32], -48 ;286.9
        mov       esi, DWORD PTR [ecx]                          ;286.24
        add       edx, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY] ;286.9
        mov       ecx, DWORD PTR [12+esp]                       ;286.9
        lea       esi, DWORD PTR [esi+esi*2]                    ;286.24
        shl       esi, 4                                        ;286.24
        mov       eax, DWORD PTR [44+esi+edx]                   ;286.24
        neg       eax                                           ;286.9
        add       eax, DWORD PTR [ecx]                          ;286.9
        imul      eax, DWORD PTR [40+esi+edx]                   ;286.9
        mov       edx, DWORD PTR [12+esi+edx]                   ;286.24
        movsx     eax, BYTE PTR [4+edx+eax]                     ;286.24
        jmp       .B11.14       ; Prob 100%                     ;286.24
                                ; LOE eax ebx ebp edi
..1.11_0.TAG.02.0.11::
.B11.10:                        ; Preds .B11.2
        mov       ecx, DWORD PTR [8+esp]                        ;284.24
        imul      edx, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY+32], -48 ;284.9
        mov       esi, DWORD PTR [ecx]                          ;284.24
        add       edx, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY] ;284.9
        mov       ecx, DWORD PTR [12+esp]                       ;284.9
        lea       esi, DWORD PTR [esi+esi*2]                    ;284.24
        shl       esi, 4                                        ;284.24
        mov       eax, DWORD PTR [44+esi+edx]                   ;284.24
        neg       eax                                           ;284.9
        add       eax, DWORD PTR [ecx]                          ;284.9
        imul      eax, DWORD PTR [40+esi+edx]                   ;284.9
        mov       edx, DWORD PTR [12+esi+edx]                   ;284.24
        movsx     eax, WORD PTR [8+edx+eax]                     ;284.24
        jmp       .B11.14       ; Prob 100%                     ;284.24
                                ; LOE eax ebx ebp edi
..1.11_0.TAG.01.0.11::
.B11.12:                        ; Preds .B11.2
        mov       ecx, DWORD PTR [8+esp]                        ;282.24
        imul      edx, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY+32], -48 ;282.9
        mov       esi, DWORD PTR [ecx]                          ;282.24
        add       edx, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY] ;282.9
        mov       ecx, DWORD PTR [12+esp]                       ;282.9
        lea       esi, DWORD PTR [esi+esi*2]                    ;282.24
        shl       esi, 4                                        ;282.24
        mov       eax, DWORD PTR [44+esi+edx]                   ;282.24
        neg       eax                                           ;282.9
        add       eax, DWORD PTR [ecx]                          ;282.9
        imul      eax, DWORD PTR [40+esi+edx]                   ;282.9
        mov       edx, DWORD PTR [12+esi+edx]                   ;282.24
        movsx     eax, BYTE PTR [10+edx+eax]                    ;282.24
        jmp       .B11.14       ; Prob 100%                     ;282.24
                                ; LOE eax ebx ebp edi
.B11.13:                        ; Preds .B11.1
        xor       eax, eax                                      ;
                                ; LOE eax ebx ebp edi
.B11.14:                        ; Preds .B11.4 .B11.6 .B11.8 .B11.10 .B11.12
                                ;       .B11.13
        pop       esi                                           ;292.1
        ret                                                     ;292.1
        ALIGN     16
                                ; LOE
; mark_end;
_DYNUST_SIGNAL_MODULE_mp_GETSIGNALPAR ENDP
_TEXT	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DB 3 DUP ( 0H)	; pad
..1..TPKT.11_0.0.11	DD	OFFSET FLAT: ..1.11_0.TAG.01.0.11
	DD	OFFSET FLAT: ..1.11_0.TAG.02.0.11
	DD	OFFSET FLAT: ..1.11_0.TAG.03.0.11
	DD	OFFSET FLAT: ..1.11_0.TAG.04.0.11
	DD	OFFSET FLAT: ..1.11_0.TAG.05.0.11
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_SIGNAL_MODULE_mp_GETSIGNALPAR
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_SIGNAL_MODULE_mp_DESTROY_CONTROL_ARRAY
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_SIGNAL_MODULE_mp_DESTROY_CONTROL_ARRAY
_DYNUST_SIGNAL_MODULE_mp_DESTROY_CONTROL_ARRAY	PROC NEAR 
.B12.1:                         ; Preds .B12.0
        push      ebp                                           ;295.12
        sub       esp, 56                                       ;295.12
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFNODES] ;297.5
        sub       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOF_MASTER_DESTINATIONS] ;297.5
        mov       eax, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY] ;308.5
        test      ecx, ecx                                      ;297.5
        jle       .B12.6        ; Prob 2%                       ;297.5
                                ; LOE eax ecx ebx esi edi
.B12.2:                         ; Preds .B12.1
        imul      ebp, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY+32], -48 ;
        mov       edx, 1                                        ;
        mov       DWORD PTR [40+esp], eax                       ;
        add       ebp, eax                                      ;
        mov       DWORD PTR [44+esp], esi                       ;
        mov       DWORD PTR [48+esp], edi                       ;
        mov       edi, ecx                                      ;
        mov       DWORD PTR [52+esp], ebx                       ;
        mov       ebx, edx                                      ;
                                ; LOE ebx ebp edi
.B12.3:                         ; Preds .B12.4 .B12.2
        lea       esi, DWORD PTR [ebx+ebx*2]                    ;298.45
        shl       esi, 4                                        ;298.45
        movsx     eax, BYTE PTR [ebp+esi]                       ;298.12
        and       eax, -2                                       ;298.45
        cmp       eax, 4                                        ;298.45
        je        .B12.8        ; Prob 1%                       ;298.45
                                ; LOE ebx ebp esi edi
.B12.4:                         ; Preds .B12.3 .B12.19
        inc       ebx                                           ;307.5
        cmp       ebx, edi                                      ;307.5
        jle       .B12.3        ; Prob 82%                      ;307.5
                                ; LOE ebx ebp edi
.B12.5:                         ; Preds .B12.4
        mov       eax, DWORD PTR [40+esp]                       ;
        mov       esi, DWORD PTR [44+esp]                       ;
        mov       edi, DWORD PTR [48+esp]                       ;
        mov       ebx, DWORD PTR [52+esp]                       ;
                                ; LOE eax ebx esi edi
.B12.6:                         ; Preds .B12.5 .B12.1
        mov       ebp, DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY+12] ;308.5
        mov       ecx, ebp                                      ;308.5
        shr       ecx, 1                                        ;308.5
        mov       edx, ebp                                      ;308.5
        and       ecx, 1                                        ;308.5
        and       edx, 1                                        ;308.5
        shl       ecx, 2                                        ;308.5
        add       edx, edx                                      ;308.5
        or        ecx, edx                                      ;308.5
        or        ecx, 262144                                   ;308.5
        push      ecx                                           ;308.5
        push      eax                                           ;308.5
        call      _for_dealloc_allocatable                      ;308.5
                                ; LOE ebx ebp esi edi
.B12.7:                         ; Preds .B12.6
        and       ebp, -2                                       ;308.5
        mov       DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY], 0 ;308.5
        mov       DWORD PTR [_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY+12], ebp ;308.5
        add       esp, 64                                       ;309.1
        pop       ebp                                           ;309.1
        ret                                                     ;309.1
                                ; LOE
.B12.8:                         ; Preds .B12.3                  ; Infreq
        movsx     edx, BYTE PTR [1+ebp+esi]                     ;299.23
        test      edx, edx                                      ;299.13
        mov       DWORD PTR [28+esp], edx                       ;299.23
        mov       eax, DWORD PTR [12+esi+ebp]                   ;305.7
        jle       .B12.18       ; Prob 28%                      ;299.13
                                ; LOE eax ebx ebp esi edi
.B12.9:                         ; Preds .B12.8                  ; Infreq
        mov       edx, DWORD PTR [40+esi+ebp]                   ;300.21
        mov       ecx, DWORD PTR [44+esi+ebp]                   ;300.21
        imul      ecx, edx                                      ;
        neg       ecx                                           ;
        add       ecx, eax                                      ;
        mov       DWORD PTR [20+esp], 1                         ;
        mov       DWORD PTR [24+esp], edx                       ;300.21
        mov       DWORD PTR [12+esp], esi                       ;
        mov       DWORD PTR [8+esp], ebp                        ;
        mov       DWORD PTR [4+esp], ebx                        ;
        mov       DWORD PTR [esp], edi                          ;
        mov       DWORD PTR [16+esp], eax                       ;
        mov       esi, ecx                                      ;
        mov       ebp, edx                                      ;
        mov       edi, DWORD PTR [24+esp]                       ;
        mov       ebx, DWORD PTR [20+esp]                       ;
                                ; LOE ebx ebp esi edi
.B12.10:                        ; Preds .B12.16 .B12.9          ; Infreq
        movsx     edx, BYTE PTR [ebp+esi]                       ;300.21
        test      edx, edx                                      ;300.11
        jle       .B12.15       ; Prob 28%                      ;300.11
                                ; LOE edx ebx ebp esi edi
.B12.11:                        ; Preds .B12.10                 ; Infreq
        mov       eax, 1                                        ;
        mov       DWORD PTR [32+esp], edx                       ;
        mov       DWORD PTR [20+esp], ebx                       ;
        mov       ebx, eax                                      ;
                                ; LOE ebx ebp esi
.B12.12:                        ; Preds .B12.13 .B12.11         ; Infreq
        mov       edx, DWORD PTR [44+ebp+esi]                   ;301.26
        mov       eax, ebx                                      ;301.15
        mov       edi, DWORD PTR [40+ebp+esi]                   ;301.26
        imul      edx, edi                                      ;301.15
        imul      eax, edi                                      ;301.15
        mov       edi, DWORD PTR [12+ebp+esi]                   ;301.26
        sub       edi, edx                                      ;301.15
        mov       ecx, DWORD PTR [20+eax+edi]                   ;301.26
        mov       edx, ecx                                      ;301.15
        shr       edx, 1                                        ;301.15
        and       ecx, 1                                        ;301.15
        and       edx, 1                                        ;301.15
        add       ecx, ecx                                      ;301.15
        shl       edx, 2                                        ;301.15
        or        edx, ecx                                      ;301.15
        or        edx, 262144                                   ;301.15
        push      edx                                           ;301.15
        push      DWORD PTR [8+eax+edi]                         ;301.15
        mov       DWORD PTR [44+esp], eax                       ;301.15
        call      _for_dealloc_allocatable                      ;301.15
                                ; LOE ebx ebp esi edi
.B12.23:                        ; Preds .B12.12                 ; Infreq
        mov       eax, DWORD PTR [44+esp]                       ;
        add       esp, 8                                        ;301.15
                                ; LOE eax ebx ebp esi edi al ah
.B12.13:                        ; Preds .B12.23                 ; Infreq
        mov       DWORD PTR [8+edi+eax], 0                      ;301.15
        mov       eax, ebx                                      ;301.15
        mov       ecx, DWORD PTR [44+ebp+esi]                   ;301.26
        inc       ebx                                           ;302.17
        mov       edx, DWORD PTR [40+ebp+esi]                   ;301.26
        imul      ecx, edx                                      ;301.15
        imul      eax, edx                                      ;301.15
        mov       edi, DWORD PTR [12+ebp+esi]                   ;301.26
        sub       edi, ecx                                      ;301.15
        and       DWORD PTR [20+edi+eax], -2                    ;301.15
        cmp       ebx, DWORD PTR [32+esp]                       ;302.17
        jle       .B12.12       ; Prob 82%                      ;302.17
                                ; LOE ebx ebp esi
.B12.14:                        ; Preds .B12.13                 ; Infreq
        mov       edi, DWORD PTR [24+esp]                       ;
        mov       ebx, DWORD PTR [20+esp]                       ;
                                ; LOE ebx ebp esi edi
.B12.15:                        ; Preds .B12.10 .B12.14         ; Infreq
        mov       eax, DWORD PTR [24+ebp+esi]                   ;303.11
        mov       edx, eax                                      ;303.11
        shr       edx, 1                                        ;303.11
        and       eax, 1                                        ;303.11
        and       edx, 1                                        ;303.11
        add       eax, eax                                      ;303.11
        shl       edx, 2                                        ;303.11
        or        edx, eax                                      ;303.11
        or        edx, 262144                                   ;303.11
        push      edx                                           ;303.11
        push      DWORD PTR [12+ebp+esi]                        ;303.11
        call      _for_dealloc_allocatable                      ;303.11
                                ; LOE ebx ebp esi edi
.B12.24:                        ; Preds .B12.15                 ; Infreq
        add       esp, 8                                        ;303.11
                                ; LOE ebx ebp esi edi
.B12.16:                        ; Preds .B12.24                 ; Infreq
        inc       ebx                                           ;304.7
        mov       DWORD PTR [12+ebp+esi], 0                     ;303.11
        and       DWORD PTR [24+ebp+esi], -2                    ;303.11
        add       ebp, edi                                      ;304.7
        cmp       ebx, DWORD PTR [28+esp]                       ;304.7
        jle       .B12.10       ; Prob 82%                      ;304.7
                                ; LOE ebx ebp esi edi
.B12.17:                        ; Preds .B12.16                 ; Infreq
        mov       eax, DWORD PTR [16+esp]                       ;
        mov       esi, DWORD PTR [12+esp]                       ;
        mov       ebp, DWORD PTR [8+esp]                        ;
        mov       ebx, DWORD PTR [4+esp]                        ;
        mov       edi, DWORD PTR [esp]                          ;
                                ; LOE eax ebx ebp esi edi
.B12.18:                        ; Preds .B12.8 .B12.17          ; Infreq
        mov       edx, DWORD PTR [24+esi+ebp]                   ;305.18
        mov       ecx, edx                                      ;305.7
        shr       ecx, 1                                        ;305.7
        and       edx, 1                                        ;305.7
        and       ecx, 1                                        ;305.7
        add       edx, edx                                      ;305.7
        shl       ecx, 2                                        ;305.7
        or        ecx, edx                                      ;305.7
        or        ecx, 262144                                   ;305.7
        push      ecx                                           ;305.7
        push      eax                                           ;305.7
        call      _for_dealloc_allocatable                      ;305.7
                                ; LOE ebx ebp esi edi
.B12.25:                        ; Preds .B12.18                 ; Infreq
        add       esp, 8                                        ;305.7
                                ; LOE ebx ebp esi edi
.B12.19:                        ; Preds .B12.25                 ; Infreq
        mov       DWORD PTR [12+ebp+esi], 0                     ;305.7
        and       DWORD PTR [24+ebp+esi], -2                    ;305.7
        jmp       .B12.4        ; Prob 100%                     ;305.7
        ALIGN     16
                                ; LOE ebx ebp edi
; mark_end;
_DYNUST_SIGNAL_MODULE_mp_DESTROY_CONTROL_ARRAY ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_SIGNAL_MODULE_mp_DESTROY_CONTROL_ARRAY
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
	PUBLIC _DYNUST_SIGNAL_MODULE_mp_ACTUATIONTHRESHOLD
_DYNUST_SIGNAL_MODULE_mp_ACTUATIONTHRESHOLD	DD	03d4ccccdH
	DD 1 DUP (0H)	; pad
	PUBLIC _DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY
_DYNUST_SIGNAL_MODULE_mp_CONTROL_ARRAY	DD	0
	DD	0
	DD	0
	DD	128
	DD	1
	DD	0
	DD	0
	DD	0
	DD	0
_DATA	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
_2il0floatpacket.32	DD	000000384H,000000384H,000000384H,000000384H
_2il0floatpacket.33	DD	0ffffffffH,000000000H,0ffffffffH,000000000H
__STRLITPACK_35	DB	97
	DB	108
	DB	108
	DB	111
	DB	99
	DB	97
	DB	116
	DB	101
	DB	32
	DB	115
	DB	105
	DB	103
	DB	110
	DB	97
	DB	108
	DB	95
	DB	97
	DB	114
	DB	114
	DB	97
	DB	121
	DB	32
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	0
__STRLITPACK_38	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_33	DB	97
	DB	108
	DB	108
	DB	111
	DB	99
	DB	97
	DB	116
	DB	101
	DB	32
	DB	99
	DB	111
	DB	110
	DB	116
	DB	114
	DB	111
	DB	108
	DB	95
	DB	97
	DB	114
	DB	114
	DB	97
	DB	121
	DB	32
	DB	112
	DB	104
	DB	97
	DB	115
	DB	101
	DB	32
	DB	97
	DB	108
	DB	108
	DB	111
	DB	99
	DB	97
	DB	116
	DB	105
	DB	111
	DB	110
	DB	32
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_40	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_31	DB	87
	DB	114
	DB	111
	DB	110
	DB	103
	DB	32
	DB	110
	DB	117
	DB	109
	DB	98
	DB	101
	DB	114
	DB	32
	DB	111
	DB	102
	DB	32
	DB	112
	DB	104
	DB	97
	DB	115
	DB	101
	DB	115
	DB	32
	DB	102
	DB	111
	DB	114
	DB	32
	DB	112
	DB	114
	DB	101
	DB	45
	DB	116
	DB	105
	DB	109
	DB	101
	DB	32
	DB	102
	DB	111
	DB	114
	DB	32
	DB	97
	DB	99
	DB	116
	DB	117
	DB	97
	DB	116
	DB	101
	DB	100
	DB	32
	DB	115
	DB	105
	DB	103
	DB	110
	DB	97
	DB	108
	DB	0
__STRLITPACK_29	DB	99
	DB	104
	DB	101
	DB	99
	DB	107
	DB	32
	DB	110
	DB	111
	DB	100
	DB	101
	DB	32
	DB	73
	DB	68
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_27	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	114
	DB	101
	DB	97
	DB	100
	DB	105
	DB	110
	DB	103
	DB	32
	DB	115
	DB	105
	DB	103
	DB	32
	DB	100
	DB	97
	DB	116
	DB	97
	DB	32
	DB	105
	DB	110
	DB	32
	DB	105
	DB	110
	DB	115
	DB	101
	DB	114
	DB	116
	DB	83
	DB	105
	DB	103
	DB	110
	DB	97
	DB	108
	DB	80
	DB	104
	DB	97
	DB	115
	DB	101
	DB	32
	DB	115
	DB	117
	DB	98
	DB	32
	DB	102
	DB	111
	DB	114
	DB	32
	DB	110
	DB	111
	DB	100
	DB	101
	DB	0
__STRLITPACK_25	DB	99
	DB	104
	DB	101
	DB	99
	DB	107
	DB	32
	DB	35
	DB	32
	DB	111
	DB	102
	DB	32
	DB	112
	DB	104
	DB	97
	DB	115
	DB	101
	DB	115
	DB	32
	DB	102
	DB	111
	DB	114
	DB	32
	DB	105
	DB	116
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_47	DB	0
	DB 3 DUP ( 0H)	; pad
_2il0floatpacket.7	DD	042700000H
_2il0floatpacket.8	DD	080000000H
_2il0floatpacket.9	DD	04b000000H
_2il0floatpacket.10	DD	03f000000H
_2il0floatpacket.11	DD	0bf000000H
__STRLITPACK_23	DB	97
	DB	108
	DB	108
	DB	111
	DB	99
	DB	97
	DB	116
	DB	101
	DB	32
	DB	99
	DB	111
	DB	110
	DB	116
	DB	114
	DB	111
	DB	108
	DB	95
	DB	97
	DB	114
	DB	114
	DB	97
	DB	121
	DB	32
	DB	112
	DB	104
	DB	97
	DB	115
	DB	101
	DB	32
	DB	97
	DB	108
	DB	108
	DB	111
	DB	99
	DB	97
	DB	116
	DB	105
	DB	111
	DB	110
	DB	32
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_49	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_21	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	99
	DB	111
	DB	110
	DB	116
	DB	114
	DB	111
	DB	108
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_19	DB	67
	DB	104
	DB	101
	DB	99
	DB	107
	DB	32
	DB	112
	DB	104
	DB	97
	DB	115
	DB	101
	DB	32
	DB	109
	DB	111
	DB	118
	DB	101
	DB	109
	DB	101
	DB	110
	DB	116
	DB	32
	DB	102
	DB	111
	DB	114
	DB	32
	DB	110
	DB	111
	DB	100
	DB	101
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_17	DB	80
	DB	104
	DB	97
	DB	115
	DB	101
	DB	32
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_55	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_16	DB	110
	DB	111
	DB	116
	DB	32
	DB	102
	DB	105
	DB	110
	DB	100
	DB	105
	DB	110
	DB	103
	DB	32
	DB	109
	DB	111
	DB	118
	DB	101
	DB	109
	DB	101
	DB	110
	DB	116
	DB	32
	DB	105
	DB	110
	DB	32
	DB	73
	DB	110
	DB	115
	DB	101
	DB	114
	DB	116
	DB	80
	DB	104
	DB	97
	DB	115
	DB	101
	DB	77
	DB	111
	DB	118
	DB	101
	DB	0
__STRLITPACK_13	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	112
	DB	104
	DB	97
	DB	115
	DB	101
	DB	45
	DB	109
	DB	111
	DB	118
	DB	101
	DB	109
	DB	101
	DB	110
	DB	116
	DB	32
	DB	102
	DB	111
	DB	114
	DB	32
	DB	110
	DB	111
	DB	100
	DB	101
	DB	32
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_12	DB	112
	DB	104
	DB	97
	DB	115
	DB	101
	DB	32
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_10	DB	80
	DB	108
	DB	101
	DB	97
	DB	115
	DB	101
	DB	32
	DB	99
	DB	104
	DB	101
	DB	99
	DB	107
	DB	32
	DB	99
	DB	111
	DB	110
	DB	116
	DB	114
	DB	111
	DB	108
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_62	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_8	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	114
	DB	101
	DB	97
	DB	100
	DB	105
	DB	110
	DB	103
	DB	32
	DB	112
	DB	104
	DB	97
	DB	115
	DB	105
	DB	110
	DB	103
	DB	32
	DB	100
	DB	97
	DB	116
	DB	97
	DB	32
	DB	102
	DB	111
	DB	114
	DB	32
	DB	110
	DB	111
	DB	100
	DB	101
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_6	DB	70
	DB	114
	DB	111
	DB	109
	DB	32
	DB	61
	DB	62
	DB	32
	DB	116
	DB	111
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_4	DB	99
	DB	104
	DB	101
	DB	99
	DB	107
	DB	32
	DB	110
	DB	117
	DB	109
	DB	98
	DB	101
	DB	114
	DB	32
	DB	111
	DB	102
	DB	32
	DB	112
	DB	104
	DB	97
	DB	115
	DB	101
	DB	115
	DB	32
	DB	109
	DB	97
	DB	116
	DB	99
	DB	104
	DB	101
	DB	115
	DB	32
	DB	116
	DB	104
	DB	101
	DB	32
	DB	115
	DB	101
	DB	99
	DB	111
	DB	110
	DB	100
	DB	32
	DB	98
	DB	108
	DB	111
	DB	99
	DB	107
	DB	0
__STRLITPACK_2	DB	97
	DB	108
	DB	115
	DB	111
	DB	32
	DB	99
	DB	104
	DB	101
	DB	99
	DB	107
	DB	32
	DB	110
	DB	111
	DB	100
	DB	101
	DB	32
	DB	110
	DB	117
	DB	109
	DB	98
	DB	101
	DB	114
	DB	115
	DB	32
	DB	102
	DB	111
	DB	114
	DB	32
	DB	116
	DB	104
	DB	101
	DB	32
	DB	109
	DB	111
	DB	118
	DB	101
	DB	109
	DB	101
	DB	110
	DB	116
	DB	32
	DB	100
	DB	111
	DB	119
	DB	110
	DB	115
	DB	116
	DB	114
	DB	101
	DB	97
	DB	109
	DB	32
	DB	110
	DB	111
	DB	100
	DB	101
	DB	115
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_69	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_0	DB	97
	DB	108
	DB	108
	DB	111
	DB	99
	DB	97
	DB	116
	DB	101
	DB	32
	DB	99
	DB	111
	DB	110
	DB	116
	DB	114
	DB	111
	DB	108
	DB	95
	DB	97
	DB	114
	DB	114
	DB	97
	DB	121
	DB	32
	DB	112
	DB	104
	DB	97
	DB	115
	DB	101
	DB	32
	DB	97
	DB	108
	DB	108
	DB	111
	DB	99
	DB	97
	DB	116
	DB	105
	DB	111
	DB	110
	DB	32
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_71	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
EXTRN	_DYNUST_MAIN_MODULE_mp_NOOF_MASTER_DESTINATIONS:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NOOFNODES:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_MAXMOVE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ERROR:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM:BYTE
	COMM _DYNUST_SIGNAL_MODULE_mp_GREENEXT:BYTE:1
	COMM _DYNUST_SIGNAL_MODULE_mp_NOFPRESIG:BYTE:1
	COMM _DYNUST_SIGNAL_MODULE_mp_NOFACTSIG:BYTE:1
	COMM _DYNUST_SIGNAL_MODULE_mp_LOSTTIME:BYTE:1
_DATA	ENDS
EXTRN	_for_dealloc_allocatable:PROC
EXTRN	_for_write_seq_lis_xmit:PROC
EXTRN	_for_allocate:PROC
EXTRN	_DYNUST_NETWORK_MODULE_mp_GETFLINKFROMNODE:PROC
EXTRN	_for_stop_core:PROC
EXTRN	_for_write_seq_lis:PROC
EXTRN	_for_alloc_allocatable:PROC
EXTRN	_for_check_mult_overflow:PROC
EXTRN	__fltused:BYTE
	INCLUDELIB <ifconsol>
	INCLUDELIB <libifcoremt>
	INCLUDELIB <libifport>
	INCLUDELIB <libmmt>
	INCLUDELIB <LIBCMT>
	INCLUDELIB <libirc>
	INCLUDELIB <svml_dispmt>
	INCLUDELIB <OLDNAMES>
	END
