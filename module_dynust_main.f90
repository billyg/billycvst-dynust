     MODULE DYNUST_MAIN_MODULE
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~ Copyright (C) 2002-2011 The Arizona Board of Regents on Behalf of The University of Arizona.  ~~!
!~~ Developed by Yi-Chang Chiu, Ph.D. as par to the DynusT project.                               ~~!
!~~ This program is free software: you can redistribute it and/or modify it                       ~~!
!~~ under the terms of the GNU General Public License version 2 as published by                   ~~!
!~~ the Free Software Foundation.                                                                 ~~!
!~~ This program is distributed in the hope that it will be useful,                               ~~!
!~~ but WITHOUT ANY WARRANTY; without even the implied warranty of                                ~~!
!~~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                                          ~~!
!~~ See the GNU General Public License for more details.                                          ~~!
!~~ You should have received a copy of the GNU General Public License                             ~~!
!~~ along with this program (in the file COPYING).                                                ~~!
!~~ IF not, see <http://www.gnu.org/licenses/>.                                                   ~~!
!~~ Please see http://www.dynust.net for contact information and names                            ~~!
!~~ of individual contributors.                                                                   ~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!

      USE RAND_GEN_INT
      
      PARAMETER (nu_ph1=6)
      PARAMETER (nu_mv=10)
      PARAMETER (nu_types=7) 
      PARAMETER (nu_classes=5)
      PARAMETER (nu_control=6)
      PARAMETER (nu_de=30)
      PARAMETER (linkdetlen = 0 ) ! detection area of link for actuated signals
      PARAMETER (nu_iGen = 12)
      PARAMETER (noofran=1000000)
      
      REAL InfoStartTime, TransitEnrouteSwitchFlag
      INTEGER PeakSpreadFlag, NoofPS
      INTEGER SkimFlag
      INTEGER MOVESFlag, MovesMode
      REAL SkimOutInt
      INTEGER MixedVehGenMap
      LOGICAL CapKnow
      REAL distscale             ! scale between the link length in network.dat and xy.dat
      INTEGER jrestore           ! counter for the number of vehicles currently loaded in the network    
      INTEGER nThread, FuelOut,error,stop_count_threshold
      INTEGER DemFormat,ReadTransit,GenTransit,Tflswitch,ReadHistArr,DllFlagRM
      REAL DelTolMean, DelTolStd
      REAL, PARAMETER :: AttScale = 100.0
      REAL(kind(1e0)), PARAMETER :: one=1.0e0, zero=0.0e0       
      type(s_options) :: iopti(2)=s_options(0,zero) 

      LOGICAL IniID1,IniID2
      INTEGER maxnu_pa
      INTEGER logout,trajalt,IncClsFlag
      REAL nv_vebuffer,baylength,RTSat,LTSat,t_cr
      INTEGER tmparysize ! read from PARAMETER, control the size of the temporary arrays related to vehicles loading from files
      CHARACTER*200 fpath
      REAL::ranx(noofran)
      INTEGER ranct, rancy
      LOGICAL::misflag = .false.
      LOGICAL::checkflag = .false.
      LOGICAL::checkflaginit = .false.      
      REAL dstmp
      type c1
        REAL pec
        INTEGER(2) num
      end type
      type(c1), ALLOCATABLE :: dstmp2(:,:,:)
      INTEGER::maxid=0
      INTEGER ivct ! counter to check IF pre-trip info class is present
      INTEGER starttime,endtime
      INTEGER amsflag, asgflg, icmint,itersub,genCheck,keepcls,keeptype
      INTEGER::gloch=0
      REAL entrymeter
      INTEGER oldnumcars, numcars
      INTEGER::EpocNum 
      REAL:: ProjPeriod
      INTEGER, ALLOCATABLE::Epoc(:,:)
      INTEGER, ALLOCATABLE::EpocNull(:)
      INTEGER EpocSize,EpocCounter,EpocPeriod, EpocNullNm

      INTEGER::UCDAirOption = 0

	  LOGICAL::callavg = .False.
      INTEGER :: tdspstep = 0
      LOGICAL :: icallksp =.false.
      LOGICAL :: CallKspReadVeh =.false.
	  INTEGER :: tupstep = 0
      INTEGER simPerAgg
      INTEGER simPerAsg
      INTEGER soint
      REAL xminPerSimInt
	  REAL time_now
      INTEGER nout_tag, nout_nontag, numNtag
      INTEGER nout_tag_i,nout_nontag_i
      INTEGER istrm
      INTEGER,ALLOCATABLE::readveh(:,:)

! --  ~~~~~~~~~~~~~~~~~~Network Topology~~~~~~~~~~~~~~~~~~~~~
      INTEGER kno_nu
	  REAL longest_link
	  INTEGER MaxLinkVeh
      INTEGER :: noofnodes = 0
      INTEGER :: noofnodes_org = 0
      INTEGER :: noofarcs = 0
	  INTEGER :: noofarcs_org = 0
      INTEGER noofarcsperzone
      INTEGER*1::SuperZoneSwitch = 0
      INTEGER nzones
      INTEGER noof_master_destinations, noof_master_destinations_alc

      type shelterset
         INTEGER node
         INTEGER(2) cap
         REAL prob
      end type
      type(shelterset),ALLOCATABLE::shelter(:)
      INTEGER shelter_num,shelter_mode
      INTEGER shelter_use

      INTEGER leftcapWb(5,3,7)
      INTEGER leftcapWOb(5,3,7,7)
      INTEGER::NoOfFlowModel=0
      REAL,ALLOCATABLE::p(:)
      INTEGER,ALLOCATABLE::destination(:)
      INTEGER(2),ALLOCATABLE::MasterDest(:) 
      INTEGER(1), ALLOCATABLE::GradeBPnt(:)
      REAL, ALLOCATABLE::LengthBPnt(:,:)
      INTEGER, ALLOCATABLE::TruckBPnt(:)
      REAL, ALLOCATABLE::PCE(:,:,:)      
      INTEGER GradeNum, LenNum, TruckNum
      INTEGER :: no_link_type =1
	  INTEGER :: no_occupancy_level = 1
      REAL time_lov_hot,time_hov_hot	!V
      REAL time_lov_ohot,time_hov_ohot !V
      INTEGER::iactual_lov_hot = 0	!V
      INTEGER::iactual_lov_ohot = 0	!V
      INTEGER::iactual_hov_hot = 0	!V
      INTEGER::iactual_hov_ohot = 0	!V


      INTEGER Link_hot
      INTEGER iread_veh_flag
	  INTEGER iread_veh_count
      INTEGER ktotal_out
	  INTEGER :: NoofVeh = 0
      INTEGER jtotal
      REAL com_frac, fracinf,ribfa,bound

      INTEGER  numGenVeh
      REAL starttm, endtm
      TYPE PROFILE
       INTEGER TYPEID
	   INTEGER ODORFILE
	   REAL    VEHFRAC
	   INTEGER mtcMODE
	   REAL mtcFRAC(5)
	  END TYPE
      TYPE(PROFILE),ALLOCATABLE::VehProfile(:)

	  REAL vhpcefactor,vhpceReturn ! vhpceReturn is to decide within certain ratio of iteMax that the vhpce will be retored
	  INTEGER::JamSwitch = 0 ! swithc for activating the jam switch rule
	  REAL jamSWbar ! threshold for activating the jamswitch rule
	  REAL JamPar ! conestion responsive swith participation rate
	  LOGICAL::allout = .false.
	  REAL:: totalvhpcect = 0
	  INTEGER VehJO,netcheck
	  REAL multf

      INTEGER :: nu_switch = 0

      INTEGER(2),ALLOCATABLE::rgtilnow(:,:)

      type kspint
       INTEGER,pointer::p(:)
       INTEGER(1)::move
      end type

      type kspint2
       INTEGER(2),pointer::p(:)
       INTEGER(1)::move
      end type
            
      type kspREAL
       REAL,pointer::p(:)
       INTEGER(1)::move
      end type
      
      LOGICAL KspMemOnCall
      LOGICAL KspSubOnCall
      INTEGER::MaxMove = 0
      REAL infinity,nil
	  INTEGER kpaths	
	  INTEGER ides
      REAL timeinterval

      INTEGER ArrIndex
! --  The arrival time at a node converted into INTEGER
! --  number. This varibale is used to determiine the time
! --  index for the time-dependent link and node cost.
      LOGICAL Found
! --  A temporary variable to indicate IF the label at the node is updated or not. 
      INTEGER destin
! --  The destination node to which shortest paths are determined.
      LOGICAL finish

      
	INTEGER kay
      type nu_mvdata
        REAL, pointer::p(:)
        INTEGER(1) psize
      end type


      REAL entrymx
      
	 INTEGER:: indelay = 0

	REAL,ALLOCATABLE::astmpt(:)

	REAL,ALLOCATABLE::apen(:,:)

	REAL,ALLOCATABLE::alet(:)

	REAL,ALLOCATABLE::diff(:,:)
      REAL,ALLOCATABLE::apenal(:,:)

      REAL,ALLOCATABLE::lint(:)

	REAL,ALLOCATABLE::lfr(:)


! --  ********** Signals**********
    INTEGER NSigOpt
    REAL SigOptInt      
    INTEGER, ALLOCATABLE::SigOptNid(:),SigOut(:,:)
    INTEGER SignCount
	Type Sign
       INTEGER node
	   INTEGER NofMajor
       INTEGER NofMinor
	end type
	type (Sign), ALLOCATABLE::SignData(:)
	     
	  Type SignApproach
       INTEGER major(4)
       INTEGER minor(4)
      end type
      type (SignApproach), ALLOCATABLE::SignApprh(:)

	  INTEGER NLevel 

      INTEGER NMove

      INTEGER Level2N, Move2N

      INTEGER isig

	  INTEGER isigcount

      INTEGER nodetmp(nu_control)
      INTEGER cmalink(nu_ph1)
      REAL cma(nu_ph1)

      REAL cma_time(nu_ph1)

      REAL::PenForPreventMove = 100000
	  REAL::PenForHOV = 100000
      INTEGER phase

      REAL length
      INTEGER gcycle

      REAL,ALLOCATABLE::strtsig(:)

      INTEGER almov(nu_mv,nu_mv)

    INTEGER justveh, justveh_i
	INTEGER::totalVeh = 0
	INTEGER::MaxVehiclesTable = 0
	INTEGER::MaxVehiclesFile = 0
    REAL tnext,tnextT,tnextH
	INTEGER CntDemtime
	INTEGER CntDemtimeT
	INTEGER CntDemtimeH
    REAL multi, multiT, multiH,multiS
	  INTEGER nints, nintsT, nintsH, nintsS
      REAL,ALLOCATABLE::begint(:)
      REAL,ALLOCATABLE::begintT(:)
      REAL,ALLOCATABLE::begintH(:)


	INTEGER,ALLOCATABLE::NoofGenLinksPerZone(:)
	LOGICAL,ALLOCATABLE::destct(:)
	INTEGER,ALLOCATABLE::LinkNoInZone(:,:)
	REAL,ALLOCATABLE::zoneDemandProfile(:,:,:)
    INTEGER, ALLOCATABLE::numActZone(:,:)
	REAL,ALLOCATABLE::DemCell(:,:)
	REAL,ALLOCATABLE::DemCellT(:,:)
	REAL,ALLOCATABLE::DemCellH(:,:)
	REAL,ALLOCATABLE::DemCellS(:,:)
	REAL,ALLOCATABLE::bg_factor(:)
    INTEGER bg_num
    LOGICAL::loadszdem=.false.

    LOGICAL::truckok, hovok
    INTEGER, ALLOCATABLE::NoofConsPerZone(:)
    INTEGER,ALLOCATABLE::ConNoInZone(:,:)
	REAL,ALLOCATABLE::TotalLinkLenPerZone(:)

    REAL demandsum, demandsumT, demandsumH
    INTEGER demandcut ! decide which demand matrix to be considered for vehicle generation. 
    INTEGER InputCode
	INTEGER TruckCT, AutoCT

	REAL,ALLOCATABLE::DemOrig(:)
	REAL,ALLOCATABLE::DemOrigT(:)
	REAL,ALLOCATABLE::DemOrigH(:)

	REAL,ALLOCATABLE::DemOrigAcu(:,:)
	REAL,ALLOCATABLE::DemOrigAcuT(:,:)
	REAL,ALLOCATABLE::DemOrigAcuH(:,:)

	REAL,ALLOCATABLE::DemDest(:)
	REAL,ALLOCATABLE::DemDestT(:)
	REAL,ALLOCATABLE::DemDestH(:)

	REAL,ALLOCATABLE::DemGenZ(:)
	REAL,ALLOCATABLE::DemGenZT(:)
	REAL,ALLOCATABLE::DemGenZH(:)

! -- ********** Control/Operational Scenarios**********
      Type WZ
       INTEGER FNode
	   INTEGER TNode
       REAL ST          ! starting time
	   REAL ET          ! ending time
	   REAL CapRed      ! percentage of capacity reduction
       INTEGER SpeedLmt ! speed limit in work zone
	   REAL Discharge ! discharge rate at work zone
	   REAL OrigDisChg
	   INTEGER OrigSpdLmt
	  end Type
	  type (WZ), ALLOCATABLE:: WorkZone(:)
      INTEGER WorkZoneNum
      LOGICAL, ALLOCATABLE:: wzstartflag(:)

      type tdtoll
        REAL stime
        REAL etime
	    REAL sov ! cent per mile
	    REAL tck
	    REAL hov
	    INTEGER switch
      end type

      Type Toll
        INTEGER Unode
	    INTEGER DNode
        INTEGER LinkNO
        INTEGER NTD
	    type(tdtoll),ALLOCATABLE::scheme(:)
      end type

      type(toll), ALLOCATABLE::TollLink(:)
      INTEGER NTollLinktmp
	  LOGICAL TollExist,NonZeroToll
	  INTEGER NTollLink
	  REAL TollVOTA, TollVOTT, TollVOTH

      REAL total_hov
      INTEGER inci_num
	  INTEGER listtotal
      INTEGER InfoPM
      INTEGER ipinit
	INTEGER nrate      
	INTEGER dec_num
	REAL,ALLOCATABLE::ramp_par(:,:)
	REAL,ALLOCATABLE::ramp_start(:)
      REAL,ALLOCATABLE::ramp_end(:)
	INTEGER,ALLOCATABLE::detector(:,:)
	REAL,ALLOCATABLE::detector_length(:)
	INTEGER,ALLOCATABLE::detector_ramp(:)
	INTEGER,ALLOCATABLE::det_link(:)
	REAL,ALLOCATABLE::occup(:)

! --  +++++ VMS +++++
      type VMSTypeTwoPathType
       INTEGER node
	   INTEGER link
	  end type
      Type (VMSTypeTwoPathType),ALLOCATABLE::vmstypetwopath(:,:,:)

      INTEGER, ALLOCATABLE:: nodeWVMS(:)
      INTEGER vms_num
      INTEGER,ALLOCATABLE::vmstype(:),vmsnnode(:,:)
      REAL,ALLOCATABLE::vmsrate(:,:)

! --  vmstype(i): the type of the vms sign.
! --  There are three different types of VMS :
! --  1. speed reduction
! --  2. assign a specific path
! --  3. divert vehciles to other paths
      INTEGER,ALLOCATABLE::vms(:,:)
! --  vms(i,j): information for vms number i.
! --  vms(i,1): the link number on which the vms is located.
! --  vms(i,2): its definition depends on the vms type.
! --    type 1: speed threshold (+ or -). IF the link speed is less than
! --            this threshold, increase the link speed by a pecentage
! --            equal to vms(i,3).
! --    type 2: the path number (among the k_paths) to which the vms refers.
! --    type 3: percentage of vehicles to be diverted.
! --  vms(i,3): its definition depends on the vms type.
! --    type 1: the percentage of reduction or increase in the speed of the link
! --            on which the vms is located.
! --    type 2: it is either 0 or a destination number.
! --             when 0, the path specified in vms(i,2) applies for all destinations
! --             when a destination number, the path is applied for vehiles heading
! --             this destination only.
! --    type 3:  number of paths among which vehicles are diverted.
!	INTEGER,ALLOCATABLE::ivms(:)
! --  ivms(j) :  an index to indicate IF a vehicle subjected to a divert vms
! --             reached the end of the link and ready to switch.
      REAL,ALLOCATABLE::vms_start(:)
! --  vms(k): the time at which the vms k starts its operation.
	REAL,ALLOCATABLE::vms_end(:)
	REAL,ALLOCATABLE::inci(:,:)
      LOGICAL,ALLOCATABLE::incistartflag(:)
	INTEGER,ALLOCATABLE::incil(:)
	INTEGER,ALLOCATABLE::incilist(:)
	INTEGER,ALLOCATABLE::itp(:)

! -- ********** Run-Time Link or Network Level Stats**********
	INTEGER itag0, itag1,itag2,itag3,information,noinformation
     REAL dtotal1,dtotal2,dtotal3,vtothr1,vtothr2,vtothr3,triptime1
     REAL, ALLOCATABLE::totaltime(:)
     REAL triptime2,triptime3,STOPtime,STOPinfo,STOPnoinfo,entry_queue1
     REAL entry_queue2,entry_queue3,totaldecsion,ave_trip1
     REAL ave_trip2,ave_trip3,vavg1,vavg2,vavg3,ave_entry1,ave_entry2
     REAL ave_entry3,aveSTOPtime,aveSTOPnoinfo,aveSTOPinfo,avedtotal1
     REAL avedtotal2,avedtotal3
     REAL entry_queue2_1,entry_queue2_2,entry_queue2_3
     REAL entry_queue3_1,entry_queue3_2,entry_queue3_3
     REAL triptime2_1,triptime2_2,triptime2_3
     REAL triptime3_1,triptime3_2,triptime3_3
     REAL vtothr2_1,vtothr2_2,vtothr2_3
     REAL vtothr3_1,vtothr3_2,vtothr3_3
     REAL dtotal2_1,dtotal2_2,dtotal2_3
     REAL dtotal3_1,dtotal3_2,dtotal3_3
     REAL STOPinfo_1,STOPinfo_2,STOPinfo_3
	 REAL STOPnoinfo_1,STOPnoinfo_2,STOPnoinfo_3
      INTEGER information_1,information_2,information_3
      INTEGER noinformation_1,noinformation_2,noinformation_3
      REAL vavg2_1, vavg2_2, vavg2_3, vavg3_1,vavg3_2,vavg3_3
      REAL avedtotal2_1,avedtotal2_2,avedtotal2_3
      REAL avedtotal3_1, avedtotal3_2,avedtotal3_3
      REAL aveSTOPinfo_1, aveSTOPinfo_2,aveSTOPinfo_3
      REAL aveSTOPnoinfo_1,aveSTOPnoinfo_2,aveSTOPnoinfo_3
      REAL ave_entry2_1, ave_entry2_2, ave_entry2_3
      REAL ave_entry3_1, ave_entry3_2, ave_entry3_3
      REAL ave_trip2_1, ave_trip2_2, ave_trip2_3
      REAL ave_trip3_1, ave_trip3_2, ave_trip3_3
      INTEGER(2),ALLOCATABLE::decisionnum(:)
      INTEGER(2),ALLOCATABLE::switchnum(:)
      INTEGER totalswitch
      INTEGER totaldecision
	
      INTEGER i18,i30,i31,i32,i33,i34,i35,i36,i37,i38,i39,i40
      INTEGER i30_t,i31_t,i32_t,i33_t,i34_t,i35_t,i36_t
      INTEGER i37_t,i38_t,i39_t,i40_t,idemand_info, i301,i301_t

      INTEGER int_d
	  REAL::GUITotalTime = 0.0
	
      INTEGER :: noofSTOPs = 1
      REAL, ALLOCATABLE::cost(:,:) ! used by SP

! -- =======================================
! -- II.c: mtc-DynusT Interfacing Variables
! -- =======================================
!     Definition for mtc   
      LOGICAL::RestrictAccess = .false.
      REAL::scale, factorcap
      INTEGER AssMtd
      INTEGER UETimeFlagR,UETimeFlagW
      LOGICAL callksp1
      INTEGER iso_ok,iue_ok,ier_ok
      INTEGER iteMax
	  INTEGER RunMode
	  REAL no_via
	  LOGICAL :: reach_converg = .false.
	  INTEGER:: minconvgite = 1000
	  REAL :: minconvggap = 100000000
      REAL, ALLOCATABLE:: converg(:,:,:)	  
	  INTEGER aggint
	  INTEGER iteration
      REAL SimPeriod
      REAL mtc_diff
      REAL TravelTimeWeight
      INTEGER,ALLOCATABLE :: TravelTime(:,:)
      REAL,ALLOCATABLE :: UETravelTime(:,:)
  
      REAL,ALLOCATABLE :: TravelPenalty(:,:,:)
      REAL,ALLOCATABLE :: UETravelPenalty(:,:,:)

	 type PathAtt 
	   INTEGER   :: node_sum
	   INTEGER   :: node_number
	 end type PathAtt

      INTEGER RoundPass, RoundPassOrig

     INTEGER::mtc_veh(nu_classes) = 0
     INTEGER Iti_nu, iti_nutmp, iti_nuEP ! for projection period
	 INTEGER mtc_path_total
	REAL TotalViolation


    CONTAINS
	
      SUBROUTINE SwapLogArray(arg1,arg2)
	LOGICAL arg1, arg2, c
        c = arg1
	  arg1 = arg2
	  arg2 = c
	END SUBROUTINE

      SUBROUTINE SwapIntArray(arg1,arg2)
	INTEGER arg1, arg2, c
        c = arg1
	  arg1 = arg2
	  arg2 = c
	END SUBROUTINE

      SUBROUTINE SwapIntArray1B(arg1,arg2)
	INTEGER(1) arg1, arg2, c
        c = arg1
	  arg1 = arg2
	  arg2 = c
	END SUBROUTINE

      SUBROUTINE SwapLOGICALArray(arg1,arg2)
	LOGICAL arg1, arg2, c
        c = arg1
	  arg1 = arg2
	  arg2 = c
	END SUBROUTINE
	
      SUBROUTINE SwapIntArray2B(arg1,arg2)
	INTEGER(2) arg1, arg2, c
        c = arg1
	  arg1 = arg2
	  arg2 = c
	END SUBROUTINE

	SUBROUTINE SwapREALArray(arg1,arg2)
	REAL arg1, arg2, c
        c = arg1
	  arg1 = arg2
	  arg2 = c
	END SUBROUTINE			

     SUBROUTINE ErReadEOF(FileUnit)
      CHARACTER *20 FileUnit
	WRITE(911,*) "Error! End-of-file while reading"
      WRITE(911,*) FileUnit
	STOP
	END SUBROUTINE

REAL FUNCTION GetPCE(a,b,c)
 REAL a, b
 INTEGER c
! a: truck %
! b: PCE
! c: volume
! GetPCE: volume equivalent PCE
      GetPCE = c/(1.0/(1+a*(b-1)))
END FUNCTION

END MODULE



