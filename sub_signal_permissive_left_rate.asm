; mark_description "Intel(R) Visual Fortran Compiler XE for applications running on IA-32, Version 13.1.1.171 Build 20130313";
; mark_description "-S";
	.686P
 	.387
	OPTION DOTNAME
	ASSUME	CS:FLAT,DS:FLAT,SS:FLAT
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
TXTST0:
; -- Begin  _SIGNAL_PERMISSIVE_LEFT_RATE
; mark_begin;
IF @Version GE 800
  .MMX
ELSEIF @Version GE 612
  .MMX
  MMWORD TEXTEQU <QWORD>
ENDIF
IF @Version GE 800
  .XMM
ELSEIF @Version GE 614
  .XMM
  XMMWORD TEXTEQU <OWORD>
ENDIF
       ALIGN     16
	PUBLIC _SIGNAL_PERMISSIVE_LEFT_RATE
_SIGNAL_PERMISSIVE_LEFT_RATE	PROC NEAR 
; parameter 1: 8 + ebp
; parameter 2: 12 + ebp
; parameter 3: 16 + ebp
; parameter 4: 20 + ebp
; parameter 5: 24 + ebp
.B1.1:                          ; Preds .B1.0
        push      ebp                                           ;1.12
        mov       ebp, esp                                      ;1.12
        and       esp, -16                                      ;1.12
        push      esi                                           ;1.12
        push      edi                                           ;1.12
        push      ebx                                           ;1.12
        sub       esp, 84                                       ;1.12
        mov       eax, DWORD PTR [20+ebp]                       ;1.12
        mov       eax, DWORD PTR [eax]                          ;33.18
        test      eax, eax                                      ;33.18
        je        .B1.47        ; Prob 20%                      ;33.18
                                ; LOE eax
.B1.2:                          ; Preds .B1.1
        cmp       eax, 1                                        ;33.18
        je        .B1.8         ; Prob 25%                      ;33.18
                                ; LOE eax
.B1.3:                          ; Preds .B1.2
        cmp       eax, 2                                        ;33.18
        jne       .B1.6         ; Prob 67%                      ;33.18
                                ; LOE eax
.B1.4:                          ; Preds .B1.3
        mov       eax, DWORD PTR [8+ebp]                        ;153.10
        mov       edx, DWORD PTR [eax]                          ;153.10
        sub       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;153.46
        imul      edx, edx, 900                                 ;153.46
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;153.7
        cmp       BYTE PTR [45+eax+edx], 0                      ;153.46
        jle       .B1.42        ; Prob 16%                      ;153.46
                                ; LOE eax edx
.B1.5:                          ; Preds .B1.4
        movss     xmm0, DWORD PTR [_DYNUST_MAIN_MODULE_mp_RTSAT] ;154.11
        divss     xmm0, DWORD PTR [_2il0floatpacket.7]          ;154.11
        movss     DWORD PTR [420+eax+edx], xmm0                 ;154.11
        add       esp, 84                                       ;154.11
        pop       ebx                                           ;154.11
        pop       edi                                           ;154.11
        pop       esi                                           ;154.11
        mov       esp, ebp                                      ;154.11
        pop       ebp                                           ;154.11
        ret                                                     ;154.11
                                ; LOE
.B1.6:                          ; Preds .B1.3
        cmp       eax, 4                                        ;33.18
        jne       .B1.41        ; Prob 50%                      ;33.18
                                ; LOE
.B1.7:                          ; Preds .B1.6
        mov       eax, DWORD PTR [8+ebp]                        ;160.7
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;160.7
        neg       edx                                           ;160.7
        add       edx, DWORD PTR [eax]                          ;160.7
        imul      ebx, edx, 900                                 ;160.7
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;160.7
        mov       edi, DWORD PTR [12+ebp]                       ;160.7
        mov       esi, DWORD PTR [440+ecx+ebx]                  ;160.7
        mov       DWORD PTR [edi], esi                          ;160.7
        add       esp, 84                                       ;160.7
        pop       ebx                                           ;160.7
        pop       edi                                           ;160.7
        pop       esi                                           ;160.7
        mov       esp, ebp                                      ;160.7
        pop       ebp                                           ;160.7
        ret                                                     ;160.7
                                ; LOE
.B1.8:                          ; Preds .B1.2
        mov       ebx, DWORD PTR [8+ebp]                        ;43.20
        xor       eax, eax                                      ;
        mov       DWORD PTR [8+esp], eax                        ;
        mov       DWORD PTR [4+esp], eax                        ;
        mov       ecx, DWORD PTR [ebx]                          ;43.20
        imul      ebx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST+32], -44 ;43.7
        mov       DWORD PTR [esp], ecx                          ;43.20
        imul      ecx, ecx, 44                                  ;43.7
        add       ebx, DWORD PTR [_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST] ;43.7
        mov       edx, DWORD PTR [40+ecx+ebx]                   ;43.7
        test      edx, edx                                      ;43.7
        mov       DWORD PTR [64+esp], edx                       ;43.7
        jle       .B1.19        ; Prob 2%                       ;43.7
                                ; LOE ecx ebx
.B1.9:                          ; Preds .B1.8
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;121.9
        mov       DWORD PTR [52+esp], edi                       ;121.9
        mov       esi, DWORD PTR [ecx+ebx]                      ;44.15
        mov       edi, DWORD PTR [32+ecx+ebx]                   ;44.15
        mov       ebx, DWORD PTR [28+ecx+ebx]                   ;44.15
        mov       ecx, ebx                                      ;
        imul      edi, ebx                                      ;
        sub       esi, edi                                      ;
        mov       DWORD PTR [68+esp], esi                       ;
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;
        neg       esi                                           ;
        mov       edi, DWORD PTR [esp]                          ;
        add       esi, edi                                      ;
        imul      esi, esi, 152                                 ;
        mov       DWORD PTR [56+esp], esi                       ;
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+32] ;
        neg       esi                                           ;
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+40] ;50.22
        add       esi, edi                                      ;
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+44] ;
        imul      edi, edx                                      ;
        mov       DWORD PTR [12+esp], edx                       ;50.22
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE] ;
        sub       edx, edi                                      ;
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;47.11
        shl       eax, 8                                        ;
        mov       DWORD PTR [20+esp], 1                         ;
        neg       eax                                           ;
        mov       DWORD PTR [72+esp], ebx                       ;
        lea       edx, DWORD PTR [edx+esi*8]                    ;
        add       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;
        mov       DWORD PTR [16+esp], edx                       ;
        mov       ebx, DWORD PTR [20+esp]                       ;
                                ; LOE eax ecx ebx
.B1.10:                         ; Preds .B1.17 .B1.9
        mov       edx, DWORD PTR [68+esp]                       ;44.9
        mov       edx, DWORD PTR [ecx+edx]                      ;44.9
        test      edx, edx                                      ;46.18
        jle       .B1.17        ; Prob 16%                      ;46.18
                                ; LOE eax edx ecx ebx
.B1.11:                         ; Preds .B1.10
        shl       edx, 8                                        ;44.9
        test      BYTE PTR [112+edx+eax], 1                     ;47.14
        je        .B1.17        ; Prob 60%                      ;47.14
                                ; LOE eax edx ecx ebx
.B1.12:                         ; Preds .B1.11
        mov       edi, DWORD PTR [56+esp]                       ;48.21
        mov       esi, DWORD PTR [52+esp]                       ;48.21
        movsx     esi, BYTE PTR [32+esi+edi]                    ;48.21
        test      esi, esi                                      ;48.14
        mov       DWORD PTR [60+esp], esi                       ;48.21
        jle       .B1.17        ; Prob 2%                       ;48.14
                                ; LOE eax edx ecx ebx edi
.B1.13:                         ; Preds .B1.12
        mov       esi, DWORD PTR [148+edx+eax]                  ;49.20
        shl       esi, 2                                        ;49.20
        neg       esi                                           ;49.20
        add       esi, DWORD PTR [116+edx+eax]                  ;49.20
        mov       DWORD PTR [40+esp], edx                       ;
        mov       DWORD PTR [44+esp], eax                       ;
        mov       edx, DWORD PTR [4+esi]                        ;49.20
        mov       DWORD PTR [24+esp], edx                       ;49.20
        mov       esi, edi                                      ;49.54
        mov       edx, DWORD PTR [52+esp]                       ;49.54
        mov       DWORD PTR [36+esp], 1                         ;
        mov       DWORD PTR [48+esp], ecx                       ;
        mov       edi, DWORD PTR [64+edx+esi]                   ;49.54
        mov       eax, DWORD PTR [68+edx+esi]                   ;49.54
        imul      eax, edi                                      ;
        mov       edx, DWORD PTR [36+edx+esi]                   ;49.54
        sub       edx, eax                                      ;
        mov       DWORD PTR [32+esp], edi                       ;49.54
        mov       DWORD PTR [28+esp], edx                       ;
        mov       DWORD PTR [20+esp], ebx                       ;
        mov       eax, DWORD PTR [28+esp]                       ;
        mov       edx, edi                                      ;
        mov       ecx, DWORD PTR [32+esp]                       ;
        mov       ebx, DWORD PTR [24+esp]                       ;
        mov       esi, DWORD PTR [36+esp]                       ;
        mov       edi, DWORD PTR [60+esp]                       ;
                                ; LOE eax edx ecx ebx esi edi
.B1.14:                         ; Preds .B1.15 .B1.13
        cmp       ebx, DWORD PTR [edx+eax]                      ;49.51
        je        .B1.43        ; Prob 20%                      ;49.51
                                ; LOE eax edx ecx ebx esi edi
.B1.15:                         ; Preds .B1.14
        inc       esi                                           ;58.14
        add       edx, ecx                                      ;58.14
        cmp       esi, edi                                      ;58.14
        jle       .B1.14        ; Prob 82%                      ;58.14
                                ; LOE eax edx ecx ebx esi edi
.B1.16:                         ; Preds .B1.15
        mov       eax, DWORD PTR [44+esp]                       ;
        mov       ecx, DWORD PTR [48+esp]                       ;
        mov       ebx, DWORD PTR [20+esp]                       ;
                                ; LOE eax ecx ebx
.B1.17:                         ; Preds .B1.45 .B1.44 .B1.43 .B1.16 .B1.12
                                ;       .B1.11 .B1.10
        inc       ebx                                           ;62.7
        add       ecx, DWORD PTR [72+esp]                       ;62.7
        cmp       ebx, DWORD PTR [64+esp]                       ;62.7
        jle       .B1.10        ; Prob 82%                      ;62.7
                                ; LOE eax ecx ebx
.B1.19:                         ; Preds .B1.17 .B1.8
        imul      ebx, DWORD PTR [esp], 900                     ;64.7
        imul      edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32], 900 ;64.7
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;64.7
        add       ebx, ecx                                      ;64.7
        sub       ebx, edx                                      ;64.7
        cmp       DWORD PTR [8+esp], 0                          ;65.21
        movsx     eax, BYTE PTR [44+ebx]                        ;64.19
        cvtsi2ss  xmm0, eax                                     ;64.19
        mulss     xmm0, DWORD PTR [_DYNUST_MAIN_MODULE_mp_BAYLENGTH] ;64.53
        divss     xmm0, DWORD PTR [_2il0floatpacket.5]          ;64.7
        jle       .B1.22        ; Prob 16%                      ;65.21
                                ; LOE eax edx ecx ebx xmm0
.B1.20:                         ; Preds .B1.19
        cvtsi2ss  xmm5, DWORD PTR [8+esp]                       ;65.91
        test      eax, eax                                      ;65.83
        jle       .B1.24        ; Prob 16%                      ;65.83
                                ; LOE eax edx ecx ebx xmm0 xmm5
.B1.21:                         ; Preds .B1.20
        comiss    xmm0, xmm5                                    ;65.102
        jb        .B1.24        ; Prob 50%                      ;65.102
                                ; LOE eax edx ecx ebx xmm0 xmm5
.B1.22:                         ; Preds .B1.21 .B1.19
        mov       edi, DWORD PTR [12+ebp]                       ;67.8
        mov       esi, DWORD PTR [440+ebx]                      ;67.8
        movss     xmm2, DWORD PTR [440+ebx]                     ;67.8
        mov       DWORD PTR [edi], esi                          ;67.8
        cmp       DWORD PTR [8+esp], 0                          ;81.19
        jle       .B1.46        ; Prob 16%                      ;81.19
                                ; LOE eax edx ecx ebx xmm2
.B1.23:                         ; Preds .B1.22
        cvtsi2ss  xmm5, DWORD PTR [8+esp]                       ;82.37
        jmp       .B1.28        ; Prob 100%                     ;82.37
                                ; LOE eax edx ecx ebx xmm2 xmm5
.B1.24:                         ; Preds .B1.20 .B1.21
        movss     xmm3, DWORD PTR [_2il0floatpacket.25]         ;71.50
        movss     xmm1, DWORD PTR [688+ebx]                     ;71.10
        comiss    xmm3, xmm1                                    ;71.50
        jbe       .B1.26        ; Prob 50%                      ;71.50
                                ; LOE eax edx ecx ebx xmm0 xmm1 xmm3 xmm5
.B1.25:                         ; Preds .B1.24
        pxor      xmm4, xmm4                                    ;72.12
        jmp       .B1.27        ; Prob 100%                     ;72.12
                                ; LOE eax edx ecx ebx xmm3 xmm4 xmm5
.B1.26:                         ; Preds .B1.24
        movaps    xmm4, xmm5                                    ;74.27
        subss     xmm4, xmm0                                    ;74.27
        divss     xmm4, xmm1                                    ;74.12
                                ; LOE eax edx ecx ebx xmm3 xmm4 xmm5
.B1.27:                         ; Preds .B1.25 .B1.26
        mulss     xmm4, DWORD PTR [_2il0floatpacket.10]         ;77.26
        movss     xmm2, DWORD PTR [440+ebx]                     ;78.16
        addss     xmm3, xmm4                                    ;77.21
        divss     xmm2, xmm3                                    ;78.9
        mov       esi, DWORD PTR [12+ebp]                       ;78.9
        movss     DWORD PTR [esi], xmm2                         ;78.9
                                ; LOE eax edx ecx ebx xmm2 xmm5
.B1.28:                         ; Preds .B1.27 .B1.23
        cvtsi2ss  xmm0, DWORD PTR [4+esp]                       ;82.23
        divss     xmm0, xmm5                                    ;82.36
        cvttss2si esi, xmm0                                     ;82.7
                                ; LOE eax edx ecx ebx esi xmm2
.B1.29:                         ; Preds .B1.28 .B1.46
        mov       edi, DWORD PTR [392+ebx]                      ;86.8
        test      edi, edi                                      ;86.49
        jne       .B1.33        ; Prob 50%                      ;86.49
                                ; LOE eax edx ecx ebx esi edi xmm2
.B1.30:                         ; Preds .B1.29
        mov       edx, 1                                        ;87.10
        test      eax, eax                                      ;87.10
        push      OFFSET FLAT: __NLITPACK_0.0.1                 ;88.15
        cmovg     edx, eax                                      ;87.10
        movsx     eax, BYTE PTR [668+ebx]                       ;87.62
        cvtsi2ss  xmm0, eax                                     ;87.62
        cvtsi2ss  xmm1, edx                                     ;87.100
        divss     xmm2, xmm0                                    ;87.61
        mulss     xmm1, DWORD PTR [_2il0floatpacket.9]          ;87.99
        mulss     xmm1, xmm2                                    ;87.10
        movss     DWORD PTR [416+ebx], xmm1                     ;87.10
        call      _RANXY                                        ;88.15
                                ; LOE f1
.B1.50:                         ; Preds .B1.30
        fstp      DWORD PTR [44+esp]                            ;88.15
        movss     xmm2, DWORD PTR [44+esp]                      ;88.15
        add       esp, 4                                        ;88.15
                                ; LOE xmm2
.B1.31:                         ; Preds .B1.50
        mov       eax, DWORD PTR [8+ebp]                        ;89.19
        mov       edx, DWORD PTR [eax]                          ;89.19
        sub       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;89.15
        imul      ecx, edx, 900                                 ;89.15
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;89.9
        movss     xmm1, DWORD PTR [416+edx+ecx]                 ;89.19
        cvttss2si eax, xmm1                                     ;89.64
        cvtsi2ss  xmm0, eax                                     ;89.64
        subss     xmm1, xmm0                                    ;89.63
        comiss    xmm1, xmm2                                    ;89.15
        jae       .B1.40        ; Prob 50%                      ;89.15
        jmp       .B1.41        ; Prob 100%                     ;89.15
                                ; LOE eax edx ecx
.B1.33:                         ; Preds .B1.29
        imul      eax, edi, 900                                 ;113.26
        pxor      xmm3, xmm3                                    ;113.9
        movss     xmm0, DWORD PTR [_DYNUST_MAIN_MODULE_mp_T_CR] ;115.31
        movss     DWORD PTR [60+esp], xmm0                      ;115.31
        add       ecx, eax                                      ;113.9
        sub       ecx, edx                                      ;113.9
        movss     xmm1, DWORD PTR [440+ecx]                     ;113.26
        mulss     xmm1, DWORD PTR [_2il0floatpacket.7]          ;113.103
        minss     xmm1, xmm3                                    ;113.9
        movss     DWORD PTR [64+esp], xmm1                      ;113.9
        comiss    xmm1, xmm3                                    ;114.23
        ja        .B1.35        ; Prob 22%                      ;114.23
                                ; LOE ebx esi xmm2
.B1.34:                         ; Preds .B1.33
        movss     xmm3, DWORD PTR [_2il0floatpacket.13]         ;117.13
        jmp       .B1.36        ; Prob 100%                     ;117.13
                                ; LOE ebx esi xmm2 xmm3
.B1.35:                         ; Preds .B1.33
        movss     xmm0, DWORD PTR [64+esp]                      ;115.31
        divss     xmm0, DWORD PTR [_2il0floatpacket.11]         ;115.31
        mulss     xmm0, DWORD PTR [60+esp]                      ;115.31
        movss     DWORD PTR [40+esp], xmm2                      ;115.31
        call      ___libm_sse2_expf                             ;115.31
                                ; LOE ebx esi xmm0
.B1.52:                         ; Preds .B1.35
        movss     xmm2, DWORD PTR [40+esp]                      ;
        movaps    xmm3, xmm0                                    ;115.31
        movss     xmm1, DWORD PTR [64+esp]                      ;115.63
        movss     xmm0, DWORD PTR [_2il0floatpacket.12]         ;115.63
        divss     xmm0, xmm2                                    ;115.63
        divss     xmm1, DWORD PTR [_2il0floatpacket.7]          ;115.63
        mulss     xmm0, xmm1                                    ;115.63
        movss     DWORD PTR [44+esp], xmm3                      ;115.63
        call      ___libm_sse2_expf                             ;115.63
                                ; LOE ebx esi xmm0
.B1.51:                         ; Preds .B1.52
        movss     xmm1, DWORD PTR [_2il0floatpacket.25]         ;115.62
        movss     xmm3, DWORD PTR [44+esp]                      ;
        subss     xmm1, xmm0                                    ;115.62
        mulss     xmm3, DWORD PTR [64+esp]                      ;115.30
        movss     xmm2, DWORD PTR [40+esp]                      ;
        divss     xmm3, xmm1                                    ;115.13
                                ; LOE ebx esi xmm2 xmm3
.B1.36:                         ; Preds .B1.51 .B1.34
        movsx     eax, BYTE PTR [668+ebx]                       ;119.61
        cvtsi2ss  xmm1, eax                                     ;119.61
        movss     xmm4, DWORD PTR [672+ebx]                     ;119.27
        divss     xmm4, xmm1                                    ;119.60
        divss     xmm4, DWORD PTR [652+ebx]                     ;119.99
        mov       edx, DWORD PTR [esp]                          ;121.88
        sub       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;121.88
        imul      edi, edx, 152                                 ;121.88
        movss     xmm5, DWORD PTR [_2il0floatpacket.25]         ;121.88
        movss     DWORD PTR [56+esp], xmm1                      ;119.61
        divss     xmm4, DWORD PTR [_2il0floatpacket.14]         ;119.9
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;121.9
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE] ;121.9
        mov       eax, DWORD PTR [24+ecx+edi]                   ;121.18
        sub       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE+32] ;121.88
        imul      ecx, eax, 84                                  ;121.88
        mov       eax, DWORD PTR [32+edx+ecx]                   ;121.18
        shl       eax, 2                                        ;121.88
        neg       eax                                           ;121.88
        add       eax, DWORD PTR [edx+ecx]                      ;121.88
        mov       edi, DWORD PTR [16+eax]                       ;121.12
        cvtsi2ss  xmm6, edi                                     ;121.12
        movss     DWORD PTR [52+esp], xmm6                      ;121.12
        comiss    xmm5, xmm6                                    ;121.88
        jbe       .B1.38        ; Prob 50%                      ;121.88
                                ; LOE ebx esi edi xmm2 xmm3 xmm4
.B1.37:                         ; Preds .B1.36
        mov       DWORD PTR [esp], 0                            ;122.11
        lea       edx, DWORD PTR [esp]                          ;122.11
        mov       DWORD PTR [32+esp], 19                        ;122.11
        lea       eax, DWORD PTR [32+esp]                       ;122.11
        mov       DWORD PTR [36+esp], OFFSET FLAT: __STRLITPACK_0 ;122.11
        push      32                                            ;122.11
        push      eax                                           ;122.11
        push      OFFSET FLAT: __STRLITPACK_1.0.1               ;122.11
        push      -2088435968                                   ;122.11
        push      -1                                            ;122.11
        push      edx                                           ;122.11
        movss     DWORD PTR [72+esp], xmm4                      ;122.11
        movss     DWORD PTR [68+esp], xmm3                      ;122.11
        movss     DWORD PTR [64+esp], xmm2                      ;122.11
        call      _for_write_seq_lis                            ;122.11
                                ; LOE ebx esi edi
.B1.53:                         ; Preds .B1.37
        movss     xmm2, DWORD PTR [64+esp]                      ;
        movss     xmm3, DWORD PTR [68+esp]                      ;
        movss     xmm4, DWORD PTR [72+esp]                      ;
        add       esp, 24                                       ;122.11
                                ; LOE ebx esi edi xmm2 xmm3 xmm4
.B1.38:                         ; Preds .B1.53 .B1.36
        movss     xmm5, DWORD PTR [_2il0floatpacket.26]         ;124.20
        andps     xmm5, xmm4                                    ;124.20
        pxor      xmm4, xmm5                                    ;124.20
        movss     xmm6, DWORD PTR [_2il0floatpacket.27]         ;124.20
        movaps    xmm1, xmm4                                    ;124.20
        mov       eax, DWORD PTR [352+ebx]                      ;127.193
        mov       ecx, eax                                      ;127.133
        shl       ecx, 6                                        ;127.133
        cmpltss   xmm1, xmm6                                    ;124.20
        mulss     xmm3, DWORD PTR [_2il0floatpacket.21]         ;127.44
        andps     xmm6, xmm1                                    ;124.20
        movaps    xmm1, xmm4                                    ;124.20
        addss     xmm1, xmm6                                    ;124.20
        subss     xmm1, xmm6                                    ;124.20
        movss     xmm6, DWORD PTR [_2il0floatpacket.28]         ;124.20
        movaps    xmm7, xmm1                                    ;124.20
        movaps    xmm0, xmm6                                    ;124.20
        subss     xmm7, xmm4                                    ;124.20
        addss     xmm0, xmm6                                    ;124.20
        movaps    xmm4, xmm7                                    ;124.20
        cmpless   xmm7, DWORD PTR [_2il0floatpacket.29]         ;124.20
        cmpnless  xmm4, xmm6                                    ;124.20
        andps     xmm4, xmm0                                    ;124.20
        andps     xmm7, xmm0                                    ;124.20
        movss     xmm6, DWORD PTR [_2il0floatpacket.7]          ;127.26
        subss     xmm1, xmm4                                    ;124.20
        movss     xmm4, DWORD PTR [_2il0floatpacket.16]         ;127.129
        addss     xmm1, xmm7                                    ;124.20
        cvtsi2ss  xmm7, ecx                                     ;127.133
        orps      xmm1, xmm5                                    ;124.20
        movaps    xmm5, xmm6                                    ;127.26
        mulss     xmm5, xmm2                                    ;127.26
        subss     xmm4, xmm7                                    ;127.129
        divss     xmm2, DWORD PTR [56+esp]                      ;127.84
        cvtsi2ss  xmm7, esi                                     ;127.251
        mulss     xmm7, DWORD PTR [_2il0floatpacket.17]         ;127.250
        subss     xmm4, xmm7                                    ;127.244
        movss     xmm7, DWORD PTR [_2il0floatpacket.25]         ;127.76
        divss     xmm7, xmm2                                    ;127.76
        cvtsi2ss  xmm2, eax                                     ;127.193
        cvtss2si  eax, xmm1                                     ;124.20
        subss     xmm7, DWORD PTR [_2il0floatpacket.19]         ;127.123
        cdq                                                     ;124.16
        mulss     xmm7, DWORD PTR [_2il0floatpacket.18]         ;127.71
        idiv      edi                                           ;124.16
        cvtsi2ss  xmm0, edx                                     ;124.16
        subss     xmm4, xmm7                                    ;127.66
        divss     xmm0, DWORD PTR [52+esp]                      ;124.9
        movss     xmm7, DWORD PTR [64+esp]                      ;127.192
        divss     xmm7, xmm2                                    ;127.192
        mulss     xmm7, DWORD PTR [_2il0floatpacket.20]         ;127.180
        mulss     xmm0, DWORD PTR [_2il0floatpacket.24]         ;127.239
        subss     xmm4, xmm7                                    ;127.174
        movss     xmm1, DWORD PTR [_2il0floatpacket.15]         ;145.10
        addss     xmm4, xmm3                                    ;127.38
        movss     xmm3, DWORD PTR [60+esp]                      ;127.60
        push      OFFSET FLAT: __NLITPACK_1.0.1                 ;146.15
        subss     xmm3, DWORD PTR [_2il0floatpacket.23]         ;127.60
        mulss     xmm3, DWORD PTR [_2il0floatpacket.22]         ;127.54
        subss     xmm4, xmm3                                    ;127.50
        addss     xmm4, xmm0                                    ;127.234
        minss     xmm5, xmm4                                    ;145.10
        maxss     xmm1, xmm5                                    ;145.10
        divss     xmm1, xmm6                                    ;145.10
        movss     DWORD PTR [416+ebx], xmm1                     ;145.10
        call      _RANXY                                        ;146.15
                                ; LOE f1
.B1.54:                         ; Preds .B1.38
        fstp      DWORD PTR [44+esp]                            ;146.15
        movss     xmm2, DWORD PTR [44+esp]                      ;146.15
        add       esp, 4                                        ;146.15
                                ; LOE xmm2
.B1.39:                         ; Preds .B1.54
        mov       eax, DWORD PTR [8+ebp]                        ;147.19
        mov       edx, DWORD PTR [eax]                          ;147.19
        sub       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;147.15
        imul      ecx, edx, 900                                 ;147.15
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;147.9
        movss     xmm1, DWORD PTR [416+edx+ecx]                 ;147.19
        cvttss2si eax, xmm1                                     ;147.64
        cvtsi2ss  xmm0, eax                                     ;147.64
        subss     xmm1, xmm0                                    ;147.63
        comiss    xmm1, xmm2                                    ;147.15
        jb        .B1.41        ; Prob 50%                      ;147.15
                                ; LOE eax edx ecx
.B1.40:                         ; Preds .B1.31 .B1.39
        inc       eax                                           ;147.215
        cvtsi2ss  xmm0, eax                                     ;147.117
        movss     DWORD PTR [416+edx+ecx], xmm0                 ;147.117
                                ; LOE
.B1.41:                         ; Preds .B1.31 .B1.39 .B1.6 .B1.40
        add       esp, 84                                       ;164.1
        pop       ebx                                           ;164.1
        pop       edi                                           ;164.1
        pop       esi                                           ;164.1
        mov       esp, ebp                                      ;164.1
        pop       ebp                                           ;164.1
        ret                                                     ;164.1
                                ; LOE
.B1.42:                         ; Preds .B1.4                   ; Infreq
        movss     xmm0, DWORD PTR [_2il0floatpacket.8]          ;156.65
        mulss     xmm0, DWORD PTR [_DYNUST_MAIN_MODULE_mp_RTSAT] ;156.65
        divss     xmm0, DWORD PTR [_2il0floatpacket.7]          ;156.11
        movss     DWORD PTR [420+eax+edx], xmm0                 ;156.11
        add       esp, 84                                       ;156.11
        pop       ebx                                           ;156.11
        pop       edi                                           ;156.11
        pop       esi                                           ;156.11
        mov       esp, ebp                                      ;156.11
        pop       ebp                                           ;156.11
        ret                                                     ;156.11
                                ; LOE
.B1.43:                         ; Preds .B1.14                  ; Infreq
        mov       DWORD PTR [36+esp], esi                       ;
        mov       edi, esi                                      ;50.65
        imul      edi, DWORD PTR [12+esp]                       ;50.65
        mov       esi, DWORD PTR [16+esp]                       ;50.65
        mov       edx, DWORD PTR [40+esp]                       ;
        mov       eax, DWORD PTR [44+esp]                       ;
        mov       ecx, DWORD PTR [48+esp]                       ;
        mov       ebx, DWORD PTR [20+esp]                       ;
        cmp       BYTE PTR [5+esi+edi], 1                       ;50.65
        jne       .B1.17        ; Prob 84%                      ;50.65
                                ; LOE eax edx ecx ebx al dl cl bl ah dh ch bh
.B1.44:                         ; Preds .B1.43                  ; Infreq
        movss     xmm1, DWORD PTR [156+edx+eax]                 ;51.44
        divss     xmm1, DWORD PTR [_2il0floatpacket.6]          ;51.69
        cvtsi2ss  xmm0, DWORD PTR [8+esp]                       ;51.33
        comiss    xmm1, DWORD PTR [_2il0floatpacket.25]         ;52.60
        addss     xmm0, xmm1                                    ;51.43
        cvttss2si edx, xmm0                                     ;51.22
        mov       DWORD PTR [8+esp], edx                        ;51.22
        jbe       .B1.17        ; Prob 50%                      ;52.60
                                ; LOE eax ecx ebx al cl bl ah ch bh xmm1
.B1.45:                         ; Preds .B1.44                  ; Infreq
        cvtsi2ss  xmm0, DWORD PTR [4+esp]                       ;53.40
        addss     xmm1, xmm0                                    ;53.54
        cvttss2si edx, xmm1                                     ;53.24
        mov       DWORD PTR [4+esp], edx                        ;53.24
        jmp       .B1.17        ; Prob 100%                     ;53.24
                                ; LOE eax ecx ebx
.B1.46:                         ; Preds .B1.22                  ; Infreq
        xor       esi, esi                                      ;84.7
        jmp       .B1.29        ; Prob 100%                     ;84.7
                                ; LOE eax edx ecx ebx esi xmm2
.B1.47:                         ; Preds .B1.1                   ; Infreq
        mov       eax, DWORD PTR [8+ebp]                        ;36.7
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;36.7
        neg       edx                                           ;36.7
        add       edx, DWORD PTR [eax]                          ;36.7
        imul      ebx, edx, 900                                 ;36.7
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;36.7
        mov       edi, DWORD PTR [12+ebp]                       ;36.7
        mov       esi, DWORD PTR [432+ecx+ebx]                  ;36.7
        mov       DWORD PTR [edi], esi                          ;36.7
        add       esp, 84                                       ;36.7
        pop       ebx                                           ;36.7
        pop       edi                                           ;36.7
        pop       esi                                           ;36.7
        mov       esp, ebp                                      ;36.7
        pop       ebp                                           ;36.7
        ret                                                     ;36.7
        ALIGN     16
                                ; LOE
; mark_end;
_SIGNAL_PERMISSIVE_LEFT_RATE ENDP
_TEXT	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
__STRLITPACK_1.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__NLITPACK_1.0.1	DD	2
__NLITPACK_0.0.1	DD	1
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _SIGNAL_PERMISSIVE_LEFT_RATE
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
__STRLITPACK_0	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	97
	DB	100
	DB	106
	DB	117
	DB	115
	DB	116
	DB	32
	DB	115
	DB	97
	DB	116
	DB	0
_2il0floatpacket.5	DD	041a00000H
_2il0floatpacket.6	DD	042c80000H
_2il0floatpacket.7	DD	045610000H
_2il0floatpacket.8	DD	03f4ccccdH
_2il0floatpacket.9	DD	040c00000H
_2il0floatpacket.10	DD	03d4ccccdH
_2il0floatpacket.11	DD	0c5610000H
_2il0floatpacket.12	DD	0bf800000H
_2il0floatpacket.13	DD	0447a0000H
_2il0floatpacket.14	DD	03dcccccdH
_2il0floatpacket.15	DD	043480000H
_2il0floatpacket.16	DD	0446fc000H
_2il0floatpacket.17	DD	0407147aeH
_2il0floatpacket.18	DD	043840000H
_2il0floatpacket.19	DD	040000000H
_2il0floatpacket.20	DD	03ebd70a4H
_2il0floatpacket.21	DD	03efef9dbH
_2il0floatpacket.22	DD	042b00000H
_2il0floatpacket.23	DD	040a00000H
_2il0floatpacket.24	DD	0430b0000H
_2il0floatpacket.25	DD	03f800000H
_2il0floatpacket.26	DD	080000000H
_2il0floatpacket.27	DD	04b000000H
_2il0floatpacket.28	DD	03f000000H
_2il0floatpacket.29	DD	0bf000000H
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
EXTRN	_DYNUST_MAIN_MODULE_mp_RTSAT:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_T_CR:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_BAYLENGTH:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE:BYTE
EXTRN	_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH:BYTE
EXTRN	_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKVEHLIST:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE:BYTE
_DATA	ENDS
EXTRN	_for_write_seq_lis:PROC
EXTRN	_RANXY:PROC
EXTRN	___libm_sse2_expf:PROC
EXTRN	__fltused:BYTE
	INCLUDELIB <ifconsol>
	INCLUDELIB <libifcoremt>
	INCLUDELIB <libifport>
	INCLUDELIB <libmmt>
	INCLUDELIB <LIBCMT>
	INCLUDELIB <libirc>
	INCLUDELIB <svml_dispmt>
	INCLUDELIB <OLDNAMES>
	END
