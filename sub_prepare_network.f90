    SUBROUTINE PREP_DATA_STRUCTURE()
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~ Copyright (C) 2002-2011 The Arizona Board of Regents on Behalf of The University of Arizona.  ~~!
!~~ Developed by Yi-Chang Chiu, Ph.D. as par to the DynusT project.                               ~~!
!~~ This program is free software: you can redistribute it and/or modify it                       ~~!
!~~ under the terms of the GNU General Public License version 2 as published by                   ~~!
!~~ the Free Software Foundation.                                                                 ~~!
!~~ This program is distributed in the hope that it will be useful,                               ~~!
!~~ but WITHOUT ANY WARRANTY; without even the implied warranty of                                ~~!
!~~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                                          ~~!
!~~ See the GNU General Public License for more details.                                          ~~!
!~~ You should have received a copy of the GNU General Public License                             ~~!
!~~ along with this program (in the file COPYING).                                                ~~!
!~~ IF not, see <http://www.gnu.org/licenses/>.                                                   ~~!
!~~ Please see http://www.dynust.net for contact information and names                            ~~!
!~~ of individual contributors.                                                                   ~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
! --
      USE DYNUST_MAIN_MODULE
      USE DYNUST_NETWORK_MODULE

      INTEGER fexist, igp
	  REAL minlength
      LOGICAL self
      LOGICAL, ALLOCATABLE::imustsee(:)
!> > > > > > > > > >                < < < < < < < < < < <
!>>>>>>>>>>>>>>>>>>>CODES START HERE<<<<<<<<<<<<<<<<<<<<<
!> > > > > > > > > >                < < < < < < < < < < <
      
      ALLOCATE(imustsee(noofarcs))
      imustsee(:) = .false.
      MaxMove = 0
      DO i=1,noofarcs
      lltmp = 0
      intmp = 0
        DO j=1,noofarcs
          IF(m_dynust_network_arc_nde(j)%iunod == m_dynust_network_arc_nde(i)%idnod) THEN
            lltmp = lltmp + 1
            IF(lltmp > MaxMove) MaxMove = lltmp
          ENDIF
          IF(m_dynust_network_arc_nde(i)%iunod == m_dynust_network_arc_nde(j)%idnod) THEN
            intmp = intmp + 1
            IF(intmp > MaxMove) MaxMove = intmp
          ENDIF
        ENDDO
      ENDDO
      
      DO i=1, noofarcs
        ALLOCATE(m_dynust_network_arc_nde(i)%llink%p(maxmove))
        m_dynust_network_arc_nde(i)%llink%p(:) = 0      
        ALLOCATE(m_dynust_network_arc_nde(i)%llink%m(max(6,maxmove)))
        m_dynust_network_arc_nde(i)%llink%m(:) = 0      
        ALLOCATE(m_dynust_network_arc_nde(i)%inlink%p(maxmove))
        m_dynust_network_arc_nde(i)%inlink%p(:) = 0      
      ENDDO

      DO i=1,noofarcs
      lltmp = 0
      intmp = 0
        DO j=1,noofarcs
          IF(m_dynust_network_arc_nde(j)%iunod == m_dynust_network_arc_nde(i)%idnod) THEN
            lltmp = lltmp + 1
            m_dynust_network_arc_nde(i)%llink%p(lltmp) = j
          ENDIF

          IF(m_dynust_network_arc_nde(i)%iunod == m_dynust_network_arc_nde(j)%idnod) THEN
            intmp = intmp + 1
            m_dynust_network_arc_nde(i)%inlink%p(intmp) = j
          ENDIF
        ENDDO
        m_dynust_network_arc_nde(i)%llink%no = lltmp
        m_dynust_network_arc_nde(i)%inlink%no = intmp
      ENDDO
      

      DO i = 1, noofarcs
	    minlength = 1000
		self=.false.
        DO j = 1,m_dynust_network_arc_nde(i)%llink%no
            IF(m_dynust_network_arc_nde(i)%s+m_dynust_network_arc_nde(m_dynust_network_arc_nde(i)%llink%p(j))%s < minlength) THEN
			   minlength = m_dynust_network_arc_nde(i)%s+m_dynust_network_arc_nde(m_dynust_network_arc_nde(i)%llink%p(j))%s
		       IF(m_dynust_network_arc_nde(i)%iunod == m_dynust_network_arc_nde(m_dynust_network_arc_nde(i)%llink%p(j))%idnod.and.m_dynust_network_arc_nde(i)%idnod == m_dynust_network_arc_nde(m_dynust_network_arc_nde(i)%llink%p(j))%iunod) THEN
			      self=.true.
               ELSE
				  self=.false.
               ENDIF
            ENDIF
		ENDDO
      IF(minlength < ((m_dynust_network_arc_de(i)%SpeedLimit+m_dynust_network_arc_de(i)%Vfadjust)/60.0)/10) THEN
       INQUIRE(UNIT = 511, OPENED = Fexist)
	     IF(.not. Fexist) THEN
	         OPEN(file='Warning.dat',unit=511,status='unknown',iostat=error)
	         IF(error /= 0) THEN
               WRITE(911,*) 'Error when opening Warning.dat'
	           STOP
	         ENDIF
         ENDIF
         sold = m_dynust_network_arc_nde(i)%s
	     IF(.not.self) THEN
		  m_dynust_network_arc_nde(i)%s = m_dynust_network_arc_nde(i)%s + ((m_dynust_network_arc_de(i)%SpeedLimit+m_dynust_network_arc_de(i)%Vfadjust)/60.0)/10 - minlength
	     ELSE
          m_dynust_network_arc_nde(i)%s = (((m_dynust_network_arc_de(i)%SpeedLimit+m_dynust_network_arc_de(i)%Vfadjust)/60.0)/10)/2
		 ENDIF
	     WRITE(511,'("Short link (",i7," -> ",i7,") length= ",f8.1," bcs VMAX= ",f8.1," , Reset to ",f8.1,"  feet")') m_dynust_network_arc_nde(i)%iunod,m_dynust_network_arc_nde(i)%idnod,sold*5280,float(m_dynust_network_arc_de(i)%SpeedLimit+m_dynust_network_arc_de(i)%Vfadjust),m_dynust_network_arc_nde(i)%s*5280

	  ENDIF
     ENDDO


	 
      k=1
      DO i=1,noofnodes
         backpointr(i)=k
        DO j=1,noofarcs
          IF(i == m_dynust_network_arc_nde(j)%idnod) THEN
            m_dynust_network_arc_nde(k)%UNodeOfBackLink=m_dynust_network_arc_nde(j)%iunod
            m_dynust_network_arc_nde(k)%BackToForLink=j
            m_dynust_network_arc_nde(j)%ForToBackLink=k
            k=k+1
          ENDIF
        ENDDO
      ENDDO
      backpointr(noofnodes+1)=k


      DO i = 1, noofarcs

        igp = max(1,backpointr(m_dynust_network_arc_nde(i)%iunod+1)-backpointr(m_dynust_network_arc_nde(i)%iunod))
        IF(igp > 10.or.igp < 1) THEN
          print *, "high number of movements"
        ENDIF
	    DO j = 1, m_dynust_network_arc_nde(i)%inlink%no

          imust = m_dynust_network_arc_nde(m_dynust_network_arc_nde(i)%inlink%p(j))%fortobacklink

! allocate memories for the following four arrays - tentative will assign more arrays either here or in another sub following the preparenetwork sub
        DO isee = 1, aggint
         IF(.not.imustsee(imust)) THEN
         ALLOCATE(m_dynust_network_arc_de(imust)%link_queue(isee)%p(igp))
         m_dynust_network_arc_de(imust)%link_queue(isee)%p(:) = 0
         m_dynust_network_arc_de(imust)%link_queue(isee)%psize = igp
         ENDIF
        ENDDO
        DO isee = 1, max(simPerAgg,tdspstep)
         IF(.not.imustsee(imust)) THEN
         ALLOCATE(m_dynust_network_arc_de(imust)%link_qmp(isee)%p(igp))
         m_dynust_network_arc_de(imust)%link_qmp(isee)%p(:) = 0
         m_dynust_network_arc_de(imust)%link_qmp(isee)%psize = igp
         ENDIF
        ENDDO
        imustsee(imust) = .true.
	    ENDDO

      ENDDO

      DEALLOCATE(imustsee)
52    FORMAT(20i4)







END SUBROUTINE