SUBROUTINE SIM_MAIN_LOOP(maxintervals)
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~ Copyright (C) 2002-2011 The Arizona Board of Regents on Behalf of The University of Arizona.  ~~!
!~~ Developed by Yi-Chang Chiu, Ph.D. as par to the DynusT project.                               ~~!
!~~ This program is free software: you can redistribute it and/or modify it                       ~~!
!~~ under the terms of the GNU General Public License version 2 as published by                   ~~!
!~~ the Free Software Foundation.                                                                 ~~!
!~~ This program is distributed in the hope that it will be useful,                               ~~!
!~~ but WITHOUT ANY WARRANTY; without even the implied warranty of                                ~~!
!~~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                                          ~~!
!~~ See the GNU General Public License for more details.                                          ~~!
!~~ You should have received a copy of the GNU General Public License                             ~~!
!~~ along with this program (in the file COPYING).                                                ~~!
!~~ IF not, see <http://www.gnu.org/licenses/>.                                                   ~~!
!~~ Please see http://www.dynust.net for contact information and names                            ~~!
!~~ of individual contributors.                                                                   ~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
      USE DYNUST_MAIN_MODULE
      USE DYNUST_VEH_MODULE
      USE DYNUST_NETWORK_MODULE
      USE DYNUST_VEH_PATH_ATT_MODULE
      USE INCIDENT_MODULE
      USE WORKZONE_MODULE
      USE VARIABLE_MESSAGE_SIGN_MODULE
      USE MEMORY_MODULE
      USE IFQWIN
      !USE DYNUST_MOVES_MODULE
      
      INTERFACE
        SUBROUTINE printscreen(printstr,intx,inty,dx1,dy1,dx2,dy2,refresh)
          INTEGER intx, inty, refresh,dx1,dy1,dx2,dy2
          CHARACTER (80) printstr
        END SUBROUTINE 
      END INTERFACE  

	  INTEGER maxintervals,soindex,NodeSum, Size
      INTEGER load_veh,dynust_mode_flag,NlnkMv
      LOGICAL FoundFlag, jsfind
      INTEGER tmpnodsum,nnk,aggtime
      CHARACTER (80) printstr1, printstr2
      INTEGER,save::icount_STOP = 0
      INTEGER link_dmain_array(dec_num)  
      REAL cput0, cput1, cput2, cput3, cput4, cput5, cput6, cput7, cput8, cput9, cput10
      REAL,ALLOCATABLE::ttimetmp(:),tpentmp(:,:)
      REAL,ALLOCATABLE::ttimetmpwz(:),tpentmpwz(:,:)      

!> > > > > > > > > >                < < < < < < < < < < <
!>>>>>>>>>>>>>>>>>>>CODES START HERE<<<<<<<<<<<<<<<<<<<<<
!> > > > > > > > > >                < < < < < < < < < < <

      dynust_mode_flag = 0
	
      printstr1 = 'Simulation Now Running'
      CALL printscreen(printstr1,20,395,20,20,500,500,1)

! --
      DO 12 l = starttime, endtime

!	  CALL CPU_TIME (cput0)
      IF(logout > 0) WRITE(711,*) "loop ", l, starttime, endtime

       aggtime = max(1,ifix(float(l-1)/simPerAgg)+1)

      IF(logout > 0) WRITE(711,*) "aggtime", aggtime
            
      IF(mod(l-1,simPerAgg) == 0) THEN
        DO iso = 1, noofarcs
         smp = sum(m_dynust_network_arc_de(iso)%link_smp(1:simPerAgg))/AttScale
         IF(l == 1)THEN
           m_dynust_network_arc_de(iso)%link_speed(aggtime) = m_dynust_network_arc_de(iso)%v*AttScale
         ELSE 
            m_dynust_network_arc_de(iso)%link_speed(aggtime) = smp/simPerAgg*AttScale
         ENDIF

         imust = iso
      
        IF(m_dynust_network_arc_nde(imust)%llink%no > 0) THEN 
         IF(logout > 0) WRITE(711,*) "iso,aggtime,m_dynust_network_arc_de(m_dynust_network_arc_nde(iso)%fortobacklink)%link_queue(aggtime)%psize",iso,aggtime,m_dynust_network_arc_de(m_dynust_network_arc_nde(iso)%fortobacklink)%link_queue(aggtime)%psize
         DO ims = 1, m_dynust_network_arc_de(m_dynust_network_arc_nde(imust)%fortobacklink)%link_queue(aggtime)%psize
           qmp = 0
           DO isee = 1, max(simPerAgg,tdspstep)
            qmp = qmp + m_dynust_network_arc_de(m_dynust_network_arc_nde(imust)%fortobacklink)%link_qmp(isee)%p(ims)
           ENDDO
           m_dynust_network_arc_de(m_dynust_network_arc_nde(imust)%fortobacklink)%link_queue(aggtime)%p(ims) = (qmp/simPerAgg)*AttScale
         ENDDO
        ENDIF
        
        ENDDO  

      ENDIF
      IF(logout > 0) WRITE(711,*) "after link_speed"
      
      icallksp = .false.    

      IF(logout > 0) WRITE(711,*) 'allocate accuvol'

      t_start=(l-1)*xminPerSimInt
      time_now=time_now+xminPerSimInt*60

      IF(iteration == 0.and..not.callavg) THEN 
         CALL MEMALLOCATE_ASSIGNMENT
         callavg = .True.
      ENDIF

      IF(mod(l,nint(1/xminPerSimInt)) == 0) THEN	 
	    CALL NEXTA_STAT(l)
	  ENDIF
      
! -- update the signal timing plan counter
! --
    IF(isig > 1) THEN
        DO mg = 2, isig
            IF((l*xminPerSimInt-strtsig(mg)) < 0.1) THEN
	            isigcount=isigcount+1
	            exit
	        ENDIF
	    ENDDO
    ENDIF

    IF(WorkZoneNum > 0) CALL WZ_SCAN(l*xminPerSimInt) 

    CALL INCI_SCAN(l*xminPerSimInt) !update link capacity and penalty
      
    IF(mod(l-1,tdspstep) == 0) THEN
        CALL CAL_PENALTY(l)
        IF(UETimeFlagR > 0) THEN ! CALL pen_cal sub - overwrite the traveltime and travelpenalty using the read uetraveltime and uettpenalty
            iggtime = max(1,ifix((l-2)*1.0/simPerAgg)+1)
            iggtime2 = min(iggtime,iti_nutmp) ! need to consider when the read data is shorter than simulation

            IF(inci_num > 0) THEN ! incidents exist
                ALLOCATE(ttimetmp(inci_num))
                ttimetmp(:) = 0
                ALLOCATE(tpentmp(inci_num,maxmov))
                tpentmp(:,:) = 0
                DO is = 1, inci_num
                    ttimetmp(is) = TravelTime(incil(is),iggtime)/AttScale
                    tpentmp(is,:) = m_dynust_network_arcmove_nde(m_dynust_network_arc_nde(incil(is))%ForToBackLink,:)%Penalty
                ENDDO
            ENDIF
            IF(wz_num > 0) THEN ! incidents exist
                ALLOCATE(ttimetmpwz(inci_num))
                ttimetmp(:) = 0
                ALLOCATE(tpentmpwz(inci_num,maxmov))
                tpentmp(:,:) = 0
                DO is = 1, inci_num
                    ttimetmpwz(is) = TravelTime(incil(is),iggtime)/AttScale
                    tpentmpwz(is,:) = m_dynust_network_arcmove_nde(m_dynust_network_arc_nde(incil(is))%ForToBackLink,:)%Penalty
                ENDDO
            ENDIF
          
            TravelTime(:,iggtime) = UETravelTime(:,iggtime2)*AttScale ! over write with the UE travel time
            m_dynust_network_arcmove_nde(:,:)%Penalty = UETravelPenalty(:,iggtime2,:)
          
            IF(inci_num > 0) THEN ! write the updated travel time and penalty back
                TravelTime(incil(is),iggtime) = ttimetmp(is)*AttScale
                m_dynust_network_arcmove_nde(m_dynust_network_arc_nde(incil(is))%ForToBackLink,:)%Penalty = tpentmp(is,:)
            ENDIF
            IF(wz_num > 0) THEN ! write the updated travel time and penalty back
                TravelTime(incil(is),iggtime) = ttimetmpwz(is)*AttScale
                m_dynust_network_arcmove_nde(m_dynust_network_arc_nde(incil(is))%ForToBackLink,:)%Penalty = tpentmpwz(is,:)
            ENDIF
        ENDIF
    ENDIF
      
    IF(logout > 0) WRITE(711,*) 'CALL demand generation, penalty and kep'   

    IF(((RunMode == 1.OR.RunMode == 3.OR.RunMode == 4).and.iteration == 0).and.(mod(l-1,tdspstep) == 0.and.l <= begint(nints+1)/xminPerSimInt)) THEN
        CALL GENERATE_DEMAND(l)
    ENDIF
      
    icallksp = .false.   

    IF(((ReadHistArr > 0.AND.RunMode == 2).OR.((RunMode == 1.OR.RunMode == 3.OR.RunMode == 4).and.iteration == 0)).and.mod(l-1,tdspstep) == 0.or. &
        ((vms_num > 0.or.ier_ok == 1).and.mod(l-1,tdspstep) == 0).and..not.CallKspReadVeh) THEN
        CALL TDSP_MAIN(dynust_mode_flag,l)
        !~~~~~~~~~~~~~~~~~Test A*
        !RunningTime = (1/xminPerSimInt)
        ! The followings create A* TDSP data
        !Size = nint(SimPeriod)
        !IF(L > 1) THEN    
        !    CALL Destroy_AStar_Scan_Eligible_Set( Size )
        !    CALL Destroy_AStar_Fixed_Label_Set
        !    CALL Destroy_Labels ()
        !ENDIF

        !CALL Create_AStar_Scan_Eligible_Set( Size )
        !CALL Create_AStar_Fixed_Label_Set
        !CALL Create_Labels (Noofarcs)
        !CALL AStar_Initialization( 2, OutToInNodeNum(111), RunningTime, 0, noofnodes_org ) 
        !CALL AStar_Get_Path( 2, OutToInNodeNum(111), RunningTime, 0, 0 )
        
        !~~~~~~~~~~~~~~~~~Test A*

        jh = 1
!       CALL CPU_TIME (cput1)
        thisistemp = 0
        DO j20 = 1, noof_master_destinations ! loop through the super destinations
            jsfind = .false. 
            DO js = 1, nzones ! loop through original zones
			    IF(MasterDest(js) == j20) THEN
                    jsfind=.true.
				    exit
				ENDIF
			ENDDO 
            IF(jsfind) THEN
!               CALL CPU_Time(cput6)
                !CALL tdsp_core(dynust_mode_flag,j20) ! CALL SP IF THERE IS AT LEAST ONE VEHICLE FOR THIS DESTINATION
!               CALL CPU_Time(cput7)
!               thisistemp = thisistemp + cput7 - cput6
                IF(vms_num > 0.or.ier_ok == 1.OR.(ReadHistArr > 0.and.RunMode == 2)) THEN
                    DO i1 = 1, noofnodes
 	                    IF(i1 <= noofnodes-1) THEN 
	                        imove = backpointr(i1+1)-backpointr(i1)+1
	                    ELSE
	                        imove = noofarcs - backpointr(i1-1) + 1
	                    ENDIF
                        DO i3 = 1, kay
                            DO i5 = 1, min(maxmove,imove)
                                !pathpointercopy1(j20,i1,i3,i5) = pathpointer(i1,1,i3,1)%p(i5)
                                !pathpointercopy2(j20,i1,i3,i5) = pathpointer(i1,1,i3,2)%p(i5)
                                !pathpointercopy3(j20,i1,i3,i5) = pathpointer(i1,1,i3,3)%p(i5)
                                !LabelOutCostCopy(j20,i1,1,i3,i5) = LabelCost(i1,1,i3)%p(i5)
                            ENDDO
                        ENDDO
                    ENDDO
                ENDIF
             
                icallksp = .true.
                IF(((RunMode == 1.OR.RunMode == 3.OR.RunMode == 4).and.iteration == 0).and.(mod(l-1,tdspstep) == 0.and.l <= begint(nints+1)/xminPerSimInt)) THEN               
			        DO while (VehAin(jh,1) == j20)
			        FoundFlag=.false.
                        IF(m_dynust_last_stand(VehAin(jh,4))%vehtype /= 6) THEN
                             CALL RETRIEVE_VEH_PATH_AStar(VehAin(jh,4),VehAin(jh,3),ipinit,1,VehAin(jh,1),1,NodeSum) ! don't assign transit vehicle
                            !CALL RETRIEVE_VEH_PATH(VehAin(jh,4),VehAin(jh,3),ipinit,1,VehAin(jh,1),1,NodeSum) ! don't assignment transit vehicle
                            CALL RETRIEVE_NEXT_LINK(t,VehAin(jh,4),VehAin(jh,3),1,Nlnk,nlnkmv) !! chiu08
                        ENDIF                 
			            jh = jh + 1
				        IF(VehAin(jh,1) < 1) exit
			        ENDDO
                ENDIF 
            ENDIF
		ENDDO !j20 = 1, noof_master_destinations ! lo
    ENDIF !IF(RunMode == 1.and.iteration == 0) THEN

	IF((RunMode /= 1).and.iteration == 0) THEN ! 2: VEH+PATH, 0: VEH ONLY, 3: demand+veh, 4: demand+veh+path
	    IF(RunMode == 0.OR.RunMode == 2) m_dynust_network_arc_de(:)%vlg=0
	    IF(mod(l-1,tdspstep) == 0) THEN
	        CALL READ_VEHICLES(t_start,l)
	    ENDIF
	ELSEIF(iteration > 0) THEN
	    m_dynust_network_arc_de(:)%vlg=0
	    load_veh = 0
        DO ji=jrestore,jtotal
            IF(abs(m_dynust_last_stand(ji)%stime-t_start) > 0.01) goto 1920
            m_dynust_network_arc_de(m_dynust_last_stand(ji)%isec)%vlg=m_dynust_network_arc_de(m_dynust_last_stand(ji)%isec)%vlg+1
            m_dynust_veh_nde(ji)%icurrnt=1
            IF(m_dynust_last_stand(ji)%stime >= starttm.and.m_dynust_last_stand(ji)%stime < endtm) THEN
                m_dynust_veh(ji)%itag=1
                load_veh = load_veh + 1
	        ELSE 
	            m_dynust_veh(ji)%itag=0
	        ENDIF
        ENDDO
1920    jrestore=ji
    ENDIF


! --
! -- VMS_MAIN is a SUBROUTINE for vms operation
! -- vms_num : is the number of vms.
! --
    IF(vms_num > 0) CALL VMS_MAIN(t_start)

!   CALL CPU_TIME (cput3)
    CALL SIM_MASTER(l,t_start,endtime)
!   CALL CPU_TIME (cput4)       
!   WRITE(811,'("Main Loop - after sim", f10.5)') cput4-cput3
       
    IF(iteration >= 0) THEN !utep
       IF(numcars == 0.and.numNtag == 0) THEN
       IF(RunMode == 1.OR.RunMode == 3.OR.RunMode == 4) THEN
         nns = nints
       ELSE
         nns = 1
       ENDIF
       
	   IF(((RunMode == 1.OR.RunMode == 3.OR.RunMode == 4).AND.l > begint(nns)/xminPerSimInt).OR.(RunMode == 2.AND.justveh == MaxVehiclesFile )) THEN ! quit only IF l goes beyond the demand
          WRITE(666,*) '**************************************************'
          WRITE(666,*) 'The program reached the end of simulation because:'
          WRITE(666,*) 'all target vehicles have reached their destinations'
          WRITE(666,*) '**************************************************'
          AllOut = .true.
          go to 975
	   ENDIF
      ENDIF
	ENDIF

       IF(dec_num > 0) THEN
         intime=nint(1/xminPerSimInt) 
         IF(mod(l,nrate) == 0) THEN
          IF(DllFlagRM == 0) THEN ! CALL THE STATIC LIBRARY
            CALL RAMP_Metering(t)
          ELSE ! call Dll
            DO i=1,dec_num
                link_dmain_array(i)=GetFLinkFromNode(detector(i,2),detector(i,3),16)
            ENDDO
           call Ramp_Meter_DLL(DllFlagRM,NoOfFlowModel,noofnodes,noofarcs,t_start,dec_num,det_link,detector_ramp,ramp_start,ramp_end,ramp_par,m_dynust_network_arc_de,nrate,xminPerSimInt,detector,backpointr,VKFunc,link_dmain_array,occup,detector_length,m_dynust_network_arc_nde)
          ENDIF
         ENDIF
       ENDIF 
! --
! --
      IF(numcars == oldnumcars) icount_STOP=icount_STOP+1
      IF(numcars /= oldnumcars) icount_STOP=0
      oldnumcars=numcars
! --
      IF(iteration > 0) THEN
      IF(icount_STOP >= stop_count_threshold*10.and.t_start >= begint(nints+1)) THEN
      WRITE(666,*) '**************************************************'
      WRITE(666,*) 'The program reached the end of simulation because:'
      WRITE(666,*) 'there are no target vehicles getting out of network'
      WRITE(666,*) 'for',stop_count_threshold, 'minutes' 
      WRITE(666,*) '**************************************************'
      AllOut = .false.
      go to 975
      ENDIF
	ENDIF

   IF(mod(l,tdspstep) == 0) THEN
      num_gen=justveh-justveh_i

!      WRITE(6666,3411) l*xminPerSimInt,justveh,num_gen,nout_nontag-nout_nontag_i,nout_tag-nout_tag_i,numcars

      WRITE(printstr1,'(i2)') iteration

      printstr1 = 'Iteration  '//printstr1
      CALL printscreen(printstr1,300,395,20,5,400,80,1)

      printstr1 = 'Running Simulation... Current Time (min)=  '

      CALL printscreen(printstr1,20,445,0,20,800,80,0)

      WRITE(printstr2,'(f7.1)') l*xminPerSimInt 
      printstr2 = trim(printstr2)       
      
      CALL printscreen(printstr2,370,450,1,1,200,5,1)
      WRITE(666,3411) l*xminPerSimInt,justveh,num_gen,nout_nontag-nout_nontag_i,nout_tag-nout_tag_i,numcars      

3411  FORMAT('T:',f6.1,' Tot Veh: ',I9,' Gen: ',i7,' Out_n: ',I7,' Out_t:',I7,' In_v:',I7)
         justveh_i = justveh
         nout_nontag_i = nout_nontag
         nout_tag_i = nout_tag
     
    ENDIF

      !IF(MovesFlag > 0.AND.MOD(L,600) == 0.AND.(reach_converg.OR.iteration == iteMax)) THEN ! print out each hour
      !  thisTime = L*xminPerSimInt
      !  call WriteOut_Moves(thisTime)
      !ENDIF

12    CONTINUE


975   CONTINUE
      MaxIntervals = min(L,endtime)

      OPEN(file="sim_terminate.dat",unit=901,status="unknown")
	  WRITE(901,*) min(SimPeriod,L*xminPerSimInt), min(nint(SimPeriod/xminPerSimInt),L)
	  close(901)
      CALL NEXTA_STAT(l)

      IF(ALLOCATED(ttimetmp)) DEALLOCATE(ttimetmp)
      IF(ALLOCATED(tpentmp)) DEALLOCATE(tpentmp)
      IF(ALLOCATED(ttimetmpwz)) DEALLOCATE(ttimetmpwz)
      IF(ALLOCATED(tpentmpwz)) DEALLOCATE(tpentmpwz)
    
END SUBROUTINE


!~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>
SUBROUTINE printscreen(printstr,intx,inty,dx1,dy1,dx2,dy2,refresh)
USE IFQWIN
USE DYNUST_MAIN_MODULE
USE DYNUST_NETWORK_MODULE
type (xycoord) pos
INTEGER intx, inty, refresh,dx1,dy1,dx2,dy2

LOGICAL(4) result, STATUS
CHARACTER (80) printstr
IF(refresh > 0) THEN ! refresh
  result = SETCOLORRGB(Z'a00000')  
  result = RECTANGLE ($GFILLINTERIOR,intx-dx1,inty-dy1,intx+dx2,inty+dy2)
ENDIF

result = SETFONT('t''Arial''h20w7v')
grstat=SETCOLOR(INT2(7))

CALL MOVETO(INT2(intx),INT2(inty),pos)

CALL OUTGTEXT (printstr)

END SUBROUTINE

