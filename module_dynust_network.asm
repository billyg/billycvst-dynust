; mark_description "Intel(R) Visual Fortran Compiler XE for applications running on IA-32, Version 13.1.1.171 Build 20130313";
; mark_description "-S";
	.686P
 	.387
	OPTION DOTNAME
	ASSUME	CS:FLAT,DS:FLAT,SS:FLAT
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
TXTST0:
; -- Begin  _DYNUST_NETWORK_MODULE$
; mark_begin;
IF @Version GE 800
  .MMX
ELSEIF @Version GE 612
  .MMX
  MMWORD TEXTEQU <QWORD>
ENDIF
IF @Version GE 800
  .XMM
ELSEIF @Version GE 614
  .XMM
  XMMWORD TEXTEQU <OWORD>
ENDIF
       ALIGN     16
	PUBLIC _DYNUST_NETWORK_MODULE$
_DYNUST_NETWORK_MODULE$	PROC NEAR 
.B1.1:                          ; Preds .B1.0
        ret                                                     ;1.8
        ALIGN     16
                                ; LOE
; mark_end;
_DYNUST_NETWORK_MODULE$ ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_NETWORK_MODULE$
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_NETWORK_MODULE_mp_GETFLINKFROMNODE
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_NETWORK_MODULE_mp_GETFLINKFROMNODE
_DYNUST_NETWORK_MODULE_mp_GETFLINKFROMNODE	PROC NEAR 
; parameter 1: 8 + ebp
; parameter 2: 12 + ebp
; parameter 3: 16 + ebp
.B2.1:                          ; Preds .B2.0
        push      ebp                                           ;218.24
        mov       ebp, esp                                      ;218.24
        and       esp, -16                                      ;218.24
        push      esi                                           ;218.24
        push      edi                                           ;218.24
        push      ebx                                           ;218.24
        sub       esp, 212                                      ;218.24
        mov       eax, DWORD PTR [8+ebp]                        ;218.24
        mov       edx, DWORD PTR [12+ebp]                       ;218.24
        mov       edi, DWORD PTR [eax]                          ;222.7
        test      edi, edi                                      ;222.16
        mov       esi, DWORD PTR [edx]                          ;267.14
        jle       .B2.12        ; Prob 1%                       ;222.16
                                ; LOE esi edi
.B2.2:                          ; Preds .B2.1
        test      esi, esi                                      ;222.29
        jle       .B2.12        ; Prob 1%                       ;222.29
                                ; LOE esi edi
.B2.3:                          ; Preds .B2.77 .B2.2
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_BACKPOINTR] ;267.7
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_BACKPOINTR+32] ;267.7
        shl       eax, 2                                        ;267.7
        lea       edx, DWORD PTR [ebx+esi*4]                    ;267.7
        sub       edx, eax                                      ;267.7
        mov       ecx, DWORD PTR [4+edx]                        ;267.32
        dec       ecx                                           ;267.7
        mov       ebx, DWORD PTR [edx]                          ;267.7
        imul      edx, ebx, 152                                 ;
        cmp       ecx, ebx                                      ;267.7
        jl        .B2.7         ; Prob 10%                      ;267.7
                                ; LOE edx ecx ebx edi
.B2.4:                          ; Preds .B2.3
        imul      eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32], -152 ;
        add       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;
                                ; LOE eax edx ecx ebx edi
.B2.5:                          ; Preds .B2.6 .B2.4
        cmp       edi, DWORD PTR [4+edx+eax]                    ;268.56
        je        .B2.9         ; Prob 20%                      ;268.56
                                ; LOE eax edx ecx ebx edi
.B2.6:                          ; Preds .B2.5
        inc       ebx                                           ;272.7
        add       edx, 152                                      ;272.7
        cmp       ebx, ecx                                      ;272.7
        jle       .B2.5         ; Prob 82%                      ;272.7
                                ; LOE eax edx ecx ebx edi
.B2.7:                          ; Preds .B2.3 .B2.6
        xor       ebx, ebx                                      ;
                                ; LOE ebx
.B2.8:                          ; Preds .B2.7 .B2.56 .B2.9
        mov       eax, ebx                                      ;276.7
        add       esp, 212                                      ;276.7
        pop       ebx                                           ;276.7
        pop       edi                                           ;276.7
        pop       esi                                           ;276.7
        mov       esp, ebp                                      ;276.7
        pop       ebp                                           ;276.7
        ret                                                     ;276.7
                                ; LOE
.B2.9:                          ; Preds .B2.5                   ; Infreq
        mov       ebx, DWORD PTR [16+edx+eax]                   ;269.7
        test      ebx, ebx                                      ;273.24
        jge       .B2.8         ; Prob 84%                      ;273.24
                                ; LOE ebx
.B2.10:                         ; Preds .B2.9                   ; Infreq
        mov       DWORD PTR [160+esp], 0                        ;274.10
        lea       edx, DWORD PTR [160+esp]                      ;274.10
        mov       DWORD PTR [192+esp], 25                       ;274.10
        lea       eax, DWORD PTR [192+esp]                      ;274.10
        mov       DWORD PTR [196+esp], OFFSET FLAT: __STRLITPACK_81 ;274.10
        push      32                                            ;274.10
        push      eax                                           ;274.10
        push      OFFSET FLAT: __STRLITPACK_143.0.2             ;274.10
        push      -2088435968                                   ;274.10
        push      -1                                            ;274.10
        push      edx                                           ;274.10
        call      _for_write_seq_lis                            ;274.10
                                ; LOE ebx
.B2.56:                         ; Preds .B2.10                  ; Infreq
        add       esp, 24                                       ;274.10
        jmp       .B2.8         ; Prob 100%                     ;274.10
                                ; LOE ebx
.B2.12:                         ; Preds .B2.2 .B2.1             ; Infreq
        mov       DWORD PTR [160+esp], 0                        ;223.8
        lea       ebx, DWORD PTR [160+esp]                      ;223.8
        mov       DWORD PTR [152+esp], 25                       ;223.8
        lea       eax, DWORD PTR [152+esp]                      ;223.8
        mov       DWORD PTR [156+esp], OFFSET FLAT: __STRLITPACK_120 ;223.8
        push      32                                            ;223.8
        push      eax                                           ;223.8
        push      OFFSET FLAT: __STRLITPACK_122.0.2             ;223.8
        push      -2088435968                                   ;223.8
        push      911                                           ;223.8
        push      ebx                                           ;223.8
        call      _for_write_seq_lis                            ;223.8
                                ; LOE ebx esi edi
.B2.57:                         ; Preds .B2.12                  ; Infreq
        add       esp, 24                                       ;223.8
                                ; LOE ebx esi edi
.B2.13:                         ; Preds .B2.57                  ; Infreq
        mov       eax, DWORD PTR [16+ebp]                       ;218.24
        mov       eax, DWORD PTR [eax]                          ;224.21
        dec       eax                                           ;224.21
        cmp       eax, 18                                       ;224.21
        ja        .B2.53        ; Prob 50%                      ;224.21
                                ; LOE eax ebx esi edi
.B2.14:                         ; Preds .B2.13                  ; Infreq
        jmp       DWORD PTR [..1..TPKT.2_0.0.2+eax*4]           ;224.21
                                ; LOE ebx esi edi
..1.2_0.TAG.013.0.2::
.B2.16:                         ; Preds .B2.14                  ; Infreq
        mov       DWORD PTR [160+esp], 0                        ;262.17
        lea       eax, DWORD PTR [144+esp]                      ;262.17
        mov       DWORD PTR [144+esp], 26                       ;262.17
        mov       DWORD PTR [148+esp], OFFSET FLAT: __STRLITPACK_82 ;262.17
        push      32                                            ;262.17
        push      eax                                           ;262.17
        push      OFFSET FLAT: __STRLITPACK_141.0.2             ;262.17
        push      -2088435968                                   ;262.17
        push      911                                           ;262.17
        push      ebx                                           ;262.17
        call      _for_write_seq_lis                            ;262.17
        jmp       .B2.76        ; Prob 100%                     ;262.17
                                ; LOE esi edi
..1.2_0.TAG.012.0.2::
.B2.18:                         ; Preds .B2.14                  ; Infreq
        mov       DWORD PTR [160+esp], 0                        ;260.17
        lea       eax, DWORD PTR [136+esp]                      ;260.17
        mov       DWORD PTR [136+esp], 34                       ;260.17
        mov       DWORD PTR [140+esp], OFFSET FLAT: __STRLITPACK_84 ;260.17
        push      32                                            ;260.17
        push      eax                                           ;260.17
        push      OFFSET FLAT: __STRLITPACK_140.0.2             ;260.17
        push      -2088435968                                   ;260.17
        push      911                                           ;260.17
        push      ebx                                           ;260.17
        call      _for_write_seq_lis                            ;260.17
        jmp       .B2.76        ; Prob 100%                     ;260.17
                                ; LOE esi edi
..1.2_0.TAG.011.0.2::
.B2.20:                         ; Preds .B2.14                  ; Infreq
        mov       DWORD PTR [160+esp], 0                        ;258.17
        lea       eax, DWORD PTR [128+esp]                      ;258.17
        mov       DWORD PTR [128+esp], 34                       ;258.17
        mov       DWORD PTR [132+esp], OFFSET FLAT: __STRLITPACK_86 ;258.17
        push      32                                            ;258.17
        push      eax                                           ;258.17
        push      OFFSET FLAT: __STRLITPACK_139.0.2             ;258.17
        push      -2088435968                                   ;258.17
        push      911                                           ;258.17
        push      ebx                                           ;258.17
        call      _for_write_seq_lis                            ;258.17
        jmp       .B2.76        ; Prob 100%                     ;258.17
                                ; LOE esi edi
..1.2_0.TAG.010.0.2::
.B2.22:                         ; Preds .B2.14                  ; Infreq
        mov       DWORD PTR [160+esp], 0                        ;256.17
        lea       eax, DWORD PTR [120+esp]                      ;256.17
        mov       DWORD PTR [120+esp], 30                       ;256.17
        mov       DWORD PTR [124+esp], OFFSET FLAT: __STRLITPACK_88 ;256.17
        push      32                                            ;256.17
        push      eax                                           ;256.17
        push      OFFSET FLAT: __STRLITPACK_138.0.2             ;256.17
        push      -2088435968                                   ;256.17
        push      911                                           ;256.17
        push      ebx                                           ;256.17
        call      _for_write_seq_lis                            ;256.17
        jmp       .B2.76        ; Prob 100%                     ;256.17
                                ; LOE esi edi
..1.2_0.TAG.0f.0.2::
.B2.24:                         ; Preds .B2.14                  ; Infreq
        mov       DWORD PTR [160+esp], 0                        ;254.17
        lea       eax, DWORD PTR [112+esp]                      ;254.17
        mov       DWORD PTR [112+esp], 26                       ;254.17
        mov       DWORD PTR [116+esp], OFFSET FLAT: __STRLITPACK_90 ;254.17
        push      32                                            ;254.17
        push      eax                                           ;254.17
        push      OFFSET FLAT: __STRLITPACK_137.0.2             ;254.17
        push      -2088435968                                   ;254.17
        push      911                                           ;254.17
        push      ebx                                           ;254.17
        call      _for_write_seq_lis                            ;254.17
        jmp       .B2.76        ; Prob 100%                     ;254.17
                                ; LOE esi edi
..1.2_0.TAG.0e.0.2::
.B2.26:                         ; Preds .B2.14                  ; Infreq
        mov       DWORD PTR [160+esp], 0                        ;252.17
        lea       eax, DWORD PTR [104+esp]                      ;252.17
        mov       DWORD PTR [104+esp], 25                       ;252.17
        mov       DWORD PTR [108+esp], OFFSET FLAT: __STRLITPACK_92 ;252.17
        push      32                                            ;252.17
        push      eax                                           ;252.17
        push      OFFSET FLAT: __STRLITPACK_136.0.2             ;252.17
        push      -2088435968                                   ;252.17
        push      911                                           ;252.17
        push      ebx                                           ;252.17
        call      _for_write_seq_lis                            ;252.17
        jmp       .B2.76        ; Prob 100%                     ;252.17
                                ; LOE esi edi
..1.2_0.TAG.0d.0.2::
.B2.28:                         ; Preds .B2.14                  ; Infreq
        mov       DWORD PTR [160+esp], 0                        ;250.17
        lea       eax, DWORD PTR [96+esp]                       ;250.17
        mov       DWORD PTR [96+esp], 41                        ;250.17
        mov       DWORD PTR [100+esp], OFFSET FLAT: __STRLITPACK_94 ;250.17
        push      32                                            ;250.17
        push      eax                                           ;250.17
        push      OFFSET FLAT: __STRLITPACK_135.0.2             ;250.17
        push      -2088435968                                   ;250.17
        push      911                                           ;250.17
        push      ebx                                           ;250.17
        call      _for_write_seq_lis                            ;250.17
        jmp       .B2.76        ; Prob 100%                     ;250.17
                                ; LOE esi edi
..1.2_0.TAG.0c.0.2::
.B2.30:                         ; Preds .B2.14                  ; Infreq
        mov       DWORD PTR [160+esp], 0                        ;248.17
        lea       eax, DWORD PTR [88+esp]                       ;248.17
        mov       DWORD PTR [88+esp], 28                        ;248.17
        mov       DWORD PTR [92+esp], OFFSET FLAT: __STRLITPACK_96 ;248.17
        push      32                                            ;248.17
        push      eax                                           ;248.17
        push      OFFSET FLAT: __STRLITPACK_134.0.2             ;248.17
        push      -2088435968                                   ;248.17
        push      911                                           ;248.17
        push      ebx                                           ;248.17
        call      _for_write_seq_lis                            ;248.17
        jmp       .B2.76        ; Prob 100%                     ;248.17
                                ; LOE esi edi
..1.2_0.TAG.0b.0.2::
.B2.32:                         ; Preds .B2.14                  ; Infreq
        mov       DWORD PTR [160+esp], 0                        ;246.17
        lea       eax, DWORD PTR [80+esp]                       ;246.17
        mov       DWORD PTR [80+esp], 24                        ;246.17
        mov       DWORD PTR [84+esp], OFFSET FLAT: __STRLITPACK_98 ;246.17
        push      32                                            ;246.17
        push      eax                                           ;246.17
        push      OFFSET FLAT: __STRLITPACK_133.0.2             ;246.17
        push      -2088435968                                   ;246.17
        push      911                                           ;246.17
        push      ebx                                           ;246.17
        call      _for_write_seq_lis                            ;246.17
        jmp       .B2.76        ; Prob 100%                     ;246.17
                                ; LOE esi edi
..1.2_0.TAG.0a.0.2::
.B2.34:                         ; Preds .B2.14                  ; Infreq
        mov       DWORD PTR [160+esp], 0                        ;244.17
        lea       eax, DWORD PTR [72+esp]                       ;244.17
        mov       DWORD PTR [72+esp], 27                        ;244.17
        mov       DWORD PTR [76+esp], OFFSET FLAT: __STRLITPACK_100 ;244.17
        push      32                                            ;244.17
        push      eax                                           ;244.17
        push      OFFSET FLAT: __STRLITPACK_132.0.2             ;244.17
        push      -2088435968                                   ;244.17
        push      911                                           ;244.17
        push      ebx                                           ;244.17
        call      _for_write_seq_lis                            ;244.17
        jmp       .B2.76        ; Prob 100%                     ;244.17
                                ; LOE esi edi
..1.2_0.TAG.09.0.2::
.B2.36:                         ; Preds .B2.14                  ; Infreq
        mov       DWORD PTR [160+esp], 0                        ;242.17
        lea       eax, DWORD PTR [64+esp]                       ;242.17
        mov       DWORD PTR [64+esp], 22                        ;242.17
        mov       DWORD PTR [68+esp], OFFSET FLAT: __STRLITPACK_102 ;242.17
        push      32                                            ;242.17
        push      eax                                           ;242.17
        push      OFFSET FLAT: __STRLITPACK_131.0.2             ;242.17
        push      -2088435968                                   ;242.17
        push      911                                           ;242.17
        push      ebx                                           ;242.17
        call      _for_write_seq_lis                            ;242.17
        jmp       .B2.76        ; Prob 100%                     ;242.17
                                ; LOE esi edi
..1.2_0.TAG.08.0.2::
.B2.38:                         ; Preds .B2.14                  ; Infreq
        mov       DWORD PTR [160+esp], 0                        ;240.17
        lea       eax, DWORD PTR [56+esp]                       ;240.17
        mov       DWORD PTR [56+esp], 27                        ;240.17
        mov       DWORD PTR [60+esp], OFFSET FLAT: __STRLITPACK_104 ;240.17
        push      32                                            ;240.17
        push      eax                                           ;240.17
        push      OFFSET FLAT: __STRLITPACK_130.0.2             ;240.17
        push      -2088435968                                   ;240.17
        push      911                                           ;240.17
        push      ebx                                           ;240.17
        call      _for_write_seq_lis                            ;240.17
        jmp       .B2.76        ; Prob 100%                     ;240.17
                                ; LOE esi edi
..1.2_0.TAG.07.0.2::
.B2.40:                         ; Preds .B2.14                  ; Infreq
        mov       DWORD PTR [160+esp], 0                        ;238.17
        lea       eax, DWORD PTR [48+esp]                       ;238.17
        mov       DWORD PTR [48+esp], 27                        ;238.17
        mov       DWORD PTR [52+esp], OFFSET FLAT: __STRLITPACK_106 ;238.17
        push      32                                            ;238.17
        push      eax                                           ;238.17
        push      OFFSET FLAT: __STRLITPACK_129.0.2             ;238.17
        push      -2088435968                                   ;238.17
        push      911                                           ;238.17
        push      ebx                                           ;238.17
        call      _for_write_seq_lis                            ;238.17
        jmp       .B2.76        ; Prob 100%                     ;238.17
                                ; LOE esi edi
..1.2_0.TAG.06.0.2::
.B2.42:                         ; Preds .B2.14                  ; Infreq
        mov       DWORD PTR [160+esp], 0                        ;236.17
        lea       eax, DWORD PTR [40+esp]                       ;236.17
        mov       DWORD PTR [40+esp], 23                        ;236.17
        mov       DWORD PTR [44+esp], OFFSET FLAT: __STRLITPACK_108 ;236.17
        push      32                                            ;236.17
        push      eax                                           ;236.17
        push      OFFSET FLAT: __STRLITPACK_128.0.2             ;236.17
        push      -2088435968                                   ;236.17
        push      911                                           ;236.17
        push      ebx                                           ;236.17
        call      _for_write_seq_lis                            ;236.17
        jmp       .B2.76        ; Prob 100%                     ;236.17
                                ; LOE esi edi
..1.2_0.TAG.05.0.2::
.B2.44:                         ; Preds .B2.14                  ; Infreq
        mov       DWORD PTR [160+esp], 0                        ;234.17
        lea       eax, DWORD PTR [32+esp]                       ;234.17
        mov       DWORD PTR [32+esp], 34                        ;234.17
        mov       DWORD PTR [36+esp], OFFSET FLAT: __STRLITPACK_110 ;234.17
        push      32                                            ;234.17
        push      eax                                           ;234.17
        push      OFFSET FLAT: __STRLITPACK_127.0.2             ;234.17
        push      -2088435968                                   ;234.17
        push      911                                           ;234.17
        push      ebx                                           ;234.17
        call      _for_write_seq_lis                            ;234.17
        jmp       .B2.76        ; Prob 100%                     ;234.17
                                ; LOE esi edi
..1.2_0.TAG.04.0.2::
.B2.46:                         ; Preds .B2.14                  ; Infreq
        mov       DWORD PTR [160+esp], 0                        ;232.17
        lea       eax, DWORD PTR [24+esp]                       ;232.17
        mov       DWORD PTR [24+esp], 28                        ;232.17
        mov       DWORD PTR [28+esp], OFFSET FLAT: __STRLITPACK_112 ;232.17
        push      32                                            ;232.17
        push      eax                                           ;232.17
        push      OFFSET FLAT: __STRLITPACK_126.0.2             ;232.17
        push      -2088435968                                   ;232.17
        push      911                                           ;232.17
        push      ebx                                           ;232.17
        call      _for_write_seq_lis                            ;232.17
        jmp       .B2.76        ; Prob 100%                     ;232.17
                                ; LOE esi edi
..1.2_0.TAG.03.0.2::
.B2.48:                         ; Preds .B2.14                  ; Infreq
        mov       DWORD PTR [160+esp], 0                        ;230.17
        lea       eax, DWORD PTR [16+esp]                       ;230.17
        mov       DWORD PTR [16+esp], 31                        ;230.17
        mov       DWORD PTR [20+esp], OFFSET FLAT: __STRLITPACK_114 ;230.17
        push      32                                            ;230.17
        push      eax                                           ;230.17
        push      OFFSET FLAT: __STRLITPACK_125.0.2             ;230.17
        push      -2088435968                                   ;230.17
        push      911                                           ;230.17
        push      ebx                                           ;230.17
        call      _for_write_seq_lis                            ;230.17
        jmp       .B2.76        ; Prob 100%                     ;230.17
                                ; LOE esi edi
..1.2_0.TAG.02.0.2::
.B2.50:                         ; Preds .B2.14                  ; Infreq
        mov       DWORD PTR [160+esp], 0                        ;228.17
        lea       eax, DWORD PTR [8+esp]                        ;228.17
        mov       DWORD PTR [8+esp], 37                         ;228.17
        mov       DWORD PTR [12+esp], OFFSET FLAT: __STRLITPACK_116 ;228.17
        push      32                                            ;228.17
        push      eax                                           ;228.17
        push      OFFSET FLAT: __STRLITPACK_124.0.2             ;228.17
        push      -2088435968                                   ;228.17
        push      911                                           ;228.17
        push      ebx                                           ;228.17
        call      _for_write_seq_lis                            ;228.17
        jmp       .B2.76        ; Prob 100%                     ;228.17
                                ; LOE esi edi
..1.2_0.TAG.01.0.2::
.B2.52:                         ; Preds .B2.14                  ; Infreq
        mov       DWORD PTR [160+esp], 0                        ;226.17
        lea       eax, DWORD PTR [esp]                          ;226.17
        mov       DWORD PTR [esp], 59                           ;226.17
        mov       DWORD PTR [4+esp], OFFSET FLAT: __STRLITPACK_118 ;226.17
        push      32                                            ;226.17
        push      eax                                           ;226.17
        push      OFFSET FLAT: __STRLITPACK_123.0.2             ;226.17
        push      -2088435968                                   ;226.17
        push      911                                           ;226.17
        push      ebx                                           ;226.17
        call      _for_write_seq_lis                            ;226.17
                                ; LOE esi edi
.B2.76:                         ; Preds .B2.50 .B2.48 .B2.46 .B2.44 .B2.42
                                ;       .B2.40 .B2.38 .B2.36 .B2.34 .B2.32
                                ;       .B2.30 .B2.28 .B2.26 .B2.24 .B2.22
                                ;       .B2.20 .B2.18 .B2.16 .B2.52 ; Infreq
        add       esp, 24                                       ;226.17
                                ; LOE esi edi
.B2.53:                         ; Preds .B2.76 .B2.13           ; Infreq
        push      32                                            ;265.8
        xor       eax, eax                                      ;265.8
        push      eax                                           ;265.8
        push      eax                                           ;265.8
        push      -2088435968                                   ;265.8
        push      eax                                           ;265.8
        push      OFFSET FLAT: __STRLITPACK_142                 ;265.8
        call      _for_stop_core                                ;265.8
                                ; LOE esi edi
.B2.77:                         ; Preds .B2.53                  ; Infreq
        add       esp, 24                                       ;265.8
        jmp       .B2.3         ; Prob 100%                     ;265.8
        ALIGN     16
                                ; LOE esi edi
; mark_end;
_DYNUST_NETWORK_MODULE_mp_GETFLINKFROMNODE ENDP
_TEXT	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
..1..TPKT.2_0.0.2	DD	OFFSET FLAT: ..1.2_0.TAG.01.0.2
	DD	OFFSET FLAT: ..1.2_0.TAG.02.0.2
	DD	OFFSET FLAT: ..1.2_0.TAG.03.0.2
	DD	OFFSET FLAT: ..1.2_0.TAG.04.0.2
	DD	OFFSET FLAT: ..1.2_0.TAG.05.0.2
	DD	OFFSET FLAT: ..1.2_0.TAG.06.0.2
	DD	OFFSET FLAT: ..1.2_0.TAG.07.0.2
	DD	OFFSET FLAT: ..1.2_0.TAG.08.0.2
	DD	OFFSET FLAT: ..1.2_0.TAG.09.0.2
	DD	OFFSET FLAT: ..1.2_0.TAG.0a.0.2
	DD	OFFSET FLAT: ..1.2_0.TAG.0b.0.2
	DD	OFFSET FLAT: ..1.2_0.TAG.0c.0.2
	DD	OFFSET FLAT: ..1.2_0.TAG.0d.0.2
	DD	OFFSET FLAT: ..1.2_0.TAG.0e.0.2
	DD	OFFSET FLAT: ..1.2_0.TAG.0f.0.2
	DD	OFFSET FLAT: ..1.2_0.TAG.010.0.2
	DD	OFFSET FLAT: ..1.2_0.TAG.011.0.2
	DD	OFFSET FLAT: ..1.2_0.TAG.012.0.2
	DD	OFFSET FLAT: ..1.2_0.TAG.013.0.2
__STRLITPACK_122.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_141.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_140.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_139.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_138.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_137.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_136.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_135.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_134.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_133.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_132.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_131.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_130.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_129.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_128.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_127.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_126.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_125.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_124.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_123.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_143.0.2	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_NETWORK_MODULE_mp_GETFLINKFROMNODE
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_NETWORK_MODULE_mp_GETBLINKFROMNODE
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_NETWORK_MODULE_mp_GETBLINKFROMNODE
_DYNUST_NETWORK_MODULE_mp_GETBLINKFROMNODE	PROC NEAR 
; parameter 1: 12 + esp
; parameter 2: 16 + esp
.B3.1:                          ; Preds .B3.0
        push      esi                                           ;279.24
        push      edi                                           ;279.24
        mov       eax, DWORD PTR [16+esp]                       ;279.24
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_BACKPOINTR] ;282.7
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_BACKPOINTR+32] ;282.7
        mov       eax, DWORD PTR [eax]                          ;282.14
        shl       edx, 2                                        ;282.7
        lea       ecx, DWORD PTR [ecx+eax*4]                    ;282.7
        mov       esi, ecx                                      ;282.7
        sub       esi, edx                                      ;282.7
        neg       edx                                           ;282.32
        mov       ecx, DWORD PTR [4+edx+ecx]                    ;282.32
        dec       ecx                                           ;282.7
        mov       eax, DWORD PTR [esi]                          ;282.7
        imul      edx, eax, 152                                 ;
        cmp       ecx, eax                                      ;282.7
        jl        .B3.5         ; Prob 10%                      ;282.7
                                ; LOE eax edx ecx ebx ebp
.B3.2:                          ; Preds .B3.1
        mov       esi, DWORD PTR [12+esp]                       ;279.24
        mov       edi, DWORD PTR [esi]                          ;283.12
        imul      esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32], -152 ;
        add       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;
                                ; LOE eax edx ecx ebx ebp esi edi
.B3.3:                          ; Preds .B3.4 .B3.2
        cmp       edi, DWORD PTR [4+edx+esi]                    ;283.56
        je        .B3.6         ; Prob 20%                      ;283.56
                                ; LOE eax edx ecx ebx ebp esi edi
.B3.4:                          ; Preds .B3.3
        inc       eax                                           ;287.7
        add       edx, 152                                      ;287.7
        cmp       eax, ecx                                      ;287.7
        jle       .B3.3         ; Prob 82%                      ;287.7
                                ; LOE eax edx ecx ebx ebp esi edi
.B3.5:                          ; Preds .B3.1 .B3.4
        xor       eax, eax                                      ;
                                ; LOE eax ebx ebp
.B3.6:                          ; Preds .B3.3 .B3.5
        pop       edi                                           ;288.7
        pop       esi                                           ;288.7
        ret                                                     ;288.7
        ALIGN     16
                                ; LOE
; mark_end;
_DYNUST_NETWORK_MODULE_mp_GETBLINKFROMNODE ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_NETWORK_MODULE_mp_GETBLINKFROMNODE
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_NETWORK_MODULE_mp_MOVENOBACKLINK
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_NETWORK_MODULE_mp_MOVENOBACKLINK
_DYNUST_NETWORK_MODULE_mp_MOVENOBACKLINK	PROC NEAR 
; parameter 1: 16 + esp
; parameter 2: 20 + esp
.B4.1:                          ; Preds .B4.0
        push      esi                                           ;290.24
        push      edi                                           ;290.24
        push      ebp                                           ;290.24
        mov       esi, DWORD PTR [20+esp]                       ;290.24
        imul      ebp, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32], 152 ;294.2
        imul      esi, DWORD PTR [esi], 152                     ;294.2
        mov       edi, ebp                                      ;294.2
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;294.2
        add       esi, ecx                                      ;294.2
        neg       edi                                           ;294.2
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_BACKPOINTR] ;294.2
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_BACKPOINTR+32] ;294.2
        shl       eax, 2                                        ;294.2
        mov       edi, DWORD PTR [28+edi+esi]                   ;294.10
        lea       edi, DWORD PTR [edx+edi*4]                    ;294.2
        mov       edx, edi                                      ;294.2
        sub       edx, eax                                      ;294.2
        neg       eax                                           ;294.60
        mov       eax, DWORD PTR [4+eax+edi]                    ;294.60
        dec       eax                                           ;294.2
        mov       esi, DWORD PTR [edx]                          ;294.2
        mov       edx, eax                                      ;294.2
        sub       edx, esi                                      ;294.2
        inc       edx                                           ;294.2
        cmp       eax, esi                                      ;294.2
        jl        .B4.5         ; Prob 10%                      ;294.2
                                ; LOE edx ecx ebx ebp esi
.B4.2:                          ; Preds .B4.1
        mov       eax, DWORD PTR [16+esp]                       ;290.24
        sub       ecx, ebp                                      ;295.15
        imul      ebp, DWORD PTR [eax], 152                     ;295.15
        xor       eax, eax                                      ;
        mov       ecx, DWORD PTR [12+ebp+ecx]                   ;295.18
                                ; LOE eax edx ecx ebx esi
.B4.3:                          ; Preds .B4.4 .B4.2
        cmp       ecx, esi                                      ;295.15
        je        .B4.7         ; Prob 20%                      ;295.15
                                ; LOE eax edx ecx ebx esi
.B4.4:                          ; Preds .B4.3
        inc       eax                                           ;294.2
        inc       esi                                           ;294.2
        cmp       eax, edx                                      ;294.2
        jb        .B4.3         ; Prob 82%                      ;294.2
                                ; LOE eax edx ecx ebx esi
.B4.5:                          ; Preds .B4.1 .B4.4
        xor       eax, eax                                      ;
                                ; LOE eax ebx
.B4.6:                          ; Preds .B4.5
        pop       ebp                                           ;300.7
        pop       edi                                           ;300.7
        pop       esi                                           ;300.7
        ret                                                     ;300.7
                                ; LOE
.B4.7:                          ; Preds .B4.3                   ; Infreq
        inc       eax                                           ;296.3
        pop       ebp                                           ;296.3
        pop       edi                                           ;296.3
        pop       esi                                           ;296.3
        ret                                                     ;296.3
        ALIGN     16
                                ; LOE eax ebx
; mark_end;
_DYNUST_NETWORK_MODULE_mp_MOVENOBACKLINK ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_NETWORK_MODULE_mp_MOVENOBACKLINK
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_NETWORK_MODULE_mp_MOVENOFORLINK
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_NETWORK_MODULE_mp_MOVENOFORLINK
_DYNUST_NETWORK_MODULE_mp_MOVENOFORLINK	PROC NEAR 
; parameter 1: 20 + esp
; parameter 2: 24 + esp
.B5.1:                          ; Preds .B5.0
        push      esi                                           ;302.24
        push      edi                                           ;302.24
        push      ebx                                           ;302.24
        push      ebp                                           ;302.24
        mov       edx, DWORD PTR [24+esp]                       ;302.24
        imul      ecx, DWORD PTR [edx], 152                     ;307.2
        imul      edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32], 152 ;307.2
        mov       eax, edx                                      ;307.2
        neg       eax                                           ;307.2
        add       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;307.2
        movsx     esi, BYTE PTR [108+eax+ecx]                   ;307.13
        test      esi, esi                                      ;307.2
        jle       .B5.5         ; Prob 2%                       ;307.2
                                ; LOE edx ecx esi
.B5.2:                          ; Preds .B5.1
        neg       edx                                           ;308.21
        mov       eax, DWORD PTR [20+esp]                       ;302.24
        mov       ebx, DWORD PTR [112+edx+ecx]                  ;308.21
        mov       edi, DWORD PTR [144+edx+ecx]                  ;308.21
        mov       ecx, DWORD PTR [140+edx+ecx]                  ;308.21
        mov       edx, ecx                                      ;
        imul      edi, ecx                                      ;
        mov       ebp, DWORD PTR [eax]                          ;308.9
        mov       eax, 1                                        ;
        sub       ebx, edi                                      ;
                                ; LOE eax edx ecx ebx ebp esi
.B5.3:                          ; Preds .B5.4 .B5.2
        cmp       ebp, DWORD PTR [edx+ebx]                      ;308.18
        je        .B5.6         ; Prob 20%                      ;308.18
                                ; LOE eax edx ecx ebx ebp esi
.B5.4:                          ; Preds .B5.3
        inc       eax                                           ;312.2
        add       edx, ecx                                      ;312.2
        cmp       eax, esi                                      ;312.2
        jle       .B5.3         ; Prob 82%                      ;312.2
                                ; LOE eax edx ecx ebx ebp esi
.B5.5:                          ; Preds .B5.1 .B5.4
        xor       eax, eax                                      ;
                                ; LOE eax
.B5.6:                          ; Preds .B5.3 .B5.5
        pop       ebp                                           ;313.7
        pop       ebx                                           ;313.7
        pop       edi                                           ;313.7
        pop       esi                                           ;313.7
        ret                                                     ;313.7
        ALIGN     16
                                ; LOE
; mark_end;
_DYNUST_NETWORK_MODULE_mp_MOVENOFORLINK ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_NETWORK_MODULE_mp_MOVENOFORLINK
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_NETWORK_MODULE_mp_MOVENOFORLINKLL
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_NETWORK_MODULE_mp_MOVENOFORLINKLL
_DYNUST_NETWORK_MODULE_mp_MOVENOFORLINKLL	PROC NEAR 
; parameter 1: 20 + esp
; parameter 2: 24 + esp
.B6.1:                          ; Preds .B6.0
        push      esi                                           ;315.18
        push      edi                                           ;315.18
        push      ebx                                           ;315.18
        push      ebp                                           ;315.18
        mov       edx, DWORD PTR [20+esp]                       ;315.18
        imul      ecx, DWORD PTR [edx], 152                     ;320.2
        imul      edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32], 152 ;320.2
        mov       eax, edx                                      ;320.2
        neg       eax                                           ;320.2
        add       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;320.2
        movsx     esi, BYTE PTR [32+eax+ecx]                    ;320.13
        test      esi, esi                                      ;320.2
        jle       .B6.5         ; Prob 2%                       ;320.2
                                ; LOE edx ecx esi
.B6.2:                          ; Preds .B6.1
        neg       edx                                           ;321.21
        mov       eax, DWORD PTR [24+esp]                       ;315.18
        mov       ebx, DWORD PTR [36+edx+ecx]                   ;321.21
        mov       edi, DWORD PTR [68+edx+ecx]                   ;321.21
        mov       ecx, DWORD PTR [64+edx+ecx]                   ;321.21
        mov       edx, ecx                                      ;
        imul      edi, ecx                                      ;
        mov       ebp, DWORD PTR [eax]                          ;321.9
        mov       eax, 1                                        ;
        sub       ebx, edi                                      ;
                                ; LOE eax edx ecx ebx ebp esi
.B6.3:                          ; Preds .B6.4 .B6.2
        cmp       ebp, DWORD PTR [edx+ebx]                      ;321.18
        je        .B6.6         ; Prob 20%                      ;321.18
                                ; LOE eax edx ecx ebx ebp esi
.B6.4:                          ; Preds .B6.3
        inc       eax                                           ;325.2
        add       edx, ecx                                      ;325.2
        cmp       eax, esi                                      ;325.2
        jle       .B6.3         ; Prob 82%                      ;325.2
                                ; LOE eax edx ecx ebx ebp esi
.B6.5:                          ; Preds .B6.1 .B6.4
        xor       eax, eax                                      ;
                                ; LOE eax
.B6.6:                          ; Preds .B6.3 .B6.5
        pop       ebp                                           ;326.7
        pop       ebx                                           ;326.7
        pop       edi                                           ;326.7
        pop       esi                                           ;326.7
        ret                                                     ;326.7
        ALIGN     16
                                ; LOE
; mark_end;
_DYNUST_NETWORK_MODULE_mp_MOVENOFORLINKLL ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_NETWORK_MODULE_mp_MOVENOFORLINKLL
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_NETWORK_MODULE_mp_GETMOVETURN_LL
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_NETWORK_MODULE_mp_GETMOVETURN_LL
_DYNUST_NETWORK_MODULE_mp_GETMOVETURN_LL	PROC NEAR 
; parameter 1: 36 + esp
; parameter 2: 40 + esp
.B7.1:                          ; Preds .B7.0
        push      esi                                           ;328.24
        push      edi                                           ;328.24
        push      ebx                                           ;328.24
        push      ebp                                           ;328.24
        sub       esp, 16                                       ;328.24
        mov       edx, DWORD PTR [36+esp]                       ;328.24
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;334.15
        mov       eax, DWORD PTR [40+esp]                       ;328.24
        mov       ecx, DWORD PTR [edx]                          ;334.22
        imul      ebp, ecx, 152                                 ;334.15
        imul      edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32], 152 ;334.15
        mov       DWORD PTR [12+esp], edx                       ;334.15
        add       ebp, edi                                      ;334.15
        neg       edx                                           ;334.15
        mov       DWORD PTR [8+esp], ecx                        ;334.22
        mov       esi, DWORD PTR [eax]                          ;341.57
        movsx     ecx, BYTE PTR [32+edx+ebp]                    ;334.22
        test      ecx, ecx                                      ;334.15
        jle       .B7.6         ; Prob 2%                       ;334.15
                                ; LOE ecx ebp esi edi
.B7.2:                          ; Preds .B7.1
        mov       edx, DWORD PTR [12+esp]                       ;335.20
        neg       edx                                           ;335.20
        mov       DWORD PTR [4+esp], 1                          ;
        mov       DWORD PTR [esp], edi                          ;
        mov       eax, DWORD PTR [36+edx+ebp]                   ;335.20
        mov       ebx, DWORD PTR [68+edx+ebp]                   ;335.20
        mov       edx, DWORD PTR [64+edx+ebp]                   ;335.20
        imul      ebx, edx                                      ;
        sub       eax, ebx                                      ;
        mov       ebx, DWORD PTR [4+esp]                        ;
        mov       edi, edx                                      ;
                                ; LOE eax edx ecx ebx ebp esi edi
.B7.3:                          ; Preds .B7.4 .B7.2
        cmp       esi, DWORD PTR [edi+eax]                      ;335.60
        je        .B7.10        ; Prob 20%                      ;335.60
                                ; LOE eax edx ecx ebx ebp esi edi
.B7.4:                          ; Preds .B7.3
        inc       ebx                                           ;339.15
        add       edi, edx                                      ;339.15
        cmp       ebx, ecx                                      ;339.15
        jle       .B7.3         ; Prob 82%                      ;339.15
                                ; LOE eax edx ecx ebx ebp esi edi
.B7.5:                          ; Preds .B7.4
        mov       edi, DWORD PTR [esp]                          ;
                                ; LOE ebp esi edi
.B7.6:                          ; Preds .B7.1 .B7.5 .B7.10
        imul      esi, esi, 152                                 ;341.54
        mov       eax, DWORD PTR [12+esp]                       ;341.54
        sub       edi, eax                                      ;341.54
        neg       eax                                           ;341.54
        mov       edx, DWORD PTR [24+esi+edi]                   ;341.57
        cmp       edx, DWORD PTR [28+eax+ebp]                   ;341.54
        jne       .B7.8         ; Prob 50%                      ;341.54
                                ; LOE edx
.B7.7:                          ; Preds .B7.6
        mov       eax, 6                                        ;342.18
        add       esp, 16                                       ;342.18
        pop       ebp                                           ;342.18
        pop       ebx                                           ;342.18
        pop       edi                                           ;342.18
        pop       esi                                           ;342.18
        ret                                                     ;342.18
                                ; LOE eax
.B7.8:                          ; Preds .B7.6
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE+32] ;344.18
        mov       ebx, 2                                        ;347.20
        neg       eax                                           ;347.20
        add       eax, edx                                      ;347.20
        imul      ecx, eax, 44                                  ;347.20
        mov       eax, 5                                        ;347.20
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE] ;344.18
        cmp       DWORD PTR [36+edx+ecx], 900000                ;347.20
        cmovg     eax, ebx                                      ;347.20
                                ; LOE eax
.B7.9:                          ; Preds .B7.8 .B7.10
        add       esp, 16                                       ;351.1
        pop       ebp                                           ;351.1
        pop       ebx                                           ;351.1
        pop       edi                                           ;351.1
        pop       esi                                           ;351.1
        ret                                                     ;351.1
                                ; LOE
.B7.10:                         ; Preds .B7.3                   ; Infreq
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+44] ;336.35
        neg       eax                                           ;336.20
        add       eax, ebx                                      ;336.20
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+32] ;336.20
        neg       ecx                                           ;336.20
        imul      eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+40] ;336.20
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE] ;336.20
        add       ecx, DWORD PTR [8+esp]                        ;336.20
        mov       edi, DWORD PTR [esp]                          ;
        lea       ebx, DWORD PTR [5+edx+ecx*8]                  ;336.20
        movsx     eax, BYTE PTR [ebx+eax]                       ;336.35
        test      eax, eax                                      ;340.30
        jle       .B7.6         ; Prob 16%                      ;340.30
        jmp       .B7.9         ; Prob 100%                     ;340.30
        ALIGN     16
                                ; LOE eax ebp esi edi
; mark_end;
_DYNUST_NETWORK_MODULE_mp_GETMOVETURN_LL ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_NETWORK_MODULE_mp_GETMOVETURN_LL
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_NETWORK_MODULE_mp_GETMOVETURN_IN
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_NETWORK_MODULE_mp_GETMOVETURN_IN
_DYNUST_NETWORK_MODULE_mp_GETMOVETURN_IN	PROC NEAR 
; parameter 1: 24 + esp
; parameter 2: 28 + esp
.B8.1:                          ; Preds .B8.0
        push      esi                                           ;353.18
        push      edi                                           ;353.18
        push      ebx                                           ;353.18
        push      ebp                                           ;353.18
        push      esi                                           ;353.18
        mov       ecx, DWORD PTR [28+esp]                       ;353.18
        mov       ebp, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;360.20
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_BACKPOINTR] ;360.20
        imul      ebx, DWORD PTR [ecx], 152                     ;360.20
        imul      ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32], 152 ;360.20
        add       ebx, ebp                                      ;360.20
        lea       esi, DWORD PTR [-28+ecx]                      ;360.20
        mov       edi, ebx                                      ;360.20
        sub       edi, esi                                      ;360.20
        mov       DWORD PTR [esp], esi                          ;360.20
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_BACKPOINTR+32] ;360.20
        shl       eax, 2                                        ;360.20
        mov       esi, DWORD PTR [edi]                          ;360.20
        lea       edi, DWORD PTR [edx+esi*4]                    ;360.20
        mov       edx, edi                                      ;360.20
        sub       edx, eax                                      ;360.20
        neg       eax                                           ;360.20
        mov       eax, DWORD PTR [4+eax+edi]                    ;360.20
        dec       eax                                           ;360.20
        mov       esi, eax                                      ;360.20
        mov       edx, DWORD PTR [edx]                          ;360.20
        sub       esi, edx                                      ;360.20
        inc       esi                                           ;360.20
        cmp       eax, edx                                      ;360.20
        jl        .B8.5         ; Prob 10%                      ;360.20
                                ; LOE edx ecx ebx ebp esi
.B8.2:                          ; Preds .B8.1
        mov       edi, DWORD PTR [24+esp]                       ;360.20
        mov       eax, ebp                                      ;360.20
        sub       eax, ecx                                      ;360.20
        imul      edi, DWORD PTR [edi], 152                     ;360.20
        mov       edi, DWORD PTR [12+edi+eax]                   ;360.20
        xor       eax, eax                                      ;
                                ; LOE eax edx ecx ebx ebp esi edi
.B8.3:                          ; Preds .B8.4 .B8.2
        cmp       edi, edx                                      ;360.20
        je        .B8.9         ; Prob 20%                      ;360.20
                                ; LOE eax edx ecx ebx ebp esi edi
.B8.4:                          ; Preds .B8.3
        inc       eax                                           ;360.20
        inc       edx                                           ;360.20
        cmp       eax, esi                                      ;360.20
        jb        .B8.3         ; Prob 82%                      ;360.20
                                ; LOE eax edx ecx ebx ebp esi edi
.B8.5:                          ; Preds .B8.1 .B8.4
        xor       eax, eax                                      ;
                                ; LOE eax ecx ebx ebp
.B8.6:                          ; Preds .B8.9 .B8.5
        lea       edx, DWORD PTR [-1+eax]                       ;363.36
        cmp       edx, 9                                        ;363.36
        jbe       .B8.8         ; Prob 50%                      ;363.36
                                ; LOE eax ecx ebx ebp
.B8.7:                          ; Preds .B8.6
        mov       eax, DWORD PTR [24+esp]                       ;364.20
        mov       edx, 6                                        ;367.19
        neg       ecx                                           ;367.19
        imul      esi, DWORD PTR [eax], 152                     ;364.20
        mov       eax, 1                                        ;367.19
        add       esi, ebp                                      ;367.19
        sub       esi, DWORD PTR [esp]                          ;367.19
        mov       ebp, DWORD PTR [esi]                          ;364.20
        cmp       ebp, DWORD PTR [24+ecx+ebx]                   ;367.19
        cmove     eax, edx                                      ;367.19
                                ; LOE eax
.B8.8:                          ; Preds .B8.7 .B8.6
        pop       ecx                                           ;370.1
        pop       ebp                                           ;370.1
        pop       ebx                                           ;370.1
        pop       edi                                           ;370.1
        pop       esi                                           ;370.1
        ret                                                     ;370.1
                                ; LOE
.B8.9:                          ; Preds .B8.3                   ; Infreq
        inc       eax                                           ;360.20
        jmp       .B8.6         ; Prob 100%                     ;360.20
        ALIGN     16
                                ; LOE eax ecx ebx ebp
; mark_end;
_DYNUST_NETWORK_MODULE_mp_GETMOVETURN_IN ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_NETWORK_MODULE_mp_GETMOVETURN_IN
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_NETWORK_MODULE_mp_GETBSTMOVE
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_NETWORK_MODULE_mp_GETBSTMOVE
_DYNUST_NETWORK_MODULE_mp_GETBSTMOVE	PROC NEAR 
; parameter 1: 8 + ebp
; parameter 2: 12 + ebp
.B9.1:                          ; Preds .B9.0
        push      ebp                                           ;372.18
        mov       ebp, esp                                      ;372.18
        and       esp, -16                                      ;372.18
        push      esi                                           ;372.18
        push      edi                                           ;372.18
        push      ebx                                           ;372.18
        sub       esp, 52                                       ;372.18
        mov       esi, DWORD PTR [12+ebp]                       ;372.18
        imul      ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32], 152 ;377.2
        imul      esi, DWORD PTR [esi], 152                     ;377.2
        mov       edi, ecx                                      ;377.2
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;377.2
        add       esi, edx                                      ;377.2
        neg       edi                                           ;377.2
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_BACKPOINTR] ;377.2
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_BACKPOINTR+32] ;377.2
        shl       ebx, 2                                        ;377.2
        mov       edi, DWORD PTR [28+edi+esi]                   ;377.12
        lea       esi, DWORD PTR [eax+edi*4]                    ;377.2
        mov       eax, esi                                      ;377.2
        sub       eax, ebx                                      ;377.2
        neg       ebx                                           ;377.58
        mov       edi, DWORD PTR [4+ebx+esi]                    ;377.58
        dec       edi                                           ;377.2
        mov       eax, DWORD PTR [eax]                          ;377.2
        imul      esi, eax, 152                                 ;
        cmp       edi, eax                                      ;377.2
        jl        .B9.5         ; Prob 10%                      ;377.2
                                ; LOE eax edx ecx esi edi
.B9.2:                          ; Preds .B9.1
        mov       ebx, DWORD PTR [8+ebp]                        ;372.18
        sub       edx, ecx                                      ;
        mov       ebx, DWORD PTR [ebx]                          ;379.7
        mov       DWORD PTR [esp], ebx                          ;379.7
        xor       ebx, ebx                                      ;
        mov       ecx, DWORD PTR [esp]                          ;
                                ; LOE eax edx ecx ebx esi edi
.B9.3:                          ; Preds .B9.4 .B9.2
        inc       ebx                                           ;378.2
        cmp       ecx, DWORD PTR [16+esi+edx]                   ;379.52
        je        .B9.9         ; Prob 20%                      ;379.52
                                ; LOE eax edx ecx ebx esi edi
.B9.4:                          ; Preds .B9.3
        inc       eax                                           ;384.2
        add       esi, 152                                      ;384.2
        cmp       eax, edi                                      ;384.2
        jle       .B9.3         ; Prob 82%                      ;384.2
                                ; LOE eax edx ecx ebx esi edi
.B9.5:                          ; Preds .B9.1 .B9.4
        xor       eax, eax                                      ;
        xor       ebx, ebx                                      ;
                                ; LOE eax ebx
.B9.6:                          ; Preds .B9.9 .B9.5
        test      al, 1                                         ;385.10
        jne       .B9.8         ; Prob 40%                      ;385.10
                                ; LOE ebx
.B9.7:                          ; Preds .B9.6
        mov       DWORD PTR [esp], 0                            ;386.4
        lea       edx, DWORD PTR [esp]                          ;386.4
        mov       DWORD PTR [32+esp], 20                        ;386.4
        lea       eax, DWORD PTR [32+esp]                       ;386.4
        mov       DWORD PTR [36+esp], OFFSET FLAT: __STRLITPACK_80 ;386.4
        push      32                                            ;386.4
        push      eax                                           ;386.4
        push      OFFSET FLAT: __STRLITPACK_144.0.9             ;386.4
        push      -2088435968                                   ;386.4
        push      -1                                            ;386.4
        push      edx                                           ;386.4
        call      _for_write_seq_lis                            ;386.4
                                ; LOE ebx
.B9.13:                         ; Preds .B9.7
        add       esp, 24                                       ;386.4
                                ; LOE ebx
.B9.8:                          ; Preds .B9.6 .B9.13
        mov       eax, ebx                                      ;389.1
        add       esp, 52                                       ;389.1
        pop       ebx                                           ;389.1
        pop       edi                                           ;389.1
        pop       esi                                           ;389.1
        mov       esp, ebp                                      ;389.1
        pop       ebp                                           ;389.1
        ret                                                     ;389.1
                                ; LOE
.B9.9:                          ; Preds .B9.3                   ; Infreq
        mov       eax, -1                                       ;381.7
        jmp       .B9.6         ; Prob 100%                     ;381.7
        ALIGN     16
                                ; LOE eax ebx
; mark_end;
_DYNUST_NETWORK_MODULE_mp_GETBSTMOVE ENDP
_TEXT	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_144.0.9	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_NETWORK_MODULE_mp_GETBSTMOVE
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_NETWORK_MODULE_mp_ALLOCATE_DYNUST_NETWORK_ARC
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_NETWORK_MODULE_mp_ALLOCATE_DYNUST_NETWORK_ARC
_DYNUST_NETWORK_MODULE_mp_ALLOCATE_DYNUST_NETWORK_ARC	PROC NEAR 
.B10.1:                         ; Preds .B10.0
        push      ebp                                           ;392.12
        mov       ebp, esp                                      ;392.12
        and       esp, -16                                      ;392.12
        push      esi                                           ;392.12
        push      edi                                           ;392.12
        push      ebx                                           ;392.12
        sub       esp, 548                                      ;392.12
        xor       eax, eax                                      ;394.13
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFARCS] ;394.13
        test      edx, edx                                      ;394.13
        lea       ecx, DWORD PTR [196+esp]                      ;394.13
        push      900                                           ;394.13
        cmovle    edx, eax                                      ;394.13
        push      edx                                           ;394.13
        push      2                                             ;394.13
        push      ecx                                           ;394.13
        call      _for_check_mult_overflow                      ;394.13
                                ; LOE eax
.B10.2:                         ; Preds .B10.1
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+12] ;394.13
        and       eax, 1                                        ;394.13
        and       edx, 1                                        ;394.13
        add       edx, edx                                      ;394.13
        shl       eax, 4                                        ;394.13
        or        edx, 1                                        ;394.13
        or        edx, eax                                      ;394.13
        or        edx, 262144                                   ;394.13
        push      edx                                           ;394.13
        push      OFFSET FLAT: _DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE ;394.13
        push      DWORD PTR [220+esp]                           ;394.13
        call      _for_alloc_allocatable                        ;394.13
                                ; LOE eax
.B10.1286:                      ; Preds .B10.2
        add       esp, 28                                       ;394.13
        mov       ebx, eax                                      ;394.13
                                ; LOE ebx
.B10.3:                         ; Preds .B10.1286
        test      ebx, ebx                                      ;394.13
        je        .B10.6        ; Prob 50%                      ;394.13
                                ; LOE ebx
.B10.4:                         ; Preds .B10.3
        mov       DWORD PTR [160+esp], 0                        ;396.14
        lea       edx, DWORD PTR [160+esp]                      ;396.14
        mov       DWORD PTR [88+esp], 58                        ;396.14
        lea       eax, DWORD PTR [88+esp]                       ;396.14
        mov       DWORD PTR [92+esp], OFFSET FLAT: __STRLITPACK_78 ;396.14
        push      32                                            ;396.14
        push      eax                                           ;396.14
        push      OFFSET FLAT: __STRLITPACK_145.0.10            ;396.14
        push      -2088435968                                   ;396.14
        push      911                                           ;396.14
        push      edx                                           ;396.14
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], ebx ;394.13
        call      _for_write_seq_lis                            ;396.14
                                ; LOE
.B10.5:                         ; Preds .B10.4
        push      32                                            ;397.14
        xor       eax, eax                                      ;397.14
        push      eax                                           ;397.14
        push      eax                                           ;397.14
        push      -2088435968                                   ;397.14
        push      eax                                           ;397.14
        push      OFFSET FLAT: __STRLITPACK_146                 ;397.14
        call      _for_stop_core                                ;397.14
                                ; LOE
.B10.1287:                      ; Preds .B10.5
        add       esp, 48                                       ;397.14
        jmp       .B10.13       ; Prob 100%                     ;397.14
                                ; LOE
.B10.6:                         ; Preds .B10.3
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFARCS] ;394.13
        xor       eax, eax                                      ;394.13
        test      esi, esi                                      ;394.13
        cmovle    esi, eax                                      ;394.13
        mov       ecx, 900                                      ;394.13
        lea       edi, DWORD PTR [192+esp]                      ;394.13
        push      ecx                                           ;394.13
        push      esi                                           ;394.13
        push      2                                             ;394.13
        push      edi                                           ;394.13
        mov       edx, 1                                        ;394.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+12], 133 ;394.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+4], ecx ;394.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+16], edx ;394.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+8], eax ;394.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32], edx ;394.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+24], esi ;394.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+28], ecx ;394.13
        call      _for_check_mult_overflow                      ;394.13
                                ; LOE ebx
.B10.1288:                      ; Preds .B10.6
        add       esp, 16                                       ;394.13
                                ; LOE ebx
.B10.7:                         ; Preds .B10.1288
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;394.13
        mov       edx, eax                                      ;394.13
        mov       ecx, DWORD PTR [192+esp]                      ;394.13
        add       ecx, eax                                      ;394.13
        cmp       eax, ecx                                      ;394.13
        jae       .B10.12       ; Prob 67%                      ;394.13
                                ; LOE eax edx ebx
.B10.9:                         ; Preds .B10.7
        xor       ecx, ecx                                      ;
                                ; LOE eax edx ecx ebx
.B10.10:                        ; Preds .B10.11 .B10.9
        add       eax, ecx                                      ;394.13
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_ALLOCATE_DYNUST_NETWORK_ARC$BLK..T1042_+896] ;394.13
        mov       DWORD PTR [896+eax], esi                      ;394.13
        mov       esi, 896                                      ;394.13
                                ; LOE eax edx ecx ebx esi
.B10.1284:                      ; Preds .B10.1284 .B10.10
        movsd     xmm0, QWORD PTR [_DYNUST_NETWORK_MODULE_mp_ALLOCATE_DYNUST_NETWORK_ARC$BLK..T1042_-8+esi] ;394.13
        movsd     QWORD PTR [-8+eax+esi], xmm0                  ;394.13
        movsd     xmm1, QWORD PTR [_DYNUST_NETWORK_MODULE_mp_ALLOCATE_DYNUST_NETWORK_ARC$BLK..T1042_-16+esi] ;394.13
        movsd     QWORD PTR [-16+eax+esi], xmm1                 ;394.13
        movsd     xmm2, QWORD PTR [_DYNUST_NETWORK_MODULE_mp_ALLOCATE_DYNUST_NETWORK_ARC$BLK..T1042_-24+esi] ;394.13
        movsd     QWORD PTR [-24+eax+esi], xmm2                 ;394.13
        movsd     xmm3, QWORD PTR [_DYNUST_NETWORK_MODULE_mp_ALLOCATE_DYNUST_NETWORK_ARC$BLK..T1042_-32+esi] ;394.13
        movsd     QWORD PTR [-32+eax+esi], xmm3                 ;394.13
        sub       esi, 32                                       ;394.13
        jne       .B10.1284     ; Prob 96%                      ;394.13
                                ; LOE eax edx ecx ebx esi
.B10.11:                        ; Preds .B10.1284
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;394.13
        add       edx, 900                                      ;394.13
        mov       esi, DWORD PTR [192+esp]                      ;394.13
        add       ecx, 900                                      ;394.13
        add       esi, eax                                      ;394.13
        cmp       edx, esi                                      ;394.13
        jb        .B10.10       ; Prob 82%                      ;394.13
                                ; LOE eax edx ecx ebx
.B10.12:                        ; Preds .B10.7 .B10.11
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], ebx ;394.13
                                ; LOE
.B10.13:                        ; Preds .B10.1287 .B10.12
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+24] ;399.10
        test      ecx, ecx                                      ;399.10
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;399.10
        jle       .B10.92       ; Prob 50%                      ;399.10
                                ; LOE edx ecx
.B10.14:                        ; Preds .B10.13
        mov       esi, ecx                                      ;399.10
        shr       esi, 31                                       ;399.10
        add       esi, ecx                                      ;399.10
        sar       esi, 1                                        ;399.10
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;399.10
        test      esi, esi                                      ;399.10
        jbe       .B10.839      ; Prob 10%                      ;399.10
                                ; LOE edx ecx ebx esi
.B10.15:                        ; Preds .B10.14
        xor       eax, eax                                      ;
        mov       DWORD PTR [64+esp], edx                       ;
        xor       edi, edi                                      ;
        xor       edx, edx                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.16:                        ; Preds .B10.16 .B10.15
        inc       eax                                           ;399.10
        mov       DWORD PTR [edi+ebx], edx                      ;399.10
        mov       DWORD PTR [900+edi+ebx], edx                  ;399.10
        add       edi, 1800                                     ;399.10
        cmp       eax, esi                                      ;399.10
        jb        .B10.16       ; Prob 63%                      ;399.10
                                ; LOE eax edx ecx ebx esi edi
.B10.17:                        ; Preds .B10.16
        mov       edx, DWORD PTR [64+esp]                       ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;399.10
                                ; LOE eax edx ecx ebx esi
.B10.18:                        ; Preds .B10.17 .B10.839
        lea       edi, DWORD PTR [-1+eax]                       ;399.10
        cmp       ecx, edi                                      ;399.10
        jbe       .B10.20       ; Prob 10%                      ;399.10
                                ; LOE eax edx ecx ebx esi
.B10.19:                        ; Preds .B10.18
        mov       edi, edx                                      ;399.10
        add       eax, edx                                      ;399.10
        neg       edi                                           ;399.10
        add       edi, eax                                      ;399.10
        imul      eax, edi, 900                                 ;399.10
        mov       DWORD PTR [-900+ebx+eax], 0                   ;399.10
                                ; LOE edx ecx ebx esi
.B10.20:                        ; Preds .B10.18 .B10.19
        test      esi, esi                                      ;400.10
        jbe       .B10.838      ; Prob 10%                      ;400.10
                                ; LOE edx ecx ebx esi
.B10.21:                        ; Preds .B10.20
        xor       eax, eax                                      ;
        mov       DWORD PTR [64+esp], edx                       ;
        xor       edi, edi                                      ;
        xor       edx, edx                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.22:                        ; Preds .B10.22 .B10.21
        inc       eax                                           ;400.10
        mov       DWORD PTR [4+edi+ebx], edx                    ;400.10
        mov       DWORD PTR [904+edi+ebx], edx                  ;400.10
        add       edi, 1800                                     ;400.10
        cmp       eax, esi                                      ;400.10
        jb        .B10.22       ; Prob 63%                      ;400.10
                                ; LOE eax edx ecx ebx esi edi
.B10.23:                        ; Preds .B10.22
        mov       edx, DWORD PTR [64+esp]                       ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;400.10
                                ; LOE eax edx ecx ebx esi
.B10.24:                        ; Preds .B10.23 .B10.838
        lea       edi, DWORD PTR [-1+eax]                       ;400.10
        cmp       ecx, edi                                      ;400.10
        jbe       .B10.26       ; Prob 10%                      ;400.10
                                ; LOE eax edx ecx ebx esi
.B10.25:                        ; Preds .B10.24
        mov       edi, edx                                      ;400.10
        add       eax, edx                                      ;400.10
        neg       edi                                           ;400.10
        add       edi, eax                                      ;400.10
        imul      eax, edi, 900                                 ;400.10
        mov       DWORD PTR [-896+ebx+eax], 0                   ;400.10
                                ; LOE edx ecx ebx esi
.B10.26:                        ; Preds .B10.24 .B10.25
        test      esi, esi                                      ;401.10
        jbe       .B10.837      ; Prob 10%                      ;401.10
                                ; LOE edx ecx ebx esi
.B10.27:                        ; Preds .B10.26
        xor       eax, eax                                      ;
        mov       DWORD PTR [64+esp], edx                       ;
        xor       edi, edi                                      ;
        xor       edx, edx                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.28:                        ; Preds .B10.28 .B10.27
        inc       eax                                           ;401.10
        mov       DWORD PTR [8+edi+ebx], edx                    ;401.10
        mov       DWORD PTR [908+edi+ebx], edx                  ;401.10
        add       edi, 1800                                     ;401.10
        cmp       eax, esi                                      ;401.10
        jb        .B10.28       ; Prob 63%                      ;401.10
                                ; LOE eax edx ecx ebx esi edi
.B10.29:                        ; Preds .B10.28
        mov       edx, DWORD PTR [64+esp]                       ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;401.10
                                ; LOE eax edx ecx ebx esi
.B10.30:                        ; Preds .B10.29 .B10.837
        lea       edi, DWORD PTR [-1+eax]                       ;401.10
        cmp       ecx, edi                                      ;401.10
        jbe       .B10.32       ; Prob 10%                      ;401.10
                                ; LOE eax edx ecx ebx esi
.B10.31:                        ; Preds .B10.30
        mov       edi, edx                                      ;401.10
        add       eax, edx                                      ;401.10
        neg       edi                                           ;401.10
        add       edi, eax                                      ;401.10
        imul      eax, edi, 900                                 ;401.10
        mov       DWORD PTR [-892+ebx+eax], 0                   ;401.10
                                ; LOE edx ecx ebx esi
.B10.32:                        ; Preds .B10.30 .B10.31
        test      esi, esi                                      ;402.10
        jbe       .B10.836      ; Prob 10%                      ;402.10
                                ; LOE edx ecx ebx esi
.B10.33:                        ; Preds .B10.32
        xor       eax, eax                                      ;
        mov       DWORD PTR [64+esp], edx                       ;
        xor       edi, edi                                      ;
        xor       edx, edx                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.34:                        ; Preds .B10.34 .B10.33
        inc       eax                                           ;402.10
        mov       BYTE PTR [12+edi+ebx], dl                     ;402.10
        mov       BYTE PTR [912+edi+ebx], dl                    ;402.10
        add       edi, 1800                                     ;402.10
        cmp       eax, esi                                      ;402.10
        jb        .B10.34       ; Prob 63%                      ;402.10
                                ; LOE eax edx ecx ebx esi edi
.B10.35:                        ; Preds .B10.34
        mov       edx, DWORD PTR [64+esp]                       ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;402.10
                                ; LOE eax edx ecx ebx esi
.B10.36:                        ; Preds .B10.35 .B10.836
        lea       edi, DWORD PTR [-1+eax]                       ;402.10
        cmp       ecx, edi                                      ;402.10
        jbe       .B10.38       ; Prob 10%                      ;402.10
                                ; LOE eax edx ecx ebx esi
.B10.37:                        ; Preds .B10.36
        mov       edi, edx                                      ;402.10
        add       eax, edx                                      ;402.10
        neg       edi                                           ;402.10
        add       edi, eax                                      ;402.10
        imul      eax, edi, 900                                 ;402.10
        mov       BYTE PTR [-888+ebx+eax], 0                    ;402.10
                                ; LOE edx ecx ebx esi
.B10.38:                        ; Preds .B10.36 .B10.37
        test      esi, esi                                      ;403.10
        jbe       .B10.835      ; Prob 10%                      ;403.10
                                ; LOE edx ecx ebx esi
.B10.39:                        ; Preds .B10.38
        xor       eax, eax                                      ;
        mov       DWORD PTR [64+esp], edx                       ;
        xor       edi, edi                                      ;
        xor       edx, edx                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.40:                        ; Preds .B10.40 .B10.39
        inc       eax                                           ;403.10
        mov       BYTE PTR [28+edi+ebx], dl                     ;403.10
        mov       BYTE PTR [928+edi+ebx], dl                    ;403.10
        add       edi, 1800                                     ;403.10
        cmp       eax, esi                                      ;403.10
        jb        .B10.40       ; Prob 63%                      ;403.10
                                ; LOE eax edx ecx ebx esi edi
.B10.41:                        ; Preds .B10.40
        mov       edx, DWORD PTR [64+esp]                       ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;403.10
                                ; LOE eax edx ecx ebx esi
.B10.42:                        ; Preds .B10.41 .B10.835
        lea       edi, DWORD PTR [-1+eax]                       ;403.10
        cmp       ecx, edi                                      ;403.10
        jbe       .B10.44       ; Prob 10%                      ;403.10
                                ; LOE eax edx ecx ebx esi
.B10.43:                        ; Preds .B10.42
        mov       edi, edx                                      ;403.10
        add       eax, edx                                      ;403.10
        neg       edi                                           ;403.10
        add       edi, eax                                      ;403.10
        imul      eax, edi, 900                                 ;403.10
        mov       BYTE PTR [-872+ebx+eax], 0                    ;403.10
                                ; LOE edx ecx ebx esi
.B10.44:                        ; Preds .B10.42 .B10.43
        test      esi, esi                                      ;404.13
        jbe       .B10.834      ; Prob 10%                      ;404.13
                                ; LOE edx ecx ebx esi
.B10.45:                        ; Preds .B10.44
        xor       eax, eax                                      ;
        mov       DWORD PTR [64+esp], edx                       ;
        xor       edi, edi                                      ;
        xor       edx, edx                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.46:                        ; Preds .B10.46 .B10.45
        inc       eax                                           ;404.13
        mov       DWORD PTR [20+edi+ebx], edx                   ;404.13
        mov       DWORD PTR [920+edi+ebx], edx                  ;404.13
        add       edi, 1800                                     ;404.13
        cmp       eax, esi                                      ;404.13
        jb        .B10.46       ; Prob 63%                      ;404.13
                                ; LOE eax edx ecx ebx esi edi
.B10.47:                        ; Preds .B10.46
        mov       edx, DWORD PTR [64+esp]                       ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;404.13
                                ; LOE eax edx ecx ebx esi
.B10.48:                        ; Preds .B10.47 .B10.834
        lea       edi, DWORD PTR [-1+eax]                       ;404.13
        cmp       ecx, edi                                      ;404.13
        jbe       .B10.50       ; Prob 10%                      ;404.13
                                ; LOE eax edx ecx ebx esi
.B10.49:                        ; Preds .B10.48
        mov       edi, edx                                      ;404.13
        add       eax, edx                                      ;404.13
        neg       edi                                           ;404.13
        add       edi, eax                                      ;404.13
        imul      eax, edi, 900                                 ;404.13
        mov       DWORD PTR [-880+ebx+eax], 0                   ;404.13
                                ; LOE edx ecx ebx esi
.B10.50:                        ; Preds .B10.48 .B10.49
        test      esi, esi                                      ;405.13
        jbe       .B10.833      ; Prob 10%                      ;405.13
                                ; LOE edx ecx ebx esi
.B10.51:                        ; Preds .B10.50
        xor       eax, eax                                      ;
        mov       DWORD PTR [64+esp], edx                       ;
        xor       edi, edi                                      ;
        xor       edx, edx                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.52:                        ; Preds .B10.52 .B10.51
        inc       eax                                           ;405.13
        mov       DWORD PTR [24+edi+ebx], edx                   ;405.13
        mov       DWORD PTR [924+edi+ebx], edx                  ;405.13
        add       edi, 1800                                     ;405.13
        cmp       eax, esi                                      ;405.13
        jb        .B10.52       ; Prob 63%                      ;405.13
                                ; LOE eax edx ecx ebx esi edi
.B10.53:                        ; Preds .B10.52
        mov       edx, DWORD PTR [64+esp]                       ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;405.13
                                ; LOE eax edx ecx ebx esi
.B10.54:                        ; Preds .B10.53 .B10.833
        lea       edi, DWORD PTR [-1+eax]                       ;405.13
        cmp       ecx, edi                                      ;405.13
        jbe       .B10.56       ; Prob 10%                      ;405.13
                                ; LOE eax edx ecx ebx esi
.B10.55:                        ; Preds .B10.54
        mov       edi, edx                                      ;405.13
        add       eax, edx                                      ;405.13
        neg       edi                                           ;405.13
        add       edi, eax                                      ;405.13
        imul      eax, edi, 900                                 ;405.13
        mov       DWORD PTR [-876+ebx+eax], 0                   ;405.13
                                ; LOE edx ecx ebx esi
.B10.56:                        ; Preds .B10.54 .B10.55
        test      esi, esi                                      ;406.10
        jbe       .B10.832      ; Prob 10%                      ;406.10
                                ; LOE edx ecx ebx esi
.B10.57:                        ; Preds .B10.56
        xor       eax, eax                                      ;
        mov       DWORD PTR [64+esp], edx                       ;
        xor       edi, edi                                      ;
        xor       edx, edx                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.58:                        ; Preds .B10.58 .B10.57
        inc       eax                                           ;406.10
        mov       DWORD PTR [16+edi+ebx], edx                   ;406.10
        mov       DWORD PTR [916+edi+ebx], edx                  ;406.10
        add       edi, 1800                                     ;406.10
        cmp       eax, esi                                      ;406.10
        jb        .B10.58       ; Prob 63%                      ;406.10
                                ; LOE eax edx ecx ebx esi edi
.B10.59:                        ; Preds .B10.58
        mov       edx, DWORD PTR [64+esp]                       ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;406.10
                                ; LOE eax edx ecx ebx esi
.B10.60:                        ; Preds .B10.59 .B10.832
        lea       edi, DWORD PTR [-1+eax]                       ;406.10
        cmp       ecx, edi                                      ;406.10
        jbe       .B10.62       ; Prob 10%                      ;406.10
                                ; LOE eax edx ecx ebx esi
.B10.61:                        ; Preds .B10.60
        mov       edi, edx                                      ;406.10
        add       eax, edx                                      ;406.10
        neg       edi                                           ;406.10
        add       edi, eax                                      ;406.10
        imul      eax, edi, 900                                 ;406.10
        mov       DWORD PTR [-884+ebx+eax], 0                   ;406.10
                                ; LOE edx ecx ebx esi
.B10.62:                        ; Preds .B10.60 .B10.61
        test      esi, esi                                      ;407.10
        jbe       .B10.831      ; Prob 10%                      ;407.10
                                ; LOE edx ecx ebx esi
.B10.63:                        ; Preds .B10.62
        xor       eax, eax                                      ;
        mov       DWORD PTR [64+esp], edx                       ;
        xor       edi, edi                                      ;
        xor       edx, edx                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.64:                        ; Preds .B10.64 .B10.63
        inc       eax                                           ;407.10
        mov       DWORD PTR [32+edi+ebx], edx                   ;407.10
        mov       DWORD PTR [932+edi+ebx], edx                  ;407.10
        add       edi, 1800                                     ;407.10
        cmp       eax, esi                                      ;407.10
        jb        .B10.64       ; Prob 63%                      ;407.10
                                ; LOE eax edx ecx ebx esi edi
.B10.65:                        ; Preds .B10.64
        mov       edx, DWORD PTR [64+esp]                       ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;407.10
                                ; LOE eax edx ecx ebx esi
.B10.66:                        ; Preds .B10.65 .B10.831
        lea       edi, DWORD PTR [-1+eax]                       ;407.10
        cmp       ecx, edi                                      ;407.10
        jbe       .B10.68       ; Prob 10%                      ;407.10
                                ; LOE eax edx ecx ebx esi
.B10.67:                        ; Preds .B10.66
        mov       edi, edx                                      ;407.10
        add       eax, edx                                      ;407.10
        neg       edi                                           ;407.10
        add       edi, eax                                      ;407.10
        imul      eax, edi, 900                                 ;407.10
        mov       DWORD PTR [-868+ebx+eax], 0                   ;407.10
                                ; LOE edx ecx ebx esi
.B10.68:                        ; Preds .B10.66 .B10.67
        test      esi, esi                                      ;408.13
        jbe       .B10.830      ; Prob 10%                      ;408.13
                                ; LOE edx ecx ebx esi
.B10.69:                        ; Preds .B10.68
        xor       eax, eax                                      ;
        mov       DWORD PTR [64+esp], edx                       ;
        xor       edi, edi                                      ;
        xor       edx, edx                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.70:                        ; Preds .B10.70 .B10.69
        inc       eax                                           ;408.13
        mov       DWORD PTR [36+edi+ebx], edx                   ;408.13
        mov       DWORD PTR [936+edi+ebx], edx                  ;408.13
        add       edi, 1800                                     ;408.13
        cmp       eax, esi                                      ;408.13
        jb        .B10.70       ; Prob 63%                      ;408.13
                                ; LOE eax edx ecx ebx esi edi
.B10.71:                        ; Preds .B10.70
        mov       edx, DWORD PTR [64+esp]                       ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;408.13
                                ; LOE eax edx ecx ebx esi
.B10.72:                        ; Preds .B10.71 .B10.830
        lea       edi, DWORD PTR [-1+eax]                       ;408.13
        cmp       ecx, edi                                      ;408.13
        jbe       .B10.74       ; Prob 10%                      ;408.13
                                ; LOE eax edx ecx ebx esi
.B10.73:                        ; Preds .B10.72
        mov       edi, edx                                      ;408.13
        add       eax, edx                                      ;408.13
        neg       edi                                           ;408.13
        add       edi, eax                                      ;408.13
        imul      eax, edi, 900                                 ;408.13
        mov       DWORD PTR [-864+ebx+eax], 0                   ;408.13
                                ; LOE edx ecx ebx esi
.B10.74:                        ; Preds .B10.72 .B10.73
        test      esi, esi                                      ;409.13
        jbe       .B10.829      ; Prob 10%                      ;409.13
                                ; LOE edx ecx ebx esi
.B10.75:                        ; Preds .B10.74
        xor       eax, eax                                      ;
        mov       DWORD PTR [64+esp], edx                       ;
        xor       edi, edi                                      ;
        xor       edx, edx                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.76:                        ; Preds .B10.76 .B10.75
        inc       eax                                           ;409.13
        mov       DWORD PTR [40+edi+ebx], edx                   ;409.13
        mov       DWORD PTR [940+edi+ebx], edx                  ;409.13
        add       edi, 1800                                     ;409.13
        cmp       eax, esi                                      ;409.13
        jb        .B10.76       ; Prob 63%                      ;409.13
                                ; LOE eax edx ecx ebx esi edi
.B10.77:                        ; Preds .B10.76
        mov       edx, DWORD PTR [64+esp]                       ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;409.13
                                ; LOE eax edx ecx ebx esi
.B10.78:                        ; Preds .B10.77 .B10.829
        lea       edi, DWORD PTR [-1+eax]                       ;409.13
        cmp       ecx, edi                                      ;409.13
        jbe       .B10.80       ; Prob 10%                      ;409.13
                                ; LOE eax edx ecx ebx esi
.B10.79:                        ; Preds .B10.78
        mov       edi, edx                                      ;409.13
        add       eax, edx                                      ;409.13
        neg       edi                                           ;409.13
        add       edi, eax                                      ;409.13
        imul      eax, edi, 900                                 ;409.13
        mov       DWORD PTR [-860+ebx+eax], 0                   ;409.13
                                ; LOE edx ecx ebx esi
.B10.80:                        ; Preds .B10.78 .B10.79
        test      esi, esi                                      ;410.13
        jbe       .B10.828      ; Prob 10%                      ;410.13
                                ; LOE edx ecx ebx esi
.B10.81:                        ; Preds .B10.80
        xor       eax, eax                                      ;
        mov       DWORD PTR [64+esp], edx                       ;
        xor       edi, edi                                      ;
        xor       edx, edx                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.82:                        ; Preds .B10.82 .B10.81
        inc       eax                                           ;410.13
        mov       BYTE PTR [44+edi+ebx], dl                     ;410.13
        mov       BYTE PTR [944+edi+ebx], dl                    ;410.13
        add       edi, 1800                                     ;410.13
        cmp       eax, esi                                      ;410.13
        jb        .B10.82       ; Prob 63%                      ;410.13
                                ; LOE eax edx ecx ebx esi edi
.B10.83:                        ; Preds .B10.82
        mov       edx, DWORD PTR [64+esp]                       ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;410.13
                                ; LOE eax edx ecx ebx esi
.B10.84:                        ; Preds .B10.83 .B10.828
        lea       edi, DWORD PTR [-1+eax]                       ;410.13
        cmp       ecx, edi                                      ;410.13
        jbe       .B10.86       ; Prob 10%                      ;410.13
                                ; LOE eax edx ecx ebx esi
.B10.85:                        ; Preds .B10.84
        mov       edi, edx                                      ;410.13
        add       eax, edx                                      ;410.13
        neg       edi                                           ;410.13
        add       edi, eax                                      ;410.13
        imul      eax, edi, 900                                 ;410.13
        mov       BYTE PTR [-856+ebx+eax], 0                    ;410.13
                                ; LOE edx ecx ebx esi
.B10.86:                        ; Preds .B10.84 .B10.85
        test      esi, esi                                      ;411.13
        jbe       .B10.1281     ; Prob 10%                      ;411.13
                                ; LOE edx ecx ebx esi
.B10.87:                        ; Preds .B10.86
        xor       eax, eax                                      ;
        mov       DWORD PTR [64+esp], edx                       ;
        xor       edi, edi                                      ;
        xor       edx, edx                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.88:                        ; Preds .B10.88 .B10.87
        inc       eax                                           ;411.13
        mov       BYTE PTR [45+edi+ebx], dl                     ;411.13
        mov       BYTE PTR [945+edi+ebx], dl                    ;411.13
        add       edi, 1800                                     ;411.13
        cmp       eax, esi                                      ;411.13
        jb        .B10.88       ; Prob 63%                      ;411.13
                                ; LOE eax edx ecx ebx esi edi
.B10.89:                        ; Preds .B10.88
        mov       edx, DWORD PTR [64+esp]                       ;
        lea       esi, DWORD PTR [1+eax+eax]                    ;411.13
                                ; LOE edx ecx ebx esi
.B10.90:                        ; Preds .B10.89 .B10.1281
        lea       eax, DWORD PTR [-1+esi]                       ;411.13
        cmp       ecx, eax                                      ;411.13
        jbe       .B10.92       ; Prob 10%                      ;411.13
                                ; LOE edx ecx ebx esi
.B10.91:                        ; Preds .B10.90
        mov       eax, edx                                      ;411.13
        add       esi, edx                                      ;411.13
        neg       eax                                           ;411.13
        add       eax, esi                                      ;411.13
        imul      esi, eax, 900                                 ;411.13
        mov       BYTE PTR [-855+ebx+esi], 0                    ;411.13
                                ; LOE edx ecx
.B10.92:                        ; Preds .B10.90 .B10.13 .B10.91
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFARCS] ;413.13
        test      eax, eax                                      ;413.13
        mov       DWORD PTR [508+esp], 1                        ;413.13
        jle       .B10.295      ; Prob 28%                      ;413.13
                                ; LOE eax edx ecx
.B10.93:                        ; Preds .B10.92
        movdqa    xmm0, XMMWORD PTR [_2il0floatpacket.22]       ;454.17
        lea       edi, DWORD PTR [324+esp]                      ;454.17
        mov       DWORD PTR [72+esp], eax                       ;454.17
        pxor      xmm2, xmm2                                    ;475.17
        pxor      xmm1, xmm1                                    ;555.13
                                ; LOE
.B10.94:                        ; Preds .B10.293 .B10.93
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_AGGINT] ;414.17
        xor       eax, eax                                      ;414.17
        test      edx, edx                                      ;414.17
        push      40                                            ;414.17
        cmovle    edx, eax                                      ;414.17
        push      edx                                           ;414.17
        push      2                                             ;414.17
        lea       ecx, DWORD PTR [336+esp]                      ;414.17
        push      ecx                                           ;414.17
        call      _for_check_mult_overflow                      ;414.17
                                ; LOE eax
.B10.95:                        ; Preds .B10.94
        imul      edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32], 900 ;414.17
        and       eax, 1                                        ;414.17
        imul      ecx, DWORD PTR [524+esp], 900                 ;414.26
        add       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;414.17
        lea       esi, DWORD PTR [-48+edx]                      ;414.17
        neg       edx                                           ;414.17
        neg       esi                                           ;414.17
        shl       eax, 4                                        ;414.17
        add       esi, ecx                                      ;414.17
        mov       ebx, DWORD PTR [60+edx+ecx]                   ;414.17
        and       ebx, 1                                        ;414.17
        add       ebx, ebx                                      ;414.17
        or        ebx, 1                                        ;414.17
        or        ebx, eax                                      ;414.17
        or        ebx, 262144                                   ;414.17
        push      ebx                                           ;414.17
        push      esi                                           ;414.17
        push      DWORD PTR [348+esp]                           ;414.17
        call      _for_alloc_allocatable                        ;414.17
                                ; LOE eax
.B10.1290:                      ; Preds .B10.95
        add       esp, 28                                       ;414.17
        mov       ebx, eax                                      ;414.17
                                ; LOE ebx
.B10.96:                        ; Preds .B10.1290
        test      ebx, ebx                                      ;414.17
        jne       .B10.99       ; Prob 50%                      ;414.17
                                ; LOE ebx
.B10.97:                        ; Preds .B10.96
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;414.17
        mov       edx, 1                                        ;414.17
        neg       ecx                                           ;414.17
        xor       eax, eax                                      ;414.17
        add       ecx, DWORD PTR [508+esp]                      ;414.17
        imul      edi, ecx, 900                                 ;414.17
        mov       ecx, 40                                       ;414.17
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;414.17
        mov       DWORD PTR [64+esi+edi], edx                   ;414.17
        mov       DWORD PTR [80+esi+edi], edx                   ;414.17
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_AGGINT] ;414.17
        test      edx, edx                                      ;414.17
        mov       DWORD PTR [56+esi+edi], eax                   ;414.17
        cmovle    edx, eax                                      ;414.17
        lea       eax, DWORD PTR [276+esp]                      ;414.17
        push      ecx                                           ;414.17
        push      edx                                           ;414.17
        push      2                                             ;414.17
        push      eax                                           ;414.17
        mov       DWORD PTR [60+esi+edi], 133                   ;414.17
        mov       DWORD PTR [52+esi+edi], ecx                   ;414.17
        mov       DWORD PTR [72+esi+edi], edx                   ;414.17
        mov       DWORD PTR [76+esi+edi], ecx                   ;414.17
        call      _for_check_mult_overflow                      ;414.17
                                ; LOE ebx
.B10.1291:                      ; Preds .B10.97
        add       esp, 16                                       ;414.17
                                ; LOE ebx
.B10.98:                        ; Preds .B10.1291
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], ebx ;414.17
        jmp       .B10.101      ; Prob 100%                     ;414.17
                                ; LOE
.B10.99:                        ; Preds .B10.96
        mov       DWORD PTR [160+esp], 0                        ;416.21
        lea       eax, DWORD PTR [328+esp]                      ;416.21
        mov       DWORD PTR [328+esp], 55                       ;416.21
        mov       DWORD PTR [332+esp], OFFSET FLAT: __STRLITPACK_76 ;416.21
        push      32                                            ;416.21
        push      eax                                           ;416.21
        push      OFFSET FLAT: __STRLITPACK_147.0.10            ;416.21
        push      -2088435968                                   ;416.21
        push      911                                           ;416.21
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], ebx ;414.17
        lea       edx, DWORD PTR [180+esp]                      ;416.21
        push      edx                                           ;416.21
        call      _for_write_seq_lis                            ;416.21
                                ; LOE
.B10.100:                       ; Preds .B10.99
        push      32                                            ;417.18
        xor       eax, eax                                      ;417.18
        push      eax                                           ;417.18
        push      eax                                           ;417.18
        push      -2088435968                                   ;417.18
        push      eax                                           ;417.18
        push      OFFSET FLAT: __STRLITPACK_148                 ;417.18
        call      _for_stop_core                                ;417.18
                                ; LOE
.B10.1292:                      ; Preds .B10.100
        add       esp, 48                                       ;417.18
                                ; LOE
.B10.101:                       ; Preds .B10.1292 .B10.98
        mov       eax, DWORD PTR [508+esp]                      ;419.17
        sub       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;419.17
        imul      ebx, eax, 900                                 ;419.17
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;419.17
        mov       esi, DWORD PTR [72+ecx+ebx]                   ;419.17
        test      esi, esi                                      ;419.17
        mov       edx, DWORD PTR [80+ecx+ebx]                   ;419.17
        jle       .B10.108      ; Prob 50%                      ;419.17
                                ; LOE edx ecx ebx esi
.B10.102:                       ; Preds .B10.101
        mov       eax, esi                                      ;419.17
        shr       eax, 31                                       ;419.17
        add       eax, esi                                      ;419.17
        sar       eax, 1                                        ;419.17
        mov       DWORD PTR [484+esp], eax                      ;419.17
        test      eax, eax                                      ;419.17
        jbe       .B10.840      ; Prob 10%                      ;419.17
                                ; LOE edx ecx ebx esi
.B10.103:                       ; Preds .B10.102
        mov       edi, DWORD PTR [48+ecx+ebx]                   ;419.17
        xor       eax, eax                                      ;
        mov       DWORD PTR [16+esp], ebx                       ;419.17
        mov       DWORD PTR [8+esp], edx                        ;419.17
        xor       edx, edx                                      ;419.17
        mov       DWORD PTR [12+esp], ecx                       ;419.17
        mov       ebx, edi                                      ;419.17
        mov       edi, DWORD PTR [484+esp]                      ;419.17
                                ; LOE eax edx ebx esi edi
.B10.104:                       ; Preds .B10.104 .B10.103
        lea       ecx, DWORD PTR [eax+eax*4]                    ;419.17
        inc       eax                                           ;419.17
        shl       ecx, 4                                        ;419.17
        cmp       eax, edi                                      ;419.17
        mov       WORD PTR [36+ebx+ecx], dx                     ;419.17
        mov       WORD PTR [76+ebx+ecx], dx                     ;419.17
        jb        .B10.104      ; Prob 64%                      ;419.17
                                ; LOE eax edx ebx esi edi
.B10.105:                       ; Preds .B10.104
        mov       edx, DWORD PTR [8+esp]                        ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;419.17
        mov       ecx, DWORD PTR [12+esp]                       ;
        mov       ebx, DWORD PTR [16+esp]                       ;
                                ; LOE eax edx ecx ebx esi
.B10.106:                       ; Preds .B10.105 .B10.840
        lea       edi, DWORD PTR [-1+eax]                       ;419.17
        cmp       esi, edi                                      ;419.17
        jbe       .B10.108      ; Prob 10%                      ;419.17
                                ; LOE eax edx ecx ebx
.B10.107:                       ; Preds .B10.106
        mov       esi, edx                                      ;419.17
        add       eax, edx                                      ;419.17
        neg       esi                                           ;419.17
        add       esi, eax                                      ;419.17
        mov       ebx, DWORD PTR [48+ecx+ebx]                   ;419.17
        xor       ecx, ecx                                      ;419.17
        lea       eax, DWORD PTR [esi+esi*4]                    ;419.17
        mov       WORD PTR [-4+ebx+eax*8], cx                   ;419.17
                                ; LOE
.B10.108:                       ; Preds .B10.101 .B10.106 .B10.107
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_AGGINT] ;421.17
        xor       eax, eax                                      ;421.17
        test      edx, edx                                      ;421.17
        push      40                                            ;421.17
        cmovle    edx, eax                                      ;421.17
        push      edx                                           ;421.17
        push      2                                             ;421.17
        lea       ecx, DWORD PTR [436+esp]                      ;421.17
        push      ecx                                           ;421.17
        call      _for_check_mult_overflow                      ;421.17
                                ; LOE eax
.B10.109:                       ; Preds .B10.108
        imul      edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32], 900 ;421.17
        and       eax, 1                                        ;421.17
        imul      ecx, DWORD PTR [524+esp], 900                 ;421.26
        add       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;421.17
        lea       esi, DWORD PTR [-120+edx]                     ;421.17
        neg       edx                                           ;421.17
        neg       esi                                           ;421.17
        shl       eax, 4                                        ;421.17
        add       esi, ecx                                      ;421.17
        mov       ebx, DWORD PTR [132+edx+ecx]                  ;421.17
        and       ebx, 1                                        ;421.17
        add       ebx, ebx                                      ;421.17
        or        ebx, 1                                        ;421.17
        or        ebx, eax                                      ;421.17
        or        ebx, 262144                                   ;421.17
        push      ebx                                           ;421.17
        push      esi                                           ;421.17
        push      DWORD PTR [448+esp]                           ;421.17
        call      _for_alloc_allocatable                        ;421.17
                                ; LOE eax
.B10.1294:                      ; Preds .B10.109
        add       esp, 28                                       ;421.17
        mov       ebx, eax                                      ;421.17
                                ; LOE ebx
.B10.110:                       ; Preds .B10.1294
        test      ebx, ebx                                      ;421.17
        jne       .B10.113      ; Prob 50%                      ;421.17
                                ; LOE ebx
.B10.111:                       ; Preds .B10.110
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;421.17
        mov       edx, 1                                        ;421.17
        neg       ecx                                           ;421.17
        xor       eax, eax                                      ;421.17
        add       ecx, DWORD PTR [508+esp]                      ;421.17
        imul      edi, ecx, 900                                 ;421.17
        mov       ecx, 40                                       ;421.17
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;421.17
        mov       DWORD PTR [136+esi+edi], edx                  ;421.17
        mov       DWORD PTR [152+esi+edi], edx                  ;421.17
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_AGGINT] ;421.17
        test      edx, edx                                      ;421.17
        mov       DWORD PTR [128+esi+edi], eax                  ;421.17
        cmovle    edx, eax                                      ;421.17
        lea       eax, DWORD PTR [280+esp]                      ;421.17
        push      ecx                                           ;421.17
        push      edx                                           ;421.17
        push      2                                             ;421.17
        push      eax                                           ;421.17
        mov       DWORD PTR [132+esi+edi], 133                  ;421.17
        mov       DWORD PTR [124+esi+edi], ecx                  ;421.17
        mov       DWORD PTR [144+esi+edi], edx                  ;421.17
        mov       DWORD PTR [148+esi+edi], ecx                  ;421.17
        call      _for_check_mult_overflow                      ;421.17
                                ; LOE ebx
.B10.1295:                      ; Preds .B10.111
        add       esp, 16                                       ;421.17
                                ; LOE ebx
.B10.112:                       ; Preds .B10.1295
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], ebx ;421.17
        jmp       .B10.115      ; Prob 100%                     ;421.17
                                ; LOE
.B10.113:                       ; Preds .B10.110
        mov       DWORD PTR [160+esp], 0                        ;423.21
        lea       eax, DWORD PTR [336+esp]                      ;423.21
        mov       DWORD PTR [336+esp], 55                       ;423.21
        mov       DWORD PTR [340+esp], OFFSET FLAT: __STRLITPACK_74 ;423.21
        push      32                                            ;423.21
        push      eax                                           ;423.21
        push      OFFSET FLAT: __STRLITPACK_149.0.10            ;423.21
        push      -2088435968                                   ;423.21
        push      911                                           ;423.21
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], ebx ;421.17
        lea       edx, DWORD PTR [180+esp]                      ;423.21
        push      edx                                           ;423.21
        call      _for_write_seq_lis                            ;423.21
                                ; LOE
.B10.114:                       ; Preds .B10.113
        push      32                                            ;424.18
        xor       eax, eax                                      ;424.18
        push      eax                                           ;424.18
        push      eax                                           ;424.18
        push      -2088435968                                   ;424.18
        push      eax                                           ;424.18
        push      OFFSET FLAT: __STRLITPACK_150                 ;424.18
        call      _for_stop_core                                ;424.18
                                ; LOE
.B10.1296:                      ; Preds .B10.114
        add       esp, 48                                       ;424.18
                                ; LOE
.B10.115:                       ; Preds .B10.1296 .B10.112
        mov       eax, DWORD PTR [508+esp]                      ;426.17
        sub       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;426.17
        imul      ebx, eax, 900                                 ;426.17
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;426.17
        mov       esi, DWORD PTR [144+ecx+ebx]                  ;426.17
        test      esi, esi                                      ;426.17
        mov       edx, DWORD PTR [152+ecx+ebx]                  ;426.17
        jle       .B10.122      ; Prob 50%                      ;426.17
                                ; LOE edx ecx ebx esi
.B10.116:                       ; Preds .B10.115
        mov       eax, esi                                      ;426.17
        shr       eax, 31                                       ;426.17
        add       eax, esi                                      ;426.17
        sar       eax, 1                                        ;426.17
        mov       DWORD PTR [488+esp], eax                      ;426.17
        test      eax, eax                                      ;426.17
        jbe       .B10.841      ; Prob 10%                      ;426.17
                                ; LOE edx ecx ebx esi
.B10.117:                       ; Preds .B10.116
        mov       edi, DWORD PTR [120+ecx+ebx]                  ;426.17
        xor       eax, eax                                      ;
        mov       DWORD PTR [28+esp], ebx                       ;426.17
        mov       DWORD PTR [20+esp], edx                       ;426.17
        xor       edx, edx                                      ;426.17
        mov       DWORD PTR [24+esp], ecx                       ;426.17
        mov       ebx, edi                                      ;426.17
        mov       edi, DWORD PTR [488+esp]                      ;426.17
                                ; LOE eax edx ebx esi edi
.B10.118:                       ; Preds .B10.118 .B10.117
        lea       ecx, DWORD PTR [eax+eax*4]                    ;426.17
        inc       eax                                           ;426.17
        shl       ecx, 4                                        ;426.17
        cmp       eax, edi                                      ;426.17
        mov       WORD PTR [36+ebx+ecx], dx                     ;426.17
        mov       WORD PTR [76+ebx+ecx], dx                     ;426.17
        jb        .B10.118      ; Prob 64%                      ;426.17
                                ; LOE eax edx ebx esi edi
.B10.119:                       ; Preds .B10.118
        mov       edx, DWORD PTR [20+esp]                       ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;426.17
        mov       ecx, DWORD PTR [24+esp]                       ;
        mov       ebx, DWORD PTR [28+esp]                       ;
                                ; LOE eax edx ecx ebx esi
.B10.120:                       ; Preds .B10.119 .B10.841
        lea       edi, DWORD PTR [-1+eax]                       ;426.17
        cmp       esi, edi                                      ;426.17
        jbe       .B10.122      ; Prob 10%                      ;426.17
                                ; LOE eax edx ecx ebx
.B10.121:                       ; Preds .B10.120
        mov       esi, edx                                      ;426.17
        add       eax, edx                                      ;426.17
        neg       esi                                           ;426.17
        add       esi, eax                                      ;426.17
        mov       ebx, DWORD PTR [120+ecx+ebx]                  ;426.17
        xor       ecx, ecx                                      ;426.17
        lea       eax, DWORD PTR [esi+esi*4]                    ;426.17
        mov       WORD PTR [-4+ebx+eax*8], cx                   ;426.17
                                ; LOE
.B10.122:                       ; Preds .B10.115 .B10.120 .B10.121
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_AGGINT] ;428.18
        xor       eax, eax                                      ;428.18
        test      edx, edx                                      ;428.18
        push      4                                             ;428.18
        cmovle    edx, eax                                      ;428.18
        push      edx                                           ;428.18
        push      2                                             ;428.18
        lea       ecx, DWORD PTR [440+esp]                      ;428.18
        push      ecx                                           ;428.18
        call      _for_check_mult_overflow                      ;428.18
                                ; LOE eax
.B10.123:                       ; Preds .B10.122
        imul      edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32], 900 ;428.18
        and       eax, 1                                        ;428.18
        imul      ecx, DWORD PTR [524+esp], 900                 ;428.27
        add       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;428.18
        lea       esi, DWORD PTR [-192+edx]                     ;428.18
        neg       edx                                           ;428.18
        neg       esi                                           ;428.18
        shl       eax, 4                                        ;428.18
        add       esi, ecx                                      ;428.18
        mov       ebx, DWORD PTR [204+edx+ecx]                  ;428.18
        and       ebx, 1                                        ;428.18
        add       ebx, ebx                                      ;428.18
        or        ebx, 1                                        ;428.18
        or        ebx, eax                                      ;428.18
        or        ebx, 262144                                   ;428.18
        push      ebx                                           ;428.18
        push      esi                                           ;428.18
        push      DWORD PTR [452+esp]                           ;428.18
        call      _for_alloc_allocatable                        ;428.18
                                ; LOE eax
.B10.1298:                      ; Preds .B10.123
        add       esp, 28                                       ;428.18
        mov       ebx, eax                                      ;428.18
                                ; LOE ebx
.B10.124:                       ; Preds .B10.1298
        test      ebx, ebx                                      ;428.18
        jne       .B10.127      ; Prob 50%                      ;428.18
                                ; LOE ebx
.B10.125:                       ; Preds .B10.124
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;428.18
        mov       edx, 1                                        ;428.18
        neg       ecx                                           ;428.18
        xor       eax, eax                                      ;428.18
        add       ecx, DWORD PTR [508+esp]                      ;428.18
        imul      edi, ecx, 900                                 ;428.18
        mov       ecx, 4                                        ;428.18
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;428.18
        mov       DWORD PTR [208+esi+edi], edx                  ;428.18
        mov       DWORD PTR [224+esi+edi], edx                  ;428.18
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_AGGINT] ;428.18
        test      edx, edx                                      ;428.18
        mov       DWORD PTR [200+esi+edi], eax                  ;428.18
        cmovle    edx, eax                                      ;428.18
        lea       eax, DWORD PTR [284+esp]                      ;428.18
        push      ecx                                           ;428.18
        push      edx                                           ;428.18
        push      2                                             ;428.18
        push      eax                                           ;428.18
        mov       DWORD PTR [204+esi+edi], 133                  ;428.18
        mov       DWORD PTR [196+esi+edi], ecx                  ;428.18
        mov       DWORD PTR [216+esi+edi], edx                  ;428.18
        mov       DWORD PTR [220+esi+edi], ecx                  ;428.18
        call      _for_check_mult_overflow                      ;428.18
                                ; LOE ebx
.B10.1299:                      ; Preds .B10.125
        add       esp, 16                                       ;428.18
                                ; LOE ebx
.B10.126:                       ; Preds .B10.1299
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], ebx ;428.18
        jmp       .B10.129      ; Prob 100%                     ;428.18
                                ; LOE
.B10.127:                       ; Preds .B10.124
        mov       DWORD PTR [160+esp], 0                        ;430.21
        lea       eax, DWORD PTR [344+esp]                      ;430.21
        mov       DWORD PTR [344+esp], 46                       ;430.21
        mov       DWORD PTR [348+esp], OFFSET FLAT: __STRLITPACK_72 ;430.21
        push      32                                            ;430.21
        push      eax                                           ;430.21
        push      OFFSET FLAT: __STRLITPACK_151.0.10            ;430.21
        push      -2088435968                                   ;430.21
        push      911                                           ;430.21
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], ebx ;428.18
        lea       edx, DWORD PTR [180+esp]                      ;430.21
        push      edx                                           ;430.21
        call      _for_write_seq_lis                            ;430.21
                                ; LOE
.B10.128:                       ; Preds .B10.127
        push      32                                            ;431.18
        xor       eax, eax                                      ;431.18
        push      eax                                           ;431.18
        push      eax                                           ;431.18
        push      -2088435968                                   ;431.18
        push      eax                                           ;431.18
        push      OFFSET FLAT: __STRLITPACK_152                 ;431.18
        call      _for_stop_core                                ;431.18
                                ; LOE
.B10.1300:                      ; Preds .B10.128
        add       esp, 48                                       ;431.18
                                ; LOE
.B10.129:                       ; Preds .B10.1300 .B10.126
        mov       eax, DWORD PTR [508+esp]                      ;433.17
        sub       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;433.17
        imul      ebx, eax, 900                                 ;433.17
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;433.17
        mov       esi, DWORD PTR [216+ecx+ebx]                  ;433.17
        test      esi, esi                                      ;433.17
        jle       .B10.132      ; Prob 50%                      ;433.17
                                ; LOE ecx ebx esi
.B10.130:                       ; Preds .B10.129
        cmp       esi, 24                                       ;433.17
        jle       .B10.842      ; Prob 0%                       ;433.17
                                ; LOE ecx ebx esi
.B10.131:                       ; Preds .B10.130
        shl       esi, 2                                        ;433.17
        push      esi                                           ;433.17
        push      0                                             ;433.17
        push      DWORD PTR [192+ecx+ebx]                       ;433.17
        call      __intel_fast_memset                           ;433.17
                                ; LOE
.B10.1301:                      ; Preds .B10.131
        add       esp, 12                                       ;433.17
                                ; LOE
.B10.132:                       ; Preds .B10.848 .B10.129 .B10.846 .B10.1301
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_AGGINT] ;435.17
        xor       eax, eax                                      ;435.17
        test      edx, edx                                      ;435.17
        push      40                                            ;435.17
        cmovle    edx, eax                                      ;435.17
        push      edx                                           ;435.17
        push      2                                             ;435.17
        lea       ecx, DWORD PTR [444+esp]                      ;435.17
        push      ecx                                           ;435.17
        call      _for_check_mult_overflow                      ;435.17
                                ; LOE eax
.B10.133:                       ; Preds .B10.132
        imul      edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32], 900 ;435.17
        and       eax, 1                                        ;435.17
        imul      ecx, DWORD PTR [524+esp], 900                 ;435.26
        add       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;435.17
        lea       esi, DWORD PTR [-156+edx]                     ;435.17
        neg       edx                                           ;435.17
        neg       esi                                           ;435.17
        shl       eax, 4                                        ;435.17
        add       esi, ecx                                      ;435.17
        mov       ebx, DWORD PTR [168+edx+ecx]                  ;435.17
        and       ebx, 1                                        ;435.17
        add       ebx, ebx                                      ;435.17
        or        ebx, 1                                        ;435.17
        or        ebx, eax                                      ;435.17
        or        ebx, 262144                                   ;435.17
        push      ebx                                           ;435.17
        push      esi                                           ;435.17
        push      DWORD PTR [456+esp]                           ;435.17
        call      _for_alloc_allocatable                        ;435.17
                                ; LOE eax
.B10.1303:                      ; Preds .B10.133
        add       esp, 28                                       ;435.17
        mov       ebx, eax                                      ;435.17
                                ; LOE ebx
.B10.134:                       ; Preds .B10.1303
        test      ebx, ebx                                      ;435.17
        jne       .B10.137      ; Prob 50%                      ;435.17
                                ; LOE ebx
.B10.135:                       ; Preds .B10.134
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;435.17
        mov       edx, 1                                        ;435.17
        neg       ecx                                           ;435.17
        xor       eax, eax                                      ;435.17
        add       ecx, DWORD PTR [508+esp]                      ;435.17
        imul      edi, ecx, 900                                 ;435.17
        mov       ecx, 40                                       ;435.17
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;435.17
        mov       DWORD PTR [172+esi+edi], edx                  ;435.17
        mov       DWORD PTR [188+esi+edi], edx                  ;435.17
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_AGGINT] ;435.17
        test      edx, edx                                      ;435.17
        mov       DWORD PTR [164+esi+edi], eax                  ;435.17
        cmovle    edx, eax                                      ;435.17
        lea       eax, DWORD PTR [288+esp]                      ;435.17
        push      ecx                                           ;435.17
        push      edx                                           ;435.17
        push      2                                             ;435.17
        push      eax                                           ;435.17
        mov       DWORD PTR [168+esi+edi], 133                  ;435.17
        mov       DWORD PTR [160+esi+edi], ecx                  ;435.17
        mov       DWORD PTR [180+esi+edi], edx                  ;435.17
        mov       DWORD PTR [184+esi+edi], ecx                  ;435.17
        call      _for_check_mult_overflow                      ;435.17
                                ; LOE ebx
.B10.1304:                      ; Preds .B10.135
        add       esp, 16                                       ;435.17
                                ; LOE ebx
.B10.136:                       ; Preds .B10.1304
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], ebx ;435.17
        jmp       .B10.139      ; Prob 100%                     ;435.17
                                ; LOE
.B10.137:                       ; Preds .B10.134
        mov       DWORD PTR [160+esp], 0                        ;437.21
        lea       eax, DWORD PTR [352+esp]                      ;437.21
        mov       DWORD PTR [352+esp], 47                       ;437.21
        mov       DWORD PTR [356+esp], OFFSET FLAT: __STRLITPACK_70 ;437.21
        push      32                                            ;437.21
        push      eax                                           ;437.21
        push      OFFSET FLAT: __STRLITPACK_153.0.10            ;437.21
        push      -2088435968                                   ;437.21
        push      911                                           ;437.21
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], ebx ;435.17
        lea       edx, DWORD PTR [180+esp]                      ;437.21
        push      edx                                           ;437.21
        call      _for_write_seq_lis                            ;437.21
                                ; LOE
.B10.138:                       ; Preds .B10.137
        push      32                                            ;438.18
        xor       eax, eax                                      ;438.18
        push      eax                                           ;438.18
        push      eax                                           ;438.18
        push      -2088435968                                   ;438.18
        push      eax                                           ;438.18
        push      OFFSET FLAT: __STRLITPACK_154                 ;438.18
        call      _for_stop_core                                ;438.18
                                ; LOE
.B10.1305:                      ; Preds .B10.138
        add       esp, 48                                       ;438.18
                                ; LOE
.B10.139:                       ; Preds .B10.1305 .B10.136
        mov       eax, DWORD PTR [508+esp]                      ;440.17
        sub       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;440.17
        imul      ebx, eax, 900                                 ;440.17
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;440.17
        mov       esi, DWORD PTR [180+ecx+ebx]                  ;440.17
        test      esi, esi                                      ;440.17
        mov       edx, DWORD PTR [188+ecx+ebx]                  ;440.17
        jle       .B10.146      ; Prob 50%                      ;440.17
                                ; LOE edx ecx ebx esi
.B10.140:                       ; Preds .B10.139
        mov       eax, esi                                      ;440.17
        shr       eax, 31                                       ;440.17
        add       eax, esi                                      ;440.17
        sar       eax, 1                                        ;440.17
        mov       DWORD PTR [480+esp], eax                      ;440.17
        test      eax, eax                                      ;440.17
        jbe       .B10.851      ; Prob 10%                      ;440.17
                                ; LOE edx ecx ebx esi
.B10.141:                       ; Preds .B10.140
        mov       edi, DWORD PTR [156+ecx+ebx]                  ;440.17
        xor       eax, eax                                      ;
        mov       DWORD PTR [40+esp], ebx                       ;440.17
        mov       DWORD PTR [32+esp], edx                       ;440.17
        xor       edx, edx                                      ;440.17
        mov       DWORD PTR [36+esp], ecx                       ;440.17
        mov       ebx, edi                                      ;440.17
        mov       edi, DWORD PTR [480+esp]                      ;440.17
                                ; LOE eax edx ebx esi edi
.B10.142:                       ; Preds .B10.142 .B10.141
        lea       ecx, DWORD PTR [eax+eax*4]                    ;440.17
        inc       eax                                           ;440.17
        shl       ecx, 4                                        ;440.17
        cmp       eax, edi                                      ;440.17
        mov       WORD PTR [36+ebx+ecx], dx                     ;440.17
        mov       WORD PTR [76+ebx+ecx], dx                     ;440.17
        jb        .B10.142      ; Prob 64%                      ;440.17
                                ; LOE eax edx ebx esi edi
.B10.143:                       ; Preds .B10.142
        mov       edx, DWORD PTR [32+esp]                       ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;440.17
        mov       ecx, DWORD PTR [36+esp]                       ;
        mov       ebx, DWORD PTR [40+esp]                       ;
                                ; LOE eax edx ecx ebx esi
.B10.144:                       ; Preds .B10.143 .B10.851
        lea       edi, DWORD PTR [-1+eax]                       ;440.17
        cmp       esi, edi                                      ;440.17
        jbe       .B10.146      ; Prob 10%                      ;440.17
                                ; LOE eax edx ecx ebx
.B10.145:                       ; Preds .B10.144
        mov       esi, edx                                      ;440.17
        add       eax, edx                                      ;440.17
        neg       esi                                           ;440.17
        add       esi, eax                                      ;440.17
        mov       ebx, DWORD PTR [156+ecx+ebx]                  ;440.17
        xor       ecx, ecx                                      ;440.17
        lea       eax, DWORD PTR [esi+esi*4]                    ;440.17
        mov       WORD PTR [-4+ebx+eax*8], cx                   ;440.17
                                ; LOE
.B10.146:                       ; Preds .B10.139 .B10.144 .B10.145
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIMPERAGG] ;442.17
        xor       eax, eax                                      ;442.17
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TDSPSTEP] ;442.17
        cmp       esi, edx                                      ;442.17
        push      40                                            ;442.17
        cmovge    edx, esi                                      ;442.17
        test      edx, edx                                      ;442.17
        cmovl     edx, eax                                      ;442.17
        push      edx                                           ;442.17
        push      2                                             ;442.17
        lea       ecx, DWORD PTR [448+esp]                      ;442.17
        push      ecx                                           ;442.17
        call      _for_check_mult_overflow                      ;442.17
                                ; LOE eax
.B10.147:                       ; Preds .B10.146
        imul      edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32], 900 ;442.17
        and       eax, 1                                        ;442.17
        imul      ecx, DWORD PTR [524+esp], 900                 ;442.26
        add       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;442.17
        lea       esi, DWORD PTR [-84+edx]                      ;442.17
        neg       edx                                           ;442.17
        neg       esi                                           ;442.17
        shl       eax, 4                                        ;442.17
        add       esi, ecx                                      ;442.17
        mov       ebx, DWORD PTR [96+edx+ecx]                   ;442.17
        and       ebx, 1                                        ;442.17
        add       ebx, ebx                                      ;442.17
        or        ebx, 1                                        ;442.17
        or        ebx, eax                                      ;442.17
        or        ebx, 262144                                   ;442.17
        push      ebx                                           ;442.17
        push      esi                                           ;442.17
        push      DWORD PTR [460+esp]                           ;442.17
        call      _for_alloc_allocatable                        ;442.17
                                ; LOE eax
.B10.1307:                      ; Preds .B10.147
        add       esp, 28                                       ;442.17
        mov       ebx, eax                                      ;442.17
                                ; LOE ebx
.B10.148:                       ; Preds .B10.1307
        test      ebx, ebx                                      ;442.17
        jne       .B10.151      ; Prob 50%                      ;442.17
                                ; LOE ebx
.B10.149:                       ; Preds .B10.148
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;442.17
        mov       edx, 1                                        ;442.17
        neg       edi                                           ;442.17
        mov       ecx, 40                                       ;442.17
        add       edi, DWORD PTR [508+esp]                      ;442.17
        xor       eax, eax                                      ;442.17
        imul      esi, edi, 900                                 ;442.17
        mov       DWORD PTR [esp], ebx                          ;
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;442.17
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIMPERAGG] ;442.17
        mov       DWORD PTR [100+ebx+esi], edx                  ;442.17
        mov       DWORD PTR [116+ebx+esi], edx                  ;442.17
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TDSPSTEP] ;442.17
        cmp       edi, edx                                      ;442.17
        mov       DWORD PTR [92+ebx+esi], eax                   ;442.17
        cmovge    edx, edi                                      ;442.17
        test      edx, edx                                      ;442.17
        mov       DWORD PTR [96+ebx+esi], 133                   ;442.17
        cmovl     edx, eax                                      ;442.17
        lea       eax, DWORD PTR [292+esp]                      ;442.17
        push      ecx                                           ;442.17
        push      edx                                           ;442.17
        push      2                                             ;442.17
        push      eax                                           ;442.17
        mov       DWORD PTR [88+ebx+esi], ecx                   ;442.17
        mov       DWORD PTR [108+ebx+esi], edx                  ;442.17
        mov       DWORD PTR [112+ebx+esi], ecx                  ;442.17
        mov       ebx, DWORD PTR [16+esp]                       ;442.17
        call      _for_check_mult_overflow                      ;442.17
                                ; LOE ebx bl bh
.B10.1308:                      ; Preds .B10.149
        add       esp, 16                                       ;442.17
                                ; LOE ebx bl bh
.B10.150:                       ; Preds .B10.1308
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], ebx ;442.17
        jmp       .B10.153      ; Prob 100%                     ;442.17
                                ; LOE
.B10.151:                       ; Preds .B10.148
        mov       DWORD PTR [160+esp], 0                        ;444.21
        lea       eax, DWORD PTR [360+esp]                      ;444.21
        mov       DWORD PTR [360+esp], 45                       ;444.21
        mov       DWORD PTR [364+esp], OFFSET FLAT: __STRLITPACK_68 ;444.21
        push      32                                            ;444.21
        push      eax                                           ;444.21
        push      OFFSET FLAT: __STRLITPACK_155.0.10            ;444.21
        push      -2088435968                                   ;444.21
        push      911                                           ;444.21
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], ebx ;442.17
        lea       edx, DWORD PTR [180+esp]                      ;444.21
        push      edx                                           ;444.21
        call      _for_write_seq_lis                            ;444.21
                                ; LOE
.B10.152:                       ; Preds .B10.151
        push      32                                            ;445.18
        xor       eax, eax                                      ;445.18
        push      eax                                           ;445.18
        push      eax                                           ;445.18
        push      -2088435968                                   ;445.18
        push      eax                                           ;445.18
        push      OFFSET FLAT: __STRLITPACK_156                 ;445.18
        call      _for_stop_core                                ;445.18
                                ; LOE
.B10.1309:                      ; Preds .B10.152
        add       esp, 48                                       ;445.18
                                ; LOE
.B10.153:                       ; Preds .B10.1309 .B10.150
        mov       eax, DWORD PTR [508+esp]                      ;447.17
        sub       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;447.17
        imul      ebx, eax, 900                                 ;447.17
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;447.17
        mov       esi, DWORD PTR [108+ecx+ebx]                  ;447.17
        test      esi, esi                                      ;447.17
        mov       edx, DWORD PTR [116+ecx+ebx]                  ;447.17
        jle       .B10.160      ; Prob 50%                      ;447.17
                                ; LOE edx ecx ebx esi
.B10.154:                       ; Preds .B10.153
        mov       eax, esi                                      ;447.17
        shr       eax, 31                                       ;447.17
        add       eax, esi                                      ;447.17
        sar       eax, 1                                        ;447.17
        mov       DWORD PTR [492+esp], eax                      ;447.17
        test      eax, eax                                      ;447.17
        jbe       .B10.852      ; Prob 10%                      ;447.17
                                ; LOE edx ecx ebx esi
.B10.155:                       ; Preds .B10.154
        mov       edi, DWORD PTR [84+ecx+ebx]                   ;447.17
        xor       eax, eax                                      ;
        mov       DWORD PTR [52+esp], ebx                       ;447.17
        mov       DWORD PTR [44+esp], edx                       ;447.17
        xor       edx, edx                                      ;447.17
        mov       DWORD PTR [48+esp], ecx                       ;447.17
        mov       ebx, edi                                      ;447.17
        mov       edi, DWORD PTR [492+esp]                      ;447.17
                                ; LOE eax edx ebx esi edi
.B10.156:                       ; Preds .B10.156 .B10.155
        lea       ecx, DWORD PTR [eax+eax*4]                    ;447.17
        inc       eax                                           ;447.17
        shl       ecx, 4                                        ;447.17
        cmp       eax, edi                                      ;447.17
        mov       WORD PTR [36+ebx+ecx], dx                     ;447.17
        mov       WORD PTR [76+ebx+ecx], dx                     ;447.17
        jb        .B10.156      ; Prob 64%                      ;447.17
                                ; LOE eax edx ebx esi edi
.B10.157:                       ; Preds .B10.156
        mov       edx, DWORD PTR [44+esp]                       ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;447.17
        mov       ecx, DWORD PTR [48+esp]                       ;
        mov       ebx, DWORD PTR [52+esp]                       ;
                                ; LOE eax edx ecx ebx esi
.B10.158:                       ; Preds .B10.157 .B10.852
        lea       edi, DWORD PTR [-1+eax]                       ;447.17
        cmp       esi, edi                                      ;447.17
        jbe       .B10.160      ; Prob 10%                      ;447.17
                                ; LOE eax edx ecx ebx
.B10.159:                       ; Preds .B10.158
        mov       esi, edx                                      ;447.17
        add       eax, edx                                      ;447.17
        neg       esi                                           ;447.17
        add       esi, eax                                      ;447.17
        mov       ebx, DWORD PTR [84+ecx+ebx]                   ;447.17
        xor       ecx, ecx                                      ;447.17
        lea       eax, DWORD PTR [esi+esi*4]                    ;447.17
        mov       WORD PTR [-4+ebx+eax*8], cx                   ;447.17
                                ; LOE
.B10.160:                       ; Preds .B10.153 .B10.158 .B10.159
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_AGGINT] ;449.17
        xor       eax, eax                                      ;449.17
        test      edx, edx                                      ;449.17
        push      4                                             ;449.17
        cmovle    edx, eax                                      ;449.17
        push      edx                                           ;449.17
        push      2                                             ;449.17
        lea       ecx, DWORD PTR [452+esp]                      ;449.17
        push      ecx                                           ;449.17
        call      _for_check_mult_overflow                      ;449.17
                                ; LOE eax
.B10.161:                       ; Preds .B10.160
        imul      edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32], 900 ;449.17
        and       eax, 1                                        ;449.17
        imul      ecx, DWORD PTR [524+esp], 900                 ;449.26
        add       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;449.17
        lea       esi, DWORD PTR [-228+edx]                     ;449.17
        neg       edx                                           ;449.17
        neg       esi                                           ;449.17
        shl       eax, 4                                        ;449.17
        add       esi, ecx                                      ;449.17
        mov       ebx, DWORD PTR [240+edx+ecx]                  ;449.17
        and       ebx, 1                                        ;449.17
        add       ebx, ebx                                      ;449.17
        or        ebx, 1                                        ;449.17
        or        ebx, eax                                      ;449.17
        or        ebx, 262144                                   ;449.17
        push      ebx                                           ;449.17
        push      esi                                           ;449.17
        push      DWORD PTR [464+esp]                           ;449.17
        call      _for_alloc_allocatable                        ;449.17
                                ; LOE eax
.B10.1311:                      ; Preds .B10.161
        add       esp, 28                                       ;449.17
        mov       ebx, eax                                      ;449.17
                                ; LOE ebx
.B10.162:                       ; Preds .B10.1311
        test      ebx, ebx                                      ;449.17
        jne       .B10.165      ; Prob 50%                      ;449.17
                                ; LOE ebx
.B10.163:                       ; Preds .B10.162
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;449.17
        mov       edx, 1                                        ;449.17
        neg       ecx                                           ;449.17
        xor       eax, eax                                      ;449.17
        add       ecx, DWORD PTR [508+esp]                      ;449.17
        imul      edi, ecx, 900                                 ;449.17
        mov       ecx, 4                                        ;449.17
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;449.17
        mov       DWORD PTR [244+esi+edi], edx                  ;449.17
        mov       DWORD PTR [260+esi+edi], edx                  ;449.17
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_AGGINT] ;449.17
        test      edx, edx                                      ;449.17
        mov       DWORD PTR [236+esi+edi], eax                  ;449.17
        cmovle    edx, eax                                      ;449.17
        lea       eax, DWORD PTR [296+esp]                      ;449.17
        push      ecx                                           ;449.17
        push      edx                                           ;449.17
        push      2                                             ;449.17
        push      eax                                           ;449.17
        mov       DWORD PTR [240+esi+edi], 133                  ;449.17
        mov       DWORD PTR [232+esi+edi], ecx                  ;449.17
        mov       DWORD PTR [252+esi+edi], edx                  ;449.17
        mov       DWORD PTR [256+esi+edi], ecx                  ;449.17
        call      _for_check_mult_overflow                      ;449.17
                                ; LOE ebx
.B10.1312:                      ; Preds .B10.163
        add       esp, 16                                       ;449.17
                                ; LOE ebx
.B10.164:                       ; Preds .B10.1312
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], ebx ;449.17
        jmp       .B10.167      ; Prob 100%                     ;449.17
                                ; LOE
.B10.165:                       ; Preds .B10.162
        mov       DWORD PTR [160+esp], 0                        ;451.21
        lea       eax, DWORD PTR [368+esp]                      ;451.21
        mov       DWORD PTR [368+esp], 47                       ;451.21
        mov       DWORD PTR [372+esp], OFFSET FLAT: __STRLITPACK_66 ;451.21
        push      32                                            ;451.21
        push      eax                                           ;451.21
        push      OFFSET FLAT: __STRLITPACK_157.0.10            ;451.21
        push      -2088435968                                   ;451.21
        push      911                                           ;451.21
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], ebx ;449.17
        lea       edx, DWORD PTR [180+esp]                      ;451.21
        push      edx                                           ;451.21
        call      _for_write_seq_lis                            ;451.21
                                ; LOE
.B10.166:                       ; Preds .B10.165
        push      32                                            ;452.18
        xor       eax, eax                                      ;452.18
        push      eax                                           ;452.18
        push      eax                                           ;452.18
        push      -2088435968                                   ;452.18
        push      eax                                           ;452.18
        push      OFFSET FLAT: __STRLITPACK_158                 ;452.18
        call      _for_stop_core                                ;452.18
                                ; LOE
.B10.1313:                      ; Preds .B10.166
        add       esp, 48                                       ;452.18
                                ; LOE
.B10.167:                       ; Preds .B10.1313 .B10.164
        mov       eax, DWORD PTR [508+esp]                      ;454.17
        sub       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;454.17
        imul      edx, eax, 900                                 ;454.17
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;454.17
        mov       esi, DWORD PTR [252+edi+edx]                  ;454.17
        test      esi, esi                                      ;454.17
        jle       .B10.184      ; Prob 50%                      ;454.17
                                ; LOE edx esi edi
.B10.168:                       ; Preds .B10.167
        cmp       esi, 4                                        ;454.17
        jl        .B10.853      ; Prob 10%                      ;454.17
                                ; LOE edx esi edi
.B10.169:                       ; Preds .B10.168
        mov       ebx, DWORD PTR [228+edi+edx]                  ;454.17
        mov       DWORD PTR [68+esp], ebx                       ;454.17
        and       ebx, 15                                       ;454.17
        je        .B10.172      ; Prob 50%                      ;454.17
                                ; LOE edx ebx esi edi
.B10.170:                       ; Preds .B10.169
        test      bl, 3                                         ;454.17
        jne       .B10.853      ; Prob 10%                      ;454.17
                                ; LOE edx ebx esi edi
.B10.171:                       ; Preds .B10.170
        neg       ebx                                           ;454.17
        add       ebx, 16                                       ;454.17
        shr       ebx, 2                                        ;454.17
                                ; LOE edx ebx esi edi
.B10.172:                       ; Preds .B10.171 .B10.169
        lea       eax, DWORD PTR [4+ebx]                        ;454.17
        cmp       esi, eax                                      ;454.17
        jl        .B10.853      ; Prob 10%                      ;454.17
                                ; LOE edx ebx esi edi
.B10.173:                       ; Preds .B10.172
        mov       ecx, esi                                      ;454.17
        sub       ecx, ebx                                      ;454.17
        and       ecx, 3                                        ;454.17
        neg       ecx                                           ;454.17
        add       ecx, esi                                      ;454.17
        test      ebx, ebx                                      ;454.17
        jbe       .B10.177      ; Prob 10%                      ;454.17
                                ; LOE edx ecx ebx esi edi
.B10.174:                       ; Preds .B10.173
        xor       eax, eax                                      ;
        mov       DWORD PTR [56+esp], edx                       ;
        mov       edx, DWORD PTR [68+esp]                       ;
                                ; LOE eax edx ecx ebx esi edi
.B10.175:                       ; Preds .B10.175 .B10.174
        mov       DWORD PTR [edx+eax*4], 100                    ;454.17
        inc       eax                                           ;454.17
        cmp       eax, ebx                                      ;454.17
        jb        .B10.175      ; Prob 82%                      ;454.17
                                ; LOE eax edx ecx ebx esi edi
.B10.176:                       ; Preds .B10.175
        mov       edx, DWORD PTR [56+esp]                       ;
                                ; LOE edx ecx ebx esi edi
.B10.177:                       ; Preds .B10.173 .B10.176
        movdqa    xmm0, XMMWORD PTR [_2il0floatpacket.22]       ;
        mov       eax, DWORD PTR [68+esp]                       ;
                                ; LOE eax edx ecx ebx esi edi xmm0
.B10.178:                       ; Preds .B10.178 .B10.177
        movdqa    XMMWORD PTR [eax+ebx*4], xmm0                 ;454.17
        add       ebx, 4                                        ;454.17
        cmp       ebx, ecx                                      ;454.17
        jb        .B10.178      ; Prob 82%                      ;454.17
                                ; LOE eax edx ecx ebx esi edi xmm0
.B10.180:                       ; Preds .B10.178 .B10.853
        cmp       ecx, esi                                      ;454.17
        jae       .B10.184      ; Prob 10%                      ;454.17
                                ; LOE edx ecx esi edi
.B10.181:                       ; Preds .B10.180
        mov       eax, DWORD PTR [228+edi+edx]                  ;454.17
                                ; LOE eax ecx esi
.B10.182:                       ; Preds .B10.182 .B10.181
        mov       DWORD PTR [eax+ecx*4], 100                    ;454.17
        inc       ecx                                           ;454.17
        cmp       ecx, esi                                      ;454.17
        jb        .B10.182      ; Prob 82%                      ;454.17
                                ; LOE eax ecx esi
.B10.184:                       ; Preds .B10.182 .B10.167 .B10.180
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIMPERAGG] ;456.17
        xor       eax, eax                                      ;456.17
        test      edx, edx                                      ;456.17
        push      4                                             ;456.17
        cmovle    edx, eax                                      ;456.17
        push      edx                                           ;456.17
        push      2                                             ;456.17
        lea       ecx, DWORD PTR [456+esp]                      ;456.17
        push      ecx                                           ;456.17
        call      _for_check_mult_overflow                      ;456.17
                                ; LOE eax
.B10.185:                       ; Preds .B10.184
        imul      edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32], 900 ;456.17
        and       eax, 1                                        ;456.17
        imul      ecx, DWORD PTR [524+esp], 900                 ;456.26
        add       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;456.17
        lea       esi, DWORD PTR [-264+edx]                     ;456.17
        neg       edx                                           ;456.17
        neg       esi                                           ;456.17
        shl       eax, 4                                        ;456.17
        add       esi, ecx                                      ;456.17
        mov       ebx, DWORD PTR [276+edx+ecx]                  ;456.17
        and       ebx, 1                                        ;456.17
        add       ebx, ebx                                      ;456.17
        or        ebx, 1                                        ;456.17
        or        ebx, eax                                      ;456.17
        or        ebx, 262144                                   ;456.17
        push      ebx                                           ;456.17
        push      esi                                           ;456.17
        push      DWORD PTR [468+esp]                           ;456.17
        call      _for_alloc_allocatable                        ;456.17
                                ; LOE eax
.B10.1315:                      ; Preds .B10.185
        add       esp, 28                                       ;456.17
        mov       ebx, eax                                      ;456.17
                                ; LOE ebx
.B10.186:                       ; Preds .B10.1315
        test      ebx, ebx                                      ;456.17
        jne       .B10.189      ; Prob 50%                      ;456.17
                                ; LOE ebx
.B10.187:                       ; Preds .B10.186
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;456.17
        mov       edx, 1                                        ;456.17
        neg       ecx                                           ;456.17
        xor       eax, eax                                      ;456.17
        add       ecx, DWORD PTR [508+esp]                      ;456.17
        imul      edi, ecx, 900                                 ;456.17
        mov       ecx, 4                                        ;456.17
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;456.17
        mov       DWORD PTR [280+esi+edi], edx                  ;456.17
        mov       DWORD PTR [296+esi+edi], edx                  ;456.17
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIMPERAGG] ;456.17
        test      edx, edx                                      ;456.17
        mov       DWORD PTR [272+esi+edi], eax                  ;456.17
        cmovle    edx, eax                                      ;456.17
        lea       eax, DWORD PTR [300+esp]                      ;456.17
        push      ecx                                           ;456.17
        push      edx                                           ;456.17
        push      2                                             ;456.17
        push      eax                                           ;456.17
        mov       DWORD PTR [276+esi+edi], 133                  ;456.17
        mov       DWORD PTR [268+esi+edi], ecx                  ;456.17
        mov       DWORD PTR [288+esi+edi], edx                  ;456.17
        mov       DWORD PTR [292+esi+edi], ecx                  ;456.17
        call      _for_check_mult_overflow                      ;456.17
                                ; LOE ebx
.B10.1316:                      ; Preds .B10.187
        add       esp, 16                                       ;456.17
                                ; LOE ebx
.B10.188:                       ; Preds .B10.1316
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], ebx ;456.17
        jmp       .B10.191      ; Prob 100%                     ;456.17
                                ; LOE
.B10.189:                       ; Preds .B10.186
        mov       DWORD PTR [160+esp], 0                        ;458.21
        lea       eax, DWORD PTR [376+esp]                      ;458.21
        mov       DWORD PTR [376+esp], 45                       ;458.21
        mov       DWORD PTR [380+esp], OFFSET FLAT: __STRLITPACK_64 ;458.21
        push      32                                            ;458.21
        push      eax                                           ;458.21
        push      OFFSET FLAT: __STRLITPACK_159.0.10            ;458.21
        push      -2088435968                                   ;458.21
        push      911                                           ;458.21
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], ebx ;456.17
        lea       edx, DWORD PTR [180+esp]                      ;458.21
        push      edx                                           ;458.21
        call      _for_write_seq_lis                            ;458.21
                                ; LOE
.B10.190:                       ; Preds .B10.189
        push      32                                            ;459.18
        xor       eax, eax                                      ;459.18
        push      eax                                           ;459.18
        push      eax                                           ;459.18
        push      -2088435968                                   ;459.18
        push      eax                                           ;459.18
        push      OFFSET FLAT: __STRLITPACK_160                 ;459.18
        call      _for_stop_core                                ;459.18
                                ; LOE
.B10.1317:                      ; Preds .B10.190
        add       esp, 48                                       ;459.18
                                ; LOE
.B10.191:                       ; Preds .B10.1317 .B10.188
        mov       eax, DWORD PTR [508+esp]                      ;461.17
        sub       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;461.17
        imul      ebx, eax, 900                                 ;461.17
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;461.17
        mov       esi, DWORD PTR [288+ecx+ebx]                  ;461.17
        test      esi, esi                                      ;461.17
        jle       .B10.194      ; Prob 50%                      ;461.17
                                ; LOE ecx ebx esi
.B10.192:                       ; Preds .B10.191
        cmp       esi, 24                                       ;461.17
        jle       .B10.856      ; Prob 0%                       ;461.17
                                ; LOE ecx ebx esi
.B10.193:                       ; Preds .B10.192
        shl       esi, 2                                        ;461.17
        push      esi                                           ;461.17
        push      0                                             ;461.17
        push      DWORD PTR [264+ecx+ebx]                       ;461.17
        call      __intel_fast_memset                           ;461.17
                                ; LOE
.B10.1318:                      ; Preds .B10.193
        add       esp, 12                                       ;461.17
                                ; LOE
.B10.194:                       ; Preds .B10.862 .B10.191 .B10.860 .B10.1318
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_AGGINT] ;463.17
        xor       eax, eax                                      ;463.17
        test      edx, edx                                      ;463.17
        push      4                                             ;463.17
        cmovle    edx, eax                                      ;463.17
        push      edx                                           ;463.17
        push      2                                             ;463.17
        lea       ecx, DWORD PTR [460+esp]                      ;463.17
        push      ecx                                           ;463.17
        call      _for_check_mult_overflow                      ;463.17
                                ; LOE eax
.B10.195:                       ; Preds .B10.194
        imul      edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32], 900 ;463.17
        and       eax, 1                                        ;463.17
        imul      ecx, DWORD PTR [524+esp], 900                 ;463.26
        add       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;463.17
        lea       esi, DWORD PTR [-300+edx]                     ;463.17
        neg       edx                                           ;463.17
        neg       esi                                           ;463.17
        shl       eax, 4                                        ;463.17
        add       esi, ecx                                      ;463.17
        mov       ebx, DWORD PTR [312+edx+ecx]                  ;463.17
        and       ebx, 1                                        ;463.17
        add       ebx, ebx                                      ;463.17
        or        ebx, 1                                        ;463.17
        or        ebx, eax                                      ;463.17
        or        ebx, 262144                                   ;463.17
        push      ebx                                           ;463.17
        push      esi                                           ;463.17
        push      DWORD PTR [472+esp]                           ;463.17
        call      _for_alloc_allocatable                        ;463.17
                                ; LOE eax
.B10.1320:                      ; Preds .B10.195
        add       esp, 28                                       ;463.17
        mov       ebx, eax                                      ;463.17
                                ; LOE ebx
.B10.196:                       ; Preds .B10.1320
        test      ebx, ebx                                      ;463.17
        jne       .B10.199      ; Prob 50%                      ;463.17
                                ; LOE ebx
.B10.197:                       ; Preds .B10.196
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;463.17
        mov       edx, 1                                        ;463.17
        neg       ecx                                           ;463.17
        xor       eax, eax                                      ;463.17
        add       ecx, DWORD PTR [508+esp]                      ;463.17
        imul      edi, ecx, 900                                 ;463.17
        mov       ecx, 4                                        ;463.17
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;463.17
        mov       DWORD PTR [316+esi+edi], edx                  ;463.17
        mov       DWORD PTR [332+esi+edi], edx                  ;463.17
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_AGGINT] ;463.17
        test      edx, edx                                      ;463.17
        mov       DWORD PTR [308+esi+edi], eax                  ;463.17
        cmovle    edx, eax                                      ;463.17
        lea       eax, DWORD PTR [304+esp]                      ;463.17
        push      ecx                                           ;463.17
        push      edx                                           ;463.17
        push      2                                             ;463.17
        push      eax                                           ;463.17
        mov       DWORD PTR [312+esi+edi], 133                  ;463.17
        mov       DWORD PTR [304+esi+edi], ecx                  ;463.17
        mov       DWORD PTR [324+esi+edi], edx                  ;463.17
        mov       DWORD PTR [328+esi+edi], ecx                  ;463.17
        call      _for_check_mult_overflow                      ;463.17
                                ; LOE ebx
.B10.1321:                      ; Preds .B10.197
        add       esp, 16                                       ;463.17
                                ; LOE ebx
.B10.198:                       ; Preds .B10.1321
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], ebx ;463.17
        jmp       .B10.201      ; Prob 100%                     ;463.17
                                ; LOE
.B10.199:                       ; Preds .B10.196
        mov       DWORD PTR [160+esp], 0                        ;465.21
        lea       eax, DWORD PTR [384+esp]                      ;465.21
        mov       DWORD PTR [384+esp], 47                       ;465.21
        mov       DWORD PTR [388+esp], OFFSET FLAT: __STRLITPACK_62 ;465.21
        push      32                                            ;465.21
        push      eax                                           ;465.21
        push      OFFSET FLAT: __STRLITPACK_161.0.10            ;465.21
        push      -2088435968                                   ;465.21
        push      911                                           ;465.21
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], ebx ;463.17
        lea       edx, DWORD PTR [180+esp]                      ;465.21
        push      edx                                           ;465.21
        call      _for_write_seq_lis                            ;465.21
                                ; LOE
.B10.200:                       ; Preds .B10.199
        push      32                                            ;466.18
        xor       eax, eax                                      ;466.18
        push      eax                                           ;466.18
        push      eax                                           ;466.18
        push      -2088435968                                   ;466.18
        push      eax                                           ;466.18
        push      OFFSET FLAT: __STRLITPACK_162                 ;466.18
        call      _for_stop_core                                ;466.18
                                ; LOE
.B10.1322:                      ; Preds .B10.200
        add       esp, 48                                       ;466.18
                                ; LOE
.B10.201:                       ; Preds .B10.1322 .B10.198
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;468.17
        mov       DWORD PTR [212+esp], esi                      ;468.17
        imul      esi, esi, 900                                 ;468.17
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;468.17
        mov       edx, ebx                                      ;468.17
        mov       eax, DWORD PTR [508+esp]                      ;468.17
        sub       edx, esi                                      ;468.17
        imul      edi, eax, 900                                 ;468.17
        mov       ecx, DWORD PTR [324+edx+edi]                  ;468.17
        test      ecx, ecx                                      ;468.17
        mov       DWORD PTR [156+esp], ebx                      ;468.17
        mov       DWORD PTR [112+esp], eax                      ;468.17
        jle       .B10.205      ; Prob 50%                      ;468.17
                                ; LOE edx ecx ebx esi edi bl bh
.B10.202:                       ; Preds .B10.201
        cmp       ecx, 24                                       ;468.17
        jle       .B10.865      ; Prob 0%                       ;468.17
                                ; LOE edx ecx ebx esi edi bl bh
.B10.203:                       ; Preds .B10.202
        shl       ecx, 2                                        ;468.17
        push      ecx                                           ;468.17
        push      0                                             ;468.17
        push      DWORD PTR [300+edx+edi]                       ;468.17
        call      __intel_fast_memset                           ;468.17
                                ; LOE
.B10.1323:                      ; Preds .B10.203
        add       esp, 12                                       ;468.17
                                ; LOE
.B10.204:                       ; Preds .B10.1323
        imul      esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32], 900 ;
        imul      edi, DWORD PTR [508+esp], 900                 ;470.26
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;470.17
                                ; LOE ebx esi edi
.B10.205:                       ; Preds .B10.871 .B10.204 .B10.869 .B10.201
        mov       eax, DWORD PTR [212+esp]                      ;470.17
        add       ebx, edi                                      ;470.17
        neg       eax                                           ;470.17
        add       esi, -864                                     ;470.17
        add       eax, DWORD PTR [112+esp]                      ;470.17
        sub       ebx, esi                                      ;470.17
        imul      ecx, eax, 900                                 ;470.17
        mov       edx, DWORD PTR [156+esp]                      ;470.17
        mov       esi, DWORD PTR [876+edx+ecx]                  ;470.17
        and       esi, 1                                        ;470.17
        add       esi, esi                                      ;470.17
        or        esi, 262145                                   ;470.17
        push      esi                                           ;470.17
        push      ebx                                           ;470.17
        push      120                                           ;470.17
        call      _for_alloc_allocatable                        ;470.17
                                ; LOE eax
.B10.1324:                      ; Preds .B10.205
        add       esp, 12                                       ;470.17
                                ; LOE eax
.B10.206:                       ; Preds .B10.1324
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;470.17
        test      eax, eax                                      ;470.17
        mov       DWORD PTR [204+esp], ebx                      ;470.17
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;470.17
        mov       edx, DWORD PTR [508+esp]                      ;470.26
        mov       DWORD PTR [120+esp], ebx                      ;470.17
        mov       DWORD PTR [140+esp], edx                      ;470.26
        jne       .B10.208      ; Prob 50%                      ;470.17
                                ; LOE eax ebx bl bh
.B10.207:                       ; Preds .B10.206
        imul      edi, DWORD PTR [204+esp], 900                 ;470.17
        mov       ecx, ebx                                      ;470.17
        imul      esi, DWORD PTR [140+esp], 900                 ;470.17
        sub       ecx, edi                                      ;470.17
        mov       edx, 4                                        ;470.17
        add       ecx, esi                                      ;470.17
        mov       DWORD PTR [476+esp], esi                      ;470.17
        mov       esi, 1                                        ;470.17
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;470.17
        mov       DWORD PTR [876+ecx], 133                      ;470.17
        mov       DWORD PTR [868+ecx], edx                      ;470.17
        mov       DWORD PTR [880+ecx], esi                      ;470.17
        mov       DWORD PTR [872+ecx], 0                        ;470.17
        mov       DWORD PTR [896+ecx], esi                      ;470.17
        mov       esi, 30                                       ;470.17
        mov       DWORD PTR [888+ecx], 30                       ;470.17
        mov       DWORD PTR [892+ecx], edx                      ;470.17
        jmp       .B10.211      ; Prob 100%                     ;470.17
                                ; LOE ecx ebx esi edi bl bh
.B10.208:                       ; Preds .B10.206
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;470.17
        lea       eax, DWORD PTR [232+esp]                      ;472.21
        mov       DWORD PTR [160+esp], 0                        ;472.21
        mov       DWORD PTR [232+esp], 50                       ;472.21
        mov       DWORD PTR [236+esp], OFFSET FLAT: __STRLITPACK_60 ;472.21
        push      32                                            ;472.21
        push      eax                                           ;472.21
        push      OFFSET FLAT: __STRLITPACK_163.0.10            ;472.21
        push      -2088435968                                   ;472.21
        push      911                                           ;472.21
        lea       edx, DWORD PTR [180+esp]                      ;472.21
        push      edx                                           ;472.21
        call      _for_write_seq_lis                            ;472.21
                                ; LOE ebx bl bh
.B10.209:                       ; Preds .B10.208
        push      32                                            ;473.18
        xor       eax, eax                                      ;473.18
        push      eax                                           ;473.18
        push      eax                                           ;473.18
        push      -2088435968                                   ;473.18
        push      eax                                           ;473.18
        push      OFFSET FLAT: __STRLITPACK_164                 ;473.18
        call      _for_stop_core                                ;473.18
                                ; LOE ebx bl bh
.B10.1325:                      ; Preds .B10.209
        add       esp, 48                                       ;473.18
                                ; LOE ebx bl bh
.B10.210:                       ; Preds .B10.1325
        imul      edi, DWORD PTR [204+esp], 900                 ;
        mov       ecx, ebx                                      ;
        imul      eax, DWORD PTR [140+esp], 900                 ;
        sub       ecx, edi                                      ;
        add       ecx, eax                                      ;
        mov       DWORD PTR [476+esp], eax                      ;
        mov       esi, DWORD PTR [888+ecx]                      ;475.17
        test      esi, esi                                      ;475.17
        jle       .B10.214      ; Prob 50%                      ;475.17
                                ; LOE ecx ebx esi edi bl bh
.B10.211:                       ; Preds .B10.207 .B10.210
        cmp       esi, 24                                       ;475.17
        jle       .B10.874      ; Prob 0%                       ;475.17
                                ; LOE ecx ebx esi edi bl bh
.B10.212:                       ; Preds .B10.211
        shl       esi, 2                                        ;475.17
        push      esi                                           ;475.17
        push      0                                             ;475.17
        push      DWORD PTR [864+ecx]                           ;475.17
        call      __intel_fast_memset                           ;475.17
                                ; LOE
.B10.1326:                      ; Preds .B10.212
        add       esp, 12                                       ;475.17
                                ; LOE
.B10.213:                       ; Preds .B10.1326
        imul      eax, DWORD PTR [508+esp], 900                 ;477.26
        imul      edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32], 900 ;
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;477.17
        mov       DWORD PTR [476+esp], eax                      ;477.26
                                ; LOE ebx edi
.B10.214:                       ; Preds .B10.880 .B10.213 .B10.878 .B10.210
        mov       eax, DWORD PTR [204+esp]                      ;477.17
        add       edi, -356                                     ;477.17
        neg       eax                                           ;477.17
        add       eax, DWORD PTR [140+esp]                      ;477.17
        imul      ecx, eax, 900                                 ;477.17
        mov       edx, DWORD PTR [120+esp]                      ;477.17
        add       ebx, DWORD PTR [476+esp]                      ;477.17
        sub       ebx, edi                                      ;477.17
        mov       esi, DWORD PTR [368+edx+ecx]                  ;477.17
        and       esi, 1                                        ;477.17
        add       esi, esi                                      ;477.17
        or        esi, 262145                                   ;477.17
        push      esi                                           ;477.17
        push      ebx                                           ;477.17
        push      8                                             ;477.17
        call      _for_alloc_allocatable                        ;477.17
                                ; LOE eax
.B10.1327:                      ; Preds .B10.214
        add       esp, 12                                       ;477.17
                                ; LOE eax
.B10.215:                       ; Preds .B10.1327
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;477.17
        test      eax, eax                                      ;477.17
        mov       DWORD PTR [208+esp], ebx                      ;477.17
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;477.17
        mov       edx, DWORD PTR [508+esp]                      ;477.26
        mov       DWORD PTR [128+esp], ebx                      ;477.17
        mov       DWORD PTR [132+esp], edx                      ;477.26
        jne       .B10.217      ; Prob 50%                      ;477.17
                                ; LOE eax ebx bl bh
.B10.216:                       ; Preds .B10.215
        imul      esi, DWORD PTR [208+esp], 900                 ;477.17
        mov       edi, ebx                                      ;477.17
        imul      ecx, DWORD PTR [132+esp], 900                 ;477.17
        sub       edi, esi                                      ;477.17
        mov       edx, 4                                        ;477.17
        add       edi, ecx                                      ;477.17
        mov       DWORD PTR [468+esp], ecx                      ;477.17
        mov       ecx, 1                                        ;477.17
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;477.17
        mov       DWORD PTR [368+edi], 133                      ;477.17
        mov       DWORD PTR [360+edi], edx                      ;477.17
        mov       DWORD PTR [372+edi], ecx                      ;477.17
        mov       DWORD PTR [364+edi], 0                        ;477.17
        mov       DWORD PTR [388+edi], ecx                      ;477.17
        mov       ecx, 2                                        ;477.17
        mov       DWORD PTR [380+edi], 2                        ;477.17
        mov       DWORD PTR [384+edi], edx                      ;477.17
        jmp       .B10.220      ; Prob 100%                     ;477.17
                                ; LOE ecx ebx esi edi bl bh
.B10.217:                       ; Preds .B10.215
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;477.17
        lea       eax, DWORD PTR [240+esp]                      ;479.21
        mov       DWORD PTR [160+esp], 0                        ;479.21
        mov       DWORD PTR [240+esp], 46                       ;479.21
        mov       DWORD PTR [244+esp], OFFSET FLAT: __STRLITPACK_58 ;479.21
        push      32                                            ;479.21
        push      eax                                           ;479.21
        push      OFFSET FLAT: __STRLITPACK_165.0.10            ;479.21
        push      -2088435968                                   ;479.21
        push      911                                           ;479.21
        lea       edx, DWORD PTR [180+esp]                      ;479.21
        push      edx                                           ;479.21
        call      _for_write_seq_lis                            ;479.21
                                ; LOE ebx bl bh
.B10.218:                       ; Preds .B10.217
        push      32                                            ;480.18
        xor       eax, eax                                      ;480.18
        push      eax                                           ;480.18
        push      eax                                           ;480.18
        push      -2088435968                                   ;480.18
        push      eax                                           ;480.18
        push      OFFSET FLAT: __STRLITPACK_166                 ;480.18
        call      _for_stop_core                                ;480.18
                                ; LOE ebx bl bh
.B10.1328:                      ; Preds .B10.218
        add       esp, 48                                       ;480.18
                                ; LOE ebx bl bh
.B10.219:                       ; Preds .B10.1328
        imul      esi, DWORD PTR [208+esp], 900                 ;
        mov       edi, ebx                                      ;
        imul      eax, DWORD PTR [132+esp], 900                 ;
        sub       edi, esi                                      ;
        add       edi, eax                                      ;
        mov       DWORD PTR [468+esp], eax                      ;
        mov       ecx, DWORD PTR [380+edi]                      ;482.17
        test      ecx, ecx                                      ;482.17
        jle       .B10.223      ; Prob 50%                      ;482.17
                                ; LOE ecx ebx esi edi bl bh
.B10.220:                       ; Preds .B10.216 .B10.219
        cmp       ecx, 24                                       ;482.17
        jle       .B10.883      ; Prob 0%                       ;482.17
                                ; LOE ecx ebx esi edi bl bh
.B10.221:                       ; Preds .B10.220
        shl       ecx, 2                                        ;482.17
        push      ecx                                           ;482.17
        push      0                                             ;482.17
        push      DWORD PTR [356+edi]                           ;482.17
        call      __intel_fast_memset                           ;482.17
                                ; LOE
.B10.1329:                      ; Preds .B10.221
        add       esp, 12                                       ;482.17
                                ; LOE
.B10.222:                       ; Preds .B10.1329
        imul      eax, DWORD PTR [508+esp], 900                 ;484.26
        imul      esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32], 900 ;
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;484.17
        mov       DWORD PTR [468+esp], eax                      ;484.26
                                ; LOE ebx esi
.B10.223:                       ; Preds .B10.889 .B10.222 .B10.887 .B10.219
        mov       eax, DWORD PTR [208+esp]                      ;484.17
        add       esi, -456                                     ;484.17
        neg       eax                                           ;484.17
        add       eax, DWORD PTR [132+esp]                      ;484.17
        imul      ecx, eax, 900                                 ;484.17
        mov       edx, DWORD PTR [128+esp]                      ;484.17
        add       ebx, DWORD PTR [468+esp]                      ;484.17
        sub       ebx, esi                                      ;484.17
        mov       esi, DWORD PTR [468+edx+ecx]                  ;484.17
        and       esi, 1                                        ;484.17
        add       esi, esi                                      ;484.17
        or        esi, 262145                                   ;484.17
        push      esi                                           ;484.17
        push      ebx                                           ;484.17
        push      120                                           ;484.17
        call      _for_alloc_allocatable                        ;484.17
                                ; LOE eax
.B10.1330:                      ; Preds .B10.223
        add       esp, 12                                       ;484.17
                                ; LOE eax
.B10.224:                       ; Preds .B10.1330
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;484.17
        test      eax, eax                                      ;484.17
        mov       DWORD PTR [200+esp], ebx                      ;484.17
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;484.17
        mov       edx, DWORD PTR [508+esp]                      ;484.26
        mov       DWORD PTR [124+esp], ebx                      ;484.17
        mov       DWORD PTR [136+esp], edx                      ;484.26
        jne       .B10.226      ; Prob 50%                      ;484.17
                                ; LOE eax ebx bl bh
.B10.225:                       ; Preds .B10.224
        imul      edi, DWORD PTR [200+esp], 900                 ;484.17
        mov       ecx, ebx                                      ;484.17
        imul      esi, DWORD PTR [136+esp], 900                 ;484.17
        sub       ecx, edi                                      ;484.17
        mov       edx, 4                                        ;484.17
        add       ecx, esi                                      ;484.17
        mov       DWORD PTR [472+esp], esi                      ;484.17
        mov       esi, 1                                        ;484.17
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;484.17
        mov       DWORD PTR [468+ecx], 133                      ;484.17
        mov       DWORD PTR [460+ecx], edx                      ;484.17
        mov       DWORD PTR [472+ecx], esi                      ;484.17
        mov       DWORD PTR [464+ecx], 0                        ;484.17
        mov       DWORD PTR [488+ecx], esi                      ;484.17
        mov       esi, 30                                       ;484.17
        mov       DWORD PTR [480+ecx], 30                       ;484.17
        mov       DWORD PTR [484+ecx], edx                      ;484.17
        jmp       .B10.229      ; Prob 100%                     ;484.17
                                ; LOE ecx ebx esi edi bl bh
.B10.226:                       ; Preds .B10.224
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;484.17
        lea       eax, DWORD PTR [248+esp]                      ;486.21
        mov       DWORD PTR [160+esp], 0                        ;486.21
        mov       DWORD PTR [248+esp], 46                       ;486.21
        mov       DWORD PTR [252+esp], OFFSET FLAT: __STRLITPACK_56 ;486.21
        push      32                                            ;486.21
        push      eax                                           ;486.21
        push      OFFSET FLAT: __STRLITPACK_167.0.10            ;486.21
        push      -2088435968                                   ;486.21
        push      911                                           ;486.21
        lea       edx, DWORD PTR [180+esp]                      ;486.21
        push      edx                                           ;486.21
        call      _for_write_seq_lis                            ;486.21
                                ; LOE ebx bl bh
.B10.227:                       ; Preds .B10.226
        push      32                                            ;487.18
        xor       eax, eax                                      ;487.18
        push      eax                                           ;487.18
        push      eax                                           ;487.18
        push      -2088435968                                   ;487.18
        push      eax                                           ;487.18
        push      OFFSET FLAT: __STRLITPACK_168                 ;487.18
        call      _for_stop_core                                ;487.18
                                ; LOE ebx bl bh
.B10.1331:                      ; Preds .B10.227
        add       esp, 48                                       ;487.18
                                ; LOE ebx bl bh
.B10.228:                       ; Preds .B10.1331
        imul      edi, DWORD PTR [200+esp], 900                 ;
        mov       ecx, ebx                                      ;
        imul      eax, DWORD PTR [136+esp], 900                 ;
        sub       ecx, edi                                      ;
        add       ecx, eax                                      ;
        mov       DWORD PTR [472+esp], eax                      ;
        mov       esi, DWORD PTR [480+ecx]                      ;489.17
        test      esi, esi                                      ;489.17
        jle       .B10.232      ; Prob 50%                      ;489.17
                                ; LOE ecx ebx esi edi bl bh
.B10.229:                       ; Preds .B10.225 .B10.228
        cmp       esi, 24                                       ;489.17
        jle       .B10.892      ; Prob 0%                       ;489.17
                                ; LOE ecx ebx esi edi bl bh
.B10.230:                       ; Preds .B10.229
        shl       esi, 2                                        ;489.17
        push      esi                                           ;489.17
        push      0                                             ;489.17
        push      DWORD PTR [456+ecx]                           ;489.17
        call      __intel_fast_memset                           ;489.17
                                ; LOE
.B10.1332:                      ; Preds .B10.230
        add       esp, 12                                       ;489.17
                                ; LOE
.B10.231:                       ; Preds .B10.1332
        imul      eax, DWORD PTR [508+esp], 900                 ;491.26
        imul      edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32], 900 ;
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;491.17
        mov       DWORD PTR [472+esp], eax                      ;491.26
                                ; LOE ebx edi
.B10.232:                       ; Preds .B10.898 .B10.231 .B10.896 .B10.228
        mov       eax, DWORD PTR [200+esp]                      ;491.17
        add       edi, -492                                     ;491.17
        neg       eax                                           ;491.17
        add       eax, DWORD PTR [136+esp]                      ;491.17
        imul      ecx, eax, 900                                 ;491.17
        mov       edx, DWORD PTR [124+esp]                      ;491.17
        add       ebx, DWORD PTR [472+esp]                      ;491.17
        sub       ebx, edi                                      ;491.17
        mov       esi, DWORD PTR [504+edx+ecx]                  ;491.17
        and       esi, 1                                        ;491.17
        add       esi, esi                                      ;491.17
        or        esi, 262145                                   ;491.17
        push      esi                                           ;491.17
        push      ebx                                           ;491.17
        push      120                                           ;491.17
        call      _for_alloc_allocatable                        ;491.17
                                ; LOE eax
.B10.1333:                      ; Preds .B10.232
        add       esp, 12                                       ;491.17
                                ; LOE eax
.B10.233:                       ; Preds .B10.1333
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;491.17
        test      eax, eax                                      ;491.17
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;491.17
        mov       ebx, DWORD PTR [508+esp]                      ;491.26
        jne       .B10.235      ; Prob 50%                      ;491.17
                                ; LOE eax ebx esi edi
.B10.234:                       ; Preds .B10.233
        sub       ebx, edi                                      ;491.17
        mov       edx, 1                                        ;491.17
        imul      ecx, ebx, 900                                 ;491.17
        mov       ebx, 4                                        ;491.17
        add       ecx, esi                                      ;491.17
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;491.17
        mov       DWORD PTR [504+ecx], 133                      ;491.17
        mov       DWORD PTR [496+ecx], ebx                      ;491.17
        mov       DWORD PTR [508+ecx], edx                      ;491.17
        mov       DWORD PTR [500+ecx], 0                        ;491.17
        mov       DWORD PTR [524+ecx], edx                      ;491.17
        mov       edx, 30                                       ;491.17
        mov       DWORD PTR [516+ecx], 30                       ;491.17
        mov       DWORD PTR [520+ecx], ebx                      ;491.17
        jmp       .B10.238      ; Prob 100%                     ;491.17
                                ; LOE edx ecx
.B10.235:                       ; Preds .B10.233
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;491.17
        lea       eax, DWORD PTR [256+esp]                      ;493.21
        mov       DWORD PTR [160+esp], 0                        ;493.21
        mov       DWORD PTR [256+esp], 46                       ;493.21
        mov       DWORD PTR [260+esp], OFFSET FLAT: __STRLITPACK_54 ;493.21
        push      32                                            ;493.21
        push      eax                                           ;493.21
        push      OFFSET FLAT: __STRLITPACK_169.0.10            ;493.21
        push      -2088435968                                   ;493.21
        push      911                                           ;493.21
        lea       edx, DWORD PTR [180+esp]                      ;493.21
        push      edx                                           ;493.21
        call      _for_write_seq_lis                            ;493.21
                                ; LOE ebx esi edi
.B10.236:                       ; Preds .B10.235
        push      32                                            ;494.18
        xor       eax, eax                                      ;494.18
        push      eax                                           ;494.18
        push      eax                                           ;494.18
        push      -2088435968                                   ;494.18
        push      eax                                           ;494.18
        push      OFFSET FLAT: __STRLITPACK_170                 ;494.18
        call      _for_stop_core                                ;494.18
                                ; LOE ebx esi edi
.B10.1334:                      ; Preds .B10.236
        add       esp, 48                                       ;494.18
                                ; LOE ebx esi edi
.B10.237:                       ; Preds .B10.1334
        sub       ebx, edi                                      ;
        imul      ecx, ebx, 900                                 ;
        add       ecx, esi                                      ;
        mov       edx, DWORD PTR [516+ecx]                      ;496.17
        test      edx, edx                                      ;496.17
        jle       .B10.240      ; Prob 50%                      ;496.17
                                ; LOE edx ecx
.B10.238:                       ; Preds .B10.234 .B10.237
        cmp       edx, 24                                       ;496.17
        jle       .B10.901      ; Prob 0%                       ;496.17
                                ; LOE edx ecx
.B10.239:                       ; Preds .B10.238
        shl       edx, 2                                        ;496.17
        push      edx                                           ;496.17
        push      0                                             ;496.17
        push      DWORD PTR [492+ecx]                           ;496.17
        call      __intel_fast_memset                           ;496.17
                                ; LOE
.B10.1335:                      ; Preds .B10.239
        add       esp, 12                                       ;496.17
                                ; LOE
.B10.240:                       ; Preds .B10.907 .B10.1335 .B10.237 .B10.905
        movss     xmm3, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIMPERIOD] ;498.61
        xor       eax, eax                                      ;498.17
        movss     xmm0, DWORD PTR [_2il0floatpacket.25]         ;498.61
        andps     xmm0, xmm3                                    ;498.61
        pxor      xmm3, xmm0                                    ;498.61
        movss     xmm1, DWORD PTR [_2il0floatpacket.26]         ;498.61
        movaps    xmm2, xmm3                                    ;498.61
        movss     xmm4, DWORD PTR [_2il0floatpacket.27]         ;498.61
        cmpltss   xmm2, xmm1                                    ;498.61
        andps     xmm1, xmm2                                    ;498.61
        movaps    xmm2, xmm3                                    ;498.61
        movaps    xmm6, xmm4                                    ;498.61
        addss     xmm2, xmm1                                    ;498.61
        addss     xmm6, xmm4                                    ;498.61
        subss     xmm2, xmm1                                    ;498.61
        movaps    xmm7, xmm2                                    ;498.61
        push      4                                             ;498.17
        subss     xmm7, xmm3                                    ;498.61
        movaps    xmm5, xmm7                                    ;498.61
        cmpless   xmm7, DWORD PTR [_2il0floatpacket.28]         ;498.61
        cmpnless  xmm5, xmm4                                    ;498.61
        andps     xmm5, xmm6                                    ;498.61
        andps     xmm7, xmm6                                    ;498.61
        subss     xmm2, xmm5                                    ;498.61
        addss     xmm2, xmm7                                    ;498.61
        orps      xmm2, xmm0                                    ;498.61
        cvtss2si  edx, xmm2                                     ;498.17
        test      edx, edx                                      ;498.17
        cmovl     edx, eax                                      ;498.17
        push      edx                                           ;498.17
        push      2                                             ;498.17
        lea       ecx, DWORD PTR [464+esp]                      ;498.17
        push      ecx                                           ;498.17
        call      _for_check_mult_overflow                      ;498.17
                                ; LOE eax
.B10.241:                       ; Preds .B10.240
        imul      edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32], 900 ;498.17
        and       eax, 1                                        ;498.17
        imul      ecx, DWORD PTR [524+esp], 900                 ;498.26
        add       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;498.17
        lea       esi, DWORD PTR [-528+edx]                     ;498.17
        neg       edx                                           ;498.17
        neg       esi                                           ;498.17
        shl       eax, 4                                        ;498.17
        add       esi, ecx                                      ;498.17
        mov       ebx, DWORD PTR [540+edx+ecx]                  ;498.17
        and       ebx, 1                                        ;498.17
        add       ebx, ebx                                      ;498.17
        or        ebx, 1                                        ;498.17
        or        ebx, eax                                      ;498.17
        or        ebx, 262144                                   ;498.17
        push      ebx                                           ;498.17
        push      esi                                           ;498.17
        push      DWORD PTR [476+esp]                           ;498.17
        call      _for_alloc_allocatable                        ;498.17
                                ; LOE eax
.B10.1337:                      ; Preds .B10.241
        add       esp, 28                                       ;498.17
        mov       ebx, eax                                      ;498.17
                                ; LOE ebx
.B10.242:                       ; Preds .B10.1337
        test      ebx, ebx                                      ;498.17
        jne       .B10.245      ; Prob 50%                      ;498.17
                                ; LOE ebx
.B10.243:                       ; Preds .B10.242
        movss     xmm3, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIMPERIOD] ;498.61
        mov       edx, 1                                        ;498.17
        movss     xmm0, DWORD PTR [_2il0floatpacket.25]         ;498.61
        xor       eax, eax                                      ;498.17
        andps     xmm0, xmm3                                    ;498.61
        pxor      xmm3, xmm0                                    ;498.61
        movss     xmm1, DWORD PTR [_2il0floatpacket.26]         ;498.61
        movaps    xmm2, xmm3                                    ;498.61
        movss     xmm4, DWORD PTR [_2il0floatpacket.27]         ;498.61
        cmpltss   xmm2, xmm1                                    ;498.61
        andps     xmm1, xmm2                                    ;498.61
        movaps    xmm2, xmm3                                    ;498.61
        movaps    xmm6, xmm4                                    ;498.61
        addss     xmm2, xmm1                                    ;498.61
        addss     xmm6, xmm4                                    ;498.61
        subss     xmm2, xmm1                                    ;498.61
        movaps    xmm7, xmm2                                    ;498.61
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;498.17
        subss     xmm7, xmm3                                    ;498.61
        movaps    xmm5, xmm7                                    ;498.61
        neg       ecx                                           ;498.17
        add       ecx, DWORD PTR [508+esp]                      ;498.17
        cmpnless  xmm5, xmm4                                    ;498.61
        imul      edi, ecx, 900                                 ;498.17
        cmpless   xmm7, DWORD PTR [_2il0floatpacket.28]         ;498.61
        andps     xmm5, xmm6                                    ;498.61
        andps     xmm7, xmm6                                    ;498.61
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;498.17
        mov       ecx, 4                                        ;498.17
        subss     xmm2, xmm5                                    ;498.61
        mov       DWORD PTR [544+esi+edi], edx                  ;498.17
        addss     xmm2, xmm7                                    ;498.61
        orps      xmm2, xmm0                                    ;498.61
        mov       DWORD PTR [560+esi+edi], edx                  ;498.17
        cvtss2si  edx, xmm2                                     ;498.17
        test      edx, edx                                      ;498.17
        mov       DWORD PTR [536+esi+edi], eax                  ;498.17
        cmovl     edx, eax                                      ;498.17
        lea       eax, DWORD PTR [308+esp]                      ;498.17
        push      ecx                                           ;498.17
        push      edx                                           ;498.17
        push      2                                             ;498.17
        push      eax                                           ;498.17
        mov       DWORD PTR [540+esi+edi], 133                  ;498.17
        mov       DWORD PTR [532+esi+edi], ecx                  ;498.17
        mov       DWORD PTR [552+esi+edi], edx                  ;498.17
        mov       DWORD PTR [556+esi+edi], ecx                  ;498.17
        call      _for_check_mult_overflow                      ;498.17
                                ; LOE ebx
.B10.1338:                      ; Preds .B10.243
        add       esp, 16                                       ;498.17
                                ; LOE ebx
.B10.244:                       ; Preds .B10.1338
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], ebx ;498.17
        jmp       .B10.247      ; Prob 100%                     ;498.17
                                ; LOE
.B10.245:                       ; Preds .B10.242
        mov       DWORD PTR [160+esp], 0                        ;500.21
        lea       eax, DWORD PTR [392+esp]                      ;500.21
        mov       DWORD PTR [392+esp], 44                       ;500.21
        mov       DWORD PTR [396+esp], OFFSET FLAT: __STRLITPACK_52 ;500.21
        push      32                                            ;500.21
        push      eax                                           ;500.21
        push      OFFSET FLAT: __STRLITPACK_171.0.10            ;500.21
        push      -2088435968                                   ;500.21
        push      911                                           ;500.21
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], ebx ;498.17
        lea       edx, DWORD PTR [180+esp]                      ;500.21
        push      edx                                           ;500.21
        call      _for_write_seq_lis                            ;500.21
                                ; LOE
.B10.246:                       ; Preds .B10.245
        push      32                                            ;501.18
        xor       eax, eax                                      ;501.18
        push      eax                                           ;501.18
        push      eax                                           ;501.18
        push      -2088435968                                   ;501.18
        push      eax                                           ;501.18
        push      OFFSET FLAT: __STRLITPACK_172                 ;501.18
        call      _for_stop_core                                ;501.18
                                ; LOE
.B10.1339:                      ; Preds .B10.246
        add       esp, 48                                       ;501.18
                                ; LOE
.B10.247:                       ; Preds .B10.1339 .B10.244
        mov       eax, DWORD PTR [508+esp]                      ;503.17
        sub       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;503.17
        imul      ebx, eax, 900                                 ;503.17
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;503.17
        mov       esi, DWORD PTR [552+ecx+ebx]                  ;503.17
        test      esi, esi                                      ;503.17
        jle       .B10.250      ; Prob 50%                      ;503.17
                                ; LOE ecx ebx esi
.B10.248:                       ; Preds .B10.247
        cmp       esi, 24                                       ;503.17
        jle       .B10.910      ; Prob 0%                       ;503.17
                                ; LOE ecx ebx esi
.B10.249:                       ; Preds .B10.248
        shl       esi, 2                                        ;503.17
        push      esi                                           ;503.17
        push      0                                             ;503.17
        push      DWORD PTR [528+ecx+ebx]                       ;503.17
        call      __intel_fast_memset                           ;503.17
                                ; LOE
.B10.1340:                      ; Preds .B10.249
        add       esp, 12                                       ;503.17
                                ; LOE
.B10.250:                       ; Preds .B10.916 .B10.1340 .B10.247 .B10.914
        movss     xmm3, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIMPERIOD] ;505.62
        xor       eax, eax                                      ;505.17
        movss     xmm0, DWORD PTR [_2il0floatpacket.25]         ;505.62
        andps     xmm0, xmm3                                    ;505.62
        pxor      xmm3, xmm0                                    ;505.62
        movss     xmm1, DWORD PTR [_2il0floatpacket.26]         ;505.62
        movaps    xmm2, xmm3                                    ;505.62
        movss     xmm4, DWORD PTR [_2il0floatpacket.27]         ;505.62
        cmpltss   xmm2, xmm1                                    ;505.62
        andps     xmm1, xmm2                                    ;505.62
        movaps    xmm2, xmm3                                    ;505.62
        movaps    xmm6, xmm4                                    ;505.62
        addss     xmm2, xmm1                                    ;505.62
        addss     xmm6, xmm4                                    ;505.62
        subss     xmm2, xmm1                                    ;505.62
        movaps    xmm7, xmm2                                    ;505.62
        push      4                                             ;505.17
        subss     xmm7, xmm3                                    ;505.62
        movaps    xmm5, xmm7                                    ;505.62
        cmpless   xmm7, DWORD PTR [_2il0floatpacket.28]         ;505.62
        cmpnless  xmm5, xmm4                                    ;505.62
        andps     xmm5, xmm6                                    ;505.62
        andps     xmm7, xmm6                                    ;505.62
        subss     xmm2, xmm5                                    ;505.62
        addss     xmm2, xmm7                                    ;505.62
        orps      xmm2, xmm0                                    ;505.62
        cvtss2si  edx, xmm2                                     ;505.17
        test      edx, edx                                      ;505.17
        cmovl     edx, eax                                      ;505.17
        push      edx                                           ;505.17
        push      2                                             ;505.17
        lea       ecx, DWORD PTR [468+esp]                      ;505.17
        push      ecx                                           ;505.17
        call      _for_check_mult_overflow                      ;505.17
                                ; LOE eax
.B10.251:                       ; Preds .B10.250
        imul      edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32], 900 ;505.17
        and       eax, 1                                        ;505.17
        imul      ecx, DWORD PTR [524+esp], 900                 ;505.26
        add       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;505.17
        lea       esi, DWORD PTR [-564+edx]                     ;505.17
        neg       edx                                           ;505.17
        neg       esi                                           ;505.17
        shl       eax, 4                                        ;505.17
        add       esi, ecx                                      ;505.17
        mov       ebx, DWORD PTR [576+edx+ecx]                  ;505.17
        and       ebx, 1                                        ;505.17
        add       ebx, ebx                                      ;505.17
        or        ebx, 1                                        ;505.17
        or        ebx, eax                                      ;505.17
        or        ebx, 262144                                   ;505.17
        push      ebx                                           ;505.17
        push      esi                                           ;505.17
        push      DWORD PTR [480+esp]                           ;505.17
        call      _for_alloc_allocatable                        ;505.17
                                ; LOE eax
.B10.1342:                      ; Preds .B10.251
        add       esp, 28                                       ;505.17
        mov       ebx, eax                                      ;505.17
                                ; LOE ebx
.B10.252:                       ; Preds .B10.1342
        test      ebx, ebx                                      ;505.17
        jne       .B10.255      ; Prob 50%                      ;505.17
                                ; LOE ebx
.B10.253:                       ; Preds .B10.252
        movss     xmm3, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIMPERIOD] ;505.62
        mov       edx, 1                                        ;505.17
        movss     xmm0, DWORD PTR [_2il0floatpacket.25]         ;505.62
        xor       eax, eax                                      ;505.17
        andps     xmm0, xmm3                                    ;505.62
        pxor      xmm3, xmm0                                    ;505.62
        movss     xmm1, DWORD PTR [_2il0floatpacket.26]         ;505.62
        movaps    xmm2, xmm3                                    ;505.62
        movss     xmm4, DWORD PTR [_2il0floatpacket.27]         ;505.62
        cmpltss   xmm2, xmm1                                    ;505.62
        andps     xmm1, xmm2                                    ;505.62
        movaps    xmm2, xmm3                                    ;505.62
        movaps    xmm6, xmm4                                    ;505.62
        addss     xmm2, xmm1                                    ;505.62
        addss     xmm6, xmm4                                    ;505.62
        subss     xmm2, xmm1                                    ;505.62
        movaps    xmm7, xmm2                                    ;505.62
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;505.17
        subss     xmm7, xmm3                                    ;505.62
        movaps    xmm5, xmm7                                    ;505.62
        neg       ecx                                           ;505.17
        add       ecx, DWORD PTR [508+esp]                      ;505.17
        cmpnless  xmm5, xmm4                                    ;505.62
        imul      edi, ecx, 900                                 ;505.17
        cmpless   xmm7, DWORD PTR [_2il0floatpacket.28]         ;505.62
        andps     xmm5, xmm6                                    ;505.62
        andps     xmm7, xmm6                                    ;505.62
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;505.17
        mov       ecx, 4                                        ;505.17
        subss     xmm2, xmm5                                    ;505.62
        mov       DWORD PTR [580+esi+edi], edx                  ;505.17
        addss     xmm2, xmm7                                    ;505.62
        orps      xmm2, xmm0                                    ;505.62
        mov       DWORD PTR [596+esi+edi], edx                  ;505.17
        cvtss2si  edx, xmm2                                     ;505.17
        test      edx, edx                                      ;505.17
        mov       DWORD PTR [572+esi+edi], eax                  ;505.17
        cmovl     edx, eax                                      ;505.17
        lea       eax, DWORD PTR [312+esp]                      ;505.17
        push      ecx                                           ;505.17
        push      edx                                           ;505.17
        push      2                                             ;505.17
        push      eax                                           ;505.17
        mov       DWORD PTR [576+esi+edi], 133                  ;505.17
        mov       DWORD PTR [568+esi+edi], ecx                  ;505.17
        mov       DWORD PTR [588+esi+edi], edx                  ;505.17
        mov       DWORD PTR [592+esi+edi], ecx                  ;505.17
        call      _for_check_mult_overflow                      ;505.17
                                ; LOE ebx
.B10.1343:                      ; Preds .B10.253
        add       esp, 16                                       ;505.17
                                ; LOE ebx
.B10.254:                       ; Preds .B10.1343
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], ebx ;505.17
        jmp       .B10.257      ; Prob 100%                     ;505.17
                                ; LOE
.B10.255:                       ; Preds .B10.252
        mov       DWORD PTR [160+esp], 0                        ;507.21
        lea       eax, DWORD PTR [400+esp]                      ;507.21
        mov       DWORD PTR [400+esp], 45                       ;507.21
        mov       DWORD PTR [404+esp], OFFSET FLAT: __STRLITPACK_50 ;507.21
        push      32                                            ;507.21
        push      eax                                           ;507.21
        push      OFFSET FLAT: __STRLITPACK_173.0.10            ;507.21
        push      -2088435968                                   ;507.21
        push      911                                           ;507.21
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], ebx ;505.17
        lea       edx, DWORD PTR [180+esp]                      ;507.21
        push      edx                                           ;507.21
        call      _for_write_seq_lis                            ;507.21
                                ; LOE
.B10.256:                       ; Preds .B10.255
        push      32                                            ;508.18
        xor       eax, eax                                      ;508.18
        push      eax                                           ;508.18
        push      eax                                           ;508.18
        push      -2088435968                                   ;508.18
        push      eax                                           ;508.18
        push      OFFSET FLAT: __STRLITPACK_174                 ;508.18
        call      _for_stop_core                                ;508.18
                                ; LOE
.B10.1344:                      ; Preds .B10.256
        add       esp, 48                                       ;508.18
                                ; LOE
.B10.257:                       ; Preds .B10.1344 .B10.254
        mov       eax, DWORD PTR [508+esp]                      ;510.17
        sub       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;510.17
        imul      ebx, eax, 900                                 ;510.17
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;510.17
        mov       esi, DWORD PTR [588+ecx+ebx]                  ;510.17
        test      esi, esi                                      ;510.17
        jle       .B10.260      ; Prob 50%                      ;510.17
                                ; LOE ecx ebx esi
.B10.258:                       ; Preds .B10.257
        cmp       esi, 24                                       ;510.17
        jle       .B10.919      ; Prob 0%                       ;510.17
                                ; LOE ecx ebx esi
.B10.259:                       ; Preds .B10.258
        shl       esi, 2                                        ;510.17
        push      esi                                           ;510.17
        push      0                                             ;510.17
        push      DWORD PTR [564+ecx+ebx]                       ;510.17
        call      __intel_fast_memset                           ;510.17
                                ; LOE
.B10.1345:                      ; Preds .B10.259
        add       esp, 12                                       ;510.17
                                ; LOE
.B10.260:                       ; Preds .B10.925 .B10.1345 .B10.257 .B10.923
        movss     xmm3, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIMPERIOD] ;512.62
        xor       eax, eax                                      ;512.17
        movss     xmm0, DWORD PTR [_2il0floatpacket.25]         ;512.62
        andps     xmm0, xmm3                                    ;512.62
        pxor      xmm3, xmm0                                    ;512.62
        movss     xmm1, DWORD PTR [_2il0floatpacket.26]         ;512.62
        movaps    xmm2, xmm3                                    ;512.62
        movss     xmm4, DWORD PTR [_2il0floatpacket.27]         ;512.62
        cmpltss   xmm2, xmm1                                    ;512.62
        andps     xmm1, xmm2                                    ;512.62
        movaps    xmm2, xmm3                                    ;512.62
        movaps    xmm6, xmm4                                    ;512.62
        addss     xmm2, xmm1                                    ;512.62
        addss     xmm6, xmm4                                    ;512.62
        subss     xmm2, xmm1                                    ;512.62
        movaps    xmm7, xmm2                                    ;512.62
        push      4                                             ;512.17
        subss     xmm7, xmm3                                    ;512.62
        movaps    xmm5, xmm7                                    ;512.62
        cmpless   xmm7, DWORD PTR [_2il0floatpacket.28]         ;512.62
        cmpnless  xmm5, xmm4                                    ;512.62
        andps     xmm5, xmm6                                    ;512.62
        andps     xmm7, xmm6                                    ;512.62
        subss     xmm2, xmm5                                    ;512.62
        addss     xmm2, xmm7                                    ;512.62
        orps      xmm2, xmm0                                    ;512.62
        cvtss2si  edx, xmm2                                     ;512.17
        test      edx, edx                                      ;512.17
        cmovl     edx, eax                                      ;512.17
        push      edx                                           ;512.17
        push      2                                             ;512.17
        lea       ecx, DWORD PTR [472+esp]                      ;512.17
        push      ecx                                           ;512.17
        call      _for_check_mult_overflow                      ;512.17
                                ; LOE eax
.B10.261:                       ; Preds .B10.260
        imul      edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32], 900 ;512.17
        and       eax, 1                                        ;512.17
        imul      ecx, DWORD PTR [524+esp], 900                 ;512.26
        add       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;512.17
        lea       esi, DWORD PTR [-600+edx]                     ;512.17
        neg       edx                                           ;512.17
        neg       esi                                           ;512.17
        shl       eax, 4                                        ;512.17
        add       esi, ecx                                      ;512.17
        mov       ebx, DWORD PTR [612+edx+ecx]                  ;512.17
        and       ebx, 1                                        ;512.17
        add       ebx, ebx                                      ;512.17
        or        ebx, 1                                        ;512.17
        or        ebx, eax                                      ;512.17
        or        ebx, 262144                                   ;512.17
        push      ebx                                           ;512.17
        push      esi                                           ;512.17
        push      DWORD PTR [484+esp]                           ;512.17
        call      _for_alloc_allocatable                        ;512.17
                                ; LOE eax
.B10.1347:                      ; Preds .B10.261
        add       esp, 28                                       ;512.17
        mov       ebx, eax                                      ;512.17
                                ; LOE ebx
.B10.262:                       ; Preds .B10.1347
        test      ebx, ebx                                      ;512.17
        jne       .B10.265      ; Prob 50%                      ;512.17
                                ; LOE ebx
.B10.263:                       ; Preds .B10.262
        movss     xmm3, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIMPERIOD] ;512.62
        mov       edx, 1                                        ;512.17
        movss     xmm0, DWORD PTR [_2il0floatpacket.25]         ;512.62
        xor       eax, eax                                      ;512.17
        andps     xmm0, xmm3                                    ;512.62
        pxor      xmm3, xmm0                                    ;512.62
        movss     xmm1, DWORD PTR [_2il0floatpacket.26]         ;512.62
        movaps    xmm2, xmm3                                    ;512.62
        movss     xmm4, DWORD PTR [_2il0floatpacket.27]         ;512.62
        cmpltss   xmm2, xmm1                                    ;512.62
        andps     xmm1, xmm2                                    ;512.62
        movaps    xmm2, xmm3                                    ;512.62
        movaps    xmm6, xmm4                                    ;512.62
        addss     xmm2, xmm1                                    ;512.62
        addss     xmm6, xmm4                                    ;512.62
        subss     xmm2, xmm1                                    ;512.62
        movaps    xmm7, xmm2                                    ;512.62
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;512.17
        subss     xmm7, xmm3                                    ;512.62
        movaps    xmm5, xmm7                                    ;512.62
        neg       ecx                                           ;512.17
        add       ecx, DWORD PTR [508+esp]                      ;512.17
        cmpnless  xmm5, xmm4                                    ;512.62
        imul      edi, ecx, 900                                 ;512.17
        cmpless   xmm7, DWORD PTR [_2il0floatpacket.28]         ;512.62
        andps     xmm5, xmm6                                    ;512.62
        andps     xmm7, xmm6                                    ;512.62
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;512.17
        mov       ecx, 4                                        ;512.17
        subss     xmm2, xmm5                                    ;512.62
        mov       DWORD PTR [616+esi+edi], edx                  ;512.17
        addss     xmm2, xmm7                                    ;512.62
        orps      xmm2, xmm0                                    ;512.62
        mov       DWORD PTR [632+esi+edi], edx                  ;512.17
        cvtss2si  edx, xmm2                                     ;512.17
        test      edx, edx                                      ;512.17
        mov       DWORD PTR [608+esi+edi], eax                  ;512.17
        cmovl     edx, eax                                      ;512.17
        lea       eax, DWORD PTR [316+esp]                      ;512.17
        push      ecx                                           ;512.17
        push      edx                                           ;512.17
        push      2                                             ;512.17
        push      eax                                           ;512.17
        mov       DWORD PTR [612+esi+edi], 133                  ;512.17
        mov       DWORD PTR [604+esi+edi], ecx                  ;512.17
        mov       DWORD PTR [624+esi+edi], edx                  ;512.17
        mov       DWORD PTR [628+esi+edi], ecx                  ;512.17
        call      _for_check_mult_overflow                      ;512.17
                                ; LOE ebx
.B10.1348:                      ; Preds .B10.263
        add       esp, 16                                       ;512.17
                                ; LOE ebx
.B10.264:                       ; Preds .B10.1348
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], ebx ;512.17
        jmp       .B10.267      ; Prob 100%                     ;512.17
                                ; LOE
.B10.265:                       ; Preds .B10.262
        mov       DWORD PTR [160+esp], 0                        ;514.21
        lea       eax, DWORD PTR [408+esp]                      ;514.21
        mov       DWORD PTR [408+esp], 45                       ;514.21
        mov       DWORD PTR [412+esp], OFFSET FLAT: __STRLITPACK_48 ;514.21
        push      32                                            ;514.21
        push      eax                                           ;514.21
        push      OFFSET FLAT: __STRLITPACK_175.0.10            ;514.21
        push      -2088435968                                   ;514.21
        push      911                                           ;514.21
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], ebx ;512.17
        lea       edx, DWORD PTR [180+esp]                      ;514.21
        push      edx                                           ;514.21
        call      _for_write_seq_lis                            ;514.21
                                ; LOE
.B10.266:                       ; Preds .B10.265
        push      32                                            ;515.18
        xor       eax, eax                                      ;515.18
        push      eax                                           ;515.18
        push      eax                                           ;515.18
        push      -2088435968                                   ;515.18
        push      eax                                           ;515.18
        push      OFFSET FLAT: __STRLITPACK_176                 ;515.18
        call      _for_stop_core                                ;515.18
                                ; LOE
.B10.1349:                      ; Preds .B10.266
        add       esp, 48                                       ;515.18
                                ; LOE
.B10.267:                       ; Preds .B10.1349 .B10.264
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;517.17
        mov       DWORD PTR [216+esp], esi                      ;517.17
        imul      esi, esi, 900                                 ;517.17
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;517.17
        mov       edx, ebx                                      ;517.17
        mov       eax, DWORD PTR [508+esp]                      ;517.17
        sub       edx, esi                                      ;517.17
        imul      edi, eax, 900                                 ;517.17
        mov       ecx, DWORD PTR [624+edx+edi]                  ;517.17
        test      ecx, ecx                                      ;517.17
        mov       DWORD PTR [152+esp], ebx                      ;517.17
        mov       DWORD PTR [116+esp], eax                      ;517.17
        jle       .B10.271      ; Prob 50%                      ;517.17
                                ; LOE edx ecx ebx esi edi bl bh
.B10.268:                       ; Preds .B10.267
        cmp       ecx, 24                                       ;517.17
        jle       .B10.928      ; Prob 0%                       ;517.17
                                ; LOE edx ecx ebx esi edi bl bh
.B10.269:                       ; Preds .B10.268
        shl       ecx, 2                                        ;517.17
        push      ecx                                           ;517.17
        push      0                                             ;517.17
        push      DWORD PTR [600+edx+edi]                       ;517.17
        call      __intel_fast_memset                           ;517.17
                                ; LOE
.B10.1350:                      ; Preds .B10.269
        add       esp, 12                                       ;517.17
                                ; LOE
.B10.270:                       ; Preds .B10.1350
        imul      esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32], 900 ;
        imul      edi, DWORD PTR [508+esp], 900                 ;519.26
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;519.17
                                ; LOE ebx esi edi
.B10.271:                       ; Preds .B10.934 .B10.270 .B10.932 .B10.267
        mov       eax, DWORD PTR [216+esp]                      ;519.17
        add       ebx, edi                                      ;519.17
        neg       eax                                           ;519.17
        add       esi, -708                                     ;519.17
        add       eax, DWORD PTR [116+esp]                      ;519.17
        sub       ebx, esi                                      ;519.17
        imul      ecx, eax, 900                                 ;519.17
        mov       edx, DWORD PTR [152+esp]                      ;519.17
        mov       esi, DWORD PTR [720+edx+ecx]                  ;519.17
        and       esi, 1                                        ;519.17
        add       esi, esi                                      ;519.17
        or        esi, 262145                                   ;519.17
        push      esi                                           ;519.17
        push      ebx                                           ;519.17
        push      24                                            ;519.17
        call      _for_alloc_allocatable                        ;519.17
                                ; LOE eax
.B10.1351:                      ; Preds .B10.271
        add       esp, 12                                       ;519.17
                                ; LOE eax
.B10.272:                       ; Preds .B10.1351
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;519.17
        test      eax, eax                                      ;519.17
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;519.17
        mov       esi, DWORD PTR [508+esp]                      ;519.26
        jne       .B10.274      ; Prob 50%                      ;519.17
                                ; LOE eax ebx esi edi
.B10.273:                       ; Preds .B10.272
        neg       ebx                                           ;519.17
        mov       edx, 1                                        ;519.17
        add       ebx, esi                                      ;519.17
        imul      ecx, ebx, 900                                 ;519.17
        mov       ebx, 2                                        ;519.17
        add       ecx, edi                                      ;519.17
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;519.17
        mov       DWORD PTR [720+ecx], 133                      ;519.17
        mov       DWORD PTR [712+ecx], ebx                      ;519.17
        mov       DWORD PTR [724+ecx], edx                      ;519.17
        mov       DWORD PTR [716+ecx], 0                        ;519.17
        mov       DWORD PTR [740+ecx], edx                      ;519.17
        mov       edx, 12                                       ;519.17
        mov       DWORD PTR [732+ecx], 12                       ;519.17
        mov       DWORD PTR [736+ecx], ebx                      ;519.17
        jmp       .B10.277      ; Prob 100%                     ;519.17
                                ; LOE edx ecx
.B10.274:                       ; Preds .B10.272
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;519.17
        lea       eax, DWORD PTR [264+esp]                      ;521.21
        mov       DWORD PTR [160+esp], 0                        ;521.21
        mov       DWORD PTR [264+esp], 44                       ;521.21
        mov       DWORD PTR [268+esp], OFFSET FLAT: __STRLITPACK_46 ;521.21
        push      32                                            ;521.21
        push      eax                                           ;521.21
        push      OFFSET FLAT: __STRLITPACK_177.0.10            ;521.21
        push      -2088435968                                   ;521.21
        push      911                                           ;521.21
        lea       edx, DWORD PTR [180+esp]                      ;521.21
        push      edx                                           ;521.21
        call      _for_write_seq_lis                            ;521.21
                                ; LOE ebx esi edi
.B10.275:                       ; Preds .B10.274
        push      32                                            ;522.18
        xor       eax, eax                                      ;522.18
        push      eax                                           ;522.18
        push      eax                                           ;522.18
        push      -2088435968                                   ;522.18
        push      eax                                           ;522.18
        push      OFFSET FLAT: __STRLITPACK_178                 ;522.18
        call      _for_stop_core                                ;522.18
                                ; LOE ebx esi edi
.B10.1352:                      ; Preds .B10.275
        add       esp, 48                                       ;522.18
                                ; LOE ebx esi edi
.B10.276:                       ; Preds .B10.1352
        neg       ebx                                           ;
        add       ebx, esi                                      ;
        imul      ecx, ebx, 900                                 ;
        add       ecx, edi                                      ;
        mov       edx, DWORD PTR [732+ecx]                      ;524.17
        test      edx, edx                                      ;524.17
        jle       .B10.279      ; Prob 50%                      ;524.17
                                ; LOE edx ecx
.B10.277:                       ; Preds .B10.273 .B10.276
        cmp       edx, 48                                       ;524.17
        jle       .B10.937      ; Prob 0%                       ;524.17
                                ; LOE edx ecx
.B10.278:                       ; Preds .B10.277
        add       edx, edx                                      ;524.17
        push      edx                                           ;524.17
        push      0                                             ;524.17
        push      DWORD PTR [708+ecx]                           ;524.17
        call      __intel_fast_memset                           ;524.17
                                ; LOE
.B10.1353:                      ; Preds .B10.278
        add       esp, 12                                       ;524.17
                                ; LOE
.B10.279:                       ; Preds .B10.943 .B10.1353 .B10.276 .B10.941
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_AGGINT] ;526.17
        xor       eax, eax                                      ;526.17
        test      edx, edx                                      ;526.17
        push      12                                            ;526.17
        cmovle    edx, eax                                      ;526.17
        push      edx                                           ;526.17
        push      2                                             ;526.17
        lea       ecx, DWORD PTR [476+esp]                      ;526.17
        push      ecx                                           ;526.17
        call      _for_check_mult_overflow                      ;526.17
                                ; LOE eax
.B10.280:                       ; Preds .B10.279
        imul      edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32], 900 ;526.17
        and       eax, 1                                        ;526.17
        imul      ecx, DWORD PTR [524+esp], 900                 ;526.26
        add       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;526.17
        lea       esi, DWORD PTR [-808+edx]                     ;526.17
        neg       edx                                           ;526.17
        neg       esi                                           ;526.17
        shl       eax, 4                                        ;526.17
        add       esi, ecx                                      ;526.17
        mov       ebx, DWORD PTR [820+edx+ecx]                  ;526.17
        and       ebx, 1                                        ;526.17
        add       ebx, ebx                                      ;526.17
        or        ebx, 1                                        ;526.17
        or        ebx, eax                                      ;526.17
        or        ebx, 262144                                   ;526.17
        push      ebx                                           ;526.17
        push      esi                                           ;526.17
        push      DWORD PTR [488+esp]                           ;526.17
        call      _for_alloc_allocatable                        ;526.17
                                ; LOE eax
.B10.1355:                      ; Preds .B10.280
        add       esp, 28                                       ;526.17
        mov       ebx, eax                                      ;526.17
                                ; LOE ebx
.B10.281:                       ; Preds .B10.1355
        test      ebx, ebx                                      ;526.17
        jne       .B10.284      ; Prob 50%                      ;526.17
                                ; LOE ebx
.B10.282:                       ; Preds .B10.281
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;526.17
        xor       edx, edx                                      ;526.17
        neg       ecx                                           ;526.17
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_AGGINT] ;526.17
        test      esi, esi                                      ;526.17
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;526.17
        cmovle    esi, edx                                      ;526.17
        add       ecx, DWORD PTR [508+esp]                      ;526.17
        imul      edi, ecx, 900                                 ;526.17
        mov       ecx, 4                                        ;526.17
        mov       DWORD PTR [4+esp], ebx                        ;
        mov       ebx, 1                                        ;526.17
        mov       DWORD PTR [816+eax+edi], edx                  ;526.17
        lea       edx, DWORD PTR [esi*4]                        ;526.17
        mov       DWORD PTR [820+eax+edi], 133                  ;526.17
        mov       DWORD PTR [812+eax+edi], ecx                  ;526.17
        mov       DWORD PTR [824+eax+edi], 2                    ;526.17
        mov       DWORD PTR [840+eax+edi], ebx                  ;526.17
        mov       DWORD PTR [832+eax+edi], esi                  ;526.17
        mov       DWORD PTR [852+eax+edi], ebx                  ;526.17
        mov       DWORD PTR [844+eax+edi], 3                    ;526.17
        mov       DWORD PTR [836+eax+edi], ecx                  ;526.17
        mov       DWORD PTR [848+eax+edi], edx                  ;526.17
        lea       eax, DWORD PTR [320+esp]                      ;526.17
        push      12                                            ;526.17
        push      esi                                           ;526.17
        push      2                                             ;526.17
        push      eax                                           ;526.17
        mov       ebx, DWORD PTR [20+esp]                       ;526.17
        call      _for_check_mult_overflow                      ;526.17
                                ; LOE ebx bl bh
.B10.1356:                      ; Preds .B10.282
        add       esp, 16                                       ;526.17
                                ; LOE ebx bl bh
.B10.283:                       ; Preds .B10.1356
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], ebx ;526.17
        jmp       .B10.286      ; Prob 100%                     ;526.17
                                ; LOE
.B10.284:                       ; Preds .B10.281
        mov       DWORD PTR [160+esp], 0                        ;528.21
        lea       eax, DWORD PTR [416+esp]                      ;528.21
        mov       DWORD PTR [416+esp], 44                       ;528.21
        mov       DWORD PTR [420+esp], OFFSET FLAT: __STRLITPACK_44 ;528.21
        push      32                                            ;528.21
        push      eax                                           ;528.21
        push      OFFSET FLAT: __STRLITPACK_179.0.10            ;528.21
        push      -2088435968                                   ;528.21
        push      911                                           ;528.21
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], ebx ;526.17
        lea       edx, DWORD PTR [180+esp]                      ;528.21
        push      edx                                           ;528.21
        call      _for_write_seq_lis                            ;528.21
                                ; LOE
.B10.285:                       ; Preds .B10.284
        push      32                                            ;529.18
        xor       eax, eax                                      ;529.18
        push      eax                                           ;529.18
        push      eax                                           ;529.18
        push      -2088435968                                   ;529.18
        push      eax                                           ;529.18
        push      OFFSET FLAT: __STRLITPACK_180                 ;529.18
        call      _for_stop_core                                ;529.18
                                ; LOE
.B10.1357:                      ; Preds .B10.285
        add       esp, 48                                       ;529.18
                                ; LOE
.B10.286:                       ; Preds .B10.1357 .B10.283
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;531.17
        mov       ecx, edx                                      ;531.17
        neg       ecx                                           ;531.17
        mov       esi, DWORD PTR [508+esp]                      ;531.17
        add       ecx, esi                                      ;531.17
        imul      ecx, ecx, 900                                 ;531.17
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;531.17
        mov       eax, DWORD PTR [852+ebx+ecx]                  ;531.17
        mov       edi, DWORD PTR [844+ebx+ecx]                  ;531.17
        test      edi, edi                                      ;531.17
        mov       DWORD PTR [536+esp], eax                      ;531.17
        jle       .B10.293      ; Prob 10%                      ;531.17
                                ; LOE edx ecx ebx esi edi
.B10.287:                       ; Preds .B10.286
        xor       eax, eax                                      ;
        mov       DWORD PTR [532+esp], eax                      ;
        mov       DWORD PTR [512+esp], edi                      ;
        mov       DWORD PTR [516+esp], ecx                      ;
        mov       DWORD PTR [60+esp], esi                       ;
        mov       DWORD PTR [520+esp], ebx                      ;
        mov       DWORD PTR [64+esp], edx                       ;
                                ; LOE
.B10.288:                       ; Preds .B10.291 .B10.287
        mov       edx, DWORD PTR [520+esp]                      ;531.17
        mov       ecx, DWORD PTR [516+esp]                      ;531.17
        mov       eax, DWORD PTR [840+edx+ecx]                  ;531.17
        mov       DWORD PTR [528+esp], eax                      ;531.17
        mov       eax, DWORD PTR [832+edx+ecx]                  ;531.17
        test      eax, eax                                      ;531.17
        jle       .B10.291      ; Prob 50%                      ;531.17
                                ; LOE eax edx ecx dl cl dh ch
.B10.289:                       ; Preds .B10.288
        cmp       eax, 24                                       ;531.17
        jle       .B10.946      ; Prob 0%                       ;531.17
                                ; LOE eax edx ecx dl cl dh ch
.B10.290:                       ; Preds .B10.289
        shl       eax, 2                                        ;531.17
        mov       ebx, DWORD PTR [852+edx+ecx]                  ;531.17
        neg       ebx                                           ;531.17
        add       ebx, DWORD PTR [536+esp]                      ;531.17
        imul      ebx, DWORD PTR [848+edx+ecx]                  ;531.17
        push      eax                                           ;531.17
        add       ebx, DWORD PTR [808+edx+ecx]                  ;531.17
        push      0                                             ;531.17
        push      ebx                                           ;531.17
        call      __intel_fast_memset                           ;531.17
                                ; LOE
.B10.1358:                      ; Preds .B10.290
        add       esp, 12                                       ;531.17
                                ; LOE
.B10.291:                       ; Preds .B10.952 .B10.1358 .B10.288 .B10.950
        mov       eax, DWORD PTR [532+esp]                      ;531.17
        inc       eax                                           ;531.17
        inc       DWORD PTR [536+esp]                           ;531.17
        mov       DWORD PTR [532+esp], eax                      ;531.17
        cmp       eax, DWORD PTR [512+esp]                      ;531.17
        jb        .B10.288      ; Prob 82%                      ;531.17
                                ; LOE
.B10.292:                       ; Preds .B10.291
        mov       esi, DWORD PTR [60+esp]                       ;
        mov       edx, DWORD PTR [64+esp]                       ;
                                ; LOE edx esi
.B10.293:                       ; Preds .B10.292 .B10.286
        inc       esi                                           ;532.13
        mov       DWORD PTR [508+esp], esi                      ;532.13
        cmp       esi, DWORD PTR [72+esp]                       ;532.13
        jle       .B10.94       ; Prob 82%                      ;532.13
                                ; LOE edx
.B10.294:                       ; Preds .B10.293
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+24] ;534.13
                                ; LOE edx ecx
.B10.295:                       ; Preds .B10.294 .B10.92
        test      ecx, ecx                                      ;534.13
        jle       .B10.386      ; Prob 50%                      ;534.13
                                ; LOE edx ecx
.B10.296:                       ; Preds .B10.295
        mov       esi, ecx                                      ;534.13
        shr       esi, 31                                       ;534.13
        add       esi, ecx                                      ;534.13
        sar       esi, 1                                        ;534.13
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;534.13
        test      esi, esi                                      ;534.13
        jbe       .B10.969      ; Prob 10%                      ;534.13
                                ; LOE edx ecx ebx esi
.B10.297:                       ; Preds .B10.296
        xor       eax, eax                                      ;
        mov       DWORD PTR [64+esp], edx                       ;
        xor       edi, edi                                      ;
        xor       edx, edx                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.298:                       ; Preds .B10.298 .B10.297
        inc       eax                                           ;534.13
        mov       DWORD PTR [336+edi+ebx], edx                  ;534.13
        mov       DWORD PTR [1236+edi+ebx], edx                 ;534.13
        add       edi, 1800                                     ;534.13
        cmp       eax, esi                                      ;534.13
        jb        .B10.298      ; Prob 63%                      ;534.13
                                ; LOE eax edx ecx ebx esi edi
.B10.299:                       ; Preds .B10.298
        mov       edx, DWORD PTR [64+esp]                       ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;534.13
                                ; LOE eax edx ecx ebx esi
.B10.300:                       ; Preds .B10.299 .B10.969
        lea       edi, DWORD PTR [-1+eax]                       ;534.13
        cmp       ecx, edi                                      ;534.13
        jbe       .B10.302      ; Prob 10%                      ;534.13
                                ; LOE eax edx ecx ebx esi
.B10.301:                       ; Preds .B10.300
        mov       edi, edx                                      ;534.13
        add       eax, edx                                      ;534.13
        neg       edi                                           ;534.13
        add       edi, eax                                      ;534.13
        imul      eax, edi, 900                                 ;534.13
        mov       DWORD PTR [-564+ebx+eax], 0                   ;534.13
                                ; LOE edx ecx ebx esi
.B10.302:                       ; Preds .B10.300 .B10.301
        test      esi, esi                                      ;535.13
        jbe       .B10.968      ; Prob 10%                      ;535.13
                                ; LOE edx ecx ebx esi
.B10.303:                       ; Preds .B10.302
        xor       eax, eax                                      ;
        mov       DWORD PTR [64+esp], edx                       ;
        xor       edi, edi                                      ;
        xor       edx, edx                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.304:                       ; Preds .B10.304 .B10.303
        inc       eax                                           ;535.13
        mov       DWORD PTR [340+edi+ebx], edx                  ;535.13
        mov       DWORD PTR [1240+edi+ebx], edx                 ;535.13
        add       edi, 1800                                     ;535.13
        cmp       eax, esi                                      ;535.13
        jb        .B10.304      ; Prob 63%                      ;535.13
                                ; LOE eax edx ecx ebx esi edi
.B10.305:                       ; Preds .B10.304
        mov       edx, DWORD PTR [64+esp]                       ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;535.13
                                ; LOE eax edx ecx ebx esi
.B10.306:                       ; Preds .B10.305 .B10.968
        lea       edi, DWORD PTR [-1+eax]                       ;535.13
        cmp       ecx, edi                                      ;535.13
        jbe       .B10.308      ; Prob 10%                      ;535.13
                                ; LOE eax edx ecx ebx esi
.B10.307:                       ; Preds .B10.306
        mov       edi, edx                                      ;535.13
        add       eax, edx                                      ;535.13
        neg       edi                                           ;535.13
        add       edi, eax                                      ;535.13
        imul      eax, edi, 900                                 ;535.13
        mov       DWORD PTR [-560+ebx+eax], 0                   ;535.13
                                ; LOE edx ecx ebx esi
.B10.308:                       ; Preds .B10.306 .B10.307
        test      esi, esi                                      ;536.13
        jbe       .B10.967      ; Prob 10%                      ;536.13
                                ; LOE edx ecx ebx esi
.B10.309:                       ; Preds .B10.308
        xor       eax, eax                                      ;
        mov       DWORD PTR [64+esp], edx                       ;
        xor       edi, edi                                      ;
        xor       edx, edx                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.310:                       ; Preds .B10.310 .B10.309
        inc       eax                                           ;536.13
        mov       DWORD PTR [344+edi+ebx], edx                  ;536.13
        mov       DWORD PTR [1244+edi+ebx], edx                 ;536.13
        add       edi, 1800                                     ;536.13
        cmp       eax, esi                                      ;536.13
        jb        .B10.310      ; Prob 63%                      ;536.13
                                ; LOE eax edx ecx ebx esi edi
.B10.311:                       ; Preds .B10.310
        mov       edx, DWORD PTR [64+esp]                       ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;536.13
                                ; LOE eax edx ecx ebx esi
.B10.312:                       ; Preds .B10.311 .B10.967
        lea       edi, DWORD PTR [-1+eax]                       ;536.13
        cmp       ecx, edi                                      ;536.13
        jbe       .B10.314      ; Prob 10%                      ;536.13
                                ; LOE eax edx ecx ebx esi
.B10.313:                       ; Preds .B10.312
        mov       edi, edx                                      ;536.13
        add       eax, edx                                      ;536.13
        neg       edi                                           ;536.13
        add       edi, eax                                      ;536.13
        imul      eax, edi, 900                                 ;536.13
        mov       DWORD PTR [-556+ebx+eax], 0                   ;536.13
                                ; LOE edx ecx ebx esi
.B10.314:                       ; Preds .B10.312 .B10.313
        test      esi, esi                                      ;537.13
        jbe       .B10.966      ; Prob 10%                      ;537.13
                                ; LOE edx ecx ebx esi
.B10.315:                       ; Preds .B10.314
        xor       eax, eax                                      ;
        mov       DWORD PTR [64+esp], edx                       ;
        xor       edi, edi                                      ;
        xor       edx, edx                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.316:                       ; Preds .B10.316 .B10.315
        inc       eax                                           ;537.13
        mov       DWORD PTR [348+edi+ebx], edx                  ;537.13
        mov       DWORD PTR [1248+edi+ebx], edx                 ;537.13
        add       edi, 1800                                     ;537.13
        cmp       eax, esi                                      ;537.13
        jb        .B10.316      ; Prob 63%                      ;537.13
                                ; LOE eax edx ecx ebx esi edi
.B10.317:                       ; Preds .B10.316
        mov       edx, DWORD PTR [64+esp]                       ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;537.13
                                ; LOE eax edx ecx ebx esi
.B10.318:                       ; Preds .B10.317 .B10.966
        lea       edi, DWORD PTR [-1+eax]                       ;537.13
        cmp       ecx, edi                                      ;537.13
        jbe       .B10.320      ; Prob 10%                      ;537.13
                                ; LOE eax edx ecx ebx esi
.B10.319:                       ; Preds .B10.318
        mov       edi, edx                                      ;537.13
        add       eax, edx                                      ;537.13
        neg       edi                                           ;537.13
        add       edi, eax                                      ;537.13
        imul      eax, edi, 900                                 ;537.13
        mov       DWORD PTR [-552+ebx+eax], 0                   ;537.13
                                ; LOE edx ecx ebx esi
.B10.320:                       ; Preds .B10.318 .B10.319
        test      esi, esi                                      ;538.13
        jbe       .B10.965      ; Prob 10%                      ;538.13
                                ; LOE edx ecx ebx esi
.B10.321:                       ; Preds .B10.320
        xor       eax, eax                                      ;
        mov       DWORD PTR [64+esp], edx                       ;
        xor       edi, edi                                      ;
        xor       edx, edx                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.322:                       ; Preds .B10.322 .B10.321
        inc       eax                                           ;538.13
        mov       DWORD PTR [352+edi+ebx], edx                  ;538.13
        mov       DWORD PTR [1252+edi+ebx], edx                 ;538.13
        add       edi, 1800                                     ;538.13
        cmp       eax, esi                                      ;538.13
        jb        .B10.322      ; Prob 63%                      ;538.13
                                ; LOE eax edx ecx ebx esi edi
.B10.323:                       ; Preds .B10.322
        mov       edx, DWORD PTR [64+esp]                       ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;538.13
                                ; LOE eax edx ecx ebx esi
.B10.324:                       ; Preds .B10.323 .B10.965
        lea       edi, DWORD PTR [-1+eax]                       ;538.13
        cmp       ecx, edi                                      ;538.13
        jbe       .B10.326      ; Prob 10%                      ;538.13
                                ; LOE eax edx ecx ebx esi
.B10.325:                       ; Preds .B10.324
        mov       edi, edx                                      ;538.13
        add       eax, edx                                      ;538.13
        neg       edi                                           ;538.13
        add       edi, eax                                      ;538.13
        imul      eax, edi, 900                                 ;538.13
        mov       DWORD PTR [-548+ebx+eax], 0                   ;538.13
                                ; LOE edx ecx ebx esi
.B10.326:                       ; Preds .B10.324 .B10.325
        test      esi, esi                                      ;539.13
        jbe       .B10.964      ; Prob 10%                      ;539.13
                                ; LOE edx ecx ebx esi
.B10.327:                       ; Preds .B10.326
        xor       eax, eax                                      ;
        mov       DWORD PTR [64+esp], edx                       ;
        xor       edi, edi                                      ;
        xor       edx, edx                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.328:                       ; Preds .B10.328 .B10.327
        inc       eax                                           ;539.13
        mov       DWORD PTR [392+edi+ebx], edx                  ;539.13
        mov       DWORD PTR [1292+edi+ebx], edx                 ;539.13
        add       edi, 1800                                     ;539.13
        cmp       eax, esi                                      ;539.13
        jb        .B10.328      ; Prob 63%                      ;539.13
                                ; LOE eax edx ecx ebx esi edi
.B10.329:                       ; Preds .B10.328
        mov       edx, DWORD PTR [64+esp]                       ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;539.13
                                ; LOE eax edx ecx ebx esi
.B10.330:                       ; Preds .B10.329 .B10.964
        lea       edi, DWORD PTR [-1+eax]                       ;539.13
        cmp       ecx, edi                                      ;539.13
        jbe       .B10.332      ; Prob 10%                      ;539.13
                                ; LOE eax edx ecx ebx esi
.B10.331:                       ; Preds .B10.330
        mov       edi, edx                                      ;539.13
        add       eax, edx                                      ;539.13
        neg       edi                                           ;539.13
        add       edi, eax                                      ;539.13
        imul      eax, edi, 900                                 ;539.13
        mov       DWORD PTR [-508+ebx+eax], 0                   ;539.13
                                ; LOE edx ecx ebx esi
.B10.332:                       ; Preds .B10.330 .B10.331
        test      esi, esi                                      ;540.13
        jbe       .B10.963      ; Prob 10%                      ;540.13
                                ; LOE edx ecx ebx esi
.B10.333:                       ; Preds .B10.332
        xor       eax, eax                                      ;
        mov       DWORD PTR [64+esp], edx                       ;
        xor       edi, edi                                      ;
        xor       edx, edx                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.334:                       ; Preds .B10.334 .B10.333
        inc       eax                                           ;540.13
        mov       DWORD PTR [396+edi+ebx], edx                  ;540.13
        mov       DWORD PTR [1296+edi+ebx], edx                 ;540.13
        add       edi, 1800                                     ;540.13
        cmp       eax, esi                                      ;540.13
        jb        .B10.334      ; Prob 63%                      ;540.13
                                ; LOE eax edx ecx ebx esi edi
.B10.335:                       ; Preds .B10.334
        mov       edx, DWORD PTR [64+esp]                       ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;540.13
                                ; LOE eax edx ecx ebx esi
.B10.336:                       ; Preds .B10.335 .B10.963
        lea       edi, DWORD PTR [-1+eax]                       ;540.13
        cmp       ecx, edi                                      ;540.13
        jbe       .B10.338      ; Prob 10%                      ;540.13
                                ; LOE eax edx ecx ebx esi
.B10.337:                       ; Preds .B10.336
        mov       edi, edx                                      ;540.13
        add       eax, edx                                      ;540.13
        neg       edi                                           ;540.13
        add       edi, eax                                      ;540.13
        imul      eax, edi, 900                                 ;540.13
        mov       DWORD PTR [-504+ebx+eax], 0                   ;540.13
                                ; LOE edx ecx ebx esi
.B10.338:                       ; Preds .B10.336 .B10.337
        test      esi, esi                                      ;541.13
        jbe       .B10.962      ; Prob 10%                      ;541.13
                                ; LOE edx ecx ebx esi
.B10.339:                       ; Preds .B10.338
        xor       eax, eax                                      ;
        mov       DWORD PTR [64+esp], edx                       ;
        xor       edi, edi                                      ;
        xor       edx, edx                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.340:                       ; Preds .B10.340 .B10.339
        inc       eax                                           ;541.13
        mov       DWORD PTR [400+edi+ebx], edx                  ;541.13
        mov       DWORD PTR [1300+edi+ebx], edx                 ;541.13
        add       edi, 1800                                     ;541.13
        cmp       eax, esi                                      ;541.13
        jb        .B10.340      ; Prob 63%                      ;541.13
                                ; LOE eax edx ecx ebx esi edi
.B10.341:                       ; Preds .B10.340
        mov       edx, DWORD PTR [64+esp]                       ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;541.13
                                ; LOE eax edx ecx ebx esi
.B10.342:                       ; Preds .B10.341 .B10.962
        lea       edi, DWORD PTR [-1+eax]                       ;541.13
        cmp       ecx, edi                                      ;541.13
        jbe       .B10.344      ; Prob 10%                      ;541.13
                                ; LOE eax edx ecx ebx esi
.B10.343:                       ; Preds .B10.342
        mov       edi, edx                                      ;541.13
        add       eax, edx                                      ;541.13
        neg       edi                                           ;541.13
        add       edi, eax                                      ;541.13
        imul      eax, edi, 900                                 ;541.13
        mov       DWORD PTR [-500+ebx+eax], 0                   ;541.13
                                ; LOE edx ecx ebx esi
.B10.344:                       ; Preds .B10.342 .B10.343
        test      esi, esi                                      ;542.13
        jbe       .B10.961      ; Prob 10%                      ;542.13
                                ; LOE edx ecx ebx esi
.B10.345:                       ; Preds .B10.344
        xor       eax, eax                                      ;
        mov       DWORD PTR [64+esp], edx                       ;
        xor       edi, edi                                      ;
        xor       edx, edx                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.346:                       ; Preds .B10.346 .B10.345
        inc       eax                                           ;542.13
        mov       DWORD PTR [404+edi+ebx], edx                  ;542.13
        mov       DWORD PTR [1304+edi+ebx], edx                 ;542.13
        add       edi, 1800                                     ;542.13
        cmp       eax, esi                                      ;542.13
        jb        .B10.346      ; Prob 63%                      ;542.13
                                ; LOE eax edx ecx ebx esi edi
.B10.347:                       ; Preds .B10.346
        mov       edx, DWORD PTR [64+esp]                       ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;542.13
                                ; LOE eax edx ecx ebx esi
.B10.348:                       ; Preds .B10.347 .B10.961
        lea       edi, DWORD PTR [-1+eax]                       ;542.13
        cmp       ecx, edi                                      ;542.13
        jbe       .B10.350      ; Prob 10%                      ;542.13
                                ; LOE eax edx ecx ebx esi
.B10.349:                       ; Preds .B10.348
        mov       edi, edx                                      ;542.13
        add       eax, edx                                      ;542.13
        neg       edi                                           ;542.13
        add       edi, eax                                      ;542.13
        imul      eax, edi, 900                                 ;542.13
        mov       DWORD PTR [-496+ebx+eax], 0                   ;542.13
                                ; LOE edx ecx ebx esi
.B10.350:                       ; Preds .B10.348 .B10.349
        test      esi, esi                                      ;543.13
        jbe       .B10.960      ; Prob 10%                      ;543.13
                                ; LOE edx ecx ebx esi
.B10.351:                       ; Preds .B10.350
        xor       eax, eax                                      ;
        mov       DWORD PTR [64+esp], edx                       ;
        xor       edi, edi                                      ;
        xor       edx, edx                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.352:                       ; Preds .B10.352 .B10.351
        inc       eax                                           ;543.13
        mov       DWORD PTR [408+edi+ebx], edx                  ;543.13
        mov       DWORD PTR [1308+edi+ebx], edx                 ;543.13
        add       edi, 1800                                     ;543.13
        cmp       eax, esi                                      ;543.13
        jb        .B10.352      ; Prob 63%                      ;543.13
                                ; LOE eax edx ecx ebx esi edi
.B10.353:                       ; Preds .B10.352
        mov       edx, DWORD PTR [64+esp]                       ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;543.13
                                ; LOE eax edx ecx ebx esi
.B10.354:                       ; Preds .B10.353 .B10.960
        lea       edi, DWORD PTR [-1+eax]                       ;543.13
        cmp       ecx, edi                                      ;543.13
        jbe       .B10.356      ; Prob 10%                      ;543.13
                                ; LOE eax edx ecx ebx esi
.B10.355:                       ; Preds .B10.354
        mov       edi, edx                                      ;543.13
        add       eax, edx                                      ;543.13
        neg       edi                                           ;543.13
        add       edi, eax                                      ;543.13
        imul      eax, edi, 900                                 ;543.13
        mov       DWORD PTR [-492+ebx+eax], 0                   ;543.13
                                ; LOE edx ecx ebx esi
.B10.356:                       ; Preds .B10.354 .B10.355
        test      esi, esi                                      ;544.13
        jbe       .B10.959      ; Prob 10%                      ;544.13
                                ; LOE edx ecx ebx esi
.B10.357:                       ; Preds .B10.356
        xor       eax, eax                                      ;
        mov       DWORD PTR [64+esp], edx                       ;
        xor       edi, edi                                      ;
        xor       edx, edx                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.358:                       ; Preds .B10.358 .B10.357
        inc       eax                                           ;544.13
        mov       DWORD PTR [412+edi+ebx], edx                  ;544.13
        mov       DWORD PTR [1312+edi+ebx], edx                 ;544.13
        add       edi, 1800                                     ;544.13
        cmp       eax, esi                                      ;544.13
        jb        .B10.358      ; Prob 63%                      ;544.13
                                ; LOE eax edx ecx ebx esi edi
.B10.359:                       ; Preds .B10.358
        mov       edx, DWORD PTR [64+esp]                       ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;544.13
                                ; LOE eax edx ecx ebx esi
.B10.360:                       ; Preds .B10.359 .B10.959
        lea       edi, DWORD PTR [-1+eax]                       ;544.13
        cmp       ecx, edi                                      ;544.13
        jbe       .B10.362      ; Prob 10%                      ;544.13
                                ; LOE eax edx ecx ebx esi
.B10.361:                       ; Preds .B10.360
        mov       edi, edx                                      ;544.13
        add       eax, edx                                      ;544.13
        neg       edi                                           ;544.13
        add       edi, eax                                      ;544.13
        imul      eax, edi, 900                                 ;544.13
        mov       DWORD PTR [-488+ebx+eax], 0                   ;544.13
                                ; LOE edx ecx ebx esi
.B10.362:                       ; Preds .B10.360 .B10.361
        test      esi, esi                                      ;545.13
        jbe       .B10.958      ; Prob 10%                      ;545.13
                                ; LOE edx ecx ebx esi
.B10.363:                       ; Preds .B10.362
        xor       eax, eax                                      ;
        mov       DWORD PTR [64+esp], edx                       ;
        xor       edi, edi                                      ;
        xor       edx, edx                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.364:                       ; Preds .B10.364 .B10.363
        inc       eax                                           ;545.13
        mov       DWORD PTR [416+edi+ebx], edx                  ;545.13
        mov       DWORD PTR [1316+edi+ebx], edx                 ;545.13
        add       edi, 1800                                     ;545.13
        cmp       eax, esi                                      ;545.13
        jb        .B10.364      ; Prob 63%                      ;545.13
                                ; LOE eax edx ecx ebx esi edi
.B10.365:                       ; Preds .B10.364
        mov       edx, DWORD PTR [64+esp]                       ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;545.13
                                ; LOE eax edx ecx ebx esi
.B10.366:                       ; Preds .B10.365 .B10.958
        lea       edi, DWORD PTR [-1+eax]                       ;545.13
        cmp       ecx, edi                                      ;545.13
        jbe       .B10.368      ; Prob 10%                      ;545.13
                                ; LOE eax edx ecx ebx esi
.B10.367:                       ; Preds .B10.366
        mov       edi, edx                                      ;545.13
        add       eax, edx                                      ;545.13
        neg       edi                                           ;545.13
        add       edi, eax                                      ;545.13
        imul      eax, edi, 900                                 ;545.13
        mov       DWORD PTR [-484+ebx+eax], 0                   ;545.13
                                ; LOE edx ecx ebx esi
.B10.368:                       ; Preds .B10.366 .B10.367
        test      esi, esi                                      ;546.13
        jbe       .B10.957      ; Prob 10%                      ;546.13
                                ; LOE edx ecx ebx esi
.B10.369:                       ; Preds .B10.368
        xor       eax, eax                                      ;
        mov       DWORD PTR [64+esp], edx                       ;
        xor       edi, edi                                      ;
        mov       edx, 1086324736                               ;
                                ; LOE eax edx ecx ebx esi edi
.B10.370:                       ; Preds .B10.370 .B10.369
        inc       eax                                           ;546.13
        mov       DWORD PTR [420+edi+ebx], edx                  ;546.13
        mov       DWORD PTR [1320+edi+ebx], edx                 ;546.13
        add       edi, 1800                                     ;546.13
        cmp       eax, esi                                      ;546.13
        jb        .B10.370      ; Prob 63%                      ;546.13
                                ; LOE eax edx ecx ebx esi edi
.B10.371:                       ; Preds .B10.370
        mov       edx, DWORD PTR [64+esp]                       ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;546.13
                                ; LOE eax edx ecx ebx esi
.B10.372:                       ; Preds .B10.371 .B10.957
        lea       edi, DWORD PTR [-1+eax]                       ;546.13
        cmp       ecx, edi                                      ;546.13
        jbe       .B10.374      ; Prob 10%                      ;546.13
                                ; LOE eax edx ecx ebx esi
.B10.373:                       ; Preds .B10.372
        mov       edi, edx                                      ;546.13
        add       eax, edx                                      ;546.13
        neg       edi                                           ;546.13
        add       edi, eax                                      ;546.13
        imul      eax, edi, 900                                 ;546.13
        mov       DWORD PTR [-480+ebx+eax], 1086324736          ;546.13
                                ; LOE edx ecx ebx esi
.B10.374:                       ; Preds .B10.372 .B10.373
        test      esi, esi                                      ;547.13
        jbe       .B10.956      ; Prob 10%                      ;547.13
                                ; LOE edx ecx ebx esi
.B10.375:                       ; Preds .B10.374
        xor       eax, eax                                      ;
        mov       DWORD PTR [64+esp], edx                       ;
        xor       edi, edi                                      ;
        xor       edx, edx                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.376:                       ; Preds .B10.376 .B10.375
        inc       eax                                           ;547.13
        mov       DWORD PTR [424+edi+ebx], edx                  ;547.13
        mov       DWORD PTR [1324+edi+ebx], edx                 ;547.13
        add       edi, 1800                                     ;547.13
        cmp       eax, esi                                      ;547.13
        jb        .B10.376      ; Prob 63%                      ;547.13
                                ; LOE eax edx ecx ebx esi edi
.B10.377:                       ; Preds .B10.376
        mov       edx, DWORD PTR [64+esp]                       ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;547.13
                                ; LOE eax edx ecx ebx esi
.B10.378:                       ; Preds .B10.377 .B10.956
        lea       edi, DWORD PTR [-1+eax]                       ;547.13
        cmp       ecx, edi                                      ;547.13
        jbe       .B10.380      ; Prob 10%                      ;547.13
                                ; LOE eax edx ecx ebx esi
.B10.379:                       ; Preds .B10.378
        mov       edi, edx                                      ;547.13
        add       eax, edx                                      ;547.13
        neg       edi                                           ;547.13
        add       edi, eax                                      ;547.13
        imul      eax, edi, 900                                 ;547.13
        mov       DWORD PTR [-476+ebx+eax], 0                   ;547.13
                                ; LOE edx ecx ebx esi
.B10.380:                       ; Preds .B10.378 .B10.379
        test      esi, esi                                      ;548.13
        jbe       .B10.955      ; Prob 10%                      ;548.13
                                ; LOE edx ecx ebx esi
.B10.381:                       ; Preds .B10.380
        xor       eax, eax                                      ;
        mov       DWORD PTR [64+esp], edx                       ;
        xor       edi, edi                                      ;
        xor       edx, edx                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.382:                       ; Preds .B10.382 .B10.381
        inc       eax                                           ;548.13
        mov       BYTE PTR [428+edi+ebx], dl                    ;548.13
        mov       BYTE PTR [1328+edi+ebx], dl                   ;548.13
        add       edi, 1800                                     ;548.13
        cmp       eax, esi                                      ;548.13
        jb        .B10.382      ; Prob 63%                      ;548.13
                                ; LOE eax edx ecx ebx esi edi
.B10.383:                       ; Preds .B10.382
        mov       edx, DWORD PTR [64+esp]                       ;
        lea       esi, DWORD PTR [1+eax+eax]                    ;548.13
                                ; LOE edx ecx ebx esi
.B10.384:                       ; Preds .B10.383 .B10.955
        lea       eax, DWORD PTR [-1+esi]                       ;548.13
        cmp       ecx, eax                                      ;548.13
        jbe       .B10.386      ; Prob 10%                      ;548.13
                                ; LOE edx ebx esi
.B10.385:                       ; Preds .B10.384
        mov       eax, edx                                      ;548.13
        add       esi, edx                                      ;548.13
        neg       eax                                           ;548.13
        add       eax, esi                                      ;548.13
        imul      edx, eax, 900                                 ;548.13
        mov       BYTE PTR [-472+ebx+edx], 0                    ;548.13
                                ; LOE
.B10.386:                       ; Preds .B10.295 .B10.384 .B10.385
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NLEVEL] ;550.13
        xor       eax, eax                                      ;550.13
        test      ecx, ecx                                      ;550.13
        lea       ebx, DWORD PTR [200+esp]                      ;550.13
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NMOVE] ;550.13
        cmovle    ecx, eax                                      ;550.13
        test      edx, edx                                      ;550.13
        push      4                                             ;550.13
        cmovle    edx, eax                                      ;550.13
        push      edx                                           ;550.13
        push      ecx                                           ;550.13
        push      3                                             ;550.13
        push      ebx                                           ;550.13
        call      _for_check_mult_overflow                      ;550.13
                                ; LOE eax
.B10.387:                       ; Preds .B10.386
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_STOPCAP4W+12] ;550.13
        and       eax, 1                                        ;550.13
        and       edx, 1                                        ;550.13
        add       edx, edx                                      ;550.13
        shl       eax, 4                                        ;550.13
        or        edx, 1                                        ;550.13
        or        edx, eax                                      ;550.13
        or        edx, 262144                                   ;550.13
        push      edx                                           ;550.13
        push      OFFSET FLAT: _DYNUST_NETWORK_MODULE_mp_STOPCAP4W ;550.13
        push      DWORD PTR [228+esp]                           ;550.13
        call      _for_alloc_allocatable                        ;550.13
                                ; LOE eax
.B10.1360:                      ; Preds .B10.387
        add       esp, 32                                       ;550.13
        mov       ebx, eax                                      ;550.13
                                ; LOE ebx
.B10.388:                       ; Preds .B10.1360
        test      ebx, ebx                                      ;550.13
        jne       .B10.391      ; Prob 50%                      ;550.13
                                ; LOE ebx
.B10.389:                       ; Preds .B10.388
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NLEVEL] ;550.13
        xor       eax, eax                                      ;550.13
        test      ecx, ecx                                      ;550.13
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NMOVE] ;550.13
        cmovle    ecx, eax                                      ;550.13
        mov       esi, 4                                        ;550.13
        mov       edi, 1                                        ;550.13
        test      edx, edx                                      ;550.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_STOPCAP4W+32], edi ;550.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_STOPCAP4W+44], edi ;550.13
        cmovle    edx, eax                                      ;550.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_STOPCAP4W+8], eax ;550.13
        lea       edi, DWORD PTR [56+esp]                       ;550.13
        push      esi                                           ;550.13
        push      edx                                           ;550.13
        push      ecx                                           ;550.13
        push      3                                             ;550.13
        push      edi                                           ;550.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_STOPCAP4W+12], 133 ;550.13
        lea       eax, DWORD PTR [ecx*4]                        ;550.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_STOPCAP4W+4], esi ;550.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_STOPCAP4W+16], 2 ;550.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_STOPCAP4W+24], ecx ;550.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_STOPCAP4W+36], edx ;550.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_STOPCAP4W+28], esi ;550.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_STOPCAP4W+40], eax ;550.13
        call      _for_check_mult_overflow                      ;550.13
                                ; LOE ebx
.B10.1361:                      ; Preds .B10.389
        add       esp, 20                                       ;550.13
                                ; LOE ebx
.B10.390:                       ; Preds .B10.1361
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], ebx ;550.13
        jmp       .B10.393      ; Prob 100%                     ;550.13
                                ; LOE
.B10.391:                       ; Preds .B10.388
        mov       DWORD PTR [160+esp], 0                        ;552.14
        lea       eax, DWORD PTR [96+esp]                       ;552.14
        mov       DWORD PTR [96+esp], 46                        ;552.14
        mov       DWORD PTR [100+esp], OFFSET FLAT: __STRLITPACK_42 ;552.14
        push      32                                            ;552.14
        push      eax                                           ;552.14
        push      OFFSET FLAT: __STRLITPACK_181.0.10            ;552.14
        push      -2088435968                                   ;552.14
        push      911                                           ;552.14
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], ebx ;550.13
        lea       edx, DWORD PTR [180+esp]                      ;552.14
        push      edx                                           ;552.14
        call      _for_write_seq_lis                            ;552.14
                                ; LOE
.B10.392:                       ; Preds .B10.391
        push      32                                            ;553.14
        xor       eax, eax                                      ;553.14
        push      eax                                           ;553.14
        push      eax                                           ;553.14
        push      -2088435968                                   ;553.14
        push      eax                                           ;553.14
        push      OFFSET FLAT: __STRLITPACK_182                 ;553.14
        call      _for_stop_core                                ;553.14
                                ; LOE
.B10.1362:                      ; Preds .B10.392
        add       esp, 48                                       ;553.14
                                ; LOE
.B10.393:                       ; Preds .B10.1362 .B10.390
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_STOPCAP4W+36] ;555.13
        test      edi, edi                                      ;555.13
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_STOPCAP4W+44] ;555.13
        jle       .B10.395      ; Prob 10%                      ;555.13
                                ; LOE edx edi
.B10.394:                       ; Preds .B10.393
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_STOPCAP4W+40] ;555.13
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_STOPCAP4W+24] ;555.13
        test      esi, esi                                      ;555.13
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_STOPCAP4W+32] ;555.13
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_STOPCAP4W] ;555.13
        mov       DWORD PTR [20+esp], ebx                       ;555.13
        jg        .B10.823      ; Prob 50%                      ;555.13
                                ; LOE eax edx ecx esi edi
.B10.395:                       ; Preds .B10.1274 .B10.826 .B10.393 .B10.394
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_LEVEL2N] ;557.13
        xor       eax, eax                                      ;557.13
        test      ecx, ecx                                      ;557.13
        lea       ebx, DWORD PTR [204+esp]                      ;557.13
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_MOVE2N] ;557.13
        cmovle    ecx, eax                                      ;557.13
        test      edx, edx                                      ;557.13
        push      4                                             ;557.13
        cmovle    edx, eax                                      ;557.13
        push      edx                                           ;557.13
        push      ecx                                           ;557.13
        push      3                                             ;557.13
        push      ebx                                           ;557.13
        call      _for_check_mult_overflow                      ;557.13
                                ; LOE eax
.B10.396:                       ; Preds .B10.395
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_STOPCAP2W+12] ;557.13
        and       eax, 1                                        ;557.13
        and       edx, 1                                        ;557.13
        add       edx, edx                                      ;557.13
        shl       eax, 4                                        ;557.13
        or        edx, 1                                        ;557.13
        or        edx, eax                                      ;557.13
        or        edx, 262144                                   ;557.13
        push      edx                                           ;557.13
        push      OFFSET FLAT: _DYNUST_NETWORK_MODULE_mp_STOPCAP2W ;557.13
        push      DWORD PTR [232+esp]                           ;557.13
        call      _for_alloc_allocatable                        ;557.13
                                ; LOE eax
.B10.1364:                      ; Preds .B10.396
        add       esp, 32                                       ;557.13
        mov       ebx, eax                                      ;557.13
                                ; LOE ebx
.B10.397:                       ; Preds .B10.1364
        test      ebx, ebx                                      ;557.13
        jne       .B10.400      ; Prob 50%                      ;557.13
                                ; LOE ebx
.B10.398:                       ; Preds .B10.397
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_LEVEL2N] ;557.13
        xor       eax, eax                                      ;557.13
        test      ecx, ecx                                      ;557.13
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_MOVE2N] ;557.13
        cmovle    ecx, eax                                      ;557.13
        mov       esi, 4                                        ;557.13
        mov       edi, 1                                        ;557.13
        test      edx, edx                                      ;557.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_STOPCAP2W+32], edi ;557.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_STOPCAP2W+44], edi ;557.13
        cmovle    edx, eax                                      ;557.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_STOPCAP2W+8], eax ;557.13
        lea       edi, DWORD PTR [60+esp]                       ;557.13
        push      esi                                           ;557.13
        push      edx                                           ;557.13
        push      ecx                                           ;557.13
        push      3                                             ;557.13
        push      edi                                           ;557.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_STOPCAP2W+12], 133 ;557.13
        lea       eax, DWORD PTR [ecx*4]                        ;557.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_STOPCAP2W+4], esi ;557.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_STOPCAP2W+16], 2 ;557.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_STOPCAP2W+24], ecx ;557.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_STOPCAP2W+36], edx ;557.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_STOPCAP2W+28], esi ;557.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_STOPCAP2W+40], eax ;557.13
        call      _for_check_mult_overflow                      ;557.13
                                ; LOE ebx
.B10.1365:                      ; Preds .B10.398
        add       esp, 20                                       ;557.13
                                ; LOE ebx
.B10.399:                       ; Preds .B10.1365
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], ebx ;557.13
        jmp       .B10.402      ; Prob 100%                     ;557.13
                                ; LOE
.B10.400:                       ; Preds .B10.397
        mov       DWORD PTR [160+esp], 0                        ;559.14
        lea       eax, DWORD PTR [104+esp]                      ;559.14
        mov       DWORD PTR [104+esp], 46                       ;559.14
        mov       DWORD PTR [108+esp], OFFSET FLAT: __STRLITPACK_40 ;559.14
        push      32                                            ;559.14
        push      eax                                           ;559.14
        push      OFFSET FLAT: __STRLITPACK_183.0.10            ;559.14
        push      -2088435968                                   ;559.14
        push      911                                           ;559.14
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], ebx ;557.13
        lea       edx, DWORD PTR [180+esp]                      ;559.14
        push      edx                                           ;559.14
        call      _for_write_seq_lis                            ;559.14
                                ; LOE
.B10.401:                       ; Preds .B10.400
        push      32                                            ;560.14
        xor       eax, eax                                      ;560.14
        push      eax                                           ;560.14
        push      eax                                           ;560.14
        push      -2088435968                                   ;560.14
        push      eax                                           ;560.14
        push      OFFSET FLAT: __STRLITPACK_184                 ;560.14
        call      _for_stop_core                                ;560.14
                                ; LOE
.B10.1366:                      ; Preds .B10.401
        add       esp, 48                                       ;560.14
                                ; LOE
.B10.402:                       ; Preds .B10.1366 .B10.399
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_STOPCAP2W+36] ;562.13
        test      edi, edi                                      ;562.13
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_STOPCAP2W+44] ;562.13
        jle       .B10.404      ; Prob 10%                      ;562.13
                                ; LOE edx edi
.B10.403:                       ; Preds .B10.402
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_STOPCAP2W+40] ;562.13
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_STOPCAP2W+24] ;562.13
        test      esi, esi                                      ;562.13
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_STOPCAP2W+32] ;562.13
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_STOPCAP2W] ;562.13
        mov       DWORD PTR [20+esp], ebx                       ;562.13
        jg        .B10.819      ; Prob 50%                      ;562.13
                                ; LOE eax edx ecx esi edi
.B10.404:                       ; Preds .B10.1252 .B10.822 .B10.402 .B10.403
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_LEVEL2N] ;564.13
        xor       eax, eax                                      ;564.13
        test      edx, edx                                      ;564.13
        lea       ecx, DWORD PTR [208+esp]                      ;564.13
        push      4                                             ;564.13
        cmovle    edx, eax                                      ;564.13
        push      edx                                           ;564.13
        push      2                                             ;564.13
        push      ecx                                           ;564.13
        call      _for_check_mult_overflow                      ;564.13
                                ; LOE eax
.B10.405:                       ; Preds .B10.404
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_STOPCAP2WIND+12] ;564.13
        and       eax, 1                                        ;564.13
        and       edx, 1                                        ;564.13
        add       edx, edx                                      ;564.13
        shl       eax, 4                                        ;564.13
        or        edx, 1                                        ;564.13
        or        edx, eax                                      ;564.13
        or        edx, 262144                                   ;564.13
        push      edx                                           ;564.13
        push      OFFSET FLAT: _DYNUST_NETWORK_MODULE_mp_STOPCAP2WIND ;564.13
        push      DWORD PTR [232+esp]                           ;564.13
        call      _for_alloc_allocatable                        ;564.13
                                ; LOE eax
.B10.1368:                      ; Preds .B10.405
        add       esp, 28                                       ;564.13
        mov       ebx, eax                                      ;564.13
                                ; LOE ebx
.B10.406:                       ; Preds .B10.1368
        test      ebx, ebx                                      ;564.13
        jne       .B10.409      ; Prob 50%                      ;564.13
                                ; LOE ebx
.B10.407:                       ; Preds .B10.406
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_LEVEL2N] ;564.13
        xor       eax, eax                                      ;564.13
        test      esi, esi                                      ;564.13
        cmovle    esi, eax                                      ;564.13
        mov       ecx, 4                                        ;564.13
        lea       edi, DWORD PTR [64+esp]                       ;564.13
        push      ecx                                           ;564.13
        push      esi                                           ;564.13
        push      2                                             ;564.13
        push      edi                                           ;564.13
        mov       edx, 1                                        ;564.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_STOPCAP2WIND+12], 133 ;564.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_STOPCAP2WIND+4], ecx ;564.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_STOPCAP2WIND+16], edx ;564.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_STOPCAP2WIND+8], eax ;564.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_STOPCAP2WIND+32], edx ;564.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_STOPCAP2WIND+24], esi ;564.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_STOPCAP2WIND+28], ecx ;564.13
        call      _for_check_mult_overflow                      ;564.13
                                ; LOE ebx
.B10.1369:                      ; Preds .B10.407
        add       esp, 16                                       ;564.13
                                ; LOE ebx
.B10.408:                       ; Preds .B10.1369
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], ebx ;564.13
        jmp       .B10.411      ; Prob 100%                     ;564.13
                                ; LOE
.B10.409:                       ; Preds .B10.406
        mov       DWORD PTR [160+esp], 0                        ;566.14
        lea       eax, DWORD PTR [112+esp]                      ;566.14
        mov       DWORD PTR [112+esp], 49                       ;566.14
        mov       DWORD PTR [116+esp], OFFSET FLAT: __STRLITPACK_38 ;566.14
        push      32                                            ;566.14
        push      eax                                           ;566.14
        push      OFFSET FLAT: __STRLITPACK_185.0.10            ;566.14
        push      -2088435968                                   ;566.14
        push      911                                           ;566.14
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], ebx ;564.13
        lea       edx, DWORD PTR [180+esp]                      ;566.14
        push      edx                                           ;566.14
        call      _for_write_seq_lis                            ;566.14
                                ; LOE
.B10.410:                       ; Preds .B10.409
        push      32                                            ;567.14
        xor       eax, eax                                      ;567.14
        push      eax                                           ;567.14
        push      eax                                           ;567.14
        push      -2088435968                                   ;567.14
        push      eax                                           ;567.14
        push      OFFSET FLAT: __STRLITPACK_186                 ;567.14
        call      _for_stop_core                                ;567.14
                                ; LOE
.B10.1370:                      ; Preds .B10.410
        add       esp, 48                                       ;567.14
                                ; LOE
.B10.411:                       ; Preds .B10.1370 .B10.408
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_STOPCAP2WIND+24] ;569.13
        test      ecx, ecx                                      ;569.13
        jle       .B10.414      ; Prob 50%                      ;569.13
                                ; LOE ecx
.B10.412:                       ; Preds .B10.411
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_STOPCAP2WIND] ;569.13
        cmp       ecx, 24                                       ;569.13
        jle       .B10.970      ; Prob 0%                       ;569.13
                                ; LOE edx ecx
.B10.413:                       ; Preds .B10.412
        shl       ecx, 2                                        ;569.13
        push      ecx                                           ;569.13
        push      0                                             ;569.13
        push      edx                                           ;569.13
        call      __intel_fast_memset                           ;569.13
                                ; LOE
.B10.1371:                      ; Preds .B10.413
        add       esp, 12                                       ;569.13
                                ; LOE
.B10.414:                       ; Preds .B10.984 .B10.1371 .B10.411 .B10.982
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_LEVEL2N] ;571.13
        xor       eax, eax                                      ;571.13
        test      ecx, ecx                                      ;571.13
        lea       ebx, DWORD PTR [212+esp]                      ;571.13
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_MOVE2N] ;571.13
        cmovle    ecx, eax                                      ;571.13
        test      edx, edx                                      ;571.13
        push      4                                             ;571.13
        cmovle    edx, eax                                      ;571.13
        push      edx                                           ;571.13
        push      ecx                                           ;571.13
        push      3                                             ;571.13
        push      ebx                                           ;571.13
        call      _for_check_mult_overflow                      ;571.13
                                ; LOE eax
.B10.415:                       ; Preds .B10.414
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_YIELDCAP+12] ;571.13
        and       eax, 1                                        ;571.13
        and       edx, 1                                        ;571.13
        add       edx, edx                                      ;571.13
        shl       eax, 4                                        ;571.13
        or        edx, 1                                        ;571.13
        or        edx, eax                                      ;571.13
        or        edx, 262144                                   ;571.13
        push      edx                                           ;571.13
        push      OFFSET FLAT: _DYNUST_NETWORK_MODULE_mp_YIELDCAP ;571.13
        push      DWORD PTR [240+esp]                           ;571.13
        call      _for_alloc_allocatable                        ;571.13
                                ; LOE eax
.B10.1373:                      ; Preds .B10.415
        add       esp, 32                                       ;571.13
        mov       ebx, eax                                      ;571.13
                                ; LOE ebx
.B10.416:                       ; Preds .B10.1373
        test      ebx, ebx                                      ;571.13
        jne       .B10.419      ; Prob 50%                      ;571.13
                                ; LOE ebx
.B10.417:                       ; Preds .B10.416
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_LEVEL2N] ;571.13
        xor       eax, eax                                      ;571.13
        test      ecx, ecx                                      ;571.13
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_MOVE2N] ;571.13
        cmovle    ecx, eax                                      ;571.13
        mov       esi, 4                                        ;571.13
        mov       edi, 1                                        ;571.13
        test      edx, edx                                      ;571.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_YIELDCAP+32], edi ;571.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_YIELDCAP+44], edi ;571.13
        cmovle    edx, eax                                      ;571.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_YIELDCAP+8], eax ;571.13
        lea       edi, DWORD PTR [68+esp]                       ;571.13
        push      esi                                           ;571.13
        push      edx                                           ;571.13
        push      ecx                                           ;571.13
        push      3                                             ;571.13
        push      edi                                           ;571.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_YIELDCAP+12], 133 ;571.13
        lea       eax, DWORD PTR [ecx*4]                        ;571.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_YIELDCAP+4], esi ;571.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_YIELDCAP+16], 2 ;571.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_YIELDCAP+24], ecx ;571.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_YIELDCAP+36], edx ;571.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_YIELDCAP+28], esi ;571.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_YIELDCAP+40], eax ;571.13
        call      _for_check_mult_overflow                      ;571.13
                                ; LOE ebx
.B10.1374:                      ; Preds .B10.417
        add       esp, 20                                       ;571.13
                                ; LOE ebx
.B10.418:                       ; Preds .B10.1374
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], ebx ;571.13
        jmp       .B10.421      ; Prob 100%                     ;571.13
                                ; LOE
.B10.419:                       ; Preds .B10.416
        mov       DWORD PTR [160+esp], 0                        ;573.14
        lea       eax, DWORD PTR [120+esp]                      ;573.14
        mov       DWORD PTR [120+esp], 45                       ;573.14
        mov       DWORD PTR [124+esp], OFFSET FLAT: __STRLITPACK_36 ;573.14
        push      32                                            ;573.14
        push      eax                                           ;573.14
        push      OFFSET FLAT: __STRLITPACK_187.0.10            ;573.14
        push      -2088435968                                   ;573.14
        push      911                                           ;573.14
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], ebx ;571.13
        lea       edx, DWORD PTR [180+esp]                      ;573.14
        push      edx                                           ;573.14
        call      _for_write_seq_lis                            ;573.14
                                ; LOE
.B10.420:                       ; Preds .B10.419
        push      32                                            ;574.14
        xor       eax, eax                                      ;574.14
        push      eax                                           ;574.14
        push      eax                                           ;574.14
        push      -2088435968                                   ;574.14
        push      eax                                           ;574.14
        push      OFFSET FLAT: __STRLITPACK_188                 ;574.14
        call      _for_stop_core                                ;574.14
                                ; LOE
.B10.1375:                      ; Preds .B10.420
        add       esp, 48                                       ;574.14
                                ; LOE
.B10.421:                       ; Preds .B10.1375 .B10.418
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_YIELDCAP+36] ;576.13
        test      edi, edi                                      ;576.13
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_YIELDCAP+44] ;576.13
        jle       .B10.423      ; Prob 10%                      ;576.13
                                ; LOE edx edi
.B10.422:                       ; Preds .B10.421
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_YIELDCAP+40] ;576.13
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_YIELDCAP+24] ;576.13
        test      esi, esi                                      ;576.13
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_YIELDCAP+32] ;576.13
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_YIELDCAP] ;576.13
        mov       DWORD PTR [20+esp], ebx                       ;576.13
        jg        .B10.815      ; Prob 50%                      ;576.13
                                ; LOE eax edx ecx esi edi
.B10.423:                       ; Preds .B10.1229 .B10.818 .B10.421 .B10.422
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_LEVEL2N] ;578.13
        xor       eax, eax                                      ;578.13
        test      edx, edx                                      ;578.13
        lea       ecx, DWORD PTR [216+esp]                      ;578.13
        push      4                                             ;578.13
        cmovle    edx, eax                                      ;578.13
        push      edx                                           ;578.13
        push      2                                             ;578.13
        push      ecx                                           ;578.13
        call      _for_check_mult_overflow                      ;578.13
                                ; LOE eax
.B10.424:                       ; Preds .B10.423
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_YIELDCAPIND+12] ;578.13
        and       eax, 1                                        ;578.13
        and       edx, 1                                        ;578.13
        add       edx, edx                                      ;578.13
        shl       eax, 4                                        ;578.13
        or        edx, 1                                        ;578.13
        or        edx, eax                                      ;578.13
        or        edx, 262144                                   ;578.13
        push      edx                                           ;578.13
        push      OFFSET FLAT: _DYNUST_NETWORK_MODULE_mp_YIELDCAPIND ;578.13
        push      DWORD PTR [240+esp]                           ;578.13
        call      _for_alloc_allocatable                        ;578.13
                                ; LOE eax
.B10.1377:                      ; Preds .B10.424
        add       esp, 28                                       ;578.13
        mov       ebx, eax                                      ;578.13
                                ; LOE ebx
.B10.425:                       ; Preds .B10.1377
        test      ebx, ebx                                      ;578.13
        jne       .B10.428      ; Prob 50%                      ;578.13
                                ; LOE ebx
.B10.426:                       ; Preds .B10.425
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_LEVEL2N] ;578.13
        xor       eax, eax                                      ;578.13
        test      esi, esi                                      ;578.13
        cmovle    esi, eax                                      ;578.13
        mov       ecx, 4                                        ;578.13
        lea       edi, DWORD PTR [72+esp]                       ;578.13
        push      ecx                                           ;578.13
        push      esi                                           ;578.13
        push      2                                             ;578.13
        push      edi                                           ;578.13
        mov       edx, 1                                        ;578.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_YIELDCAPIND+12], 133 ;578.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_YIELDCAPIND+4], ecx ;578.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_YIELDCAPIND+16], edx ;578.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_YIELDCAPIND+8], eax ;578.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_YIELDCAPIND+32], edx ;578.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_YIELDCAPIND+24], esi ;578.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_YIELDCAPIND+28], ecx ;578.13
        call      _for_check_mult_overflow                      ;578.13
                                ; LOE ebx
.B10.1378:                      ; Preds .B10.426
        add       esp, 16                                       ;578.13
                                ; LOE ebx
.B10.427:                       ; Preds .B10.1378
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], ebx ;578.13
        jmp       .B10.430      ; Prob 100%                     ;578.13
                                ; LOE
.B10.428:                       ; Preds .B10.425
        mov       DWORD PTR [160+esp], 0                        ;580.14
        lea       eax, DWORD PTR [128+esp]                      ;580.14
        mov       DWORD PTR [128+esp], 48                       ;580.14
        mov       DWORD PTR [132+esp], OFFSET FLAT: __STRLITPACK_34 ;580.14
        push      32                                            ;580.14
        push      eax                                           ;580.14
        push      OFFSET FLAT: __STRLITPACK_189.0.10            ;580.14
        push      -2088435968                                   ;580.14
        push      911                                           ;580.14
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], ebx ;578.13
        lea       edx, DWORD PTR [180+esp]                      ;580.14
        push      edx                                           ;580.14
        call      _for_write_seq_lis                            ;580.14
                                ; LOE
.B10.429:                       ; Preds .B10.428
        push      32                                            ;581.14
        xor       eax, eax                                      ;581.14
        push      eax                                           ;581.14
        push      eax                                           ;581.14
        push      -2088435968                                   ;581.14
        push      eax                                           ;581.14
        push      OFFSET FLAT: __STRLITPACK_190                 ;581.14
        call      _for_stop_core                                ;581.14
                                ; LOE
.B10.1379:                      ; Preds .B10.429
        add       esp, 48                                       ;581.14
                                ; LOE
.B10.430:                       ; Preds .B10.1379 .B10.427
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_YIELDCAPIND+24] ;583.13
        test      ecx, ecx                                      ;583.13
        jle       .B10.433      ; Prob 50%                      ;583.13
                                ; LOE ecx
.B10.431:                       ; Preds .B10.430
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_YIELDCAPIND] ;583.13
        cmp       ecx, 24                                       ;583.13
        jle       .B10.989      ; Prob 0%                       ;583.13
                                ; LOE edx ecx
.B10.432:                       ; Preds .B10.431
        shl       ecx, 2                                        ;583.13
        push      ecx                                           ;583.13
        push      0                                             ;583.13
        push      edx                                           ;583.13
        call      __intel_fast_memset                           ;583.13
                                ; LOE
.B10.1380:                      ; Preds .B10.432
        add       esp, 12                                       ;583.13
                                ; LOE
.B10.433:                       ; Preds .B10.1003 .B10.1380 .B10.430 .B10.1001
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+24] ;585.13
        test      edx, edx                                      ;585.13
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;585.13
        jle       .B10.470      ; Prob 50%                      ;585.13
                                ; LOE edx esi
.B10.434:                       ; Preds .B10.433
        mov       ebx, edx                                      ;585.13
        shr       ebx, 31                                       ;585.13
        add       ebx, edx                                      ;585.13
        sar       ebx, 1                                        ;585.13
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;585.13
        test      ebx, ebx                                      ;585.13
        jbe       .B10.1013     ; Prob 10%                      ;585.13
                                ; LOE edx ecx ebx esi
.B10.435:                       ; Preds .B10.434
        xor       eax, eax                                      ;
        mov       DWORD PTR [esp], esi                          ;
        xor       edi, edi                                      ;
        mov       esi, eax                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.436:                       ; Preds .B10.436 .B10.435
        inc       edi                                           ;585.13
        mov       DWORD PTR [432+esi+ecx], eax                  ;585.13
        mov       DWORD PTR [1332+esi+ecx], eax                 ;585.13
        add       esi, 1800                                     ;585.13
        cmp       edi, ebx                                      ;585.13
        jb        .B10.436      ; Prob 63%                      ;585.13
                                ; LOE eax edx ecx ebx esi edi
.B10.437:                       ; Preds .B10.436
        mov       esi, DWORD PTR [esp]                          ;
        lea       eax, DWORD PTR [1+edi+edi]                    ;585.13
                                ; LOE eax edx ecx ebx esi
.B10.438:                       ; Preds .B10.437 .B10.1013
        lea       edi, DWORD PTR [-1+eax]                       ;585.13
        cmp       edx, edi                                      ;585.13
        jbe       .B10.440      ; Prob 10%                      ;585.13
                                ; LOE eax edx ecx ebx esi
.B10.439:                       ; Preds .B10.438
        mov       edi, esi                                      ;585.13
        add       eax, esi                                      ;585.13
        neg       edi                                           ;585.13
        add       edi, eax                                      ;585.13
        imul      eax, edi, 900                                 ;585.13
        mov       DWORD PTR [-468+ecx+eax], 0                   ;585.13
                                ; LOE edx ecx ebx esi
.B10.440:                       ; Preds .B10.438 .B10.439
        test      ebx, ebx                                      ;586.13
        jbe       .B10.1012     ; Prob 10%                      ;586.13
                                ; LOE edx ecx ebx esi
.B10.441:                       ; Preds .B10.440
        xor       eax, eax                                      ;
        mov       DWORD PTR [esp], esi                          ;
        xor       edi, edi                                      ;
        mov       esi, eax                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.442:                       ; Preds .B10.442 .B10.441
        inc       edi                                           ;586.13
        mov       DWORD PTR [436+esi+ecx], eax                  ;586.13
        mov       DWORD PTR [1336+esi+ecx], eax                 ;586.13
        add       esi, 1800                                     ;586.13
        cmp       edi, ebx                                      ;586.13
        jb        .B10.442      ; Prob 63%                      ;586.13
                                ; LOE eax edx ecx ebx esi edi
.B10.443:                       ; Preds .B10.442
        mov       esi, DWORD PTR [esp]                          ;
        lea       eax, DWORD PTR [1+edi+edi]                    ;586.13
                                ; LOE eax edx ecx ebx esi
.B10.444:                       ; Preds .B10.443 .B10.1012
        lea       edi, DWORD PTR [-1+eax]                       ;586.13
        cmp       edx, edi                                      ;586.13
        jbe       .B10.446      ; Prob 10%                      ;586.13
                                ; LOE eax edx ecx ebx esi
.B10.445:                       ; Preds .B10.444
        mov       edi, esi                                      ;586.13
        add       eax, esi                                      ;586.13
        neg       edi                                           ;586.13
        add       edi, eax                                      ;586.13
        imul      eax, edi, 900                                 ;586.13
        mov       DWORD PTR [-464+ecx+eax], 0                   ;586.13
                                ; LOE edx ecx ebx esi
.B10.446:                       ; Preds .B10.444 .B10.445
        test      ebx, ebx                                      ;587.13
        jbe       .B10.1011     ; Prob 10%                      ;587.13
                                ; LOE edx ecx ebx esi
.B10.447:                       ; Preds .B10.446
        xor       eax, eax                                      ;
        mov       DWORD PTR [esp], esi                          ;
        xor       edi, edi                                      ;
        mov       esi, eax                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.448:                       ; Preds .B10.448 .B10.447
        inc       edi                                           ;587.13
        mov       DWORD PTR [440+esi+ecx], eax                  ;587.13
        mov       DWORD PTR [1340+esi+ecx], eax                 ;587.13
        add       esi, 1800                                     ;587.13
        cmp       edi, ebx                                      ;587.13
        jb        .B10.448      ; Prob 63%                      ;587.13
                                ; LOE eax edx ecx ebx esi edi
.B10.449:                       ; Preds .B10.448
        mov       esi, DWORD PTR [esp]                          ;
        lea       eax, DWORD PTR [1+edi+edi]                    ;587.13
                                ; LOE eax edx ecx ebx esi
.B10.450:                       ; Preds .B10.449 .B10.1011
        lea       edi, DWORD PTR [-1+eax]                       ;587.13
        cmp       edx, edi                                      ;587.13
        jbe       .B10.452      ; Prob 10%                      ;587.13
                                ; LOE eax edx ecx ebx esi
.B10.451:                       ; Preds .B10.450
        mov       edi, esi                                      ;587.13
        add       eax, esi                                      ;587.13
        neg       edi                                           ;587.13
        add       edi, eax                                      ;587.13
        imul      eax, edi, 900                                 ;587.13
        mov       DWORD PTR [-460+ecx+eax], 0                   ;587.13
                                ; LOE edx ecx ebx esi
.B10.452:                       ; Preds .B10.450 .B10.451
        test      ebx, ebx                                      ;588.13
        jbe       .B10.1010     ; Prob 10%                      ;588.13
                                ; LOE edx ecx ebx esi
.B10.453:                       ; Preds .B10.452
        xor       eax, eax                                      ;
        mov       DWORD PTR [esp], esi                          ;
        xor       edi, edi                                      ;
        mov       esi, eax                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.454:                       ; Preds .B10.454 .B10.453
        inc       edi                                           ;588.13
        mov       BYTE PTR [444+esi+ecx], al                    ;588.13
        mov       BYTE PTR [1344+esi+ecx], al                   ;588.13
        add       esi, 1800                                     ;588.13
        cmp       edi, ebx                                      ;588.13
        jb        .B10.454      ; Prob 63%                      ;588.13
                                ; LOE eax edx ecx ebx esi edi
.B10.455:                       ; Preds .B10.454
        mov       esi, DWORD PTR [esp]                          ;
        lea       eax, DWORD PTR [1+edi+edi]                    ;588.13
                                ; LOE eax edx ecx ebx esi
.B10.456:                       ; Preds .B10.455 .B10.1010
        lea       edi, DWORD PTR [-1+eax]                       ;588.13
        cmp       edx, edi                                      ;588.13
        jbe       .B10.458      ; Prob 10%                      ;588.13
                                ; LOE eax edx ecx ebx esi
.B10.457:                       ; Preds .B10.456
        mov       edi, esi                                      ;588.13
        add       eax, esi                                      ;588.13
        neg       edi                                           ;588.13
        add       edi, eax                                      ;588.13
        imul      eax, edi, 900                                 ;588.13
        mov       BYTE PTR [-456+ecx+eax], 0                    ;588.13
                                ; LOE edx ecx ebx esi
.B10.458:                       ; Preds .B10.456 .B10.457
        test      ebx, ebx                                      ;589.13
        jbe       .B10.1009     ; Prob 10%                      ;589.13
                                ; LOE edx ecx ebx esi
.B10.459:                       ; Preds .B10.458
        xor       eax, eax                                      ;
        mov       DWORD PTR [esp], esi                          ;
        xor       edi, edi                                      ;
        mov       esi, eax                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.460:                       ; Preds .B10.460 .B10.459
        inc       edi                                           ;589.13
        mov       DWORD PTR [448+esi+ecx], eax                  ;589.13
        mov       DWORD PTR [1348+esi+ecx], eax                 ;589.13
        add       esi, 1800                                     ;589.13
        cmp       edi, ebx                                      ;589.13
        jb        .B10.460      ; Prob 63%                      ;589.13
                                ; LOE eax edx ecx ebx esi edi
.B10.461:                       ; Preds .B10.460
        mov       esi, DWORD PTR [esp]                          ;
        lea       eax, DWORD PTR [1+edi+edi]                    ;589.13
                                ; LOE eax edx ecx ebx esi
.B10.462:                       ; Preds .B10.461 .B10.1009
        lea       edi, DWORD PTR [-1+eax]                       ;589.13
        cmp       edx, edi                                      ;589.13
        jbe       .B10.464      ; Prob 10%                      ;589.13
                                ; LOE eax edx ecx ebx esi
.B10.463:                       ; Preds .B10.462
        mov       edi, esi                                      ;589.13
        add       eax, esi                                      ;589.13
        neg       edi                                           ;589.13
        add       edi, eax                                      ;589.13
        imul      eax, edi, 900                                 ;589.13
        mov       DWORD PTR [-452+ecx+eax], 0                   ;589.13
                                ; LOE edx ecx ebx esi
.B10.464:                       ; Preds .B10.462 .B10.463
        test      ebx, ebx                                      ;590.13
        jbe       .B10.1008     ; Prob 10%                      ;590.13
                                ; LOE edx ecx ebx esi
.B10.465:                       ; Preds .B10.464
        xor       eax, eax                                      ;
        mov       DWORD PTR [esp], esi                          ;
        xor       edi, edi                                      ;
        mov       esi, eax                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.466:                       ; Preds .B10.466 .B10.465
        inc       edi                                           ;590.13
        mov       DWORD PTR [452+esi+ecx], eax                  ;590.13
        mov       DWORD PTR [1352+esi+ecx], eax                 ;590.13
        add       esi, 1800                                     ;590.13
        cmp       edi, ebx                                      ;590.13
        jb        .B10.466      ; Prob 63%                      ;590.13
                                ; LOE eax edx ecx ebx esi edi
.B10.467:                       ; Preds .B10.466
        mov       esi, DWORD PTR [esp]                          ;
        lea       ebx, DWORD PTR [1+edi+edi]                    ;590.13
                                ; LOE edx ecx ebx esi
.B10.468:                       ; Preds .B10.467 .B10.1008
        lea       eax, DWORD PTR [-1+ebx]                       ;590.13
        cmp       edx, eax                                      ;590.13
        jbe       .B10.470      ; Prob 10%                      ;590.13
                                ; LOE ecx ebx esi
.B10.469:                       ; Preds .B10.468
        mov       eax, esi                                      ;590.13
        add       ebx, esi                                      ;590.13
        neg       eax                                           ;590.13
        add       eax, ebx                                      ;590.13
        imul      edx, eax, 900                                 ;590.13
        mov       DWORD PTR [-448+ecx+edx], 0                   ;590.13
                                ; LOE
.B10.470:                       ; Preds .B10.433 .B10.468 .B10.469
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NZONES] ;592.13
        xor       eax, eax                                      ;592.13
        test      edx, edx                                      ;592.13
        lea       ecx, DWORD PTR [220+esp]                      ;592.13
        push      4                                             ;592.13
        cmovle    edx, eax                                      ;592.13
        push      edx                                           ;592.13
        push      2                                             ;592.13
        push      ecx                                           ;592.13
        call      _for_check_mult_overflow                      ;592.13
                                ; LOE eax
.B10.471:                       ; Preds .B10.470
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_LOADWEIGHTID+12] ;592.13
        and       eax, 1                                        ;592.13
        and       edx, 1                                        ;592.13
        add       edx, edx                                      ;592.13
        shl       eax, 4                                        ;592.13
        or        edx, 1                                        ;592.13
        or        edx, eax                                      ;592.13
        or        edx, 262144                                   ;592.13
        push      edx                                           ;592.13
        push      OFFSET FLAT: _DYNUST_NETWORK_MODULE_mp_LOADWEIGHTID ;592.13
        push      DWORD PTR [244+esp]                           ;592.13
        call      _for_alloc_allocatable                        ;592.13
                                ; LOE eax
.B10.1382:                      ; Preds .B10.471
        add       esp, 28                                       ;592.13
        mov       ebx, eax                                      ;592.13
                                ; LOE ebx
.B10.472:                       ; Preds .B10.1382
        test      ebx, ebx                                      ;592.13
        jne       .B10.475      ; Prob 50%                      ;592.13
                                ; LOE ebx
.B10.473:                       ; Preds .B10.472
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NZONES] ;592.13
        xor       eax, eax                                      ;592.13
        test      esi, esi                                      ;592.13
        cmovle    esi, eax                                      ;592.13
        mov       ecx, 4                                        ;592.13
        lea       edi, DWORD PTR [76+esp]                       ;592.13
        push      ecx                                           ;592.13
        push      esi                                           ;592.13
        push      2                                             ;592.13
        push      edi                                           ;592.13
        mov       edx, 1                                        ;592.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_LOADWEIGHTID+12], 133 ;592.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_LOADWEIGHTID+4], ecx ;592.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_LOADWEIGHTID+16], edx ;592.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_LOADWEIGHTID+8], eax ;592.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_LOADWEIGHTID+32], edx ;592.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_LOADWEIGHTID+24], esi ;592.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_LOADWEIGHTID+28], ecx ;592.13
        call      _for_check_mult_overflow                      ;592.13
                                ; LOE ebx
.B10.1383:                      ; Preds .B10.473
        add       esp, 16                                       ;592.13
                                ; LOE ebx
.B10.474:                       ; Preds .B10.1383
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], ebx ;592.13
        jmp       .B10.477      ; Prob 100%                     ;592.13
                                ; LOE
.B10.475:                       ; Preds .B10.472
        mov       DWORD PTR [160+esp], 0                        ;594.14
        lea       eax, DWORD PTR [136+esp]                      ;594.14
        mov       DWORD PTR [136+esp], 49                       ;594.14
        mov       DWORD PTR [140+esp], OFFSET FLAT: __STRLITPACK_32 ;594.14
        push      32                                            ;594.14
        push      eax                                           ;594.14
        push      OFFSET FLAT: __STRLITPACK_191.0.10            ;594.14
        push      -2088435968                                   ;594.14
        push      911                                           ;594.14
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], ebx ;592.13
        lea       edx, DWORD PTR [180+esp]                      ;594.14
        push      edx                                           ;594.14
        call      _for_write_seq_lis                            ;594.14
                                ; LOE
.B10.476:                       ; Preds .B10.475
        push      32                                            ;595.14
        xor       eax, eax                                      ;595.14
        push      eax                                           ;595.14
        push      eax                                           ;595.14
        push      -2088435968                                   ;595.14
        push      eax                                           ;595.14
        push      OFFSET FLAT: __STRLITPACK_192                 ;595.14
        call      _for_stop_core                                ;595.14
                                ; LOE
.B10.1384:                      ; Preds .B10.476
        add       esp, 48                                       ;595.14
                                ; LOE
.B10.477:                       ; Preds .B10.1384 .B10.474
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_LOADWEIGHTID+24] ;597.10
        test      ecx, ecx                                      ;597.10
        jle       .B10.480      ; Prob 50%                      ;597.10
                                ; LOE ecx
.B10.478:                       ; Preds .B10.477
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_LOADWEIGHTID] ;597.10
        cmp       ecx, 24                                       ;597.10
        jle       .B10.1014     ; Prob 0%                       ;597.10
                                ; LOE edx ecx
.B10.479:                       ; Preds .B10.478
        shl       ecx, 2                                        ;597.10
        push      ecx                                           ;597.10
        push      0                                             ;597.10
        push      edx                                           ;597.10
        call      __intel_fast_memset                           ;597.10
                                ; LOE
.B10.1385:                      ; Preds .B10.479
        add       esp, 12                                       ;597.10
                                ; LOE
.B10.480:                       ; Preds .B10.1028 .B10.1385 .B10.477 .B10.1026
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+24] ;599.13
        test      edx, edx                                      ;599.13
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;599.13
        jle       .B10.547      ; Prob 50%                      ;599.13
                                ; LOE edx esi
.B10.481:                       ; Preds .B10.480
        mov       ebx, edx                                      ;599.13
        shr       ebx, 31                                       ;599.13
        add       ebx, edx                                      ;599.13
        sar       ebx, 1                                        ;599.13
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;599.13
        test      ebx, ebx                                      ;599.13
        jbe       .B10.1043     ; Prob 10%                      ;599.13
                                ; LOE edx ecx ebx esi
.B10.482:                       ; Preds .B10.481
        xor       eax, eax                                      ;
        mov       DWORD PTR [esp], esi                          ;
        xor       edi, edi                                      ;
        mov       esi, eax                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.483:                       ; Preds .B10.483 .B10.482
        inc       edi                                           ;599.13
        mov       DWORD PTR [636+esi+ecx], eax                  ;599.13
        mov       DWORD PTR [1536+esi+ecx], eax                 ;599.13
        add       esi, 1800                                     ;599.13
        cmp       edi, ebx                                      ;599.13
        jb        .B10.483      ; Prob 63%                      ;599.13
                                ; LOE eax edx ecx ebx esi edi
.B10.484:                       ; Preds .B10.483
        mov       esi, DWORD PTR [esp]                          ;
        lea       eax, DWORD PTR [1+edi+edi]                    ;599.13
                                ; LOE eax edx ecx ebx esi
.B10.485:                       ; Preds .B10.484 .B10.1043
        lea       edi, DWORD PTR [-1+eax]                       ;599.13
        cmp       edx, edi                                      ;599.13
        jbe       .B10.487      ; Prob 10%                      ;599.13
                                ; LOE eax edx ecx ebx esi
.B10.486:                       ; Preds .B10.485
        mov       edi, esi                                      ;599.13
        add       eax, esi                                      ;599.13
        neg       edi                                           ;599.13
        add       edi, eax                                      ;599.13
        imul      eax, edi, 900                                 ;599.13
        mov       DWORD PTR [-264+ecx+eax], 0                   ;599.13
                                ; LOE edx ecx ebx esi
.B10.487:                       ; Preds .B10.485 .B10.486
        test      ebx, ebx                                      ;600.13
        jbe       .B10.1042     ; Prob 10%                      ;600.13
                                ; LOE edx ecx ebx esi
.B10.488:                       ; Preds .B10.487
        xor       eax, eax                                      ;
        mov       DWORD PTR [esp], esi                          ;
        xor       edi, edi                                      ;
        mov       esi, eax                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.489:                       ; Preds .B10.489 .B10.488
        inc       edi                                           ;600.13
        mov       DWORD PTR [640+esi+ecx], eax                  ;600.13
        mov       DWORD PTR [1540+esi+ecx], eax                 ;600.13
        add       esi, 1800                                     ;600.13
        cmp       edi, ebx                                      ;600.13
        jb        .B10.489      ; Prob 63%                      ;600.13
                                ; LOE eax edx ecx ebx esi edi
.B10.490:                       ; Preds .B10.489
        mov       esi, DWORD PTR [esp]                          ;
        lea       eax, DWORD PTR [1+edi+edi]                    ;600.13
                                ; LOE eax edx ecx ebx esi
.B10.491:                       ; Preds .B10.490 .B10.1042
        lea       edi, DWORD PTR [-1+eax]                       ;600.13
        cmp       edx, edi                                      ;600.13
        jbe       .B10.493      ; Prob 10%                      ;600.13
                                ; LOE eax edx ecx ebx esi
.B10.492:                       ; Preds .B10.491
        mov       edi, esi                                      ;600.13
        add       eax, esi                                      ;600.13
        neg       edi                                           ;600.13
        add       edi, eax                                      ;600.13
        imul      eax, edi, 900                                 ;600.13
        mov       DWORD PTR [-260+ecx+eax], 0                   ;600.13
                                ; LOE edx ecx ebx esi
.B10.493:                       ; Preds .B10.491 .B10.492
        test      ebx, ebx                                      ;601.13
        jbe       .B10.1041     ; Prob 10%                      ;601.13
                                ; LOE edx ecx ebx esi
.B10.494:                       ; Preds .B10.493
        xor       eax, eax                                      ;
        mov       DWORD PTR [esp], esi                          ;
        xor       edi, edi                                      ;
        mov       esi, eax                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.495:                       ; Preds .B10.495 .B10.494
        inc       edi                                           ;601.13
        mov       DWORD PTR [644+esi+ecx], eax                  ;601.13
        mov       DWORD PTR [1544+esi+ecx], eax                 ;601.13
        add       esi, 1800                                     ;601.13
        cmp       edi, ebx                                      ;601.13
        jb        .B10.495      ; Prob 63%                      ;601.13
                                ; LOE eax edx ecx ebx esi edi
.B10.496:                       ; Preds .B10.495
        mov       esi, DWORD PTR [esp]                          ;
        lea       eax, DWORD PTR [1+edi+edi]                    ;601.13
                                ; LOE eax edx ecx ebx esi
.B10.497:                       ; Preds .B10.496 .B10.1041
        lea       edi, DWORD PTR [-1+eax]                       ;601.13
        cmp       edx, edi                                      ;601.13
        jbe       .B10.499      ; Prob 10%                      ;601.13
                                ; LOE eax edx ecx ebx esi
.B10.498:                       ; Preds .B10.497
        mov       edi, esi                                      ;601.13
        add       eax, esi                                      ;601.13
        neg       edi                                           ;601.13
        add       edi, eax                                      ;601.13
        imul      eax, edi, 900                                 ;601.13
        mov       DWORD PTR [-256+ecx+eax], 0                   ;601.13
                                ; LOE edx ecx ebx esi
.B10.499:                       ; Preds .B10.497 .B10.498
        test      ebx, ebx                                      ;602.13
        jbe       .B10.1040     ; Prob 10%                      ;602.13
                                ; LOE edx ecx ebx esi
.B10.500:                       ; Preds .B10.499
        xor       eax, eax                                      ;
        mov       DWORD PTR [esp], esi                          ;
        xor       edi, edi                                      ;
        mov       esi, eax                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.501:                       ; Preds .B10.501 .B10.500
        inc       edi                                           ;602.13
        mov       DWORD PTR [648+esi+ecx], eax                  ;602.13
        mov       DWORD PTR [1548+esi+ecx], eax                 ;602.13
        add       esi, 1800                                     ;602.13
        cmp       edi, ebx                                      ;602.13
        jb        .B10.501      ; Prob 63%                      ;602.13
                                ; LOE eax edx ecx ebx esi edi
.B10.502:                       ; Preds .B10.501
        mov       esi, DWORD PTR [esp]                          ;
        lea       eax, DWORD PTR [1+edi+edi]                    ;602.13
                                ; LOE eax edx ecx ebx esi
.B10.503:                       ; Preds .B10.502 .B10.1040
        lea       edi, DWORD PTR [-1+eax]                       ;602.13
        cmp       edx, edi                                      ;602.13
        jbe       .B10.505      ; Prob 10%                      ;602.13
                                ; LOE eax edx ecx ebx esi
.B10.504:                       ; Preds .B10.503
        mov       edi, esi                                      ;602.13
        add       eax, esi                                      ;602.13
        neg       edi                                           ;602.13
        add       edi, eax                                      ;602.13
        imul      eax, edi, 900                                 ;602.13
        mov       DWORD PTR [-252+ecx+eax], 0                   ;602.13
                                ; LOE edx ecx ebx esi
.B10.505:                       ; Preds .B10.503 .B10.504
        test      ebx, ebx                                      ;603.13
        jbe       .B10.1039     ; Prob 10%                      ;603.13
                                ; LOE edx ecx ebx esi
.B10.506:                       ; Preds .B10.505
        xor       eax, eax                                      ;
        mov       DWORD PTR [esp], esi                          ;
        xor       edi, edi                                      ;
        mov       esi, eax                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.507:                       ; Preds .B10.507 .B10.506
        inc       edi                                           ;603.13
        mov       DWORD PTR [652+esi+ecx], eax                  ;603.13
        mov       DWORD PTR [1552+esi+ecx], eax                 ;603.13
        add       esi, 1800                                     ;603.13
        cmp       edi, ebx                                      ;603.13
        jb        .B10.507      ; Prob 63%                      ;603.13
                                ; LOE eax edx ecx ebx esi edi
.B10.508:                       ; Preds .B10.507
        mov       esi, DWORD PTR [esp]                          ;
        lea       eax, DWORD PTR [1+edi+edi]                    ;603.13
                                ; LOE eax edx ecx ebx esi
.B10.509:                       ; Preds .B10.508 .B10.1039
        lea       edi, DWORD PTR [-1+eax]                       ;603.13
        cmp       edx, edi                                      ;603.13
        jbe       .B10.511      ; Prob 10%                      ;603.13
                                ; LOE eax edx ecx ebx esi
.B10.510:                       ; Preds .B10.509
        mov       edi, esi                                      ;603.13
        add       eax, esi                                      ;603.13
        neg       edi                                           ;603.13
        add       edi, eax                                      ;603.13
        imul      eax, edi, 900                                 ;603.13
        mov       DWORD PTR [-248+ecx+eax], 0                   ;603.13
                                ; LOE edx ecx ebx esi
.B10.511:                       ; Preds .B10.509 .B10.510
        test      ebx, ebx                                      ;604.13
        jbe       .B10.1038     ; Prob 10%                      ;604.13
                                ; LOE edx ecx ebx esi
.B10.512:                       ; Preds .B10.511
        xor       eax, eax                                      ;
        mov       DWORD PTR [esp], esi                          ;
        xor       edi, edi                                      ;
        mov       esi, eax                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.513:                       ; Preds .B10.513 .B10.512
        inc       edi                                           ;604.13
        mov       DWORD PTR [656+esi+ecx], eax                  ;604.13
        mov       DWORD PTR [1556+esi+ecx], eax                 ;604.13
        add       esi, 1800                                     ;604.13
        cmp       edi, ebx                                      ;604.13
        jb        .B10.513      ; Prob 63%                      ;604.13
                                ; LOE eax edx ecx ebx esi edi
.B10.514:                       ; Preds .B10.513
        mov       esi, DWORD PTR [esp]                          ;
        lea       eax, DWORD PTR [1+edi+edi]                    ;604.13
                                ; LOE eax edx ecx ebx esi
.B10.515:                       ; Preds .B10.514 .B10.1038
        lea       edi, DWORD PTR [-1+eax]                       ;604.13
        cmp       edx, edi                                      ;604.13
        jbe       .B10.517      ; Prob 10%                      ;604.13
                                ; LOE eax edx ecx ebx esi
.B10.516:                       ; Preds .B10.515
        mov       edi, esi                                      ;604.13
        add       eax, esi                                      ;604.13
        neg       edi                                           ;604.13
        add       edi, eax                                      ;604.13
        imul      eax, edi, 900                                 ;604.13
        mov       DWORD PTR [-244+ecx+eax], 0                   ;604.13
                                ; LOE edx ecx ebx esi
.B10.517:                       ; Preds .B10.515 .B10.516
        test      ebx, ebx                                      ;605.13
        jbe       .B10.1037     ; Prob 10%                      ;605.13
                                ; LOE edx ecx ebx esi
.B10.518:                       ; Preds .B10.517
        xor       eax, eax                                      ;
        mov       DWORD PTR [esp], esi                          ;
        xor       edi, edi                                      ;
        mov       esi, eax                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.519:                       ; Preds .B10.519 .B10.518
        inc       edi                                           ;605.13
        mov       DWORD PTR [660+esi+ecx], eax                  ;605.13
        mov       DWORD PTR [1560+esi+ecx], eax                 ;605.13
        add       esi, 1800                                     ;605.13
        cmp       edi, ebx                                      ;605.13
        jb        .B10.519      ; Prob 63%                      ;605.13
                                ; LOE eax edx ecx ebx esi edi
.B10.520:                       ; Preds .B10.519
        mov       esi, DWORD PTR [esp]                          ;
        lea       eax, DWORD PTR [1+edi+edi]                    ;605.13
                                ; LOE eax edx ecx ebx esi
.B10.521:                       ; Preds .B10.520 .B10.1037
        lea       edi, DWORD PTR [-1+eax]                       ;605.13
        cmp       edx, edi                                      ;605.13
        jbe       .B10.523      ; Prob 10%                      ;605.13
                                ; LOE eax edx ecx ebx esi
.B10.522:                       ; Preds .B10.521
        mov       edi, esi                                      ;605.13
        add       eax, esi                                      ;605.13
        neg       edi                                           ;605.13
        add       edi, eax                                      ;605.13
        imul      eax, edi, 900                                 ;605.13
        mov       DWORD PTR [-240+ecx+eax], 0                   ;605.13
                                ; LOE edx ecx ebx esi
.B10.523:                       ; Preds .B10.521 .B10.522
        test      ebx, ebx                                      ;606.13
        jbe       .B10.1036     ; Prob 10%                      ;606.13
                                ; LOE edx ecx ebx esi
.B10.524:                       ; Preds .B10.523
        xor       eax, eax                                      ;
        mov       DWORD PTR [esp], esi                          ;
        xor       edi, edi                                      ;
        mov       esi, eax                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.525:                       ; Preds .B10.525 .B10.524
        inc       edi                                           ;606.13
        mov       DWORD PTR [664+esi+ecx], eax                  ;606.13
        mov       DWORD PTR [1564+esi+ecx], eax                 ;606.13
        add       esi, 1800                                     ;606.13
        cmp       edi, ebx                                      ;606.13
        jb        .B10.525      ; Prob 63%                      ;606.13
                                ; LOE eax edx ecx ebx esi edi
.B10.526:                       ; Preds .B10.525
        mov       esi, DWORD PTR [esp]                          ;
        lea       eax, DWORD PTR [1+edi+edi]                    ;606.13
                                ; LOE eax edx ecx ebx esi
.B10.527:                       ; Preds .B10.526 .B10.1036
        lea       edi, DWORD PTR [-1+eax]                       ;606.13
        cmp       edx, edi                                      ;606.13
        jbe       .B10.529      ; Prob 10%                      ;606.13
                                ; LOE eax edx ecx ebx esi
.B10.528:                       ; Preds .B10.527
        mov       edi, esi                                      ;606.13
        add       eax, esi                                      ;606.13
        neg       edi                                           ;606.13
        add       edi, eax                                      ;606.13
        imul      eax, edi, 900                                 ;606.13
        mov       DWORD PTR [-236+ecx+eax], 0                   ;606.13
                                ; LOE edx ecx ebx esi
.B10.529:                       ; Preds .B10.527 .B10.528
        test      ebx, ebx                                      ;607.13
        jbe       .B10.1035     ; Prob 10%                      ;607.13
                                ; LOE edx ecx ebx esi
.B10.530:                       ; Preds .B10.529
        xor       eax, eax                                      ;
        mov       DWORD PTR [esp], esi                          ;
        xor       edi, edi                                      ;
        mov       esi, eax                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.531:                       ; Preds .B10.531 .B10.530
        inc       edi                                           ;607.13
        mov       BYTE PTR [668+esi+ecx], al                    ;607.13
        mov       BYTE PTR [1568+esi+ecx], al                   ;607.13
        add       esi, 1800                                     ;607.13
        cmp       edi, ebx                                      ;607.13
        jb        .B10.531      ; Prob 63%                      ;607.13
                                ; LOE eax edx ecx ebx esi edi
.B10.532:                       ; Preds .B10.531
        mov       esi, DWORD PTR [esp]                          ;
        lea       eax, DWORD PTR [1+edi+edi]                    ;607.13
                                ; LOE eax edx ecx ebx esi
.B10.533:                       ; Preds .B10.532 .B10.1035
        lea       edi, DWORD PTR [-1+eax]                       ;607.13
        cmp       edx, edi                                      ;607.13
        jbe       .B10.535      ; Prob 10%                      ;607.13
                                ; LOE eax edx ecx ebx esi
.B10.534:                       ; Preds .B10.533
        mov       edi, esi                                      ;607.13
        add       eax, esi                                      ;607.13
        neg       edi                                           ;607.13
        add       edi, eax                                      ;607.13
        imul      eax, edi, 900                                 ;607.13
        mov       BYTE PTR [-232+ecx+eax], 0                    ;607.13
                                ; LOE edx ecx ebx esi
.B10.535:                       ; Preds .B10.533 .B10.534
        test      ebx, ebx                                      ;608.13
        jbe       .B10.1034     ; Prob 10%                      ;608.13
                                ; LOE edx ecx ebx esi
.B10.536:                       ; Preds .B10.535
        xor       eax, eax                                      ;
        mov       DWORD PTR [esp], esi                          ;
        xor       edi, edi                                      ;
        mov       esi, eax                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.537:                       ; Preds .B10.537 .B10.536
        inc       edi                                           ;608.13
        mov       DWORD PTR [672+esi+ecx], eax                  ;608.13
        mov       DWORD PTR [1572+esi+ecx], eax                 ;608.13
        add       esi, 1800                                     ;608.13
        cmp       edi, ebx                                      ;608.13
        jb        .B10.537      ; Prob 63%                      ;608.13
                                ; LOE eax edx ecx ebx esi edi
.B10.538:                       ; Preds .B10.537
        mov       esi, DWORD PTR [esp]                          ;
        lea       eax, DWORD PTR [1+edi+edi]                    ;608.13
                                ; LOE eax edx ecx ebx esi
.B10.539:                       ; Preds .B10.538 .B10.1034
        lea       edi, DWORD PTR [-1+eax]                       ;608.13
        cmp       edx, edi                                      ;608.13
        jbe       .B10.541      ; Prob 10%                      ;608.13
                                ; LOE eax edx ecx ebx esi
.B10.540:                       ; Preds .B10.539
        mov       edi, esi                                      ;608.13
        add       eax, esi                                      ;608.13
        neg       edi                                           ;608.13
        add       edi, eax                                      ;608.13
        imul      eax, edi, 900                                 ;608.13
        mov       DWORD PTR [-228+ecx+eax], 0                   ;608.13
                                ; LOE edx ecx ebx esi
.B10.541:                       ; Preds .B10.539 .B10.540
        test      ebx, ebx                                      ;609.13
        jbe       .B10.1033     ; Prob 10%                      ;609.13
                                ; LOE edx ecx ebx esi
.B10.542:                       ; Preds .B10.541
        xor       eax, eax                                      ;
        mov       DWORD PTR [esp], esi                          ;
        xor       edi, edi                                      ;
        mov       esi, eax                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.543:                       ; Preds .B10.543 .B10.542
        inc       edi                                           ;609.13
        mov       DWORD PTR [676+esi+ecx], eax                  ;609.13
        mov       DWORD PTR [1576+esi+ecx], eax                 ;609.13
        add       esi, 1800                                     ;609.13
        cmp       edi, ebx                                      ;609.13
        jb        .B10.543      ; Prob 63%                      ;609.13
                                ; LOE eax edx ecx ebx esi edi
.B10.544:                       ; Preds .B10.543
        mov       esi, DWORD PTR [esp]                          ;
        lea       ebx, DWORD PTR [1+edi+edi]                    ;609.13
                                ; LOE edx ecx ebx esi
.B10.545:                       ; Preds .B10.544 .B10.1033
        lea       eax, DWORD PTR [-1+ebx]                       ;609.13
        cmp       edx, eax                                      ;609.13
        jbe       .B10.547      ; Prob 10%                      ;609.13
                                ; LOE ecx ebx esi
.B10.546:                       ; Preds .B10.545
        mov       eax, esi                                      ;609.13
        add       ebx, esi                                      ;609.13
        neg       eax                                           ;609.13
        add       eax, ebx                                      ;609.13
        imul      edx, eax, 900                                 ;609.13
        mov       DWORD PTR [-224+ecx+edx], 0                   ;609.13
                                ; LOE
.B10.547:                       ; Preds .B10.480 .B10.545 .B10.546
        mov       DWORD PTR [160+esp], 0                        ;611.13
        lea       eax, DWORD PTR [32+esp]                       ;611.13
        mov       DWORD PTR [32+esp], 20                        ;611.13
        mov       DWORD PTR [36+esp], OFFSET FLAT: __STRLITPACK_31 ;611.13
        mov       DWORD PTR [40+esp], 3                         ;611.13
        mov       DWORD PTR [44+esp], OFFSET FLAT: __STRLITPACK_30 ;611.13
        push      32                                            ;611.13
        push      eax                                           ;611.13
        push      OFFSET FLAT: __STRLITPACK_193.0.10            ;611.13
        push      -2088435965                                   ;611.13
        push      55                                            ;611.13
        lea       edx, DWORD PTR [180+esp]                      ;611.13
        push      edx                                           ;611.13
        call      _for_open                                     ;611.13
                                ; LOE eax
.B10.1386:                      ; Preds .B10.547
        add       esp, 24                                       ;611.13
                                ; LOE eax
.B10.548:                       ; Preds .B10.1386
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;611.13
        test      eax, eax                                      ;612.22
        jne       .B10.1211     ; Prob 5%                       ;612.22
                                ; LOE
.B10.549:                       ; Preds .B10.1412 .B10.548
        mov       DWORD PTR [160+esp], 0                        ;617.13
        lea       eax, DWORD PTR [272+esp]                      ;617.13
        mov       DWORD PTR [272+esp], OFFSET FLAT: _DYNUST_MAIN_MODULE_mp_NOOFFLOWMODEL ;617.13
        push      32                                            ;617.13
        push      eax                                           ;617.13
        push      OFFSET FLAT: __STRLITPACK_196.0.10            ;617.13
        push      -2088435965                                   ;617.13
        push      55                                            ;617.13
        lea       edx, DWORD PTR [180+esp]                      ;617.13
        push      edx                                           ;617.13
        call      _for_read_seq_lis                             ;617.13
                                ; LOE eax
.B10.550:                       ; Preds .B10.549
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;617.13
        xor       eax, eax                                      ;618.19
        mov       DWORD PTR [184+esp], eax                      ;618.19
        push      32                                            ;618.19
        push      eax                                           ;618.19
        push      OFFSET FLAT: __STRLITPACK_197.0.10            ;618.19
        push      -2088435968                                   ;618.19
        push      55                                            ;618.19
        lea       edx, DWORD PTR [204+esp]                      ;618.19
        push      edx                                           ;618.19
        call      _for_close                                    ;618.19
                                ; LOE
.B10.551:                       ; Preds .B10.550
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFFLOWMODEL] ;620.13
        xor       eax, eax                                      ;620.13
        test      esi, esi                                      ;620.13
        lea       ecx, DWORD PTR [272+esp]                      ;620.13
        push      32                                            ;620.13
        lea       edx, DWORD PTR [1+esi]                        ;620.13
        cmovl     edx, eax                                      ;620.13
        push      edx                                           ;620.13
        push      2                                             ;620.13
        push      ecx                                           ;620.13
        call      _for_check_mult_overflow                      ;620.13
                                ; LOE eax
.B10.552:                       ; Preds .B10.551
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC+12] ;620.13
        and       eax, 1                                        ;620.13
        and       edx, 1                                        ;620.13
        add       edx, edx                                      ;620.13
        shl       eax, 4                                        ;620.13
        or        edx, 1                                        ;620.13
        or        edx, eax                                      ;620.13
        or        edx, 262144                                   ;620.13
        push      edx                                           ;620.13
        push      OFFSET FLAT: _DYNUST_NETWORK_MODULE_mp_VKFUNC ;620.13
        push      DWORD PTR [296+esp]                           ;620.13
        call      _for_alloc_allocatable                        ;620.13
                                ; LOE eax
.B10.1389:                      ; Preds .B10.552
        add       esp, 76                                       ;620.13
        mov       ebx, eax                                      ;620.13
                                ; LOE ebx
.B10.553:                       ; Preds .B10.1389
        test      ebx, ebx                                      ;620.13
        je        .B10.556      ; Prob 50%                      ;620.13
                                ; LOE ebx
.B10.554:                       ; Preds .B10.553
        mov       DWORD PTR [160+esp], 0                        ;622.14
        lea       eax, DWORD PTR [144+esp]                      ;622.14
        mov       DWORD PTR [144+esp], 43                       ;622.14
        mov       DWORD PTR [148+esp], OFFSET FLAT: __STRLITPACK_26 ;622.14
        push      32                                            ;622.14
        push      eax                                           ;622.14
        push      OFFSET FLAT: __STRLITPACK_198.0.10            ;622.14
        push      -2088435968                                   ;622.14
        push      911                                           ;622.14
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], ebx ;620.13
        lea       edx, DWORD PTR [180+esp]                      ;622.14
        push      edx                                           ;622.14
        call      _for_write_seq_lis                            ;622.14
                                ; LOE
.B10.555:                       ; Preds .B10.554
        push      32                                            ;623.14
        xor       eax, eax                                      ;623.14
        push      eax                                           ;623.14
        push      eax                                           ;623.14
        push      -2088435968                                   ;623.14
        push      eax                                           ;623.14
        push      OFFSET FLAT: __STRLITPACK_199                 ;623.14
        call      _for_stop_core                                ;623.14
                                ; LOE
.B10.1390:                      ; Preds .B10.555
        add       esp, 48                                       ;623.14
        jmp       .B10.558      ; Prob 100%                     ;623.14
                                ; LOE
.B10.556:                       ; Preds .B10.553
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFFLOWMODEL] ;620.13
        xor       eax, eax                                      ;620.13
        test      ecx, ecx                                      ;620.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC+8], eax ;620.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC+12], 133 ;620.13
        lea       edi, DWORD PTR [1+ecx]                        ;620.13
        cmovl     edi, eax                                      ;620.13
        mov       esi, 32                                       ;620.13
        mov       edx, 1                                        ;620.13
        lea       eax, DWORD PTR [80+esp]                       ;620.13
        push      esi                                           ;620.13
        push      edi                                           ;620.13
        push      2                                             ;620.13
        push      eax                                           ;620.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC+4], esi ;620.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC+16], edx ;620.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC+32], edx ;620.42
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC+24], edi ;620.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC+28], esi ;620.13
        call      _for_check_mult_overflow                      ;620.13
                                ; LOE ebx
.B10.1391:                      ; Preds .B10.556
        add       esp, 16                                       ;620.13
                                ; LOE ebx
.B10.557:                       ; Preds .B10.1391
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], ebx ;620.13
                                ; LOE
.B10.558:                       ; Preds .B10.1390 .B10.557
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC+24] ;625.10
        test      edx, edx                                      ;625.10
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC+32] ;625.10
        jle       .B10.607      ; Prob 50%                      ;625.10
                                ; LOE edx esi
.B10.559:                       ; Preds .B10.558
        mov       ebx, edx                                      ;625.10
        shr       ebx, 31                                       ;625.10
        add       ebx, edx                                      ;625.10
        sar       ebx, 1                                        ;625.10
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC] ;625.10
        test      ebx, ebx                                      ;625.10
        jbe       .B10.1051     ; Prob 10%                      ;625.10
                                ; LOE edx ecx ebx esi
.B10.560:                       ; Preds .B10.559
        xor       eax, eax                                      ;
        mov       DWORD PTR [esp], esi                          ;
        xor       edi, edi                                      ;
                                ; LOE eax edx ecx ebx edi
.B10.561:                       ; Preds .B10.561 .B10.560
        mov       esi, edi                                      ;625.10
        inc       edi                                           ;625.10
        shl       esi, 6                                        ;625.10
        cmp       edi, ebx                                      ;625.10
        mov       DWORD PTR [ecx+esi], eax                      ;625.10
        mov       DWORD PTR [32+ecx+esi], eax                   ;625.10
        jb        .B10.561      ; Prob 63%                      ;625.10
                                ; LOE eax edx ecx ebx edi
.B10.562:                       ; Preds .B10.561
        mov       esi, DWORD PTR [esp]                          ;
        lea       eax, DWORD PTR [1+edi+edi]                    ;625.10
                                ; LOE eax edx ecx ebx esi
.B10.563:                       ; Preds .B10.562 .B10.1051
        lea       edi, DWORD PTR [-1+eax]                       ;625.10
        cmp       edx, edi                                      ;625.10
        jbe       .B10.565      ; Prob 10%                      ;625.10
                                ; LOE eax edx ecx ebx esi
.B10.564:                       ; Preds .B10.563
        mov       edi, esi                                      ;625.10
        add       eax, esi                                      ;625.10
        neg       edi                                           ;625.10
        add       edi, eax                                      ;625.10
        shl       edi, 5                                        ;625.10
        mov       DWORD PTR [-32+ecx+edi], 0                    ;625.10
                                ; LOE edx ecx ebx esi
.B10.565:                       ; Preds .B10.563 .B10.564
        test      ebx, ebx                                      ;626.10
        jbe       .B10.1050     ; Prob 10%                      ;626.10
                                ; LOE edx ecx ebx esi
.B10.566:                       ; Preds .B10.565
        xor       eax, eax                                      ;
        mov       DWORD PTR [esp], esi                          ;
        xor       edi, edi                                      ;
                                ; LOE eax edx ecx ebx edi
.B10.567:                       ; Preds .B10.567 .B10.566
        mov       esi, edi                                      ;626.10
        inc       edi                                           ;626.10
        shl       esi, 6                                        ;626.10
        cmp       edi, ebx                                      ;626.10
        mov       DWORD PTR [4+ecx+esi], eax                    ;626.10
        mov       DWORD PTR [36+ecx+esi], eax                   ;626.10
        jb        .B10.567      ; Prob 63%                      ;626.10
                                ; LOE eax edx ecx ebx edi
.B10.568:                       ; Preds .B10.567
        mov       esi, DWORD PTR [esp]                          ;
        lea       eax, DWORD PTR [1+edi+edi]                    ;626.10
                                ; LOE eax edx ecx ebx esi
.B10.569:                       ; Preds .B10.568 .B10.1050
        lea       edi, DWORD PTR [-1+eax]                       ;626.10
        cmp       edx, edi                                      ;626.10
        jbe       .B10.571      ; Prob 10%                      ;626.10
                                ; LOE eax edx ecx ebx esi
.B10.570:                       ; Preds .B10.569
        mov       edi, esi                                      ;626.10
        add       eax, esi                                      ;626.10
        neg       edi                                           ;626.10
        add       edi, eax                                      ;626.10
        shl       edi, 5                                        ;626.10
        mov       DWORD PTR [-28+ecx+edi], 0                    ;626.10
                                ; LOE edx ecx ebx esi
.B10.571:                       ; Preds .B10.569 .B10.570
        test      ebx, ebx                                      ;627.10
        jbe       .B10.1049     ; Prob 10%                      ;627.10
                                ; LOE edx ecx ebx esi
.B10.572:                       ; Preds .B10.571
        xor       eax, eax                                      ;
        mov       DWORD PTR [esp], esi                          ;
        xor       edi, edi                                      ;
                                ; LOE eax edx ecx ebx edi
.B10.573:                       ; Preds .B10.573 .B10.572
        mov       esi, edi                                      ;627.10
        inc       edi                                           ;627.10
        shl       esi, 6                                        ;627.10
        cmp       edi, ebx                                      ;627.10
        mov       DWORD PTR [8+ecx+esi], eax                    ;627.10
        mov       DWORD PTR [40+ecx+esi], eax                   ;627.10
        jb        .B10.573      ; Prob 63%                      ;627.10
                                ; LOE eax edx ecx ebx edi
.B10.574:                       ; Preds .B10.573
        mov       esi, DWORD PTR [esp]                          ;
        lea       eax, DWORD PTR [1+edi+edi]                    ;627.10
                                ; LOE eax edx ecx ebx esi
.B10.575:                       ; Preds .B10.574 .B10.1049
        lea       edi, DWORD PTR [-1+eax]                       ;627.10
        cmp       edx, edi                                      ;627.10
        jbe       .B10.577      ; Prob 10%                      ;627.10
                                ; LOE eax edx ecx ebx esi
.B10.576:                       ; Preds .B10.575
        mov       edi, esi                                      ;627.10
        add       eax, esi                                      ;627.10
        neg       edi                                           ;627.10
        add       edi, eax                                      ;627.10
        shl       edi, 5                                        ;627.10
        mov       DWORD PTR [-24+ecx+edi], 0                    ;627.10
                                ; LOE edx ecx ebx esi
.B10.577:                       ; Preds .B10.575 .B10.576
        test      ebx, ebx                                      ;628.10
        jbe       .B10.1048     ; Prob 10%                      ;628.10
                                ; LOE edx ecx ebx esi
.B10.578:                       ; Preds .B10.577
        xor       eax, eax                                      ;
        mov       DWORD PTR [esp], esi                          ;
        xor       edi, edi                                      ;
                                ; LOE eax edx ecx ebx edi
.B10.579:                       ; Preds .B10.579 .B10.578
        mov       esi, edi                                      ;628.10
        inc       edi                                           ;628.10
        shl       esi, 6                                        ;628.10
        cmp       edi, ebx                                      ;628.10
        mov       DWORD PTR [12+ecx+esi], eax                   ;628.10
        mov       DWORD PTR [44+ecx+esi], eax                   ;628.10
        jb        .B10.579      ; Prob 63%                      ;628.10
                                ; LOE eax edx ecx ebx edi
.B10.580:                       ; Preds .B10.579
        mov       esi, DWORD PTR [esp]                          ;
        lea       eax, DWORD PTR [1+edi+edi]                    ;628.10
                                ; LOE eax edx ecx ebx esi
.B10.581:                       ; Preds .B10.580 .B10.1048
        lea       edi, DWORD PTR [-1+eax]                       ;628.10
        cmp       edx, edi                                      ;628.10
        jbe       .B10.583      ; Prob 10%                      ;628.10
                                ; LOE eax edx ecx ebx esi
.B10.582:                       ; Preds .B10.581
        mov       edi, esi                                      ;628.10
        add       eax, esi                                      ;628.10
        neg       edi                                           ;628.10
        add       edi, eax                                      ;628.10
        shl       edi, 5                                        ;628.10
        mov       DWORD PTR [-20+ecx+edi], 0                    ;628.10
                                ; LOE edx ecx ebx esi
.B10.583:                       ; Preds .B10.581 .B10.582
        test      ebx, ebx                                      ;629.10
        jbe       .B10.1047     ; Prob 10%                      ;629.10
                                ; LOE edx ecx ebx esi
.B10.584:                       ; Preds .B10.583
        xor       eax, eax                                      ;
        mov       DWORD PTR [esp], esi                          ;
        xor       edi, edi                                      ;
        mov       esi, eax                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.585:                       ; Preds .B10.585 .B10.584
        inc       edi                                           ;629.10
        mov       DWORD PTR [16+esi+ecx], eax                   ;629.10
        mov       DWORD PTR [48+esi+ecx], eax                   ;629.10
        add       esi, 64                                       ;629.10
        cmp       edi, ebx                                      ;629.10
        jb        .B10.585      ; Prob 63%                      ;629.10
                                ; LOE eax edx ecx ebx esi edi
.B10.586:                       ; Preds .B10.585
        mov       esi, DWORD PTR [esp]                          ;
        lea       eax, DWORD PTR [1+edi+edi]                    ;629.10
                                ; LOE eax edx ecx ebx esi
.B10.587:                       ; Preds .B10.586 .B10.1047
        lea       edi, DWORD PTR [-1+eax]                       ;629.10
        cmp       edx, edi                                      ;629.10
        jbe       .B10.589      ; Prob 10%                      ;629.10
                                ; LOE eax edx ecx ebx esi
.B10.588:                       ; Preds .B10.587
        mov       edi, esi                                      ;629.10
        add       eax, esi                                      ;629.10
        neg       edi                                           ;629.10
        add       edi, eax                                      ;629.10
        shl       edi, 5                                        ;629.10
        mov       DWORD PTR [-16+ecx+edi], 0                    ;629.10
                                ; LOE edx ecx ebx esi
.B10.589:                       ; Preds .B10.587 .B10.588
        test      ebx, ebx                                      ;630.10
        jbe       .B10.1046     ; Prob 10%                      ;630.10
                                ; LOE edx ecx ebx esi
.B10.590:                       ; Preds .B10.589
        xor       eax, eax                                      ;
        mov       DWORD PTR [esp], esi                          ;
        xor       edi, edi                                      ;
        mov       esi, eax                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.591:                       ; Preds .B10.591 .B10.590
        inc       edi                                           ;630.10
        mov       DWORD PTR [20+esi+ecx], eax                   ;630.10
        mov       DWORD PTR [52+esi+ecx], eax                   ;630.10
        add       esi, 64                                       ;630.10
        cmp       edi, ebx                                      ;630.10
        jb        .B10.591      ; Prob 63%                      ;630.10
                                ; LOE eax edx ecx ebx esi edi
.B10.592:                       ; Preds .B10.591
        mov       esi, DWORD PTR [esp]                          ;
        lea       eax, DWORD PTR [1+edi+edi]                    ;630.10
                                ; LOE eax edx ecx ebx esi
.B10.593:                       ; Preds .B10.592 .B10.1046
        lea       edi, DWORD PTR [-1+eax]                       ;630.10
        cmp       edx, edi                                      ;630.10
        jbe       .B10.595      ; Prob 10%                      ;630.10
                                ; LOE eax edx ecx ebx esi
.B10.594:                       ; Preds .B10.593
        mov       edi, esi                                      ;630.10
        add       eax, esi                                      ;630.10
        neg       edi                                           ;630.10
        add       edi, eax                                      ;630.10
        shl       edi, 5                                        ;630.10
        mov       DWORD PTR [-12+ecx+edi], 0                    ;630.10
                                ; LOE edx ecx ebx esi
.B10.595:                       ; Preds .B10.593 .B10.594
        test      ebx, ebx                                      ;631.10
        jbe       .B10.1045     ; Prob 10%                      ;631.10
                                ; LOE edx ecx ebx esi
.B10.596:                       ; Preds .B10.595
        xor       eax, eax                                      ;
        mov       DWORD PTR [esp], esi                          ;
        xor       edi, edi                                      ;
                                ; LOE eax edx ecx ebx edi
.B10.597:                       ; Preds .B10.597 .B10.596
        mov       esi, edi                                      ;631.10
        inc       edi                                           ;631.10
        shl       esi, 6                                        ;631.10
        cmp       edi, ebx                                      ;631.10
        mov       DWORD PTR [24+ecx+esi], eax                   ;631.10
        mov       DWORD PTR [56+ecx+esi], eax                   ;631.10
        jb        .B10.597      ; Prob 63%                      ;631.10
                                ; LOE eax edx ecx ebx edi
.B10.598:                       ; Preds .B10.597
        mov       esi, DWORD PTR [esp]                          ;
        lea       eax, DWORD PTR [1+edi+edi]                    ;631.10
                                ; LOE eax edx ecx ebx esi
.B10.599:                       ; Preds .B10.598 .B10.1045
        lea       edi, DWORD PTR [-1+eax]                       ;631.10
        cmp       edx, edi                                      ;631.10
        jbe       .B10.601      ; Prob 10%                      ;631.10
                                ; LOE eax edx ecx ebx esi
.B10.600:                       ; Preds .B10.599
        mov       edi, esi                                      ;631.10
        add       eax, esi                                      ;631.10
        neg       edi                                           ;631.10
        add       edi, eax                                      ;631.10
        shl       edi, 5                                        ;631.10
        mov       DWORD PTR [-8+ecx+edi], 0                     ;631.10
                                ; LOE edx ecx ebx esi
.B10.601:                       ; Preds .B10.599 .B10.600
        test      ebx, ebx                                      ;632.10
        jbe       .B10.1044     ; Prob 10%                      ;632.10
                                ; LOE edx ecx ebx esi
.B10.602:                       ; Preds .B10.601
        xor       eax, eax                                      ;
        mov       DWORD PTR [esp], esi                          ;
        xor       edi, edi                                      ;
                                ; LOE eax edx ecx ebx edi
.B10.603:                       ; Preds .B10.603 .B10.602
        mov       esi, edi                                      ;632.10
        inc       edi                                           ;632.10
        shl       esi, 6                                        ;632.10
        cmp       edi, ebx                                      ;632.10
        mov       DWORD PTR [28+ecx+esi], eax                   ;632.10
        mov       DWORD PTR [60+ecx+esi], eax                   ;632.10
        jb        .B10.603      ; Prob 63%                      ;632.10
                                ; LOE eax edx ecx ebx edi
.B10.604:                       ; Preds .B10.603
        mov       esi, DWORD PTR [esp]                          ;
        lea       ebx, DWORD PTR [1+edi+edi]                    ;632.10
                                ; LOE edx ecx ebx esi
.B10.605:                       ; Preds .B10.604 .B10.1044
        lea       eax, DWORD PTR [-1+ebx]                       ;632.10
        cmp       edx, eax                                      ;632.10
        jbe       .B10.607      ; Prob 10%                      ;632.10
                                ; LOE ecx ebx esi
.B10.606:                       ; Preds .B10.605
        mov       eax, esi                                      ;632.10
        add       ebx, esi                                      ;632.10
        neg       eax                                           ;632.10
        add       eax, ebx                                      ;632.10
        shl       eax, 5                                        ;632.10
        mov       DWORD PTR [-4+ecx+eax], 0                     ;632.10
                                ; LOE
.B10.607:                       ; Preds .B10.558 .B10.605 .B10.606
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+24] ;634.13
        test      edx, edx                                      ;634.13
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;634.13
        jle       .B10.764      ; Prob 50%                      ;634.13
                                ; LOE edx esi
.B10.608:                       ; Preds .B10.607
        mov       ebx, edx                                      ;634.13
        shr       ebx, 31                                       ;634.13
        add       ebx, edx                                      ;634.13
        sar       ebx, 1                                        ;634.13
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;634.13
        test      ebx, ebx                                      ;634.13
        jbe       .B10.1077     ; Prob 10%                      ;634.13
                                ; LOE edx ecx ebx esi
.B10.609:                       ; Preds .B10.608
        xor       eax, eax                                      ;
        mov       DWORD PTR [esp], esi                          ;
        xor       edi, edi                                      ;
        mov       esi, eax                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.610:                       ; Preds .B10.610 .B10.609
        inc       edi                                           ;634.13
        mov       BYTE PTR [680+esi+ecx], al                    ;634.13
        mov       BYTE PTR [1580+esi+ecx], al                   ;634.13
        add       esi, 1800                                     ;634.13
        cmp       edi, ebx                                      ;634.13
        jb        .B10.610      ; Prob 63%                      ;634.13
                                ; LOE eax edx ecx ebx esi edi
.B10.611:                       ; Preds .B10.610
        mov       esi, DWORD PTR [esp]                          ;
        lea       eax, DWORD PTR [1+edi+edi]                    ;634.13
                                ; LOE eax edx ecx ebx esi
.B10.612:                       ; Preds .B10.611 .B10.1077
        lea       edi, DWORD PTR [-1+eax]                       ;634.13
        cmp       edx, edi                                      ;634.13
        jbe       .B10.614      ; Prob 10%                      ;634.13
                                ; LOE eax edx ecx ebx esi
.B10.613:                       ; Preds .B10.612
        mov       edi, esi                                      ;634.13
        add       eax, esi                                      ;634.13
        neg       edi                                           ;634.13
        add       edi, eax                                      ;634.13
        imul      eax, edi, 900                                 ;634.13
        mov       BYTE PTR [-220+ecx+eax], 0                    ;634.13
                                ; LOE edx ecx ebx esi
.B10.614:                       ; Preds .B10.612 .B10.613
        test      ebx, ebx                                      ;636.13
        jbe       .B10.1076     ; Prob 10%                      ;636.13
                                ; LOE edx ecx ebx esi
.B10.615:                       ; Preds .B10.614
        xor       eax, eax                                      ;
        mov       DWORD PTR [esp], esi                          ;
        xor       edi, edi                                      ;
        mov       esi, eax                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.616:                       ; Preds .B10.616 .B10.615
        inc       edi                                           ;636.13
        mov       BYTE PTR [681+esi+ecx], al                    ;636.13
        mov       BYTE PTR [1581+esi+ecx], al                   ;636.13
        add       esi, 1800                                     ;636.13
        cmp       edi, ebx                                      ;636.13
        jb        .B10.616      ; Prob 63%                      ;636.13
                                ; LOE eax edx ecx ebx esi edi
.B10.617:                       ; Preds .B10.616
        mov       esi, DWORD PTR [esp]                          ;
        lea       eax, DWORD PTR [1+edi+edi]                    ;636.13
                                ; LOE eax edx ecx ebx esi
.B10.618:                       ; Preds .B10.617 .B10.1076
        lea       edi, DWORD PTR [-1+eax]                       ;636.13
        cmp       edx, edi                                      ;636.13
        jbe       .B10.620      ; Prob 10%                      ;636.13
                                ; LOE eax edx ecx ebx esi
.B10.619:                       ; Preds .B10.618
        mov       edi, esi                                      ;636.13
        add       eax, esi                                      ;636.13
        neg       edi                                           ;636.13
        add       edi, eax                                      ;636.13
        imul      eax, edi, 900                                 ;636.13
        mov       BYTE PTR [-219+ecx+eax], 0                    ;636.13
                                ; LOE edx ecx ebx esi
.B10.620:                       ; Preds .B10.618 .B10.619
        test      ebx, ebx                                      ;637.13
        jbe       .B10.1075     ; Prob 10%                      ;637.13
                                ; LOE edx ecx ebx esi
.B10.621:                       ; Preds .B10.620
        xor       eax, eax                                      ;
        mov       DWORD PTR [esp], esi                          ;
        xor       edi, edi                                      ;
        mov       esi, eax                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.622:                       ; Preds .B10.622 .B10.621
        inc       edi                                           ;637.13
        mov       WORD PTR [682+esi+ecx], ax                    ;637.13
        mov       WORD PTR [1582+esi+ecx], ax                   ;637.13
        add       esi, 1800                                     ;637.13
        cmp       edi, ebx                                      ;637.13
        jb        .B10.622      ; Prob 63%                      ;637.13
                                ; LOE eax edx ecx ebx esi edi
.B10.623:                       ; Preds .B10.622
        mov       esi, DWORD PTR [esp]                          ;
        lea       eax, DWORD PTR [1+edi+edi]                    ;637.13
                                ; LOE eax edx ecx ebx esi
.B10.624:                       ; Preds .B10.623 .B10.1075
        lea       edi, DWORD PTR [-1+eax]                       ;637.13
        cmp       edx, edi                                      ;637.13
        jbe       .B10.626      ; Prob 10%                      ;637.13
                                ; LOE eax edx ecx ebx esi
.B10.625:                       ; Preds .B10.624
        mov       edi, esi                                      ;637.13
        add       eax, esi                                      ;637.13
        neg       edi                                           ;637.13
        add       edi, eax                                      ;637.13
        xor       eax, eax                                      ;637.13
        imul      edi, edi, 900                                 ;637.13
        mov       WORD PTR [-218+ecx+edi], ax                   ;637.13
                                ; LOE edx ecx ebx esi
.B10.626:                       ; Preds .B10.624 .B10.625
        test      ebx, ebx                                      ;638.13
        jbe       .B10.1074     ; Prob 10%                      ;638.13
                                ; LOE edx ecx ebx esi
.B10.627:                       ; Preds .B10.626
        xor       eax, eax                                      ;
        mov       DWORD PTR [esp], esi                          ;
        xor       edi, edi                                      ;
        mov       esi, eax                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.628:                       ; Preds .B10.628 .B10.627
        inc       edi                                           ;638.13
        mov       WORD PTR [684+esi+ecx], ax                    ;638.13
        mov       WORD PTR [1584+esi+ecx], ax                   ;638.13
        add       esi, 1800                                     ;638.13
        cmp       edi, ebx                                      ;638.13
        jb        .B10.628      ; Prob 63%                      ;638.13
                                ; LOE eax edx ecx ebx esi edi
.B10.629:                       ; Preds .B10.628
        mov       esi, DWORD PTR [esp]                          ;
        lea       eax, DWORD PTR [1+edi+edi]                    ;638.13
                                ; LOE eax edx ecx ebx esi
.B10.630:                       ; Preds .B10.629 .B10.1074
        lea       edi, DWORD PTR [-1+eax]                       ;638.13
        cmp       edx, edi                                      ;638.13
        jbe       .B10.632      ; Prob 10%                      ;638.13
                                ; LOE eax edx ecx ebx esi
.B10.631:                       ; Preds .B10.630
        mov       edi, esi                                      ;638.13
        add       eax, esi                                      ;638.13
        neg       edi                                           ;638.13
        add       edi, eax                                      ;638.13
        xor       eax, eax                                      ;638.13
        imul      edi, edi, 900                                 ;638.13
        mov       WORD PTR [-216+ecx+edi], ax                   ;638.13
                                ; LOE edx ecx ebx esi
.B10.632:                       ; Preds .B10.630 .B10.631
        test      ebx, ebx                                      ;639.13
        jbe       .B10.1073     ; Prob 10%                      ;639.13
                                ; LOE edx ecx ebx esi
.B10.633:                       ; Preds .B10.632
        xor       eax, eax                                      ;
        mov       DWORD PTR [esp], esi                          ;
        xor       edi, edi                                      ;
        mov       esi, eax                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.634:                       ; Preds .B10.634 .B10.633
        inc       edi                                           ;639.13
        mov       WORD PTR [686+esi+ecx], ax                    ;639.13
        mov       WORD PTR [1586+esi+ecx], ax                   ;639.13
        add       esi, 1800                                     ;639.13
        cmp       edi, ebx                                      ;639.13
        jb        .B10.634      ; Prob 63%                      ;639.13
                                ; LOE eax edx ecx ebx esi edi
.B10.635:                       ; Preds .B10.634
        mov       esi, DWORD PTR [esp]                          ;
        lea       eax, DWORD PTR [1+edi+edi]                    ;639.13
                                ; LOE eax edx ecx ebx esi
.B10.636:                       ; Preds .B10.635 .B10.1073
        lea       edi, DWORD PTR [-1+eax]                       ;639.13
        cmp       edx, edi                                      ;639.13
        jbe       .B10.638      ; Prob 10%                      ;639.13
                                ; LOE eax edx ecx ebx esi
.B10.637:                       ; Preds .B10.636
        mov       edi, esi                                      ;639.13
        add       eax, esi                                      ;639.13
        neg       edi                                           ;639.13
        add       edi, eax                                      ;639.13
        xor       eax, eax                                      ;639.13
        imul      edi, edi, 900                                 ;639.13
        mov       WORD PTR [-214+ecx+edi], ax                   ;639.13
                                ; LOE edx ecx ebx esi
.B10.638:                       ; Preds .B10.636 .B10.637
        test      ebx, ebx                                      ;640.13
        jbe       .B10.1072     ; Prob 10%                      ;640.13
                                ; LOE edx ecx ebx esi
.B10.639:                       ; Preds .B10.638
        xor       eax, eax                                      ;
        mov       DWORD PTR [esp], esi                          ;
        xor       edi, edi                                      ;
        mov       esi, eax                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.640:                       ; Preds .B10.640 .B10.639
        inc       edi                                           ;640.13
        mov       DWORD PTR [688+esi+ecx], eax                  ;640.13
        mov       DWORD PTR [1588+esi+ecx], eax                 ;640.13
        add       esi, 1800                                     ;640.13
        cmp       edi, ebx                                      ;640.13
        jb        .B10.640      ; Prob 63%                      ;640.13
                                ; LOE eax edx ecx ebx esi edi
.B10.641:                       ; Preds .B10.640
        mov       esi, DWORD PTR [esp]                          ;
        lea       eax, DWORD PTR [1+edi+edi]                    ;640.13
                                ; LOE eax edx ecx ebx esi
.B10.642:                       ; Preds .B10.641 .B10.1072
        lea       edi, DWORD PTR [-1+eax]                       ;640.13
        cmp       edx, edi                                      ;640.13
        jbe       .B10.644      ; Prob 10%                      ;640.13
                                ; LOE eax edx ecx ebx esi
.B10.643:                       ; Preds .B10.642
        mov       edi, esi                                      ;640.13
        add       eax, esi                                      ;640.13
        neg       edi                                           ;640.13
        add       edi, eax                                      ;640.13
        imul      eax, edi, 900                                 ;640.13
        mov       DWORD PTR [-212+ecx+eax], 0                   ;640.13
                                ; LOE edx ecx ebx esi
.B10.644:                       ; Preds .B10.642 .B10.643
        test      ebx, ebx                                      ;641.13
        jbe       .B10.1071     ; Prob 10%                      ;641.13
                                ; LOE edx ecx ebx esi
.B10.645:                       ; Preds .B10.644
        xor       eax, eax                                      ;
        mov       DWORD PTR [esp], esi                          ;
        xor       edi, edi                                      ;
        mov       esi, eax                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.646:                       ; Preds .B10.646 .B10.645
        inc       edi                                           ;641.13
        mov       WORD PTR [692+esi+ecx], ax                    ;641.13
        mov       WORD PTR [1592+esi+ecx], ax                   ;641.13
        add       esi, 1800                                     ;641.13
        cmp       edi, ebx                                      ;641.13
        jb        .B10.646      ; Prob 63%                      ;641.13
                                ; LOE eax edx ecx ebx esi edi
.B10.647:                       ; Preds .B10.646
        mov       esi, DWORD PTR [esp]                          ;
        lea       eax, DWORD PTR [1+edi+edi]                    ;641.13
                                ; LOE eax edx ecx ebx esi
.B10.648:                       ; Preds .B10.647 .B10.1071
        lea       edi, DWORD PTR [-1+eax]                       ;641.13
        cmp       edx, edi                                      ;641.13
        jbe       .B10.650      ; Prob 10%                      ;641.13
                                ; LOE eax edx ecx ebx esi
.B10.649:                       ; Preds .B10.648
        mov       edi, esi                                      ;641.13
        add       eax, esi                                      ;641.13
        neg       edi                                           ;641.13
        add       edi, eax                                      ;641.13
        xor       eax, eax                                      ;641.13
        imul      edi, edi, 900                                 ;641.13
        mov       WORD PTR [-208+ecx+edi], ax                   ;641.13
                                ; LOE edx ecx ebx esi
.B10.650:                       ; Preds .B10.648 .B10.649
        test      ebx, ebx                                      ;642.13
        jbe       .B10.1070     ; Prob 10%                      ;642.13
                                ; LOE edx ecx ebx esi
.B10.651:                       ; Preds .B10.650
        xor       eax, eax                                      ;
        mov       DWORD PTR [esp], esi                          ;
        xor       edi, edi                                      ;
        mov       esi, eax                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.652:                       ; Preds .B10.652 .B10.651
        inc       edi                                           ;642.13
        mov       DWORD PTR [696+esi+ecx], eax                  ;642.13
        mov       DWORD PTR [1596+esi+ecx], eax                 ;642.13
        add       esi, 1800                                     ;642.13
        cmp       edi, ebx                                      ;642.13
        jb        .B10.652      ; Prob 63%                      ;642.13
                                ; LOE eax edx ecx ebx esi edi
.B10.653:                       ; Preds .B10.652
        mov       esi, DWORD PTR [esp]                          ;
        lea       eax, DWORD PTR [1+edi+edi]                    ;642.13
                                ; LOE eax edx ecx ebx esi
.B10.654:                       ; Preds .B10.653 .B10.1070
        lea       edi, DWORD PTR [-1+eax]                       ;642.13
        cmp       edx, edi                                      ;642.13
        jbe       .B10.656      ; Prob 10%                      ;642.13
                                ; LOE eax edx ecx ebx esi
.B10.655:                       ; Preds .B10.654
        mov       edi, esi                                      ;642.13
        add       eax, esi                                      ;642.13
        neg       edi                                           ;642.13
        add       edi, eax                                      ;642.13
        imul      eax, edi, 900                                 ;642.13
        mov       DWORD PTR [-204+ecx+eax], 0                   ;642.13
                                ; LOE edx ecx ebx esi
.B10.656:                       ; Preds .B10.654 .B10.655
        test      ebx, ebx                                      ;643.13
        jbe       .B10.1069     ; Prob 10%                      ;643.13
                                ; LOE edx ecx ebx esi
.B10.657:                       ; Preds .B10.656
        xor       eax, eax                                      ;
        mov       DWORD PTR [esp], esi                          ;
        xor       edi, edi                                      ;
        mov       esi, eax                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.658:                       ; Preds .B10.658 .B10.657
        inc       edi                                           ;643.13
        mov       DWORD PTR [700+esi+ecx], eax                  ;643.13
        mov       DWORD PTR [1600+esi+ecx], eax                 ;643.13
        add       esi, 1800                                     ;643.13
        cmp       edi, ebx                                      ;643.13
        jb        .B10.658      ; Prob 63%                      ;643.13
                                ; LOE eax edx ecx ebx esi edi
.B10.659:                       ; Preds .B10.658
        mov       esi, DWORD PTR [esp]                          ;
        lea       eax, DWORD PTR [1+edi+edi]                    ;643.13
                                ; LOE eax edx ecx ebx esi
.B10.660:                       ; Preds .B10.659 .B10.1069
        lea       edi, DWORD PTR [-1+eax]                       ;643.13
        cmp       edx, edi                                      ;643.13
        jbe       .B10.662      ; Prob 10%                      ;643.13
                                ; LOE eax edx ecx ebx esi
.B10.661:                       ; Preds .B10.660
        mov       edi, esi                                      ;643.13
        add       eax, esi                                      ;643.13
        neg       edi                                           ;643.13
        add       edi, eax                                      ;643.13
        imul      eax, edi, 900                                 ;643.13
        mov       DWORD PTR [-200+ecx+eax], 0                   ;643.13
                                ; LOE edx ecx ebx esi
.B10.662:                       ; Preds .B10.660 .B10.661
        test      ebx, ebx                                      ;644.13
        jbe       .B10.1068     ; Prob 10%                      ;644.13
                                ; LOE edx ecx ebx esi
.B10.663:                       ; Preds .B10.662
        xor       eax, eax                                      ;
        mov       DWORD PTR [esp], esi                          ;
        xor       edi, edi                                      ;
        mov       esi, eax                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.664:                       ; Preds .B10.664 .B10.663
        inc       edi                                           ;644.13
        mov       DWORD PTR [704+esi+ecx], eax                  ;644.13
        mov       DWORD PTR [1604+esi+ecx], eax                 ;644.13
        add       esi, 1800                                     ;644.13
        cmp       edi, ebx                                      ;644.13
        jb        .B10.664      ; Prob 63%                      ;644.13
                                ; LOE eax edx ecx ebx esi edi
.B10.665:                       ; Preds .B10.664
        mov       esi, DWORD PTR [esp]                          ;
        lea       eax, DWORD PTR [1+edi+edi]                    ;644.13
                                ; LOE eax edx ecx ebx esi
.B10.666:                       ; Preds .B10.665 .B10.1068
        lea       edi, DWORD PTR [-1+eax]                       ;644.13
        cmp       edx, edi                                      ;644.13
        jbe       .B10.668      ; Prob 10%                      ;644.13
                                ; LOE eax edx ecx ebx esi
.B10.667:                       ; Preds .B10.666
        mov       edi, esi                                      ;644.13
        add       eax, esi                                      ;644.13
        neg       edi                                           ;644.13
        add       edi, eax                                      ;644.13
        imul      eax, edi, 900                                 ;644.13
        mov       DWORD PTR [-196+ecx+eax], 0                   ;644.13
                                ; LOE edx ecx ebx esi
.B10.668:                       ; Preds .B10.666 .B10.667
        test      ebx, ebx                                      ;645.13
        jbe       .B10.1067     ; Prob 10%                      ;645.13
                                ; LOE edx ecx ebx esi
.B10.669:                       ; Preds .B10.668
        xor       eax, eax                                      ;
        mov       DWORD PTR [esp], esi                          ;
        xor       edi, edi                                      ;
        mov       esi, eax                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.670:                       ; Preds .B10.670 .B10.669
        inc       edi                                           ;645.13
        mov       DWORD PTR [744+esi+ecx], eax                  ;645.13
        mov       DWORD PTR [1644+esi+ecx], eax                 ;645.13
        add       esi, 1800                                     ;645.13
        cmp       edi, ebx                                      ;645.13
        jb        .B10.670      ; Prob 63%                      ;645.13
                                ; LOE eax edx ecx ebx esi edi
.B10.671:                       ; Preds .B10.670
        mov       esi, DWORD PTR [esp]                          ;
        lea       eax, DWORD PTR [1+edi+edi]                    ;645.13
                                ; LOE eax edx ecx ebx esi
.B10.672:                       ; Preds .B10.671 .B10.1067
        lea       edi, DWORD PTR [-1+eax]                       ;645.13
        cmp       edx, edi                                      ;645.13
        jbe       .B10.674      ; Prob 10%                      ;645.13
                                ; LOE eax edx ecx ebx esi
.B10.673:                       ; Preds .B10.672
        mov       edi, esi                                      ;645.13
        add       eax, esi                                      ;645.13
        neg       edi                                           ;645.13
        add       edi, eax                                      ;645.13
        imul      eax, edi, 900                                 ;645.13
        mov       DWORD PTR [-156+ecx+eax], 0                   ;645.13
                                ; LOE edx ecx ebx esi
.B10.674:                       ; Preds .B10.672 .B10.673
        test      ebx, ebx                                      ;646.13
        jbe       .B10.1066     ; Prob 10%                      ;646.13
                                ; LOE edx ecx ebx esi
.B10.675:                       ; Preds .B10.674
        xor       eax, eax                                      ;
        mov       DWORD PTR [esp], esi                          ;
        xor       edi, edi                                      ;
        mov       esi, eax                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.676:                       ; Preds .B10.676 .B10.675
        inc       edi                                           ;646.13
        mov       DWORD PTR [748+esi+ecx], eax                  ;646.13
        mov       DWORD PTR [1648+esi+ecx], eax                 ;646.13
        add       esi, 1800                                     ;646.13
        cmp       edi, ebx                                      ;646.13
        jb        .B10.676      ; Prob 63%                      ;646.13
                                ; LOE eax edx ecx ebx esi edi
.B10.677:                       ; Preds .B10.676
        mov       esi, DWORD PTR [esp]                          ;
        lea       eax, DWORD PTR [1+edi+edi]                    ;646.13
                                ; LOE eax edx ecx ebx esi
.B10.678:                       ; Preds .B10.677 .B10.1066
        lea       edi, DWORD PTR [-1+eax]                       ;646.13
        cmp       edx, edi                                      ;646.13
        jbe       .B10.680      ; Prob 10%                      ;646.13
                                ; LOE eax edx ecx ebx esi
.B10.679:                       ; Preds .B10.678
        mov       edi, esi                                      ;646.13
        add       eax, esi                                      ;646.13
        neg       edi                                           ;646.13
        add       edi, eax                                      ;646.13
        imul      eax, edi, 900                                 ;646.13
        mov       DWORD PTR [-152+ecx+eax], 0                   ;646.13
                                ; LOE edx ecx ebx esi
.B10.680:                       ; Preds .B10.678 .B10.679
        test      ebx, ebx                                      ;647.13
        jbe       .B10.1065     ; Prob 10%                      ;647.13
                                ; LOE edx ecx ebx esi
.B10.681:                       ; Preds .B10.680
        xor       eax, eax                                      ;
        mov       DWORD PTR [esp], esi                          ;
        xor       edi, edi                                      ;
        mov       esi, eax                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.682:                       ; Preds .B10.682 .B10.681
        inc       edi                                           ;647.13
        mov       DWORD PTR [752+esi+ecx], eax                  ;647.13
        mov       DWORD PTR [1652+esi+ecx], eax                 ;647.13
        add       esi, 1800                                     ;647.13
        cmp       edi, ebx                                      ;647.13
        jb        .B10.682      ; Prob 63%                      ;647.13
                                ; LOE eax edx ecx ebx esi edi
.B10.683:                       ; Preds .B10.682
        mov       esi, DWORD PTR [esp]                          ;
        lea       eax, DWORD PTR [1+edi+edi]                    ;647.13
                                ; LOE eax edx ecx ebx esi
.B10.684:                       ; Preds .B10.683 .B10.1065
        lea       edi, DWORD PTR [-1+eax]                       ;647.13
        cmp       edx, edi                                      ;647.13
        jbe       .B10.686      ; Prob 10%                      ;647.13
                                ; LOE eax edx ecx ebx esi
.B10.685:                       ; Preds .B10.684
        mov       edi, esi                                      ;647.13
        add       eax, esi                                      ;647.13
        neg       edi                                           ;647.13
        add       edi, eax                                      ;647.13
        imul      eax, edi, 900                                 ;647.13
        mov       DWORD PTR [-148+ecx+eax], 0                   ;647.13
                                ; LOE edx ecx ebx esi
.B10.686:                       ; Preds .B10.684 .B10.685
        test      ebx, ebx                                      ;648.13
        jbe       .B10.1064     ; Prob 10%                      ;648.13
                                ; LOE edx ecx ebx esi
.B10.687:                       ; Preds .B10.686
        xor       eax, eax                                      ;
        mov       DWORD PTR [esp], esi                          ;
        xor       edi, edi                                      ;
        mov       esi, eax                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.688:                       ; Preds .B10.688 .B10.687
        inc       edi                                           ;648.13
        mov       DWORD PTR [756+esi+ecx], eax                  ;648.13
        mov       DWORD PTR [1656+esi+ecx], eax                 ;648.13
        add       esi, 1800                                     ;648.13
        cmp       edi, ebx                                      ;648.13
        jb        .B10.688      ; Prob 63%                      ;648.13
                                ; LOE eax edx ecx ebx esi edi
.B10.689:                       ; Preds .B10.688
        mov       esi, DWORD PTR [esp]                          ;
        lea       eax, DWORD PTR [1+edi+edi]                    ;648.13
                                ; LOE eax edx ecx ebx esi
.B10.690:                       ; Preds .B10.689 .B10.1064
        lea       edi, DWORD PTR [-1+eax]                       ;648.13
        cmp       edx, edi                                      ;648.13
        jbe       .B10.692      ; Prob 10%                      ;648.13
                                ; LOE eax edx ecx ebx esi
.B10.691:                       ; Preds .B10.690
        mov       edi, esi                                      ;648.13
        add       eax, esi                                      ;648.13
        neg       edi                                           ;648.13
        add       edi, eax                                      ;648.13
        imul      eax, edi, 900                                 ;648.13
        mov       DWORD PTR [-144+ecx+eax], 0                   ;648.13
                                ; LOE edx ecx ebx esi
.B10.692:                       ; Preds .B10.690 .B10.691
        test      ebx, ebx                                      ;649.13
        jbe       .B10.1063     ; Prob 10%                      ;649.13
                                ; LOE edx ecx ebx esi
.B10.693:                       ; Preds .B10.692
        xor       eax, eax                                      ;
        mov       DWORD PTR [esp], esi                          ;
        xor       edi, edi                                      ;
        mov       esi, eax                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.694:                       ; Preds .B10.694 .B10.693
        inc       edi                                           ;649.13
        mov       DWORD PTR [760+esi+ecx], eax                  ;649.13
        mov       DWORD PTR [1660+esi+ecx], eax                 ;649.13
        add       esi, 1800                                     ;649.13
        cmp       edi, ebx                                      ;649.13
        jb        .B10.694      ; Prob 63%                      ;649.13
                                ; LOE eax edx ecx ebx esi edi
.B10.695:                       ; Preds .B10.694
        mov       esi, DWORD PTR [esp]                          ;
        lea       eax, DWORD PTR [1+edi+edi]                    ;649.13
                                ; LOE eax edx ecx ebx esi
.B10.696:                       ; Preds .B10.695 .B10.1063
        lea       edi, DWORD PTR [-1+eax]                       ;649.13
        cmp       edx, edi                                      ;649.13
        jbe       .B10.698      ; Prob 10%                      ;649.13
                                ; LOE eax edx ecx ebx esi
.B10.697:                       ; Preds .B10.696
        mov       edi, esi                                      ;649.13
        add       eax, esi                                      ;649.13
        neg       edi                                           ;649.13
        add       edi, eax                                      ;649.13
        imul      eax, edi, 900                                 ;649.13
        mov       DWORD PTR [-140+ecx+eax], 0                   ;649.13
                                ; LOE edx ecx ebx esi
.B10.698:                       ; Preds .B10.696 .B10.697
        test      ebx, ebx                                      ;650.13
        jbe       .B10.1062     ; Prob 10%                      ;650.13
                                ; LOE edx ecx ebx esi
.B10.699:                       ; Preds .B10.698
        xor       eax, eax                                      ;
        mov       DWORD PTR [esp], esi                          ;
        xor       edi, edi                                      ;
        mov       esi, eax                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.700:                       ; Preds .B10.700 .B10.699
        inc       edi                                           ;650.13
        mov       DWORD PTR [764+esi+ecx], eax                  ;650.13
        mov       DWORD PTR [1664+esi+ecx], eax                 ;650.13
        add       esi, 1800                                     ;650.13
        cmp       edi, ebx                                      ;650.13
        jb        .B10.700      ; Prob 63%                      ;650.13
                                ; LOE eax edx ecx ebx esi edi
.B10.701:                       ; Preds .B10.700
        mov       esi, DWORD PTR [esp]                          ;
        lea       eax, DWORD PTR [1+edi+edi]                    ;650.13
                                ; LOE eax edx ecx ebx esi
.B10.702:                       ; Preds .B10.701 .B10.1062
        lea       edi, DWORD PTR [-1+eax]                       ;650.13
        cmp       edx, edi                                      ;650.13
        jbe       .B10.704      ; Prob 10%                      ;650.13
                                ; LOE eax edx ecx ebx esi
.B10.703:                       ; Preds .B10.702
        mov       edi, esi                                      ;650.13
        add       eax, esi                                      ;650.13
        neg       edi                                           ;650.13
        add       edi, eax                                      ;650.13
        imul      eax, edi, 900                                 ;650.13
        mov       DWORD PTR [-136+ecx+eax], 0                   ;650.13
                                ; LOE edx ecx ebx esi
.B10.704:                       ; Preds .B10.702 .B10.703
        test      ebx, ebx                                      ;651.13
        jbe       .B10.1061     ; Prob 10%                      ;651.13
                                ; LOE edx ecx ebx esi
.B10.705:                       ; Preds .B10.704
        xor       eax, eax                                      ;
        mov       DWORD PTR [esp], esi                          ;
        xor       edi, edi                                      ;
        mov       esi, eax                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.706:                       ; Preds .B10.706 .B10.705
        inc       edi                                           ;651.13
        mov       DWORD PTR [768+esi+ecx], eax                  ;651.13
        mov       DWORD PTR [1668+esi+ecx], eax                 ;651.13
        add       esi, 1800                                     ;651.13
        cmp       edi, ebx                                      ;651.13
        jb        .B10.706      ; Prob 63%                      ;651.13
                                ; LOE eax edx ecx ebx esi edi
.B10.707:                       ; Preds .B10.706
        mov       esi, DWORD PTR [esp]                          ;
        lea       eax, DWORD PTR [1+edi+edi]                    ;651.13
                                ; LOE eax edx ecx ebx esi
.B10.708:                       ; Preds .B10.707 .B10.1061
        lea       edi, DWORD PTR [-1+eax]                       ;651.13
        cmp       edx, edi                                      ;651.13
        jbe       .B10.710      ; Prob 10%                      ;651.13
                                ; LOE eax edx ecx ebx esi
.B10.709:                       ; Preds .B10.708
        mov       edi, esi                                      ;651.13
        add       eax, esi                                      ;651.13
        neg       edi                                           ;651.13
        add       edi, eax                                      ;651.13
        imul      eax, edi, 900                                 ;651.13
        mov       DWORD PTR [-132+ecx+eax], 0                   ;651.13
                                ; LOE edx ecx ebx esi
.B10.710:                       ; Preds .B10.708 .B10.709
        test      ebx, ebx                                      ;652.13
        jbe       .B10.1060     ; Prob 10%                      ;652.13
                                ; LOE edx ecx ebx esi
.B10.711:                       ; Preds .B10.710
        xor       eax, eax                                      ;
        mov       DWORD PTR [esp], esi                          ;
        xor       edi, edi                                      ;
        mov       esi, eax                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.712:                       ; Preds .B10.712 .B10.711
        inc       edi                                           ;652.13
        mov       DWORD PTR [772+esi+ecx], eax                  ;652.13
        mov       DWORD PTR [1672+esi+ecx], eax                 ;652.13
        add       esi, 1800                                     ;652.13
        cmp       edi, ebx                                      ;652.13
        jb        .B10.712      ; Prob 63%                      ;652.13
                                ; LOE eax edx ecx ebx esi edi
.B10.713:                       ; Preds .B10.712
        mov       esi, DWORD PTR [esp]                          ;
        lea       eax, DWORD PTR [1+edi+edi]                    ;652.13
                                ; LOE eax edx ecx ebx esi
.B10.714:                       ; Preds .B10.713 .B10.1060
        lea       edi, DWORD PTR [-1+eax]                       ;652.13
        cmp       edx, edi                                      ;652.13
        jbe       .B10.716      ; Prob 10%                      ;652.13
                                ; LOE eax edx ecx ebx esi
.B10.715:                       ; Preds .B10.714
        mov       edi, esi                                      ;652.13
        add       eax, esi                                      ;652.13
        neg       edi                                           ;652.13
        add       edi, eax                                      ;652.13
        imul      eax, edi, 900                                 ;652.13
        mov       DWORD PTR [-128+ecx+eax], 0                   ;652.13
                                ; LOE edx ecx ebx esi
.B10.716:                       ; Preds .B10.714 .B10.715
        test      ebx, ebx                                      ;653.13
        jbe       .B10.1059     ; Prob 10%                      ;653.13
                                ; LOE edx ecx ebx esi
.B10.717:                       ; Preds .B10.716
        xor       eax, eax                                      ;
        mov       DWORD PTR [esp], esi                          ;
        xor       edi, edi                                      ;
        mov       esi, eax                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.718:                       ; Preds .B10.718 .B10.717
        inc       edi                                           ;653.13
        mov       DWORD PTR [776+esi+ecx], eax                  ;653.13
        mov       DWORD PTR [1676+esi+ecx], eax                 ;653.13
        add       esi, 1800                                     ;653.13
        cmp       edi, ebx                                      ;653.13
        jb        .B10.718      ; Prob 63%                      ;653.13
                                ; LOE eax edx ecx ebx esi edi
.B10.719:                       ; Preds .B10.718
        mov       esi, DWORD PTR [esp]                          ;
        lea       eax, DWORD PTR [1+edi+edi]                    ;653.13
                                ; LOE eax edx ecx ebx esi
.B10.720:                       ; Preds .B10.719 .B10.1059
        lea       edi, DWORD PTR [-1+eax]                       ;653.13
        cmp       edx, edi                                      ;653.13
        jbe       .B10.722      ; Prob 10%                      ;653.13
                                ; LOE eax edx ecx ebx esi
.B10.721:                       ; Preds .B10.720
        mov       edi, esi                                      ;653.13
        add       eax, esi                                      ;653.13
        neg       edi                                           ;653.13
        add       edi, eax                                      ;653.13
        imul      eax, edi, 900                                 ;653.13
        mov       DWORD PTR [-124+ecx+eax], 0                   ;653.13
                                ; LOE edx ecx ebx esi
.B10.722:                       ; Preds .B10.720 .B10.721
        test      ebx, ebx                                      ;654.13
        jbe       .B10.1058     ; Prob 10%                      ;654.13
                                ; LOE edx ecx ebx esi
.B10.723:                       ; Preds .B10.722
        xor       eax, eax                                      ;
        mov       DWORD PTR [esp], esi                          ;
        xor       edi, edi                                      ;
        mov       esi, eax                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.724:                       ; Preds .B10.724 .B10.723
        inc       edi                                           ;654.13
        mov       DWORD PTR [780+esi+ecx], eax                  ;654.13
        mov       DWORD PTR [1680+esi+ecx], eax                 ;654.13
        add       esi, 1800                                     ;654.13
        cmp       edi, ebx                                      ;654.13
        jb        .B10.724      ; Prob 63%                      ;654.13
                                ; LOE eax edx ecx ebx esi edi
.B10.725:                       ; Preds .B10.724
        mov       esi, DWORD PTR [esp]                          ;
        lea       eax, DWORD PTR [1+edi+edi]                    ;654.13
                                ; LOE eax edx ecx ebx esi
.B10.726:                       ; Preds .B10.725 .B10.1058
        lea       edi, DWORD PTR [-1+eax]                       ;654.13
        cmp       edx, edi                                      ;654.13
        jbe       .B10.728      ; Prob 10%                      ;654.13
                                ; LOE eax edx ecx ebx esi
.B10.727:                       ; Preds .B10.726
        mov       edi, esi                                      ;654.13
        add       eax, esi                                      ;654.13
        neg       edi                                           ;654.13
        add       edi, eax                                      ;654.13
        imul      eax, edi, 900                                 ;654.13
        mov       DWORD PTR [-120+ecx+eax], 0                   ;654.13
                                ; LOE edx ecx ebx esi
.B10.728:                       ; Preds .B10.726 .B10.727
        test      ebx, ebx                                      ;655.13
        jbe       .B10.1057     ; Prob 10%                      ;655.13
                                ; LOE edx ecx ebx esi
.B10.729:                       ; Preds .B10.728
        xor       eax, eax                                      ;
        mov       DWORD PTR [esp], esi                          ;
        xor       edi, edi                                      ;
        mov       esi, eax                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.730:                       ; Preds .B10.730 .B10.729
        inc       edi                                           ;655.13
        mov       DWORD PTR [784+esi+ecx], eax                  ;655.13
        mov       DWORD PTR [1684+esi+ecx], eax                 ;655.13
        add       esi, 1800                                     ;655.13
        cmp       edi, ebx                                      ;655.13
        jb        .B10.730      ; Prob 63%                      ;655.13
                                ; LOE eax edx ecx ebx esi edi
.B10.731:                       ; Preds .B10.730
        mov       esi, DWORD PTR [esp]                          ;
        lea       eax, DWORD PTR [1+edi+edi]                    ;655.13
                                ; LOE eax edx ecx ebx esi
.B10.732:                       ; Preds .B10.731 .B10.1057
        lea       edi, DWORD PTR [-1+eax]                       ;655.13
        cmp       edx, edi                                      ;655.13
        jbe       .B10.734      ; Prob 10%                      ;655.13
                                ; LOE eax edx ecx ebx esi
.B10.733:                       ; Preds .B10.732
        mov       edi, esi                                      ;655.13
        add       eax, esi                                      ;655.13
        neg       edi                                           ;655.13
        add       edi, eax                                      ;655.13
        imul      eax, edi, 900                                 ;655.13
        mov       DWORD PTR [-116+ecx+eax], 0                   ;655.13
                                ; LOE edx ecx ebx esi
.B10.734:                       ; Preds .B10.732 .B10.733
        test      ebx, ebx                                      ;656.13
        jbe       .B10.1056     ; Prob 10%                      ;656.13
                                ; LOE edx ecx ebx esi
.B10.735:                       ; Preds .B10.734
        xor       eax, eax                                      ;
        mov       DWORD PTR [esp], esi                          ;
        xor       edi, edi                                      ;
        mov       esi, eax                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.736:                       ; Preds .B10.736 .B10.735
        inc       edi                                           ;656.13
        mov       DWORD PTR [788+esi+ecx], eax                  ;656.13
        mov       DWORD PTR [1688+esi+ecx], eax                 ;656.13
        add       esi, 1800                                     ;656.13
        cmp       edi, ebx                                      ;656.13
        jb        .B10.736      ; Prob 63%                      ;656.13
                                ; LOE eax edx ecx ebx esi edi
.B10.737:                       ; Preds .B10.736
        mov       esi, DWORD PTR [esp]                          ;
        lea       eax, DWORD PTR [1+edi+edi]                    ;656.13
                                ; LOE eax edx ecx ebx esi
.B10.738:                       ; Preds .B10.737 .B10.1056
        lea       edi, DWORD PTR [-1+eax]                       ;656.13
        cmp       edx, edi                                      ;656.13
        jbe       .B10.740      ; Prob 10%                      ;656.13
                                ; LOE eax edx ecx ebx esi
.B10.739:                       ; Preds .B10.738
        mov       edi, esi                                      ;656.13
        add       eax, esi                                      ;656.13
        neg       edi                                           ;656.13
        add       edi, eax                                      ;656.13
        imul      eax, edi, 900                                 ;656.13
        mov       DWORD PTR [-112+ecx+eax], 0                   ;656.13
                                ; LOE edx ecx ebx esi
.B10.740:                       ; Preds .B10.738 .B10.739
        test      ebx, ebx                                      ;657.13
        jbe       .B10.1055     ; Prob 10%                      ;657.13
                                ; LOE edx ecx ebx esi
.B10.741:                       ; Preds .B10.740
        xor       eax, eax                                      ;
        mov       DWORD PTR [esp], esi                          ;
        xor       edi, edi                                      ;
        mov       esi, eax                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.742:                       ; Preds .B10.742 .B10.741
        inc       edi                                           ;657.13
        mov       DWORD PTR [792+esi+ecx], eax                  ;657.13
        mov       DWORD PTR [1692+esi+ecx], eax                 ;657.13
        add       esi, 1800                                     ;657.13
        cmp       edi, ebx                                      ;657.13
        jb        .B10.742      ; Prob 63%                      ;657.13
                                ; LOE eax edx ecx ebx esi edi
.B10.743:                       ; Preds .B10.742
        mov       esi, DWORD PTR [esp]                          ;
        lea       eax, DWORD PTR [1+edi+edi]                    ;657.13
                                ; LOE eax edx ecx ebx esi
.B10.744:                       ; Preds .B10.743 .B10.1055
        lea       edi, DWORD PTR [-1+eax]                       ;657.13
        cmp       edx, edi                                      ;657.13
        jbe       .B10.746      ; Prob 10%                      ;657.13
                                ; LOE eax edx ecx ebx esi
.B10.745:                       ; Preds .B10.744
        mov       edi, esi                                      ;657.13
        add       eax, esi                                      ;657.13
        neg       edi                                           ;657.13
        add       edi, eax                                      ;657.13
        imul      eax, edi, 900                                 ;657.13
        mov       DWORD PTR [-108+ecx+eax], 0                   ;657.13
                                ; LOE edx ecx ebx esi
.B10.746:                       ; Preds .B10.744 .B10.745
        test      ebx, ebx                                      ;658.13
        jbe       .B10.1054     ; Prob 10%                      ;658.13
                                ; LOE edx ecx ebx esi
.B10.747:                       ; Preds .B10.746
        xor       eax, eax                                      ;
        mov       DWORD PTR [esp], esi                          ;
        xor       edi, edi                                      ;
        mov       esi, eax                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.748:                       ; Preds .B10.748 .B10.747
        inc       edi                                           ;658.13
        mov       DWORD PTR [796+esi+ecx], eax                  ;658.13
        mov       DWORD PTR [1696+esi+ecx], eax                 ;658.13
        add       esi, 1800                                     ;658.13
        cmp       edi, ebx                                      ;658.13
        jb        .B10.748      ; Prob 63%                      ;658.13
                                ; LOE eax edx ecx ebx esi edi
.B10.749:                       ; Preds .B10.748
        mov       esi, DWORD PTR [esp]                          ;
        lea       eax, DWORD PTR [1+edi+edi]                    ;658.13
                                ; LOE eax edx ecx ebx esi
.B10.750:                       ; Preds .B10.749 .B10.1054
        lea       edi, DWORD PTR [-1+eax]                       ;658.13
        cmp       edx, edi                                      ;658.13
        jbe       .B10.752      ; Prob 10%                      ;658.13
                                ; LOE eax edx ecx ebx esi
.B10.751:                       ; Preds .B10.750
        mov       edi, esi                                      ;658.13
        add       eax, esi                                      ;658.13
        neg       edi                                           ;658.13
        add       edi, eax                                      ;658.13
        imul      eax, edi, 900                                 ;658.13
        mov       DWORD PTR [-104+ecx+eax], 0                   ;658.13
                                ; LOE edx ecx ebx esi
.B10.752:                       ; Preds .B10.750 .B10.751
        test      ebx, ebx                                      ;659.13
        jbe       .B10.1053     ; Prob 10%                      ;659.13
                                ; LOE edx ecx ebx esi
.B10.753:                       ; Preds .B10.752
        xor       eax, eax                                      ;
        mov       DWORD PTR [esp], esi                          ;
        xor       edi, edi                                      ;
        mov       esi, eax                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.754:                       ; Preds .B10.754 .B10.753
        inc       edi                                           ;659.13
        mov       DWORD PTR [800+esi+ecx], eax                  ;659.13
        mov       DWORD PTR [1700+esi+ecx], eax                 ;659.13
        add       esi, 1800                                     ;659.13
        cmp       edi, ebx                                      ;659.13
        jb        .B10.754      ; Prob 63%                      ;659.13
                                ; LOE eax edx ecx ebx esi edi
.B10.755:                       ; Preds .B10.754
        mov       esi, DWORD PTR [esp]                          ;
        lea       eax, DWORD PTR [1+edi+edi]                    ;659.13
                                ; LOE eax edx ecx ebx esi
.B10.756:                       ; Preds .B10.755 .B10.1053
        lea       edi, DWORD PTR [-1+eax]                       ;659.13
        cmp       edx, edi                                      ;659.13
        jbe       .B10.758      ; Prob 10%                      ;659.13
                                ; LOE eax edx ecx ebx esi
.B10.757:                       ; Preds .B10.756
        mov       edi, esi                                      ;659.13
        add       eax, esi                                      ;659.13
        neg       edi                                           ;659.13
        add       edi, eax                                      ;659.13
        imul      eax, edi, 900                                 ;659.13
        mov       DWORD PTR [-100+ecx+eax], 0                   ;659.13
                                ; LOE edx ecx ebx esi
.B10.758:                       ; Preds .B10.756 .B10.757
        test      ebx, ebx                                      ;660.13
        jbe       .B10.1052     ; Prob 10%                      ;660.13
                                ; LOE edx ecx ebx esi
.B10.759:                       ; Preds .B10.758
        xor       eax, eax                                      ;
        mov       DWORD PTR [esp], esi                          ;
        xor       edi, edi                                      ;
        mov       esi, eax                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.760:                       ; Preds .B10.760 .B10.759
        inc       edi                                           ;660.13
        mov       DWORD PTR [804+esi+ecx], eax                  ;660.13
        mov       DWORD PTR [1704+esi+ecx], eax                 ;660.13
        add       esi, 1800                                     ;660.13
        cmp       edi, ebx                                      ;660.13
        jb        .B10.760      ; Prob 63%                      ;660.13
                                ; LOE eax edx ecx ebx esi edi
.B10.761:                       ; Preds .B10.760
        mov       esi, DWORD PTR [esp]                          ;
        lea       ebx, DWORD PTR [1+edi+edi]                    ;660.13
                                ; LOE edx ecx ebx esi
.B10.762:                       ; Preds .B10.761 .B10.1052
        lea       eax, DWORD PTR [-1+ebx]                       ;660.13
        cmp       edx, eax                                      ;660.13
        jbe       .B10.764      ; Prob 10%                      ;660.13
                                ; LOE ecx ebx esi
.B10.763:                       ; Preds .B10.762
        mov       eax, esi                                      ;660.13
        add       ebx, esi                                      ;660.13
        neg       eax                                           ;660.13
        add       eax, ebx                                      ;660.13
        imul      edx, eax, 900                                 ;660.13
        mov       DWORD PTR [-96+ecx+edx], 0                    ;660.13
                                ; LOE
.B10.764:                       ; Preds .B10.762 .B10.607 .B10.763
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_LINKIDMAP+12] ;662.13
        and       eax, 1                                        ;662.13
        add       eax, eax                                      ;662.13
        or        eax, 262145                                   ;662.13
        push      eax                                           ;662.13
        push      OFFSET FLAT: _DYNUST_NETWORK_MODULE_mp_LINKIDMAP ;662.13
        push      100                                           ;662.13
        call      _for_alloc_allocatable                        ;662.13
                                ; LOE eax
.B10.1392:                      ; Preds .B10.764
        add       esp, 12                                       ;662.13
                                ; LOE eax
.B10.765:                       ; Preds .B10.1392
        test      eax, eax                                      ;662.13
        jne       .B10.767      ; Prob 50%                      ;662.13
                                ; LOE eax
.B10.766:                       ; Preds .B10.765
        mov       edi, 2                                        ;662.13
        mov       ebx, 1                                        ;662.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_LINKIDMAP+4], edi ;662.13
        mov       esi, 50                                       ;
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_LINKIDMAP+28], edi ;662.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_LINKIDMAP+12], 133 ;662.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_LINKIDMAP+16], ebx ;662.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_LINKIDMAP+8], 0 ;662.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_LINKIDMAP+32], ebx ;662.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_LINKIDMAP+24], 50 ;662.13
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;662.13
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_LINKIDMAP] ;667.10
        jmp       .B10.771      ; Prob 100%                     ;667.10
                                ; LOE ebx esi edi
.B10.767:                       ; Preds .B10.765
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;662.13
        lea       eax, DWORD PTR [48+esp]                       ;664.16
        mov       DWORD PTR [160+esp], 0                        ;664.16
        mov       DWORD PTR [48+esp], 46                        ;664.16
        mov       DWORD PTR [52+esp], OFFSET FLAT: __STRLITPACK_24 ;664.16
        push      32                                            ;664.16
        push      eax                                           ;664.16
        push      OFFSET FLAT: __STRLITPACK_200.0.10            ;664.16
        push      -2088435968                                   ;664.16
        push      911                                           ;664.16
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_LINKIDMAP+32] ;667.10
        lea       edx, DWORD PTR [180+esp]                      ;664.16
        push      edx                                           ;664.16
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_LINKIDMAP+24] ;667.10
        call      _for_write_seq_lis                            ;664.16
                                ; LOE ebx esi
.B10.768:                       ; Preds .B10.767
        push      32                                            ;665.16
        xor       eax, eax                                      ;665.16
        push      eax                                           ;665.16
        push      eax                                           ;665.16
        push      -2088435968                                   ;665.16
        push      eax                                           ;665.16
        push      OFFSET FLAT: __STRLITPACK_201                 ;665.16
        call      _for_stop_core                                ;665.16
                                ; LOE ebx esi
.B10.1393:                      ; Preds .B10.768
        add       esp, 48                                       ;665.16
                                ; LOE ebx esi
.B10.769:                       ; Preds .B10.1393
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_LINKIDMAP] ;668.10
        test      esi, esi                                      ;667.10
        jle       .B10.1210     ; Prob 10%                      ;667.10
                                ; LOE ebx esi edi
.B10.770:                       ; Preds .B10.769
        cmp       esi, 8                                        ;667.10
        jl        .B10.1209     ; Prob 10%                      ;667.10
                                ; LOE ebx esi edi
.B10.771:                       ; Preds .B10.766 .B10.770
        mov       edx, edi                                      ;667.10
        add       ebx, ebx                                      ;667.10
        and       edx, 15                                       ;667.10
        je        .B10.774      ; Prob 50%                      ;667.10
                                ; LOE edx ebx esi edi
.B10.772:                       ; Preds .B10.771
        test      dl, 1                                         ;667.10
        jne       .B10.1078     ; Prob 10%                      ;667.10
                                ; LOE edx ebx esi edi
.B10.773:                       ; Preds .B10.772
        neg       edx                                           ;667.10
        add       edx, 16                                       ;667.10
        shr       edx, 1                                        ;667.10
                                ; LOE edx ebx esi edi
.B10.774:                       ; Preds .B10.773 .B10.771
        lea       eax, DWORD PTR [8+edx]                        ;667.10
        cmp       esi, eax                                      ;667.10
        jl        .B10.1078     ; Prob 10%                      ;667.10
                                ; LOE edx ebx esi edi
.B10.775:                       ; Preds .B10.774
        mov       ecx, esi                                      ;667.10
        mov       eax, edi                                      ;
        sub       ecx, edx                                      ;667.10
        sub       eax, ebx                                      ;
        and       ecx, 7                                        ;667.10
        neg       ecx                                           ;667.10
        add       ecx, esi                                      ;667.10
        mov       DWORD PTR [esp], eax                          ;
        test      edx, edx                                      ;667.10
        jbe       .B10.779      ; Prob 0%                       ;667.10
                                ; LOE edx ecx ebx esi edi
.B10.776:                       ; Preds .B10.775
        xor       eax, eax                                      ;
        mov       DWORD PTR [4+esp], esi                        ;
        mov       esi, 1                                        ;
                                ; LOE eax edx ecx ebx esi edi
.B10.777:                       ; Preds .B10.777 .B10.776
        mov       WORD PTR [edi+eax*2], si                      ;667.10
        inc       eax                                           ;667.10
        cmp       eax, edx                                      ;667.10
        jb        .B10.777      ; Prob 82%                      ;667.10
                                ; LOE eax edx ecx ebx esi edi
.B10.778:                       ; Preds .B10.777
        mov       esi, DWORD PTR [4+esp]                        ;
                                ; LOE edx ecx ebx esi edi
.B10.779:                       ; Preds .B10.775 .B10.778
        movdqa    xmm0, XMMWORD PTR [_2il0floatpacket.24]       ;667.10
                                ; LOE edx ecx ebx esi edi xmm0
.B10.780:                       ; Preds .B10.780 .B10.779
        movdqa    XMMWORD PTR [edi+edx*2], xmm0                 ;667.10
        add       edx, 8                                        ;667.10
        cmp       edx, ecx                                      ;667.10
        jb        .B10.780      ; Prob 82%                      ;667.10
                                ; LOE edx ecx ebx esi edi xmm0
.B10.782:                       ; Preds .B10.780 .B10.1078 .B10.1209
        cmp       ecx, esi                                      ;667.10
        jae       .B10.786      ; Prob 0%                       ;667.10
                                ; LOE ecx ebx esi
.B10.783:                       ; Preds .B10.782
        add       ebx, DWORD PTR [esp]                          ;
        mov       eax, 1                                        ;
                                ; LOE eax ecx ebx esi
.B10.784:                       ; Preds .B10.784 .B10.783
        mov       WORD PTR [ebx+ecx*2], ax                      ;667.10
        inc       ecx                                           ;667.10
        cmp       ecx, esi                                      ;667.10
        jb        .B10.784      ; Prob 82%                      ;667.10
                                ; LOE eax ecx ebx esi
.B10.786:                       ; Preds .B10.784 .B10.782 .B10.1210
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFNODES] ;679.13
        xor       edx, edx                                      ;679.13
        test      eax, eax                                      ;679.13
        lea       ebx, DWORD PTR [228+esp]                      ;679.13
        mov       edi, DWORD PTR [esp]                          ;668.10
        push      4                                             ;679.13
        movdqa    xmm0, XMMWORD PTR [_2__cnst_pck.21]           ;668.10
        lea       ecx, DWORD PTR [1+eax]                        ;679.13
        cmovl     ecx, edx                                      ;679.13
        push      1                                             ;679.13
        pop       esi                                           ;679.13
        push      ecx                                           ;679.13
        push      2                                             ;679.13
        push      ebx                                           ;679.13
        movdqu    XMMWORD PTR [2+edi], xmm0                     ;668.10
        mov       WORD PTR [18+edi], si                         ;668.10
        mov       WORD PTR [20+edi], si                         ;668.10
        call      _for_check_mult_overflow                      ;679.13
                                ; LOE eax
.B10.787:                       ; Preds .B10.786
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_KGPOINT+12] ;679.13
        and       eax, 1                                        ;679.13
        and       edx, 1                                        ;679.13
        add       edx, edx                                      ;679.13
        shl       eax, 4                                        ;679.13
        or        edx, 1                                        ;679.13
        or        edx, eax                                      ;679.13
        or        edx, 262144                                   ;679.13
        push      edx                                           ;679.13
        push      OFFSET FLAT: _DYNUST_NETWORK_MODULE_mp_KGPOINT ;679.13
        push      DWORD PTR [252+esp]                           ;679.13
        call      _for_alloc_allocatable                        ;679.13
                                ; LOE eax
.B10.1395:                      ; Preds .B10.787
        add       esp, 28                                       ;679.13
        mov       ebx, eax                                      ;679.13
                                ; LOE ebx
.B10.788:                       ; Preds .B10.1395
        test      ebx, ebx                                      ;679.13
        je        .B10.791      ; Prob 50%                      ;679.13
                                ; LOE ebx
.B10.789:                       ; Preds .B10.788
        mov       DWORD PTR [160+esp], 0                        ;681.14
        lea       eax, DWORD PTR [152+esp]                      ;681.14
        mov       DWORD PTR [152+esp], 44                       ;681.14
        mov       DWORD PTR [156+esp], OFFSET FLAT: __STRLITPACK_22 ;681.14
        push      32                                            ;681.14
        push      eax                                           ;681.14
        push      OFFSET FLAT: __STRLITPACK_202.0.10            ;681.14
        push      -2088435968                                   ;681.14
        push      911                                           ;681.14
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], ebx ;679.13
        lea       edx, DWORD PTR [180+esp]                      ;681.14
        push      edx                                           ;681.14
        call      _for_write_seq_lis                            ;681.14
                                ; LOE
.B10.790:                       ; Preds .B10.789
        push      32                                            ;682.14
        xor       eax, eax                                      ;682.14
        push      eax                                           ;682.14
        push      eax                                           ;682.14
        push      -2088435968                                   ;682.14
        push      eax                                           ;682.14
        push      OFFSET FLAT: __STRLITPACK_203                 ;682.14
        call      _for_stop_core                                ;682.14
                                ; LOE
.B10.1396:                      ; Preds .B10.790
        add       esp, 48                                       ;682.14
        jmp       .B10.793      ; Prob 100%                     ;682.14
                                ; LOE
.B10.791:                       ; Preds .B10.788
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFNODES] ;679.13
        xor       eax, eax                                      ;679.13
        test      ecx, ecx                                      ;679.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_KGPOINT+8], eax ;679.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_KGPOINT+12], 133 ;679.13
        lea       edi, DWORD PTR [1+ecx]                        ;679.13
        cmovl     edi, eax                                      ;679.13
        mov       esi, 4                                        ;679.13
        mov       edx, 1                                        ;679.13
        lea       eax, DWORD PTR [84+esp]                       ;679.13
        push      esi                                           ;679.13
        push      edi                                           ;679.13
        push      2                                             ;679.13
        push      eax                                           ;679.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_KGPOINT+4], esi ;679.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_KGPOINT+16], edx ;679.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_KGPOINT+32], edx ;679.39
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_KGPOINT+24], edi ;679.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_KGPOINT+28], esi ;679.13
        call      _for_check_mult_overflow                      ;679.13
                                ; LOE ebx
.B10.1397:                      ; Preds .B10.791
        add       esp, 16                                       ;679.13
                                ; LOE ebx
.B10.792:                       ; Preds .B10.1397
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], ebx ;679.13
                                ; LOE
.B10.793:                       ; Preds .B10.1396 .B10.792
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_KGPOINT+24] ;684.10
        test      ecx, ecx                                      ;684.10
        jle       .B10.796      ; Prob 50%                      ;684.10
                                ; LOE ecx
.B10.794:                       ; Preds .B10.793
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_KGPOINT] ;684.10
        cmp       ecx, 24                                       ;684.10
        jle       .B10.1079     ; Prob 0%                       ;684.10
                                ; LOE edx ecx
.B10.795:                       ; Preds .B10.794
        shl       ecx, 2                                        ;684.10
        push      ecx                                           ;684.10
        push      0                                             ;684.10
        push      edx                                           ;684.10
        call      __intel_fast_memset                           ;684.10
                                ; LOE
.B10.1398:                      ; Preds .B10.795
        add       esp, 12                                       ;684.10
                                ; LOE
.B10.796:                       ; Preds .B10.1093 .B10.793 .B10.1091 .B10.1398
        push      OFFSET FLAT: _DYNUST_MAIN_MODULE_mp_NOOFARCS  ;686.18
        call      _DYNUST_LINK_VEH_LIST_MODULE_mp_LINKATT_2DSETUP ;686.18
                                ; LOE
.B10.797:                       ; Preds .B10.796
        push      OFFSET FLAT: _DYNUST_MAIN_MODULE_mp_NOOFARCS  ;687.18
        call      _DYNUST_LINK_VEH_LIST_MODULE_mp_LINKTEMP_2DSETUP ;687.18
                                ; LOE
.B10.1399:                      ; Preds .B10.797
        add       esp, 8                                        ;687.18
                                ; LOE
.B10.798:                       ; Preds .B10.1399
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFARCS] ;689.13
        test      esi, esi                                      ;689.13
        mov       DWORD PTR [508+esp], 1                        ;689.13
        jle       .B10.803      ; Prob 2%                       ;689.13
                                ; LOE esi
.B10.799:                       ; Preds .B10.798
        lea       ebx, DWORD PTR [508+esp]                      ;690.22
                                ; LOE ebx esi
.B10.800:                       ; Preds .B10.799 .B10.801
        push      OFFSET FLAT: __NLITPACK_0.0.10                ;690.22
        push      ebx                                           ;690.22
        call      _DYNUST_LINK_VEH_LIST_MODULE_mp_LINKATT_SETUP ;690.22
                                ; LOE ebx esi
.B10.1400:                      ; Preds .B10.800
        add       esp, 8                                        ;690.22
                                ; LOE ebx esi
.B10.801:                       ; Preds .B10.1400
        mov       eax, DWORD PTR [508+esp]                      ;691.10
        inc       eax                                           ;691.10
        mov       DWORD PTR [508+esp], eax                      ;691.10
        cmp       eax, esi                                      ;691.10
        jle       .B10.800      ; Prob 82%                      ;691.10
                                ; LOE ebx esi
.B10.803:                       ; Preds .B10.801 .B10.798
        push      OFFSET FLAT: _DYNUST_MAIN_MODULE_mp_NOOFARCS  ;693.15
        call      _DYNUST_LINK_VEH_LIST_MODULE_mp_ENQATT_2DSETUP ;693.15
                                ; LOE
.B10.1401:                      ; Preds .B10.803
        add       esp, 4                                        ;693.15
                                ; LOE
.B10.804:                       ; Preds .B10.1401
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFARCS] ;694.13
        test      esi, esi                                      ;694.13
        mov       DWORD PTR [508+esp], 1                        ;694.13
        jle       .B10.809      ; Prob 2%                       ;694.13
                                ; LOE esi
.B10.805:                       ; Preds .B10.804
        lea       ebx, DWORD PTR [508+esp]                      ;690.22
                                ; LOE ebx esi
.B10.806:                       ; Preds .B10.805 .B10.807
        push      OFFSET FLAT: __NLITPACK_0.0.10                ;695.22
        push      ebx                                           ;695.22
        call      _DYNUST_LINK_VEH_LIST_MODULE_mp_ENQATT_SETUP  ;695.22
                                ; LOE ebx esi
.B10.1402:                      ; Preds .B10.806
        add       esp, 8                                        ;695.22
                                ; LOE ebx esi
.B10.807:                       ; Preds .B10.1402
        mov       eax, DWORD PTR [508+esp]                      ;696.10
        inc       eax                                           ;696.10
        mov       DWORD PTR [508+esp], eax                      ;696.10
        cmp       eax, esi                                      ;696.10
        jle       .B10.806      ; Prob 82%                      ;696.10
                                ; LOE ebx esi
.B10.809:                       ; Preds .B10.807 .B10.804
        push      OFFSET FLAT: _DYNUST_MAIN_MODULE_mp_NOOFARCS  ;698.15
        call      _DYNUST_LINK_VEH_LIST_MODULE_mp_TCHAINATT_2DSETUP ;698.15
                                ; LOE
.B10.1403:                      ; Preds .B10.809
        add       esp, 4                                        ;698.15
                                ; LOE
.B10.810:                       ; Preds .B10.1403
        mov       ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFARCS] ;699.13
        mov       eax, 1                                        ;699.13
        mov       DWORD PTR [4+esp], eax                        ;699.13
        test      ebx, ebx                                      ;699.13
        jle       .B10.1099     ; Prob 2%                       ;699.13
                                ; LOE ebx
.B10.811:                       ; Preds .B10.810
        mov       DWORD PTR [508+esp], 1                        ;413.13
        lea       esi, DWORD PTR [508+esp]                      ;413.13
        jmp       .B10.812      ; Prob 100%                     ;413.13
                                ; LOE ebx esi
.B10.814:                       ; Preds .B10.813
        mov       DWORD PTR [508+esp], eax                      ;413.13
                                ; LOE ebx esi
.B10.812:                       ; Preds .B10.811 .B10.814
        push      OFFSET FLAT: __NLITPACK_0.0.10                ;700.22
        push      esi                                           ;700.22
        call      _DYNUST_LINK_VEH_LIST_MODULE_mp_TCHAINATT_SETUP ;700.22
                                ; LOE ebx esi
.B10.1404:                      ; Preds .B10.812
        add       esp, 8                                        ;700.22
                                ; LOE ebx esi
.B10.813:                       ; Preds .B10.1404
        mov       eax, DWORD PTR [508+esp]                      ;701.10
        inc       eax                                           ;701.10
        cmp       eax, ebx                                      ;701.10
        jle       .B10.814      ; Prob 82%                      ;701.10
        jmp       .B10.1098     ; Prob 100%                     ;701.10
                                ; LOE eax ebx esi
.B10.815:                       ; Preds .B10.422
        imul      edx, DWORD PTR [20+esp]                       ;
        xor       ebx, ebx                                      ;
        pxor      xmm0, xmm0                                    ;576.13
        mov       DWORD PTR [12+esp], edi                       ;
        mov       edi, ecx                                      ;
        shl       eax, 2                                        ;
        sub       edi, eax                                      ;
        sub       eax, edx                                      ;
        add       edi, eax                                      ;
        lea       eax, DWORD PTR [esi*4]                        ;
        add       edi, edx                                      ;
        mov       DWORD PTR [esp], ecx                          ;
        mov       DWORD PTR [4+esp], edi                        ;
        mov       DWORD PTR [28+esp], esi                       ;
        mov       DWORD PTR [16+esp], ebx                       ;
        mov       DWORD PTR [8+esp], eax                        ;
        mov       DWORD PTR [24+esp], ecx                       ;
        mov       edi, DWORD PTR [4+esp]                        ;
        mov       esi, DWORD PTR [esp]                          ;
                                ; LOE ebx esi edi
.B10.816:                       ; Preds .B10.818 .B10.1229 .B10.815
        cmp       DWORD PTR [28+esp], 24                        ;576.13
        jle       .B10.1214     ; Prob 0%                       ;576.13
                                ; LOE ebx esi edi
.B10.817:                       ; Preds .B10.816
        push      DWORD PTR [8+esp]                             ;576.13
        push      0                                             ;576.13
        push      edi                                           ;576.13
        call      __intel_fast_memset                           ;576.13
                                ; LOE ebx esi edi
.B10.1405:                      ; Preds .B10.817
        add       esp, 12                                       ;576.13
                                ; LOE ebx esi edi
.B10.818:                       ; Preds .B10.1226 .B10.1405
        mov       edx, DWORD PTR [16+esp]                       ;576.13
        inc       edx                                           ;576.13
        mov       eax, DWORD PTR [20+esp]                       ;576.13
        add       esi, eax                                      ;576.13
        add       edi, eax                                      ;576.13
        add       ebx, eax                                      ;576.13
        mov       DWORD PTR [16+esp], edx                       ;576.13
        cmp       edx, DWORD PTR [12+esp]                       ;576.13
        jb        .B10.816      ; Prob 82%                      ;576.13
        jmp       .B10.423      ; Prob 100%                     ;576.13
                                ; LOE ebx esi edi
.B10.819:                       ; Preds .B10.403
        imul      edx, DWORD PTR [20+esp]                       ;
        xor       ebx, ebx                                      ;
        pxor      xmm0, xmm0                                    ;562.13
        mov       DWORD PTR [12+esp], edi                       ;
        mov       edi, ecx                                      ;
        shl       eax, 2                                        ;
        sub       edi, eax                                      ;
        sub       eax, edx                                      ;
        add       edi, eax                                      ;
        lea       eax, DWORD PTR [esi*4]                        ;
        add       edi, edx                                      ;
        mov       DWORD PTR [esp], ecx                          ;
        mov       DWORD PTR [4+esp], edi                        ;
        mov       DWORD PTR [28+esp], esi                       ;
        mov       DWORD PTR [16+esp], ebx                       ;
        mov       DWORD PTR [8+esp], eax                        ;
        mov       DWORD PTR [24+esp], ecx                       ;
        mov       edi, DWORD PTR [4+esp]                        ;
        mov       esi, DWORD PTR [esp]                          ;
                                ; LOE ebx esi edi
.B10.820:                       ; Preds .B10.822 .B10.1252 .B10.819
        cmp       DWORD PTR [28+esp], 24                        ;562.13
        jle       .B10.1237     ; Prob 0%                       ;562.13
                                ; LOE ebx esi edi
.B10.821:                       ; Preds .B10.820
        push      DWORD PTR [8+esp]                             ;562.13
        push      0                                             ;562.13
        push      edi                                           ;562.13
        call      __intel_fast_memset                           ;562.13
                                ; LOE ebx esi edi
.B10.1406:                      ; Preds .B10.821
        add       esp, 12                                       ;562.13
                                ; LOE ebx esi edi
.B10.822:                       ; Preds .B10.1249 .B10.1406
        mov       edx, DWORD PTR [16+esp]                       ;562.13
        inc       edx                                           ;562.13
        mov       eax, DWORD PTR [20+esp]                       ;562.13
        add       esi, eax                                      ;562.13
        add       edi, eax                                      ;562.13
        add       ebx, eax                                      ;562.13
        mov       DWORD PTR [16+esp], edx                       ;562.13
        cmp       edx, DWORD PTR [12+esp]                       ;562.13
        jb        .B10.820      ; Prob 82%                      ;562.13
        jmp       .B10.404      ; Prob 100%                     ;562.13
                                ; LOE ebx esi edi
.B10.823:                       ; Preds .B10.394
        imul      edx, DWORD PTR [20+esp]                       ;
        xor       ebx, ebx                                      ;
        pxor      xmm0, xmm0                                    ;555.13
        mov       DWORD PTR [12+esp], edi                       ;
        mov       edi, ecx                                      ;
        shl       eax, 2                                        ;
        sub       edi, eax                                      ;
        sub       eax, edx                                      ;
        add       edi, eax                                      ;
        lea       eax, DWORD PTR [esi*4]                        ;
        add       edi, edx                                      ;
        mov       DWORD PTR [esp], ecx                          ;
        mov       DWORD PTR [4+esp], edi                        ;
        mov       DWORD PTR [28+esp], esi                       ;
        mov       DWORD PTR [16+esp], ebx                       ;
        mov       DWORD PTR [8+esp], eax                        ;
        mov       DWORD PTR [24+esp], ecx                       ;
        mov       edi, DWORD PTR [4+esp]                        ;
        mov       esi, DWORD PTR [esp]                          ;
                                ; LOE ebx esi edi
.B10.824:                       ; Preds .B10.826 .B10.1274 .B10.823
        cmp       DWORD PTR [28+esp], 24                        ;555.13
        jle       .B10.1259     ; Prob 0%                       ;555.13
                                ; LOE ebx esi edi
.B10.825:                       ; Preds .B10.824
        push      DWORD PTR [8+esp]                             ;555.13
        push      0                                             ;555.13
        push      edi                                           ;555.13
        call      __intel_fast_memset                           ;555.13
                                ; LOE ebx esi edi
.B10.1407:                      ; Preds .B10.825
        add       esp, 12                                       ;555.13
                                ; LOE ebx esi edi
.B10.826:                       ; Preds .B10.1271 .B10.1407
        mov       edx, DWORD PTR [16+esp]                       ;555.13
        inc       edx                                           ;555.13
        mov       eax, DWORD PTR [20+esp]                       ;555.13
        add       esi, eax                                      ;555.13
        add       edi, eax                                      ;555.13
        add       ebx, eax                                      ;555.13
        mov       DWORD PTR [16+esp], edx                       ;555.13
        cmp       edx, DWORD PTR [12+esp]                       ;555.13
        jb        .B10.824      ; Prob 82%                      ;555.13
        jmp       .B10.395      ; Prob 100%                     ;555.13
                                ; LOE ebx esi edi
.B10.828:                       ; Preds .B10.80                 ; Infreq
        mov       eax, 1                                        ;
        jmp       .B10.84       ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B10.829:                       ; Preds .B10.74                 ; Infreq
        mov       eax, 1                                        ;
        jmp       .B10.78       ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B10.830:                       ; Preds .B10.68                 ; Infreq
        mov       eax, 1                                        ;
        jmp       .B10.72       ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B10.831:                       ; Preds .B10.62                 ; Infreq
        mov       eax, 1                                        ;
        jmp       .B10.66       ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B10.832:                       ; Preds .B10.56                 ; Infreq
        mov       eax, 1                                        ;
        jmp       .B10.60       ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B10.833:                       ; Preds .B10.50                 ; Infreq
        mov       eax, 1                                        ;
        jmp       .B10.54       ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B10.834:                       ; Preds .B10.44                 ; Infreq
        mov       eax, 1                                        ;
        jmp       .B10.48       ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B10.835:                       ; Preds .B10.38                 ; Infreq
        mov       eax, 1                                        ;
        jmp       .B10.42       ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B10.836:                       ; Preds .B10.32                 ; Infreq
        mov       eax, 1                                        ;
        jmp       .B10.36       ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B10.837:                       ; Preds .B10.26                 ; Infreq
        mov       eax, 1                                        ;
        jmp       .B10.30       ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B10.838:                       ; Preds .B10.20                 ; Infreq
        mov       eax, 1                                        ;
        jmp       .B10.24       ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B10.839:                       ; Preds .B10.14                 ; Infreq
        mov       eax, 1                                        ;
        jmp       .B10.18       ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B10.840:                       ; Preds .B10.102                ; Infreq
        mov       eax, 1                                        ;
        jmp       .B10.106      ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B10.841:                       ; Preds .B10.116                ; Infreq
        mov       eax, 1                                        ;
        jmp       .B10.120      ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B10.842:                       ; Preds .B10.130                ; Infreq
        cmp       esi, 4                                        ;433.17
        jl        .B10.850      ; Prob 10%                      ;433.17
                                ; LOE ecx ebx esi
.B10.843:                       ; Preds .B10.842                ; Infreq
        mov       edx, esi                                      ;433.17
        xor       eax, eax                                      ;433.17
        mov       edi, DWORD PTR [192+ecx+ebx]                  ;433.17
        and       edx, -4                                       ;433.17
        pxor      xmm0, xmm0                                    ;433.17
                                ; LOE eax edx ecx ebx esi edi xmm0
.B10.844:                       ; Preds .B10.844 .B10.843       ; Infreq
        movdqu    XMMWORD PTR [edi+eax*4], xmm0                 ;433.17
        add       eax, 4                                        ;433.17
        cmp       eax, edx                                      ;433.17
        jb        .B10.844      ; Prob 82%                      ;433.17
                                ; LOE eax edx ecx ebx esi edi xmm0
.B10.846:                       ; Preds .B10.844 .B10.850       ; Infreq
        cmp       edx, esi                                      ;433.17
        jae       .B10.132      ; Prob 10%                      ;433.17
                                ; LOE edx ecx ebx esi
.B10.847:                       ; Preds .B10.846                ; Infreq
        mov       eax, DWORD PTR [192+ecx+ebx]                  ;433.17
                                ; LOE eax edx esi
.B10.848:                       ; Preds .B10.848 .B10.847       ; Infreq
        mov       DWORD PTR [eax+edx*4], 0                      ;433.17
        inc       edx                                           ;433.17
        cmp       edx, esi                                      ;433.17
        jb        .B10.848      ; Prob 82%                      ;433.17
        jmp       .B10.132      ; Prob 100%                     ;433.17
                                ; LOE eax edx esi
.B10.850:                       ; Preds .B10.842                ; Infreq
        xor       edx, edx                                      ;433.17
        jmp       .B10.846      ; Prob 100%                     ;433.17
                                ; LOE edx ecx ebx esi
.B10.851:                       ; Preds .B10.140                ; Infreq
        mov       eax, 1                                        ;
        jmp       .B10.144      ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B10.852:                       ; Preds .B10.154                ; Infreq
        mov       eax, 1                                        ;
        jmp       .B10.158      ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B10.853:                       ; Preds .B10.168 .B10.172 .B10.170 ; Infreq
        xor       ecx, ecx                                      ;454.17
        jmp       .B10.180      ; Prob 100%                     ;454.17
                                ; LOE edx ecx esi edi
.B10.856:                       ; Preds .B10.192                ; Infreq
        cmp       esi, 4                                        ;461.17
        jl        .B10.864      ; Prob 10%                      ;461.17
                                ; LOE ecx ebx esi
.B10.857:                       ; Preds .B10.856                ; Infreq
        mov       edx, esi                                      ;461.17
        xor       eax, eax                                      ;461.17
        mov       edi, DWORD PTR [264+ecx+ebx]                  ;461.17
        and       edx, -4                                       ;461.17
        pxor      xmm0, xmm0                                    ;461.17
                                ; LOE eax edx ecx ebx esi edi xmm0
.B10.858:                       ; Preds .B10.858 .B10.857       ; Infreq
        movdqu    XMMWORD PTR [edi+eax*4], xmm0                 ;461.17
        add       eax, 4                                        ;461.17
        cmp       eax, edx                                      ;461.17
        jb        .B10.858      ; Prob 82%                      ;461.17
                                ; LOE eax edx ecx ebx esi edi xmm0
.B10.860:                       ; Preds .B10.858 .B10.864       ; Infreq
        cmp       edx, esi                                      ;461.17
        jae       .B10.194      ; Prob 10%                      ;461.17
                                ; LOE edx ecx ebx esi
.B10.861:                       ; Preds .B10.860                ; Infreq
        mov       eax, DWORD PTR [264+ecx+ebx]                  ;461.17
                                ; LOE eax edx esi
.B10.862:                       ; Preds .B10.862 .B10.861       ; Infreq
        mov       DWORD PTR [eax+edx*4], 0                      ;461.17
        inc       edx                                           ;461.17
        cmp       edx, esi                                      ;461.17
        jb        .B10.862      ; Prob 82%                      ;461.17
        jmp       .B10.194      ; Prob 100%                     ;461.17
                                ; LOE eax edx esi
.B10.864:                       ; Preds .B10.856                ; Infreq
        xor       edx, edx                                      ;461.17
        jmp       .B10.860      ; Prob 100%                     ;461.17
                                ; LOE edx ecx ebx esi
.B10.865:                       ; Preds .B10.202                ; Infreq
        cmp       ecx, 4                                        ;468.17
        jl        .B10.873      ; Prob 10%                      ;468.17
                                ; LOE edx ecx ebx esi edi bl bh
.B10.866:                       ; Preds .B10.865                ; Infreq
        mov       eax, ecx                                      ;468.17
        and       eax, -4                                       ;468.17
        pxor      xmm0, xmm0                                    ;468.17
        mov       DWORD PTR [504+esp], eax                      ;468.17
        mov       eax, DWORD PTR [300+edx+edi]                  ;468.17
        mov       DWORD PTR [144+esp], 0                        ;468.17
        mov       DWORD PTR [272+esp], eax                      ;468.17
        mov       DWORD PTR [76+esp], esi                       ;468.17
        mov       DWORD PTR [80+esp], ebx                       ;468.17
        mov       eax, DWORD PTR [504+esp]                      ;468.17
        mov       ebx, DWORD PTR [272+esp]                      ;468.17
        mov       esi, DWORD PTR [144+esp]                      ;468.17
                                ; LOE eax edx ecx ebx esi edi xmm0
.B10.867:                       ; Preds .B10.867 .B10.866       ; Infreq
        movdqu    XMMWORD PTR [ebx+esi*4], xmm0                 ;468.17
        add       esi, 4                                        ;468.17
        cmp       esi, eax                                      ;468.17
        jb        .B10.867      ; Prob 82%                      ;468.17
                                ; LOE eax edx ecx ebx esi edi xmm0
.B10.868:                       ; Preds .B10.867                ; Infreq
        mov       DWORD PTR [504+esp], eax                      ;
        mov       esi, DWORD PTR [76+esp]                       ;
        mov       ebx, DWORD PTR [80+esp]                       ;
                                ; LOE edx ecx ebx esi edi
.B10.869:                       ; Preds .B10.868 .B10.873       ; Infreq
        cmp       ecx, DWORD PTR [504+esp]                      ;468.17
        jbe       .B10.205      ; Prob 10%                      ;468.17
                                ; LOE edx ecx ebx esi edi
.B10.870:                       ; Preds .B10.869                ; Infreq
        mov       eax, DWORD PTR [300+edx+edi]                  ;468.17
        mov       edx, DWORD PTR [504+esp]                      ;468.17
                                ; LOE eax edx ecx ebx esi edi
.B10.871:                       ; Preds .B10.871 .B10.870       ; Infreq
        mov       DWORD PTR [eax+edx*4], 0                      ;468.17
        inc       edx                                           ;468.17
        cmp       edx, ecx                                      ;468.17
        jb        .B10.871      ; Prob 82%                      ;468.17
        jmp       .B10.205      ; Prob 100%                     ;468.17
                                ; LOE eax edx ecx ebx esi edi
.B10.873:                       ; Preds .B10.865                ; Infreq
        xor       eax, eax                                      ;468.17
        mov       DWORD PTR [504+esp], eax                      ;468.17
        jmp       .B10.869      ; Prob 100%                     ;468.17
                                ; LOE edx ecx ebx esi edi
.B10.874:                       ; Preds .B10.211                ; Infreq
        cmp       esi, 8                                        ;475.17
        jl        .B10.882      ; Prob 10%                      ;475.17
                                ; LOE ecx ebx esi edi bl bh
.B10.875:                       ; Preds .B10.874                ; Infreq
        xor       eax, eax                                      ;475.17
        mov       edx, esi                                      ;475.17
        mov       DWORD PTR [220+esp], eax                      ;475.17
        and       edx, -8                                       ;475.17
        mov       DWORD PTR [84+esp], ebx                       ;475.17
        pxor      xmm0, xmm0                                    ;475.17
        mov       eax, DWORD PTR [864+ecx]                      ;475.17
        mov       ebx, DWORD PTR [220+esp]                      ;475.17
                                ; LOE eax edx ecx ebx esi edi xmm0
.B10.876:                       ; Preds .B10.876 .B10.875       ; Infreq
        movups    XMMWORD PTR [eax+ebx*4], xmm0                 ;475.17
        movups    XMMWORD PTR [16+eax+ebx*4], xmm0              ;475.17
        add       ebx, 8                                        ;475.17
        cmp       ebx, edx                                      ;475.17
        jb        .B10.876      ; Prob 82%                      ;475.17
                                ; LOE eax edx ecx ebx esi edi xmm0
.B10.877:                       ; Preds .B10.876                ; Infreq
        mov       ebx, DWORD PTR [84+esp]                       ;
                                ; LOE edx ecx ebx esi edi
.B10.878:                       ; Preds .B10.877 .B10.882       ; Infreq
        cmp       edx, esi                                      ;475.17
        jae       .B10.214      ; Prob 10%                      ;475.17
                                ; LOE edx ecx ebx esi edi
.B10.879:                       ; Preds .B10.878                ; Infreq
        mov       ecx, DWORD PTR [864+ecx]                      ;475.17
        xor       eax, eax                                      ;475.17
                                ; LOE eax edx ecx ebx esi edi
.B10.880:                       ; Preds .B10.880 .B10.879       ; Infreq
        mov       DWORD PTR [ecx+edx*4], eax                    ;475.17
        inc       edx                                           ;475.17
        cmp       edx, esi                                      ;475.17
        jb        .B10.880      ; Prob 82%                      ;475.17
        jmp       .B10.214      ; Prob 100%                     ;475.17
                                ; LOE eax edx ecx ebx esi edi
.B10.882:                       ; Preds .B10.874                ; Infreq
        xor       edx, edx                                      ;475.17
        jmp       .B10.878      ; Prob 100%                     ;475.17
                                ; LOE edx ecx ebx esi edi
.B10.883:                       ; Preds .B10.220                ; Infreq
        cmp       ecx, 4                                        ;482.17
        jl        .B10.891      ; Prob 10%                      ;482.17
                                ; LOE ecx ebx esi edi bl bh
.B10.884:                       ; Preds .B10.883                ; Infreq
        xor       eax, eax                                      ;482.17
        mov       edx, ecx                                      ;482.17
        mov       DWORD PTR [496+esp], eax                      ;482.17
        and       edx, -4                                       ;482.17
        mov       eax, DWORD PTR [356+edi]                      ;482.17
        pxor      xmm0, xmm0                                    ;482.17
        mov       DWORD PTR [96+esp], ebx                       ;482.17
        mov       ebx, DWORD PTR [496+esp]                      ;482.17
                                ; LOE eax edx ecx ebx esi edi xmm0
.B10.885:                       ; Preds .B10.885 .B10.884       ; Infreq
        movdqu    XMMWORD PTR [eax+ebx*4], xmm0                 ;482.17
        add       ebx, 4                                        ;482.17
        cmp       ebx, edx                                      ;482.17
        jb        .B10.885      ; Prob 82%                      ;482.17
                                ; LOE eax edx ecx ebx esi edi xmm0
.B10.886:                       ; Preds .B10.885                ; Infreq
        mov       ebx, DWORD PTR [96+esp]                       ;
                                ; LOE edx ecx ebx esi edi
.B10.887:                       ; Preds .B10.886 .B10.891       ; Infreq
        cmp       edx, ecx                                      ;482.17
        jae       .B10.223      ; Prob 10%                      ;482.17
                                ; LOE edx ecx ebx esi edi
.B10.888:                       ; Preds .B10.887                ; Infreq
        mov       eax, DWORD PTR [356+edi]                      ;482.17
                                ; LOE eax edx ecx ebx esi
.B10.889:                       ; Preds .B10.889 .B10.888       ; Infreq
        mov       DWORD PTR [eax+edx*4], 0                      ;482.17
        inc       edx                                           ;482.17
        cmp       edx, ecx                                      ;482.17
        jb        .B10.889      ; Prob 82%                      ;482.17
        jmp       .B10.223      ; Prob 100%                     ;482.17
                                ; LOE eax edx ecx ebx esi
.B10.891:                       ; Preds .B10.883                ; Infreq
        xor       edx, edx                                      ;482.17
        jmp       .B10.887      ; Prob 100%                     ;482.17
                                ; LOE edx ecx ebx esi edi
.B10.892:                       ; Preds .B10.229                ; Infreq
        cmp       esi, 8                                        ;489.17
        jl        .B10.900      ; Prob 10%                      ;489.17
                                ; LOE ecx ebx esi edi bl bh
.B10.893:                       ; Preds .B10.892                ; Infreq
        xor       eax, eax                                      ;489.17
        mov       edx, esi                                      ;489.17
        mov       DWORD PTR [224+esp], eax                      ;489.17
        and       edx, -8                                       ;489.17
        mov       DWORD PTR [100+esp], ebx                      ;489.17
        pxor      xmm0, xmm0                                    ;489.17
        mov       eax, DWORD PTR [456+ecx]                      ;489.17
        mov       ebx, DWORD PTR [224+esp]                      ;489.17
                                ; LOE eax edx ecx ebx esi edi xmm0
.B10.894:                       ; Preds .B10.894 .B10.893       ; Infreq
        movups    XMMWORD PTR [eax+ebx*4], xmm0                 ;489.17
        movups    XMMWORD PTR [16+eax+ebx*4], xmm0              ;489.17
        add       ebx, 8                                        ;489.17
        cmp       ebx, edx                                      ;489.17
        jb        .B10.894      ; Prob 82%                      ;489.17
                                ; LOE eax edx ecx ebx esi edi xmm0
.B10.895:                       ; Preds .B10.894                ; Infreq
        mov       ebx, DWORD PTR [100+esp]                      ;
                                ; LOE edx ecx ebx esi edi
.B10.896:                       ; Preds .B10.895 .B10.900       ; Infreq
        cmp       edx, esi                                      ;489.17
        jae       .B10.232      ; Prob 10%                      ;489.17
                                ; LOE edx ecx ebx esi edi
.B10.897:                       ; Preds .B10.896                ; Infreq
        mov       ecx, DWORD PTR [456+ecx]                      ;489.17
        xor       eax, eax                                      ;489.17
                                ; LOE eax edx ecx ebx esi edi
.B10.898:                       ; Preds .B10.898 .B10.897       ; Infreq
        mov       DWORD PTR [ecx+edx*4], eax                    ;489.17
        inc       edx                                           ;489.17
        cmp       edx, esi                                      ;489.17
        jb        .B10.898      ; Prob 82%                      ;489.17
        jmp       .B10.232      ; Prob 100%                     ;489.17
                                ; LOE eax edx ecx ebx esi edi
.B10.900:                       ; Preds .B10.892                ; Infreq
        xor       edx, edx                                      ;489.17
        jmp       .B10.896      ; Prob 100%                     ;489.17
                                ; LOE edx ecx ebx esi edi
.B10.901:                       ; Preds .B10.238                ; Infreq
        cmp       edx, 8                                        ;496.17
        jl        .B10.909      ; Prob 10%                      ;496.17
                                ; LOE edx ecx
.B10.902:                       ; Preds .B10.901                ; Infreq
        mov       eax, edx                                      ;496.17
        xor       ebx, ebx                                      ;496.17
        and       eax, -8                                       ;496.17
        xor       esi, esi                                      ;496.17
        mov       ebx, DWORD PTR [492+ecx]                      ;496.17
        pxor      xmm0, xmm0                                    ;496.17
                                ; LOE eax edx ecx ebx esi xmm0
.B10.903:                       ; Preds .B10.903 .B10.902       ; Infreq
        movups    XMMWORD PTR [ebx+esi*4], xmm0                 ;496.17
        movups    XMMWORD PTR [16+ebx+esi*4], xmm0              ;496.17
        add       esi, 8                                        ;496.17
        cmp       esi, eax                                      ;496.17
        jb        .B10.903      ; Prob 82%                      ;496.17
                                ; LOE eax edx ecx ebx esi xmm0
.B10.905:                       ; Preds .B10.903 .B10.909       ; Infreq
        cmp       eax, edx                                      ;496.17
        jae       .B10.240      ; Prob 10%                      ;496.17
                                ; LOE eax edx ecx
.B10.906:                       ; Preds .B10.905                ; Infreq
        mov       ecx, DWORD PTR [492+ecx]                      ;496.17
        xor       ebx, ebx                                      ;496.17
                                ; LOE eax edx ecx ebx
.B10.907:                       ; Preds .B10.907 .B10.906       ; Infreq
        mov       DWORD PTR [ecx+eax*4], ebx                    ;496.17
        inc       eax                                           ;496.17
        cmp       eax, edx                                      ;496.17
        jb        .B10.907      ; Prob 82%                      ;496.17
        jmp       .B10.240      ; Prob 100%                     ;496.17
                                ; LOE eax edx ecx ebx
.B10.909:                       ; Preds .B10.901                ; Infreq
        xor       eax, eax                                      ;496.17
        jmp       .B10.905      ; Prob 100%                     ;496.17
                                ; LOE eax edx ecx
.B10.910:                       ; Preds .B10.248                ; Infreq
        cmp       esi, 4                                        ;503.17
        jl        .B10.918      ; Prob 10%                      ;503.17
                                ; LOE ecx ebx esi
.B10.911:                       ; Preds .B10.910                ; Infreq
        mov       edx, esi                                      ;503.17
        xor       eax, eax                                      ;503.17
        mov       edi, DWORD PTR [528+ecx+ebx]                  ;503.17
        and       edx, -4                                       ;503.17
        pxor      xmm0, xmm0                                    ;503.17
                                ; LOE eax edx ecx ebx esi edi xmm0
.B10.912:                       ; Preds .B10.912 .B10.911       ; Infreq
        movdqu    XMMWORD PTR [edi+eax*4], xmm0                 ;503.17
        add       eax, 4                                        ;503.17
        cmp       eax, edx                                      ;503.17
        jb        .B10.912      ; Prob 82%                      ;503.17
                                ; LOE eax edx ecx ebx esi edi xmm0
.B10.914:                       ; Preds .B10.912 .B10.918       ; Infreq
        cmp       edx, esi                                      ;503.17
        jae       .B10.250      ; Prob 10%                      ;503.17
                                ; LOE edx ecx ebx esi
.B10.915:                       ; Preds .B10.914                ; Infreq
        mov       eax, DWORD PTR [528+ecx+ebx]                  ;503.17
                                ; LOE eax edx esi
.B10.916:                       ; Preds .B10.916 .B10.915       ; Infreq
        mov       DWORD PTR [eax+edx*4], 0                      ;503.17
        inc       edx                                           ;503.17
        cmp       edx, esi                                      ;503.17
        jb        .B10.916      ; Prob 82%                      ;503.17
        jmp       .B10.250      ; Prob 100%                     ;503.17
                                ; LOE eax edx esi
.B10.918:                       ; Preds .B10.910                ; Infreq
        xor       edx, edx                                      ;503.17
        jmp       .B10.914      ; Prob 100%                     ;503.17
                                ; LOE edx ecx ebx esi
.B10.919:                       ; Preds .B10.258                ; Infreq
        cmp       esi, 4                                        ;510.17
        jl        .B10.927      ; Prob 10%                      ;510.17
                                ; LOE ecx ebx esi
.B10.920:                       ; Preds .B10.919                ; Infreq
        mov       edx, esi                                      ;510.17
        xor       eax, eax                                      ;510.17
        mov       edi, DWORD PTR [564+ecx+ebx]                  ;510.17
        and       edx, -4                                       ;510.17
        pxor      xmm0, xmm0                                    ;510.17
                                ; LOE eax edx ecx ebx esi edi xmm0
.B10.921:                       ; Preds .B10.921 .B10.920       ; Infreq
        movdqu    XMMWORD PTR [edi+eax*4], xmm0                 ;510.17
        add       eax, 4                                        ;510.17
        cmp       eax, edx                                      ;510.17
        jb        .B10.921      ; Prob 82%                      ;510.17
                                ; LOE eax edx ecx ebx esi edi xmm0
.B10.923:                       ; Preds .B10.921 .B10.927       ; Infreq
        cmp       edx, esi                                      ;510.17
        jae       .B10.260      ; Prob 10%                      ;510.17
                                ; LOE edx ecx ebx esi
.B10.924:                       ; Preds .B10.923                ; Infreq
        mov       eax, DWORD PTR [564+ecx+ebx]                  ;510.17
                                ; LOE eax edx esi
.B10.925:                       ; Preds .B10.925 .B10.924       ; Infreq
        mov       DWORD PTR [eax+edx*4], 0                      ;510.17
        inc       edx                                           ;510.17
        cmp       edx, esi                                      ;510.17
        jb        .B10.925      ; Prob 82%                      ;510.17
        jmp       .B10.260      ; Prob 100%                     ;510.17
                                ; LOE eax edx esi
.B10.927:                       ; Preds .B10.919                ; Infreq
        xor       edx, edx                                      ;510.17
        jmp       .B10.923      ; Prob 100%                     ;510.17
                                ; LOE edx ecx ebx esi
.B10.928:                       ; Preds .B10.268                ; Infreq
        cmp       ecx, 4                                        ;517.17
        jl        .B10.936      ; Prob 10%                      ;517.17
                                ; LOE edx ecx ebx esi edi bl bh
.B10.929:                       ; Preds .B10.928                ; Infreq
        mov       eax, ecx                                      ;517.17
        and       eax, -4                                       ;517.17
        pxor      xmm0, xmm0                                    ;517.17
        mov       DWORD PTR [500+esp], eax                      ;517.17
        mov       eax, DWORD PTR [600+edx+edi]                  ;517.17
        mov       DWORD PTR [228+esp], 0                        ;517.17
        mov       DWORD PTR [148+esp], eax                      ;517.17
        mov       DWORD PTR [104+esp], esi                      ;517.17
        mov       DWORD PTR [108+esp], ebx                      ;517.17
        mov       eax, DWORD PTR [500+esp]                      ;517.17
        mov       ebx, DWORD PTR [148+esp]                      ;517.17
        mov       esi, DWORD PTR [228+esp]                      ;517.17
                                ; LOE eax edx ecx ebx esi edi xmm0
.B10.930:                       ; Preds .B10.930 .B10.929       ; Infreq
        movdqu    XMMWORD PTR [ebx+esi*4], xmm0                 ;517.17
        add       esi, 4                                        ;517.17
        cmp       esi, eax                                      ;517.17
        jb        .B10.930      ; Prob 82%                      ;517.17
                                ; LOE eax edx ecx ebx esi edi xmm0
.B10.931:                       ; Preds .B10.930                ; Infreq
        mov       DWORD PTR [500+esp], eax                      ;
        mov       esi, DWORD PTR [104+esp]                      ;
        mov       ebx, DWORD PTR [108+esp]                      ;
                                ; LOE edx ecx ebx esi edi
.B10.932:                       ; Preds .B10.931 .B10.936       ; Infreq
        cmp       ecx, DWORD PTR [500+esp]                      ;517.17
        jbe       .B10.271      ; Prob 10%                      ;517.17
                                ; LOE edx ecx ebx esi edi
.B10.933:                       ; Preds .B10.932                ; Infreq
        mov       eax, DWORD PTR [600+edx+edi]                  ;517.17
        mov       edx, DWORD PTR [500+esp]                      ;517.17
                                ; LOE eax edx ecx ebx esi edi
.B10.934:                       ; Preds .B10.934 .B10.933       ; Infreq
        mov       DWORD PTR [eax+edx*4], 0                      ;517.17
        inc       edx                                           ;517.17
        cmp       edx, ecx                                      ;517.17
        jb        .B10.934      ; Prob 82%                      ;517.17
        jmp       .B10.271      ; Prob 100%                     ;517.17
                                ; LOE eax edx ecx ebx esi edi
.B10.936:                       ; Preds .B10.928                ; Infreq
        xor       eax, eax                                      ;517.17
        mov       DWORD PTR [500+esp], eax                      ;517.17
        jmp       .B10.932      ; Prob 100%                     ;517.17
                                ; LOE edx ecx ebx esi edi
.B10.937:                       ; Preds .B10.277                ; Infreq
        cmp       edx, 8                                        ;524.17
        jl        .B10.945      ; Prob 10%                      ;524.17
                                ; LOE edx ecx
.B10.938:                       ; Preds .B10.937                ; Infreq
        mov       eax, edx                                      ;524.17
        xor       ebx, ebx                                      ;524.17
        and       eax, -8                                       ;524.17
        xor       esi, esi                                      ;524.17
        mov       ebx, DWORD PTR [708+ecx]                      ;524.17
        pxor      xmm0, xmm0                                    ;524.17
                                ; LOE eax edx ecx ebx esi xmm0
.B10.939:                       ; Preds .B10.939 .B10.938       ; Infreq
        movdqu    XMMWORD PTR [ebx+esi*2], xmm0                 ;524.17
        add       esi, 8                                        ;524.17
        cmp       esi, eax                                      ;524.17
        jb        .B10.939      ; Prob 82%                      ;524.17
                                ; LOE eax edx ecx ebx esi xmm0
.B10.941:                       ; Preds .B10.939 .B10.945       ; Infreq
        cmp       eax, edx                                      ;524.17
        jae       .B10.279      ; Prob 10%                      ;524.17
                                ; LOE eax edx ecx
.B10.942:                       ; Preds .B10.941                ; Infreq
        mov       ecx, DWORD PTR [708+ecx]                      ;524.17
        xor       ebx, ebx                                      ;524.17
                                ; LOE eax edx ecx ebx
.B10.943:                       ; Preds .B10.943 .B10.942       ; Infreq
        mov       WORD PTR [ecx+eax*2], bx                      ;524.17
        inc       eax                                           ;524.17
        cmp       eax, edx                                      ;524.17
        jb        .B10.943      ; Prob 82%                      ;524.17
        jmp       .B10.279      ; Prob 100%                     ;524.17
                                ; LOE eax edx ecx ebx
.B10.945:                       ; Preds .B10.937                ; Infreq
        xor       eax, eax                                      ;524.17
        jmp       .B10.941      ; Prob 100%                     ;524.17
                                ; LOE eax edx ecx
.B10.946:                       ; Preds .B10.289                ; Infreq
        cmp       eax, 8                                        ;531.17
        jl        .B10.954      ; Prob 10%                      ;531.17
                                ; LOE eax edx ecx dl cl dh ch
.B10.947:                       ; Preds .B10.946                ; Infreq
        mov       esi, edx                                      ;531.17
        mov       edi, ecx                                      ;531.17
        pxor      xmm0, xmm0                                    ;
        mov       DWORD PTR [540+esp], eax                      ;
        and       eax, -8                                       ;531.17
        mov       DWORD PTR [544+esp], eax                      ;531.17
        mov       edx, DWORD PTR [848+esi+edi]                  ;531.17
        mov       ecx, DWORD PTR [808+esi+edi]                  ;531.17
        mov       esi, DWORD PTR [852+esi+edi]                  ;531.17
        imul      esi, edx                                      ;
        imul      edx, DWORD PTR [536+esp]                      ;
        mov       eax, DWORD PTR [528+esp]                      ;
        sub       ecx, esi                                      ;
        mov       DWORD PTR [524+esp], 0                        ;531.17
        mov       esi, DWORD PTR [524+esp]                      ;
        lea       ebx, DWORD PTR [eax*4]                        ;
        neg       ebx                                           ;
        lea       eax, DWORD PTR [edx+eax*4]                    ;
        add       ebx, ecx                                      ;
        add       ecx, edx                                      ;
        mov       edx, DWORD PTR [544+esp]                      ;
        add       ebx, eax                                      ;
        mov       eax, DWORD PTR [540+esp]                      ;
                                ; LOE eax edx ecx ebx esi xmm0
.B10.948:                       ; Preds .B10.948 .B10.947       ; Infreq
        movups    XMMWORD PTR [ecx+esi*4], xmm0                 ;531.17
        movups    XMMWORD PTR [16+ebx+esi*4], xmm0              ;531.17
        add       esi, 8                                        ;531.17
        cmp       esi, edx                                      ;531.17
        jb        .B10.948      ; Prob 82%                      ;531.17
                                ; LOE eax edx ecx ebx esi xmm0
.B10.949:                       ; Preds .B10.948                ; Infreq
        mov       DWORD PTR [544+esp], edx                      ;
                                ; LOE eax
.B10.950:                       ; Preds .B10.949 .B10.954       ; Infreq
        cmp       eax, DWORD PTR [544+esp]                      ;531.17
        jbe       .B10.291      ; Prob 10%                      ;531.17
                                ; LOE eax
.B10.951:                       ; Preds .B10.950                ; Infreq
        mov       ecx, DWORD PTR [520+esp]                      ;531.17
        mov       ebx, DWORD PTR [516+esp]                      ;531.17
        mov       edx, DWORD PTR [852+ecx+ebx]                  ;531.17
        neg       edx                                           ;
        add       edx, DWORD PTR [536+esp]                      ;
        imul      edx, DWORD PTR [848+ecx+ebx]                  ;
        add       edx, DWORD PTR [808+ecx+ebx]                  ;
        xor       ebx, ebx                                      ;
        mov       ecx, DWORD PTR [544+esp]                      ;
                                ; LOE eax edx ecx ebx
.B10.952:                       ; Preds .B10.952 .B10.951       ; Infreq
        mov       DWORD PTR [edx+ecx*4], ebx                    ;531.17
        inc       ecx                                           ;531.17
        cmp       ecx, eax                                      ;531.17
        jb        .B10.952      ; Prob 82%                      ;531.17
        jmp       .B10.291      ; Prob 100%                     ;531.17
                                ; LOE eax edx ecx ebx
.B10.954:                       ; Preds .B10.946                ; Infreq
        xor       edx, edx                                      ;531.17
        mov       DWORD PTR [544+esp], edx                      ;531.17
        jmp       .B10.950      ; Prob 100%                     ;531.17
                                ; LOE eax
.B10.955:                       ; Preds .B10.380                ; Infreq
        mov       esi, 1                                        ;
        jmp       .B10.384      ; Prob 100%                     ;
                                ; LOE edx ecx ebx esi
.B10.956:                       ; Preds .B10.374                ; Infreq
        mov       eax, 1                                        ;
        jmp       .B10.378      ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B10.957:                       ; Preds .B10.368                ; Infreq
        mov       eax, 1                                        ;
        jmp       .B10.372      ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B10.958:                       ; Preds .B10.362                ; Infreq
        mov       eax, 1                                        ;
        jmp       .B10.366      ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B10.959:                       ; Preds .B10.356                ; Infreq
        mov       eax, 1                                        ;
        jmp       .B10.360      ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B10.960:                       ; Preds .B10.350                ; Infreq
        mov       eax, 1                                        ;
        jmp       .B10.354      ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B10.961:                       ; Preds .B10.344                ; Infreq
        mov       eax, 1                                        ;
        jmp       .B10.348      ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B10.962:                       ; Preds .B10.338                ; Infreq
        mov       eax, 1                                        ;
        jmp       .B10.342      ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B10.963:                       ; Preds .B10.332                ; Infreq
        mov       eax, 1                                        ;
        jmp       .B10.336      ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B10.964:                       ; Preds .B10.326                ; Infreq
        mov       eax, 1                                        ;
        jmp       .B10.330      ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B10.965:                       ; Preds .B10.320                ; Infreq
        mov       eax, 1                                        ;
        jmp       .B10.324      ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B10.966:                       ; Preds .B10.314                ; Infreq
        mov       eax, 1                                        ;
        jmp       .B10.318      ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B10.967:                       ; Preds .B10.308                ; Infreq
        mov       eax, 1                                        ;
        jmp       .B10.312      ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B10.968:                       ; Preds .B10.302                ; Infreq
        mov       eax, 1                                        ;
        jmp       .B10.306      ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B10.969:                       ; Preds .B10.296                ; Infreq
        mov       eax, 1                                        ;
        jmp       .B10.300      ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B10.970:                       ; Preds .B10.412                ; Infreq
        cmp       ecx, 4                                        ;569.13
        jl        .B10.986      ; Prob 10%                      ;569.13
                                ; LOE edx ecx
.B10.971:                       ; Preds .B10.970                ; Infreq
        mov       eax, edx                                      ;569.13
        and       eax, 15                                       ;569.13
        je        .B10.974      ; Prob 50%                      ;569.13
                                ; LOE eax edx ecx
.B10.972:                       ; Preds .B10.971                ; Infreq
        test      al, 3                                         ;569.13
        jne       .B10.986      ; Prob 10%                      ;569.13
                                ; LOE eax edx ecx
.B10.973:                       ; Preds .B10.972                ; Infreq
        neg       eax                                           ;569.13
        add       eax, 16                                       ;569.13
        shr       eax, 2                                        ;569.13
                                ; LOE eax edx ecx
.B10.974:                       ; Preds .B10.973 .B10.971       ; Infreq
        lea       ebx, DWORD PTR [4+eax]                        ;569.13
        cmp       ecx, ebx                                      ;569.13
        jl        .B10.986      ; Prob 10%                      ;569.13
                                ; LOE eax edx ecx
.B10.975:                       ; Preds .B10.974                ; Infreq
        mov       esi, ecx                                      ;569.13
        sub       esi, eax                                      ;569.13
        and       esi, 3                                        ;569.13
        neg       esi                                           ;569.13
        add       esi, ecx                                      ;569.13
        test      eax, eax                                      ;569.13
        jbe       .B10.979      ; Prob 10%                      ;569.13
                                ; LOE eax edx ecx esi
.B10.976:                       ; Preds .B10.975                ; Infreq
        xor       ebx, ebx                                      ;
                                ; LOE eax edx ecx ebx esi
.B10.977:                       ; Preds .B10.977 .B10.976       ; Infreq
        mov       DWORD PTR [edx+ebx*4], 0                      ;569.13
        inc       ebx                                           ;569.13
        cmp       ebx, eax                                      ;569.13
        jb        .B10.977      ; Prob 82%                      ;569.13
                                ; LOE eax edx ecx ebx esi
.B10.979:                       ; Preds .B10.977 .B10.975       ; Infreq
        pxor      xmm0, xmm0                                    ;569.13
                                ; LOE eax edx ecx esi xmm0
.B10.980:                       ; Preds .B10.980 .B10.979       ; Infreq
        movdqa    XMMWORD PTR [edx+eax*4], xmm0                 ;569.13
        add       eax, 4                                        ;569.13
        cmp       eax, esi                                      ;569.13
        jb        .B10.980      ; Prob 82%                      ;569.13
                                ; LOE eax edx ecx esi xmm0
.B10.982:                       ; Preds .B10.980 .B10.986       ; Infreq
        cmp       esi, ecx                                      ;569.13
        jae       .B10.414      ; Prob 10%                      ;569.13
                                ; LOE edx ecx esi
.B10.984:                       ; Preds .B10.982 .B10.984       ; Infreq
        mov       DWORD PTR [edx+esi*4], 0                      ;569.13
        inc       esi                                           ;569.13
        cmp       esi, ecx                                      ;569.13
        jb        .B10.984      ; Prob 82%                      ;569.13
        jmp       .B10.414      ; Prob 100%                     ;569.13
                                ; LOE edx ecx esi
.B10.986:                       ; Preds .B10.970 .B10.974 .B10.972 ; Infreq
        xor       eax, eax                                      ;569.13
        xor       esi, esi                                      ;569.13
        jmp       .B10.982      ; Prob 100%                     ;569.13
                                ; LOE edx ecx esi
.B10.989:                       ; Preds .B10.431                ; Infreq
        cmp       ecx, 4                                        ;583.13
        jl        .B10.1005     ; Prob 10%                      ;583.13
                                ; LOE edx ecx
.B10.990:                       ; Preds .B10.989                ; Infreq
        mov       eax, edx                                      ;583.13
        and       eax, 15                                       ;583.13
        je        .B10.993      ; Prob 50%                      ;583.13
                                ; LOE eax edx ecx
.B10.991:                       ; Preds .B10.990                ; Infreq
        test      al, 3                                         ;583.13
        jne       .B10.1005     ; Prob 10%                      ;583.13
                                ; LOE eax edx ecx
.B10.992:                       ; Preds .B10.991                ; Infreq
        neg       eax                                           ;583.13
        add       eax, 16                                       ;583.13
        shr       eax, 2                                        ;583.13
                                ; LOE eax edx ecx
.B10.993:                       ; Preds .B10.992 .B10.990       ; Infreq
        lea       ebx, DWORD PTR [4+eax]                        ;583.13
        cmp       ecx, ebx                                      ;583.13
        jl        .B10.1005     ; Prob 10%                      ;583.13
                                ; LOE eax edx ecx
.B10.994:                       ; Preds .B10.993                ; Infreq
        mov       esi, ecx                                      ;583.13
        sub       esi, eax                                      ;583.13
        and       esi, 3                                        ;583.13
        neg       esi                                           ;583.13
        add       esi, ecx                                      ;583.13
        test      eax, eax                                      ;583.13
        jbe       .B10.998      ; Prob 10%                      ;583.13
                                ; LOE eax edx ecx esi
.B10.995:                       ; Preds .B10.994                ; Infreq
        xor       ebx, ebx                                      ;
                                ; LOE eax edx ecx ebx esi
.B10.996:                       ; Preds .B10.996 .B10.995       ; Infreq
        mov       DWORD PTR [edx+ebx*4], 0                      ;583.13
        inc       ebx                                           ;583.13
        cmp       ebx, eax                                      ;583.13
        jb        .B10.996      ; Prob 82%                      ;583.13
                                ; LOE eax edx ecx ebx esi
.B10.998:                       ; Preds .B10.996 .B10.994       ; Infreq
        pxor      xmm0, xmm0                                    ;583.13
                                ; LOE eax edx ecx esi xmm0
.B10.999:                       ; Preds .B10.999 .B10.998       ; Infreq
        movdqa    XMMWORD PTR [edx+eax*4], xmm0                 ;583.13
        add       eax, 4                                        ;583.13
        cmp       eax, esi                                      ;583.13
        jb        .B10.999      ; Prob 82%                      ;583.13
                                ; LOE eax edx ecx esi xmm0
.B10.1001:                      ; Preds .B10.999 .B10.1005      ; Infreq
        cmp       esi, ecx                                      ;583.13
        jae       .B10.433      ; Prob 10%                      ;583.13
                                ; LOE edx ecx esi
.B10.1003:                      ; Preds .B10.1001 .B10.1003     ; Infreq
        mov       DWORD PTR [edx+esi*4], 0                      ;583.13
        inc       esi                                           ;583.13
        cmp       esi, ecx                                      ;583.13
        jb        .B10.1003     ; Prob 82%                      ;583.13
        jmp       .B10.433      ; Prob 100%                     ;583.13
                                ; LOE edx ecx esi
.B10.1005:                      ; Preds .B10.989 .B10.993 .B10.991 ; Infreq
        xor       eax, eax                                      ;583.13
        xor       esi, esi                                      ;583.13
        jmp       .B10.1001     ; Prob 100%                     ;583.13
                                ; LOE edx ecx esi
.B10.1008:                      ; Preds .B10.464                ; Infreq
        mov       ebx, 1                                        ;
        jmp       .B10.468      ; Prob 100%                     ;
                                ; LOE edx ecx ebx esi
.B10.1009:                      ; Preds .B10.458                ; Infreq
        mov       eax, 1                                        ;
        jmp       .B10.462      ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B10.1010:                      ; Preds .B10.452                ; Infreq
        mov       eax, 1                                        ;
        jmp       .B10.456      ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B10.1011:                      ; Preds .B10.446                ; Infreq
        mov       eax, 1                                        ;
        jmp       .B10.450      ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B10.1012:                      ; Preds .B10.440                ; Infreq
        mov       eax, 1                                        ;
        jmp       .B10.444      ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B10.1013:                      ; Preds .B10.434                ; Infreq
        mov       eax, 1                                        ;
        jmp       .B10.438      ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B10.1014:                      ; Preds .B10.478                ; Infreq
        cmp       ecx, 4                                        ;597.10
        jl        .B10.1030     ; Prob 10%                      ;597.10
                                ; LOE edx ecx
.B10.1015:                      ; Preds .B10.1014               ; Infreq
        mov       eax, edx                                      ;597.10
        and       eax, 15                                       ;597.10
        je        .B10.1018     ; Prob 50%                      ;597.10
                                ; LOE eax edx ecx
.B10.1016:                      ; Preds .B10.1015               ; Infreq
        test      al, 3                                         ;597.10
        jne       .B10.1030     ; Prob 10%                      ;597.10
                                ; LOE eax edx ecx
.B10.1017:                      ; Preds .B10.1016               ; Infreq
        neg       eax                                           ;597.10
        add       eax, 16                                       ;597.10
        shr       eax, 2                                        ;597.10
                                ; LOE eax edx ecx
.B10.1018:                      ; Preds .B10.1017 .B10.1015     ; Infreq
        lea       ebx, DWORD PTR [4+eax]                        ;597.10
        cmp       ecx, ebx                                      ;597.10
        jl        .B10.1030     ; Prob 10%                      ;597.10
                                ; LOE eax edx ecx
.B10.1019:                      ; Preds .B10.1018               ; Infreq
        mov       esi, ecx                                      ;597.10
        sub       esi, eax                                      ;597.10
        and       esi, 3                                        ;597.10
        neg       esi                                           ;597.10
        add       esi, ecx                                      ;597.10
        test      eax, eax                                      ;597.10
        jbe       .B10.1023     ; Prob 10%                      ;597.10
                                ; LOE eax edx ecx esi
.B10.1020:                      ; Preds .B10.1019               ; Infreq
        xor       ebx, ebx                                      ;
                                ; LOE eax edx ecx ebx esi
.B10.1021:                      ; Preds .B10.1021 .B10.1020     ; Infreq
        mov       DWORD PTR [edx+ebx*4], 0                      ;597.10
        inc       ebx                                           ;597.10
        cmp       ebx, eax                                      ;597.10
        jb        .B10.1021     ; Prob 82%                      ;597.10
                                ; LOE eax edx ecx ebx esi
.B10.1023:                      ; Preds .B10.1021 .B10.1019     ; Infreq
        pxor      xmm0, xmm0                                    ;597.10
                                ; LOE eax edx ecx esi xmm0
.B10.1024:                      ; Preds .B10.1024 .B10.1023     ; Infreq
        movdqa    XMMWORD PTR [edx+eax*4], xmm0                 ;597.10
        add       eax, 4                                        ;597.10
        cmp       eax, esi                                      ;597.10
        jb        .B10.1024     ; Prob 82%                      ;597.10
                                ; LOE eax edx ecx esi xmm0
.B10.1026:                      ; Preds .B10.1024 .B10.1030     ; Infreq
        cmp       esi, ecx                                      ;597.10
        jae       .B10.480      ; Prob 10%                      ;597.10
                                ; LOE edx ecx esi
.B10.1028:                      ; Preds .B10.1026 .B10.1028     ; Infreq
        mov       DWORD PTR [edx+esi*4], 0                      ;597.10
        inc       esi                                           ;597.10
        cmp       esi, ecx                                      ;597.10
        jb        .B10.1028     ; Prob 82%                      ;597.10
        jmp       .B10.480      ; Prob 100%                     ;597.10
                                ; LOE edx ecx esi
.B10.1030:                      ; Preds .B10.1014 .B10.1018 .B10.1016 ; Infreq
        xor       eax, eax                                      ;597.10
        xor       esi, esi                                      ;597.10
        jmp       .B10.1026     ; Prob 100%                     ;597.10
                                ; LOE edx ecx esi
.B10.1033:                      ; Preds .B10.541                ; Infreq
        mov       ebx, 1                                        ;
        jmp       .B10.545      ; Prob 100%                     ;
                                ; LOE edx ecx ebx esi
.B10.1034:                      ; Preds .B10.535                ; Infreq
        mov       eax, 1                                        ;
        jmp       .B10.539      ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B10.1035:                      ; Preds .B10.529                ; Infreq
        mov       eax, 1                                        ;
        jmp       .B10.533      ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B10.1036:                      ; Preds .B10.523                ; Infreq
        mov       eax, 1                                        ;
        jmp       .B10.527      ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B10.1037:                      ; Preds .B10.517                ; Infreq
        mov       eax, 1                                        ;
        jmp       .B10.521      ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B10.1038:                      ; Preds .B10.511                ; Infreq
        mov       eax, 1                                        ;
        jmp       .B10.515      ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B10.1039:                      ; Preds .B10.505                ; Infreq
        mov       eax, 1                                        ;
        jmp       .B10.509      ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B10.1040:                      ; Preds .B10.499                ; Infreq
        mov       eax, 1                                        ;
        jmp       .B10.503      ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B10.1041:                      ; Preds .B10.493                ; Infreq
        mov       eax, 1                                        ;
        jmp       .B10.497      ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B10.1042:                      ; Preds .B10.487                ; Infreq
        mov       eax, 1                                        ;
        jmp       .B10.491      ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B10.1043:                      ; Preds .B10.481                ; Infreq
        mov       eax, 1                                        ;
        jmp       .B10.485      ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B10.1044:                      ; Preds .B10.601                ; Infreq
        mov       ebx, 1                                        ;
        jmp       .B10.605      ; Prob 100%                     ;
                                ; LOE edx ecx ebx esi
.B10.1045:                      ; Preds .B10.595                ; Infreq
        mov       eax, 1                                        ;
        jmp       .B10.599      ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B10.1046:                      ; Preds .B10.589                ; Infreq
        mov       eax, 1                                        ;
        jmp       .B10.593      ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B10.1047:                      ; Preds .B10.583                ; Infreq
        mov       eax, 1                                        ;
        jmp       .B10.587      ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B10.1048:                      ; Preds .B10.577                ; Infreq
        mov       eax, 1                                        ;
        jmp       .B10.581      ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B10.1049:                      ; Preds .B10.571                ; Infreq
        mov       eax, 1                                        ;
        jmp       .B10.575      ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B10.1050:                      ; Preds .B10.565                ; Infreq
        mov       eax, 1                                        ;
        jmp       .B10.569      ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B10.1051:                      ; Preds .B10.559                ; Infreq
        mov       eax, 1                                        ;
        jmp       .B10.563      ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B10.1052:                      ; Preds .B10.758                ; Infreq
        mov       ebx, 1                                        ;
        jmp       .B10.762      ; Prob 100%                     ;
                                ; LOE edx ecx ebx esi
.B10.1053:                      ; Preds .B10.752                ; Infreq
        mov       eax, 1                                        ;
        jmp       .B10.756      ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B10.1054:                      ; Preds .B10.746                ; Infreq
        mov       eax, 1                                        ;
        jmp       .B10.750      ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B10.1055:                      ; Preds .B10.740                ; Infreq
        mov       eax, 1                                        ;
        jmp       .B10.744      ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B10.1056:                      ; Preds .B10.734                ; Infreq
        mov       eax, 1                                        ;
        jmp       .B10.738      ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B10.1057:                      ; Preds .B10.728                ; Infreq
        mov       eax, 1                                        ;
        jmp       .B10.732      ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B10.1058:                      ; Preds .B10.722                ; Infreq
        mov       eax, 1                                        ;
        jmp       .B10.726      ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B10.1059:                      ; Preds .B10.716                ; Infreq
        mov       eax, 1                                        ;
        jmp       .B10.720      ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B10.1060:                      ; Preds .B10.710                ; Infreq
        mov       eax, 1                                        ;
        jmp       .B10.714      ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B10.1061:                      ; Preds .B10.704                ; Infreq
        mov       eax, 1                                        ;
        jmp       .B10.708      ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B10.1062:                      ; Preds .B10.698                ; Infreq
        mov       eax, 1                                        ;
        jmp       .B10.702      ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B10.1063:                      ; Preds .B10.692                ; Infreq
        mov       eax, 1                                        ;
        jmp       .B10.696      ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B10.1064:                      ; Preds .B10.686                ; Infreq
        mov       eax, 1                                        ;
        jmp       .B10.690      ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B10.1065:                      ; Preds .B10.680                ; Infreq
        mov       eax, 1                                        ;
        jmp       .B10.684      ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B10.1066:                      ; Preds .B10.674                ; Infreq
        mov       eax, 1                                        ;
        jmp       .B10.678      ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B10.1067:                      ; Preds .B10.668                ; Infreq
        mov       eax, 1                                        ;
        jmp       .B10.672      ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B10.1068:                      ; Preds .B10.662                ; Infreq
        mov       eax, 1                                        ;
        jmp       .B10.666      ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B10.1069:                      ; Preds .B10.656                ; Infreq
        mov       eax, 1                                        ;
        jmp       .B10.660      ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B10.1070:                      ; Preds .B10.650                ; Infreq
        mov       eax, 1                                        ;
        jmp       .B10.654      ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B10.1071:                      ; Preds .B10.644                ; Infreq
        mov       eax, 1                                        ;
        jmp       .B10.648      ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B10.1072:                      ; Preds .B10.638                ; Infreq
        mov       eax, 1                                        ;
        jmp       .B10.642      ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B10.1073:                      ; Preds .B10.632                ; Infreq
        mov       eax, 1                                        ;
        jmp       .B10.636      ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B10.1074:                      ; Preds .B10.626                ; Infreq
        mov       eax, 1                                        ;
        jmp       .B10.630      ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B10.1075:                      ; Preds .B10.620                ; Infreq
        mov       eax, 1                                        ;
        jmp       .B10.624      ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B10.1076:                      ; Preds .B10.614                ; Infreq
        mov       eax, 1                                        ;
        jmp       .B10.618      ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B10.1077:                      ; Preds .B10.608                ; Infreq
        mov       eax, 1                                        ;
        jmp       .B10.612      ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B10.1078:                      ; Preds .B10.774 .B10.772       ; Infreq
        sub       edi, ebx                                      ;
        xor       ecx, ecx                                      ;667.10
        mov       DWORD PTR [esp], edi                          ;
        jmp       .B10.782      ; Prob 100%                     ;
                                ; LOE ecx ebx esi
.B10.1079:                      ; Preds .B10.794                ; Infreq
        cmp       ecx, 4                                        ;684.10
        jl        .B10.1095     ; Prob 10%                      ;684.10
                                ; LOE edx ecx
.B10.1080:                      ; Preds .B10.1079               ; Infreq
        mov       eax, edx                                      ;684.10
        and       eax, 15                                       ;684.10
        je        .B10.1083     ; Prob 50%                      ;684.10
                                ; LOE eax edx ecx
.B10.1081:                      ; Preds .B10.1080               ; Infreq
        test      al, 3                                         ;684.10
        jne       .B10.1095     ; Prob 10%                      ;684.10
                                ; LOE eax edx ecx
.B10.1082:                      ; Preds .B10.1081               ; Infreq
        neg       eax                                           ;684.10
        add       eax, 16                                       ;684.10
        shr       eax, 2                                        ;684.10
                                ; LOE eax edx ecx
.B10.1083:                      ; Preds .B10.1082 .B10.1080     ; Infreq
        lea       ebx, DWORD PTR [4+eax]                        ;684.10
        cmp       ecx, ebx                                      ;684.10
        jl        .B10.1095     ; Prob 10%                      ;684.10
                                ; LOE eax edx ecx
.B10.1084:                      ; Preds .B10.1083               ; Infreq
        mov       esi, ecx                                      ;684.10
        sub       esi, eax                                      ;684.10
        and       esi, 3                                        ;684.10
        neg       esi                                           ;684.10
        add       esi, ecx                                      ;684.10
        test      eax, eax                                      ;684.10
        jbe       .B10.1088     ; Prob 10%                      ;684.10
                                ; LOE eax edx ecx esi
.B10.1085:                      ; Preds .B10.1084               ; Infreq
        xor       ebx, ebx                                      ;
                                ; LOE eax edx ecx ebx esi
.B10.1086:                      ; Preds .B10.1086 .B10.1085     ; Infreq
        mov       DWORD PTR [edx+ebx*4], 0                      ;684.10
        inc       ebx                                           ;684.10
        cmp       ebx, eax                                      ;684.10
        jb        .B10.1086     ; Prob 82%                      ;684.10
                                ; LOE eax edx ecx ebx esi
.B10.1088:                      ; Preds .B10.1086 .B10.1084     ; Infreq
        pxor      xmm0, xmm0                                    ;684.10
                                ; LOE eax edx ecx esi xmm0
.B10.1089:                      ; Preds .B10.1089 .B10.1088     ; Infreq
        movdqa    XMMWORD PTR [edx+eax*4], xmm0                 ;684.10
        add       eax, 4                                        ;684.10
        cmp       eax, esi                                      ;684.10
        jb        .B10.1089     ; Prob 82%                      ;684.10
                                ; LOE eax edx ecx esi xmm0
.B10.1091:                      ; Preds .B10.1089 .B10.1095     ; Infreq
        cmp       esi, ecx                                      ;684.10
        jae       .B10.796      ; Prob 10%                      ;684.10
                                ; LOE edx ecx esi
.B10.1093:                      ; Preds .B10.1091 .B10.1093     ; Infreq
        mov       DWORD PTR [edx+esi*4], 0                      ;684.10
        inc       esi                                           ;684.10
        cmp       esi, ecx                                      ;684.10
        jb        .B10.1093     ; Prob 82%                      ;684.10
        jmp       .B10.796      ; Prob 100%                     ;684.10
                                ; LOE edx ecx esi
.B10.1095:                      ; Preds .B10.1079 .B10.1083 .B10.1081 ; Infreq
        xor       eax, eax                                      ;684.10
        xor       esi, esi                                      ;684.10
        jmp       .B10.1091     ; Prob 100%                     ;684.10
                                ; LOE edx ecx esi
.B10.1098:                      ; Preds .B10.813                ; Infreq
        mov       DWORD PTR [4+esp], eax                        ;
                                ; LOE
.B10.1099:                      ; Preds .B10.810 .B10.1098      ; Infreq
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+24] ;703.10
        test      edx, edx                                      ;703.10
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;703.10
        jle       .B10.1118     ; Prob 50%                      ;703.10
                                ; LOE edx esi
.B10.1100:                      ; Preds .B10.1099               ; Infreq
        mov       ebx, edx                                      ;703.10
        shr       ebx, 31                                       ;703.10
        add       ebx, edx                                      ;703.10
        sar       ebx, 1                                        ;703.10
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;703.10
        test      ebx, ebx                                      ;703.10
        jbe       .B10.1122     ; Prob 10%                      ;703.10
                                ; LOE edx ecx ebx esi
.B10.1101:                      ; Preds .B10.1100               ; Infreq
        xor       eax, eax                                      ;
        mov       DWORD PTR [esp], esi                          ;
        xor       edi, edi                                      ;
        mov       esi, eax                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.1102:                      ; Preds .B10.1102 .B10.1101     ; Infreq
        inc       edi                                           ;703.10
        mov       DWORD PTR [856+esi+ecx], eax                  ;703.10
        mov       DWORD PTR [1756+esi+ecx], eax                 ;703.10
        add       esi, 1800                                     ;703.10
        cmp       edi, ebx                                      ;703.10
        jb        .B10.1102     ; Prob 63%                      ;703.10
                                ; LOE eax edx ecx ebx esi edi
.B10.1103:                      ; Preds .B10.1102               ; Infreq
        mov       esi, DWORD PTR [esp]                          ;
        lea       eax, DWORD PTR [1+edi+edi]                    ;703.10
                                ; LOE eax edx ecx ebx esi
.B10.1104:                      ; Preds .B10.1103 .B10.1122     ; Infreq
        lea       edi, DWORD PTR [-1+eax]                       ;703.10
        cmp       edx, edi                                      ;703.10
        jbe       .B10.1106     ; Prob 10%                      ;703.10
                                ; LOE eax edx ecx ebx esi
.B10.1105:                      ; Preds .B10.1104               ; Infreq
        mov       edi, esi                                      ;703.10
        add       eax, esi                                      ;703.10
        neg       edi                                           ;703.10
        add       edi, eax                                      ;703.10
        imul      eax, edi, 900                                 ;703.10
        mov       DWORD PTR [-44+ecx+eax], 0                    ;703.10
                                ; LOE edx ecx ebx esi
.B10.1106:                      ; Preds .B10.1104 .B10.1105     ; Infreq
        test      ebx, ebx                                      ;704.10
        jbe       .B10.1121     ; Prob 10%                      ;704.10
                                ; LOE edx ecx ebx esi
.B10.1107:                      ; Preds .B10.1106               ; Infreq
        xor       eax, eax                                      ;
        mov       DWORD PTR [esp], esi                          ;
        xor       edi, edi                                      ;
        mov       esi, eax                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.1108:                      ; Preds .B10.1108 .B10.1107     ; Infreq
        inc       edi                                           ;704.10
        mov       WORD PTR [860+esi+ecx], ax                    ;704.10
        mov       WORD PTR [1760+esi+ecx], ax                   ;704.10
        add       esi, 1800                                     ;704.10
        cmp       edi, ebx                                      ;704.10
        jb        .B10.1108     ; Prob 63%                      ;704.10
                                ; LOE eax edx ecx ebx esi edi
.B10.1109:                      ; Preds .B10.1108               ; Infreq
        mov       esi, DWORD PTR [esp]                          ;
        lea       eax, DWORD PTR [1+edi+edi]                    ;704.10
                                ; LOE eax edx ecx ebx esi
.B10.1110:                      ; Preds .B10.1109 .B10.1121     ; Infreq
        lea       edi, DWORD PTR [-1+eax]                       ;704.10
        cmp       edx, edi                                      ;704.10
        jbe       .B10.1112     ; Prob 10%                      ;704.10
                                ; LOE eax edx ecx ebx esi
.B10.1111:                      ; Preds .B10.1110               ; Infreq
        mov       edi, esi                                      ;704.10
        add       eax, esi                                      ;704.10
        neg       edi                                           ;704.10
        add       edi, eax                                      ;704.10
        xor       eax, eax                                      ;704.10
        imul      edi, edi, 900                                 ;704.10
        mov       WORD PTR [-40+ecx+edi], ax                    ;704.10
                                ; LOE edx ecx ebx esi
.B10.1112:                      ; Preds .B10.1110 .B10.1111     ; Infreq
        test      ebx, ebx                                      ;705.10
        jbe       .B10.1120     ; Prob 10%                      ;705.10
                                ; LOE edx ecx ebx esi
.B10.1113:                      ; Preds .B10.1112               ; Infreq
        xor       eax, eax                                      ;
        mov       DWORD PTR [esp], esi                          ;
        xor       edi, edi                                      ;
        mov       esi, eax                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.1114:                      ; Preds .B10.1114 .B10.1113     ; Infreq
        inc       edi                                           ;705.10
        mov       WORD PTR [862+esi+ecx], ax                    ;705.10
        mov       WORD PTR [1762+esi+ecx], ax                   ;705.10
        add       esi, 1800                                     ;705.10
        cmp       edi, ebx                                      ;705.10
        jb        .B10.1114     ; Prob 63%                      ;705.10
                                ; LOE eax edx ecx ebx esi edi
.B10.1115:                      ; Preds .B10.1114               ; Infreq
        mov       esi, DWORD PTR [esp]                          ;
        lea       ebx, DWORD PTR [1+edi+edi]                    ;705.10
                                ; LOE edx ecx ebx esi
.B10.1116:                      ; Preds .B10.1115 .B10.1120     ; Infreq
        lea       eax, DWORD PTR [-1+ebx]                       ;705.10
        cmp       edx, eax                                      ;705.10
        jbe       .B10.1118     ; Prob 10%                      ;705.10
                                ; LOE ecx ebx esi
.B10.1117:                      ; Preds .B10.1116               ; Infreq
        mov       edi, esi                                      ;705.10
        add       ebx, esi                                      ;705.10
        neg       edi                                           ;705.10
        xor       eax, eax                                      ;705.10
        add       edi, ebx                                      ;705.10
        imul      edx, edi, 900                                 ;705.10
        mov       WORD PTR [-38+ecx+edx], ax                    ;705.10
                                ; LOE
.B10.1118:                      ; Preds .B10.1099 .B10.1116 .B10.1117 ; Infreq
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ITERATION], 0 ;708.24
        je        .B10.1123     ; Prob 5%                       ;708.24
                                ; LOE
.B10.1119:                      ; Preds .B10.1118 .B10.1130 .B10.1195 ; Infreq
        add       esp, 548                                      ;728.9
        pop       ebx                                           ;728.9
        pop       edi                                           ;728.9
        pop       esi                                           ;728.9
        mov       esp, ebp                                      ;728.9
        pop       ebp                                           ;728.9
        ret                                                     ;728.9
                                ; LOE
.B10.1120:                      ; Preds .B10.1112               ; Infreq
        mov       ebx, 1                                        ;
        jmp       .B10.1116     ; Prob 100%                     ;
                                ; LOE edx ecx ebx esi
.B10.1121:                      ; Preds .B10.1106               ; Infreq
        mov       eax, 1                                        ;
        jmp       .B10.1110     ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B10.1122:                      ; Preds .B10.1100               ; Infreq
        mov       eax, 1                                        ;
        jmp       .B10.1104     ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B10.1123:                      ; Preds .B10.1118               ; Infreq
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFARCS] ;709.13
        xor       eax, eax                                      ;709.13
        test      edx, edx                                      ;709.13
        mov       esi, DWORD PTR [4+esp]                        ;413.13
        mov       DWORD PTR [508+esp], esi                      ;413.13
        lea       ecx, DWORD PTR [24+esp]                       ;709.13
        push      152                                           ;709.13
        cmovle    edx, eax                                      ;709.13
        push      edx                                           ;709.13
        push      2                                             ;709.13
        push      ecx                                           ;709.13
        call      _for_check_mult_overflow                      ;709.13
                                ; LOE eax
.B10.1124:                      ; Preds .B10.1123               ; Infreq
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+12] ;709.13
        and       eax, 1                                        ;709.13
        and       edx, 1                                        ;709.13
        add       edx, edx                                      ;709.13
        shl       eax, 4                                        ;709.13
        or        edx, 1                                        ;709.13
        or        edx, eax                                      ;709.13
        or        edx, 262144                                   ;709.13
        push      edx                                           ;709.13
        push      OFFSET FLAT: _DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE ;709.13
        push      DWORD PTR [48+esp]                            ;709.13
        call      _for_alloc_allocatable                        ;709.13
                                ; LOE eax
.B10.1409:                      ; Preds .B10.1124               ; Infreq
        add       esp, 28                                       ;709.13
        mov       ebx, eax                                      ;709.13
                                ; LOE ebx
.B10.1125:                      ; Preds .B10.1409               ; Infreq
        test      ebx, ebx                                      ;709.13
        je        .B10.1128     ; Prob 50%                      ;709.13
                                ; LOE ebx
.B10.1126:                      ; Preds .B10.1125               ; Infreq
        mov       DWORD PTR [160+esp], 0                        ;711.14
        lea       eax, DWORD PTR [16+esp]                       ;711.14
        mov       DWORD PTR [16+esp], 59                        ;711.14
        mov       DWORD PTR [20+esp], OFFSET FLAT: __STRLITPACK_20 ;711.14
        push      32                                            ;711.14
        push      eax                                           ;711.14
        push      OFFSET FLAT: __STRLITPACK_204.0.10            ;711.14
        push      -2088435968                                   ;711.14
        push      911                                           ;711.14
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], ebx ;709.13
        lea       edx, DWORD PTR [180+esp]                      ;711.14
        push      edx                                           ;711.14
        call      _for_write_seq_lis                            ;711.14
                                ; LOE
.B10.1127:                      ; Preds .B10.1126               ; Infreq
        push      32                                            ;712.14
        xor       eax, eax                                      ;712.14
        push      eax                                           ;712.14
        push      eax                                           ;712.14
        push      -2088435968                                   ;712.14
        push      eax                                           ;712.14
        push      OFFSET FLAT: __STRLITPACK_205                 ;712.14
        call      _for_stop_core                                ;712.14
                                ; LOE
.B10.1410:                      ; Preds .B10.1127               ; Infreq
        add       esp, 48                                       ;712.14
        jmp       .B10.1130     ; Prob 100%                     ;712.14
                                ; LOE
.B10.1128:                      ; Preds .B10.1125               ; Infreq
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFARCS] ;709.13
        xor       eax, eax                                      ;709.13
        test      esi, esi                                      ;709.13
        cmovle    esi, eax                                      ;709.13
        mov       ecx, 152                                      ;709.13
        lea       edi, DWORD PTR [esp]                          ;709.13
        push      ecx                                           ;709.13
        push      esi                                           ;709.13
        push      2                                             ;709.13
        push      edi                                           ;709.13
        mov       edx, 1                                        ;709.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+12], 133 ;709.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+4], ecx ;709.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+16], edx ;709.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+8], eax ;709.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32], edx ;709.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+24], esi ;709.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+28], ecx ;709.13
        call      _for_check_mult_overflow                      ;709.13
                                ; LOE ebx
.B10.1411:                      ; Preds .B10.1128               ; Infreq
        add       esp, 16                                       ;709.13
                                ; LOE ebx
.B10.1129:                      ; Preds .B10.1411               ; Infreq
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], ebx ;709.13
                                ; LOE
.B10.1130:                      ; Preds .B10.1410 .B10.1129     ; Infreq
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+24] ;715.13
        test      eax, eax                                      ;715.13
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;715.13
        jle       .B10.1119     ; Prob 50%                      ;715.13
                                ; LOE eax ecx
.B10.1131:                      ; Preds .B10.1130               ; Infreq
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;715.13
        mov       DWORD PTR [28+esp], edx                       ;715.13
        mov       edx, eax                                      ;715.13
        shr       edx, 31                                       ;715.13
        add       edx, eax                                      ;715.13
        sar       edx, 1                                        ;715.13
        test      edx, edx                                      ;715.13
        jbe       .B10.1207     ; Prob 10%                      ;715.13
                                ; LOE eax edx ecx
.B10.1132:                      ; Preds .B10.1131               ; Infreq
        xor       ebx, ebx                                      ;
        mov       DWORD PTR [4+esp], ecx                        ;
        xor       esi, esi                                      ;
        mov       edi, DWORD PTR [28+esp]                       ;
        xor       ecx, ecx                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.1133:                      ; Preds .B10.1133 .B10.1132     ; Infreq
        inc       esi                                           ;715.13
        mov       DWORD PTR [ebx+edi], ecx                      ;715.13
        mov       DWORD PTR [152+ebx+edi], ecx                  ;715.13
        add       ebx, 304                                      ;715.13
        cmp       esi, edx                                      ;715.13
        jb        .B10.1133     ; Prob 64%                      ;715.13
                                ; LOE eax edx ecx ebx esi edi
.B10.1134:                      ; Preds .B10.1133               ; Infreq
        mov       ecx, DWORD PTR [4+esp]                        ;
        lea       esi, DWORD PTR [1+esi+esi]                    ;715.13
                                ; LOE eax edx ecx esi
.B10.1135:                      ; Preds .B10.1134 .B10.1207     ; Infreq
        lea       ebx, DWORD PTR [-1+esi]                       ;715.13
        cmp       eax, ebx                                      ;715.13
        jbe       .B10.1137     ; Prob 10%                      ;715.13
                                ; LOE eax edx ecx esi
.B10.1136:                      ; Preds .B10.1135               ; Infreq
        mov       ebx, ecx                                      ;715.13
        add       esi, ecx                                      ;715.13
        neg       ebx                                           ;715.13
        add       ebx, esi                                      ;715.13
        imul      edi, ebx, 152                                 ;715.13
        mov       esi, DWORD PTR [28+esp]                       ;715.13
        mov       DWORD PTR [-152+esi+edi], 0                   ;715.13
                                ; LOE eax edx ecx
.B10.1137:                      ; Preds .B10.1135 .B10.1136     ; Infreq
        test      edx, edx                                      ;716.13
        jbe       .B10.1206     ; Prob 10%                      ;716.13
                                ; LOE eax edx ecx
.B10.1138:                      ; Preds .B10.1137               ; Infreq
        xor       ebx, ebx                                      ;
        mov       DWORD PTR [4+esp], ecx                        ;
        xor       esi, esi                                      ;
        mov       edi, DWORD PTR [28+esp]                       ;
        xor       ecx, ecx                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.1139:                      ; Preds .B10.1139 .B10.1138     ; Infreq
        inc       esi                                           ;716.13
        mov       DWORD PTR [4+ebx+edi], ecx                    ;716.13
        mov       DWORD PTR [156+ebx+edi], ecx                  ;716.13
        add       ebx, 304                                      ;716.13
        cmp       esi, edx                                      ;716.13
        jb        .B10.1139     ; Prob 64%                      ;716.13
                                ; LOE eax edx ecx ebx esi edi
.B10.1140:                      ; Preds .B10.1139               ; Infreq
        mov       ecx, DWORD PTR [4+esp]                        ;
        lea       esi, DWORD PTR [1+esi+esi]                    ;716.13
                                ; LOE eax edx ecx esi
.B10.1141:                      ; Preds .B10.1140 .B10.1206     ; Infreq
        lea       ebx, DWORD PTR [-1+esi]                       ;716.13
        cmp       eax, ebx                                      ;716.13
        jbe       .B10.1143     ; Prob 10%                      ;716.13
                                ; LOE eax edx ecx esi
.B10.1142:                      ; Preds .B10.1141               ; Infreq
        mov       ebx, ecx                                      ;716.13
        add       esi, ecx                                      ;716.13
        neg       ebx                                           ;716.13
        add       ebx, esi                                      ;716.13
        imul      edi, ebx, 152                                 ;716.13
        mov       esi, DWORD PTR [28+esp]                       ;716.13
        mov       DWORD PTR [-148+esi+edi], 0                   ;716.13
                                ; LOE eax edx ecx
.B10.1143:                      ; Preds .B10.1141 .B10.1142     ; Infreq
        test      edx, edx                                      ;717.13
        jbe       .B10.1205     ; Prob 10%                      ;717.13
                                ; LOE eax edx ecx
.B10.1144:                      ; Preds .B10.1143               ; Infreq
        xor       ebx, ebx                                      ;
        mov       DWORD PTR [4+esp], ecx                        ;
        xor       esi, esi                                      ;
        mov       edi, DWORD PTR [28+esp]                       ;
        xor       ecx, ecx                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.1145:                      ; Preds .B10.1145 .B10.1144     ; Infreq
        inc       esi                                           ;717.13
        mov       DWORD PTR [8+ebx+edi], ecx                    ;717.13
        mov       DWORD PTR [160+ebx+edi], ecx                  ;717.13
        add       ebx, 304                                      ;717.13
        cmp       esi, edx                                      ;717.13
        jb        .B10.1145     ; Prob 64%                      ;717.13
                                ; LOE eax edx ecx ebx esi edi
.B10.1146:                      ; Preds .B10.1145               ; Infreq
        mov       ecx, DWORD PTR [4+esp]                        ;
        lea       esi, DWORD PTR [1+esi+esi]                    ;717.13
                                ; LOE eax edx ecx esi
.B10.1147:                      ; Preds .B10.1146 .B10.1205     ; Infreq
        lea       ebx, DWORD PTR [-1+esi]                       ;717.13
        cmp       eax, ebx                                      ;717.13
        jbe       .B10.1149     ; Prob 10%                      ;717.13
                                ; LOE eax edx ecx esi
.B10.1148:                      ; Preds .B10.1147               ; Infreq
        mov       ebx, ecx                                      ;717.13
        add       esi, ecx                                      ;717.13
        neg       ebx                                           ;717.13
        add       ebx, esi                                      ;717.13
        imul      edi, ebx, 152                                 ;717.13
        mov       esi, DWORD PTR [28+esp]                       ;717.13
        mov       DWORD PTR [-144+esi+edi], 0                   ;717.13
                                ; LOE eax edx ecx
.B10.1149:                      ; Preds .B10.1147 .B10.1148     ; Infreq
        test      edx, edx                                      ;718.13
        jbe       .B10.1204     ; Prob 10%                      ;718.13
                                ; LOE eax edx ecx
.B10.1150:                      ; Preds .B10.1149               ; Infreq
        xor       ebx, ebx                                      ;
        mov       DWORD PTR [4+esp], ecx                        ;
        xor       esi, esi                                      ;
        mov       edi, DWORD PTR [28+esp]                       ;
        xor       ecx, ecx                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.1151:                      ; Preds .B10.1151 .B10.1150     ; Infreq
        inc       esi                                           ;718.13
        mov       DWORD PTR [12+ebx+edi], ecx                   ;718.13
        mov       DWORD PTR [164+ebx+edi], ecx                  ;718.13
        add       ebx, 304                                      ;718.13
        cmp       esi, edx                                      ;718.13
        jb        .B10.1151     ; Prob 64%                      ;718.13
                                ; LOE eax edx ecx ebx esi edi
.B10.1152:                      ; Preds .B10.1151               ; Infreq
        mov       ecx, DWORD PTR [4+esp]                        ;
        lea       esi, DWORD PTR [1+esi+esi]                    ;718.13
                                ; LOE eax edx ecx esi
.B10.1153:                      ; Preds .B10.1152 .B10.1204     ; Infreq
        lea       ebx, DWORD PTR [-1+esi]                       ;718.13
        cmp       eax, ebx                                      ;718.13
        jbe       .B10.1155     ; Prob 10%                      ;718.13
                                ; LOE eax edx ecx esi
.B10.1154:                      ; Preds .B10.1153               ; Infreq
        mov       ebx, ecx                                      ;718.13
        add       esi, ecx                                      ;718.13
        neg       ebx                                           ;718.13
        add       ebx, esi                                      ;718.13
        imul      edi, ebx, 152                                 ;718.13
        mov       esi, DWORD PTR [28+esp]                       ;718.13
        mov       DWORD PTR [-140+esi+edi], 0                   ;718.13
                                ; LOE eax edx ecx
.B10.1155:                      ; Preds .B10.1153 .B10.1154     ; Infreq
        test      edx, edx                                      ;719.13
        jbe       .B10.1203     ; Prob 10%                      ;719.13
                                ; LOE eax edx ecx
.B10.1156:                      ; Preds .B10.1155               ; Infreq
        xor       ebx, ebx                                      ;
        mov       DWORD PTR [4+esp], ecx                        ;
        xor       esi, esi                                      ;
        mov       edi, DWORD PTR [28+esp]                       ;
        xor       ecx, ecx                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.1157:                      ; Preds .B10.1157 .B10.1156     ; Infreq
        inc       esi                                           ;719.13
        mov       DWORD PTR [16+ebx+edi], ecx                   ;719.13
        mov       DWORD PTR [168+ebx+edi], ecx                  ;719.13
        add       ebx, 304                                      ;719.13
        cmp       esi, edx                                      ;719.13
        jb        .B10.1157     ; Prob 64%                      ;719.13
                                ; LOE eax edx ecx ebx esi edi
.B10.1158:                      ; Preds .B10.1157               ; Infreq
        mov       ecx, DWORD PTR [4+esp]                        ;
        lea       esi, DWORD PTR [1+esi+esi]                    ;719.13
                                ; LOE eax edx ecx esi
.B10.1159:                      ; Preds .B10.1158 .B10.1203     ; Infreq
        lea       ebx, DWORD PTR [-1+esi]                       ;719.13
        cmp       eax, ebx                                      ;719.13
        jbe       .B10.1161     ; Prob 10%                      ;719.13
                                ; LOE eax edx ecx esi
.B10.1160:                      ; Preds .B10.1159               ; Infreq
        mov       ebx, ecx                                      ;719.13
        add       esi, ecx                                      ;719.13
        neg       ebx                                           ;719.13
        add       ebx, esi                                      ;719.13
        imul      edi, ebx, 152                                 ;719.13
        mov       esi, DWORD PTR [28+esp]                       ;719.13
        mov       DWORD PTR [-136+esi+edi], 0                   ;719.13
                                ; LOE eax edx ecx
.B10.1161:                      ; Preds .B10.1159 .B10.1160     ; Infreq
        test      edx, edx                                      ;720.13
        jbe       .B10.1202     ; Prob 10%                      ;720.13
                                ; LOE eax edx ecx
.B10.1162:                      ; Preds .B10.1161               ; Infreq
        xor       ebx, ebx                                      ;
        mov       DWORD PTR [4+esp], ecx                        ;
        xor       esi, esi                                      ;
        mov       edi, DWORD PTR [28+esp]                       ;
        xor       ecx, ecx                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.1163:                      ; Preds .B10.1163 .B10.1162     ; Infreq
        inc       esi                                           ;720.13
        mov       DWORD PTR [20+ebx+edi], ecx                   ;720.13
        mov       DWORD PTR [172+ebx+edi], ecx                  ;720.13
        add       ebx, 304                                      ;720.13
        cmp       esi, edx                                      ;720.13
        jb        .B10.1163     ; Prob 64%                      ;720.13
                                ; LOE eax edx ecx ebx esi edi
.B10.1164:                      ; Preds .B10.1163               ; Infreq
        mov       ecx, DWORD PTR [4+esp]                        ;
        lea       esi, DWORD PTR [1+esi+esi]                    ;720.13
                                ; LOE eax edx ecx esi
.B10.1165:                      ; Preds .B10.1164 .B10.1202     ; Infreq
        lea       ebx, DWORD PTR [-1+esi]                       ;720.13
        cmp       eax, ebx                                      ;720.13
        jbe       .B10.1167     ; Prob 10%                      ;720.13
                                ; LOE eax edx ecx esi
.B10.1166:                      ; Preds .B10.1165               ; Infreq
        mov       ebx, ecx                                      ;720.13
        add       esi, ecx                                      ;720.13
        neg       ebx                                           ;720.13
        add       ebx, esi                                      ;720.13
        imul      edi, ebx, 152                                 ;720.13
        mov       esi, DWORD PTR [28+esp]                       ;720.13
        mov       DWORD PTR [-132+esi+edi], 0                   ;720.13
                                ; LOE eax edx ecx
.B10.1167:                      ; Preds .B10.1165 .B10.1166     ; Infreq
        test      edx, edx                                      ;721.13
        jbe       .B10.1201     ; Prob 10%                      ;721.13
                                ; LOE eax edx ecx
.B10.1168:                      ; Preds .B10.1167               ; Infreq
        xor       ebx, ebx                                      ;
        mov       DWORD PTR [4+esp], ecx                        ;
        xor       esi, esi                                      ;
        mov       edi, DWORD PTR [28+esp]                       ;
        xor       ecx, ecx                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.1169:                      ; Preds .B10.1169 .B10.1168     ; Infreq
        inc       esi                                           ;721.13
        mov       DWORD PTR [24+ebx+edi], ecx                   ;721.13
        mov       DWORD PTR [176+ebx+edi], ecx                  ;721.13
        add       ebx, 304                                      ;721.13
        cmp       esi, edx                                      ;721.13
        jb        .B10.1169     ; Prob 64%                      ;721.13
                                ; LOE eax edx ecx ebx esi edi
.B10.1170:                      ; Preds .B10.1169               ; Infreq
        mov       ecx, DWORD PTR [4+esp]                        ;
        lea       esi, DWORD PTR [1+esi+esi]                    ;721.13
                                ; LOE eax edx ecx esi
.B10.1171:                      ; Preds .B10.1170 .B10.1201     ; Infreq
        lea       ebx, DWORD PTR [-1+esi]                       ;721.13
        cmp       eax, ebx                                      ;721.13
        jbe       .B10.1173     ; Prob 10%                      ;721.13
                                ; LOE eax edx ecx esi
.B10.1172:                      ; Preds .B10.1171               ; Infreq
        mov       ebx, ecx                                      ;721.13
        add       esi, ecx                                      ;721.13
        neg       ebx                                           ;721.13
        add       ebx, esi                                      ;721.13
        imul      edi, ebx, 152                                 ;721.13
        mov       esi, DWORD PTR [28+esp]                       ;721.13
        mov       DWORD PTR [-128+esi+edi], 0                   ;721.13
                                ; LOE eax edx ecx
.B10.1173:                      ; Preds .B10.1171 .B10.1172     ; Infreq
        test      edx, edx                                      ;722.13
        jbe       .B10.1200     ; Prob 10%                      ;722.13
                                ; LOE eax edx ecx
.B10.1174:                      ; Preds .B10.1173               ; Infreq
        xor       ebx, ebx                                      ;
        mov       DWORD PTR [4+esp], ecx                        ;
        xor       esi, esi                                      ;
        mov       edi, DWORD PTR [28+esp]                       ;
        xor       ecx, ecx                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.1175:                      ; Preds .B10.1175 .B10.1174     ; Infreq
        inc       esi                                           ;722.13
        mov       DWORD PTR [28+ebx+edi], ecx                   ;722.13
        mov       DWORD PTR [180+ebx+edi], ecx                  ;722.13
        add       ebx, 304                                      ;722.13
        cmp       esi, edx                                      ;722.13
        jb        .B10.1175     ; Prob 64%                      ;722.13
                                ; LOE eax edx ecx ebx esi edi
.B10.1176:                      ; Preds .B10.1175               ; Infreq
        mov       ecx, DWORD PTR [4+esp]                        ;
        lea       esi, DWORD PTR [1+esi+esi]                    ;722.13
                                ; LOE eax edx ecx esi
.B10.1177:                      ; Preds .B10.1176 .B10.1200     ; Infreq
        lea       ebx, DWORD PTR [-1+esi]                       ;722.13
        cmp       eax, ebx                                      ;722.13
        jbe       .B10.1179     ; Prob 10%                      ;722.13
                                ; LOE eax edx ecx esi
.B10.1178:                      ; Preds .B10.1177               ; Infreq
        mov       ebx, ecx                                      ;722.13
        add       esi, ecx                                      ;722.13
        neg       ebx                                           ;722.13
        add       ebx, esi                                      ;722.13
        imul      edi, ebx, 152                                 ;722.13
        mov       esi, DWORD PTR [28+esp]                       ;722.13
        mov       DWORD PTR [-124+esi+edi], 0                   ;722.13
                                ; LOE eax edx ecx
.B10.1179:                      ; Preds .B10.1177 .B10.1178     ; Infreq
        test      edx, edx                                      ;723.13
        jbe       .B10.1199     ; Prob 10%                      ;723.13
                                ; LOE eax edx ecx
.B10.1180:                      ; Preds .B10.1179               ; Infreq
        xor       ebx, ebx                                      ;
        mov       DWORD PTR [4+esp], ecx                        ;
        xor       esi, esi                                      ;
        mov       edi, DWORD PTR [28+esp]                       ;
        xor       ecx, ecx                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.1181:                      ; Preds .B10.1181 .B10.1180     ; Infreq
        inc       esi                                           ;723.13
        mov       BYTE PTR [32+ebx+edi], cl                     ;723.13
        mov       BYTE PTR [184+ebx+edi], cl                    ;723.13
        add       ebx, 304                                      ;723.13
        cmp       esi, edx                                      ;723.13
        jb        .B10.1181     ; Prob 64%                      ;723.13
                                ; LOE eax edx ecx ebx esi edi
.B10.1182:                      ; Preds .B10.1181               ; Infreq
        mov       ecx, DWORD PTR [4+esp]                        ;
        lea       esi, DWORD PTR [1+esi+esi]                    ;723.13
                                ; LOE eax edx ecx esi
.B10.1183:                      ; Preds .B10.1182 .B10.1199     ; Infreq
        lea       ebx, DWORD PTR [-1+esi]                       ;723.13
        cmp       eax, ebx                                      ;723.13
        jbe       .B10.1185     ; Prob 10%                      ;723.13
                                ; LOE eax edx ecx esi
.B10.1184:                      ; Preds .B10.1183               ; Infreq
        mov       ebx, ecx                                      ;723.13
        add       esi, ecx                                      ;723.13
        neg       ebx                                           ;723.13
        add       ebx, esi                                      ;723.13
        imul      edi, ebx, 152                                 ;723.13
        mov       esi, DWORD PTR [28+esp]                       ;723.13
        mov       BYTE PTR [-120+esi+edi], 0                    ;723.13
                                ; LOE eax edx ecx
.B10.1185:                      ; Preds .B10.1183 .B10.1184     ; Infreq
        test      edx, edx                                      ;724.13
        jbe       .B10.1198     ; Prob 10%                      ;724.13
                                ; LOE eax edx ecx
.B10.1186:                      ; Preds .B10.1185               ; Infreq
        xor       ebx, ebx                                      ;
        mov       DWORD PTR [4+esp], ecx                        ;
        xor       esi, esi                                      ;
        mov       edi, DWORD PTR [28+esp]                       ;
        xor       ecx, ecx                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.1187:                      ; Preds .B10.1187 .B10.1186     ; Infreq
        inc       esi                                           ;724.13
        mov       BYTE PTR [108+ebx+edi], cl                    ;724.13
        mov       BYTE PTR [260+ebx+edi], cl                    ;724.13
        add       ebx, 304                                      ;724.13
        cmp       esi, edx                                      ;724.13
        jb        .B10.1187     ; Prob 64%                      ;724.13
                                ; LOE eax edx ecx ebx esi edi
.B10.1188:                      ; Preds .B10.1187               ; Infreq
        mov       ecx, DWORD PTR [4+esp]                        ;
        lea       esi, DWORD PTR [1+esi+esi]                    ;724.13
                                ; LOE eax edx ecx esi
.B10.1189:                      ; Preds .B10.1188 .B10.1198     ; Infreq
        lea       ebx, DWORD PTR [-1+esi]                       ;724.13
        cmp       eax, ebx                                      ;724.13
        jbe       .B10.1191     ; Prob 10%                      ;724.13
                                ; LOE eax edx ecx esi
.B10.1190:                      ; Preds .B10.1189               ; Infreq
        mov       ebx, ecx                                      ;724.13
        add       esi, ecx                                      ;724.13
        neg       ebx                                           ;724.13
        add       ebx, esi                                      ;724.13
        imul      edi, ebx, 152                                 ;724.13
        mov       esi, DWORD PTR [28+esp]                       ;724.13
        mov       BYTE PTR [-44+esi+edi], 0                     ;724.13
                                ; LOE eax edx ecx
.B10.1191:                      ; Preds .B10.1189 .B10.1190     ; Infreq
        test      edx, edx                                      ;725.13
        jbe       .B10.1197     ; Prob 0%                       ;725.13
                                ; LOE eax edx ecx
.B10.1192:                      ; Preds .B10.1191               ; Infreq
        xor       ebx, ebx                                      ;
        mov       DWORD PTR [4+esp], ecx                        ;
        xor       esi, esi                                      ;
        mov       edi, DWORD PTR [28+esp]                       ;
        xor       ecx, ecx                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.1193:                      ; Preds .B10.1193 .B10.1192     ; Infreq
        inc       esi                                           ;725.13
        mov       WORD PTR [148+ebx+edi], cx                    ;725.13
        mov       WORD PTR [300+ebx+edi], cx                    ;725.13
        add       ebx, 304                                      ;725.13
        cmp       esi, edx                                      ;725.13
        jb        .B10.1193     ; Prob 64%                      ;725.13
                                ; LOE eax edx ecx ebx esi edi
.B10.1194:                      ; Preds .B10.1193               ; Infreq
        mov       ecx, DWORD PTR [4+esp]                        ;
        lea       ebx, DWORD PTR [1+esi+esi]                    ;725.13
                                ; LOE eax ecx ebx
.B10.1195:                      ; Preds .B10.1194 .B10.1197     ; Infreq
        lea       edx, DWORD PTR [-1+ebx]                       ;725.13
        cmp       eax, edx                                      ;725.13
        jbe       .B10.1119     ; Prob 0%                       ;725.13
                                ; LOE ecx ebx
.B10.1196:                      ; Preds .B10.1195               ; Infreq
        mov       esi, ecx                                      ;725.13
        add       ebx, ecx                                      ;725.13
        neg       esi                                           ;725.13
        xor       eax, eax                                      ;725.13
        add       esi, ebx                                      ;725.13
        imul      ecx, esi, 152                                 ;725.13
        mov       edx, DWORD PTR [28+esp]                       ;725.13
        mov       WORD PTR [-4+edx+ecx], ax                     ;725.13
        add       esp, 548                                      ;725.13
        pop       ebx                                           ;725.13
        pop       edi                                           ;725.13
        pop       esi                                           ;725.13
        mov       esp, ebp                                      ;725.13
        pop       ebp                                           ;725.13
        ret                                                     ;725.13
                                ; LOE
.B10.1197:                      ; Preds .B10.1191               ; Infreq
        mov       ebx, 1                                        ;
        jmp       .B10.1195     ; Prob 100%                     ;
                                ; LOE eax ecx ebx
.B10.1198:                      ; Preds .B10.1185               ; Infreq
        mov       esi, 1                                        ;
        jmp       .B10.1189     ; Prob 100%                     ;
                                ; LOE eax edx ecx esi
.B10.1199:                      ; Preds .B10.1179               ; Infreq
        mov       esi, 1                                        ;
        jmp       .B10.1183     ; Prob 100%                     ;
                                ; LOE eax edx ecx esi
.B10.1200:                      ; Preds .B10.1173               ; Infreq
        mov       esi, 1                                        ;
        jmp       .B10.1177     ; Prob 100%                     ;
                                ; LOE eax edx ecx esi
.B10.1201:                      ; Preds .B10.1167               ; Infreq
        mov       esi, 1                                        ;
        jmp       .B10.1171     ; Prob 100%                     ;
                                ; LOE eax edx ecx esi
.B10.1202:                      ; Preds .B10.1161               ; Infreq
        mov       esi, 1                                        ;
        jmp       .B10.1165     ; Prob 100%                     ;
                                ; LOE eax edx ecx esi
.B10.1203:                      ; Preds .B10.1155               ; Infreq
        mov       esi, 1                                        ;
        jmp       .B10.1159     ; Prob 100%                     ;
                                ; LOE eax edx ecx esi
.B10.1204:                      ; Preds .B10.1149               ; Infreq
        mov       esi, 1                                        ;
        jmp       .B10.1153     ; Prob 100%                     ;
                                ; LOE eax edx ecx esi
.B10.1205:                      ; Preds .B10.1143               ; Infreq
        mov       esi, 1                                        ;
        jmp       .B10.1147     ; Prob 100%                     ;
                                ; LOE eax edx ecx esi
.B10.1206:                      ; Preds .B10.1137               ; Infreq
        mov       esi, 1                                        ;
        jmp       .B10.1141     ; Prob 100%                     ;
                                ; LOE eax edx ecx esi
.B10.1207:                      ; Preds .B10.1131               ; Infreq
        mov       esi, 1                                        ;
        jmp       .B10.1135     ; Prob 100%                     ;
                                ; LOE eax edx ecx esi
.B10.1209:                      ; Preds .B10.770                ; Infreq
        add       ebx, ebx                                      ;
        xor       ecx, ecx                                      ;667.10
        sub       edi, ebx                                      ;
        mov       DWORD PTR [esp], edi                          ;
        jmp       .B10.782      ; Prob 100%                     ;
                                ; LOE ecx ebx esi
.B10.1210:                      ; Preds .B10.769                ; Infreq
        add       ebx, ebx                                      ;
        sub       edi, ebx                                      ;
        mov       DWORD PTR [esp], edi                          ;
        jmp       .B10.786      ; Prob 100%                     ;
                                ; LOE
.B10.1211:                      ; Preds .B10.548                ; Infreq
        mov       DWORD PTR [160+esp], 0                        ;613.15
        lea       eax, DWORD PTR [8+esp]                        ;613.15
        mov       DWORD PTR [8+esp], 38                         ;613.15
        mov       DWORD PTR [12+esp], OFFSET FLAT: __STRLITPACK_28 ;613.15
        push      32                                            ;613.15
        push      eax                                           ;613.15
        push      OFFSET FLAT: __STRLITPACK_194.0.10            ;613.15
        push      -2088435968                                   ;613.15
        push      911                                           ;613.15
        lea       edx, DWORD PTR [180+esp]                      ;613.15
        push      edx                                           ;613.15
        call      _for_write_seq_lis                            ;613.15
                                ; LOE
.B10.1212:                      ; Preds .B10.1211               ; Infreq
        push      32                                            ;614.12
        xor       eax, eax                                      ;614.12
        push      eax                                           ;614.12
        push      eax                                           ;614.12
        push      -2088435968                                   ;614.12
        push      eax                                           ;614.12
        push      OFFSET FLAT: __STRLITPACK_195                 ;614.12
        call      _for_stop_core                                ;614.12
                                ; LOE
.B10.1412:                      ; Preds .B10.1212               ; Infreq
        add       esp, 48                                       ;614.12
        jmp       .B10.549      ; Prob 100%                     ;614.12
                                ; LOE
.B10.1214:                      ; Preds .B10.816                ; Infreq
        cmp       DWORD PTR [28+esp], 4                         ;576.13
        jl        .B10.1231     ; Prob 10%                      ;576.13
                                ; LOE ebx esi edi
.B10.1215:                      ; Preds .B10.1214               ; Infreq
        mov       eax, DWORD PTR [24+esp]                       ;576.13
        lea       edx, DWORD PTR [eax+ebx]                      ;576.13
        and       edx, 15                                       ;576.13
        je        .B10.1218     ; Prob 50%                      ;576.13
                                ; LOE edx ebx esi edi
.B10.1216:                      ; Preds .B10.1215               ; Infreq
        test      dl, 3                                         ;576.13
        jne       .B10.1231     ; Prob 10%                      ;576.13
                                ; LOE edx ebx esi edi
.B10.1217:                      ; Preds .B10.1216               ; Infreq
        neg       edx                                           ;576.13
        add       edx, 16                                       ;576.13
        shr       edx, 2                                        ;576.13
                                ; LOE edx ebx esi edi
.B10.1218:                      ; Preds .B10.1217 .B10.1215     ; Infreq
        lea       eax, DWORD PTR [4+edx]                        ;576.13
        cmp       eax, DWORD PTR [28+esp]                       ;576.13
        jg        .B10.1231     ; Prob 10%                      ;576.13
                                ; LOE edx ebx esi edi
.B10.1219:                      ; Preds .B10.1218               ; Infreq
        mov       eax, DWORD PTR [28+esp]                       ;576.13
        mov       ecx, eax                                      ;576.13
        sub       ecx, edx                                      ;576.13
        and       ecx, 3                                        ;576.13
        neg       ecx                                           ;576.13
        add       ecx, eax                                      ;576.13
        mov       eax, DWORD PTR [24+esp]                       ;
        test      edx, edx                                      ;576.13
        mov       DWORD PTR [esp], ecx                          ;576.13
        lea       ecx, DWORD PTR [eax+ebx]                      ;
        mov       DWORD PTR [4+esp], ecx                        ;
        jbe       .B10.1223     ; Prob 10%                      ;576.13
                                ; LOE edx ecx ebx esi edi cl ch
.B10.1220:                      ; Preds .B10.1219               ; Infreq
        xor       eax, eax                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.1221:                      ; Preds .B10.1221 .B10.1220     ; Infreq
        mov       DWORD PTR [ecx+eax*4], 0                      ;576.13
        inc       eax                                           ;576.13
        cmp       eax, edx                                      ;576.13
        jb        .B10.1221     ; Prob 82%                      ;576.13
                                ; LOE eax edx ecx ebx esi edi
.B10.1223:                      ; Preds .B10.1221 .B10.1219     ; Infreq
        mov       eax, DWORD PTR [esp]                          ;
        mov       ecx, DWORD PTR [4+esp]                        ;
        pxor      xmm0, xmm0                                    ;
                                ; LOE eax edx ecx ebx esi edi xmm0
.B10.1224:                      ; Preds .B10.1224 .B10.1223     ; Infreq
        movdqa    XMMWORD PTR [ecx+edx*4], xmm0                 ;576.13
        add       edx, 4                                        ;576.13
        cmp       edx, eax                                      ;576.13
        jb        .B10.1224     ; Prob 82%                      ;576.13
                                ; LOE eax edx ecx ebx esi edi xmm0
.B10.1225:                      ; Preds .B10.1224               ; Infreq
        mov       DWORD PTR [esp], eax                          ;
                                ; LOE eax ebx esi edi al ah
.B10.1226:                      ; Preds .B10.1225 .B10.1231     ; Infreq
        cmp       eax, DWORD PTR [28+esp]                       ;576.13
        jae       .B10.818      ; Prob 10%                      ;576.13
                                ; LOE eax ebx esi edi al ah
.B10.1227:                      ; Preds .B10.1226               ; Infreq
        mov       edx, DWORD PTR [28+esp]                       ;
                                ; LOE eax edx ebx esi edi
.B10.1228:                      ; Preds .B10.1228 .B10.1227     ; Infreq
        mov       DWORD PTR [esi+eax*4], 0                      ;576.13
        inc       eax                                           ;576.13
        cmp       eax, edx                                      ;576.13
        jb        .B10.1228     ; Prob 82%                      ;576.13
                                ; LOE eax edx ebx esi edi
.B10.1229:                      ; Preds .B10.1228               ; Infreq
        mov       edx, DWORD PTR [16+esp]                       ;576.13
        inc       edx                                           ;576.13
        mov       eax, DWORD PTR [20+esp]                       ;576.13
        add       esi, eax                                      ;576.13
        add       edi, eax                                      ;576.13
        add       ebx, eax                                      ;576.13
        mov       DWORD PTR [16+esp], edx                       ;576.13
        cmp       edx, DWORD PTR [12+esp]                       ;576.13
        jb        .B10.816      ; Prob 82%                      ;576.13
        jmp       .B10.423      ; Prob 100%                     ;576.13
                                ; LOE ebx esi edi
.B10.1231:                      ; Preds .B10.1214 .B10.1218 .B10.1216 ; Infreq
        xor       eax, eax                                      ;576.13
        mov       DWORD PTR [esp], eax                          ;576.13
        jmp       .B10.1226     ; Prob 100%                     ;576.13
                                ; LOE eax ebx esi edi al ah
.B10.1237:                      ; Preds .B10.820                ; Infreq
        cmp       DWORD PTR [28+esp], 4                         ;562.13
        jl        .B10.1254     ; Prob 10%                      ;562.13
                                ; LOE ebx esi edi
.B10.1238:                      ; Preds .B10.1237               ; Infreq
        mov       eax, DWORD PTR [24+esp]                       ;562.13
        lea       edx, DWORD PTR [eax+ebx]                      ;562.13
        and       edx, 15                                       ;562.13
        je        .B10.1241     ; Prob 50%                      ;562.13
                                ; LOE edx ebx esi edi
.B10.1239:                      ; Preds .B10.1238               ; Infreq
        test      dl, 3                                         ;562.13
        jne       .B10.1254     ; Prob 10%                      ;562.13
                                ; LOE edx ebx esi edi
.B10.1240:                      ; Preds .B10.1239               ; Infreq
        neg       edx                                           ;562.13
        add       edx, 16                                       ;562.13
        shr       edx, 2                                        ;562.13
                                ; LOE edx ebx esi edi
.B10.1241:                      ; Preds .B10.1240 .B10.1238     ; Infreq
        lea       eax, DWORD PTR [4+edx]                        ;562.13
        cmp       eax, DWORD PTR [28+esp]                       ;562.13
        jg        .B10.1254     ; Prob 10%                      ;562.13
                                ; LOE edx ebx esi edi
.B10.1242:                      ; Preds .B10.1241               ; Infreq
        mov       eax, DWORD PTR [28+esp]                       ;562.13
        mov       ecx, eax                                      ;562.13
        sub       ecx, edx                                      ;562.13
        and       ecx, 3                                        ;562.13
        neg       ecx                                           ;562.13
        add       ecx, eax                                      ;562.13
        mov       eax, DWORD PTR [24+esp]                       ;
        test      edx, edx                                      ;562.13
        mov       DWORD PTR [esp], ecx                          ;562.13
        lea       ecx, DWORD PTR [eax+ebx]                      ;
        mov       DWORD PTR [4+esp], ecx                        ;
        jbe       .B10.1246     ; Prob 10%                      ;562.13
                                ; LOE edx ecx ebx esi edi cl ch
.B10.1243:                      ; Preds .B10.1242               ; Infreq
        xor       eax, eax                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.1244:                      ; Preds .B10.1244 .B10.1243     ; Infreq
        mov       DWORD PTR [ecx+eax*4], 0                      ;562.13
        inc       eax                                           ;562.13
        cmp       eax, edx                                      ;562.13
        jb        .B10.1244     ; Prob 82%                      ;562.13
                                ; LOE eax edx ecx ebx esi edi
.B10.1246:                      ; Preds .B10.1244 .B10.1242     ; Infreq
        mov       eax, DWORD PTR [esp]                          ;
        mov       ecx, DWORD PTR [4+esp]                        ;
        pxor      xmm0, xmm0                                    ;
                                ; LOE eax edx ecx ebx esi edi xmm0
.B10.1247:                      ; Preds .B10.1247 .B10.1246     ; Infreq
        movdqa    XMMWORD PTR [ecx+edx*4], xmm0                 ;562.13
        add       edx, 4                                        ;562.13
        cmp       edx, eax                                      ;562.13
        jb        .B10.1247     ; Prob 82%                      ;562.13
                                ; LOE eax edx ecx ebx esi edi xmm0
.B10.1248:                      ; Preds .B10.1247               ; Infreq
        mov       DWORD PTR [esp], eax                          ;
                                ; LOE eax ebx esi edi al ah
.B10.1249:                      ; Preds .B10.1248 .B10.1254     ; Infreq
        cmp       eax, DWORD PTR [28+esp]                       ;562.13
        jae       .B10.822      ; Prob 10%                      ;562.13
                                ; LOE eax ebx esi edi al ah
.B10.1250:                      ; Preds .B10.1249               ; Infreq
        mov       edx, DWORD PTR [28+esp]                       ;
                                ; LOE eax edx ebx esi edi
.B10.1251:                      ; Preds .B10.1251 .B10.1250     ; Infreq
        mov       DWORD PTR [esi+eax*4], 0                      ;562.13
        inc       eax                                           ;562.13
        cmp       eax, edx                                      ;562.13
        jb        .B10.1251     ; Prob 82%                      ;562.13
                                ; LOE eax edx ebx esi edi
.B10.1252:                      ; Preds .B10.1251               ; Infreq
        mov       edx, DWORD PTR [16+esp]                       ;562.13
        inc       edx                                           ;562.13
        mov       eax, DWORD PTR [20+esp]                       ;562.13
        add       esi, eax                                      ;562.13
        add       edi, eax                                      ;562.13
        add       ebx, eax                                      ;562.13
        mov       DWORD PTR [16+esp], edx                       ;562.13
        cmp       edx, DWORD PTR [12+esp]                       ;562.13
        jb        .B10.820      ; Prob 82%                      ;562.13
        jmp       .B10.404      ; Prob 100%                     ;562.13
                                ; LOE ebx esi edi
.B10.1254:                      ; Preds .B10.1237 .B10.1241 .B10.1239 ; Infreq
        xor       eax, eax                                      ;562.13
        mov       DWORD PTR [esp], eax                          ;562.13
        jmp       .B10.1249     ; Prob 100%                     ;562.13
                                ; LOE eax ebx esi edi al ah
.B10.1259:                      ; Preds .B10.824                ; Infreq
        cmp       DWORD PTR [28+esp], 4                         ;555.13
        jl        .B10.1276     ; Prob 10%                      ;555.13
                                ; LOE ebx esi edi
.B10.1260:                      ; Preds .B10.1259               ; Infreq
        mov       eax, DWORD PTR [24+esp]                       ;555.13
        lea       edx, DWORD PTR [eax+ebx]                      ;555.13
        and       edx, 15                                       ;555.13
        je        .B10.1263     ; Prob 50%                      ;555.13
                                ; LOE edx ebx esi edi
.B10.1261:                      ; Preds .B10.1260               ; Infreq
        test      dl, 3                                         ;555.13
        jne       .B10.1276     ; Prob 10%                      ;555.13
                                ; LOE edx ebx esi edi
.B10.1262:                      ; Preds .B10.1261               ; Infreq
        neg       edx                                           ;555.13
        add       edx, 16                                       ;555.13
        shr       edx, 2                                        ;555.13
                                ; LOE edx ebx esi edi
.B10.1263:                      ; Preds .B10.1262 .B10.1260     ; Infreq
        lea       eax, DWORD PTR [4+edx]                        ;555.13
        cmp       eax, DWORD PTR [28+esp]                       ;555.13
        jg        .B10.1276     ; Prob 10%                      ;555.13
                                ; LOE edx ebx esi edi
.B10.1264:                      ; Preds .B10.1263               ; Infreq
        mov       eax, DWORD PTR [28+esp]                       ;555.13
        mov       ecx, eax                                      ;555.13
        sub       ecx, edx                                      ;555.13
        and       ecx, 3                                        ;555.13
        neg       ecx                                           ;555.13
        add       ecx, eax                                      ;555.13
        mov       eax, DWORD PTR [24+esp]                       ;
        test      edx, edx                                      ;555.13
        mov       DWORD PTR [esp], ecx                          ;555.13
        lea       ecx, DWORD PTR [eax+ebx]                      ;
        mov       DWORD PTR [4+esp], ecx                        ;
        jbe       .B10.1268     ; Prob 10%                      ;555.13
                                ; LOE edx ecx ebx esi edi cl ch
.B10.1265:                      ; Preds .B10.1264               ; Infreq
        xor       eax, eax                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B10.1266:                      ; Preds .B10.1266 .B10.1265     ; Infreq
        mov       DWORD PTR [ecx+eax*4], 0                      ;555.13
        inc       eax                                           ;555.13
        cmp       eax, edx                                      ;555.13
        jb        .B10.1266     ; Prob 82%                      ;555.13
                                ; LOE eax edx ecx ebx esi edi
.B10.1268:                      ; Preds .B10.1266 .B10.1264     ; Infreq
        mov       eax, DWORD PTR [esp]                          ;
        mov       ecx, DWORD PTR [4+esp]                        ;
        pxor      xmm0, xmm0                                    ;
                                ; LOE eax edx ecx ebx esi edi xmm0
.B10.1269:                      ; Preds .B10.1269 .B10.1268     ; Infreq
        movdqa    XMMWORD PTR [ecx+edx*4], xmm0                 ;555.13
        add       edx, 4                                        ;555.13
        cmp       edx, eax                                      ;555.13
        jb        .B10.1269     ; Prob 82%                      ;555.13
                                ; LOE eax edx ecx ebx esi edi xmm0
.B10.1270:                      ; Preds .B10.1269               ; Infreq
        mov       DWORD PTR [esp], eax                          ;
                                ; LOE eax ebx esi edi al ah
.B10.1271:                      ; Preds .B10.1270 .B10.1276     ; Infreq
        cmp       eax, DWORD PTR [28+esp]                       ;555.13
        jae       .B10.826      ; Prob 10%                      ;555.13
                                ; LOE eax ebx esi edi al ah
.B10.1272:                      ; Preds .B10.1271               ; Infreq
        mov       edx, DWORD PTR [28+esp]                       ;
                                ; LOE eax edx ebx esi edi
.B10.1273:                      ; Preds .B10.1273 .B10.1272     ; Infreq
        mov       DWORD PTR [esi+eax*4], 0                      ;555.13
        inc       eax                                           ;555.13
        cmp       eax, edx                                      ;555.13
        jb        .B10.1273     ; Prob 82%                      ;555.13
                                ; LOE eax edx ebx esi edi
.B10.1274:                      ; Preds .B10.1273               ; Infreq
        mov       edx, DWORD PTR [16+esp]                       ;555.13
        inc       edx                                           ;555.13
        mov       eax, DWORD PTR [20+esp]                       ;555.13
        add       esi, eax                                      ;555.13
        add       edi, eax                                      ;555.13
        add       ebx, eax                                      ;555.13
        mov       DWORD PTR [16+esp], edx                       ;555.13
        cmp       edx, DWORD PTR [12+esp]                       ;555.13
        jb        .B10.824      ; Prob 82%                      ;555.13
        jmp       .B10.395      ; Prob 100%                     ;555.13
                                ; LOE ebx esi edi
.B10.1276:                      ; Preds .B10.1259 .B10.1263 .B10.1261 ; Infreq
        xor       eax, eax                                      ;555.13
        mov       DWORD PTR [esp], eax                          ;555.13
        jmp       .B10.1271     ; Prob 100%                     ;555.13
                                ; LOE eax ebx esi edi al ah
.B10.1281:                      ; Preds .B10.86                 ; Infreq
        mov       esi, 1                                        ;
        jmp       .B10.90       ; Prob 100%                     ;
        ALIGN     16
                                ; LOE edx ecx ebx esi
; mark_end;
_DYNUST_NETWORK_MODULE_mp_ALLOCATE_DYNUST_NETWORK_ARC ENDP
_TEXT	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_145.0.10	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_147.0.10	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_149.0.10	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_151.0.10	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_153.0.10	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_155.0.10	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_157.0.10	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_159.0.10	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_161.0.10	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_163.0.10	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_165.0.10	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_167.0.10	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_169.0.10	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_171.0.10	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_173.0.10	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_175.0.10	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_177.0.10	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_179.0.10	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_181.0.10	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_183.0.10	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_185.0.10	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_187.0.10	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_189.0.10	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_191.0.10	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_193.0.10	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	26
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_194.0.10	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_196.0.10	DB	9
	DB	3
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_197.0.10	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_198.0.10	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_200.0.10	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_202.0.10	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__NLITPACK_0.0.10	DD	10
__STRLITPACK_204.0.10	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_NETWORK_MODULE_mp_ALLOCATE_DYNUST_NETWORK_ARC
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_NETWORK_MODULE_mp_ALLOCATE_DYNUST_NETWORK_MAXLINKVEH
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_NETWORK_MODULE_mp_ALLOCATE_DYNUST_NETWORK_MAXLINKVEH
_DYNUST_NETWORK_MODULE_mp_ALLOCATE_DYNUST_NETWORK_MAXLINKVEH	PROC NEAR 
.B11.1:                         ; Preds .B11.0
        sub       esp, 20                                       ;731.20
        movss     xmm0, DWORD PTR [_2il0floatpacket.29]         ;735.26
        mulss     xmm0, DWORD PTR [_DYNUST_MAIN_MODULE_mp_LONGEST_LINK] ;735.26
        cvttss2si eax, xmm0                                     ;735.10
        push      OFFSET FLAT: _DYNUST_MAIN_MODULE_mp_NOOFARCS  ;737.15
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_MAXLINKVEH], eax ;735.10
        call      _DYNUST_TRANSLINK_MODULE_mp_TRANLINK_2DSETUP  ;737.15
                                ; LOE ebx ebp esi edi
.B11.11:                        ; Preds .B11.1
        add       esp, 4                                        ;737.15
                                ; LOE ebx ebp esi edi
.B11.2:                         ; Preds .B11.11
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFARCS] ;738.11
        test      ecx, ecx                                      ;738.11
        jle       .B11.8        ; Prob 2%                       ;738.11
                                ; LOE ecx ebx ebp esi edi
.B11.3:                         ; Preds .B11.2
        mov       DWORD PTR [12+esp], 1                         ;738.11
        lea       edx, DWORD PTR [12+esp]                       ;738.11
        mov       DWORD PTR [8+esp], esi                        ;738.11
        lea       eax, DWORD PTR [16+esp]                       ;738.11
        mov       DWORD PTR [4+esp], edi                        ;738.11
        mov       esi, edx                                      ;738.11
        mov       DWORD PTR [esp], ebx                          ;738.11
        mov       ebx, eax                                      ;738.11
        mov       edi, ecx                                      ;738.11
        jmp       .B11.4        ; Prob 100%                     ;738.11
                                ; LOE ebx ebp esi edi
.B11.6:                         ; Preds .B11.5
        mov       DWORD PTR [12+esp], eax                       ;738.11
                                ; LOE ebx ebp esi edi
.B11.4:                         ; Preds .B11.3 .B11.6
        mov       DWORD PTR [16+esp], 50                        ;739.17
        push      ebx                                           ;740.22
        push      esi                                           ;740.22
        call      _DYNUST_TRANSLINK_MODULE_mp_TRANLINK_SETUP    ;740.22
                                ; LOE ebx ebp esi edi
.B11.12:                        ; Preds .B11.4
        add       esp, 8                                        ;740.22
                                ; LOE ebx ebp esi edi
.B11.5:                         ; Preds .B11.12
        mov       eax, DWORD PTR [12+esp]                       ;741.13
        inc       eax                                           ;741.13
        cmp       eax, edi                                      ;741.13
        jle       .B11.6        ; Prob 82%                      ;741.13
                                ; LOE eax ebx ebp esi edi
.B11.7:                         ; Preds .B11.5                  ; Infreq
        mov       esi, DWORD PTR [8+esp]                        ;
        mov       edi, DWORD PTR [4+esp]                        ;
        mov       ebx, DWORD PTR [esp]                          ;
                                ; LOE ebx ebp esi edi
.B11.8:                         ; Preds .B11.7 .B11.2           ; Infreq
        add       esp, 20                                       ;743.9
        ret                                                     ;743.9
        ALIGN     16
                                ; LOE
; mark_end;
_DYNUST_NETWORK_MODULE_mp_ALLOCATE_DYNUST_NETWORK_MAXLINKVEH ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_NETWORK_MODULE_mp_ALLOCATE_DYNUST_NETWORK_MAXLINKVEH
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_NETWORK_MODULE_mp_ALLOCATE_DYNUST_NETWORK_NODE
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_NETWORK_MODULE_mp_ALLOCATE_DYNUST_NETWORK_NODE
_DYNUST_NETWORK_MODULE_mp_ALLOCATE_DYNUST_NETWORK_NODE	PROC NEAR 
.B12.1:                         ; Preds .B12.0
        push      ebp                                           ;746.20
        mov       ebp, esp                                      ;746.20
        and       esp, -16                                      ;746.20
        push      esi                                           ;746.20
        push      edi                                           ;746.20
        push      ebx                                           ;746.20
        sub       esp, 100                                      ;746.20
        xor       eax, eax                                      ;750.13
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFNODES] ;750.13
        test      edx, edx                                      ;750.13
        lea       ecx, DWORD PTR [68+esp]                       ;750.13
        push      84                                            ;750.13
        cmovle    edx, eax                                      ;750.13
        push      edx                                           ;750.13
        push      2                                             ;750.13
        push      ecx                                           ;750.13
        call      _for_check_mult_overflow                      ;750.13
                                ; LOE eax
.B12.2:                         ; Preds .B12.1
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE+12] ;750.13
        and       eax, 1                                        ;750.13
        and       edx, 1                                        ;750.13
        add       edx, edx                                      ;750.13
        shl       eax, 4                                        ;750.13
        or        edx, 1                                        ;750.13
        or        edx, eax                                      ;750.13
        or        edx, 262144                                   ;750.13
        push      edx                                           ;750.13
        push      OFFSET FLAT: _DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE ;750.13
        push      DWORD PTR [92+esp]                            ;750.13
        call      _for_alloc_allocatable                        ;750.13
                                ; LOE eax
.B12.137:                       ; Preds .B12.2
        add       esp, 28                                       ;750.13
        mov       ebx, eax                                      ;750.13
                                ; LOE ebx
.B12.3:                         ; Preds .B12.137
        test      ebx, ebx                                      ;750.13
        je        .B12.6        ; Prob 50%                      ;750.13
                                ; LOE ebx
.B12.4:                         ; Preds .B12.3
        mov       DWORD PTR [32+esp], 0                         ;752.14
        lea       edx, DWORD PTR [32+esp]                       ;752.14
        mov       DWORD PTR [24+esp], 56                        ;752.14
        lea       eax, DWORD PTR [24+esp]                       ;752.14
        mov       DWORD PTR [28+esp], OFFSET FLAT: __STRLITPACK_18 ;752.14
        push      32                                            ;752.14
        push      eax                                           ;752.14
        push      OFFSET FLAT: __STRLITPACK_206.0.12            ;752.14
        push      -2088435968                                   ;752.14
        push      911                                           ;752.14
        push      edx                                           ;752.14
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], ebx ;750.13
        call      _for_write_seq_lis                            ;752.14
                                ; LOE
.B12.5:                         ; Preds .B12.4
        push      32                                            ;753.14
        xor       eax, eax                                      ;753.14
        push      eax                                           ;753.14
        push      eax                                           ;753.14
        push      -2088435968                                   ;753.14
        push      eax                                           ;753.14
        push      OFFSET FLAT: __STRLITPACK_207                 ;753.14
        call      _for_stop_core                                ;753.14
                                ; LOE
.B12.138:                       ; Preds .B12.5
        add       esp, 48                                       ;753.14
        jmp       .B12.13       ; Prob 100%                     ;753.14
                                ; LOE
.B12.6:                         ; Preds .B12.3
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFNODES] ;750.13
        xor       edx, edx                                      ;750.13
        test      esi, esi                                      ;750.13
        cmovle    esi, edx                                      ;750.13
        mov       ecx, 84                                       ;750.13
        lea       edi, DWORD PTR [64+esp]                       ;750.13
        push      ecx                                           ;750.13
        push      esi                                           ;750.13
        push      2                                             ;750.13
        push      edi                                           ;750.13
        mov       eax, 1                                        ;750.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE+12], 133 ;750.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE+4], ecx ;750.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE+16], eax ;750.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE+8], edx ;750.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE+32], eax ;750.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE+24], esi ;750.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE+28], ecx ;750.13
        call      _for_check_mult_overflow                      ;750.13
                                ; LOE ebx
.B12.139:                       ; Preds .B12.6
        add       esp, 16                                       ;750.13
                                ; LOE ebx
.B12.7:                         ; Preds .B12.139
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE] ;750.13
        mov       edx, eax                                      ;750.13
        mov       ecx, DWORD PTR [64+esp]                       ;750.13
        add       ecx, eax                                      ;750.13
        cmp       eax, ecx                                      ;750.13
        jae       .B12.12       ; Prob 67%                      ;750.13
                                ; LOE eax edx ebx
.B12.9:                         ; Preds .B12.7
        xor       ecx, ecx                                      ;
                                ; LOE eax edx ecx ebx
.B12.10:                        ; Preds .B12.11 .B12.9
        add       eax, ecx                                      ;750.13
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_ALLOCATE_DYNUST_NETWORK_NODE$BLK..T1886_+80] ;750.13
        mov       DWORD PTR [80+eax], esi                       ;750.13
        mov       esi, 80                                       ;750.13
                                ; LOE eax edx ecx ebx esi
.B12.135:                       ; Preds .B12.135 .B12.10
        movsd     xmm0, QWORD PTR [_DYNUST_NETWORK_MODULE_mp_ALLOCATE_DYNUST_NETWORK_NODE$BLK..T1886_-8+esi] ;750.13
        movsd     QWORD PTR [-8+eax+esi], xmm0                  ;750.13
        movsd     xmm1, QWORD PTR [_DYNUST_NETWORK_MODULE_mp_ALLOCATE_DYNUST_NETWORK_NODE$BLK..T1886_-16+esi] ;750.13
        movsd     QWORD PTR [-16+eax+esi], xmm1                 ;750.13
        movsd     xmm2, QWORD PTR [_DYNUST_NETWORK_MODULE_mp_ALLOCATE_DYNUST_NETWORK_NODE$BLK..T1886_-24+esi] ;750.13
        movsd     QWORD PTR [-24+eax+esi], xmm2                 ;750.13
        movsd     xmm3, QWORD PTR [_DYNUST_NETWORK_MODULE_mp_ALLOCATE_DYNUST_NETWORK_NODE$BLK..T1886_-32+esi] ;750.13
        movsd     QWORD PTR [-32+eax+esi], xmm3                 ;750.13
        movsd     xmm4, QWORD PTR [_DYNUST_NETWORK_MODULE_mp_ALLOCATE_DYNUST_NETWORK_NODE$BLK..T1886_-40+esi] ;750.13
        movsd     QWORD PTR [-40+eax+esi], xmm4                 ;750.13
        sub       esi, 40                                       ;750.13
        jne       .B12.135      ; Prob 50%                      ;750.13
                                ; LOE eax edx ecx ebx esi
.B12.11:                        ; Preds .B12.135
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE] ;750.13
        add       edx, 84                                       ;750.13
        mov       esi, DWORD PTR [64+esp]                       ;750.13
        add       ecx, 84                                       ;750.13
        add       esi, eax                                      ;750.13
        cmp       edx, esi                                      ;750.13
        jb        .B12.10       ; Prob 82%                      ;750.13
                                ; LOE eax edx ecx ebx
.B12.12:                        ; Preds .B12.7 .B12.11
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], ebx ;750.13
                                ; LOE
.B12.13:                        ; Preds .B12.138 .B12.12
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE+24] ;756.10
        test      edx, edx                                      ;756.10
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE+32] ;756.10
        jle       .B12.32       ; Prob 50%                      ;756.10
                                ; LOE edx edi
.B12.14:                        ; Preds .B12.13
        mov       eax, edx                                      ;756.10
        shr       eax, 31                                       ;756.10
        add       eax, edx                                      ;756.10
        sar       eax, 1                                        ;756.10
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE] ;756.10
        test      eax, eax                                      ;756.10
        jbe       .B12.58       ; Prob 10%                      ;756.10
                                ; LOE eax edx ecx edi
.B12.15:                        ; Preds .B12.14
        xor       esi, esi                                      ;
        mov       DWORD PTR [16+esp], edi                       ;
        xor       ebx, ebx                                      ;
        xor       edi, edi                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B12.16:                        ; Preds .B12.16 .B12.15
        inc       esi                                           ;756.10
        mov       DWORD PTR [76+ebx+ecx], edi                   ;756.10
        mov       DWORD PTR [160+ebx+ecx], edi                  ;756.10
        add       ebx, 168                                      ;756.10
        cmp       esi, eax                                      ;756.10
        jb        .B12.16       ; Prob 63%                      ;756.10
                                ; LOE eax edx ecx ebx esi edi
.B12.17:                        ; Preds .B12.16
        mov       edi, DWORD PTR [16+esp]                       ;
        lea       ebx, DWORD PTR [1+esi+esi]                    ;756.10
                                ; LOE eax edx ecx ebx edi
.B12.18:                        ; Preds .B12.17 .B12.58
        lea       esi, DWORD PTR [-1+ebx]                       ;756.10
        cmp       edx, esi                                      ;756.10
        jbe       .B12.20       ; Prob 10%                      ;756.10
                                ; LOE eax edx ecx ebx edi
.B12.19:                        ; Preds .B12.18
        mov       esi, edi                                      ;756.10
        add       ebx, edi                                      ;756.10
        neg       esi                                           ;756.10
        add       esi, ebx                                      ;756.10
        imul      ebx, esi, 84                                  ;756.10
        mov       DWORD PTR [-8+ecx+ebx], 0                     ;756.10
                                ; LOE eax edx ecx edi
.B12.20:                        ; Preds .B12.18 .B12.19
        test      eax, eax                                      ;757.13
        jbe       .B12.57       ; Prob 10%                      ;757.13
                                ; LOE eax edx ecx edi
.B12.21:                        ; Preds .B12.20
        xor       esi, esi                                      ;
        mov       DWORD PTR [16+esp], edi                       ;
        xor       ebx, ebx                                      ;
        xor       edi, edi                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B12.22:                        ; Preds .B12.22 .B12.21
        inc       esi                                           ;757.13
        mov       DWORD PTR [80+ebx+ecx], edi                   ;757.13
        mov       DWORD PTR [164+ebx+ecx], edi                  ;757.13
        add       ebx, 168                                      ;757.13
        cmp       esi, eax                                      ;757.13
        jb        .B12.22       ; Prob 63%                      ;757.13
                                ; LOE eax edx ecx ebx esi edi
.B12.23:                        ; Preds .B12.22
        mov       edi, DWORD PTR [16+esp]                       ;
        lea       ebx, DWORD PTR [1+esi+esi]                    ;757.13
                                ; LOE eax edx ecx ebx edi
.B12.24:                        ; Preds .B12.23 .B12.57
        lea       esi, DWORD PTR [-1+ebx]                       ;757.13
        cmp       edx, esi                                      ;757.13
        jbe       .B12.26       ; Prob 10%                      ;757.13
                                ; LOE eax edx ecx ebx edi
.B12.25:                        ; Preds .B12.24
        mov       esi, edi                                      ;757.13
        add       ebx, edi                                      ;757.13
        neg       esi                                           ;757.13
        add       esi, ebx                                      ;757.13
        imul      ebx, esi, 84                                  ;757.13
        mov       DWORD PTR [-4+ecx+ebx], 0                     ;757.13
                                ; LOE eax edx ecx edi
.B12.26:                        ; Preds .B12.24 .B12.25
        test      eax, eax                                      ;758.13
        jbe       .B12.56       ; Prob 10%                      ;758.13
                                ; LOE eax edx ecx edi
.B12.27:                        ; Preds .B12.26
        xor       esi, esi                                      ;
        mov       DWORD PTR [16+esp], edi                       ;
        xor       ebx, ebx                                      ;
        xor       edi, edi                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B12.28:                        ; Preds .B12.28 .B12.27
        inc       esi                                           ;758.13
        mov       WORD PTR [36+ebx+ecx], di                     ;758.13
        mov       WORD PTR [120+ebx+ecx], di                    ;758.13
        add       ebx, 168                                      ;758.13
        cmp       esi, eax                                      ;758.13
        jb        .B12.28       ; Prob 63%                      ;758.13
                                ; LOE eax edx ecx ebx esi edi
.B12.29:                        ; Preds .B12.28
        mov       edi, DWORD PTR [16+esp]                       ;
        lea       ebx, DWORD PTR [1+esi+esi]                    ;758.13
                                ; LOE edx ecx ebx edi
.B12.30:                        ; Preds .B12.29 .B12.56
        lea       eax, DWORD PTR [-1+ebx]                       ;758.13
        cmp       edx, eax                                      ;758.13
        jbe       .B12.32       ; Prob 10%                      ;758.13
                                ; LOE ecx ebx edi
.B12.31:                        ; Preds .B12.30
        mov       eax, edi                                      ;758.13
        add       ebx, edi                                      ;758.13
        neg       eax                                           ;758.13
        xor       edx, edx                                      ;758.13
        add       eax, ebx                                      ;758.13
        imul      ebx, eax, 84                                  ;758.13
        mov       WORD PTR [-48+ecx+ebx], dx                    ;758.13
                                ; LOE edi
.B12.32:                        ; Preds .B12.30 .B12.13 .B12.31
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFNODES] ;763.13
        test      ecx, ecx                                      ;763.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_ICONZONEPAR], 10 ;761.13
        jle       .B12.54       ; Prob 28%                      ;763.13
                                ; LOE ecx edi
.B12.33:                        ; Preds .B12.32
        mov       edx, 1                                        ;
        mov       ebx, 84                                       ;
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE] ;764.17
        mov       DWORD PTR [12+esp], edx                       ;
        pxor      xmm0, xmm0                                    ;786.14
        mov       DWORD PTR [8+esp], ecx                        ;
        mov       DWORD PTR [16+esp], edi                       ;
                                ; LOE ebx esi
.B12.34:                        ; Preds .B12.52 .B12.33
        imul      eax, DWORD PTR [16+esp], -84                  ;764.17
        imul      edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE+32], -84 ;764.17
        add       esi, eax                                      ;764.17
        add       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE] ;764.17
        add       edx, ebx                                      ;764.17
        mov       esi, DWORD PTR [12+esi+ebx]                   ;764.17
        and       esi, 1                                        ;764.17
        add       esi, esi                                      ;764.17
        or        esi, 262145                                   ;764.17
        push      esi                                           ;764.17
        push      edx                                           ;764.17
        push      16                                            ;764.17
        call      _for_alloc_allocatable                        ;764.17
                                ; LOE eax ebx
.B12.140:                       ; Preds .B12.34
        add       esp, 12                                       ;764.17
                                ; LOE eax ebx
.B12.35:                        ; Preds .B12.140
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE+32] ;764.17
        test      eax, eax                                      ;764.17
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE] ;764.17
        jne       .B12.37       ; Prob 50%                      ;764.17
                                ; LOE eax ebx esi edi
.B12.36:                        ; Preds .B12.35
        imul      edx, edi, -84                                 ;764.17
        mov       ecx, 1                                        ;764.17
        add       edx, ebx                                      ;764.17
        add       esi, edx                                      ;764.17
        mov       edx, 4                                        ;764.17
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;764.17
        mov       DWORD PTR [12+esi], 133                       ;764.17
        mov       DWORD PTR [4+esi], edx                        ;764.17
        mov       DWORD PTR [16+esi], ecx                       ;764.17
        mov       DWORD PTR [8+esi], 0                          ;764.17
        mov       DWORD PTR [32+esi], ecx                       ;764.17
        mov       DWORD PTR [24+esi], edx                       ;764.17
        mov       DWORD PTR [28+esi], edx                       ;764.17
        jmp       .B12.40       ; Prob 100%                     ;764.17
                                ; LOE edx ebx esi
.B12.37:                        ; Preds .B12.35
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;764.17
        lea       edx, DWORD PTR [32+esp]                       ;766.18
        mov       DWORD PTR [32+esp], 0                         ;766.18
        lea       eax, DWORD PTR [72+esp]                       ;766.18
        mov       DWORD PTR [72+esp], 41                        ;766.18
        mov       DWORD PTR [76+esp], OFFSET FLAT: __STRLITPACK_16 ;766.18
        push      32                                            ;766.18
        push      eax                                           ;766.18
        push      OFFSET FLAT: __STRLITPACK_208.0.12            ;766.18
        push      -2088435968                                   ;766.18
        push      911                                           ;766.18
        push      edx                                           ;766.18
        call      _for_write_seq_lis                            ;766.18
                                ; LOE ebx esi edi
.B12.38:                        ; Preds .B12.37
        push      32                                            ;767.18
        xor       eax, eax                                      ;767.18
        push      eax                                           ;767.18
        push      eax                                           ;767.18
        push      -2088435968                                   ;767.18
        push      eax                                           ;767.18
        push      OFFSET FLAT: __STRLITPACK_209                 ;767.18
        call      _for_stop_core                                ;767.18
                                ; LOE ebx esi edi
.B12.141:                       ; Preds .B12.38
        add       esp, 48                                       ;767.18
                                ; LOE ebx esi edi
.B12.39:                        ; Preds .B12.141
        imul      eax, edi, -84                                 ;
        add       eax, ebx                                      ;
        add       esi, eax                                      ;
        mov       edx, DWORD PTR [24+esi]                       ;769.14
        test      edx, edx                                      ;769.14
        jle       .B12.42       ; Prob 50%                      ;769.14
                                ; LOE edx ebx esi
.B12.40:                        ; Preds .B12.36 .B12.39
        cmp       edx, 24                                       ;769.14
        jle       .B12.59       ; Prob 0%                       ;769.14
                                ; LOE edx ebx esi
.B12.41:                        ; Preds .B12.40
        shl       edx, 2                                        ;769.14
        push      edx                                           ;769.14
        push      0                                             ;769.14
        push      DWORD PTR [esi]                               ;769.14
        call      __intel_fast_memset                           ;769.14
                                ; LOE ebx
.B12.142:                       ; Preds .B12.41
        add       esp, 12                                       ;769.14
                                ; LOE ebx
.B12.42:                        ; Preds .B12.65 .B12.39 .B12.63 .B12.142
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_ICONZONEPAR] ;771.17
        xor       eax, eax                                      ;771.17
        mov       ecx, 2                                        ;771.17
        test      edx, edx                                      ;771.17
        push      ecx                                           ;771.17
        cmovle    edx, eax                                      ;771.17
        push      edx                                           ;771.17
        push      ecx                                           ;771.17
        lea       esi, DWORD PTR [96+esp]                       ;771.17
        push      esi                                           ;771.17
        call      _for_check_mult_overflow                      ;771.17
                                ; LOE eax ebx
.B12.43:                        ; Preds .B12.42
        imul      edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE+32], -84 ;771.17
        and       eax, 1                                        ;771.17
        add       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE] ;771.17
        shl       eax, 4                                        ;771.17
        mov       ecx, DWORD PTR [52+ebx+edx]                   ;771.17
        lea       esi, DWORD PTR [40+edx+ebx]                   ;771.17
        and       ecx, 1                                        ;771.17
        add       ecx, ecx                                      ;771.17
        or        ecx, 1                                        ;771.17
        or        ecx, eax                                      ;771.17
        or        ecx, 262144                                   ;771.17
        push      ecx                                           ;771.17
        push      esi                                           ;771.17
        push      DWORD PTR [108+esp]                           ;771.17
        call      _for_alloc_allocatable                        ;771.17
                                ; LOE eax ebx
.B12.144:                       ; Preds .B12.43
        add       esp, 28                                       ;771.17
        mov       esi, eax                                      ;771.17
                                ; LOE ebx esi
.B12.44:                        ; Preds .B12.144
        test      esi, esi                                      ;771.17
        jne       .B12.47       ; Prob 50%                      ;771.17
                                ; LOE ebx esi
.B12.45:                        ; Preds .B12.44
        mov       DWORD PTR [esp], esi                          ;
        mov       eax, 1                                        ;771.17
        imul      esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE+32], -84 ;771.17
        xor       edi, edi                                      ;771.17
        add       esi, ebx                                      ;771.17
        mov       ecx, 2                                        ;771.17
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE] ;771.17
        mov       DWORD PTR [56+edx+esi], eax                   ;771.17
        mov       DWORD PTR [72+edx+esi], eax                   ;771.17
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_ICONZONEPAR] ;771.17
        test      eax, eax                                      ;771.17
        mov       DWORD PTR [52+edx+esi], 133                   ;771.17
        cmovle    eax, edi                                      ;771.17
        mov       DWORD PTR [44+edx+esi], ecx                   ;771.17
        mov       DWORD PTR [48+edx+esi], edi                   ;771.17
        mov       DWORD PTR [64+edx+esi], eax                   ;771.17
        mov       DWORD PTR [68+edx+esi], ecx                   ;771.17
        lea       edx, DWORD PTR [80+esp]                       ;771.17
        push      ecx                                           ;771.17
        push      eax                                           ;771.17
        push      ecx                                           ;771.17
        push      edx                                           ;771.17
        mov       esi, DWORD PTR [16+esp]                       ;771.17
        call      _for_check_mult_overflow                      ;771.17
                                ; LOE ebx esi
.B12.145:                       ; Preds .B12.45
        add       esp, 16                                       ;771.17
                                ; LOE ebx esi
.B12.46:                        ; Preds .B12.145
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], esi ;771.17
        jmp       .B12.49       ; Prob 100%                     ;771.17
                                ; LOE ebx
.B12.47:                        ; Preds .B12.44
        mov       DWORD PTR [32+esp], 0                         ;773.18
        lea       edx, DWORD PTR [32+esp]                       ;773.18
        mov       DWORD PTR [88+esp], 45                        ;773.18
        lea       eax, DWORD PTR [88+esp]                       ;773.18
        mov       DWORD PTR [92+esp], OFFSET FLAT: __STRLITPACK_14 ;773.18
        push      32                                            ;773.18
        push      eax                                           ;773.18
        push      OFFSET FLAT: __STRLITPACK_210.0.12            ;773.18
        push      -2088435968                                   ;773.18
        push      911                                           ;773.18
        push      edx                                           ;773.18
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], esi ;771.17
        call      _for_write_seq_lis                            ;773.18
                                ; LOE ebx
.B12.48:                        ; Preds .B12.47
        push      32                                            ;774.18
        xor       eax, eax                                      ;774.18
        push      eax                                           ;774.18
        push      eax                                           ;774.18
        push      -2088435968                                   ;774.18
        push      eax                                           ;774.18
        push      OFFSET FLAT: __STRLITPACK_211                 ;774.18
        call      _for_stop_core                                ;774.18
                                ; LOE ebx
.B12.146:                       ; Preds .B12.48
        add       esp, 48                                       ;774.18
                                ; LOE ebx
.B12.49:                        ; Preds .B12.146 .B12.46
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE+32] ;776.14
        mov       DWORD PTR [16+esp], eax                       ;776.14
        imul      eax, eax, -84                                 ;776.14
        add       eax, ebx                                      ;776.14
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE] ;776.14
        mov       ecx, DWORD PTR [64+eax+esi]                   ;776.14
        test      ecx, ecx                                      ;776.14
        jle       .B12.52       ; Prob 50%                      ;776.14
                                ; LOE eax ecx ebx esi
.B12.50:                        ; Preds .B12.49
        cmp       ecx, 48                                       ;776.14
        jle       .B12.68       ; Prob 0%                       ;776.14
                                ; LOE eax ecx ebx esi
.B12.51:                        ; Preds .B12.50
        add       ecx, ecx                                      ;776.14
        push      ecx                                           ;776.14
        push      0                                             ;776.14
        push      DWORD PTR [40+eax+esi]                        ;776.14
        call      __intel_fast_memset                           ;776.14
                                ; LOE ebx esi
.B12.147:                       ; Preds .B12.51
        add       esp, 12                                       ;776.14
                                ; LOE ebx esi
.B12.52:                        ; Preds .B12.74 .B12.72 .B12.49 .B12.147
        mov       eax, DWORD PTR [12+esp]                       ;778.13
        add       ebx, 84                                       ;778.13
        inc       eax                                           ;778.13
        mov       DWORD PTR [12+esp], eax                       ;778.13
        cmp       eax, DWORD PTR [8+esp]                        ;778.13
        jle       .B12.34       ; Prob 82%                      ;778.13
                                ; LOE ebx esi
.B12.54:                        ; Preds .B12.52 .B12.32
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ITERATION], 0 ;780.26
        je        .B12.77       ; Prob 5%                       ;780.26
                                ; LOE
.B12.55:                        ; Preds .B12.109 .B12.92 .B12.107 .B12.54
        add       esp, 100                                      ;796.9
        pop       ebx                                           ;796.9
        pop       edi                                           ;796.9
        pop       esi                                           ;796.9
        mov       esp, ebp                                      ;796.9
        pop       ebp                                           ;796.9
        ret                                                     ;796.9
                                ; LOE
.B12.56:                        ; Preds .B12.26                 ; Infreq
        mov       ebx, 1                                        ;
        jmp       .B12.30       ; Prob 100%                     ;
                                ; LOE edx ecx ebx edi
.B12.57:                        ; Preds .B12.20                 ; Infreq
        mov       ebx, 1                                        ;
        jmp       .B12.24       ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx edi
.B12.58:                        ; Preds .B12.14                 ; Infreq
        mov       ebx, 1                                        ;
        jmp       .B12.18       ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx edi
.B12.59:                        ; Preds .B12.40                 ; Infreq
        cmp       edx, 4                                        ;769.14
        jl        .B12.67       ; Prob 10%                      ;769.14
                                ; LOE edx ebx esi
.B12.60:                        ; Preds .B12.59                 ; Infreq
        mov       eax, edx                                      ;769.14
        xor       edi, edi                                      ;769.14
        mov       ecx, DWORD PTR [esi]                          ;769.14
        and       eax, -4                                       ;769.14
        pxor      xmm0, xmm0                                    ;769.14
                                ; LOE eax edx ecx ebx esi edi xmm0
.B12.61:                        ; Preds .B12.61 .B12.60         ; Infreq
        movdqu    XMMWORD PTR [ecx+edi*4], xmm0                 ;769.14
        add       edi, 4                                        ;769.14
        cmp       edi, eax                                      ;769.14
        jb        .B12.61       ; Prob 82%                      ;769.14
                                ; LOE eax edx ecx ebx esi edi xmm0
.B12.63:                        ; Preds .B12.61 .B12.67         ; Infreq
        cmp       eax, edx                                      ;769.14
        jae       .B12.42       ; Prob 10%                      ;769.14
                                ; LOE eax edx ebx esi
.B12.64:                        ; Preds .B12.63                 ; Infreq
        mov       ecx, DWORD PTR [esi]                          ;769.14
                                ; LOE eax edx ecx ebx
.B12.65:                        ; Preds .B12.65 .B12.64         ; Infreq
        mov       DWORD PTR [ecx+eax*4], 0                      ;769.14
        inc       eax                                           ;769.14
        cmp       eax, edx                                      ;769.14
        jb        .B12.65       ; Prob 82%                      ;769.14
        jmp       .B12.42       ; Prob 100%                     ;769.14
                                ; LOE eax edx ecx ebx
.B12.67:                        ; Preds .B12.59                 ; Infreq
        xor       eax, eax                                      ;769.14
        jmp       .B12.63       ; Prob 100%                     ;769.14
                                ; LOE eax edx ebx esi
.B12.68:                        ; Preds .B12.50                 ; Infreq
        cmp       ecx, 8                                        ;776.14
        jl        .B12.76       ; Prob 10%                      ;776.14
                                ; LOE eax ecx ebx esi
.B12.69:                        ; Preds .B12.68                 ; Infreq
        xor       edi, edi                                      ;776.14
        mov       edx, ecx                                      ;776.14
        mov       DWORD PTR [20+esp], edi                       ;776.14
        and       edx, -8                                       ;776.14
        mov       edi, DWORD PTR [40+eax+esi]                   ;776.14
        pxor      xmm0, xmm0                                    ;776.14
        mov       DWORD PTR [4+esp], esi                        ;776.14
        mov       esi, edi                                      ;776.14
        mov       edi, DWORD PTR [20+esp]                       ;776.14
                                ; LOE eax edx ecx ebx esi edi xmm0
.B12.70:                        ; Preds .B12.70 .B12.69         ; Infreq
        movdqu    XMMWORD PTR [esi+edi*2], xmm0                 ;776.14
        add       edi, 8                                        ;776.14
        cmp       edi, edx                                      ;776.14
        jb        .B12.70       ; Prob 82%                      ;776.14
                                ; LOE eax edx ecx ebx esi edi xmm0
.B12.71:                        ; Preds .B12.70                 ; Infreq
        mov       esi, DWORD PTR [4+esp]                        ;
                                ; LOE eax edx ecx ebx esi
.B12.72:                        ; Preds .B12.71 .B12.76         ; Infreq
        cmp       edx, ecx                                      ;776.14
        jae       .B12.52       ; Prob 10%                      ;776.14
                                ; LOE eax edx ecx ebx esi
.B12.73:                        ; Preds .B12.72                 ; Infreq
        mov       eax, DWORD PTR [40+eax+esi]                   ;776.14
        xor       edi, edi                                      ;776.14
                                ; LOE eax edx ecx ebx esi edi
.B12.74:                        ; Preds .B12.74 .B12.73         ; Infreq
        mov       WORD PTR [eax+edx*2], di                      ;776.14
        inc       edx                                           ;776.14
        cmp       edx, ecx                                      ;776.14
        jb        .B12.74       ; Prob 82%                      ;776.14
        jmp       .B12.52       ; Prob 100%                     ;776.14
                                ; LOE eax edx ecx ebx esi edi
.B12.76:                        ; Preds .B12.68                 ; Infreq
        xor       edx, edx                                      ;776.14
        jmp       .B12.72       ; Prob 100%                     ;776.14
                                ; LOE eax edx ecx ebx esi
.B12.77:                        ; Preds .B12.54                 ; Infreq
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFNODES] ;781.17
        xor       edx, edx                                      ;781.17
        test      eax, eax                                      ;781.17
        lea       ebx, DWORD PTR [12+esp]                       ;781.17
        push      4                                             ;781.17
        lea       ecx, DWORD PTR [1+eax]                        ;781.17
        cmovl     ecx, edx                                      ;781.17
        push      ecx                                           ;781.17
        push      2                                             ;781.17
        push      ebx                                           ;781.17
        call      _for_check_mult_overflow                      ;781.17
                                ; LOE eax
.B12.78:                        ; Preds .B12.77                 ; Infreq
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_BACKPOINTR+12] ;781.17
        and       eax, 1                                        ;781.17
        and       edx, 1                                        ;781.17
        add       edx, edx                                      ;781.17
        shl       eax, 4                                        ;781.17
        or        edx, 1                                        ;781.17
        or        edx, eax                                      ;781.17
        or        edx, 262144                                   ;781.17
        push      edx                                           ;781.17
        push      OFFSET FLAT: _DYNUST_NETWORK_MODULE_mp_BACKPOINTR ;781.17
        push      DWORD PTR [36+esp]                            ;781.17
        call      _for_alloc_allocatable                        ;781.17
                                ; LOE eax
.B12.149:                       ; Preds .B12.78                 ; Infreq
        add       esp, 28                                       ;781.17
        mov       ebx, eax                                      ;781.17
                                ; LOE ebx
.B12.79:                        ; Preds .B12.149                ; Infreq
        test      ebx, ebx                                      ;781.17
        je        .B12.82       ; Prob 50%                      ;781.17
                                ; LOE ebx
.B12.80:                        ; Preds .B12.79                 ; Infreq
        mov       DWORD PTR [32+esp], 0                         ;783.18
        lea       edx, DWORD PTR [32+esp]                       ;783.18
        mov       DWORD PTR [16+esp], 47                        ;783.18
        lea       eax, DWORD PTR [16+esp]                       ;783.18
        mov       DWORD PTR [20+esp], OFFSET FLAT: __STRLITPACK_12 ;783.18
        push      32                                            ;783.18
        push      eax                                           ;783.18
        push      OFFSET FLAT: __STRLITPACK_212.0.12            ;783.18
        push      -2088435968                                   ;783.18
        push      911                                           ;783.18
        push      edx                                           ;783.18
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], ebx ;781.17
        call      _for_write_seq_lis                            ;783.18
                                ; LOE
.B12.81:                        ; Preds .B12.80                 ; Infreq
        push      32                                            ;784.18
        xor       eax, eax                                      ;784.18
        push      eax                                           ;784.18
        push      eax                                           ;784.18
        push      -2088435968                                   ;784.18
        push      eax                                           ;784.18
        push      OFFSET FLAT: __STRLITPACK_213                 ;784.18
        call      _for_stop_core                                ;784.18
                                ; LOE
.B12.150:                       ; Preds .B12.81                 ; Infreq
        add       esp, 48                                       ;784.18
        jmp       .B12.84       ; Prob 100%                     ;784.18
                                ; LOE
.B12.82:                        ; Preds .B12.79                 ; Infreq
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFNODES] ;781.17
        xor       esi, esi                                      ;781.17
        mov       edx, 1                                        ;781.17
        test      ecx, ecx                                      ;781.17
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_BACKPOINTR+16], edx ;781.17
        lea       edi, DWORD PTR [1+ecx]                        ;781.17
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_BACKPOINTR+32], edx ;781.46
        cmovl     edi, esi                                      ;781.17
        mov       eax, 4                                        ;781.17
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_BACKPOINTR+12], 133 ;781.17
        lea       edx, DWORD PTR [8+esp]                        ;781.17
        push      eax                                           ;781.17
        push      edi                                           ;781.17
        push      2                                             ;781.17
        push      edx                                           ;781.17
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_BACKPOINTR+4], eax ;781.17
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_BACKPOINTR+8], esi ;781.17
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_BACKPOINTR+24], edi ;781.17
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_BACKPOINTR+28], eax ;781.17
        call      _for_check_mult_overflow                      ;781.17
                                ; LOE ebx
.B12.151:                       ; Preds .B12.82                 ; Infreq
        add       esp, 16                                       ;781.17
                                ; LOE ebx
.B12.83:                        ; Preds .B12.151                ; Infreq
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], ebx ;781.17
                                ; LOE
.B12.84:                        ; Preds .B12.150 .B12.83        ; Infreq
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_BACKPOINTR+24] ;786.14
        test      edx, edx                                      ;786.14
        jle       .B12.87       ; Prob 50%                      ;786.14
                                ; LOE edx
.B12.85:                        ; Preds .B12.84                 ; Infreq
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_BACKPOINTR] ;786.14
        cmp       edx, 24                                       ;786.14
        jle       .B12.114      ; Prob 0%                       ;786.14
                                ; LOE edx ecx
.B12.86:                        ; Preds .B12.85                 ; Infreq
        shl       edx, 2                                        ;786.14
        push      edx                                           ;786.14
        push      0                                             ;786.14
        push      ecx                                           ;786.14
        call      __intel_fast_memset                           ;786.14
                                ; LOE
.B12.152:                       ; Preds .B12.86                 ; Infreq
        add       esp, 12                                       ;786.14
                                ; LOE
.B12.87:                        ; Preds .B12.128 .B12.152 .B12.84 .B12.126 ; Infreq
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM+12] ;788.14
        and       eax, 1                                        ;788.14
        add       eax, eax                                      ;788.14
        or        eax, 262145                                   ;788.14
        push      eax                                           ;788.14
        push      OFFSET FLAT: _DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM ;788.14
        push      3999996                                       ;788.14
        call      _for_alloc_allocatable                        ;788.14
                                ; LOE eax
.B12.153:                       ; Preds .B12.87                 ; Infreq
        add       esp, 12                                       ;788.14
                                ; LOE eax
.B12.88:                        ; Preds .B12.153                ; Infreq
        test      eax, eax                                      ;788.14
        jne       .B12.90       ; Prob 50%                      ;788.14
                                ; LOE eax
.B12.89:                        ; Preds .B12.88                 ; Infreq
        mov       ecx, 4                                        ;788.14
        mov       edx, 1                                        ;788.14
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM+12], 133 ;788.14
        mov       ebx, 999999                                   ;
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM+4], ecx ;788.14
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM+16], edx ;788.14
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM+8], 0 ;788.14
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM+32], edx ;788.14
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM+24], 999999 ;788.14
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM+28], ecx ;788.14
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;788.14
        jmp       .B12.93       ; Prob 100%                     ;788.14
                                ; LOE ebx
.B12.90:                        ; Preds .B12.88                 ; Infreq
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;788.14
        lea       edx, DWORD PTR [32+esp]                       ;790.18
        mov       DWORD PTR [32+esp], 0                         ;790.18
        lea       eax, DWORD PTR [esp]                          ;790.18
        mov       DWORD PTR [esp], 51                           ;790.18
        mov       DWORD PTR [4+esp], OFFSET FLAT: __STRLITPACK_10 ;790.18
        push      32                                            ;790.18
        push      eax                                           ;790.18
        push      OFFSET FLAT: __STRLITPACK_214.0.12            ;790.18
        push      -2088435968                                   ;790.18
        push      911                                           ;790.18
        push      edx                                           ;790.18
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM+24] ;793.14
        call      _for_write_seq_lis                            ;790.18
                                ; LOE ebx
.B12.91:                        ; Preds .B12.90                 ; Infreq
        push      32                                            ;791.18
        xor       eax, eax                                      ;791.18
        push      eax                                           ;791.18
        push      eax                                           ;791.18
        push      -2088435968                                   ;791.18
        push      eax                                           ;791.18
        push      OFFSET FLAT: __STRLITPACK_215                 ;791.18
        call      _for_stop_core                                ;791.18
                                ; LOE ebx
.B12.154:                       ; Preds .B12.91                 ; Infreq
        add       esp, 48                                       ;791.18
                                ; LOE ebx
.B12.92:                        ; Preds .B12.154                ; Infreq
        test      ebx, ebx                                      ;793.14
        jle       .B12.55       ; Prob 10%                      ;793.14
                                ; LOE ebx
.B12.93:                        ; Preds .B12.89 .B12.92         ; Infreq
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM] ;793.14
        cmp       ebx, 24                                       ;793.14
        jle       .B12.95       ; Prob 0%                       ;793.14
                                ; LOE edx ebx
.B12.94:                        ; Preds .B12.93                 ; Infreq
        shl       ebx, 2                                        ;793.14
        push      ebx                                           ;793.14
        push      0                                             ;793.14
        push      edx                                           ;793.14
        call      __intel_fast_memset                           ;793.14
                                ; LOE
.B12.155:                       ; Preds .B12.94                 ; Infreq
        add       esp, 112                                      ;793.14
        pop       ebx                                           ;793.14
        pop       edi                                           ;793.14
        pop       esi                                           ;793.14
        mov       esp, ebp                                      ;793.14
        pop       ebp                                           ;793.14
        ret                                                     ;793.14
                                ; LOE
.B12.95:                        ; Preds .B12.93                 ; Infreq
        cmp       ebx, 4                                        ;793.14
        jl        .B12.111      ; Prob 10%                      ;793.14
                                ; LOE edx ebx
.B12.96:                        ; Preds .B12.95                 ; Infreq
        mov       eax, edx                                      ;793.14
        and       eax, 15                                       ;793.14
        je        .B12.99       ; Prob 50%                      ;793.14
                                ; LOE eax edx ebx
.B12.97:                        ; Preds .B12.96                 ; Infreq
        test      al, 3                                         ;793.14
        jne       .B12.111      ; Prob 10%                      ;793.14
                                ; LOE eax edx ebx
.B12.98:                        ; Preds .B12.97                 ; Infreq
        neg       eax                                           ;793.14
        add       eax, 16                                       ;793.14
        shr       eax, 2                                        ;793.14
                                ; LOE eax edx ebx
.B12.99:                        ; Preds .B12.98 .B12.96         ; Infreq
        lea       ecx, DWORD PTR [4+eax]                        ;793.14
        cmp       ebx, ecx                                      ;793.14
        jl        .B12.111      ; Prob 10%                      ;793.14
                                ; LOE eax edx ebx
.B12.100:                       ; Preds .B12.99                 ; Infreq
        mov       esi, ebx                                      ;793.14
        sub       esi, eax                                      ;793.14
        and       esi, 3                                        ;793.14
        neg       esi                                           ;793.14
        add       esi, ebx                                      ;793.14
        test      eax, eax                                      ;793.14
        jbe       .B12.104      ; Prob 0%                       ;793.14
                                ; LOE eax edx ebx esi
.B12.101:                       ; Preds .B12.100                ; Infreq
        xor       ecx, ecx                                      ;
                                ; LOE eax edx ecx ebx esi
.B12.102:                       ; Preds .B12.102 .B12.101       ; Infreq
        mov       DWORD PTR [edx+ecx*4], 0                      ;793.14
        inc       ecx                                           ;793.14
        cmp       ecx, eax                                      ;793.14
        jb        .B12.102      ; Prob 82%                      ;793.14
                                ; LOE eax edx ecx ebx esi
.B12.104:                       ; Preds .B12.102 .B12.100       ; Infreq
        pxor      xmm0, xmm0                                    ;793.14
                                ; LOE eax edx ebx esi xmm0
.B12.105:                       ; Preds .B12.105 .B12.104       ; Infreq
        movdqa    XMMWORD PTR [edx+eax*4], xmm0                 ;793.14
        add       eax, 4                                        ;793.14
        cmp       eax, esi                                      ;793.14
        jb        .B12.105      ; Prob 82%                      ;793.14
                                ; LOE eax edx ebx esi xmm0
.B12.107:                       ; Preds .B12.105 .B12.111       ; Infreq
        cmp       esi, ebx                                      ;793.14
        jae       .B12.55       ; Prob 0%                       ;793.14
                                ; LOE edx ebx esi
.B12.109:                       ; Preds .B12.107 .B12.109       ; Infreq
        mov       DWORD PTR [edx+esi*4], 0                      ;793.14
        inc       esi                                           ;793.14
        cmp       esi, ebx                                      ;793.14
        jb        .B12.109      ; Prob 82%                      ;793.14
        jmp       .B12.55       ; Prob 100%                     ;793.14
                                ; LOE edx ebx esi
.B12.111:                       ; Preds .B12.95 .B12.99 .B12.97 ; Infreq
        xor       esi, esi                                      ;793.14
        jmp       .B12.107      ; Prob 100%                     ;793.14
                                ; LOE edx ebx esi
.B12.114:                       ; Preds .B12.85                 ; Infreq
        cmp       edx, 4                                        ;786.14
        jl        .B12.130      ; Prob 10%                      ;786.14
                                ; LOE edx ecx
.B12.115:                       ; Preds .B12.114                ; Infreq
        mov       eax, ecx                                      ;786.14
        and       eax, 15                                       ;786.14
        je        .B12.118      ; Prob 50%                      ;786.14
                                ; LOE eax edx ecx
.B12.116:                       ; Preds .B12.115                ; Infreq
        test      al, 3                                         ;786.14
        jne       .B12.130      ; Prob 10%                      ;786.14
                                ; LOE eax edx ecx
.B12.117:                       ; Preds .B12.116                ; Infreq
        neg       eax                                           ;786.14
        add       eax, 16                                       ;786.14
        shr       eax, 2                                        ;786.14
                                ; LOE eax edx ecx
.B12.118:                       ; Preds .B12.117 .B12.115       ; Infreq
        lea       ebx, DWORD PTR [4+eax]                        ;786.14
        cmp       edx, ebx                                      ;786.14
        jl        .B12.130      ; Prob 10%                      ;786.14
                                ; LOE eax edx ecx
.B12.119:                       ; Preds .B12.118                ; Infreq
        mov       esi, edx                                      ;786.14
        sub       esi, eax                                      ;786.14
        and       esi, 3                                        ;786.14
        neg       esi                                           ;786.14
        add       esi, edx                                      ;786.14
        test      eax, eax                                      ;786.14
        jbe       .B12.123      ; Prob 10%                      ;786.14
                                ; LOE eax edx ecx esi
.B12.120:                       ; Preds .B12.119                ; Infreq
        xor       ebx, ebx                                      ;
                                ; LOE eax edx ecx ebx esi
.B12.121:                       ; Preds .B12.121 .B12.120       ; Infreq
        mov       DWORD PTR [ecx+ebx*4], 0                      ;786.14
        inc       ebx                                           ;786.14
        cmp       ebx, eax                                      ;786.14
        jb        .B12.121      ; Prob 82%                      ;786.14
                                ; LOE eax edx ecx ebx esi
.B12.123:                       ; Preds .B12.121 .B12.119       ; Infreq
        pxor      xmm0, xmm0                                    ;786.14
                                ; LOE eax edx ecx esi xmm0
.B12.124:                       ; Preds .B12.124 .B12.123       ; Infreq
        movdqa    XMMWORD PTR [ecx+eax*4], xmm0                 ;786.14
        add       eax, 4                                        ;786.14
        cmp       eax, esi                                      ;786.14
        jb        .B12.124      ; Prob 82%                      ;786.14
                                ; LOE eax edx ecx esi xmm0
.B12.126:                       ; Preds .B12.124 .B12.130       ; Infreq
        cmp       esi, edx                                      ;786.14
        jae       .B12.87       ; Prob 10%                      ;786.14
                                ; LOE edx ecx esi
.B12.128:                       ; Preds .B12.126 .B12.128       ; Infreq
        mov       DWORD PTR [ecx+esi*4], 0                      ;786.14
        inc       esi                                           ;786.14
        cmp       esi, edx                                      ;786.14
        jb        .B12.128      ; Prob 82%                      ;786.14
        jmp       .B12.87       ; Prob 100%                     ;786.14
                                ; LOE edx ecx esi
.B12.130:                       ; Preds .B12.114 .B12.118 .B12.116 ; Infreq
        xor       esi, esi                                      ;786.14
        jmp       .B12.126      ; Prob 100%                     ;786.14
        ALIGN     16
                                ; LOE edx ecx esi
; mark_end;
_DYNUST_NETWORK_MODULE_mp_ALLOCATE_DYNUST_NETWORK_NODE ENDP
_TEXT	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_206.0.12	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_208.0.12	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_210.0.12	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_212.0.12	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_214.0.12	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_NETWORK_MODULE_mp_ALLOCATE_DYNUST_NETWORK_NODE
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_NETWORK_MODULE_mp_DEALLOCATE_DYNUST_NETWORK_NODE
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_NETWORK_MODULE_mp_DEALLOCATE_DYNUST_NETWORK_NODE
_DYNUST_NETWORK_MODULE_mp_DEALLOCATE_DYNUST_NETWORK_NODE	PROC NEAR 
.B13.1:                         ; Preds .B13.0
        push      esi                                           ;799.20
        push      edi                                           ;799.20
        push      ebx                                           ;799.20
        push      ebp                                           ;799.20
        push      esi                                           ;799.20
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFNODES] ;800.13
        test      ecx, ecx                                      ;800.13
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE] ;804.13
        jle       .B13.7        ; Prob 28%                      ;800.13
                                ; LOE eax ecx esi edi
.B13.2:                         ; Preds .B13.1
        imul      ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE+32], -84 ;
        mov       edx, 1                                        ;
        mov       DWORD PTR [esp], eax                          ;
        add       ebx, eax                                      ;
        mov       ebp, 84                                       ;
        mov       esi, edx                                      ;
        mov       edi, ecx                                      ;
                                ; LOE ebx ebp esi edi
.B13.3:                         ; Preds .B13.5 .B13.2
        mov       eax, DWORD PTR [12+ebp+ebx]                   ;801.28
        mov       edx, eax                                      ;801.17
        shr       edx, 1                                        ;801.17
        and       eax, 1                                        ;801.17
        and       edx, 1                                        ;801.17
        add       eax, eax                                      ;801.17
        shl       edx, 2                                        ;801.17
        or        edx, eax                                      ;801.17
        or        edx, 262144                                   ;801.17
        push      edx                                           ;801.17
        push      DWORD PTR [ebp+ebx]                           ;801.17
        call      _for_dealloc_allocatable                      ;801.17
                                ; LOE ebx ebp esi edi
.B13.4:                         ; Preds .B13.3
        mov       eax, DWORD PTR [52+ebp+ebx]                   ;802.28
        mov       edx, eax                                      ;802.17
        shr       edx, 1                                        ;802.17
        and       eax, 1                                        ;802.17
        and       edx, 1                                        ;802.17
        add       eax, eax                                      ;802.17
        shl       edx, 2                                        ;802.17
        or        edx, eax                                      ;802.17
        or        edx, 262144                                   ;802.17
        push      edx                                           ;802.17
        push      DWORD PTR [40+ebp+ebx]                        ;802.17
        mov       DWORD PTR [ebp+ebx], 0                        ;801.17
        and       DWORD PTR [12+ebp+ebx], -2                    ;801.17
        call      _for_dealloc_allocatable                      ;802.17
                                ; LOE ebx ebp esi edi
.B13.22:                        ; Preds .B13.4
        add       esp, 16                                       ;802.17
                                ; LOE ebx ebp esi edi
.B13.5:                         ; Preds .B13.22
        inc       esi                                           ;803.13
        mov       DWORD PTR [40+ebp+ebx], 0                     ;802.17
        and       DWORD PTR [52+ebp+ebx], -2                    ;802.17
        add       ebp, 84                                       ;803.13
        cmp       esi, edi                                      ;803.13
        jle       .B13.3        ; Prob 82%                      ;803.13
                                ; LOE ebx ebp esi edi
.B13.6:                         ; Preds .B13.5
        mov       eax, DWORD PTR [esp]                          ;
                                ; LOE eax esi edi
.B13.7:                         ; Preds .B13.1 .B13.6
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE+12] ;804.13
        test      bl, 1                                         ;804.13
        je        .B13.14       ; Prob 60%                      ;804.13
                                ; LOE eax ebx esi edi
.B13.8:                         ; Preds .B13.7
        imul      ebp, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE+24], 84 ;804.13
        mov       edx, eax                                      ;804.13
        add       ebp, eax                                      ;804.13
        cmp       eax, ebp                                      ;804.13
        jae       .B13.14       ; Prob 10%                      ;804.13
                                ; LOE eax edx ebx ebp esi edi
.B13.9:                         ; Preds .B13.8
        mov       DWORD PTR [esp], eax                          ;
        mov       esi, edx                                      ;
                                ; LOE ebx ebp esi
.B13.10:                        ; Preds .B13.12 .B13.9
        mov       edi, DWORD PTR [12+esi]                       ;804.13
        test      edi, 1                                        ;804.13
        jne       .B13.18       ; Prob 3%                       ;804.13
                                ; LOE ebx ebp esi edi
.B13.11:                        ; Preds .B13.10 .B13.19
        mov       edi, DWORD PTR [52+esi]                       ;804.13
        test      edi, 1                                        ;804.13
        jne       .B13.16       ; Prob 3%                       ;804.13
                                ; LOE ebx ebp esi edi
.B13.12:                        ; Preds .B13.11 .B13.17
        add       esi, 84                                       ;804.13
        cmp       esi, ebp                                      ;804.13
        jb        .B13.10       ; Prob 82%                      ;804.13
                                ; LOE ebx ebp esi
.B13.13:                        ; Preds .B13.12
        mov       eax, DWORD PTR [esp]                          ;
                                ; LOE eax ebx esi edi
.B13.14:                        ; Preds .B13.13 .B13.8 .B13.7
        mov       ecx, ebx                                      ;804.13
        mov       edx, ebx                                      ;804.13
        shr       ecx, 1                                        ;804.13
        and       edx, 1                                        ;804.13
        and       ecx, 1                                        ;804.13
        add       edx, edx                                      ;804.13
        shl       ecx, 2                                        ;804.13
        or        ecx, edx                                      ;804.13
        or        ecx, 262144                                   ;804.13
        push      ecx                                           ;804.13
        push      eax                                           ;804.13
        call      _for_dealloc_allocatable                      ;804.13
                                ; LOE ebx esi edi
.B13.15:                        ; Preds .B13.14
        and       ebx, -2                                       ;804.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE], 0 ;804.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE+12], ebx ;804.13
        add       esp, 12                                       ;805.9
        pop       ebp                                           ;805.9
        pop       ebx                                           ;805.9
        pop       edi                                           ;805.9
        pop       esi                                           ;805.9
        ret                                                     ;805.9
                                ; LOE
.B13.16:                        ; Preds .B13.11                 ; Infreq
        push      262145                                        ;804.13
        push      DWORD PTR [40+esi]                            ;804.13
        call      _for_deallocate                               ;804.13
                                ; LOE ebx ebp esi edi
.B13.24:                        ; Preds .B13.16                 ; Infreq
        add       esp, 8                                        ;804.13
                                ; LOE ebx ebp esi edi
.B13.17:                        ; Preds .B13.24                 ; Infreq
        and       edi, -2                                       ;804.13
        mov       DWORD PTR [40+esi], 0                         ;804.13
        mov       DWORD PTR [52+esi], edi                       ;804.13
        jmp       .B13.12       ; Prob 100%                     ;804.13
                                ; LOE ebx ebp esi
.B13.18:                        ; Preds .B13.10                 ; Infreq
        push      262145                                        ;804.13
        push      DWORD PTR [esi]                               ;804.13
        call      _for_deallocate                               ;804.13
                                ; LOE ebx ebp esi edi
.B13.25:                        ; Preds .B13.18                 ; Infreq
        add       esp, 8                                        ;804.13
                                ; LOE ebx ebp esi edi
.B13.19:                        ; Preds .B13.25                 ; Infreq
        and       edi, -2                                       ;804.13
        mov       DWORD PTR [esi], 0                            ;804.13
        mov       DWORD PTR [12+esi], edi                       ;804.13
        jmp       .B13.11       ; Prob 100%                     ;804.13
        ALIGN     16
                                ; LOE ebx ebp esi
; mark_end;
_DYNUST_NETWORK_MODULE_mp_DEALLOCATE_DYNUST_NETWORK_NODE ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_NETWORK_MODULE_mp_DEALLOCATE_DYNUST_NETWORK_NODE
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_NETWORK_MODULE_mp_ALLOCATE_DYNUST_NETWORK_NODE_NDE
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_NETWORK_MODULE_mp_ALLOCATE_DYNUST_NETWORK_NODE_NDE
_DYNUST_NETWORK_MODULE_mp_ALLOCATE_DYNUST_NETWORK_NODE_NDE	PROC NEAR 
.B14.1:                         ; Preds .B14.0
        push      ebp                                           ;810.20
        mov       ebp, esp                                      ;810.20
        and       esp, -16                                      ;810.20
        push      esi                                           ;810.20
        push      edi                                           ;810.20
        push      ebx                                           ;810.20
        sub       esp, 84                                       ;810.20
        xor       eax, eax                                      ;812.13
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFNODES] ;812.13
        test      edx, edx                                      ;812.13
        lea       ecx, DWORD PTR [12+esp]                       ;812.13
        push      44                                            ;812.13
        cmovle    edx, eax                                      ;812.13
        push      edx                                           ;812.13
        push      2                                             ;812.13
        push      ecx                                           ;812.13
        call      _for_check_mult_overflow                      ;812.13
                                ; LOE eax
.B14.2:                         ; Preds .B14.1
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE+12] ;812.13
        and       eax, 1                                        ;812.13
        and       edx, 1                                        ;812.13
        add       edx, edx                                      ;812.13
        shl       eax, 4                                        ;812.13
        or        edx, 1                                        ;812.13
        or        edx, eax                                      ;812.13
        or        edx, 262144                                   ;812.13
        push      edx                                           ;812.13
        push      OFFSET FLAT: _DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE ;812.13
        push      DWORD PTR [36+esp]                            ;812.13
        call      _for_alloc_allocatable                        ;812.13
                                ; LOE eax
.B14.44:                        ; Preds .B14.2
        add       esp, 28                                       ;812.13
        mov       ebx, eax                                      ;812.13
                                ; LOE ebx
.B14.3:                         ; Preds .B14.44
        test      ebx, ebx                                      ;812.13
        jne       .B14.13       ; Prob 50%                      ;812.13
                                ; LOE ebx
.B14.4:                         ; Preds .B14.3
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFNODES] ;812.13
        xor       edx, edx                                      ;812.13
        test      esi, esi                                      ;812.13
        cmovle    esi, edx                                      ;812.13
        mov       ecx, 44                                       ;812.13
        lea       edi, DWORD PTR [8+esp]                        ;812.13
        push      ecx                                           ;812.13
        push      esi                                           ;812.13
        push      2                                             ;812.13
        push      edi                                           ;812.13
        mov       eax, 1                                        ;812.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE+12], 133 ;812.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE+4], ecx ;812.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE+16], eax ;812.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE+8], edx ;812.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE+32], eax ;812.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE+24], esi ;812.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE+28], ecx ;812.13
        call      _for_check_mult_overflow                      ;812.13
                                ; LOE ebx
.B14.45:                        ; Preds .B14.4
        add       esp, 16                                       ;812.13
                                ; LOE ebx
.B14.5:                         ; Preds .B14.45
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE] ;812.13
        mov       edx, DWORD PTR [8+esp]                        ;812.13
        add       edx, esi                                      ;812.13
        cmp       esi, edx                                      ;812.13
        jae       .B14.14       ; Prob 50%                      ;812.13
                                ; LOE edx ebx esi
.B14.6:                         ; Preds .B14.5
        xor       eax, eax                                      ;812.13
        xor       ecx, ecx                                      ;812.13
        sub       edx, esi                                      ;812.13
        sbb       ecx, 0                                        ;812.13
        add       edx, 43                                       ;812.13
        push      eax                                           ;812.13
        push      44                                            ;812.13
        adc       ecx, 0                                        ;812.13
        push      ecx                                           ;812.13
        push      edx                                           ;812.13
        call      __alldiv                                      ;812.13
                                ; LOE eax ebx esi
.B14.46:                        ; Preds .B14.6
        mov       edx, eax                                      ;812.13
        shr       edx, 1                                        ;812.13
        test      edx, edx                                      ;812.13
        ja        .B14.8        ; Prob 67%                      ;812.13
                                ; LOE eax edx ebx esi
.B14.7:                         ; Preds .B14.46
        mov       edx, 1                                        ;
        jmp       .B14.11       ; Prob 100%                     ;
                                ; LOE eax edx ebx esi
.B14.8:                         ; Preds .B14.46
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_ALLOCATE_DYNUST_NETWORK_NODE_NDE$BLK..T1998_+40] ;
        xor       ecx, ecx                                      ;
        mov       DWORD PTR [esp], ebx                          ;
        movsd     xmm4, QWORD PTR [_DYNUST_NETWORK_MODULE_mp_ALLOCATE_DYNUST_NETWORK_NODE_NDE$BLK..T1998_] ;
        movsd     xmm3, QWORD PTR [_DYNUST_NETWORK_MODULE_mp_ALLOCATE_DYNUST_NETWORK_NODE_NDE$BLK..T1998_+8] ;
        movsd     xmm2, QWORD PTR [_DYNUST_NETWORK_MODULE_mp_ALLOCATE_DYNUST_NETWORK_NODE_NDE$BLK..T1998_+16] ;
        movsd     xmm1, QWORD PTR [_DYNUST_NETWORK_MODULE_mp_ALLOCATE_DYNUST_NETWORK_NODE_NDE$BLK..T1998_+24] ;
        movsd     xmm0, QWORD PTR [_DYNUST_NETWORK_MODULE_mp_ALLOCATE_DYNUST_NETWORK_NODE_NDE$BLK..T1998_+32] ;
        mov       ebx, edi                                      ;
        mov       edi, ecx                                      ;
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1 xmm2 xmm3 xmm4
.B14.9:                         ; Preds .B14.9 .B14.8
        inc       ecx                                           ;812.13
        movsd     QWORD PTR [edi+esi], xmm4                     ;812.13
        movsd     QWORD PTR [8+edi+esi], xmm3                   ;812.13
        movsd     QWORD PTR [16+edi+esi], xmm2                  ;812.13
        movsd     QWORD PTR [24+edi+esi], xmm1                  ;812.13
        movsd     QWORD PTR [32+edi+esi], xmm0                  ;812.13
        mov       DWORD PTR [40+edi+esi], ebx                   ;812.13
        movsd     QWORD PTR [44+edi+esi], xmm4                  ;812.13
        movsd     QWORD PTR [52+edi+esi], xmm3                  ;812.13
        movsd     QWORD PTR [60+edi+esi], xmm2                  ;812.13
        movsd     QWORD PTR [68+edi+esi], xmm1                  ;812.13
        movsd     QWORD PTR [76+edi+esi], xmm0                  ;812.13
        mov       DWORD PTR [84+edi+esi], ebx                   ;812.13
        add       edi, 88                                       ;812.13
        cmp       ecx, edx                                      ;812.13
        jb        .B14.9        ; Prob 24%                      ;812.13
                                ; LOE eax edx ecx ebx esi edi xmm0 xmm1 xmm2 xmm3 xmm4
.B14.10:                        ; Preds .B14.9
        mov       ebx, DWORD PTR [esp]                          ;
        lea       edx, DWORD PTR [1+ecx+ecx]                    ;812.13
                                ; LOE eax edx ebx esi
.B14.11:                        ; Preds .B14.10 .B14.7
        dec       edx                                           ;812.13
        cmp       edx, eax                                      ;812.13
        jae       .B14.53       ; Prob 33%                      ;812.13
                                ; LOE edx ebx esi
.B14.12:                        ; Preds .B14.11
        imul      edx, edx, 44                                  ;812.13
        movsd     xmm0, QWORD PTR [_DYNUST_NETWORK_MODULE_mp_ALLOCATE_DYNUST_NETWORK_NODE_NDE$BLK..T1998_] ;812.13
        movsd     xmm1, QWORD PTR [_DYNUST_NETWORK_MODULE_mp_ALLOCATE_DYNUST_NETWORK_NODE_NDE$BLK..T1998_+8] ;812.13
        movsd     xmm2, QWORD PTR [_DYNUST_NETWORK_MODULE_mp_ALLOCATE_DYNUST_NETWORK_NODE_NDE$BLK..T1998_+16] ;812.13
        movsd     xmm3, QWORD PTR [_DYNUST_NETWORK_MODULE_mp_ALLOCATE_DYNUST_NETWORK_NODE_NDE$BLK..T1998_+24] ;812.13
        movsd     xmm4, QWORD PTR [_DYNUST_NETWORK_MODULE_mp_ALLOCATE_DYNUST_NETWORK_NODE_NDE$BLK..T1998_+32] ;812.13
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_ALLOCATE_DYNUST_NETWORK_NODE_NDE$BLK..T1998_+40] ;812.13
        movsd     QWORD PTR [edx+esi], xmm0                     ;812.13
        movsd     QWORD PTR [8+edx+esi], xmm1                   ;812.13
        movsd     QWORD PTR [16+edx+esi], xmm2                  ;812.13
        movsd     QWORD PTR [24+edx+esi], xmm3                  ;812.13
        movsd     QWORD PTR [32+edx+esi], xmm4                  ;812.13
        mov       DWORD PTR [40+edx+esi], eax                   ;812.13
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], ebx ;812.13
        jmp       .B14.15       ; Prob 100%                     ;812.13
                                ; LOE
.B14.13:                        ; Preds .B14.3 .B14.53
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], ebx ;812.13
        jne       .B14.30       ; Prob 5%                       ;813.22
        jmp       .B14.15       ; Prob 100%                     ;813.22
                                ; LOE
.B14.14:                        ; Preds .B14.5
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], ebx ;812.13
                                ; LOE
.B14.15:                        ; Preds .B14.13 .B14.52 .B14.12 .B14.14
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFNODES] ;818.13
        test      ecx, ecx                                      ;818.13
        jle       .B14.29       ; Prob 29%                      ;818.13
                                ; LOE ecx
.B14.16:                        ; Preds .B14.15
        xor       eax, eax                                      ;
        movdqa    xmm0, XMMWORD PTR [_2il0floatpacket.30]       ;824.14
        xor       ebx, ebx                                      ;
        mov       DWORD PTR [76+esp], eax                       ;
        mov       DWORD PTR [72+esp], ecx                       ;
                                ; LOE ebx
.B14.17:                        ; Preds .B14.27 .B14.16
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOF_MASTER_DESTINATIONS] ;819.14
        xor       eax, eax                                      ;819.14
        test      edx, edx                                      ;819.14
        push      1                                             ;819.14
        cmovle    edx, eax                                      ;819.14
        push      edx                                           ;819.14
        push      2                                             ;819.14
        lea       ecx, DWORD PTR [76+esp]                       ;819.14
        push      ecx                                           ;819.14
        call      _for_check_mult_overflow                      ;819.14
                                ; LOE eax ebx
.B14.18:                        ; Preds .B14.17
        imul      ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE+32], -44 ;819.14
        and       eax, 1                                        ;819.14
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE] ;819.14
        shl       eax, 4                                        ;819.14
        lea       esi, DWORD PTR [edx+ecx]                      ;819.14
        mov       edi, DWORD PTR [56+esi+ebx]                   ;819.14
        lea       edx, DWORD PTR [44+edx+ecx]                   ;819.14
        and       edi, 1                                        ;819.14
        add       edx, ebx                                      ;819.14
        add       edi, edi                                      ;819.14
        or        edi, 1                                        ;819.14
        or        edi, eax                                      ;819.14
        or        edi, 262144                                   ;819.14
        push      edi                                           ;819.14
        push      edx                                           ;819.14
        push      DWORD PTR [88+esp]                            ;819.14
        call      _for_alloc_allocatable                        ;819.14
                                ; LOE eax ebx
.B14.48:                        ; Preds .B14.18
        add       esp, 28                                       ;819.14
        mov       esi, eax                                      ;819.14
                                ; LOE ebx esi
.B14.19:                        ; Preds .B14.48
        test      esi, esi                                      ;819.14
        jne       .B14.22       ; Prob 50%                      ;819.14
                                ; LOE ebx esi
.B14.20:                        ; Preds .B14.19
        imul      edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE+32], -44 ;819.14
        mov       edx, 1                                        ;819.14
        add       edi, ebx                                      ;819.14
        xor       ecx, ecx                                      ;819.14
        mov       DWORD PTR [52+esp], esi                       ;
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE] ;819.14
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOF_MASTER_DESTINATIONS] ;819.14
        test      eax, eax                                      ;819.14
        mov       DWORD PTR [52+esi+edi], ecx                   ;819.14
        cmovle    eax, ecx                                      ;819.14
        mov       DWORD PTR [56+esi+edi], 133                   ;819.14
        lea       ecx, DWORD PTR [48+esp]                       ;819.14
        push      edx                                           ;819.14
        push      eax                                           ;819.14
        push      2                                             ;819.14
        push      ecx                                           ;819.14
        mov       DWORD PTR [48+esi+edi], edx                   ;819.14
        mov       DWORD PTR [60+esi+edi], edx                   ;819.14
        mov       DWORD PTR [76+esi+edi], edx                   ;819.14
        mov       DWORD PTR [68+esi+edi], eax                   ;819.14
        mov       DWORD PTR [72+esi+edi], edx                   ;819.14
        mov       esi, DWORD PTR [68+esp]                       ;819.14
        call      _for_check_mult_overflow                      ;819.14
                                ; LOE ebx esi
.B14.49:                        ; Preds .B14.20
        add       esp, 16                                       ;819.14
                                ; LOE ebx esi
.B14.21:                        ; Preds .B14.49
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], esi ;819.14
        jmp       .B14.24       ; Prob 100%                     ;819.14
                                ; LOE ebx
.B14.22:                        ; Preds .B14.19
        mov       DWORD PTR [16+esp], 0                         ;821.18
        lea       edx, DWORD PTR [16+esp]                       ;821.18
        mov       DWORD PTR [56+esp], 49                        ;821.18
        lea       eax, DWORD PTR [56+esp]                       ;821.18
        mov       DWORD PTR [60+esp], OFFSET FLAT: __STRLITPACK_6 ;821.18
        push      32                                            ;821.18
        push      eax                                           ;821.18
        push      OFFSET FLAT: __STRLITPACK_218.0.14            ;821.18
        push      -2088435968                                   ;821.18
        push      911                                           ;821.18
        push      edx                                           ;821.18
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], esi ;819.14
        call      _for_write_seq_lis                            ;821.18
                                ; LOE ebx
.B14.23:                        ; Preds .B14.22
        push      32                                            ;822.18
        xor       eax, eax                                      ;822.18
        push      eax                                           ;822.18
        push      eax                                           ;822.18
        push      -2088435968                                   ;822.18
        push      eax                                           ;822.18
        push      OFFSET FLAT: __STRLITPACK_219                 ;822.18
        call      _for_stop_core                                ;822.18
                                ; LOE ebx
.B14.50:                        ; Preds .B14.23
        add       esp, 48                                       ;822.18
                                ; LOE ebx
.B14.24:                        ; Preds .B14.50 .B14.21
        imul      edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE+32], -44 ;824.14
        add       edx, ebx                                      ;824.14
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE] ;824.14
        mov       edi, DWORD PTR [68+ecx+edx]                   ;824.14
        test      edi, edi                                      ;824.14
        jle       .B14.27       ; Prob 50%                      ;824.14
                                ; LOE edx ecx ebx edi
.B14.25:                        ; Preds .B14.24
        cmp       edi, 96                                       ;824.14
        jle       .B14.32       ; Prob 0%                       ;824.14
                                ; LOE edx ecx ebx edi
.B14.26:                        ; Preds .B14.25
        push      edi                                           ;824.14
        push      1                                             ;824.14
        push      DWORD PTR [44+ecx+edx]                        ;824.14
        call      __intel_fast_memset                           ;824.14
                                ; LOE ebx
.B14.51:                        ; Preds .B14.26
        add       esp, 12                                       ;824.14
                                ; LOE ebx
.B14.27:                        ; Preds .B14.38 .B14.51 .B14.24 .B14.36
        mov       eax, DWORD PTR [76+esp]                       ;818.13
        add       ebx, 44                                       ;818.13
        inc       eax                                           ;818.13
        mov       DWORD PTR [76+esp], eax                       ;818.13
        cmp       eax, DWORD PTR [72+esp]                       ;818.13
        jb        .B14.17       ; Prob 82%                      ;818.13
                                ; LOE ebx
.B14.29:                        ; Preds .B14.27 .B14.15
        add       esp, 84                                       ;826.9
        pop       ebx                                           ;826.9
        pop       edi                                           ;826.9
        pop       esi                                           ;826.9
        mov       esp, ebp                                      ;826.9
        pop       ebp                                           ;826.9
        ret                                                     ;826.9
                                ; LOE
.B14.30:                        ; Preds .B14.13                 ; Infreq
        mov       DWORD PTR [16+esp], 0                         ;814.14
        lea       edx, DWORD PTR [16+esp]                       ;814.14
        mov       DWORD PTR [esp], 60                           ;814.14
        lea       eax, DWORD PTR [esp]                          ;814.14
        mov       DWORD PTR [4+esp], OFFSET FLAT: __STRLITPACK_8 ;814.14
        push      32                                            ;814.14
        push      eax                                           ;814.14
        push      OFFSET FLAT: __STRLITPACK_216.0.14            ;814.14
        push      -2088435968                                   ;814.14
        push      911                                           ;814.14
        push      edx                                           ;814.14
        call      _for_write_seq_lis                            ;814.14
                                ; LOE
.B14.31:                        ; Preds .B14.30                 ; Infreq
        push      32                                            ;815.14
        xor       eax, eax                                      ;815.14
        push      eax                                           ;815.14
        push      eax                                           ;815.14
        push      -2088435968                                   ;815.14
        push      eax                                           ;815.14
        push      OFFSET FLAT: __STRLITPACK_217                 ;815.14
        call      _for_stop_core                                ;815.14
                                ; LOE
.B14.52:                        ; Preds .B14.31                 ; Infreq
        add       esp, 48                                       ;815.14
        jmp       .B14.15       ; Prob 100%                     ;815.14
                                ; LOE
.B14.32:                        ; Preds .B14.25                 ; Infreq
        cmp       edi, 16                                       ;824.14
        jl        .B14.40       ; Prob 10%                      ;824.14
                                ; LOE edx ecx ebx edi
.B14.33:                        ; Preds .B14.32                 ; Infreq
        xor       eax, eax                                      ;824.14
        mov       esi, edi                                      ;824.14
        mov       DWORD PTR [80+esp], eax                       ;824.14
        and       esi, -16                                      ;824.14
        mov       DWORD PTR [68+esp], ebx                       ;824.14
        mov       eax, DWORD PTR [44+ecx+edx]                   ;824.14
        movdqa    xmm0, XMMWORD PTR [_2il0floatpacket.30]       ;824.14
        mov       ebx, DWORD PTR [80+esp]                       ;824.14
                                ; LOE eax edx ecx ebx esi edi xmm0
.B14.34:                        ; Preds .B14.34 .B14.33         ; Infreq
        movdqu    XMMWORD PTR [ebx+eax], xmm0                   ;824.14
        add       ebx, 16                                       ;824.14
        cmp       ebx, esi                                      ;824.14
        jb        .B14.34       ; Prob 82%                      ;824.14
                                ; LOE eax edx ecx ebx esi edi xmm0
.B14.35:                        ; Preds .B14.34                 ; Infreq
        mov       ebx, DWORD PTR [68+esp]                       ;
                                ; LOE edx ecx ebx esi edi
.B14.36:                        ; Preds .B14.35 .B14.40         ; Infreq
        cmp       esi, edi                                      ;824.14
        jae       .B14.27       ; Prob 10%                      ;824.14
                                ; LOE edx ecx ebx esi edi
.B14.37:                        ; Preds .B14.36                 ; Infreq
        mov       eax, DWORD PTR [44+ecx+edx]                   ;824.14
                                ; LOE eax ebx esi edi
.B14.38:                        ; Preds .B14.38 .B14.37         ; Infreq
        mov       BYTE PTR [esi+eax], 1                         ;824.14
        inc       esi                                           ;824.14
        cmp       esi, edi                                      ;824.14
        jb        .B14.38       ; Prob 82%                      ;824.14
        jmp       .B14.27       ; Prob 100%                     ;824.14
                                ; LOE eax ebx esi edi
.B14.40:                        ; Preds .B14.32                 ; Infreq
        xor       esi, esi                                      ;824.14
        jmp       .B14.36       ; Prob 100%                     ;824.14
                                ; LOE edx ecx ebx esi edi
.B14.53:                        ; Preds .B14.11                 ; Infreq
        test      ebx, ebx                                      ;812.13
        jmp       .B14.13       ; Prob 100%                     ;812.13
        ALIGN     16
                                ; LOE ebx
; mark_end;
_DYNUST_NETWORK_MODULE_mp_ALLOCATE_DYNUST_NETWORK_NODE_NDE ENDP
_TEXT	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_216.0.14	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_218.0.14	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_NETWORK_MODULE_mp_ALLOCATE_DYNUST_NETWORK_NODE_NDE
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_NETWORK_MODULE_mp_ALLOCATE_DYNUST_NETWORK_ARCMOVE
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_NETWORK_MODULE_mp_ALLOCATE_DYNUST_NETWORK_ARCMOVE
_DYNUST_NETWORK_MODULE_mp_ALLOCATE_DYNUST_NETWORK_ARCMOVE	PROC NEAR 
.B15.1:                         ; Preds .B15.0
        push      ebp                                           ;829.20
        mov       ebp, esp                                      ;829.20
        and       esp, -16                                      ;829.20
        push      esi                                           ;829.20
        push      edi                                           ;829.20
        push      ebx                                           ;829.20
        sub       esp, 116                                      ;829.20
        xor       eax, eax                                      ;833.13
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFARCS] ;833.13
        test      ecx, ecx                                      ;833.13
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_MAXMOVE] ;833.13
        lea       ebx, DWORD PTR [92+esp]                       ;833.13
        cmovle    ecx, eax                                      ;833.13
        test      edx, edx                                      ;833.13
        push      28                                            ;833.13
        cmovle    edx, eax                                      ;833.13
        push      edx                                           ;833.13
        push      ecx                                           ;833.13
        push      3                                             ;833.13
        push      ebx                                           ;833.13
        call      _for_check_mult_overflow                      ;833.13
                                ; LOE eax
.B15.2:                         ; Preds .B15.1
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+12] ;833.13
        and       eax, 1                                        ;833.13
        and       edx, 1                                        ;833.13
        add       edx, edx                                      ;833.13
        shl       eax, 4                                        ;833.13
        or        edx, 1                                        ;833.13
        or        edx, eax                                      ;833.13
        or        edx, 262144                                   ;833.13
        push      edx                                           ;833.13
        push      OFFSET FLAT: _DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE ;833.13
        push      DWORD PTR [120+esp]                           ;833.13
        call      _for_alloc_allocatable                        ;833.13
                                ; LOE eax
.B15.213:                       ; Preds .B15.2
        add       esp, 32                                       ;833.13
        mov       ebx, eax                                      ;833.13
                                ; LOE ebx
.B15.3:                         ; Preds .B15.213
        test      ebx, ebx                                      ;833.13
        jne       .B15.6        ; Prob 50%                      ;833.13
                                ; LOE ebx
.B15.4:                         ; Preds .B15.3
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFARCS] ;833.13
        xor       eax, eax                                      ;833.13
        test      ecx, ecx                                      ;833.13
        cmovle    ecx, eax                                      ;833.13
        mov       edi, 1                                        ;833.13
        mov       esi, 28                                       ;833.13
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_MAXMOVE] ;833.13
        test      edx, edx                                      ;833.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+32], edi ;833.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+44], edi ;833.13
        mov       edi, ecx                                      ;833.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+8], eax ;833.13
        cmovle    edx, eax                                      ;833.13
        shl       edi, 5                                        ;833.13
        lea       eax, DWORD PTR [ecx*4]                        ;833.13
        sub       edi, eax                                      ;833.13
        lea       eax, DWORD PTR [84+esp]                       ;833.13
        push      esi                                           ;833.13
        push      edx                                           ;833.13
        push      ecx                                           ;833.13
        push      3                                             ;833.13
        push      eax                                           ;833.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+12], 133 ;833.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+4], esi ;833.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+16], 2 ;833.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+24], ecx ;833.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+36], edx ;833.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+28], esi ;833.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+40], edi ;833.13
        call      _for_check_mult_overflow                      ;833.13
                                ; LOE ebx
.B15.214:                       ; Preds .B15.4
        add       esp, 20                                       ;833.13
                                ; LOE ebx
.B15.5:                         ; Preds .B15.214
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], ebx ;833.13
        jmp       .B15.8        ; Prob 100%                     ;833.13
                                ; LOE
.B15.6:                         ; Preds .B15.3
        mov       DWORD PTR [32+esp], 0                         ;835.14
        lea       edx, DWORD PTR [32+esp]                       ;835.14
        mov       DWORD PTR [96+esp], 62                        ;835.14
        lea       eax, DWORD PTR [96+esp]                       ;835.14
        mov       DWORD PTR [100+esp], OFFSET FLAT: __STRLITPACK_4 ;835.14
        push      32                                            ;835.14
        push      eax                                           ;835.14
        push      OFFSET FLAT: __STRLITPACK_220.0.15            ;835.14
        push      -2088435968                                   ;835.14
        push      911                                           ;835.14
        push      edx                                           ;835.14
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], ebx ;833.13
        call      _for_write_seq_lis                            ;835.14
                                ; LOE
.B15.7:                         ; Preds .B15.6
        push      32                                            ;836.14
        xor       eax, eax                                      ;836.14
        push      eax                                           ;836.14
        push      eax                                           ;836.14
        push      -2088435968                                   ;836.14
        push      eax                                           ;836.14
        push      OFFSET FLAT: __STRLITPACK_221                 ;836.14
        call      _for_stop_core                                ;836.14
                                ; LOE
.B15.215:                       ; Preds .B15.7
        add       esp, 48                                       ;836.14
                                ; LOE
.B15.8:                         ; Preds .B15.215 .B15.5
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+44] ;839.10
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+36] ;839.10
        test      edx, edx                                      ;839.10
        mov       DWORD PTR [4+esp], eax                        ;839.10
        mov       DWORD PTR [8+esp], edx                        ;839.10
        jle       .B15.19       ; Prob 10%                      ;839.10
                                ; LOE
.B15.9:                         ; Preds .B15.8
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+32] ;839.10
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE] ;848.10
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+40] ;848.10
        mov       DWORD PTR [esp], edx                          ;839.10
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+24] ;839.10
        test      edx, edx                                      ;839.10
        mov       DWORD PTR [16+esp], eax                       ;848.10
        mov       DWORD PTR [12+esp], ecx                       ;848.10
        jg        .B15.97       ; Prob 50%                      ;839.10
                                ; LOE edx
.B15.10:                        ; Preds .B15.104 .B15.206 .B15.9
        test      edx, edx                                      ;840.10
        jg        .B15.90       ; Prob 50%                      ;840.10
                                ; LOE edx
.B15.11:                        ; Preds .B15.160 .B15.10
        test      edx, edx                                      ;841.10
        jg        .B15.83       ; Prob 50%                      ;841.10
                                ; LOE edx
.B15.12:                        ; Preds .B15.156 .B15.11
        test      edx, edx                                      ;842.10
        jg        .B15.76       ; Prob 50%                      ;842.10
                                ; LOE edx
.B15.13:                        ; Preds .B15.152 .B15.12
        test      edx, edx                                      ;843.10
        jg        .B15.69       ; Prob 50%                      ;843.10
                                ; LOE edx
.B15.14:                        ; Preds .B15.148 .B15.13
        test      edx, edx                                      ;844.10
        jg        .B15.62       ; Prob 50%                      ;844.10
                                ; LOE edx
.B15.15:                        ; Preds .B15.144 .B15.14
        test      edx, edx                                      ;845.10
        jg        .B15.55       ; Prob 50%                      ;845.10
                                ; LOE edx
.B15.16:                        ; Preds .B15.140 .B15.15
        test      edx, edx                                      ;846.10
        jg        .B15.48       ; Prob 50%                      ;846.10
                                ; LOE edx
.B15.17:                        ; Preds .B15.136 .B15.16
        test      edx, edx                                      ;847.10
        jg        .B15.41       ; Prob 50%                      ;847.10
                                ; LOE edx
.B15.18:                        ; Preds .B15.132 .B15.17
        test      edx, edx                                      ;848.10
        jg        .B15.34       ; Prob 50%                      ;848.10
                                ; LOE edx
.B15.19:                        ; Preds .B15.208 .B15.40 .B15.8 .B15.18
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFNODES] ;850.10
        xor       eax, eax                                      ;850.10
        imul      edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_MAXMOVE] ;850.10
        lea       ecx, DWORD PTR [112+esp]                      ;850.10
        test      edx, edx                                      ;850.10
        push      56                                            ;850.10
        cmovl     edx, eax                                      ;850.10
        push      edx                                           ;850.10
        push      2                                             ;850.10
        push      ecx                                           ;850.10
        call      _for_check_mult_overflow                      ;850.10
                                ; LOE eax
.B15.20:                        ; Preds .B15.19
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_NSIGN+12] ;850.10
        and       eax, 1                                        ;850.10
        and       edx, 1                                        ;850.10
        add       edx, edx                                      ;850.10
        shl       eax, 4                                        ;850.10
        or        edx, 1                                        ;850.10
        or        edx, eax                                      ;850.10
        or        edx, 262144                                   ;850.10
        push      edx                                           ;850.10
        push      OFFSET FLAT: _DYNUST_NETWORK_MODULE_mp_NSIGN  ;850.10
        push      DWORD PTR [136+esp]                           ;850.10
        call      _for_alloc_allocatable                        ;850.10
                                ; LOE eax
.B15.217:                       ; Preds .B15.20
        add       esp, 28                                       ;850.10
        mov       ebx, eax                                      ;850.10
                                ; LOE ebx
.B15.21:                        ; Preds .B15.217
        test      ebx, ebx                                      ;850.10
        jne       .B15.24       ; Prob 50%                      ;850.10
                                ; LOE ebx
.B15.22:                        ; Preds .B15.21
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFNODES] ;850.10
        xor       eax, eax                                      ;850.10
        imul      edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_MAXMOVE] ;850.10
        mov       edx, 1                                        ;850.10
        test      edi, edi                                      ;850.10
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_NSIGN+32], edx ;850.34
        cmovl     edi, eax                                      ;850.10
        mov       esi, 2                                        ;850.10
        mov       ecx, 4                                        ;850.10
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_NSIGN+44], edx ;850.10
        lea       edx, DWORD PTR [88+esp]                       ;850.10
        push      56                                            ;850.10
        push      edi                                           ;850.10
        push      esi                                           ;850.10
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_NSIGN+8], eax ;850.10
        lea       eax, DWORD PTR [edi*4]                        ;850.10
        push      edx                                           ;850.10
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_NSIGN+12], 133 ;850.10
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_NSIGN+4], ecx ;850.10
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_NSIGN+16], esi ;850.10
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_NSIGN+24], edi ;850.10
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_NSIGN+36], 14 ;850.10
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_NSIGN+28], ecx ;850.10
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_NSIGN+40], eax ;850.10
        call      _for_check_mult_overflow                      ;850.10
                                ; LOE ebx
.B15.218:                       ; Preds .B15.22
        add       esp, 16                                       ;850.10
                                ; LOE ebx
.B15.23:                        ; Preds .B15.218
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], ebx ;850.10
        jmp       .B15.26       ; Prob 100%                     ;850.10
                                ; LOE
.B15.24:                        ; Preds .B15.21
        mov       DWORD PTR [32+esp], 0                         ;852.14
        lea       edx, DWORD PTR [32+esp]                       ;852.14
        mov       DWORD PTR [104+esp], 42                       ;852.14
        lea       eax, DWORD PTR [104+esp]                      ;852.14
        mov       DWORD PTR [108+esp], OFFSET FLAT: __STRLITPACK_2 ;852.14
        push      32                                            ;852.14
        push      eax                                           ;852.14
        push      OFFSET FLAT: __STRLITPACK_222.0.15            ;852.14
        push      -2088435968                                   ;852.14
        push      911                                           ;852.14
        push      edx                                           ;852.14
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], ebx ;850.10
        call      _for_write_seq_lis                            ;852.14
                                ; LOE
.B15.25:                        ; Preds .B15.24
        push      32                                            ;853.14
        xor       eax, eax                                      ;853.14
        push      eax                                           ;853.14
        push      eax                                           ;853.14
        push      -2088435968                                   ;853.14
        push      eax                                           ;853.14
        push      OFFSET FLAT: __STRLITPACK_223                 ;853.14
        call      _for_stop_core                                ;853.14
                                ; LOE
.B15.219:                       ; Preds .B15.25
        add       esp, 48                                       ;853.14
                                ; LOE
.B15.26:                        ; Preds .B15.219 .B15.23
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_NSIGN+36] ;855.10
        test      edi, edi                                      ;855.10
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_NSIGN+44] ;855.10
        jle       .B15.28       ; Prob 10%                      ;855.10
                                ; LOE edx edi
.B15.27:                        ; Preds .B15.26
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_NSIGN+40] ;855.10
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_NSIGN+24] ;855.10
        test      esi, esi                                      ;855.10
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_NSIGN+32] ;855.10
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_NSIGN] ;855.10
        mov       DWORD PTR [20+esp], ebx                       ;855.10
        jg        .B15.30       ; Prob 50%                      ;855.10
                                ; LOE eax edx ecx esi edi
.B15.28:                        ; Preds .B15.121 .B15.33 .B15.26 .B15.27
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ITERATION], 0 ;857.23
        je        .B15.162      ; Prob 5%                       ;857.23
                                ; LOE
.B15.29:                        ; Preds .B15.203 .B15.195 .B15.171 .B15.169 .B15.28
                                ;      
        add       esp, 116                                      ;867.9
        pop       ebx                                           ;867.9
        pop       edi                                           ;867.9
        pop       esi                                           ;867.9
        mov       esp, ebp                                      ;867.9
        pop       ebp                                           ;867.9
        ret                                                     ;867.9
                                ; LOE
.B15.30:                        ; Preds .B15.27
        imul      edx, DWORD PTR [20+esp]                       ;
        xor       ebx, ebx                                      ;
        pxor      xmm0, xmm0                                    ;855.10
        mov       DWORD PTR [12+esp], edi                       ;
        mov       edi, ecx                                      ;
        shl       eax, 2                                        ;
        sub       edi, eax                                      ;
        sub       eax, edx                                      ;
        add       edi, eax                                      ;
        lea       eax, DWORD PTR [esi*4]                        ;
        add       edi, edx                                      ;
        mov       DWORD PTR [esp], ecx                          ;
        mov       DWORD PTR [4+esp], edi                        ;
        mov       DWORD PTR [28+esp], esi                       ;
        mov       DWORD PTR [16+esp], ebx                       ;
        mov       DWORD PTR [8+esp], eax                        ;
        mov       DWORD PTR [24+esp], ecx                       ;
        mov       edi, DWORD PTR [4+esp]                        ;
        mov       esi, DWORD PTR [esp]                          ;
                                ; LOE ebx esi edi
.B15.31:                        ; Preds .B15.33 .B15.121 .B15.30
        cmp       DWORD PTR [28+esp], 24                        ;855.10
        jle       .B15.106      ; Prob 0%                       ;855.10
                                ; LOE ebx esi edi
.B15.32:                        ; Preds .B15.31
        push      DWORD PTR [8+esp]                             ;855.10
        push      0                                             ;855.10
        push      edi                                           ;855.10
        call      __intel_fast_memset                           ;855.10
                                ; LOE ebx esi edi
.B15.220:                       ; Preds .B15.32
        add       esp, 12                                       ;855.10
                                ; LOE ebx esi edi
.B15.33:                        ; Preds .B15.118 .B15.220
        mov       edx, DWORD PTR [16+esp]                       ;855.10
        inc       edx                                           ;855.10
        mov       eax, DWORD PTR [20+esp]                       ;855.10
        add       esi, eax                                      ;855.10
        add       edi, eax                                      ;855.10
        add       ebx, eax                                      ;855.10
        mov       DWORD PTR [16+esp], edx                       ;855.10
        cmp       edx, DWORD PTR [12+esp]                       ;855.10
        jb        .B15.31       ; Prob 82%                      ;855.10
        jmp       .B15.28       ; Prob 100%                     ;855.10
                                ; LOE ebx esi edi
.B15.34:                        ; Preds .B15.18
        mov       edi, DWORD PTR [esp]                          ;
        xor       ecx, ecx                                      ;
        xor       esi, esi                                      ;
        mov       ecx, edx                                      ;
        mov       ebx, DWORD PTR [16+esp]                       ;
        shr       ecx, 31                                       ;
        lea       eax, DWORD PTR [edi*4]                        ;
        shl       edi, 5                                        ;
        add       ecx, edx                                      ;
        sub       edi, eax                                      ;
        mov       eax, DWORD PTR [4+esp]                        ;
        sub       ebx, edi                                      ;
        imul      eax, DWORD PTR [12+esp]                       ;
        sub       edi, eax                                      ;
        add       ebx, edi                                      ;
        add       ebx, eax                                      ;
        xor       eax, eax                                      ;
        sar       ecx, 1                                        ;
        mov       DWORD PTR [16+esp], ebx                       ;
        mov       DWORD PTR [64+esp], edx                       ;
                                ; LOE eax ecx ebx esi
.B15.35:                        ; Preds .B15.40 .B15.208 .B15.34
        test      ecx, ecx                                      ;848.10
        jbe       .B15.129      ; Prob 10%                      ;848.10
                                ; LOE eax ecx ebx esi
.B15.36:                        ; Preds .B15.35
        xor       edx, edx                                      ;
        xor       edi, edi                                      ;
        mov       edx, ebx                                      ;
        mov       DWORD PTR [esp], esi                          ;
        mov       esi, edx                                      ;
        xor       edx, edx                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B15.37:                        ; Preds .B15.37 .B15.36
        inc       edi                                           ;848.10
        mov       DWORD PTR [24+esi], edx                       ;848.10
        mov       DWORD PTR [52+esi], edx                       ;848.10
        add       esi, 56                                       ;848.10
        cmp       edi, ecx                                      ;848.10
        jb        .B15.37       ; Prob 64%                      ;848.10
                                ; LOE eax edx ecx ebx esi edi
.B15.38:                        ; Preds .B15.37
        mov       esi, DWORD PTR [esp]                          ;
        lea       edx, DWORD PTR [1+edi+edi]                    ;848.10
                                ; LOE eax edx ecx ebx esi
.B15.39:                        ; Preds .B15.38 .B15.129
        lea       edi, DWORD PTR [-1+edx]                       ;848.10
        cmp       edi, DWORD PTR [64+esp]                       ;848.10
        jae       .B15.208      ; Prob 10%                      ;848.10
                                ; LOE eax edx ecx ebx esi
.B15.40:                        ; Preds .B15.39
        inc       esi                                           ;848.10
        lea       edi, DWORD PTR [edx*4]                        ;848.10
        shl       edx, 5                                        ;848.10
        sub       edx, edi                                      ;848.10
        add       edx, DWORD PTR [16+esp]                       ;848.10
        mov       DWORD PTR [-4+edx+eax], 0                     ;848.10
        mov       edx, DWORD PTR [12+esp]                       ;848.10
        add       ebx, edx                                      ;848.10
        add       eax, edx                                      ;848.10
        cmp       esi, DWORD PTR [8+esp]                        ;848.10
        jb        .B15.35       ; Prob 82%                      ;848.10
        jmp       .B15.19       ; Prob 100%                     ;848.10
                                ; LOE eax ecx ebx esi
.B15.41:                        ; Preds .B15.17
        mov       ecx, DWORD PTR [4+esp]                        ;
        mov       esi, edx                                      ;
        mov       edi, DWORD PTR [esp]                          ;
        xor       ebx, ebx                                      ;
        imul      ecx, DWORD PTR [12+esp]                       ;
        shr       esi, 31                                       ;
        lea       eax, DWORD PTR [edi*4]                        ;
        shl       edi, 5                                        ;
        add       esi, edx                                      ;
        sub       edi, eax                                      ;
        mov       eax, DWORD PTR [16+esp]                       ;
        sub       eax, edi                                      ;
        sub       edi, ecx                                      ;
        add       eax, edi                                      ;
        add       eax, ecx                                      ;
        xor       ecx, ecx                                      ;
        sar       esi, 1                                        ;
        mov       edi, eax                                      ;
        mov       DWORD PTR [24+esp], eax                       ;
        mov       DWORD PTR [64+esp], edx                       ;
                                ; LOE ecx ebx esi edi
.B15.42:                        ; Preds .B15.47 .B15.131 .B15.41
        test      esi, esi                                      ;847.10
        jbe       .B15.133      ; Prob 10%                      ;847.10
                                ; LOE ecx ebx esi edi
.B15.43:                        ; Preds .B15.42
        xor       eax, eax                                      ;
        mov       edx, edi                                      ;
        mov       DWORD PTR [20+esp], ebx                       ;
        mov       ebx, edx                                      ;
        xor       edx, edx                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B15.44:                        ; Preds .B15.44 .B15.43
        inc       eax                                           ;847.10
        mov       DWORD PTR [20+ebx], edx                       ;847.10
        mov       DWORD PTR [48+ebx], edx                       ;847.10
        add       ebx, 56                                       ;847.10
        cmp       eax, esi                                      ;847.10
        jb        .B15.44       ; Prob 64%                      ;847.10
                                ; LOE eax edx ecx ebx esi edi
.B15.45:                        ; Preds .B15.44
        mov       ebx, DWORD PTR [20+esp]                       ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;847.10
                                ; LOE eax ecx ebx esi edi
.B15.46:                        ; Preds .B15.45 .B15.133
        lea       edx, DWORD PTR [-1+eax]                       ;847.10
        cmp       edx, DWORD PTR [64+esp]                       ;847.10
        jae       .B15.131      ; Prob 10%                      ;847.10
                                ; LOE eax ecx ebx esi edi
.B15.47:                        ; Preds .B15.46
        inc       ebx                                           ;847.10
        lea       edx, DWORD PTR [eax*4]                        ;847.10
        shl       eax, 5                                        ;847.10
        sub       eax, edx                                      ;847.10
        add       eax, DWORD PTR [24+esp]                       ;847.10
        mov       DWORD PTR [-8+eax+ecx], 0                     ;847.10
        mov       eax, DWORD PTR [12+esp]                       ;847.10
        add       edi, eax                                      ;847.10
        add       ecx, eax                                      ;847.10
        cmp       ebx, DWORD PTR [8+esp]                        ;847.10
        jb        .B15.42       ; Prob 82%                      ;847.10
        jmp       .B15.132      ; Prob 100%                     ;847.10
                                ; LOE ecx ebx esi edi
.B15.48:                        ; Preds .B15.16
        mov       ecx, DWORD PTR [4+esp]                        ;
        mov       esi, edx                                      ;
        mov       edi, DWORD PTR [esp]                          ;
        xor       ebx, ebx                                      ;
        imul      ecx, DWORD PTR [12+esp]                       ;
        shr       esi, 31                                       ;
        lea       eax, DWORD PTR [edi*4]                        ;
        shl       edi, 5                                        ;
        add       esi, edx                                      ;
        sub       edi, eax                                      ;
        mov       eax, DWORD PTR [16+esp]                       ;
        sub       eax, edi                                      ;
        sub       edi, ecx                                      ;
        add       eax, edi                                      ;
        add       eax, ecx                                      ;
        xor       ecx, ecx                                      ;
        sar       esi, 1                                        ;
        mov       edi, eax                                      ;
        mov       DWORD PTR [24+esp], eax                       ;
        mov       DWORD PTR [64+esp], edx                       ;
                                ; LOE ecx ebx esi edi
.B15.49:                        ; Preds .B15.54 .B15.135 .B15.48
        test      esi, esi                                      ;846.10
        jbe       .B15.137      ; Prob 10%                      ;846.10
                                ; LOE ecx ebx esi edi
.B15.50:                        ; Preds .B15.49
        xor       eax, eax                                      ;
        mov       edx, edi                                      ;
        mov       DWORD PTR [20+esp], ebx                       ;
        mov       ebx, edx                                      ;
        xor       edx, edx                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B15.51:                        ; Preds .B15.51 .B15.50
        inc       eax                                           ;846.10
        mov       DWORD PTR [16+ebx], edx                       ;846.10
        mov       DWORD PTR [44+ebx], edx                       ;846.10
        add       ebx, 56                                       ;846.10
        cmp       eax, esi                                      ;846.10
        jb        .B15.51       ; Prob 64%                      ;846.10
                                ; LOE eax edx ecx ebx esi edi
.B15.52:                        ; Preds .B15.51
        mov       ebx, DWORD PTR [20+esp]                       ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;846.10
                                ; LOE eax ecx ebx esi edi
.B15.53:                        ; Preds .B15.52 .B15.137
        lea       edx, DWORD PTR [-1+eax]                       ;846.10
        cmp       edx, DWORD PTR [64+esp]                       ;846.10
        jae       .B15.135      ; Prob 10%                      ;846.10
                                ; LOE eax ecx ebx esi edi
.B15.54:                        ; Preds .B15.53
        inc       ebx                                           ;846.10
        lea       edx, DWORD PTR [eax*4]                        ;846.10
        shl       eax, 5                                        ;846.10
        sub       eax, edx                                      ;846.10
        add       eax, DWORD PTR [24+esp]                       ;846.10
        mov       DWORD PTR [-12+eax+ecx], 0                    ;846.10
        mov       eax, DWORD PTR [12+esp]                       ;846.10
        add       edi, eax                                      ;846.10
        add       ecx, eax                                      ;846.10
        cmp       ebx, DWORD PTR [8+esp]                        ;846.10
        jb        .B15.49       ; Prob 82%                      ;846.10
        jmp       .B15.136      ; Prob 100%                     ;846.10
                                ; LOE ecx ebx esi edi
.B15.55:                        ; Preds .B15.15
        mov       ecx, DWORD PTR [4+esp]                        ;
        mov       esi, edx                                      ;
        mov       edi, DWORD PTR [esp]                          ;
        xor       ebx, ebx                                      ;
        imul      ecx, DWORD PTR [12+esp]                       ;
        shr       esi, 31                                       ;
        lea       eax, DWORD PTR [edi*4]                        ;
        shl       edi, 5                                        ;
        add       esi, edx                                      ;
        sub       edi, eax                                      ;
        mov       eax, DWORD PTR [16+esp]                       ;
        sub       eax, edi                                      ;
        sub       edi, ecx                                      ;
        add       eax, edi                                      ;
        add       eax, ecx                                      ;
        xor       ecx, ecx                                      ;
        sar       esi, 1                                        ;
        mov       edi, eax                                      ;
        mov       DWORD PTR [24+esp], eax                       ;
        mov       DWORD PTR [64+esp], edx                       ;
                                ; LOE ecx ebx esi edi
.B15.56:                        ; Preds .B15.61 .B15.139 .B15.55
        test      esi, esi                                      ;845.10
        jbe       .B15.141      ; Prob 10%                      ;845.10
                                ; LOE ecx ebx esi edi
.B15.57:                        ; Preds .B15.56
        xor       eax, eax                                      ;
        mov       edx, edi                                      ;
        mov       DWORD PTR [20+esp], ebx                       ;
        mov       ebx, edx                                      ;
        xor       edx, edx                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B15.58:                        ; Preds .B15.58 .B15.57
        inc       eax                                           ;845.10
        mov       DWORD PTR [12+ebx], edx                       ;845.10
        mov       DWORD PTR [40+ebx], edx                       ;845.10
        add       ebx, 56                                       ;845.10
        cmp       eax, esi                                      ;845.10
        jb        .B15.58       ; Prob 64%                      ;845.10
                                ; LOE eax edx ecx ebx esi edi
.B15.59:                        ; Preds .B15.58
        mov       ebx, DWORD PTR [20+esp]                       ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;845.10
                                ; LOE eax ecx ebx esi edi
.B15.60:                        ; Preds .B15.59 .B15.141
        lea       edx, DWORD PTR [-1+eax]                       ;845.10
        cmp       edx, DWORD PTR [64+esp]                       ;845.10
        jae       .B15.139      ; Prob 10%                      ;845.10
                                ; LOE eax ecx ebx esi edi
.B15.61:                        ; Preds .B15.60
        inc       ebx                                           ;845.10
        lea       edx, DWORD PTR [eax*4]                        ;845.10
        shl       eax, 5                                        ;845.10
        sub       eax, edx                                      ;845.10
        add       eax, DWORD PTR [24+esp]                       ;845.10
        mov       DWORD PTR [-16+eax+ecx], 0                    ;845.10
        mov       eax, DWORD PTR [12+esp]                       ;845.10
        add       edi, eax                                      ;845.10
        add       ecx, eax                                      ;845.10
        cmp       ebx, DWORD PTR [8+esp]                        ;845.10
        jb        .B15.56       ; Prob 82%                      ;845.10
        jmp       .B15.140      ; Prob 100%                     ;845.10
                                ; LOE ecx ebx esi edi
.B15.62:                        ; Preds .B15.14
        mov       ebx, DWORD PTR [4+esp]                        ;
        mov       ecx, edx                                      ;
        mov       esi, DWORD PTR [esp]                          ;
        xor       eax, eax                                      ;
        imul      ebx, DWORD PTR [12+esp]                       ;
        shr       ecx, 31                                       ;
        lea       edi, DWORD PTR [esi*4]                        ;
        shl       esi, 5                                        ;
        add       ecx, edx                                      ;
        sub       esi, edi                                      ;
        mov       edi, DWORD PTR [16+esp]                       ;
        sub       edi, esi                                      ;
        sub       esi, ebx                                      ;
        add       edi, esi                                      ;
        xor       esi, esi                                      ;
        add       edi, ebx                                      ;
        sar       ecx, 1                                        ;
        mov       ebx, edi                                      ;
        mov       DWORD PTR [24+esp], edi                       ;
        mov       DWORD PTR [64+esp], edx                       ;
        mov       edx, eax                                      ;
                                ; LOE edx ecx ebx esi
.B15.63:                        ; Preds .B15.68 .B15.143 .B15.62
        test      ecx, ecx                                      ;844.10
        jbe       .B15.145      ; Prob 10%                      ;844.10
                                ; LOE edx ecx ebx esi
.B15.64:                        ; Preds .B15.63
        mov       DWORD PTR [28+esp], ebx                       ;
        xor       eax, eax                                      ;
        mov       DWORD PTR [20+esp], edx                       ;
        mov       edi, ebx                                      ;
        mov       edx, 1                                        ;
                                ; LOE eax edx ecx esi edi
.B15.65:                        ; Preds .B15.65 .B15.64
        inc       eax                                           ;844.10
        mov       BYTE PTR [11+edi], dl                         ;844.10
        mov       BYTE PTR [39+edi], dl                         ;844.10
        add       edi, 56                                       ;844.10
        cmp       eax, ecx                                      ;844.10
        jb        .B15.65       ; Prob 64%                      ;844.10
                                ; LOE eax edx ecx esi edi
.B15.66:                        ; Preds .B15.65
        mov       ebx, DWORD PTR [28+esp]                       ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;844.10
        mov       edx, DWORD PTR [20+esp]                       ;
                                ; LOE eax edx ecx ebx esi
.B15.67:                        ; Preds .B15.66 .B15.145
        lea       edi, DWORD PTR [-1+eax]                       ;844.10
        cmp       edi, DWORD PTR [64+esp]                       ;844.10
        jae       .B15.143      ; Prob 10%                      ;844.10
                                ; LOE eax edx ecx ebx esi
.B15.68:                        ; Preds .B15.67
        inc       edx                                           ;844.10
        lea       edi, DWORD PTR [eax*4]                        ;844.10
        shl       eax, 5                                        ;844.10
        sub       eax, edi                                      ;844.10
        add       eax, DWORD PTR [24+esp]                       ;844.10
        mov       edi, DWORD PTR [12+esp]                       ;844.10
        add       ebx, edi                                      ;844.10
        mov       BYTE PTR [-17+eax+esi], 1                     ;844.10
        add       esi, edi                                      ;844.10
        cmp       edx, DWORD PTR [8+esp]                        ;844.10
        jb        .B15.63       ; Prob 82%                      ;844.10
        jmp       .B15.144      ; Prob 100%                     ;844.10
                                ; LOE edx ecx ebx esi
.B15.69:                        ; Preds .B15.13
        mov       ebx, DWORD PTR [4+esp]                        ;
        mov       ecx, edx                                      ;
        mov       esi, DWORD PTR [esp]                          ;
        xor       eax, eax                                      ;
        imul      ebx, DWORD PTR [12+esp]                       ;
        shr       ecx, 31                                       ;
        lea       edi, DWORD PTR [esi*4]                        ;
        shl       esi, 5                                        ;
        add       ecx, edx                                      ;
        sub       esi, edi                                      ;
        mov       edi, DWORD PTR [16+esp]                       ;
        sub       edi, esi                                      ;
        sub       esi, ebx                                      ;
        add       edi, esi                                      ;
        xor       esi, esi                                      ;
        add       edi, ebx                                      ;
        sar       ecx, 1                                        ;
        mov       ebx, edi                                      ;
        mov       DWORD PTR [24+esp], edi                       ;
        mov       DWORD PTR [64+esp], edx                       ;
        mov       edx, eax                                      ;
                                ; LOE edx ecx ebx esi
.B15.70:                        ; Preds .B15.75 .B15.147 .B15.69
        test      ecx, ecx                                      ;843.10
        jbe       .B15.149      ; Prob 10%                      ;843.10
                                ; LOE edx ecx ebx esi
.B15.71:                        ; Preds .B15.70
        mov       DWORD PTR [28+esp], ebx                       ;
        xor       eax, eax                                      ;
        mov       DWORD PTR [20+esp], edx                       ;
        mov       edi, ebx                                      ;
        mov       edx, 1                                        ;
                                ; LOE eax edx ecx esi edi
.B15.72:                        ; Preds .B15.72 .B15.71
        inc       eax                                           ;843.10
        mov       BYTE PTR [10+edi], dl                         ;843.10
        mov       BYTE PTR [38+edi], dl                         ;843.10
        add       edi, 56                                       ;843.10
        cmp       eax, ecx                                      ;843.10
        jb        .B15.72       ; Prob 64%                      ;843.10
                                ; LOE eax edx ecx esi edi
.B15.73:                        ; Preds .B15.72
        mov       ebx, DWORD PTR [28+esp]                       ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;843.10
        mov       edx, DWORD PTR [20+esp]                       ;
                                ; LOE eax edx ecx ebx esi
.B15.74:                        ; Preds .B15.73 .B15.149
        lea       edi, DWORD PTR [-1+eax]                       ;843.10
        cmp       edi, DWORD PTR [64+esp]                       ;843.10
        jae       .B15.147      ; Prob 10%                      ;843.10
                                ; LOE eax edx ecx ebx esi
.B15.75:                        ; Preds .B15.74
        inc       edx                                           ;843.10
        lea       edi, DWORD PTR [eax*4]                        ;843.10
        shl       eax, 5                                        ;843.10
        sub       eax, edi                                      ;843.10
        add       eax, DWORD PTR [24+esp]                       ;843.10
        mov       edi, DWORD PTR [12+esp]                       ;843.10
        add       ebx, edi                                      ;843.10
        mov       BYTE PTR [-18+eax+esi], 1                     ;843.10
        add       esi, edi                                      ;843.10
        cmp       edx, DWORD PTR [8+esp]                        ;843.10
        jb        .B15.70       ; Prob 82%                      ;843.10
        jmp       .B15.148      ; Prob 100%                     ;843.10
                                ; LOE edx ecx ebx esi
.B15.76:                        ; Preds .B15.12
        mov       ecx, DWORD PTR [4+esp]                        ;
        mov       esi, edx                                      ;
        mov       edi, DWORD PTR [esp]                          ;
        xor       ebx, ebx                                      ;
        imul      ecx, DWORD PTR [12+esp]                       ;
        shr       esi, 31                                       ;
        lea       eax, DWORD PTR [edi*4]                        ;
        shl       edi, 5                                        ;
        add       esi, edx                                      ;
        sub       edi, eax                                      ;
        mov       eax, DWORD PTR [16+esp]                       ;
        sub       eax, edi                                      ;
        sub       edi, ecx                                      ;
        add       eax, edi                                      ;
        add       eax, ecx                                      ;
        xor       ecx, ecx                                      ;
        sar       esi, 1                                        ;
        mov       edi, eax                                      ;
        mov       DWORD PTR [24+esp], eax                       ;
        mov       DWORD PTR [64+esp], edx                       ;
                                ; LOE ecx ebx esi edi
.B15.77:                        ; Preds .B15.82 .B15.151 .B15.76
        test      esi, esi                                      ;842.10
        jbe       .B15.153      ; Prob 10%                      ;842.10
                                ; LOE ecx ebx esi edi
.B15.78:                        ; Preds .B15.77
        xor       eax, eax                                      ;
        mov       edx, edi                                      ;
        mov       DWORD PTR [20+esp], ebx                       ;
        mov       ebx, edx                                      ;
        xor       edx, edx                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B15.79:                        ; Preds .B15.79 .B15.78
        inc       eax                                           ;842.10
        mov       BYTE PTR [9+ebx], dl                          ;842.10
        mov       BYTE PTR [37+ebx], dl                         ;842.10
        add       ebx, 56                                       ;842.10
        cmp       eax, esi                                      ;842.10
        jb        .B15.79       ; Prob 64%                      ;842.10
                                ; LOE eax edx ecx ebx esi edi
.B15.80:                        ; Preds .B15.79
        mov       ebx, DWORD PTR [20+esp]                       ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;842.10
                                ; LOE eax ecx ebx esi edi
.B15.81:                        ; Preds .B15.80 .B15.153
        lea       edx, DWORD PTR [-1+eax]                       ;842.10
        cmp       edx, DWORD PTR [64+esp]                       ;842.10
        jae       .B15.151      ; Prob 10%                      ;842.10
                                ; LOE eax ecx ebx esi edi
.B15.82:                        ; Preds .B15.81
        inc       ebx                                           ;842.10
        lea       edx, DWORD PTR [eax*4]                        ;842.10
        shl       eax, 5                                        ;842.10
        sub       eax, edx                                      ;842.10
        add       eax, DWORD PTR [24+esp]                       ;842.10
        mov       BYTE PTR [-19+eax+ecx], 0                     ;842.10
        mov       eax, DWORD PTR [12+esp]                       ;842.10
        add       edi, eax                                      ;842.10
        add       ecx, eax                                      ;842.10
        cmp       ebx, DWORD PTR [8+esp]                        ;842.10
        jb        .B15.77       ; Prob 82%                      ;842.10
        jmp       .B15.152      ; Prob 100%                     ;842.10
                                ; LOE ecx ebx esi edi
.B15.83:                        ; Preds .B15.11
        mov       ebx, DWORD PTR [4+esp]                        ;
        mov       ecx, edx                                      ;
        mov       esi, DWORD PTR [esp]                          ;
        xor       eax, eax                                      ;
        imul      ebx, DWORD PTR [12+esp]                       ;
        shr       ecx, 31                                       ;
        lea       edi, DWORD PTR [esi*4]                        ;
        shl       esi, 5                                        ;
        add       ecx, edx                                      ;
        sub       esi, edi                                      ;
        mov       edi, DWORD PTR [16+esp]                       ;
        sub       edi, esi                                      ;
        sub       esi, ebx                                      ;
        add       edi, esi                                      ;
        xor       esi, esi                                      ;
        add       edi, ebx                                      ;
        sar       ecx, 1                                        ;
        mov       ebx, edi                                      ;
        mov       DWORD PTR [24+esp], edi                       ;
        mov       DWORD PTR [64+esp], edx                       ;
        mov       edx, eax                                      ;
                                ; LOE edx ecx ebx esi
.B15.84:                        ; Preds .B15.89 .B15.155 .B15.83
        test      ecx, ecx                                      ;841.10
        jbe       .B15.157      ; Prob 10%                      ;841.10
                                ; LOE edx ecx ebx esi
.B15.85:                        ; Preds .B15.84
        mov       DWORD PTR [28+esp], ebx                       ;
        xor       eax, eax                                      ;
        mov       DWORD PTR [20+esp], edx                       ;
        mov       edi, ebx                                      ;
        mov       edx, 1                                        ;
                                ; LOE eax edx ecx esi edi
.B15.86:                        ; Preds .B15.86 .B15.85
        inc       eax                                           ;841.10
        mov       BYTE PTR [8+edi], dl                          ;841.10
        mov       BYTE PTR [36+edi], dl                         ;841.10
        add       edi, 56                                       ;841.10
        cmp       eax, ecx                                      ;841.10
        jb        .B15.86       ; Prob 64%                      ;841.10
                                ; LOE eax edx ecx esi edi
.B15.87:                        ; Preds .B15.86
        mov       ebx, DWORD PTR [28+esp]                       ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;841.10
        mov       edx, DWORD PTR [20+esp]                       ;
                                ; LOE eax edx ecx ebx esi
.B15.88:                        ; Preds .B15.87 .B15.157
        lea       edi, DWORD PTR [-1+eax]                       ;841.10
        cmp       edi, DWORD PTR [64+esp]                       ;841.10
        jae       .B15.155      ; Prob 10%                      ;841.10
                                ; LOE eax edx ecx ebx esi
.B15.89:                        ; Preds .B15.88
        inc       edx                                           ;841.10
        lea       edi, DWORD PTR [eax*4]                        ;841.10
        shl       eax, 5                                        ;841.10
        sub       eax, edi                                      ;841.10
        add       eax, DWORD PTR [24+esp]                       ;841.10
        mov       edi, DWORD PTR [12+esp]                       ;841.10
        add       ebx, edi                                      ;841.10
        mov       BYTE PTR [-20+eax+esi], 1                     ;841.10
        add       esi, edi                                      ;841.10
        cmp       edx, DWORD PTR [8+esp]                        ;841.10
        jb        .B15.84       ; Prob 82%                      ;841.10
        jmp       .B15.156      ; Prob 100%                     ;841.10
                                ; LOE edx ecx ebx esi
.B15.90:                        ; Preds .B15.10
        mov       ecx, DWORD PTR [4+esp]                        ;
        mov       esi, edx                                      ;
        mov       edi, DWORD PTR [esp]                          ;
        xor       ebx, ebx                                      ;
        imul      ecx, DWORD PTR [12+esp]                       ;
        shr       esi, 31                                       ;
        lea       eax, DWORD PTR [edi*4]                        ;
        shl       edi, 5                                        ;
        add       esi, edx                                      ;
        sub       edi, eax                                      ;
        mov       eax, DWORD PTR [16+esp]                       ;
        sub       eax, edi                                      ;
        sub       edi, ecx                                      ;
        add       eax, edi                                      ;
        add       eax, ecx                                      ;
        xor       ecx, ecx                                      ;
        sar       esi, 1                                        ;
        mov       edi, eax                                      ;
        mov       DWORD PTR [24+esp], eax                       ;
        mov       DWORD PTR [64+esp], edx                       ;
                                ; LOE ecx ebx esi edi
.B15.91:                        ; Preds .B15.96 .B15.159 .B15.90
        test      esi, esi                                      ;840.10
        jbe       .B15.161      ; Prob 10%                      ;840.10
                                ; LOE ecx ebx esi edi
.B15.92:                        ; Preds .B15.91
        xor       eax, eax                                      ;
        mov       edx, edi                                      ;
        mov       DWORD PTR [20+esp], ebx                       ;
        mov       ebx, edx                                      ;
        xor       edx, edx                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B15.93:                        ; Preds .B15.93 .B15.92
        inc       eax                                           ;840.10
        mov       DWORD PTR [4+ebx], edx                        ;840.10
        mov       DWORD PTR [32+ebx], edx                       ;840.10
        add       ebx, 56                                       ;840.10
        cmp       eax, esi                                      ;840.10
        jb        .B15.93       ; Prob 64%                      ;840.10
                                ; LOE eax edx ecx ebx esi edi
.B15.94:                        ; Preds .B15.93
        mov       ebx, DWORD PTR [20+esp]                       ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;840.10
                                ; LOE eax ecx ebx esi edi
.B15.95:                        ; Preds .B15.94 .B15.161
        lea       edx, DWORD PTR [-1+eax]                       ;840.10
        cmp       edx, DWORD PTR [64+esp]                       ;840.10
        jae       .B15.159      ; Prob 10%                      ;840.10
                                ; LOE eax ecx ebx esi edi
.B15.96:                        ; Preds .B15.95
        inc       ebx                                           ;840.10
        lea       edx, DWORD PTR [eax*4]                        ;840.10
        shl       eax, 5                                        ;840.10
        sub       eax, edx                                      ;840.10
        add       eax, DWORD PTR [24+esp]                       ;840.10
        mov       DWORD PTR [-24+eax+ecx], 0                    ;840.10
        mov       eax, DWORD PTR [12+esp]                       ;840.10
        add       edi, eax                                      ;840.10
        add       ecx, eax                                      ;840.10
        cmp       ebx, DWORD PTR [8+esp]                        ;840.10
        jb        .B15.91       ; Prob 82%                      ;840.10
        jmp       .B15.160      ; Prob 100%                     ;840.10
                                ; LOE ecx ebx esi edi
.B15.97:                        ; Preds .B15.9
        mov       eax, DWORD PTR [esp]                          ;
        xor       ecx, ecx                                      ;
        xor       edi, edi                                      ;
        mov       esi, edx                                      ;
        mov       ecx, DWORD PTR [16+esp]                       ;
        shr       esi, 31                                       ;
        lea       ebx, DWORD PTR [eax*4]                        ;
        shl       eax, 5                                        ;
        add       esi, edx                                      ;
        sub       eax, ebx                                      ;
        mov       ebx, DWORD PTR [4+esp]                        ;
        sub       ecx, eax                                      ;
        imul      ebx, DWORD PTR [12+esp]                       ;
        sub       eax, ebx                                      ;
        add       ecx, eax                                      ;
        xor       eax, eax                                      ;
        add       ecx, ebx                                      ;
        sar       esi, 1                                        ;
        mov       DWORD PTR [24+esp], ecx                       ;
        mov       DWORD PTR [64+esp], edx                       ;
                                ; LOE eax esi edi
.B15.98:                        ; Preds .B15.103 .B15.205 .B15.97
        test      esi, esi                                      ;839.10
        jbe       .B15.207      ; Prob 10%                      ;839.10
                                ; LOE eax esi edi
.B15.99:                        ; Preds .B15.98
        mov       edx, DWORD PTR [24+esp]                       ;
        xor       ecx, ecx                                      ;
        mov       ebx, DWORD PTR [16+esp]                       ;
        mov       DWORD PTR [20+esp], edi                       ;
        add       edx, eax                                      ;
        mov       DWORD PTR [28+esp], eax                       ;
        add       ebx, eax                                      ;
        mov       edi, ecx                                      ;
        xor       eax, eax                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B15.100:                       ; Preds .B15.100 .B15.99
        inc       ecx                                           ;839.10
        mov       DWORD PTR [edi+ebx], eax                      ;839.10
        mov       DWORD PTR [28+edi+edx], eax                   ;839.10
        add       edi, 56                                       ;839.10
        cmp       ecx, esi                                      ;839.10
        jb        .B15.100      ; Prob 64%                      ;839.10
                                ; LOE eax edx ecx ebx esi edi
.B15.101:                       ; Preds .B15.100
        mov       eax, DWORD PTR [28+esp]                       ;
        lea       ebx, DWORD PTR [1+ecx+ecx]                    ;839.10
        mov       edi, DWORD PTR [20+esp]                       ;
                                ; LOE eax ebx esi edi
.B15.102:                       ; Preds .B15.101 .B15.207
        lea       edx, DWORD PTR [-1+ebx]                       ;839.10
        cmp       edx, DWORD PTR [64+esp]                       ;839.10
        jae       .B15.205      ; Prob 10%                      ;839.10
                                ; LOE eax ebx esi edi
.B15.103:                       ; Preds .B15.102
        inc       edi                                           ;839.10
        lea       edx, DWORD PTR [ebx*4]                        ;839.10
        shl       ebx, 5                                        ;839.10
        sub       ebx, edx                                      ;839.10
        add       ebx, DWORD PTR [24+esp]                       ;839.10
        mov       DWORD PTR [-28+ebx+eax], 0                    ;839.10
        add       eax, DWORD PTR [12+esp]                       ;839.10
        cmp       edi, DWORD PTR [8+esp]                        ;839.10
        jb        .B15.98       ; Prob 82%                      ;839.10
                                ; LOE eax esi edi
.B15.104:                       ; Preds .B15.103                ; Infreq
        mov       edx, DWORD PTR [64+esp]                       ;
        jmp       .B15.10       ; Prob 100%                     ;
                                ; LOE edx
.B15.106:                       ; Preds .B15.31                 ; Infreq
        cmp       DWORD PTR [28+esp], 4                         ;855.10
        jl        .B15.123      ; Prob 10%                      ;855.10
                                ; LOE ebx esi edi
.B15.107:                       ; Preds .B15.106                ; Infreq
        mov       eax, DWORD PTR [24+esp]                       ;855.10
        lea       edx, DWORD PTR [eax+ebx]                      ;855.10
        and       edx, 15                                       ;855.10
        je        .B15.110      ; Prob 50%                      ;855.10
                                ; LOE edx ebx esi edi
.B15.108:                       ; Preds .B15.107                ; Infreq
        test      dl, 3                                         ;855.10
        jne       .B15.123      ; Prob 10%                      ;855.10
                                ; LOE edx ebx esi edi
.B15.109:                       ; Preds .B15.108                ; Infreq
        neg       edx                                           ;855.10
        add       edx, 16                                       ;855.10
        shr       edx, 2                                        ;855.10
                                ; LOE edx ebx esi edi
.B15.110:                       ; Preds .B15.109 .B15.107       ; Infreq
        lea       eax, DWORD PTR [4+edx]                        ;855.10
        cmp       eax, DWORD PTR [28+esp]                       ;855.10
        jg        .B15.123      ; Prob 10%                      ;855.10
                                ; LOE edx ebx esi edi
.B15.111:                       ; Preds .B15.110                ; Infreq
        mov       eax, DWORD PTR [28+esp]                       ;855.10
        mov       ecx, eax                                      ;855.10
        sub       ecx, edx                                      ;855.10
        and       ecx, 3                                        ;855.10
        neg       ecx                                           ;855.10
        add       ecx, eax                                      ;855.10
        mov       eax, DWORD PTR [24+esp]                       ;
        test      edx, edx                                      ;855.10
        mov       DWORD PTR [esp], ecx                          ;855.10
        lea       ecx, DWORD PTR [eax+ebx]                      ;
        mov       DWORD PTR [4+esp], ecx                        ;
        jbe       .B15.115      ; Prob 10%                      ;855.10
                                ; LOE edx ecx ebx esi edi cl ch
.B15.112:                       ; Preds .B15.111                ; Infreq
        xor       eax, eax                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B15.113:                       ; Preds .B15.113 .B15.112       ; Infreq
        mov       DWORD PTR [ecx+eax*4], 0                      ;855.10
        inc       eax                                           ;855.10
        cmp       eax, edx                                      ;855.10
        jb        .B15.113      ; Prob 82%                      ;855.10
                                ; LOE eax edx ecx ebx esi edi
.B15.115:                       ; Preds .B15.113 .B15.111       ; Infreq
        mov       eax, DWORD PTR [esp]                          ;
        mov       ecx, DWORD PTR [4+esp]                        ;
        pxor      xmm0, xmm0                                    ;
                                ; LOE eax edx ecx ebx esi edi xmm0
.B15.116:                       ; Preds .B15.116 .B15.115       ; Infreq
        movdqa    XMMWORD PTR [ecx+edx*4], xmm0                 ;855.10
        add       edx, 4                                        ;855.10
        cmp       edx, eax                                      ;855.10
        jb        .B15.116      ; Prob 82%                      ;855.10
                                ; LOE eax edx ecx ebx esi edi xmm0
.B15.117:                       ; Preds .B15.116                ; Infreq
        mov       DWORD PTR [esp], eax                          ;
                                ; LOE eax ebx esi edi al ah
.B15.118:                       ; Preds .B15.117 .B15.123       ; Infreq
        cmp       eax, DWORD PTR [28+esp]                       ;855.10
        jae       .B15.33       ; Prob 10%                      ;855.10
                                ; LOE eax ebx esi edi al ah
.B15.119:                       ; Preds .B15.118                ; Infreq
        mov       edx, DWORD PTR [28+esp]                       ;
                                ; LOE eax edx ebx esi edi
.B15.120:                       ; Preds .B15.120 .B15.119       ; Infreq
        mov       DWORD PTR [esi+eax*4], 0                      ;855.10
        inc       eax                                           ;855.10
        cmp       eax, edx                                      ;855.10
        jb        .B15.120      ; Prob 82%                      ;855.10
                                ; LOE eax edx ebx esi edi
.B15.121:                       ; Preds .B15.120                ; Infreq
        mov       edx, DWORD PTR [16+esp]                       ;855.10
        inc       edx                                           ;855.10
        mov       eax, DWORD PTR [20+esp]                       ;855.10
        add       esi, eax                                      ;855.10
        add       edi, eax                                      ;855.10
        add       ebx, eax                                      ;855.10
        mov       DWORD PTR [16+esp], edx                       ;855.10
        cmp       edx, DWORD PTR [12+esp]                       ;855.10
        jb        .B15.31       ; Prob 82%                      ;855.10
        jmp       .B15.28       ; Prob 100%                     ;855.10
                                ; LOE ebx esi edi
.B15.123:                       ; Preds .B15.106 .B15.110 .B15.108 ; Infreq
        xor       eax, eax                                      ;855.10
        mov       DWORD PTR [esp], eax                          ;855.10
        jmp       .B15.118      ; Prob 100%                     ;855.10
                                ; LOE eax ebx esi edi al ah
.B15.129:                       ; Preds .B15.35                 ; Infreq
        mov       edx, 1                                        ;
        jmp       .B15.39       ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B15.131:                       ; Preds .B15.46                 ; Infreq
        inc       ebx                                           ;847.10
        mov       eax, DWORD PTR [12+esp]                       ;847.10
        add       edi, eax                                      ;847.10
        add       ecx, eax                                      ;847.10
        cmp       ebx, DWORD PTR [8+esp]                        ;847.10
        jb        .B15.42       ; Prob 82%                      ;847.10
                                ; LOE ecx ebx esi edi
.B15.132:                       ; Preds .B15.47 .B15.131        ; Infreq
        mov       edx, DWORD PTR [64+esp]                       ;
        jmp       .B15.18       ; Prob 100%                     ;
                                ; LOE edx
.B15.133:                       ; Preds .B15.42                 ; Infreq
        mov       eax, 1                                        ;
        jmp       .B15.46       ; Prob 100%                     ;
                                ; LOE eax ecx ebx esi edi
.B15.135:                       ; Preds .B15.53                 ; Infreq
        inc       ebx                                           ;846.10
        mov       eax, DWORD PTR [12+esp]                       ;846.10
        add       edi, eax                                      ;846.10
        add       ecx, eax                                      ;846.10
        cmp       ebx, DWORD PTR [8+esp]                        ;846.10
        jb        .B15.49       ; Prob 82%                      ;846.10
                                ; LOE ecx ebx esi edi
.B15.136:                       ; Preds .B15.54 .B15.135        ; Infreq
        mov       edx, DWORD PTR [64+esp]                       ;
        jmp       .B15.17       ; Prob 100%                     ;
                                ; LOE edx
.B15.137:                       ; Preds .B15.49                 ; Infreq
        mov       eax, 1                                        ;
        jmp       .B15.53       ; Prob 100%                     ;
                                ; LOE eax ecx ebx esi edi
.B15.139:                       ; Preds .B15.60                 ; Infreq
        inc       ebx                                           ;845.10
        mov       eax, DWORD PTR [12+esp]                       ;845.10
        add       edi, eax                                      ;845.10
        add       ecx, eax                                      ;845.10
        cmp       ebx, DWORD PTR [8+esp]                        ;845.10
        jb        .B15.56       ; Prob 82%                      ;845.10
                                ; LOE ecx ebx esi edi
.B15.140:                       ; Preds .B15.61 .B15.139        ; Infreq
        mov       edx, DWORD PTR [64+esp]                       ;
        jmp       .B15.16       ; Prob 100%                     ;
                                ; LOE edx
.B15.141:                       ; Preds .B15.56                 ; Infreq
        mov       eax, 1                                        ;
        jmp       .B15.60       ; Prob 100%                     ;
                                ; LOE eax ecx ebx esi edi
.B15.143:                       ; Preds .B15.67                 ; Infreq
        inc       edx                                           ;844.10
        mov       edi, DWORD PTR [12+esp]                       ;844.10
        add       ebx, edi                                      ;844.10
        add       esi, edi                                      ;844.10
        cmp       edx, DWORD PTR [8+esp]                        ;844.10
        jb        .B15.63       ; Prob 82%                      ;844.10
                                ; LOE edx ecx ebx esi
.B15.144:                       ; Preds .B15.68 .B15.143        ; Infreq
        mov       edx, DWORD PTR [64+esp]                       ;
        jmp       .B15.15       ; Prob 100%                     ;
                                ; LOE edx
.B15.145:                       ; Preds .B15.63                 ; Infreq
        mov       eax, 1                                        ;
        jmp       .B15.67       ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B15.147:                       ; Preds .B15.74                 ; Infreq
        inc       edx                                           ;843.10
        mov       edi, DWORD PTR [12+esp]                       ;843.10
        add       ebx, edi                                      ;843.10
        add       esi, edi                                      ;843.10
        cmp       edx, DWORD PTR [8+esp]                        ;843.10
        jb        .B15.70       ; Prob 82%                      ;843.10
                                ; LOE edx ecx ebx esi
.B15.148:                       ; Preds .B15.75 .B15.147        ; Infreq
        mov       edx, DWORD PTR [64+esp]                       ;
        jmp       .B15.14       ; Prob 100%                     ;
                                ; LOE edx
.B15.149:                       ; Preds .B15.70                 ; Infreq
        mov       eax, 1                                        ;
        jmp       .B15.74       ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B15.151:                       ; Preds .B15.81                 ; Infreq
        inc       ebx                                           ;842.10
        mov       eax, DWORD PTR [12+esp]                       ;842.10
        add       edi, eax                                      ;842.10
        add       ecx, eax                                      ;842.10
        cmp       ebx, DWORD PTR [8+esp]                        ;842.10
        jb        .B15.77       ; Prob 82%                      ;842.10
                                ; LOE ecx ebx esi edi
.B15.152:                       ; Preds .B15.82 .B15.151        ; Infreq
        mov       edx, DWORD PTR [64+esp]                       ;
        jmp       .B15.13       ; Prob 100%                     ;
                                ; LOE edx
.B15.153:                       ; Preds .B15.77                 ; Infreq
        mov       eax, 1                                        ;
        jmp       .B15.81       ; Prob 100%                     ;
                                ; LOE eax ecx ebx esi edi
.B15.155:                       ; Preds .B15.88                 ; Infreq
        inc       edx                                           ;841.10
        mov       edi, DWORD PTR [12+esp]                       ;841.10
        add       ebx, edi                                      ;841.10
        add       esi, edi                                      ;841.10
        cmp       edx, DWORD PTR [8+esp]                        ;841.10
        jb        .B15.84       ; Prob 82%                      ;841.10
                                ; LOE edx ecx ebx esi
.B15.156:                       ; Preds .B15.89 .B15.155        ; Infreq
        mov       edx, DWORD PTR [64+esp]                       ;
        jmp       .B15.12       ; Prob 100%                     ;
                                ; LOE edx
.B15.157:                       ; Preds .B15.84                 ; Infreq
        mov       eax, 1                                        ;
        jmp       .B15.88       ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx esi
.B15.159:                       ; Preds .B15.95                 ; Infreq
        inc       ebx                                           ;840.10
        mov       eax, DWORD PTR [12+esp]                       ;840.10
        add       edi, eax                                      ;840.10
        add       ecx, eax                                      ;840.10
        cmp       ebx, DWORD PTR [8+esp]                        ;840.10
        jb        .B15.91       ; Prob 82%                      ;840.10
                                ; LOE ecx ebx esi edi
.B15.160:                       ; Preds .B15.96 .B15.159        ; Infreq
        mov       edx, DWORD PTR [64+esp]                       ;
        jmp       .B15.11       ; Prob 100%                     ;
                                ; LOE edx
.B15.161:                       ; Preds .B15.91                 ; Infreq
        mov       eax, 1                                        ;
        jmp       .B15.95       ; Prob 100%                     ;
                                ; LOE eax ecx ebx esi edi
.B15.162:                       ; Preds .B15.28                 ; Infreq
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFARCS] ;858.10
        xor       eax, eax                                      ;858.10
        test      ecx, ecx                                      ;858.10
        lea       ebx, DWORD PTR [24+esp]                       ;858.10
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_MAXMOVE] ;858.10
        cmovle    ecx, eax                                      ;858.10
        test      edx, edx                                      ;858.10
        push      8                                             ;858.10
        cmovle    edx, eax                                      ;858.10
        push      edx                                           ;858.10
        push      ecx                                           ;858.10
        push      3                                             ;858.10
        push      ebx                                           ;858.10
        call      _for_check_mult_overflow                      ;858.10
                                ; LOE eax
.B15.163:                       ; Preds .B15.162                ; Infreq
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+12] ;858.10
        and       eax, 1                                        ;858.10
        and       edx, 1                                        ;858.10
        add       edx, edx                                      ;858.10
        shl       eax, 4                                        ;858.10
        or        edx, 1                                        ;858.10
        or        edx, eax                                      ;858.10
        or        edx, 262144                                   ;858.10
        push      edx                                           ;858.10
        push      OFFSET FLAT: _DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE ;858.10
        push      DWORD PTR [52+esp]                            ;858.10
        call      _for_alloc_allocatable                        ;858.10
                                ; LOE eax
.B15.222:                       ; Preds .B15.163                ; Infreq
        add       esp, 32                                       ;858.10
        mov       ebx, eax                                      ;858.10
                                ; LOE ebx
.B15.164:                       ; Preds .B15.222                ; Infreq
        test      ebx, ebx                                      ;858.10
        je        .B15.167      ; Prob 50%                      ;858.10
                                ; LOE ebx
.B15.165:                       ; Preds .B15.164                ; Infreq
        mov       DWORD PTR [32+esp], 0                         ;860.14
        lea       edx, DWORD PTR [32+esp]                       ;860.14
        mov       DWORD PTR [8+esp], 63                         ;860.14
        lea       eax, DWORD PTR [8+esp]                        ;860.14
        mov       DWORD PTR [12+esp], OFFSET FLAT: __STRLITPACK_0 ;860.14
        push      32                                            ;860.14
        push      eax                                           ;860.14
        push      OFFSET FLAT: __STRLITPACK_224.0.15            ;860.14
        push      -2088435968                                   ;860.14
        push      911                                           ;860.14
        push      edx                                           ;860.14
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], ebx ;858.10
        call      _for_write_seq_lis                            ;860.14
                                ; LOE
.B15.166:                       ; Preds .B15.165                ; Infreq
        push      32                                            ;861.14
        xor       eax, eax                                      ;861.14
        push      eax                                           ;861.14
        push      eax                                           ;861.14
        push      -2088435968                                   ;861.14
        push      eax                                           ;861.14
        push      OFFSET FLAT: __STRLITPACK_225                 ;861.14
        call      _for_stop_core                                ;861.14
                                ; LOE
.B15.223:                       ; Preds .B15.166                ; Infreq
        add       esp, 48                                       ;861.14
        jmp       .B15.169      ; Prob 100%                     ;861.14
                                ; LOE
.B15.167:                       ; Preds .B15.164                ; Infreq
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFARCS] ;858.10
        xor       eax, eax                                      ;858.10
        test      ecx, ecx                                      ;858.10
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_MAXMOVE] ;858.10
        cmovle    ecx, eax                                      ;858.10
        mov       esi, 8                                        ;858.10
        mov       edi, 1                                        ;858.10
        test      edx, edx                                      ;858.10
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+32], edi ;858.10
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+44], edi ;858.10
        cmovle    edx, eax                                      ;858.10
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+8], eax ;858.10
        lea       edi, DWORD PTR [esp]                          ;858.10
        push      esi                                           ;858.10
        push      edx                                           ;858.10
        push      ecx                                           ;858.10
        push      3                                             ;858.10
        push      edi                                           ;858.10
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+12], 133 ;858.10
        lea       eax, DWORD PTR [ecx*8]                        ;858.10
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+4], esi ;858.10
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+16], 2 ;858.10
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+24], ecx ;858.10
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+36], edx ;858.10
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+28], esi ;858.10
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+40], eax ;858.10
        call      _for_check_mult_overflow                      ;858.10
                                ; LOE ebx
.B15.224:                       ; Preds .B15.167                ; Infreq
        add       esp, 20                                       ;858.10
                                ; LOE ebx
.B15.168:                       ; Preds .B15.224                ; Infreq
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], ebx ;858.10
                                ; LOE
.B15.169:                       ; Preds .B15.223 .B15.168       ; Infreq
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+36] ;863.10
        test      eax, eax                                      ;863.10
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+44] ;863.10
        mov       DWORD PTR [28+esp], eax                       ;863.10
        jle       .B15.29       ; Prob 10%                      ;863.10
                                ; LOE ecx
.B15.170:                       ; Preds .B15.169                ; Infreq
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+40] ;865.10
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+24] ;863.10
        test      edx, edx                                      ;863.10
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+32] ;863.10
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE] ;865.10
        mov       DWORD PTR [64+esp], eax                       ;865.10
        jg        .B15.179      ; Prob 50%                      ;863.10
                                ; LOE edx ecx ebx esi
.B15.171:                       ; Preds .B15.186 .B15.201 .B15.170 ; Infreq
        test      edx, edx                                      ;864.10
        jle       .B15.29       ; Prob 50%                      ;864.10
                                ; LOE edx ecx ebx esi
.B15.172:                       ; Preds .B15.171                ; Infreq
        mov       edi, edx                                      ;
        xor       eax, eax                                      ;
        shr       edi, 31                                       ;
        add       edi, edx                                      ;
        sar       edi, 1                                        ;
        mov       DWORD PTR [20+esp], edi                       ;
        mov       edi, ecx                                      ;
        imul      edi, DWORD PTR [64+esp]                       ;
        mov       DWORD PTR [80+esp], edx                       ;
        lea       edx, DWORD PTR [ebx*8]                        ;
        mov       DWORD PTR [68+esp], esi                       ;
        sub       esi, edx                                      ;
        sub       edx, edi                                      ;
        add       esi, edx                                      ;
        mov       DWORD PTR [76+esp], eax                       ;
        add       esi, edi                                      ;
        mov       DWORD PTR [72+esp], esi                       ;
        mov       edi, esi                                      ;
        mov       DWORD PTR [16+esp], ebx                       ;
        mov       DWORD PTR [4+esp], ecx                        ;
        mov       ebx, DWORD PTR [20+esp]                       ;
        mov       ecx, eax                                      ;
                                ; LOE eax ecx ebx edi
.B15.173:                       ; Preds .B15.178 .B15.187 .B15.172 ; Infreq
        test      ebx, ebx                                      ;864.10
        jbe       .B15.198      ; Prob 10%                      ;864.10
                                ; LOE eax ecx ebx edi
.B15.174:                       ; Preds .B15.173                ; Infreq
        xor       edx, edx                                      ;
        mov       DWORD PTR [76+esp], ecx                       ;
        xor       ecx, ecx                                      ;
                                ; LOE eax edx ecx ebx edi
.B15.175:                       ; Preds .B15.175 .B15.174       ; Infreq
        mov       esi, edx                                      ;864.10
        inc       edx                                           ;864.10
        shl       esi, 4                                        ;864.10
        cmp       edx, ebx                                      ;864.10
        mov       BYTE PTR [4+esi+edi], cl                      ;864.10
        mov       BYTE PTR [12+esi+edi], cl                     ;864.10
        jb        .B15.175      ; Prob 64%                      ;864.10
                                ; LOE eax edx ecx ebx edi
.B15.176:                       ; Preds .B15.175                ; Infreq
        mov       ecx, DWORD PTR [76+esp]                       ;
        lea       edx, DWORD PTR [1+edx+edx]                    ;864.10
                                ; LOE eax edx ecx ebx edi
.B15.177:                       ; Preds .B15.176 .B15.198       ; Infreq
        lea       esi, DWORD PTR [-1+edx]                       ;864.10
        cmp       esi, DWORD PTR [80+esp]                       ;864.10
        jae       .B15.187      ; Prob 10%                      ;864.10
                                ; LOE eax edx ecx ebx edi
.B15.178:                       ; Preds .B15.177                ; Infreq
        mov       esi, DWORD PTR [72+esp]                       ;864.10
        inc       ecx                                           ;864.10
        lea       edx, DWORD PTR [esi+edx*8]                    ;864.10
        mov       BYTE PTR [-4+edx+eax], 0                      ;864.10
        mov       edx, DWORD PTR [64+esp]                       ;864.10
        add       edi, edx                                      ;864.10
        add       eax, edx                                      ;864.10
        cmp       ecx, DWORD PTR [28+esp]                       ;864.10
        jb        .B15.173      ; Prob 82%                      ;864.10
        jmp       .B15.188      ; Prob 100%                     ;864.10
                                ; LOE eax ecx ebx edi
.B15.179:                       ; Preds .B15.170                ; Infreq
        mov       edi, edx                                      ;
        xor       eax, eax                                      ;
        shr       edi, 31                                       ;
        add       edi, edx                                      ;
        sar       edi, 1                                        ;
        mov       DWORD PTR [68+esp], esi                       ;
        mov       DWORD PTR [20+esp], edi                       ;
        mov       edi, esi                                      ;
        mov       esi, ecx                                      ;
        imul      esi, DWORD PTR [64+esp]                       ;
        mov       DWORD PTR [80+esp], edx                       ;
        lea       edx, DWORD PTR [ebx*8]                        ;
        sub       edi, edx                                      ;
        sub       edx, esi                                      ;
        add       edi, edx                                      ;
        mov       DWORD PTR [76+esp], eax                       ;
        add       edi, esi                                      ;
        mov       DWORD PTR [72+esp], edi                       ;
        mov       esi, DWORD PTR [68+esp]                       ;
        mov       edx, DWORD PTR [20+esp]                       ;
        mov       edi, eax                                      ;
        mov       DWORD PTR [16+esp], ebx                       ;
        mov       DWORD PTR [4+esp], ecx                        ;
                                ; LOE eax edx edi
.B15.180:                       ; Preds .B15.185 .B15.200 .B15.179 ; Infreq
        test      edx, edx                                      ;863.10
        jbe       .B15.202      ; Prob 10%                      ;863.10
                                ; LOE eax edx edi
.B15.181:                       ; Preds .B15.180                ; Infreq
        mov       ecx, DWORD PTR [72+esp]                       ;
        xor       ebx, ebx                                      ;
        xor       esi, esi                                      ;
        mov       DWORD PTR [76+esp], edi                       ;
        lea       ebx, DWORD PTR [ecx+eax]                      ;
        mov       ecx, DWORD PTR [68+esp]                       ;
        mov       edi, esi                                      ;
        mov       DWORD PTR [20+esp], eax                       ;
        add       ecx, eax                                      ;
        xor       eax, eax                                      ;
                                ; LOE eax edx ecx ebx esi edi
.B15.182:                       ; Preds .B15.182 .B15.181       ; Infreq
        inc       esi                                           ;863.10
        mov       DWORD PTR [edi+ecx], eax                      ;863.10
        mov       DWORD PTR [8+edi+ebx], eax                    ;863.10
        add       edi, 16                                       ;863.10
        cmp       esi, edx                                      ;863.10
        jb        .B15.182      ; Prob 64%                      ;863.10
                                ; LOE eax edx ecx ebx esi edi
.B15.183:                       ; Preds .B15.182                ; Infreq
        mov       eax, DWORD PTR [20+esp]                       ;
        lea       ebx, DWORD PTR [1+esi+esi]                    ;863.10
        mov       edi, DWORD PTR [76+esp]                       ;
                                ; LOE eax edx ebx edi
.B15.184:                       ; Preds .B15.183 .B15.202       ; Infreq
        lea       ecx, DWORD PTR [-1+ebx]                       ;863.10
        cmp       ecx, DWORD PTR [80+esp]                       ;863.10
        jae       .B15.200      ; Prob 10%                      ;863.10
                                ; LOE eax edx ebx edi
.B15.185:                       ; Preds .B15.184                ; Infreq
        mov       ecx, DWORD PTR [72+esp]                       ;863.10
        inc       edi                                           ;863.10
        lea       ebx, DWORD PTR [ecx+ebx*8]                    ;863.10
        mov       DWORD PTR [-8+ebx+eax], 0                     ;863.10
        add       eax, DWORD PTR [64+esp]                       ;863.10
        cmp       edi, DWORD PTR [28+esp]                       ;863.10
        jb        .B15.180      ; Prob 82%                      ;863.10
                                ; LOE eax edx edi
.B15.186:                       ; Preds .B15.185                ; Infreq
        mov       esi, DWORD PTR [68+esp]                       ;
        mov       edx, DWORD PTR [80+esp]                       ;
        mov       ebx, DWORD PTR [16+esp]                       ;
        mov       ecx, DWORD PTR [4+esp]                        ;
        jmp       .B15.171      ; Prob 100%                     ;
                                ; LOE edx ecx ebx esi
.B15.187:                       ; Preds .B15.177                ; Infreq
        inc       ecx                                           ;864.10
        mov       edx, DWORD PTR [64+esp]                       ;864.10
        add       edi, edx                                      ;864.10
        add       eax, edx                                      ;864.10
        cmp       ecx, DWORD PTR [28+esp]                       ;864.10
        jb        .B15.173      ; Prob 82%                      ;864.10
                                ; LOE eax ecx ebx edi
.B15.188:                       ; Preds .B15.178 .B15.187       ; Infreq
        mov       esi, DWORD PTR [68+esp]                       ;
        mov       edx, DWORD PTR [80+esp]                       ;
        mov       ebx, DWORD PTR [16+esp]                       ;
        mov       ecx, DWORD PTR [4+esp]                        ;
                                ; LOE edx ecx ebx esi dl cl bl dh ch bh
.B15.189:                       ; Preds .B15.188                ; Infreq
        imul      ecx, DWORD PTR [64+esp]                       ;
        mov       edi, edx                                      ;
        shl       ebx, 3                                        ;
        xor       eax, eax                                      ;
        sub       esi, ebx                                      ;
        sub       ebx, ecx                                      ;
        shr       edi, 31                                       ;
        add       esi, ebx                                      ;
        add       edi, edx                                      ;
        add       esi, ecx                                      ;
        sar       edi, 1                                        ;
        mov       ebx, esi                                      ;
        mov       DWORD PTR [68+esp], esi                       ;
        xor       ecx, ecx                                      ;
        mov       DWORD PTR [80+esp], edx                       ;
                                ; LOE eax ecx ebx edi
.B15.190:                       ; Preds .B15.195 .B15.203 .B15.189 ; Infreq
        test      edi, edi                                      ;865.10
        jbe       .B15.197      ; Prob 10%                      ;865.10
                                ; LOE eax ecx ebx edi
.B15.191:                       ; Preds .B15.190                ; Infreq
        xor       edx, edx                                      ;
        mov       DWORD PTR [4+esp], eax                        ;
        xor       eax, eax                                      ;
                                ; LOE eax edx ecx ebx edi
.B15.192:                       ; Preds .B15.192 .B15.191       ; Infreq
        mov       esi, edx                                      ;865.10
        inc       edx                                           ;865.10
        shl       esi, 4                                        ;865.10
        cmp       edx, edi                                      ;865.10
        mov       BYTE PTR [5+esi+ebx], al                      ;865.10
        mov       BYTE PTR [13+esi+ebx], al                     ;865.10
        jb        .B15.192      ; Prob 64%                      ;865.10
                                ; LOE eax edx ecx ebx edi
.B15.193:                       ; Preds .B15.192                ; Infreq
        mov       eax, DWORD PTR [4+esp]                        ;
        lea       edx, DWORD PTR [1+edx+edx]                    ;865.10
                                ; LOE eax edx ecx ebx edi
.B15.194:                       ; Preds .B15.193 .B15.197       ; Infreq
        lea       esi, DWORD PTR [-1+edx]                       ;865.10
        cmp       esi, DWORD PTR [80+esp]                       ;865.10
        jae       .B15.203      ; Prob 10%                      ;865.10
                                ; LOE eax edx ecx ebx edi
.B15.195:                       ; Preds .B15.194                ; Infreq
        mov       esi, DWORD PTR [68+esp]                       ;865.10
        inc       eax                                           ;865.10
        lea       edx, DWORD PTR [esi+edx*8]                    ;865.10
        mov       BYTE PTR [-3+edx+ecx], 0                      ;865.10
        mov       edx, DWORD PTR [64+esp]                       ;865.10
        add       ebx, edx                                      ;865.10
        add       ecx, edx                                      ;865.10
        cmp       eax, DWORD PTR [28+esp]                       ;865.10
        jb        .B15.190      ; Prob 82%                      ;865.10
        jmp       .B15.29       ; Prob 100%                     ;865.10
                                ; LOE eax ecx ebx edi
.B15.197:                       ; Preds .B15.190                ; Infreq
        mov       edx, 1                                        ;
        jmp       .B15.194      ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx edi
.B15.198:                       ; Preds .B15.173                ; Infreq
        mov       edx, 1                                        ;
        jmp       .B15.177      ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx edi
.B15.200:                       ; Preds .B15.184                ; Infreq
        inc       edi                                           ;863.10
        add       eax, DWORD PTR [64+esp]                       ;863.10
        cmp       edi, DWORD PTR [28+esp]                       ;863.10
        jb        .B15.180      ; Prob 82%                      ;863.10
                                ; LOE eax edx edi
.B15.201:                       ; Preds .B15.200                ; Infreq
        mov       esi, DWORD PTR [68+esp]                       ;
        mov       edx, DWORD PTR [80+esp]                       ;
        mov       ebx, DWORD PTR [16+esp]                       ;
        mov       ecx, DWORD PTR [4+esp]                        ;
        jmp       .B15.171      ; Prob 100%                     ;
                                ; LOE edx ecx ebx esi
.B15.202:                       ; Preds .B15.180                ; Infreq
        mov       ebx, 1                                        ;
        jmp       .B15.184      ; Prob 100%                     ;
                                ; LOE eax edx ebx edi
.B15.203:                       ; Preds .B15.194                ; Infreq
        inc       eax                                           ;865.10
        mov       edx, DWORD PTR [64+esp]                       ;865.10
        add       ebx, edx                                      ;865.10
        add       ecx, edx                                      ;865.10
        cmp       eax, DWORD PTR [28+esp]                       ;865.10
        jb        .B15.190      ; Prob 82%                      ;865.10
        jmp       .B15.29       ; Prob 100%                     ;865.10
                                ; LOE eax ecx ebx edi
.B15.205:                       ; Preds .B15.102                ; Infreq
        inc       edi                                           ;839.10
        add       eax, DWORD PTR [12+esp]                       ;839.10
        cmp       edi, DWORD PTR [8+esp]                        ;839.10
        jb        .B15.98       ; Prob 82%                      ;839.10
                                ; LOE eax esi edi
.B15.206:                       ; Preds .B15.205                ; Infreq
        mov       edx, DWORD PTR [64+esp]                       ;
        jmp       .B15.10       ; Prob 100%                     ;
                                ; LOE edx
.B15.207:                       ; Preds .B15.98                 ; Infreq
        mov       ebx, 1                                        ;
        jmp       .B15.102      ; Prob 100%                     ;
                                ; LOE eax ebx esi edi
.B15.208:                       ; Preds .B15.39                 ; Infreq
        inc       esi                                           ;848.10
        mov       edx, DWORD PTR [12+esp]                       ;848.10
        add       ebx, edx                                      ;848.10
        add       eax, edx                                      ;848.10
        cmp       esi, DWORD PTR [8+esp]                        ;848.10
        jb        .B15.35       ; Prob 82%                      ;848.10
        jmp       .B15.19       ; Prob 100%                     ;848.10
        ALIGN     16
                                ; LOE eax ecx ebx esi
; mark_end;
_DYNUST_NETWORK_MODULE_mp_ALLOCATE_DYNUST_NETWORK_ARCMOVE ENDP
_TEXT	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_220.0.15	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_222.0.15	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_224.0.15	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_NETWORK_MODULE_mp_ALLOCATE_DYNUST_NETWORK_ARCMOVE
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_NETWORK_MODULE_mp_DEALLOCATE_DYNUST_NETWORK_ARC
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_NETWORK_MODULE_mp_DEALLOCATE_DYNUST_NETWORK_ARC
_DYNUST_NETWORK_MODULE_mp_DEALLOCATE_DYNUST_NETWORK_ARC	PROC NEAR 
.B16.1:                         ; Preds .B16.0
        push      esi                                           ;870.20
        push      edi                                           ;870.20
        push      ebx                                           ;870.20
        push      ebp                                           ;870.20
        sub       esp, 48                                       ;870.20
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFARCS] ;874.14
        test      ecx, ecx                                      ;874.14
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;904.13
        jle       .B16.42       ; Prob 28%                      ;874.14
                                ; LOE eax ecx
.B16.2:                         ; Preds .B16.1
        imul      ebp, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32], -900 ;
        mov       edi, 1                                        ;
        mov       ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_SIMPERAGG] ;881.17
        add       ebp, eax                                      ;
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_TDSPSTEP] ;881.17
        cmp       ebx, esi                                      ;881.17
        mov       edx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_AGGINT] ;875.17
        cmovge    esi, ebx                                      ;881.17
        mov       ebx, 900                                      ;
        mov       DWORD PTR [28+esp], edi                       ;
        mov       DWORD PTR [32+esp], esi                       ;
        mov       DWORD PTR [40+esp], edx                       ;
        mov       DWORD PTR [esp], eax                          ;
        mov       DWORD PTR [36+esp], ecx                       ;
                                ; LOE ebx ebp
.B16.3:                         ; Preds .B16.40 .B16.2
        mov       eax, DWORD PTR [156+ebx+ebp]                  ;888.17
        mov       edx, DWORD PTR [120+ebx+ebp]                  ;887.17
        mov       DWORD PTR [24+esp], eax                       ;888.17
        mov       DWORD PTR [20+esp], edx                       ;887.17
        mov       eax, DWORD PTR [48+ebx+ebp]                   ;885.17
        cmp       DWORD PTR [40+esp], 0                         ;875.17
        jle       .B16.16       ; Prob 2%                       ;875.17
                                ; LOE eax ebx ebp
.B16.4:                         ; Preds .B16.3
        imul      ecx, DWORD PTR [80+ebx+ebp], -40              ;
        mov       edx, 1                                        ;
        imul      edi, DWORD PTR [188+ebx+ebp], -40             ;
        imul      esi, DWORD PTR [152+ebx+ebp], -40             ;
        add       ecx, eax                                      ;
        add       edi, DWORD PTR [24+esp]                       ;
        add       esi, DWORD PTR [20+esp]                       ;
        mov       DWORD PTR [44+esp], ecx                       ;
        mov       DWORD PTR [4+esp], eax                        ;
        mov       DWORD PTR [8+esp], ebx                        ;
        mov       ebx, edx                                      ;
        mov       DWORD PTR [12+esp], ebp                       ;
                                ; LOE ebx esi edi
.B16.5:                         ; Preds .B16.14 .B16.4
        mov       eax, DWORD PTR [44+esp]                       ;876.25
        lea       ebp, DWORD PTR [ebx+ebx*4]                    ;876.83
        movsx     ecx, WORD PTR [36+eax+ebp*8]                  ;876.25
        test      ecx, ecx                                      ;876.83
        jle       .B16.8        ; Prob 79%                      ;876.83
                                ; LOE eax ebx ebp esi edi al ah
.B16.6:                         ; Preds .B16.5
        mov       edx, eax                                      ;876.100
        mov       eax, DWORD PTR [12+edx+ebp*8]                 ;876.100
        mov       ecx, eax                                      ;876.89
        shr       ecx, 1                                        ;876.89
        and       eax, 1                                        ;876.89
        and       ecx, 1                                        ;876.89
        add       eax, eax                                      ;876.89
        shl       ecx, 2                                        ;876.89
        or        ecx, eax                                      ;876.89
        or        ecx, 262144                                   ;876.89
        push      ecx                                           ;876.89
        push      DWORD PTR [edx+ebp*8]                         ;876.89
        call      _for_dealloc_allocatable                      ;876.89
                                ; LOE ebx ebp esi edi
.B16.111:                       ; Preds .B16.6
        add       esp, 8                                        ;876.89
                                ; LOE ebx ebp esi edi
.B16.7:                         ; Preds .B16.111
        mov       eax, DWORD PTR [44+esp]                       ;876.89
        mov       DWORD PTR [eax+ebp*8], 0                      ;876.89
        and       DWORD PTR [12+eax+ebp*8], -2                  ;876.89
                                ; LOE ebx ebp esi edi
.B16.8:                         ; Preds .B16.5 .B16.7
        movsx     eax, WORD PTR [36+esi+ebp*8]                  ;877.25
        test      eax, eax                                      ;877.83
        jle       .B16.11       ; Prob 79%                      ;877.83
                                ; LOE ebx ebp esi edi
.B16.9:                         ; Preds .B16.8
        mov       eax, DWORD PTR [12+esi+ebp*8]                 ;877.100
        mov       ecx, eax                                      ;877.89
        shr       ecx, 1                                        ;877.89
        and       eax, 1                                        ;877.89
        and       ecx, 1                                        ;877.89
        add       eax, eax                                      ;877.89
        shl       ecx, 2                                        ;877.89
        or        ecx, eax                                      ;877.89
        or        ecx, 262144                                   ;877.89
        push      ecx                                           ;877.89
        push      DWORD PTR [esi+ebp*8]                         ;877.89
        call      _for_dealloc_allocatable                      ;877.89
                                ; LOE ebx ebp esi edi
.B16.112:                       ; Preds .B16.9
        add       esp, 8                                        ;877.89
                                ; LOE ebx ebp esi edi
.B16.10:                        ; Preds .B16.112
        mov       DWORD PTR [esi+ebp*8], 0                      ;877.89
        and       DWORD PTR [12+esi+ebp*8], -2                  ;877.89
                                ; LOE ebx ebp esi edi
.B16.11:                        ; Preds .B16.8 .B16.10
        movsx     eax, WORD PTR [36+edi+ebp*8]                  ;878.25
        test      eax, eax                                      ;878.75
        jle       .B16.14       ; Prob 79%                      ;878.75
                                ; LOE ebx ebp esi edi
.B16.12:                        ; Preds .B16.11
        mov       eax, DWORD PTR [12+edi+ebp*8]                 ;878.92
        mov       ecx, eax                                      ;878.81
        shr       ecx, 1                                        ;878.81
        and       eax, 1                                        ;878.81
        and       ecx, 1                                        ;878.81
        add       eax, eax                                      ;878.81
        shl       ecx, 2                                        ;878.81
        or        ecx, eax                                      ;878.81
        or        ecx, 262144                                   ;878.81
        push      ecx                                           ;878.81
        push      DWORD PTR [edi+ebp*8]                         ;878.81
        call      _for_dealloc_allocatable                      ;878.81
                                ; LOE ebx ebp esi edi
.B16.113:                       ; Preds .B16.12
        add       esp, 8                                        ;878.81
                                ; LOE ebx ebp esi edi
.B16.13:                        ; Preds .B16.113
        mov       DWORD PTR [edi+ebp*8], 0                      ;878.81
        and       DWORD PTR [12+edi+ebp*8], -2                  ;878.81
                                ; LOE ebx esi edi
.B16.14:                        ; Preds .B16.11 .B16.13
        inc       ebx                                           ;879.17
        cmp       ebx, DWORD PTR [40+esp]                       ;879.17
        jle       .B16.5        ; Prob 82%                      ;879.17
                                ; LOE ebx esi edi
.B16.15:                        ; Preds .B16.14
        mov       eax, DWORD PTR [4+esp]                        ;
        mov       ebx, DWORD PTR [8+esp]                        ;
        mov       ebp, DWORD PTR [12+esp]                       ;
                                ; LOE eax ebx ebp
.B16.16:                        ; Preds .B16.3 .B16.15
        cmp       DWORD PTR [32+esp], 0                         ;881.17
        mov       esi, DWORD PTR [84+ebx+ebp]                   ;886.17
        jle       .B16.23       ; Prob 2%                       ;881.17
                                ; LOE eax ebx ebp esi
.B16.17:                        ; Preds .B16.16
        imul      edi, DWORD PTR [116+ebx+ebp], -40             ;
        mov       edx, 1                                        ;
        mov       DWORD PTR [12+esp], ebp                       ;
        add       edi, esi                                      ;
        mov       DWORD PTR [16+esp], esi                       ;
        mov       DWORD PTR [4+esp], eax                        ;
        mov       DWORD PTR [8+esp], ebx                        ;
        mov       ebx, edx                                      ;
        mov       ebp, DWORD PTR [32+esp]                       ;
                                ; LOE ebx ebp edi
.B16.18:                        ; Preds .B16.21 .B16.17
        lea       esi, DWORD PTR [ebx+ebx*4]                    ;882.73
        movsx     eax, WORD PTR [36+edi+esi*8]                  ;882.25
        test      eax, eax                                      ;882.73
        jle       .B16.21       ; Prob 79%                      ;882.73
                                ; LOE ebx ebp esi edi
.B16.19:                        ; Preds .B16.18
        mov       eax, DWORD PTR [12+edi+esi*8]                 ;882.90
        mov       ecx, eax                                      ;882.79
        shr       ecx, 1                                        ;882.79
        and       eax, 1                                        ;882.79
        and       ecx, 1                                        ;882.79
        add       eax, eax                                      ;882.79
        shl       ecx, 2                                        ;882.79
        or        ecx, eax                                      ;882.79
        or        ecx, 262144                                   ;882.79
        push      ecx                                           ;882.79
        push      DWORD PTR [edi+esi*8]                         ;882.79
        call      _for_dealloc_allocatable                      ;882.79
                                ; LOE ebx ebp esi edi
.B16.114:                       ; Preds .B16.19
        add       esp, 8                                        ;882.79
                                ; LOE ebx ebp esi edi
.B16.20:                        ; Preds .B16.114
        mov       DWORD PTR [edi+esi*8], 0                      ;882.79
        and       DWORD PTR [12+edi+esi*8], -2                  ;882.79
                                ; LOE ebx ebp edi
.B16.21:                        ; Preds .B16.18 .B16.20
        inc       ebx                                           ;883.17
        cmp       ebx, ebp                                      ;883.17
        jle       .B16.18       ; Prob 82%                      ;883.17
                                ; LOE ebx ebp edi
.B16.22:                        ; Preds .B16.21
        mov       esi, DWORD PTR [16+esp]                       ;
        mov       eax, DWORD PTR [4+esp]                        ;
        mov       ebx, DWORD PTR [8+esp]                        ;
        mov       ebp, DWORD PTR [12+esp]                       ;
                                ; LOE eax ebx ebp esi
.B16.23:                        ; Preds .B16.16 .B16.22
        mov       edx, DWORD PTR [60+ebx+ebp]                   ;885.28
        mov       ecx, edx                                      ;885.17
        shr       ecx, 1                                        ;885.17
        and       edx, 1                                        ;885.17
        and       ecx, 1                                        ;885.17
        add       edx, edx                                      ;885.17
        shl       ecx, 2                                        ;885.17
        or        ecx, 1                                        ;885.17
        or        ecx, edx                                      ;885.17
        or        ecx, 262144                                   ;885.17
        push      ecx                                           ;885.17
        push      eax                                           ;885.17
        call      _for_dealloc_allocatable                      ;885.17
                                ; LOE eax ebx ebp esi
.B16.24:                        ; Preds .B16.23
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;885.17
        mov       eax, DWORD PTR [96+ebx+ebp]                   ;886.28
        mov       edx, eax                                      ;886.17
        shr       edx, 1                                        ;886.17
        and       eax, 1                                        ;886.17
        and       edx, 1                                        ;886.17
        add       eax, eax                                      ;886.17
        shl       edx, 2                                        ;886.17
        or        edx, 1                                        ;886.17
        or        edx, eax                                      ;886.17
        or        edx, 262144                                   ;886.17
        push      edx                                           ;886.17
        push      esi                                           ;886.17
        mov       DWORD PTR [48+ebp+ebx], 0                     ;885.17
        and       DWORD PTR [60+ebp+ebx], -2                    ;885.17
        call      _for_dealloc_allocatable                      ;886.17
                                ; LOE eax ebx ebp
.B16.25:                        ; Preds .B16.24
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;886.17
        mov       eax, DWORD PTR [132+ebx+ebp]                  ;887.28
        mov       edx, eax                                      ;887.17
        shr       edx, 1                                        ;887.17
        and       eax, 1                                        ;887.17
        and       edx, 1                                        ;887.17
        add       eax, eax                                      ;887.17
        shl       edx, 2                                        ;887.17
        or        edx, 1                                        ;887.17
        or        edx, eax                                      ;887.17
        or        edx, 262144                                   ;887.17
        mov       DWORD PTR [84+ebp+ebx], 0                     ;886.17
        and       DWORD PTR [96+ebp+ebx], -2                    ;886.17
        push      edx                                           ;887.17
        push      DWORD PTR [40+esp]                            ;887.17
        call      _for_dealloc_allocatable                      ;887.17
                                ; LOE eax ebx ebp
.B16.26:                        ; Preds .B16.25
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;887.17
        mov       eax, DWORD PTR [168+ebx+ebp]                  ;888.28
        mov       edx, eax                                      ;888.17
        shr       edx, 1                                        ;888.17
        and       eax, 1                                        ;888.17
        and       edx, 1                                        ;888.17
        add       eax, eax                                      ;888.17
        shl       edx, 2                                        ;888.17
        or        edx, 1                                        ;888.17
        or        edx, eax                                      ;888.17
        or        edx, 262144                                   ;888.17
        mov       DWORD PTR [120+ebp+ebx], 0                    ;887.17
        and       DWORD PTR [132+ebp+ebx], -2                   ;887.17
        push      edx                                           ;888.17
        push      DWORD PTR [52+esp]                            ;888.17
        call      _for_dealloc_allocatable                      ;888.17
                                ; LOE eax ebx ebp
.B16.27:                        ; Preds .B16.26
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;888.17
        mov       eax, DWORD PTR [204+ebx+ebp]                  ;889.28
        mov       edx, eax                                      ;889.17
        shr       edx, 1                                        ;889.17
        and       eax, 1                                        ;889.17
        and       edx, 1                                        ;889.17
        add       eax, eax                                      ;889.17
        shl       edx, 2                                        ;889.17
        or        edx, 1                                        ;889.17
        or        edx, eax                                      ;889.17
        or        edx, 262144                                   ;889.17
        push      edx                                           ;889.17
        push      DWORD PTR [192+ebx+ebp]                       ;889.17
        mov       DWORD PTR [156+ebp+ebx], 0                    ;888.17
        and       DWORD PTR [168+ebp+ebx], -2                   ;888.17
        call      _for_dealloc_allocatable                      ;889.17
                                ; LOE eax ebx ebp
.B16.28:                        ; Preds .B16.27
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;889.17
        mov       eax, DWORD PTR [240+ebx+ebp]                  ;890.28
        mov       edx, eax                                      ;890.17
        shr       edx, 1                                        ;890.17
        and       eax, 1                                        ;890.17
        and       edx, 1                                        ;890.17
        add       eax, eax                                      ;890.17
        shl       edx, 2                                        ;890.17
        or        edx, 1                                        ;890.17
        or        edx, eax                                      ;890.17
        or        edx, 262144                                   ;890.17
        push      edx                                           ;890.17
        push      DWORD PTR [228+ebx+ebp]                       ;890.17
        mov       DWORD PTR [192+ebp+ebx], 0                    ;889.17
        and       DWORD PTR [204+ebp+ebx], -2                   ;889.17
        call      _for_dealloc_allocatable                      ;890.17
                                ; LOE eax ebx ebp
.B16.29:                        ; Preds .B16.28
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;890.17
        mov       eax, DWORD PTR [276+ebx+ebp]                  ;891.28
        mov       edx, eax                                      ;891.17
        shr       edx, 1                                        ;891.17
        and       eax, 1                                        ;891.17
        and       edx, 1                                        ;891.17
        add       eax, eax                                      ;891.17
        shl       edx, 2                                        ;891.17
        or        edx, 1                                        ;891.17
        or        edx, eax                                      ;891.17
        or        edx, 262144                                   ;891.17
        push      edx                                           ;891.17
        push      DWORD PTR [264+ebx+ebp]                       ;891.17
        mov       DWORD PTR [228+ebp+ebx], 0                    ;890.17
        and       DWORD PTR [240+ebp+ebx], -2                   ;890.17
        call      _for_dealloc_allocatable                      ;891.17
                                ; LOE eax ebx ebp
.B16.30:                        ; Preds .B16.29
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;891.17
        mov       eax, DWORD PTR [312+ebx+ebp]                  ;892.28
        mov       edx, eax                                      ;892.17
        shr       edx, 1                                        ;892.17
        and       eax, 1                                        ;892.17
        and       edx, 1                                        ;892.17
        add       eax, eax                                      ;892.17
        shl       edx, 2                                        ;892.17
        or        edx, 1                                        ;892.17
        or        edx, eax                                      ;892.17
        or        edx, 262144                                   ;892.17
        push      edx                                           ;892.17
        push      DWORD PTR [300+ebx+ebp]                       ;892.17
        mov       DWORD PTR [264+ebp+ebx], 0                    ;891.17
        and       DWORD PTR [276+ebp+ebx], -2                   ;891.17
        call      _for_dealloc_allocatable                      ;892.17
                                ; LOE eax ebx ebp
.B16.31:                        ; Preds .B16.30
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;892.17
        mov       eax, DWORD PTR [368+ebx+ebp]                  ;893.28
        mov       edx, eax                                      ;893.17
        shr       edx, 1                                        ;893.17
        and       eax, 1                                        ;893.17
        and       edx, 1                                        ;893.17
        add       eax, eax                                      ;893.17
        shl       edx, 2                                        ;893.17
        or        edx, 1                                        ;893.17
        or        edx, eax                                      ;893.17
        or        edx, 262144                                   ;893.17
        push      edx                                           ;893.17
        push      DWORD PTR [356+ebx+ebp]                       ;893.17
        mov       DWORD PTR [300+ebp+ebx], 0                    ;892.17
        and       DWORD PTR [312+ebp+ebx], -2                   ;892.17
        call      _for_dealloc_allocatable                      ;893.17
                                ; LOE eax ebx ebp
.B16.32:                        ; Preds .B16.31
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;893.17
        mov       eax, DWORD PTR [468+ebx+ebp]                  ;894.28
        mov       edx, eax                                      ;894.17
        shr       edx, 1                                        ;894.17
        and       eax, 1                                        ;894.17
        and       edx, 1                                        ;894.17
        add       eax, eax                                      ;894.17
        shl       edx, 2                                        ;894.17
        or        edx, 1                                        ;894.17
        or        edx, eax                                      ;894.17
        or        edx, 262144                                   ;894.17
        push      edx                                           ;894.17
        push      DWORD PTR [456+ebx+ebp]                       ;894.17
        mov       DWORD PTR [356+ebp+ebx], 0                    ;893.17
        and       DWORD PTR [368+ebp+ebx], -2                   ;893.17
        call      _for_dealloc_allocatable                      ;894.17
                                ; LOE eax ebx ebp
.B16.33:                        ; Preds .B16.32
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;894.17
        mov       eax, DWORD PTR [504+ebx+ebp]                  ;895.28
        mov       edx, eax                                      ;895.17
        shr       edx, 1                                        ;895.17
        and       eax, 1                                        ;895.17
        and       edx, 1                                        ;895.17
        add       eax, eax                                      ;895.17
        shl       edx, 2                                        ;895.17
        or        edx, 1                                        ;895.17
        or        edx, eax                                      ;895.17
        or        edx, 262144                                   ;895.17
        push      edx                                           ;895.17
        push      DWORD PTR [492+ebx+ebp]                       ;895.17
        mov       DWORD PTR [456+ebp+ebx], 0                    ;894.17
        and       DWORD PTR [468+ebp+ebx], -2                   ;894.17
        call      _for_dealloc_allocatable                      ;895.17
                                ; LOE eax ebx ebp
.B16.34:                        ; Preds .B16.33
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;895.17
        mov       eax, DWORD PTR [540+ebx+ebp]                  ;896.28
        mov       edx, eax                                      ;896.17
        shr       edx, 1                                        ;896.17
        and       eax, 1                                        ;896.17
        and       edx, 1                                        ;896.17
        add       eax, eax                                      ;896.17
        shl       edx, 2                                        ;896.17
        or        edx, 1                                        ;896.17
        or        edx, eax                                      ;896.17
        or        edx, 262144                                   ;896.17
        push      edx                                           ;896.17
        push      DWORD PTR [528+ebx+ebp]                       ;896.17
        mov       DWORD PTR [492+ebp+ebx], 0                    ;895.17
        and       DWORD PTR [504+ebp+ebx], -2                   ;895.17
        call      _for_dealloc_allocatable                      ;896.17
                                ; LOE eax ebx ebp
.B16.35:                        ; Preds .B16.34
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;896.17
        mov       eax, DWORD PTR [576+ebx+ebp]                  ;897.28
        mov       edx, eax                                      ;897.17
        shr       edx, 1                                        ;897.17
        and       eax, 1                                        ;897.17
        and       edx, 1                                        ;897.17
        add       eax, eax                                      ;897.17
        shl       edx, 2                                        ;897.17
        or        edx, 1                                        ;897.17
        or        edx, eax                                      ;897.17
        or        edx, 262144                                   ;897.17
        push      edx                                           ;897.17
        push      DWORD PTR [564+ebx+ebp]                       ;897.17
        mov       DWORD PTR [528+ebp+ebx], 0                    ;896.17
        and       DWORD PTR [540+ebp+ebx], -2                   ;896.17
        call      _for_dealloc_allocatable                      ;897.17
                                ; LOE eax ebx ebp
.B16.36:                        ; Preds .B16.35
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;897.17
        mov       eax, DWORD PTR [612+ebx+ebp]                  ;898.28
        mov       edx, eax                                      ;898.17
        shr       edx, 1                                        ;898.17
        and       eax, 1                                        ;898.17
        and       edx, 1                                        ;898.17
        add       eax, eax                                      ;898.17
        shl       edx, 2                                        ;898.17
        or        edx, 1                                        ;898.17
        or        edx, eax                                      ;898.17
        or        edx, 262144                                   ;898.17
        push      edx                                           ;898.17
        push      DWORD PTR [600+ebx+ebp]                       ;898.17
        mov       DWORD PTR [564+ebp+ebx], 0                    ;897.17
        and       DWORD PTR [576+ebp+ebx], -2                   ;897.17
        call      _for_dealloc_allocatable                      ;898.17
                                ; LOE eax ebx ebp
.B16.37:                        ; Preds .B16.36
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;898.17
        mov       eax, DWORD PTR [720+ebx+ebp]                  ;899.28
        mov       edx, eax                                      ;899.17
        shr       edx, 1                                        ;899.17
        and       eax, 1                                        ;899.17
        and       edx, 1                                        ;899.17
        add       eax, eax                                      ;899.17
        shl       edx, 2                                        ;899.17
        or        edx, 1                                        ;899.17
        or        edx, eax                                      ;899.17
        or        edx, 262144                                   ;899.17
        push      edx                                           ;899.17
        push      DWORD PTR [708+ebx+ebp]                       ;899.17
        mov       DWORD PTR [600+ebp+ebx], 0                    ;898.17
        and       DWORD PTR [612+ebp+ebx], -2                   ;898.17
        call      _for_dealloc_allocatable                      ;899.17
                                ; LOE eax ebx ebp
.B16.129:                       ; Preds .B16.37
        add       esp, 120                                      ;899.17
                                ; LOE eax ebx ebp
.B16.38:                        ; Preds .B16.129
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;899.17
        mov       eax, DWORD PTR [820+ebx+ebp]                  ;900.28
        mov       edx, eax                                      ;900.17
        shr       edx, 1                                        ;900.17
        and       eax, 1                                        ;900.17
        and       edx, 1                                        ;900.17
        add       eax, eax                                      ;900.17
        shl       edx, 2                                        ;900.17
        or        edx, 1                                        ;900.17
        or        edx, eax                                      ;900.17
        or        edx, 262144                                   ;900.17
        push      edx                                           ;900.17
        push      DWORD PTR [808+ebx+ebp]                       ;900.17
        mov       DWORD PTR [708+ebp+ebx], 0                    ;899.17
        and       DWORD PTR [720+ebp+ebx], -2                   ;899.17
        call      _for_dealloc_allocatable                      ;900.17
                                ; LOE eax ebx ebp
.B16.39:                        ; Preds .B16.38
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;900.17
        mov       eax, DWORD PTR [876+ebx+ebp]                  ;901.28
        mov       edx, eax                                      ;901.17
        shr       edx, 1                                        ;901.17
        and       eax, 1                                        ;901.17
        and       edx, 1                                        ;901.17
        add       eax, eax                                      ;901.17
        shl       edx, 2                                        ;901.17
        or        edx, 1                                        ;901.17
        or        edx, eax                                      ;901.17
        or        edx, 262144                                   ;901.17
        push      edx                                           ;901.17
        push      DWORD PTR [864+ebx+ebp]                       ;901.17
        mov       DWORD PTR [808+ebp+ebx], 0                    ;900.17
        and       DWORD PTR [820+ebp+ebx], -2                   ;900.17
        call      _for_dealloc_allocatable                      ;901.17
                                ; LOE eax ebx ebp
.B16.131:                       ; Preds .B16.39
        add       esp, 16                                       ;901.17
                                ; LOE eax ebx ebp
.B16.40:                        ; Preds .B16.131
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;901.17
        mov       eax, DWORD PTR [28+esp]                       ;902.13
        inc       eax                                           ;902.13
        mov       DWORD PTR [864+ebp+ebx], 0                    ;901.17
        and       DWORD PTR [876+ebp+ebx], -2                   ;901.17
        add       ebx, 900                                      ;902.13
        mov       DWORD PTR [28+esp], eax                       ;902.13
        cmp       eax, DWORD PTR [36+esp]                       ;902.13
        jle       .B16.3        ; Prob 82%                      ;902.13
                                ; LOE ebx ebp
.B16.41:                        ; Preds .B16.40
        mov       eax, DWORD PTR [esp]                          ;
                                ; LOE eax
.B16.42:                        ; Preds .B16.41 .B16.1
        mov       ebp, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+12] ;904.13
        test      ebp, 1                                        ;904.13
        je        .B16.64       ; Prob 60%                      ;904.13
                                ; LOE eax ebp
.B16.43:                        ; Preds .B16.42
        imul      ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+24], 900 ;904.13
        mov       esi, eax                                      ;904.13
        add       ebx, eax                                      ;904.13
        cmp       eax, ebx                                      ;904.13
        jae       .B16.64       ; Prob 10%                      ;904.13
                                ; LOE eax ebx ebp esi
.B16.44:                        ; Preds .B16.43
        mov       DWORD PTR [esp], eax                          ;
                                ; LOE ebx ebp esi
.B16.45:                        ; Preds .B16.62 .B16.44
        mov       edi, DWORD PTR [60+esi]                       ;904.13
        test      edi, 1                                        ;904.13
        jne       .B16.107      ; Prob 3%                       ;904.13
                                ; LOE ebx ebp esi edi
.B16.46:                        ; Preds .B16.45 .B16.108
        mov       edi, DWORD PTR [96+esi]                       ;904.13
        test      edi, 1                                        ;904.13
        jne       .B16.105      ; Prob 3%                       ;904.13
                                ; LOE ebx ebp esi edi
.B16.47:                        ; Preds .B16.46 .B16.106
        mov       edi, DWORD PTR [132+esi]                      ;904.13
        test      edi, 1                                        ;904.13
        jne       .B16.103      ; Prob 3%                       ;904.13
                                ; LOE ebx ebp esi edi
.B16.48:                        ; Preds .B16.47 .B16.104
        mov       edi, DWORD PTR [168+esi]                      ;904.13
        test      edi, 1                                        ;904.13
        jne       .B16.101      ; Prob 3%                       ;904.13
                                ; LOE ebx ebp esi edi
.B16.49:                        ; Preds .B16.48 .B16.102
        mov       edi, DWORD PTR [204+esi]                      ;904.13
        test      edi, 1                                        ;904.13
        jne       .B16.99       ; Prob 3%                       ;904.13
                                ; LOE ebx ebp esi edi
.B16.50:                        ; Preds .B16.49 .B16.100
        mov       edi, DWORD PTR [240+esi]                      ;904.13
        test      edi, 1                                        ;904.13
        jne       .B16.97       ; Prob 3%                       ;904.13
                                ; LOE ebx ebp esi edi
.B16.51:                        ; Preds .B16.50 .B16.98
        mov       edi, DWORD PTR [276+esi]                      ;904.13
        test      edi, 1                                        ;904.13
        jne       .B16.95       ; Prob 3%                       ;904.13
                                ; LOE ebx ebp esi edi
.B16.52:                        ; Preds .B16.51 .B16.96
        mov       edi, DWORD PTR [312+esi]                      ;904.13
        test      edi, 1                                        ;904.13
        jne       .B16.93       ; Prob 3%                       ;904.13
                                ; LOE ebx ebp esi edi
.B16.53:                        ; Preds .B16.52 .B16.94
        mov       edi, DWORD PTR [368+esi]                      ;904.13
        test      edi, 1                                        ;904.13
        jne       .B16.91       ; Prob 3%                       ;904.13
                                ; LOE ebx ebp esi edi
.B16.54:                        ; Preds .B16.53 .B16.92
        mov       edi, DWORD PTR [468+esi]                      ;904.13
        test      edi, 1                                        ;904.13
        jne       .B16.89       ; Prob 3%                       ;904.13
                                ; LOE ebx ebp esi edi
.B16.55:                        ; Preds .B16.54 .B16.90
        mov       edi, DWORD PTR [504+esi]                      ;904.13
        test      edi, 1                                        ;904.13
        jne       .B16.87       ; Prob 3%                       ;904.13
                                ; LOE ebx ebp esi edi
.B16.56:                        ; Preds .B16.55 .B16.88
        mov       edi, DWORD PTR [540+esi]                      ;904.13
        test      edi, 1                                        ;904.13
        jne       .B16.85       ; Prob 3%                       ;904.13
                                ; LOE ebx ebp esi edi
.B16.57:                        ; Preds .B16.56 .B16.86
        mov       edi, DWORD PTR [576+esi]                      ;904.13
        test      edi, 1                                        ;904.13
        jne       .B16.83       ; Prob 3%                       ;904.13
                                ; LOE ebx ebp esi edi
.B16.58:                        ; Preds .B16.57 .B16.84
        mov       edi, DWORD PTR [612+esi]                      ;904.13
        test      edi, 1                                        ;904.13
        jne       .B16.81       ; Prob 3%                       ;904.13
                                ; LOE ebx ebp esi edi
.B16.59:                        ; Preds .B16.58 .B16.82
        mov       edi, DWORD PTR [720+esi]                      ;904.13
        test      edi, 1                                        ;904.13
        jne       .B16.79       ; Prob 3%                       ;904.13
                                ; LOE ebx ebp esi edi
.B16.60:                        ; Preds .B16.59 .B16.80
        mov       edi, DWORD PTR [820+esi]                      ;904.13
        test      edi, 1                                        ;904.13
        jne       .B16.77       ; Prob 3%                       ;904.13
                                ; LOE ebx ebp esi edi
.B16.61:                        ; Preds .B16.60 .B16.78
        mov       edi, DWORD PTR [876+esi]                      ;904.13
        test      edi, 1                                        ;904.13
        jne       .B16.75       ; Prob 3%                       ;904.13
                                ; LOE ebx ebp esi edi
.B16.62:                        ; Preds .B16.61 .B16.76
        add       esi, 900                                      ;904.13
        cmp       esi, ebx                                      ;904.13
        jb        .B16.45       ; Prob 82%                      ;904.13
                                ; LOE ebx ebp esi
.B16.63:                        ; Preds .B16.62
        mov       eax, DWORD PTR [esp]                          ;
                                ; LOE eax ebp
.B16.64:                        ; Preds .B16.63 .B16.43 .B16.42
        mov       ecx, ebp                                      ;904.13
        mov       edx, ebp                                      ;904.13
        shr       ecx, 1                                        ;904.13
        and       edx, 1                                        ;904.13
        and       ecx, 1                                        ;904.13
        add       edx, edx                                      ;904.13
        shl       ecx, 2                                        ;904.13
        or        ecx, 1                                        ;904.13
        or        ecx, edx                                      ;904.13
        or        ecx, 262144                                   ;904.13
        push      ecx                                           ;904.13
        push      eax                                           ;904.13
        call      _for_dealloc_allocatable                      ;904.13
                                ; LOE eax ebp
.B16.65:                        ; Preds .B16.64
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_STOPCAP4W+12] ;906.13
        mov       edx, ebx                                      ;906.13
        shr       edx, 1                                        ;906.13
        and       ebp, -2                                       ;904.13
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;904.13
        and       edx, 1                                        ;906.13
        mov       eax, ebx                                      ;906.13
        shl       edx, 2                                        ;906.13
        and       eax, 1                                        ;906.13
        or        edx, 1                                        ;906.13
        add       eax, eax                                      ;906.13
        or        edx, eax                                      ;906.13
        or        edx, 262144                                   ;906.13
        push      edx                                           ;906.13
        push      DWORD PTR [_DYNUST_NETWORK_MODULE_mp_STOPCAP4W] ;906.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE], 0 ;904.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+12], ebp ;904.13
        call      _for_dealloc_allocatable                      ;906.13
                                ; LOE eax ebx
.B16.66:                        ; Preds .B16.65
        and       ebx, -2                                       ;906.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_STOPCAP4W+12], ebx ;906.13
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_STOPCAP2W+12] ;907.13
        mov       edx, ebx                                      ;907.13
        shr       edx, 1                                        ;907.13
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;906.13
        and       edx, 1                                        ;907.13
        mov       eax, ebx                                      ;907.13
        shl       edx, 2                                        ;907.13
        and       eax, 1                                        ;907.13
        or        edx, 1                                        ;907.13
        add       eax, eax                                      ;907.13
        or        edx, eax                                      ;907.13
        or        edx, 262144                                   ;907.13
        push      edx                                           ;907.13
        push      DWORD PTR [_DYNUST_NETWORK_MODULE_mp_STOPCAP2W] ;907.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_STOPCAP4W], 0 ;906.13
        call      _for_dealloc_allocatable                      ;907.13
                                ; LOE eax ebx
.B16.67:                        ; Preds .B16.66
        and       ebx, -2                                       ;907.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_STOPCAP2W+12], ebx ;907.13
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_YIELDCAP+12] ;908.10
        mov       edx, ebx                                      ;908.10
        shr       edx, 1                                        ;908.10
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;907.13
        and       edx, 1                                        ;908.10
        mov       eax, ebx                                      ;908.10
        shl       edx, 2                                        ;908.10
        and       eax, 1                                        ;908.10
        or        edx, 1                                        ;908.10
        add       eax, eax                                      ;908.10
        or        edx, eax                                      ;908.10
        or        edx, 262144                                   ;908.10
        push      edx                                           ;908.10
        push      DWORD PTR [_DYNUST_NETWORK_MODULE_mp_YIELDCAP] ;908.10
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_STOPCAP2W], 0 ;907.13
        call      _for_dealloc_allocatable                      ;908.10
                                ; LOE eax ebx
.B16.68:                        ; Preds .B16.67
        and       ebx, -2                                       ;908.10
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_YIELDCAP+12], ebx ;908.10
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_YIELDCAPIND+12] ;909.10
        mov       edx, ebx                                      ;909.10
        shr       edx, 1                                        ;909.10
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;908.10
        and       edx, 1                                        ;909.10
        mov       eax, ebx                                      ;909.10
        shl       edx, 2                                        ;909.10
        and       eax, 1                                        ;909.10
        or        edx, 1                                        ;909.10
        add       eax, eax                                      ;909.10
        or        edx, eax                                      ;909.10
        or        edx, 262144                                   ;909.10
        push      edx                                           ;909.10
        push      DWORD PTR [_DYNUST_NETWORK_MODULE_mp_YIELDCAPIND] ;909.10
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_YIELDCAP], 0 ;908.10
        call      _for_dealloc_allocatable                      ;909.10
                                ; LOE eax ebx
.B16.69:                        ; Preds .B16.68
        and       ebx, -2                                       ;909.10
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_YIELDCAPIND+12], ebx ;909.10
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_LOADWEIGHTID+12] ;910.10
        mov       edx, ebx                                      ;910.10
        shr       edx, 1                                        ;910.10
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;909.10
        and       edx, 1                                        ;910.10
        mov       eax, ebx                                      ;910.10
        shl       edx, 2                                        ;910.10
        and       eax, 1                                        ;910.10
        or        edx, 1                                        ;910.10
        add       eax, eax                                      ;910.10
        or        edx, eax                                      ;910.10
        or        edx, 262144                                   ;910.10
        push      edx                                           ;910.10
        push      DWORD PTR [_DYNUST_NETWORK_MODULE_mp_LOADWEIGHTID] ;910.10
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_YIELDCAPIND], 0 ;909.10
        call      _for_dealloc_allocatable                      ;910.10
                                ; LOE eax ebx
.B16.70:                        ; Preds .B16.69
        and       ebx, -2                                       ;910.10
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_LOADWEIGHTID+12], ebx ;910.10
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC+12] ;911.10
        mov       edx, ebx                                      ;911.10
        shr       edx, 1                                        ;911.10
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;910.10
        and       edx, 1                                        ;911.10
        mov       eax, ebx                                      ;911.10
        shl       edx, 2                                        ;911.10
        and       eax, 1                                        ;911.10
        or        edx, 1                                        ;911.10
        add       eax, eax                                      ;911.10
        or        edx, eax                                      ;911.10
        or        edx, 262144                                   ;911.10
        push      edx                                           ;911.10
        push      DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC]  ;911.10
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_LOADWEIGHTID], 0 ;910.10
        call      _for_dealloc_allocatable                      ;911.10
                                ; LOE eax ebx
.B16.71:                        ; Preds .B16.70
        and       ebx, -2                                       ;911.10
        xor       edx, edx                                      ;911.10
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;911.10
        mov       eax, ebx                                      ;912.10
        shr       eax, 1                                        ;912.10
        and       eax, 1                                        ;912.10
        shl       eax, 2                                        ;912.10
        or        eax, 262145                                   ;912.10
        push      eax                                           ;912.10
        push      edx                                           ;912.10
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC], edx ;911.10
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC+12], ebx ;911.10
        call      _for_dealloc_allocatable                      ;912.10
                                ; LOE eax ebx
.B16.72:                        ; Preds .B16.71
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC+12], ebx ;912.10
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_LINKIDMAP+12] ;913.10
        mov       edx, ebx                                      ;913.10
        shr       edx, 1                                        ;913.10
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;912.10
        and       edx, 1                                        ;913.10
        mov       eax, ebx                                      ;913.10
        shl       edx, 2                                        ;913.10
        and       eax, 1                                        ;913.10
        or        edx, 1                                        ;913.10
        add       eax, eax                                      ;913.10
        or        edx, eax                                      ;913.10
        or        edx, 262144                                   ;913.10
        push      edx                                           ;913.10
        push      DWORD PTR [_DYNUST_NETWORK_MODULE_mp_LINKIDMAP] ;913.10
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC], 0 ;912.10
        call      _for_dealloc_allocatable                      ;913.10
                                ; LOE eax ebx
.B16.73:                        ; Preds .B16.72
        and       ebx, -2                                       ;913.10
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_LINKIDMAP+12], ebx ;913.10
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_KGPOINT+12] ;914.10
        mov       edx, ebx                                      ;914.10
        shr       edx, 1                                        ;914.10
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;913.10
        and       edx, 1                                        ;914.10
        mov       eax, ebx                                      ;914.10
        shl       edx, 2                                        ;914.10
        and       eax, 1                                        ;914.10
        or        edx, 1                                        ;914.10
        add       eax, eax                                      ;914.10
        or        edx, eax                                      ;914.10
        or        edx, 262144                                   ;914.10
        push      edx                                           ;914.10
        push      DWORD PTR [_DYNUST_NETWORK_MODULE_mp_KGPOINT] ;914.10
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_LINKIDMAP], 0 ;913.10
        call      _for_dealloc_allocatable                      ;914.10
                                ; LOE eax ebx
.B16.74:                        ; Preds .B16.73
        and       ebx, -2                                       ;914.10
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_KGPOINT], 0 ;914.10
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_KGPOINT+12], ebx ;914.10
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;914.10
        add       esp, 128                                      ;915.9
        pop       ebp                                           ;915.9
        pop       ebx                                           ;915.9
        pop       edi                                           ;915.9
        pop       esi                                           ;915.9
        ret                                                     ;915.9
                                ; LOE
.B16.75:                        ; Preds .B16.61                 ; Infreq
        push      262145                                        ;904.13
        push      DWORD PTR [864+esi]                           ;904.13
        call      _for_deallocate                               ;904.13
                                ; LOE ebx ebp esi edi
.B16.142:                       ; Preds .B16.75                 ; Infreq
        add       esp, 8                                        ;904.13
                                ; LOE ebx ebp esi edi
.B16.76:                        ; Preds .B16.142                ; Infreq
        and       edi, -2                                       ;904.13
        mov       DWORD PTR [864+esi], 0                        ;904.13
        mov       DWORD PTR [876+esi], edi                      ;904.13
        jmp       .B16.62       ; Prob 100%                     ;904.13
                                ; LOE ebx ebp esi
.B16.77:                        ; Preds .B16.60                 ; Infreq
        push      262145                                        ;904.13
        push      DWORD PTR [808+esi]                           ;904.13
        call      _for_deallocate                               ;904.13
                                ; LOE ebx ebp esi edi
.B16.143:                       ; Preds .B16.77                 ; Infreq
        add       esp, 8                                        ;904.13
                                ; LOE ebx ebp esi edi
.B16.78:                        ; Preds .B16.143                ; Infreq
        and       edi, -2                                       ;904.13
        mov       DWORD PTR [808+esi], 0                        ;904.13
        mov       DWORD PTR [820+esi], edi                      ;904.13
        jmp       .B16.61       ; Prob 100%                     ;904.13
                                ; LOE ebx ebp esi
.B16.79:                        ; Preds .B16.59                 ; Infreq
        push      262145                                        ;904.13
        push      DWORD PTR [708+esi]                           ;904.13
        call      _for_deallocate                               ;904.13
                                ; LOE ebx ebp esi edi
.B16.144:                       ; Preds .B16.79                 ; Infreq
        add       esp, 8                                        ;904.13
                                ; LOE ebx ebp esi edi
.B16.80:                        ; Preds .B16.144                ; Infreq
        and       edi, -2                                       ;904.13
        mov       DWORD PTR [708+esi], 0                        ;904.13
        mov       DWORD PTR [720+esi], edi                      ;904.13
        jmp       .B16.60       ; Prob 100%                     ;904.13
                                ; LOE ebx ebp esi
.B16.81:                        ; Preds .B16.58                 ; Infreq
        push      262145                                        ;904.13
        push      DWORD PTR [600+esi]                           ;904.13
        call      _for_deallocate                               ;904.13
                                ; LOE ebx ebp esi edi
.B16.145:                       ; Preds .B16.81                 ; Infreq
        add       esp, 8                                        ;904.13
                                ; LOE ebx ebp esi edi
.B16.82:                        ; Preds .B16.145                ; Infreq
        and       edi, -2                                       ;904.13
        mov       DWORD PTR [600+esi], 0                        ;904.13
        mov       DWORD PTR [612+esi], edi                      ;904.13
        jmp       .B16.59       ; Prob 100%                     ;904.13
                                ; LOE ebx ebp esi
.B16.83:                        ; Preds .B16.57                 ; Infreq
        push      262145                                        ;904.13
        push      DWORD PTR [564+esi]                           ;904.13
        call      _for_deallocate                               ;904.13
                                ; LOE ebx ebp esi edi
.B16.146:                       ; Preds .B16.83                 ; Infreq
        add       esp, 8                                        ;904.13
                                ; LOE ebx ebp esi edi
.B16.84:                        ; Preds .B16.146                ; Infreq
        and       edi, -2                                       ;904.13
        mov       DWORD PTR [564+esi], 0                        ;904.13
        mov       DWORD PTR [576+esi], edi                      ;904.13
        jmp       .B16.58       ; Prob 100%                     ;904.13
                                ; LOE ebx ebp esi
.B16.85:                        ; Preds .B16.56                 ; Infreq
        push      262145                                        ;904.13
        push      DWORD PTR [528+esi]                           ;904.13
        call      _for_deallocate                               ;904.13
                                ; LOE ebx ebp esi edi
.B16.147:                       ; Preds .B16.85                 ; Infreq
        add       esp, 8                                        ;904.13
                                ; LOE ebx ebp esi edi
.B16.86:                        ; Preds .B16.147                ; Infreq
        and       edi, -2                                       ;904.13
        mov       DWORD PTR [528+esi], 0                        ;904.13
        mov       DWORD PTR [540+esi], edi                      ;904.13
        jmp       .B16.57       ; Prob 100%                     ;904.13
                                ; LOE ebx ebp esi
.B16.87:                        ; Preds .B16.55                 ; Infreq
        push      262145                                        ;904.13
        push      DWORD PTR [492+esi]                           ;904.13
        call      _for_deallocate                               ;904.13
                                ; LOE ebx ebp esi edi
.B16.148:                       ; Preds .B16.87                 ; Infreq
        add       esp, 8                                        ;904.13
                                ; LOE ebx ebp esi edi
.B16.88:                        ; Preds .B16.148                ; Infreq
        and       edi, -2                                       ;904.13
        mov       DWORD PTR [492+esi], 0                        ;904.13
        mov       DWORD PTR [504+esi], edi                      ;904.13
        jmp       .B16.56       ; Prob 100%                     ;904.13
                                ; LOE ebx ebp esi
.B16.89:                        ; Preds .B16.54                 ; Infreq
        push      262145                                        ;904.13
        push      DWORD PTR [456+esi]                           ;904.13
        call      _for_deallocate                               ;904.13
                                ; LOE ebx ebp esi edi
.B16.149:                       ; Preds .B16.89                 ; Infreq
        add       esp, 8                                        ;904.13
                                ; LOE ebx ebp esi edi
.B16.90:                        ; Preds .B16.149                ; Infreq
        and       edi, -2                                       ;904.13
        mov       DWORD PTR [456+esi], 0                        ;904.13
        mov       DWORD PTR [468+esi], edi                      ;904.13
        jmp       .B16.55       ; Prob 100%                     ;904.13
                                ; LOE ebx ebp esi
.B16.91:                        ; Preds .B16.53                 ; Infreq
        push      262145                                        ;904.13
        push      DWORD PTR [356+esi]                           ;904.13
        call      _for_deallocate                               ;904.13
                                ; LOE ebx ebp esi edi
.B16.150:                       ; Preds .B16.91                 ; Infreq
        add       esp, 8                                        ;904.13
                                ; LOE ebx ebp esi edi
.B16.92:                        ; Preds .B16.150                ; Infreq
        and       edi, -2                                       ;904.13
        mov       DWORD PTR [356+esi], 0                        ;904.13
        mov       DWORD PTR [368+esi], edi                      ;904.13
        jmp       .B16.54       ; Prob 100%                     ;904.13
                                ; LOE ebx ebp esi
.B16.93:                        ; Preds .B16.52                 ; Infreq
        push      262145                                        ;904.13
        push      DWORD PTR [300+esi]                           ;904.13
        call      _for_deallocate                               ;904.13
                                ; LOE ebx ebp esi edi
.B16.151:                       ; Preds .B16.93                 ; Infreq
        add       esp, 8                                        ;904.13
                                ; LOE ebx ebp esi edi
.B16.94:                        ; Preds .B16.151                ; Infreq
        and       edi, -2                                       ;904.13
        mov       DWORD PTR [300+esi], 0                        ;904.13
        mov       DWORD PTR [312+esi], edi                      ;904.13
        jmp       .B16.53       ; Prob 100%                     ;904.13
                                ; LOE ebx ebp esi
.B16.95:                        ; Preds .B16.51                 ; Infreq
        push      262145                                        ;904.13
        push      DWORD PTR [264+esi]                           ;904.13
        call      _for_deallocate                               ;904.13
                                ; LOE ebx ebp esi edi
.B16.152:                       ; Preds .B16.95                 ; Infreq
        add       esp, 8                                        ;904.13
                                ; LOE ebx ebp esi edi
.B16.96:                        ; Preds .B16.152                ; Infreq
        and       edi, -2                                       ;904.13
        mov       DWORD PTR [264+esi], 0                        ;904.13
        mov       DWORD PTR [276+esi], edi                      ;904.13
        jmp       .B16.52       ; Prob 100%                     ;904.13
                                ; LOE ebx ebp esi
.B16.97:                        ; Preds .B16.50                 ; Infreq
        push      262145                                        ;904.13
        push      DWORD PTR [228+esi]                           ;904.13
        call      _for_deallocate                               ;904.13
                                ; LOE ebx ebp esi edi
.B16.153:                       ; Preds .B16.97                 ; Infreq
        add       esp, 8                                        ;904.13
                                ; LOE ebx ebp esi edi
.B16.98:                        ; Preds .B16.153                ; Infreq
        and       edi, -2                                       ;904.13
        mov       DWORD PTR [228+esi], 0                        ;904.13
        mov       DWORD PTR [240+esi], edi                      ;904.13
        jmp       .B16.51       ; Prob 100%                     ;904.13
                                ; LOE ebx ebp esi
.B16.99:                        ; Preds .B16.49                 ; Infreq
        push      262145                                        ;904.13
        push      DWORD PTR [192+esi]                           ;904.13
        call      _for_deallocate                               ;904.13
                                ; LOE ebx ebp esi edi
.B16.154:                       ; Preds .B16.99                 ; Infreq
        add       esp, 8                                        ;904.13
                                ; LOE ebx ebp esi edi
.B16.100:                       ; Preds .B16.154                ; Infreq
        and       edi, -2                                       ;904.13
        mov       DWORD PTR [192+esi], 0                        ;904.13
        mov       DWORD PTR [204+esi], edi                      ;904.13
        jmp       .B16.50       ; Prob 100%                     ;904.13
                                ; LOE ebx ebp esi
.B16.101:                       ; Preds .B16.48                 ; Infreq
        push      262145                                        ;904.13
        push      DWORD PTR [156+esi]                           ;904.13
        call      _for_deallocate                               ;904.13
                                ; LOE ebx ebp esi edi
.B16.155:                       ; Preds .B16.101                ; Infreq
        add       esp, 8                                        ;904.13
                                ; LOE ebx ebp esi edi
.B16.102:                       ; Preds .B16.155                ; Infreq
        and       edi, -2                                       ;904.13
        mov       DWORD PTR [156+esi], 0                        ;904.13
        mov       DWORD PTR [168+esi], edi                      ;904.13
        jmp       .B16.49       ; Prob 100%                     ;904.13
                                ; LOE ebx ebp esi
.B16.103:                       ; Preds .B16.47                 ; Infreq
        push      262145                                        ;904.13
        push      DWORD PTR [120+esi]                           ;904.13
        call      _for_deallocate                               ;904.13
                                ; LOE ebx ebp esi edi
.B16.156:                       ; Preds .B16.103                ; Infreq
        add       esp, 8                                        ;904.13
                                ; LOE ebx ebp esi edi
.B16.104:                       ; Preds .B16.156                ; Infreq
        and       edi, -2                                       ;904.13
        mov       DWORD PTR [120+esi], 0                        ;904.13
        mov       DWORD PTR [132+esi], edi                      ;904.13
        jmp       .B16.48       ; Prob 100%                     ;904.13
                                ; LOE ebx ebp esi
.B16.105:                       ; Preds .B16.46                 ; Infreq
        push      262145                                        ;904.13
        push      DWORD PTR [84+esi]                            ;904.13
        call      _for_deallocate                               ;904.13
                                ; LOE ebx ebp esi edi
.B16.157:                       ; Preds .B16.105                ; Infreq
        add       esp, 8                                        ;904.13
                                ; LOE ebx ebp esi edi
.B16.106:                       ; Preds .B16.157                ; Infreq
        and       edi, -2                                       ;904.13
        mov       DWORD PTR [84+esi], 0                         ;904.13
        mov       DWORD PTR [96+esi], edi                       ;904.13
        jmp       .B16.47       ; Prob 100%                     ;904.13
                                ; LOE ebx ebp esi
.B16.107:                       ; Preds .B16.45                 ; Infreq
        push      262145                                        ;904.13
        push      DWORD PTR [48+esi]                            ;904.13
        call      _for_deallocate                               ;904.13
                                ; LOE ebx ebp esi edi
.B16.158:                       ; Preds .B16.107                ; Infreq
        add       esp, 8                                        ;904.13
                                ; LOE ebx ebp esi edi
.B16.108:                       ; Preds .B16.158                ; Infreq
        and       edi, -2                                       ;904.13
        mov       DWORD PTR [48+esi], 0                         ;904.13
        mov       DWORD PTR [60+esi], edi                       ;904.13
        jmp       .B16.46       ; Prob 100%                     ;904.13
        ALIGN     16
                                ; LOE ebx ebp esi
; mark_end;
_DYNUST_NETWORK_MODULE_mp_DEALLOCATE_DYNUST_NETWORK_ARC ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_NETWORK_MODULE_mp_DEALLOCATE_DYNUST_NETWORK_ARC
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
; -- Begin  _DYNUST_NETWORK_MODULE_mp_DEALLOCATE_DYNUST_NETWORK_ARCMOVE
; mark_begin;
       ALIGN     16
	PUBLIC _DYNUST_NETWORK_MODULE_mp_DEALLOCATE_DYNUST_NETWORK_ARCMOVE
_DYNUST_NETWORK_MODULE_mp_DEALLOCATE_DYNUST_NETWORK_ARCMOVE	PROC NEAR 
.B17.1:                         ; Preds .B17.0
        push      esi                                           ;918.20
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+12] ;919.13
        mov       edx, esi                                      ;919.13
        shr       edx, 1                                        ;919.13
        mov       eax, esi                                      ;919.13
        and       edx, 1                                        ;919.13
        and       eax, 1                                        ;919.13
        shl       edx, 2                                        ;919.13
        add       eax, eax                                      ;919.13
        or        edx, 1                                        ;919.13
        or        edx, eax                                      ;919.13
        or        edx, 262144                                   ;919.13
        push      edx                                           ;919.13
        push      DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE] ;919.13
        call      _for_dealloc_allocatable                      ;919.13
                                ; LOE eax ebx ebp esi edi
.B17.2:                         ; Preds .B17.1
        and       esi, -2                                       ;919.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+12], esi ;919.13
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_NSIGN+12] ;920.13
        mov       edx, esi                                      ;920.13
        shr       edx, 1                                        ;920.13
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;919.13
        and       edx, 1                                        ;920.13
        mov       eax, esi                                      ;920.13
        shl       edx, 2                                        ;920.13
        and       eax, 1                                        ;920.13
        or        edx, 1                                        ;920.13
        add       eax, eax                                      ;920.13
        or        edx, eax                                      ;920.13
        or        edx, 262144                                   ;920.13
        push      edx                                           ;920.13
        push      DWORD PTR [_DYNUST_NETWORK_MODULE_mp_NSIGN]   ;920.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE], 0 ;919.13
        call      _for_dealloc_allocatable                      ;920.13
                                ; LOE eax ebx ebp esi edi
.B17.3:                         ; Preds .B17.2
        and       esi, -2                                       ;920.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_NSIGN], 0 ;920.13
        mov       DWORD PTR [_DYNUST_NETWORK_MODULE_mp_NSIGN+12], esi ;920.13
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;920.13
        add       esp, 16                                       ;921.9
        pop       esi                                           ;921.9
        ret                                                     ;921.9
        ALIGN     16
                                ; LOE
; mark_end;
_DYNUST_NETWORK_MODULE_mp_DEALLOCATE_DYNUST_NETWORK_ARCMOVE ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _DYNUST_NETWORK_MODULE_mp_DEALLOCATE_DYNUST_NETWORK_ARCMOVE
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
	PUBLIC _DYNUST_NETWORK_MODULE_mp_NSIGN
_DYNUST_NETWORK_MODULE_mp_NSIGN	DD	0
	DD	0
	DD	0
	DD	128
	DD	2
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	PUBLIC _DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE
_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE	DD	0
	DD	0
	DD	0
	DD	128
	DD	2
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	PUBLIC _DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE
_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE	DD	0
	DD	0
	DD	0
	DD	128
	DD	2
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	PUBLIC _DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM
_DYNUST_NETWORK_MODULE_mp_OUTTOINNODENUM	DD	0
	DD	0
	DD	0
	DD	128
	DD	1
	DD	0
	DD	0
	DD	0
	DD	0
	DD 1 DUP (0H)	; pad
	PUBLIC _DYNUST_NETWORK_MODULE_mp_BACKPOINTR
_DYNUST_NETWORK_MODULE_mp_BACKPOINTR	DD	0
	DD	0
	DD	0
	DD	128
	DD	1
	DD	0
	DD	0
	DD	0
	DD	0
	DD 1 DUP (0H)	; pad
	PUBLIC _DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE
_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE	DD	0
	DD	0
	DD	0
	DD	128
	DD	1
	DD	0
	DD	0
	DD	0
	DD	0
	DD 1 DUP (0H)	; pad
	PUBLIC _DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE
_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE	DD	0
	DD	0
	DD	0
	DD	128
	DD	1
	DD	0
	DD	0
	DD	0
	DD	0
	DD 1 DUP (0H)	; pad
	PUBLIC _DYNUST_NETWORK_MODULE_mp_KGPOINT
_DYNUST_NETWORK_MODULE_mp_KGPOINT	DD	0
	DD	0
	DD	0
	DD	128
	DD	1
	DD	0
	DD	0
	DD	0
	DD	0
	DD 1 DUP (0H)	; pad
	PUBLIC _DYNUST_NETWORK_MODULE_mp_LINKIDMAP
_DYNUST_NETWORK_MODULE_mp_LINKIDMAP	DD	0
	DD	0
	DD	0
	DD	128
	DD	1
	DD	0
	DD	0
	DD	0
	DD	0
	DD 1 DUP (0H)	; pad
	PUBLIC _DYNUST_NETWORK_MODULE_mp_VKFUNC
_DYNUST_NETWORK_MODULE_mp_VKFUNC	DD	0
	DD	0
	DD	0
	DD	128
	DD	1
	DD	0
	DD	0
	DD	0
	DD	0
	DD 1 DUP (0H)	; pad
	PUBLIC _DYNUST_NETWORK_MODULE_mp_LOADWEIGHTID
_DYNUST_NETWORK_MODULE_mp_LOADWEIGHTID	DD	0
	DD	0
	DD	0
	DD	128
	DD	1
	DD	0
	DD	0
	DD	0
	DD	0
	DD 1 DUP (0H)	; pad
	PUBLIC _DYNUST_NETWORK_MODULE_mp_YIELDCAPIND
_DYNUST_NETWORK_MODULE_mp_YIELDCAPIND	DD	0
	DD	0
	DD	0
	DD	128
	DD	1
	DD	0
	DD	0
	DD	0
	DD	0
	DD 1 DUP (0H)	; pad
	PUBLIC _DYNUST_NETWORK_MODULE_mp_YIELDCAP
_DYNUST_NETWORK_MODULE_mp_YIELDCAP	DD	0
	DD	0
	DD	0
	DD	128
	DD	2
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	PUBLIC _DYNUST_NETWORK_MODULE_mp_STOPCAP2WIND
_DYNUST_NETWORK_MODULE_mp_STOPCAP2WIND	DD	0
	DD	0
	DD	0
	DD	128
	DD	1
	DD	0
	DD	0
	DD	0
	DD	0
	DD 1 DUP (0H)	; pad
	PUBLIC _DYNUST_NETWORK_MODULE_mp_STOPCAP2W
_DYNUST_NETWORK_MODULE_mp_STOPCAP2W	DD	0
	DD	0
	DD	0
	DD	128
	DD	2
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	PUBLIC _DYNUST_NETWORK_MODULE_mp_STOPCAP4W
_DYNUST_NETWORK_MODULE_mp_STOPCAP4W	DD	0
	DD	0
	DD	0
	DD	128
	DD	2
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	PUBLIC _DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE
_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE	DD	0
	DD	0
	DD	0
	DD	128
	DD	1
	DD	0
	DD	0
	DD	0
	DD	0
	DD 1 DUP (0H)	; pad
	PUBLIC _DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE
_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE	DD	0
	DD	0
	DD	0
	DD	128
	DD	1
	DD	0
	DD	0
	DD	0
	DD	0
	DD 1 DUP (0H)	; pad
	PUBLIC _DYNUST_NETWORK_MODULE_mp_Y
_DYNUST_NETWORK_MODULE_mp_Y	DD	0
	DD	0
	DD	0
	DD	128
	DD	1
	DD	0
	DD	0
	DD	0
	DD	0
	DD 1 DUP (0H)	; pad
	PUBLIC _DYNUST_NETWORK_MODULE_mp_X
_DYNUST_NETWORK_MODULE_mp_X	DD	0
	DD	0
	DD	0
	DD	128
	DD	1
	DD	0
	DD	0
	DD	0
	DD	0
	DD 1 DUP (0H)	; pad
	PUBLIC _DYNUST_NETWORK_MODULE_mp_CONZONETMP
_DYNUST_NETWORK_MODULE_mp_CONZONETMP	DD	0
	DD	0
	DD	0
	DD	128
	DD	2
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
_DYNUST_NETWORK_MODULE_mp_ALLOCATE_DYNUST_NETWORK_ARC$BLK..T1042_	DD 12 DUP (0H)	; pad
	DD	0
	DD	0
	DD	0
	DD	128
	DD	1
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	128
	DD	1
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	128
	DD	1
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	128
	DD	1
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	128
	DD	1
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	128
	DD	1
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	128
	DD	1
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	128
	DD	1
	DD	0
	DD	0
	DD	0
	DD	0
	DD 5 DUP (0H)	; pad
	DD	0
	DD	0
	DD	0
	DD	128
	DD	1
	DD	0
	DD	0
	DD	0
	DD	0
	DD 16 DUP (0H)	; pad
	DD	0
	DD	0
	DD	0
	DD	128
	DD	1
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	128
	DD	1
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	128
	DD	1
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	128
	DD	1
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	128
	DD	1
	DD	0
	DD	0
	DD	0
	DD	0
	DD 18 DUP (0H)	; pad
	DD	0
	DD	0
	DD	0
	DD	128
	DD	1
	DD	0
	DD	0
	DD	0
	DD	0
	DD 16 DUP (0H)	; pad
	DD	0
	DD	0
	DD	0
	DD	128
	DD	2
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD	0
	DD 2 DUP (0H)	; pad
	DD	0
	DD	0
	DD	0
	DD	128
	DD	1
	DD	0
	DD	0
	DD	0
	DD	0
	DD 7 DUP (0H)	; pad
_DYNUST_NETWORK_MODULE_mp_ALLOCATE_DYNUST_NETWORK_NODE$BLK..T1886_	DD	0
	DD	0
	DD	0
	DD	128
	DD	1
	DD	0
	DD	0
	DD	0
	DD	0
	DD 1 DUP (0H)	; pad
	DD	0
	DD	0
	DD	0
	DD	128
	DD	1
	DD	0
	DD	0
	DD	0
	DD	0
	DD 2 DUP (0H)	; pad
	DD 3 DUP (0H)	; pad
_DYNUST_NETWORK_MODULE_mp_ALLOCATE_DYNUST_NETWORK_NODE_NDE$BLK..T1998_	DD	0
	DD	0
	DD	0
	DD	128
	DD	1
	DD	0
	DD	0
	DD	0
	DD	0
	DD 2 DUP (0H)	; pad
_DATA	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DD 2 DUP (0H)	; pad
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_0	DB	97
	DB	108
	DB	108
	DB	111
	DB	99
	DB	97
	DB	116
	DB	101
	DB	32
	DB	100
	DB	121
	DB	110
	DB	117
	DB	115
	DB	116
	DB	95
	DB	110
	DB	101
	DB	116
	DB	119
	DB	111
	DB	114
	DB	107
	DB	95
	DB	97
	DB	114
	DB	99
	DB	109
	DB	111
	DB	118
	DB	101
	DB	95
	DB	110
	DB	100
	DB	101
	DB	32
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	45
	DB	32
	DB	105
	DB	110
	DB	115
	DB	117
	DB	102
	DB	102
	DB	105
	DB	99
	DB	105
	DB	101
	DB	110
	DB	116
	DB	32
	DB	109
	DB	101
	DB	109
	DB	111
	DB	114
	DB	121
	DB	0
_2__cnst_pck.21	DW	1
	DW	1
	DW	3
	DW	4
	DW	2
	DW	1
	DW	2
	DW	1
	DW	1
	DW	1
	DD 3 DUP (0H)	; pad
_2il0floatpacket.22	DD	000000064H,000000064H,000000064H,000000064H
_2il0floatpacket.24	DD	000010001H,000010001H,000010001H,000010001H
_2il0floatpacket.30	DD	001010101H,001010101H,001010101H,001010101H
__STRLITPACK_120	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	71
	DB	101
	DB	116
	DB	70
	DB	108
	DB	105
	DB	110
	DB	107
	DB	70
	DB	114
	DB	111
	DB	109
	DB	78
	DB	111
	DB	100
	DB	101
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_82	DB	69
	DB	82
	DB	82
	DB	79
	DB	82
	DB	32
	DB	83
	DB	79
	DB	85
	DB	82
	DB	67
	DB	69
	DB	58
	DB	32
	DB	77
	DB	79
	DB	86
	DB	69
	DB	83
	DB	32
	DB	109
	DB	111
	DB	100
	DB	117
	DB	108
	DB	101
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_84	DB	69
	DB	82
	DB	82
	DB	79
	DB	82
	DB	32
	DB	83
	DB	79
	DB	85
	DB	82
	DB	67
	DB	69
	DB	58
	DB	32
	DB	82
	DB	97
	DB	109
	DB	112
	DB	32
	DB	109
	DB	101
	DB	116
	DB	101
	DB	114
	DB	105
	DB	110
	DB	103
	DB	32
	DB	109
	DB	111
	DB	100
	DB	117
	DB	108
	DB	101
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_86	DB	69
	DB	82
	DB	82
	DB	79
	DB	82
	DB	32
	DB	83
	DB	79
	DB	85
	DB	82
	DB	67
	DB	69
	DB	58
	DB	32
	DB	87
	DB	114
	DB	105
	DB	116
	DB	101
	DB	32
	DB	115
	DB	117
	DB	109
	DB	109
	DB	97
	DB	114
	DB	121
	DB	32
	DB	109
	DB	111
	DB	100
	DB	117
	DB	108
	DB	101
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_88	DB	69
	DB	82
	DB	82
	DB	79
	DB	82
	DB	32
	DB	83
	DB	79
	DB	85
	DB	82
	DB	67
	DB	69
	DB	58
	DB	32
	DB	77
	DB	97
	DB	105
	DB	110
	DB	32
	DB	108
	DB	111
	DB	111
	DB	112
	DB	32
	DB	109
	DB	111
	DB	100
	DB	117
	DB	108
	DB	101
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_90	DB	69
	DB	82
	DB	82
	DB	79
	DB	82
	DB	32
	DB	83
	DB	79
	DB	85
	DB	82
	DB	67
	DB	69
	DB	58
	DB	32
	DB	86
	DB	101
	DB	104
	DB	105
	DB	99
	DB	108
	DB	101
	DB	32
	DB	100
	DB	97
	DB	116
	DB	97
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_92	DB	69
	DB	82
	DB	82
	DB	79
	DB	82
	DB	32
	DB	83
	DB	79
	DB	85
	DB	82
	DB	67
	DB	69
	DB	58
	DB	32
	DB	83
	DB	105
	DB	103
	DB	110
	DB	97
	DB	108
	DB	32
	DB	100
	DB	97
	DB	116
	DB	97
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_94	DB	69
	DB	82
	DB	82
	DB	79
	DB	82
	DB	32
	DB	83
	DB	79
	DB	85
	DB	82
	DB	67
	DB	69
	DB	58
	DB	32
	DB	73
	DB	110
	DB	116
	DB	101
	DB	114
	DB	115
	DB	101
	DB	99
	DB	116
	DB	105
	DB	111
	DB	110
	DB	32
	DB	99
	DB	111
	DB	110
	DB	116
	DB	114
	DB	111
	DB	108
	DB	32
	DB	109
	DB	111
	DB	100
	DB	117
	DB	108
	DB	101
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_96	DB	69
	DB	82
	DB	82
	DB	79
	DB	82
	DB	32
	DB	83
	DB	79
	DB	85
	DB	82
	DB	67
	DB	69
	DB	58
	DB	32
	DB	87
	DB	111
	DB	114
	DB	107
	DB	32
	DB	122
	DB	111
	DB	110
	DB	101
	DB	32
	DB	100
	DB	97
	DB	116
	DB	97
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_98	DB	69
	DB	82
	DB	82
	DB	79
	DB	82
	DB	32
	DB	83
	DB	79
	DB	85
	DB	82
	DB	67
	DB	69
	DB	58
	DB	32
	DB	79
	DB	114
	DB	105
	DB	103
	DB	105
	DB	110
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_100	DB	69
	DB	82
	DB	82
	DB	79
	DB	82
	DB	32
	DB	83
	DB	79
	DB	85
	DB	82
	DB	67
	DB	69
	DB	58
	DB	32
	DB	73
	DB	110
	DB	99
	DB	105
	DB	100
	DB	101
	DB	110
	DB	116
	DB	32
	DB	100
	DB	97
	DB	116
	DB	97
	DB	0
__STRLITPACK_102	DB	69
	DB	82
	DB	82
	DB	79
	DB	82
	DB	32
	DB	83
	DB	79
	DB	85
	DB	82
	DB	67
	DB	69
	DB	58
	DB	32
	DB	86
	DB	77
	DB	83
	DB	32
	DB	100
	DB	97
	DB	116
	DB	97
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_104	DB	69
	DB	82
	DB	82
	DB	79
	DB	82
	DB	32
	DB	83
	DB	79
	DB	85
	DB	82
	DB	67
	DB	69
	DB	58
	DB	32
	DB	77
	DB	111
	DB	118
	DB	101
	DB	109
	DB	101
	DB	110
	DB	116
	DB	32
	DB	100
	DB	97
	DB	116
	DB	97
	DB	0
__STRLITPACK_106	DB	69
	DB	82
	DB	82
	DB	79
	DB	82
	DB	32
	DB	83
	DB	79
	DB	85
	DB	82
	DB	67
	DB	69
	DB	58
	DB	32
	DB	68
	DB	101
	DB	116
	DB	101
	DB	99
	DB	116
	DB	111
	DB	114
	DB	32
	DB	100
	DB	97
	DB	116
	DB	97
	DB	0
__STRLITPACK_108	DB	69
	DB	82
	DB	82
	DB	79
	DB	82
	DB	32
	DB	83
	DB	79
	DB	85
	DB	82
	DB	67
	DB	69
	DB	58
	DB	32
	DB	84
	DB	111
	DB	108
	DB	108
	DB	32
	DB	100
	DB	97
	DB	116
	DB	97
	DB	0
__STRLITPACK_110	DB	69
	DB	82
	DB	82
	DB	79
	DB	82
	DB	32
	DB	83
	DB	79
	DB	85
	DB	82
	DB	67
	DB	69
	DB	58
	DB	32
	DB	87
	DB	111
	DB	114
	DB	107
	DB	32
	DB	122
	DB	111
	DB	110
	DB	101
	DB	32
	DB	115
	DB	117
	DB	98
	DB	114
	DB	111
	DB	117
	DB	116
	DB	105
	DB	110
	DB	101
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_112	DB	69
	DB	82
	DB	82
	DB	79
	DB	82
	DB	32
	DB	83
	DB	79
	DB	85
	DB	82
	DB	67
	DB	69
	DB	58
	DB	32
	DB	84
	DB	114
	DB	97
	DB	110
	DB	115
	DB	105
	DB	116
	DB	32
	DB	109
	DB	111
	DB	100
	DB	117
	DB	108
	DB	101
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_114	DB	69
	DB	82
	DB	82
	DB	79
	DB	82
	DB	32
	DB	83
	DB	79
	DB	85
	DB	82
	DB	67
	DB	69
	DB	58
	DB	32
	DB	83
	DB	105
	DB	103
	DB	110
	DB	97
	DB	108
	DB	32
	DB	115
	DB	117
	DB	98
	DB	114
	DB	111
	DB	117
	DB	116
	DB	105
	DB	110
	DB	101
	DB	0
__STRLITPACK_116	DB	69
	DB	82
	DB	82
	DB	79
	DB	82
	DB	32
	DB	83
	DB	79
	DB	85
	DB	82
	DB	67
	DB	69
	DB	58
	DB	32
	DB	82
	DB	111
	DB	117
	DB	116
	DB	101
	DB	32
	DB	115
	DB	119
	DB	105
	DB	116
	DB	99
	DB	104
	DB	32
	DB	115
	DB	117
	DB	98
	DB	114
	DB	111
	DB	117
	DB	116
	DB	105
	DB	110
	DB	101
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_118	DB	69
	DB	82
	DB	82
	DB	79
	DB	82
	DB	32
	DB	83
	DB	79
	DB	85
	DB	82
	DB	67
	DB	69
	DB	58
	DB	32
	DB	114
	DB	101
	DB	97
	DB	100
	DB	105
	DB	110
	DB	103
	DB	32
	DB	99
	DB	111
	DB	110
	DB	103
	DB	101
	DB	115
	DB	116
	DB	105
	DB	111
	DB	110
	DB	32
	DB	112
	DB	114
	DB	105
	DB	99
	DB	105
	DB	110
	DB	103
	DB	32
	DB	99
	DB	111
	DB	110
	DB	102
	DB	105
	DB	103
	DB	117
	DB	114
	DB	97
	DB	116
	DB	105
	DB	111
	DB	110
	DB	32
	DB	102
	DB	105
	DB	108
	DB	101
	DB	0
__STRLITPACK_142	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_81	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	71
	DB	101
	DB	116
	DB	70
	DB	76
	DB	105
	DB	110
	DB	107
	DB	70
	DB	114
	DB	111
	DB	109
	DB	78
	DB	111
	DB	100
	DB	101
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_80	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	103
	DB	101
	DB	116
	DB	109
	DB	111
	DB	118
	DB	101
	DB	109
	DB	101
	DB	110
	DB	116
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_78	DB	97
	DB	108
	DB	108
	DB	111
	DB	99
	DB	97
	DB	116
	DB	101
	DB	32
	DB	100
	DB	121
	DB	110
	DB	117
	DB	115
	DB	116
	DB	95
	DB	110
	DB	101
	DB	116
	DB	119
	DB	111
	DB	114
	DB	107
	DB	95
	DB	97
	DB	114
	DB	99
	DB	95
	DB	100
	DB	101
	DB	32
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	45
	DB	32
	DB	105
	DB	110
	DB	115
	DB	117
	DB	102
	DB	102
	DB	105
	DB	99
	DB	105
	DB	101
	DB	110
	DB	116
	DB	32
	DB	109
	DB	101
	DB	109
	DB	111
	DB	114
	DB	121
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_146	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_76	DB	97
	DB	108
	DB	108
	DB	111
	DB	99
	DB	97
	DB	116
	DB	101
	DB	32
	DB	108
	DB	105
	DB	110
	DB	107
	DB	95
	DB	111
	DB	117
	DB	116
	DB	102
	DB	108
	DB	111
	DB	119
	DB	95
	DB	100
	DB	101
	DB	108
	DB	97
	DB	121
	DB	32
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	45
	DB	32
	DB	105
	DB	110
	DB	115
	DB	117
	DB	102
	DB	102
	DB	105
	DB	99
	DB	105
	DB	101
	DB	110
	DB	116
	DB	32
	DB	109
	DB	101
	DB	109
	DB	111
	DB	114
	DB	121
	DB	0
__STRLITPACK_148	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_74	DB	97
	DB	108
	DB	108
	DB	111
	DB	99
	DB	97
	DB	116
	DB	101
	DB	32
	DB	108
	DB	105
	DB	110
	DB	107
	DB	95
	DB	111
	DB	117
	DB	116
	DB	102
	DB	108
	DB	111
	DB	119
	DB	95
	DB	99
	DB	111
	DB	117
	DB	110
	DB	116
	DB	32
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	45
	DB	32
	DB	105
	DB	110
	DB	115
	DB	117
	DB	102
	DB	102
	DB	105
	DB	99
	DB	105
	DB	101
	DB	110
	DB	116
	DB	32
	DB	109
	DB	101
	DB	109
	DB	111
	DB	114
	DB	121
	DB	0
__STRLITPACK_150	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_72	DB	97
	DB	108
	DB	108
	DB	111
	DB	99
	DB	97
	DB	116
	DB	101
	DB	32
	DB	108
	DB	105
	DB	110
	DB	107
	DB	95
	DB	116
	DB	105
	DB	109
	DB	101
	DB	32
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	45
	DB	32
	DB	105
	DB	110
	DB	115
	DB	117
	DB	102
	DB	102
	DB	105
	DB	99
	DB	105
	DB	101
	DB	110
	DB	116
	DB	32
	DB	109
	DB	101
	DB	109
	DB	111
	DB	114
	DB	121
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_152	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_70	DB	97
	DB	108
	DB	108
	DB	111
	DB	99
	DB	97
	DB	116
	DB	101
	DB	32
	DB	108
	DB	105
	DB	110
	DB	107
	DB	95
	DB	113
	DB	117
	DB	101
	DB	117
	DB	101
	DB	32
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	45
	DB	32
	DB	105
	DB	110
	DB	115
	DB	117
	DB	102
	DB	102
	DB	105
	DB	99
	DB	105
	DB	101
	DB	110
	DB	116
	DB	32
	DB	109
	DB	101
	DB	109
	DB	111
	DB	114
	DB	121
	DB	0
__STRLITPACK_154	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_68	DB	97
	DB	108
	DB	108
	DB	111
	DB	99
	DB	97
	DB	116
	DB	101
	DB	32
	DB	108
	DB	105
	DB	110
	DB	107
	DB	95
	DB	81
	DB	109
	DB	112
	DB	32
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	45
	DB	32
	DB	105
	DB	110
	DB	115
	DB	117
	DB	102
	DB	102
	DB	105
	DB	99
	DB	105
	DB	101
	DB	110
	DB	116
	DB	32
	DB	109
	DB	101
	DB	109
	DB	111
	DB	114
	DB	121
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_156	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_66	DB	97
	DB	108
	DB	108
	DB	111
	DB	99
	DB	97
	DB	116
	DB	101
	DB	32
	DB	108
	DB	105
	DB	110
	DB	107
	DB	95
	DB	115
	DB	112
	DB	101
	DB	101
	DB	100
	DB	32
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	45
	DB	32
	DB	105
	DB	110
	DB	115
	DB	117
	DB	102
	DB	102
	DB	105
	DB	99
	DB	105
	DB	101
	DB	110
	DB	116
	DB	32
	DB	109
	DB	101
	DB	109
	DB	111
	DB	114
	DB	121
	DB	0
__STRLITPACK_158	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_64	DB	97
	DB	108
	DB	108
	DB	111
	DB	99
	DB	97
	DB	116
	DB	101
	DB	32
	DB	108
	DB	105
	DB	110
	DB	107
	DB	95
	DB	83
	DB	109
	DB	112
	DB	32
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	45
	DB	32
	DB	105
	DB	110
	DB	115
	DB	117
	DB	102
	DB	102
	DB	105
	DB	99
	DB	105
	DB	101
	DB	110
	DB	116
	DB	32
	DB	109
	DB	101
	DB	109
	DB	111
	DB	114
	DB	121
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_160	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_62	DB	97
	DB	108
	DB	108
	DB	111
	DB	99
	DB	97
	DB	116
	DB	101
	DB	32
	DB	108
	DB	105
	DB	110
	DB	107
	DB	95
	DB	99
	DB	111
	DB	117
	DB	110
	DB	116
	DB	32
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	45
	DB	32
	DB	105
	DB	110
	DB	115
	DB	117
	DB	102
	DB	102
	DB	105
	DB	99
	DB	105
	DB	101
	DB	110
	DB	116
	DB	32
	DB	109
	DB	101
	DB	109
	DB	111
	DB	114
	DB	121
	DB	0
__STRLITPACK_162	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_60	DB	97
	DB	108
	DB	108
	DB	111
	DB	99
	DB	97
	DB	116
	DB	101
	DB	32
	DB	101
	DB	110
	DB	116
	DB	114
	DB	121
	DB	95
	DB	115
	DB	101
	DB	114
	DB	118
	DB	105
	DB	99
	DB	101
	DB	32
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	45
	DB	32
	DB	105
	DB	110
	DB	115
	DB	117
	DB	102
	DB	102
	DB	105
	DB	99
	DB	105
	DB	101
	DB	110
	DB	116
	DB	32
	DB	109
	DB	101
	DB	109
	DB	111
	DB	114
	DB	121
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_164	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_58	DB	97
	DB	108
	DB	108
	DB	111
	DB	99
	DB	97
	DB	116
	DB	101
	DB	32
	DB	111
	DB	112
	DB	112
	DB	95
	DB	108
	DB	105
	DB	110
	DB	107
	DB	80
	DB	32
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	45
	DB	32
	DB	105
	DB	110
	DB	115
	DB	117
	DB	102
	DB	102
	DB	105
	DB	99
	DB	105
	DB	101
	DB	110
	DB	116
	DB	32
	DB	109
	DB	101
	DB	109
	DB	111
	DB	114
	DB	121
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_166	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_56	DB	97
	DB	108
	DB	108
	DB	111
	DB	99
	DB	97
	DB	116
	DB	101
	DB	32
	DB	100
	DB	101
	DB	108
	DB	97
	DB	121
	DB	115
	DB	116
	DB	101
	DB	112
	DB	32
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	45
	DB	32
	DB	105
	DB	110
	DB	115
	DB	117
	DB	102
	DB	102
	DB	105
	DB	99
	DB	105
	DB	101
	DB	110
	DB	116
	DB	32
	DB	109
	DB	101
	DB	109
	DB	111
	DB	114
	DB	121
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_168	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_54	DB	97
	DB	108
	DB	108
	DB	111
	DB	99
	DB	97
	DB	116
	DB	101
	DB	32
	DB	100
	DB	101
	DB	108
	DB	97
	DB	121
	DB	108
	DB	101
	DB	102
	DB	116
	DB	32
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	45
	DB	32
	DB	105
	DB	110
	DB	115
	DB	117
	DB	102
	DB	102
	DB	105
	DB	99
	DB	105
	DB	101
	DB	110
	DB	116
	DB	32
	DB	109
	DB	101
	DB	109
	DB	111
	DB	114
	DB	121
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_170	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_52	DB	97
	DB	108
	DB	108
	DB	111
	DB	99
	DB	97
	DB	116
	DB	101
	DB	32
	DB	65
	DB	99
	DB	99
	DB	117
	DB	86
	DB	111
	DB	108
	DB	32
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	45
	DB	32
	DB	105
	DB	110
	DB	115
	DB	117
	DB	102
	DB	102
	DB	105
	DB	99
	DB	105
	DB	101
	DB	110
	DB	116
	DB	32
	DB	109
	DB	101
	DB	109
	DB	111
	DB	114
	DB	121
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_172	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_50	DB	97
	DB	108
	DB	108
	DB	111
	DB	99
	DB	97
	DB	116
	DB	101
	DB	32
	DB	65
	DB	99
	DB	99
	DB	117
	DB	86
	DB	111
	DB	108
	DB	84
	DB	32
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	45
	DB	32
	DB	105
	DB	110
	DB	115
	DB	117
	DB	102
	DB	102
	DB	105
	DB	99
	DB	105
	DB	101
	DB	110
	DB	116
	DB	32
	DB	109
	DB	101
	DB	109
	DB	111
	DB	114
	DB	121
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_174	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_48	DB	97
	DB	108
	DB	108
	DB	111
	DB	99
	DB	97
	DB	116
	DB	101
	DB	32
	DB	65
	DB	99
	DB	99
	DB	117
	DB	86
	DB	111
	DB	108
	DB	72
	DB	32
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	45
	DB	32
	DB	105
	DB	110
	DB	115
	DB	117
	DB	102
	DB	102
	DB	105
	DB	99
	DB	105
	DB	101
	DB	110
	DB	116
	DB	32
	DB	109
	DB	101
	DB	109
	DB	111
	DB	114
	DB	121
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_176	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_46	DB	97
	DB	108
	DB	108
	DB	111
	DB	99
	DB	97
	DB	116
	DB	101
	DB	32
	DB	78
	DB	71
	DB	101
	DB	110
	DB	90
	DB	50
	DB	73
	DB	32
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	45
	DB	32
	DB	105
	DB	110
	DB	115
	DB	117
	DB	102
	DB	102
	DB	105
	DB	99
	DB	105
	DB	101
	DB	110
	DB	116
	DB	32
	DB	109
	DB	101
	DB	109
	DB	111
	DB	114
	DB	121
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_178	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_44	DB	97
	DB	108
	DB	108
	DB	111
	DB	99
	DB	97
	DB	116
	DB	101
	DB	32
	DB	99
	DB	111
	DB	115
	DB	116
	DB	101
	DB	120
	DB	112
	DB	32
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	45
	DB	32
	DB	105
	DB	110
	DB	115
	DB	117
	DB	102
	DB	102
	DB	105
	DB	99
	DB	105
	DB	101
	DB	110
	DB	116
	DB	32
	DB	109
	DB	101
	DB	109
	DB	111
	DB	114
	DB	121
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_180	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_42	DB	97
	DB	108
	DB	108
	DB	111
	DB	99
	DB	97
	DB	116
	DB	101
	DB	32
	DB	83
	DB	84
	DB	79
	DB	80
	DB	99
	DB	97
	DB	112
	DB	52
	DB	119
	DB	32
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	45
	DB	32
	DB	105
	DB	110
	DB	115
	DB	117
	DB	102
	DB	102
	DB	105
	DB	99
	DB	105
	DB	101
	DB	110
	DB	116
	DB	32
	DB	109
	DB	101
	DB	109
	DB	111
	DB	114
	DB	121
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_182	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_40	DB	97
	DB	108
	DB	108
	DB	111
	DB	99
	DB	97
	DB	116
	DB	101
	DB	32
	DB	83
	DB	84
	DB	79
	DB	80
	DB	99
	DB	97
	DB	112
	DB	50
	DB	119
	DB	32
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	45
	DB	32
	DB	105
	DB	110
	DB	115
	DB	117
	DB	102
	DB	102
	DB	105
	DB	99
	DB	105
	DB	101
	DB	110
	DB	116
	DB	32
	DB	109
	DB	101
	DB	109
	DB	111
	DB	114
	DB	121
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_184	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_38	DB	97
	DB	108
	DB	108
	DB	111
	DB	99
	DB	97
	DB	116
	DB	101
	DB	32
	DB	83
	DB	84
	DB	79
	DB	80
	DB	99
	DB	97
	DB	112
	DB	50
	DB	119
	DB	73
	DB	78
	DB	68
	DB	32
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	45
	DB	32
	DB	105
	DB	110
	DB	115
	DB	117
	DB	102
	DB	102
	DB	105
	DB	99
	DB	105
	DB	101
	DB	110
	DB	116
	DB	32
	DB	109
	DB	101
	DB	109
	DB	111
	DB	114
	DB	121
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_186	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_36	DB	97
	DB	108
	DB	108
	DB	111
	DB	99
	DB	97
	DB	116
	DB	101
	DB	32
	DB	89
	DB	105
	DB	101
	DB	108
	DB	100
	DB	67
	DB	97
	DB	112
	DB	32
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	45
	DB	32
	DB	105
	DB	110
	DB	115
	DB	117
	DB	102
	DB	102
	DB	105
	DB	99
	DB	105
	DB	101
	DB	110
	DB	116
	DB	32
	DB	109
	DB	101
	DB	109
	DB	111
	DB	114
	DB	121
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_188	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_34	DB	97
	DB	108
	DB	108
	DB	111
	DB	99
	DB	97
	DB	116
	DB	101
	DB	32
	DB	89
	DB	105
	DB	101
	DB	108
	DB	100
	DB	67
	DB	97
	DB	112
	DB	73
	DB	78
	DB	68
	DB	32
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	45
	DB	32
	DB	105
	DB	110
	DB	115
	DB	117
	DB	102
	DB	102
	DB	105
	DB	99
	DB	105
	DB	101
	DB	110
	DB	116
	DB	32
	DB	109
	DB	101
	DB	109
	DB	111
	DB	114
	DB	121
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_190	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_32	DB	97
	DB	108
	DB	108
	DB	111
	DB	99
	DB	97
	DB	116
	DB	101
	DB	32
	DB	76
	DB	111
	DB	97
	DB	100
	DB	87
	DB	101
	DB	105
	DB	103
	DB	104
	DB	116
	DB	73
	DB	68
	DB	32
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	45
	DB	32
	DB	105
	DB	110
	DB	115
	DB	117
	DB	102
	DB	102
	DB	105
	DB	99
	DB	105
	DB	101
	DB	110
	DB	116
	DB	32
	DB	109
	DB	101
	DB	109
	DB	111
	DB	114
	DB	121
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_192	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_31	DB	84
	DB	114
	DB	97
	DB	102
	DB	102
	DB	105
	DB	99
	DB	70
	DB	108
	DB	111
	DB	119
	DB	77
	DB	111
	DB	100
	DB	101
	DB	108
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_30	DB	111
	DB	108
	DB	100
	DB	0
__STRLITPACK_28	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	119
	DB	104
	DB	101
	DB	110
	DB	32
	DB	111
	DB	112
	DB	101
	DB	110
	DB	105
	DB	110
	DB	103
	DB	32
	DB	84
	DB	114
	DB	102
	DB	102
	DB	105
	DB	99
	DB	70
	DB	108
	DB	111
	DB	119
	DB	77
	DB	111
	DB	100
	DB	101
	DB	108
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_195	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_26	DB	97
	DB	108
	DB	108
	DB	111
	DB	99
	DB	97
	DB	116
	DB	101
	DB	32
	DB	86
	DB	75
	DB	70
	DB	117
	DB	110
	DB	99
	DB	32
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	45
	DB	32
	DB	105
	DB	110
	DB	115
	DB	117
	DB	102
	DB	102
	DB	105
	DB	99
	DB	105
	DB	101
	DB	110
	DB	116
	DB	32
	DB	109
	DB	101
	DB	109
	DB	111
	DB	114
	DB	121
	DB	0
__STRLITPACK_199	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_24	DB	97
	DB	108
	DB	108
	DB	111
	DB	99
	DB	97
	DB	116
	DB	101
	DB	32
	DB	108
	DB	105
	DB	110
	DB	107
	DB	95
	DB	105
	DB	100
	DB	101
	DB	110
	DB	32
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	45
	DB	32
	DB	105
	DB	110
	DB	115
	DB	117
	DB	102
	DB	102
	DB	105
	DB	99
	DB	105
	DB	101
	DB	110
	DB	116
	DB	32
	DB	109
	DB	101
	DB	109
	DB	111
	DB	114
	DB	121
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_201	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_22	DB	97
	DB	108
	DB	108
	DB	111
	DB	99
	DB	97
	DB	116
	DB	101
	DB	32
	DB	107
	DB	103
	DB	112
	DB	111
	DB	105
	DB	110
	DB	116
	DB	32
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	45
	DB	32
	DB	105
	DB	110
	DB	115
	DB	117
	DB	102
	DB	102
	DB	105
	DB	99
	DB	105
	DB	101
	DB	110
	DB	116
	DB	32
	DB	109
	DB	101
	DB	109
	DB	111
	DB	114
	DB	121
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_203	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_20	DB	97
	DB	108
	DB	108
	DB	111
	DB	99
	DB	97
	DB	116
	DB	101
	DB	32
	DB	100
	DB	121
	DB	110
	DB	117
	DB	115
	DB	116
	DB	95
	DB	110
	DB	101
	DB	116
	DB	119
	DB	111
	DB	114
	DB	107
	DB	95
	DB	97
	DB	114
	DB	99
	DB	95
	DB	110
	DB	100
	DB	101
	DB	32
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	45
	DB	32
	DB	105
	DB	110
	DB	115
	DB	117
	DB	102
	DB	102
	DB	105
	DB	99
	DB	105
	DB	101
	DB	110
	DB	116
	DB	32
	DB	109
	DB	101
	DB	109
	DB	111
	DB	114
	DB	121
	DB	0
__STRLITPACK_205	DB	0
	DB 3 DUP ( 0H)	; pad
_2il0floatpacket.23	DD	040c00000H
_2il0floatpacket.25	DD	080000000H
_2il0floatpacket.26	DD	04b000000H
_2il0floatpacket.27	DD	03f000000H
_2il0floatpacket.28	DD	0bf000000H
_2il0floatpacket.29	DD	0437a0000H
__STRLITPACK_18	DB	97
	DB	108
	DB	108
	DB	111
	DB	99
	DB	97
	DB	116
	DB	101
	DB	32
	DB	100
	DB	121
	DB	110
	DB	117
	DB	115
	DB	116
	DB	95
	DB	110
	DB	101
	DB	116
	DB	119
	DB	111
	DB	114
	DB	107
	DB	95
	DB	110
	DB	111
	DB	100
	DB	101
	DB	32
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	45
	DB	32
	DB	105
	DB	110
	DB	115
	DB	117
	DB	102
	DB	102
	DB	105
	DB	99
	DB	105
	DB	101
	DB	110
	DB	116
	DB	32
	DB	109
	DB	101
	DB	109
	DB	111
	DB	114
	DB	121
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_207	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_16	DB	97
	DB	108
	DB	108
	DB	111
	DB	99
	DB	97
	DB	116
	DB	101
	DB	32
	DB	110
	DB	111
	DB	100
	DB	101
	DB	32
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	45
	DB	32
	DB	105
	DB	110
	DB	115
	DB	117
	DB	102
	DB	102
	DB	105
	DB	99
	DB	105
	DB	101
	DB	110
	DB	116
	DB	32
	DB	109
	DB	101
	DB	109
	DB	111
	DB	114
	DB	121
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_209	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_14	DB	97
	DB	108
	DB	108
	DB	111
	DB	99
	DB	97
	DB	116
	DB	101
	DB	32
	DB	105
	DB	67
	DB	111
	DB	110
	DB	90
	DB	111
	DB	110
	DB	101
	DB	32
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	45
	DB	32
	DB	105
	DB	110
	DB	115
	DB	117
	DB	102
	DB	102
	DB	105
	DB	99
	DB	105
	DB	101
	DB	110
	DB	116
	DB	32
	DB	109
	DB	101
	DB	109
	DB	111
	DB	114
	DB	121
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_211	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_12	DB	97
	DB	108
	DB	108
	DB	111
	DB	99
	DB	97
	DB	116
	DB	101
	DB	32
	DB	66
	DB	97
	DB	99
	DB	107
	DB	80
	DB	111
	DB	105
	DB	110
	DB	116
	DB	114
	DB	32
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	45
	DB	32
	DB	105
	DB	110
	DB	115
	DB	117
	DB	102
	DB	102
	DB	105
	DB	99
	DB	105
	DB	101
	DB	110
	DB	116
	DB	32
	DB	109
	DB	101
	DB	109
	DB	111
	DB	114
	DB	121
	DB	0
__STRLITPACK_213	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_10	DB	97
	DB	108
	DB	108
	DB	111
	DB	99
	DB	97
	DB	116
	DB	101
	DB	32
	DB	79
	DB	117
	DB	116
	DB	84
	DB	111
	DB	73
	DB	110
	DB	78
	DB	111
	DB	100
	DB	101
	DB	78
	DB	117
	DB	109
	DB	32
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	45
	DB	32
	DB	105
	DB	110
	DB	115
	DB	117
	DB	102
	DB	102
	DB	105
	DB	99
	DB	105
	DB	101
	DB	110
	DB	116
	DB	32
	DB	109
	DB	101
	DB	109
	DB	111
	DB	114
	DB	121
	DB	0
__STRLITPACK_215	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_8	DB	97
	DB	108
	DB	108
	DB	111
	DB	99
	DB	97
	DB	116
	DB	101
	DB	32
	DB	100
	DB	121
	DB	110
	DB	117
	DB	115
	DB	116
	DB	95
	DB	110
	DB	101
	DB	116
	DB	119
	DB	111
	DB	114
	DB	107
	DB	95
	DB	110
	DB	111
	DB	100
	DB	101
	DB	95
	DB	110
	DB	100
	DB	101
	DB	32
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	45
	DB	32
	DB	105
	DB	110
	DB	115
	DB	117
	DB	102
	DB	102
	DB	105
	DB	99
	DB	105
	DB	101
	DB	110
	DB	116
	DB	32
	DB	109
	DB	101
	DB	109
	DB	111
	DB	114
	DB	121
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_217	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_6	DB	97
	DB	108
	DB	108
	DB	111
	DB	99
	DB	97
	DB	116
	DB	101
	DB	32
	DB	99
	DB	111
	DB	110
	DB	110
	DB	101
	DB	99
	DB	116
	DB	105
	DB	118
	DB	105
	DB	116
	DB	121
	DB	32
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	45
	DB	32
	DB	105
	DB	110
	DB	115
	DB	117
	DB	102
	DB	102
	DB	105
	DB	99
	DB	105
	DB	101
	DB	110
	DB	116
	DB	32
	DB	109
	DB	101
	DB	109
	DB	111
	DB	114
	DB	121
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_219	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_4	DB	97
	DB	108
	DB	108
	DB	111
	DB	99
	DB	97
	DB	116
	DB	101
	DB	32
	DB	100
	DB	121
	DB	110
	DB	117
	DB	115
	DB	116
	DB	95
	DB	110
	DB	101
	DB	116
	DB	119
	DB	111
	DB	114
	DB	107
	DB	95
	DB	97
	DB	114
	DB	99
	DB	109
	DB	111
	DB	118
	DB	101
	DB	95
	DB	100
	DB	101
	DB	32
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	45
	DB	32
	DB	105
	DB	110
	DB	115
	DB	117
	DB	102
	DB	102
	DB	105
	DB	99
	DB	105
	DB	101
	DB	110
	DB	116
	DB	32
	DB	109
	DB	101
	DB	109
	DB	111
	DB	114
	DB	121
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_221	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_2	DB	97
	DB	108
	DB	108
	DB	99
	DB	111
	DB	97
	DB	116
	DB	101
	DB	32
	DB	110
	DB	115
	DB	105
	DB	103
	DB	110
	DB	32
	DB	101
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	45
	DB	32
	DB	105
	DB	110
	DB	115
	DB	117
	DB	102
	DB	102
	DB	105
	DB	99
	DB	105
	DB	101
	DB	110
	DB	116
	DB	32
	DB	109
	DB	101
	DB	109
	DB	111
	DB	114
	DB	121
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_223	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_225	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
EXTRN	_DYNUST_MAIN_MODULE_mp_MAXMOVE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NOOF_MASTER_DESTINATIONS:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_LONGEST_LINK:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_MAXLINKVEH:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ITERATION:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NOOFNODES:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NOOFFLOWMODEL:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NZONES:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_MOVE2N:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_LEVEL2N:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NMOVE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NLEVEL:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_SIMPERIOD:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TDSPSTEP:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_SIMPERAGG:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_AGGINT:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NOOFARCS:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ERROR:BYTE
	COMM _DYNUST_NETWORK_MODULE_mp_ICONZONEPAR:BYTE:4
	COMM _DYNUST_NETWORK_MODULE_mp_MAXDENPAR:BYTE:4
	COMM _DYNUST_NETWORK_MODULE_mp_AGGINDEX:BYTE:4
_DATA	ENDS
EXTRN	_for_deallocate:PROC
EXTRN	_for_dealloc_allocatable:PROC
EXTRN	_for_close:PROC
EXTRN	_for_read_seq_lis:PROC
EXTRN	_for_open:PROC
EXTRN	_for_alloc_allocatable:PROC
EXTRN	_for_check_mult_overflow:PROC
EXTRN	_for_stop_core:PROC
EXTRN	_for_write_seq_lis:PROC
EXTRN	_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKATT_2DSETUP:PROC
EXTRN	_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKTEMP_2DSETUP:PROC
EXTRN	_DYNUST_LINK_VEH_LIST_MODULE_mp_LINKATT_SETUP:PROC
EXTRN	_DYNUST_LINK_VEH_LIST_MODULE_mp_ENQATT_2DSETUP:PROC
EXTRN	_DYNUST_LINK_VEH_LIST_MODULE_mp_ENQATT_SETUP:PROC
EXTRN	_DYNUST_LINK_VEH_LIST_MODULE_mp_TCHAINATT_2DSETUP:PROC
EXTRN	_DYNUST_LINK_VEH_LIST_MODULE_mp_TCHAINATT_SETUP:PROC
EXTRN	_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_2DSETUP:PROC
EXTRN	_DYNUST_TRANSLINK_MODULE_mp_TRANLINK_SETUP:PROC
EXTRN	__alldiv:PROC
EXTRN	__intel_fast_memset:PROC
EXTRN	__fltused:BYTE
	INCLUDELIB <ifconsol>
	INCLUDELIB <libifcoremt>
	INCLUDELIB <libifport>
	INCLUDELIB <libmmt>
	INCLUDELIB <LIBCMT>
	INCLUDELIB <libirc>
	INCLUDELIB <svml_dispmt>
	INCLUDELIB <OLDNAMES>
	END
