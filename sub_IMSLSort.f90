SUBROUTINE IMSLSort(AIN,ROWS,COLS)
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~ Copyright (C) 2002-2011 The Arizona Board of Regents on Behalf of The University of Arizona.  ~~!
!~~ Developed by Yi-Chang Chiu, Ph.D. as par to the DynusT project.                               ~~!
!~~ This program is free software: you can redistribute it and/or modify it                       ~~!
!~~ under the terms of the GNU General Public License version 2 as published by                   ~~!
!~~ the Free Software Foundation.                                                                 ~~!
!~~ This program is distributed in the hope that it will be useful,                               ~~!
!~~ but WITHOUT ANY WARRANTY; without even the implied warranty of                                ~~!
!~~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                                          ~~!
!~~ See the GNU General Public License for more details.                                          ~~!
!~~ You should have received a copy of the GNU General Public License                             ~~!
!~~ along with this program (in the file COPYING).                                                ~~!
!~~ IF not, see <http://www.gnu.org/licenses/>.                                                   ~~!
!~~ Please see http://www.dynust.net for contact information and names                            ~~!
!~~ of individual contributors.                                                                   ~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!

INCLUDE 'link_f90_static.h'	
!use imsl_libraries
USE SROWR_INT

INTEGER I,J,K,INDEX,STAT
INTEGER COLS, ROWS, NGROUP

INTEGER, INTENT(inout), DIMENSION(ROWS,COLS) :: AIN
REAL, ALLOCATABLE :: A(:,:)
INTEGER, ALLOCATABLE :: IPERM(:),INDKEY(:),NI(:) 
INTEGER ERR

!> > > > > > > > > >                < < < < < < < < < < <
!>>>>>>>>>>>>>>>>>>>CODES START HERE<<<<<<<<<<<<<<<<<<<<<
!> > > > > > > > > >                < < < < < < < < < < <

    IF(ROWS > 0)THEN
    
	ALLOCATE(INDKEY(COLS), STAT = ERR)
	IF(ERR /= 0) WRITE(911,*) 'allocate INDKEY error in IMSLSort', STOP
    DO I = 1, COLS
      INDKEY(I) = i
    ENDDO
	ALLOCATE(IPERM(ROWS), STAT = ERR)
	IF(ERR /= 0) WRITE(911,*) 'allocate IPERM error in IMSLSort', STOP
    IPERM = 0 ! syntax

	ALLOCATE(NI(ROWS), STAT = ERR)
	IF(ERR /= 0) WRITE(911,*) 'allocate NI error in IMSLSort', STOP
    NI = 0
    
	ALLOCATE(A(ROWS,COLS), STAT = ERR)
	IF(ERR /= 0) WRITE(911,*) 'allocate A error in IMSLSort', STOP

    A = 0
	A = AIN ! assign fortran array to c array
       
    CALL SROWR(A,INDKEY,IPERM,NGROUP,NI)
   
    AIN = A
    IF(ALLOCATED(A)) DEALLOCATE(A)
	IF(ALLOCATED(INDKEY)) DEALLOCATE(INDKEY)
	IF(ALLOCATED(IPERM)) DEALLOCATE(IPERM)
	IF(ALLOCATED(NI)) DEALLOCATE(NI)

 ENDIF !ROWS > 0
 
    END SUBROUTINE 
