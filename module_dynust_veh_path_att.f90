MODULE DYNUST_VEH_PATH_ATT_MODULE
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~ Copyright (C) 2002-2011 The Arizona Board of Regents on Behalf of The University of Arizona.  ~~!
!~~ Developed by Yi-Chang Chiu, Ph.D. as par to the DynusT project.                               ~~!
!~~ This program is free software: you can redistribute it and/or modify it                       ~~!
!~~ under the terms of the GNU General Public License version 2 as published by                   ~~!
!~~ the Free Software Foundation.                                                                 ~~!
!~~ This program is distributed in the hope that it will be useful,                               ~~!
!~~ but WITHOUT ANY WARRANTY; without even the implied warranty of                                ~~!
!~~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                                          ~~!
!~~ See the GNU General Public License for more details.                                          ~~!
!~~ You should have received a copy of the GNU General Public License                             ~~!
!~~ along with this program (in the file COPYING).                                                ~~!
!~~ IF not, see <http://www.gnu.org/licenses/>.                                                   ~~!
!~~ Please see http://www.dynust.net for contact information and names                            ~~!
!~~ of individual contributors.                                                                   ~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!

TYPE pathAttribute ! Define the data structure for generic vehicles
  INTEGER PathNode ! Node numbers
END TYPE pathAttribute

TYPE Attribute ! Define the data structure for generic vehicles
  INTEGER    PathSTOP ! STOP time at each node
  INTEGER    PathTime ! Time stamp at each node
  INTEGER    HistArrTime ! HISTORICAL NODE ARRIVAL TIME
END TYPE Attribute

TYPE MEMBER
  TYPE(pathAttribute), POINTER :: P(:) ! size ASSOCIATED with the path length
  TYPE(Attribute), POINTER     :: A(:) ! size ASSOCIATED with the path length  
  INTEGER(2)                   :: PSize ! size of the ALLOCATED P()
  INTEGER(2)                   :: AcSize
END TYPE MEMBER
   
TYPE(MEMBER), ALLOCATABLE :: vehatt_Array(:) ! Declare generic vehicle arrays

INTEGER ::v_IncreaseSize = 50 ! increment for P() 
INTEGER vehatt_ArraySize
INTEGER vectorerror

INTEGER, ALLOCATABLE::VehAin(:,:) !1: dest, 2: orig, 3: link id, 4: veh id
INTEGER, ALLOCATABLE::VehLod(:,:) !1: vehid, 2: dest
INTEGER, ALLOCATABLE::VehLodtmp(:,:) !1: vehid, 2: dest
LOGICAL, ALLOCATABLE::vehlodzone(:)

INTEGER vlgcounter
INTEGER :: VehAinCounter =50000
INTEGER ::VehAinIncre = 10000
INTEGER mop, demandmidcounter
LOGICAL, ALLOCATABLE::DestDemandOK(:)

CONTAINS

!~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>
! *** Start of vehatt_Array implementation ***
! This initialize the initial path arrays for vehicles
SUBROUTINE vehatt_2DSetup(Size)

  INTEGER Size
  INTEGER :: vi
! Remove existing array
  IF (ALLOCATED(vehatt_Array)) THEN
    CALL vehatt_2DRemove()
  ENDIF

! Initialize increase size.
! Setup new array
  IF (.NOT. ALLOCATED(vehatt_Array)) THEN
    ALLOCATE(vehatt_Array(Size),stat=vectorerror)
	IF(vectorerror /= 0) THEN
 	  WRITE(911,*)"VhcAtt Setup error"
	  STOP
	ENDIF
  ENDIF
  
! Initialize new size for each element
   vehatt_Array(1:Size)%PSize = 0
   vehatt_Array(1:Size)%AcSize = 0
   vehatt_ArraySIZE = Size
END SUBROUTINE 


!~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>
! this SUBROUTINE copies the existing array into a longer one
SUBROUTINE vehatt_P_Setup(it,NewSize)
	
 INTEGER it, NewSize
! TYPE(pathAttribute), POINTER :: tempP(:)
 TYPE(pathAttribute),ALLOCATABLE :: tempP(:)
 INTEGER :: vi
 INTEGER :: OldSize
 OldSize = 0
	
! create temp pointer to store contents of array
IF (vehatt_Array(it)%PSize > 0 ) THEN
     OldSize = vehatt_Array(it)%PSize
     ALLOCATE(tempP(vehatt_Array(it)%PSize),stat=vectorerror)
	 IF(vectorerror /= 0) THEN
	   WRITE(911,*) 'allocate tmpP vectorerror'
	   STOP
	 ENDIF

! Copy content of old array to temp pointer
   tempP(1:vehatt_Array(it)%PSize)%PathNode = vehatt_Array(it)%P(1:vehatt_Array(it)%PSize)%PathNode 
  
! Delete the old array
     IF (ASSOCIATED(vehatt_Array(it)%P)) THEN
	  DEALLOCATE(vehatt_Array(it)%P,stat=vectorerror)
	  IF(vectorerror /= 0) THEN
	    WRITE(911,*)"DEALLOCATE vehatt_1DArray vector error"
	    STOP
	  ENDIF
     ENDIF
! REALlocate array
   ALLOCATE(vehatt_Array(it)%P(NewSize),stat=vectorerror)
	IF(vectorerror /= 0) THEN
      WRITE(911,*)"allocate vehatt_Array%P vector error"
	  STOP
	ENDIF
   
! Copy contents from temp back to array 
   vehatt_Array(it)%P(1:OldSize)%PathNode = tempP(1:OldSize)%PathNode

! initialize array for the remaining elements
   vehatt_Array(it)%P(OldSize+1:NewSize)%PathNode = 0

   vehatt_Array(it)%PSize = NewSize
   IF(ALLOCATED(tempP)) DEALLOCATE(tempP)

ELSE

! REALlocate array
   ALLOCATE(vehatt_Array(it)%P(NewSize),stat=vectorerror)
	IF(vectorerror /= 0) THEN
      WRITE(911,*)"allocate vehatt_Array%P vector error"
	  STOP
	ENDIF

! initialize array for the remaining elements
      vehatt_Array(it)%P(OldSize+1:NewSize)%PathNode = 0
	  vehatt_Array(it)%PSize = NewSize
ENDIF
END SUBROUTINE 

!~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>
SUBROUTINE vehatt_A_Setup(it,NewSize)
	
 INTEGER it, NewSize
 TYPE(Attribute), ALLOCATABLE :: tempA(:)
 INTEGER :: vi
 INTEGER :: OldSize
 OldSize = 0
	
! create temp pointer to store contents of array
IF (vehatt_Array(it)%AcSize > 0) THEN ! this array has already existed
     OldSize = vehatt_Array(it)%AcSize
     ALLOCATE(tempA(max(OldSize,NewSize)),stat=vectorerror)
	 IF(vectorerror /= 0) THEN
	   WRITE(911,*) 'allocate tmpA vectorerror'
	   STOP
	 ENDIF

! Copy content of old array to temp pointer

    tempA(1:vehatt_Array(it)%AcSize)%PathSTOP     = vehatt_Array(it)%A(1:vehatt_Array(it)%AcSize)%PathSTOP
    tempA(1:vehatt_Array(it)%AcSize)%PathTime     = vehatt_Array(it)%A(1:vehatt_Array(it)%AcSize)%PathTime 
    tempA(1:vehatt_Array(it)%AcSize)%HistArrTime  = vehatt_Array(it)%A(1:vehatt_Array(it)%AcSize)%HistArrTime

! Delete the old array
     IF (ASSOCIATED(vehatt_Array(it)%A)) THEN
	  DEALLOCATE(vehatt_Array(it)%A,stat=vectorerror)
	  IF(vectorerror /= 0) THEN
	    WRITE(911,*)"DEALLOCATE vehatt_Array%A vector error"
	    STOP
	  ENDIF
     ENDIF

! REALlocate array
   ALLOCATE(vehatt_Array(it)%A(max(OldSize,NewSize)),stat=vectorerror)
	IF(vectorerror /= 0) THEN
      WRITE(911,*)"allocate vehatt_Array%A vector error"
	  STOP
	ENDIF
   
! Copy contents from temp back to array 
     vehatt_Array(it)%A(1:OldSize)%PathSTOP = tempA(1:OldSize)%PathSTOP
     vehatt_Array(it)%A(1:OldSize)%PathTime = tempA(1:OldSize)%PathTime
     vehatt_Array(it)%A(1:OldSize)%HistArrTime = tempA(1:OldSize)%HistArrTime

! initialize array for the remaining elements
      vehatt_Array(it)%A(OldSize+1:NewSize)%PathSTOP = 0
      vehatt_Array(it)%A(OldSize+1:NewSize)%PathTime = 0
      vehatt_Array(it)%A(OldSize+1:NewSize)%HistArrTime = 0

   IF(ALLOCATED(tempA)) DEALLOCATE(tempA)

ELSE !AcSize  = 0
   !print *, "newsize, oldsize", newsize, oldsize
   ALLOCATE(vehatt_Array(it)%A(NewSize),stat=vectorerror)
	IF(vectorerror /= 0) THEN
      WRITE(911,*)"allocate vehatt_Array%A vector error"
	  STOP
	ENDIF
! initialize array for the remaining elements
   vehatt_Array(it)%A(OldSize+1:NewSize)%PathSTOP = 0
   vehatt_Array(it)%A(OldSize+1:NewSize)%PathTime = 0
   vehatt_Array(it)%A(OldSize+1:NewSize)%HistArrTime = 0
ENDIF
END SUBROUTINE 


!~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>
! This SUBROUTINE inserts a new element into the vehicle attribute array
SUBROUTINE vehatt_Insert(it, Index1D, AttNo, eValue)
  USE DYNUST_MAIN_MODULE
  
  INTEGER it, AttNo, NewSize,itocheck 
  INTEGER(2) Index1D
  REAL    eValue
  itocheck = vehatt_Array(it)%PSize
  IF (AttNo == 1.and.Index1D > itocheck) THEN
	 NewSize = Index1D + v_IncreaseSize 
     CALL vehatt_P_Setup(it,NewSize)
     CALL vehatt_A_Setup(it,NewSize) ! non initial setup
  ENDIF
  IF (AttNo == 1) THEN
    vehatt_Array(it)%P(Index1D)%PathNode = nint(eValue)
  ELSEIF (AttNo == 2) THEN
    vehatt_Array(it)%A(Index1D)%PathSTOP = nint(eValue*AttScale)
  ELSEIF (AttNo == 3) THEN
    vehatt_Array(it)%A(Index1D)%PathTime = nint(eValue*AttScale)
  ELSEIF (AttNo == 4) THEN
    vehatt_Array(it)%A(Index1D)%HistArrTime = nint(eValue*AttScale)
  ENDIF
END SUBROUTINE 

!~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>
! This function returns a value
REAL FUNCTION vehatt_Value(it,Index1D,AttNo)
  INTEGER it, Index1D, AttNo
  REAL Value
  IF (Index1D > vehatt_Array(it)%PSize) THEN
     WRITE(911,*)"VhcAtt GetValue vector error", it
     WRITE(911,*) 'Index1D =', Index1D
	 WRITE(911,*) 'vehatt_Array(it)%PSize =',vehatt_Array(it)%PSize
     STOP
  ENDIF

  IF (AttNo == 1) THEN
     vehatt_Value = float(vehatt_Array(it)%P(Index1D)%PathNode)
  ELSEIF (AttNo == 2) THEN
     vehatt_Value = vehatt_Array(it)%A(Index1D)%PathSTOP
  ELSEIF (AttNo == 3) THEN
     vehatt_Value = vehatt_Array(it)%A(Index1D)%PathTime
  ELSEIF (AttNo == 4) THEN
     vehatt_Value = vehatt_Array(it)%A(Index1D)%HistArrTime
  ELSE
    WRITE(911,*) 'get vehatt_value error'
    STOP
  ENDIF
  IF(AttNo == 1.and.vehatt_Value < 1) THEN
    WRITE(911,*) "Return vehatt_Value error"
	STOP
  ENDIF
END FUNCTION 

!~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>
SUBROUTINE vehatt_Active_Size(it,isize)
   vehatt_array(it)%AcSize = isize
END SUBROUTINE

INTEGER FUNCTION vehatt_P_Size(it)
   vehatt_P_Size = vehatt_array(it)%AcSize
END FUNCTION 

INTEGER FUNCTION vehatt_A_Size(it)
   vehatt_A_Size = vehatt_Array(it)%AcSize
END FUNCTION 

!~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>
SUBROUTINE vehatt_P_Remove(it)
 
  INTEGER it
  IF (vehatt_Array(it)%PSize > 0) THEN
    DEALLOCATE(vehatt_Array(it)%P,stat=vectorerror)
	IF(vectorerror /= 0) THEN
	  WRITE(911,*)"DEALLOCATE vehatt_Array%P vectorerror"
	  WRITE(911,*) it
	  STOP
	ENDIF
    vehatt_Array(it)%PSize = 0 
  ENDIF
END SUBROUTINE 

!~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>
SUBROUTINE vehatt_A_Remove(it)
  INTEGER it
   IF (vehatt_Array(it)%AcSize > 0) THEN
    DEALLOCATE(vehatt_Array(it)%A,stat=vectorerror)
	IF(vectorerror /= 0) THEN
	  WRITE(911,*)"DEALLOCATE vehatt_Array%A vectorerror"
	  WRITE(911,*) it
	  STOP
	ENDIF
    vehatt_Array(it)%AcSize = 0 
  ENDIF
END SUBROUTINE 


!~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>
SUBROUTINE vehatt_Clear(it,start)
  INTEGER it
  INTEGER start
  INTEGER vi
! Clean the remaining elements     
     vehatt_Array(it)%P(start:vehatt_Array(it)%PSize)%PathNode = 0 
     vehatt_Array(it)%A(start:vehatt_Array(it)%PSize)%PathSTOP = 0 
     vehatt_Array(it)%A(start:vehatt_Array(it)%PSize)%PathTime = 0 
     vehatt_Array(it)%A(start:vehatt_Array(it)%PSize)%HistArrtime = 0      
END SUBROUTINE 

!~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>
SUBROUTINE vehatt_2DRemove()
 
  INTEGER :: vi
! Remove every element
  DO vi = 1, vehatt_ArraySize
    CALL vehatt_A_Remove(vi) 
    CALL vehatt_P_Remove(vi)     
  ENDDO
! Remove entire array
  IF (ALLOCATED(vehatt_Array)) THEN
    DEALLOCATE(vehatt_Array,stat=vectorerror)
	IF(vectorerror /= 0) THEN
 	  WRITE(911,*) "VhcAtt Destory"
 	  STOP
	ENDIF
  ENDIF
  vehatt_ArraySize = 0
END SUBROUTINE 
    
END MODULE 
