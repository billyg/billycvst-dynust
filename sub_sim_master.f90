     SUBROUTINE SIM_MASTER(l,t,edtime)
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~ Copyright (C) 2002-2011 The Arizona Board of Regents on Behalf of The University of Arizona.  ~~!
!~~ Developed by Yi-Chang Chiu, Ph.D. as par to the DynusT project.                               ~~!
!~~ This program is free software: you can redistribute it and/or modify it                       ~~!
!~~ under the terms of the GNU General Public License version 2 as published by                   ~~!
!~~ the Free Software Foundation.                                                                 ~~!
!~~ This program is distributed in the hope that it will be useful,                               ~~!
!~~ but WITHOUT ANY WARRANTY; without even the implied warranty of                                ~~!
!~~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                                          ~~!
!~~ See the GNU General Public License for more details.                                          ~~!
!~~ You should have received a copy of the GNU General Public License                             ~~!
!~~ along with this program (in the file COPYING).                                                ~~!
!~~ IF not, see <http://www.gnu.org/licenses/>.                                                   ~~!
!~~ Please see http://www.dynust.net for contact information and names                            ~~!
!~~ of individual contributors.                                                                   ~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
      USE DYNUST_MAIN_MODULE
      USE DYNUST_NETWORK_MODULE
      USE DYNUST_AMS_MODULE
      USE OMP_LIB
      
      INTEGER edtime,i
      REAL t0, t1
!> > > > > > > > > >                < < < < < < < < < < <
!>>>>>>>>>>>>>>>>>>>CODES START HERE<<<<<<<<<<<<<<<<<<<<<
!> > > > > > > > > >                < < < < < < < < < < <

      tend=t+xminPerSimInt

      CALL CPU_TIME(t0)
      CALL SIM_LOAD_VEHICLES(t)

      CALL FLOW_MODEL_UPDATE_AMTS ! OpenMP inside

      iso = mod(l,simPerAgg)
      IF(iso == 0) iso = simPerAgg
      DO i=1,noofarcs
        m_dynust_network_arc_de(i)%link_smp(iso) = m_dynust_network_arc_de(i)%v*AttScale
      ENDDO

	  CALL SIGNAL_INTERSECTION_CONTROL(t)
      CALL CAL_LINK_DISCHARGE_RATE(t)
	  CALL SIM_MOVE_VEHICLES(t,l)
      CALL CPU_Time(t1)


      CALL SIM_TRANSFER_VEHICLES(l,t,tend,edtime)

     IF((iteMax == 0.or.(iteMax > 0.and.(iteration == iteMax.or.reach_converg)))) CALL CAL_LINK_MOE(l,t,tend)

     END SUBROUTINE



