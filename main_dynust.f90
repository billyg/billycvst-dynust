PROGRAM DYNUSTMAIN
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~ Copyright (C) 2002-2011 The Arizona Board of Regents on Behalf of The University of Arizona.  ~~!
!~~ Developed by Yi-Chang Chiu, Ph.D. as par to the DynusT project.                               ~~!
!~~ This program is free software: you can redistribute it and/or modify it                       ~~!
!~~ under the terms of the GNU General Public License version 2 as published by                   ~~!
!~~ the Free Software Foundation.                                                                 ~~!
!~~ This program is distributed in the hope that it will be useful,                               ~~!
!~~ but WITHOUT ANY WARRANTY; without even the implied warranty of                                ~~!
!~~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                                          ~~!
!~~ See the GNU General Public License for more details.                                          ~~!
!~~ You should have received a copy of the GNU General Public License                             ~~!
!~~ along with this program (in the file COPYING).                                                ~~!
!~~ IF not, see <http://www.gnu.org/licenses/>.                                                   ~~!
!~~ Please see http://www.dynust.net for contact information and names                            ~~!
!~~ of individual contributors.                                                                   ~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!

!DEC$ ATTRIBUTES DEFAULT :: WinApp3Sub

  !USE USER32
  !USE IFQWIN
  !USE IFLOGM
  USE DYNUST_MAIN_MODULE
  USE DYNUST_VEH_MODULE
  USE DYNUST_VEH_PATH_ATT_MODULE
  USE DYNUST_LINK_VEH_LIST_MODULE
  USE MEMORY_MODULE
  USE RAND_GEN_INT
  

  INTERFACE
      SUBROUTINE printscreen(printstr,intx,inty,dx1,dy1,dx2,dy2,refresh)
       INTEGER intx, inty, refresh,dx1,dy1,dx2,dy2
       CHARACTER (80) printstr
      END SUBROUTINE 
  END INTERFACE   
  INTERFACE
    SUBROUTINE SIM_DYNUST(MaxI)
      INTEGER MAXI
    END SUBROUTINE
  END INTERFACE  
  INTERFACE
    SUBROUTINE MIVAGFV(mode_flag,MaxI)
      INTEGER mode_flag, MaxI
    END SUBROUTINE
  END INTERFACE
  
  REAL time_x0,time_x1, time_x2, time_x3, time_x4, time_x5, time_x6, time_x7, TA(2), cputime
  REAL Totime_sim, Totime_asg
  
  CHARACTER(80) printstr1
  CHARACTER *1 reply
  LOGICAL EXT1, EXT2, EXT0,EOFRESULT
  INTEGER dynust_mode_flag
  INTEGER,save:: MaxIntervals=0
  CHARACTER (50) file_name1   
  INTEGER message

  dynust_mode_flag = 0
  
  CALL CPU_TIME ( time_x0 )  

  CALL winsetup
  Totime_sim = 0
  Totime_asg = 0
    
  iteration = 0
  maxtime = 0
  soint = 0

! --- ----------------------------------  
  IF(logout > 0) WRITE(711,*) 'CALL DynusT'
  CALL CPU_TIME ( time_x1 )  
  CALL SIM_DYNUST(MaxIntervals)
  CALL CPU_TIME ( time_x2 )   
  totime_sim = Totime_sim+ time_x2-time_x1
! --------------------------------------

     DO WHILE(iteration < iteMax.and.(iso_ok == 1.or.iue_ok == 1)) 
      CALL CLOSEfile      
      
      TotalViolation = 0.0
      iteration = iteration + 1
      IF(iteration == 1.and.vhpcefactor < 1.0) THEN
       DO ih = 1, justveh 
         r = ranxy(16) 
         isee = IFix(r*(iteMax*vhpceReturn))+1
         isee = min(iteMax,isee)
         isee = max(1,isee)
       ENDDO
      ENDIF


      IF(CALLksp1) THEN
        CALL DEALLOCATE_SP1
        !CALL DEALLOCATE_SP2
        CALLksp1=.false.
	  ENDIF

      IF(iue_ok > 0) THEN !IF UE vehicles exist
	    IF(logout > 0) WRITE(711,*) "UE-KSP ..."
        RESULT = SETCOLORRGB(Z'a00000')          
        RESULT = RECTANGLE ($GFILLINTERIOR, 0, 395, 550, 500)
	    dynust_mode_flag = 3
		 IF(logout > 0) WRITE(711,*) "UE_lov assignment ..."
         printstr1 = 'Dynamic UE Assignment'
         CALL printscreen(printstr1,20,395,0,0,0,0,0)		 

         CALL CPU_TIME (time_x3)

        IF(NoofPS > 0) THEN
          IF(MOD(iteration,iteMax/NoofPS) == 0) THEN
              ! CALL DEPARTURE TIME CHOICE CODES
          ELSE
            CALL MIVAGFV(dynust_mode_flag,MaxIntervals)
          ENDIF
        ELSE
            CALL MIVAGFV(dynust_mode_flag,MaxIntervals)
        ENDIF
       
        CALL CPU_TIME (time_x4)           
         Totime_asg = totime_asg + time_x4-time_x3 
         IF(total_hov > 0.00011) THEN
 	      IF(logout > 0) WRITE(711,*) "UE_hov assignment ..."
        ENDIF

      ENDIF
      IF(mtc_veh(2) > 0) THEN
!      Currently no SO here
      ENDIF


      CALL OPENFILE

      jtotal = justveh

     CALL CPU_TIME (time_x4)
     CALL SIM_DYNUST(maxintervals)
     CALL CPU_TIME (time_x5)
     Totime_sim = totime_sim + time_x5-time_x4
     
     IF(reach_converg) THEN 
       exit ! gap function convergence criteria is met
     ENDIF
! ----------------------------------
	ENDDO
! --  End of MIVA loop
! ----------------------------------

     CALL CLOSEfile
     CLOSE(912)

     OPEN(unit=9999,file='SimPeriod.opt',status="unknown")
	 WRITE(9999,'(f8.1)') MaxIntervals*xminPerSimInt
	 CLOSE(9999)

!     CALL system('del .\executing mtcph.itf binpath.bip binlabel.bip fort.6666 fort.6667')
     CALL system('del .\executing binpath.bip binlabel.bip fort.6666 fort.6667')     
   
     RESULT = SETEXITQQ(QWIN$EXITNOPERSIST)

     CALL CPU_TIME (time_x7)
     
     cputime = DTIME(TA)

     WRITE(180,*)
     WRITE(180,'(" CPU Times of operation were   ", f12.2, " seconds  ")') time_x7-time_x0
     WRITE(180,'(" CPU Times of simulation were  ", f12.2, " seconds  ", f6.2," % of total)")') totime_sim, totime_sim/(time_x7-time_x0)*100
     WRITE(180,'(" CPU Times of assignment were  ", f12.2, " seconds  ", f6.2," % of total)")') totime_asg, totime_asg/(time_x7-time_x0)*100
     WRITE(180,'(" CPU Times of others were      ", f12.2, " seconds  ", f6.2," % of total)")') (time_x7-time_x0)-(totime_sim+totime_asg),((time_x7-time_x0)-(totime_sim+totime_asg))/(time_x7-time_x0)*100
     WRITE(180,'(" CPU Time of operation was             ", f12.2, " seconds by DTime")') cputime     
     WRITE(180,'(" User and System Time of operation was ", 2f12.2, " seconds by DTime")') ta(1),ta(2)
     END
