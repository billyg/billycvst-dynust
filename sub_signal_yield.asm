; mark_description "Intel(R) Visual Fortran Compiler XE for applications running on IA-32, Version 13.1.1.171 Build 20130313";
; mark_description "-S";
	.686P
 	.387
	OPTION DOTNAME
	ASSUME	CS:FLAT,DS:FLAT,SS:FLAT
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
TXTST0:
; -- Begin  _SIGNAL_YIELD
; mark_begin;
IF @Version GE 800
  .MMX
ELSEIF @Version GE 612
  .MMX
  MMWORD TEXTEQU <QWORD>
ENDIF
IF @Version GE 800
  .XMM
ELSEIF @Version GE 614
  .XMM
  XMMWORD TEXTEQU <OWORD>
ENDIF
       ALIGN     16
	PUBLIC _SIGNAL_YIELD
_SIGNAL_YIELD	PROC NEAR 
; parameter 1: 68 + esp
.B1.1:                          ; Preds .B1.0
        push      esi                                           ;1.18
        push      edi                                           ;1.18
        push      ebx                                           ;1.18
        push      ebp                                           ;1.18
        sub       esp, 48                                       ;1.18
        mov       edx, DWORD PTR [68+esp]                       ;1.18
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_BACKPOINTR] ;29.7
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_BACKPOINTR+32] ;29.7
        mov       ecx, DWORD PTR [edx]                          ;29.10
        shl       ebx, 2                                        ;29.7
        lea       eax, DWORD PTR [eax+ecx*4]                    ;29.7
        mov       edx, eax                                      ;29.7
        sub       edx, ebx                                      ;29.7
        neg       ebx                                           ;30.10
        mov       esi, DWORD PTR [4+ebx+eax]                    ;30.10
        dec       esi                                           ;30.7
        mov       ebp, esi                                      ;30.7
        mov       edx, DWORD PTR [edx]                          ;29.7
        sub       ebp, edx                                      ;30.7
        inc       ebp                                           ;30.7
        mov       DWORD PTR [40+esp], ebp                       ;30.7
        cmp       esi, edx                                      ;37.9
        jl        .B1.12        ; Prob 10%                      ;37.9
                                ; LOE edx
.B1.2:                          ; Preds .B1.1
        imul      edx, edx, 152                                 ;
        xor       eax, eax                                      ;
        imul      ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32], 152 ;
        movss     xmm0, DWORD PTR [_2il0floatpacket.1]          ;39.99
        mulss     xmm0, DWORD PTR [_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT] ;39.99
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+40] ;39.7
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+44] ;39.7
        imul      edi, esi                                      ;
        mov       ebp, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;38.5
        mov       ecx, ebp                                      ;
        add       edx, ebp                                      ;
        sub       ecx, ebx                                      ;
        sub       edx, ebx                                      ;
        mov       DWORD PTR [32+esp], ecx                       ;
        mov       DWORD PTR [36+esp], edx                       ;
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE] ;
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+32] ;
        sub       ecx, edi                                      ;
        mov       ebp, ecx                                      ;
        mov       DWORD PTR [44+esp], eax                       ;
        cvttss2si edi, xmm0                                     ;39.7
        lea       ebx, DWORD PTR [edx*4]                        ;
        shl       edx, 5                                        ;
        sub       edx, ebx                                      ;
        lea       ebx, DWORD PTR [ecx+esi]                      ;
        sub       ebp, edx                                      ;
        lea       ecx, DWORD PTR [ecx+esi*2]                    ;
        sub       ebx, edx                                      ;
        sub       ecx, edx                                      ;
        mov       DWORD PTR [8+esp], ecx                        ;
        mov       DWORD PTR [4+esp], ebx                        ;
        mov       DWORD PTR [esp], ebp                          ;
                                ; LOE eax esi edi
.B1.3:                          ; Preds .B1.10 .B1.2
        mov       edx, DWORD PTR [36+esp]                       ;38.12
        mov       ebx, DWORD PTR [32+esp]                       ;38.12
        mov       ecx, DWORD PTR [16+eax+edx]                   ;38.12
        imul      ebp, ecx, 152                                 ;38.12
        movsx     edx, BYTE PTR [32+ebx+ebp]                    ;38.12
        test      edx, edx                                      ;38.5
        mov       DWORD PTR [28+esp], ecx                       ;38.12
        jle       .B1.10        ; Prob 50%                      ;38.5
                                ; LOE eax edx esi edi
.B1.4:                          ; Preds .B1.3
        mov       ecx, edx                                      ;38.5
        shr       ecx, 31                                       ;38.5
        add       ecx, edx                                      ;38.5
        sar       ecx, 1                                        ;38.5
        test      ecx, ecx                                      ;38.5
        jbe       .B1.13        ; Prob 3%                       ;38.5
                                ; LOE eax edx ecx esi edi
.B1.5:                          ; Preds .B1.4
        mov       ebp, DWORD PTR [36+esp]                       ;39.7
        xor       ebx, ebx                                      ;
        mov       DWORD PTR [24+esp], ecx                       ;
        mov       ecx, DWORD PTR [16+eax+ebp]                   ;39.7
        mov       DWORD PTR [16+esp], edx                       ;
        mov       DWORD PTR [12+esp], eax                       ;
        mov       eax, ebx                                      ;
        lea       ebp, DWORD PTR [ecx*4]                        ;39.7
        shl       ecx, 5                                        ;39.7
        sub       ecx, ebp                                      ;39.7
        mov       ebp, DWORD PTR [4+esp]                        ;
        add       ebp, ecx                                      ;
        add       ecx, DWORD PTR [8+esp]                        ;
        mov       DWORD PTR [20+esp], ecx                       ;
        mov       ecx, DWORD PTR [24+esp]                       ;
        mov       edx, DWORD PTR [20+esp]                       ;
                                ; LOE eax edx ecx ebx ebp esi edi
.B1.6:                          ; Preds .B1.6 .B1.5
        inc       ebx                                           ;38.5
        mov       DWORD PTR [4+ebp+eax*2], edi                  ;39.7
        mov       DWORD PTR [4+edx+eax*2], edi                  ;39.7
        add       eax, esi                                      ;38.5
        cmp       ebx, ecx                                      ;38.5
        jb        .B1.6         ; Prob 63%                      ;38.5
                                ; LOE eax edx ecx ebx ebp esi edi
.B1.7:                          ; Preds .B1.6
        mov       edx, DWORD PTR [16+esp]                       ;
        lea       ecx, DWORD PTR [1+ebx+ebx]                    ;38.5
        mov       eax, DWORD PTR [12+esp]                       ;
                                ; LOE eax edx ecx esi edi
.B1.8:                          ; Preds .B1.7 .B1.13
        lea       ebx, DWORD PTR [-1+ecx]                       ;38.5
        cmp       edx, ebx                                      ;38.5
        jbe       .B1.10        ; Prob 3%                       ;38.5
                                ; LOE eax ecx esi edi
.B1.9:                          ; Preds .B1.8
        mov       ebx, DWORD PTR [28+esp]                       ;39.7
        imul      ecx, esi                                      ;39.7
        add       ecx, DWORD PTR [esp]                          ;39.7
        lea       edx, DWORD PTR [ebx*4]                        ;39.7
        shl       ebx, 5                                        ;39.7
        sub       ebx, edx                                      ;39.7
        mov       DWORD PTR [4+ecx+ebx], edi                    ;39.7
                                ; LOE eax esi edi
.B1.10:                         ; Preds .B1.8 .B1.3 .B1.9
        mov       edx, DWORD PTR [44+esp]                       ;37.9
        add       eax, 152                                      ;37.9
        inc       edx                                           ;37.9
        mov       DWORD PTR [44+esp], edx                       ;37.9
        cmp       edx, DWORD PTR [40+esp]                       ;37.9
        jb        .B1.3         ; Prob 82%                      ;37.9
                                ; LOE eax esi edi
.B1.12:                         ; Preds .B1.10 .B1.1
        add       esp, 48                                       ;43.1
        pop       ebp                                           ;43.1
        pop       ebx                                           ;43.1
        pop       edi                                           ;43.1
        pop       esi                                           ;43.1
        ret                                                     ;43.1
                                ; LOE
.B1.13:                         ; Preds .B1.4                   ; Infreq
        mov       ecx, 1                                        ;
        jmp       .B1.8         ; Prob 100%                     ;
        ALIGN     16
                                ; LOE eax edx ecx esi edi
; mark_end;
_SIGNAL_YIELD ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _SIGNAL_YIELD
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
_2il0floatpacket.1	DD	042700000H
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
EXTRN	_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_BACKPOINTR:BYTE
_DATA	ENDS
EXTRN	__fltused:BYTE
	INCLUDELIB <ifconsol>
	INCLUDELIB <libifcoremt>
	INCLUDELIB <libifport>
	INCLUDELIB <libmmt>
	INCLUDELIB <LIBCMT>
	INCLUDELIB <libirc>
	INCLUDELIB <svml_dispmt>
	INCLUDELIB <OLDNAMES>
	END
