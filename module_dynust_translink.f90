MODULE DYNUST_TRANSLINK_MODULE
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~ Copyright (C) 2002-2011 The Arizona Board of Regents on Behalf of The University of Arizona.  ~~!
!~~ Developed by Yi-Chang Chiu, Ph.D. as par to the DynusT project.                               ~~!
!~~ This program is free software: you can redistribute it and/or modify it                       ~~!
!~~ under the terms of the GNU General Public License version 2 as published by                   ~~!
!~~ the Free Software Foundation.                                                                 ~~!
!~~ This program is distributed in the hope that it will be useful,                               ~~!
!~~ but WITHOUT ANY WARRANTY; without even the implied warranty of                                ~~!
!~~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                                          ~~!
!~~ See the GNU General Public License for more details.                                          ~~!
!~~ You should have received a copy of the GNU General Public License                             ~~!
!~~ along with this program (in the file COPYING).                                                ~~!
!~~ IF not, see <http://www.gnu.org/licenses/>.                                                   ~~!
!~~ Please see http://www.dynust.net for contact information and names                            ~~!
!~~ of individual contributors.                                                                   ~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!

TYPE TRANSFER ! Define the data structure for transferred vehicles
  INTEGER VehID ! Vehicle ID
  INTEGER InLink ! Link Id of the inbound link
  REAL    AvTime   ! arrival time of this vehicle at the intersection
END TYPE TRANSFER

TYPE LINKMEMBER
  TYPE(TRANSFER), POINTER :: P(:) ! size ASSOCIATED with the path length
  INTEGER(2)              :: PSize ! size of the ALLOCATED P()
  INTEGER(2)              :: AvSize ! size of filled P()
END TYPE LINKMEMBER
   
TYPE(LINKMEMBER), ALLOCATABLE :: TranLink_Array(:) ! Declare generic vehicle arrays

!INTEGER m_IncreaseSize ! increment for P() 
INTEGER TranLink_ArraySize
INTEGER vectorerror

PARAMETER (i_IncreaseSize = 50)
PARAMETER (m_InitSize = 50)



CONTAINS

! *** Start of TranLink_Array implementation ***

SUBROUTINE TranLink_2DSetup(Size)

  INTEGER Size
  INTEGER :: vi
! Remove existing array
  IF (ALLOCATED(TranLink_Array)) THEN
    CALL TranLink_2DRemove()
  ENDIF

! Initialize increase size.
! Setup new array
  IF (.NOT. ALLOCATED(TranLink_Array)) THEN
    ALLOCATE(TranLink_Array(Size),stat=vectorerror)
	IF(vectorerror /= 0) THEN
 	  WRITE(911,*)"TranLink_Array Setup error"
	  STOP
	ENDIF
  ENDIF
  
! Initialize new size for each element
  DO vi = 1, Size
   TranLink_Array(vi)%PSize = 0
   TranLink_Array(vi)%AvSize = 0   
  ENDDO

  TranLink_ArraySIZE = Size
	

END SUBROUTINE 

! this SUBROUTINE copies the existing array into a longer one
SUBROUTINE TranLink_Setup(it,NewSize)
	
 INTEGER it, NewSize
 TYPE(TRANSFER), POINTER :: tempP(:)
 INTEGER :: vi
 INTEGER :: OldSize
 OldSize = 0  
 !WRITE(923,*) it, newsize
! create temp pointer to store contents of array
   IF (TranLink_Array(it)%PSize > 0) THEN ! CALL from mid of simulation
     OldSize = TranLink_Array(it)%PSize
     ALLOCATE(tempP(TranLink_Array(it)%PSize),stat=vectorerror)
	 IF(vectorerror /= 0) THEN
	   WRITE(911,*) 'error in allocating tmpP vectorerror in TranLink_Setup'
	   STOP
	 ENDIF

! Copy content of old array to temp pointer
     DO vi = 1, TranLink_Array(it)%PSize
	   tempP(vi)%VehID = TranLink_Array(it)%P(vi)%VehID 
	   tempP(vi)%InLink = TranLink_Array(it)%P(vi)%InLink 
	   tempP(vi)%AvTime = TranLink_Array(it)%P(vi)%AvTime
	 ENDDO
  
! Delete the old array
!     IF (ASSOCIATED(TranLink_Array(it)%P)) THEN
	  DEALLOCATE(TranLink_Array(it)%P,stat=vectorerror)
	  IF(vectorerror /= 0) THEN
	    WRITE(911,*)"error in deallocating TranLink_Array vector error"
	    STOP
	  ENDIF
!     ENDIF

! REALlocate array
   ALLOCATE(TranLink_Array(it)%P(NewSize),stat=vectorerror)
	IF(vectorerror /= 0) THEN
      WRITE(911,*)"error in allocating TranLink_Array vector error"
	  STOP
	ENDIF
   
! Copy contents from temp back to array 
   DO vi = 1, OldSize
     TranLink_Array(it)%P(vi)%VehID  = tempP(vi)%VehID
     TranLink_Array(it)%P(vi)%InLink = tempP(vi)%InLink
     TranLink_Array(it)%P(vi)%AvTime  = tempP(vi)%AvTime
   ENDDO

! initialize array for the remaining elements
   DO vi = OldSize +1, NewSize
     TranLink_Array(it)%P(vi)%VehID  = 0
     TranLink_Array(it)%P(vi)%InLink = 0
     TranLink_Array(it)%P(vi)%AvTime  = 0.0
   ENDDO

   TranLink_Array(it)%PSize = NewSize

   DEALLOCATE(tempP)

   ELSE  ! initial setup
 
   ALLOCATE(TranLink_Array(it)%P(NewSize),stat=vectorerror)
	IF(vectorerror /= 0) THEN
      WRITE(911,*)"error in allocating TranLink_Array vector error"
	  STOP
	ENDIF
   TranLink_Array(it)%PSize = NewSize
   ENDIF
   
   
END SUBROUTINE 

! This initialize the initial path arrays for vehicles

SUBROUTINE TranLink_AvSizeIncre(it)
TranLink_Array(it)%AvSize = TranLink_Array(it)%AvSize + 1
END SUBROUTINE


SUBROUTINE TranLink_AvSizeDecre(it)
TranLink_Array(it)%AvSize = TranLink_Array(it)%AvSize - 1
END SUBROUTINE


SUBROUTINE TranLink_Scan(ft,it,eValue,VID,ReturnID)
  USE DYNUST_MAIN_MODULE
  INTEGER ReturnID,VID
  LOGICAL::Founds=.false.
  LOGICAL::iFound=.false.

  REAL ft
  ifound=.false.
  Founds=.false.
  ReturnID = 0
  
  IF(.not.iFound) THEN
  icheck = TranLink_Array(it)%AvSize
  cho = TranLink_Array(it)%P(1)%AvTime

  IF(TranLink_Array(it)%AvSize > 0) THEN
  isee = TranLink_Array(it)%AvSize

  ReturnID = 0
    DO k = ISEE, 1, -1
     value2=TranLink_Array(it)%P(K)%AvTime
      IF(value2 < eValue) THEN
	    ReturnID = k+1
	    idone = .true.
	    exit
      ENDIF
    ENDDO
    IF(ReturnID == 0) THEN ! not found 
      ReturnID = 1
    ENDIF
    IF (isee+1 > TranLink_Array(it)%PSize) THEN
        NewSize = TranLink_Array(it)%PSize + i_IncreaseSize 
        CALL TranLink_Setup(it,NewSize)

    ENDIF
    IF(ReturnID < isee+1) THEN ! insert into the last unit
       DO j = isee,ReturnID,-1
          TranLink_Array(it)%P(j+1)%VehID=TranLink_Array(it)%P(j)%VehID  
          TranLink_Array(it)%P(j+1)%InLink=TranLink_Array(it)%P(j)%InLink
          TranLink_Array(it)%P(j+1)%AvTime=TranLink_Array(it)%P(j)%AvTime
       ENDDO
    ENDIF
  ELSE
   ReturnID = 1
  ENDIF !TranLink_Array(it)%AvSize > 0
  
  ELSE ! found same id
   ReturnID = -1
  ENDIF

END SUBROUTINE


SUBROUTINE TranLink_Erase(it,nc)
  INTEGER it
  IF(nc == TranLink_Array(it)%AvSize) THEN
    TranLink_Array(it)%P(:)%VehID  = 0
    TranLink_Array(it)%P(:)%InLink = 0
    TranLink_Array(it)%P(:)%AvTime = 0
    TranLink_Array(it)%AvSize = 0
  ELSEIF(nc < TranLink_Array(it)%AvSize) THEN
	DO isee = nc+1,TranLink_Array(it)%AvSize
        TranLink_Array(it)%P(isee-nc)%VehID  = TranLink_Array(it)%P(isee)%VehID
        TranLink_Array(it)%P(isee-nc)%InLink = TranLink_Array(it)%P(isee)%InLink
        TranLink_Array(it)%P(isee-nc)%AvTime = TranLink_Array(it)%P(isee)%AvTime
    ENDDO
    TranLink_Array(it)%P(TranLink_Array(it)%AvSize-nc+1:TranLink_Array(it)%PSize)%VehID  = 0
    TranLink_Array(it)%P(TranLink_Array(it)%AvSize-nc+1:TranLink_Array(it)%PSize)%InLink = 0
    TranLink_Array(it)%P(TranLink_Array(it)%AvSize-nc+1:TranLink_Array(it)%PSize)%AvTime = 0
    TranLink_Array(it)%AvSize = TranLink_Array(it)%AvSize - nc
  ELSE
    WRITE(911,*) 'error in TranLink_Erase'
    STOP
  ENDIF
END SUBROUTINE 


SUBROUTINE TranLink_Insert(it, Index1D, AttNo, iValue)
  INTEGER it, Index1D, AttNo, NewSize
  INTEGER    iValue, oldsize
  oldsize = TranLink_Array(it)%PSize
!  print *, "in Tran_isert, it, index1d", it, index1d
  IF (Index1D > OldSize) THEN
	 NewSize = TranLink_Array(it)%PSize + i_IncreaseSize 
     CALL TranLink_Setup(it,NewSize)
  ENDIF
 
  IF (AttNo == 1) THEN
    TranLink_Array(it)%P(Index1D)%VehID  = iValue
  ELSEIF (AttNo == 2) THEN
    TranLink_Array(it)%P(Index1D)%InLink = iValue
  ELSEIF (AttNo == 3) THEN
    TranLink_Array(it)%P(Index1D)%AvTime  = float(iValue)/1000.0
  ENDIF
END SUBROUTINE 


! This function returns a value
INTEGER FUNCTION TranLink_Value(it,Index1D,AttNo)
  INTEGER it, Index1D, AttNo,iseesvsize

  iseeavsize = TranLink_Array(it)%AvSize
100 CONTINUE  
  IF (Index1D > TranLink_Array(it)%PSize) THEN
     WRITE(911,*)"TranLink GetValue vector error"
     WRITE(911,*) 'Index1D =', Index1D
	 WRITE(911,*) 'TranLink_Array(it)%PSize =',TranLink_Array(it)%PSize
     STOP
  ENDIF

  IF (AttNo == 1) THEN
     TranLink_Value = TranLink_Array(it)%P(Index1D)%VehID
  ELSEIF (AttNo == 2) THEN
     TranLink_Value = TranLink_Array(it)%P(Index1D)%InLink
  ELSEIF (AttNo == 3) THEN
     TranLink_Value = nint(1000*TranLink_Array(it)%P(Index1D)%AvTime)
  ELSE
    WRITE(911,*) 'get TranLink_value error'
    STOP
  ENDIF
  IF(AttNo == 1.and.TranLink_Value < 1) THEN
   Index1D = Index1D + 1
   go to 100
  ENDIF
  
  IF(TranLink_Value < 1.or.Index1D > iseeavsize) THEN
     WRITE(911,*) 'Error in TransLink_Value'
     WRITE(911,*) "error in TranLink_Value", TranLink_Value
	 STOP
  ENDIF

END FUNCTION 


INTEGER FUNCTION TranLink_Size(it)
   i = TranLink_array(it)%PSize
   DO while (TranLink_array(it)%p(i)%VehID < 1) 
       i = i - 1
   ENDDO
   TranLink_Size = i

END FUNCTION 

! -----------------------------------Remove
SUBROUTINE TranLink_Remove(it)
 
  INTEGER it
  IF (TranLink_Array(it)%PSize > 0) THEN
    DEALLOCATE(TranLink_Array(it)%P,stat=vectorerror)
	IF(vectorerror /= 0) THEN
	  WRITE(911,*)"DEALLOCATE TranLink_Array vectorerror"
	  WRITE(911,*) it
	  STOP
	ENDIF
    TranLink_Array(it)%PSize = 0 
    TranLink_Array(it)%AvSize = 0 
  ENDIF

END SUBROUTINE 

! -----------------------------------Clear
SUBROUTINE TranLink_Clear(it,start)
 
  INTEGER it
  INTEGER start
  INTEGER vi
! Clean the remaining elements     
   DO vi = start, TranLink_Array(it)%PSize
     TranLink_Array(it)%P(vi)%VehID = 0 
     TranLink_Array(it)%P(vi)%InLink = 0 
     TranLink_Array(it)%P(vi)%AvTime = 0.0 
   ENDDO

END SUBROUTINE 

! -----------------------------------2DRemove
SUBROUTINE TranLink_2DRemove()
 
  INTEGER :: vi
! Remove every element
  DO vi = 1, TranLink_ArraySize
    CALL TranLink_Remove(vi) 
  ENDDO

! Remove entire array
  IF (ALLOCATED(TranLink_Array)) THEN
    DEALLOCATE(TranLink_Array,stat=vectorerror)
	IF(vectorerror /= 0) THEN
 	  WRITE(911,*) "TranLink Destory"
 	  STOP
	ENDIF
  ENDIF
  TranLink_ArraySize = 0
	
END SUBROUTINE 

END MODULE 
