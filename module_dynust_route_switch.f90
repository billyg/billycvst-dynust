MODULE DYNUST_ROUTE_SWITCH_MODULE
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~ Copyright (C) 2002-2011 The Arizona Board of Regents on Behalf of The University of Arizona.  ~~!
!~~ Developed by Yi-Chang Chiu, Ph.D. as par to the DynusT project.                               ~~!
!~~ This program is free software: you can redistribute it and/or modify it                       ~~!
!~~ under the terms of the GNU General Public License version 2 as published by                   ~~!
!~~ the Free Software Foundation.                                                                 ~~!
!~~ This program is distributed in the hope that it will be useful,                               ~~!
!~~ but WITHOUT ANY WARRANTY; without even the implied warranty of                                ~~!
!~~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                                          ~~!
!~~ See the GNU General Public License for more details.                                          ~~!
!~~ You should have received a copy of the GNU General Public License                             ~~!
!~~ along with this program (in the file COPYING).                                                ~~!
!~~ IF not, see <http://www.gnu.org/licenses/>.                                                   ~~!
!~~ Please see http://www.dynust.net for contact information and names                            ~~!
!~~ of individual contributors.                                                                   ~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
    USE DYNUST_MAIN_MODULE
    USE DYNUST_VEH_MODULE
    USE DYNUST_NETWORK_MODULE
	USE DYNUST_VEH_PATH_ATT_MODULE
    USE DFPORT
    USE RAND_GEN_INT
    USE IFQWIN

CONTAINS

SUBROUTINE DELAY_SWITCH_AUTO(j,icu2,culnk,l)

    INTERFACE 
      SUBROUTINE RETRIEVE_PATH_TIME(i,jdst,gencost)
        INTEGER i,jdst
        REAL gencost
      END SUBROUTINE
    END INTERFACE
    INTERFACE
      SUBROUTINE SHELTER_UPDATE(icu2)
        INTEGER icu2
      END SUBROUTINE
    END INTERFACE
    !INTERFACE
    !  !SUBROUTINE RETRIEVE_VEH_PATH(j,i,iselect,CurNode,jdst,itrace,NodeSum2)
    !  SUBROUTINE RETRIEVE_VEH_PATH_AStar(j,i,iselect,CurNode,jdst,itrace,NodeSum2)
    !    INTEGER j,i,iselect,itrace,curnode,jdst,NodeSum2
    !  END SUBROUTINE
    !END INTERFACE
	
	INTEGER Itp1, Nlnk, Culnk, j,icu,icu2
	INTEGER jdstmp
    LOGICAL::Itp2
    LOGICAL::BottleneckAhead = .FALSE.
    REAL r
    INTEGER NodeSumOrg,NodeSumNew
!> > > > > > > > > >                < < < < < < < < < < <
!>>>>>>>>>>>>>>>>>>>CODES START HERE<<<<<<<<<<<<<<<<<<<<<
!> > > > > > > > > >                < < < < < < < < < < <

CALL GET_AUTO_REMAIN_TIME(j,icu2,culnk,l,current_time,NodeSumOrg,BottleneckAhead)

IF(current_time > 1000) THEN ! the current path is blocked
      jdstmp = m_dynust_last_stand(j)%jdest ! jdstmp is KIND = 4, m_dynust_last_stand(j)%jdest is KIND = 2 
      !CALL RETRIEVE_PATH_TIME(icu2,jdstmp,generalized_cost) ! check travel time of the best path
      IF(generalized_cost > 1000) THEN ! roadway is blocked to the original destination
        iassign = m_dynust_last_stand(j)%jdest
        iassign2 = m_dynust_network_node_nde(m_dynust_network_arc_nde(m_dynust_last_stand(j)%isec)%idnod)%izone
        !CALL RETRIEVE_PATH_TIME(icu2,iassign2,generalized_cost) ! check travel time back to origin     
        iseeloop = 0
        IF((generalized_cost >= 1000.or.m_dynust_veh(j)%EvacSta > 0).and.shelter_use > 0) THEN ! assign shelter IF shelters are specified
           IF(shelter_num < 1) THEN
             WRITE(911,*) "need to specify shelter.dat, which is missing"
             STOP
           ELSE
             !CALL SHELTER_UPDATE(icu2)
           ENDIF
           iassign = 0
222        CONTINUE
           r3 = ranxy(13) 
           
           DO ih = 1, shelter_num
             IF(shelter(ih)%prob >= r3) THEN
               iassign = m_dynust_network_node_nde(shelter(ih)%node)%izone ! assign the shelter ih
               exit
             ENDIF
           ENDDO
           iseeloop = iseeloop +  1
           !CALL RETRIEVE_PATH_TIME(icu2,iassign,generalized_cost) ! check travel time to the shelter 
           IF(generalized_cost > 1000) THEN
             IF(iseeloop > 10) THEN
               go to 222
             ELSE
               WRITE(511,*) "Error in assigning shelter, vehid = ", j
             ENDIF
           ENDIF
        ENDIF
        m_dynust_last_stand(j)%jdest = iassign
        !CALL RETRIEVE_VEH_PATH      (j,icu2,1,Culnk,iassign,1,NodeSumOrg)
         CALL RETRIEVE_VEH_PATH_AStar(j,icu2,1,Culnk,iassign,1,NodeSumOrg)
        m_dynust_veh(j)%decision(2)=m_dynust_veh(j)%decision(2)+1        
        go to 10000 ! skip the rest of the codes which are for normal en-route routing
      ENDIF
ENDIF

5       movetmp=m_dynust_network_arc_nde(icu2)%ForToBackLink-backpointr(m_dynust_network_arc_nde(icu2)%idnod)+1

        ict = 1
        know = 1
        k=1
        !!!best=labeloutCostCopy(m_dynust_last_stand(j)%jdest,m_dynust_network_arc_nde(icu2)%idnod,1,1,movetmp)

!     IF((best < current_time*(1-m_dynust_veh_nde(j)%ribf)).and.(best < current_time-bound)) THEN
       nsnode = vehatt_Value(j,Culnk,1)
       icc = 0
       IF(m_dynust_network_node(nsnode)%iConZone(1) > 0) THEN
        DO ih = 1, m_dynust_network_node(nsnode)%iConZone(1)
         IF(m_dynust_network_node(nsnode)%iConZone(ih+1) == m_dynust_last_stand(j)%jdest) THEN ! IF the downstream node is the destination node for vehicle j
            icc = 1
            exit
         ENDIF
        ENDDO
       ENDIF
       IF(icc == 0.AND.BottleneckAhead) THEN
        iassign = m_dynust_last_stand(j)%jdest
        !CALL RETRIEVE_VEH_PATH      (j,icu2,1,Culnk,iassign,1,NodeSumNew)
        CALL  RETRIEVE_VEH_PATH_AStar(j,icu2,1,Culnk,iassign,1,NodeSumNew)
        IF(NodeSumNew /= NodeSumOrg)  THEN
          m_dynust_veh(j)%DivStatus = 2 ! THIS VEHICLE HAS BEEN DIVERTED
          WRITE(8456,'("Time: ",f7.1," Vehicle no",i7, " OD zone", 2i6," #Curn,UN,DN ",i4,2i6," delay tole:(min) ", f6.1," curn delay:(min) ",f6.1)') (l-1)*xminPerSimInt,j,m_dynust_last_stand(j)%jorig,m_dynust_last_stand(j)%jdest,m_dynust_veh_nde(j)%icurrnt,m_dynust_network_node_nde(m_dynust_network_arc_nde(icu2)%iunod)%IntoOutNodeNum,&
                       m_dynust_network_node_nde(m_dynust_network_arc_nde(icu2)%idnod)%IntoOutNodeNum,m_dynust_veh(j)%delaytole/AttScale, m_dynust_veh(j)%currentdelay/AttScale
        ENDIF
        m_dynust_veh(j)%decision(1)=m_dynust_veh(j)%decision(1)+1
        IF(m_dynust_veh(j)%switch > 0) m_dynust_veh(j)%switch=(-1)*m_dynust_veh(j)%switch
       ENDIF 
!     ENDIF

10000 CONTINUE
     
END SUBROUTINE


!~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>
SUBROUTINE DELAY_SWITCH_MULT(j,i,culnk,l,LowestR,LowestS,LowestOnNode,LowestOffNode,CapOk) ! MULTI MODAL REROUTE IS NEEDED 
    USE DYNUST_TRANSIT_MODULE
    USE DYNUST_VEH_PATH_ATT_MODULE
    !INTERFACE
    !  SUBROUTINE RETRIEVE_VEH_PATH(j,i,iselect,CurNode,jdst,itrace,NodeSum2)
    !    INTEGER j,i,iselect,itrace,curnode,jdst,NodeSum2
    !  END SUBROUTINE
    !END INTERFACE
	
	INTEGER Itp1, Nlnk, Culnk, j,icu,icu2
	INTEGER jdstmp,NodeSum,destnode,predestnode
    LOGICAL Itp2,CapOk
    REAL r,evalue
    
!> > > > > > > > > >                < < < < < < < < < < <
!>>>>>>>>>>>>>>>>>>>CODES START HERE<<<<<<<<<<<<<<<<<<<<<
!> > > > > > > > > >                < < < < < < < < < < <
!   CHANGE THE DESTINATION
    icu2 = i
    ITONODE = m_route(LowestR)%node(LowestOnNode)
    m_dynust_last_stand(j)%jdest = MasterDest(m_dynust_network_node(ITONODE)%dzone)
    iassign = m_dynust_last_stand(j)%jdest

    IF(CapKnow.and.CapOk) THEN !CAP AWARE AND REMAINING CAP IS GOOD
      !CALL RETRIEVE_VEH_PATH(j,icu2,1,Culnk,iassign,1,NodeSum)
      CALL RETRIEVE_VEH_PATH_AStar(j,icu2,1,Culnk,iassign,1,NodeSum)
!     ASSIGN TRANSIT ROUTE TO THE REMAINING TRIP       
      ins = culnk
      ins2 = LowestOnNode
      destnode = vehatt_value(j,vehatt_P_Size(j)-1,1)
      IF(destnode /= m_route(LowestS)%Node(LowestOnNode)) THEN
         WRITE(911,*) "ERROR IN ASSIGNING TRANSIT PATH"
      ELSE
         DO ISEE = LowestOnNode,LowestOffNode
           ins = ins + 1
           ins2 = ins2 + 1
           evalue = float(m_route(LowestR)%Node(ins2))
           call vehatt_insert(j,ins,1,evalue)
         ENDDO
         ins = ins + 1
         evalue = destination(MasterDest(m_dynust_last_stand(j)%jdest))
         call vehatt_insert(j,ins,1,evalue)
      ENDIF
      ITONODE = m_route(LowestR)%node(LowestOffNode)
      m_dynust_last_stand(j)%jdest = MasterDest(m_dynust_network_node(ITONODE)%dzone)
      vehatt_array(j)%Psize = vehatt_array(j)%Psize + ins
      vehatt_array(j)%AcSize = vehatt_array(j)%AcSize + ins
    ELSEIF(.not.CapKnow.and..not.CapOk) THEN ! NOT CAP AWARE AND NO CAP AVAILABLE , THEN ASSIGN/APPEND AUTO ROUTE
      !CALL RETRIEVE_VEH_PATH(j,icu2,1,Culnk,iassign,1,NodeSum)
      CALL RETRIEVE_VEH_PATH_AStar(j,icu2,1,Culnk,iassign,1,NodeSum)
      Culnk = vehatt_P_Size(j)-1
      destnode = vehatt_value(j,vehatt_P_Size(j)-1,1)
      predestnode = vehatt_value(j,vehatt_P_Size(j)-2,1)
      icu2 = GetFLinkFromNode(predestnode,destnode,2)
      ITONODE = m_route(LowestR)%node(LowestOffNode)
      m_dynust_last_stand(j)%jdest = MasterDest(m_dynust_network_node(ITONODE)%dzone)
      iassign = m_dynust_last_stand(j)%jdest
      !CALL RETRIEVE_VEH_PATH(j,icu2,1,Culnk,iassign,1,NodeSum)
      CALL RETRIEVE_VEH_PATH_AStar(j,icu2,1,Culnk,iassign,1,NodeSum)
    ENDIF
       
END SUBROUTINE

!~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>
SUBROUTINE INFO_SWITCH(j,icu2,culnk,l)
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~ Copyright (C) 2002-2009 The Arizona Board of Regents on Behalf of The University of Arizona.  ~~!
!~~ Developed by Yi-Chang Chiu, Ph.D. as par to the DynusT project.                               ~~!
!~~ This program is free software: you can redistribute it and/or modify it                       ~~!
!~~ under the terms of the GNU General Public License version 2 as published by                   ~~!
!~~ the Free Software Foundation.                                                                 ~~!
!~~ This program is distributed in the hope that it will be useful,                               ~~!
!~~ but WITHOUT ANY WARRANTY; without even the implied warranty of                                ~~!
!~~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                                          ~~!
!~~ See the GNU General Public License for more details.                                          ~~!
!~~ You should have received a copy of the GNU General Public License                             ~~!
!~~ along with this program (in the file COPYING).                                                ~~!
!~~ IF not, see <http://www.gnu.org/licenses/>.                                                   ~~!
!~~ Please see http://dynust.net/wikibin/doku.php for contact information and names               ~~!
!~~ of individual contributors.                                                                   ~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~! 

    USE DYNUST_MAIN_MODULE
    USE DYNUST_VEH_MODULE
    USE DYNUST_NETWORK_MODULE
	USE DYNUST_VEH_PATH_ATT_MODULE
    USE DFPORT
    USE RAND_GEN_INT
    USE IFQWIN

    !INTERFACE 
    !  SUBROUTINE RETRIEVE_PATH_TIME(i,jdst,gencost)
    !    INTEGER i,jdst
    !    REAL gencost
    !  END SUBROUTINE
    !END INTERFACE
    INTERFACE
      SUBROUTINE SHELTER_UPDATE(icu2)
        INTEGER icu2
      END SUBROUTINE
    END INTERFACE
    !INTERFACE
    !SUBROUTINE RETRIEVE_VEH_PATH_AStar(j,i,iselect,CurNode,jdst,itrace,NodeSum2)
    !    INTEGER j,i,iselect,itrace,curnode,jdst,NodeSum2
    !  END SUBROUTINE
    !END INTERFACE
	
	INTEGER Itp1, Nlnk, Culnk, j,icu,icu2
	INTEGER jdstmp,NodeSumNew,NodeSumOrg
    LOGICAL Itp2,BottleneckAhead
    REAL r
!> > > > > > > > > >                < < < < < < < < < < <
!>>>>>>>>>>>>>>>>>>>CODES START HERE<<<<<<<<<<<<<<<<<<<<<
!> > > > > > > > > >                < < < < < < < < < < <

CALL GET_AUTO_REMAIN_TIME(j,icu2,culnk,l,current_time,NodeSumOrg,BottleneckAhead)

IF(current_time > 1000) THEN ! the current path is blocked
      jdstmp = m_dynust_last_stand(j)%jdest ! jdstmp is KIND = 4, m_dynust_last_stand(j)%jdest is KIND = 2 
      !CALL RETRIEVE_PATH_TIME(icu2,jdstmp,generalized_cost) ! check travel time of the best path
      IF(generalized_cost > 1000) THEN ! roadway is blocked to the original destination
        iassign = m_dynust_last_stand(j)%jdest
        iassign2 = m_dynust_network_node_nde(m_dynust_network_arc_nde(m_dynust_last_stand(j)%isec)%idnod)%izone
        !CALL RETRIEVE_PATH_TIME(icu2,iassign2,generalized_cost) ! check travel time back to origin     
        iseeloop = 0
        IF((generalized_cost >= 1000.or.m_dynust_veh(j)%EvacSta > 0).and.shelter_use > 0) THEN ! assign shelter IF shelters are specified
           IF(shelter_num < 1) THEN
             WRITE(911,*) "need to specify shelter.dat, which is missing"
             STOP
           ELSE
             !CALL SHELTER_UPDATE(icu2)
           ENDIF
           iassign = 0
222        CONTINUE
           r3 = ranxy(13) 
           
           DO ih = 1, shelter_num
             IF(shelter(ih)%prob >= r3) THEN
               iassign = m_dynust_network_node_nde(shelter(ih)%node)%izone ! assign the shelter ih
               exit
             ENDIF
           ENDDO
           iseeloop = iseeloop +  1
           !CALL RETRIEVE_PATH_TIME(icu2,iassign,generalized_cost) ! check travel time to the shelter 
           IF(generalized_cost > 1000) THEN
             IF(iseeloop > 10) THEN
               go to 222
             ELSE
               WRITE(511,*) "Error in assigning shelter, vehid = ", j
             ENDIF
           ENDIF
        ENDIF
        m_dynust_last_stand(j)%jdest = iassign
        !CALL RETRIEVE_VEH_PATH(j,icu2,1,Culnk,iassign,1,NodeSumOrg)
         CALL RETRIEVE_VEH_PATH_AStar(j,icu2,1,Culnk,iassign,1,NodeSumOrg)
        m_dynust_veh(j)%decision(2)=m_dynust_veh(j)%decision(2)+1        
        go to 10000 ! skip the rest of the codes which are for normal en-route routing
      ENDIF
ENDIF


5    movetmp=m_dynust_network_arc_nde(icu2)%ForToBackLink-backpointr(m_dynust_network_arc_nde(icu2)%idnod)+1

     ict = 1
     know = 1
     k=1
     !!!best=labeloutCostCopy(m_dynust_last_stand(j)%jdest,m_dynust_network_arc_nde(icu2)%idnod,1,1,movetmp)

     IF((best < current_time*(1-m_dynust_veh_nde(j)%ribf)).and.(best < current_time-bound)) THEN
       nsnode = vehatt_Value(j,Culnk,1)
       icc = 0
       IF(m_dynust_network_node(nsnode)%iConZone(1) > 0) THEN
        DO ih = 1, m_dynust_network_node(nsnode)%iConZone(1)
         IF(m_dynust_network_node(nsnode)%iConZone(ih+1) == m_dynust_last_stand(j)%jdest) THEN ! IF the downstream node is the destination node for vehicle j
            icc = 1
            exit
         ENDIF
        ENDDO
       ENDIF
       IF(icc == 0) THEN
        iassign = m_dynust_last_stand(j)%jdest
        !CALL RETRIEVE_VEH_PATH(j,icu2,1,Culnk,iassign,1,NodeSumNew)
        CALL RETRIEVE_VEH_PATH_AStar(j,icu2,1,Culnk,iassign,1,NodeSumNew)
        m_dynust_veh(j)%decision(1)=m_dynust_veh(j)%decision(1)+1
        IF(m_dynust_veh(j)%switch > 0) m_dynust_veh(j)%switch=(-1)*m_dynust_veh(j)%switch
       ENDIF 
     ENDIF

10000 CONTINUE
     
END SUBROUTINE


!~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>
SUBROUTINE CHECK_IF_MULTM(distscale,j,icu2,culnk,l,CallMultM,LowestR,LowestS,LowestOnNode,LowestOffNode)
  LOGICAL,INTENT(OUT)::CallMultM
  INTEGER,INTENT(IN):: j,icu2,culnk,l
  REAL current_time,current_multM,distscale
  INTEGER,INTENT(OUT):: LowestR,LowestS,LowestOnNode,LowestOffNode
  INTEGER NodeSumOrg
  LOGICAL BottleneckAhead,CapOk
  
  CallMultM = .false.
  LowestR = 0
  LowestS = 0
  LowestOnNode = 0
  LowestOffNode = 0
  CapOk = .false.
    
! CALCULATE THE MULTI-MODAL TIME BY ADDING THE AUTO-TRANSIT-WALK TIME
  
  CALL GET_MULT_REMAIN_TIME(distscale,j,icu2,culnk,l,current_time_multM,LowestR,LowestS,LowestOnNode,LowestOffNode,CapOk)

  CALL GET_AUTO_REMAIN_TIME(j,icu2,culnk,l,current_time_auto,NodeSumOrg,BottleneckAhead)
  
  IF(current_time_multM < current_time_auto.AND.BottleneckAhead) THEN
    WRITE(8456,'("Veh # ",i7," To Transit: ", "Transit TT ",f8.1, " Auto TT ",f8.1,4i5)') j,current_time_multM,current_time_auto,LowestR,LowestS,LowestOnNode,LowestOffNode
    CallMultM = .true.
  ENDIF
  
END SUBROUTINE

!~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>
SUBROUTINE GET_MULT_REMAIN_TIME(distscale,VehID,icu2,culnk,LoopID,current_time_multM,LowestR,LowestS,LowestOnNode,LowestOffNode,CapOk)
  USE DYNUST_TRANSIT_MODULE
  USE DYNUST_VEH_PATH_ATT_MODULE
  USE DYNUST_NETWORK_MODULE
  
  INTEGER,INTENT(IN):: VehID,icu2,culnk,LoopID
  REAL,INTENT(IN):: distscale
  INTEGER destnode,orignode,busonN,busoffN
  REAL time1,time2,time3,LowestT
  INTEGER,INTENT(OUT):: LowestR,LowestS,LowestOnNode,LowestOffNode
  LOGICAL IFOUND,CapOk

!> > > > > > > > > >                < < < < < < < < < < <
!>>>>>>>>>>>>>>>>>>>CODES START HERE<<<<<<<<<<<<<<<<<<<<<
!> > > > > > > > > >                < < < < < < < < < < <
  
  LowestT = 1000000.0 ! SET LOWEST TIME, ROUTES AND SCHEDULE
  LowestR = 0
  LowestS = 0
  LowestOnNode = 0
  LowestOffNode = 0
  
  destnode = vehatt_value(VehID,vehatt_P_Size(VehID)-1,1)
  orignode = m_dynust_network_arc_nde(icu2)%idnod
! SEARCH THROUGH ALL TRANSIT ROUTES  
  DO I = 1, NumRoutes
   DO J = 1, m_route(i)%NumNode
     IF(m_route(i)%StopType(j) > 0) THEN                                      ! THIS NODE IS A STOP
       iczone = MasterDest(m_dynust_network_node(m_route(i)%node(j))%dzone)   ! ICZONE IS WHICH ZONE THE BUS STOP BELONGS TO      
       !!!time1 = labeloutCostCopy(iczone,orignode,1,1,1) 
       busstoparrtime = LoopID*xminPerSimInt + time1
       IF(busstoparrtime < m_schedule(i)%Stinfo(m_schedule(i)%NumTrip)%DepartTime(m_schedule(i)%NumStop)) THEN ! ARRIVING AT BUS STOP BEFORE THE LAST BUS TRIP
         DO K = J+1, m_route(i)%NumNode                                       ! CHECK DOWNSTREAM BUSOFF NODE TO THE FINAL DESTINATION
           IF(m_route(i)%StopType(k) > 0) THEN                                ! THIS NODE IS A STOP
             stop2dest = ABS(m_dynust_network_node(m_route(i)%node(k))%xcor-m_dynust_network_node(destnode)%xcor)+ & ! CALCULATE MANHATTAN DISTANCE
                         ABS(m_dynust_network_node(m_route(i)%node(k))%ycor-m_dynust_network_node(destnode)%ycor)
             time3 = (stop2dest*distscale)*5280/4.0/60.0                      ! ASSUME 4 FEET PER SECOND, CONVERTED TO MIN
                                                                              ! WITH K AND J FIXED, CALCULATE BUS TRAVEL TIME
             DO M = 1, m_schedule(i)%NumTrip
               Ifound = .FALSE.
               DO MM = 1, m_schedule(i)%NumStop
                 IF(m_schedule(i)%Stinfo(M)%DepartTime(mm) >= busstoparrtime) THEN
                   Ifound = .TRUE.
                   EXIT
                 ENDIF
               ENDDO
               mm2 = m_route(i)%R2SMap(k)
               mm = min(mm,m_schedule(i)%NumStop)
               IF(IFound.AND.mm2 > mm) THEN ! CURRENT CHECKED NODE IS GREATER THAN BUS STOP ARRIVING NODE
                 time2 = m_schedule(i)%Stinfo(M)%DepartTime(mm2) - m_schedule(i)%Stinfo(M)%DepartTime(mm)
                 IF(time2 < 0) THEN
                   WRITE(911,*) "Error in time2 in Get_Mult_Remain_Time"
                   STOP
                 ENDIF
                 IF(time1+time2+time3 < LowestT) THEN                         ! UPDATE ALL RELEVANT INFO
                   LowestT = time1+time2+time3                                ! TRAVEL TIME
                   LowestR = I                                                ! ROUTE #
                   LowestS = M                                                ! TRIP #
                   LowestOnNode = j                                           ! ON NODE #
                   LowestOffNode = k                                          ! OFF NODE # 
                 ENDIF
               ENDIF
             ENDDO !M = 1, m_schedule(i)%NumTrip
           ENDIF
         ENDDO
       ENDIF ! busstoparrtime < m_schedule(i)
     ENDIF   
   ENDDO
  ENDDO
! FOR VehID, we found the min time needed to go through transit
current_time_multM = LowestT
if(LowestR > 0) THEN
IF(CapKnow.AND.m_route(LowestR)%StopCapacity(LowestOffNode) < 1) THEN !IF AWARE THE CAPACITY AT THE BUSOFF STOP AND THE CAP IS 0, THEN ADD HIGH PENALITY
   current_time_multM = LowestT + 1000.0
   CapOk = .false.
ENDIF
ELSE
   CapOk = .false.
ENDIF

END SUBROUTINE

!~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>~~<>
SUBROUTINE GET_AUTO_REMAIN_TIME(j,icu2,culnk,l,current_time,NodeSum,BottleneckAhead)
    USE DYNUST_NETWORK_MODULE
    USE DYNUST_VEH_MODULE
    INTEGER aggtime
    INTEGER,INTENT(IN)::J,ICU2,CULNK,L
    REAL,INTENT(OUT)::current_time
    INTEGER NodeSum
    LOGICAL BottleneckAhead

!> > > > > > > > > >                < < < < < < < < < < <
!>>>>>>>>>>>>>>>>>>>CODES START HERE<<<<<<<<<<<<<<<<<<<<<
!> > > > > > > > > >                < < < < < < < < < < <
    BottleneckAhead = .False.
    NodeSum = 0
    aggtime = max(1,ifix(float(l-1)/simPerAgg)+1)
    current_time=0.0
    icu3 = icu2
    DO 3 k=Culnk,vehatt_P_Size(j)-1
         icunod=nint(vehatt_Value(j,k,1))	     
         NodeSum = NodeSum + icunod
         nexnod=nint(vehatt_Value(j,k+1,1))
         ig = m_dynust_network_node_nde(icunod)%IntoOutNodeNum
         ih = m_dynust_network_node_nde(nexnod)%IntoOutNodeNum
         ij = m_dynust_network_node_nde(m_dynust_network_arc_nde(icu3)%iunod)%IntoOutNodeNum
         ip = m_dynust_network_node_nde(m_dynust_network_arc_nde(icu3)%idnod)%IntoOutNodeNum
         icu0 = GetFLinkFromNode(icunod,nexnod,2)
         IF(m_dynust_network_arc_de(icu0)%LinkStatus%inci == 2)THEN
           BottleneckAhead = .TRUE. !INCIDENT IS AHEAD IN THE ROUTE
         ENDIF
         mmv = getBstMove(icu3,icu0)
         icu3 = icu0
         IF(mmv < 1) THEN
           WRITE(911,*) "error in INFO_SWITCH"
           WRITE(911,*) "j, i, iccurrnt",j,icu2,m_dynust_veh_nde(j)%icurrnt
           STOP
         ENDIF
         current_time = current_time + TravelTime(icu0,aggtime)/AttScale+m_dynust_network_arcmove_nde(m_dynust_network_arc_nde(icu0)%fortobacklink,mmv)%penalty ! -- add the travel time of the link to the current_time
!        ADD LINK TOLL EQULVALENT TIME
         icv = m_dynust_last_stand(j)%vehtype
         if(m_dynust_last_stand(j)%vehtype == 6) icv = 3 ! transit are consider HOV vehicle from cost standpoint
         current_time = current_time + m_dynust_network_arc_de(icu0)%costexp(aggtime,icv)
         
3    CONTINUE
END SUBROUTINE



END MODULE