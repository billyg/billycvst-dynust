; mark_description "Intel(R) Visual Fortran Compiler XE for applications running on IA-32, Version 13.1.1.171 Build 20130313";
; mark_description "-S";
	.686P
 	.387
	OPTION DOTNAME
	ASSUME	CS:FLAT,DS:FLAT,SS:FLAT
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
TXTST0:
; -- Begin  _CLOSEFILE
; mark_begin;
IF @Version GE 800
  .MMX
ELSEIF @Version GE 612
  .MMX
  MMWORD TEXTEQU <QWORD>
ENDIF
IF @Version GE 800
  .XMM
ELSEIF @Version GE 614
  .XMM
  XMMWORD TEXTEQU <OWORD>
ENDIF
       ALIGN     16
	PUBLIC _CLOSEFILE
_CLOSEFILE	PROC NEAR 
.B1.1:                          ; Preds .B1.0
        push      ebp                                           ;1.16
        mov       ebp, esp                                      ;1.16
        and       esp, -16                                      ;1.16
        push      esi                                           ;1.16
        push      ebx                                           ;1.16
        sub       esp, 40                                       ;1.16
        xor       esi, esi                                      ;22.8
        mov       DWORD PTR [esp], esi                          ;22.8
        lea       ebx, DWORD PTR [esp]                          ;22.8
        push      32                                            ;22.8
        push      esi                                           ;22.8
        push      OFFSET FLAT: __STRLITPACK_0.0.1               ;22.8
        push      -2088435968                                   ;22.8
        push      41                                            ;22.8
        push      ebx                                           ;22.8
        call      _for_close                                    ;22.8
                                ; LOE ebx esi edi
.B1.2:                          ; Preds .B1.1
        mov       DWORD PTR [24+esp], esi                       ;23.8
        push      32                                            ;23.8
        push      esi                                           ;23.8
        push      OFFSET FLAT: __STRLITPACK_1.0.1               ;23.8
        push      -2088435968                                   ;23.8
        push      42                                            ;23.8
        push      ebx                                           ;23.8
        call      _for_close                                    ;23.8
                                ; LOE ebx esi edi
.B1.3:                          ; Preds .B1.2
        mov       DWORD PTR [48+esp], esi                       ;24.8
        push      32                                            ;24.8
        push      esi                                           ;24.8
        push      OFFSET FLAT: __STRLITPACK_2.0.1               ;24.8
        push      -2088435968                                   ;24.8
        push      43                                            ;24.8
        push      ebx                                           ;24.8
        call      _for_close                                    ;24.8
                                ; LOE ebx esi edi
.B1.4:                          ; Preds .B1.3
        mov       DWORD PTR [72+esp], esi                       ;25.8
        push      32                                            ;25.8
        push      esi                                           ;25.8
        push      OFFSET FLAT: __STRLITPACK_3.0.1               ;25.8
        push      -2088435968                                   ;25.8
        push      44                                            ;25.8
        push      ebx                                           ;25.8
        call      _for_close                                    ;25.8
                                ; LOE ebx esi edi
.B1.5:                          ; Preds .B1.4
        mov       DWORD PTR [96+esp], esi                       ;26.8
        push      32                                            ;26.8
        push      esi                                           ;26.8
        push      OFFSET FLAT: __STRLITPACK_4.0.1               ;26.8
        push      -2088435968                                   ;26.8
        push      45                                            ;26.8
        push      ebx                                           ;26.8
        call      _for_close                                    ;26.8
                                ; LOE ebx esi edi
.B1.86:                         ; Preds .B1.5
        add       esp, 120                                      ;26.8
                                ; LOE ebx esi edi
.B1.6:                          ; Preds .B1.86
        mov       DWORD PTR [esp], esi                          ;27.8
        push      32                                            ;27.8
        push      esi                                           ;27.8
        push      OFFSET FLAT: __STRLITPACK_5.0.1               ;27.8
        push      -2088435968                                   ;27.8
        push      46                                            ;27.8
        push      ebx                                           ;27.8
        call      _for_close                                    ;27.8
                                ; LOE ebx esi edi
.B1.7:                          ; Preds .B1.6
        mov       DWORD PTR [24+esp], esi                       ;28.8
        push      32                                            ;28.8
        push      esi                                           ;28.8
        push      OFFSET FLAT: __STRLITPACK_6.0.1               ;28.8
        push      -2088435968                                   ;28.8
        push      47                                            ;28.8
        push      ebx                                           ;28.8
        call      _for_close                                    ;28.8
                                ; LOE ebx esi edi
.B1.8:                          ; Preds .B1.7
        mov       DWORD PTR [48+esp], esi                       ;29.8
        push      32                                            ;29.8
        push      esi                                           ;29.8
        push      OFFSET FLAT: __STRLITPACK_7.0.1               ;29.8
        push      -2088435968                                   ;29.8
        push      48                                            ;29.8
        push      ebx                                           ;29.8
        call      _for_close                                    ;29.8
                                ; LOE ebx esi edi
.B1.9:                          ; Preds .B1.8
        mov       DWORD PTR [72+esp], esi                       ;30.8
        push      32                                            ;30.8
        push      esi                                           ;30.8
        push      OFFSET FLAT: __STRLITPACK_8.0.1               ;30.8
        push      -2088435968                                   ;30.8
        push      49                                            ;30.8
        push      ebx                                           ;30.8
        call      _for_close                                    ;30.8
                                ; LOE ebx esi edi
.B1.10:                         ; Preds .B1.9
        mov       DWORD PTR [96+esp], esi                       ;31.8
        push      32                                            ;31.8
        push      esi                                           ;31.8
        push      OFFSET FLAT: __STRLITPACK_9.0.1               ;31.8
        push      -2088435968                                   ;31.8
        push      50                                            ;31.8
        push      ebx                                           ;31.8
        call      _for_close                                    ;31.8
                                ; LOE ebx esi edi
.B1.87:                         ; Preds .B1.10
        add       esp, 120                                      ;31.8
                                ; LOE ebx esi edi
.B1.11:                         ; Preds .B1.87
        mov       DWORD PTR [esp], esi                          ;32.8
        push      32                                            ;32.8
        push      esi                                           ;32.8
        push      OFFSET FLAT: __STRLITPACK_10.0.1              ;32.8
        push      -2088435968                                   ;32.8
        push      51                                            ;32.8
        push      ebx                                           ;32.8
        call      _for_close                                    ;32.8
                                ; LOE ebx esi edi
.B1.12:                         ; Preds .B1.11
        mov       DWORD PTR [24+esp], esi                       ;33.8
        push      32                                            ;33.8
        push      esi                                           ;33.8
        push      OFFSET FLAT: __STRLITPACK_11.0.1              ;33.8
        push      -2088435968                                   ;33.8
        push      52                                            ;33.8
        push      ebx                                           ;33.8
        call      _for_close                                    ;33.8
                                ; LOE ebx esi edi
.B1.13:                         ; Preds .B1.12
        mov       DWORD PTR [48+esp], esi                       ;34.8
        push      32                                            ;34.8
        push      esi                                           ;34.8
        push      OFFSET FLAT: __STRLITPACK_12.0.1              ;34.8
        push      -2088435968                                   ;34.8
        push      53                                            ;34.8
        push      ebx                                           ;34.8
        call      _for_close                                    ;34.8
                                ; LOE ebx esi edi
.B1.14:                         ; Preds .B1.13
        mov       DWORD PTR [72+esp], esi                       ;35.11
        push      32                                            ;35.11
        push      esi                                           ;35.11
        push      OFFSET FLAT: __STRLITPACK_13.0.1              ;35.11
        push      -2088435968                                   ;35.11
        push      54                                            ;35.11
        push      ebx                                           ;35.11
        call      _for_close                                    ;35.11
                                ; LOE ebx esi edi
.B1.15:                         ; Preds .B1.14
        mov       DWORD PTR [96+esp], esi                       ;36.11
        push      32                                            ;36.11
        push      esi                                           ;36.11
        push      OFFSET FLAT: __STRLITPACK_14.0.1              ;36.11
        push      -2088435968                                   ;36.11
        push      544                                           ;36.11
        push      ebx                                           ;36.11
        call      _for_close                                    ;36.11
                                ; LOE ebx esi edi
.B1.88:                         ; Preds .B1.15
        add       esp, 120                                      ;36.11
                                ; LOE ebx esi edi
.B1.16:                         ; Preds .B1.88
        mov       DWORD PTR [esp], esi                          ;37.11
        push      32                                            ;37.11
        push      esi                                           ;37.11
        push      OFFSET FLAT: __STRLITPACK_15.0.1              ;37.11
        push      -2088435968                                   ;37.11
        push      545                                           ;37.11
        push      ebx                                           ;37.11
        call      _for_close                                    ;37.11
                                ; LOE ebx esi edi
.B1.17:                         ; Preds .B1.16
        mov       DWORD PTR [24+esp], esi                       ;38.11
        push      32                                            ;38.11
        push      esi                                           ;38.11
        push      OFFSET FLAT: __STRLITPACK_16.0.1              ;38.11
        push      -2088435968                                   ;38.11
        push      7890                                          ;38.11
        push      ebx                                           ;38.11
        call      _for_close                                    ;38.11
                                ; LOE ebx esi edi
.B1.18:                         ; Preds .B1.17
        mov       DWORD PTR [48+esp], esi                       ;39.11
        push      32                                            ;39.11
        push      esi                                           ;39.11
        push      OFFSET FLAT: __STRLITPACK_17.0.1              ;39.11
        push      -2088435968                                   ;39.11
        push      55                                            ;39.11
        push      ebx                                           ;39.11
        call      _for_close                                    ;39.11
                                ; LOE ebx esi edi
.B1.19:                         ; Preds .B1.18
        mov       DWORD PTR [72+esp], esi                       ;40.11
        push      32                                            ;40.11
        push      esi                                           ;40.11
        push      OFFSET FLAT: __STRLITPACK_18.0.1              ;40.11
        push      -2088435968                                   ;40.11
        push      56                                            ;40.11
        push      ebx                                           ;40.11
        call      _for_close                                    ;40.11
                                ; LOE ebx esi edi
.B1.20:                         ; Preds .B1.19
        mov       DWORD PTR [96+esp], esi                       ;41.11
        push      32                                            ;41.11
        push      esi                                           ;41.11
        push      OFFSET FLAT: __STRLITPACK_19.0.1              ;41.11
        push      -2088435968                                   ;41.11
        push      57                                            ;41.11
        push      ebx                                           ;41.11
        call      _for_close                                    ;41.11
                                ; LOE ebx esi edi
.B1.89:                         ; Preds .B1.20
        add       esp, 120                                      ;41.11
                                ; LOE ebx esi edi
.B1.21:                         ; Preds .B1.89
        mov       DWORD PTR [esp], esi                          ;42.11
        push      32                                            ;42.11
        push      esi                                           ;42.11
        push      OFFSET FLAT: __STRLITPACK_20.0.1              ;42.11
        push      -2088435968                                   ;42.11
        push      58                                            ;42.11
        push      ebx                                           ;42.11
        call      _for_close                                    ;42.11
                                ; LOE ebx esi edi
.B1.22:                         ; Preds .B1.21
        mov       DWORD PTR [24+esp], esi                       ;43.11
        push      32                                            ;43.11
        push      esi                                           ;43.11
        push      OFFSET FLAT: __STRLITPACK_21.0.1              ;43.11
        push      -2088435968                                   ;43.11
        push      59                                            ;43.11
        push      ebx                                           ;43.11
        call      _for_close                                    ;43.11
                                ; LOE ebx esi edi
.B1.23:                         ; Preds .B1.22
        mov       DWORD PTR [48+esp], esi                       ;44.11
        push      32                                            ;44.11
        push      esi                                           ;44.11
        push      OFFSET FLAT: __STRLITPACK_22.0.1              ;44.11
        push      -2088435968                                   ;44.11
        push      60                                            ;44.11
        push      ebx                                           ;44.11
        call      _for_close                                    ;44.11
                                ; LOE ebx esi edi
.B1.24:                         ; Preds .B1.23
        mov       DWORD PTR [72+esp], esi                       ;45.8
        push      32                                            ;45.8
        push      esi                                           ;45.8
        push      OFFSET FLAT: __STRLITPACK_23.0.1              ;45.8
        push      -2088435968                                   ;45.8
        push      95                                            ;45.8
        push      ebx                                           ;45.8
        call      _for_close                                    ;45.8
                                ; LOE ebx esi edi
.B1.25:                         ; Preds .B1.24
        mov       DWORD PTR [96+esp], esi                       ;46.8
        push      32                                            ;46.8
        push      esi                                           ;46.8
        push      OFFSET FLAT: __STRLITPACK_24.0.1              ;46.8
        push      -2088435968                                   ;46.8
        push      101                                           ;46.8
        push      ebx                                           ;46.8
        call      _for_close                                    ;46.8
                                ; LOE ebx esi edi
.B1.90:                         ; Preds .B1.25
        add       esp, 120                                      ;46.8
                                ; LOE ebx esi edi
.B1.26:                         ; Preds .B1.90
        mov       DWORD PTR [esp], esi                          ;47.8
        push      32                                            ;47.8
        push      esi                                           ;47.8
        push      OFFSET FLAT: __STRLITPACK_25.0.1              ;47.8
        push      -2088435968                                   ;47.8
        push      97                                            ;47.8
        push      ebx                                           ;47.8
        call      _for_close                                    ;47.8
                                ; LOE ebx esi edi
.B1.27:                         ; Preds .B1.26
        mov       DWORD PTR [24+esp], esi                       ;48.8
        push      32                                            ;48.8
        push      esi                                           ;48.8
        push      OFFSET FLAT: __STRLITPACK_26.0.1              ;48.8
        push      -2088435968                                   ;48.8
        push      98                                            ;48.8
        push      ebx                                           ;48.8
        call      _for_close                                    ;48.8
                                ; LOE ebx esi edi
.B1.28:                         ; Preds .B1.27
        mov       DWORD PTR [48+esp], esi                       ;49.11
        push      32                                            ;49.11
        push      esi                                           ;49.11
        push      OFFSET FLAT: __STRLITPACK_27.0.1              ;49.11
        push      -2088435968                                   ;49.11
        push      18                                            ;49.11
        push      ebx                                           ;49.11
        call      _for_close                                    ;49.11
                                ; LOE ebx esi edi
.B1.29:                         ; Preds .B1.28
        mov       DWORD PTR [72+esp], esi                       ;50.8
        push      32                                            ;50.8
        push      esi                                           ;50.8
        push      OFFSET FLAT: __STRLITPACK_28.0.1              ;50.8
        push      -2088435968                                   ;50.8
        push      30                                            ;50.8
        push      ebx                                           ;50.8
        call      _for_close                                    ;50.8
                                ; LOE ebx esi edi
.B1.30:                         ; Preds .B1.29
        mov       DWORD PTR [96+esp], esi                       ;51.11
        push      32                                            ;51.11
        push      esi                                           ;51.11
        push      OFFSET FLAT: __STRLITPACK_29.0.1              ;51.11
        push      -2088435968                                   ;51.11
        push      301                                           ;51.11
        push      ebx                                           ;51.11
        call      _for_close                                    ;51.11
                                ; LOE ebx esi edi
.B1.91:                         ; Preds .B1.30
        add       esp, 120                                      ;51.11
                                ; LOE ebx esi edi
.B1.31:                         ; Preds .B1.91
        mov       DWORD PTR [esp], esi                          ;52.8
        push      32                                            ;52.8
        push      esi                                           ;52.8
        push      OFFSET FLAT: __STRLITPACK_30.0.1              ;52.8
        push      -2088435968                                   ;52.8
        push      31                                            ;52.8
        push      ebx                                           ;52.8
        call      _for_close                                    ;52.8
                                ; LOE ebx esi edi
.B1.32:                         ; Preds .B1.31
        mov       eax, 32                                       ;53.8
        mov       DWORD PTR [24+esp], esi                       ;53.8
        push      eax                                           ;53.8
        push      esi                                           ;53.8
        push      OFFSET FLAT: __STRLITPACK_31.0.1              ;53.8
        push      -2088435968                                   ;53.8
        push      eax                                           ;53.8
        push      ebx                                           ;53.8
        call      _for_close                                    ;53.8
                                ; LOE ebx esi edi
.B1.33:                         ; Preds .B1.32
        mov       DWORD PTR [48+esp], esi                       ;54.8
        push      32                                            ;54.8
        push      esi                                           ;54.8
        push      OFFSET FLAT: __STRLITPACK_32.0.1              ;54.8
        push      -2088435968                                   ;54.8
        push      33                                            ;54.8
        push      ebx                                           ;54.8
        call      _for_close                                    ;54.8
                                ; LOE ebx esi edi
.B1.34:                         ; Preds .B1.33
        mov       DWORD PTR [72+esp], esi                       ;55.8
        push      32                                            ;55.8
        push      esi                                           ;55.8
        push      OFFSET FLAT: __STRLITPACK_33.0.1              ;55.8
        push      -2088435968                                   ;55.8
        push      34                                            ;55.8
        push      ebx                                           ;55.8
        call      _for_close                                    ;55.8
                                ; LOE ebx esi edi
.B1.35:                         ; Preds .B1.34
        mov       DWORD PTR [96+esp], esi                       ;56.8
        push      32                                            ;56.8
        push      esi                                           ;56.8
        push      OFFSET FLAT: __STRLITPACK_34.0.1              ;56.8
        push      -2088435968                                   ;56.8
        push      35                                            ;56.8
        push      ebx                                           ;56.8
        call      _for_close                                    ;56.8
                                ; LOE ebx esi edi
.B1.92:                         ; Preds .B1.35
        add       esp, 120                                      ;56.8
                                ; LOE ebx esi edi
.B1.36:                         ; Preds .B1.92
        mov       DWORD PTR [esp], esi                          ;57.8
        push      32                                            ;57.8
        push      esi                                           ;57.8
        push      OFFSET FLAT: __STRLITPACK_35.0.1              ;57.8
        push      -2088435968                                   ;57.8
        push      36                                            ;57.8
        push      ebx                                           ;57.8
        call      _for_close                                    ;57.8
                                ; LOE ebx esi edi
.B1.37:                         ; Preds .B1.36
        mov       DWORD PTR [24+esp], esi                       ;58.8
        push      32                                            ;58.8
        push      esi                                           ;58.8
        push      OFFSET FLAT: __STRLITPACK_36.0.1              ;58.8
        push      -2088435968                                   ;58.8
        push      37                                            ;58.8
        push      ebx                                           ;58.8
        call      _for_close                                    ;58.8
                                ; LOE ebx esi edi
.B1.38:                         ; Preds .B1.37
        mov       DWORD PTR [48+esp], esi                       ;59.8
        push      32                                            ;59.8
        push      esi                                           ;59.8
        push      OFFSET FLAT: __STRLITPACK_37.0.1              ;59.8
        push      -2088435968                                   ;59.8
        push      38                                            ;59.8
        push      ebx                                           ;59.8
        call      _for_close                                    ;59.8
                                ; LOE ebx esi edi
.B1.39:                         ; Preds .B1.38
        mov       DWORD PTR [72+esp], esi                       ;60.8
        push      32                                            ;60.8
        push      esi                                           ;60.8
        push      OFFSET FLAT: __STRLITPACK_38.0.1              ;60.8
        push      -2088435968                                   ;60.8
        push      39                                            ;60.8
        push      ebx                                           ;60.8
        call      _for_close                                    ;60.8
                                ; LOE ebx esi edi
.B1.40:                         ; Preds .B1.39
        mov       DWORD PTR [96+esp], esi                       ;61.11
        push      32                                            ;61.11
        push      esi                                           ;61.11
        push      OFFSET FLAT: __STRLITPACK_39.0.1              ;61.11
        push      -2088435968                                   ;61.11
        push      40                                            ;61.11
        push      ebx                                           ;61.11
        call      _for_close                                    ;61.11
                                ; LOE ebx esi edi
.B1.93:                         ; Preds .B1.40
        add       esp, 120                                      ;61.11
                                ; LOE ebx esi edi
.B1.41:                         ; Preds .B1.93
        mov       DWORD PTR [esp], esi                          ;62.11
        push      32                                            ;62.11
        push      esi                                           ;62.11
        push      OFFSET FLAT: __STRLITPACK_40.0.1              ;62.11
        push      -2088435968                                   ;62.11
        push      500                                           ;62.11
        push      ebx                                           ;62.11
        call      _for_close                                    ;62.11
                                ; LOE ebx esi edi
.B1.42:                         ; Preds .B1.41
        mov       DWORD PTR [24+esp], esi                       ;63.8
        push      32                                            ;63.8
        push      esi                                           ;63.8
        push      OFFSET FLAT: __STRLITPACK_41.0.1              ;63.8
        push      -2088435968                                   ;63.8
        push      550                                           ;63.8
        push      ebx                                           ;63.8
        call      _for_close                                    ;63.8
                                ; LOE ebx esi edi
.B1.43:                         ; Preds .B1.42
        mov       DWORD PTR [48+esp], esi                       ;64.8
        push      32                                            ;64.8
        push      esi                                           ;64.8
        push      OFFSET FLAT: __STRLITPACK_42.0.1              ;64.8
        push      -2088435968                                   ;64.8
        push      188                                           ;64.8
        push      ebx                                           ;64.8
        call      _for_close                                    ;64.8
                                ; LOE ebx esi edi
.B1.44:                         ; Preds .B1.43
        mov       DWORD PTR [72+esp], esi                       ;65.8
        push      32                                            ;65.8
        push      esi                                           ;65.8
        push      OFFSET FLAT: __STRLITPACK_43.0.1              ;65.8
        push      -2088435968                                   ;65.8
        push      600                                           ;65.8
        push      ebx                                           ;65.8
        call      _for_close                                    ;65.8
                                ; LOE ebx esi edi
.B1.45:                         ; Preds .B1.44
        mov       DWORD PTR [96+esp], esi                       ;66.8
        push      32                                            ;66.8
        push      esi                                           ;66.8
        push      OFFSET FLAT: __STRLITPACK_44.0.1              ;66.8
        push      -2088435968                                   ;66.8
        push      700                                           ;66.8
        push      ebx                                           ;66.8
        call      _for_close                                    ;66.8
                                ; LOE ebx esi edi
.B1.94:                         ; Preds .B1.45
        add       esp, 120                                      ;66.8
                                ; LOE ebx esi edi
.B1.46:                         ; Preds .B1.94
        mov       DWORD PTR [esp], esi                          ;67.8
        push      32                                            ;67.8
        push      esi                                           ;67.8
        push      OFFSET FLAT: __STRLITPACK_45.0.1              ;67.8
        push      -2088435968                                   ;67.8
        push      800                                           ;67.8
        push      ebx                                           ;67.8
        call      _for_close                                    ;67.8
                                ; LOE ebx esi edi
.B1.47:                         ; Preds .B1.46
        mov       DWORD PTR [24+esp], esi                       ;68.8
        push      32                                            ;68.8
        push      esi                                           ;68.8
        push      OFFSET FLAT: __STRLITPACK_46.0.1              ;68.8
        push      -2088435968                                   ;68.8
        push      900                                           ;68.8
        push      ebx                                           ;68.8
        call      _for_close                                    ;68.8
                                ; LOE ebx esi edi
.B1.48:                         ; Preds .B1.47
        mov       DWORD PTR [48+esp], esi                       ;69.8
        push      32                                            ;69.8
        push      esi                                           ;69.8
        push      OFFSET FLAT: __STRLITPACK_47.0.1              ;69.8
        push      -2088435968                                   ;69.8
        push      911                                           ;69.8
        push      ebx                                           ;69.8
        call      _for_close                                    ;69.8
                                ; LOE ebx esi edi
.B1.49:                         ; Preds .B1.48
        mov       DWORD PTR [72+esp], esi                       ;70.11
        push      32                                            ;70.11
        push      esi                                           ;70.11
        push      OFFSET FLAT: __STRLITPACK_48.0.1              ;70.11
        push      -2088435968                                   ;70.11
        push      200                                           ;70.11
        push      ebx                                           ;70.11
        call      _for_close                                    ;70.11
                                ; LOE ebx esi edi
.B1.50:                         ; Preds .B1.49
        mov       DWORD PTR [96+esp], esi                       ;71.11
        push      32                                            ;71.11
        push      esi                                           ;71.11
        push      OFFSET FLAT: __STRLITPACK_49.0.1              ;71.11
        push      -2088435968                                   ;71.11
        push      461                                           ;71.11
        push      ebx                                           ;71.11
        call      _for_close                                    ;71.11
                                ; LOE ebx esi edi
.B1.95:                         ; Preds .B1.50
        add       esp, 120                                      ;71.11
                                ; LOE ebx esi edi
.B1.51:                         ; Preds .B1.95
        mov       DWORD PTR [esp], esi                          ;72.8
        push      32                                            ;72.8
        push      esi                                           ;72.8
        push      OFFSET FLAT: __STRLITPACK_50.0.1              ;72.8
        push      -2088435968                                   ;72.8
        push      6544                                          ;72.8
        push      ebx                                           ;72.8
        call      _for_close                                    ;72.8
                                ; LOE ebx esi edi
.B1.52:                         ; Preds .B1.51
        mov       DWORD PTR [24+esp], esi                       ;73.12
        push      32                                            ;73.12
        push      esi                                           ;73.12
        push      OFFSET FLAT: __STRLITPACK_51.0.1              ;73.12
        push      -2088435968                                   ;73.12
        push      911                                           ;73.12
        push      ebx                                           ;73.12
        call      _for_close                                    ;73.12
                                ; LOE ebx esi edi
.B1.53:                         ; Preds .B1.52
        mov       DWORD PTR [48+esp], esi                       ;74.11
        push      32                                            ;74.11
        push      esi                                           ;74.11
        push      OFFSET FLAT: __STRLITPACK_52.0.1              ;74.11
        push      -2088435968                                   ;74.11
        push      62                                            ;74.11
        push      ebx                                           ;74.11
        call      _for_close                                    ;74.11
                                ; LOE ebx esi edi
.B1.54:                         ; Preds .B1.53
        mov       DWORD PTR [72+esp], esi                       ;75.11
        push      32                                            ;75.11
        push      esi                                           ;75.11
        push      OFFSET FLAT: __STRLITPACK_53.0.1              ;75.11
        push      -2088435968                                   ;75.11
        push      63                                            ;75.11
        push      ebx                                           ;75.11
        call      _for_close                                    ;75.11
                                ; LOE ebx esi edi
.B1.55:                         ; Preds .B1.54
        mov       DWORD PTR [96+esp], esi                       ;76.11
        push      32                                            ;76.11
        push      esi                                           ;76.11
        push      OFFSET FLAT: __STRLITPACK_54.0.1              ;76.11
        push      -2088435968                                   ;76.11
        push      97                                            ;76.11
        push      ebx                                           ;76.11
        call      _for_close                                    ;76.11
                                ; LOE ebx esi edi
.B1.96:                         ; Preds .B1.55
        add       esp, 120                                      ;76.11
                                ; LOE ebx esi edi
.B1.56:                         ; Preds .B1.96
        mov       DWORD PTR [esp], esi                          ;77.11
        push      32                                            ;77.11
        push      esi                                           ;77.11
        push      OFFSET FLAT: __STRLITPACK_55.0.1              ;77.11
        push      -2088435968                                   ;77.11
        push      98                                            ;77.11
        push      ebx                                           ;77.11
        call      _for_close                                    ;77.11
                                ; LOE ebx esi edi
.B1.57:                         ; Preds .B1.56
        mov       DWORD PTR [24+esp], esi                       ;78.11
        push      32                                            ;78.11
        push      esi                                           ;78.11
        push      OFFSET FLAT: __STRLITPACK_56.0.1              ;78.11
        push      -2088435968                                   ;78.11
        push      511                                           ;78.11
        push      ebx                                           ;78.11
        call      _for_close                                    ;78.11
                                ; LOE ebx esi edi
.B1.58:                         ; Preds .B1.57
        mov       DWORD PTR [48+esp], esi                       ;79.11
        push      32                                            ;79.11
        push      esi                                           ;79.11
        push      OFFSET FLAT: __STRLITPACK_57.0.1              ;79.11
        push      -2088435968                                   ;79.11
        push      400                                           ;79.11
        push      ebx                                           ;79.11
        call      _for_close                                    ;79.11
                                ; LOE ebx esi edi
.B1.59:                         ; Preds .B1.58
        mov       DWORD PTR [72+esp], esi                       ;80.11
        push      32                                            ;80.11
        push      esi                                           ;80.11
        push      OFFSET FLAT: __STRLITPACK_58.0.1              ;80.11
        push      -2088435968                                   ;80.11
        push      401                                           ;80.11
        push      ebx                                           ;80.11
        call      _for_close                                    ;80.11
                                ; LOE ebx esi edi
.B1.97:                         ; Preds .B1.59
        add       esp, 96                                       ;80.11
                                ; LOE ebx esi edi
.B1.60:                         ; Preds .B1.97
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_TRAJALT], 0 ;81.16
        jle       .B1.63        ; Prob 16%                      ;81.16
                                ; LOE ebx esi edi
.B1.61:                         ; Preds .B1.60
        mov       DWORD PTR [esp], esi                          ;82.10
        push      32                                            ;82.10
        push      esi                                           ;82.10
        push      OFFSET FLAT: __STRLITPACK_59.0.1              ;82.10
        push      -2088435968                                   ;82.10
        push      988                                           ;82.10
        push      ebx                                           ;82.10
        call      _for_close                                    ;82.10
                                ; LOE ebx esi edi
.B1.62:                         ; Preds .B1.61
        mov       DWORD PTR [24+esp], esi                       ;83.10
        push      32                                            ;83.10
        push      esi                                           ;83.10
        push      OFFSET FLAT: __STRLITPACK_60.0.1              ;83.10
        push      -2088435968                                   ;83.10
        push      990                                           ;83.10
        push      ebx                                           ;83.10
        call      _for_close                                    ;83.10
                                ; LOE ebx esi edi
.B1.98:                         ; Preds .B1.62
        add       esp, 48                                       ;83.10
                                ; LOE ebx esi edi
.B1.63:                         ; Preds .B1.98 .B1.60
        mov       DWORD PTR [esp], 0                            ;85.5
        push      32                                            ;85.5
        push      -2088435968                                   ;85.5
        push      989                                           ;85.5
        push      ebx                                           ;85.5
        call      _for_rewind                                   ;85.5
                                ; LOE ebx esi edi
.B1.64:                         ; Preds .B1.63
        mov       DWORD PTR [16+esp], esi                       ;86.8
        push      32                                            ;86.8
        push      esi                                           ;86.8
        push      OFFSET FLAT: __STRLITPACK_61.0.1              ;86.8
        push      -2088435968                                   ;86.8
        push      888                                           ;86.8
        push      ebx                                           ;86.8
        call      _for_close                                    ;86.8
                                ; LOE ebx esi edi
.B1.65:                         ; Preds .B1.64
        mov       DWORD PTR [40+esp], esi                       ;87.8
        push      32                                            ;87.8
        push      esi                                           ;87.8
        push      OFFSET FLAT: __STRLITPACK_62.0.1              ;87.8
        push      -2088435968                                   ;87.8
        push      777                                           ;87.8
        push      ebx                                           ;87.8
        call      _for_close                                    ;87.8
                                ; LOE ebx esi edi
.B1.66:                         ; Preds .B1.65
        mov       DWORD PTR [64+esp], esi                       ;88.8
        push      32                                            ;88.8
        push      esi                                           ;88.8
        push      OFFSET FLAT: __STRLITPACK_63.0.1              ;88.8
        push      -2088435968                                   ;88.8
        push      512                                           ;88.8
        push      ebx                                           ;88.8
        call      _for_close                                    ;88.8
                                ; LOE ebx esi edi
.B1.67:                         ; Preds .B1.66
        mov       DWORD PTR [88+esp], esi                       ;89.8
        push      32                                            ;89.8
        push      esi                                           ;89.8
        push      OFFSET FLAT: __STRLITPACK_64.0.1              ;89.8
        push      -2088435968                                   ;89.8
        push      513                                           ;89.8
        push      ebx                                           ;89.8
        call      _for_close                                    ;89.8
                                ; LOE ebx esi edi
.B1.99:                         ; Preds .B1.67
        add       esp, 112                                      ;89.8
                                ; LOE ebx esi edi
.B1.68:                         ; Preds .B1.99
        mov       DWORD PTR [esp], esi                          ;90.8
        push      32                                            ;90.8
        push      esi                                           ;90.8
        push      OFFSET FLAT: __STRLITPACK_65.0.1              ;90.8
        push      -2088435968                                   ;90.8
        push      514                                           ;90.8
        push      ebx                                           ;90.8
        call      _for_close                                    ;90.8
                                ; LOE ebx esi edi
.B1.69:                         ; Preds .B1.68
        mov       DWORD PTR [24+esp], esi                       ;91.8
        push      32                                            ;91.8
        push      esi                                           ;91.8
        push      OFFSET FLAT: __STRLITPACK_66.0.1              ;91.8
        push      -2088435968                                   ;91.8
        push      6057                                          ;91.8
        push      ebx                                           ;91.8
        call      _for_close                                    ;91.8
                                ; LOE ebx esi edi
.B1.70:                         ; Preds .B1.69
        mov       DWORD PTR [48+esp], esi                       ;92.8
        push      32                                            ;92.8
        push      esi                                           ;92.8
        push      OFFSET FLAT: __STRLITPACK_67.0.1              ;92.8
        push      -2088435968                                   ;92.8
        push      6058                                          ;92.8
        push      ebx                                           ;92.8
        call      _for_close                                    ;92.8
                                ; LOE ebx esi edi
.B1.71:                         ; Preds .B1.70
        mov       DWORD PTR [72+esp], esi                       ;93.8
        push      32                                            ;93.8
        push      esi                                           ;93.8
        push      OFFSET FLAT: __STRLITPACK_68.0.1              ;93.8
        push      -2088435968                                   ;93.8
        push      6059                                          ;93.8
        push      ebx                                           ;93.8
        call      _for_close                                    ;93.8
                                ; LOE ebx esi edi
.B1.72:                         ; Preds .B1.71
        mov       DWORD PTR [96+esp], esi                       ;94.8
        push      32                                            ;94.8
        push      esi                                           ;94.8
        push      OFFSET FLAT: __STRLITPACK_69.0.1              ;94.8
        push      -2088435968                                   ;94.8
        push      6060                                          ;94.8
        push      ebx                                           ;94.8
        call      _for_close                                    ;94.8
                                ; LOE ebx esi edi
.B1.100:                        ; Preds .B1.72
        add       esp, 120                                      ;94.8
                                ; LOE ebx esi edi
.B1.73:                         ; Preds .B1.100
        mov       DWORD PTR [esp], esi                          ;95.11
        push      32                                            ;95.11
        push      esi                                           ;95.11
        push      OFFSET FLAT: __STRLITPACK_70.0.1              ;95.11
        push      -2088435968                                   ;95.11
        push      95                                            ;95.11
        push      ebx                                           ;95.11
        call      _for_close                                    ;95.11
                                ; LOE ebx esi edi
.B1.74:                         ; Preds .B1.73
        mov       DWORD PTR [24+esp], esi                       ;96.11
        push      32                                            ;96.11
        push      esi                                           ;96.11
        push      OFFSET FLAT: __STRLITPACK_71.0.1              ;96.11
        push      -2088435968                                   ;96.11
        push      2000                                          ;96.11
        push      ebx                                           ;96.11
        call      _for_close                                    ;96.11
                                ; LOE ebx esi edi
.B1.75:                         ; Preds .B1.74
        mov       DWORD PTR [48+esp], esi                       ;97.11
        push      32                                            ;97.11
        push      esi                                           ;97.11
        push      OFFSET FLAT: __STRLITPACK_72.0.1              ;97.11
        push      -2088435968                                   ;97.11
        push      2001                                          ;97.11
        push      ebx                                           ;97.11
        call      _for_close                                    ;97.11
                                ; LOE ebx esi edi
.B1.76:                         ; Preds .B1.75
        mov       DWORD PTR [72+esp], esi                       ;98.11
        push      32                                            ;98.11
        push      esi                                           ;98.11
        push      OFFSET FLAT: __STRLITPACK_73.0.1              ;98.11
        push      -2088435968                                   ;98.11
        push      4567                                          ;98.11
        push      ebx                                           ;98.11
        call      _for_close                                    ;98.11
                                ; LOE ebx esi edi
.B1.77:                         ; Preds .B1.76
        mov       DWORD PTR [96+esp], esi                       ;99.11
        push      32                                            ;99.11
        push      esi                                           ;99.11
        push      OFFSET FLAT: __STRLITPACK_74.0.1              ;99.11
        push      -2088435968                                   ;99.11
        push      5543                                          ;99.11
        push      ebx                                           ;99.11
        call      _for_close                                    ;99.11
                                ; LOE ebx esi edi
.B1.101:                        ; Preds .B1.77
        add       esp, 120                                      ;99.11
                                ; LOE ebx esi edi
.B1.78:                         ; Preds .B1.101
        mov       DWORD PTR [esp], esi                          ;100.11
        push      32                                            ;100.11
        push      esi                                           ;100.11
        push      OFFSET FLAT: __STRLITPACK_75.0.1              ;100.11
        push      -2088435968                                   ;100.11
        push      139                                           ;100.11
        push      ebx                                           ;100.11
        call      _for_close                                    ;100.11
                                ; LOE ebx esi edi
.B1.79:                         ; Preds .B1.78
        mov       DWORD PTR [24+esp], esi                       ;101.11
        push      32                                            ;101.11
        push      esi                                           ;101.11
        push      OFFSET FLAT: __STRLITPACK_76.0.1              ;101.11
        push      -2088435968                                   ;101.11
        push      8456                                          ;101.11
        push      ebx                                           ;101.11
        call      _for_close                                    ;101.11
                                ; LOE ebx esi edi
.B1.80:                         ; Preds .B1.79
        mov       DWORD PTR [48+esp], esi                       ;102.11
        push      32                                            ;102.11
        push      esi                                           ;102.11
        push      OFFSET FLAT: __STRLITPACK_77.0.1              ;102.11
        push      -2088435968                                   ;102.11
        push      7099                                          ;102.11
        push      ebx                                           ;102.11
        call      _for_close                                    ;102.11
                                ; LOE ebx esi edi
.B1.81:                         ; Preds .B1.80
        mov       DWORD PTR [72+esp], esi                       ;103.11
        push      32                                            ;103.11
        push      esi                                           ;103.11
        push      OFFSET FLAT: __STRLITPACK_78.0.1              ;103.11
        push      -2088435968                                   ;103.11
        push      9987                                          ;103.11
        push      ebx                                           ;103.11
        call      _for_close                                    ;103.11
                                ; LOE ebx esi edi
.B1.82:                         ; Preds .B1.81
        mov       DWORD PTR [96+esp], esi                       ;104.11
        push      32                                            ;104.11
        push      esi                                           ;104.11
        push      OFFSET FLAT: __STRLITPACK_79.0.1              ;104.11
        push      -2088435968                                   ;104.11
        push      5454                                          ;104.11
        push      ebx                                           ;104.11
        call      _for_close                                    ;104.11
                                ; LOE edi
.B1.83:                         ; Preds .B1.82
        add       esp, 160                                      ;105.1
        pop       ebx                                           ;105.1
        pop       esi                                           ;105.1
        mov       esp, ebp                                      ;105.1
        pop       ebp                                           ;105.1
        ret                                                     ;105.1
        ALIGN     16
                                ; LOE
; mark_end;
_CLOSEFILE ENDP
_TEXT	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
__STRLITPACK_0.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_1.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_2.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_3.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_4.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_5.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_6.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_7.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_8.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_9.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_10.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_11.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_12.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_13.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_14.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_15.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_16.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_17.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_18.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_19.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_20.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_21.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_22.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_23.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_24.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_25.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_26.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_27.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_28.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_29.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_30.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_31.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_32.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_33.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_34.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_35.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_36.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_37.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_38.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_39.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_40.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_41.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_42.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_43.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_44.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_45.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_46.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_47.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_48.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_49.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_50.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_51.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_52.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_53.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_54.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_55.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_56.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_57.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_58.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_59.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_60.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_61.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_62.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_63.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_64.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_65.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_66.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_67.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_68.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_69.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_70.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_71.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_72.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_73.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_74.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_75.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_76.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_77.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_78.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_79.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _CLOSEFILE
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
EXTRN	_DYNUST_MAIN_MODULE_mp_TRAJALT:BYTE
_DATA	ENDS
EXTRN	_for_rewind:PROC
EXTRN	_for_close:PROC
	INCLUDELIB <ifconsol>
	INCLUDELIB <libifcoremt>
	INCLUDELIB <libifport>
	INCLUDELIB <libmmt>
	INCLUDELIB <LIBCMT>
	INCLUDELIB <libirc>
	INCLUDELIB <svml_dispmt>
	INCLUDELIB <OLDNAMES>
	END
