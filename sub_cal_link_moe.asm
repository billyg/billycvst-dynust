; mark_description "Intel(R) Visual Fortran Compiler XE for applications running on IA-32, Version 13.1.1.171 Build 20130313";
; mark_description "-S";
	.686P
 	.387
	OPTION DOTNAME
	ASSUME	CS:FLAT,DS:FLAT,SS:FLAT
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
TXTST0:
; -- Begin  _CAL_LINK_MOE
; mark_begin;
IF @Version GE 800
  .MMX
ELSEIF @Version GE 612
  .MMX
  MMWORD TEXTEQU <QWORD>
ENDIF
IF @Version GE 800
  .XMM
ELSEIF @Version GE 614
  .XMM
  XMMWORD TEXTEQU <OWORD>
ENDIF
       ALIGN     16
	PUBLIC _CAL_LINK_MOE
_CAL_LINK_MOE	PROC NEAR 
; parameter 1: 8 + ebx
; parameter 2: 12 + ebx
; parameter 3: 16 + ebx
.B1.1:                          ; Preds .B1.0
        push      ebx                                           ;1.12
        mov       ebx, esp                                      ;1.12
        and       esp, -16                                      ;1.12
        push      ebp                                           ;1.12
        push      ebp                                           ;1.12
        mov       ebp, DWORD PTR [4+ebx]                        ;1.12
        mov       DWORD PTR [4+esp], ebp                        ;1.12
        mov       ebp, esp                                      ;1.12
        sub       esp, 408                                      ;1.12
        xor       eax, eax                                      ;26.23
        mov       DWORD PTR [-268+ebp], edi                     ;1.12
        mov       edi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFARCS] ;1.12
        test      edi, edi                                      ;1.12
        mov       DWORD PTR [-256+ebp], eax                     ;26.23
        mov       DWORD PTR [-252+ebp], eax                     ;26.23
        mov       DWORD PTR [-248+ebp], eax                     ;26.23
        mov       DWORD PTR [-236+ebp], eax                     ;26.23
        mov       DWORD PTR [-232+ebp], eax                     ;26.23
        mov       DWORD PTR [-228+ebp], eax                     ;26.23
        mov       DWORD PTR [-224+ebp], eax                     ;26.23
        mov       DWORD PTR [-220+ebp], eax                     ;26.23
        mov       DWORD PTR [-216+ebp], eax                     ;26.23
        mov       DWORD PTR [-212+ebp], eax                     ;26.23
        cmovg     eax, edi                                      ;1.12
        shl       eax, 2                                        ;1.12
        mov       DWORD PTR [-276+ebp], esi                     ;1.12
        mov       DWORD PTR [-244+ebp], 128                     ;26.23
        mov       DWORD PTR [-240+ebp], 2                       ;26.23
        mov       DWORD PTR [-284+ebp], esp                     ;1.12
        call      __alloca_probe                                ;1.12
        and       esp, -16                                      ;1.12
        mov       eax, esp                                      ;1.12
                                ; LOE eax edi
.B1.844:                        ; Preds .B1.1
        mov       DWORD PTR [-164+ebp], eax                     ;1.12
        test      edi, edi                                      ;33.2
        jle       .B1.4         ; Prob 50%                      ;33.2
                                ; LOE edi
.B1.2:                          ; Preds .B1.844
        cmp       edi, 24                                       ;33.2
        jle       .B1.812       ; Prob 0%                       ;33.2
                                ; LOE edi
.B1.3:                          ; Preds .B1.2
        lea       eax, DWORD PTR [edi*4]                        ;33.2
        push      eax                                           ;33.2
        push      0                                             ;33.2
        push      DWORD PTR [-164+ebp]                          ;33.2
        call      __intel_fast_memset                           ;33.2
                                ; LOE edi
.B1.845:                        ; Preds .B1.3
        add       esp, 12                                       ;33.2
        test      edi, edi                                      ;33.2
                                ; LOE edi
.B1.4:                          ; Preds .B1.819 .B1.844 .B1.845
        mov       esi, 1                                        ;34.2
        mov       eax, 0                                        ;34.2
        mov       DWORD PTR [-224+ebp], esi                     ;34.2
        mov       ecx, eax                                      ;34.2
        mov       DWORD PTR [-212+ebp], esi                     ;34.2
        cmovg     ecx, edi                                      ;34.2
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_MAXMOVE] ;34.2
        mov       edx, 4                                        ;34.2
        test      esi, esi                                      ;34.2
        mov       DWORD PTR [-264+ebp], edi                     ;
        push      edx                                           ;34.2
        mov       DWORD PTR [-248+ebp], eax                     ;34.2
        lea       edi, DWORD PTR [1+esi]                        ;34.2
        cmovl     edi, eax                                      ;34.2
        lea       eax, DWORD PTR [ecx*4]                        ;34.2
        push      edi                                           ;34.2
        push      ecx                                           ;34.2
        mov       DWORD PTR [-216+ebp], eax                     ;34.2
        lea       eax, DWORD PTR [-292+ebp]                     ;34.2
        push      3                                             ;34.2
        mov       DWORD PTR [-220+ebp], edi                     ;34.2
        push      eax                                           ;34.2
        mov       DWORD PTR [-244+ebp], 133                     ;34.2
        mov       DWORD PTR [-252+ebp], edx                     ;34.2
        mov       DWORD PTR [-240+ebp], 2                       ;34.2
        mov       DWORD PTR [-232+ebp], ecx                     ;34.2
        mov       DWORD PTR [-228+ebp], edx                     ;34.2
        mov       edi, DWORD PTR [-264+ebp]                     ;34.2
        call      _for_check_mult_overflow                      ;34.2
                                ; LOE eax edi
.B1.846:                        ; Preds .B1.4
        add       esp, 20                                       ;34.2
                                ; LOE eax edi
.B1.5:                          ; Preds .B1.846
        mov       edx, DWORD PTR [-244+ebp]                     ;34.2
        and       eax, 1                                        ;34.2
        and       edx, 1                                        ;34.2
        lea       ecx, DWORD PTR [-256+ebp]                     ;34.2
        shl       eax, 4                                        ;34.2
        add       edx, edx                                      ;34.2
        or        edx, eax                                      ;34.2
        or        edx, 262144                                   ;34.2
        push      edx                                           ;34.2
        push      ecx                                           ;34.2
        push      DWORD PTR [-292+ebp]                          ;34.2
        call      _for_alloc_allocatable                        ;34.2
                                ; LOE edi
.B1.847:                        ; Preds .B1.5
        add       esp, 12                                       ;34.2
                                ; LOE edi
.B1.6:                          ; Preds .B1.847
        mov       edx, DWORD PTR [-212+ebp]                     ;35.2
        mov       eax, DWORD PTR [-256+ebp]                     ;274.41
        mov       DWORD PTR [-368+ebp], edx                     ;35.2
        mov       edx, DWORD PTR [-220+ebp]                     ;35.2
        test      edx, edx                                      ;35.2
        mov       DWORD PTR [-108+ebp], eax                     ;274.41
        jle       .B1.8         ; Prob 10%                      ;35.2
                                ; LOE edx edi
.B1.7:                          ; Preds .B1.6
        mov       ecx, DWORD PTR [-232+ebp]                     ;35.2
        test      ecx, ecx                                      ;35.2
        mov       eax, DWORD PTR [-224+ebp]                     ;35.2
        mov       esi, DWORD PTR [-216+ebp]                     ;274.10
        jg        .B1.188       ; Prob 50%                      ;35.2
                                ; LOE eax edx ecx esi edi
.B1.8:                          ; Preds .B1.7 .B1.192 .B1.6 .B1.831 .B1.829
                                ;      
        test      edi, edi                                      ;37.5
        jle       .B1.11        ; Prob 50%                      ;37.5
                                ; LOE edi
.B1.9:                          ; Preds .B1.8
        cmp       edi, 24                                       ;37.5
        jle       .B1.833       ; Prob 0%                       ;37.5
                                ; LOE edi
.B1.10:                         ; Preds .B1.9
        lea       eax, DWORD PTR [edi*4]                        ;37.5
        push      eax                                           ;37.5
        push      0                                             ;37.5
        push      DWORD PTR [-164+ebp]                          ;37.5
        call      __intel_fast_memset                           ;37.5
                                ; LOE edi
.B1.848:                        ; Preds .B1.10
        add       esp, 12                                       ;37.5
                                ; LOE edi
.B1.11:                         ; Preds .B1.839 .B1.8 .B1.837 .B1.848
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;56.11
        mov       esi, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFARCS] ;40.9
        mov       DWORD PTR [-260+ebp], eax                     ;56.11
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_I30], 1     ;39.14
        je        .B1.770       ; Prob 16%                      ;39.14
                                ; LOE esi edi
.B1.12:                         ; Preds .B1.11
        mov       eax, DWORD PTR [8+ebx]                        ;66.8
        movss     xmm0, DWORD PTR [_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT] ;76.26
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+24] ;79.11
        mov       edx, DWORD PTR [eax]                          ;66.8
        movss     DWORD PTR [-300+ebp], xmm0                    ;76.26
        mov       DWORD PTR [-308+ebp], edx                     ;66.8
        mov       DWORD PTR [-188+ebp], ecx                     ;79.11
                                ; LOE esi edi
.B1.13:                         ; Preds .B1.807 .B1.806 .B1.801 .B1.808 .B1.12
                                ;      
        test      edi, edi                                      ;60.7
        jle       .B1.16        ; Prob 50%                      ;60.7
                                ; LOE esi edi
.B1.14:                         ; Preds .B1.13
        cmp       edi, 24                                       ;60.7
        jle       .B1.193       ; Prob 0%                       ;60.7
                                ; LOE esi edi
.B1.15:                         ; Preds .B1.14
        lea       eax, DWORD PTR [edi*4]                        ;60.7
        push      eax                                           ;60.7
        push      0                                             ;60.7
        push      DWORD PTR [-164+ebp]                          ;60.7
        call      __intel_fast_memset                           ;60.7
                                ; LOE esi edi
.B1.849:                        ; Preds .B1.15
        add       esp, 12                                       ;60.7
                                ; LOE esi edi
.B1.16:                         ; Preds .B1.199 .B1.849 .B1.13 .B1.197
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_I301], 1    ;61.15
        je        .B1.729       ; Prob 16%                      ;61.15
                                ; LOE esi edi
.B1.17:                         ; Preds .B1.736 .B1.760 .B1.765 .B1.16 .B1.766
                                ;      
        test      edi, edi                                      ;84.7
        jle       .B1.20        ; Prob 50%                      ;84.7
                                ; LOE esi edi
.B1.18:                         ; Preds .B1.17
        cmp       edi, 24                                       ;84.7
        jle       .B1.202       ; Prob 0%                       ;84.7
                                ; LOE esi edi
.B1.19:                         ; Preds .B1.18
        lea       eax, DWORD PTR [edi*4]                        ;84.7
        push      eax                                           ;84.7
        push      0                                             ;84.7
        push      DWORD PTR [-164+ebp]                          ;84.7
        call      __intel_fast_memset                           ;84.7
                                ; LOE esi edi
.B1.850:                        ; Preds .B1.19
        add       esp, 12                                       ;84.7
                                ; LOE esi edi
.B1.20:                         ; Preds .B1.208 .B1.850 .B1.17 .B1.206
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_I31], 1     ;85.14
        je        .B1.687       ; Prob 16%                      ;85.14
                                ; LOE esi edi
.B1.21:                         ; Preds .B1.20 .B1.694 .B1.695 .B1.719 .B1.724
                                ;       .B1.725
        test      edi, edi                                      ;110.7
        jle       .B1.24        ; Prob 50%                      ;110.7
                                ; LOE esi edi
.B1.22:                         ; Preds .B1.21
        cmp       edi, 24                                       ;110.7
        jle       .B1.211       ; Prob 0%                       ;110.7
                                ; LOE esi edi
.B1.23:                         ; Preds .B1.22
        lea       eax, DWORD PTR [edi*4]                        ;110.7
        push      eax                                           ;110.7
        push      0                                             ;110.7
        push      DWORD PTR [-164+ebp]                          ;110.7
        call      __intel_fast_memset                           ;110.7
                                ; LOE esi edi
.B1.851:                        ; Preds .B1.23
        add       esp, 12                                       ;110.7
                                ; LOE esi edi
.B1.24:                         ; Preds .B1.217 .B1.851 .B1.21 .B1.215
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_I32], 1     ;111.14
        je        .B1.646       ; Prob 16%                      ;111.14
                                ; LOE esi edi
.B1.25:                         ; Preds .B1.24 .B1.653 .B1.677 .B1.682 .B1.683
                                ;      
        test      edi, edi                                      ;136.7
        jle       .B1.28        ; Prob 50%                      ;136.7
                                ; LOE esi edi
.B1.26:                         ; Preds .B1.25
        cmp       edi, 24                                       ;136.7
        jle       .B1.220       ; Prob 0%                       ;136.7
                                ; LOE esi edi
.B1.27:                         ; Preds .B1.26
        lea       eax, DWORD PTR [edi*4]                        ;136.7
        push      eax                                           ;136.7
        push      0                                             ;136.7
        push      DWORD PTR [-164+ebp]                          ;136.7
        call      __intel_fast_memset                           ;136.7
                                ; LOE esi edi
.B1.852:                        ; Preds .B1.27
        add       esp, 12                                       ;136.7
                                ; LOE esi edi
.B1.28:                         ; Preds .B1.226 .B1.852 .B1.25 .B1.224
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_I33], 1     ;137.14
        je        .B1.605       ; Prob 16%                      ;137.14
                                ; LOE esi edi
.B1.29:                         ; Preds .B1.28 .B1.612 .B1.641 .B1.636 .B1.642
                                ;      
        test      edi, edi                                      ;162.7
        jle       .B1.32        ; Prob 50%                      ;162.7
                                ; LOE esi edi
.B1.30:                         ; Preds .B1.29
        cmp       edi, 24                                       ;162.7
        jle       .B1.229       ; Prob 0%                       ;162.7
                                ; LOE esi edi
.B1.31:                         ; Preds .B1.30
        lea       eax, DWORD PTR [edi*4]                        ;162.7
        push      eax                                           ;162.7
        push      0                                             ;162.7
        push      DWORD PTR [-164+ebp]                          ;162.7
        call      __intel_fast_memset                           ;162.7
                                ; LOE esi edi
.B1.853:                        ; Preds .B1.31
        add       esp, 12                                       ;162.7
                                ; LOE esi edi
.B1.32:                         ; Preds .B1.235 .B1.853 .B1.29 .B1.233
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_I34], 1     ;163.14
        je        .B1.564       ; Prob 16%                      ;163.14
                                ; LOE esi edi
.B1.33:                         ; Preds .B1.32 .B1.571 .B1.595 .B1.600 .B1.601
                                ;      
        test      edi, edi                                      ;187.7
        jle       .B1.36        ; Prob 50%                      ;187.7
                                ; LOE esi edi
.B1.34:                         ; Preds .B1.33
        cmp       edi, 24                                       ;187.7
        jle       .B1.238       ; Prob 0%                       ;187.7
                                ; LOE esi edi
.B1.35:                         ; Preds .B1.34
        lea       eax, DWORD PTR [edi*4]                        ;187.7
        push      eax                                           ;187.7
        push      0                                             ;187.7
        push      DWORD PTR [-164+ebp]                          ;187.7
        call      __intel_fast_memset                           ;187.7
                                ; LOE esi edi
.B1.854:                        ; Preds .B1.35
        add       esp, 12                                       ;187.7
                                ; LOE esi edi
.B1.36:                         ; Preds .B1.244 .B1.854 .B1.33 .B1.242
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_I35], 1     ;188.14
        je        .B1.523       ; Prob 16%                      ;188.14
                                ; LOE esi edi
.B1.37:                         ; Preds .B1.36 .B1.530 .B1.554 .B1.559 .B1.560
                                ;      
        test      edi, edi                                      ;212.7
        jle       .B1.40        ; Prob 50%                      ;212.7
                                ; LOE esi edi
.B1.38:                         ; Preds .B1.37
        cmp       edi, 24                                       ;212.7
        jle       .B1.247       ; Prob 0%                       ;212.7
                                ; LOE esi edi
.B1.39:                         ; Preds .B1.38
        lea       eax, DWORD PTR [edi*4]                        ;212.7
        push      eax                                           ;212.7
        push      0                                             ;212.7
        push      DWORD PTR [-164+ebp]                          ;212.7
        call      __intel_fast_memset                           ;212.7
                                ; LOE esi edi
.B1.855:                        ; Preds .B1.39
        add       esp, 12                                       ;212.7
                                ; LOE esi edi
.B1.40:                         ; Preds .B1.253 .B1.855 .B1.37 .B1.251
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_I36], 1     ;213.14
        je        .B1.482       ; Prob 16%                      ;213.14
                                ; LOE esi edi
.B1.41:                         ; Preds .B1.40 .B1.489 .B1.513 .B1.518 .B1.519
                                ;      
        test      edi, edi                                      ;237.7
        jle       .B1.44        ; Prob 50%                      ;237.7
                                ; LOE esi edi
.B1.42:                         ; Preds .B1.41
        cmp       edi, 24                                       ;237.7
        jle       .B1.256       ; Prob 0%                       ;237.7
                                ; LOE esi edi
.B1.43:                         ; Preds .B1.42
        lea       eax, DWORD PTR [edi*4]                        ;237.7
        push      eax                                           ;237.7
        push      0                                             ;237.7
        push      DWORD PTR [-164+ebp]                          ;237.7
        call      __intel_fast_memset                           ;237.7
                                ; LOE esi edi
.B1.856:                        ; Preds .B1.43
        add       esp, 12                                       ;237.7
                                ; LOE esi edi
.B1.44:                         ; Preds .B1.262 .B1.856 .B1.41 .B1.260
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_I37], 1     ;238.14
        je        .B1.441       ; Prob 16%                      ;238.14
                                ; LOE esi edi
.B1.45:                         ; Preds .B1.44 .B1.448 .B1.472 .B1.477 .B1.478
                                ;      
        test      edi, edi                                      ;262.7
        jle       .B1.48        ; Prob 50%                      ;262.7
                                ; LOE esi edi
.B1.46:                         ; Preds .B1.45
        cmp       edi, 24                                       ;262.7
        jle       .B1.265       ; Prob 0%                       ;262.7
                                ; LOE esi edi
.B1.47:                         ; Preds .B1.46
        lea       eax, DWORD PTR [edi*4]                        ;262.7
        push      eax                                           ;262.7
        push      0                                             ;262.7
        push      DWORD PTR [-164+ebp]                          ;262.7
        call      __intel_fast_memset                           ;262.7
                                ; LOE esi edi
.B1.857:                        ; Preds .B1.47
        add       esp, 12                                       ;262.7
                                ; LOE esi edi
.B1.48:                         ; Preds .B1.271 .B1.857 .B1.45 .B1.269
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_I38], 1     ;263.14
        je        .B1.410       ; Prob 16%                      ;263.14
                                ; LOE esi edi
.B1.49:                         ; Preds .B1.896 .B1.48 .B1.421
        test      edi, edi                                      ;284.7
        jle       .B1.52        ; Prob 50%                      ;284.7
                                ; LOE esi edi
.B1.50:                         ; Preds .B1.49
        cmp       edi, 24                                       ;284.7
        jle       .B1.274       ; Prob 0%                       ;284.7
                                ; LOE esi edi
.B1.51:                         ; Preds .B1.50
        lea       eax, DWORD PTR [edi*4]                        ;284.7
        push      eax                                           ;284.7
        push      0                                             ;284.7
        push      DWORD PTR [-164+ebp]                          ;284.7
        call      __intel_fast_memset                           ;284.7
                                ; LOE esi edi
.B1.858:                        ; Preds .B1.51
        add       esp, 12                                       ;284.7
                                ; LOE esi edi
.B1.52:                         ; Preds .B1.280 .B1.858 .B1.49 .B1.278
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_I39], 1     ;285.14
        je        .B1.369       ; Prob 16%                      ;285.14
                                ; LOE esi edi
.B1.53:                         ; Preds .B1.52 .B1.376 .B1.400 .B1.405 .B1.406
                                ;      
        test      edi, edi                                      ;308.7
        jle       .B1.56        ; Prob 50%                      ;308.7
                                ; LOE esi edi
.B1.54:                         ; Preds .B1.53
        cmp       edi, 24                                       ;308.7
        jle       .B1.283       ; Prob 0%                       ;308.7
                                ; LOE esi edi
.B1.55:                         ; Preds .B1.54
        shl       edi, 2                                        ;308.7
        push      edi                                           ;308.7
        push      0                                             ;308.7
        push      DWORD PTR [-164+ebp]                          ;308.7
        call      __intel_fast_memset                           ;308.7
                                ; LOE esi
.B1.859:                        ; Preds .B1.55
        add       esp, 12                                       ;308.7
                                ; LOE esi
.B1.56:                         ; Preds .B1.289 .B1.859 .B1.53 .B1.287
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_I40], 1     ;309.14
        je        .B1.305       ; Prob 16%                      ;309.14
                                ; LOE esi
.B1.57:                         ; Preds .B1.56
        movss     xmm1, DWORD PTR [_2il0floatpacket.18]         ;358.12
        divss     xmm1, DWORD PTR [-300+ebp]                    ;358.12
        movss     xmm0, DWORD PTR [_2il0floatpacket.14]         ;358.12
        andps     xmm0, xmm1                                    ;358.12
        pxor      xmm1, xmm0                                    ;358.12
        movss     xmm2, DWORD PTR [_2il0floatpacket.15]         ;358.12
        movaps    xmm3, xmm1                                    ;358.12
        movss     xmm4, DWORD PTR [_2il0floatpacket.16]         ;358.12
        cmpltss   xmm3, xmm2                                    ;358.12
        andps     xmm2, xmm3                                    ;358.12
        movaps    xmm3, xmm1                                    ;358.12
        movaps    xmm6, xmm4                                    ;358.12
        addss     xmm3, xmm2                                    ;358.12
        addss     xmm6, xmm4                                    ;358.12
        subss     xmm3, xmm2                                    ;358.12
        movaps    xmm7, xmm3                                    ;358.12
        mov       eax, DWORD PTR [-308+ebp]                     ;358.6
        subss     xmm7, xmm1                                    ;358.12
        cdq                                                     ;358.6
        movaps    xmm5, xmm7                                    ;358.12
        cmpless   xmm7, DWORD PTR [_2il0floatpacket.17]         ;358.12
        cmpnless  xmm5, xmm4                                    ;358.12
        andps     xmm5, xmm6                                    ;358.12
        andps     xmm7, xmm6                                    ;358.12
        subss     xmm3, xmm5                                    ;358.12
        addss     xmm3, xmm7                                    ;358.12
        orps      xmm3, xmm0                                    ;358.12
        cvtss2si  ecx, xmm3                                     ;358.12
        idiv      ecx                                           ;358.6
        mov       DWORD PTR [-332+ebp], ecx                     ;358.12
        mov       DWORD PTR [-324+ebp], edx                     ;358.6
                                ; LOE esi
.B1.58:                         ; Preds .B1.887 .B1.334 .B1.305 .B1.57
        test      esi, esi                                      ;349.5
        jle       .B1.70        ; Prob 0%                       ;349.5
                                ; LOE esi
.B1.59:                         ; Preds .B1.58
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;350.7
        cmp       esi, 4                                        ;349.5
        jl        .B1.304       ; Prob 10%                      ;349.5
                                ; LOE eax esi
.B1.60:                         ; Preds .B1.59
        imul      ecx, DWORD PTR [-260+ebp], -900               ;
        mov       edx, esi                                      ;349.5
        movaps    xmm0, XMMWORD PTR [_2il0floatpacket.11]       ;350.40
        and       edx, -4                                       ;349.5
        xor       edi, edi                                      ;349.5
        add       ecx, eax                                      ;
                                ; LOE eax edx ecx esi edi xmm0
.B1.61:                         ; Preds .B1.61 .B1.60
        movss     xmm4, DWORD PTR [1572+ecx]                    ;350.10
        add       edi, 4                                        ;349.5
        movss     xmm1, DWORD PTR [3372+ecx]                    ;350.10
        movss     xmm2, DWORD PTR [2472+ecx]                    ;350.10
        movss     xmm3, DWORD PTR [4272+ecx]                    ;350.10
        unpcklps  xmm4, xmm1                                    ;350.10
        unpcklps  xmm2, xmm3                                    ;350.10
        unpcklps  xmm4, xmm2                                    ;350.10
        movss     xmm3, DWORD PTR [1696+ecx]                    ;353.43
        movaps    xmm1, xmm4                                    ;350.40
        movss     xmm6, DWORD PTR [3496+ecx]                    ;353.43
        movss     xmm2, DWORD PTR [2596+ecx]                    ;353.43
        movss     xmm7, DWORD PTR [4396+ecx]                    ;353.43
        unpcklps  xmm3, xmm6                                    ;353.43
        unpcklps  xmm2, xmm7                                    ;353.43
        unpcklps  xmm3, xmm2                                    ;353.43
        movd      xmm5, DWORD PTR [4000+ecx]                    ;351.85
        movd      xmm2, DWORD PTR [2200+ecx]                    ;351.85
        movd      xmm6, DWORD PTR [3100+ecx]                    ;351.85
        movd      xmm7, DWORD PTR [1300+ecx]                    ;351.85
        punpckldq xmm2, xmm5                                    ;351.85
        punpckldq xmm7, xmm6                                    ;351.85
        punpckldq xmm7, xmm2                                    ;351.85
        movd      xmm6, DWORD PTR [3608+ecx]                    ;351.127
        movd      xmm5, DWORD PTR [1808+ecx]                    ;351.127
        cvtdq2ps  xmm2, xmm7                                    ;351.85
        cmpltps   xmm1, xmm0                                    ;350.40
        punpckldq xmm5, xmm6                                    ;351.127
        movd      xmm7, DWORD PTR [2708+ecx]                    ;351.127
        movd      xmm6, DWORD PTR [908+ecx]                     ;351.127
        punpckldq xmm6, xmm7                                    ;351.127
        punpckldq xmm6, xmm5                                    ;351.127
        movaps    xmm5, xmm0                                    ;351.160
        cvtdq2ps  xmm6, xmm6                                    ;351.127
        mulps     xmm5, xmm6                                    ;351.160
        mulps     xmm6, xmm4                                    ;353.160
        rcpps     xmm7, xmm5                                    ;351.125
        rcpps     xmm4, xmm6                                    ;353.125
        mulps     xmm5, xmm7                                    ;351.125
        mulps     xmm6, xmm4                                    ;353.125
        mulps     xmm5, xmm7                                    ;351.125
        addps     xmm7, xmm7                                    ;351.125
        mulps     xmm6, xmm4                                    ;353.125
        addps     xmm4, xmm4                                    ;353.125
        subps     xmm7, xmm5                                    ;351.125
        subps     xmm4, xmm6                                    ;353.125
        mulps     xmm7, xmm2                                    ;351.125
        mulps     xmm2, xmm4                                    ;353.125
        movaps    xmm5, XMMWORD PTR [_2il0floatpacket.12]       ;351.77
        movaps    xmm6, XMMWORD PTR [_2il0floatpacket.12]       ;353.77
        minps     xmm5, xmm7                                    ;351.77
        minps     xmm6, xmm2                                    ;353.77
        andps     xmm5, xmm1                                    ;351.77
        andnps    xmm1, xmm6                                    ;353.77
        orps      xmm5, xmm1                                    ;353.77
        addps     xmm3, xmm5                                    ;353.9
        movss     DWORD PTR [1696+ecx], xmm3                    ;351.9
        shufps    xmm3, xmm3, 229                               ;351.9
        movss     DWORD PTR [2596+ecx], xmm3                    ;351.9
        unpckhps  xmm3, xmm3                                    ;351.9
        movss     DWORD PTR [3496+ecx], xmm3                    ;351.9
        unpckhps  xmm3, xmm3                                    ;351.9
        movss     DWORD PTR [4396+ecx], xmm3                    ;351.9
        add       ecx, 3600                                     ;349.5
        cmp       edi, edx                                      ;349.5
        jb        .B1.61        ; Prob 82%                      ;349.5
                                ; LOE eax edx ecx esi edi xmm0
.B1.63:                         ; Preds .B1.61 .B1.304
        imul      ecx, edx, 900                                 ;
        cmp       edx, esi                                      ;349.5
        jae       .B1.70        ; Prob 3%                       ;349.5
                                ; LOE eax edx ecx esi
.B1.64:                         ; Preds .B1.63
        imul      edi, DWORD PTR [-260+ebp], -900               ;
        movss     xmm0, DWORD PTR [_2il0floatpacket.9]          ;350.40
        movss     xmm4, DWORD PTR [_2il0floatpacket.18]         ;350.40
        add       eax, edi                                      ;
                                ; LOE eax edx ecx esi xmm0 xmm4
.B1.65:                         ; Preds .B1.68 .B1.64
        movss     xmm1, DWORD PTR [1572+ecx+eax]                ;350.10
        cvtsi2ss  xmm3, DWORD PTR [1300+ecx+eax]                ;351.85
        cvtsi2ss  xmm2, DWORD PTR [908+ecx+eax]                 ;351.127
        comiss    xmm0, xmm1                                    ;350.40
        jbe       .B1.67        ; Prob 50%                      ;350.40
                                ; LOE eax edx ecx esi xmm0 xmm1 xmm2 xmm3 xmm4
.B1.66:                         ; Preds .B1.65
        mulss     xmm2, xmm0                                    ;351.160
        movaps    xmm1, xmm4                                    ;351.9
        divss     xmm3, xmm2                                    ;351.125
        minss     xmm1, xmm3                                    ;351.9
        addss     xmm1, DWORD PTR [1696+ecx+eax]                ;351.9
        jmp       .B1.68        ; Prob 100%                     ;351.9
                                ; LOE eax edx ecx esi xmm0 xmm1 xmm4
.B1.67:                         ; Preds .B1.65
        mulss     xmm2, xmm1                                    ;353.160
        movaps    xmm1, xmm4                                    ;353.9
        divss     xmm3, xmm2                                    ;353.125
        minss     xmm1, xmm3                                    ;353.9
        addss     xmm1, DWORD PTR [1696+ecx+eax]                ;353.9
                                ; LOE eax edx ecx esi xmm0 xmm1 xmm4
.B1.68:                         ; Preds .B1.66 .B1.67
        inc       edx                                           ;349.5
        movss     DWORD PTR [1696+ecx+eax], xmm1                ;351.9
        add       ecx, 900                                      ;349.5
        cmp       edx, esi                                      ;349.5
        jb        .B1.65        ; Prob 82%                      ;349.5
                                ; LOE eax edx ecx esi xmm0 xmm4
.B1.70:                         ; Preds .B1.68 .B1.58 .B1.63
        cmp       DWORD PTR [-324+ebp], 0                       ;358.35
        jne       .B1.89        ; Prob 50%                      ;358.35
                                ; LOE esi
.B1.71:                         ; Preds .B1.70
        xor       eax, eax                                      ;
        test      esi, esi                                      ;359.2
        mov       DWORD PTR [-388+ebp], eax                     ;
        jle       .B1.78        ; Prob 2%                       ;359.2
                                ; LOE esi
.B1.72:                         ; Preds .B1.71
        imul      eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32], -152 ;
        mov       edi, 1                                        ;
        imul      ecx, DWORD PTR [-260+ebp], -900               ;
        cvtsi2ss  xmm0, DWORD PTR [-332+ebp]                    ;362.56
        add       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;
        mov       edx, 152                                      ;
        add       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;
        mov       DWORD PTR [-364+ebp], 900                     ;
        mov       DWORD PTR [-316+ebp], eax                     ;
        mov       DWORD PTR [-116+ebp], esi                     ;
        movss     DWORD PTR [-348+ebp], xmm0                    ;
        mov       DWORD PTR [-340+ebp], ecx                     ;
        mov       eax, DWORD PTR [-364+ebp]                     ;
        mov       esi, DWORD PTR [-388+ebp]                     ;
                                ; LOE eax edx esi edi
.B1.73:                         ; Preds .B1.76 .B1.72
        mov       ecx, DWORD PTR [-316+ebp]                     ;360.8
        movsx     ecx, WORD PTR [148+edx+ecx]                   ;360.8
        cmp       ecx, 99                                       ;360.46
        jge       .B1.76        ; Prob 50%                      ;360.46
                                ; LOE eax edx esi edi
.B1.74:                         ; Preds .B1.73
        mov       ecx, DWORD PTR [-340+ebp]                     ;362.22
        inc       esi                                           ;361.4
        movss     xmm0, DWORD PTR [796+eax+ecx]                 ;362.22
        divss     xmm0, DWORD PTR [-348+ebp]                    ;362.4
        mov       ecx, DWORD PTR [-164+ebp]                     ;362.4
        comiss    xmm0, DWORD PTR [_2il0floatpacket.18]         ;363.25
        movss     DWORD PTR [-4+ecx+esi*4], xmm0                ;362.4
        jbe       .B1.76        ; Prob 50%                      ;363.25
                                ; LOE eax edx esi edi
.B1.75:                         ; Preds .B1.74
        push      32                                            ;364.6
        mov       DWORD PTR [-152+ebp], 0                       ;364.6
        lea       ecx, DWORD PTR [-160+ebp]                     ;364.6
        push      ecx                                           ;364.6
        push      OFFSET FLAT: __STRLITPACK_71.0.1              ;364.6
        push      -2088435968                                   ;364.6
        push      -1                                            ;364.6
        mov       DWORD PTR [-160+ebp], 11                      ;364.6
        lea       ecx, DWORD PTR [-152+ebp]                     ;364.6
        push      ecx                                           ;364.6
        mov       DWORD PTR [-156+ebp], OFFSET FLAT: __STRLITPACK_0 ;364.6
        mov       DWORD PTR [-364+ebp], eax                     ;364.6
        mov       DWORD PTR [-356+ebp], edx                     ;364.6
        call      _for_write_seq_lis                            ;364.6
                                ; LOE esi edi
.B1.860:                        ; Preds .B1.75
        mov       edx, DWORD PTR [-356+ebp]                     ;
        add       esp, 24                                       ;364.6
        mov       eax, DWORD PTR [-364+ebp]                     ;
                                ; LOE eax edx esi edi
.B1.76:                         ; Preds .B1.860 .B1.74 .B1.73
        inc       edi                                           ;367.7
        add       edx, 152                                      ;367.7
        add       eax, 900                                      ;367.7
        cmp       edi, DWORD PTR [-116+ebp]                     ;367.7
        jle       .B1.73        ; Prob 82%                      ;367.7
                                ; LOE eax edx esi edi
.B1.77:                         ; Preds .B1.76
        mov       DWORD PTR [-388+ebp], esi                     ;
        mov       esi, DWORD PTR [-116+ebp]                     ;
                                ; LOE esi
.B1.78:                         ; Preds .B1.71 .B1.77
        push      32                                            ;369.7
        xor       eax, eax                                      ;369.7
        lea       edx, DWORD PTR [-152+ebp]                     ;369.7
        push      eax                                           ;369.7
        push      OFFSET FLAT: __STRLITPACK_72.0.1              ;369.7
        push      -2088435968                                   ;369.7
        push      600                                           ;369.7
        push      edx                                           ;369.7
        mov       DWORD PTR [-152+ebp], eax                     ;369.7
        call      _for_write_seq_lis                            ;369.7
                                ; LOE esi
.B1.861:                        ; Preds .B1.78
        add       esp, 24                                       ;369.7
                                ; LOE esi
.B1.79:                         ; Preds .B1.861
        cvtsi2ss  xmm0, DWORD PTR [-308+ebp]                    ;370.23
        push      32                                            ;370.7
        push      OFFSET FLAT: CAL_LINK_MOE$format_pack.0.1+140 ;370.7
        mulss     xmm0, DWORD PTR [-300+ebp]                    ;370.7
        lea       eax, DWORD PTR [-40+ebp]                      ;370.7
        push      eax                                           ;370.7
        push      OFFSET FLAT: __STRLITPACK_73.0.1              ;370.7
        push      -2088435968                                   ;370.7
        push      600                                           ;370.7
        mov       DWORD PTR [-152+ebp], 0                       ;370.7
        lea       edx, DWORD PTR [-152+ebp]                     ;370.7
        push      edx                                           ;370.7
        movss     DWORD PTR [-40+ebp], xmm0                     ;370.7
        call      _for_write_seq_fmt                            ;370.7
                                ; LOE esi
.B1.862:                        ; Preds .B1.79
        add       esp, 28                                       ;370.7
                                ; LOE esi
.B1.80:                         ; Preds .B1.862
        mov       eax, DWORD PTR [-164+ebp]                     ;371.4
        mov       edx, 1                                        ;371.4
        push      32                                            ;371.4
        mov       DWORD PTR [-204+ebp], eax                     ;371.4
        lea       eax, DWORD PTR [-208+ebp]                     ;371.4
        push      OFFSET FLAT: CAL_LINK_MOE$format_pack.0.1+120 ;371.4
        push      eax                                           ;371.4
        push      OFFSET FLAT: __STRLITPACK_74.0.1              ;371.4
        mov       edi, DWORD PTR [-388+ebp]                     ;371.4
        xor       ecx, ecx                                      ;371.4
        test      edi, edi                                      ;371.4
        push      -2088435968                                   ;371.4
        mov       DWORD PTR [-208+ebp], edx                     ;371.4
        mov       DWORD PTR [-200+ebp], edx                     ;371.4
        lea       edx, DWORD PTR [-152+ebp]                     ;371.4
        push      600                                           ;371.4
        push      edx                                           ;371.4
        cmovl     edi, ecx                                      ;371.4
        mov       DWORD PTR [-152+ebp], ecx                     ;371.4
        mov       DWORD PTR [-196+ebp], edi                     ;371.4
        mov       DWORD PTR [-192+ebp], 4                       ;371.4
        call      _for_write_seq_fmt                            ;371.4
                                ; LOE esi
.B1.863:                        ; Preds .B1.80
        add       esp, 28                                       ;371.4
                                ; LOE esi
.B1.81:                         ; Preds .B1.863
        push      32                                            ;372.7
        xor       eax, eax                                      ;372.7
        lea       edx, DWORD PTR [-152+ebp]                     ;372.7
        push      eax                                           ;372.7
        push      OFFSET FLAT: __STRLITPACK_75.0.1              ;372.7
        push      -2088435968                                   ;372.7
        push      600                                           ;372.7
        push      edx                                           ;372.7
        mov       DWORD PTR [-152+ebp], eax                     ;372.7
        call      _for_write_seq_lis                            ;372.7
                                ; LOE esi
.B1.864:                        ; Preds .B1.81
        add       esp, 24                                       ;372.7
                                ; LOE esi
.B1.82:                         ; Preds .B1.864
        cmp       DWORD PTR [-188+ebp], 0                       ;373.7
        jle       .B1.89        ; Prob 50%                      ;373.7
                                ; LOE esi
.B1.83:                         ; Preds .B1.82
        mov       edx, DWORD PTR [-188+ebp]                     ;373.7
        mov       edi, edx                                      ;373.7
        shr       edi, 31                                       ;373.7
        add       edi, edx                                      ;373.7
        sar       edi, 1                                        ;373.7
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;373.7
        test      edi, edi                                      ;373.7
        jbe       .B1.292       ; Prob 10%                      ;373.7
                                ; LOE eax esi edi
.B1.84:                         ; Preds .B1.83
        xor       ecx, ecx                                      ;
        mov       DWORD PTR [-116+ebp], esi                     ;
        xor       edx, edx                                      ;
        xor       esi, esi                                      ;
                                ; LOE eax edx ecx esi edi
.B1.85:                         ; Preds .B1.85 .B1.84
        inc       ecx                                           ;373.7
        mov       DWORD PTR [796+edx+eax], esi                  ;373.7
        mov       DWORD PTR [1696+edx+eax], esi                 ;373.7
        add       edx, 1800                                     ;373.7
        cmp       ecx, edi                                      ;373.7
        jb        .B1.85        ; Prob 63%                      ;373.7
                                ; LOE eax edx ecx esi edi
.B1.86:                         ; Preds .B1.85
        mov       esi, DWORD PTR [-116+ebp]                     ;
        lea       edx, DWORD PTR [1+ecx+ecx]                    ;373.7
                                ; LOE eax edx esi
.B1.87:                         ; Preds .B1.86 .B1.292
        lea       ecx, DWORD PTR [-1+edx]                       ;373.7
        cmp       ecx, DWORD PTR [-188+ebp]                     ;373.7
        jae       .B1.89        ; Prob 10%                      ;373.7
                                ; LOE eax edx esi
.B1.88:                         ; Preds .B1.87
        mov       ecx, DWORD PTR [-260+ebp]                     ;373.7
        mov       edi, ecx                                      ;373.7
        neg       edi                                           ;373.7
        add       edx, ecx                                      ;373.7
        add       edi, edx                                      ;373.7
        imul      edx, edi, 900                                 ;373.7
        mov       DWORD PTR [-104+eax+edx], 0                   ;373.7
                                ; LOE esi
.B1.89:                         ; Preds .B1.70 .B1.82 .B1.87 .B1.88
        test      esi, esi                                      ;377.5
        jle       .B1.96        ; Prob 50%                      ;377.5
                                ; LOE esi
.B1.90:                         ; Preds .B1.89
        mov       edi, esi                                      ;377.5
        shr       edi, 31                                       ;377.5
        add       edi, esi                                      ;377.5
        sar       edi, 1                                        ;377.5
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;378.7
        test      edi, edi                                      ;377.5
        jbe       .B1.293       ; Prob 3%                       ;377.5
                                ; LOE ecx esi edi
.B1.91:                         ; Preds .B1.90
        imul      eax, DWORD PTR [-260+ebp], -900               ;
        xor       edx, edx                                      ;
        add       eax, ecx                                      ;
        mov       DWORD PTR [-116+ebp], esi                     ;
        mov       esi, edx                                      ;
        ALIGN     16
                                ; LOE eax edx ecx esi edi
.B1.92:                         ; Preds .B1.92 .B1.91
        pxor      xmm0, xmm0                                    ;378.124
        pxor      xmm2, xmm2                                    ;378.124
        cvtsi2ss  xmm0, DWORD PTR [1540+esi+eax]                ;378.124
        cvtsi2ss  xmm2, DWORD PTR [2440+esi+eax]                ;378.124
        divss     xmm0, DWORD PTR [1572+esi+eax]                ;378.155
        divss     xmm2, DWORD PTR [2472+esi+eax]                ;378.155
        pxor      xmm1, xmm1                                    ;378.83
        pxor      xmm3, xmm3                                    ;378.83
        cvtsi2ss  xmm1, DWORD PTR [908+esi+eax]                 ;378.83
        cvtsi2ss  xmm3, DWORD PTR [1808+esi+eax]                ;378.83
        minss     xmm1, xmm0                                    ;378.7
        minss     xmm3, xmm2                                    ;378.7
        addss     xmm1, DWORD PTR [1700+esi+eax]                ;378.7
        addss     xmm3, DWORD PTR [2600+esi+eax]                ;378.7
        inc       edx                                           ;377.5
        movss     DWORD PTR [1700+esi+eax], xmm1                ;378.7
        movss     DWORD PTR [2600+esi+eax], xmm3                ;378.7
        add       esi, 1800                                     ;377.5
        cmp       edx, edi                                      ;377.5
        jb        .B1.92        ; Prob 63%                      ;377.5
                                ; LOE eax edx ecx esi edi
.B1.93:                         ; Preds .B1.92
        mov       esi, DWORD PTR [-116+ebp]                     ;
        lea       edx, DWORD PTR [1+edx+edx]                    ;377.5
                                ; LOE edx ecx esi
.B1.94:                         ; Preds .B1.93 .B1.293
        lea       eax, DWORD PTR [-1+edx]                       ;377.5
        cmp       esi, eax                                      ;377.5
        jbe       .B1.96        ; Prob 3%                       ;377.5
                                ; LOE edx ecx esi
.B1.95:                         ; Preds .B1.94
        mov       eax, DWORD PTR [-260+ebp]                     ;378.7
        neg       eax                                           ;378.7
        add       eax, edx                                      ;378.7
        imul      edx, eax, 900                                 ;378.7
        cvtsi2ss  xmm0, DWORD PTR [640+ecx+edx]                 ;378.124
        cvtsi2ss  xmm1, DWORD PTR [8+ecx+edx]                   ;378.83
        divss     xmm0, DWORD PTR [672+ecx+edx]                 ;378.155
        minss     xmm1, xmm0                                    ;378.7
        addss     xmm1, DWORD PTR [800+ecx+edx]                 ;378.7
        movss     DWORD PTR [800+ecx+edx], xmm1                 ;378.7
                                ; LOE esi
.B1.96:                         ; Preds .B1.89 .B1.94 .B1.95
        cmp       DWORD PTR [-324+ebp], 0                       ;381.37
        jne       .B1.127       ; Prob 50%                      ;381.37
                                ; LOE esi
.B1.97:                         ; Preds .B1.96
        test      esi, esi                                      ;383.2
        jle       .B1.115       ; Prob 50%                      ;383.2
                                ; LOE esi
.B1.98:                         ; Preds .B1.97
        mov       eax, esi                                      ;383.2
        shr       eax, 31                                       ;383.2
        add       eax, esi                                      ;383.2
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;398.7
        sar       eax, 1                                        ;383.2
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;384.4
        test      eax, eax                                      ;383.2
        mov       DWORD PTR [-348+ebp], edx                     ;398.7
        mov       DWORD PTR [-356+ebp], edi                     ;384.4
        mov       edi, 0                                        ;
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;384.4
        mov       DWORD PTR [-184+ebp], eax                     ;383.2
        jbe       .B1.294       ; Prob 3%                       ;383.2
                                ; LOE edx esi edi
.B1.99:                         ; Preds .B1.98
        imul      ecx, DWORD PTR [-260+ebp], -900               ;
        xor       eax, eax                                      ;
        movss     xmm0, DWORD PTR [_2il0floatpacket.13]         ;386.39
        cvtsi2ss  xmm1, DWORD PTR [-332+ebp]                    ;389.62
        add       ecx, DWORD PTR [-348+ebp]                     ;
        mov       DWORD PTR [-340+ebp], ecx                     ;
        imul      ecx, edx, -152                                ;
        add       ecx, DWORD PTR [-356+ebp]                     ;
        mov       DWORD PTR [-364+ebp], eax                     ;
        mov       DWORD PTR [-316+ebp], ecx                     ;
        mov       DWORD PTR [-388+ebp], edx                     ;
        mov       DWORD PTR [-116+ebp], esi                     ;
        mov       ecx, eax                                      ;
        mov       edx, DWORD PTR [-364+ebp]                     ;
                                ; LOE eax edx ecx edi xmm0 xmm1
.B1.100:                        ; Preds .B1.108 .B1.99
        mov       esi, DWORD PTR [-316+ebp]                     ;384.8
        movsx     esi, WORD PTR [300+edx+esi]                   ;384.8
        cmp       esi, 99                                       ;384.46
        jge       .B1.104       ; Prob 50%                      ;384.46
                                ; LOE eax edx ecx edi xmm0 xmm1
.B1.101:                        ; Preds .B1.100
        mov       esi, DWORD PTR [-340+ebp]                     ;386.39
        inc       edi                                           ;385.6
        comiss    xmm0, DWORD PTR [1572+ecx+esi]                ;386.39
        jbe       .B1.103       ; Prob 50%                      ;386.39
                                ; LOE eax edx ecx esi edi xmm0 xmm1
.B1.102:                        ; Preds .B1.101
        cvtsi2ss  xmm2, DWORD PTR [908+ecx+esi]                 ;387.8
        mov       esi, DWORD PTR [-164+ebp]                     ;387.8
        movss     DWORD PTR [-4+esi+edi*4], xmm2                ;387.8
        jmp       .B1.104       ; Prob 100%                     ;387.8
                                ; LOE eax edx ecx edi xmm0 xmm1
.B1.103:                        ; Preds .B1.101
        movss     xmm2, DWORD PTR [1700+ecx+esi]                ;389.28
        divss     xmm2, xmm1                                    ;389.8
        mov       esi, DWORD PTR [-164+ebp]                     ;389.8
        movss     DWORD PTR [-4+esi+edi*4], xmm2                ;389.8
                                ; LOE eax edx ecx edi xmm0 xmm1
.B1.104:                        ; Preds .B1.102 .B1.103 .B1.100
        mov       esi, DWORD PTR [-316+ebp]                     ;384.8
        movsx     esi, WORD PTR [452+edx+esi]                   ;384.8
        cmp       esi, 99                                       ;384.46
        jge       .B1.108       ; Prob 50%                      ;384.46
                                ; LOE eax edx ecx edi xmm0 xmm1
.B1.105:                        ; Preds .B1.104
        mov       esi, DWORD PTR [-340+ebp]                     ;386.39
        inc       edi                                           ;385.6
        comiss    xmm0, DWORD PTR [2472+ecx+esi]                ;386.39
        jbe       .B1.107       ; Prob 50%                      ;386.39
                                ; LOE eax edx ecx esi edi xmm0 xmm1
.B1.106:                        ; Preds .B1.105
        cvtsi2ss  xmm2, DWORD PTR [1808+ecx+esi]                ;387.8
        mov       esi, DWORD PTR [-164+ebp]                     ;387.8
        movss     DWORD PTR [-4+esi+edi*4], xmm2                ;387.8
        jmp       .B1.108       ; Prob 100%                     ;387.8
                                ; LOE eax edx ecx edi xmm0 xmm1
.B1.107:                        ; Preds .B1.105
        movss     xmm2, DWORD PTR [2600+ecx+esi]                ;389.28
        divss     xmm2, xmm1                                    ;389.8
        mov       esi, DWORD PTR [-164+ebp]                     ;389.8
        movss     DWORD PTR [-4+esi+edi*4], xmm2                ;389.8
                                ; LOE eax edx ecx edi xmm0 xmm1
.B1.108:                        ; Preds .B1.106 .B1.107 .B1.104
        inc       eax                                           ;383.2
        add       edx, 304                                      ;383.2
        add       ecx, 1800                                     ;383.2
        cmp       eax, DWORD PTR [-184+ebp]                     ;383.2
        jb        .B1.100       ; Prob 63%                      ;383.2
                                ; LOE eax edx ecx edi xmm0 xmm1
.B1.109:                        ; Preds .B1.108
        mov       edx, DWORD PTR [-388+ebp]                     ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;383.2
        mov       esi, DWORD PTR [-116+ebp]                     ;
                                ; LOE eax edx esi edi
.B1.110:                        ; Preds .B1.109 .B1.294
        lea       ecx, DWORD PTR [-1+eax]                       ;383.2
        cmp       esi, ecx                                      ;383.2
        jbe       .B1.116       ; Prob 3%                       ;383.2
                                ; LOE eax edx esi edi
.B1.111:                        ; Preds .B1.110
        neg       edx                                           ;384.46
        add       edx, eax                                      ;384.46
        imul      ecx, edx, 152                                 ;384.46
        mov       edx, DWORD PTR [-356+ebp]                     ;384.8
        movsx     edx, WORD PTR [148+edx+ecx]                   ;384.8
        cmp       edx, 99                                       ;384.46
        jge       .B1.116       ; Prob 50%                      ;384.46
                                ; LOE eax esi edi
.B1.112:                        ; Preds .B1.111
        mov       edx, DWORD PTR [-260+ebp]                     ;386.39
        inc       edi                                           ;385.6
        neg       edx                                           ;386.39
        add       edx, eax                                      ;386.39
        imul      eax, edx, 900                                 ;386.39
        movss     xmm0, DWORD PTR [_2il0floatpacket.13]         ;386.39
        mov       ecx, DWORD PTR [-348+ebp]                     ;386.39
        comiss    xmm0, DWORD PTR [672+ecx+eax]                 ;386.39
        jbe       .B1.114       ; Prob 50%                      ;386.39
                                ; LOE eax ecx esi edi cl ch
.B1.113:                        ; Preds .B1.112
        mov       edx, ecx                                      ;387.8
        cvtsi2ss  xmm0, DWORD PTR [8+edx+eax]                   ;387.8
        mov       eax, DWORD PTR [-164+ebp]                     ;387.8
        movss     DWORD PTR [-4+eax+edi*4], xmm0                ;387.8
        jmp       .B1.116       ; Prob 100%                     ;387.8
                                ; LOE esi edi
.B1.114:                        ; Preds .B1.112
        cvtsi2ss  xmm0, DWORD PTR [-332+ebp]                    ;389.62
        mov       edx, ecx                                      ;389.28
        movss     xmm1, DWORD PTR [800+edx+eax]                 ;389.28
        divss     xmm1, xmm0                                    ;389.8
        mov       eax, DWORD PTR [-164+ebp]                     ;389.8
        movss     DWORD PTR [-4+eax+edi*4], xmm1                ;389.8
        jmp       .B1.116       ; Prob 100%                     ;389.8
                                ; LOE esi edi
.B1.115:                        ; Preds .B1.97
        xor       edi, edi                                      ;
                                ; LOE esi edi
.B1.116:                        ; Preds .B1.113 .B1.114 .B1.111 .B1.110 .B1.115
                                ;      
        push      32                                            ;394.7
        xor       eax, eax                                      ;394.7
        lea       edx, DWORD PTR [-152+ebp]                     ;394.7
        push      eax                                           ;394.7
        push      OFFSET FLAT: __STRLITPACK_76.0.1              ;394.7
        push      -2088435968                                   ;394.7
        push      700                                           ;394.7
        push      edx                                           ;394.7
        mov       DWORD PTR [-152+ebp], eax                     ;394.7
        call      _for_write_seq_lis                            ;394.7
                                ; LOE esi edi
.B1.865:                        ; Preds .B1.116
        add       esp, 24                                       ;394.7
                                ; LOE esi edi
.B1.117:                        ; Preds .B1.865
        cvtsi2ss  xmm0, DWORD PTR [-308+ebp]                    ;395.23
        push      32                                            ;395.7
        push      OFFSET FLAT: CAL_LINK_MOE$format_pack.0.1+140 ;395.7
        mulss     xmm0, DWORD PTR [-300+ebp]                    ;395.7
        lea       eax, DWORD PTR [-32+ebp]                      ;395.7
        push      eax                                           ;395.7
        push      OFFSET FLAT: __STRLITPACK_77.0.1              ;395.7
        push      -2088435968                                   ;395.7
        push      700                                           ;395.7
        mov       DWORD PTR [-152+ebp], 0                       ;395.7
        lea       edx, DWORD PTR [-152+ebp]                     ;395.7
        push      edx                                           ;395.7
        movss     DWORD PTR [-32+ebp], xmm0                     ;395.7
        call      _for_write_seq_fmt                            ;395.7
                                ; LOE esi edi
.B1.866:                        ; Preds .B1.117
        add       esp, 28                                       ;395.7
                                ; LOE esi edi
.B1.118:                        ; Preds .B1.866
        xor       ecx, ecx                                      ;396.4
        test      edi, edi                                      ;396.4
        push      32                                            ;396.4
        cmovl     edi, ecx                                      ;396.4
        mov       edx, 1                                        ;396.4
        mov       DWORD PTR [-172+ebp], edi                     ;396.4
        lea       edi, DWORD PTR [-184+ebp]                     ;396.4
        push      OFFSET FLAT: CAL_LINK_MOE$format_pack.0.1+120 ;396.4
        push      edi                                           ;396.4
        push      OFFSET FLAT: __STRLITPACK_78.0.1              ;396.4
        mov       eax, DWORD PTR [-164+ebp]                     ;396.4
        push      -2088435968                                   ;396.4
        mov       DWORD PTR [-180+ebp], eax                     ;396.4
        lea       eax, DWORD PTR [-152+ebp]                     ;396.4
        push      700                                           ;396.4
        push      eax                                           ;396.4
        mov       DWORD PTR [-152+ebp], ecx                     ;396.4
        mov       DWORD PTR [-184+ebp], edx                     ;396.4
        mov       DWORD PTR [-176+ebp], edx                     ;396.4
        mov       DWORD PTR [-168+ebp], 4                       ;396.4
        call      _for_write_seq_fmt                            ;396.4
                                ; LOE esi
.B1.867:                        ; Preds .B1.118
        add       esp, 28                                       ;396.4
                                ; LOE esi
.B1.119:                        ; Preds .B1.867
        push      32                                            ;397.7
        xor       eax, eax                                      ;397.7
        lea       edx, DWORD PTR [-152+ebp]                     ;397.7
        push      eax                                           ;397.7
        push      OFFSET FLAT: __STRLITPACK_79.0.1              ;397.7
        push      -2088435968                                   ;397.7
        push      700                                           ;397.7
        push      edx                                           ;397.7
        mov       DWORD PTR [-152+ebp], eax                     ;397.7
        call      _for_write_seq_lis                            ;397.7
                                ; LOE esi
.B1.868:                        ; Preds .B1.119
        add       esp, 24                                       ;397.7
                                ; LOE esi
.B1.120:                        ; Preds .B1.868
        cmp       DWORD PTR [-188+ebp], 0                       ;398.7
        jle       .B1.127       ; Prob 50%                      ;398.7
                                ; LOE esi
.B1.121:                        ; Preds .B1.120
        mov       edx, DWORD PTR [-188+ebp]                     ;398.7
        mov       edi, edx                                      ;398.7
        shr       edi, 31                                       ;398.7
        add       edi, edx                                      ;398.7
        sar       edi, 1                                        ;398.7
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;398.7
        test      edi, edi                                      ;398.7
        jbe       .B1.295       ; Prob 10%                      ;398.7
                                ; LOE eax esi edi
.B1.122:                        ; Preds .B1.121
        xor       ecx, ecx                                      ;
        mov       DWORD PTR [-116+ebp], esi                     ;
        xor       edx, edx                                      ;
        xor       esi, esi                                      ;
                                ; LOE eax edx ecx esi edi
.B1.123:                        ; Preds .B1.123 .B1.122
        inc       ecx                                           ;398.7
        mov       DWORD PTR [800+edx+eax], esi                  ;398.7
        mov       DWORD PTR [1700+edx+eax], esi                 ;398.7
        add       edx, 1800                                     ;398.7
        cmp       ecx, edi                                      ;398.7
        jb        .B1.123       ; Prob 63%                      ;398.7
                                ; LOE eax edx ecx esi edi
.B1.124:                        ; Preds .B1.123
        mov       esi, DWORD PTR [-116+ebp]                     ;
        lea       edx, DWORD PTR [1+ecx+ecx]                    ;398.7
                                ; LOE eax edx esi
.B1.125:                        ; Preds .B1.124 .B1.295
        lea       ecx, DWORD PTR [-1+edx]                       ;398.7
        cmp       ecx, DWORD PTR [-188+ebp]                     ;398.7
        jae       .B1.127       ; Prob 10%                      ;398.7
                                ; LOE eax edx esi
.B1.126:                        ; Preds .B1.125
        mov       ecx, DWORD PTR [-260+ebp]                     ;398.7
        mov       edi, ecx                                      ;398.7
        neg       edi                                           ;398.7
        add       edx, ecx                                      ;398.7
        add       edi, edx                                      ;398.7
        imul      edx, edi, 900                                 ;398.7
        mov       DWORD PTR [-100+eax+edx], 0                   ;398.7
                                ; LOE esi
.B1.127:                        ; Preds .B1.96 .B1.120 .B1.125 .B1.126
        mov       edx, DWORD PTR [-188+ebp]                     ;400.5
        mov       DWORD PTR [-388+ebp], esp                     ;400.5
        lea       eax, DWORD PTR [edx*4]                        ;400.5
        call      __alloca_probe                                ;400.5
        and       esp, -16                                      ;400.5
        mov       eax, esp                                      ;400.5
                                ; LOE eax edx esi dl dh
.B1.869:                        ; Preds .B1.127
        mov       DWORD PTR [-364+ebp], eax                     ;400.5
        cmp       DWORD PTR [-188+ebp], 0                       ;400.5
        jle       .B1.140       ; Prob 50%                      ;400.5
                                ; LOE edx esi dl dh
.B1.128:                        ; Preds .B1.869
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;400.5
        mov       eax, edx                                      ;400.5
        mov       DWORD PTR [-356+ebp], ecx                     ;400.5
        mov       ecx, eax                                      ;400.5
        shr       ecx, 31                                       ;400.5
        add       ecx, eax                                      ;400.5
        sar       ecx, 1                                        ;400.5
        test      ecx, ecx                                      ;400.5
        jbe       .B1.297       ; Prob 10%                      ;400.5
                                ; LOE ecx esi
.B1.129:                        ; Preds .B1.128
        xor       edx, edx                                      ;
        mov       DWORD PTR [-116+ebp], esi                     ;
        xor       eax, eax                                      ;
        mov       esi, DWORD PTR [-356+ebp]                     ;
        mov       edi, DWORD PTR [-364+ebp]                     ;
                                ; LOE eax edx ecx esi edi
.B1.130:                        ; Preds .B1.130 .B1.129
        movss     xmm0, DWORD PTR [652+eax+esi]                 ;400.5
        movss     xmm1, DWORD PTR [1552+eax+esi]                ;400.5
        addss     xmm0, DWORD PTR [804+eax+esi]                 ;400.5
        addss     xmm1, DWORD PTR [1704+eax+esi]                ;400.5
        movss     DWORD PTR [edi+edx*8], xmm0                   ;400.5
        add       eax, 1800                                     ;400.5
        movss     DWORD PTR [4+edi+edx*8], xmm1                 ;400.5
        inc       edx                                           ;400.5
        cmp       edx, ecx                                      ;400.5
        jb        .B1.130       ; Prob 63%                      ;400.5
                                ; LOE eax edx ecx esi edi
.B1.131:                        ; Preds .B1.130
        mov       esi, DWORD PTR [-116+ebp]                     ;
        lea       edx, DWORD PTR [1+edx+edx]                    ;400.5
                                ; LOE edx ecx esi
.B1.132:                        ; Preds .B1.131 .B1.297
        lea       eax, DWORD PTR [-1+edx]                       ;400.5
        cmp       eax, DWORD PTR [-188+ebp]                     ;400.5
        jae       .B1.134       ; Prob 10%                      ;400.5
                                ; LOE edx ecx esi
.B1.133:                        ; Preds .B1.132
        mov       edi, DWORD PTR [-260+ebp]                     ;400.5
        mov       eax, edi                                      ;400.5
        neg       eax                                           ;400.5
        add       edi, edx                                      ;400.5
        add       eax, edi                                      ;400.5
        imul      edi, eax, 900                                 ;400.5
        mov       eax, DWORD PTR [-356+ebp]                     ;400.5
        movss     xmm0, DWORD PTR [-248+eax+edi]                ;400.5
        addss     xmm0, DWORD PTR [-96+eax+edi]                 ;400.5
        mov       eax, DWORD PTR [-364+ebp]                     ;400.5
        movss     DWORD PTR [-4+eax+edx*4], xmm0                ;400.5
                                ; LOE ecx esi
.B1.134:                        ; Preds .B1.132 .B1.133
        test      ecx, ecx                                      ;400.5
        jbe       .B1.296       ; Prob 10%                      ;400.5
                                ; LOE ecx esi
.B1.135:                        ; Preds .B1.134
        xor       edx, edx                                      ;
        mov       DWORD PTR [-116+ebp], esi                     ;
        xor       eax, eax                                      ;
        mov       DWORD PTR [-348+ebp], ecx                     ;
        mov       esi, DWORD PTR [-364+ebp]                     ;
                                ; LOE eax edx esi
.B1.136:                        ; Preds .B1.136 .B1.135
        mov       ecx, DWORD PTR [-356+ebp]                     ;400.5
        mov       edi, DWORD PTR [esi+edx*8]                    ;400.5
        mov       DWORD PTR [804+eax+ecx], edi                  ;400.5
        mov       edi, DWORD PTR [4+esi+edx*8]                  ;400.5
        inc       edx                                           ;400.5
        mov       DWORD PTR [1704+eax+ecx], edi                 ;400.5
        add       eax, 1800                                     ;400.5
        cmp       edx, DWORD PTR [-348+ebp]                     ;400.5
        jb        .B1.136       ; Prob 63%                      ;400.5
                                ; LOE eax edx esi
.B1.137:                        ; Preds .B1.136
        mov       esi, DWORD PTR [-116+ebp]                     ;
        lea       edx, DWORD PTR [1+edx+edx]                    ;400.5
                                ; LOE edx esi
.B1.138:                        ; Preds .B1.137 .B1.296
        lea       eax, DWORD PTR [-1+edx]                       ;400.5
        cmp       eax, DWORD PTR [-188+ebp]                     ;400.5
        jae       .B1.140       ; Prob 10%                      ;400.5
                                ; LOE edx esi
.B1.139:                        ; Preds .B1.138
        mov       ecx, DWORD PTR [-260+ebp]                     ;400.5
        mov       eax, ecx                                      ;400.5
        neg       eax                                           ;400.5
        lea       edi, DWORD PTR [ecx+edx]                      ;400.5
        mov       ecx, DWORD PTR [-364+ebp]                     ;400.5
        add       eax, edi                                      ;400.5
        imul      eax, eax, 900                                 ;400.5
        mov       edx, DWORD PTR [-4+ecx+edx*4]                 ;400.5
        mov       ecx, DWORD PTR [-356+ebp]                     ;400.5
        mov       DWORD PTR [-96+ecx+eax], edx                  ;400.5
                                ; LOE esi
.B1.140:                        ; Preds .B1.869 .B1.138 .B1.139
        mov       eax, DWORD PTR [-388+ebp]                     ;400.5
        mov       esp, eax                                      ;400.5
                                ; LOE esi
.B1.870:                        ; Preds .B1.140
        cmp       DWORD PTR [-324+ebp], 0                       ;402.37
        jne       .B1.171       ; Prob 50%                      ;402.37
                                ; LOE esi
.B1.141:                        ; Preds .B1.870
        test      esi, esi                                      ;404.2
        jle       .B1.153       ; Prob 50%                      ;404.2
                                ; LOE esi
.B1.142:                        ; Preds .B1.141
        mov       eax, esi                                      ;404.2
        shr       eax, 31                                       ;404.2
        add       eax, esi                                      ;404.2
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;415.7
        sar       eax, 1                                        ;404.2
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;405.4
        test      eax, eax                                      ;404.2
        mov       DWORD PTR [-348+ebp], edx                     ;415.7
        mov       DWORD PTR [-356+ebp], edi                     ;405.4
        mov       edi, 0                                        ;
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;405.4
        mov       DWORD PTR [-316+ebp], eax                     ;404.2
        jbe       .B1.298       ; Prob 3%                       ;404.2
                                ; LOE edx esi edi
.B1.143:                        ; Preds .B1.142
        imul      ecx, DWORD PTR [-260+ebp], -900               ;
        xor       eax, eax                                      ;
        cvtsi2ss  xmm0, DWORD PTR [-332+ebp]                    ;407.60
        add       ecx, DWORD PTR [-348+ebp]                     ;
        mov       DWORD PTR [-340+ebp], ecx                     ;
        imul      ecx, edx, -152                                ;
        add       ecx, DWORD PTR [-356+ebp]                     ;
        mov       DWORD PTR [-364+ebp], eax                     ;
        mov       DWORD PTR [-324+ebp], ecx                     ;
        mov       DWORD PTR [-388+ebp], edx                     ;
        mov       DWORD PTR [-116+ebp], esi                     ;
        mov       ecx, eax                                      ;
        mov       edx, DWORD PTR [-364+ebp]                     ;
                                ; LOE eax edx ecx edi xmm0
.B1.144:                        ; Preds .B1.148 .B1.143
        mov       esi, DWORD PTR [-324+ebp]                     ;405.8
        movsx     esi, WORD PTR [300+ecx+esi]                   ;405.8
        cmp       esi, 99                                       ;405.46
        jge       .B1.146       ; Prob 50%                      ;405.46
                                ; LOE eax edx ecx edi xmm0
.B1.145:                        ; Preds .B1.144
        mov       esi, DWORD PTR [-340+ebp]                     ;407.26
        inc       edi                                           ;406.6
        movss     xmm1, DWORD PTR [1704+edx+esi]                ;407.26
        divss     xmm1, xmm0                                    ;407.6
        mov       esi, DWORD PTR [-164+ebp]                     ;407.6
        movss     DWORD PTR [-4+esi+edi*4], xmm1                ;407.6
                                ; LOE eax edx ecx edi xmm0
.B1.146:                        ; Preds .B1.145 .B1.144
        mov       esi, DWORD PTR [-324+ebp]                     ;405.8
        movsx     esi, WORD PTR [452+ecx+esi]                   ;405.8
        cmp       esi, 99                                       ;405.46
        jge       .B1.148       ; Prob 50%                      ;405.46
                                ; LOE eax edx ecx edi xmm0
.B1.147:                        ; Preds .B1.146
        mov       esi, DWORD PTR [-340+ebp]                     ;407.26
        inc       edi                                           ;406.6
        movss     xmm1, DWORD PTR [2604+edx+esi]                ;407.26
        divss     xmm1, xmm0                                    ;407.6
        mov       esi, DWORD PTR [-164+ebp]                     ;407.6
        movss     DWORD PTR [-4+esi+edi*4], xmm1                ;407.6
                                ; LOE eax edx ecx edi xmm0
.B1.148:                        ; Preds .B1.147 .B1.146
        inc       eax                                           ;404.2
        add       edx, 1800                                     ;404.2
        add       ecx, 304                                      ;404.2
        cmp       eax, DWORD PTR [-316+ebp]                     ;404.2
        jb        .B1.144       ; Prob 63%                      ;404.2
                                ; LOE eax edx ecx edi xmm0
.B1.149:                        ; Preds .B1.148
        mov       edx, DWORD PTR [-388+ebp]                     ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;404.2
        mov       esi, DWORD PTR [-116+ebp]                     ;
                                ; LOE eax edx esi edi
.B1.150:                        ; Preds .B1.149 .B1.298
        lea       ecx, DWORD PTR [-1+eax]                       ;404.2
        cmp       esi, ecx                                      ;404.2
        jbe       .B1.154       ; Prob 3%                       ;404.2
                                ; LOE eax edx edi
.B1.151:                        ; Preds .B1.150
        neg       edx                                           ;405.46
        add       edx, eax                                      ;405.46
        imul      ecx, edx, 152                                 ;405.46
        mov       edx, DWORD PTR [-356+ebp]                     ;405.8
        movsx     esi, WORD PTR [148+edx+ecx]                   ;405.8
        cmp       esi, 99                                       ;405.46
        jge       .B1.154       ; Prob 50%                      ;405.46
                                ; LOE eax edi
.B1.152:                        ; Preds .B1.151
        mov       edx, DWORD PTR [-260+ebp]                     ;407.6
        inc       edi                                           ;406.6
        neg       edx                                           ;407.6
        add       edx, eax                                      ;407.6
        imul      ecx, edx, 900                                 ;407.6
        cvtsi2ss  xmm0, DWORD PTR [-332+ebp]                    ;407.60
        mov       eax, DWORD PTR [-348+ebp]                     ;407.26
        mov       esi, DWORD PTR [-164+ebp]                     ;407.6
        movss     xmm1, DWORD PTR [804+eax+ecx]                 ;407.26
        divss     xmm1, xmm0                                    ;407.6
        movss     DWORD PTR [-4+esi+edi*4], xmm1                ;407.6
        jmp       .B1.154       ; Prob 100%                     ;407.6
                                ; LOE edi
.B1.153:                        ; Preds .B1.141
        xor       edi, edi                                      ;
                                ; LOE edi
.B1.154:                        ; Preds .B1.152 .B1.151 .B1.150 .B1.153
        push      32                                            ;411.7
        xor       edx, edx                                      ;411.7
        lea       esi, DWORD PTR [-152+ebp]                     ;411.7
        push      edx                                           ;411.7
        push      OFFSET FLAT: __STRLITPACK_80.0.1              ;411.7
        push      -2088435968                                   ;411.7
        push      900                                           ;411.7
        push      esi                                           ;411.7
        mov       DWORD PTR [-152+ebp], edx                     ;411.7
        call      _for_write_seq_lis                            ;411.7
                                ; LOE esi edi
.B1.871:                        ; Preds .B1.154
        add       esp, 24                                       ;411.7
                                ; LOE esi edi
.B1.155:                        ; Preds .B1.871
        cvtsi2ss  xmm0, DWORD PTR [-308+ebp]                    ;412.23
        push      32                                            ;412.7
        push      OFFSET FLAT: CAL_LINK_MOE$format_pack.0.1+140 ;412.7
        movss     xmm1, DWORD PTR [-300+ebp]                    ;412.7
        lea       edx, DWORD PTR [-24+ebp]                      ;412.7
        push      edx                                           ;412.7
        push      OFFSET FLAT: __STRLITPACK_81.0.1              ;412.7
        mulss     xmm1, xmm0                                    ;412.7
        push      -2088435968                                   ;412.7
        push      900                                           ;412.7
        push      esi                                           ;412.7
        mov       DWORD PTR [-152+ebp], 0                       ;412.7
        movss     DWORD PTR [-24+ebp], xmm1                     ;412.7
        call      _for_write_seq_fmt                            ;412.7
                                ; LOE esi edi
.B1.872:                        ; Preds .B1.155
        add       esp, 28                                       ;412.7
                                ; LOE esi edi
.B1.156:                        ; Preds .B1.872
        push      32                                            ;413.4
        push      OFFSET FLAT: CAL_LINK_MOE$format_pack.0.1+160 ;413.4
        xor       edx, edx                                      ;413.4
        push      edx                                           ;413.4
        push      OFFSET FLAT: __STRLITPACK_82.0.1              ;413.4
        push      -2088435968                                   ;413.4
        push      900                                           ;413.4
        push      esi                                           ;413.4
        mov       DWORD PTR [-152+ebp], edx                     ;413.4
        call      _for_write_seq_fmt                            ;413.4
                                ; LOE esi edi
.B1.873:                        ; Preds .B1.156
        add       esp, 28                                       ;413.4
                                ; LOE esi edi
.B1.157:                        ; Preds .B1.873
        test      edi, edi                                      ;413.4
        jle       .B1.162       ; Prob 2%                       ;413.4
                                ; LOE esi edi
.B1.158:                        ; Preds .B1.157
        mov       eax, 1                                        ;
        mov       edx, eax                                      ;413.31
        mov       eax, esi                                      ;413.31
        mov       DWORD PTR [-388+ebp], edi                     ;413.31
        mov       esi, edx                                      ;413.31
        mov       edi, DWORD PTR [-164+ebp]                     ;413.31
                                ; LOE eax esi edi
.B1.159:                        ; Preds .B1.160 .B1.158
        movss     xmm0, DWORD PTR [-4+edi+esi*4]                ;413.21
        lea       edx, DWORD PTR [-16+ebp]                      ;413.4
        mulss     xmm0, DWORD PTR [_2il0floatpacket.10]         ;413.4
        push      edx                                           ;413.4
        push      OFFSET FLAT: __STRLITPACK_83.0.1              ;413.4
        push      eax                                           ;413.4
        movss     DWORD PTR [-16+ebp], xmm0                     ;413.4
        call      _for_write_seq_fmt_xmit                       ;413.4
                                ; LOE esi edi
.B1.874:                        ; Preds .B1.159
        add       esp, 12                                       ;413.4
        lea       eax, DWORD PTR [-152+ebp]                     ;
                                ; LOE eax esi edi
.B1.160:                        ; Preds .B1.874
        inc       esi                                           ;413.4
        cmp       esi, DWORD PTR [-388+ebp]                     ;413.4
        jle       .B1.159       ; Prob 82%                      ;413.4
                                ; LOE eax esi edi
.B1.161:                        ; Preds .B1.160
        mov       esi, eax                                      ;
                                ; LOE esi
.B1.162:                        ; Preds .B1.157 .B1.161
        push      0                                             ;413.4
        push      OFFSET FLAT: __STRLITPACK_84.0.1              ;413.4
        push      esi                                           ;413.4
        call      _for_write_seq_fmt_xmit                       ;413.4
                                ; LOE esi
.B1.875:                        ; Preds .B1.162
        add       esp, 12                                       ;413.4
                                ; LOE esi
.B1.163:                        ; Preds .B1.875
        push      32                                            ;414.7
        xor       edx, edx                                      ;414.7
        push      edx                                           ;414.7
        push      OFFSET FLAT: __STRLITPACK_85.0.1              ;414.7
        push      -2088435968                                   ;414.7
        push      900                                           ;414.7
        push      esi                                           ;414.7
        mov       DWORD PTR [-152+ebp], edx                     ;414.7
        call      _for_write_seq_lis                            ;414.7
                                ; LOE
.B1.876:                        ; Preds .B1.163
        add       esp, 24                                       ;414.7
                                ; LOE
.B1.164:                        ; Preds .B1.876
        cmp       DWORD PTR [-188+ebp], 0                       ;415.7
        jle       .B1.185       ; Prob 50%                      ;415.7
                                ; LOE
.B1.165:                        ; Preds .B1.164
        mov       eax, DWORD PTR [-188+ebp]                     ;415.7
        mov       ecx, eax                                      ;415.7
        shr       ecx, 31                                       ;415.7
        add       ecx, eax                                      ;415.7
        sar       ecx, 1                                        ;415.7
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;415.7
        test      ecx, ecx                                      ;415.7
        jbe       .B1.299       ; Prob 10%                      ;415.7
                                ; LOE edx ecx
.B1.166:                        ; Preds .B1.165
        xor       esi, esi                                      ;
        xor       eax, eax                                      ;
        xor       edi, edi                                      ;
                                ; LOE eax edx ecx esi edi
.B1.167:                        ; Preds .B1.167 .B1.166
        inc       esi                                           ;415.7
        mov       DWORD PTR [804+eax+edx], edi                  ;415.7
        mov       DWORD PTR [1704+eax+edx], edi                 ;415.7
        add       eax, 1800                                     ;415.7
        cmp       esi, ecx                                      ;415.7
        jb        .B1.167       ; Prob 63%                      ;415.7
                                ; LOE eax edx ecx esi edi
.B1.168:                        ; Preds .B1.167
        lea       eax, DWORD PTR [1+esi+esi]                    ;415.7
                                ; LOE eax edx ecx
.B1.169:                        ; Preds .B1.168 .B1.299
        lea       esi, DWORD PTR [-1+eax]                       ;415.7
        cmp       esi, DWORD PTR [-188+ebp]                     ;415.7
        jae       .B1.173       ; Prob 10%                      ;415.7
                                ; LOE eax edx ecx
.B1.170:                        ; Preds .B1.169
        mov       esi, DWORD PTR [-260+ebp]                     ;415.7
        mov       edi, esi                                      ;415.7
        neg       edi                                           ;415.7
        add       eax, esi                                      ;415.7
        add       edi, eax                                      ;415.7
        imul      eax, edi, 900                                 ;415.7
        mov       DWORD PTR [-96+edx+eax], 0                    ;415.7
        jmp       .B1.173       ; Prob 100%                     ;415.7
                                ; LOE edx ecx
.B1.171:                        ; Preds .B1.870
        cmp       DWORD PTR [-188+ebp], 0                       ;418.7
        jle       .B1.185       ; Prob 50%                      ;418.7
                                ; LOE
.B1.172:                        ; Preds .B1.171
        mov       eax, DWORD PTR [-188+ebp]                     ;
        mov       ecx, eax                                      ;
        shr       ecx, 31                                       ;
        add       ecx, eax                                      ;
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;418.7
        sar       ecx, 1                                        ;
                                ; LOE edx ecx
.B1.173:                        ; Preds .B1.170 .B1.169 .B1.172
        test      ecx, ecx                                      ;418.7
        jbe       .B1.301       ; Prob 10%                      ;418.7
                                ; LOE edx ecx
.B1.174:                        ; Preds .B1.173
        xor       esi, esi                                      ;
        xor       eax, eax                                      ;
        xor       edi, edi                                      ;
                                ; LOE eax edx ecx esi edi
.B1.175:                        ; Preds .B1.175 .B1.174
        inc       esi                                           ;418.7
        mov       DWORD PTR [404+eax+edx], edi                  ;418.7
        mov       DWORD PTR [1304+eax+edx], edi                 ;418.7
        add       eax, 1800                                     ;418.7
        cmp       esi, ecx                                      ;418.7
        jb        .B1.175       ; Prob 63%                      ;418.7
                                ; LOE eax edx ecx esi edi
.B1.176:                        ; Preds .B1.175
        lea       eax, DWORD PTR [1+esi+esi]                    ;418.7
                                ; LOE eax edx ecx
.B1.177:                        ; Preds .B1.176 .B1.301
        lea       esi, DWORD PTR [-1+eax]                       ;418.7
        cmp       esi, DWORD PTR [-188+ebp]                     ;418.7
        jae       .B1.179       ; Prob 10%                      ;418.7
                                ; LOE eax edx ecx
.B1.178:                        ; Preds .B1.177
        mov       esi, DWORD PTR [-260+ebp]                     ;418.7
        mov       edi, esi                                      ;418.7
        neg       edi                                           ;418.7
        add       eax, esi                                      ;418.7
        add       edi, eax                                      ;418.7
        imul      eax, edi, 900                                 ;418.7
        mov       DWORD PTR [-496+edx+eax], 0                   ;418.7
                                ; LOE edx ecx
.B1.179:                        ; Preds .B1.177 .B1.178
        test      ecx, ecx                                      ;419.7
        jbe       .B1.300       ; Prob 10%                      ;419.7
                                ; LOE edx ecx
.B1.180:                        ; Preds .B1.179
        xor       esi, esi                                      ;
        xor       eax, eax                                      ;
        xor       edi, edi                                      ;
                                ; LOE eax edx ecx esi edi
.B1.181:                        ; Preds .B1.181 .B1.180
        inc       esi                                           ;419.7
        mov       DWORD PTR [408+eax+edx], edi                  ;419.7
        mov       DWORD PTR [1308+eax+edx], edi                 ;419.7
        add       eax, 1800                                     ;419.7
        cmp       esi, ecx                                      ;419.7
        jb        .B1.181       ; Prob 63%                      ;419.7
                                ; LOE eax edx ecx esi edi
.B1.182:                        ; Preds .B1.181
        lea       esi, DWORD PTR [1+esi+esi]                    ;419.7
                                ; LOE edx esi
.B1.183:                        ; Preds .B1.182 .B1.300
        lea       eax, DWORD PTR [-1+esi]                       ;419.7
        cmp       eax, DWORD PTR [-188+ebp]                     ;419.7
        jae       .B1.185       ; Prob 10%                      ;419.7
                                ; LOE edx esi
.B1.184:                        ; Preds .B1.183
        mov       eax, DWORD PTR [-260+ebp]                     ;419.7
        mov       ecx, eax                                      ;419.7
        neg       ecx                                           ;419.7
        add       esi, eax                                      ;419.7
        add       ecx, esi                                      ;419.7
        imul      esi, ecx, 900                                 ;419.7
        mov       DWORD PTR [-492+edx+esi], 0                   ;419.7
                                ; LOE
.B1.185:                        ; Preds .B1.164 .B1.171 .B1.183 .B1.184
        mov       esi, DWORD PTR [-244+ebp]                     ;421.1
        mov       edx, esi                                      ;421.1
        shr       edx, 1                                        ;421.1
        mov       eax, esi                                      ;421.1
        and       edx, 1                                        ;421.1
        and       eax, 1                                        ;421.1
        shl       edx, 2                                        ;421.1
        add       eax, eax                                      ;421.1
        or        edx, eax                                      ;421.1
        or        edx, 262144                                   ;421.1
        push      edx                                           ;421.1
        push      DWORD PTR [-108+ebp]                          ;421.1
        call      _for_dealloc_allocatable                      ;421.1
                                ; LOE esi
.B1.877:                        ; Preds .B1.185
        add       esp, 8                                        ;421.1
                                ; LOE esi
.B1.186:                        ; Preds .B1.877
        and       esi, -2                                       ;421.1
        mov       DWORD PTR [-256+ebp], 0                       ;421.1
        mov       DWORD PTR [-244+ebp], esi                     ;421.1
                                ; LOE
.B1.187:                        ; Preds .B1.186
        mov       eax, DWORD PTR [-284+ebp]                     ;423.1
        mov       esp, eax                                      ;423.1
                                ; LOE
.B1.878:                        ; Preds .B1.187
        mov       esi, DWORD PTR [-276+ebp]                     ;423.1
        mov       edi, DWORD PTR [-268+ebp]                     ;423.1
        mov       esp, ebp                                      ;423.1
        pop       ebp                                           ;423.1
        mov       esp, ebx                                      ;423.1
        pop       ebx                                           ;423.1
        ret                                                     ;423.1
                                ; LOE
.B1.188:                        ; Preds .B1.7
        mov       DWORD PTR [-364+ebp], ecx                     ;
        and       ecx, -8                                       ;35.2
        mov       DWORD PTR [-384+ebp], ecx                     ;35.2
        mov       ecx, DWORD PTR [-368+ebp]                     ;
        pxor      xmm0, xmm0                                    ;37.5
        imul      ecx, esi                                      ;
        mov       DWORD PTR [-264+ebp], edi                     ;
        mov       DWORD PTR [-392+ebp], edx                     ;
        xor       edx, edx                                      ;
        mov       edi, DWORD PTR [-108+ebp]                     ;
        mov       DWORD PTR [-400+ebp], edx                     ;
        mov       edx, edi                                      ;
        shl       eax, 2                                        ;
        sub       edx, eax                                      ;
        sub       eax, ecx                                      ;
        add       edx, eax                                      ;
        add       edx, ecx                                      ;
        mov       ecx, DWORD PTR [-364+ebp]                     ;
        mov       eax, edx                                      ;
        mov       DWORD PTR [-404+ebp], edi                     ;
        mov       DWORD PTR [-380+ebp], edx                     ;
        lea       edi, DWORD PTR [ecx*4]                        ;
        mov       DWORD PTR [-388+ebp], edi                     ;
        mov       edi, DWORD PTR [-400+ebp]                     ;
        mov       DWORD PTR [-408+ebp], edi                     ;
        mov       DWORD PTR [-396+ebp], esi                     ;
        mov       esi, DWORD PTR [-408+ebp]                     ;
        mov       edx, DWORD PTR [-404+ebp]                     ;
        mov       edi, DWORD PTR [-400+ebp]                     ;
                                ; LOE eax edx esi edi
.B1.189:                        ; Preds .B1.191 .B1.830 .B1.828 .B1.188
        cmp       DWORD PTR [-364+ebp], 24                      ;35.2
        jle       .B1.821       ; Prob 0%                       ;35.2
                                ; LOE eax edx esi edi
.B1.190:                        ; Preds .B1.189
        push      DWORD PTR [-388+ebp]                          ;35.2
        push      0                                             ;35.2
        push      eax                                           ;35.2
        mov       DWORD PTR [-408+ebp], eax                     ;35.2
        mov       DWORD PTR [-404+ebp], edx                     ;35.2
        call      __intel_fast_memset                           ;35.2
                                ; LOE esi edi
.B1.879:                        ; Preds .B1.190
        mov       edx, DWORD PTR [-404+ebp]                     ;
        add       esp, 12                                       ;35.2
        mov       eax, DWORD PTR [-408+ebp]                     ;
                                ; LOE eax edx esi edi al dl ah dh
.B1.191:                        ; Preds .B1.879
        inc       edi                                           ;35.2
        mov       ecx, DWORD PTR [-396+ebp]                     ;35.2
        add       edx, ecx                                      ;35.2
        add       eax, ecx                                      ;35.2
        add       esi, ecx                                      ;35.2
        cmp       edi, DWORD PTR [-392+ebp]                     ;35.2
        jb        .B1.189       ; Prob 82%                      ;35.2
                                ; LOE eax edx esi edi
.B1.192:                        ; Preds .B1.191                 ; Infreq
        mov       edi, DWORD PTR [-264+ebp]                     ;
        jmp       .B1.8         ; Prob 100%                     ;
                                ; LOE edi
.B1.193:                        ; Preds .B1.14                  ; Infreq
        cmp       edi, 8                                        ;60.7
        jl        .B1.201       ; Prob 10%                      ;60.7
                                ; LOE esi edi
.B1.194:                        ; Preds .B1.193                 ; Infreq
        mov       eax, edi                                      ;60.7
        xor       edx, edx                                      ;60.7
        mov       ecx, DWORD PTR [-164+ebp]                     ;60.7
        and       eax, -8                                       ;60.7
        pxor      xmm0, xmm0                                    ;60.7
                                ; LOE eax edx ecx esi edi xmm0
.B1.195:                        ; Preds .B1.195 .B1.194         ; Infreq
        movups    XMMWORD PTR [ecx+edx*4], xmm0                 ;60.7
        movups    XMMWORD PTR [16+ecx+edx*4], xmm0              ;60.7
        add       edx, 8                                        ;60.7
        cmp       edx, eax                                      ;60.7
        jb        .B1.195       ; Prob 82%                      ;60.7
                                ; LOE eax edx ecx esi edi xmm0
.B1.197:                        ; Preds .B1.195 .B1.201         ; Infreq
        cmp       eax, edi                                      ;60.7
        jae       .B1.16        ; Prob 10%                      ;60.7
                                ; LOE eax esi edi
.B1.198:                        ; Preds .B1.197                 ; Infreq
        mov       edx, DWORD PTR [-164+ebp]                     ;
        xor       ecx, ecx                                      ;
                                ; LOE eax edx ecx esi edi
.B1.199:                        ; Preds .B1.199 .B1.198         ; Infreq
        mov       DWORD PTR [edx+eax*4], ecx                    ;60.7
        inc       eax                                           ;60.7
        cmp       eax, edi                                      ;60.7
        jb        .B1.199       ; Prob 82%                      ;60.7
        jmp       .B1.16        ; Prob 100%                     ;60.7
                                ; LOE eax edx ecx esi edi
.B1.201:                        ; Preds .B1.193                 ; Infreq
        xor       eax, eax                                      ;60.7
        jmp       .B1.197       ; Prob 100%                     ;60.7
                                ; LOE eax esi edi
.B1.202:                        ; Preds .B1.18                  ; Infreq
        cmp       edi, 8                                        ;84.7
        jl        .B1.210       ; Prob 10%                      ;84.7
                                ; LOE esi edi
.B1.203:                        ; Preds .B1.202                 ; Infreq
        mov       eax, edi                                      ;84.7
        xor       edx, edx                                      ;84.7
        mov       ecx, DWORD PTR [-164+ebp]                     ;84.7
        and       eax, -8                                       ;84.7
        pxor      xmm0, xmm0                                    ;84.7
                                ; LOE eax edx ecx esi edi xmm0
.B1.204:                        ; Preds .B1.204 .B1.203         ; Infreq
        movups    XMMWORD PTR [ecx+edx*4], xmm0                 ;84.7
        movups    XMMWORD PTR [16+ecx+edx*4], xmm0              ;84.7
        add       edx, 8                                        ;84.7
        cmp       edx, eax                                      ;84.7
        jb        .B1.204       ; Prob 82%                      ;84.7
                                ; LOE eax edx ecx esi edi xmm0
.B1.206:                        ; Preds .B1.204 .B1.210         ; Infreq
        cmp       eax, edi                                      ;84.7
        jae       .B1.20        ; Prob 10%                      ;84.7
                                ; LOE eax esi edi
.B1.207:                        ; Preds .B1.206                 ; Infreq
        mov       edx, DWORD PTR [-164+ebp]                     ;
        xor       ecx, ecx                                      ;
                                ; LOE eax edx ecx esi edi
.B1.208:                        ; Preds .B1.208 .B1.207         ; Infreq
        mov       DWORD PTR [edx+eax*4], ecx                    ;84.7
        inc       eax                                           ;84.7
        cmp       eax, edi                                      ;84.7
        jb        .B1.208       ; Prob 82%                      ;84.7
        jmp       .B1.20        ; Prob 100%                     ;84.7
                                ; LOE eax edx ecx esi edi
.B1.210:                        ; Preds .B1.202                 ; Infreq
        xor       eax, eax                                      ;84.7
        jmp       .B1.206       ; Prob 100%                     ;84.7
                                ; LOE eax esi edi
.B1.211:                        ; Preds .B1.22                  ; Infreq
        cmp       edi, 8                                        ;110.7
        jl        .B1.219       ; Prob 10%                      ;110.7
                                ; LOE esi edi
.B1.212:                        ; Preds .B1.211                 ; Infreq
        mov       eax, edi                                      ;110.7
        xor       edx, edx                                      ;110.7
        mov       ecx, DWORD PTR [-164+ebp]                     ;110.7
        and       eax, -8                                       ;110.7
        pxor      xmm0, xmm0                                    ;110.7
                                ; LOE eax edx ecx esi edi xmm0
.B1.213:                        ; Preds .B1.213 .B1.212         ; Infreq
        movups    XMMWORD PTR [ecx+edx*4], xmm0                 ;110.7
        movups    XMMWORD PTR [16+ecx+edx*4], xmm0              ;110.7
        add       edx, 8                                        ;110.7
        cmp       edx, eax                                      ;110.7
        jb        .B1.213       ; Prob 82%                      ;110.7
                                ; LOE eax edx ecx esi edi xmm0
.B1.215:                        ; Preds .B1.213 .B1.219         ; Infreq
        cmp       eax, edi                                      ;110.7
        jae       .B1.24        ; Prob 10%                      ;110.7
                                ; LOE eax esi edi
.B1.216:                        ; Preds .B1.215                 ; Infreq
        mov       edx, DWORD PTR [-164+ebp]                     ;
        xor       ecx, ecx                                      ;
                                ; LOE eax edx ecx esi edi
.B1.217:                        ; Preds .B1.217 .B1.216         ; Infreq
        mov       DWORD PTR [edx+eax*4], ecx                    ;110.7
        inc       eax                                           ;110.7
        cmp       eax, edi                                      ;110.7
        jb        .B1.217       ; Prob 82%                      ;110.7
        jmp       .B1.24        ; Prob 100%                     ;110.7
                                ; LOE eax edx ecx esi edi
.B1.219:                        ; Preds .B1.211                 ; Infreq
        xor       eax, eax                                      ;110.7
        jmp       .B1.215       ; Prob 100%                     ;110.7
                                ; LOE eax esi edi
.B1.220:                        ; Preds .B1.26                  ; Infreq
        cmp       edi, 8                                        ;136.7
        jl        .B1.228       ; Prob 10%                      ;136.7
                                ; LOE esi edi
.B1.221:                        ; Preds .B1.220                 ; Infreq
        mov       eax, edi                                      ;136.7
        xor       edx, edx                                      ;136.7
        mov       ecx, DWORD PTR [-164+ebp]                     ;136.7
        and       eax, -8                                       ;136.7
        pxor      xmm0, xmm0                                    ;136.7
                                ; LOE eax edx ecx esi edi xmm0
.B1.222:                        ; Preds .B1.222 .B1.221         ; Infreq
        movups    XMMWORD PTR [ecx+edx*4], xmm0                 ;136.7
        movups    XMMWORD PTR [16+ecx+edx*4], xmm0              ;136.7
        add       edx, 8                                        ;136.7
        cmp       edx, eax                                      ;136.7
        jb        .B1.222       ; Prob 82%                      ;136.7
                                ; LOE eax edx ecx esi edi xmm0
.B1.224:                        ; Preds .B1.222 .B1.228         ; Infreq
        cmp       eax, edi                                      ;136.7
        jae       .B1.28        ; Prob 10%                      ;136.7
                                ; LOE eax esi edi
.B1.225:                        ; Preds .B1.224                 ; Infreq
        mov       edx, DWORD PTR [-164+ebp]                     ;
        xor       ecx, ecx                                      ;
                                ; LOE eax edx ecx esi edi
.B1.226:                        ; Preds .B1.226 .B1.225         ; Infreq
        mov       DWORD PTR [edx+eax*4], ecx                    ;136.7
        inc       eax                                           ;136.7
        cmp       eax, edi                                      ;136.7
        jb        .B1.226       ; Prob 82%                      ;136.7
        jmp       .B1.28        ; Prob 100%                     ;136.7
                                ; LOE eax edx ecx esi edi
.B1.228:                        ; Preds .B1.220                 ; Infreq
        xor       eax, eax                                      ;136.7
        jmp       .B1.224       ; Prob 100%                     ;136.7
                                ; LOE eax esi edi
.B1.229:                        ; Preds .B1.30                  ; Infreq
        cmp       edi, 8                                        ;162.7
        jl        .B1.237       ; Prob 10%                      ;162.7
                                ; LOE esi edi
.B1.230:                        ; Preds .B1.229                 ; Infreq
        mov       eax, edi                                      ;162.7
        xor       edx, edx                                      ;162.7
        mov       ecx, DWORD PTR [-164+ebp]                     ;162.7
        and       eax, -8                                       ;162.7
        pxor      xmm0, xmm0                                    ;162.7
                                ; LOE eax edx ecx esi edi xmm0
.B1.231:                        ; Preds .B1.231 .B1.230         ; Infreq
        movups    XMMWORD PTR [ecx+edx*4], xmm0                 ;162.7
        movups    XMMWORD PTR [16+ecx+edx*4], xmm0              ;162.7
        add       edx, 8                                        ;162.7
        cmp       edx, eax                                      ;162.7
        jb        .B1.231       ; Prob 82%                      ;162.7
                                ; LOE eax edx ecx esi edi xmm0
.B1.233:                        ; Preds .B1.231 .B1.237         ; Infreq
        cmp       eax, edi                                      ;162.7
        jae       .B1.32        ; Prob 10%                      ;162.7
                                ; LOE eax esi edi
.B1.234:                        ; Preds .B1.233                 ; Infreq
        mov       edx, DWORD PTR [-164+ebp]                     ;
        xor       ecx, ecx                                      ;
                                ; LOE eax edx ecx esi edi
.B1.235:                        ; Preds .B1.235 .B1.234         ; Infreq
        mov       DWORD PTR [edx+eax*4], ecx                    ;162.7
        inc       eax                                           ;162.7
        cmp       eax, edi                                      ;162.7
        jb        .B1.235       ; Prob 82%                      ;162.7
        jmp       .B1.32        ; Prob 100%                     ;162.7
                                ; LOE eax edx ecx esi edi
.B1.237:                        ; Preds .B1.229                 ; Infreq
        xor       eax, eax                                      ;162.7
        jmp       .B1.233       ; Prob 100%                     ;162.7
                                ; LOE eax esi edi
.B1.238:                        ; Preds .B1.34                  ; Infreq
        cmp       edi, 8                                        ;187.7
        jl        .B1.246       ; Prob 10%                      ;187.7
                                ; LOE esi edi
.B1.239:                        ; Preds .B1.238                 ; Infreq
        mov       eax, edi                                      ;187.7
        xor       edx, edx                                      ;187.7
        mov       ecx, DWORD PTR [-164+ebp]                     ;187.7
        and       eax, -8                                       ;187.7
        pxor      xmm0, xmm0                                    ;187.7
                                ; LOE eax edx ecx esi edi xmm0
.B1.240:                        ; Preds .B1.240 .B1.239         ; Infreq
        movups    XMMWORD PTR [ecx+edx*4], xmm0                 ;187.7
        movups    XMMWORD PTR [16+ecx+edx*4], xmm0              ;187.7
        add       edx, 8                                        ;187.7
        cmp       edx, eax                                      ;187.7
        jb        .B1.240       ; Prob 82%                      ;187.7
                                ; LOE eax edx ecx esi edi xmm0
.B1.242:                        ; Preds .B1.240 .B1.246         ; Infreq
        cmp       eax, edi                                      ;187.7
        jae       .B1.36        ; Prob 10%                      ;187.7
                                ; LOE eax esi edi
.B1.243:                        ; Preds .B1.242                 ; Infreq
        mov       edx, DWORD PTR [-164+ebp]                     ;
        xor       ecx, ecx                                      ;
                                ; LOE eax edx ecx esi edi
.B1.244:                        ; Preds .B1.244 .B1.243         ; Infreq
        mov       DWORD PTR [edx+eax*4], ecx                    ;187.7
        inc       eax                                           ;187.7
        cmp       eax, edi                                      ;187.7
        jb        .B1.244       ; Prob 82%                      ;187.7
        jmp       .B1.36        ; Prob 100%                     ;187.7
                                ; LOE eax edx ecx esi edi
.B1.246:                        ; Preds .B1.238                 ; Infreq
        xor       eax, eax                                      ;187.7
        jmp       .B1.242       ; Prob 100%                     ;187.7
                                ; LOE eax esi edi
.B1.247:                        ; Preds .B1.38                  ; Infreq
        cmp       edi, 8                                        ;212.7
        jl        .B1.255       ; Prob 10%                      ;212.7
                                ; LOE esi edi
.B1.248:                        ; Preds .B1.247                 ; Infreq
        mov       eax, edi                                      ;212.7
        xor       edx, edx                                      ;212.7
        mov       ecx, DWORD PTR [-164+ebp]                     ;212.7
        and       eax, -8                                       ;212.7
        pxor      xmm0, xmm0                                    ;212.7
                                ; LOE eax edx ecx esi edi xmm0
.B1.249:                        ; Preds .B1.249 .B1.248         ; Infreq
        movups    XMMWORD PTR [ecx+edx*4], xmm0                 ;212.7
        movups    XMMWORD PTR [16+ecx+edx*4], xmm0              ;212.7
        add       edx, 8                                        ;212.7
        cmp       edx, eax                                      ;212.7
        jb        .B1.249       ; Prob 82%                      ;212.7
                                ; LOE eax edx ecx esi edi xmm0
.B1.251:                        ; Preds .B1.249 .B1.255         ; Infreq
        cmp       eax, edi                                      ;212.7
        jae       .B1.40        ; Prob 10%                      ;212.7
                                ; LOE eax esi edi
.B1.252:                        ; Preds .B1.251                 ; Infreq
        mov       edx, DWORD PTR [-164+ebp]                     ;
        xor       ecx, ecx                                      ;
                                ; LOE eax edx ecx esi edi
.B1.253:                        ; Preds .B1.253 .B1.252         ; Infreq
        mov       DWORD PTR [edx+eax*4], ecx                    ;212.7
        inc       eax                                           ;212.7
        cmp       eax, edi                                      ;212.7
        jb        .B1.253       ; Prob 82%                      ;212.7
        jmp       .B1.40        ; Prob 100%                     ;212.7
                                ; LOE eax edx ecx esi edi
.B1.255:                        ; Preds .B1.247                 ; Infreq
        xor       eax, eax                                      ;212.7
        jmp       .B1.251       ; Prob 100%                     ;212.7
                                ; LOE eax esi edi
.B1.256:                        ; Preds .B1.42                  ; Infreq
        cmp       edi, 8                                        ;237.7
        jl        .B1.264       ; Prob 10%                      ;237.7
                                ; LOE esi edi
.B1.257:                        ; Preds .B1.256                 ; Infreq
        mov       eax, edi                                      ;237.7
        xor       edx, edx                                      ;237.7
        mov       ecx, DWORD PTR [-164+ebp]                     ;237.7
        and       eax, -8                                       ;237.7
        pxor      xmm0, xmm0                                    ;237.7
                                ; LOE eax edx ecx esi edi xmm0
.B1.258:                        ; Preds .B1.258 .B1.257         ; Infreq
        movups    XMMWORD PTR [ecx+edx*4], xmm0                 ;237.7
        movups    XMMWORD PTR [16+ecx+edx*4], xmm0              ;237.7
        add       edx, 8                                        ;237.7
        cmp       edx, eax                                      ;237.7
        jb        .B1.258       ; Prob 82%                      ;237.7
                                ; LOE eax edx ecx esi edi xmm0
.B1.260:                        ; Preds .B1.258 .B1.264         ; Infreq
        cmp       eax, edi                                      ;237.7
        jae       .B1.44        ; Prob 10%                      ;237.7
                                ; LOE eax esi edi
.B1.261:                        ; Preds .B1.260                 ; Infreq
        mov       edx, DWORD PTR [-164+ebp]                     ;
        xor       ecx, ecx                                      ;
                                ; LOE eax edx ecx esi edi
.B1.262:                        ; Preds .B1.262 .B1.261         ; Infreq
        mov       DWORD PTR [edx+eax*4], ecx                    ;237.7
        inc       eax                                           ;237.7
        cmp       eax, edi                                      ;237.7
        jb        .B1.262       ; Prob 82%                      ;237.7
        jmp       .B1.44        ; Prob 100%                     ;237.7
                                ; LOE eax edx ecx esi edi
.B1.264:                        ; Preds .B1.256                 ; Infreq
        xor       eax, eax                                      ;237.7
        jmp       .B1.260       ; Prob 100%                     ;237.7
                                ; LOE eax esi edi
.B1.265:                        ; Preds .B1.46                  ; Infreq
        cmp       edi, 8                                        ;262.7
        jl        .B1.273       ; Prob 10%                      ;262.7
                                ; LOE esi edi
.B1.266:                        ; Preds .B1.265                 ; Infreq
        mov       eax, edi                                      ;262.7
        xor       edx, edx                                      ;262.7
        mov       ecx, DWORD PTR [-164+ebp]                     ;262.7
        and       eax, -8                                       ;262.7
        pxor      xmm0, xmm0                                    ;262.7
                                ; LOE eax edx ecx esi edi xmm0
.B1.267:                        ; Preds .B1.267 .B1.266         ; Infreq
        movups    XMMWORD PTR [ecx+edx*4], xmm0                 ;262.7
        movups    XMMWORD PTR [16+ecx+edx*4], xmm0              ;262.7
        add       edx, 8                                        ;262.7
        cmp       edx, eax                                      ;262.7
        jb        .B1.267       ; Prob 82%                      ;262.7
                                ; LOE eax edx ecx esi edi xmm0
.B1.269:                        ; Preds .B1.267 .B1.273         ; Infreq
        cmp       eax, edi                                      ;262.7
        jae       .B1.48        ; Prob 10%                      ;262.7
                                ; LOE eax esi edi
.B1.270:                        ; Preds .B1.269                 ; Infreq
        mov       edx, DWORD PTR [-164+ebp]                     ;
        xor       ecx, ecx                                      ;
                                ; LOE eax edx ecx esi edi
.B1.271:                        ; Preds .B1.271 .B1.270         ; Infreq
        mov       DWORD PTR [edx+eax*4], ecx                    ;262.7
        inc       eax                                           ;262.7
        cmp       eax, edi                                      ;262.7
        jb        .B1.271       ; Prob 82%                      ;262.7
        jmp       .B1.48        ; Prob 100%                     ;262.7
                                ; LOE eax edx ecx esi edi
.B1.273:                        ; Preds .B1.265                 ; Infreq
        xor       eax, eax                                      ;262.7
        jmp       .B1.269       ; Prob 100%                     ;262.7
                                ; LOE eax esi edi
.B1.274:                        ; Preds .B1.50                  ; Infreq
        cmp       edi, 8                                        ;284.7
        jl        .B1.282       ; Prob 10%                      ;284.7
                                ; LOE esi edi
.B1.275:                        ; Preds .B1.274                 ; Infreq
        mov       eax, edi                                      ;284.7
        xor       edx, edx                                      ;284.7
        mov       ecx, DWORD PTR [-164+ebp]                     ;284.7
        and       eax, -8                                       ;284.7
        pxor      xmm0, xmm0                                    ;284.7
                                ; LOE eax edx ecx esi edi xmm0
.B1.276:                        ; Preds .B1.276 .B1.275         ; Infreq
        movups    XMMWORD PTR [ecx+edx*4], xmm0                 ;284.7
        movups    XMMWORD PTR [16+ecx+edx*4], xmm0              ;284.7
        add       edx, 8                                        ;284.7
        cmp       edx, eax                                      ;284.7
        jb        .B1.276       ; Prob 82%                      ;284.7
                                ; LOE eax edx ecx esi edi xmm0
.B1.278:                        ; Preds .B1.276 .B1.282         ; Infreq
        cmp       eax, edi                                      ;284.7
        jae       .B1.52        ; Prob 10%                      ;284.7
                                ; LOE eax esi edi
.B1.279:                        ; Preds .B1.278                 ; Infreq
        mov       edx, DWORD PTR [-164+ebp]                     ;
        xor       ecx, ecx                                      ;
                                ; LOE eax edx ecx esi edi
.B1.280:                        ; Preds .B1.280 .B1.279         ; Infreq
        mov       DWORD PTR [edx+eax*4], ecx                    ;284.7
        inc       eax                                           ;284.7
        cmp       eax, edi                                      ;284.7
        jb        .B1.280       ; Prob 82%                      ;284.7
        jmp       .B1.52        ; Prob 100%                     ;284.7
                                ; LOE eax edx ecx esi edi
.B1.282:                        ; Preds .B1.274                 ; Infreq
        xor       eax, eax                                      ;284.7
        jmp       .B1.278       ; Prob 100%                     ;284.7
                                ; LOE eax esi edi
.B1.283:                        ; Preds .B1.54                  ; Infreq
        cmp       edi, 8                                        ;308.7
        jl        .B1.291       ; Prob 10%                      ;308.7
                                ; LOE esi edi
.B1.284:                        ; Preds .B1.283                 ; Infreq
        mov       eax, edi                                      ;308.7
        xor       edx, edx                                      ;308.7
        mov       ecx, DWORD PTR [-164+ebp]                     ;308.7
        and       eax, -8                                       ;308.7
        pxor      xmm0, xmm0                                    ;308.7
                                ; LOE eax edx ecx esi edi xmm0
.B1.285:                        ; Preds .B1.285 .B1.284         ; Infreq
        movups    XMMWORD PTR [ecx+edx*4], xmm0                 ;308.7
        movups    XMMWORD PTR [16+ecx+edx*4], xmm0              ;308.7
        add       edx, 8                                        ;308.7
        cmp       edx, eax                                      ;308.7
        jb        .B1.285       ; Prob 82%                      ;308.7
                                ; LOE eax edx ecx esi edi xmm0
.B1.287:                        ; Preds .B1.285 .B1.291         ; Infreq
        cmp       eax, edi                                      ;308.7
        jae       .B1.56        ; Prob 10%                      ;308.7
                                ; LOE eax esi edi
.B1.288:                        ; Preds .B1.287                 ; Infreq
        mov       edx, DWORD PTR [-164+ebp]                     ;
        xor       ecx, ecx                                      ;
                                ; LOE eax edx ecx esi edi
.B1.289:                        ; Preds .B1.289 .B1.288         ; Infreq
        mov       DWORD PTR [edx+eax*4], ecx                    ;308.7
        inc       eax                                           ;308.7
        cmp       eax, edi                                      ;308.7
        jb        .B1.289       ; Prob 82%                      ;308.7
        jmp       .B1.56        ; Prob 100%                     ;308.7
                                ; LOE eax edx ecx esi edi
.B1.291:                        ; Preds .B1.283                 ; Infreq
        xor       eax, eax                                      ;308.7
        jmp       .B1.287       ; Prob 100%                     ;308.7
                                ; LOE eax esi edi
.B1.292:                        ; Preds .B1.83                  ; Infreq
        mov       edx, 1                                        ;
        jmp       .B1.87        ; Prob 100%                     ;
                                ; LOE eax edx esi
.B1.293:                        ; Preds .B1.90                  ; Infreq
        mov       edx, 1                                        ;
        jmp       .B1.94        ; Prob 100%                     ;
                                ; LOE edx ecx esi
.B1.294:                        ; Preds .B1.98                  ; Infreq
        mov       eax, 1                                        ;
        jmp       .B1.110       ; Prob 100%                     ;
                                ; LOE eax edx esi edi
.B1.295:                        ; Preds .B1.121                 ; Infreq
        mov       edx, 1                                        ;
        jmp       .B1.125       ; Prob 100%                     ;
                                ; LOE eax edx esi
.B1.296:                        ; Preds .B1.134                 ; Infreq
        mov       edx, 1                                        ;
        jmp       .B1.138       ; Prob 100%                     ;
                                ; LOE edx esi
.B1.297:                        ; Preds .B1.128                 ; Infreq
        mov       edx, 1                                        ;
        jmp       .B1.132       ; Prob 100%                     ;
                                ; LOE edx ecx esi
.B1.298:                        ; Preds .B1.142                 ; Infreq
        mov       eax, 1                                        ;
        jmp       .B1.150       ; Prob 100%                     ;
                                ; LOE eax edx esi edi
.B1.299:                        ; Preds .B1.165                 ; Infreq
        mov       eax, 1                                        ;
        jmp       .B1.169       ; Prob 100%                     ;
                                ; LOE eax edx ecx
.B1.300:                        ; Preds .B1.179                 ; Infreq
        mov       esi, 1                                        ;
        jmp       .B1.183       ; Prob 100%                     ;
                                ; LOE edx esi
.B1.301:                        ; Preds .B1.173                 ; Infreq
        mov       eax, 1                                        ;
        jmp       .B1.177       ; Prob 100%                     ;
                                ; LOE eax edx ecx
.B1.304:                        ; Preds .B1.59                  ; Infreq
        xor       edx, edx                                      ;349.5
        jmp       .B1.63        ; Prob 100%                     ;349.5
                                ; LOE eax edx esi
.B1.305:                        ; Preds .B1.56                  ; Infreq
        movss     xmm1, DWORD PTR [_2il0floatpacket.18]         ;312.13
        divss     xmm1, DWORD PTR [-300+ebp]                    ;312.13
        movss     xmm0, DWORD PTR [_2il0floatpacket.14]         ;312.13
        andps     xmm0, xmm1                                    ;312.13
        pxor      xmm1, xmm0                                    ;312.13
        movss     xmm2, DWORD PTR [_2il0floatpacket.15]         ;312.13
        movaps    xmm3, xmm1                                    ;312.13
        movss     xmm4, DWORD PTR [_2il0floatpacket.16]         ;312.13
        cmpltss   xmm3, xmm2                                    ;312.13
        andps     xmm2, xmm3                                    ;312.13
        movaps    xmm3, xmm1                                    ;312.13
        movaps    xmm6, xmm4                                    ;312.13
        addss     xmm3, xmm2                                    ;312.13
        addss     xmm6, xmm4                                    ;312.13
        subss     xmm3, xmm2                                    ;312.13
        movaps    xmm7, xmm3                                    ;312.13
        mov       eax, DWORD PTR [-308+ebp]                     ;312.7
        subss     xmm7, xmm1                                    ;312.13
        cdq                                                     ;312.7
        movaps    xmm5, xmm7                                    ;312.13
        cmpless   xmm7, DWORD PTR [_2il0floatpacket.17]         ;312.13
        cmpnless  xmm5, xmm4                                    ;312.13
        andps     xmm5, xmm6                                    ;312.13
        andps     xmm7, xmm6                                    ;312.13
        subss     xmm3, xmm5                                    ;312.13
        addss     xmm3, xmm7                                    ;312.13
        orps      xmm3, xmm0                                    ;312.13
        cvtss2si  ecx, xmm3                                     ;312.13
        idiv      ecx                                           ;312.7
        mov       DWORD PTR [-332+ebp], ecx                     ;312.13
        test      edx, edx                                      ;312.36
        mov       DWORD PTR [-324+ebp], edx                     ;312.7
        jne       .B1.58        ; Prob 50%                      ;312.36
                                ; LOE esi
.B1.306:                        ; Preds .B1.305                 ; Infreq
        mov       eax, DWORD PTR [-308+ebp]                     ;313.20
        mov       DWORD PTR [-400+ebp], 0                       ;
        lea       edx, DWORD PTR [-1+eax]                       ;313.20
        cvtsi2ss  xmm0, edx                                     ;313.20
        mulss     xmm0, DWORD PTR [-300+ebp]                    ;313.14
        cvttss2si ecx, xmm0                                     ;313.14
        inc       ecx                                           ;313.4
        mov       DWORD PTR [-348+ebp], ecx                     ;313.4
        test      esi, esi                                      ;316.6
        jle       .B1.330       ; Prob 3%                       ;316.6
                                ; LOE esi
.B1.307:                        ; Preds .B1.306                 ; Infreq
        imul      edi, DWORD PTR [-260+ebp], -900               ;
        xor       eax, eax                                      ;
        imul      ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32], -152 ;
        xor       edx, edx                                      ;
        add       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;
        add       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;
        mov       DWORD PTR [-408+ebp], edx                     ;
        mov       DWORD PTR [-388+ebp], edx                     ;
        mov       DWORD PTR [-384+ebp], ecx                     ;
        mov       DWORD PTR [-404+ebp], edi                     ;
        mov       DWORD PTR [-380+ebp], eax                     ;
        mov       DWORD PTR [-116+ebp], esi                     ;
        mov       edx, DWORD PTR [-408+ebp]                     ;
                                ; LOE edx
.B1.308:                        ; Preds .B1.328 .B1.307         ; Infreq
        mov       ecx, DWORD PTR [-384+ebp]                     ;317.12
        mov       eax, DWORD PTR [-388+ebp]                     ;317.12
        movsx     esi, WORD PTR [300+eax+ecx]                   ;317.12
        cmp       esi, 99                                       ;317.50
        jge       .B1.328       ; Prob 50%                      ;317.50
                                ; LOE edx
.B1.309:                        ; Preds .B1.308                 ; Infreq
        inc       DWORD PTR [-400+ebp]                          ;318.10
        cmp       DWORD PTR [-348+ebp], 0                       ;319.33
        jle       .B1.326       ; Prob 50%                      ;319.33
                                ; LOE edx
.B1.310:                        ; Preds .B1.309                 ; Infreq
        cmp       DWORD PTR [-348+ebp], 4                       ;319.33
        jl        .B1.363       ; Prob 10%                      ;319.33
                                ; LOE edx
.B1.311:                        ; Preds .B1.310                 ; Infreq
        mov       eax, DWORD PTR [-404+ebp]                     ;319.33
        mov       ecx, DWORD PTR [1460+edx+eax]                 ;319.33
        shl       ecx, 2                                        ;319.33
        mov       eax, DWORD PTR [1428+edx+eax]                 ;319.33
        sub       eax, ecx                                      ;319.33
        add       eax, 4                                        ;319.33
        and       eax, 15                                       ;319.33
        je        .B1.314       ; Prob 50%                      ;319.33
                                ; LOE eax edx
.B1.312:                        ; Preds .B1.311                 ; Infreq
        test      al, 3                                         ;319.33
        jne       .B1.363       ; Prob 10%                      ;319.33
                                ; LOE eax edx
.B1.313:                        ; Preds .B1.312                 ; Infreq
        neg       eax                                           ;319.33
        add       eax, 16                                       ;319.33
        shr       eax, 2                                        ;319.33
                                ; LOE eax edx
.B1.314:                        ; Preds .B1.313 .B1.311         ; Infreq
        lea       ecx, DWORD PTR [4+eax]                        ;319.33
        cmp       ecx, DWORD PTR [-348+ebp]                     ;319.33
        jg        .B1.363       ; Prob 10%                      ;319.33
                                ; LOE eax edx
.B1.315:                        ; Preds .B1.314                 ; Infreq
        mov       esi, DWORD PTR [-348+ebp]                     ;319.33
        mov       ecx, esi                                      ;319.33
        sub       ecx, eax                                      ;319.33
        mov       edi, DWORD PTR [-404+ebp]                     ;319.33
        and       ecx, 3                                        ;319.33
        neg       ecx                                           ;319.33
        add       ecx, esi                                      ;319.33
        mov       esi, DWORD PTR [1460+edx+edi]                 ;319.33
        shl       esi, 2                                        ;
        neg       esi                                           ;
        add       esi, DWORD PTR [1428+edx+edi]                 ;
        mov       DWORD PTR [-392+ebp], 0                       ;
        test      eax, eax                                      ;319.33
        mov       DWORD PTR [-396+ebp], esi                     ;
        jbe       .B1.319       ; Prob 11%                      ;319.33
                                ; LOE eax edx ecx
.B1.316:                        ; Preds .B1.315                 ; Infreq
        xor       esi, esi                                      ;
        mov       DWORD PTR [-408+ebp], edx                     ;
        mov       edx, esi                                      ;
        mov       esi, DWORD PTR [-396+ebp]                     ;
        mov       edi, DWORD PTR [-392+ebp]                     ;
                                ; LOE eax edx ecx esi edi
.B1.317:                        ; Preds .B1.317 .B1.316         ; Infreq
        add       edi, DWORD PTR [4+esi+edx*4]                  ;319.33
        inc       edx                                           ;319.33
        cmp       edx, eax                                      ;319.33
        jb        .B1.317       ; Prob 82%                      ;319.33
                                ; LOE eax edx ecx esi edi
.B1.318:                        ; Preds .B1.317                 ; Infreq
        mov       DWORD PTR [-392+ebp], edi                     ;
        mov       edx, DWORD PTR [-408+ebp]                     ;
                                ; LOE eax edx ecx
.B1.319:                        ; Preds .B1.315 .B1.318         ; Infreq
        movd      xmm0, DWORD PTR [-392+ebp]                    ;319.33
        mov       esi, DWORD PTR [-396+ebp]                     ;319.33
                                ; LOE eax edx ecx esi xmm0
.B1.320:                        ; Preds .B1.320 .B1.319         ; Infreq
        paddd     xmm0, XMMWORD PTR [4+esi+eax*4]               ;319.33
        add       eax, 4                                        ;319.33
        cmp       eax, ecx                                      ;319.33
        jb        .B1.320       ; Prob 82%                      ;319.33
                                ; LOE eax edx ecx esi xmm0
.B1.321:                        ; Preds .B1.320                 ; Infreq
        movdqa    xmm1, xmm0                                    ;319.33
        psrldq    xmm1, 8                                       ;319.33
        paddd     xmm0, xmm1                                    ;319.33
        movdqa    xmm2, xmm0                                    ;319.33
        psrldq    xmm2, 4                                       ;319.33
        paddd     xmm0, xmm2                                    ;319.33
        movd      eax, xmm0                                     ;319.33
                                ; LOE eax edx ecx
.B1.322:                        ; Preds .B1.321 .B1.363         ; Infreq
        cmp       ecx, DWORD PTR [-348+ebp]                     ;319.33
        jae       .B1.327       ; Prob 11%                      ;319.33
                                ; LOE eax edx ecx
.B1.323:                        ; Preds .B1.322                 ; Infreq
        mov       esi, DWORD PTR [-404+ebp]                     ;319.33
        mov       edi, DWORD PTR [1460+edx+esi]                 ;319.33
        shl       edi, 2                                        ;
        neg       edi                                           ;
        add       edi, DWORD PTR [1428+edx+esi]                 ;
        mov       esi, edi                                      ;
        mov       edi, DWORD PTR [-348+ebp]                     ;
                                ; LOE eax edx ecx esi edi
.B1.324:                        ; Preds .B1.324 .B1.323         ; Infreq
        add       eax, DWORD PTR [4+esi+ecx*4]                  ;319.33
        inc       ecx                                           ;319.33
        cmp       ecx, edi                                      ;319.33
        jb        .B1.324       ; Prob 82%                      ;319.33
        jmp       .B1.327       ; Prob 100%                     ;319.33
                                ; LOE eax edx ecx esi edi
.B1.326:                        ; Preds .B1.309                 ; Infreq
        xor       eax, eax                                      ;
                                ; LOE eax edx
.B1.327:                        ; Preds .B1.324 .B1.322 .B1.326 ; Infreq
        cvtsi2ss  xmm0, eax                                     ;319.13
        mov       eax, DWORD PTR [-164+ebp]                     ;319.13
        mov       ecx, DWORD PTR [-400+ebp]                     ;319.13
        movss     DWORD PTR [-4+eax+ecx*4], xmm0                ;319.13
                                ; LOE edx
.B1.328:                        ; Preds .B1.327 .B1.308         ; Infreq
        mov       ecx, DWORD PTR [-380+ebp]                     ;316.6
        add       edx, 900                                      ;316.6
        inc       ecx                                           ;316.6
        mov       eax, DWORD PTR [-388+ebp]                     ;316.6
        add       eax, 152                                      ;316.6
        mov       DWORD PTR [-388+ebp], eax                     ;316.6
        mov       DWORD PTR [-380+ebp], ecx                     ;316.6
        cmp       ecx, DWORD PTR [-116+ebp]                     ;316.6
        jb        .B1.308       ; Prob 82%                      ;316.6
                                ; LOE edx
.B1.329:                        ; Preds .B1.328                 ; Infreq
        mov       esi, DWORD PTR [-116+ebp]                     ;
                                ; LOE esi
.B1.330:                        ; Preds .B1.306 .B1.329         ; Infreq
        push      32                                            ;322.8
        xor       eax, eax                                      ;322.8
        lea       edx, DWORD PTR [-152+ebp]                     ;322.8
        push      eax                                           ;322.8
        push      OFFSET FLAT: __STRLITPACK_63.0.1              ;322.8
        push      -2088435968                                   ;322.8
        push      400                                           ;322.8
        push      edx                                           ;322.8
        mov       DWORD PTR [-152+ebp], eax                     ;322.8
        call      _for_write_seq_lis                            ;322.8
                                ; LOE esi
.B1.880:                        ; Preds .B1.330                 ; Infreq
        add       esp, 24                                       ;322.8
                                ; LOE esi
.B1.331:                        ; Preds .B1.880                 ; Infreq
        cvtsi2ss  xmm0, DWORD PTR [-308+ebp]                    ;323.19
        push      32                                            ;323.3
        push      OFFSET FLAT: CAL_LINK_MOE$format_pack.0.1+140 ;323.3
        mulss     xmm0, DWORD PTR [-300+ebp]                    ;323.3
        lea       eax, DWORD PTR [-264+ebp]                     ;323.3
        push      eax                                           ;323.3
        push      OFFSET FLAT: __STRLITPACK_64.0.1              ;323.3
        push      -2088435968                                   ;323.3
        push      400                                           ;323.3
        mov       DWORD PTR [-152+ebp], 0                       ;323.3
        lea       edx, DWORD PTR [-152+ebp]                     ;323.3
        push      edx                                           ;323.3
        movss     DWORD PTR [-404+ebp], xmm0                    ;323.3
        movss     DWORD PTR [-264+ebp], xmm0                    ;323.3
        call      _for_write_seq_fmt                            ;323.3
                                ; LOE esi
.B1.881:                        ; Preds .B1.331                 ; Infreq
        add       esp, 28                                       ;323.3
                                ; LOE esi
.B1.332:                        ; Preds .B1.881                 ; Infreq
        mov       eax, DWORD PTR [-164+ebp]                     ;324.5
        mov       edx, 1                                        ;324.5
        push      32                                            ;324.5
        mov       DWORD PTR [-380+ebp], eax                     ;324.5
        lea       eax, DWORD PTR [-384+ebp]                     ;324.5
        push      OFFSET FLAT: CAL_LINK_MOE$format_pack.0.1     ;324.5
        push      eax                                           ;324.5
        push      OFFSET FLAT: __STRLITPACK_65.0.1              ;324.5
        mov       edi, DWORD PTR [-400+ebp]                     ;324.5
        xor       ecx, ecx                                      ;324.5
        test      edi, edi                                      ;324.5
        push      -2088435968                                   ;324.5
        mov       DWORD PTR [-384+ebp], edx                     ;324.5
        mov       DWORD PTR [-376+ebp], edx                     ;324.5
        lea       edx, DWORD PTR [-152+ebp]                     ;324.5
        push      400                                           ;324.5
        push      edx                                           ;324.5
        cmovl     edi, ecx                                      ;324.5
        mov       DWORD PTR [-152+ebp], ecx                     ;324.5
        mov       DWORD PTR [-372+ebp], edi                     ;324.5
        mov       DWORD PTR [-368+ebp], 4                       ;324.5
        call      _for_write_seq_fmt                            ;324.5
                                ; LOE esi
.B1.882:                        ; Preds .B1.332                 ; Infreq
        add       esp, 28                                       ;324.5
                                ; LOE esi
.B1.333:                        ; Preds .B1.882                 ; Infreq
        push      32                                            ;325.3
        xor       eax, eax                                      ;325.3
        lea       edx, DWORD PTR [-152+ebp]                     ;325.3
        push      eax                                           ;325.3
        push      OFFSET FLAT: __STRLITPACK_66.0.1              ;325.3
        push      -2088435968                                   ;325.3
        push      400                                           ;325.3
        push      edx                                           ;325.3
        mov       DWORD PTR [-152+ebp], eax                     ;325.3
        call      _for_write_seq_lis                            ;325.3
                                ; LOE esi
.B1.883:                        ; Preds .B1.333                 ; Infreq
        add       esp, 24                                       ;325.3
                                ; LOE esi
.B1.334:                        ; Preds .B1.883                 ; Infreq
        test      BYTE PTR [_DYNUST_MAIN_MODULE_mp_TRUCKOK], 1  ;327.12
        je        .B1.58        ; Prob 60%                      ;327.12
                                ; LOE esi
.B1.335:                        ; Preds .B1.334                 ; Infreq
        mov       DWORD PTR [-388+ebp], 0                       ;
        test      esi, esi                                      ;329.6
        jle       .B1.359       ; Prob 3%                       ;329.6
                                ; LOE esi
.B1.336:                        ; Preds .B1.335                 ; Infreq
        imul      edi, DWORD PTR [-260+ebp], -900               ;
        xor       eax, eax                                      ;
        imul      ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32], -152 ;
        xor       edx, edx                                      ;
        add       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;
        add       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;
        mov       DWORD PTR [-408+ebp], edx                     ;
        mov       DWORD PTR [-364+ebp], edx                     ;
        mov       DWORD PTR [-360+ebp], ecx                     ;
        mov       DWORD PTR [-400+ebp], edi                     ;
        mov       DWORD PTR [-356+ebp], eax                     ;
        mov       DWORD PTR [-116+ebp], esi                     ;
        mov       edx, DWORD PTR [-408+ebp]                     ;
                                ; LOE edx
.B1.337:                        ; Preds .B1.357 .B1.336         ; Infreq
        mov       ecx, DWORD PTR [-360+ebp]                     ;330.12
        mov       eax, DWORD PTR [-364+ebp]                     ;330.12
        movsx     esi, WORD PTR [300+eax+ecx]                   ;330.12
        cmp       esi, 99                                       ;330.50
        jge       .B1.357       ; Prob 50%                      ;330.50
                                ; LOE edx
.B1.338:                        ; Preds .B1.337                 ; Infreq
        inc       DWORD PTR [-388+ebp]                          ;331.10
        cmp       DWORD PTR [-348+ebp], 0                       ;332.33
        jle       .B1.355       ; Prob 50%                      ;332.33
                                ; LOE edx
.B1.339:                        ; Preds .B1.338                 ; Infreq
        cmp       DWORD PTR [-348+ebp], 4                       ;332.33
        jl        .B1.366       ; Prob 10%                      ;332.33
                                ; LOE edx
.B1.340:                        ; Preds .B1.339                 ; Infreq
        mov       eax, DWORD PTR [-400+ebp]                     ;332.33
        mov       ecx, DWORD PTR [1496+edx+eax]                 ;332.33
        shl       ecx, 2                                        ;332.33
        mov       eax, DWORD PTR [1464+edx+eax]                 ;332.33
        sub       eax, ecx                                      ;332.33
        add       eax, 4                                        ;332.33
        and       eax, 15                                       ;332.33
        je        .B1.343       ; Prob 50%                      ;332.33
                                ; LOE eax edx
.B1.341:                        ; Preds .B1.340                 ; Infreq
        test      al, 3                                         ;332.33
        jne       .B1.366       ; Prob 10%                      ;332.33
                                ; LOE eax edx
.B1.342:                        ; Preds .B1.341                 ; Infreq
        neg       eax                                           ;332.33
        add       eax, 16                                       ;332.33
        shr       eax, 2                                        ;332.33
                                ; LOE eax edx
.B1.343:                        ; Preds .B1.342 .B1.340         ; Infreq
        lea       ecx, DWORD PTR [4+eax]                        ;332.33
        cmp       ecx, DWORD PTR [-348+ebp]                     ;332.33
        jg        .B1.366       ; Prob 10%                      ;332.33
                                ; LOE eax edx
.B1.344:                        ; Preds .B1.343                 ; Infreq
        mov       esi, DWORD PTR [-348+ebp]                     ;332.33
        mov       ecx, esi                                      ;332.33
        sub       ecx, eax                                      ;332.33
        mov       edi, DWORD PTR [-400+ebp]                     ;332.33
        and       ecx, 3                                        ;332.33
        neg       ecx                                           ;332.33
        add       ecx, esi                                      ;332.33
        mov       esi, DWORD PTR [1496+edx+edi]                 ;332.33
        shl       esi, 2                                        ;
        neg       esi                                           ;
        add       esi, DWORD PTR [1464+edx+edi]                 ;
        mov       DWORD PTR [-392+ebp], 0                       ;
        test      eax, eax                                      ;332.33
        mov       DWORD PTR [-396+ebp], esi                     ;
        jbe       .B1.348       ; Prob 10%                      ;332.33
                                ; LOE eax edx ecx
.B1.345:                        ; Preds .B1.344                 ; Infreq
        xor       esi, esi                                      ;
        mov       DWORD PTR [-408+ebp], edx                     ;
        mov       edx, esi                                      ;
        mov       esi, DWORD PTR [-396+ebp]                     ;
        mov       edi, DWORD PTR [-392+ebp]                     ;
                                ; LOE eax edx ecx esi edi
.B1.346:                        ; Preds .B1.346 .B1.345         ; Infreq
        add       edi, DWORD PTR [4+esi+edx*4]                  ;332.33
        inc       edx                                           ;332.33
        cmp       edx, eax                                      ;332.33
        jb        .B1.346       ; Prob 82%                      ;332.33
                                ; LOE eax edx ecx esi edi
.B1.347:                        ; Preds .B1.346                 ; Infreq
        mov       DWORD PTR [-392+ebp], edi                     ;
        mov       edx, DWORD PTR [-408+ebp]                     ;
                                ; LOE eax edx ecx
.B1.348:                        ; Preds .B1.344 .B1.347         ; Infreq
        movd      xmm0, DWORD PTR [-392+ebp]                    ;332.33
        mov       esi, DWORD PTR [-396+ebp]                     ;332.33
                                ; LOE eax edx ecx esi xmm0
.B1.349:                        ; Preds .B1.349 .B1.348         ; Infreq
        paddd     xmm0, XMMWORD PTR [4+esi+eax*4]               ;332.33
        add       eax, 4                                        ;332.33
        cmp       eax, ecx                                      ;332.33
        jb        .B1.349       ; Prob 82%                      ;332.33
                                ; LOE eax edx ecx esi xmm0
.B1.350:                        ; Preds .B1.349                 ; Infreq
        movdqa    xmm1, xmm0                                    ;332.33
        psrldq    xmm1, 8                                       ;332.33
        paddd     xmm0, xmm1                                    ;332.33
        movdqa    xmm2, xmm0                                    ;332.33
        psrldq    xmm2, 4                                       ;332.33
        paddd     xmm0, xmm2                                    ;332.33
        movd      eax, xmm0                                     ;332.33
                                ; LOE eax edx ecx
.B1.351:                        ; Preds .B1.350 .B1.366         ; Infreq
        cmp       ecx, DWORD PTR [-348+ebp]                     ;332.33
        jae       .B1.356       ; Prob 10%                      ;332.33
                                ; LOE eax edx ecx
.B1.352:                        ; Preds .B1.351                 ; Infreq
        mov       esi, DWORD PTR [-400+ebp]                     ;332.33
        mov       edi, DWORD PTR [1496+edx+esi]                 ;332.33
        shl       edi, 2                                        ;
        neg       edi                                           ;
        add       edi, DWORD PTR [1464+edx+esi]                 ;
        mov       esi, edi                                      ;
        mov       edi, DWORD PTR [-348+ebp]                     ;
                                ; LOE eax edx ecx esi edi
.B1.353:                        ; Preds .B1.353 .B1.352         ; Infreq
        add       eax, DWORD PTR [4+esi+ecx*4]                  ;332.33
        inc       ecx                                           ;332.33
        cmp       ecx, edi                                      ;332.33
        jb        .B1.353       ; Prob 82%                      ;332.33
        jmp       .B1.356       ; Prob 100%                     ;332.33
                                ; LOE eax edx ecx esi edi
.B1.355:                        ; Preds .B1.338                 ; Infreq
        xor       eax, eax                                      ;
                                ; LOE eax edx
.B1.356:                        ; Preds .B1.353 .B1.351 .B1.355 ; Infreq
        cvtsi2ss  xmm0, eax                                     ;332.13
        mov       eax, DWORD PTR [-164+ebp]                     ;332.13
        mov       ecx, DWORD PTR [-388+ebp]                     ;332.13
        movss     DWORD PTR [-4+eax+ecx*4], xmm0                ;332.13
                                ; LOE edx
.B1.357:                        ; Preds .B1.356 .B1.337         ; Infreq
        mov       ecx, DWORD PTR [-356+ebp]                     ;329.6
        add       edx, 900                                      ;329.6
        inc       ecx                                           ;329.6
        mov       eax, DWORD PTR [-364+ebp]                     ;329.6
        add       eax, 152                                      ;329.6
        mov       DWORD PTR [-364+ebp], eax                     ;329.6
        mov       DWORD PTR [-356+ebp], ecx                     ;329.6
        cmp       ecx, DWORD PTR [-116+ebp]                     ;329.6
        jb        .B1.337       ; Prob 82%                      ;329.6
                                ; LOE edx
.B1.358:                        ; Preds .B1.357                 ; Infreq
        mov       esi, DWORD PTR [-116+ebp]                     ;
                                ; LOE esi
.B1.359:                        ; Preds .B1.335 .B1.358         ; Infreq
        push      32                                            ;335.8
        xor       eax, eax                                      ;335.8
        lea       edx, DWORD PTR [-152+ebp]                     ;335.8
        push      eax                                           ;335.8
        push      OFFSET FLAT: __STRLITPACK_67.0.1              ;335.8
        push      -2088435968                                   ;335.8
        push      401                                           ;335.8
        push      edx                                           ;335.8
        mov       DWORD PTR [-152+ebp], eax                     ;335.8
        call      _for_write_seq_lis                            ;335.8
                                ; LOE esi
.B1.884:                        ; Preds .B1.359                 ; Infreq
        add       esp, 24                                       ;335.8
                                ; LOE esi
.B1.360:                        ; Preds .B1.884                 ; Infreq
        push      32                                            ;336.3
        push      OFFSET FLAT: CAL_LINK_MOE$format_pack.0.1+140 ;336.3
        movss     xmm0, DWORD PTR [-404+ebp]                    ;336.3
        lea       eax, DWORD PTR [-360+ebp]                     ;336.3
        push      eax                                           ;336.3
        push      OFFSET FLAT: __STRLITPACK_68.0.1              ;336.3
        push      -2088435968                                   ;336.3
        push      401                                           ;336.3
        mov       DWORD PTR [-152+ebp], 0                       ;336.3
        lea       edx, DWORD PTR [-152+ebp]                     ;336.3
        push      edx                                           ;336.3
        movss     DWORD PTR [-360+ebp], xmm0                    ;336.3
        call      _for_write_seq_fmt                            ;336.3
                                ; LOE esi
.B1.885:                        ; Preds .B1.360                 ; Infreq
        add       esp, 28                                       ;336.3
                                ; LOE esi
.B1.361:                        ; Preds .B1.885                 ; Infreq
        mov       eax, DWORD PTR [-164+ebp]                     ;337.5
        mov       edx, 1                                        ;337.5
        push      32                                            ;337.5
        mov       DWORD PTR [-404+ebp], eax                     ;337.5
        lea       eax, DWORD PTR [-408+ebp]                     ;337.5
        push      OFFSET FLAT: CAL_LINK_MOE$format_pack.0.1     ;337.5
        push      eax                                           ;337.5
        push      OFFSET FLAT: __STRLITPACK_69.0.1              ;337.5
        mov       edi, DWORD PTR [-388+ebp]                     ;337.5
        xor       ecx, ecx                                      ;337.5
        test      edi, edi                                      ;337.5
        push      -2088435968                                   ;337.5
        mov       DWORD PTR [-408+ebp], edx                     ;337.5
        mov       DWORD PTR [-400+ebp], edx                     ;337.5
        lea       edx, DWORD PTR [-152+ebp]                     ;337.5
        push      401                                           ;337.5
        push      edx                                           ;337.5
        cmovl     edi, ecx                                      ;337.5
        mov       DWORD PTR [-152+ebp], ecx                     ;337.5
        mov       DWORD PTR [-396+ebp], edi                     ;337.5
        mov       DWORD PTR [-392+ebp], 4                       ;337.5
        call      _for_write_seq_fmt                            ;337.5
                                ; LOE esi
.B1.886:                        ; Preds .B1.361                 ; Infreq
        add       esp, 28                                       ;337.5
                                ; LOE esi
.B1.362:                        ; Preds .B1.886                 ; Infreq
        push      32                                            ;338.3
        xor       eax, eax                                      ;338.3
        lea       edx, DWORD PTR [-152+ebp]                     ;338.3
        push      eax                                           ;338.3
        push      OFFSET FLAT: __STRLITPACK_70.0.1              ;338.3
        push      -2088435968                                   ;338.3
        push      401                                           ;338.3
        push      edx                                           ;338.3
        mov       DWORD PTR [-152+ebp], eax                     ;338.3
        call      _for_write_seq_lis                            ;338.3
                                ; LOE esi
.B1.887:                        ; Preds .B1.362                 ; Infreq
        add       esp, 24                                       ;338.3
        jmp       .B1.58        ; Prob 100%                     ;338.3
                                ; LOE esi
.B1.363:                        ; Preds .B1.310 .B1.314 .B1.312 ; Infreq
        xor       eax, eax                                      ;
        xor       ecx, ecx                                      ;
        jmp       .B1.322       ; Prob 100%                     ;
                                ; LOE eax edx ecx
.B1.366:                        ; Preds .B1.339 .B1.343 .B1.341 ; Infreq
        xor       eax, eax                                      ;
        xor       ecx, ecx                                      ;
        jmp       .B1.351       ; Prob 100%                     ;
                                ; LOE eax edx ecx
.B1.369:                        ; Preds .B1.52                  ; Infreq
        test      esi, esi                                      ;286.9
        jle       .B1.376       ; Prob 50%                      ;286.9
                                ; LOE esi edi
.B1.370:                        ; Preds .B1.369                 ; Infreq
        mov       ecx, esi                                      ;286.9
        shr       ecx, 31                                       ;286.9
        add       ecx, esi                                      ;286.9
        sar       ecx, 1                                        ;286.9
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;287.11
        test      ecx, ecx                                      ;286.9
        mov       DWORD PTR [-408+ebp], eax                     ;287.11
        jbe       .B1.407       ; Prob 3%                       ;286.9
                                ; LOE ecx esi edi
.B1.371:                        ; Preds .B1.370                 ; Infreq
        imul      eax, DWORD PTR [-260+ebp], -900               ;
        xor       edx, edx                                      ;
        mov       DWORD PTR [-264+ebp], edi                     ;
        add       eax, DWORD PTR [-408+ebp]                     ;
        mov       edi, edx                                      ;
        ALIGN     16
                                ; LOE eax edx ecx esi edi
.B1.372:                        ; Preds .B1.372 .B1.371         ; Infreq
        pxor      xmm0, xmm0                                    ;287.77
        pxor      xmm1, xmm1                                    ;287.77
        cvtsi2ss  xmm0, DWORD PTR [1304+edi+eax]                ;287.77
        cvtsi2ss  xmm1, DWORD PTR [2204+edi+eax]                ;287.77
        addss     xmm0, DWORD PTR [1688+edi+eax]                ;287.11
        addss     xmm1, DWORD PTR [2588+edi+eax]                ;287.11
        inc       edx                                           ;286.9
        movss     DWORD PTR [1688+edi+eax], xmm0                ;287.11
        movss     DWORD PTR [2588+edi+eax], xmm1                ;287.11
        add       edi, 1800                                     ;286.9
        cmp       edx, ecx                                      ;286.9
        jb        .B1.372       ; Prob 64%                      ;286.9
                                ; LOE eax edx ecx esi edi
.B1.373:                        ; Preds .B1.372                 ; Infreq
        mov       edi, DWORD PTR [-264+ebp]                     ;
        lea       edx, DWORD PTR [1+edx+edx]                    ;286.9
                                ; LOE edx esi edi
.B1.374:                        ; Preds .B1.373 .B1.407         ; Infreq
        lea       eax, DWORD PTR [-1+edx]                       ;286.9
        cmp       esi, eax                                      ;286.9
        jbe       .B1.376       ; Prob 3%                       ;286.9
                                ; LOE edx esi edi
.B1.375:                        ; Preds .B1.374                 ; Infreq
        mov       eax, DWORD PTR [-260+ebp]                     ;287.11
        neg       eax                                           ;287.11
        add       eax, edx                                      ;287.11
        imul      ecx, eax, 900                                 ;287.11
        mov       edx, DWORD PTR [-408+ebp]                     ;287.77
        cvtsi2ss  xmm0, DWORD PTR [404+edx+ecx]                 ;287.77
        addss     xmm0, DWORD PTR [788+edx+ecx]                 ;287.11
        movss     DWORD PTR [788+edx+ecx], xmm0                 ;287.11
                                ; LOE esi edi
.B1.376:                        ; Preds .B1.369 .B1.374 .B1.375 ; Infreq
        mov       eax, DWORD PTR [-308+ebp]                     ;290.12
        cdq                                                     ;290.12
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_I39_T] ;290.9
        idiv      ecx                                           ;290.12
        mov       DWORD PTR [-384+ebp], ecx                     ;290.9
        test      edx, edx                                      ;290.25
        jne       .B1.53        ; Prob 50%                      ;290.25
                                ; LOE esi edi
.B1.377:                        ; Preds .B1.376                 ; Infreq
        test      esi, esi                                      ;292.6
        jle       .B1.389       ; Prob 50%                      ;292.6
                                ; LOE esi edi
.B1.378:                        ; Preds .B1.377                 ; Infreq
        mov       edx, esi                                      ;292.6
        shr       edx, 31                                       ;292.6
        add       edx, esi                                      ;292.6
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;303.11
        sar       edx, 1                                        ;292.6
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;293.8
        test      edx, edx                                      ;292.6
        mov       DWORD PTR [-396+ebp], ecx                     ;303.11
        mov       DWORD PTR [-400+ebp], eax                     ;293.8
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;293.8
        mov       DWORD PTR [-376+ebp], edx                     ;292.6
        mov       DWORD PTR [-372+ebp], 0                       ;
        jbe       .B1.408       ; Prob 3%                       ;292.6
                                ; LOE ecx esi edi
.B1.379:                        ; Preds .B1.378                 ; Infreq
        imul      edx, DWORD PTR [-260+ebp], -900               ;
        xor       eax, eax                                      ;
        add       edx, DWORD PTR [-396+ebp]                     ;
        mov       DWORD PTR [-388+ebp], edx                     ;
        imul      edx, ecx, -152                                ;
        add       edx, DWORD PTR [-400+ebp]                     ;
        mov       DWORD PTR [-404+ebp], eax                     ;
        mov       DWORD PTR [-392+ebp], eax                     ;
        mov       DWORD PTR [-380+ebp], edx                     ;
        mov       DWORD PTR [-408+ebp], ecx                     ;
        mov       DWORD PTR [-116+ebp], esi                     ;
        mov       DWORD PTR [-264+ebp], edi                     ;
        mov       edx, eax                                      ;
        mov       ecx, DWORD PTR [-404+ebp]                     ;
                                ; LOE eax edx ecx
.B1.380:                        ; Preds .B1.384 .B1.379         ; Infreq
        mov       esi, DWORD PTR [-380+ebp]                     ;293.12
        movsx     edi, WORD PTR [300+edx+esi]                   ;293.12
        cmp       edi, 99                                       ;293.50
        jge       .B1.382       ; Prob 50%                      ;293.50
                                ; LOE eax edx ecx
.B1.381:                        ; Preds .B1.380                 ; Infreq
        mov       esi, DWORD PTR [-388+ebp]                     ;295.10
        mov       DWORD PTR [-392+ebp], edx                     ;
        mov       edx, DWORD PTR [-372+ebp]                     ;294.10
        inc       edx                                           ;294.10
        mov       edi, DWORD PTR [1688+ecx+esi]                 ;295.10
        mov       esi, DWORD PTR [-164+ebp]                     ;295.10
        mov       DWORD PTR [-372+ebp], edx                     ;294.10
        mov       DWORD PTR [-4+esi+edx*4], edi                 ;295.10
        mov       edx, DWORD PTR [-392+ebp]                     ;295.10
                                ; LOE eax edx ecx
.B1.382:                        ; Preds .B1.381 .B1.380         ; Infreq
        mov       esi, DWORD PTR [-380+ebp]                     ;293.12
        movsx     edi, WORD PTR [452+edx+esi]                   ;293.12
        cmp       edi, 99                                       ;293.50
        jge       .B1.384       ; Prob 50%                      ;293.50
                                ; LOE eax edx ecx
.B1.383:                        ; Preds .B1.382                 ; Infreq
        mov       esi, DWORD PTR [-388+ebp]                     ;295.10
        mov       DWORD PTR [-392+ebp], edx                     ;
        mov       edx, DWORD PTR [-372+ebp]                     ;294.10
        inc       edx                                           ;294.10
        mov       edi, DWORD PTR [2588+ecx+esi]                 ;295.10
        mov       esi, DWORD PTR [-164+ebp]                     ;295.10
        mov       DWORD PTR [-372+ebp], edx                     ;294.10
        mov       DWORD PTR [-4+esi+edx*4], edi                 ;295.10
        mov       edx, DWORD PTR [-392+ebp]                     ;295.10
                                ; LOE eax edx ecx
.B1.384:                        ; Preds .B1.383 .B1.382         ; Infreq
        inc       eax                                           ;292.6
        add       ecx, 1800                                     ;292.6
        add       edx, 304                                      ;292.6
        cmp       eax, DWORD PTR [-376+ebp]                     ;292.6
        jb        .B1.380       ; Prob 64%                      ;292.6
                                ; LOE eax edx ecx
.B1.385:                        ; Preds .B1.384                 ; Infreq
        mov       ecx, DWORD PTR [-408+ebp]                     ;
        lea       edx, DWORD PTR [1+eax+eax]                    ;292.6
        mov       esi, DWORD PTR [-116+ebp]                     ;
        mov       edi, DWORD PTR [-264+ebp]                     ;
                                ; LOE edx ecx esi edi
.B1.386:                        ; Preds .B1.385 .B1.408         ; Infreq
        lea       eax, DWORD PTR [-1+edx]                       ;292.6
        cmp       esi, eax                                      ;292.6
        jbe       .B1.390       ; Prob 3%                       ;292.6
                                ; LOE edx ecx esi edi
.B1.387:                        ; Preds .B1.386                 ; Infreq
        neg       ecx                                           ;293.50
        add       ecx, edx                                      ;293.50
        imul      ecx, ecx, 152                                 ;293.50
        mov       eax, DWORD PTR [-400+ebp]                     ;293.12
        movsx     eax, WORD PTR [148+eax+ecx]                   ;293.12
        cmp       eax, 99                                       ;293.50
        jge       .B1.390       ; Prob 50%                      ;293.50
                                ; LOE edx esi edi
.B1.388:                        ; Preds .B1.387                 ; Infreq
        mov       ecx, DWORD PTR [-260+ebp]                     ;295.10
        neg       ecx                                           ;295.10
        add       ecx, edx                                      ;295.10
        imul      ecx, ecx, 900                                 ;295.10
        mov       edx, DWORD PTR [-396+ebp]                     ;295.10
        mov       eax, DWORD PTR [-372+ebp]                     ;294.10
        inc       eax                                           ;294.10
        mov       edx, DWORD PTR [788+edx+ecx]                  ;295.10
        mov       ecx, DWORD PTR [-164+ebp]                     ;295.10
        mov       DWORD PTR [-372+ebp], eax                     ;294.10
        mov       DWORD PTR [-4+ecx+eax*4], edx                 ;295.10
        jmp       .B1.390       ; Prob 100%                     ;295.10
                                ; LOE esi edi
.B1.389:                        ; Preds .B1.377                 ; Infreq
        xor       eax, eax                                      ;
        mov       DWORD PTR [-372+ebp], eax                     ;
                                ; LOE esi edi
.B1.390:                        ; Preds .B1.388 .B1.387 .B1.386 .B1.389 ; Infreq
        push      32                                            ;299.8
        xor       eax, eax                                      ;299.8
        lea       edx, DWORD PTR [-152+ebp]                     ;299.8
        push      eax                                           ;299.8
        push      OFFSET FLAT: __STRLITPACK_57.0.1              ;299.8
        push      -2088435968                                   ;299.8
        push      39                                            ;299.8
        push      edx                                           ;299.8
        mov       DWORD PTR [-152+ebp], eax                     ;299.8
        call      _for_write_seq_lis                            ;299.8
                                ; LOE esi edi
.B1.888:                        ; Preds .B1.390                 ; Infreq
        add       esp, 24                                       ;299.8
                                ; LOE esi edi
.B1.391:                        ; Preds .B1.888                 ; Infreq
        cvtsi2ss  xmm0, DWORD PTR [-308+ebp]                    ;300.17
        push      32                                            ;300.3
        push      OFFSET FLAT: CAL_LINK_MOE$format_pack.0.1+140 ;300.3
        mulss     xmm0, DWORD PTR [-300+ebp]                    ;300.3
        lea       eax, DWORD PTR [-272+ebp]                     ;300.3
        push      eax                                           ;300.3
        push      OFFSET FLAT: __STRLITPACK_58.0.1              ;300.3
        push      -2088435968                                   ;300.3
        push      39                                            ;300.3
        mov       DWORD PTR [-152+ebp], 0                       ;300.3
        lea       edx, DWORD PTR [-152+ebp]                     ;300.3
        push      edx                                           ;300.3
        movss     DWORD PTR [-272+ebp], xmm0                    ;300.3
        call      _for_write_seq_fmt                            ;300.3
                                ; LOE esi edi
.B1.889:                        ; Preds .B1.391                 ; Infreq
        add       esp, 28                                       ;300.3
                                ; LOE esi edi
.B1.392:                        ; Preds .B1.889                 ; Infreq
        push      32                                            ;301.5
        push      OFFSET FLAT: CAL_LINK_MOE$format_pack.0.1+40  ;301.5
        xor       eax, eax                                      ;301.5
        lea       edx, DWORD PTR [-152+ebp]                     ;301.5
        push      eax                                           ;301.5
        push      OFFSET FLAT: __STRLITPACK_59.0.1              ;301.5
        push      -2088435968                                   ;301.5
        push      39                                            ;301.5
        push      edx                                           ;301.5
        mov       DWORD PTR [-152+ebp], eax                     ;301.5
        call      _for_write_seq_fmt                            ;301.5
                                ; LOE esi edi
.B1.890:                        ; Preds .B1.392                 ; Infreq
        add       esp, 28                                       ;301.5
                                ; LOE esi edi
.B1.393:                        ; Preds .B1.890                 ; Infreq
        cmp       DWORD PTR [-372+ebp], 0                       ;301.5
        jle       .B1.398       ; Prob 2%                       ;301.5
                                ; LOE esi edi
.B1.394:                        ; Preds .B1.393                 ; Infreq
        cvtsi2ss  xmm0, DWORD PTR [-384+ebp]                    ;301.32
        mov       eax, 1                                        ;
        mov       DWORD PTR [-264+ebp], edi                     ;301.32
        movss     DWORD PTR [-408+ebp], xmm0                    ;301.32
        mov       DWORD PTR [-116+ebp], esi                     ;301.32
        mov       esi, eax                                      ;301.32
        mov       edi, DWORD PTR [-164+ebp]                     ;301.32
                                ; LOE esi edi
.B1.395:                        ; Preds .B1.396 .B1.394         ; Infreq
        movss     xmm0, DWORD PTR [-4+edi+esi*4]                ;301.21
        lea       edx, DWORD PTR [-48+ebp]                      ;301.5
        divss     xmm0, DWORD PTR [-408+ebp]                    ;301.5
        push      edx                                           ;301.5
        push      OFFSET FLAT: __STRLITPACK_60.0.1              ;301.5
        movss     DWORD PTR [-48+ebp], xmm0                     ;301.5
        lea       ecx, DWORD PTR [-152+ebp]                     ;301.5
        push      ecx                                           ;301.5
        call      _for_write_seq_fmt_xmit                       ;301.5
                                ; LOE esi edi
.B1.891:                        ; Preds .B1.395                 ; Infreq
        add       esp, 12                                       ;301.5
                                ; LOE esi edi
.B1.396:                        ; Preds .B1.891                 ; Infreq
        inc       esi                                           ;301.5
        cmp       esi, DWORD PTR [-372+ebp]                     ;301.5
        jle       .B1.395       ; Prob 82%                      ;301.5
                                ; LOE esi edi
.B1.397:                        ; Preds .B1.396                 ; Infreq
        mov       esi, DWORD PTR [-116+ebp]                     ;
        mov       edi, DWORD PTR [-264+ebp]                     ;
                                ; LOE esi edi
.B1.398:                        ; Preds .B1.393 .B1.397         ; Infreq
        push      0                                             ;301.5
        push      OFFSET FLAT: __STRLITPACK_61.0.1              ;301.5
        lea       eax, DWORD PTR [-152+ebp]                     ;301.5
        push      eax                                           ;301.5
        call      _for_write_seq_fmt_xmit                       ;301.5
                                ; LOE esi edi
.B1.892:                        ; Preds .B1.398                 ; Infreq
        add       esp, 12                                       ;301.5
                                ; LOE esi edi
.B1.399:                        ; Preds .B1.892                 ; Infreq
        push      32                                            ;302.3
        xor       eax, eax                                      ;302.3
        lea       edx, DWORD PTR [-152+ebp]                     ;302.3
        push      eax                                           ;302.3
        push      OFFSET FLAT: __STRLITPACK_62.0.1              ;302.3
        push      -2088435968                                   ;302.3
        push      39                                            ;302.3
        push      edx                                           ;302.3
        mov       DWORD PTR [-152+ebp], eax                     ;302.3
        call      _for_write_seq_lis                            ;302.3
                                ; LOE esi edi
.B1.893:                        ; Preds .B1.399                 ; Infreq
        add       esp, 24                                       ;302.3
                                ; LOE esi edi
.B1.400:                        ; Preds .B1.893                 ; Infreq
        cmp       DWORD PTR [-188+ebp], 0                       ;303.11
        jle       .B1.53        ; Prob 50%                      ;303.11
                                ; LOE esi edi
.B1.401:                        ; Preds .B1.400                 ; Infreq
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;303.11
        mov       eax, DWORD PTR [-188+ebp]                     ;303.11
        mov       DWORD PTR [-408+ebp], ecx                     ;303.11
        mov       ecx, eax                                      ;303.11
        shr       ecx, 31                                       ;303.11
        add       ecx, eax                                      ;303.11
        sar       ecx, 1                                        ;303.11
        test      ecx, ecx                                      ;303.11
        jbe       .B1.409       ; Prob 10%                      ;303.11
                                ; LOE ecx esi edi
.B1.402:                        ; Preds .B1.401                 ; Infreq
        xor       edx, edx                                      ;
        mov       DWORD PTR [-116+ebp], esi                     ;
        xor       eax, eax                                      ;
        mov       DWORD PTR [-264+ebp], edi                     ;
        xor       edi, edi                                      ;
        mov       esi, DWORD PTR [-408+ebp]                     ;
                                ; LOE eax edx ecx esi edi
.B1.403:                        ; Preds .B1.403 .B1.402         ; Infreq
        inc       edx                                           ;303.11
        mov       DWORD PTR [788+eax+esi], edi                  ;303.11
        mov       DWORD PTR [1688+eax+esi], edi                 ;303.11
        add       eax, 1800                                     ;303.11
        cmp       edx, ecx                                      ;303.11
        jb        .B1.403       ; Prob 64%                      ;303.11
                                ; LOE eax edx ecx esi edi
.B1.404:                        ; Preds .B1.403                 ; Infreq
        mov       esi, DWORD PTR [-116+ebp]                     ;
        lea       eax, DWORD PTR [1+edx+edx]                    ;303.11
        mov       edi, DWORD PTR [-264+ebp]                     ;
                                ; LOE eax esi edi
.B1.405:                        ; Preds .B1.404 .B1.409         ; Infreq
        lea       edx, DWORD PTR [-1+eax]                       ;303.11
        cmp       edx, DWORD PTR [-188+ebp]                     ;303.11
        jae       .B1.53        ; Prob 10%                      ;303.11
                                ; LOE eax esi edi
.B1.406:                        ; Preds .B1.405                 ; Infreq
        mov       edx, DWORD PTR [-260+ebp]                     ;303.11
        mov       ecx, edx                                      ;303.11
        neg       ecx                                           ;303.11
        add       eax, edx                                      ;303.11
        add       ecx, eax                                      ;303.11
        imul      edx, ecx, 900                                 ;303.11
        mov       eax, DWORD PTR [-408+ebp]                     ;303.11
        mov       DWORD PTR [-112+eax+edx], 0                   ;303.11
        jmp       .B1.53        ; Prob 100%                     ;303.11
                                ; LOE esi edi
.B1.407:                        ; Preds .B1.370                 ; Infreq
        mov       edx, 1                                        ;
        jmp       .B1.374       ; Prob 100%                     ;
                                ; LOE edx esi edi
.B1.408:                        ; Preds .B1.378                 ; Infreq
        mov       edx, 1                                        ;
        jmp       .B1.386       ; Prob 100%                     ;
                                ; LOE edx ecx esi edi
.B1.409:                        ; Preds .B1.401                 ; Infreq
        mov       eax, 1                                        ;
        jmp       .B1.405       ; Prob 100%                     ;
                                ; LOE eax esi edi
.B1.410:                        ; Preds .B1.48                  ; Infreq
        test      esi, esi                                      ;264.9
        jle       .B1.421       ; Prob 3%                       ;264.9
                                ; LOE esi edi
.B1.411:                        ; Preds .B1.410                 ; Infreq
        mov       DWORD PTR [-116+ebp], esi                     ;
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+40] ;273.10
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+44] ;
        imul      ecx, esi                                      ;
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+32] ;
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+36] ;265.11
        mov       DWORD PTR [-264+ebp], edi                     ;
        xor       edi, edi                                      ;
        mov       DWORD PTR [-388+ebp], edi                     ;
        lea       edi, DWORD PTR [edx*4]                        ;
        shl       edx, 5                                        ;
        mov       DWORD PTR [-384+ebp], eax                     ;265.11
        sub       edx, edi                                      ;
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE] ;
        lea       edi, DWORD PTR [esi+ecx]                      ;
        sub       eax, ecx                                      ;
        add       edi, eax                                      ;
        sub       edi, edx                                      ;
        mov       DWORD PTR [-408+ebp], esi                     ;273.10
        neg       esi                                           ;
        mov       DWORD PTR [-392+ebp], edi                     ;
        add       ecx, esi                                      ;
        mov       DWORD PTR [-404+ebp], edx                     ;
        add       eax, ecx                                      ;
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE] ;
        sub       edi, edx                                      ;
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+36] ;
        mov       DWORD PTR [-400+ebp], edi                     ;
        mov       edi, edx                                      ;
        shr       edi, 31                                       ;
        add       edi, edx                                      ;
        sub       eax, DWORD PTR [-404+ebp]                     ;
        sar       edi, 1                                        ;
        mov       DWORD PTR [-380+ebp], edi                     ;
        mov       edx, DWORD PTR [-388+ebp]                     ;
        mov       esi, DWORD PTR [-116+ebp]                     ;
        mov       edi, DWORD PTR [-264+ebp]                     ;
        mov       ecx, DWORD PTR [-408+ebp]                     ;
        mov       DWORD PTR [-396+ebp], eax                     ;
                                ; LOE edx ecx
.B1.412:                        ; Preds .B1.894 .B1.411         ; Infreq
        mov       DWORD PTR [-404+ebp], esp                     ;265.11
        cmp       DWORD PTR [-384+ebp], 0                       ;265.11
        jle       .B1.419       ; Prob 50%                      ;265.11
                                ; LOE edx ecx
.B1.413:                        ; Preds .B1.412                 ; Infreq
        cmp       DWORD PTR [-380+ebp], 0                       ;265.11
        jbe       .B1.439       ; Prob 11%                      ;265.11
                                ; LOE edx ecx
.B1.414:                        ; Preds .B1.413                 ; Infreq
        mov       edi, DWORD PTR [-400+ebp]                     ;
        xor       esi, esi                                      ;
        mov       eax, DWORD PTR [-392+ebp]                     ;
        mov       DWORD PTR [-408+ebp], edx                     ;
        add       edi, edx                                      ;
        add       eax, edx                                      ;
        mov       edx, esi                                      ;
                                ; LOE eax edx ecx esi edi
.B1.415:                        ; Preds .B1.415 .B1.414         ; Infreq
        pxor      xmm0, xmm0                                    ;265.89
        pxor      xmm1, xmm1                                    ;265.89
        cvtsi2ss  xmm0, DWORD PTR [32+edi+edx*2]                ;265.89
        cvtsi2ss  xmm1, DWORD PTR [32+eax+edx*2]                ;265.89
        addss     xmm0, DWORD PTR [52+edi+edx*2]                ;265.11
        movss     DWORD PTR [52+edi+edx*2], xmm0                ;265.11
        inc       esi                                           ;265.11
        addss     xmm1, DWORD PTR [52+eax+edx*2]                ;265.11
        movss     DWORD PTR [52+eax+edx*2], xmm1                ;265.11
        add       edx, ecx                                      ;265.11
        cmp       esi, DWORD PTR [-380+ebp]                     ;265.11
        jb        .B1.415       ; Prob 64%                      ;265.11
                                ; LOE eax edx ecx esi edi
.B1.416:                        ; Preds .B1.415                 ; Infreq
        mov       edx, DWORD PTR [-408+ebp]                     ;
        lea       eax, DWORD PTR [1+esi+esi]                    ;265.11
                                ; LOE eax edx ecx
.B1.417:                        ; Preds .B1.416 .B1.439         ; Infreq
        lea       esi, DWORD PTR [-1+eax]                       ;265.11
        cmp       esi, DWORD PTR [-384+ebp]                     ;265.11
        jae       .B1.419       ; Prob 11%                      ;265.11
                                ; LOE eax edx ecx
.B1.418:                        ; Preds .B1.417                 ; Infreq
        imul      eax, ecx                                      ;265.11
        add       eax, DWORD PTR [-396+ebp]                     ;265.11
        cvtsi2ss  xmm0, DWORD PTR [32+eax+edx]                  ;265.89
        addss     xmm0, DWORD PTR [52+eax+edx]                  ;265.11
        movss     DWORD PTR [52+eax+edx], xmm0                  ;265.11
                                ; LOE edx ecx
.B1.419:                        ; Preds .B1.412 .B1.417 .B1.418 ; Infreq
        mov       eax, DWORD PTR [-404+ebp]                     ;265.11
        mov       esp, eax                                      ;265.11
                                ; LOE edx ecx
.B1.894:                        ; Preds .B1.419                 ; Infreq
        mov       eax, DWORD PTR [-388+ebp]                     ;264.9
        add       edx, 28                                       ;264.9
        inc       eax                                           ;264.9
        mov       DWORD PTR [-388+ebp], eax                     ;264.9
        cmp       eax, DWORD PTR [-116+ebp]                     ;264.9
        jb        .B1.412       ; Prob 82%                      ;264.9
                                ; LOE edx ecx
.B1.420:                        ; Preds .B1.894                 ; Infreq
        mov       esi, DWORD PTR [-116+ebp]                     ;
        mov       edi, DWORD PTR [-264+ebp]                     ;
                                ; LOE esi edi
.B1.421:                        ; Preds .B1.410 .B1.420         ; Infreq
        mov       eax, DWORD PTR [-308+ebp]                     ;268.9
        cdq                                                     ;268.9
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_I38_T] ;268.6
        idiv      ecx                                           ;268.9
        test      edx, edx                                      ;268.22
        jne       .B1.49        ; Prob 50%                      ;268.22
                                ; LOE esi edi
.B1.422:                        ; Preds .B1.421                 ; Infreq
        test      esi, esi                                      ;270.6
        jle       .B1.437       ; Prob 3%                       ;270.6
                                ; LOE esi edi
.B1.423:                        ; Preds .B1.422                 ; Infreq
        imul      edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32], -152 ;
        mov       DWORD PTR [-264+ebp], edi                     ;
        mov       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+40] ;273.10
        add       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;
        mov       eax, DWORD PTR [-216+ebp]                     ;274.10
        mov       DWORD PTR [-348+ebp], edi                     ;273.10
        mov       DWORD PTR [-364+ebp], edx                     ;
        mov       edi, DWORD PTR [-368+ebp]                     ;
        mov       edx, DWORD PTR [-224+ebp]                     ;
        imul      edi, eax                                      ;
        shl       edx, 2                                        ;
        neg       edx                                           ;
        add       edx, DWORD PTR [-108+ebp]                     ;
        mov       DWORD PTR [-116+ebp], esi                     ;
        xor       esi, esi                                      ;
        mov       DWORD PTR [-408+ebp], esi                     ;
        mov       DWORD PTR [-396+ebp], esi                     ;
        mov       esi, edx                                      ;
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_MAXMOVE] ;273.10
        sub       esi, edi                                      ;
        mov       DWORD PTR [-368+ebp], edi                     ;
        sub       edi, eax                                      ;
        neg       edi                                           ;
        mov       DWORD PTR [-404+ebp], edx                     ;
        add       edi, edx                                      ;
        mov       DWORD PTR [-388+ebp], esi                     ;
        lea       edx, DWORD PTR [1+ecx]                        ;274.10
        imul      edx, eax                                      ;274.10
        add       edx, esi                                      ;
        mov       esi, ecx                                      ;
        mov       DWORD PTR [-376+ebp], edx                     ;
        shr       esi, 31                                       ;
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+40] ;
        add       esi, ecx                                      ;
        mov       DWORD PTR [-372+ebp], ecx                     ;273.10
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+44] ;
        imul      ecx, edx                                      ;
        sar       esi, 1                                        ;
        mov       DWORD PTR [-360+ebp], esi                     ;
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE] ;
        sub       esi, ecx                                      ;
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE+32] ;
        mov       DWORD PTR [-392+ebp], edi                     ;
        mov       DWORD PTR [-356+ebp], eax                     ;274.10
        add       eax, eax                                      ;
        lea       edi, DWORD PTR [ecx*4]                        ;
        shl       ecx, 5                                        ;
        sub       ecx, edi                                      ;
        mov       edi, esi                                      ;
        sub       edi, ecx                                      ;
        mov       DWORD PTR [-380+ebp], edi                     ;
        lea       edi, DWORD PTR [edx+esi]                      ;
        sub       edi, ecx                                      ;
        lea       edx, DWORD PTR [esi+edx*2]                    ;
        mov       DWORD PTR [-384+ebp], edi                     ;
        sub       edx, ecx                                      ;
        mov       edi, DWORD PTR [-368+ebp]                     ;
        sub       edi, eax                                      ;
        mov       eax, DWORD PTR [-404+ebp]                     ;
        sub       eax, edi                                      ;
        mov       DWORD PTR [-400+ebp], edx                     ;
        mov       edx, DWORD PTR [-396+ebp]                     ;
        mov       ecx, DWORD PTR [-408+ebp]                     ;
        mov       DWORD PTR [-404+ebp], eax                     ;
                                ; LOE edx ecx
.B1.424:                        ; Preds .B1.435 .B1.423         ; Infreq
        imul      esi, edx, 152                                 ;271.50
        mov       eax, DWORD PTR [-364+ebp]                     ;271.12
        movsx     edi, WORD PTR [300+eax+esi]                   ;271.12
        cmp       edi, 99                                       ;271.50
        jge       .B1.434       ; Prob 50%                      ;271.50
                                ; LOE edx ecx
.B1.425:                        ; Preds .B1.424                 ; Infreq
        inc       ecx                                           ;272.10
        cmp       DWORD PTR [-372+ebp], 0                       ;273.10
        jle       .B1.432       ; Prob 50%                      ;273.10
                                ; LOE edx ecx
.B1.426:                        ; Preds .B1.425                 ; Infreq
        cmp       DWORD PTR [-360+ebp], 0                       ;273.10
        lea       eax, DWORD PTR [ecx*4]                        ;
        mov       DWORD PTR [-368+ebp], eax                     ;
        jbe       .B1.440       ; Prob 11%                      ;273.10
                                ; LOE edx ecx
.B1.427:                        ; Preds .B1.426                 ; Infreq
        mov       edi, DWORD PTR [-392+ebp]                     ;
        xor       eax, eax                                      ;
        mov       DWORD PTR [-396+ebp], edx                     ;
        mov       DWORD PTR [-408+ebp], ecx                     ;
        lea       esi, DWORD PTR [edi+ecx*4]                    ;
        mov       DWORD PTR [-340+ebp], esi                     ;
        mov       esi, edx                                      ;
        lea       edi, DWORD PTR [edx*4]                        ;
        shl       esi, 5                                        ;
        sub       esi, edi                                      ;
        mov       edi, DWORD PTR [-384+ebp]                     ;
        add       edi, esi                                      ;
        mov       DWORD PTR [-316+ebp], edi                     ;
        mov       edi, DWORD PTR [-404+ebp]                     ;
        add       esi, DWORD PTR [-400+ebp]                     ;
        mov       DWORD PTR [-332+ebp], esi                     ;
        xor       esi, esi                                      ;
        lea       edi, DWORD PTR [edi+ecx*4]                    ;
        mov       DWORD PTR [-324+ebp], edi                     ;
        mov       edx, esi                                      ;
                                ; LOE eax edx esi
.B1.428:                        ; Preds .B1.428 .B1.427         ; Infreq
        mov       ecx, DWORD PTR [-316+ebp]                     ;273.10
        inc       eax                                           ;273.10
        mov       edi, DWORD PTR [52+ecx+edx*2]                 ;273.10
        mov       ecx, DWORD PTR [-340+ebp]                     ;273.10
        mov       DWORD PTR [ecx+esi*2], edi                    ;273.10
        mov       edi, DWORD PTR [-332+ebp]                     ;273.10
        mov       ecx, DWORD PTR [52+edi+edx*2]                 ;273.10
        mov       edi, DWORD PTR [-324+ebp]                     ;273.10
        add       edx, DWORD PTR [-348+ebp]                     ;273.10
        mov       DWORD PTR [edi+esi*2], ecx                    ;273.10
        add       esi, DWORD PTR [-356+ebp]                     ;273.10
        cmp       eax, DWORD PTR [-360+ebp]                     ;273.10
        jb        .B1.428       ; Prob 64%                      ;273.10
                                ; LOE eax edx esi
.B1.429:                        ; Preds .B1.428                 ; Infreq
        mov       edx, DWORD PTR [-396+ebp]                     ;
        lea       eax, DWORD PTR [1+eax+eax]                    ;273.10
        mov       ecx, DWORD PTR [-408+ebp]                     ;
                                ; LOE eax edx ecx
.B1.430:                        ; Preds .B1.429 .B1.440         ; Infreq
        lea       esi, DWORD PTR [-1+eax]                       ;273.10
        cmp       esi, DWORD PTR [-372+ebp]                     ;273.10
        jae       .B1.433       ; Prob 11%                      ;273.10
                                ; LOE eax edx ecx
.B1.431:                        ; Preds .B1.430                 ; Infreq
        mov       edi, eax                                      ;273.10
        lea       esi, DWORD PTR [edx*4]                        ;273.10
        imul      edi, DWORD PTR [-348+ebp]                     ;273.10
        imul      eax, DWORD PTR [-356+ebp]                     ;273.10
        mov       DWORD PTR [-396+ebp], edx                     ;
        shl       edx, 5                                        ;273.10
        sub       edx, esi                                      ;273.10
        add       edi, DWORD PTR [-380+ebp]                     ;273.10
        mov       esi, DWORD PTR [-388+ebp]                     ;273.10
        mov       edx, DWORD PTR [52+edi+edx]                   ;273.10
        lea       esi, DWORD PTR [esi+ecx*4]                    ;273.10
        mov       DWORD PTR [eax+esi], edx                      ;273.10
        mov       edx, DWORD PTR [-396+ebp]                     ;273.10
        jmp       .B1.433       ; Prob 100%                     ;273.10
                                ; LOE edx ecx
.B1.432:                        ; Preds .B1.425                 ; Infreq
        lea       eax, DWORD PTR [ecx*4]                        ;
        mov       DWORD PTR [-368+ebp], eax                     ;
                                ; LOE edx ecx
.B1.433:                        ; Preds .B1.431 .B1.430 .B1.432 ; Infreq
        inc       edx                                           ;274.10
        cvtsi2ss  xmm0, edx                                     ;274.10
        mov       eax, DWORD PTR [-376+ebp]                     ;274.10
        mov       esi, DWORD PTR [-368+ebp]                     ;274.10
        movss     DWORD PTR [eax+esi], xmm0                     ;274.10
        jmp       .B1.435       ; Prob 100%                     ;274.10
                                ; LOE edx ecx
.B1.434:                        ; Preds .B1.424                 ; Infreq
        inc       edx                                           ;
                                ; LOE edx ecx
.B1.435:                        ; Preds .B1.433 .B1.434         ; Infreq
        cmp       edx, DWORD PTR [-116+ebp]                     ;270.6
        jb        .B1.424       ; Prob 82%                      ;270.6
                                ; LOE edx ecx
.B1.436:                        ; Preds .B1.435                 ; Infreq
        mov       esi, DWORD PTR [-116+ebp]                     ;
        mov       edi, DWORD PTR [-264+ebp]                     ;
                                ; LOE esi edi
.B1.437:                        ; Preds .B1.422 .B1.436         ; Infreq
        push      32                                            ;278.6
        xor       eax, eax                                      ;278.6
        lea       edx, DWORD PTR [-152+ebp]                     ;278.6
        push      eax                                           ;278.6
        push      OFFSET FLAT: __STRLITPACK_55.0.1              ;278.6
        push      -2088435968                                   ;278.6
        push      38                                            ;278.6
        push      edx                                           ;278.6
        mov       DWORD PTR [-152+ebp], eax                     ;278.6
        call      _for_write_seq_lis                            ;278.6
                                ; LOE esi edi
.B1.895:                        ; Preds .B1.437                 ; Infreq
        add       esp, 24                                       ;278.6
                                ; LOE esi edi
.B1.438:                        ; Preds .B1.895                 ; Infreq
        cvtsi2ss  xmm0, DWORD PTR [-308+ebp]                    ;279.23
        push      32                                            ;279.9
        push      OFFSET FLAT: CAL_LINK_MOE$format_pack.0.1+140 ;279.9
        mulss     xmm0, DWORD PTR [-300+ebp]                    ;279.9
        lea       eax, DWORD PTR [-280+ebp]                     ;279.9
        push      eax                                           ;279.9
        push      OFFSET FLAT: __STRLITPACK_56.0.1              ;279.9
        push      -2088435968                                   ;279.9
        push      38                                            ;279.9
        mov       DWORD PTR [-152+ebp], 0                       ;279.9
        lea       edx, DWORD PTR [-152+ebp]                     ;279.9
        push      edx                                           ;279.9
        movss     DWORD PTR [-280+ebp], xmm0                    ;279.9
        call      _for_write_seq_fmt                            ;279.9
                                ; LOE esi edi
.B1.896:                        ; Preds .B1.438                 ; Infreq
        add       esp, 28                                       ;279.9
        jmp       .B1.49        ; Prob 100%                     ;279.9
                                ; LOE esi edi
.B1.439:                        ; Preds .B1.413                 ; Infreq
        mov       eax, 1                                        ;
        jmp       .B1.417       ; Prob 100%                     ;
                                ; LOE eax edx ecx
.B1.440:                        ; Preds .B1.426                 ; Infreq
        mov       eax, 1                                        ;
        jmp       .B1.430       ; Prob 100%                     ;
                                ; LOE eax edx ecx
.B1.441:                        ; Preds .B1.44                  ; Infreq
        test      esi, esi                                      ;239.9
        jle       .B1.448       ; Prob 50%                      ;239.9
                                ; LOE esi edi
.B1.442:                        ; Preds .B1.441                 ; Infreq
        mov       edx, esi                                      ;239.9
        shr       edx, 31                                       ;239.9
        add       edx, esi                                      ;239.9
        sar       edx, 1                                        ;239.9
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;240.11
        test      edx, edx                                      ;239.9
        mov       DWORD PTR [-408+ebp], eax                     ;240.11
        jbe       .B1.479       ; Prob 3%                       ;239.9
                                ; LOE edx esi edi
.B1.443:                        ; Preds .B1.442                 ; Infreq
        imul      eax, DWORD PTR [-260+ebp], -900               ;
        xor       ecx, ecx                                      ;
        mov       DWORD PTR [-264+ebp], edi                     ;
        add       eax, DWORD PTR [-408+ebp]                     ;
        mov       edi, ecx                                      ;
        ALIGN     16
                                ; LOE eax edx ecx esi edi
.B1.444:                        ; Preds .B1.444 .B1.443         ; Infreq
        pxor      xmm0, xmm0                                    ;240.77
        pxor      xmm1, xmm1                                    ;240.77
        cvtsi2ss  xmm0, DWORD PTR [1308+edi+eax]                ;240.77
        cvtsi2ss  xmm1, DWORD PTR [2208+edi+eax]                ;240.77
        addss     xmm0, DWORD PTR [1684+edi+eax]                ;240.11
        addss     xmm1, DWORD PTR [2584+edi+eax]                ;240.11
        inc       ecx                                           ;239.9
        movss     DWORD PTR [1684+edi+eax], xmm0                ;240.11
        movss     DWORD PTR [2584+edi+eax], xmm1                ;240.11
        add       edi, 1800                                     ;239.9
        cmp       ecx, edx                                      ;239.9
        jb        .B1.444       ; Prob 64%                      ;239.9
                                ; LOE eax edx ecx esi edi
.B1.445:                        ; Preds .B1.444                 ; Infreq
        mov       edi, DWORD PTR [-264+ebp]                     ;
        lea       edx, DWORD PTR [1+ecx+ecx]                    ;239.9
                                ; LOE edx esi edi
.B1.446:                        ; Preds .B1.445 .B1.479         ; Infreq
        lea       eax, DWORD PTR [-1+edx]                       ;239.9
        cmp       esi, eax                                      ;239.9
        jbe       .B1.448       ; Prob 3%                       ;239.9
                                ; LOE edx esi edi
.B1.447:                        ; Preds .B1.446                 ; Infreq
        mov       eax, DWORD PTR [-260+ebp]                     ;240.11
        neg       eax                                           ;240.11
        add       eax, edx                                      ;240.11
        imul      ecx, eax, 900                                 ;240.11
        mov       edx, DWORD PTR [-408+ebp]                     ;240.77
        cvtsi2ss  xmm0, DWORD PTR [408+edx+ecx]                 ;240.77
        addss     xmm0, DWORD PTR [784+edx+ecx]                 ;240.11
        movss     DWORD PTR [784+edx+ecx], xmm0                 ;240.11
                                ; LOE esi edi
.B1.448:                        ; Preds .B1.441 .B1.446 .B1.447 ; Infreq
        mov       eax, DWORD PTR [-308+ebp]                     ;243.12
        cdq                                                     ;243.12
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_I37_T] ;243.9
        idiv      ecx                                           ;243.12
        mov       DWORD PTR [-384+ebp], ecx                     ;243.9
        test      edx, edx                                      ;243.25
        jne       .B1.45        ; Prob 50%                      ;243.25
                                ; LOE esi edi
.B1.449:                        ; Preds .B1.448                 ; Infreq
        test      esi, esi                                      ;245.6
        jle       .B1.461       ; Prob 50%                      ;245.6
                                ; LOE esi edi
.B1.450:                        ; Preds .B1.449                 ; Infreq
        mov       edx, esi                                      ;245.6
        shr       edx, 31                                       ;245.6
        add       edx, esi                                      ;245.6
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;256.11
        sar       edx, 1                                        ;245.6
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;246.8
        test      edx, edx                                      ;245.6
        mov       DWORD PTR [-396+ebp], ecx                     ;256.11
        mov       DWORD PTR [-400+ebp], eax                     ;246.8
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;246.8
        mov       DWORD PTR [-376+ebp], edx                     ;245.6
        mov       DWORD PTR [-372+ebp], 0                       ;
        jbe       .B1.480       ; Prob 3%                       ;245.6
                                ; LOE ecx esi edi
.B1.451:                        ; Preds .B1.450                 ; Infreq
        imul      edx, DWORD PTR [-260+ebp], -900               ;
        xor       eax, eax                                      ;
        add       edx, DWORD PTR [-396+ebp]                     ;
        mov       DWORD PTR [-388+ebp], edx                     ;
        imul      edx, ecx, -152                                ;
        add       edx, DWORD PTR [-400+ebp]                     ;
        mov       DWORD PTR [-404+ebp], eax                     ;
        mov       DWORD PTR [-392+ebp], eax                     ;
        mov       DWORD PTR [-380+ebp], edx                     ;
        mov       DWORD PTR [-408+ebp], ecx                     ;
        mov       DWORD PTR [-116+ebp], esi                     ;
        mov       DWORD PTR [-264+ebp], edi                     ;
        mov       edx, eax                                      ;
        mov       ecx, DWORD PTR [-404+ebp]                     ;
                                ; LOE eax edx ecx
.B1.452:                        ; Preds .B1.456 .B1.451         ; Infreq
        mov       esi, DWORD PTR [-380+ebp]                     ;246.12
        movsx     edi, WORD PTR [300+edx+esi]                   ;246.12
        cmp       edi, 99                                       ;246.50
        jge       .B1.454       ; Prob 50%                      ;246.50
                                ; LOE eax edx ecx
.B1.453:                        ; Preds .B1.452                 ; Infreq
        mov       esi, DWORD PTR [-388+ebp]                     ;248.10
        mov       DWORD PTR [-392+ebp], edx                     ;
        mov       edx, DWORD PTR [-372+ebp]                     ;247.10
        inc       edx                                           ;247.10
        mov       edi, DWORD PTR [1684+ecx+esi]                 ;248.10
        mov       esi, DWORD PTR [-164+ebp]                     ;248.10
        mov       DWORD PTR [-372+ebp], edx                     ;247.10
        mov       DWORD PTR [-4+esi+edx*4], edi                 ;248.10
        mov       edx, DWORD PTR [-392+ebp]                     ;248.10
                                ; LOE eax edx ecx
.B1.454:                        ; Preds .B1.453 .B1.452         ; Infreq
        mov       esi, DWORD PTR [-380+ebp]                     ;246.12
        movsx     edi, WORD PTR [452+edx+esi]                   ;246.12
        cmp       edi, 99                                       ;246.50
        jge       .B1.456       ; Prob 50%                      ;246.50
                                ; LOE eax edx ecx
.B1.455:                        ; Preds .B1.454                 ; Infreq
        mov       esi, DWORD PTR [-388+ebp]                     ;248.10
        mov       DWORD PTR [-392+ebp], edx                     ;
        mov       edx, DWORD PTR [-372+ebp]                     ;247.10
        inc       edx                                           ;247.10
        mov       edi, DWORD PTR [2584+ecx+esi]                 ;248.10
        mov       esi, DWORD PTR [-164+ebp]                     ;248.10
        mov       DWORD PTR [-372+ebp], edx                     ;247.10
        mov       DWORD PTR [-4+esi+edx*4], edi                 ;248.10
        mov       edx, DWORD PTR [-392+ebp]                     ;248.10
                                ; LOE eax edx ecx
.B1.456:                        ; Preds .B1.455 .B1.454         ; Infreq
        inc       eax                                           ;245.6
        add       ecx, 1800                                     ;245.6
        add       edx, 304                                      ;245.6
        cmp       eax, DWORD PTR [-376+ebp]                     ;245.6
        jb        .B1.452       ; Prob 64%                      ;245.6
                                ; LOE eax edx ecx
.B1.457:                        ; Preds .B1.456                 ; Infreq
        mov       ecx, DWORD PTR [-408+ebp]                     ;
        lea       edx, DWORD PTR [1+eax+eax]                    ;245.6
        mov       esi, DWORD PTR [-116+ebp]                     ;
        mov       edi, DWORD PTR [-264+ebp]                     ;
                                ; LOE edx ecx esi edi
.B1.458:                        ; Preds .B1.457 .B1.480         ; Infreq
        lea       eax, DWORD PTR [-1+edx]                       ;245.6
        cmp       esi, eax                                      ;245.6
        jbe       .B1.462       ; Prob 3%                       ;245.6
                                ; LOE edx ecx esi edi
.B1.459:                        ; Preds .B1.458                 ; Infreq
        neg       ecx                                           ;246.50
        add       ecx, edx                                      ;246.50
        imul      ecx, ecx, 152                                 ;246.50
        mov       eax, DWORD PTR [-400+ebp]                     ;246.12
        movsx     eax, WORD PTR [148+eax+ecx]                   ;246.12
        cmp       eax, 99                                       ;246.50
        jge       .B1.462       ; Prob 50%                      ;246.50
                                ; LOE edx esi edi
.B1.460:                        ; Preds .B1.459                 ; Infreq
        mov       ecx, DWORD PTR [-260+ebp]                     ;248.10
        neg       ecx                                           ;248.10
        add       ecx, edx                                      ;248.10
        imul      ecx, ecx, 900                                 ;248.10
        mov       edx, DWORD PTR [-396+ebp]                     ;248.10
        mov       eax, DWORD PTR [-372+ebp]                     ;247.10
        inc       eax                                           ;247.10
        mov       edx, DWORD PTR [784+edx+ecx]                  ;248.10
        mov       ecx, DWORD PTR [-164+ebp]                     ;248.10
        mov       DWORD PTR [-372+ebp], eax                     ;247.10
        mov       DWORD PTR [-4+ecx+eax*4], edx                 ;248.10
        jmp       .B1.462       ; Prob 100%                     ;248.10
                                ; LOE esi edi
.B1.461:                        ; Preds .B1.449                 ; Infreq
        xor       eax, eax                                      ;
        mov       DWORD PTR [-372+ebp], eax                     ;
                                ; LOE esi edi
.B1.462:                        ; Preds .B1.460 .B1.459 .B1.458 .B1.461 ; Infreq
        push      32                                            ;252.6
        xor       eax, eax                                      ;252.6
        lea       edx, DWORD PTR [-152+ebp]                     ;252.6
        push      eax                                           ;252.6
        push      OFFSET FLAT: __STRLITPACK_49.0.1              ;252.6
        push      -2088435968                                   ;252.6
        push      37                                            ;252.6
        push      edx                                           ;252.6
        mov       DWORD PTR [-152+ebp], eax                     ;252.6
        call      _for_write_seq_lis                            ;252.6
                                ; LOE esi edi
.B1.897:                        ; Preds .B1.462                 ; Infreq
        add       esp, 24                                       ;252.6
                                ; LOE esi edi
.B1.463:                        ; Preds .B1.897                 ; Infreq
        cvtsi2ss  xmm0, DWORD PTR [-308+ebp]                    ;253.23
        push      32                                            ;253.9
        push      OFFSET FLAT: CAL_LINK_MOE$format_pack.0.1+140 ;253.9
        mulss     xmm0, DWORD PTR [-300+ebp]                    ;253.9
        lea       eax, DWORD PTR [-288+ebp]                     ;253.9
        push      eax                                           ;253.9
        push      OFFSET FLAT: __STRLITPACK_50.0.1              ;253.9
        push      -2088435968                                   ;253.9
        push      37                                            ;253.9
        mov       DWORD PTR [-152+ebp], 0                       ;253.9
        lea       edx, DWORD PTR [-152+ebp]                     ;253.9
        push      edx                                           ;253.9
        movss     DWORD PTR [-288+ebp], xmm0                    ;253.9
        call      _for_write_seq_fmt                            ;253.9
                                ; LOE esi edi
.B1.898:                        ; Preds .B1.463                 ; Infreq
        add       esp, 28                                       ;253.9
                                ; LOE esi edi
.B1.464:                        ; Preds .B1.898                 ; Infreq
        push      32                                            ;254.9
        push      OFFSET FLAT: CAL_LINK_MOE$format_pack.0.1+60  ;254.9
        xor       eax, eax                                      ;254.9
        lea       edx, DWORD PTR [-152+ebp]                     ;254.9
        push      eax                                           ;254.9
        push      OFFSET FLAT: __STRLITPACK_51.0.1              ;254.9
        push      -2088435968                                   ;254.9
        push      37                                            ;254.9
        push      edx                                           ;254.9
        mov       DWORD PTR [-152+ebp], eax                     ;254.9
        call      _for_write_seq_fmt                            ;254.9
                                ; LOE esi edi
.B1.899:                        ; Preds .B1.464                 ; Infreq
        add       esp, 28                                       ;254.9
                                ; LOE esi edi
.B1.465:                        ; Preds .B1.899                 ; Infreq
        cmp       DWORD PTR [-372+ebp], 0                       ;254.9
        jle       .B1.470       ; Prob 2%                       ;254.9
                                ; LOE esi edi
.B1.466:                        ; Preds .B1.465                 ; Infreq
        cvtsi2ss  xmm0, DWORD PTR [-384+ebp]                    ;254.36
        mov       eax, 1                                        ;
        mov       DWORD PTR [-264+ebp], edi                     ;254.36
        movss     DWORD PTR [-408+ebp], xmm0                    ;254.36
        mov       DWORD PTR [-116+ebp], esi                     ;254.36
        mov       esi, eax                                      ;254.36
        mov       edi, DWORD PTR [-164+ebp]                     ;254.36
                                ; LOE esi edi
.B1.467:                        ; Preds .B1.468 .B1.466         ; Infreq
        movss     xmm0, DWORD PTR [-4+edi+esi*4]                ;254.25
        lea       edx, DWORD PTR [-56+ebp]                      ;254.9
        divss     xmm0, DWORD PTR [-408+ebp]                    ;254.9
        push      edx                                           ;254.9
        push      OFFSET FLAT: __STRLITPACK_52.0.1              ;254.9
        movss     DWORD PTR [-56+ebp], xmm0                     ;254.9
        lea       ecx, DWORD PTR [-152+ebp]                     ;254.9
        push      ecx                                           ;254.9
        call      _for_write_seq_fmt_xmit                       ;254.9
                                ; LOE esi edi
.B1.900:                        ; Preds .B1.467                 ; Infreq
        add       esp, 12                                       ;254.9
                                ; LOE esi edi
.B1.468:                        ; Preds .B1.900                 ; Infreq
        inc       esi                                           ;254.9
        cmp       esi, DWORD PTR [-372+ebp]                     ;254.9
        jle       .B1.467       ; Prob 82%                      ;254.9
                                ; LOE esi edi
.B1.469:                        ; Preds .B1.468                 ; Infreq
        mov       esi, DWORD PTR [-116+ebp]                     ;
        mov       edi, DWORD PTR [-264+ebp]                     ;
                                ; LOE esi edi
.B1.470:                        ; Preds .B1.465 .B1.469         ; Infreq
        push      0                                             ;254.9
        push      OFFSET FLAT: __STRLITPACK_53.0.1              ;254.9
        lea       eax, DWORD PTR [-152+ebp]                     ;254.9
        push      eax                                           ;254.9
        call      _for_write_seq_fmt_xmit                       ;254.9
                                ; LOE esi edi
.B1.901:                        ; Preds .B1.470                 ; Infreq
        add       esp, 12                                       ;254.9
                                ; LOE esi edi
.B1.471:                        ; Preds .B1.901                 ; Infreq
        push      32                                            ;255.3
        xor       eax, eax                                      ;255.3
        lea       edx, DWORD PTR [-152+ebp]                     ;255.3
        push      eax                                           ;255.3
        push      OFFSET FLAT: __STRLITPACK_54.0.1              ;255.3
        push      -2088435968                                   ;255.3
        push      37                                            ;255.3
        push      edx                                           ;255.3
        mov       DWORD PTR [-152+ebp], eax                     ;255.3
        call      _for_write_seq_lis                            ;255.3
                                ; LOE esi edi
.B1.902:                        ; Preds .B1.471                 ; Infreq
        add       esp, 24                                       ;255.3
                                ; LOE esi edi
.B1.472:                        ; Preds .B1.902                 ; Infreq
        cmp       DWORD PTR [-188+ebp], 0                       ;256.11
        jle       .B1.45        ; Prob 50%                      ;256.11
                                ; LOE esi edi
.B1.473:                        ; Preds .B1.472                 ; Infreq
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;256.11
        mov       eax, DWORD PTR [-188+ebp]                     ;256.11
        mov       DWORD PTR [-408+ebp], ecx                     ;256.11
        mov       ecx, eax                                      ;256.11
        shr       ecx, 31                                       ;256.11
        add       ecx, eax                                      ;256.11
        sar       ecx, 1                                        ;256.11
        test      ecx, ecx                                      ;256.11
        jbe       .B1.481       ; Prob 10%                      ;256.11
                                ; LOE ecx esi edi
.B1.474:                        ; Preds .B1.473                 ; Infreq
        xor       edx, edx                                      ;
        mov       DWORD PTR [-116+ebp], esi                     ;
        xor       eax, eax                                      ;
        mov       DWORD PTR [-264+ebp], edi                     ;
        xor       edi, edi                                      ;
        mov       esi, DWORD PTR [-408+ebp]                     ;
                                ; LOE eax edx ecx esi edi
.B1.475:                        ; Preds .B1.475 .B1.474         ; Infreq
        inc       edx                                           ;256.11
        mov       DWORD PTR [784+eax+esi], edi                  ;256.11
        mov       DWORD PTR [1684+eax+esi], edi                 ;256.11
        add       eax, 1800                                     ;256.11
        cmp       edx, ecx                                      ;256.11
        jb        .B1.475       ; Prob 64%                      ;256.11
                                ; LOE eax edx ecx esi edi
.B1.476:                        ; Preds .B1.475                 ; Infreq
        mov       esi, DWORD PTR [-116+ebp]                     ;
        lea       eax, DWORD PTR [1+edx+edx]                    ;256.11
        mov       edi, DWORD PTR [-264+ebp]                     ;
                                ; LOE eax esi edi
.B1.477:                        ; Preds .B1.476 .B1.481         ; Infreq
        lea       edx, DWORD PTR [-1+eax]                       ;256.11
        cmp       edx, DWORD PTR [-188+ebp]                     ;256.11
        jae       .B1.45        ; Prob 10%                      ;256.11
                                ; LOE eax esi edi
.B1.478:                        ; Preds .B1.477                 ; Infreq
        mov       edx, DWORD PTR [-260+ebp]                     ;256.11
        mov       ecx, edx                                      ;256.11
        neg       ecx                                           ;256.11
        add       eax, edx                                      ;256.11
        add       ecx, eax                                      ;256.11
        imul      edx, ecx, 900                                 ;256.11
        mov       eax, DWORD PTR [-408+ebp]                     ;256.11
        mov       DWORD PTR [-116+eax+edx], 0                   ;256.11
        jmp       .B1.45        ; Prob 100%                     ;256.11
                                ; LOE esi edi
.B1.479:                        ; Preds .B1.442                 ; Infreq
        mov       edx, 1                                        ;
        jmp       .B1.446       ; Prob 100%                     ;
                                ; LOE edx esi edi
.B1.480:                        ; Preds .B1.450                 ; Infreq
        mov       edx, 1                                        ;
        jmp       .B1.458       ; Prob 100%                     ;
                                ; LOE edx ecx esi edi
.B1.481:                        ; Preds .B1.473                 ; Infreq
        mov       eax, 1                                        ;
        jmp       .B1.477       ; Prob 100%                     ;
                                ; LOE eax esi edi
.B1.482:                        ; Preds .B1.40                  ; Infreq
        test      esi, esi                                      ;214.9
        jle       .B1.489       ; Prob 50%                      ;214.9
                                ; LOE esi edi
.B1.483:                        ; Preds .B1.482                 ; Infreq
        mov       edx, esi                                      ;214.9
        shr       edx, 31                                       ;214.9
        add       edx, esi                                      ;214.9
        sar       edx, 1                                        ;214.9
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;215.11
        test      edx, edx                                      ;214.9
        mov       DWORD PTR [-408+ebp], eax                     ;215.11
        jbe       .B1.520       ; Prob 3%                       ;214.9
                                ; LOE edx esi edi
.B1.484:                        ; Preds .B1.483                 ; Infreq
        imul      eax, DWORD PTR [-260+ebp], -900               ;
        xor       ecx, ecx                                      ;
        mov       DWORD PTR [-264+ebp], edi                     ;
        add       eax, DWORD PTR [-408+ebp]                     ;
        mov       edi, ecx                                      ;
        ALIGN     16
                                ; LOE eax edx ecx esi edi
.B1.485:                        ; Preds .B1.485 .B1.484         ; Infreq
        movss     xmm0, DWORD PTR [1680+edi+eax]                ;215.44
        inc       ecx                                           ;214.9
        movss     xmm1, DWORD PTR [2580+edi+eax]                ;215.44
        addss     xmm0, DWORD PTR [1556+edi+eax]                ;215.11
        addss     xmm1, DWORD PTR [2456+edi+eax]                ;215.11
        movss     DWORD PTR [1680+edi+eax], xmm0                ;215.11
        movss     DWORD PTR [2580+edi+eax], xmm1                ;215.11
        add       edi, 1800                                     ;214.9
        cmp       ecx, edx                                      ;214.9
        jb        .B1.485       ; Prob 64%                      ;214.9
                                ; LOE eax edx ecx esi edi
.B1.486:                        ; Preds .B1.485                 ; Infreq
        mov       edi, DWORD PTR [-264+ebp]                     ;
        lea       edx, DWORD PTR [1+ecx+ecx]                    ;214.9
                                ; LOE edx esi edi
.B1.487:                        ; Preds .B1.486 .B1.520         ; Infreq
        lea       eax, DWORD PTR [-1+edx]                       ;214.9
        cmp       esi, eax                                      ;214.9
        jbe       .B1.489       ; Prob 3%                       ;214.9
                                ; LOE edx esi edi
.B1.488:                        ; Preds .B1.487                 ; Infreq
        mov       eax, DWORD PTR [-260+ebp]                     ;215.11
        neg       eax                                           ;215.11
        add       eax, edx                                      ;215.11
        imul      ecx, eax, 900                                 ;215.11
        mov       edx, DWORD PTR [-408+ebp]                     ;215.44
        movss     xmm0, DWORD PTR [780+edx+ecx]                 ;215.44
        addss     xmm0, DWORD PTR [656+edx+ecx]                 ;215.11
        movss     DWORD PTR [780+edx+ecx], xmm0                 ;215.11
                                ; LOE esi edi
.B1.489:                        ; Preds .B1.482 .B1.487 .B1.488 ; Infreq
        mov       eax, DWORD PTR [-308+ebp]                     ;218.12
        cdq                                                     ;218.12
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_I36_T] ;218.9
        idiv      ecx                                           ;218.12
        mov       DWORD PTR [-384+ebp], ecx                     ;218.9
        test      edx, edx                                      ;218.25
        jne       .B1.41        ; Prob 50%                      ;218.25
                                ; LOE esi edi
.B1.490:                        ; Preds .B1.489                 ; Infreq
        test      esi, esi                                      ;220.6
        jle       .B1.502       ; Prob 50%                      ;220.6
                                ; LOE esi edi
.B1.491:                        ; Preds .B1.490                 ; Infreq
        mov       edx, esi                                      ;220.6
        shr       edx, 31                                       ;220.6
        add       edx, esi                                      ;220.6
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;231.11
        sar       edx, 1                                        ;220.6
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;221.8
        test      edx, edx                                      ;220.6
        mov       DWORD PTR [-396+ebp], ecx                     ;231.11
        mov       DWORD PTR [-400+ebp], eax                     ;221.8
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;221.8
        mov       DWORD PTR [-376+ebp], edx                     ;220.6
        mov       DWORD PTR [-372+ebp], 0                       ;
        jbe       .B1.521       ; Prob 3%                       ;220.6
                                ; LOE ecx esi edi
.B1.492:                        ; Preds .B1.491                 ; Infreq
        imul      edx, DWORD PTR [-260+ebp], -900               ;
        xor       eax, eax                                      ;
        add       edx, DWORD PTR [-396+ebp]                     ;
        mov       DWORD PTR [-388+ebp], edx                     ;
        imul      edx, ecx, -152                                ;
        add       edx, DWORD PTR [-400+ebp]                     ;
        mov       DWORD PTR [-404+ebp], eax                     ;
        mov       DWORD PTR [-392+ebp], eax                     ;
        mov       DWORD PTR [-380+ebp], edx                     ;
        mov       DWORD PTR [-408+ebp], ecx                     ;
        mov       DWORD PTR [-116+ebp], esi                     ;
        mov       DWORD PTR [-264+ebp], edi                     ;
        mov       edx, eax                                      ;
        mov       ecx, DWORD PTR [-404+ebp]                     ;
                                ; LOE eax edx ecx
.B1.493:                        ; Preds .B1.497 .B1.492         ; Infreq
        mov       esi, DWORD PTR [-380+ebp]                     ;221.12
        movsx     edi, WORD PTR [300+edx+esi]                   ;221.12
        cmp       edi, 99                                       ;221.50
        jge       .B1.495       ; Prob 50%                      ;221.50
                                ; LOE eax edx ecx
.B1.494:                        ; Preds .B1.493                 ; Infreq
        mov       esi, DWORD PTR [-388+ebp]                     ;223.10
        mov       DWORD PTR [-392+ebp], edx                     ;
        mov       edx, DWORD PTR [-372+ebp]                     ;222.10
        inc       edx                                           ;222.10
        mov       edi, DWORD PTR [1680+ecx+esi]                 ;223.10
        mov       esi, DWORD PTR [-164+ebp]                     ;223.10
        mov       DWORD PTR [-372+ebp], edx                     ;222.10
        mov       DWORD PTR [-4+esi+edx*4], edi                 ;223.10
        mov       edx, DWORD PTR [-392+ebp]                     ;223.10
                                ; LOE eax edx ecx
.B1.495:                        ; Preds .B1.494 .B1.493         ; Infreq
        mov       esi, DWORD PTR [-380+ebp]                     ;221.12
        movsx     edi, WORD PTR [452+edx+esi]                   ;221.12
        cmp       edi, 99                                       ;221.50
        jge       .B1.497       ; Prob 50%                      ;221.50
                                ; LOE eax edx ecx
.B1.496:                        ; Preds .B1.495                 ; Infreq
        mov       esi, DWORD PTR [-388+ebp]                     ;223.10
        mov       DWORD PTR [-392+ebp], edx                     ;
        mov       edx, DWORD PTR [-372+ebp]                     ;222.10
        inc       edx                                           ;222.10
        mov       edi, DWORD PTR [2580+ecx+esi]                 ;223.10
        mov       esi, DWORD PTR [-164+ebp]                     ;223.10
        mov       DWORD PTR [-372+ebp], edx                     ;222.10
        mov       DWORD PTR [-4+esi+edx*4], edi                 ;223.10
        mov       edx, DWORD PTR [-392+ebp]                     ;223.10
                                ; LOE eax edx ecx
.B1.497:                        ; Preds .B1.496 .B1.495         ; Infreq
        inc       eax                                           ;220.6
        add       ecx, 1800                                     ;220.6
        add       edx, 304                                      ;220.6
        cmp       eax, DWORD PTR [-376+ebp]                     ;220.6
        jb        .B1.493       ; Prob 64%                      ;220.6
                                ; LOE eax edx ecx
.B1.498:                        ; Preds .B1.497                 ; Infreq
        mov       ecx, DWORD PTR [-408+ebp]                     ;
        lea       edx, DWORD PTR [1+eax+eax]                    ;220.6
        mov       esi, DWORD PTR [-116+ebp]                     ;
        mov       edi, DWORD PTR [-264+ebp]                     ;
                                ; LOE edx ecx esi edi
.B1.499:                        ; Preds .B1.498 .B1.521         ; Infreq
        lea       eax, DWORD PTR [-1+edx]                       ;220.6
        cmp       esi, eax                                      ;220.6
        jbe       .B1.503       ; Prob 3%                       ;220.6
                                ; LOE edx ecx esi edi
.B1.500:                        ; Preds .B1.499                 ; Infreq
        neg       ecx                                           ;221.50
        add       ecx, edx                                      ;221.50
        imul      ecx, ecx, 152                                 ;221.50
        mov       eax, DWORD PTR [-400+ebp]                     ;221.12
        movsx     eax, WORD PTR [148+eax+ecx]                   ;221.12
        cmp       eax, 99                                       ;221.50
        jge       .B1.503       ; Prob 50%                      ;221.50
                                ; LOE edx esi edi
.B1.501:                        ; Preds .B1.500                 ; Infreq
        mov       ecx, DWORD PTR [-260+ebp]                     ;223.10
        neg       ecx                                           ;223.10
        add       ecx, edx                                      ;223.10
        imul      ecx, ecx, 900                                 ;223.10
        mov       edx, DWORD PTR [-396+ebp]                     ;223.10
        mov       eax, DWORD PTR [-372+ebp]                     ;222.10
        inc       eax                                           ;222.10
        mov       edx, DWORD PTR [780+edx+ecx]                  ;223.10
        mov       ecx, DWORD PTR [-164+ebp]                     ;223.10
        mov       DWORD PTR [-372+ebp], eax                     ;222.10
        mov       DWORD PTR [-4+ecx+eax*4], edx                 ;223.10
        jmp       .B1.503       ; Prob 100%                     ;223.10
                                ; LOE esi edi
.B1.502:                        ; Preds .B1.490                 ; Infreq
        xor       eax, eax                                      ;
        mov       DWORD PTR [-372+ebp], eax                     ;
                                ; LOE esi edi
.B1.503:                        ; Preds .B1.501 .B1.500 .B1.499 .B1.502 ; Infreq
        push      32                                            ;227.6
        xor       eax, eax                                      ;227.6
        lea       edx, DWORD PTR [-152+ebp]                     ;227.6
        push      eax                                           ;227.6
        push      OFFSET FLAT: __STRLITPACK_43.0.1              ;227.6
        push      -2088435968                                   ;227.6
        push      36                                            ;227.6
        push      edx                                           ;227.6
        mov       DWORD PTR [-152+ebp], eax                     ;227.6
        call      _for_write_seq_lis                            ;227.6
                                ; LOE esi edi
.B1.903:                        ; Preds .B1.503                 ; Infreq
        add       esp, 24                                       ;227.6
                                ; LOE esi edi
.B1.504:                        ; Preds .B1.903                 ; Infreq
        cvtsi2ss  xmm0, DWORD PTR [-308+ebp]                    ;228.25
        push      32                                            ;228.11
        push      OFFSET FLAT: CAL_LINK_MOE$format_pack.0.1+140 ;228.11
        mulss     xmm0, DWORD PTR [-300+ebp]                    ;228.11
        lea       eax, DWORD PTR [-296+ebp]                     ;228.11
        push      eax                                           ;228.11
        push      OFFSET FLAT: __STRLITPACK_44.0.1              ;228.11
        push      -2088435968                                   ;228.11
        push      36                                            ;228.11
        mov       DWORD PTR [-152+ebp], 0                       ;228.11
        lea       edx, DWORD PTR [-152+ebp]                     ;228.11
        push      edx                                           ;228.11
        movss     DWORD PTR [-296+ebp], xmm0                    ;228.11
        call      _for_write_seq_fmt                            ;228.11
                                ; LOE esi edi
.B1.904:                        ; Preds .B1.504                 ; Infreq
        add       esp, 28                                       ;228.11
                                ; LOE esi edi
.B1.505:                        ; Preds .B1.904                 ; Infreq
        push      32                                            ;229.3
        push      OFFSET FLAT: CAL_LINK_MOE$format_pack.0.1+80  ;229.3
        xor       eax, eax                                      ;229.3
        lea       edx, DWORD PTR [-152+ebp]                     ;229.3
        push      eax                                           ;229.3
        push      OFFSET FLAT: __STRLITPACK_45.0.1              ;229.3
        push      -2088435968                                   ;229.3
        push      36                                            ;229.3
        push      edx                                           ;229.3
        mov       DWORD PTR [-152+ebp], eax                     ;229.3
        call      _for_write_seq_fmt                            ;229.3
                                ; LOE esi edi
.B1.905:                        ; Preds .B1.505                 ; Infreq
        add       esp, 28                                       ;229.3
                                ; LOE esi edi
.B1.506:                        ; Preds .B1.905                 ; Infreq
        cmp       DWORD PTR [-372+ebp], 0                       ;229.3
        jle       .B1.511       ; Prob 2%                       ;229.3
                                ; LOE esi edi
.B1.507:                        ; Preds .B1.506                 ; Infreq
        cvtsi2ss  xmm0, DWORD PTR [-384+ebp]                    ;229.30
        mov       eax, 1                                        ;
        mov       DWORD PTR [-264+ebp], edi                     ;229.30
        movss     DWORD PTR [-408+ebp], xmm0                    ;229.30
        mov       DWORD PTR [-116+ebp], esi                     ;229.30
        mov       esi, eax                                      ;229.30
        mov       edi, DWORD PTR [-164+ebp]                     ;229.30
                                ; LOE esi edi
.B1.508:                        ; Preds .B1.509 .B1.507         ; Infreq
        movss     xmm0, DWORD PTR [-4+edi+esi*4]                ;229.19
        lea       edx, DWORD PTR [-64+ebp]                      ;229.3
        divss     xmm0, DWORD PTR [-408+ebp]                    ;229.3
        push      edx                                           ;229.3
        push      OFFSET FLAT: __STRLITPACK_46.0.1              ;229.3
        movss     DWORD PTR [-64+ebp], xmm0                     ;229.3
        lea       ecx, DWORD PTR [-152+ebp]                     ;229.3
        push      ecx                                           ;229.3
        call      _for_write_seq_fmt_xmit                       ;229.3
                                ; LOE esi edi
.B1.906:                        ; Preds .B1.508                 ; Infreq
        add       esp, 12                                       ;229.3
                                ; LOE esi edi
.B1.509:                        ; Preds .B1.906                 ; Infreq
        inc       esi                                           ;229.3
        cmp       esi, DWORD PTR [-372+ebp]                     ;229.3
        jle       .B1.508       ; Prob 82%                      ;229.3
                                ; LOE esi edi
.B1.510:                        ; Preds .B1.509                 ; Infreq
        mov       esi, DWORD PTR [-116+ebp]                     ;
        mov       edi, DWORD PTR [-264+ebp]                     ;
                                ; LOE esi edi
.B1.511:                        ; Preds .B1.506 .B1.510         ; Infreq
        push      0                                             ;229.3
        push      OFFSET FLAT: __STRLITPACK_47.0.1              ;229.3
        lea       eax, DWORD PTR [-152+ebp]                     ;229.3
        push      eax                                           ;229.3
        call      _for_write_seq_fmt_xmit                       ;229.3
                                ; LOE esi edi
.B1.907:                        ; Preds .B1.511                 ; Infreq
        add       esp, 12                                       ;229.3
                                ; LOE esi edi
.B1.512:                        ; Preds .B1.907                 ; Infreq
        push      32                                            ;230.11
        xor       eax, eax                                      ;230.11
        lea       edx, DWORD PTR [-152+ebp]                     ;230.11
        push      eax                                           ;230.11
        push      OFFSET FLAT: __STRLITPACK_48.0.1              ;230.11
        push      -2088435968                                   ;230.11
        push      36                                            ;230.11
        push      edx                                           ;230.11
        mov       DWORD PTR [-152+ebp], eax                     ;230.11
        call      _for_write_seq_lis                            ;230.11
                                ; LOE esi edi
.B1.908:                        ; Preds .B1.512                 ; Infreq
        add       esp, 24                                       ;230.11
                                ; LOE esi edi
.B1.513:                        ; Preds .B1.908                 ; Infreq
        cmp       DWORD PTR [-188+ebp], 0                       ;231.11
        jle       .B1.41        ; Prob 50%                      ;231.11
                                ; LOE esi edi
.B1.514:                        ; Preds .B1.513                 ; Infreq
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;231.11
        mov       eax, DWORD PTR [-188+ebp]                     ;231.11
        mov       DWORD PTR [-408+ebp], ecx                     ;231.11
        mov       ecx, eax                                      ;231.11
        shr       ecx, 31                                       ;231.11
        add       ecx, eax                                      ;231.11
        sar       ecx, 1                                        ;231.11
        test      ecx, ecx                                      ;231.11
        jbe       .B1.522       ; Prob 10%                      ;231.11
                                ; LOE ecx esi edi
.B1.515:                        ; Preds .B1.514                 ; Infreq
        xor       edx, edx                                      ;
        mov       DWORD PTR [-116+ebp], esi                     ;
        xor       eax, eax                                      ;
        mov       DWORD PTR [-264+ebp], edi                     ;
        xor       edi, edi                                      ;
        mov       esi, DWORD PTR [-408+ebp]                     ;
                                ; LOE eax edx ecx esi edi
.B1.516:                        ; Preds .B1.516 .B1.515         ; Infreq
        inc       edx                                           ;231.11
        mov       DWORD PTR [780+eax+esi], edi                  ;231.11
        mov       DWORD PTR [1680+eax+esi], edi                 ;231.11
        add       eax, 1800                                     ;231.11
        cmp       edx, ecx                                      ;231.11
        jb        .B1.516       ; Prob 64%                      ;231.11
                                ; LOE eax edx ecx esi edi
.B1.517:                        ; Preds .B1.516                 ; Infreq
        mov       esi, DWORD PTR [-116+ebp]                     ;
        lea       eax, DWORD PTR [1+edx+edx]                    ;231.11
        mov       edi, DWORD PTR [-264+ebp]                     ;
                                ; LOE eax esi edi
.B1.518:                        ; Preds .B1.517 .B1.522         ; Infreq
        lea       edx, DWORD PTR [-1+eax]                       ;231.11
        cmp       edx, DWORD PTR [-188+ebp]                     ;231.11
        jae       .B1.41        ; Prob 10%                      ;231.11
                                ; LOE eax esi edi
.B1.519:                        ; Preds .B1.518                 ; Infreq
        mov       edx, DWORD PTR [-260+ebp]                     ;231.11
        mov       ecx, edx                                      ;231.11
        neg       ecx                                           ;231.11
        add       eax, edx                                      ;231.11
        add       ecx, eax                                      ;231.11
        imul      edx, ecx, 900                                 ;231.11
        mov       eax, DWORD PTR [-408+ebp]                     ;231.11
        mov       DWORD PTR [-120+eax+edx], 0                   ;231.11
        jmp       .B1.41        ; Prob 100%                     ;231.11
                                ; LOE esi edi
.B1.520:                        ; Preds .B1.483                 ; Infreq
        mov       edx, 1                                        ;
        jmp       .B1.487       ; Prob 100%                     ;
                                ; LOE edx esi edi
.B1.521:                        ; Preds .B1.491                 ; Infreq
        mov       edx, 1                                        ;
        jmp       .B1.499       ; Prob 100%                     ;
                                ; LOE edx ecx esi edi
.B1.522:                        ; Preds .B1.514                 ; Infreq
        mov       eax, 1                                        ;
        jmp       .B1.518       ; Prob 100%                     ;
                                ; LOE eax esi edi
.B1.523:                        ; Preds .B1.36                  ; Infreq
        test      esi, esi                                      ;189.9
        jle       .B1.530       ; Prob 50%                      ;189.9
                                ; LOE esi edi
.B1.524:                        ; Preds .B1.523                 ; Infreq
        mov       edx, esi                                      ;189.9
        shr       edx, 31                                       ;189.9
        add       edx, esi                                      ;189.9
        sar       edx, 1                                        ;189.9
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;190.11
        test      edx, edx                                      ;189.9
        mov       DWORD PTR [-408+ebp], eax                     ;190.11
        jbe       .B1.561       ; Prob 3%                       ;189.9
                                ; LOE edx esi edi
.B1.525:                        ; Preds .B1.524                 ; Infreq
        imul      eax, DWORD PTR [-260+ebp], -900               ;
        xor       ecx, ecx                                      ;
        mov       DWORD PTR [-264+ebp], edi                     ;
        add       eax, DWORD PTR [-408+ebp]                     ;
        mov       edi, ecx                                      ;
        ALIGN     16
                                ; LOE eax edx ecx esi edi
.B1.526:                        ; Preds .B1.526 .B1.525         ; Infreq
        movss     xmm0, DWORD PTR [1676+edi+eax]                ;190.44
        inc       ecx                                           ;189.9
        movss     xmm1, DWORD PTR [2576+edi+eax]                ;190.44
        addss     xmm0, DWORD PTR [1560+edi+eax]                ;190.11
        addss     xmm1, DWORD PTR [2460+edi+eax]                ;190.11
        movss     DWORD PTR [1676+edi+eax], xmm0                ;190.11
        movss     DWORD PTR [2576+edi+eax], xmm1                ;190.11
        add       edi, 1800                                     ;189.9
        cmp       ecx, edx                                      ;189.9
        jb        .B1.526       ; Prob 64%                      ;189.9
                                ; LOE eax edx ecx esi edi
.B1.527:                        ; Preds .B1.526                 ; Infreq
        mov       edi, DWORD PTR [-264+ebp]                     ;
        lea       edx, DWORD PTR [1+ecx+ecx]                    ;189.9
                                ; LOE edx esi edi
.B1.528:                        ; Preds .B1.527 .B1.561         ; Infreq
        lea       eax, DWORD PTR [-1+edx]                       ;189.9
        cmp       esi, eax                                      ;189.9
        jbe       .B1.530       ; Prob 3%                       ;189.9
                                ; LOE edx esi edi
.B1.529:                        ; Preds .B1.528                 ; Infreq
        mov       eax, DWORD PTR [-260+ebp]                     ;190.11
        neg       eax                                           ;190.11
        add       eax, edx                                      ;190.11
        imul      ecx, eax, 900                                 ;190.11
        mov       edx, DWORD PTR [-408+ebp]                     ;190.44
        movss     xmm0, DWORD PTR [776+edx+ecx]                 ;190.44
        addss     xmm0, DWORD PTR [660+edx+ecx]                 ;190.11
        movss     DWORD PTR [776+edx+ecx], xmm0                 ;190.11
                                ; LOE esi edi
.B1.530:                        ; Preds .B1.523 .B1.528 .B1.529 ; Infreq
        mov       eax, DWORD PTR [-308+ebp]                     ;193.12
        cdq                                                     ;193.12
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_I35_T] ;193.9
        idiv      ecx                                           ;193.12
        mov       DWORD PTR [-384+ebp], ecx                     ;193.9
        test      edx, edx                                      ;193.25
        jne       .B1.37        ; Prob 50%                      ;193.25
                                ; LOE esi edi
.B1.531:                        ; Preds .B1.530                 ; Infreq
        test      esi, esi                                      ;195.6
        jle       .B1.543       ; Prob 50%                      ;195.6
                                ; LOE esi edi
.B1.532:                        ; Preds .B1.531                 ; Infreq
        mov       edx, esi                                      ;195.6
        shr       edx, 31                                       ;195.6
        add       edx, esi                                      ;195.6
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;206.11
        sar       edx, 1                                        ;195.6
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;196.8
        test      edx, edx                                      ;195.6
        mov       DWORD PTR [-396+ebp], ecx                     ;206.11
        mov       DWORD PTR [-400+ebp], eax                     ;196.8
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;196.8
        mov       DWORD PTR [-376+ebp], edx                     ;195.6
        mov       DWORD PTR [-372+ebp], 0                       ;
        jbe       .B1.562       ; Prob 3%                       ;195.6
                                ; LOE ecx esi edi
.B1.533:                        ; Preds .B1.532                 ; Infreq
        imul      edx, DWORD PTR [-260+ebp], -900               ;
        xor       eax, eax                                      ;
        add       edx, DWORD PTR [-396+ebp]                     ;
        mov       DWORD PTR [-388+ebp], edx                     ;
        imul      edx, ecx, -152                                ;
        add       edx, DWORD PTR [-400+ebp]                     ;
        mov       DWORD PTR [-404+ebp], eax                     ;
        mov       DWORD PTR [-392+ebp], eax                     ;
        mov       DWORD PTR [-380+ebp], edx                     ;
        mov       DWORD PTR [-408+ebp], ecx                     ;
        mov       DWORD PTR [-116+ebp], esi                     ;
        mov       DWORD PTR [-264+ebp], edi                     ;
        mov       edx, eax                                      ;
        mov       ecx, DWORD PTR [-404+ebp]                     ;
                                ; LOE eax edx ecx
.B1.534:                        ; Preds .B1.538 .B1.533         ; Infreq
        mov       esi, DWORD PTR [-380+ebp]                     ;196.12
        movsx     edi, WORD PTR [300+edx+esi]                   ;196.12
        cmp       edi, 99                                       ;196.50
        jge       .B1.536       ; Prob 50%                      ;196.50
                                ; LOE eax edx ecx
.B1.535:                        ; Preds .B1.534                 ; Infreq
        mov       esi, DWORD PTR [-388+ebp]                     ;198.10
        mov       DWORD PTR [-392+ebp], edx                     ;
        mov       edx, DWORD PTR [-372+ebp]                     ;197.10
        inc       edx                                           ;197.10
        mov       edi, DWORD PTR [1676+ecx+esi]                 ;198.10
        mov       esi, DWORD PTR [-164+ebp]                     ;198.10
        mov       DWORD PTR [-372+ebp], edx                     ;197.10
        mov       DWORD PTR [-4+esi+edx*4], edi                 ;198.10
        mov       edx, DWORD PTR [-392+ebp]                     ;198.10
                                ; LOE eax edx ecx
.B1.536:                        ; Preds .B1.535 .B1.534         ; Infreq
        mov       esi, DWORD PTR [-380+ebp]                     ;196.12
        movsx     edi, WORD PTR [452+edx+esi]                   ;196.12
        cmp       edi, 99                                       ;196.50
        jge       .B1.538       ; Prob 50%                      ;196.50
                                ; LOE eax edx ecx
.B1.537:                        ; Preds .B1.536                 ; Infreq
        mov       esi, DWORD PTR [-388+ebp]                     ;198.10
        mov       DWORD PTR [-392+ebp], edx                     ;
        mov       edx, DWORD PTR [-372+ebp]                     ;197.10
        inc       edx                                           ;197.10
        mov       edi, DWORD PTR [2576+ecx+esi]                 ;198.10
        mov       esi, DWORD PTR [-164+ebp]                     ;198.10
        mov       DWORD PTR [-372+ebp], edx                     ;197.10
        mov       DWORD PTR [-4+esi+edx*4], edi                 ;198.10
        mov       edx, DWORD PTR [-392+ebp]                     ;198.10
                                ; LOE eax edx ecx
.B1.538:                        ; Preds .B1.537 .B1.536         ; Infreq
        inc       eax                                           ;195.6
        add       ecx, 1800                                     ;195.6
        add       edx, 304                                      ;195.6
        cmp       eax, DWORD PTR [-376+ebp]                     ;195.6
        jb        .B1.534       ; Prob 64%                      ;195.6
                                ; LOE eax edx ecx
.B1.539:                        ; Preds .B1.538                 ; Infreq
        mov       ecx, DWORD PTR [-408+ebp]                     ;
        lea       edx, DWORD PTR [1+eax+eax]                    ;195.6
        mov       esi, DWORD PTR [-116+ebp]                     ;
        mov       edi, DWORD PTR [-264+ebp]                     ;
                                ; LOE edx ecx esi edi
.B1.540:                        ; Preds .B1.539 .B1.562         ; Infreq
        lea       eax, DWORD PTR [-1+edx]                       ;195.6
        cmp       esi, eax                                      ;195.6
        jbe       .B1.544       ; Prob 3%                       ;195.6
                                ; LOE edx ecx esi edi
.B1.541:                        ; Preds .B1.540                 ; Infreq
        neg       ecx                                           ;196.50
        add       ecx, edx                                      ;196.50
        imul      ecx, ecx, 152                                 ;196.50
        mov       eax, DWORD PTR [-400+ebp]                     ;196.12
        movsx     eax, WORD PTR [148+eax+ecx]                   ;196.12
        cmp       eax, 99                                       ;196.50
        jge       .B1.544       ; Prob 50%                      ;196.50
                                ; LOE edx esi edi
.B1.542:                        ; Preds .B1.541                 ; Infreq
        mov       ecx, DWORD PTR [-260+ebp]                     ;198.10
        neg       ecx                                           ;198.10
        add       ecx, edx                                      ;198.10
        imul      ecx, ecx, 900                                 ;198.10
        mov       edx, DWORD PTR [-396+ebp]                     ;198.10
        mov       eax, DWORD PTR [-372+ebp]                     ;197.10
        inc       eax                                           ;197.10
        mov       edx, DWORD PTR [776+edx+ecx]                  ;198.10
        mov       ecx, DWORD PTR [-164+ebp]                     ;198.10
        mov       DWORD PTR [-372+ebp], eax                     ;197.10
        mov       DWORD PTR [-4+ecx+eax*4], edx                 ;198.10
        jmp       .B1.544       ; Prob 100%                     ;198.10
                                ; LOE esi edi
.B1.543:                        ; Preds .B1.531                 ; Infreq
        xor       eax, eax                                      ;
        mov       DWORD PTR [-372+ebp], eax                     ;
                                ; LOE esi edi
.B1.544:                        ; Preds .B1.542 .B1.541 .B1.540 .B1.543 ; Infreq
        push      32                                            ;202.6
        xor       eax, eax                                      ;202.6
        lea       edx, DWORD PTR [-152+ebp]                     ;202.6
        push      eax                                           ;202.6
        push      OFFSET FLAT: __STRLITPACK_37.0.1              ;202.6
        push      -2088435968                                   ;202.6
        push      35                                            ;202.6
        push      edx                                           ;202.6
        mov       DWORD PTR [-152+ebp], eax                     ;202.6
        call      _for_write_seq_lis                            ;202.6
                                ; LOE esi edi
.B1.909:                        ; Preds .B1.544                 ; Infreq
        add       esp, 24                                       ;202.6
                                ; LOE esi edi
.B1.545:                        ; Preds .B1.909                 ; Infreq
        cvtsi2ss  xmm0, DWORD PTR [-308+ebp]                    ;203.25
        push      32                                            ;203.11
        push      OFFSET FLAT: CAL_LINK_MOE$format_pack.0.1+140 ;203.11
        mulss     xmm0, DWORD PTR [-300+ebp]                    ;203.11
        lea       eax, DWORD PTR [-304+ebp]                     ;203.11
        push      eax                                           ;203.11
        push      OFFSET FLAT: __STRLITPACK_38.0.1              ;203.11
        push      -2088435968                                   ;203.11
        push      35                                            ;203.11
        mov       DWORD PTR [-152+ebp], 0                       ;203.11
        lea       edx, DWORD PTR [-152+ebp]                     ;203.11
        push      edx                                           ;203.11
        movss     DWORD PTR [-304+ebp], xmm0                    ;203.11
        call      _for_write_seq_fmt                            ;203.11
                                ; LOE esi edi
.B1.910:                        ; Preds .B1.545                 ; Infreq
        add       esp, 28                                       ;203.11
                                ; LOE esi edi
.B1.546:                        ; Preds .B1.910                 ; Infreq
        push      32                                            ;204.10
        push      OFFSET FLAT: CAL_LINK_MOE$format_pack.0.1+100 ;204.10
        xor       eax, eax                                      ;204.10
        lea       edx, DWORD PTR [-152+ebp]                     ;204.10
        push      eax                                           ;204.10
        push      OFFSET FLAT: __STRLITPACK_39.0.1              ;204.10
        push      -2088435968                                   ;204.10
        push      35                                            ;204.10
        push      edx                                           ;204.10
        mov       DWORD PTR [-152+ebp], eax                     ;204.10
        call      _for_write_seq_fmt                            ;204.10
                                ; LOE esi edi
.B1.911:                        ; Preds .B1.546                 ; Infreq
        add       esp, 28                                       ;204.10
                                ; LOE esi edi
.B1.547:                        ; Preds .B1.911                 ; Infreq
        cmp       DWORD PTR [-372+ebp], 0                       ;204.10
        jle       .B1.552       ; Prob 2%                       ;204.10
                                ; LOE esi edi
.B1.548:                        ; Preds .B1.547                 ; Infreq
        cvtsi2ss  xmm0, DWORD PTR [-384+ebp]                    ;204.37
        mov       eax, 1                                        ;
        mov       DWORD PTR [-264+ebp], edi                     ;204.37
        movss     xmm1, DWORD PTR [_2il0floatpacket.10]         ;204.42
        movss     DWORD PTR [-408+ebp], xmm0                    ;204.37
        mov       DWORD PTR [-116+ebp], esi                     ;204.37
        mov       esi, eax                                      ;204.37
        mov       edi, DWORD PTR [-164+ebp]                     ;204.37
                                ; LOE esi edi
.B1.549:                        ; Preds .B1.550 .B1.548         ; Infreq
        movss     xmm0, DWORD PTR [-4+edi+esi*4]                ;204.26
        lea       edx, DWORD PTR [-72+ebp]                      ;204.10
        divss     xmm0, DWORD PTR [-408+ebp]                    ;204.36
        mulss     xmm0, DWORD PTR [_2il0floatpacket.10]         ;204.10
        lea       ecx, DWORD PTR [-152+ebp]                     ;204.10
        push      edx                                           ;204.10
        push      OFFSET FLAT: __STRLITPACK_40.0.1              ;204.10
        push      ecx                                           ;204.10
        movss     DWORD PTR [-72+ebp], xmm0                     ;204.10
        call      _for_write_seq_fmt_xmit                       ;204.10
                                ; LOE esi edi
.B1.912:                        ; Preds .B1.549                 ; Infreq
        add       esp, 12                                       ;204.10
                                ; LOE esi edi
.B1.550:                        ; Preds .B1.912                 ; Infreq
        inc       esi                                           ;204.10
        cmp       esi, DWORD PTR [-372+ebp]                     ;204.10
        jle       .B1.549       ; Prob 82%                      ;204.10
                                ; LOE esi edi
.B1.551:                        ; Preds .B1.550                 ; Infreq
        mov       esi, DWORD PTR [-116+ebp]                     ;
        mov       edi, DWORD PTR [-264+ebp]                     ;
                                ; LOE esi edi
.B1.552:                        ; Preds .B1.547 .B1.551         ; Infreq
        push      0                                             ;204.10
        push      OFFSET FLAT: __STRLITPACK_41.0.1              ;204.10
        lea       eax, DWORD PTR [-152+ebp]                     ;204.10
        push      eax                                           ;204.10
        call      _for_write_seq_fmt_xmit                       ;204.10
                                ; LOE esi edi
.B1.913:                        ; Preds .B1.552                 ; Infreq
        add       esp, 12                                       ;204.10
                                ; LOE esi edi
.B1.553:                        ; Preds .B1.913                 ; Infreq
        push      32                                            ;205.6
        xor       eax, eax                                      ;205.6
        lea       edx, DWORD PTR [-152+ebp]                     ;205.6
        push      eax                                           ;205.6
        push      OFFSET FLAT: __STRLITPACK_42.0.1              ;205.6
        push      -2088435968                                   ;205.6
        push      35                                            ;205.6
        push      edx                                           ;205.6
        mov       DWORD PTR [-152+ebp], eax                     ;205.6
        call      _for_write_seq_lis                            ;205.6
                                ; LOE esi edi
.B1.914:                        ; Preds .B1.553                 ; Infreq
        add       esp, 24                                       ;205.6
                                ; LOE esi edi
.B1.554:                        ; Preds .B1.914                 ; Infreq
        cmp       DWORD PTR [-188+ebp], 0                       ;206.11
        jle       .B1.37        ; Prob 50%                      ;206.11
                                ; LOE esi edi
.B1.555:                        ; Preds .B1.554                 ; Infreq
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;206.11
        mov       eax, DWORD PTR [-188+ebp]                     ;206.11
        mov       DWORD PTR [-408+ebp], ecx                     ;206.11
        mov       ecx, eax                                      ;206.11
        shr       ecx, 31                                       ;206.11
        add       ecx, eax                                      ;206.11
        sar       ecx, 1                                        ;206.11
        test      ecx, ecx                                      ;206.11
        jbe       .B1.563       ; Prob 10%                      ;206.11
                                ; LOE ecx esi edi
.B1.556:                        ; Preds .B1.555                 ; Infreq
        xor       edx, edx                                      ;
        mov       DWORD PTR [-116+ebp], esi                     ;
        xor       eax, eax                                      ;
        mov       DWORD PTR [-264+ebp], edi                     ;
        xor       edi, edi                                      ;
        mov       esi, DWORD PTR [-408+ebp]                     ;
                                ; LOE eax edx ecx esi edi
.B1.557:                        ; Preds .B1.557 .B1.556         ; Infreq
        inc       edx                                           ;206.11
        mov       DWORD PTR [776+eax+esi], edi                  ;206.11
        mov       DWORD PTR [1676+eax+esi], edi                 ;206.11
        add       eax, 1800                                     ;206.11
        cmp       edx, ecx                                      ;206.11
        jb        .B1.557       ; Prob 64%                      ;206.11
                                ; LOE eax edx ecx esi edi
.B1.558:                        ; Preds .B1.557                 ; Infreq
        mov       esi, DWORD PTR [-116+ebp]                     ;
        lea       eax, DWORD PTR [1+edx+edx]                    ;206.11
        mov       edi, DWORD PTR [-264+ebp]                     ;
                                ; LOE eax esi edi
.B1.559:                        ; Preds .B1.558 .B1.563         ; Infreq
        lea       edx, DWORD PTR [-1+eax]                       ;206.11
        cmp       edx, DWORD PTR [-188+ebp]                     ;206.11
        jae       .B1.37        ; Prob 10%                      ;206.11
                                ; LOE eax esi edi
.B1.560:                        ; Preds .B1.559                 ; Infreq
        mov       edx, DWORD PTR [-260+ebp]                     ;206.11
        mov       ecx, edx                                      ;206.11
        neg       ecx                                           ;206.11
        add       eax, edx                                      ;206.11
        add       ecx, eax                                      ;206.11
        imul      edx, ecx, 900                                 ;206.11
        mov       eax, DWORD PTR [-408+ebp]                     ;206.11
        mov       DWORD PTR [-124+eax+edx], 0                   ;206.11
        jmp       .B1.37        ; Prob 100%                     ;206.11
                                ; LOE esi edi
.B1.561:                        ; Preds .B1.524                 ; Infreq
        mov       edx, 1                                        ;
        jmp       .B1.528       ; Prob 100%                     ;
                                ; LOE edx esi edi
.B1.562:                        ; Preds .B1.532                 ; Infreq
        mov       edx, 1                                        ;
        jmp       .B1.540       ; Prob 100%                     ;
                                ; LOE edx ecx esi edi
.B1.563:                        ; Preds .B1.555                 ; Infreq
        mov       eax, 1                                        ;
        jmp       .B1.559       ; Prob 100%                     ;
                                ; LOE eax esi edi
.B1.564:                        ; Preds .B1.32                  ; Infreq
        test      esi, esi                                      ;164.9
        jle       .B1.571       ; Prob 50%                      ;164.9
                                ; LOE esi edi
.B1.565:                        ; Preds .B1.564                 ; Infreq
        mov       edx, esi                                      ;164.9
        shr       edx, 31                                       ;164.9
        add       edx, esi                                      ;164.9
        sar       edx, 1                                        ;164.9
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;165.10
        test      edx, edx                                      ;164.9
        mov       DWORD PTR [-408+ebp], eax                     ;165.10
        jbe       .B1.602       ; Prob 3%                       ;164.9
                                ; LOE edx esi edi
.B1.566:                        ; Preds .B1.565                 ; Infreq
        imul      eax, DWORD PTR [-260+ebp], -900               ;
        xor       ecx, ecx                                      ;
        mov       DWORD PTR [-264+ebp], edi                     ;
        add       eax, DWORD PTR [-408+ebp]                     ;
        mov       edi, ecx                                      ;
        ALIGN     16
                                ; LOE eax edx ecx esi edi
.B1.567:                        ; Preds .B1.567 .B1.566         ; Infreq
        movss     xmm0, DWORD PTR [1672+edi+eax]                ;165.43
        inc       ecx                                           ;164.9
        movss     xmm1, DWORD PTR [2572+edi+eax]                ;165.43
        addss     xmm0, DWORD PTR [1548+edi+eax]                ;165.10
        addss     xmm1, DWORD PTR [2448+edi+eax]                ;165.10
        movss     DWORD PTR [1672+edi+eax], xmm0                ;165.10
        movss     DWORD PTR [2572+edi+eax], xmm1                ;165.10
        add       edi, 1800                                     ;164.9
        cmp       ecx, edx                                      ;164.9
        jb        .B1.567       ; Prob 64%                      ;164.9
                                ; LOE eax edx ecx esi edi
.B1.568:                        ; Preds .B1.567                 ; Infreq
        mov       edi, DWORD PTR [-264+ebp]                     ;
        lea       edx, DWORD PTR [1+ecx+ecx]                    ;164.9
                                ; LOE edx esi edi
.B1.569:                        ; Preds .B1.568 .B1.602         ; Infreq
        lea       eax, DWORD PTR [-1+edx]                       ;164.9
        cmp       esi, eax                                      ;164.9
        jbe       .B1.571       ; Prob 3%                       ;164.9
                                ; LOE edx esi edi
.B1.570:                        ; Preds .B1.569                 ; Infreq
        mov       eax, DWORD PTR [-260+ebp]                     ;165.10
        neg       eax                                           ;165.10
        add       eax, edx                                      ;165.10
        imul      ecx, eax, 900                                 ;165.10
        mov       edx, DWORD PTR [-408+ebp]                     ;165.43
        movss     xmm0, DWORD PTR [772+edx+ecx]                 ;165.43
        addss     xmm0, DWORD PTR [648+edx+ecx]                 ;165.10
        movss     DWORD PTR [772+edx+ecx], xmm0                 ;165.10
                                ; LOE esi edi
.B1.571:                        ; Preds .B1.564 .B1.569 .B1.570 ; Infreq
        mov       eax, DWORD PTR [-308+ebp]                     ;168.12
        cdq                                                     ;168.12
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_I34_T] ;168.9
        idiv      ecx                                           ;168.12
        mov       DWORD PTR [-384+ebp], ecx                     ;168.9
        test      edx, edx                                      ;168.25
        jne       .B1.33        ; Prob 50%                      ;168.25
                                ; LOE esi edi
.B1.572:                        ; Preds .B1.571                 ; Infreq
        test      esi, esi                                      ;170.6
        jle       .B1.584       ; Prob 50%                      ;170.6
                                ; LOE esi edi
.B1.573:                        ; Preds .B1.572                 ; Infreq
        mov       edx, esi                                      ;170.6
        shr       edx, 31                                       ;170.6
        add       edx, esi                                      ;170.6
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;181.11
        sar       edx, 1                                        ;170.6
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;171.8
        test      edx, edx                                      ;170.6
        mov       DWORD PTR [-396+ebp], ecx                     ;181.11
        mov       DWORD PTR [-400+ebp], eax                     ;171.8
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;171.8
        mov       DWORD PTR [-376+ebp], edx                     ;170.6
        mov       DWORD PTR [-372+ebp], 0                       ;
        jbe       .B1.603       ; Prob 3%                       ;170.6
                                ; LOE ecx esi edi
.B1.574:                        ; Preds .B1.573                 ; Infreq
        imul      edx, DWORD PTR [-260+ebp], -900               ;
        xor       eax, eax                                      ;
        add       edx, DWORD PTR [-396+ebp]                     ;
        mov       DWORD PTR [-388+ebp], edx                     ;
        imul      edx, ecx, -152                                ;
        add       edx, DWORD PTR [-400+ebp]                     ;
        mov       DWORD PTR [-404+ebp], eax                     ;
        mov       DWORD PTR [-392+ebp], eax                     ;
        mov       DWORD PTR [-380+ebp], edx                     ;
        mov       DWORD PTR [-408+ebp], ecx                     ;
        mov       DWORD PTR [-116+ebp], esi                     ;
        mov       DWORD PTR [-264+ebp], edi                     ;
        mov       edx, eax                                      ;
        mov       ecx, DWORD PTR [-404+ebp]                     ;
                                ; LOE eax edx ecx
.B1.575:                        ; Preds .B1.579 .B1.574         ; Infreq
        mov       esi, DWORD PTR [-380+ebp]                     ;171.12
        movsx     edi, WORD PTR [300+edx+esi]                   ;171.12
        cmp       edi, 99                                       ;171.50
        jge       .B1.577       ; Prob 50%                      ;171.50
                                ; LOE eax edx ecx
.B1.576:                        ; Preds .B1.575                 ; Infreq
        mov       esi, DWORD PTR [-388+ebp]                     ;173.10
        mov       DWORD PTR [-392+ebp], edx                     ;
        mov       edx, DWORD PTR [-372+ebp]                     ;172.10
        inc       edx                                           ;172.10
        mov       edi, DWORD PTR [1672+ecx+esi]                 ;173.10
        mov       esi, DWORD PTR [-164+ebp]                     ;173.10
        mov       DWORD PTR [-372+ebp], edx                     ;172.10
        mov       DWORD PTR [-4+esi+edx*4], edi                 ;173.10
        mov       edx, DWORD PTR [-392+ebp]                     ;173.10
                                ; LOE eax edx ecx
.B1.577:                        ; Preds .B1.576 .B1.575         ; Infreq
        mov       esi, DWORD PTR [-380+ebp]                     ;171.12
        movsx     edi, WORD PTR [452+edx+esi]                   ;171.12
        cmp       edi, 99                                       ;171.50
        jge       .B1.579       ; Prob 50%                      ;171.50
                                ; LOE eax edx ecx
.B1.578:                        ; Preds .B1.577                 ; Infreq
        mov       esi, DWORD PTR [-388+ebp]                     ;173.10
        mov       DWORD PTR [-392+ebp], edx                     ;
        mov       edx, DWORD PTR [-372+ebp]                     ;172.10
        inc       edx                                           ;172.10
        mov       edi, DWORD PTR [2572+ecx+esi]                 ;173.10
        mov       esi, DWORD PTR [-164+ebp]                     ;173.10
        mov       DWORD PTR [-372+ebp], edx                     ;172.10
        mov       DWORD PTR [-4+esi+edx*4], edi                 ;173.10
        mov       edx, DWORD PTR [-392+ebp]                     ;173.10
                                ; LOE eax edx ecx
.B1.579:                        ; Preds .B1.578 .B1.577         ; Infreq
        inc       eax                                           ;170.6
        add       ecx, 1800                                     ;170.6
        add       edx, 304                                      ;170.6
        cmp       eax, DWORD PTR [-376+ebp]                     ;170.6
        jb        .B1.575       ; Prob 64%                      ;170.6
                                ; LOE eax edx ecx
.B1.580:                        ; Preds .B1.579                 ; Infreq
        mov       ecx, DWORD PTR [-408+ebp]                     ;
        lea       edx, DWORD PTR [1+eax+eax]                    ;170.6
        mov       esi, DWORD PTR [-116+ebp]                     ;
        mov       edi, DWORD PTR [-264+ebp]                     ;
                                ; LOE edx ecx esi edi
.B1.581:                        ; Preds .B1.580 .B1.603         ; Infreq
        lea       eax, DWORD PTR [-1+edx]                       ;170.6
        cmp       esi, eax                                      ;170.6
        jbe       .B1.585       ; Prob 3%                       ;170.6
                                ; LOE edx ecx esi edi
.B1.582:                        ; Preds .B1.581                 ; Infreq
        neg       ecx                                           ;171.50
        add       ecx, edx                                      ;171.50
        imul      ecx, ecx, 152                                 ;171.50
        mov       eax, DWORD PTR [-400+ebp]                     ;171.12
        movsx     eax, WORD PTR [148+eax+ecx]                   ;171.12
        cmp       eax, 99                                       ;171.50
        jge       .B1.585       ; Prob 50%                      ;171.50
                                ; LOE edx esi edi
.B1.583:                        ; Preds .B1.582                 ; Infreq
        mov       ecx, DWORD PTR [-260+ebp]                     ;173.10
        neg       ecx                                           ;173.10
        add       ecx, edx                                      ;173.10
        imul      ecx, ecx, 900                                 ;173.10
        mov       edx, DWORD PTR [-396+ebp]                     ;173.10
        mov       eax, DWORD PTR [-372+ebp]                     ;172.10
        inc       eax                                           ;172.10
        mov       edx, DWORD PTR [772+edx+ecx]                  ;173.10
        mov       ecx, DWORD PTR [-164+ebp]                     ;173.10
        mov       DWORD PTR [-372+ebp], eax                     ;172.10
        mov       DWORD PTR [-4+ecx+eax*4], edx                 ;173.10
        jmp       .B1.585       ; Prob 100%                     ;173.10
                                ; LOE esi edi
.B1.584:                        ; Preds .B1.572                 ; Infreq
        xor       eax, eax                                      ;
        mov       DWORD PTR [-372+ebp], eax                     ;
                                ; LOE esi edi
.B1.585:                        ; Preds .B1.583 .B1.582 .B1.581 .B1.584 ; Infreq
        push      32                                            ;177.6
        xor       eax, eax                                      ;177.6
        lea       edx, DWORD PTR [-152+ebp]                     ;177.6
        push      eax                                           ;177.6
        push      OFFSET FLAT: __STRLITPACK_31.0.1              ;177.6
        push      -2088435968                                   ;177.6
        push      34                                            ;177.6
        push      edx                                           ;177.6
        mov       DWORD PTR [-152+ebp], eax                     ;177.6
        call      _for_write_seq_lis                            ;177.6
                                ; LOE esi edi
.B1.915:                        ; Preds .B1.585                 ; Infreq
        add       esp, 24                                       ;177.6
                                ; LOE esi edi
.B1.586:                        ; Preds .B1.915                 ; Infreq
        cvtsi2ss  xmm0, DWORD PTR [-308+ebp]                    ;178.17
        push      32                                            ;178.3
        push      OFFSET FLAT: CAL_LINK_MOE$format_pack.0.1+140 ;178.3
        mulss     xmm0, DWORD PTR [-300+ebp]                    ;178.3
        lea       eax, DWORD PTR [-312+ebp]                     ;178.3
        push      eax                                           ;178.3
        push      OFFSET FLAT: __STRLITPACK_32.0.1              ;178.3
        push      -2088435968                                   ;178.3
        push      34                                            ;178.3
        mov       DWORD PTR [-152+ebp], 0                       ;178.3
        lea       edx, DWORD PTR [-152+ebp]                     ;178.3
        push      edx                                           ;178.3
        movss     DWORD PTR [-312+ebp], xmm0                    ;178.3
        call      _for_write_seq_fmt                            ;178.3
                                ; LOE esi edi
.B1.916:                        ; Preds .B1.586                 ; Infreq
        add       esp, 28                                       ;178.3
                                ; LOE esi edi
.B1.587:                        ; Preds .B1.916                 ; Infreq
        push      32                                            ;179.6
        push      OFFSET FLAT: CAL_LINK_MOE$format_pack.0.1+120 ;179.6
        xor       eax, eax                                      ;179.6
        lea       edx, DWORD PTR [-152+ebp]                     ;179.6
        push      eax                                           ;179.6
        push      OFFSET FLAT: __STRLITPACK_33.0.1              ;179.6
        push      -2088435968                                   ;179.6
        push      34                                            ;179.6
        push      edx                                           ;179.6
        mov       DWORD PTR [-152+ebp], eax                     ;179.6
        call      _for_write_seq_fmt                            ;179.6
                                ; LOE esi edi
.B1.917:                        ; Preds .B1.587                 ; Infreq
        add       esp, 28                                       ;179.6
                                ; LOE esi edi
.B1.588:                        ; Preds .B1.917                 ; Infreq
        cmp       DWORD PTR [-372+ebp], 0                       ;179.6
        jle       .B1.593       ; Prob 2%                       ;179.6
                                ; LOE esi edi
.B1.589:                        ; Preds .B1.588                 ; Infreq
        cvtsi2ss  xmm0, DWORD PTR [-384+ebp]                    ;179.33
        mov       eax, 1                                        ;
        mov       DWORD PTR [-264+ebp], edi                     ;179.33
        movss     DWORD PTR [-408+ebp], xmm0                    ;179.33
        mov       DWORD PTR [-116+ebp], esi                     ;179.33
        mov       esi, eax                                      ;179.33
        mov       edi, DWORD PTR [-164+ebp]                     ;179.33
                                ; LOE esi edi
.B1.590:                        ; Preds .B1.591 .B1.589         ; Infreq
        movss     xmm0, DWORD PTR [-4+edi+esi*4]                ;179.22
        lea       edx, DWORD PTR [-80+ebp]                      ;179.6
        divss     xmm0, DWORD PTR [-408+ebp]                    ;179.6
        push      edx                                           ;179.6
        push      OFFSET FLAT: __STRLITPACK_34.0.1              ;179.6
        movss     DWORD PTR [-80+ebp], xmm0                     ;179.6
        lea       ecx, DWORD PTR [-152+ebp]                     ;179.6
        push      ecx                                           ;179.6
        call      _for_write_seq_fmt_xmit                       ;179.6
                                ; LOE esi edi
.B1.918:                        ; Preds .B1.590                 ; Infreq
        add       esp, 12                                       ;179.6
                                ; LOE esi edi
.B1.591:                        ; Preds .B1.918                 ; Infreq
        inc       esi                                           ;179.6
        cmp       esi, DWORD PTR [-372+ebp]                     ;179.6
        jle       .B1.590       ; Prob 82%                      ;179.6
                                ; LOE esi edi
.B1.592:                        ; Preds .B1.591                 ; Infreq
        mov       esi, DWORD PTR [-116+ebp]                     ;
        mov       edi, DWORD PTR [-264+ebp]                     ;
                                ; LOE esi edi
.B1.593:                        ; Preds .B1.588 .B1.592         ; Infreq
        push      0                                             ;179.6
        push      OFFSET FLAT: __STRLITPACK_35.0.1              ;179.6
        lea       eax, DWORD PTR [-152+ebp]                     ;179.6
        push      eax                                           ;179.6
        call      _for_write_seq_fmt_xmit                       ;179.6
                                ; LOE esi edi
.B1.919:                        ; Preds .B1.593                 ; Infreq
        add       esp, 12                                       ;179.6
                                ; LOE esi edi
.B1.594:                        ; Preds .B1.919                 ; Infreq
        push      32                                            ;180.3
        xor       eax, eax                                      ;180.3
        lea       edx, DWORD PTR [-152+ebp]                     ;180.3
        push      eax                                           ;180.3
        push      OFFSET FLAT: __STRLITPACK_36.0.1              ;180.3
        push      -2088435968                                   ;180.3
        push      34                                            ;180.3
        push      edx                                           ;180.3
        mov       DWORD PTR [-152+ebp], eax                     ;180.3
        call      _for_write_seq_lis                            ;180.3
                                ; LOE esi edi
.B1.920:                        ; Preds .B1.594                 ; Infreq
        add       esp, 24                                       ;180.3
                                ; LOE esi edi
.B1.595:                        ; Preds .B1.920                 ; Infreq
        cmp       DWORD PTR [-188+ebp], 0                       ;181.11
        jle       .B1.33        ; Prob 50%                      ;181.11
                                ; LOE esi edi
.B1.596:                        ; Preds .B1.595                 ; Infreq
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;181.11
        mov       eax, DWORD PTR [-188+ebp]                     ;181.11
        mov       DWORD PTR [-408+ebp], ecx                     ;181.11
        mov       ecx, eax                                      ;181.11
        shr       ecx, 31                                       ;181.11
        add       ecx, eax                                      ;181.11
        sar       ecx, 1                                        ;181.11
        test      ecx, ecx                                      ;181.11
        jbe       .B1.604       ; Prob 10%                      ;181.11
                                ; LOE ecx esi edi
.B1.597:                        ; Preds .B1.596                 ; Infreq
        xor       edx, edx                                      ;
        mov       DWORD PTR [-116+ebp], esi                     ;
        xor       eax, eax                                      ;
        mov       DWORD PTR [-264+ebp], edi                     ;
        xor       edi, edi                                      ;
        mov       esi, DWORD PTR [-408+ebp]                     ;
                                ; LOE eax edx ecx esi edi
.B1.598:                        ; Preds .B1.598 .B1.597         ; Infreq
        inc       edx                                           ;181.11
        mov       DWORD PTR [772+eax+esi], edi                  ;181.11
        mov       DWORD PTR [1672+eax+esi], edi                 ;181.11
        add       eax, 1800                                     ;181.11
        cmp       edx, ecx                                      ;181.11
        jb        .B1.598       ; Prob 64%                      ;181.11
                                ; LOE eax edx ecx esi edi
.B1.599:                        ; Preds .B1.598                 ; Infreq
        mov       esi, DWORD PTR [-116+ebp]                     ;
        lea       eax, DWORD PTR [1+edx+edx]                    ;181.11
        mov       edi, DWORD PTR [-264+ebp]                     ;
                                ; LOE eax esi edi
.B1.600:                        ; Preds .B1.599 .B1.604         ; Infreq
        lea       edx, DWORD PTR [-1+eax]                       ;181.11
        cmp       edx, DWORD PTR [-188+ebp]                     ;181.11
        jae       .B1.33        ; Prob 10%                      ;181.11
                                ; LOE eax esi edi
.B1.601:                        ; Preds .B1.600                 ; Infreq
        mov       edx, DWORD PTR [-260+ebp]                     ;181.11
        mov       ecx, edx                                      ;181.11
        neg       ecx                                           ;181.11
        add       eax, edx                                      ;181.11
        add       ecx, eax                                      ;181.11
        imul      edx, ecx, 900                                 ;181.11
        mov       eax, DWORD PTR [-408+ebp]                     ;181.11
        mov       DWORD PTR [-128+eax+edx], 0                   ;181.11
        jmp       .B1.33        ; Prob 100%                     ;181.11
                                ; LOE esi edi
.B1.602:                        ; Preds .B1.565                 ; Infreq
        mov       edx, 1                                        ;
        jmp       .B1.569       ; Prob 100%                     ;
                                ; LOE edx esi edi
.B1.603:                        ; Preds .B1.573                 ; Infreq
        mov       edx, 1                                        ;
        jmp       .B1.581       ; Prob 100%                     ;
                                ; LOE edx ecx esi edi
.B1.604:                        ; Preds .B1.596                 ; Infreq
        mov       eax, 1                                        ;
        jmp       .B1.600       ; Prob 100%                     ;
                                ; LOE eax esi edi
.B1.605:                        ; Preds .B1.28                  ; Infreq
        test      esi, esi                                      ;138.9
        jle       .B1.612       ; Prob 50%                      ;138.9
                                ; LOE esi edi
.B1.606:                        ; Preds .B1.605                 ; Infreq
        mov       edx, esi                                      ;138.9
        shr       edx, 31                                       ;138.9
        add       edx, esi                                      ;138.9
        sar       edx, 1                                        ;138.9
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;139.6
        test      edx, edx                                      ;138.9
        mov       DWORD PTR [-408+ebp], eax                     ;139.6
        jbe       .B1.643       ; Prob 3%                       ;138.9
                                ; LOE edx esi edi
.B1.607:                        ; Preds .B1.606                 ; Infreq
        imul      eax, DWORD PTR [-260+ebp], -900               ;
        xor       ecx, ecx                                      ;
        mov       DWORD PTR [-264+ebp], edi                     ;
        add       eax, DWORD PTR [-408+ebp]                     ;
        mov       edi, ecx                                      ;
        ALIGN     16
                                ; LOE eax edx ecx esi edi
.B1.608:                        ; Preds .B1.608 .B1.607         ; Infreq
        movss     xmm0, DWORD PTR [1668+edi+eax]                ;139.39
        inc       ecx                                           ;138.9
        movss     xmm1, DWORD PTR [2568+edi+eax]                ;139.39
        addss     xmm0, DWORD PTR [1552+edi+eax]                ;139.6
        addss     xmm1, DWORD PTR [2452+edi+eax]                ;139.6
        movss     DWORD PTR [1668+edi+eax], xmm0                ;139.6
        movss     DWORD PTR [2568+edi+eax], xmm1                ;139.6
        add       edi, 1800                                     ;138.9
        cmp       ecx, edx                                      ;138.9
        jb        .B1.608       ; Prob 64%                      ;138.9
                                ; LOE eax edx ecx esi edi
.B1.609:                        ; Preds .B1.608                 ; Infreq
        mov       edi, DWORD PTR [-264+ebp]                     ;
        lea       edx, DWORD PTR [1+ecx+ecx]                    ;138.9
                                ; LOE edx esi edi
.B1.610:                        ; Preds .B1.609 .B1.643         ; Infreq
        lea       eax, DWORD PTR [-1+edx]                       ;138.9
        cmp       esi, eax                                      ;138.9
        jbe       .B1.612       ; Prob 3%                       ;138.9
                                ; LOE edx esi edi
.B1.611:                        ; Preds .B1.610                 ; Infreq
        mov       eax, DWORD PTR [-260+ebp]                     ;139.6
        neg       eax                                           ;139.6
        add       eax, edx                                      ;139.6
        imul      ecx, eax, 900                                 ;139.6
        mov       edx, DWORD PTR [-408+ebp]                     ;139.39
        movss     xmm0, DWORD PTR [768+edx+ecx]                 ;139.39
        addss     xmm0, DWORD PTR [652+edx+ecx]                 ;139.6
        movss     DWORD PTR [768+edx+ecx], xmm0                 ;139.6
                                ; LOE esi edi
.B1.612:                        ; Preds .B1.605 .B1.610 .B1.611 ; Infreq
        mov       eax, DWORD PTR [-308+ebp]                     ;142.12
        cdq                                                     ;142.12
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_I33_T] ;142.9
        idiv      ecx                                           ;142.12
        mov       DWORD PTR [-384+ebp], ecx                     ;142.9
        test      edx, edx                                      ;142.25
        jne       .B1.29        ; Prob 50%                      ;142.25
                                ; LOE esi edi
.B1.613:                        ; Preds .B1.612                 ; Infreq
        test      esi, esi                                      ;144.6
        jle       .B1.625       ; Prob 50%                      ;144.6
                                ; LOE esi edi
.B1.614:                        ; Preds .B1.613                 ; Infreq
        mov       edx, esi                                      ;144.6
        shr       edx, 31                                       ;144.6
        add       edx, esi                                      ;144.6
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;155.11
        sar       edx, 1                                        ;144.6
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;145.8
        test      edx, edx                                      ;144.6
        mov       DWORD PTR [-396+ebp], ecx                     ;155.11
        mov       DWORD PTR [-400+ebp], eax                     ;145.8
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;145.8
        mov       DWORD PTR [-376+ebp], edx                     ;144.6
        mov       DWORD PTR [-372+ebp], 0                       ;
        jbe       .B1.644       ; Prob 3%                       ;144.6
                                ; LOE ecx esi edi
.B1.615:                        ; Preds .B1.614                 ; Infreq
        imul      edx, DWORD PTR [-260+ebp], -900               ;
        xor       eax, eax                                      ;
        add       edx, DWORD PTR [-396+ebp]                     ;
        mov       DWORD PTR [-388+ebp], edx                     ;
        imul      edx, ecx, -152                                ;
        add       edx, DWORD PTR [-400+ebp]                     ;
        mov       DWORD PTR [-404+ebp], eax                     ;
        mov       DWORD PTR [-392+ebp], eax                     ;
        mov       DWORD PTR [-380+ebp], edx                     ;
        mov       DWORD PTR [-408+ebp], ecx                     ;
        mov       DWORD PTR [-116+ebp], esi                     ;
        mov       DWORD PTR [-264+ebp], edi                     ;
        mov       edx, eax                                      ;
        mov       ecx, DWORD PTR [-404+ebp]                     ;
                                ; LOE eax edx ecx
.B1.616:                        ; Preds .B1.620 .B1.615         ; Infreq
        mov       esi, DWORD PTR [-380+ebp]                     ;145.12
        movsx     edi, WORD PTR [300+edx+esi]                   ;145.12
        cmp       edi, 99                                       ;145.50
        jge       .B1.618       ; Prob 50%                      ;145.50
                                ; LOE eax edx ecx
.B1.617:                        ; Preds .B1.616                 ; Infreq
        mov       esi, DWORD PTR [-388+ebp]                     ;147.10
        mov       DWORD PTR [-392+ebp], edx                     ;
        mov       edx, DWORD PTR [-372+ebp]                     ;146.10
        inc       edx                                           ;146.10
        mov       edi, DWORD PTR [1668+ecx+esi]                 ;147.10
        mov       esi, DWORD PTR [-164+ebp]                     ;147.10
        mov       DWORD PTR [-372+ebp], edx                     ;146.10
        mov       DWORD PTR [-4+esi+edx*4], edi                 ;147.10
        mov       edx, DWORD PTR [-392+ebp]                     ;147.10
                                ; LOE eax edx ecx
.B1.618:                        ; Preds .B1.617 .B1.616         ; Infreq
        mov       esi, DWORD PTR [-380+ebp]                     ;145.12
        movsx     edi, WORD PTR [452+edx+esi]                   ;145.12
        cmp       edi, 99                                       ;145.50
        jge       .B1.620       ; Prob 50%                      ;145.50
                                ; LOE eax edx ecx
.B1.619:                        ; Preds .B1.618                 ; Infreq
        mov       esi, DWORD PTR [-388+ebp]                     ;147.10
        mov       DWORD PTR [-392+ebp], edx                     ;
        mov       edx, DWORD PTR [-372+ebp]                     ;146.10
        inc       edx                                           ;146.10
        mov       edi, DWORD PTR [2568+ecx+esi]                 ;147.10
        mov       esi, DWORD PTR [-164+ebp]                     ;147.10
        mov       DWORD PTR [-372+ebp], edx                     ;146.10
        mov       DWORD PTR [-4+esi+edx*4], edi                 ;147.10
        mov       edx, DWORD PTR [-392+ebp]                     ;147.10
                                ; LOE eax edx ecx
.B1.620:                        ; Preds .B1.619 .B1.618         ; Infreq
        inc       eax                                           ;144.6
        add       ecx, 1800                                     ;144.6
        add       edx, 304                                      ;144.6
        cmp       eax, DWORD PTR [-376+ebp]                     ;144.6
        jb        .B1.616       ; Prob 64%                      ;144.6
                                ; LOE eax edx ecx
.B1.621:                        ; Preds .B1.620                 ; Infreq
        mov       ecx, DWORD PTR [-408+ebp]                     ;
        lea       edx, DWORD PTR [1+eax+eax]                    ;144.6
        mov       esi, DWORD PTR [-116+ebp]                     ;
        mov       edi, DWORD PTR [-264+ebp]                     ;
                                ; LOE edx ecx esi edi
.B1.622:                        ; Preds .B1.621 .B1.644         ; Infreq
        lea       eax, DWORD PTR [-1+edx]                       ;144.6
        cmp       esi, eax                                      ;144.6
        jbe       .B1.626       ; Prob 3%                       ;144.6
                                ; LOE edx ecx esi edi
.B1.623:                        ; Preds .B1.622                 ; Infreq
        neg       ecx                                           ;145.50
        add       ecx, edx                                      ;145.50
        imul      ecx, ecx, 152                                 ;145.50
        mov       eax, DWORD PTR [-400+ebp]                     ;145.12
        movsx     eax, WORD PTR [148+eax+ecx]                   ;145.12
        cmp       eax, 99                                       ;145.50
        jge       .B1.626       ; Prob 50%                      ;145.50
                                ; LOE edx esi edi
.B1.624:                        ; Preds .B1.623                 ; Infreq
        mov       ecx, DWORD PTR [-260+ebp]                     ;147.10
        neg       ecx                                           ;147.10
        add       ecx, edx                                      ;147.10
        imul      ecx, ecx, 900                                 ;147.10
        mov       edx, DWORD PTR [-396+ebp]                     ;147.10
        mov       eax, DWORD PTR [-372+ebp]                     ;146.10
        inc       eax                                           ;146.10
        mov       edx, DWORD PTR [768+edx+ecx]                  ;147.10
        mov       ecx, DWORD PTR [-164+ebp]                     ;147.10
        mov       DWORD PTR [-372+ebp], eax                     ;146.10
        mov       DWORD PTR [-4+ecx+eax*4], edx                 ;147.10
        jmp       .B1.626       ; Prob 100%                     ;147.10
                                ; LOE esi edi
.B1.625:                        ; Preds .B1.613                 ; Infreq
        xor       eax, eax                                      ;
        mov       DWORD PTR [-372+ebp], eax                     ;
                                ; LOE esi edi
.B1.626:                        ; Preds .B1.624 .B1.623 .B1.622 .B1.625 ; Infreq
        push      32                                            ;151.6
        xor       eax, eax                                      ;151.6
        lea       edx, DWORD PTR [-152+ebp]                     ;151.6
        push      eax                                           ;151.6
        push      OFFSET FLAT: __STRLITPACK_25.0.1              ;151.6
        push      -2088435968                                   ;151.6
        push      33                                            ;151.6
        push      edx                                           ;151.6
        mov       DWORD PTR [-152+ebp], eax                     ;151.6
        call      _for_write_seq_lis                            ;151.6
                                ; LOE esi edi
.B1.921:                        ; Preds .B1.626                 ; Infreq
        add       esp, 24                                       ;151.6
                                ; LOE esi edi
.B1.627:                        ; Preds .B1.921                 ; Infreq
        cvtsi2ss  xmm0, DWORD PTR [-308+ebp]                    ;152.25
        push      32                                            ;152.11
        push      OFFSET FLAT: CAL_LINK_MOE$format_pack.0.1+140 ;152.11
        mulss     xmm0, DWORD PTR [-300+ebp]                    ;152.11
        lea       eax, DWORD PTR [-320+ebp]                     ;152.11
        push      eax                                           ;152.11
        push      OFFSET FLAT: __STRLITPACK_26.0.1              ;152.11
        push      -2088435968                                   ;152.11
        push      33                                            ;152.11
        mov       DWORD PTR [-152+ebp], 0                       ;152.11
        lea       edx, DWORD PTR [-152+ebp]                     ;152.11
        push      edx                                           ;152.11
        movss     DWORD PTR [-320+ebp], xmm0                    ;152.11
        call      _for_write_seq_fmt                            ;152.11
                                ; LOE esi edi
.B1.922:                        ; Preds .B1.627                 ; Infreq
        add       esp, 28                                       ;152.11
                                ; LOE esi edi
.B1.628:                        ; Preds .B1.922                 ; Infreq
        push      32                                            ;153.6
        push      OFFSET FLAT: CAL_LINK_MOE$format_pack.0.1+160 ;153.6
        xor       eax, eax                                      ;153.6
        lea       edx, DWORD PTR [-152+ebp]                     ;153.6
        push      eax                                           ;153.6
        push      OFFSET FLAT: __STRLITPACK_27.0.1              ;153.6
        push      -2088435968                                   ;153.6
        push      33                                            ;153.6
        push      edx                                           ;153.6
        mov       DWORD PTR [-152+ebp], eax                     ;153.6
        call      _for_write_seq_fmt                            ;153.6
                                ; LOE esi edi
.B1.923:                        ; Preds .B1.628                 ; Infreq
        add       esp, 28                                       ;153.6
                                ; LOE esi edi
.B1.629:                        ; Preds .B1.923                 ; Infreq
        cmp       DWORD PTR [-372+ebp], 0                       ;153.6
        jle       .B1.634       ; Prob 2%                       ;153.6
                                ; LOE esi edi
.B1.630:                        ; Preds .B1.629                 ; Infreq
        cvtsi2ss  xmm0, DWORD PTR [-384+ebp]                    ;153.33
        mov       eax, 1                                        ;
        mov       DWORD PTR [-264+ebp], edi                     ;153.33
        movss     xmm1, DWORD PTR [_2il0floatpacket.10]         ;153.38
        movss     DWORD PTR [-408+ebp], xmm0                    ;153.33
        mov       DWORD PTR [-116+ebp], esi                     ;153.33
        mov       esi, eax                                      ;153.33
        mov       edi, DWORD PTR [-164+ebp]                     ;153.33
                                ; LOE esi edi
.B1.631:                        ; Preds .B1.632 .B1.630         ; Infreq
        movss     xmm0, DWORD PTR [-4+edi+esi*4]                ;153.22
        lea       edx, DWORD PTR [-88+ebp]                      ;153.6
        divss     xmm0, DWORD PTR [-408+ebp]                    ;153.32
        mulss     xmm0, DWORD PTR [_2il0floatpacket.10]         ;153.6
        lea       ecx, DWORD PTR [-152+ebp]                     ;153.6
        push      edx                                           ;153.6
        push      OFFSET FLAT: __STRLITPACK_28.0.1              ;153.6
        push      ecx                                           ;153.6
        movss     DWORD PTR [-88+ebp], xmm0                     ;153.6
        call      _for_write_seq_fmt_xmit                       ;153.6
                                ; LOE esi edi
.B1.924:                        ; Preds .B1.631                 ; Infreq
        add       esp, 12                                       ;153.6
                                ; LOE esi edi
.B1.632:                        ; Preds .B1.924                 ; Infreq
        inc       esi                                           ;153.6
        cmp       esi, DWORD PTR [-372+ebp]                     ;153.6
        jle       .B1.631       ; Prob 82%                      ;153.6
                                ; LOE esi edi
.B1.633:                        ; Preds .B1.632                 ; Infreq
        mov       esi, DWORD PTR [-116+ebp]                     ;
        mov       edi, DWORD PTR [-264+ebp]                     ;
                                ; LOE esi edi
.B1.634:                        ; Preds .B1.629 .B1.633         ; Infreq
        push      0                                             ;153.6
        push      OFFSET FLAT: __STRLITPACK_29.0.1              ;153.6
        lea       eax, DWORD PTR [-152+ebp]                     ;153.6
        push      eax                                           ;153.6
        call      _for_write_seq_fmt_xmit                       ;153.6
                                ; LOE esi edi
.B1.925:                        ; Preds .B1.634                 ; Infreq
        add       esp, 12                                       ;153.6
                                ; LOE esi edi
.B1.635:                        ; Preds .B1.925                 ; Infreq
        push      32                                            ;154.11
        xor       eax, eax                                      ;154.11
        lea       edx, DWORD PTR [-152+ebp]                     ;154.11
        push      eax                                           ;154.11
        push      OFFSET FLAT: __STRLITPACK_30.0.1              ;154.11
        push      -2088435968                                   ;154.11
        push      33                                            ;154.11
        push      edx                                           ;154.11
        mov       DWORD PTR [-152+ebp], eax                     ;154.11
        call      _for_write_seq_lis                            ;154.11
                                ; LOE esi edi
.B1.926:                        ; Preds .B1.635                 ; Infreq
        add       esp, 24                                       ;154.11
                                ; LOE esi edi
.B1.636:                        ; Preds .B1.926                 ; Infreq
        cmp       DWORD PTR [-188+ebp], 0                       ;155.11
        jle       .B1.29        ; Prob 50%                      ;155.11
                                ; LOE esi edi
.B1.637:                        ; Preds .B1.636                 ; Infreq
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;155.11
        mov       eax, DWORD PTR [-188+ebp]                     ;155.11
        mov       DWORD PTR [-408+ebp], ecx                     ;155.11
        mov       ecx, eax                                      ;155.11
        shr       ecx, 31                                       ;155.11
        add       ecx, eax                                      ;155.11
        sar       ecx, 1                                        ;155.11
        test      ecx, ecx                                      ;155.11
        jbe       .B1.645       ; Prob 10%                      ;155.11
                                ; LOE ecx esi edi
.B1.638:                        ; Preds .B1.637                 ; Infreq
        xor       edx, edx                                      ;
        mov       DWORD PTR [-116+ebp], esi                     ;
        xor       eax, eax                                      ;
        mov       DWORD PTR [-264+ebp], edi                     ;
        xor       edi, edi                                      ;
        mov       esi, DWORD PTR [-408+ebp]                     ;
                                ; LOE eax edx ecx esi edi
.B1.639:                        ; Preds .B1.639 .B1.638         ; Infreq
        inc       edx                                           ;155.11
        mov       DWORD PTR [768+eax+esi], edi                  ;155.11
        mov       DWORD PTR [1668+eax+esi], edi                 ;155.11
        add       eax, 1800                                     ;155.11
        cmp       edx, ecx                                      ;155.11
        jb        .B1.639       ; Prob 64%                      ;155.11
                                ; LOE eax edx ecx esi edi
.B1.640:                        ; Preds .B1.639                 ; Infreq
        mov       esi, DWORD PTR [-116+ebp]                     ;
        lea       eax, DWORD PTR [1+edx+edx]                    ;155.11
        mov       edi, DWORD PTR [-264+ebp]                     ;
                                ; LOE eax esi edi
.B1.641:                        ; Preds .B1.640 .B1.645         ; Infreq
        lea       edx, DWORD PTR [-1+eax]                       ;155.11
        cmp       edx, DWORD PTR [-188+ebp]                     ;155.11
        jae       .B1.29        ; Prob 10%                      ;155.11
                                ; LOE eax esi edi
.B1.642:                        ; Preds .B1.641                 ; Infreq
        mov       edx, DWORD PTR [-260+ebp]                     ;155.11
        mov       ecx, edx                                      ;155.11
        neg       ecx                                           ;155.11
        add       eax, edx                                      ;155.11
        add       ecx, eax                                      ;155.11
        imul      edx, ecx, 900                                 ;155.11
        mov       eax, DWORD PTR [-408+ebp]                     ;155.11
        mov       DWORD PTR [-132+eax+edx], 0                   ;155.11
        jmp       .B1.29        ; Prob 100%                     ;155.11
                                ; LOE esi edi
.B1.643:                        ; Preds .B1.606                 ; Infreq
        mov       edx, 1                                        ;
        jmp       .B1.610       ; Prob 100%                     ;
                                ; LOE edx esi edi
.B1.644:                        ; Preds .B1.614                 ; Infreq
        mov       edx, 1                                        ;
        jmp       .B1.622       ; Prob 100%                     ;
                                ; LOE edx ecx esi edi
.B1.645:                        ; Preds .B1.637                 ; Infreq
        mov       eax, 1                                        ;
        jmp       .B1.641       ; Prob 100%                     ;
                                ; LOE eax esi edi
.B1.646:                        ; Preds .B1.24                  ; Infreq
        test      esi, esi                                      ;112.9
        jle       .B1.653       ; Prob 50%                      ;112.9
                                ; LOE esi edi
.B1.647:                        ; Preds .B1.646                 ; Infreq
        mov       edx, esi                                      ;112.9
        shr       edx, 31                                       ;112.9
        add       edx, esi                                      ;112.9
        sar       edx, 1                                        ;112.9
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;113.6
        test      edx, edx                                      ;112.9
        mov       DWORD PTR [-408+ebp], eax                     ;113.6
        jbe       .B1.684       ; Prob 3%                       ;112.9
                                ; LOE edx esi edi
.B1.648:                        ; Preds .B1.647                 ; Infreq
        imul      eax, DWORD PTR [-260+ebp], -900               ;
        xor       ecx, ecx                                      ;
        mov       DWORD PTR [-264+ebp], edi                     ;
        add       eax, DWORD PTR [-408+ebp]                     ;
        mov       edi, ecx                                      ;
        ALIGN     16
                                ; LOE eax edx ecx esi edi
.B1.649:                        ; Preds .B1.649 .B1.648         ; Infreq
        pxor      xmm0, xmm0                                    ;113.72
        pxor      xmm1, xmm1                                    ;113.72
        cvtsi2ss  xmm0, DWORD PTR [1300+edi+eax]                ;113.72
        cvtsi2ss  xmm1, DWORD PTR [2200+edi+eax]                ;113.72
        addss     xmm0, DWORD PTR [1664+edi+eax]                ;113.6
        addss     xmm1, DWORD PTR [2564+edi+eax]                ;113.6
        inc       ecx                                           ;112.9
        movss     DWORD PTR [1664+edi+eax], xmm0                ;113.6
        movss     DWORD PTR [2564+edi+eax], xmm1                ;113.6
        add       edi, 1800                                     ;112.9
        cmp       ecx, edx                                      ;112.9
        jb        .B1.649       ; Prob 64%                      ;112.9
                                ; LOE eax edx ecx esi edi
.B1.650:                        ; Preds .B1.649                 ; Infreq
        mov       edi, DWORD PTR [-264+ebp]                     ;
        lea       edx, DWORD PTR [1+ecx+ecx]                    ;112.9
                                ; LOE edx esi edi
.B1.651:                        ; Preds .B1.650 .B1.684         ; Infreq
        lea       eax, DWORD PTR [-1+edx]                       ;112.9
        cmp       esi, eax                                      ;112.9
        jbe       .B1.653       ; Prob 3%                       ;112.9
                                ; LOE edx esi edi
.B1.652:                        ; Preds .B1.651                 ; Infreq
        mov       eax, DWORD PTR [-260+ebp]                     ;113.6
        neg       eax                                           ;113.6
        add       eax, edx                                      ;113.6
        imul      ecx, eax, 900                                 ;113.6
        mov       edx, DWORD PTR [-408+ebp]                     ;113.72
        cvtsi2ss  xmm0, DWORD PTR [400+edx+ecx]                 ;113.72
        addss     xmm0, DWORD PTR [764+edx+ecx]                 ;113.6
        movss     DWORD PTR [764+edx+ecx], xmm0                 ;113.6
                                ; LOE esi edi
.B1.653:                        ; Preds .B1.646 .B1.651 .B1.652 ; Infreq
        mov       eax, DWORD PTR [-308+ebp]                     ;117.12
        cdq                                                     ;117.12
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_I32_T] ;117.9
        idiv      ecx                                           ;117.12
        mov       DWORD PTR [-384+ebp], ecx                     ;117.9
        test      edx, edx                                      ;117.25
        jne       .B1.25        ; Prob 50%                      ;117.25
                                ; LOE esi edi
.B1.654:                        ; Preds .B1.653                 ; Infreq
        test      esi, esi                                      ;119.6
        jle       .B1.666       ; Prob 50%                      ;119.6
                                ; LOE esi edi
.B1.655:                        ; Preds .B1.654                 ; Infreq
        mov       edx, esi                                      ;119.6
        shr       edx, 31                                       ;119.6
        add       edx, esi                                      ;119.6
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;130.11
        sar       edx, 1                                        ;119.6
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;120.8
        test      edx, edx                                      ;119.6
        mov       DWORD PTR [-396+ebp], ecx                     ;130.11
        mov       DWORD PTR [-400+ebp], eax                     ;120.8
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;120.8
        mov       DWORD PTR [-376+ebp], edx                     ;119.6
        mov       DWORD PTR [-372+ebp], 0                       ;
        jbe       .B1.685       ; Prob 3%                       ;119.6
                                ; LOE ecx esi edi
.B1.656:                        ; Preds .B1.655                 ; Infreq
        imul      edx, DWORD PTR [-260+ebp], -900               ;
        xor       eax, eax                                      ;
        add       edx, DWORD PTR [-396+ebp]                     ;
        mov       DWORD PTR [-388+ebp], edx                     ;
        imul      edx, ecx, -152                                ;
        add       edx, DWORD PTR [-400+ebp]                     ;
        mov       DWORD PTR [-404+ebp], eax                     ;
        mov       DWORD PTR [-392+ebp], eax                     ;
        mov       DWORD PTR [-380+ebp], edx                     ;
        mov       DWORD PTR [-408+ebp], ecx                     ;
        mov       DWORD PTR [-116+ebp], esi                     ;
        mov       DWORD PTR [-264+ebp], edi                     ;
        mov       edx, eax                                      ;
        mov       ecx, DWORD PTR [-404+ebp]                     ;
                                ; LOE eax edx ecx
.B1.657:                        ; Preds .B1.661 .B1.656         ; Infreq
        mov       esi, DWORD PTR [-380+ebp]                     ;120.12
        movsx     edi, WORD PTR [300+edx+esi]                   ;120.12
        cmp       edi, 99                                       ;120.50
        jge       .B1.659       ; Prob 50%                      ;120.50
                                ; LOE eax edx ecx
.B1.658:                        ; Preds .B1.657                 ; Infreq
        mov       esi, DWORD PTR [-388+ebp]                     ;122.10
        mov       DWORD PTR [-392+ebp], edx                     ;
        mov       edx, DWORD PTR [-372+ebp]                     ;121.10
        inc       edx                                           ;121.10
        mov       edi, DWORD PTR [1664+ecx+esi]                 ;122.10
        mov       esi, DWORD PTR [-164+ebp]                     ;122.10
        mov       DWORD PTR [-372+ebp], edx                     ;121.10
        mov       DWORD PTR [-4+esi+edx*4], edi                 ;122.10
        mov       edx, DWORD PTR [-392+ebp]                     ;122.10
                                ; LOE eax edx ecx
.B1.659:                        ; Preds .B1.658 .B1.657         ; Infreq
        mov       esi, DWORD PTR [-380+ebp]                     ;120.12
        movsx     edi, WORD PTR [452+edx+esi]                   ;120.12
        cmp       edi, 99                                       ;120.50
        jge       .B1.661       ; Prob 50%                      ;120.50
                                ; LOE eax edx ecx
.B1.660:                        ; Preds .B1.659                 ; Infreq
        mov       esi, DWORD PTR [-388+ebp]                     ;122.10
        mov       DWORD PTR [-392+ebp], edx                     ;
        mov       edx, DWORD PTR [-372+ebp]                     ;121.10
        inc       edx                                           ;121.10
        mov       edi, DWORD PTR [2564+ecx+esi]                 ;122.10
        mov       esi, DWORD PTR [-164+ebp]                     ;122.10
        mov       DWORD PTR [-372+ebp], edx                     ;121.10
        mov       DWORD PTR [-4+esi+edx*4], edi                 ;122.10
        mov       edx, DWORD PTR [-392+ebp]                     ;122.10
                                ; LOE eax edx ecx
.B1.661:                        ; Preds .B1.660 .B1.659         ; Infreq
        inc       eax                                           ;119.6
        add       ecx, 1800                                     ;119.6
        add       edx, 304                                      ;119.6
        cmp       eax, DWORD PTR [-376+ebp]                     ;119.6
        jb        .B1.657       ; Prob 64%                      ;119.6
                                ; LOE eax edx ecx
.B1.662:                        ; Preds .B1.661                 ; Infreq
        mov       ecx, DWORD PTR [-408+ebp]                     ;
        lea       edx, DWORD PTR [1+eax+eax]                    ;119.6
        mov       esi, DWORD PTR [-116+ebp]                     ;
        mov       edi, DWORD PTR [-264+ebp]                     ;
                                ; LOE edx ecx esi edi
.B1.663:                        ; Preds .B1.662 .B1.685         ; Infreq
        lea       eax, DWORD PTR [-1+edx]                       ;119.6
        cmp       esi, eax                                      ;119.6
        jbe       .B1.667       ; Prob 3%                       ;119.6
                                ; LOE edx ecx esi edi
.B1.664:                        ; Preds .B1.663                 ; Infreq
        neg       ecx                                           ;120.50
        add       ecx, edx                                      ;120.50
        imul      ecx, ecx, 152                                 ;120.50
        mov       eax, DWORD PTR [-400+ebp]                     ;120.12
        movsx     eax, WORD PTR [148+eax+ecx]                   ;120.12
        cmp       eax, 99                                       ;120.50
        jge       .B1.667       ; Prob 50%                      ;120.50
                                ; LOE edx esi edi
.B1.665:                        ; Preds .B1.664                 ; Infreq
        mov       ecx, DWORD PTR [-260+ebp]                     ;122.10
        neg       ecx                                           ;122.10
        add       ecx, edx                                      ;122.10
        imul      ecx, ecx, 900                                 ;122.10
        mov       edx, DWORD PTR [-396+ebp]                     ;122.10
        mov       eax, DWORD PTR [-372+ebp]                     ;121.10
        inc       eax                                           ;121.10
        mov       edx, DWORD PTR [764+edx+ecx]                  ;122.10
        mov       ecx, DWORD PTR [-164+ebp]                     ;122.10
        mov       DWORD PTR [-372+ebp], eax                     ;121.10
        mov       DWORD PTR [-4+ecx+eax*4], edx                 ;122.10
        jmp       .B1.667       ; Prob 100%                     ;122.10
                                ; LOE esi edi
.B1.666:                        ; Preds .B1.654                 ; Infreq
        xor       eax, eax                                      ;
        mov       DWORD PTR [-372+ebp], eax                     ;
                                ; LOE esi edi
.B1.667:                        ; Preds .B1.665 .B1.664 .B1.663 .B1.666 ; Infreq
        mov       edx, 32                                       ;126.11
        xor       eax, eax                                      ;126.11
        push      edx                                           ;126.11
        push      eax                                           ;126.11
        push      OFFSET FLAT: __STRLITPACK_19.0.1              ;126.11
        push      -2088435968                                   ;126.11
        push      edx                                           ;126.11
        mov       DWORD PTR [-152+ebp], eax                     ;126.11
        lea       ecx, DWORD PTR [-152+ebp]                     ;126.11
        push      ecx                                           ;126.11
        call      _for_write_seq_lis                            ;126.11
                                ; LOE esi edi
.B1.927:                        ; Preds .B1.667                 ; Infreq
        add       esp, 24                                       ;126.11
                                ; LOE esi edi
.B1.668:                        ; Preds .B1.927                 ; Infreq
        cvtsi2ss  xmm0, DWORD PTR [-308+ebp]                    ;127.25
        mov       edx, 32                                       ;127.11
        lea       eax, DWORD PTR [-328+ebp]                     ;127.11
        push      edx                                           ;127.11
        push      OFFSET FLAT: CAL_LINK_MOE$format_pack.0.1+140 ;127.11
        push      eax                                           ;127.11
        push      OFFSET FLAT: __STRLITPACK_20.0.1              ;127.11
        mulss     xmm0, DWORD PTR [-300+ebp]                    ;127.11
        lea       ecx, DWORD PTR [-152+ebp]                     ;127.11
        push      -2088435968                                   ;127.11
        push      edx                                           ;127.11
        push      ecx                                           ;127.11
        mov       DWORD PTR [-152+ebp], 0                       ;127.11
        movss     DWORD PTR [-328+ebp], xmm0                    ;127.11
        call      _for_write_seq_fmt                            ;127.11
                                ; LOE esi edi
.B1.928:                        ; Preds .B1.668                 ; Infreq
        add       esp, 28                                       ;127.11
                                ; LOE esi edi
.B1.669:                        ; Preds .B1.928                 ; Infreq
        mov       edx, 32                                       ;128.6
        xor       eax, eax                                      ;128.6
        push      edx                                           ;128.6
        push      OFFSET FLAT: CAL_LINK_MOE$format_pack.0.1+180 ;128.6
        push      eax                                           ;128.6
        push      OFFSET FLAT: __STRLITPACK_21.0.1              ;128.6
        push      -2088435968                                   ;128.6
        push      edx                                           ;128.6
        mov       DWORD PTR [-152+ebp], eax                     ;128.6
        lea       ecx, DWORD PTR [-152+ebp]                     ;128.6
        push      ecx                                           ;128.6
        call      _for_write_seq_fmt                            ;128.6
                                ; LOE esi edi
.B1.929:                        ; Preds .B1.669                 ; Infreq
        add       esp, 28                                       ;128.6
                                ; LOE esi edi
.B1.670:                        ; Preds .B1.929                 ; Infreq
        cmp       DWORD PTR [-372+ebp], 0                       ;128.6
        jle       .B1.675       ; Prob 2%                       ;128.6
                                ; LOE esi edi
.B1.671:                        ; Preds .B1.670                 ; Infreq
        cvtsi2ss  xmm0, DWORD PTR [-384+ebp]                    ;128.33
        mov       eax, 1                                        ;
        mov       DWORD PTR [-264+ebp], edi                     ;128.33
        movss     DWORD PTR [-408+ebp], xmm0                    ;128.33
        mov       DWORD PTR [-116+ebp], esi                     ;128.33
        mov       esi, eax                                      ;128.33
        mov       edi, DWORD PTR [-164+ebp]                     ;128.33
                                ; LOE esi edi
.B1.672:                        ; Preds .B1.673 .B1.671         ; Infreq
        movss     xmm0, DWORD PTR [-4+edi+esi*4]                ;128.22
        lea       edx, DWORD PTR [-96+ebp]                      ;128.6
        divss     xmm0, DWORD PTR [-408+ebp]                    ;128.6
        push      edx                                           ;128.6
        push      OFFSET FLAT: __STRLITPACK_22.0.1              ;128.6
        movss     DWORD PTR [-96+ebp], xmm0                     ;128.6
        lea       ecx, DWORD PTR [-152+ebp]                     ;128.6
        push      ecx                                           ;128.6
        call      _for_write_seq_fmt_xmit                       ;128.6
                                ; LOE esi edi
.B1.930:                        ; Preds .B1.672                 ; Infreq
        add       esp, 12                                       ;128.6
                                ; LOE esi edi
.B1.673:                        ; Preds .B1.930                 ; Infreq
        inc       esi                                           ;128.6
        cmp       esi, DWORD PTR [-372+ebp]                     ;128.6
        jle       .B1.672       ; Prob 82%                      ;128.6
                                ; LOE esi edi
.B1.674:                        ; Preds .B1.673                 ; Infreq
        mov       esi, DWORD PTR [-116+ebp]                     ;
        mov       edi, DWORD PTR [-264+ebp]                     ;
                                ; LOE esi edi
.B1.675:                        ; Preds .B1.670 .B1.674         ; Infreq
        push      0                                             ;128.6
        push      OFFSET FLAT: __STRLITPACK_23.0.1              ;128.6
        lea       eax, DWORD PTR [-152+ebp]                     ;128.6
        push      eax                                           ;128.6
        call      _for_write_seq_fmt_xmit                       ;128.6
                                ; LOE esi edi
.B1.931:                        ; Preds .B1.675                 ; Infreq
        add       esp, 12                                       ;128.6
                                ; LOE esi edi
.B1.676:                        ; Preds .B1.931                 ; Infreq
        mov       edx, 32                                       ;129.11
        xor       eax, eax                                      ;129.11
        push      edx                                           ;129.11
        push      eax                                           ;129.11
        push      OFFSET FLAT: __STRLITPACK_24.0.1              ;129.11
        push      -2088435968                                   ;129.11
        push      edx                                           ;129.11
        mov       DWORD PTR [-152+ebp], eax                     ;129.11
        lea       ecx, DWORD PTR [-152+ebp]                     ;129.11
        push      ecx                                           ;129.11
        call      _for_write_seq_lis                            ;129.11
                                ; LOE esi edi
.B1.932:                        ; Preds .B1.676                 ; Infreq
        add       esp, 24                                       ;129.11
                                ; LOE esi edi
.B1.677:                        ; Preds .B1.932                 ; Infreq
        cmp       DWORD PTR [-188+ebp], 0                       ;130.11
        jle       .B1.25        ; Prob 50%                      ;130.11
                                ; LOE esi edi
.B1.678:                        ; Preds .B1.677                 ; Infreq
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;130.11
        mov       eax, DWORD PTR [-188+ebp]                     ;130.11
        mov       DWORD PTR [-408+ebp], ecx                     ;130.11
        mov       ecx, eax                                      ;130.11
        shr       ecx, 31                                       ;130.11
        add       ecx, eax                                      ;130.11
        sar       ecx, 1                                        ;130.11
        test      ecx, ecx                                      ;130.11
        jbe       .B1.686       ; Prob 10%                      ;130.11
                                ; LOE ecx esi edi
.B1.679:                        ; Preds .B1.678                 ; Infreq
        xor       edx, edx                                      ;
        mov       DWORD PTR [-116+ebp], esi                     ;
        xor       eax, eax                                      ;
        mov       DWORD PTR [-264+ebp], edi                     ;
        xor       edi, edi                                      ;
        mov       esi, DWORD PTR [-408+ebp]                     ;
                                ; LOE eax edx ecx esi edi
.B1.680:                        ; Preds .B1.680 .B1.679         ; Infreq
        inc       edx                                           ;130.11
        mov       DWORD PTR [764+eax+esi], edi                  ;130.11
        mov       DWORD PTR [1664+eax+esi], edi                 ;130.11
        add       eax, 1800                                     ;130.11
        cmp       edx, ecx                                      ;130.11
        jb        .B1.680       ; Prob 64%                      ;130.11
                                ; LOE eax edx ecx esi edi
.B1.681:                        ; Preds .B1.680                 ; Infreq
        mov       esi, DWORD PTR [-116+ebp]                     ;
        lea       eax, DWORD PTR [1+edx+edx]                    ;130.11
        mov       edi, DWORD PTR [-264+ebp]                     ;
                                ; LOE eax esi edi
.B1.682:                        ; Preds .B1.681 .B1.686         ; Infreq
        lea       edx, DWORD PTR [-1+eax]                       ;130.11
        cmp       edx, DWORD PTR [-188+ebp]                     ;130.11
        jae       .B1.25        ; Prob 10%                      ;130.11
                                ; LOE eax esi edi
.B1.683:                        ; Preds .B1.682                 ; Infreq
        mov       edx, DWORD PTR [-260+ebp]                     ;130.11
        mov       ecx, edx                                      ;130.11
        neg       ecx                                           ;130.11
        add       eax, edx                                      ;130.11
        add       ecx, eax                                      ;130.11
        imul      edx, ecx, 900                                 ;130.11
        mov       eax, DWORD PTR [-408+ebp]                     ;130.11
        mov       DWORD PTR [-136+eax+edx], 0                   ;130.11
        jmp       .B1.25        ; Prob 100%                     ;130.11
                                ; LOE esi edi
.B1.684:                        ; Preds .B1.647                 ; Infreq
        mov       edx, 1                                        ;
        jmp       .B1.651       ; Prob 100%                     ;
                                ; LOE edx esi edi
.B1.685:                        ; Preds .B1.655                 ; Infreq
        mov       edx, 1                                        ;
        jmp       .B1.663       ; Prob 100%                     ;
                                ; LOE edx ecx esi edi
.B1.686:                        ; Preds .B1.678                 ; Infreq
        mov       eax, 1                                        ;
        jmp       .B1.682       ; Prob 100%                     ;
                                ; LOE eax esi edi
.B1.687:                        ; Preds .B1.20                  ; Infreq
        test      esi, esi                                      ;86.9
        jle       .B1.694       ; Prob 50%                      ;86.9
                                ; LOE esi edi
.B1.688:                        ; Preds .B1.687                 ; Infreq
        mov       edx, esi                                      ;86.9
        shr       edx, 31                                       ;86.9
        add       edx, esi                                      ;86.9
        sar       edx, 1                                        ;86.9
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;87.11
        test      edx, edx                                      ;86.9
        mov       DWORD PTR [-408+ebp], eax                     ;87.11
        jbe       .B1.728       ; Prob 3%                       ;86.9
                                ; LOE edx esi edi
.B1.689:                        ; Preds .B1.688                 ; Infreq
        imul      eax, DWORD PTR [-260+ebp], -900               ;
        xor       ecx, ecx                                      ;
        mov       DWORD PTR [-264+ebp], edi                     ;
        add       eax, DWORD PTR [-408+ebp]                     ;
        mov       edi, ecx                                      ;
        ALIGN     16
                                ; LOE eax edx ecx esi edi
.B1.690:                        ; Preds .B1.690 .B1.689         ; Infreq
        pxor      xmm0, xmm0                                    ;87.77
        pxor      xmm1, xmm1                                    ;87.77
        cvtsi2ss  xmm0, DWORD PTR [1296+edi+eax]                ;87.77
        cvtsi2ss  xmm1, DWORD PTR [2196+edi+eax]                ;87.77
        addss     xmm0, DWORD PTR [1660+edi+eax]                ;87.11
        addss     xmm1, DWORD PTR [2560+edi+eax]                ;87.11
        inc       ecx                                           ;86.9
        movss     DWORD PTR [1660+edi+eax], xmm0                ;87.11
        movss     DWORD PTR [2560+edi+eax], xmm1                ;87.11
        add       edi, 1800                                     ;86.9
        cmp       ecx, edx                                      ;86.9
        jb        .B1.690       ; Prob 64%                      ;86.9
                                ; LOE eax edx ecx esi edi
.B1.691:                        ; Preds .B1.690                 ; Infreq
        mov       edi, DWORD PTR [-264+ebp]                     ;
        lea       edx, DWORD PTR [1+ecx+ecx]                    ;86.9
                                ; LOE edx esi edi
.B1.692:                        ; Preds .B1.691 .B1.728         ; Infreq
        lea       eax, DWORD PTR [-1+edx]                       ;86.9
        cmp       esi, eax                                      ;86.9
        jbe       .B1.694       ; Prob 3%                       ;86.9
                                ; LOE edx esi edi
.B1.693:                        ; Preds .B1.692                 ; Infreq
        mov       eax, DWORD PTR [-260+ebp]                     ;87.11
        neg       eax                                           ;87.11
        add       eax, edx                                      ;87.11
        imul      ecx, eax, 900                                 ;87.11
        mov       edx, DWORD PTR [-408+ebp]                     ;87.77
        cvtsi2ss  xmm0, DWORD PTR [396+edx+ecx]                 ;87.77
        addss     xmm0, DWORD PTR [760+edx+ecx]                 ;87.11
        movss     DWORD PTR [760+edx+ecx], xmm0                 ;87.11
                                ; LOE esi edi
.B1.694:                        ; Preds .B1.687 .B1.692 .B1.693 ; Infreq
        cmp       DWORD PTR [-308+ebp], 1                       ;90.9
        jle       .B1.21        ; Prob 16%                      ;90.9
                                ; LOE esi edi
.B1.695:                        ; Preds .B1.694                 ; Infreq
        mov       eax, DWORD PTR [-308+ebp]                     ;91.12
        cdq                                                     ;91.12
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_I31_T] ;91.9
        idiv      ecx                                           ;91.12
        mov       DWORD PTR [-384+ebp], ecx                     ;91.9
        test      edx, edx                                      ;91.25
        jne       .B1.21        ; Prob 50%                      ;91.25
                                ; LOE esi edi
.B1.696:                        ; Preds .B1.695                 ; Infreq
        test      esi, esi                                      ;93.6
        jle       .B1.708       ; Prob 50%                      ;93.6
                                ; LOE esi edi
.B1.697:                        ; Preds .B1.696                 ; Infreq
        mov       edx, esi                                      ;93.6
        shr       edx, 31                                       ;93.6
        add       edx, esi                                      ;93.6
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;104.11
        sar       edx, 1                                        ;93.6
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;94.8
        test      edx, edx                                      ;93.6
        mov       DWORD PTR [-396+ebp], ecx                     ;104.11
        mov       DWORD PTR [-400+ebp], eax                     ;94.8
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;94.8
        mov       DWORD PTR [-376+ebp], edx                     ;93.6
        mov       DWORD PTR [-372+ebp], 0                       ;
        jbe       .B1.726       ; Prob 3%                       ;93.6
                                ; LOE ecx esi edi
.B1.698:                        ; Preds .B1.697                 ; Infreq
        imul      edx, DWORD PTR [-260+ebp], -900               ;
        xor       eax, eax                                      ;
        add       edx, DWORD PTR [-396+ebp]                     ;
        mov       DWORD PTR [-388+ebp], edx                     ;
        imul      edx, ecx, -152                                ;
        add       edx, DWORD PTR [-400+ebp]                     ;
        mov       DWORD PTR [-404+ebp], eax                     ;
        mov       DWORD PTR [-392+ebp], eax                     ;
        mov       DWORD PTR [-380+ebp], edx                     ;
        mov       DWORD PTR [-408+ebp], ecx                     ;
        mov       DWORD PTR [-116+ebp], esi                     ;
        mov       DWORD PTR [-264+ebp], edi                     ;
        mov       edx, eax                                      ;
        mov       ecx, DWORD PTR [-404+ebp]                     ;
                                ; LOE eax edx ecx
.B1.699:                        ; Preds .B1.703 .B1.698         ; Infreq
        mov       esi, DWORD PTR [-380+ebp]                     ;94.12
        movsx     edi, WORD PTR [300+edx+esi]                   ;94.12
        cmp       edi, 99                                       ;94.50
        jge       .B1.701       ; Prob 50%                      ;94.50
                                ; LOE eax edx ecx
.B1.700:                        ; Preds .B1.699                 ; Infreq
        mov       esi, DWORD PTR [-388+ebp]                     ;96.10
        mov       DWORD PTR [-392+ebp], edx                     ;
        mov       edx, DWORD PTR [-372+ebp]                     ;95.10
        inc       edx                                           ;95.10
        mov       edi, DWORD PTR [1660+ecx+esi]                 ;96.10
        mov       esi, DWORD PTR [-164+ebp]                     ;96.10
        mov       DWORD PTR [-372+ebp], edx                     ;95.10
        mov       DWORD PTR [-4+esi+edx*4], edi                 ;96.10
        mov       edx, DWORD PTR [-392+ebp]                     ;96.10
                                ; LOE eax edx ecx
.B1.701:                        ; Preds .B1.700 .B1.699         ; Infreq
        mov       esi, DWORD PTR [-380+ebp]                     ;94.12
        movsx     edi, WORD PTR [452+edx+esi]                   ;94.12
        cmp       edi, 99                                       ;94.50
        jge       .B1.703       ; Prob 50%                      ;94.50
                                ; LOE eax edx ecx
.B1.702:                        ; Preds .B1.701                 ; Infreq
        mov       esi, DWORD PTR [-388+ebp]                     ;96.10
        mov       DWORD PTR [-392+ebp], edx                     ;
        mov       edx, DWORD PTR [-372+ebp]                     ;95.10
        inc       edx                                           ;95.10
        mov       edi, DWORD PTR [2560+ecx+esi]                 ;96.10
        mov       esi, DWORD PTR [-164+ebp]                     ;96.10
        mov       DWORD PTR [-372+ebp], edx                     ;95.10
        mov       DWORD PTR [-4+esi+edx*4], edi                 ;96.10
        mov       edx, DWORD PTR [-392+ebp]                     ;96.10
                                ; LOE eax edx ecx
.B1.703:                        ; Preds .B1.702 .B1.701         ; Infreq
        inc       eax                                           ;93.6
        add       ecx, 1800                                     ;93.6
        add       edx, 304                                      ;93.6
        cmp       eax, DWORD PTR [-376+ebp]                     ;93.6
        jb        .B1.699       ; Prob 64%                      ;93.6
                                ; LOE eax edx ecx
.B1.704:                        ; Preds .B1.703                 ; Infreq
        mov       ecx, DWORD PTR [-408+ebp]                     ;
        lea       edx, DWORD PTR [1+eax+eax]                    ;93.6
        mov       esi, DWORD PTR [-116+ebp]                     ;
        mov       edi, DWORD PTR [-264+ebp]                     ;
                                ; LOE edx ecx esi edi
.B1.705:                        ; Preds .B1.704 .B1.726         ; Infreq
        lea       eax, DWORD PTR [-1+edx]                       ;93.6
        cmp       esi, eax                                      ;93.6
        jbe       .B1.709       ; Prob 3%                       ;93.6
                                ; LOE edx ecx esi edi
.B1.706:                        ; Preds .B1.705                 ; Infreq
        neg       ecx                                           ;94.50
        add       ecx, edx                                      ;94.50
        imul      ecx, ecx, 152                                 ;94.50
        mov       eax, DWORD PTR [-400+ebp]                     ;94.12
        movsx     eax, WORD PTR [148+eax+ecx]                   ;94.12
        cmp       eax, 99                                       ;94.50
        jge       .B1.709       ; Prob 50%                      ;94.50
                                ; LOE edx esi edi
.B1.707:                        ; Preds .B1.706                 ; Infreq
        mov       ecx, DWORD PTR [-260+ebp]                     ;96.10
        neg       ecx                                           ;96.10
        add       ecx, edx                                      ;96.10
        imul      ecx, ecx, 900                                 ;96.10
        mov       edx, DWORD PTR [-396+ebp]                     ;96.10
        mov       eax, DWORD PTR [-372+ebp]                     ;95.10
        inc       eax                                           ;95.10
        mov       edx, DWORD PTR [760+edx+ecx]                  ;96.10
        mov       ecx, DWORD PTR [-164+ebp]                     ;96.10
        mov       DWORD PTR [-372+ebp], eax                     ;95.10
        mov       DWORD PTR [-4+ecx+eax*4], edx                 ;96.10
        jmp       .B1.709       ; Prob 100%                     ;96.10
                                ; LOE esi edi
.B1.708:                        ; Preds .B1.696                 ; Infreq
        xor       eax, eax                                      ;
        mov       DWORD PTR [-372+ebp], eax                     ;
                                ; LOE esi edi
.B1.709:                        ; Preds .B1.707 .B1.706 .B1.705 .B1.708 ; Infreq
        push      32                                            ;100.11
        xor       eax, eax                                      ;100.11
        lea       edx, DWORD PTR [-152+ebp]                     ;100.11
        push      eax                                           ;100.11
        push      OFFSET FLAT: __STRLITPACK_13.0.1              ;100.11
        push      -2088435968                                   ;100.11
        push      31                                            ;100.11
        push      edx                                           ;100.11
        mov       DWORD PTR [-152+ebp], eax                     ;100.11
        call      _for_write_seq_lis                            ;100.11
                                ; LOE esi edi
.B1.933:                        ; Preds .B1.709                 ; Infreq
        add       esp, 24                                       ;100.11
                                ; LOE esi edi
.B1.710:                        ; Preds .B1.933                 ; Infreq
        cvtsi2ss  xmm0, DWORD PTR [-308+ebp]                    ;101.25
        push      32                                            ;101.11
        push      OFFSET FLAT: CAL_LINK_MOE$format_pack.0.1+140 ;101.11
        mulss     xmm0, DWORD PTR [-300+ebp]                    ;101.11
        lea       eax, DWORD PTR [-352+ebp]                     ;101.11
        push      eax                                           ;101.11
        push      OFFSET FLAT: __STRLITPACK_14.0.1              ;101.11
        push      -2088435968                                   ;101.11
        push      31                                            ;101.11
        mov       DWORD PTR [-152+ebp], 0                       ;101.11
        lea       edx, DWORD PTR [-152+ebp]                     ;101.11
        push      edx                                           ;101.11
        movss     DWORD PTR [-352+ebp], xmm0                    ;101.11
        call      _for_write_seq_fmt                            ;101.11
                                ; LOE esi edi
.B1.934:                        ; Preds .B1.710                 ; Infreq
        add       esp, 28                                       ;101.11
                                ; LOE esi edi
.B1.711:                        ; Preds .B1.934                 ; Infreq
        push      32                                            ;102.11
        push      OFFSET FLAT: CAL_LINK_MOE$format_pack.0.1+200 ;102.11
        xor       eax, eax                                      ;102.11
        lea       edx, DWORD PTR [-152+ebp]                     ;102.11
        push      eax                                           ;102.11
        push      OFFSET FLAT: __STRLITPACK_15.0.1              ;102.11
        push      -2088435968                                   ;102.11
        push      31                                            ;102.11
        push      edx                                           ;102.11
        mov       DWORD PTR [-152+ebp], eax                     ;102.11
        call      _for_write_seq_fmt                            ;102.11
                                ; LOE esi edi
.B1.935:                        ; Preds .B1.711                 ; Infreq
        add       esp, 28                                       ;102.11
                                ; LOE esi edi
.B1.712:                        ; Preds .B1.935                 ; Infreq
        cmp       DWORD PTR [-372+ebp], 0                       ;102.11
        jle       .B1.717       ; Prob 2%                       ;102.11
                                ; LOE esi edi
.B1.713:                        ; Preds .B1.712                 ; Infreq
        cvtsi2ss  xmm0, DWORD PTR [-384+ebp]                    ;102.38
        mov       eax, 1                                        ;
        mov       DWORD PTR [-264+ebp], edi                     ;102.38
        movss     DWORD PTR [-408+ebp], xmm0                    ;102.38
        mov       DWORD PTR [-116+ebp], esi                     ;102.38
        mov       esi, eax                                      ;102.38
        mov       edi, DWORD PTR [-164+ebp]                     ;102.38
                                ; LOE esi edi
.B1.714:                        ; Preds .B1.715 .B1.713         ; Infreq
        movss     xmm0, DWORD PTR [-4+edi+esi*4]                ;102.27
        lea       edx, DWORD PTR [-120+ebp]                     ;102.11
        divss     xmm0, DWORD PTR [-408+ebp]                    ;102.11
        push      edx                                           ;102.11
        push      OFFSET FLAT: __STRLITPACK_16.0.1              ;102.11
        movss     DWORD PTR [-120+ebp], xmm0                    ;102.11
        lea       ecx, DWORD PTR [-152+ebp]                     ;102.11
        push      ecx                                           ;102.11
        call      _for_write_seq_fmt_xmit                       ;102.11
                                ; LOE esi edi
.B1.936:                        ; Preds .B1.714                 ; Infreq
        add       esp, 12                                       ;102.11
                                ; LOE esi edi
.B1.715:                        ; Preds .B1.936                 ; Infreq
        inc       esi                                           ;102.11
        cmp       esi, DWORD PTR [-372+ebp]                     ;102.11
        jle       .B1.714       ; Prob 82%                      ;102.11
                                ; LOE esi edi
.B1.716:                        ; Preds .B1.715                 ; Infreq
        mov       esi, DWORD PTR [-116+ebp]                     ;
        mov       edi, DWORD PTR [-264+ebp]                     ;
                                ; LOE esi edi
.B1.717:                        ; Preds .B1.712 .B1.716         ; Infreq
        push      0                                             ;102.11
        push      OFFSET FLAT: __STRLITPACK_17.0.1              ;102.11
        lea       eax, DWORD PTR [-152+ebp]                     ;102.11
        push      eax                                           ;102.11
        call      _for_write_seq_fmt_xmit                       ;102.11
                                ; LOE esi edi
.B1.937:                        ; Preds .B1.717                 ; Infreq
        add       esp, 12                                       ;102.11
                                ; LOE esi edi
.B1.718:                        ; Preds .B1.937                 ; Infreq
        push      32                                            ;103.8
        xor       eax, eax                                      ;103.8
        lea       edx, DWORD PTR [-152+ebp]                     ;103.8
        push      eax                                           ;103.8
        push      OFFSET FLAT: __STRLITPACK_18.0.1              ;103.8
        push      -2088435968                                   ;103.8
        push      31                                            ;103.8
        push      edx                                           ;103.8
        mov       DWORD PTR [-152+ebp], eax                     ;103.8
        call      _for_write_seq_lis                            ;103.8
                                ; LOE esi edi
.B1.938:                        ; Preds .B1.718                 ; Infreq
        add       esp, 24                                       ;103.8
                                ; LOE esi edi
.B1.719:                        ; Preds .B1.938                 ; Infreq
        cmp       DWORD PTR [-188+ebp], 0                       ;104.11
        jle       .B1.21        ; Prob 50%                      ;104.11
                                ; LOE esi edi
.B1.720:                        ; Preds .B1.719                 ; Infreq
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;104.11
        mov       eax, DWORD PTR [-188+ebp]                     ;104.11
        mov       DWORD PTR [-408+ebp], ecx                     ;104.11
        mov       ecx, eax                                      ;104.11
        shr       ecx, 31                                       ;104.11
        add       ecx, eax                                      ;104.11
        sar       ecx, 1                                        ;104.11
        test      ecx, ecx                                      ;104.11
        jbe       .B1.727       ; Prob 10%                      ;104.11
                                ; LOE ecx esi edi
.B1.721:                        ; Preds .B1.720                 ; Infreq
        xor       edx, edx                                      ;
        mov       DWORD PTR [-116+ebp], esi                     ;
        xor       eax, eax                                      ;
        mov       DWORD PTR [-264+ebp], edi                     ;
        xor       edi, edi                                      ;
        mov       esi, DWORD PTR [-408+ebp]                     ;
                                ; LOE eax edx ecx esi edi
.B1.722:                        ; Preds .B1.722 .B1.721         ; Infreq
        inc       edx                                           ;104.11
        mov       DWORD PTR [760+eax+esi], edi                  ;104.11
        mov       DWORD PTR [1660+eax+esi], edi                 ;104.11
        add       eax, 1800                                     ;104.11
        cmp       edx, ecx                                      ;104.11
        jb        .B1.722       ; Prob 64%                      ;104.11
                                ; LOE eax edx ecx esi edi
.B1.723:                        ; Preds .B1.722                 ; Infreq
        mov       esi, DWORD PTR [-116+ebp]                     ;
        lea       eax, DWORD PTR [1+edx+edx]                    ;104.11
        mov       edi, DWORD PTR [-264+ebp]                     ;
                                ; LOE eax esi edi
.B1.724:                        ; Preds .B1.723 .B1.727         ; Infreq
        lea       edx, DWORD PTR [-1+eax]                       ;104.11
        cmp       edx, DWORD PTR [-188+ebp]                     ;104.11
        jae       .B1.21        ; Prob 10%                      ;104.11
                                ; LOE eax esi edi
.B1.725:                        ; Preds .B1.724                 ; Infreq
        mov       edx, DWORD PTR [-260+ebp]                     ;104.11
        mov       ecx, edx                                      ;104.11
        neg       ecx                                           ;104.11
        add       eax, edx                                      ;104.11
        add       ecx, eax                                      ;104.11
        imul      edx, ecx, 900                                 ;104.11
        mov       eax, DWORD PTR [-408+ebp]                     ;104.11
        mov       DWORD PTR [-140+eax+edx], 0                   ;104.11
        jmp       .B1.21        ; Prob 100%                     ;104.11
                                ; LOE esi edi
.B1.726:                        ; Preds .B1.697                 ; Infreq
        mov       edx, 1                                        ;
        jmp       .B1.705       ; Prob 100%                     ;
                                ; LOE edx ecx esi edi
.B1.727:                        ; Preds .B1.720                 ; Infreq
        mov       eax, 1                                        ;
        jmp       .B1.724       ; Prob 100%                     ;
                                ; LOE eax esi edi
.B1.728:                        ; Preds .B1.688                 ; Infreq
        mov       edx, 1                                        ;
        jmp       .B1.692       ; Prob 100%                     ;
                                ; LOE edx esi edi
.B1.729:                        ; Preds .B1.16                  ; Infreq
        test      esi, esi                                      ;62.9
        jle       .B1.736       ; Prob 50%                      ;62.9
                                ; LOE esi edi
.B1.730:                        ; Preds .B1.729                 ; Infreq
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;63.9
        mov       DWORD PTR [-388+ebp], eax                     ;63.9
        mov       eax, esi                                      ;62.9
        shr       eax, 31                                       ;62.9
        add       eax, esi                                      ;62.9
        sar       eax, 1                                        ;62.9
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;63.44
        test      eax, eax                                      ;62.9
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;63.44
        mov       DWORD PTR [-392+ebp], edx                     ;63.44
        mov       DWORD PTR [-396+ebp], ecx                     ;63.44
        jbe       .B1.767       ; Prob 3%                       ;62.9
                                ; LOE eax esi edi
.B1.731:                        ; Preds .B1.730                 ; Infreq
        imul      ecx, DWORD PTR [-260+ebp], -900               ;
        xor       edx, edx                                      ;
        add       ecx, DWORD PTR [-388+ebp]                     ;
        mov       DWORD PTR [-400+ebp], ecx                     ;
        imul      ecx, DWORD PTR [-396+ebp], -152               ;
        add       ecx, DWORD PTR [-392+ebp]                     ;
        mov       DWORD PTR [-404+ebp], ecx                     ;
        mov       DWORD PTR [-408+ebp], edx                     ;
        mov       DWORD PTR [-384+ebp], eax                     ;
        mov       DWORD PTR [-116+ebp], esi                     ;
        mov       DWORD PTR [-264+ebp], edi                     ;
        mov       eax, edx                                      ;
        mov       ecx, DWORD PTR [-408+ebp]                     ;
        mov       esi, DWORD PTR [-404+ebp]                     ;
        mov       edi, DWORD PTR [-400+ebp]                     ;
        ALIGN     16
                                ; LOE eax edx ecx esi edi
.B1.732:                        ; Preds .B1.732 .B1.731         ; Infreq
        movss     xmm0, DWORD PTR [_2il0floatpacket.9]          ;63.9
        inc       edx                                           ;62.9
        movss     xmm2, DWORD PTR [_2il0floatpacket.9]          ;63.9
        movss     xmm1, DWORD PTR [172+eax+esi]                 ;63.79
        movss     xmm3, DWORD PTR [324+eax+esi]                 ;63.79
        add       eax, 304                                      ;62.9
        maxss     xmm0, DWORD PTR [1552+ecx+edi]                ;63.9
        maxss     xmm2, DWORD PTR [2452+ecx+edi]                ;63.9
        divss     xmm1, xmm0                                    ;63.109
        divss     xmm3, xmm2                                    ;63.109
        addss     xmm1, DWORD PTR [1656+ecx+edi]                ;63.9
        addss     xmm3, DWORD PTR [2556+ecx+edi]                ;63.9
        movss     DWORD PTR [1656+ecx+edi], xmm1                ;63.9
        movss     DWORD PTR [2556+ecx+edi], xmm3                ;63.9
        add       ecx, 1800                                     ;62.9
        cmp       edx, DWORD PTR [-384+ebp]                     ;62.9
        jb        .B1.732       ; Prob 64%                      ;62.9
                                ; LOE eax edx ecx esi edi
.B1.733:                        ; Preds .B1.732                 ; Infreq
        mov       esi, DWORD PTR [-116+ebp]                     ;
        lea       edx, DWORD PTR [1+edx+edx]                    ;62.9
        mov       edi, DWORD PTR [-264+ebp]                     ;
                                ; LOE edx esi edi
.B1.734:                        ; Preds .B1.733 .B1.767         ; Infreq
        lea       eax, DWORD PTR [-1+edx]                       ;62.9
        cmp       esi, eax                                      ;62.9
        jbe       .B1.736       ; Prob 3%                       ;62.9
                                ; LOE edx esi edi
.B1.735:                        ; Preds .B1.734                 ; Infreq
        mov       ecx, DWORD PTR [-260+ebp]                     ;63.9
        neg       ecx                                           ;63.9
        add       ecx, edx                                      ;63.9
        imul      eax, ecx, 900                                 ;63.9
        movss     xmm0, DWORD PTR [_2il0floatpacket.9]          ;63.120
        mov       ecx, DWORD PTR [-396+ebp]                     ;63.9
        neg       ecx                                           ;63.9
        add       ecx, edx                                      ;63.9
        imul      ecx, ecx, 152                                 ;63.9
        mov       edx, DWORD PTR [-392+ebp]                     ;63.79
        movss     xmm1, DWORD PTR [20+edx+ecx]                  ;63.79
        mov       edx, DWORD PTR [-388+ebp]                     ;63.9
        maxss     xmm0, DWORD PTR [652+edx+eax]                 ;63.9
        divss     xmm1, xmm0                                    ;63.109
        addss     xmm1, DWORD PTR [756+edx+eax]                 ;63.9
        movss     DWORD PTR [756+edx+eax], xmm1                 ;63.9
                                ; LOE esi edi
.B1.736:                        ; Preds .B1.729 .B1.734 .B1.735 ; Infreq
        mov       eax, DWORD PTR [-308+ebp]                     ;66.11
        cdq                                                     ;66.11
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_I301_T] ;66.8
        idiv      ecx                                           ;66.11
        mov       DWORD PTR [-384+ebp], ecx                     ;66.8
        test      edx, edx                                      ;66.25
        jne       .B1.17        ; Prob 50%                      ;66.25
                                ; LOE esi edi
.B1.737:                        ; Preds .B1.736                 ; Infreq
        test      esi, esi                                      ;68.6
        jle       .B1.749       ; Prob 50%                      ;68.6
                                ; LOE esi edi
.B1.738:                        ; Preds .B1.737                 ; Infreq
        mov       edx, esi                                      ;68.6
        shr       edx, 31                                       ;68.6
        add       edx, esi                                      ;68.6
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;79.11
        sar       edx, 1                                        ;68.6
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;69.8
        test      edx, edx                                      ;68.6
        mov       DWORD PTR [-396+ebp], ecx                     ;79.11
        mov       DWORD PTR [-400+ebp], eax                     ;69.8
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;69.8
        mov       DWORD PTR [-376+ebp], edx                     ;68.6
        mov       DWORD PTR [-372+ebp], 0                       ;
        jbe       .B1.768       ; Prob 3%                       ;68.6
                                ; LOE ecx esi edi
.B1.739:                        ; Preds .B1.738                 ; Infreq
        imul      edx, DWORD PTR [-260+ebp], -900               ;
        xor       eax, eax                                      ;
        add       edx, DWORD PTR [-396+ebp]                     ;
        mov       DWORD PTR [-388+ebp], edx                     ;
        imul      edx, ecx, -152                                ;
        add       edx, DWORD PTR [-400+ebp]                     ;
        mov       DWORD PTR [-404+ebp], eax                     ;
        mov       DWORD PTR [-392+ebp], eax                     ;
        mov       DWORD PTR [-380+ebp], edx                     ;
        mov       DWORD PTR [-408+ebp], ecx                     ;
        mov       DWORD PTR [-116+ebp], esi                     ;
        mov       DWORD PTR [-264+ebp], edi                     ;
        mov       edx, eax                                      ;
        mov       ecx, DWORD PTR [-404+ebp]                     ;
                                ; LOE eax edx ecx
.B1.740:                        ; Preds .B1.744 .B1.739         ; Infreq
        mov       esi, DWORD PTR [-380+ebp]                     ;69.12
        movsx     edi, WORD PTR [300+edx+esi]                   ;69.12
        cmp       edi, 99                                       ;69.50
        jge       .B1.742       ; Prob 50%                      ;69.50
                                ; LOE eax edx ecx
.B1.741:                        ; Preds .B1.740                 ; Infreq
        mov       esi, DWORD PTR [-388+ebp]                     ;71.10
        mov       DWORD PTR [-392+ebp], edx                     ;
        mov       edx, DWORD PTR [-372+ebp]                     ;70.10
        inc       edx                                           ;70.10
        mov       edi, DWORD PTR [1656+ecx+esi]                 ;71.10
        mov       esi, DWORD PTR [-164+ebp]                     ;71.10
        mov       DWORD PTR [-372+ebp], edx                     ;70.10
        mov       DWORD PTR [-4+esi+edx*4], edi                 ;71.10
        mov       edx, DWORD PTR [-392+ebp]                     ;71.10
                                ; LOE eax edx ecx
.B1.742:                        ; Preds .B1.741 .B1.740         ; Infreq
        mov       esi, DWORD PTR [-380+ebp]                     ;69.12
        movsx     edi, WORD PTR [452+edx+esi]                   ;69.12
        cmp       edi, 99                                       ;69.50
        jge       .B1.744       ; Prob 50%                      ;69.50
                                ; LOE eax edx ecx
.B1.743:                        ; Preds .B1.742                 ; Infreq
        mov       esi, DWORD PTR [-388+ebp]                     ;71.10
        mov       DWORD PTR [-392+ebp], edx                     ;
        mov       edx, DWORD PTR [-372+ebp]                     ;70.10
        inc       edx                                           ;70.10
        mov       edi, DWORD PTR [2556+ecx+esi]                 ;71.10
        mov       esi, DWORD PTR [-164+ebp]                     ;71.10
        mov       DWORD PTR [-372+ebp], edx                     ;70.10
        mov       DWORD PTR [-4+esi+edx*4], edi                 ;71.10
        mov       edx, DWORD PTR [-392+ebp]                     ;71.10
                                ; LOE eax edx ecx
.B1.744:                        ; Preds .B1.743 .B1.742         ; Infreq
        inc       eax                                           ;68.6
        add       ecx, 1800                                     ;68.6
        add       edx, 304                                      ;68.6
        cmp       eax, DWORD PTR [-376+ebp]                     ;68.6
        jb        .B1.740       ; Prob 64%                      ;68.6
                                ; LOE eax edx ecx
.B1.745:                        ; Preds .B1.744                 ; Infreq
        mov       ecx, DWORD PTR [-408+ebp]                     ;
        lea       edx, DWORD PTR [1+eax+eax]                    ;68.6
        mov       esi, DWORD PTR [-116+ebp]                     ;
        mov       edi, DWORD PTR [-264+ebp]                     ;
                                ; LOE edx ecx esi edi
.B1.746:                        ; Preds .B1.745 .B1.768         ; Infreq
        lea       eax, DWORD PTR [-1+edx]                       ;68.6
        cmp       esi, eax                                      ;68.6
        jbe       .B1.750       ; Prob 3%                       ;68.6
                                ; LOE edx ecx esi edi
.B1.747:                        ; Preds .B1.746                 ; Infreq
        neg       ecx                                           ;69.50
        add       ecx, edx                                      ;69.50
        imul      ecx, ecx, 152                                 ;69.50
        mov       eax, DWORD PTR [-400+ebp]                     ;69.12
        movsx     eax, WORD PTR [148+eax+ecx]                   ;69.12
        cmp       eax, 99                                       ;69.50
        jge       .B1.750       ; Prob 50%                      ;69.50
                                ; LOE edx esi edi
.B1.748:                        ; Preds .B1.747                 ; Infreq
        mov       ecx, DWORD PTR [-260+ebp]                     ;71.10
        neg       ecx                                           ;71.10
        add       ecx, edx                                      ;71.10
        imul      ecx, ecx, 900                                 ;71.10
        mov       edx, DWORD PTR [-396+ebp]                     ;71.10
        mov       eax, DWORD PTR [-372+ebp]                     ;70.10
        inc       eax                                           ;70.10
        mov       edx, DWORD PTR [756+edx+ecx]                  ;71.10
        mov       ecx, DWORD PTR [-164+ebp]                     ;71.10
        mov       DWORD PTR [-372+ebp], eax                     ;70.10
        mov       DWORD PTR [-4+ecx+eax*4], edx                 ;71.10
        jmp       .B1.750       ; Prob 100%                     ;71.10
                                ; LOE esi edi
.B1.749:                        ; Preds .B1.737                 ; Infreq
        xor       eax, eax                                      ;
        mov       DWORD PTR [-372+ebp], eax                     ;
                                ; LOE esi edi
.B1.750:                        ; Preds .B1.748 .B1.747 .B1.746 .B1.749 ; Infreq
        push      32                                            ;75.11
        xor       eax, eax                                      ;75.11
        lea       edx, DWORD PTR [-152+ebp]                     ;75.11
        push      eax                                           ;75.11
        push      OFFSET FLAT: __STRLITPACK_7.0.1               ;75.11
        push      -2088435968                                   ;75.11
        push      301                                           ;75.11
        push      edx                                           ;75.11
        mov       DWORD PTR [-152+ebp], eax                     ;75.11
        call      _for_write_seq_lis                            ;75.11
                                ; LOE esi edi
.B1.939:                        ; Preds .B1.750                 ; Infreq
        add       esp, 24                                       ;75.11
                                ; LOE esi edi
.B1.751:                        ; Preds .B1.939                 ; Infreq
        cvtsi2ss  xmm0, DWORD PTR [-308+ebp]                    ;76.26
        push      32                                            ;76.11
        push      OFFSET FLAT: CAL_LINK_MOE$format_pack.0.1+140 ;76.11
        mulss     xmm0, DWORD PTR [-300+ebp]                    ;76.11
        lea       eax, DWORD PTR [-336+ebp]                     ;76.11
        push      eax                                           ;76.11
        push      OFFSET FLAT: __STRLITPACK_8.0.1               ;76.11
        push      -2088435968                                   ;76.11
        push      301                                           ;76.11
        mov       DWORD PTR [-152+ebp], 0                       ;76.11
        lea       edx, DWORD PTR [-152+ebp]                     ;76.11
        push      edx                                           ;76.11
        movss     DWORD PTR [-336+ebp], xmm0                    ;76.11
        call      _for_write_seq_fmt                            ;76.11
                                ; LOE esi edi
.B1.940:                        ; Preds .B1.751                 ; Infreq
        add       esp, 28                                       ;76.11
                                ; LOE esi edi
.B1.752:                        ; Preds .B1.940                 ; Infreq
        push      32                                            ;77.11
        push      OFFSET FLAT: CAL_LINK_MOE$format_pack.0.1+200 ;77.11
        xor       eax, eax                                      ;77.11
        lea       edx, DWORD PTR [-152+ebp]                     ;77.11
        push      eax                                           ;77.11
        push      OFFSET FLAT: __STRLITPACK_9.0.1               ;77.11
        push      -2088435968                                   ;77.11
        push      301                                           ;77.11
        push      edx                                           ;77.11
        mov       DWORD PTR [-152+ebp], eax                     ;77.11
        call      _for_write_seq_fmt                            ;77.11
                                ; LOE esi edi
.B1.941:                        ; Preds .B1.752                 ; Infreq
        add       esp, 28                                       ;77.11
                                ; LOE esi edi
.B1.753:                        ; Preds .B1.941                 ; Infreq
        cmp       DWORD PTR [-372+ebp], 0                       ;77.11
        jle       .B1.758       ; Prob 2%                       ;77.11
                                ; LOE esi edi
.B1.754:                        ; Preds .B1.753                 ; Infreq
        cvtsi2ss  xmm0, DWORD PTR [-384+ebp]                    ;77.39
        mov       eax, 1                                        ;
        mov       DWORD PTR [-264+ebp], edi                     ;77.39
        movss     DWORD PTR [-408+ebp], xmm0                    ;77.39
        mov       DWORD PTR [-116+ebp], esi                     ;77.39
        mov       esi, eax                                      ;77.39
        mov       edi, DWORD PTR [-164+ebp]                     ;77.39
                                ; LOE esi edi
.B1.755:                        ; Preds .B1.756 .B1.754         ; Infreq
        movss     xmm0, DWORD PTR [-4+edi+esi*4]                ;77.28
        lea       edx, DWORD PTR [-104+ebp]                     ;77.11
        divss     xmm0, DWORD PTR [-408+ebp]                    ;77.11
        push      edx                                           ;77.11
        push      OFFSET FLAT: __STRLITPACK_10.0.1              ;77.11
        movss     DWORD PTR [-104+ebp], xmm0                    ;77.11
        lea       ecx, DWORD PTR [-152+ebp]                     ;77.11
        push      ecx                                           ;77.11
        call      _for_write_seq_fmt_xmit                       ;77.11
                                ; LOE esi edi
.B1.942:                        ; Preds .B1.755                 ; Infreq
        add       esp, 12                                       ;77.11
                                ; LOE esi edi
.B1.756:                        ; Preds .B1.942                 ; Infreq
        inc       esi                                           ;77.11
        cmp       esi, DWORD PTR [-372+ebp]                     ;77.11
        jle       .B1.755       ; Prob 82%                      ;77.11
                                ; LOE esi edi
.B1.757:                        ; Preds .B1.756                 ; Infreq
        mov       esi, DWORD PTR [-116+ebp]                     ;
        mov       edi, DWORD PTR [-264+ebp]                     ;
                                ; LOE esi edi
.B1.758:                        ; Preds .B1.753 .B1.757         ; Infreq
        push      0                                             ;77.11
        push      OFFSET FLAT: __STRLITPACK_11.0.1              ;77.11
        lea       eax, DWORD PTR [-152+ebp]                     ;77.11
        push      eax                                           ;77.11
        call      _for_write_seq_fmt_xmit                       ;77.11
                                ; LOE esi edi
.B1.943:                        ; Preds .B1.758                 ; Infreq
        add       esp, 12                                       ;77.11
                                ; LOE esi edi
.B1.759:                        ; Preds .B1.943                 ; Infreq
        push      32                                            ;78.8
        xor       eax, eax                                      ;78.8
        lea       edx, DWORD PTR [-152+ebp]                     ;78.8
        push      eax                                           ;78.8
        push      OFFSET FLAT: __STRLITPACK_12.0.1              ;78.8
        push      -2088435968                                   ;78.8
        push      301                                           ;78.8
        push      edx                                           ;78.8
        mov       DWORD PTR [-152+ebp], eax                     ;78.8
        call      _for_write_seq_lis                            ;78.8
                                ; LOE esi edi
.B1.944:                        ; Preds .B1.759                 ; Infreq
        add       esp, 24                                       ;78.8
                                ; LOE esi edi
.B1.760:                        ; Preds .B1.944                 ; Infreq
        cmp       DWORD PTR [-188+ebp], 0                       ;79.11
        jle       .B1.17        ; Prob 50%                      ;79.11
                                ; LOE esi edi
.B1.761:                        ; Preds .B1.760                 ; Infreq
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;79.11
        mov       eax, DWORD PTR [-188+ebp]                     ;79.11
        mov       DWORD PTR [-408+ebp], ecx                     ;79.11
        mov       ecx, eax                                      ;79.11
        shr       ecx, 31                                       ;79.11
        add       ecx, eax                                      ;79.11
        sar       ecx, 1                                        ;79.11
        test      ecx, ecx                                      ;79.11
        jbe       .B1.769       ; Prob 10%                      ;79.11
                                ; LOE ecx esi edi
.B1.762:                        ; Preds .B1.761                 ; Infreq
        xor       edx, edx                                      ;
        mov       DWORD PTR [-116+ebp], esi                     ;
        xor       eax, eax                                      ;
        mov       DWORD PTR [-264+ebp], edi                     ;
        xor       edi, edi                                      ;
        mov       esi, DWORD PTR [-408+ebp]                     ;
                                ; LOE eax edx ecx esi edi
.B1.763:                        ; Preds .B1.763 .B1.762         ; Infreq
        inc       edx                                           ;79.11
        mov       DWORD PTR [756+eax+esi], edi                  ;79.11
        mov       DWORD PTR [1656+eax+esi], edi                 ;79.11
        add       eax, 1800                                     ;79.11
        cmp       edx, ecx                                      ;79.11
        jb        .B1.763       ; Prob 64%                      ;79.11
                                ; LOE eax edx ecx esi edi
.B1.764:                        ; Preds .B1.763                 ; Infreq
        mov       esi, DWORD PTR [-116+ebp]                     ;
        lea       eax, DWORD PTR [1+edx+edx]                    ;79.11
        mov       edi, DWORD PTR [-264+ebp]                     ;
                                ; LOE eax esi edi
.B1.765:                        ; Preds .B1.764 .B1.769         ; Infreq
        lea       edx, DWORD PTR [-1+eax]                       ;79.11
        cmp       edx, DWORD PTR [-188+ebp]                     ;79.11
        jae       .B1.17        ; Prob 10%                      ;79.11
                                ; LOE eax esi edi
.B1.766:                        ; Preds .B1.765                 ; Infreq
        mov       edx, DWORD PTR [-260+ebp]                     ;79.11
        mov       ecx, edx                                      ;79.11
        neg       ecx                                           ;79.11
        add       eax, edx                                      ;79.11
        add       ecx, eax                                      ;79.11
        imul      edx, ecx, 900                                 ;79.11
        mov       eax, DWORD PTR [-408+ebp]                     ;79.11
        mov       DWORD PTR [-144+eax+edx], 0                   ;79.11
        jmp       .B1.17        ; Prob 100%                     ;79.11
                                ; LOE esi edi
.B1.767:                        ; Preds .B1.730                 ; Infreq
        mov       edx, 1                                        ;
        jmp       .B1.734       ; Prob 100%                     ;
                                ; LOE edx esi edi
.B1.768:                        ; Preds .B1.738                 ; Infreq
        mov       edx, 1                                        ;
        jmp       .B1.746       ; Prob 100%                     ;
                                ; LOE edx ecx esi edi
.B1.769:                        ; Preds .B1.761                 ; Infreq
        mov       eax, 1                                        ;
        jmp       .B1.765       ; Prob 100%                     ;
                                ; LOE eax esi edi
.B1.770:                        ; Preds .B1.11                  ; Infreq
        test      esi, esi                                      ;40.9
        jle       .B1.777       ; Prob 2%                       ;40.9
                                ; LOE esi edi
.B1.771:                        ; Preds .B1.770                 ; Infreq
        mov       ecx, esi                                      ;41.44
        shr       ecx, 31                                       ;41.44
        add       ecx, esi                                      ;41.44
        sar       ecx, 1                                        ;41.44
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;41.11
        test      ecx, ecx                                      ;41.44
        mov       DWORD PTR [-408+ebp], eax                     ;41.11
        jbe       .B1.811       ; Prob 0%                       ;41.44
                                ; LOE ecx esi edi
.B1.772:                        ; Preds .B1.771                 ; Infreq
        imul      eax, DWORD PTR [-260+ebp], -900               ;
        xor       edx, edx                                      ;
        mov       DWORD PTR [-116+ebp], esi                     ;
        add       eax, DWORD PTR [-408+ebp]                     ;
        mov       esi, edx                                      ;
        mov       DWORD PTR [-264+ebp], edi                     ;
        ALIGN     16
                                ; LOE eax edx ecx esi
.B1.773:                        ; Preds .B1.773 .B1.772         ; Infreq
        pxor      xmm0, xmm0                                    ;41.77
        pxor      xmm1, xmm1                                    ;41.77
        movsx     edi, WORD PTR [1584+esi+eax]                  ;41.77
        inc       edx                                           ;41.44
        cvtsi2ss  xmm0, edi                                     ;41.77
        movsx     edi, WORD PTR [2484+esi+eax]                  ;41.77
        cvtsi2ss  xmm1, edi                                     ;41.77
        addss     xmm0, DWORD PTR [1652+esi+eax]                ;41.11
        addss     xmm1, DWORD PTR [2552+esi+eax]                ;41.11
        movss     DWORD PTR [1652+esi+eax], xmm0                ;41.11
        movss     DWORD PTR [2552+esi+eax], xmm1                ;41.11
        add       esi, 1800                                     ;41.44
        cmp       edx, ecx                                      ;41.44
        jb        .B1.773       ; Prob 64%                      ;41.44
                                ; LOE eax edx ecx esi
.B1.774:                        ; Preds .B1.773                 ; Infreq
        mov       esi, DWORD PTR [-116+ebp]                     ;
        lea       edx, DWORD PTR [1+edx+edx]                    ;41.44
        mov       edi, DWORD PTR [-264+ebp]                     ;
                                ; LOE edx esi edi
.B1.775:                        ; Preds .B1.774 .B1.811         ; Infreq
        lea       eax, DWORD PTR [-1+edx]                       ;41.44
        cmp       esi, eax                                      ;41.44
        jbe       .B1.777       ; Prob 0%                       ;41.44
                                ; LOE edx esi edi
.B1.776:                        ; Preds .B1.775                 ; Infreq
        mov       eax, DWORD PTR [-260+ebp]                     ;41.11
        neg       eax                                           ;41.11
        add       eax, edx                                      ;41.11
        imul      eax, eax, 900                                 ;41.11
        mov       ecx, DWORD PTR [-408+ebp]                     ;41.77
        movsx     edx, WORD PTR [684+ecx+eax]                   ;41.77
        cvtsi2ss  xmm0, edx                                     ;41.77
        addss     xmm0, DWORD PTR [752+ecx+eax]                 ;41.11
        movss     DWORD PTR [752+ecx+eax], xmm0                 ;41.11
                                ; LOE esi edi
.B1.777:                        ; Preds .B1.775 .B1.770 .B1.776 ; Infreq
        mov       ecx, DWORD PTR [8+ebx]                        ;43.8
        mov       eax, DWORD PTR [ecx]                          ;43.8
        cdq                                                     ;43.11
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_I30_T] ;43.8
        mov       DWORD PTR [-308+ebp], eax                     ;43.8
        idiv      ecx                                           ;43.11
        mov       DWORD PTR [-384+ebp], ecx                     ;43.8
        test      edx, edx                                      ;43.24
        jne       .B1.808       ; Prob 50%                      ;43.24
                                ; LOE esi edi
.B1.778:                        ; Preds .B1.777                 ; Infreq
        test      esi, esi                                      ;45.6
        jle       .B1.790       ; Prob 50%                      ;45.6
                                ; LOE esi edi
.B1.779:                        ; Preds .B1.778                 ; Infreq
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;56.11
        mov       DWORD PTR [-396+ebp], edx                     ;56.11
        mov       edx, esi                                      ;45.6
        shr       edx, 31                                       ;45.6
        add       edx, esi                                      ;45.6
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;46.8
        sar       edx, 1                                        ;45.6
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;46.8
        test      edx, edx                                      ;45.6
        mov       DWORD PTR [-400+ebp], eax                     ;46.8
        mov       eax, 0                                        ;
        mov       DWORD PTR [-404+ebp], ecx                     ;46.8
        mov       DWORD PTR [-372+ebp], eax                     ;
        jbe       .B1.809       ; Prob 3%                       ;45.6
                                ; LOE edx esi edi
.B1.780:                        ; Preds .B1.779                 ; Infreq
        imul      ecx, DWORD PTR [-260+ebp], -900               ;
        xor       eax, eax                                      ;
        add       ecx, DWORD PTR [-396+ebp]                     ;
        mov       DWORD PTR [-388+ebp], ecx                     ;
        imul      ecx, DWORD PTR [-404+ebp], -152               ;
        add       ecx, DWORD PTR [-400+ebp]                     ;
        mov       DWORD PTR [-408+ebp], eax                     ;
        mov       DWORD PTR [-392+ebp], eax                     ;
        mov       DWORD PTR [-376+ebp], ecx                     ;
        mov       DWORD PTR [-380+ebp], edx                     ;
        mov       DWORD PTR [-116+ebp], esi                     ;
        mov       DWORD PTR [-264+ebp], edi                     ;
        mov       edx, eax                                      ;
        mov       ecx, DWORD PTR [-408+ebp]                     ;
                                ; LOE eax edx ecx
.B1.781:                        ; Preds .B1.785 .B1.780         ; Infreq
        mov       esi, DWORD PTR [-376+ebp]                     ;46.12
        movsx     edi, WORD PTR [300+edx+esi]                   ;46.12
        cmp       edi, 99                                       ;46.50
        jge       .B1.783       ; Prob 50%                      ;46.50
                                ; LOE eax edx ecx
.B1.782:                        ; Preds .B1.781                 ; Infreq
        mov       esi, DWORD PTR [-388+ebp]                     ;48.10
        mov       DWORD PTR [-392+ebp], edx                     ;
        mov       edx, DWORD PTR [-372+ebp]                     ;47.10
        inc       edx                                           ;47.10
        mov       edi, DWORD PTR [1652+ecx+esi]                 ;48.10
        mov       esi, DWORD PTR [-164+ebp]                     ;48.10
        mov       DWORD PTR [-372+ebp], edx                     ;47.10
        mov       DWORD PTR [-4+esi+edx*4], edi                 ;48.10
        mov       edx, DWORD PTR [-392+ebp]                     ;48.10
                                ; LOE eax edx ecx
.B1.783:                        ; Preds .B1.782 .B1.781         ; Infreq
        mov       esi, DWORD PTR [-376+ebp]                     ;46.12
        movsx     edi, WORD PTR [452+edx+esi]                   ;46.12
        cmp       edi, 99                                       ;46.50
        jge       .B1.785       ; Prob 50%                      ;46.50
                                ; LOE eax edx ecx
.B1.784:                        ; Preds .B1.783                 ; Infreq
        mov       esi, DWORD PTR [-388+ebp]                     ;48.10
        mov       DWORD PTR [-392+ebp], edx                     ;
        mov       edx, DWORD PTR [-372+ebp]                     ;47.10
        inc       edx                                           ;47.10
        mov       edi, DWORD PTR [2552+ecx+esi]                 ;48.10
        mov       esi, DWORD PTR [-164+ebp]                     ;48.10
        mov       DWORD PTR [-372+ebp], edx                     ;47.10
        mov       DWORD PTR [-4+esi+edx*4], edi                 ;48.10
        mov       edx, DWORD PTR [-392+ebp]                     ;48.10
                                ; LOE eax edx ecx
.B1.785:                        ; Preds .B1.784 .B1.783         ; Infreq
        inc       eax                                           ;45.6
        add       ecx, 1800                                     ;45.6
        add       edx, 304                                      ;45.6
        cmp       eax, DWORD PTR [-380+ebp]                     ;45.6
        jb        .B1.781       ; Prob 64%                      ;45.6
                                ; LOE eax edx ecx
.B1.786:                        ; Preds .B1.785                 ; Infreq
        mov       esi, DWORD PTR [-116+ebp]                     ;
        lea       edx, DWORD PTR [1+eax+eax]                    ;45.6
        mov       edi, DWORD PTR [-264+ebp]                     ;
                                ; LOE edx esi edi
.B1.787:                        ; Preds .B1.786 .B1.809         ; Infreq
        lea       eax, DWORD PTR [-1+edx]                       ;45.6
        cmp       esi, eax                                      ;45.6
        jbe       .B1.791       ; Prob 3%                       ;45.6
                                ; LOE edx esi edi
.B1.788:                        ; Preds .B1.787                 ; Infreq
        mov       eax, DWORD PTR [-404+ebp]                     ;46.50
        neg       eax                                           ;46.50
        add       eax, edx                                      ;46.50
        imul      eax, eax, 152                                 ;46.50
        mov       ecx, DWORD PTR [-400+ebp]                     ;46.12
        movsx     ecx, WORD PTR [148+ecx+eax]                   ;46.12
        cmp       ecx, 99                                       ;46.50
        jge       .B1.791       ; Prob 50%                      ;46.50
                                ; LOE edx esi edi
.B1.789:                        ; Preds .B1.788                 ; Infreq
        mov       ecx, DWORD PTR [-260+ebp]                     ;48.10
        neg       ecx                                           ;48.10
        add       ecx, edx                                      ;48.10
        imul      ecx, ecx, 900                                 ;48.10
        mov       edx, DWORD PTR [-396+ebp]                     ;48.10
        mov       eax, DWORD PTR [-372+ebp]                     ;47.10
        inc       eax                                           ;47.10
        mov       edx, DWORD PTR [752+edx+ecx]                  ;48.10
        mov       ecx, DWORD PTR [-164+ebp]                     ;48.10
        mov       DWORD PTR [-372+ebp], eax                     ;47.10
        mov       DWORD PTR [-4+ecx+eax*4], edx                 ;48.10
        jmp       .B1.791       ; Prob 100%                     ;48.10
                                ; LOE esi edi
.B1.790:                        ; Preds .B1.778                 ; Infreq
        xor       eax, eax                                      ;
        mov       DWORD PTR [-372+ebp], eax                     ;
                                ; LOE esi edi
.B1.791:                        ; Preds .B1.789 .B1.788 .B1.787 .B1.790 ; Infreq
        push      32                                            ;52.11
        xor       eax, eax                                      ;52.11
        lea       edx, DWORD PTR [-152+ebp]                     ;52.11
        push      eax                                           ;52.11
        push      OFFSET FLAT: __STRLITPACK_1.0.1               ;52.11
        push      -2088435968                                   ;52.11
        push      30                                            ;52.11
        push      edx                                           ;52.11
        mov       DWORD PTR [-152+ebp], eax                     ;52.11
        call      _for_write_seq_lis                            ;52.11
                                ; LOE esi edi
.B1.945:                        ; Preds .B1.791                 ; Infreq
        add       esp, 24                                       ;52.11
                                ; LOE esi edi
.B1.792:                        ; Preds .B1.945                 ; Infreq
        cvtsi2ss  xmm1, DWORD PTR [-308+ebp]                    ;53.25
        push      32                                            ;53.11
        push      OFFSET FLAT: CAL_LINK_MOE$format_pack.0.1+140 ;53.11
        movss     xmm0, DWORD PTR [_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT] ;53.25
        lea       eax, DWORD PTR [-344+ebp]                     ;53.11
        push      eax                                           ;53.11
        push      OFFSET FLAT: __STRLITPACK_2.0.1               ;53.11
        mulss     xmm1, xmm0                                    ;53.11
        lea       edx, DWORD PTR [-152+ebp]                     ;53.11
        push      -2088435968                                   ;53.11
        push      30                                            ;53.11
        push      edx                                           ;53.11
        mov       DWORD PTR [-152+ebp], 0                       ;53.11
        movss     DWORD PTR [-300+ebp], xmm0                    ;53.25
        movss     DWORD PTR [-344+ebp], xmm1                    ;53.11
        call      _for_write_seq_fmt                            ;53.11
                                ; LOE esi edi
.B1.946:                        ; Preds .B1.792                 ; Infreq
        add       esp, 28                                       ;53.11
                                ; LOE esi edi
.B1.793:                        ; Preds .B1.946                 ; Infreq
        push      32                                            ;54.11
        push      OFFSET FLAT: CAL_LINK_MOE$format_pack.0.1+200 ;54.11
        xor       eax, eax                                      ;54.11
        lea       edx, DWORD PTR [-152+ebp]                     ;54.11
        push      eax                                           ;54.11
        push      OFFSET FLAT: __STRLITPACK_3.0.1               ;54.11
        push      -2088435968                                   ;54.11
        push      30                                            ;54.11
        push      edx                                           ;54.11
        mov       DWORD PTR [-152+ebp], eax                     ;54.11
        call      _for_write_seq_fmt                            ;54.11
                                ; LOE esi edi
.B1.947:                        ; Preds .B1.793                 ; Infreq
        add       esp, 28                                       ;54.11
                                ; LOE esi edi
.B1.794:                        ; Preds .B1.947                 ; Infreq
        cmp       DWORD PTR [-372+ebp], 0                       ;54.11
        jle       .B1.799       ; Prob 2%                       ;54.11
                                ; LOE esi edi
.B1.795:                        ; Preds .B1.794                 ; Infreq
        cvtsi2ss  xmm0, DWORD PTR [-384+ebp]                    ;54.38
        mov       eax, 1                                        ;
        mov       DWORD PTR [-264+ebp], edi                     ;54.38
        movss     DWORD PTR [-408+ebp], xmm0                    ;54.38
        mov       DWORD PTR [-116+ebp], esi                     ;54.38
        mov       esi, eax                                      ;54.38
        mov       edi, DWORD PTR [-164+ebp]                     ;54.38
                                ; LOE esi edi
.B1.796:                        ; Preds .B1.797 .B1.795         ; Infreq
        movss     xmm0, DWORD PTR [-4+edi+esi*4]                ;54.27
        lea       edx, DWORD PTR [-112+ebp]                     ;54.11
        divss     xmm0, DWORD PTR [-408+ebp]                    ;54.11
        push      edx                                           ;54.11
        push      OFFSET FLAT: __STRLITPACK_4.0.1               ;54.11
        movss     DWORD PTR [-112+ebp], xmm0                    ;54.11
        lea       ecx, DWORD PTR [-152+ebp]                     ;54.11
        push      ecx                                           ;54.11
        call      _for_write_seq_fmt_xmit                       ;54.11
                                ; LOE esi edi
.B1.948:                        ; Preds .B1.796                 ; Infreq
        add       esp, 12                                       ;54.11
                                ; LOE esi edi
.B1.797:                        ; Preds .B1.948                 ; Infreq
        inc       esi                                           ;54.11
        cmp       esi, DWORD PTR [-372+ebp]                     ;54.11
        jle       .B1.796       ; Prob 82%                      ;54.11
                                ; LOE esi edi
.B1.798:                        ; Preds .B1.797                 ; Infreq
        mov       esi, DWORD PTR [-116+ebp]                     ;
        mov       edi, DWORD PTR [-264+ebp]                     ;
                                ; LOE esi edi
.B1.799:                        ; Preds .B1.798 .B1.794         ; Infreq
        push      0                                             ;54.11
        push      OFFSET FLAT: __STRLITPACK_5.0.1               ;54.11
        lea       eax, DWORD PTR [-152+ebp]                     ;54.11
        push      eax                                           ;54.11
        call      _for_write_seq_fmt_xmit                       ;54.11
                                ; LOE esi edi
.B1.949:                        ; Preds .B1.799                 ; Infreq
        add       esp, 12                                       ;54.11
                                ; LOE esi edi
.B1.800:                        ; Preds .B1.949                 ; Infreq
        push      32                                            ;55.8
        xor       eax, eax                                      ;55.8
        lea       edx, DWORD PTR [-152+ebp]                     ;55.8
        push      eax                                           ;55.8
        push      OFFSET FLAT: __STRLITPACK_6.0.1               ;55.8
        push      -2088435968                                   ;55.8
        push      30                                            ;55.8
        push      edx                                           ;55.8
        mov       DWORD PTR [-152+ebp], eax                     ;55.8
        call      _for_write_seq_lis                            ;55.8
                                ; LOE esi edi
.B1.950:                        ; Preds .B1.800                 ; Infreq
        add       esp, 24                                       ;55.8
                                ; LOE esi edi
.B1.801:                        ; Preds .B1.950                 ; Infreq
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+24] ;56.11
        test      eax, eax                                      ;56.11
        mov       DWORD PTR [-188+ebp], eax                     ;56.11
        jle       .B1.13        ; Prob 50%                      ;56.11
                                ; LOE eax esi edi al ah
.B1.802:                        ; Preds .B1.801                 ; Infreq
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;56.11
        mov       DWORD PTR [-408+ebp], ecx                     ;56.11
        mov       ecx, eax                                      ;56.11
        shr       ecx, 31                                       ;56.11
        add       ecx, eax                                      ;56.11
        sar       ecx, 1                                        ;56.11
        test      ecx, ecx                                      ;56.11
        jbe       .B1.810       ; Prob 10%                      ;56.11
                                ; LOE ecx esi edi
.B1.803:                        ; Preds .B1.802                 ; Infreq
        xor       edx, edx                                      ;
        mov       DWORD PTR [-116+ebp], esi                     ;
        xor       eax, eax                                      ;
        mov       DWORD PTR [-264+ebp], edi                     ;
        xor       edi, edi                                      ;
        mov       esi, DWORD PTR [-408+ebp]                     ;
                                ; LOE eax edx ecx esi edi
.B1.804:                        ; Preds .B1.804 .B1.803         ; Infreq
        inc       edx                                           ;56.11
        mov       DWORD PTR [752+eax+esi], edi                  ;56.11
        mov       DWORD PTR [1652+eax+esi], edi                 ;56.11
        add       eax, 1800                                     ;56.11
        cmp       edx, ecx                                      ;56.11
        jb        .B1.804       ; Prob 64%                      ;56.11
                                ; LOE eax edx ecx esi edi
.B1.805:                        ; Preds .B1.804                 ; Infreq
        mov       esi, DWORD PTR [-116+ebp]                     ;
        lea       eax, DWORD PTR [1+edx+edx]                    ;56.11
        mov       edi, DWORD PTR [-264+ebp]                     ;
                                ; LOE eax esi edi
.B1.806:                        ; Preds .B1.805 .B1.810         ; Infreq
        lea       edx, DWORD PTR [-1+eax]                       ;56.11
        cmp       edx, DWORD PTR [-188+ebp]                     ;56.11
        jae       .B1.13        ; Prob 10%                      ;56.11
                                ; LOE eax esi edi
.B1.807:                        ; Preds .B1.806                 ; Infreq
        mov       edx, DWORD PTR [-260+ebp]                     ;56.11
        mov       ecx, edx                                      ;56.11
        neg       ecx                                           ;56.11
        add       eax, edx                                      ;56.11
        add       ecx, eax                                      ;56.11
        imul      edx, ecx, 900                                 ;56.11
        mov       eax, DWORD PTR [-408+ebp]                     ;56.11
        mov       DWORD PTR [-148+eax+edx], 0                   ;56.11
        jmp       .B1.13        ; Prob 100%                     ;56.11
                                ; LOE esi edi
.B1.808:                        ; Preds .B1.777                 ; Infreq
        movss     xmm0, DWORD PTR [_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT] ;76.26
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+24] ;79.11
        movss     DWORD PTR [-300+ebp], xmm0                    ;76.26
        mov       DWORD PTR [-188+ebp], eax                     ;79.11
        jmp       .B1.13        ; Prob 100%                     ;79.11
                                ; LOE esi edi
.B1.809:                        ; Preds .B1.779                 ; Infreq
        mov       edx, 1                                        ;
        jmp       .B1.787       ; Prob 100%                     ;
                                ; LOE edx esi edi
.B1.810:                        ; Preds .B1.802                 ; Infreq
        mov       eax, 1                                        ;
        jmp       .B1.806       ; Prob 100%                     ;
                                ; LOE eax esi edi
.B1.811:                        ; Preds .B1.771                 ; Infreq
        mov       edx, 1                                        ;
        jmp       .B1.775       ; Prob 100%                     ;
                                ; LOE edx esi edi
.B1.812:                        ; Preds .B1.2                   ; Infreq
        cmp       edi, 8                                        ;33.2
        jl        .B1.820       ; Prob 10%                      ;33.2
                                ; LOE edi
.B1.813:                        ; Preds .B1.812                 ; Infreq
        mov       eax, edi                                      ;33.2
        xor       edx, edx                                      ;33.2
        mov       ecx, DWORD PTR [-164+ebp]                     ;33.2
        and       eax, -8                                       ;33.2
        pxor      xmm0, xmm0                                    ;33.2
                                ; LOE eax edx ecx edi xmm0
.B1.814:                        ; Preds .B1.814 .B1.813         ; Infreq
        movups    XMMWORD PTR [ecx+edx*4], xmm0                 ;33.2
        movups    XMMWORD PTR [16+ecx+edx*4], xmm0              ;33.2
        add       edx, 8                                        ;33.2
        cmp       edx, eax                                      ;33.2
        jb        .B1.814       ; Prob 82%                      ;33.2
                                ; LOE eax edx ecx edi xmm0
.B1.816:                        ; Preds .B1.814 .B1.820         ; Infreq
        cmp       eax, edi                                      ;33.2
        jae       .B1.819       ; Prob 10%                      ;33.2
                                ; LOE eax edi
.B1.817:                        ; Preds .B1.816                 ; Infreq
        mov       edx, DWORD PTR [-164+ebp]                     ;
        xor       ecx, ecx                                      ;
                                ; LOE eax edx ecx edi
.B1.818:                        ; Preds .B1.818 .B1.817         ; Infreq
        mov       DWORD PTR [edx+eax*4], ecx                    ;33.2
        inc       eax                                           ;33.2
        cmp       eax, edi                                      ;33.2
        jb        .B1.818       ; Prob 82%                      ;33.2
                                ; LOE eax edx ecx edi
.B1.819:                        ; Preds .B1.816 .B1.818         ; Infreq
        test      edi, edi                                      ;1.12
        jmp       .B1.4         ; Prob 100%                     ;1.12
                                ; LOE edi
.B1.820:                        ; Preds .B1.812                 ; Infreq
        xor       eax, eax                                      ;33.2
        jmp       .B1.816       ; Prob 100%                     ;33.2
                                ; LOE eax edi
.B1.821:                        ; Preds .B1.189                 ; Infreq
        cmp       DWORD PTR [-364+ebp], 8                       ;35.2
        jl        .B1.832       ; Prob 10%                      ;35.2
                                ; LOE eax edx esi edi
.B1.822:                        ; Preds .B1.821                 ; Infreq
        mov       ecx, DWORD PTR [-384+ebp]                     ;35.2
        mov       DWORD PTR [-360+ebp], ecx                     ;35.2
        pxor      xmm0, xmm0                                    ;
        mov       ecx, DWORD PTR [-380+ebp]                     ;
        mov       DWORD PTR [-372+ebp], 0                       ;35.2
        mov       DWORD PTR [-408+ebp], eax                     ;
        mov       DWORD PTR [-404+ebp], edx                     ;
        add       ecx, esi                                      ;
        mov       DWORD PTR [-376+ebp], ecx                     ;
        mov       ecx, DWORD PTR [-108+ebp]                     ;
        mov       DWORD PTR [-400+ebp], edi                     ;
        mov       edx, DWORD PTR [-376+ebp]                     ;
        mov       edi, DWORD PTR [-384+ebp]                     ;
        add       ecx, esi                                      ;
        mov       eax, ecx                                      ;
        mov       ecx, DWORD PTR [-372+ebp]                     ;
                                ; LOE eax edx ecx esi edi xmm0
.B1.823:                        ; Preds .B1.823 .B1.822         ; Infreq
        movups    XMMWORD PTR [eax+ecx*4], xmm0                 ;35.2
        movups    XMMWORD PTR [16+edx+ecx*4], xmm0              ;35.2
        add       ecx, 8                                        ;35.2
        cmp       ecx, edi                                      ;35.2
        jb        .B1.823       ; Prob 82%                      ;35.2
                                ; LOE eax edx ecx esi edi xmm0
.B1.824:                        ; Preds .B1.823                 ; Infreq
        mov       eax, DWORD PTR [-408+ebp]                     ;
        mov       edx, DWORD PTR [-404+ebp]                     ;
        mov       edi, DWORD PTR [-400+ebp]                     ;
                                ; LOE eax edx esi edi
.B1.825:                        ; Preds .B1.824 .B1.832         ; Infreq
        mov       ecx, DWORD PTR [-360+ebp]                     ;35.2
        cmp       ecx, DWORD PTR [-364+ebp]                     ;35.2
        jae       .B1.830       ; Prob 10%                      ;35.2
                                ; LOE eax edx ecx esi edi cl ch
.B1.826:                        ; Preds .B1.825                 ; Infreq
        mov       DWORD PTR [-408+ebp], eax                     ;
        mov       DWORD PTR [-400+ebp], edi                     ;
        xor       edi, edi                                      ;
        mov       eax, ecx                                      ;
        mov       ecx, DWORD PTR [-364+ebp]                     ;
                                ; LOE eax edx ecx esi edi
.B1.827:                        ; Preds .B1.827 .B1.826         ; Infreq
        mov       DWORD PTR [edx+eax*4], edi                    ;35.2
        inc       eax                                           ;35.2
        cmp       eax, ecx                                      ;35.2
        jb        .B1.827       ; Prob 82%                      ;35.2
                                ; LOE eax edx ecx esi edi
.B1.828:                        ; Preds .B1.827                 ; Infreq
        mov       edi, DWORD PTR [-400+ebp]                     ;
        inc       edi                                           ;35.2
        mov       eax, DWORD PTR [-408+ebp]                     ;
        mov       ecx, DWORD PTR [-396+ebp]                     ;35.2
        add       edx, ecx                                      ;35.2
        add       eax, ecx                                      ;35.2
        add       esi, ecx                                      ;35.2
        cmp       edi, DWORD PTR [-392+ebp]                     ;35.2
        jb        .B1.189       ; Prob 82%                      ;35.2
                                ; LOE eax edx esi edi
.B1.829:                        ; Preds .B1.828                 ; Infreq
        mov       edi, DWORD PTR [-264+ebp]                     ;
        jmp       .B1.8         ; Prob 100%                     ;
                                ; LOE edi
.B1.830:                        ; Preds .B1.825                 ; Infreq
        inc       edi                                           ;35.2
        mov       ecx, DWORD PTR [-396+ebp]                     ;35.2
        add       edx, ecx                                      ;35.2
        add       eax, ecx                                      ;35.2
        add       esi, ecx                                      ;35.2
        cmp       edi, DWORD PTR [-392+ebp]                     ;35.2
        jb        .B1.189       ; Prob 82%                      ;35.2
                                ; LOE eax edx esi edi
.B1.831:                        ; Preds .B1.830                 ; Infreq
        mov       edi, DWORD PTR [-264+ebp]                     ;
        jmp       .B1.8         ; Prob 100%                     ;
                                ; LOE edi
.B1.832:                        ; Preds .B1.821                 ; Infreq
        xor       ecx, ecx                                      ;35.2
        mov       DWORD PTR [-360+ebp], ecx                     ;35.2
        jmp       .B1.825       ; Prob 100%                     ;35.2
                                ; LOE eax edx esi edi
.B1.833:                        ; Preds .B1.9                   ; Infreq
        cmp       edi, 8                                        ;37.5
        jl        .B1.841       ; Prob 10%                      ;37.5
                                ; LOE edi
.B1.834:                        ; Preds .B1.833                 ; Infreq
        mov       eax, edi                                      ;37.5
        xor       edx, edx                                      ;37.5
        mov       ecx, DWORD PTR [-164+ebp]                     ;37.5
        and       eax, -8                                       ;37.5
        pxor      xmm0, xmm0                                    ;37.5
                                ; LOE eax edx ecx edi xmm0
.B1.835:                        ; Preds .B1.835 .B1.834         ; Infreq
        movups    XMMWORD PTR [ecx+edx*4], xmm0                 ;37.5
        movups    XMMWORD PTR [16+ecx+edx*4], xmm0              ;37.5
        add       edx, 8                                        ;37.5
        cmp       edx, eax                                      ;37.5
        jb        .B1.835       ; Prob 82%                      ;37.5
                                ; LOE eax edx ecx edi xmm0
.B1.837:                        ; Preds .B1.835 .B1.841         ; Infreq
        cmp       eax, edi                                      ;37.5
        jae       .B1.11        ; Prob 10%                      ;37.5
                                ; LOE eax edi
.B1.838:                        ; Preds .B1.837                 ; Infreq
        mov       edx, DWORD PTR [-164+ebp]                     ;
        xor       ecx, ecx                                      ;
                                ; LOE eax edx ecx edi
.B1.839:                        ; Preds .B1.839 .B1.838         ; Infreq
        mov       DWORD PTR [edx+eax*4], ecx                    ;37.5
        inc       eax                                           ;37.5
        cmp       eax, edi                                      ;37.5
        jb        .B1.839       ; Prob 82%                      ;37.5
        jmp       .B1.11        ; Prob 100%                     ;37.5
                                ; LOE eax edx ecx edi
.B1.841:                        ; Preds .B1.833                 ; Infreq
        xor       eax, eax                                      ;37.5
        jmp       .B1.837       ; Prob 100%                     ;37.5
        ALIGN     16
                                ; LOE eax edi
; mark_end;
_CAL_LINK_MOE ENDP
_TEXT	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
CAL_LINK_MOE$format_pack.0.1	DB	54
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	2
	DB	10
	DB	0
	DB	0
	DB	0
	DB	14
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	5
	DB	10
	DB	0
	DB	0
	DB	0
	DB	11
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	5
	DB	10
	DB	0
	DB	0
	DB	0
	DB	11
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	5
	DB	10
	DB	0
	DB	0
	DB	0
	DB	11
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	5
	DB	10
	DB	0
	DB	0
	DB	0
	DB	11
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	5
	DB	10
	DB	0
	DB	0
	DB	0
	DB	11
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	5
	DB	10
	DB	0
	DB	0
	DB	0
	DB	11
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	1
	DB	1
	DB	0
	DB	0
	DB	0
	DB	8
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	5
	DB	10
	DB	0
	DB	0
	DB	0
	DB	11
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	5
	DB	10
	DB	0
	DB	0
	DB	0
	DB	11
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	5
	DB	10
	DB	0
	DB	0
	DB	0
	DB	11
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
__STRLITPACK_1.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_2.0.1	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_3.0.1	DB	2
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_4.0.1	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_5.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_6.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_7.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_8.0.1	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_9.0.1	DB	2
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_10.0.1	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_11.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_12.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_13.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_14.0.1	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_15.0.1	DB	2
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_16.0.1	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_17.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_18.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_19.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_20.0.1	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_21.0.1	DB	2
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_22.0.1	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_23.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_24.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_25.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_26.0.1	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_27.0.1	DB	2
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_28.0.1	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_29.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_30.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_31.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_32.0.1	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_33.0.1	DB	2
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_34.0.1	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_35.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_36.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_37.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_38.0.1	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_39.0.1	DB	2
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_40.0.1	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_41.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_42.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_43.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_44.0.1	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_45.0.1	DB	2
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_46.0.1	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_47.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_48.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_49.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_50.0.1	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_51.0.1	DB	2
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_52.0.1	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_53.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_54.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_55.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_56.0.1	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_57.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_58.0.1	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_59.0.1	DB	2
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_60.0.1	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_61.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_62.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_63.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_64.0.1	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_65.0.1	DB	26
	DB	7
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_66.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_67.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_68.0.1	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_69.0.1	DB	26
	DB	7
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_70.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_71.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_72.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_73.0.1	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_74.0.1	DB	26
	DB	7
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_75.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_76.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_77.0.1	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_78.0.1	DB	26
	DB	7
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_79.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_80.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_81.0.1	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_82.0.1	DB	2
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_83.0.1	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_84.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_85.0.1	DB	1
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _CAL_LINK_MOE
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DB 1 DUP ( 0H)	; pad
_2il0floatpacket.11	DD	03a83126fH,03a83126fH,03a83126fH,03a83126fH
_2il0floatpacket.12	DD	03f800000H,03f800000H,03f800000H,03f800000H
__STRLITPACK_0	DB	113
	DB	117
	DB	101
	DB	117
	DB	101
	DB	32
	DB	62
	DB	32
	DB	49
	DB	46
	DB	48
	DB	0
_2il0floatpacket.9	DD	03a83126fH
_2il0floatpacket.10	DD	042700000H
_2il0floatpacket.13	DD	03c23d70aH
_2il0floatpacket.14	DD	080000000H
_2il0floatpacket.15	DD	04b000000H
_2il0floatpacket.16	DD	03f000000H
_2il0floatpacket.17	DD	0bf000000H
_2il0floatpacket.18	DD	03f800000H
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
EXTRN	_DYNUST_MAIN_MODULE_mp_TRUCKOK:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_I40:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_I39_T:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_I39:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_I38_T:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_DE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_I38:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_I37_T:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_I37:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_I36_T:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_I36:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_I35_T:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_I35:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_I34_T:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_I34:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_I33_T:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_I33:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_I32_T:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_I32:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_I31_T:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_I31:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_I301_T:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_I301:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_XMINPERSIMINT:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_I30_T:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_I30:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_MAXMOVE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NOOFARCS:BYTE
_DATA	ENDS
EXTRN	_for_dealloc_allocatable:PROC
EXTRN	_for_write_seq_fmt_xmit:PROC
EXTRN	_for_write_seq_fmt:PROC
EXTRN	_for_write_seq_lis:PROC
EXTRN	_for_alloc_allocatable:PROC
EXTRN	_for_check_mult_overflow:PROC
EXTRN	__intel_fast_memset:PROC
EXTRN	__alloca_probe:PROC
EXTRN	__fltused:BYTE
	INCLUDELIB <ifconsol>
	INCLUDELIB <libifcoremt>
	INCLUDELIB <libifport>
	INCLUDELIB <libmmt>
	INCLUDELIB <LIBCMT>
	INCLUDELIB <libirc>
	INCLUDELIB <svml_dispmt>
	INCLUDELIB <OLDNAMES>
	END
