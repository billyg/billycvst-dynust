      SUBROUTINE GENERATE_VEH_ATTRIBUTES(t,ilink,i,j,jdst)
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~ Copyright (C) 2002-2011 The Arizona Board of Regents on Behalf of The University of Arizona.  ~~!
!~~ Developed by Yi-Chang Chiu, Ph.D. as par to the DynusT project.                               ~~!
!~~ This program is free software: you can redistribute it and/or modify it                       ~~!
!~~ under the terms of the GNU General Public License version 2 as published by                   ~~!
!~~ the Free Software Foundation.                                                                 ~~!
!~~ This program is distributed in the hope that it will be useful,                               ~~!
!~~ but WITHOUT ANY WARRANTY; without even the implied warranty of                                ~~!
!~~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                                          ~~!
!~~ See the GNU General Public License for more details.                                          ~~!
!~~ You should have received a copy of the GNU General Public License                             ~~!
!~~ along with this program (in the file COPYING).                                                ~~!
!~~ IF not, see <http://www.gnu.org/licenses/>.                                                   ~~!
!~~ Please see http://www.dynust.net for contact information and names                            ~~!
!~~ of individual contributors.                                                                   ~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
    USE DYNUST_MAIN_MODULE
    USE DYNUST_VEH_MODULE
    USE DYNUST_NETWORK_MODULE
    USE DYNUST_FUEL_MODULE
    USE RAND_GEN_INT
    !USE DYNUST_MOVES_MODULE
        
	REAL  CumDemtmp(10) ! CumDemTmp() is the temporary array the store the accum prob for a gen link from which it receives demand
    INTEGER misscount,ilink
	REAL r0, r1, r2, r3, r4
    LOGICAL mfind
    
    INTEGER resct

!> > > > > > > > > >                < < < < < < < < < < <
!>>>>>>>>>>>>>>>>>>>CODES START HERE<<<<<<<<<<<<<<<<<<<<<
!> > > > > > > > > >                < < < < < < < < < < <

    IF(.not.checkflaginit) THEN
      ALLOCATE(dstmp2(3,nzones,nzones))
      dstmp2(:,:,:)%pec = 0
      dstmp2(:,:,:)%num = 0      
    ENDIF
    resct = 0

800 CONTINUE    
    IF(.not.checkflag) THEN ! checkflag is the global variable. It becomes true after this sub been CALL the first time
      numActZone(:,:) = nzones ! first time to check this afte reading the new demand
    ENDIF

    missct = 0
    
    r0 = ranxy(19) 

    IF((backpointr(m_dynust_network_arc_nde(ilink)%iunod+1)-backpointr(m_dynust_network_arc_nde(ilink)%iunod) < 1).and.m_dynust_network_arc_nde(ilink)%link_iden /= 5) THEN ! no inbound link - terminal nodes
      m_dynust_veh(j)%position= nint(m_dynust_network_arc_nde(ilink)%s*100000)/100000.0 ! load vehicles at the upstream of the link
    ELSE
      m_dynust_veh(j)%position= nint(m_dynust_network_arc_nde(ilink)%s*r0*100000)/100000.0 ! load vehicles randomly on the link
    ENDIF

    m_dynust_veh_nde(j)%xparinit = m_dynust_veh(j)%position
    ise1 = m_dynust_network_node_nde(m_dynust_network_arc_nde(ilink)%iunod)%IntoOutNodeNum
    ise2 = m_dynust_network_node_nde(m_dynust_network_arc_nde(ilink)%idnod)%IntoOutNodeNum
    IF(t >= starttm.and.t < endtm) THEN
      m_dynust_veh(j)%itag=1
    ELSE
      m_dynust_veh(j)%itag=0
      numNtag = numNtag + 1	 	    
    ENDIF
    m_dynust_last_stand(j)%stime=t
  
    mDemID = i
    misscount = 0

100   CONTINUE

!   Assign ID to this vehicle. Note that since this is in demand gen mode, the ID is the same as internal ID
!   But if RunMode == 0 or 2, then Vehicle ID is the first column of vehicle.dat    
    m_dynust_veh_nde(j)%ID = j
    
! IF this sub is called for the first time or IF there is a misflag on    
    mfind = .False.
    dstotal = 0
    IF(.not.checkflag.or.misflag) THEN 
        numActZone(:,:) = nzones
        DO isg = 1, nzones
           dstmp2(1,:,isg)%pec = DemOrigAcu(:,isg)
           dstmp2(1,:,isg)%num = isg
           dstmp2(2,:,isg)%pec = DemOrigAcuT(:,isg)  
           dstmp2(2,:,nzones)%pec = 1.0                    
           dstmp2(2,:,isg)%num = isg
           dstmp2(3,:,isg)%pec = DemOrigAcuH(:,isg)  
           dstmp2(3,:,nzones)%pec = 1.0                 
           dstmp2(3,:,isg)%num = isg
        ENDDO
        dstmp2(:,:,nzones)%pec = 1.0
    ENDIF

299   CONTINUE

       r2 = ranxy(20) 

       DO isg = 1, numActZone(m_dynust_last_stand(j)%vehtype,m_dynust_last_stand(j)%jorig)
        IF(r2 < dstmp2(m_dynust_last_stand(j)%vehtype,m_dynust_last_stand(j)%jorig,isg)%pec) exit
       ENDDO
       is = isg
       m_dynust_last_stand(j)%jdest = is
       m_dynust_last_stand(j)%jdestNode = OutToInNodeNum(ConZoneTmp(is,1))
       
101 CONTINUE     

       m_dynust_veh(j)%DestVisit = 1 
       m_dynust_veh(j)%NoOfIntDst = 1
       m_dynust_veh_nde(j)%IntDestZone(m_dynust_veh(j)%NoOfIntDst)=m_dynust_last_stand(j)%jdest ! jdest carries original zone number        

       jdst = m_dynust_last_stand(j)%jdest

       r2 = ranxy(23) 
       is = m_dynust_last_stand(j)%vehtype
              
       DO is2 = 1, 5
         IF(r2 < vehprofile(is)%mtcfrac(is2)) exit
       ENDDO
       m_dynust_last_stand(j)%vehclass = is2       

       readveh(is,is2) = readveh(is,is2) + 1

       IF(m_dynust_last_stand(j)%vehclass == 4.or.m_dynust_last_stand(j)%vehtype >= 4) THEN ! the default value for these 3 arrays are 0, given values only IF with in-vehicle information (BR)
          m_dynust_veh_nde(j)%info = 1
       ENDIF  
       m_dynust_veh_nde(j)%ribf=ribfa

       m_dynust_veh_nde(j)%compliance=com_frac

       r3 = ranxy(24) 
         
       IF(m_dynust_last_stand(j)%vehtype == 2) THEN
         m_dynust_veh(j)%vhpce=nint(1.5*AttScale) ! according to HCM 1998, it should be 1.5 as default
       ENDIF


       mtc_veh(m_dynust_last_stand(j)%vehclass)=mtc_veh(m_dynust_last_stand(j)%vehclass)+1   

      IF(.not.checkflag) checkflag = .true.
      IF(.not.checkflaginit) checkflaginit = .true.      
      
      !IF(MOVESFlag > 0) THEN
      !  CALL AssignMovesType(j,itype)
      !  m_dynust_veh_nde(j)%MovesType = itype       
      !ENDIF

END SUBROUTINE