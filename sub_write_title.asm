; mark_description "Intel(R) Visual Fortran Compiler XE for applications running on IA-32, Version 13.1.1.171 Build 20130313";
; mark_description "-S";
	.686P
 	.387
	OPTION DOTNAME
	ASSUME	CS:FLAT,DS:FLAT,SS:FLAT
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
TXTST0:
; -- Begin  _WRITE_TITLE
; mark_begin;
IF @Version GE 800
  .MMX
ELSEIF @Version GE 612
  .MMX
  MMWORD TEXTEQU <QWORD>
ENDIF
IF @Version GE 800
  .XMM
ELSEIF @Version GE 614
  .XMM
  XMMWORD TEXTEQU <OWORD>
ENDIF
       ALIGN     16
	PUBLIC _WRITE_TITLE
_WRITE_TITLE	PROC NEAR 
.B1.1:                          ; Preds .B1.0
        push      ebp                                           ;1.18
        mov       ebp, esp                                      ;1.18
        and       esp, -16                                      ;1.18
        push      esi                                           ;1.18
        push      edi                                           ;1.18
        push      ebx                                           ;1.18
        sub       esp, 596                                      ;1.18
        xor       edi, edi                                      ;25.7
        mov       DWORD PTR [496+esp], edi                      ;25.7
        lea       esi, DWORD PTR [496+esp]                      ;25.7
        push      32                                            ;25.7
        push      edi                                           ;25.7
        push      OFFSET FLAT: __STRLITPACK_173.0.1             ;25.7
        push      -2088435968                                   ;25.7
        push      666                                           ;25.7
        push      esi                                           ;25.7
        call      _for_write_seq_lis                            ;25.7
                                ; LOE esi edi
.B1.2:                          ; Preds .B1.1
        mov       DWORD PTR [520+esp], edi                      ;26.7
        push      32                                            ;26.7
        push      OFFSET FLAT: WRITE_TITLE$format_pack.0.1+192  ;26.7
        push      edi                                           ;26.7
        push      OFFSET FLAT: __STRLITPACK_174.0.1             ;26.7
        push      -2088435968                                   ;26.7
        push      666                                           ;26.7
        push      esi                                           ;26.7
        call      _for_write_seq_fmt                            ;26.7
                                ; LOE esi edi
.B1.3:                          ; Preds .B1.2
        mov       DWORD PTR [548+esp], edi                      ;27.7
        push      32                                            ;27.7
        push      OFFSET FLAT: WRITE_TITLE$format_pack.0.1+276  ;27.7
        push      edi                                           ;27.7
        push      OFFSET FLAT: __STRLITPACK_175.0.1             ;27.7
        push      -2088435968                                   ;27.7
        push      666                                           ;27.7
        push      esi                                           ;27.7
        call      _for_write_seq_fmt                            ;27.7
                                ; LOE esi edi
.B1.4:                          ; Preds .B1.3
        mov       DWORD PTR [576+esp], edi                      ;28.7
        push      32                                            ;28.7
        push      OFFSET FLAT: WRITE_TITLE$format_pack.0.1+360  ;28.7
        push      edi                                           ;28.7
        push      OFFSET FLAT: __STRLITPACK_176.0.1             ;28.7
        push      -2088435968                                   ;28.7
        push      666                                           ;28.7
        push      esi                                           ;28.7
        call      _for_write_seq_fmt                            ;28.7
                                ; LOE esi edi
.B1.125:                        ; Preds .B1.4
        add       esp, 108                                      ;28.7
                                ; LOE esi edi
.B1.5:                          ; Preds .B1.125
        mov       DWORD PTR [496+esp], edi                      ;29.4
        push      32                                            ;29.4
        push      OFFSET FLAT: WRITE_TITLE$format_pack.0.1+444  ;29.4
        push      edi                                           ;29.4
        push      OFFSET FLAT: __STRLITPACK_177.0.1             ;29.4
        push      -2088435968                                   ;29.4
        push      666                                           ;29.4
        push      esi                                           ;29.4
        call      _for_write_seq_fmt                            ;29.4
                                ; LOE esi edi
.B1.6:                          ; Preds .B1.5
        mov       DWORD PTR [524+esp], edi                      ;30.4
        push      32                                            ;30.4
        push      OFFSET FLAT: WRITE_TITLE$format_pack.0.1+528  ;30.4
        push      edi                                           ;30.4
        push      OFFSET FLAT: __STRLITPACK_178.0.1             ;30.4
        push      -2088435968                                   ;30.4
        push      666                                           ;30.4
        push      esi                                           ;30.4
        call      _for_write_seq_fmt                            ;30.4
                                ; LOE esi edi
.B1.7:                          ; Preds .B1.6
        mov       DWORD PTR [552+esp], edi                      ;31.4
        push      32                                            ;31.4
        push      OFFSET FLAT: WRITE_TITLE$format_pack.0.1+612  ;31.4
        push      edi                                           ;31.4
        push      OFFSET FLAT: __STRLITPACK_179.0.1             ;31.4
        push      -2088435968                                   ;31.4
        push      666                                           ;31.4
        push      esi                                           ;31.4
        call      _for_write_seq_fmt                            ;31.4
                                ; LOE esi edi
.B1.8:                          ; Preds .B1.7
        mov       DWORD PTR [580+esp], edi                      ;32.4
        push      32                                            ;32.4
        push      OFFSET FLAT: WRITE_TITLE$format_pack.0.1+696  ;32.4
        push      edi                                           ;32.4
        push      OFFSET FLAT: __STRLITPACK_180.0.1             ;32.4
        push      -2088435968                                   ;32.4
        push      666                                           ;32.4
        push      esi                                           ;32.4
        call      _for_write_seq_fmt                            ;32.4
                                ; LOE esi edi
.B1.126:                        ; Preds .B1.8
        add       esp, 112                                      ;32.4
                                ; LOE esi edi
.B1.9:                          ; Preds .B1.126
        mov       DWORD PTR [496+esp], edi                      ;33.4
        push      32                                            ;33.4
        push      OFFSET FLAT: WRITE_TITLE$format_pack.0.1+780  ;33.4
        push      edi                                           ;33.4
        push      OFFSET FLAT: __STRLITPACK_181.0.1             ;33.4
        push      -2088435968                                   ;33.4
        push      666                                           ;33.4
        push      esi                                           ;33.4
        call      _for_write_seq_fmt                            ;33.4
                                ; LOE esi edi
.B1.10:                         ; Preds .B1.9
        mov       DWORD PTR [524+esp], edi                      ;34.4
        push      32                                            ;34.4
        push      OFFSET FLAT: WRITE_TITLE$format_pack.0.1+864  ;34.4
        push      edi                                           ;34.4
        push      OFFSET FLAT: __STRLITPACK_182.0.1             ;34.4
        push      -2088435968                                   ;34.4
        push      666                                           ;34.4
        push      esi                                           ;34.4
        call      _for_write_seq_fmt                            ;34.4
                                ; LOE esi edi
.B1.11:                         ; Preds .B1.10
        mov       DWORD PTR [552+esp], edi                      ;35.4
        push      32                                            ;35.4
        push      OFFSET FLAT: WRITE_TITLE$format_pack.0.1+948  ;35.4
        push      edi                                           ;35.4
        push      OFFSET FLAT: __STRLITPACK_183.0.1             ;35.4
        push      -2088435968                                   ;35.4
        push      666                                           ;35.4
        push      esi                                           ;35.4
        call      _for_write_seq_fmt                            ;35.4
                                ; LOE esi edi
.B1.12:                         ; Preds .B1.11
        mov       DWORD PTR [580+esp], edi                      ;36.4
        push      32                                            ;36.4
        push      OFFSET FLAT: WRITE_TITLE$format_pack.0.1+1032 ;36.4
        push      edi                                           ;36.4
        push      OFFSET FLAT: __STRLITPACK_184.0.1             ;36.4
        push      -2088435968                                   ;36.4
        push      666                                           ;36.4
        push      esi                                           ;36.4
        call      _for_write_seq_fmt                            ;36.4
                                ; LOE esi edi
.B1.127:                        ; Preds .B1.12
        add       esp, 112                                      ;36.4
                                ; LOE esi edi
.B1.13:                         ; Preds .B1.127
        mov       DWORD PTR [496+esp], edi                      ;37.4
        push      32                                            ;37.4
        push      OFFSET FLAT: WRITE_TITLE$format_pack.0.1+1116 ;37.4
        push      edi                                           ;37.4
        push      OFFSET FLAT: __STRLITPACK_185.0.1             ;37.4
        push      -2088435968                                   ;37.4
        push      666                                           ;37.4
        push      esi                                           ;37.4
        call      _for_write_seq_fmt                            ;37.4
                                ; LOE esi edi
.B1.14:                         ; Preds .B1.13
        mov       DWORD PTR [524+esp], edi                      ;38.4
        push      32                                            ;38.4
        push      OFFSET FLAT: WRITE_TITLE$format_pack.0.1+1200 ;38.4
        push      edi                                           ;38.4
        push      OFFSET FLAT: __STRLITPACK_186.0.1             ;38.4
        push      -2088435968                                   ;38.4
        push      666                                           ;38.4
        push      esi                                           ;38.4
        call      _for_write_seq_fmt                            ;38.4
                                ; LOE esi edi
.B1.15:                         ; Preds .B1.14
        mov       DWORD PTR [552+esp], edi                      ;39.4
        push      32                                            ;39.4
        push      OFFSET FLAT: WRITE_TITLE$format_pack.0.1+1284 ;39.4
        push      edi                                           ;39.4
        push      OFFSET FLAT: __STRLITPACK_187.0.1             ;39.4
        push      -2088435968                                   ;39.4
        push      666                                           ;39.4
        push      esi                                           ;39.4
        call      _for_write_seq_fmt                            ;39.4
                                ; LOE esi edi
.B1.16:                         ; Preds .B1.15
        mov       DWORD PTR [580+esp], edi                      ;40.7
        push      32                                            ;40.7
        push      OFFSET FLAT: WRITE_TITLE$format_pack.0.1+1368 ;40.7
        push      edi                                           ;40.7
        push      OFFSET FLAT: __STRLITPACK_188.0.1             ;40.7
        push      -2088435968                                   ;40.7
        push      666                                           ;40.7
        push      esi                                           ;40.7
        call      _for_write_seq_fmt                            ;40.7
                                ; LOE esi edi
.B1.128:                        ; Preds .B1.16
        add       esp, 112                                      ;40.7
                                ; LOE esi edi
.B1.17:                         ; Preds .B1.128
        mov       DWORD PTR [496+esp], edi                      ;41.7
        push      32                                            ;41.7
        push      OFFSET FLAT: WRITE_TITLE$format_pack.0.1+1452 ;41.7
        push      edi                                           ;41.7
        push      OFFSET FLAT: __STRLITPACK_189.0.1             ;41.7
        push      -2088435968                                   ;41.7
        push      666                                           ;41.7
        push      esi                                           ;41.7
        call      _for_write_seq_fmt                            ;41.7
                                ; LOE esi edi
.B1.129:                        ; Preds .B1.17
        add       esp, 28                                       ;41.7
                                ; LOE esi edi
.B1.18:                         ; Preds .B1.129
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_I18], 1     ;43.14
        je        .B1.20        ; Prob 16%                      ;43.14
                                ; LOE esi edi
.B1.19:                         ; Preds .B1.18
        test      BYTE PTR [_DYNUST_MAIN_MODULE_mp_REACH_CONVERG], 1 ;43.22
        je        .B1.24        ; Prob 60%                      ;43.22
                                ; LOE esi edi
.B1.20:                         ; Preds .B1.18 .B1.19
        mov       DWORD PTR [496+esp], edi                      ;44.7
        push      32                                            ;44.7
        push      OFFSET FLAT: WRITE_TITLE$format_pack.0.1+124  ;44.7
        push      edi                                           ;44.7
        push      OFFSET FLAT: __STRLITPACK_190.0.1             ;44.7
        push      -2088435968                                   ;44.7
        push      18                                            ;44.7
        push      esi                                           ;44.7
        call      _for_write_seq_fmt                            ;44.7
                                ; LOE esi edi
.B1.21:                         ; Preds .B1.20
        mov       DWORD PTR [524+esp], edi                      ;45.7
        push      32                                            ;45.7
        push      OFFSET FLAT: WRITE_TITLE$format_pack.0.1+60   ;45.7
        push      edi                                           ;45.7
        push      OFFSET FLAT: __STRLITPACK_191.0.1             ;45.7
        push      -2088435968                                   ;45.7
        push      18                                            ;45.7
        push      esi                                           ;45.7
        call      _for_write_seq_fmt                            ;45.7
                                ; LOE esi edi
.B1.22:                         ; Preds .B1.21
        mov       DWORD PTR [552+esp], edi                      ;46.7
        push      32                                            ;46.7
        push      OFFSET FLAT: WRITE_TITLE$format_pack.0.1      ;46.7
        push      edi                                           ;46.7
        push      OFFSET FLAT: __STRLITPACK_192.0.1             ;46.7
        push      -2088435968                                   ;46.7
        push      18                                            ;46.7
        push      esi                                           ;46.7
        call      _for_write_seq_fmt                            ;46.7
                                ; LOE esi edi
.B1.23:                         ; Preds .B1.22
        mov       DWORD PTR [580+esp], edi                      ;50.7
        push      32                                            ;50.7
        push      edi                                           ;50.7
        push      OFFSET FLAT: __STRLITPACK_193.0.1             ;50.7
        push      -2088435968                                   ;50.7
        push      18                                            ;50.7
        push      esi                                           ;50.7
        call      _for_write_seq_lis                            ;50.7
                                ; LOE esi edi
.B1.130:                        ; Preds .B1.23
        add       esp, 108                                      ;50.7
                                ; LOE esi edi
.B1.24:                         ; Preds .B1.130 .B1.19
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_I30], 1     ;53.14
        je        .B1.116       ; Prob 16%                      ;53.14
                                ; LOE esi edi
.B1.25:                         ; Preds .B1.152 .B1.24
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_I31], 1     ;61.14
        je        .B1.109       ; Prob 16%                      ;61.14
                                ; LOE esi edi
.B1.26:                         ; Preds .B1.150 .B1.25
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_I32], 1     ;69.14
        je        .B1.102       ; Prob 16%                      ;69.14
                                ; LOE esi edi
.B1.27:                         ; Preds .B1.148 .B1.26
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_I33], 1     ;77.14
        je        .B1.95        ; Prob 16%                      ;77.14
                                ; LOE esi edi
.B1.28:                         ; Preds .B1.146 .B1.27
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_I34], 1     ;85.14
        je        .B1.88        ; Prob 16%                      ;85.14
                                ; LOE esi edi
.B1.29:                         ; Preds .B1.144 .B1.28
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_I35], 1     ;93.14
        je        .B1.80        ; Prob 16%                      ;93.14
                                ; LOE esi edi
.B1.30:                         ; Preds .B1.142 .B1.29
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_I36], 1     ;102.14
        je        .B1.72        ; Prob 16%                      ;102.14
                                ; LOE esi edi
.B1.31:                         ; Preds .B1.140 .B1.30
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_I37], 1     ;111.14
        je        .B1.64        ; Prob 16%                      ;111.14
                                ; LOE esi edi
.B1.32:                         ; Preds .B1.138 .B1.31
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_I38], 1     ;120.14
        je        .B1.57        ; Prob 16%                      ;120.14
                                ; LOE esi edi
.B1.33:                         ; Preds .B1.136 .B1.32
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_I39], 1     ;128.14
        je        .B1.50        ; Prob 16%                      ;128.14
                                ; LOE esi edi
.B1.34:                         ; Preds .B1.134 .B1.33
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_I40], 1     ;136.14
        je        .B1.36        ; Prob 16%                      ;136.14
                                ; LOE esi edi
.B1.35:                         ; Preds .B1.39 .B1.37 .B1.34
        add       esp, 596                                      ;153.1
        pop       ebx                                           ;153.1
        pop       edi                                           ;153.1
        pop       esi                                           ;153.1
        mov       esp, ebp                                      ;153.1
        pop       ebp                                           ;153.1
        ret                                                     ;153.1
                                ; LOE
.B1.36:                         ; Preds .B1.34                  ; Infreq
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ITEMAX] ;137.7
        test      eax, eax                                      ;137.18
        je        .B1.40        ; Prob 50%                      ;137.18
                                ; LOE eax esi edi
.B1.37:                         ; Preds .B1.36                  ; Infreq
        jle       .B1.35        ; Prob 16%                      ;137.34
                                ; LOE eax esi edi
.B1.38:                         ; Preds .B1.37                  ; Infreq
        cmp       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_ITERATION] ;137.53
        je        .B1.40        ; Prob 50%                      ;137.53
                                ; LOE esi edi
.B1.39:                         ; Preds .B1.38                  ; Infreq
        test      BYTE PTR [_DYNUST_MAIN_MODULE_mp_REACH_CONVERG], 1 ;137.66
        je        .B1.35        ; Prob 60%                      ;137.66
                                ; LOE esi edi
.B1.40:                         ; Preds .B1.39 .B1.38 .B1.36    ; Infreq
        mov       DWORD PTR [496+esp], 0                        ;138.7
        lea       eax, DWORD PTR [esp]                          ;138.7
        mov       DWORD PTR [esp], 35                           ;138.7
        mov       DWORD PTR [4+esp], OFFSET FLAT: __STRLITPACK_30 ;138.7
        push      32                                            ;138.7
        push      eax                                           ;138.7
        push      OFFSET FLAT: __STRLITPACK_267.0.1             ;138.7
        push      -2088435968                                   ;138.7
        push      400                                           ;138.7
        push      esi                                           ;138.7
        call      _for_write_seq_lis                            ;138.7
                                ; LOE esi edi
.B1.41:                         ; Preds .B1.40                  ; Infreq
        mov       DWORD PTR [520+esp], 0                        ;139.7
        lea       eax, DWORD PTR [32+esp]                       ;139.7
        mov       DWORD PTR [32+esp], 49                        ;139.7
        mov       DWORD PTR [36+esp], OFFSET FLAT: __STRLITPACK_28 ;139.7
        push      32                                            ;139.7
        push      eax                                           ;139.7
        push      OFFSET FLAT: __STRLITPACK_268.0.1             ;139.7
        push      -2088435968                                   ;139.7
        push      400                                           ;139.7
        push      esi                                           ;139.7
        call      _for_write_seq_lis                            ;139.7
                                ; LOE esi edi
.B1.42:                         ; Preds .B1.41                  ; Infreq
        mov       DWORD PTR [544+esp], 0                        ;140.7
        lea       eax, DWORD PTR [64+esp]                       ;140.7
        mov       DWORD PTR [64+esp], 50                        ;140.7
        mov       DWORD PTR [68+esp], OFFSET FLAT: __STRLITPACK_26 ;140.7
        push      32                                            ;140.7
        push      eax                                           ;140.7
        push      OFFSET FLAT: __STRLITPACK_269.0.1             ;140.7
        push      -2088435968                                   ;140.7
        push      400                                           ;140.7
        push      esi                                           ;140.7
        call      _for_write_seq_lis                            ;140.7
                                ; LOE esi edi
.B1.43:                         ; Preds .B1.42                  ; Infreq
        mov       DWORD PTR [568+esp], 0                        ;141.7
        lea       eax, DWORD PTR [96+esp]                       ;141.7
        mov       DWORD PTR [96+esp], 44                        ;141.7
        mov       DWORD PTR [100+esp], OFFSET FLAT: __STRLITPACK_24 ;141.7
        push      32                                            ;141.7
        push      eax                                           ;141.7
        push      OFFSET FLAT: __STRLITPACK_270.0.1             ;141.7
        push      -2088435968                                   ;141.7
        push      400                                           ;141.7
        push      esi                                           ;141.7
        call      _for_write_seq_lis                            ;141.7
                                ; LOE esi edi
.B1.44:                         ; Preds .B1.43                  ; Infreq
        mov       DWORD PTR [592+esp], edi                      ;142.7
        push      32                                            ;142.7
        push      edi                                           ;142.7
        push      OFFSET FLAT: __STRLITPACK_271.0.1             ;142.7
        push      -2088435968                                   ;142.7
        push      400                                           ;142.7
        push      esi                                           ;142.7
        call      _for_write_seq_lis                            ;142.7
                                ; LOE esi edi
.B1.131:                        ; Preds .B1.44                  ; Infreq
        add       esp, 120                                      ;142.7
                                ; LOE esi edi
.B1.45:                         ; Preds .B1.131                 ; Infreq
        mov       DWORD PTR [496+esp], 0                        ;144.7
        lea       eax, DWORD PTR [32+esp]                       ;144.7
        mov       DWORD PTR [32+esp], 35                        ;144.7
        mov       DWORD PTR [36+esp], OFFSET FLAT: __STRLITPACK_22 ;144.7
        push      32                                            ;144.7
        push      eax                                           ;144.7
        push      OFFSET FLAT: __STRLITPACK_272.0.1             ;144.7
        push      -2088435968                                   ;144.7
        push      401                                           ;144.7
        push      esi                                           ;144.7
        call      _for_write_seq_lis                            ;144.7
                                ; LOE esi edi
.B1.46:                         ; Preds .B1.45                  ; Infreq
        mov       DWORD PTR [520+esp], 0                        ;145.7
        lea       eax, DWORD PTR [64+esp]                       ;145.7
        mov       DWORD PTR [64+esp], 49                        ;145.7
        mov       DWORD PTR [68+esp], OFFSET FLAT: __STRLITPACK_20 ;145.7
        push      32                                            ;145.7
        push      eax                                           ;145.7
        push      OFFSET FLAT: __STRLITPACK_273.0.1             ;145.7
        push      -2088435968                                   ;145.7
        push      401                                           ;145.7
        push      esi                                           ;145.7
        call      _for_write_seq_lis                            ;145.7
                                ; LOE esi edi
.B1.47:                         ; Preds .B1.46                  ; Infreq
        mov       DWORD PTR [544+esp], 0                        ;146.7
        lea       eax, DWORD PTR [96+esp]                       ;146.7
        mov       DWORD PTR [96+esp], 50                        ;146.7
        mov       DWORD PTR [100+esp], OFFSET FLAT: __STRLITPACK_18 ;146.7
        push      32                                            ;146.7
        push      eax                                           ;146.7
        push      OFFSET FLAT: __STRLITPACK_274.0.1             ;146.7
        push      -2088435968                                   ;146.7
        push      401                                           ;146.7
        push      esi                                           ;146.7
        call      _for_write_seq_lis                            ;146.7
                                ; LOE esi edi
.B1.48:                         ; Preds .B1.47                  ; Infreq
        mov       DWORD PTR [568+esp], 0                        ;147.7
        lea       eax, DWORD PTR [128+esp]                      ;147.7
        mov       DWORD PTR [128+esp], 44                       ;147.7
        mov       DWORD PTR [132+esp], OFFSET FLAT: __STRLITPACK_16 ;147.7
        push      32                                            ;147.7
        push      eax                                           ;147.7
        push      OFFSET FLAT: __STRLITPACK_275.0.1             ;147.7
        push      -2088435968                                   ;147.7
        push      401                                           ;147.7
        push      esi                                           ;147.7
        call      _for_write_seq_lis                            ;147.7
                                ; LOE esi edi
.B1.49:                         ; Preds .B1.48                  ; Infreq
        mov       DWORD PTR [592+esp], edi                      ;148.7
        push      32                                            ;148.7
        push      edi                                           ;148.7
        push      OFFSET FLAT: __STRLITPACK_276.0.1             ;148.7
        push      -2088435968                                   ;148.7
        push      401                                           ;148.7
        push      esi                                           ;148.7
        call      _for_write_seq_lis                            ;148.7
                                ; LOE
.B1.132:                        ; Preds .B1.49                  ; Infreq
        add       esp, 716                                      ;148.7
        pop       ebx                                           ;148.7
        pop       edi                                           ;148.7
        pop       esi                                           ;148.7
        mov       esp, ebp                                      ;148.7
        pop       ebp                                           ;148.7
        ret                                                     ;148.7
                                ; LOE
.B1.50:                         ; Preds .B1.33                  ; Infreq
        mov       DWORD PTR [496+esp], 0                        ;129.7
        lea       eax, DWORD PTR [376+esp]                      ;129.7
        mov       DWORD PTR [376+esp], 25                       ;129.7
        mov       DWORD PTR [380+esp], OFFSET FLAT: __STRLITPACK_40 ;129.7
        push      32                                            ;129.7
        push      eax                                           ;129.7
        push      OFFSET FLAT: __STRLITPACK_260.0.1             ;129.7
        push      -2088435968                                   ;129.7
        push      39                                            ;129.7
        push      esi                                           ;129.7
        call      _for_write_seq_lis                            ;129.7
                                ; LOE esi edi
.B1.51:                         ; Preds .B1.50                  ; Infreq
        mov       DWORD PTR [520+esp], 0                        ;130.7
        lea       eax, DWORD PTR [432+esp]                      ;130.7
        mov       DWORD PTR [432+esp], 49                       ;130.7
        mov       DWORD PTR [436+esp], OFFSET FLAT: __STRLITPACK_38 ;130.7
        push      32                                            ;130.7
        push      eax                                           ;130.7
        push      OFFSET FLAT: __STRLITPACK_261.0.1             ;130.7
        push      -2088435968                                   ;130.7
        push      39                                            ;130.7
        push      esi                                           ;130.7
        call      _for_write_seq_lis                            ;130.7
                                ; LOE esi edi
.B1.52:                         ; Preds .B1.51                  ; Infreq
        mov       DWORD PTR [544+esp], 0                        ;131.7
        lea       eax, DWORD PTR [488+esp]                      ;131.7
        mov       DWORD PTR [488+esp], 49                       ;131.7
        mov       DWORD PTR [492+esp], OFFSET FLAT: __STRLITPACK_36 ;131.7
        push      32                                            ;131.7
        push      eax                                           ;131.7
        push      OFFSET FLAT: __STRLITPACK_262.0.1             ;131.7
        push      -2088435968                                   ;131.7
        push      39                                            ;131.7
        push      esi                                           ;131.7
        call      _for_write_seq_lis                            ;131.7
                                ; LOE esi edi
.B1.53:                         ; Preds .B1.52                  ; Infreq
        mov       DWORD PTR [568+esp], 0                        ;132.7
        lea       eax, DWORD PTR [528+esp]                      ;132.7
        mov       DWORD PTR [528+esp], 44                       ;132.7
        mov       DWORD PTR [532+esp], OFFSET FLAT: __STRLITPACK_33 ;132.7
        push      32                                            ;132.7
        push      eax                                           ;132.7
        push      OFFSET FLAT: __STRLITPACK_263.0.1             ;132.7
        push      -2088435968                                   ;132.7
        push      39                                            ;132.7
        push      esi                                           ;132.7
        call      _for_write_seq_lis                            ;132.7
                                ; LOE esi edi
.B1.54:                         ; Preds .B1.53                  ; Infreq
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_I39_T] ;132.7
        lea       edx, DWORD PTR [688+esp]                      ;132.7
        mov       DWORD PTR [688+esp], eax                      ;132.7
        push      edx                                           ;132.7
        push      OFFSET FLAT: __STRLITPACK_264.0.1             ;132.7
        push      esi                                           ;132.7
        call      _for_write_seq_lis_xmit                       ;132.7
                                ; LOE esi edi
.B1.55:                         ; Preds .B1.54                  ; Infreq
        mov       DWORD PTR [588+esp], 9                        ;132.7
        lea       eax, DWORD PTR [588+esp]                      ;132.7
        mov       DWORD PTR [592+esp], OFFSET FLAT: __STRLITPACK_32 ;132.7
        push      eax                                           ;132.7
        push      OFFSET FLAT: __STRLITPACK_265.0.1             ;132.7
        push      esi                                           ;132.7
        call      _for_write_seq_lis_xmit                       ;132.7
                                ; LOE esi edi
.B1.133:                        ; Preds .B1.55                  ; Infreq
        add       esp, 120                                      ;132.7
                                ; LOE esi edi
.B1.56:                         ; Preds .B1.133                 ; Infreq
        mov       DWORD PTR [496+esp], edi                      ;133.7
        push      32                                            ;133.7
        push      edi                                           ;133.7
        push      OFFSET FLAT: __STRLITPACK_266.0.1             ;133.7
        push      -2088435968                                   ;133.7
        push      39                                            ;133.7
        push      esi                                           ;133.7
        call      _for_write_seq_lis                            ;133.7
                                ; LOE esi edi
.B1.134:                        ; Preds .B1.56                  ; Infreq
        add       esp, 24                                       ;133.7
        jmp       .B1.34        ; Prob 100%                     ;133.7
                                ; LOE esi edi
.B1.57:                         ; Preds .B1.32                  ; Infreq
        mov       DWORD PTR [496+esp], 0                        ;121.7
        lea       eax, DWORD PTR [328+esp]                      ;121.7
        mov       DWORD PTR [328+esp], 27                       ;121.7
        mov       DWORD PTR [332+esp], OFFSET FLAT: __STRLITPACK_50 ;121.7
        push      32                                            ;121.7
        push      eax                                           ;121.7
        push      OFFSET FLAT: __STRLITPACK_253.0.1             ;121.7
        push      -2088435968                                   ;121.7
        push      38                                            ;121.7
        push      esi                                           ;121.7
        call      _for_write_seq_lis                            ;121.7
                                ; LOE esi edi
.B1.58:                         ; Preds .B1.57                  ; Infreq
        mov       DWORD PTR [520+esp], 0                        ;122.7
        lea       eax, DWORD PTR [392+esp]                      ;122.7
        mov       DWORD PTR [392+esp], 49                       ;122.7
        mov       DWORD PTR [396+esp], OFFSET FLAT: __STRLITPACK_48 ;122.7
        push      32                                            ;122.7
        push      eax                                           ;122.7
        push      OFFSET FLAT: __STRLITPACK_254.0.1             ;122.7
        push      -2088435968                                   ;122.7
        push      38                                            ;122.7
        push      esi                                           ;122.7
        call      _for_write_seq_lis                            ;122.7
                                ; LOE esi edi
.B1.59:                         ; Preds .B1.58                  ; Infreq
        mov       DWORD PTR [544+esp], 0                        ;123.7
        lea       eax, DWORD PTR [448+esp]                      ;123.7
        mov       DWORD PTR [448+esp], 41                       ;123.7
        mov       DWORD PTR [452+esp], OFFSET FLAT: __STRLITPACK_46 ;123.7
        push      32                                            ;123.7
        push      eax                                           ;123.7
        push      OFFSET FLAT: __STRLITPACK_255.0.1             ;123.7
        push      -2088435968                                   ;123.7
        push      38                                            ;123.7
        push      esi                                           ;123.7
        call      _for_write_seq_lis                            ;123.7
                                ; LOE esi edi
.B1.60:                         ; Preds .B1.59                  ; Infreq
        mov       DWORD PTR [568+esp], 0                        ;124.7
        lea       eax, DWORD PTR [504+esp]                      ;124.7
        mov       DWORD PTR [504+esp], 47                       ;124.7
        mov       DWORD PTR [508+esp], OFFSET FLAT: __STRLITPACK_43 ;124.7
        push      32                                            ;124.7
        push      eax                                           ;124.7
        push      OFFSET FLAT: __STRLITPACK_256.0.1             ;124.7
        push      -2088435968                                   ;124.7
        push      38                                            ;124.7
        push      esi                                           ;124.7
        call      _for_write_seq_lis                            ;124.7
                                ; LOE esi edi
.B1.61:                         ; Preds .B1.60                  ; Infreq
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_I38_T] ;124.7
        lea       edx, DWORD PTR [680+esp]                      ;124.7
        mov       DWORD PTR [680+esp], eax                      ;124.7
        push      edx                                           ;124.7
        push      OFFSET FLAT: __STRLITPACK_257.0.1             ;124.7
        push      esi                                           ;124.7
        call      _for_write_seq_lis_xmit                       ;124.7
                                ; LOE esi edi
.B1.62:                         ; Preds .B1.61                  ; Infreq
        mov       DWORD PTR [580+esp], 9                        ;124.7
        lea       eax, DWORD PTR [580+esp]                      ;124.7
        mov       DWORD PTR [584+esp], OFFSET FLAT: __STRLITPACK_42 ;124.7
        push      eax                                           ;124.7
        push      OFFSET FLAT: __STRLITPACK_258.0.1             ;124.7
        push      esi                                           ;124.7
        call      _for_write_seq_lis_xmit                       ;124.7
                                ; LOE esi edi
.B1.135:                        ; Preds .B1.62                  ; Infreq
        add       esp, 120                                      ;124.7
                                ; LOE esi edi
.B1.63:                         ; Preds .B1.135                 ; Infreq
        mov       DWORD PTR [496+esp], edi                      ;125.7
        push      32                                            ;125.7
        push      edi                                           ;125.7
        push      OFFSET FLAT: __STRLITPACK_259.0.1             ;125.7
        push      -2088435968                                   ;125.7
        push      38                                            ;125.7
        push      esi                                           ;125.7
        call      _for_write_seq_lis                            ;125.7
                                ; LOE esi edi
.B1.136:                        ; Preds .B1.63                  ; Infreq
        add       esp, 24                                       ;125.7
        jmp       .B1.33        ; Prob 100%                     ;125.7
                                ; LOE esi edi
.B1.64:                         ; Preds .B1.31                  ; Infreq
        mov       edx, 37                                       ;112.7
        lea       eax, DWORD PTR [288+esp]                      ;112.7
        mov       DWORD PTR [496+esp], 0                        ;112.7
        mov       DWORD PTR [288+esp], edx                      ;112.7
        mov       DWORD PTR [292+esp], OFFSET FLAT: __STRLITPACK_62 ;112.7
        push      32                                            ;112.7
        push      eax                                           ;112.7
        push      OFFSET FLAT: __STRLITPACK_245.0.1             ;112.7
        push      -2088435968                                   ;112.7
        push      edx                                           ;112.7
        push      esi                                           ;112.7
        call      _for_write_seq_lis                            ;112.7
                                ; LOE esi edi
.B1.65:                         ; Preds .B1.64                  ; Infreq
        mov       DWORD PTR [520+esp], 0                        ;113.7
        lea       eax, DWORD PTR [344+esp]                      ;113.7
        mov       DWORD PTR [344+esp], 49                       ;113.7
        mov       DWORD PTR [348+esp], OFFSET FLAT: __STRLITPACK_60 ;113.7
        push      32                                            ;113.7
        push      eax                                           ;113.7
        push      OFFSET FLAT: __STRLITPACK_246.0.1             ;113.7
        push      -2088435968                                   ;113.7
        push      37                                            ;113.7
        push      esi                                           ;113.7
        call      _for_write_seq_lis                            ;113.7
                                ; LOE esi edi
.B1.66:                         ; Preds .B1.65                  ; Infreq
        mov       DWORD PTR [544+esp], 0                        ;114.7
        lea       eax, DWORD PTR [408+esp]                      ;114.7
        mov       DWORD PTR [408+esp], 40                       ;114.7
        mov       DWORD PTR [412+esp], OFFSET FLAT: __STRLITPACK_58 ;114.7
        push      32                                            ;114.7
        push      eax                                           ;114.7
        push      OFFSET FLAT: __STRLITPACK_247.0.1             ;114.7
        push      -2088435968                                   ;114.7
        push      37                                            ;114.7
        push      esi                                           ;114.7
        call      _for_write_seq_lis                            ;114.7
                                ; LOE esi edi
.B1.67:                         ; Preds .B1.66                  ; Infreq
        mov       DWORD PTR [568+esp], 0                        ;115.7
        lea       eax, DWORD PTR [464+esp]                      ;115.7
        mov       DWORD PTR [464+esp], 34                       ;115.7
        mov       DWORD PTR [468+esp], OFFSET FLAT: __STRLITPACK_56 ;115.7
        push      32                                            ;115.7
        push      eax                                           ;115.7
        push      OFFSET FLAT: __STRLITPACK_248.0.1             ;115.7
        push      -2088435968                                   ;115.7
        push      37                                            ;115.7
        push      esi                                           ;115.7
        call      _for_write_seq_lis                            ;115.7
                                ; LOE esi edi
.B1.68:                         ; Preds .B1.67                  ; Infreq
        mov       DWORD PTR [592+esp], 0                        ;116.7
        lea       eax, DWORD PTR [520+esp]                      ;116.7
        mov       DWORD PTR [520+esp], 23                       ;116.7
        mov       DWORD PTR [524+esp], OFFSET FLAT: __STRLITPACK_53 ;116.7
        push      32                                            ;116.7
        push      eax                                           ;116.7
        push      OFFSET FLAT: __STRLITPACK_249.0.1             ;116.7
        push      -2088435968                                   ;116.7
        push      37                                            ;116.7
        push      esi                                           ;116.7
        call      _for_write_seq_lis                            ;116.7
                                ; LOE esi edi
.B1.137:                        ; Preds .B1.68                  ; Infreq
        add       esp, 120                                      ;116.7
                                ; LOE esi edi
.B1.69:                         ; Preds .B1.137                 ; Infreq
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_I37_T] ;116.7
        lea       edx, DWORD PTR [576+esp]                      ;116.7
        mov       DWORD PTR [576+esp], eax                      ;116.7
        push      edx                                           ;116.7
        push      OFFSET FLAT: __STRLITPACK_250.0.1             ;116.7
        push      esi                                           ;116.7
        call      _for_write_seq_lis_xmit                       ;116.7
                                ; LOE esi edi
.B1.70:                         ; Preds .B1.69                  ; Infreq
        mov       DWORD PTR [476+esp], 9                        ;116.7
        lea       eax, DWORD PTR [476+esp]                      ;116.7
        mov       DWORD PTR [480+esp], OFFSET FLAT: __STRLITPACK_52 ;116.7
        push      eax                                           ;116.7
        push      OFFSET FLAT: __STRLITPACK_251.0.1             ;116.7
        push      esi                                           ;116.7
        call      _for_write_seq_lis_xmit                       ;116.7
                                ; LOE esi edi
.B1.71:                         ; Preds .B1.70                  ; Infreq
        mov       DWORD PTR [520+esp], edi                      ;117.7
        push      32                                            ;117.7
        push      edi                                           ;117.7
        push      OFFSET FLAT: __STRLITPACK_252.0.1             ;117.7
        push      -2088435968                                   ;117.7
        push      37                                            ;117.7
        push      esi                                           ;117.7
        call      _for_write_seq_lis                            ;117.7
                                ; LOE esi edi
.B1.138:                        ; Preds .B1.71                  ; Infreq
        add       esp, 48                                       ;117.7
        jmp       .B1.32        ; Prob 100%                     ;117.7
                                ; LOE esi edi
.B1.72:                         ; Preds .B1.30                  ; Infreq
        mov       DWORD PTR [496+esp], 0                        ;103.7
        lea       eax, DWORD PTR [248+esp]                      ;103.7
        mov       DWORD PTR [248+esp], 45                       ;103.7
        mov       DWORD PTR [252+esp], OFFSET FLAT: __STRLITPACK_74 ;103.7
        push      32                                            ;103.7
        push      eax                                           ;103.7
        push      OFFSET FLAT: __STRLITPACK_237.0.1             ;103.7
        push      -2088435968                                   ;103.7
        push      36                                            ;103.7
        push      esi                                           ;103.7
        call      _for_write_seq_lis                            ;103.7
                                ; LOE esi edi
.B1.73:                         ; Preds .B1.72                  ; Infreq
        mov       DWORD PTR [520+esp], 0                        ;104.7
        lea       eax, DWORD PTR [304+esp]                      ;104.7
        mov       DWORD PTR [304+esp], 49                       ;104.7
        mov       DWORD PTR [308+esp], OFFSET FLAT: __STRLITPACK_72 ;104.7
        push      32                                            ;104.7
        push      eax                                           ;104.7
        push      OFFSET FLAT: __STRLITPACK_238.0.1             ;104.7
        push      -2088435968                                   ;104.7
        push      36                                            ;104.7
        push      esi                                           ;104.7
        call      _for_write_seq_lis                            ;104.7
                                ; LOE esi edi
.B1.74:                         ; Preds .B1.73                  ; Infreq
        mov       DWORD PTR [544+esp], 0                        ;105.7
        lea       eax, DWORD PTR [360+esp]                      ;105.7
        mov       DWORD PTR [360+esp], 41                       ;105.7
        mov       DWORD PTR [364+esp], OFFSET FLAT: __STRLITPACK_70 ;105.7
        push      32                                            ;105.7
        push      eax                                           ;105.7
        push      OFFSET FLAT: __STRLITPACK_239.0.1             ;105.7
        push      -2088435968                                   ;105.7
        push      36                                            ;105.7
        push      esi                                           ;105.7
        call      _for_write_seq_lis                            ;105.7
                                ; LOE esi edi
.B1.75:                         ; Preds .B1.74                  ; Infreq
        mov       DWORD PTR [568+esp], 0                        ;106.7
        lea       eax, DWORD PTR [424+esp]                      ;106.7
        mov       DWORD PTR [424+esp], 35                       ;106.7
        mov       DWORD PTR [428+esp], OFFSET FLAT: __STRLITPACK_68 ;106.7
        push      32                                            ;106.7
        push      eax                                           ;106.7
        push      OFFSET FLAT: __STRLITPACK_240.0.1             ;106.7
        push      -2088435968                                   ;106.7
        push      36                                            ;106.7
        push      esi                                           ;106.7
        call      _for_write_seq_lis                            ;106.7
                                ; LOE esi edi
.B1.76:                         ; Preds .B1.75                  ; Infreq
        mov       DWORD PTR [592+esp], 0                        ;107.7
        lea       eax, DWORD PTR [480+esp]                      ;107.7
        mov       DWORD PTR [480+esp], 23                       ;107.7
        mov       DWORD PTR [484+esp], OFFSET FLAT: __STRLITPACK_65 ;107.7
        push      32                                            ;107.7
        push      eax                                           ;107.7
        push      OFFSET FLAT: __STRLITPACK_241.0.1             ;107.7
        push      -2088435968                                   ;107.7
        push      36                                            ;107.7
        push      esi                                           ;107.7
        call      _for_write_seq_lis                            ;107.7
                                ; LOE esi edi
.B1.139:                        ; Preds .B1.76                  ; Infreq
        add       esp, 120                                      ;107.7
                                ; LOE esi edi
.B1.77:                         ; Preds .B1.139                 ; Infreq
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_I36_T] ;107.7
        lea       edx, DWORD PTR [568+esp]                      ;107.7
        mov       DWORD PTR [568+esp], eax                      ;107.7
        push      edx                                           ;107.7
        push      OFFSET FLAT: __STRLITPACK_242.0.1             ;107.7
        push      esi                                           ;107.7
        call      _for_write_seq_lis_xmit                       ;107.7
                                ; LOE esi edi
.B1.78:                         ; Preds .B1.77                  ; Infreq
        mov       DWORD PTR [460+esp], 9                        ;107.7
        lea       eax, DWORD PTR [460+esp]                      ;107.7
        mov       DWORD PTR [464+esp], OFFSET FLAT: __STRLITPACK_64 ;107.7
        push      eax                                           ;107.7
        push      OFFSET FLAT: __STRLITPACK_243.0.1             ;107.7
        push      esi                                           ;107.7
        call      _for_write_seq_lis_xmit                       ;107.7
                                ; LOE esi edi
.B1.79:                         ; Preds .B1.78                  ; Infreq
        mov       DWORD PTR [520+esp], edi                      ;108.7
        push      32                                            ;108.7
        push      edi                                           ;108.7
        push      OFFSET FLAT: __STRLITPACK_244.0.1             ;108.7
        push      -2088435968                                   ;108.7
        push      36                                            ;108.7
        push      esi                                           ;108.7
        call      _for_write_seq_lis                            ;108.7
                                ; LOE esi edi
.B1.140:                        ; Preds .B1.79                  ; Infreq
        add       esp, 48                                       ;108.7
        jmp       .B1.31        ; Prob 100%                     ;108.7
                                ; LOE esi edi
.B1.80:                         ; Preds .B1.29                  ; Infreq
        mov       DWORD PTR [496+esp], 0                        ;94.7
        lea       eax, DWORD PTR [208+esp]                      ;94.7
        mov       DWORD PTR [208+esp], 43                       ;94.7
        mov       DWORD PTR [212+esp], OFFSET FLAT: __STRLITPACK_86 ;94.7
        push      32                                            ;94.7
        push      eax                                           ;94.7
        push      OFFSET FLAT: __STRLITPACK_229.0.1             ;94.7
        push      -2088435968                                   ;94.7
        push      35                                            ;94.7
        push      esi                                           ;94.7
        call      _for_write_seq_lis                            ;94.7
                                ; LOE esi edi
.B1.81:                         ; Preds .B1.80                  ; Infreq
        mov       DWORD PTR [520+esp], 0                        ;95.7
        lea       eax, DWORD PTR [264+esp]                      ;95.7
        mov       DWORD PTR [264+esp], 49                       ;95.7
        mov       DWORD PTR [268+esp], OFFSET FLAT: __STRLITPACK_84 ;95.7
        push      32                                            ;95.7
        push      eax                                           ;95.7
        push      OFFSET FLAT: __STRLITPACK_230.0.1             ;95.7
        push      -2088435968                                   ;95.7
        push      35                                            ;95.7
        push      esi                                           ;95.7
        call      _for_write_seq_lis                            ;95.7
                                ; LOE esi edi
.B1.82:                         ; Preds .B1.81                  ; Infreq
        mov       DWORD PTR [544+esp], 0                        ;96.7
        lea       eax, DWORD PTR [320+esp]                      ;96.7
        mov       DWORD PTR [320+esp], 39                       ;96.7
        mov       DWORD PTR [324+esp], OFFSET FLAT: __STRLITPACK_82 ;96.7
        push      32                                            ;96.7
        push      eax                                           ;96.7
        push      OFFSET FLAT: __STRLITPACK_231.0.1             ;96.7
        push      -2088435968                                   ;96.7
        push      35                                            ;96.7
        push      esi                                           ;96.7
        call      _for_write_seq_lis                            ;96.7
                                ; LOE esi edi
.B1.83:                         ; Preds .B1.82                  ; Infreq
        mov       DWORD PTR [568+esp], 0                        ;97.7
        lea       eax, DWORD PTR [376+esp]                      ;97.7
        mov       DWORD PTR [376+esp], 37                       ;97.7
        mov       DWORD PTR [380+esp], OFFSET FLAT: __STRLITPACK_80 ;97.7
        push      32                                            ;97.7
        push      eax                                           ;97.7
        push      OFFSET FLAT: __STRLITPACK_232.0.1             ;97.7
        push      -2088435968                                   ;97.7
        push      35                                            ;97.7
        push      esi                                           ;97.7
        call      _for_write_seq_lis                            ;97.7
                                ; LOE esi edi
.B1.84:                         ; Preds .B1.83                  ; Infreq
        mov       DWORD PTR [592+esp], 0                        ;98.7
        lea       eax, DWORD PTR [440+esp]                      ;98.7
        mov       DWORD PTR [440+esp], 23                       ;98.7
        mov       DWORD PTR [444+esp], OFFSET FLAT: __STRLITPACK_77 ;98.7
        push      32                                            ;98.7
        push      eax                                           ;98.7
        push      OFFSET FLAT: __STRLITPACK_233.0.1             ;98.7
        push      -2088435968                                   ;98.7
        push      35                                            ;98.7
        push      esi                                           ;98.7
        call      _for_write_seq_lis                            ;98.7
                                ; LOE esi edi
.B1.141:                        ; Preds .B1.84                  ; Infreq
        add       esp, 120                                      ;98.7
                                ; LOE esi edi
.B1.85:                         ; Preds .B1.141                 ; Infreq
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_I35_T] ;98.7
        lea       edx, DWORD PTR [560+esp]                      ;98.7
        mov       DWORD PTR [560+esp], eax                      ;98.7
        push      edx                                           ;98.7
        push      OFFSET FLAT: __STRLITPACK_234.0.1             ;98.7
        push      esi                                           ;98.7
        call      _for_write_seq_lis_xmit                       ;98.7
                                ; LOE esi edi
.B1.86:                         ; Preds .B1.85                  ; Infreq
        mov       DWORD PTR [428+esp], 9                        ;98.7
        lea       eax, DWORD PTR [428+esp]                      ;98.7
        mov       DWORD PTR [432+esp], OFFSET FLAT: __STRLITPACK_76 ;98.7
        push      eax                                           ;98.7
        push      OFFSET FLAT: __STRLITPACK_235.0.1             ;98.7
        push      esi                                           ;98.7
        call      _for_write_seq_lis_xmit                       ;98.7
                                ; LOE esi edi
.B1.87:                         ; Preds .B1.86                  ; Infreq
        mov       DWORD PTR [520+esp], edi                      ;99.7
        push      32                                            ;99.7
        push      edi                                           ;99.7
        push      OFFSET FLAT: __STRLITPACK_236.0.1             ;99.7
        push      -2088435968                                   ;99.7
        push      35                                            ;99.7
        push      esi                                           ;99.7
        call      _for_write_seq_lis                            ;99.7
                                ; LOE esi edi
.B1.142:                        ; Preds .B1.87                  ; Infreq
        add       esp, 48                                       ;99.7
        jmp       .B1.30        ; Prob 100%                     ;99.7
                                ; LOE esi edi
.B1.88:                         ; Preds .B1.28                  ; Infreq
        mov       DWORD PTR [496+esp], 0                        ;86.7
        lea       eax, DWORD PTR [168+esp]                      ;86.7
        mov       DWORD PTR [168+esp], 48                       ;86.7
        mov       DWORD PTR [172+esp], OFFSET FLAT: __STRLITPACK_96 ;86.7
        push      32                                            ;86.7
        push      eax                                           ;86.7
        push      OFFSET FLAT: __STRLITPACK_222.0.1             ;86.7
        push      -2088435968                                   ;86.7
        push      34                                            ;86.7
        push      esi                                           ;86.7
        call      _for_write_seq_lis                            ;86.7
                                ; LOE esi edi
.B1.89:                         ; Preds .B1.88                  ; Infreq
        mov       DWORD PTR [520+esp], 0                        ;87.7
        lea       eax, DWORD PTR [224+esp]                      ;87.7
        mov       DWORD PTR [224+esp], 49                       ;87.7
        mov       DWORD PTR [228+esp], OFFSET FLAT: __STRLITPACK_94 ;87.7
        push      32                                            ;87.7
        push      eax                                           ;87.7
        push      OFFSET FLAT: __STRLITPACK_223.0.1             ;87.7
        push      -2088435968                                   ;87.7
        push      34                                            ;87.7
        push      esi                                           ;87.7
        call      _for_write_seq_lis                            ;87.7
                                ; LOE esi edi
.B1.90:                         ; Preds .B1.89                  ; Infreq
        mov       DWORD PTR [544+esp], 0                        ;88.7
        lea       eax, DWORD PTR [280+esp]                      ;88.7
        mov       DWORD PTR [280+esp], 40                       ;88.7
        mov       DWORD PTR [284+esp], OFFSET FLAT: __STRLITPACK_92 ;88.7
        push      32                                            ;88.7
        push      eax                                           ;88.7
        push      OFFSET FLAT: __STRLITPACK_224.0.1             ;88.7
        push      -2088435968                                   ;88.7
        push      34                                            ;88.7
        push      esi                                           ;88.7
        call      _for_write_seq_lis                            ;88.7
                                ; LOE esi edi
.B1.91:                         ; Preds .B1.90                  ; Infreq
        mov       DWORD PTR [568+esp], 0                        ;89.7
        lea       eax, DWORD PTR [336+esp]                      ;89.7
        mov       DWORD PTR [336+esp], 36                       ;89.7
        mov       DWORD PTR [340+esp], OFFSET FLAT: __STRLITPACK_89 ;89.7
        push      32                                            ;89.7
        push      eax                                           ;89.7
        push      OFFSET FLAT: __STRLITPACK_225.0.1             ;89.7
        push      -2088435968                                   ;89.7
        push      34                                            ;89.7
        push      esi                                           ;89.7
        call      _for_write_seq_lis                            ;89.7
                                ; LOE esi edi
.B1.92:                         ; Preds .B1.91                  ; Infreq
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_I34_T] ;89.7
        lea       edx, DWORD PTR [648+esp]                      ;89.7
        mov       DWORD PTR [648+esp], eax                      ;89.7
        push      edx                                           ;89.7
        push      OFFSET FLAT: __STRLITPACK_226.0.1             ;89.7
        push      esi                                           ;89.7
        call      _for_write_seq_lis_xmit                       ;89.7
                                ; LOE esi edi
.B1.93:                         ; Preds .B1.92                  ; Infreq
        mov       DWORD PTR [444+esp], 9                        ;89.7
        lea       eax, DWORD PTR [444+esp]                      ;89.7
        mov       DWORD PTR [448+esp], OFFSET FLAT: __STRLITPACK_88 ;89.7
        push      eax                                           ;89.7
        push      OFFSET FLAT: __STRLITPACK_227.0.1             ;89.7
        push      esi                                           ;89.7
        call      _for_write_seq_lis_xmit                       ;89.7
                                ; LOE esi edi
.B1.143:                        ; Preds .B1.93                  ; Infreq
        add       esp, 120                                      ;89.7
                                ; LOE esi edi
.B1.94:                         ; Preds .B1.143                 ; Infreq
        mov       DWORD PTR [496+esp], edi                      ;90.7
        push      32                                            ;90.7
        push      edi                                           ;90.7
        push      OFFSET FLAT: __STRLITPACK_228.0.1             ;90.7
        push      -2088435968                                   ;90.7
        push      34                                            ;90.7
        push      esi                                           ;90.7
        call      _for_write_seq_lis                            ;90.7
                                ; LOE esi edi
.B1.144:                        ; Preds .B1.94                  ; Infreq
        add       esp, 24                                       ;90.7
        jmp       .B1.29        ; Prob 100%                     ;90.7
                                ; LOE esi edi
.B1.95:                         ; Preds .B1.27                  ; Infreq
        mov       DWORD PTR [496+esp], 0                        ;78.7
        lea       eax, DWORD PTR [136+esp]                      ;78.7
        mov       DWORD PTR [136+esp], 49                       ;78.7
        mov       DWORD PTR [140+esp], OFFSET FLAT: __STRLITPACK_106 ;78.7
        push      32                                            ;78.7
        push      eax                                           ;78.7
        push      OFFSET FLAT: __STRLITPACK_215.0.1             ;78.7
        push      -2088435968                                   ;78.7
        push      33                                            ;78.7
        push      esi                                           ;78.7
        call      _for_write_seq_lis                            ;78.7
                                ; LOE esi edi
.B1.96:                         ; Preds .B1.95                  ; Infreq
        mov       DWORD PTR [520+esp], 0                        ;79.7
        lea       eax, DWORD PTR [184+esp]                      ;79.7
        mov       DWORD PTR [184+esp], 49                       ;79.7
        mov       DWORD PTR [188+esp], OFFSET FLAT: __STRLITPACK_104 ;79.7
        push      32                                            ;79.7
        push      eax                                           ;79.7
        push      OFFSET FLAT: __STRLITPACK_216.0.1             ;79.7
        push      -2088435968                                   ;79.7
        push      33                                            ;79.7
        push      esi                                           ;79.7
        call      _for_write_seq_lis                            ;79.7
                                ; LOE esi edi
.B1.97:                         ; Preds .B1.96                  ; Infreq
        mov       DWORD PTR [544+esp], 0                        ;80.7
        lea       eax, DWORD PTR [240+esp]                      ;80.7
        mov       DWORD PTR [240+esp], 38                       ;80.7
        mov       DWORD PTR [244+esp], OFFSET FLAT: __STRLITPACK_102 ;80.7
        push      32                                            ;80.7
        push      eax                                           ;80.7
        push      OFFSET FLAT: __STRLITPACK_217.0.1             ;80.7
        push      -2088435968                                   ;80.7
        push      33                                            ;80.7
        push      esi                                           ;80.7
        call      _for_write_seq_lis                            ;80.7
                                ; LOE esi edi
.B1.98:                         ; Preds .B1.97                  ; Infreq
        mov       DWORD PTR [568+esp], 0                        ;81.7
        lea       eax, DWORD PTR [296+esp]                      ;81.7
        mov       DWORD PTR [296+esp], 37                       ;81.7
        mov       DWORD PTR [300+esp], OFFSET FLAT: __STRLITPACK_99 ;81.7
        push      32                                            ;81.7
        push      eax                                           ;81.7
        push      OFFSET FLAT: __STRLITPACK_218.0.1             ;81.7
        push      -2088435968                                   ;81.7
        push      33                                            ;81.7
        push      esi                                           ;81.7
        call      _for_write_seq_lis                            ;81.7
                                ; LOE esi edi
.B1.99:                         ; Preds .B1.98                  ; Infreq
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_I33_T] ;81.7
        lea       edx, DWORD PTR [640+esp]                      ;81.7
        mov       DWORD PTR [640+esp], eax                      ;81.7
        push      edx                                           ;81.7
        push      OFFSET FLAT: __STRLITPACK_219.0.1             ;81.7
        push      esi                                           ;81.7
        call      _for_write_seq_lis_xmit                       ;81.7
                                ; LOE esi edi
.B1.100:                        ; Preds .B1.99                  ; Infreq
        mov       DWORD PTR [404+esp], 9                        ;81.7
        lea       eax, DWORD PTR [404+esp]                      ;81.7
        mov       DWORD PTR [408+esp], OFFSET FLAT: __STRLITPACK_98 ;81.7
        push      eax                                           ;81.7
        push      OFFSET FLAT: __STRLITPACK_220.0.1             ;81.7
        push      esi                                           ;81.7
        call      _for_write_seq_lis_xmit                       ;81.7
                                ; LOE esi edi
.B1.145:                        ; Preds .B1.100                 ; Infreq
        add       esp, 120                                      ;81.7
                                ; LOE esi edi
.B1.101:                        ; Preds .B1.145                 ; Infreq
        mov       DWORD PTR [496+esp], edi                      ;82.7
        push      32                                            ;82.7
        push      edi                                           ;82.7
        push      OFFSET FLAT: __STRLITPACK_221.0.1             ;82.7
        push      -2088435968                                   ;82.7
        push      33                                            ;82.7
        push      esi                                           ;82.7
        call      _for_write_seq_lis                            ;82.7
                                ; LOE esi edi
.B1.146:                        ; Preds .B1.101                 ; Infreq
        add       esp, 24                                       ;82.7
        jmp       .B1.28        ; Prob 100%                     ;82.7
                                ; LOE esi edi
.B1.102:                        ; Preds .B1.26                  ; Infreq
        mov       ebx, 32                                       ;70.7
        lea       eax, DWORD PTR [104+esp]                      ;70.7
        mov       DWORD PTR [496+esp], 0                        ;70.7
        mov       DWORD PTR [104+esp], 49                       ;70.7
        mov       DWORD PTR [108+esp], OFFSET FLAT: __STRLITPACK_116 ;70.7
        push      ebx                                           ;70.7
        push      eax                                           ;70.7
        push      OFFSET FLAT: __STRLITPACK_208.0.1             ;70.7
        push      -2088435968                                   ;70.7
        push      ebx                                           ;70.7
        push      esi                                           ;70.7
        call      _for_write_seq_lis                            ;70.7
                                ; LOE ebx esi edi
.B1.103:                        ; Preds .B1.102                 ; Infreq
        mov       DWORD PTR [520+esp], 0                        ;71.7
        lea       eax, DWORD PTR [152+esp]                      ;71.7
        mov       DWORD PTR [152+esp], 49                       ;71.7
        mov       DWORD PTR [156+esp], OFFSET FLAT: __STRLITPACK_114 ;71.7
        push      ebx                                           ;71.7
        push      eax                                           ;71.7
        push      OFFSET FLAT: __STRLITPACK_209.0.1             ;71.7
        push      -2088435968                                   ;71.7
        push      ebx                                           ;71.7
        push      esi                                           ;71.7
        call      _for_write_seq_lis                            ;71.7
                                ; LOE ebx esi edi
.B1.104:                        ; Preds .B1.103                 ; Infreq
        mov       DWORD PTR [544+esp], 0                        ;72.7
        lea       eax, DWORD PTR [200+esp]                      ;72.7
        mov       DWORD PTR [200+esp], 49                       ;72.7
        mov       DWORD PTR [204+esp], OFFSET FLAT: __STRLITPACK_112 ;72.7
        push      ebx                                           ;72.7
        push      eax                                           ;72.7
        push      OFFSET FLAT: __STRLITPACK_210.0.1             ;72.7
        push      -2088435968                                   ;72.7
        push      ebx                                           ;72.7
        push      esi                                           ;72.7
        call      _for_write_seq_lis                            ;72.7
                                ; LOE ebx esi edi
.B1.105:                        ; Preds .B1.104                 ; Infreq
        mov       DWORD PTR [568+esp], 0                        ;73.7
        lea       eax, DWORD PTR [256+esp]                      ;73.7
        mov       DWORD PTR [256+esp], 50                       ;73.7
        mov       DWORD PTR [260+esp], OFFSET FLAT: __STRLITPACK_109 ;73.7
        push      ebx                                           ;73.7
        push      eax                                           ;73.7
        push      OFFSET FLAT: __STRLITPACK_211.0.1             ;73.7
        push      -2088435968                                   ;73.7
        push      ebx                                           ;73.7
        push      esi                                           ;73.7
        call      _for_write_seq_lis                            ;73.7
                                ; LOE ebx esi edi
.B1.106:                        ; Preds .B1.105                 ; Infreq
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_I32_T] ;73.7
        lea       edx, DWORD PTR [632+esp]                      ;73.7
        mov       DWORD PTR [632+esp], eax                      ;73.7
        push      edx                                           ;73.7
        push      OFFSET FLAT: __STRLITPACK_212.0.1             ;73.7
        push      esi                                           ;73.7
        call      _for_write_seq_lis_xmit                       ;73.7
                                ; LOE ebx esi edi
.B1.107:                        ; Preds .B1.106                 ; Infreq
        mov       DWORD PTR [364+esp], 9                        ;73.7
        lea       eax, DWORD PTR [364+esp]                      ;73.7
        mov       DWORD PTR [368+esp], OFFSET FLAT: __STRLITPACK_108 ;73.7
        push      eax                                           ;73.7
        push      OFFSET FLAT: __STRLITPACK_213.0.1             ;73.7
        push      esi                                           ;73.7
        call      _for_write_seq_lis_xmit                       ;73.7
                                ; LOE ebx esi edi
.B1.147:                        ; Preds .B1.107                 ; Infreq
        add       esp, 120                                      ;73.7
                                ; LOE ebx esi edi
.B1.108:                        ; Preds .B1.147                 ; Infreq
        mov       DWORD PTR [496+esp], edi                      ;74.7
        push      ebx                                           ;74.7
        push      edi                                           ;74.7
        push      OFFSET FLAT: __STRLITPACK_214.0.1             ;74.7
        push      -2088435968                                   ;74.7
        push      ebx                                           ;74.7
        push      esi                                           ;74.7
        call      _for_write_seq_lis                            ;74.7
                                ; LOE esi edi
.B1.148:                        ; Preds .B1.108                 ; Infreq
        add       esp, 24                                       ;74.7
        jmp       .B1.27        ; Prob 100%                     ;74.7
                                ; LOE esi edi
.B1.109:                        ; Preds .B1.25                  ; Infreq
        mov       DWORD PTR [496+esp], 0                        ;62.7
        lea       eax, DWORD PTR [80+esp]                       ;62.7
        mov       DWORD PTR [80+esp], 49                        ;62.7
        mov       DWORD PTR [84+esp], OFFSET FLAT: __STRLITPACK_126 ;62.7
        push      32                                            ;62.7
        push      eax                                           ;62.7
        push      OFFSET FLAT: __STRLITPACK_201.0.1             ;62.7
        push      -2088435968                                   ;62.7
        push      31                                            ;62.7
        push      esi                                           ;62.7
        call      _for_write_seq_lis                            ;62.7
                                ; LOE esi edi
.B1.110:                        ; Preds .B1.109                 ; Infreq
        mov       DWORD PTR [520+esp], 0                        ;63.7
        lea       eax, DWORD PTR [120+esp]                      ;63.7
        mov       DWORD PTR [120+esp], 49                       ;63.7
        mov       DWORD PTR [124+esp], OFFSET FLAT: __STRLITPACK_124 ;63.7
        push      32                                            ;63.7
        push      eax                                           ;63.7
        push      OFFSET FLAT: __STRLITPACK_202.0.1             ;63.7
        push      -2088435968                                   ;63.7
        push      31                                            ;63.7
        push      esi                                           ;63.7
        call      _for_write_seq_lis                            ;63.7
                                ; LOE esi edi
.B1.111:                        ; Preds .B1.110                 ; Infreq
        mov       DWORD PTR [544+esp], 0                        ;64.7
        lea       eax, DWORD PTR [168+esp]                      ;64.7
        mov       DWORD PTR [168+esp], 49                       ;64.7
        mov       DWORD PTR [172+esp], OFFSET FLAT: __STRLITPACK_122 ;64.7
        push      32                                            ;64.7
        push      eax                                           ;64.7
        push      OFFSET FLAT: __STRLITPACK_203.0.1             ;64.7
        push      -2088435968                                   ;64.7
        push      31                                            ;64.7
        push      esi                                           ;64.7
        call      _for_write_seq_lis                            ;64.7
                                ; LOE esi edi
.B1.112:                        ; Preds .B1.111                 ; Infreq
        mov       DWORD PTR [568+esp], 0                        ;65.7
        lea       eax, DWORD PTR [216+esp]                      ;65.7
        mov       DWORD PTR [216+esp], 27                       ;65.7
        mov       DWORD PTR [220+esp], OFFSET FLAT: __STRLITPACK_119 ;65.7
        push      32                                            ;65.7
        push      eax                                           ;65.7
        push      OFFSET FLAT: __STRLITPACK_204.0.1             ;65.7
        push      -2088435968                                   ;65.7
        push      31                                            ;65.7
        push      esi                                           ;65.7
        call      _for_write_seq_lis                            ;65.7
                                ; LOE esi edi
.B1.113:                        ; Preds .B1.112                 ; Infreq
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_I31_T] ;65.7
        lea       edx, DWORD PTR [624+esp]                      ;65.7
        mov       DWORD PTR [624+esp], eax                      ;65.7
        push      edx                                           ;65.7
        push      OFFSET FLAT: __STRLITPACK_205.0.1             ;65.7
        push      esi                                           ;65.7
        call      _for_write_seq_lis_xmit                       ;65.7
                                ; LOE esi edi
.B1.114:                        ; Preds .B1.113                 ; Infreq
        mov       DWORD PTR [324+esp], 9                        ;65.7
        lea       eax, DWORD PTR [324+esp]                      ;65.7
        mov       DWORD PTR [328+esp], OFFSET FLAT: __STRLITPACK_118 ;65.7
        push      eax                                           ;65.7
        push      OFFSET FLAT: __STRLITPACK_206.0.1             ;65.7
        push      esi                                           ;65.7
        call      _for_write_seq_lis_xmit                       ;65.7
                                ; LOE esi edi
.B1.149:                        ; Preds .B1.114                 ; Infreq
        add       esp, 120                                      ;65.7
                                ; LOE esi edi
.B1.115:                        ; Preds .B1.149                 ; Infreq
        mov       DWORD PTR [496+esp], edi                      ;66.7
        push      32                                            ;66.7
        push      edi                                           ;66.7
        push      OFFSET FLAT: __STRLITPACK_207.0.1             ;66.7
        push      -2088435968                                   ;66.7
        push      31                                            ;66.7
        push      esi                                           ;66.7
        call      _for_write_seq_lis                            ;66.7
                                ; LOE esi edi
.B1.150:                        ; Preds .B1.115                 ; Infreq
        add       esp, 24                                       ;66.7
        jmp       .B1.26        ; Prob 100%                     ;66.7
                                ; LOE esi edi
.B1.116:                        ; Preds .B1.24                  ; Infreq
        mov       DWORD PTR [496+esp], 0                        ;54.7
        lea       eax, DWORD PTR [64+esp]                       ;54.7
        mov       DWORD PTR [64+esp], 49                        ;54.7
        mov       DWORD PTR [68+esp], OFFSET FLAT: __STRLITPACK_136 ;54.7
        push      32                                            ;54.7
        push      eax                                           ;54.7
        push      OFFSET FLAT: __STRLITPACK_194.0.1             ;54.7
        push      -2088435968                                   ;54.7
        push      30                                            ;54.7
        push      esi                                           ;54.7
        call      _for_write_seq_lis                            ;54.7
                                ; LOE esi edi
.B1.117:                        ; Preds .B1.116                 ; Infreq
        mov       DWORD PTR [520+esp], 0                        ;55.7
        lea       eax, DWORD PTR [96+esp]                       ;55.7
        mov       DWORD PTR [96+esp], 49                        ;55.7
        mov       DWORD PTR [100+esp], OFFSET FLAT: __STRLITPACK_134 ;55.7
        push      32                                            ;55.7
        push      eax                                           ;55.7
        push      OFFSET FLAT: __STRLITPACK_195.0.1             ;55.7
        push      -2088435968                                   ;55.7
        push      30                                            ;55.7
        push      esi                                           ;55.7
        call      _for_write_seq_lis                            ;55.7
                                ; LOE esi edi
.B1.118:                        ; Preds .B1.117                 ; Infreq
        mov       DWORD PTR [544+esp], 0                        ;56.7
        lea       eax, DWORD PTR [136+esp]                      ;56.7
        mov       DWORD PTR [136+esp], 49                       ;56.7
        mov       DWORD PTR [140+esp], OFFSET FLAT: __STRLITPACK_132 ;56.7
        push      32                                            ;56.7
        push      eax                                           ;56.7
        push      OFFSET FLAT: __STRLITPACK_196.0.1             ;56.7
        push      -2088435968                                   ;56.7
        push      30                                            ;56.7
        push      esi                                           ;56.7
        call      _for_write_seq_lis                            ;56.7
                                ; LOE esi edi
.B1.119:                        ; Preds .B1.118                 ; Infreq
        mov       DWORD PTR [568+esp], 0                        ;57.7
        lea       eax, DWORD PTR [184+esp]                      ;57.7
        mov       DWORD PTR [184+esp], 28                       ;57.7
        mov       DWORD PTR [188+esp], OFFSET FLAT: __STRLITPACK_129 ;57.7
        push      32                                            ;57.7
        push      eax                                           ;57.7
        push      OFFSET FLAT: __STRLITPACK_197.0.1             ;57.7
        push      -2088435968                                   ;57.7
        push      30                                            ;57.7
        push      esi                                           ;57.7
        call      _for_write_seq_lis                            ;57.7
                                ; LOE esi edi
.B1.120:                        ; Preds .B1.119                 ; Infreq
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_I30_T] ;57.7
        lea       edx, DWORD PTR [584+esp]                      ;57.7
        mov       DWORD PTR [584+esp], eax                      ;57.7
        push      edx                                           ;57.7
        push      OFFSET FLAT: __STRLITPACK_198.0.1             ;57.7
        push      esi                                           ;57.7
        call      _for_write_seq_lis_xmit                       ;57.7
                                ; LOE esi edi
.B1.121:                        ; Preds .B1.120                 ; Infreq
        mov       DWORD PTR [284+esp], 9                        ;57.7
        lea       eax, DWORD PTR [284+esp]                      ;57.7
        mov       DWORD PTR [288+esp], OFFSET FLAT: __STRLITPACK_128 ;57.7
        push      eax                                           ;57.7
        push      OFFSET FLAT: __STRLITPACK_199.0.1             ;57.7
        push      esi                                           ;57.7
        call      _for_write_seq_lis_xmit                       ;57.7
                                ; LOE esi edi
.B1.151:                        ; Preds .B1.121                 ; Infreq
        add       esp, 120                                      ;57.7
                                ; LOE esi edi
.B1.122:                        ; Preds .B1.151                 ; Infreq
        mov       DWORD PTR [496+esp], edi                      ;58.7
        push      32                                            ;58.7
        push      edi                                           ;58.7
        push      OFFSET FLAT: __STRLITPACK_200.0.1             ;58.7
        push      -2088435968                                   ;58.7
        push      30                                            ;58.7
        push      esi                                           ;58.7
        call      _for_write_seq_lis                            ;58.7
                                ; LOE esi edi
.B1.152:                        ; Preds .B1.122                 ; Infreq
        add       esp, 24                                       ;58.7
        jmp       .B1.25        ; Prob 100%                     ;58.7
        ALIGN     16
                                ; LOE esi edi
; mark_end;
_WRITE_TITLE ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
WRITE_TITLE$format_pack.0.1	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	48
	DB	0
	DB	84
	DB	104
	DB	105
	DB	115
	DB	32
	DB	102
	DB	105
	DB	108
	DB	101
	DB	32
	DB	112
	DB	114
	DB	111
	DB	118
	DB	105
	DB	100
	DB	101
	DB	115
	DB	32
	DB	97
	DB	108
	DB	108
	DB	32
	DB	116
	DB	104
	DB	101
	DB	32
	DB	118
	DB	101
	DB	104
	DB	105
	DB	99
	DB	108
	DB	101
	DB	115
	DB	32
	DB	116
	DB	114
	DB	97
	DB	106
	DB	101
	DB	99
	DB	116
	DB	111
	DB	114
	DB	105
	DB	101
	DB	115
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	49
	DB	0
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	71
	DB	0
	DB	1
	DB	0
	DB	28
	DB	0
	DB	49
	DB	0
	DB	42
	DB	42
	DB	42
	DB	42
	DB	32
	DB	32
	DB	79
	DB	117
	DB	116
	DB	112
	DB	117
	DB	116
	DB	32
	DB	102
	DB	105
	DB	108
	DB	101
	DB	32
	DB	102
	DB	111
	DB	114
	DB	32
	DB	118
	DB	101
	DB	104
	DB	105
	DB	99
	DB	108
	DB	101
	DB	115
	DB	32
	DB	116
	DB	114
	DB	97
	DB	110
	DB	106
	DB	101
	DB	99
	DB	116
	DB	111
	DB	114
	DB	105
	DB	101
	DB	115
	DB	32
	DB	42
	DB	42
	DB	42
	DB	42
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	14
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	11
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	58
	DB	0
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	14
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	11
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	58
	DB	0
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	14
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	11
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	58
	DB	0
	DB	72
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	68
	DB	121
	DB	110
	DB	117
	DB	115
	DB	84
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	72
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	14
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	11
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	58
	DB	0
	DB	72
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	72
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	14
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	11
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	58
	DB	0
	DB	72
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	68
	DB	121
	DB	110
	DB	97
	DB	109
	DB	105
	DB	99
	DB	32
	DB	85
	DB	114
	DB	98
	DB	97
	DB	110
	DB	32
	DB	83
	DB	121
	DB	115
	DB	116
	DB	101
	DB	109
	DB	115
	DB	32
	DB	102
	DB	111
	DB	114
	DB	32
	DB	84
	DB	114
	DB	97
	DB	110
	DB	115
	DB	112
	DB	111
	DB	114
	DB	116
	DB	97
	DB	116
	DB	105
	DB	111
	DB	110
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	72
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	14
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	11
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	58
	DB	0
	DB	72
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	72
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	14
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	11
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	58
	DB	0
	DB	72
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	86
	DB	101
	DB	114
	DB	115
	DB	105
	DB	111
	DB	110
	DB	32
	DB	40
	DB	50
	DB	46
	DB	48
	DB	46
	DB	49
	DB	32
	DB	66
	DB	101
	DB	116
	DB	97
	DB	41
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	72
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	14
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	11
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	58
	DB	0
	DB	72
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	72
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	14
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	11
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	58
	DB	0
	DB	72
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	72
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	14
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	11
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	58
	DB	0
	DB	72
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	82
	DB	101
	DB	108
	DB	101
	DB	97
	DB	115
	DB	101
	DB	100
	DB	32
	DB	98
	DB	121
	DB	58
	DB	32
	DB	70
	DB	101
	DB	100
	DB	101
	DB	114
	DB	97
	DB	108
	DB	32
	DB	72
	DB	105
	DB	103
	DB	104
	DB	119
	DB	97
	DB	121
	DB	32
	DB	65
	DB	100
	DB	109
	DB	105
	DB	110
	DB	105
	DB	115
	DB	116
	DB	114
	DB	97
	DB	116
	DB	105
	DB	111
	DB	110
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	72
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	14
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	11
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	58
	DB	0
	DB	72
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	67
	DB	111
	DB	112
	DB	121
	DB	114
	DB	105
	DB	103
	DB	104
	DB	116
	DB	58
	DB	32
	DB	89
	DB	105
	DB	45
	DB	67
	DB	104
	DB	97
	DB	110
	DB	103
	DB	32
	DB	67
	DB	104
	DB	105
	DB	117
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	72
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	14
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	11
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	58
	DB	0
	DB	72
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	72
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	14
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	11
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	58
	DB	0
	DB	72
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	83
	DB	99
	DB	104
	DB	101
	DB	100
	DB	117
	DB	108
	DB	101
	DB	100
	DB	32
	DB	82
	DB	101
	DB	108
	DB	101
	DB	97
	DB	115
	DB	101
	DB	32
	DB	68
	DB	97
	DB	116
	DB	101
	DB	58
	DB	32
	DB	79
	DB	99
	DB	116
	DB	111
	DB	98
	DB	101
	DB	114
	DB	44
	DB	32
	DB	50
	DB	48
	DB	48
	DB	57
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	72
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	14
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	11
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	58
	DB	0
	DB	72
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	72
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	14
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	11
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	58
	DB	0
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	14
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	11
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	58
	DB	0
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
_DATA	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
__STRLITPACK_173.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_174.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_175.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_176.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_177.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_178.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_179.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_180.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_181.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_182.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_183.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_184.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_185.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_186.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_187.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_188.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_189.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_190.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_191.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_192.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_193.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_194.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_195.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_196.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_197.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_198.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_199.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_200.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_201.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_202.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_203.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_204.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_205.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_206.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_207.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_208.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_209.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_210.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_211.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_212.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_213.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_214.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_215.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_216.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_217.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_218.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_219.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_220.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_221.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_222.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_223.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_224.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_225.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_226.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_227.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_228.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_229.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_230.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_231.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_232.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_233.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_234.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_235.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_236.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_237.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_238.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_239.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_240.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_241.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_242.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_243.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_244.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_245.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_246.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_247.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_248.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_249.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_250.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_251.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_252.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_253.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_254.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_255.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_256.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_257.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_258.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_259.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_260.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_261.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_262.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_263.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_264.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_265.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_266.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_267.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_268.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_269.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_270.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_271.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_272.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_273.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_274.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_275.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_276.0.1	DB	1
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _WRITE_TITLE
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_136	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	32
	DB	32
	DB	79
	DB	117
	DB	116
	DB	112
	DB	117
	DB	116
	DB	32
	DB	102
	DB	105
	DB	108
	DB	101
	DB	32
	DB	102
	DB	111
	DB	114
	DB	32
	DB	118
	DB	101
	DB	104
	DB	105
	DB	99
	DB	108
	DB	101
	DB	32
	DB	103
	DB	101
	DB	110
	DB	101
	DB	114
	DB	97
	DB	116
	DB	105
	DB	111
	DB	110
	DB	32
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_134	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_132	DB	84
	DB	104
	DB	105
	DB	115
	DB	32
	DB	102
	DB	105
	DB	108
	DB	101
	DB	32
	DB	112
	DB	114
	DB	111
	DB	118
	DB	105
	DB	100
	DB	101
	DB	115
	DB	32
	DB	116
	DB	104
	DB	101
	DB	32
	DB	97
	DB	118
	DB	101
	DB	114
	DB	97
	DB	103
	DB	101
	DB	32
	DB	110
	DB	117
	DB	109
	DB	98
	DB	101
	DB	114
	DB	32
	DB	111
	DB	102
	DB	32
	DB	118
	DB	101
	DB	104
	DB	105
	DB	99
	DB	108
	DB	101
	DB	115
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_129	DB	112
	DB	101
	DB	114
	DB	32
	DB	115
	DB	105
	DB	109
	DB	46
	DB	32
	DB	105
	DB	110
	DB	116
	DB	46
	DB	44
	DB	32
	DB	97
	DB	118
	DB	101
	DB	114
	DB	97
	DB	103
	DB	101
	DB	100
	DB	32
	DB	111
	DB	118
	DB	101
	DB	114
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_128	DB	115
	DB	105
	DB	109
	DB	46
	DB	32
	DB	105
	DB	110
	DB	116
	DB	46
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_126	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	32
	DB	32
	DB	79
	DB	117
	DB	116
	DB	112
	DB	117
	DB	116
	DB	32
	DB	102
	DB	105
	DB	108
	DB	101
	DB	32
	DB	102
	DB	111
	DB	114
	DB	32
	DB	108
	DB	105
	DB	110
	DB	107
	DB	32
	DB	118
	DB	111
	DB	108
	DB	117
	DB	109
	DB	101
	DB	115
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_124	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_122	DB	84
	DB	104
	DB	105
	DB	115
	DB	32
	DB	102
	DB	105
	DB	108
	DB	101
	DB	32
	DB	112
	DB	114
	DB	111
	DB	118
	DB	105
	DB	100
	DB	101
	DB	115
	DB	32
	DB	116
	DB	104
	DB	101
	DB	32
	DB	97
	DB	118
	DB	101
	DB	114
	DB	97
	DB	103
	DB	101
	DB	32
	DB	110
	DB	117
	DB	109
	DB	98
	DB	101
	DB	114
	DB	32
	DB	111
	DB	102
	DB	32
	DB	118
	DB	101
	DB	104
	DB	105
	DB	99
	DB	108
	DB	101
	DB	115
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_119	DB	112
	DB	101
	DB	114
	DB	32
	DB	115
	DB	105
	DB	109
	DB	46
	DB	32
	DB	105
	DB	110
	DB	116
	DB	46
	DB	32
	DB	97
	DB	118
	DB	101
	DB	114
	DB	97
	DB	103
	DB	101
	DB	100
	DB	32
	DB	111
	DB	118
	DB	101
	DB	114
	DB	0
__STRLITPACK_118	DB	115
	DB	105
	DB	109
	DB	46
	DB	32
	DB	105
	DB	110
	DB	116
	DB	46
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_116	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	32
	DB	32
	DB	79
	DB	117
	DB	116
	DB	112
	DB	117
	DB	116
	DB	32
	DB	102
	DB	105
	DB	108
	DB	101
	DB	32
	DB	102
	DB	111
	DB	114
	DB	32
	DB	118
	DB	101
	DB	104
	DB	105
	DB	99
	DB	108
	DB	101
	DB	32
	DB	113
	DB	117
	DB	101
	DB	117
	DB	101
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_114	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_112	DB	84
	DB	104
	DB	105
	DB	115
	DB	32
	DB	102
	DB	105
	DB	108
	DB	101
	DB	32
	DB	112
	DB	114
	DB	111
	DB	118
	DB	105
	DB	100
	DB	101
	DB	115
	DB	32
	DB	116
	DB	104
	DB	101
	DB	32
	DB	97
	DB	118
	DB	101
	DB	114
	DB	97
	DB	103
	DB	101
	DB	32
	DB	110
	DB	117
	DB	109
	DB	98
	DB	101
	DB	114
	DB	32
	DB	111
	DB	102
	DB	32
	DB	118
	DB	101
	DB	104
	DB	105
	DB	99
	DB	108
	DB	101
	DB	115
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_109	DB	105
	DB	110
	DB	32
	DB	116
	DB	104
	DB	101
	DB	32
	DB	113
	DB	117
	DB	101
	DB	117
	DB	101
	DB	115
	DB	32
	DB	111
	DB	110
	DB	32
	DB	108
	DB	105
	DB	110
	DB	107
	DB	115
	DB	32
	DB	112
	DB	101
	DB	114
	DB	32
	DB	115
	DB	105
	DB	109
	DB	46
	DB	32
	DB	105
	DB	110
	DB	116
	DB	46
	DB	32
	DB	97
	DB	118
	DB	101
	DB	114
	DB	97
	DB	103
	DB	101
	DB	100
	DB	32
	DB	111
	DB	118
	DB	101
	DB	114
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_108	DB	115
	DB	105
	DB	109
	DB	46
	DB	32
	DB	105
	DB	110
	DB	116
	DB	46
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_106	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	32
	DB	32
	DB	79
	DB	117
	DB	116
	DB	112
	DB	117
	DB	116
	DB	32
	DB	102
	DB	105
	DB	108
	DB	101
	DB	32
	DB	102
	DB	111
	DB	114
	DB	32
	DB	108
	DB	105
	DB	110
	DB	107
	DB	32
	DB	115
	DB	112
	DB	101
	DB	101
	DB	100
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_104	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_102	DB	84
	DB	104
	DB	105
	DB	115
	DB	32
	DB	102
	DB	105
	DB	108
	DB	101
	DB	32
	DB	112
	DB	114
	DB	111
	DB	118
	DB	105
	DB	100
	DB	101
	DB	115
	DB	32
	DB	116
	DB	104
	DB	101
	DB	32
	DB	97
	DB	118
	DB	101
	DB	114
	DB	97
	DB	103
	DB	101
	DB	32
	DB	115
	DB	112
	DB	101
	DB	101
	DB	100
	DB	32
	DB	32
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_99	DB	111
	DB	110
	DB	32
	DB	108
	DB	105
	DB	110
	DB	107
	DB	115
	DB	32
	DB	112
	DB	101
	DB	114
	DB	32
	DB	115
	DB	105
	DB	109
	DB	46
	DB	32
	DB	105
	DB	110
	DB	116
	DB	46
	DB	32
	DB	97
	DB	118
	DB	101
	DB	114
	DB	97
	DB	103
	DB	101
	DB	100
	DB	32
	DB	111
	DB	118
	DB	101
	DB	114
	DB	32
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_98	DB	115
	DB	105
	DB	109
	DB	46
	DB	32
	DB	105
	DB	110
	DB	116
	DB	46
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_96	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	32
	DB	32
	DB	79
	DB	117
	DB	116
	DB	112
	DB	117
	DB	116
	DB	32
	DB	102
	DB	105
	DB	108
	DB	101
	DB	32
	DB	102
	DB	111
	DB	114
	DB	32
	DB	108
	DB	105
	DB	110
	DB	107
	DB	32
	DB	100
	DB	101
	DB	110
	DB	115
	DB	105
	DB	116
	DB	121
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	42
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_94	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_92	DB	84
	DB	104
	DB	105
	DB	115
	DB	32
	DB	102
	DB	105
	DB	108
	DB	101
	DB	32
	DB	112
	DB	114
	DB	111
	DB	118
	DB	105
	DB	100
	DB	101
	DB	115
	DB	32
	DB	116
	DB	104
	DB	101
	DB	32
	DB	97
	DB	118
	DB	101
	DB	114
	DB	97
	DB	103
	DB	101
	DB	32
	DB	100
	DB	101
	DB	110
	DB	115
	DB	105
	DB	116
	DB	121
	DB	32
	DB	32
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_89	DB	111
	DB	110
	DB	32
	DB	108
	DB	105
	DB	110
	DB	107
	DB	115
	DB	32
	DB	112
	DB	101
	DB	114
	DB	32
	DB	115
	DB	105
	DB	109
	DB	46
	DB	32
	DB	105
	DB	110
	DB	116
	DB	46
	DB	32
	DB	97
	DB	118
	DB	101
	DB	114
	DB	97
	DB	103
	DB	101
	DB	100
	DB	32
	DB	111
	DB	118
	DB	101
	DB	114
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_88	DB	115
	DB	105
	DB	109
	DB	46
	DB	32
	DB	105
	DB	110
	DB	116
	DB	46
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_86	DB	83
	DB	112
	DB	101
	DB	101
	DB	100
	DB	32
	DB	111
	DB	110
	DB	32
	DB	116
	DB	104
	DB	101
	DB	32
	DB	113
	DB	117
	DB	101
	DB	117
	DB	101
	DB	45
	DB	102
	DB	114
	DB	101
	DB	101
	DB	32
	DB	112
	DB	111
	DB	114
	DB	116
	DB	105
	DB	111
	DB	110
	DB	32
	DB	111
	DB	102
	DB	32
	DB	116
	DB	104
	DB	101
	DB	32
	DB	108
	DB	105
	DB	110
	DB	107
	DB	0
__STRLITPACK_84	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_82	DB	84
	DB	104
	DB	105
	DB	115
	DB	32
	DB	102
	DB	105
	DB	108
	DB	101
	DB	32
	DB	112
	DB	114
	DB	111
	DB	118
	DB	105
	DB	100
	DB	101
	DB	115
	DB	32
	DB	116
	DB	104
	DB	101
	DB	32
	DB	97
	DB	118
	DB	101
	DB	114
	DB	97
	DB	103
	DB	101
	DB	32
	DB	115
	DB	112
	DB	101
	DB	101
	DB	100
	DB	32
	DB	111
	DB	110
	DB	0
__STRLITPACK_80	DB	116
	DB	104
	DB	101
	DB	32
	DB	113
	DB	117
	DB	101
	DB	117
	DB	101
	DB	45
	DB	102
	DB	114
	DB	101
	DB	101
	DB	32
	DB	112
	DB	111
	DB	114
	DB	116
	DB	105
	DB	111
	DB	110
	DB	32
	DB	111
	DB	110
	DB	32
	DB	108
	DB	105
	DB	110
	DB	107
	DB	115
	DB	32
	DB	101
	DB	118
	DB	101
	DB	114
	DB	121
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_77	DB	115
	DB	105
	DB	109
	DB	46
	DB	32
	DB	105
	DB	110
	DB	116
	DB	46
	DB	32
	DB	97
	DB	118
	DB	101
	DB	114
	DB	97
	DB	103
	DB	101
	DB	100
	DB	32
	DB	111
	DB	118
	DB	101
	DB	114
	DB	0
__STRLITPACK_76	DB	115
	DB	105
	DB	109
	DB	46
	DB	32
	DB	105
	DB	110
	DB	116
	DB	46
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_74	DB	68
	DB	101
	DB	110
	DB	115
	DB	105
	DB	116
	DB	121
	DB	32
	DB	111
	DB	110
	DB	32
	DB	116
	DB	104
	DB	101
	DB	32
	DB	113
	DB	117
	DB	101
	DB	117
	DB	101
	DB	45
	DB	102
	DB	114
	DB	101
	DB	101
	DB	32
	DB	112
	DB	111
	DB	114
	DB	116
	DB	105
	DB	111
	DB	110
	DB	32
	DB	111
	DB	102
	DB	32
	DB	116
	DB	104
	DB	101
	DB	32
	DB	108
	DB	105
	DB	110
	DB	107
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_72	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_70	DB	84
	DB	104
	DB	105
	DB	115
	DB	32
	DB	102
	DB	105
	DB	108
	DB	101
	DB	32
	DB	112
	DB	114
	DB	111
	DB	118
	DB	105
	DB	100
	DB	101
	DB	115
	DB	32
	DB	116
	DB	104
	DB	101
	DB	32
	DB	97
	DB	118
	DB	101
	DB	114
	DB	97
	DB	103
	DB	101
	DB	32
	DB	100
	DB	101
	DB	110
	DB	115
	DB	105
	DB	116
	DB	121
	DB	32
	DB	111
	DB	110
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_68	DB	116
	DB	104
	DB	101
	DB	32
	DB	113
	DB	117
	DB	101
	DB	117
	DB	101
	DB	45
	DB	102
	DB	114
	DB	101
	DB	101
	DB	32
	DB	112
	DB	111
	DB	114
	DB	116
	DB	105
	DB	111
	DB	110
	DB	32
	DB	111
	DB	110
	DB	32
	DB	108
	DB	105
	DB	110
	DB	107
	DB	115
	DB	32
	DB	112
	DB	101
	DB	114
	DB	0
__STRLITPACK_65	DB	115
	DB	105
	DB	109
	DB	46
	DB	32
	DB	105
	DB	110
	DB	116
	DB	46
	DB	32
	DB	97
	DB	118
	DB	101
	DB	114
	DB	97
	DB	103
	DB	101
	DB	100
	DB	32
	DB	111
	DB	118
	DB	101
	DB	114
	DB	0
__STRLITPACK_64	DB	115
	DB	105
	DB	109
	DB	46
	DB	32
	DB	105
	DB	110
	DB	116
	DB	46
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_62	DB	79
	DB	117
	DB	116
	DB	112
	DB	117
	DB	116
	DB	32
	DB	102
	DB	105
	DB	108
	DB	101
	DB	32
	DB	102
	DB	111
	DB	114
	DB	32
	DB	108
	DB	101
	DB	102
	DB	116
	DB	32
	DB	116
	DB	117
	DB	114
	DB	110
	DB	105
	DB	110
	DB	103
	DB	32
	DB	111
	DB	117
	DB	116
	DB	32
	DB	102
	DB	108
	DB	111
	DB	119
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_60	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_58	DB	84
	DB	104
	DB	105
	DB	115
	DB	32
	DB	102
	DB	105
	DB	108
	DB	101
	DB	32
	DB	112
	DB	114
	DB	111
	DB	118
	DB	105
	DB	100
	DB	101
	DB	115
	DB	32
	DB	116
	DB	104
	DB	101
	DB	32
	DB	97
	DB	118
	DB	101
	DB	114
	DB	97
	DB	103
	DB	101
	DB	32
	DB	110
	DB	117
	DB	109
	DB	98
	DB	101
	DB	114
	DB	32
	DB	111
	DB	102
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_56	DB	108
	DB	101
	DB	102
	DB	116
	DB	32
	DB	116
	DB	117
	DB	114
	DB	110
	DB	105
	DB	110
	DB	103
	DB	32
	DB	118
	DB	101
	DB	104
	DB	105
	DB	99
	DB	108
	DB	101
	DB	115
	DB	32
	DB	111
	DB	110
	DB	32
	DB	108
	DB	105
	DB	110
	DB	107
	DB	115
	DB	32
	DB	112
	DB	101
	DB	114
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_53	DB	115
	DB	105
	DB	109
	DB	46
	DB	32
	DB	105
	DB	110
	DB	116
	DB	46
	DB	32
	DB	97
	DB	118
	DB	101
	DB	114
	DB	97
	DB	103
	DB	101
	DB	100
	DB	32
	DB	111
	DB	118
	DB	101
	DB	114
	DB	0
__STRLITPACK_52	DB	115
	DB	105
	DB	109
	DB	46
	DB	32
	DB	105
	DB	110
	DB	116
	DB	46
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_50	DB	79
	DB	117
	DB	116
	DB	112
	DB	117
	DB	116
	DB	32
	DB	102
	DB	105
	DB	108
	DB	101
	DB	32
	DB	102
	DB	111
	DB	114
	DB	32
	DB	103
	DB	114
	DB	101
	DB	101
	DB	110
	DB	32
	DB	116
	DB	105
	DB	109
	DB	101
	DB	32
	DB	0
__STRLITPACK_48	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_46	DB	84
	DB	104
	DB	105
	DB	115
	DB	32
	DB	102
	DB	105
	DB	108
	DB	101
	DB	32
	DB	112
	DB	114
	DB	111
	DB	118
	DB	105
	DB	100
	DB	101
	DB	115
	DB	32
	DB	116
	DB	104
	DB	101
	DB	32
	DB	97
	DB	118
	DB	101
	DB	114
	DB	97
	DB	103
	DB	101
	DB	32
	DB	103
	DB	114
	DB	101
	DB	101
	DB	110
	DB	32
	DB	116
	DB	105
	DB	109
	DB	101
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_43	DB	102
	DB	111
	DB	114
	DB	32
	DB	101
	DB	97
	DB	99
	DB	104
	DB	32
	DB	108
	DB	105
	DB	110
	DB	107
	DB	32
	DB	101
	DB	118
	DB	101
	DB	114
	DB	121
	DB	32
	DB	112
	DB	101
	DB	114
	DB	32
	DB	115
	DB	105
	DB	109
	DB	46
	DB	32
	DB	105
	DB	110
	DB	116
	DB	46
	DB	32
	DB	97
	DB	118
	DB	101
	DB	114
	DB	97
	DB	116
	DB	101
	DB	100
	DB	32
	DB	111
	DB	118
	DB	101
	DB	114
	DB	0
__STRLITPACK_42	DB	115
	DB	105
	DB	109
	DB	46
	DB	32
	DB	105
	DB	110
	DB	116
	DB	46
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_40	DB	79
	DB	117
	DB	116
	DB	112
	DB	117
	DB	116
	DB	32
	DB	102
	DB	105
	DB	108
	DB	101
	DB	32
	DB	102
	DB	111
	DB	114
	DB	32
	DB	111
	DB	117
	DB	116
	DB	32
	DB	102
	DB	108
	DB	111
	DB	119
	DB	32
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_38	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_36	DB	84
	DB	104
	DB	105
	DB	115
	DB	32
	DB	102
	DB	105
	DB	108
	DB	101
	DB	32
	DB	112
	DB	114
	DB	111
	DB	118
	DB	105
	DB	100
	DB	101
	DB	115
	DB	32
	DB	116
	DB	104
	DB	101
	DB	32
	DB	97
	DB	118
	DB	101
	DB	114
	DB	97
	DB	103
	DB	101
	DB	32
	DB	110
	DB	117
	DB	109
	DB	98
	DB	101
	DB	114
	DB	32
	DB	111
	DB	102
	DB	32
	DB	118
	DB	101
	DB	104
	DB	105
	DB	99
	DB	108
	DB	101
	DB	115
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_33	DB	111
	DB	117
	DB	116
	DB	32
	DB	111
	DB	102
	DB	32
	DB	101
	DB	97
	DB	99
	DB	104
	DB	32
	DB	108
	DB	105
	DB	110
	DB	107
	DB	32
	DB	112
	DB	101
	DB	114
	DB	32
	DB	115
	DB	105
	DB	109
	DB	46
	DB	32
	DB	105
	DB	110
	DB	116
	DB	46
	DB	32
	DB	97
	DB	118
	DB	101
	DB	114
	DB	97
	DB	103
	DB	101
	DB	100
	DB	32
	DB	111
	DB	118
	DB	101
	DB	114
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_32	DB	115
	DB	105
	DB	109
	DB	115
	DB	32
	DB	105
	DB	110
	DB	116
	DB	115
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_30	DB	79
	DB	117
	DB	116
	DB	112
	DB	117
	DB	116
	DB	32
	DB	102
	DB	105
	DB	108
	DB	101
	DB	32
	DB	102
	DB	111
	DB	114
	DB	32
	DB	97
	DB	99
	DB	99
	DB	117
	DB	109
	DB	117
	DB	108
	DB	97
	DB	116
	DB	101
	DB	100
	DB	32
	DB	118
	DB	111
	DB	108
	DB	117
	DB	109
	DB	101
	DB	32
	DB	0
__STRLITPACK_28	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_26	DB	84
	DB	104
	DB	105
	DB	115
	DB	32
	DB	102
	DB	105
	DB	108
	DB	101
	DB	32
	DB	112
	DB	114
	DB	111
	DB	118
	DB	105
	DB	100
	DB	101
	DB	115
	DB	32
	DB	116
	DB	104
	DB	101
	DB	32
	DB	97
	DB	99
	DB	99
	DB	117
	DB	109
	DB	109
	DB	117
	DB	108
	DB	97
	DB	116
	DB	101
	DB	100
	DB	32
	DB	110
	DB	117
	DB	109
	DB	98
	DB	101
	DB	114
	DB	32
	DB	111
	DB	102
	DB	32
	DB	118
	DB	101
	DB	104
	DB	46
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_24	DB	111
	DB	110
	DB	32
	DB	111
	DB	102
	DB	32
	DB	101
	DB	97
	DB	99
	DB	104
	DB	32
	DB	108
	DB	105
	DB	110
	DB	107
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	49
	DB	48
	DB	32
	DB	101
	DB	118
	DB	101
	DB	114
	DB	121
	DB	32
	DB	115
	DB	105
	DB	109
	DB	115
	DB	32
	DB	105
	DB	110
	DB	116
	DB	115
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_22	DB	79
	DB	117
	DB	116
	DB	112
	DB	117
	DB	116
	DB	32
	DB	102
	DB	105
	DB	108
	DB	101
	DB	32
	DB	102
	DB	111
	DB	114
	DB	32
	DB	97
	DB	99
	DB	99
	DB	117
	DB	109
	DB	117
	DB	108
	DB	97
	DB	116
	DB	101
	DB	100
	DB	32
	DB	118
	DB	111
	DB	108
	DB	117
	DB	109
	DB	101
	DB	32
	DB	0
__STRLITPACK_20	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	61
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_18	DB	84
	DB	104
	DB	105
	DB	115
	DB	32
	DB	102
	DB	105
	DB	108
	DB	101
	DB	32
	DB	112
	DB	114
	DB	111
	DB	118
	DB	105
	DB	100
	DB	101
	DB	115
	DB	32
	DB	116
	DB	104
	DB	101
	DB	32
	DB	97
	DB	99
	DB	99
	DB	117
	DB	109
	DB	109
	DB	117
	DB	108
	DB	97
	DB	116
	DB	101
	DB	100
	DB	32
	DB	110
	DB	117
	DB	109
	DB	98
	DB	101
	DB	114
	DB	32
	DB	111
	DB	102
	DB	32
	DB	118
	DB	101
	DB	104
	DB	46
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_16	DB	111
	DB	110
	DB	32
	DB	111
	DB	102
	DB	32
	DB	101
	DB	97
	DB	99
	DB	104
	DB	32
	DB	108
	DB	105
	DB	110
	DB	107
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	49
	DB	48
	DB	32
	DB	101
	DB	118
	DB	101
	DB	114
	DB	121
	DB	32
	DB	115
	DB	105
	DB	109
	DB	115
	DB	32
	DB	105
	DB	110
	DB	116
	DB	115
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
EXTRN	_DYNUST_MAIN_MODULE_mp_ITERATION:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ITEMAX:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_I40:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_I39_T:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_I39:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_I38_T:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_I38:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_I37_T:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_I37:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_I36_T:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_I36:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_I35_T:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_I35:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_I34_T:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_I34:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_I33_T:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_I33:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_I32_T:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_I32:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_I31_T:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_I31:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_I30_T:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_I30:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_REACH_CONVERG:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_I18:BYTE
_DATA	ENDS
EXTRN	_for_write_seq_lis_xmit:PROC
EXTRN	_for_write_seq_fmt:PROC
EXTRN	_for_write_seq_lis:PROC
	INCLUDELIB <ifconsol>
	INCLUDELIB <libifcoremt>
	INCLUDELIB <libifport>
	INCLUDELIB <libmmt>
	INCLUDELIB <LIBCMT>
	INCLUDELIB <libirc>
	INCLUDELIB <svml_dispmt>
	INCLUDELIB <OLDNAMES>
	END
