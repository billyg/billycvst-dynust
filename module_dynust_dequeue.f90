MODULE DEQUEUE
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~ Copyright (C) 2002-2011 The Arizona Board of Regents on Behalf of The University of Arizona.  ~~!
!~~ Developed by Yi-Chang Chiu, Ph.D. as par to the DynusT project.                               ~~!
!~~ This program is free software: you can redistribute it and/or modify it                       ~~!
!~~ under the terms of the GNU General Public License version 2 as published by                   ~~!
!~~ the Free Software Foundation.                                                                 ~~!
!~~ This program is distributed in the hope that it will be useful,                               ~~!
!~~ but WITHOUT ANY WARRANTY; without even the implied warranty of                                ~~!
!~~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                                          ~~!
!~~ See the GNU General Public License for more details.                                          ~~!
!~~ You should have received a copy of the GNU General Public License                             ~~!
!~~ along with this program (in the file COPYING).                                                ~~!
!~~ IF not, see <http://www.gnu.org/licenses/>.                                                   ~~!
!~~ Please see http://www.dynust.net for contact information and names                            ~~!
!~~ of individual contributors.                                                                   ~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
    PUBLIC :: Create_Queue_Array, Destroy_Queue_Array, Enqueue_Back_Array, Enqueue_Front_Array
    PUBLIC :: Disqueue_Front_Array, Disqueue_Back_Array, Is_Empty_Array, Is_Full_Array
    
    INTEGER, PRIVATE :: M
    TYPE, PUBLIC :: Queue_Type_Array
        PRIVATE
        INTEGER :: Maximum_Size, Current_Size = 0, Front = 0, Rear = 1
        INTEGER, POINTER, DIMENSION(:) :: LinkID
        REAL, POINTER, DIMENSION(:) :: Label
    END TYPE !:: Queue_Type_Array

CONTAINS

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
SUBROUTINE Set_Queue_Size( QueSize )
    INTEGER QueSize
    ! start SUBROUTINE Create_Queue
    M = QueSize
    RETURN
END SUBROUTINE Set_Queue_Size

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
SUBROUTINE Create_Queue_Array( InternQ )
    TYPE (Queue_Type_Array), INTENT(in out) :: InternQ
    ! start SUBROUTINE Create_Queue
    ALLOCATE(InternQ%LinkID(0:M-1))
    ALLOCATE(InternQ%Label(0:M-1))    
    InternQ%Maximum_Size = M
    InternQ%LinkID = 0
    InternQ%Label = 0    
    RETURN
END SUBROUTINE Create_Queue_Array

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
SUBROUTINE Destroy_Queue_Array( InternQ )
    TYPE (Queue_Type_Array), INTENT(in out) :: InternQ
    INTEGER :: LinkID
    REAL :: Label
    ! start SUBROUTINE Destroy_Queue
    DO WHILE (.not. Is_Empty_Array( InternQ ))
        CALL Disqueue_Front_Array( InternQ, LinkID, Label )
        !PRINT *, " Left on Queue: ", Info
    END DO
    DEALLOCATE( InternQ%LinkID )
    DEALLOCATE( InternQ%Label )
    RETURN
END SUBROUTINE Destroy_Queue_Array

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
SUBROUTINE Enqueue_Back_Array( InternQ, LinkID, Label )
    TYPE (Queue_Type_Array), INTENT(in out) :: InternQ
    INTEGER, INTENT(in) :: LinkID
    REAL, INTENT(in) :: Label
    ! start SUBROUTINE Enqueue
    IF (Is_Full_Array( InternQ )) then
        PRINT *, " Attempt to Enqueue when queue is full. "
    ELSE
        InternQ%LinkID(InternQ%Rear) = LinkID
        InternQ%LinkID(InternQ%Rear) = Label
        IF(InternQ%Rear == InternQ%Maximum_Size-1) THEN
            InternQ%Rear = 0
        ELSE
            InternQ%Rear = InternQ%Rear + 1
        ENDIF
        InternQ%Current_Size = InternQ % Current_Size + 1
    END IF
    RETURN
END SUBROUTINE Enqueue_Back_Array

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
SUBROUTINE Enqueue_Front_Array( InternQ, LinkID, Label )
    TYPE (Queue_Type_Array), INTENT(in out) :: InternQ
    INTEGER, INTENT(in) :: LinkID
    REAL, INTENT(in) :: Label
    ! start SUBROUTINE Enqueue
    IF (Is_Full_Array( InternQ )) then
        PRINT *, " Attempt to Enqueue when queue is full. "
    ELSE
        InternQ%LinkID(InternQ%Front) = LinkID
        InternQ%Label(InternQ%Front) = Label
        IF(InternQ%Front == 0) THEN
            InternQ%Front = InternQ%Maximum_Size-1
        ELSE
            InternQ%Front = InternQ%Front - 1
        ENDIF
        InternQ%Current_Size = InternQ%Current_Size + 1
    END IF
    RETURN
END SUBROUTINE Enqueue_Front_Array

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
SUBROUTINE Disqueue_Front_Array( InternQ, LinkID, Label )
    TYPE (Queue_Type_Array), INTENT(in out) :: InternQ
    INTEGER, INTENT(out) :: LinkID
    REAL, INTENT(out) :: Label    
    ! start SUBROUTINE Disqueue
    IF (Is_Empty_Array( InternQ )) then
        PRINT *, " Attempt to Disqueue when queue is empty. "
        STOP
        LinkID = 0
        Label = 0.0
    ELSE
        IF(InternQ%Front < InternQ%Maximum_Size-1) THEN
            InternQ%Front = InternQ%Front + 1
        ELSE
            InternQ%Front = 0
        ENDIF
        LinkID = InternQ%LinkID(InternQ%Front)
        Label = InternQ%Label(InternQ%Front)
        InternQ%LinkID(InternQ%Front) = 0
        InternQ%Label(InternQ%Front) = 0.0
        InternQ%Current_Size = InternQ%Current_Size - 1
    END IF
    RETURN
END SUBROUTINE Disqueue_Front_Array

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
SUBROUTINE Disqueue_Back_Array( InternQ, LinkID, Label )
    TYPE (Queue_Type_Array), INTENT(in out) :: InternQ
    INTEGER, INTENT(out) :: LinkID
    REAL, INTENT(out) :: Label        
    ! start SUBROUTINE Disqueue
    IF (Is_Empty_Array( InternQ )) then
        PRINT *, " Attempt to Disqueue when queue is empty. "
        LinkID = 0
        Label = 0.0
    ELSE
        IF(InternQ%Rear > 0) THEN
            InternQ%Rear = InternQ%Rear - 1
        ELSE
            InternQ%Rear = InternQ%Maximum_Size - 1
        ENDIF
        LinkID = InternQ%LinkID(InternQ%Rear)
        Label = InternQ%Label(InternQ%Rear)
        InternQ%LinkID(InternQ%Rear) = 0
        InternQ%Label(InternQ%Rear) = 0
        InternQ%Current_Size = InternQ%Current_Size - 1
    END IF
    RETURN
END SUBROUTINE Disqueue_Back_Array

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
PURE FUNCTION Is_Empty_Array( InternQ ) result( X )
    TYPE (Queue_Type_Array), INTENT(in) :: InternQ
    LOGICAL :: X
    ! start FUNCTION Is_Empty
    X = (InternQ % Current_Size <= 0)
    RETURN
END FUNCTION Is_Empty_Array

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
PURE FUNCTION Is_Full_Array( InternQ ) result( X )
    TYPE (Queue_Type_Array), INTENT(in) :: InternQ
    LOGICAL :: X
    ! start FUNCTION Is_Full
    X = (InternQ%Current_Size >= InternQ%Maximum_Size)
    RETURN
END FUNCTION Is_Full_Array

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
SUBROUTINE Print_Queue_Array( InternQ )
    TYPE (Queue_Type_Array), INTENT(in) :: InternQ
    WRITE(*,*) (InternQ%LinkID(k), k = 0, M-1)
    WRITE(*,*) "Front Back Poiner and size", InternQ%Front, InternQ%rear, InternQ%Current_Size
    RETURN
END SUBROUTINE Print_Queue_Array

END MODULE DEQUEUE