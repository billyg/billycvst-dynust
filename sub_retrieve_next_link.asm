; mark_description "Intel(R) Visual Fortran Compiler XE for applications running on IA-32, Version 13.1.1.171 Build 20130313";
; mark_description "-S";
	.686P
 	.387
	OPTION DOTNAME
	ASSUME	CS:FLAT,DS:FLAT,SS:FLAT
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
TXTST0:
; -- Begin  _RETRIEVE_NEXT_LINK
; mark_begin;
IF @Version GE 800
  .MMX
ELSEIF @Version GE 612
  .MMX
  MMWORD TEXTEQU <QWORD>
ENDIF
IF @Version GE 800
  .XMM
ELSEIF @Version GE 614
  .XMM
  XMMWORD TEXTEQU <OWORD>
ENDIF
       ALIGN     16
	PUBLIC _RETRIEVE_NEXT_LINK
_RETRIEVE_NEXT_LINK	PROC NEAR 
; parameter 1: 8 + ebp
; parameter 2: 12 + ebp
; parameter 3: 16 + ebp
; parameter 4: 20 + ebp
; parameter 5: 24 + ebp
; parameter 6: 28 + ebp
.B1.1:                          ; Preds .B1.0
        push      ebp                                           ;1.12
        mov       ebp, esp                                      ;1.12
        and       esp, -16                                      ;1.12
        push      esi                                           ;1.12
        push      edi                                           ;1.12
        push      ebx                                           ;1.12
        sub       esp, 180                                      ;1.12
        xor       edx, edx                                      ;35.2
        mov       eax, DWORD PTR [28+ebp]                       ;1.12
        mov       ebx, DWORD PTR [20+ebp]                       ;1.12
        mov       ecx, DWORD PTR [24+ebp]                       ;1.12
        mov       DWORD PTR [eax], edx                          ;35.2
        mov       eax, DWORD PTR [ebx]                          ;38.2
        inc       eax                                           ;38.2
        mov       DWORD PTR [ecx], edx                          ;36.2
        lea       edx, DWORD PTR [116+esp]                      ;40.15
        mov       DWORD PTR [116+esp], eax                      ;38.2
        push      OFFSET FLAT: __NLITPACK_0.0.1                 ;40.15
        push      edx                                           ;40.15
        push      DWORD PTR [12+ebp]                            ;40.15
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_VALUE   ;40.15
                                ; LOE f1
.B1.53:                         ; Preds .B1.1
        fstp      DWORD PTR [180+esp]                           ;40.15
        movss     xmm3, DWORD PTR [180+esp]                     ;40.15
        add       esp, 12                                       ;40.15
                                ; LOE xmm3
.B1.2:                          ; Preds .B1.53
        movss     xmm0, DWORD PTR [_2il0floatpacket.7]          ;40.10
        andps     xmm0, xmm3                                    ;40.10
        pxor      xmm3, xmm0                                    ;40.10
        movss     xmm1, DWORD PTR [_2il0floatpacket.8]          ;40.10
        movaps    xmm2, xmm3                                    ;40.10
        movaps    xmm7, xmm3                                    ;40.10
        cmpltss   xmm2, xmm1                                    ;40.10
        imul      ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32], 152 ;42.6
        andps     xmm1, xmm2                                    ;40.10
        mov       edx, DWORD PTR [16+ebp]                       ;1.12
        addss     xmm7, xmm1                                    ;40.10
        mov       eax, DWORD PTR [edx]                          ;42.17
        subss     xmm7, xmm1                                    ;40.10
        imul      edx, eax, 152                                 ;42.6
        movaps    xmm6, xmm7                                    ;40.10
        mov       DWORD PTR [104+esp], eax                      ;42.17
        subss     xmm6, xmm3                                    ;40.10
        movss     xmm3, DWORD PTR [_2il0floatpacket.9]          ;40.10
        movaps    xmm4, xmm6                                    ;40.10
        movaps    xmm5, xmm3                                    ;40.10
        cmpnless  xmm4, xmm3                                    ;40.10
        addss     xmm5, xmm3                                    ;40.10
        cmpless   xmm6, DWORD PTR [_2il0floatpacket.10]         ;40.10
        andps     xmm4, xmm5                                    ;40.10
        andps     xmm6, xmm5                                    ;40.10
        subss     xmm7, xmm4                                    ;40.10
        addss     xmm7, xmm6                                    ;40.10
        orps      xmm7, xmm0                                    ;40.10
        cvtss2si  ecx, xmm7                                     ;40.2
        mov       DWORD PTR [124+esp], ecx                      ;40.2
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;42.6
        add       edx, ecx                                      ;42.6
        sub       edx, ebx                                      ;42.6
        sub       ecx, ebx                                      ;
        movsx     esi, BYTE PTR [32+edx]                        ;42.17
        test      esi, esi                                      ;42.6
        mov       DWORD PTR [esp], esi                          ;42.17
        jle       .B1.6         ; Prob 2%                       ;42.6
                                ; LOE edx ecx
.B1.3:                          ; Preds .B1.2
        mov       esi, DWORD PTR [68+edx]                       ;43.8
        mov       ebx, 1                                        ;
        mov       edi, DWORD PTR [64+edx]                       ;43.8
        imul      esi, edi                                      ;
        mov       eax, DWORD PTR [36+edx]                       ;43.8
        mov       edx, edi                                      ;
        mov       DWORD PTR [4+esp], edi                        ;
        sub       eax, esi                                      ;
        mov       edi, DWORD PTR [124+esp]                      ;
                                ; LOE eax edx ecx ebx edi
.B1.4:                          ; Preds .B1.5 .B1.3
        imul      esi, DWORD PTR [edx+eax], 152                 ;43.11
        cmp       edi, DWORD PTR [24+ecx+esi]                   ;43.83
        je        .B1.49        ; Prob 20%                      ;43.83
                                ; LOE eax edx ecx ebx edi
.B1.5:                          ; Preds .B1.4
        inc       ebx                                           ;49.6
        add       edx, DWORD PTR [4+esp]                        ;49.6
        cmp       ebx, DWORD PTR [esp]                          ;49.6
        jle       .B1.4         ; Prob 82%                      ;49.6
                                ; LOE eax edx ecx ebx edi
.B1.6:                          ; Preds .B1.2 .B1.5
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;51.5
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;51.5
        mov       DWORD PTR [16+esp], eax                       ;51.5
        mov       DWORD PTR [12+esp], 0                         ;
                                ; LOE edx ecx
.B1.7:                          ; Preds .B1.49 .B1.6
        mov       edi, DWORD PTR [12+ebp]                       ;51.5
        neg       edx                                           ;53.63
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_BACKPOINTR+32] ;52.10
        neg       esi                                           ;53.63
        mov       eax, DWORD PTR [edi]                          ;51.5
        add       edx, eax                                      ;53.63
        imul      edi, DWORD PTR [104+esp], 152                 ;53.63
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_BACKPOINTR] ;52.10
        add       esi, DWORD PTR [24+edi+ecx]                   ;53.63
        mov       DWORD PTR [96+esp], eax                       ;51.5
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+44] ;53.8
        neg       eax                                           ;52.5
        add       eax, DWORD PTR [12+edi+ecx]                   ;52.5
        sub       eax, DWORD PTR [ebx+esi*4]                    ;52.5
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+40] ;52.5
        imul      eax, esi                                      ;52.5
        neg       esi                                           ;53.63
        shl       edx, 8                                        ;53.63
        mov       DWORD PTR [esp], eax                          ;52.5
        mov       eax, DWORD PTR [16+esp]                       ;51.5
        mov       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE+32] ;53.5
        neg       ebx                                           ;52.5
        mov       edi, DWORD PTR [148+eax+edx]                  ;51.5
        shl       edi, 2                                        ;53.63
        neg       edi                                           ;53.63
        add       edi, DWORD PTR [116+eax+edx]                  ;53.63
        imul      edx, DWORD PTR [4+edi], 152                   ;51.11
        add       ebx, DWORD PTR [12+edx+ecx]                   ;52.5
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE] ;53.5
        sub       ecx, esi                                      ;53.63
        mov       edx, DWORD PTR [esp]                          ;53.8
        lea       eax, DWORD PTR [ecx+ebx*8]                    ;52.5
        movss     xmm0, DWORD PTR [eax+edx]                     ;53.8
        comiss    xmm0, DWORD PTR [_2il0floatpacket.6]          ;53.57
        jbe       .B1.17        ; Prob 50%                      ;53.57
                                ; LOE
.B1.8:                          ; Preds .B1.7
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ISIGCOUNT], 1 ;53.78
        jle       .B1.17        ; Prob 16%                      ;53.78
                                ; LOE
.B1.9:                          ; Preds .B1.8
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;54.8
        lea       ebx, DWORD PTR [36+esp]                       ;56.13
        neg       edx                                           ;54.8
        lea       esi, DWORD PTR [32+esp]                       ;56.13
        add       edx, DWORD PTR [96+esp]                       ;54.8
        shl       edx, 5                                        ;54.8
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;54.8
        movsx     ecx, WORD PTR [20+eax+edx]                    ;54.14
        mov       DWORD PTR [32+esp], ecx                       ;54.8
        push      ebx                                           ;56.13
        push      OFFSET FLAT: __NLITPACK_1.0.1                 ;56.13
        push      esi                                           ;56.13
        push      DWORD PTR [20+ebp]                            ;56.13
        push      OFFSET FLAT: __NLITPACK_0.0.1                 ;56.13
        push      DWORD PTR [16+ebp]                            ;56.13
        push      DWORD PTR [12+ebp]                            ;56.13
        call      _RETRIEVE_VEH_PATH_ASTAR                      ;56.13
                                ; LOE
.B1.10:                         ; Preds .B1.9
        mov       eax, DWORD PTR [20+ebp]                       ;57.9
        lea       ecx, DWORD PTR [68+esp]                       ;57.9
        mov       edx, DWORD PTR [eax]                          ;57.9
        inc       edx                                           ;57.14
        mov       DWORD PTR [68+esp], edx                       ;57.14
        push      OFFSET FLAT: __NLITPACK_0.0.1                 ;57.9
        push      ecx                                           ;57.9
        push      DWORD PTR [12+ebp]                            ;57.9
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_VALUE   ;57.9
                                ; LOE f1
.B1.54:                         ; Preds .B1.10
        fstp      DWORD PTR [208+esp]                           ;57.9
        movss     xmm3, DWORD PTR [208+esp]                     ;57.9
                                ; LOE xmm3
.B1.11:                         ; Preds .B1.54
        movss     xmm0, DWORD PTR [_2il0floatpacket.7]          ;57.9
        lea       edi, DWORD PTR [84+esp]                       ;57.9
        andps     xmm0, xmm3                                    ;57.9
        pxor      xmm3, xmm0                                    ;57.9
        movss     xmm1, DWORD PTR [_2il0floatpacket.8]          ;57.9
        movaps    xmm2, xmm3                                    ;57.9
        movaps    xmm7, xmm3                                    ;57.9
        cmpltss   xmm2, xmm1                                    ;57.9
        andps     xmm1, xmm2                                    ;57.9
        mov       ecx, DWORD PTR [20+ebp]                       ;57.9
        addss     xmm7, xmm1                                    ;57.9
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_BACKPOINTR+32] ;57.9
        subss     xmm7, xmm1                                    ;57.9
        movaps    xmm6, xmm7                                    ;57.9
        neg       edx                                           ;57.9
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_BACKPOINTR] ;57.9
        subss     xmm6, xmm3                                    ;57.9
        movss     xmm3, DWORD PTR [_2il0floatpacket.9]          ;57.9
        movaps    xmm4, xmm6                                    ;57.9
        movaps    xmm5, xmm3                                    ;57.9
        cmpnless  xmm4, xmm3                                    ;57.9
        addss     xmm5, xmm3                                    ;57.9
        cmpless   xmm6, DWORD PTR [_2il0floatpacket.10]         ;57.9
        andps     xmm4, xmm5                                    ;57.9
        andps     xmm6, xmm5                                    ;57.9
        mov       esi, DWORD PTR [ecx]                          ;57.9
        subss     xmm7, xmm4                                    ;57.9
        inc       esi                                           ;57.58
        addss     xmm7, xmm6                                    ;57.9
        orps      xmm7, xmm0                                    ;57.9
        cvtss2si  ebx, xmm7                                     ;57.14
        add       edx, ebx                                      ;57.9
        mov       DWORD PTR [84+esp], esi                       ;57.58
        push      OFFSET FLAT: __NLITPACK_0.0.1                 ;57.9
        push      edi                                           ;57.9
        push      DWORD PTR [12+ebp]                            ;57.9
        mov       ebx, DWORD PTR [eax+edx*4]                    ;57.9
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_VALUE   ;57.9
                                ; LOE ebx f1
.B1.55:                         ; Preds .B1.11
        fstp      DWORD PTR [220+esp]                           ;57.9
        movss     xmm3, DWORD PTR [220+esp]                     ;57.9
        add       esp, 52                                       ;57.9
                                ; LOE ebx xmm3
.B1.12:                         ; Preds .B1.55
        movss     xmm0, DWORD PTR [_2il0floatpacket.7]          ;57.9
        andps     xmm0, xmm3                                    ;57.9
        pxor      xmm3, xmm0                                    ;57.9
        movss     xmm1, DWORD PTR [_2il0floatpacket.8]          ;57.9
        movaps    xmm2, xmm3                                    ;57.9
        movaps    xmm7, xmm3                                    ;57.9
        cmpltss   xmm2, xmm1                                    ;57.9
        imul      esi, ebx, 152                                 ;
        andps     xmm1, xmm2                                    ;57.9
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_BACKPOINTR+32] ;57.9
        addss     xmm7, xmm1                                    ;57.9
        neg       ecx                                           ;57.58
        subss     xmm7, xmm1                                    ;57.9
        movaps    xmm6, xmm7                                    ;57.9
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_BACKPOINTR] ;57.9
        subss     xmm6, xmm3                                    ;57.9
        movss     xmm3, DWORD PTR [_2il0floatpacket.9]          ;57.9
        movaps    xmm4, xmm6                                    ;57.9
        movaps    xmm5, xmm3                                    ;57.9
        cmpnless  xmm4, xmm3                                    ;57.9
        addss     xmm5, xmm3                                    ;57.9
        cmpless   xmm6, DWORD PTR [_2il0floatpacket.10]         ;57.9
        andps     xmm4, xmm5                                    ;57.9
        andps     xmm6, xmm5                                    ;57.9
        mov       edi, DWORD PTR [16+ebp]                       ;72.5
        subss     xmm7, xmm4                                    ;57.9
        mov       DWORD PTR [esp], esi                          ;
        addss     xmm7, xmm6                                    ;57.9
        orps      xmm7, xmm0                                    ;57.9
        cvtss2si  eax, xmm7                                     ;57.9
        add       ecx, eax                                      ;57.58
        mov       eax, DWORD PTR [4+edx+ecx*4]                  ;57.58
        dec       eax                                           ;57.9
        mov       edx, DWORD PTR [edi]                          ;72.5
        cmp       eax, ebx                                      ;57.9
        mov       DWORD PTR [104+esp], edx                      ;72.5
        jl        .B1.16        ; Prob 10%                      ;57.9
                                ; LOE eax ebx
.B1.13:                         ; Preds .B1.12
        imul      edx, DWORD PTR [104+esp], 152                 ;58.14
        imul      esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32], 152 ;
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;58.11
        add       edx, ecx                                      ;58.14
        sub       edx, esi                                      ;58.14
        sub       ecx, esi                                      ;
        mov       esi, DWORD PTR [esp]                          ;
        mov       edx, DWORD PTR [24+edx]                       ;58.14
                                ; LOE eax edx ecx ebx esi
.B1.14:                         ; Preds .B1.15 .B1.13
        cmp       edx, DWORD PTR [4+esi+ecx]                    ;58.48
        je        .B1.47        ; Prob 20%                      ;58.48
                                ; LOE eax edx ecx ebx esi
.B1.15:                         ; Preds .B1.14
        inc       ebx                                           ;62.9
        add       esi, 152                                      ;62.9
        cmp       ebx, eax                                      ;62.9
        jle       .B1.14        ; Prob 82%                      ;62.9
                                ; LOE eax edx ecx ebx esi
.B1.16:                         ; Preds .B1.12 .B1.15
        mov       eax, DWORD PTR [12+ebp]                       ;67.34
        mov       edx, DWORD PTR [eax]                          ;67.34
        mov       DWORD PTR [96+esp], edx                       ;67.34
                                ; LOE
.B1.17:                         ; Preds .B1.47 .B1.16 .B1.8 .B1.7
        test      BYTE PTR [12+esp], 1                          ;65.13
        je        .B1.19        ; Prob 60%                      ;65.13
                                ; LOE
.B1.18:                         ; Preds .B1.17
        mov       edx, DWORD PTR [96+esp]                       ;65.86
        sub       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;65.86
        shl       edx, 8                                        ;65.86
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;65.8
        mov       ecx, DWORD PTR [148+eax+edx]                  ;65.26
        shl       ecx, 2                                        ;65.86
        neg       ecx                                           ;65.86
        mov       eax, DWORD PTR [116+eax+edx]                  ;65.26
        mov       edx, DWORD PTR [4+ecx+eax]                    ;65.26
        cmp       edx, DWORD PTR [104+esp]                      ;65.23
        jne       .B1.44        ; Prob 50%                      ;65.23
                                ; LOE eax edx ecx
.B1.19:                         ; Preds .B1.45 .B1.44 .B1.18 .B1.17
        mov       DWORD PTR [esp], 0                            ;66.8
        lea       ebx, DWORD PTR [esp]                          ;66.8
        mov       DWORD PTR [48+esp], 27                        ;66.8
        lea       eax, DWORD PTR [48+esp]                       ;66.8
        mov       DWORD PTR [52+esp], OFFSET FLAT: __STRLITPACK_16 ;66.8
        push      32                                            ;66.8
        push      eax                                           ;66.8
        push      OFFSET FLAT: __STRLITPACK_18.0.1              ;66.8
        push      -2088435968                                   ;66.8
        push      911                                           ;66.8
        push      ebx                                           ;66.8
        call      _for_write_seq_lis                            ;66.8
                                ; LOE ebx
.B1.20:                         ; Preds .B1.19
        mov       DWORD PTR [24+esp], 0                         ;67.5
        lea       eax, DWORD PTR [80+esp]                       ;67.5
        mov       DWORD PTR [80+esp], 12                        ;67.5
        mov       DWORD PTR [84+esp], OFFSET FLAT: __STRLITPACK_14 ;67.5
        push      32                                            ;67.5
        push      eax                                           ;67.5
        push      OFFSET FLAT: __STRLITPACK_19.0.1              ;67.5
        push      -2088435968                                   ;67.5
        push      911                                           ;67.5
        push      ebx                                           ;67.5
        call      _for_write_seq_lis                            ;67.5
                                ; LOE ebx
.B1.21:                         ; Preds .B1.20
        mov       esi, DWORD PTR [144+esp]                      ;67.5
        lea       edx, DWORD PTR [160+esp]                      ;67.5
        sub       esi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND+32] ;67.5
        shl       esi, 5                                        ;67.5
        mov       edi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND] ;67.5
        movzx     eax, BYTE PTR [5+edi+esi]                     ;67.5
        mov       BYTE PTR [160+esp], al                        ;67.5
        push      edx                                           ;67.5
        push      OFFSET FLAT: __STRLITPACK_20.0.1              ;67.5
        push      ebx                                           ;67.5
        call      _for_write_seq_lis_xmit                       ;67.5
                                ; LOE ebx esi edi
.B1.22:                         ; Preds .B1.21
        mov       DWORD PTR [60+esp], 0                         ;68.5
        lea       eax, DWORD PTR [124+esp]                      ;68.5
        mov       DWORD PTR [124+esp], 13                       ;68.5
        mov       DWORD PTR [128+esp], OFFSET FLAT: __STRLITPACK_12 ;68.5
        push      32                                            ;68.5
        push      eax                                           ;68.5
        push      OFFSET FLAT: __STRLITPACK_21.0.1              ;68.5
        push      -2088435968                                   ;68.5
        push      911                                           ;68.5
        push      ebx                                           ;68.5
        call      _for_write_seq_lis                            ;68.5
                                ; LOE ebx esi edi
.B1.23:                         ; Preds .B1.22
        movzx     eax, BYTE PTR [4+edi+esi]                     ;68.5
        lea       edx, DWORD PTR [204+esp]                      ;68.5
        mov       BYTE PTR [204+esp], al                        ;68.5
        push      edx                                           ;68.5
        push      OFFSET FLAT: __STRLITPACK_22.0.1              ;68.5
        push      ebx                                           ;68.5
        call      _for_write_seq_lis_xmit                       ;68.5
                                ; LOE ebx esi edi
.B1.24:                         ; Preds .B1.23
        mov       DWORD PTR [96+esp], 0                         ;69.8
        lea       eax, DWORD PTR [168+esp]                      ;69.8
        mov       DWORD PTR [168+esp], 19                       ;69.8
        mov       DWORD PTR [172+esp], OFFSET FLAT: __STRLITPACK_10 ;69.8
        push      32                                            ;69.8
        push      eax                                           ;69.8
        push      OFFSET FLAT: __STRLITPACK_23.0.1              ;69.8
        push      -2088435968                                   ;69.8
        push      911                                           ;69.8
        push      ebx                                           ;69.8
        call      _for_write_seq_lis                            ;69.8
                                ; LOE ebx esi edi
.B1.56:                         ; Preds .B1.24
        add       esp, 120                                      ;69.8
                                ; LOE ebx esi edi
.B1.25:                         ; Preds .B1.56
        movzx     eax, WORD PTR [20+edi+esi]                    ;69.8
        lea       edx, DWORD PTR [128+esp]                      ;69.8
        mov       WORD PTR [128+esp], ax                        ;69.8
        push      edx                                           ;69.8
        push      OFFSET FLAT: __STRLITPACK_24.0.1              ;69.8
        push      ebx                                           ;69.8
        call      _for_write_seq_lis_xmit                       ;69.8
                                ; LOE ebx
.B1.26:                         ; Preds .B1.25
        mov       DWORD PTR [12+esp], 0                         ;70.5
        lea       eax, DWORD PTR [92+esp]                       ;70.5
        mov       DWORD PTR [92+esp], 14                        ;70.5
        mov       DWORD PTR [96+esp], OFFSET FLAT: __STRLITPACK_8 ;70.5
        push      32                                            ;70.5
        push      eax                                           ;70.5
        push      OFFSET FLAT: __STRLITPACK_25.0.1              ;70.5
        push      -2088435968                                   ;70.5
        push      911                                           ;70.5
        push      ebx                                           ;70.5
        call      _for_write_seq_lis                            ;70.5
                                ; LOE ebx
.B1.27:                         ; Preds .B1.26
        mov       eax, DWORD PTR [132+esp]                      ;70.5
        lea       edx, DWORD PTR [172+esp]                      ;70.5
        mov       DWORD PTR [172+esp], eax                      ;70.5
        push      edx                                           ;70.5
        push      OFFSET FLAT: __STRLITPACK_26.0.1              ;70.5
        push      ebx                                           ;70.5
        call      _for_write_seq_lis_xmit                       ;70.5
                                ; LOE ebx
.B1.28:                         ; Preds .B1.27
        mov       DWORD PTR [48+esp], 0                         ;71.5
        lea       eax, DWORD PTR [136+esp]                      ;71.5
        mov       DWORD PTR [136+esp], 14                       ;71.5
        mov       DWORD PTR [140+esp], OFFSET FLAT: __STRLITPACK_6 ;71.5
        push      32                                            ;71.5
        push      eax                                           ;71.5
        push      OFFSET FLAT: __STRLITPACK_27.0.1              ;71.5
        push      -2088435968                                   ;71.5
        push      911                                           ;71.5
        push      ebx                                           ;71.5
        call      _for_write_seq_lis                            ;71.5
                                ; LOE ebx
.B1.29:                         ; Preds .B1.28
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE+32] ;71.5
        lea       esi, DWORD PTR [216+esp]                      ;71.5
        neg       edx                                           ;71.5
        add       edx, DWORD PTR [168+esp]                      ;71.5
        shl       edx, 6                                        ;71.5
        mov       eax, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE] ;71.5
        movzx     ecx, WORD PTR [12+eax+edx]                    ;71.5
        mov       WORD PTR [216+esp], cx                        ;71.5
        push      esi                                           ;71.5
        push      OFFSET FLAT: __STRLITPACK_28.0.1              ;71.5
        push      ebx                                           ;71.5
        call      _for_write_seq_lis_xmit                       ;71.5
                                ; LOE ebx
.B1.30:                         ; Preds .B1.29
        mov       DWORD PTR [84+esp], 0                         ;72.5
        lea       eax, DWORD PTR [180+esp]                      ;72.5
        mov       DWORD PTR [180+esp], 21                       ;72.5
        mov       DWORD PTR [184+esp], OFFSET FLAT: __STRLITPACK_4 ;72.5
        push      32                                            ;72.5
        push      eax                                           ;72.5
        push      OFFSET FLAT: __STRLITPACK_29.0.1              ;72.5
        push      -2088435968                                   ;72.5
        push      911                                           ;72.5
        push      ebx                                           ;72.5
        call      _for_write_seq_lis                            ;72.5
                                ; LOE ebx
.B1.31:                         ; Preds .B1.30
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;72.5
        neg       eax                                           ;72.5
        add       eax, DWORD PTR [212+esp]                      ;72.5
        imul      ecx, eax, 152                                 ;72.5
        imul      edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE+32], 44 ;72.5
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;72.5
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE] ;72.5
        imul      eax, DWORD PTR [24+edx+ecx], 44               ;72.43
        lea       ecx, DWORD PTR [260+esp]                      ;72.5
        add       eax, esi                                      ;72.5
        sub       eax, edi                                      ;72.5
        mov       edx, DWORD PTR [36+eax]                       ;72.5
        mov       DWORD PTR [260+esp], edx                      ;72.5
        push      ecx                                           ;72.5
        push      OFFSET FLAT: __STRLITPACK_30.0.1              ;72.5
        push      ebx                                           ;72.5
        call      _for_write_seq_lis_xmit                       ;72.5
                                ; LOE ebx esi edi
.B1.57:                         ; Preds .B1.31
        add       esp, 120                                      ;72.5
                                ; LOE ebx esi edi
.B1.32:                         ; Preds .B1.57
        mov       DWORD PTR [esp], 0                            ;73.5
        lea       eax, DWORD PTR [104+esp]                      ;73.5
        mov       DWORD PTR [104+esp], 9                        ;73.5
        mov       DWORD PTR [108+esp], OFFSET FLAT: __STRLITPACK_2 ;73.5
        push      32                                            ;73.5
        push      eax                                           ;73.5
        push      OFFSET FLAT: __STRLITPACK_31.0.1              ;73.5
        push      -2088435968                                   ;73.5
        push      911                                           ;73.5
        push      ebx                                           ;73.5
        call      _for_write_seq_lis                            ;73.5
                                ; LOE ebx esi edi
.B1.33:                         ; Preds .B1.32
        imul      eax, DWORD PTR [148+esp], 44                  ;40.2
        lea       ecx, DWORD PTR [184+esp]                      ;73.5
        add       eax, esi                                      ;73.5
        sub       eax, edi                                      ;73.5
        mov       edx, DWORD PTR [36+eax]                       ;73.5
        mov       DWORD PTR [184+esp], edx                      ;73.5
        push      ecx                                           ;73.5
        push      OFFSET FLAT: __STRLITPACK_32.0.1              ;73.5
        push      ebx                                           ;73.5
        call      _for_write_seq_lis_xmit                       ;73.5
                                ; LOE ebx
.B1.34:                         ; Preds .B1.33
        xor       eax, eax                                      ;74.8
        mov       DWORD PTR [36+esp], eax                       ;74.8
        push      32                                            ;74.8
        push      OFFSET FLAT: RETRIEVE_NEXT_LINK$format_pack.0.1 ;74.8
        push      eax                                           ;74.8
        push      OFFSET FLAT: __STRLITPACK_33.0.1              ;74.8
        push      -2088435968                                   ;74.8
        push      911                                           ;74.8
        push      ebx                                           ;74.8
        call      _for_write_seq_fmt                            ;74.8
                                ; LOE ebx
.B1.35:                         ; Preds .B1.34
        push      DWORD PTR [12+ebp]                            ;74.103
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_P_SIZE  ;74.103
                                ; LOE eax ebx
.B1.58:                         ; Preds .B1.35
        add       esp, 68                                       ;74.103
                                ; LOE eax ebx
.B1.36:                         ; Preds .B1.58
        mov       DWORD PTR [124+esp], 1                        ;74.8
        test      eax, eax                                      ;74.8
        jle       .B1.42        ; Prob 2%                       ;74.8
                                ; LOE eax ebx
.B1.37:                         ; Preds .B1.36
        mov       DWORD PTR [132+esp], eax                      ;74.8
        lea       esi, DWORD PTR [124+esp]                      ;74.8
        mov       edi, DWORD PTR [12+ebp]                       ;74.8
                                ; LOE ebx esi edi
.B1.38:                         ; Preds .B1.40 .B1.37
        push      OFFSET FLAT: __NLITPACK_0.0.1                 ;74.8
        push      esi                                           ;74.8
        push      edi                                           ;74.8
        call      _DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_VALUE   ;74.8
                                ; LOE ebx esi edi f1
.B1.59:                         ; Preds .B1.38
        fstp      DWORD PTR [180+esp]                           ;74.8
        movss     xmm3, DWORD PTR [180+esp]                     ;74.8
                                ; LOE ebx esi edi xmm3
.B1.39:                         ; Preds .B1.59
        movss     xmm0, DWORD PTR [_2il0floatpacket.7]          ;74.8
        andps     xmm0, xmm3                                    ;74.8
        pxor      xmm3, xmm0                                    ;74.8
        movss     xmm1, DWORD PTR [_2il0floatpacket.8]          ;74.8
        movaps    xmm2, xmm3                                    ;74.8
        movaps    xmm7, xmm3                                    ;74.8
        cmpltss   xmm2, xmm1                                    ;74.8
        andps     xmm1, xmm2                                    ;74.8
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE+32] ;74.8
        addss     xmm7, xmm1                                    ;74.8
        neg       edx                                           ;74.8
        subss     xmm7, xmm1                                    ;74.8
        movaps    xmm6, xmm7                                    ;74.8
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE] ;74.8
        subss     xmm6, xmm3                                    ;74.8
        movss     xmm3, DWORD PTR [_2il0floatpacket.9]          ;74.8
        movaps    xmm4, xmm6                                    ;74.8
        movaps    xmm5, xmm3                                    ;74.8
        cmpnless  xmm4, xmm3                                    ;74.8
        addss     xmm5, xmm3                                    ;74.8
        cmpless   xmm6, DWORD PTR [_2il0floatpacket.10]         ;74.8
        andps     xmm4, xmm5                                    ;74.8
        andps     xmm6, xmm5                                    ;74.8
        subss     xmm7, xmm4                                    ;74.8
        addss     xmm7, xmm6                                    ;74.8
        orps      xmm7, xmm0                                    ;74.8
        cvtss2si  eax, xmm7                                     ;74.29
        add       edx, eax                                      ;74.8
        imul      eax, edx, 44                                  ;74.8
        mov       edx, DWORD PTR [36+ecx+eax]                   ;74.8
        lea       ecx, DWORD PTR [188+esp]                      ;74.8
        mov       DWORD PTR [188+esp], edx                      ;74.8
        push      ecx                                           ;74.8
        push      OFFSET FLAT: __STRLITPACK_34.0.1              ;74.8
        push      ebx                                           ;74.8
        call      _for_write_seq_fmt_xmit                       ;74.8
                                ; LOE ebx esi edi
.B1.60:                         ; Preds .B1.39
        add       esp, 24                                       ;74.8
                                ; LOE ebx esi edi
.B1.40:                         ; Preds .B1.60
        mov       eax, DWORD PTR [124+esp]                      ;74.8
        inc       eax                                           ;74.8
        mov       DWORD PTR [124+esp], eax                      ;74.8
        cmp       eax, DWORD PTR [132+esp]                      ;74.8
        jle       .B1.38        ; Prob 82%                      ;74.8
                                ; LOE ebx esi edi
.B1.42:                         ; Preds .B1.40 .B1.36
        push      0                                             ;74.8
        push      OFFSET FLAT: __STRLITPACK_35.0.1              ;74.8
        push      ebx                                           ;74.8
        call      _for_write_seq_fmt_xmit                       ;74.8
                                ; LOE
.B1.43:                         ; Preds .B1.42
        push      32                                            ;75.5
        xor       eax, eax                                      ;75.5
        push      eax                                           ;75.5
        push      eax                                           ;75.5
        push      -2088435968                                   ;75.5
        push      eax                                           ;75.5
        push      OFFSET FLAT: __STRLITPACK_36                  ;75.5
        call      _for_stop_core                                ;75.5
                                ; LOE
.B1.61:                         ; Preds .B1.43
        add       esp, 216                                      ;75.5
        pop       ebx                                           ;75.5
        pop       edi                                           ;75.5
        pop       esi                                           ;75.5
        mov       esp, ebp                                      ;75.5
        pop       ebp                                           ;75.5
        ret                                                     ;75.5
                                ; LOE
.B1.44:                         ; Preds .B1.18
        cmp       DWORD PTR [8+ecx+eax], 0                      ;65.83
        jle       .B1.19        ; Prob 16%                      ;65.83
                                ; LOE edx
.B1.45:                         ; Preds .B1.44
        test      edx, edx                                      ;65.117
        jle       .B1.19        ; Prob 16%                      ;65.117
                                ; LOE
.B1.46:                         ; Preds .B1.45
        add       esp, 180                                      ;78.1
        pop       ebx                                           ;78.1
        pop       edi                                           ;78.1
        pop       esi                                           ;78.1
        mov       esp, ebp                                      ;78.1
        pop       ebp                                           ;78.1
        ret                                                     ;78.1
                                ; LOE
.B1.47:                         ; Preds .B1.14                  ; Infreq
        mov       edx, DWORD PTR [12+ebp]                       ;59.13
        mov       DWORD PTR [esp], esi                          ;
        mov       ebx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;59.42
        mov       esi, DWORD PTR [edx]                          ;59.13
        sub       esi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;59.13
        shl       esi, 8                                        ;59.13
        mov       edi, DWORD PTR [esp]                          ;59.13
        mov       eax, DWORD PTR [148+ebx+esi]                  ;59.13
        shl       eax, 2                                        ;59.13
        neg       eax                                           ;59.13
        add       eax, DWORD PTR [116+ebx+esi]                  ;59.13
        mov       ecx, DWORD PTR [16+edi+ecx]                   ;59.13
        mov       DWORD PTR [4+eax], ecx                        ;59.13
        mov       eax, DWORD PTR [16+ebp]                       ;72.5
        mov       edx, DWORD PTR [edx]                          ;67.34
        mov       DWORD PTR [96+esp], edx                       ;67.34
        mov       ecx, DWORD PTR [eax]                          ;72.5
        mov       DWORD PTR [104+esp], ecx                      ;72.5
        jmp       .B1.17        ; Prob 100%                     ;72.5
                                ; LOE
.B1.49:                         ; Preds .B1.4                   ; Infreq
        mov       DWORD PTR [esp], ebx                          ;
        mov       ebx, DWORD PTR [12+ebp]                       ;44.10
        mov       DWORD PTR [4+esp], ecx                        ;
        mov       esi, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH] ;44.39
        mov       edi, DWORD PTR [ebx]                          ;44.10
        shl       edi, 8                                        ;44.10
        mov       ecx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;44.10
        add       edi, esi                                      ;44.10
        shl       ecx, 8                                        ;44.10
        sub       edi, ecx                                      ;44.10
        mov       DWORD PTR [8+esp], ecx                        ;44.10
        mov       eax, DWORD PTR [edx+eax]                      ;44.10
        mov       DWORD PTR [16+esp], esi                       ;44.39
        mov       ecx, DWORD PTR [148+edi]                      ;44.10
        shl       ecx, 2                                        ;44.10
        neg       ecx                                           ;44.10
        add       ecx, DWORD PTR [116+edi]                      ;44.10
        mov       DWORD PTR [12+esp], -1                        ;
        mov       DWORD PTR [4+ecx], eax                        ;44.10
        mov       edx, DWORD PTR [ebx]                          ;45.10
        shl       edx, 8                                        ;45.10
        add       edx, esi                                      ;45.10
        sub       edx, DWORD PTR [8+esp]                        ;45.10
        mov       ecx, DWORD PTR [16+ebp]                       ;52.10
        mov       eax, DWORD PTR [148+edx]                      ;45.10
        shl       eax, 2                                        ;45.10
        neg       eax                                           ;45.10
        add       eax, DWORD PTR [116+edx]                      ;45.10
        mov       edx, DWORD PTR [esp]                          ;45.10
        mov       DWORD PTR [8+eax], edx                        ;45.10
        mov       ebx, DWORD PTR [ecx]                          ;52.10
        mov       DWORD PTR [104+esp], ebx                      ;52.10
        mov       ecx, DWORD PTR [4+esp]                        ;
        mov       edx, DWORD PTR [_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH+32] ;
        jmp       .B1.7         ; Prob 100%                     ;
        ALIGN     16
                                ; LOE edx ecx
; mark_end;
_RETRIEVE_NEXT_LINK ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
RETRIEVE_NEXT_LINK$format_pack.0.1	DB	54
	DB	0
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	20
	DB	0
	DB	0
	DB	0
	DB	7
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
_DATA	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
__NLITPACK_0.0.1	DD	1
__NLITPACK_1.0.1	DD	0
__STRLITPACK_18.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_19.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_20.0.1	DB	5
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_21.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_22.0.1	DB	5
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_23.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_24.0.1	DB	7
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_25.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_26.0.1	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_27.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_28.0.1	DB	7
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_29.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_30.0.1	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_31.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_32.0.1	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_33.0.1	DB	2
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_34.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_35.0.1	DB	1
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _RETRIEVE_NEXT_LINK
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_16	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	82
	DB	69
	DB	84
	DB	82
	DB	73
	DB	69
	DB	86
	DB	69
	DB	95
	DB	78
	DB	69
	DB	88
	DB	84
	DB	95
	DB	76
	DB	73
	DB	78
	DB	75
	DB	0
__STRLITPACK_14	DB	86
	DB	101
	DB	104
	DB	105
	DB	99
	DB	108
	DB	101
	DB	32
	DB	84
	DB	121
	DB	112
	DB	101
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_12	DB	86
	DB	101
	DB	104
	DB	105
	DB	99
	DB	108
	DB	101
	DB	32
	DB	99
	DB	108
	DB	97
	DB	115
	DB	115
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_10	DB	86
	DB	101
	DB	104
	DB	105
	DB	99
	DB	108
	DB	101
	DB	32
	DB	100
	DB	101
	DB	115
	DB	116
	DB	105
	DB	110
	DB	97
	DB	116
	DB	105
	DB	111
	DB	110
	DB	0
__STRLITPACK_8	DB	86
	DB	101
	DB	104
	DB	105
	DB	99
	DB	108
	DB	101
	DB	32
	DB	110
	DB	117
	DB	109
	DB	98
	DB	101
	DB	114
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_6	DB	67
	DB	117
	DB	114
	DB	114
	DB	101
	DB	110
	DB	116
	DB	32
	DB	110
	DB	111
	DB	100
	DB	101
	DB	32
	DB	35
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_4	DB	99
	DB	117
	DB	114
	DB	114
	DB	101
	DB	110
	DB	116
	DB	32
	DB	117
	DB	112
	DB	99
	DB	111
	DB	109
	DB	105
	DB	110
	DB	103
	DB	32
	DB	110
	DB	111
	DB	100
	DB	101
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_2	DB	110
	DB	101
	DB	120
	DB	116
	DB	32
	DB	110
	DB	111
	DB	100
	DB	101
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_36	DB	0
	DB 3 DUP ( 0H)	; pad
_2il0floatpacket.6	DD	0461c1800H
_2il0floatpacket.7	DD	080000000H
_2il0floatpacket.8	DD	04b000000H
_2il0floatpacket.9	DD	03f000000H
_2il0floatpacket.10	DD	0bf000000H
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE:BYTE
EXTRN	_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH_NDE:BYTE
EXTRN	_DYNUST_VEH_MODULE_mp_M_DYNUST_LAST_STAND:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ISIGCOUNT:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARCMOVE_NDE:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_BACKPOINTR:BYTE
EXTRN	_DYNUST_VEH_MODULE_mp_M_DYNUST_VEH:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE:BYTE
_DATA	ENDS
EXTRN	_for_stop_core:PROC
EXTRN	_for_write_seq_fmt_xmit:PROC
EXTRN	_for_write_seq_fmt:PROC
EXTRN	_for_write_seq_lis_xmit:PROC
EXTRN	_for_write_seq_lis:PROC
EXTRN	_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_VALUE:PROC
EXTRN	_DYNUST_VEH_PATH_ATT_MODULE_mp_VEHATT_P_SIZE:PROC
EXTRN	_RETRIEVE_VEH_PATH_ASTAR:PROC
EXTRN	__fltused:BYTE
	INCLUDELIB <ifconsol>
	INCLUDELIB <libifcoremt>
	INCLUDELIB <libifport>
	INCLUDELIB <libmmt>
	INCLUDELIB <LIBCMT>
	INCLUDELIB <libirc>
	INCLUDELIB <svml_dispmt>
	INCLUDELIB <OLDNAMES>
	END
