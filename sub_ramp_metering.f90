SUBROUTINE RAMP_METERING(t)

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~ Copyright (C) 2002-2011 The Arizona Board of Regents on Behalf of The University of Arizona.  ~~!
!~~ Developed by Yi-Chang Chiu, Ph.D. as par to the DynusT project.                               ~~!
!~~ This program is free software: you can redistribute it and/or modify it                       ~~!
!~~ under the terms of the GNU General Public License version 2 as published by                   ~~!
!~~ the Free Software Foundation.                                                                 ~~!
!~~ This program is distributed in the hope that it will be useful,                               ~~!
!~~ but WITHOUT ANY WARRANTY; without even the implied warranty of                                ~~!
!~~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                                          ~~!
!~~ See the GNU General Public License for more details.                                          ~~!
!~~ You should have received a copy of the GNU General Public License                             ~~!
!~~ along with this program (in the file COPYING).                                                ~~!
!~~ IF not, see <http://www.gnu.org/licenses/>.                                                   ~~!
!~~ Please see http://www.dynust.net for contact information and names                            ~~!
!~~ of individual contributors.                                                                   ~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!

	USE DYNUST_MAIN_MODULE
	USE DYNUST_NETWORK_MODULE

!> > > > > > > > > >                < < < < < < < < < < <
!>>>>>>>>>>>>>>>>>>>CODES START HERE<<<<<<<<<<<<<<<<<<<<<
!> > > > > > > > > >                < < < < < < < < < < <

      DO i=1,dec_num
       link_number=det_link(i)
	   link_ramp=detector_ramp(i)
	   
       IF(t.ge.ramp_start(i).and.t.le.ramp_end(i)) THEN
	    IF(ramp_par(i,4).eq.2) THEN ! OLD feedback ramp control
         occup(i)=occup(i)/detector_length(i)
         occup(i)=occup(i)/(m_dynust_network_arc_de(link_number)%nlanes*nrate/tii)           
	     m_dynust_network_arc_de(link_ramp)%MaxFlowRate=(m_dynust_network_arc_de(link_ramp)%MaxFlowRate/m_dynust_network_arc_de(link_ramp)%nlanes)+ramp_par(i,1)*(ramp_par(i,2)-occup(i))
         m_dynust_network_arc_de(link_ramp)%MaxFlowRate=m_dynust_network_arc_de(link_ramp)%nlanes*m_dynust_network_arc_de(link_ramp)%MaxFlowRate
         
         
         m_dynust_network_arc_de(link_ramp)%MaxFlowRate = min(ramp_par(i,3)*m_dynust_network_arc_de(link_ramp)%nlanes,m_dynust_network_arc_de(link_ramp)%MaxFlowRate)
         m_dynust_network_arc_de(link_ramp)%MaxFlowRate = max(0.08,m_dynust_network_arc_de(link_ramp)%MaxFlowRate)

        ELSEIF(ramp_par(i,4).eq.1) THEN ! Fixed rate
          link_ramp=detector_ramp(i)
          m_dynust_network_arc_de(link_ramp)%MaxFlowRate = min(ramp_par(i,3)/3600.0*m_dynust_network_arc_de(link_ramp)%nlanes,m_dynust_network_arc_de(link_ramp)%MaxFlowRateOrig)
        
        ELSEIF(ramp_par(i,4).eq.3) THEN ! NEW feedback control
          link_ramp=detector_ramp(i)
          link_dmain=GetFLinkFromNode(detector(i,2),detector(i,3),18)
          link_umain = 0
          DO ih = backpointr(detector(i,2)),backpointr(detector(i,2)+1)-1
          IF(m_dynust_network_arc_nde(m_dynust_network_arc_nde(ih)%BacktoForLink)%link_iden.ne.5.and.m_dynust_network_arc_nde(m_dynust_network_arc_nde(ih)%BacktoForLink)%link_iden.ne.3.and.m_dynust_network_arc_nde(m_dynust_network_arc_nde(ih)%BacktoForLink)%link_iden.ne.4) THEN ! freeway
            link_umain=m_dynust_network_arc_nde(ih)%BacktoForLink
            exit
          ENDIF
          ENDDO
          IF(link_umain.lt.1) THEN
            WRITE(911,*) "Error in ramp control number ", i, " Please check"
            STOP
          ENDIF
          avgden = 0.9*m_dynust_network_arc_de(link_umain)%c+0.1*m_dynust_network_arc_de(link_dmain)%c
          IH = m_dynust_network_arc_de(link_umain)%FlowModelnum
          IQ = VKFunc(IH)%regime
          IF(IQ.eq.1) THEN ! with flat part
            IF(avgden.le.VKFunc(IH)%KCut) THEN !free-flow regime
               pardel = 1.0
            ELSE
               pardel = 1-((avgden-VKFunc(IH)%KCut)/(VKFunc(IH)%Kjam2-VKFunc(IH)%KCut))**1.5
            ENDIF
          ENDIF
          m_dynust_network_arc_de(link_ramp)%MaxFlowRate = min(ramp_par(i,3)/3600.0*m_dynust_network_arc_de(link_ramp)%nlanes,m_dynust_network_arc_de(link_ramp)%MaxFlowRateOrig) + pardel * max(0.0,(m_dynust_network_arc_de(link_ramp)%MaxFlowRateOrig-ramp_par(i,3)/3600.0*m_dynust_network_arc_de(link_ramp)%nlanes))
          
        ELSE
          print *, "error in ramp metering"
        ENDIF
       
       ELSE ! outside the metering period
          m_dynust_network_arc_de(link_ramp)%MaxFlowRate = m_dynust_network_arc_de(link_ramp)%MaxFlowRateOrig
       ENDIF
         occup(i)=0.0
      ENDDO

END SUBROUTINE





