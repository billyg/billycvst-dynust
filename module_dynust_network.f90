MODULE DYNUST_NETWORK_MODULE
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~ Copyright (C) 2002-2011 The Arizona Board of Regents on Behalf of The University of Arizona.  ~~!
!~~ Developed by Yi-Chang Chiu, Ph.D. as par to the DynusT project.                               ~~!
!~~ This program is free software: you can redistribute it and/or modify it                       ~~!
!~~ under the terms of the GNU General Public License version 2 as published by                   ~~!
!~~ the Free Software Foundation.                                                                 ~~!
!~~ This program is distributed in the hope that it will be useful,                               ~~!
!~~ but WITHOUT ANY WARRANTY; without even the implied warranty of                                ~~!
!~~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                                          ~~!
!~~ See the GNU General Public License for more details.                                          ~~!
!~~ You should have received a copy of the GNU General Public License                             ~~!
!~~ along with this program (in the file COPYING).                                                ~~!
!~~ IF not, see <http://www.gnu.org/licenses/>.                                                   ~~!
!~~ Please see http://www.dynust.net for contact information and names                            ~~!
!~~ of individual contributors.                                                                   ~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!

   USE DYNUST_MAIN_MODULE
   USE DYNUST_LINK_VEH_LIST_MODULE
   USE DYNUST_TRANSLINK_MODULE
   
   INTEGER, PUBLIC, ALLOCATABLE :: ConZoneTmp(:,:)
   REAL, ALLOCATABLE, DIMENSION (:) :: X, Y
   
   INTEGER aggindex
   REAL MaxDenPar
    
    TYPE LStatus
        INTEGER(1) inci
	    REAL incisev
  	    REAL incistarttime
	    REAL inciendtime
	    INTEGER(1) wz
	    REAL wzsev
	    REAL wzstarttime
	    REAL wzendtime
	END TYPE LStatus
	
	TYPE nu_mvdataint
        INTEGER,POINTER::p(:)
        INTEGER(2) psize
    END TYPE

    TYPE nu_mvdataintshort
        INTEGER(2),POINTER::p(:)
        INTEGER(2) psize
    END TYPE
    
    TYPE LINKCLASS
        LOGICAL DetStatus ! to see IF vehicles appear within the detection zone applicable for only signalized intersections
	END TYPE  
    
    TYPE VehLink
        INTEGER NVehIn ! # of vehicels ready to move in 
	    INTEGER NVehOut ! # of vehicles ready to move out
	END TYPE
	
	TYPE linkinfo1
        INTEGER(1) no
        INTEGER,POINTER::p(:)
    END TYPE
      
	TYPE linkinfo2
        INTEGER(1) no
        INTEGER,POINTER::p(:)! P(i) link number of the ith movement i: llink(i) forward star sequence
        INTEGER,POINTER::m(:)! m(1) link number of LT, m(2) link number of straingmovement, etc.         
    END TYPE
      
    TYPE dynust_network_arc_de
        INTEGER       isMovesLink
        LOGICAL       ShortLink
        INTEGER       maxden
        TYPE(LStatus) LinkStatus
        INTEGER(1)    bay,bayR   
        TYPE(nu_mvdataint),ALLOCATABLE::link_outflow_delay(:),link_Qmp(:)    
        TYPE(nu_mvdataintshort),ALLOCATABLE::link_outflow_count(:),link_queue(:)
        INTEGER,ALLOCATABLE::link_time(:),link_speed(:),link_Smp(:)
        INTEGER,ALLOCATABLE::link_count(:)
        REAL link_entry_time
        REAL entryrate 
        INTEGER link_entry_queue
        REAL gcratio
        INTEGER opp_lane
        INTEGER,ALLOCATABLE::opp_linkP(:) ! 1: movement number, 2: link number
        INTEGER opp_linkS
        INTEGER(4) volume
        INTEGER(4) vehicle_queue
        INTEGER outflow
        INTEGER outleft
        REAL truckpct
        REAL left_capacity,right_capacity
        INTEGER total_count
        INTEGER(1) RightLane
        REAL MaxFlowRate,MaxFlowRateOrig
        REAL SatFlowRate
        INTEGER(1) LGrade
        INTEGER link_detector
        REAL LoadWeight
        REAL,ALLOCATABLE::delaystep(:)
        REAL,ALLOCATABLE::delayleft(:)
        INTEGER,ALLOCATABLE::AccuVol(:)  ! CUMULATIVE # OF VEHICLES (UNIT = VEHICLE)
        INTEGER,ALLOCATABLE::AccuVolT(:) ! CUMULATIVE # OF TRUCK VEHICLES (UNIT = VEHICLE)
        INTEGER,ALLOCATABLE::AccuVolH(:) ! CUMULATIVE # OF HOV VEHICLES (UNIT = VEHICLE)
        INTEGER nout
        INTEGER npar
        INTEGER nTruck
        REAL c,v
        REAL ctmp,vtmp
        TYPE(LINKCLASS) UnivLink
        INTEGER(1) nlanes
        REAL xl
        REAL cmax
        INTEGER(1) FlowModelNum
        INTEGER(1) Vfadjust
        INTEGER(2) SpeedLimit
        INTEGER(2) vlg
        INTEGER(2) gen
        REAL pcetotal
        INTEGER(2) entryqueue
        REAL statmpt
        TYPE(VehLink) inoutbuff
        INTEGER(2),ALLOCATABLE::NGenZ2I(:)
        REAL expgen
        REAL expgenT
        REAL tmp30,tmp301,tmp31,tmp32,tmp33,tmp34,tmp35,tmp36,tmp37,tmp39,tmp40,tmp600,tmp700,tmp900
        REAL,ALLOCATABLE::costexp(:,:)
        REAL DynPCE
        INTEGER(2) GRDInd
        INTEGER(2) LENInd
        REAL,ALLOCATABLE::entry_service(:)
    END TYPE dynust_network_arc_de
    
    ! not DEALLOCATED
    TYPE dynust_network_arc_nde
        LOGICAL link_ams
        INTEGER UNodeOfBackLink
        REAL TTimeOfBackLink
        INTEGER ForToBackLink
        INTEGER BackToForLink        
        REAL s
        INTEGER idnod
        INTEGER iunod
        TYPE(linkinfo2) llink
        TYPE(linkinfo1) inlink
        INTEGER(2) link_iden
    END TYPE
    
    TYPE(dynust_network_arc_de),ALLOCATABLE::m_dynust_network_arc_de(:)
    TYPE(dynust_network_arc_nde),ALLOCATABLE::m_dynust_network_arc_nde(:)
    INTEGER,ALLOCATABLE::STOPcap4w(:,:)
    INTEGER,ALLOCATABLE::STOPcap2w(:,:)
    INTEGER,ALLOCATABLE::STOPcap2wIND(:)
    INTEGER,ALLOCATABLE::YieldCap(:,:)
    INTEGER,ALLOCATABLE::YieldCapIND(:)
    LOGICAL,ALLOCATABLE::LoadWeightID(:)
    TYPE MGS 
	    INTEGER Kcut
        INTEGER Vf2
        INTEGER V02
        INTEGER Kjam2
        REAL alpha2
        REAL beta2
        INTEGER::regime
        INTEGER::form
    END TYPE
    TYPE(MGS),ALLOCATABLE::VKFunc(:)
    INTEGER(2),ALLOCATABLE::linkidmap(:)
    INTEGER,ALLOCATABLE::kgpoint(:)
    
    ! Not DEALLOCATED
    TYPE dynust_network_node
        INTEGER,ALLOCATABLE::node(:)  ! STORE INFO 4-COLUMNS IN CONTROL.DAT
        INTEGER(2)::dzone
        INTEGER(2),ALLOCATABLE::iConZone(:)
        REAL xcor,ycor
    END TYPE
    
    TYPE dynust_network_node_nde
        INTEGER(1),ALLOCATABLE::connectivity(:)    
        INTEGER::IntoOutNodeNum
        INTEGER(2)::izone
    END TYPE
    
    TYPE(dynust_network_node),ALLOCATABLE::m_dynust_network_node(:)
    TYPE(dynust_network_node_nde),ALLOCATABLE::m_dynust_network_node_nde(:)
    
    INTEGER iConZonePar
    INTEGER,ALLOCATABLE::BackPointr(:)
    INTEGER,ALLOCATABLE::OutToInNodeNum(:) 
    
    TYPE dynust_network_arcmove_de
        REAL openalty
        INTEGER green
        INTEGER(1) SignalPreventBack
        INTEGER(1) SignalPreventFor
        INTEGER(1) GeoPreventFor,GeoPreventBack
        REAL turnveh,turnvehso
        REAL capacity
        REAL tmp38
    END TYPE
    
    TYPE dynust_network_arcmove_nde
        REAL penalty
        INTEGER(1) movein,move
    END TYPE
    
    TYPE(dynust_network_arcmove_de),ALLOCATABLE::m_dynust_network_arcmove_de(:,:)
    TYPE(dynust_network_arcmove_nde),ALLOCATABLE::m_dynust_network_arcmove_nde(:,:)
    INTEGER,ALLOCATABLE::nsign(:,:)
    
    
    CONTAINS
    
       
      INTEGER Function GetFLinkFromNode(Unode,Dnode,mos)

      INTEGER Unode,Dnode,mos
      GetFLinkFromNode = 0
      IF(Unode < 1.or.Dnode < 1) THEN
       WRITE(911,*) "Error in GetFlinkFromNode"
       SELECT CASE (MOS)
            CASE(1)
                WRITE(911,*) "ERROR SOURCE: reading congestion pricing configuration file"
            CASE(2)
                WRITE(911,*) "ERROR SOURCE: Route switch subroutine"
            CASE(3)
                WRITE(911,*) "ERROR SOURCE: Signal subroutine"
            CASE(4)
                WRITE(911,*) "ERROR SOURCE: Transit module"
            CASE(5)
                WRITE(911,*) "ERROR SOURCE: Work zone subroutine"
            CASE(6)
                WRITE(911,*) "ERROR SOURCE: Toll data"
            CASE(7)
                WRITE(911,*) "ERROR SOURCE: Detector data"
            CASE(8)
                WRITE(911,*) "ERROR SOURCE: Movement data"
            CASE(9)
                WRITE(911,*) "ERROR SOURCE: VMS data"
            CASE(10)
                WRITE(911,*) "ERROR SOURCE: Incident data"
            CASE(11)
                WRITE(911,*) "ERROR SOURCE: Origin.dat"
            CASE(12)
                WRITE(911,*) "ERROR SOURCE: Work zone data"
            CASE(13)
                WRITE(911,*) "ERROR SOURCE: Intersection control module"
            CASE(14)
                WRITE(911,*) "ERROR SOURCE: Signal data"
            CASE(15)
                WRITE(911,*) "ERROR SOURCE: Vehicle data"
            CASE(16)
                WRITE(911,*) "ERROR SOURCE: Main loop module"
            CASE(17)
                WRITE(911,*) "ERROR SOURCE: Write summary module"
            CASE(18)
                WRITE(911,*) "ERROR SOURCE: Ramp metering module"
            CAse(19)
                WRITE(911,*) "ERROR SOURCE: MOVES module"

       END SELECT
       STOP
      ENDIF
      DO i = BackPointr(Dnode),BackPointr(Dnode+1)-1
        IF(m_dynust_network_arc_nde(i)%UNodeOfBackLink == Unode) THEN
	     GetFLinkFromNode=m_dynust_network_arc_nde(i)%BackToForLink
	     exit
	  ENDIF
      ENDDO
	  IF(GetFLinkFromNode < 0) THEN
         print *, 'error in GetFLinkFromNode'
	  ENDIF
      end function
      
      ! --  Get Backward * link number from up and downstream nodes
      INTEGER Function GetBLinkFromNode(Unode,Dnode)
      INTEGER Unode,Dnode
      GetBLinkFromNode = 0
      DO i = BackPointr(Dnode),BackPointr(Dnode+1)-1
        IF(m_dynust_network_arc_nde(i)%UNodeOfBackLink == Unode) THEN
	     GetBLinkFromNode = i
	     exit
	  ENDIF
      ENDDO
      end function
      
      INTEGER Function MoveNoBackLink(ULink,DLink)
      INTEGER ULink,DLink
      MoveNoBackLink = 0

	DO MO = backpointr(m_dynust_network_arc_nde(Dlink)%iunod),backpointr(m_dynust_network_arc_nde(DLink)%iunod+1)-1
        IF(MO == m_dynust_network_arc_nde(ULink)%ForToBackLink) THEN
		MoveNoBackLink = MO - backpointr(m_dynust_network_arc_nde(Dlink)%iunod) + 1
          exit
	  ENDIF
	ENDDO
      end function
      
      INTEGER Function MoveNoForLink(ULink,DLink)
      INTEGER ULink,DLink

      MoveNoForLink = 0

	DO MO = 1, m_dynust_network_arc_nde(DLink)%inlink%no
        IF(ULink == m_dynust_network_arc_nde(DLink)%inlink%p(MO)) THEN
		MoveNoForLink = MO
          exit
	  ENDIF
	ENDDO
      end function

INTEGER Function MoveNoForLinkLL(ULink,DLink)
      INTEGER ULink,DLink

      MoveNoForLinkLL = 0

	DO MO = 1, m_dynust_network_arc_nde(ULink)%llink%no
        IF(DLink == m_dynust_network_arc_nde(ULink)%llink%p(MO)) THEN
		MoveNoForLinkLL = MO
        exit
	  ENDIF
	ENDDO
      end function
      
      INTEGER FUNCTION GETMOVETURN_LL(in,i)
	INTEGER i, in
	LOGICAL FGet
	FGet=.false.
	GetMoveTurn_LL = 0

              DO k=1,m_dynust_network_arc_nde(in)%llink%no
                IF(m_dynust_network_arc_nde(in)%llink%p(k) == i) THEN
                   GetMoveTurn_LL=m_dynust_network_arcmove_nde(in,k)%move
				  exit
				ENDIF
              ENDDO
	          IF(GetMoveTurn_LL < 1) THEN
	              IF(m_dynust_network_arc_nde(in)%iunod == m_dynust_network_arc_nde(i)%idnod) THEN
	                GetMoveTurn_LL = 6
	              ELSE
	                IF(m_dynust_network_node_nde(m_dynust_network_arc_nde(i)%idnod)%IntoOutNodeNum > 900000) THEN
                      GetMoveturn_LL = 2
                     ELSE 
	                  GetMoveturn_LL = 5
	                ENDIF
	             ENDIF
              ENDIF
END FUNCTION

INTEGER FUNCTION GETMOVETURN_IN(in,i)
	INTEGER i, in
	LOGICAL FGet
	FGet=.false.
    
	GetMoveTurn_in = 0
			
            MVPB = MoveNoBackLink(in,i)
	        GetMoveTurn_in=MVPB
  
              IF(GetMoveTurn_in < 1.or.GetMoveTurn_in > nu_mv) THEN
                IF(m_dynust_network_arc_nde(in)%iunod == m_dynust_network_arc_nde(i)%idnod) THEN
                  getmoveturn_in = 6
                ELSE
                  getmoveturn_in = 1
                ENDIF
              ENDIF
END FUNCTION

INTEGER FUNCTION getBstMove(in,i)
	INTEGER i, in
	LOGICAL ifound
    ifound = .false.
    id = 0
	DO isee = backpointr(m_dynust_network_arc_nde(i)%iunod),backpointr(m_dynust_network_arc_nde(i)%iunod+1)-1
	id = id + 1
	  IF(m_dynust_network_arc_nde(isee)%backtoforlink == in) THEN
	     getBstMove = id
	     ifound = .true.
	     exit
	  ENDIF
	ENDDO
	IF(.not.ifound) THEN
	  print *, "error in getmovement"
	ENDIF

END FUNCTION

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
SUBROUTINE allocate_dynust_network_arc
           
            ALLOCATE(m_dynust_network_arc_de(noofarcs),stat=error)
            IF(error /= 0) THEN
	            WRITE(911,*) 'allocate dynust_network_arc_de error - insufficient memory'
	            STOP
	        ENDIF
	        m_dynust_network_arc_de(:)%isMovesLink=0
	        m_dynust_network_arc_de(:)%ShortLink=.false.
	        m_dynust_network_arc_de(:)%maxden=0
	        m_dynust_network_arc_de(:)%LinkStatus%inci = 0
	        m_dynust_network_arc_de(:)%LinkStatus%wz = 0
            m_dynust_network_arc_de(:)%LinkStatus%incistarttime = 0
            m_dynust_network_arc_de(:)%LinkStatus%inciendtime = 0
	        m_dynust_network_arc_de(:)%LinkStatus%incisev = 0.0
	        m_dynust_network_arc_de(:)%LinkStatus%wzsev = 0.0
            m_dynust_network_arc_de(:)%LinkStatus%wzstarttime = 0
            m_dynust_network_arc_de(:)%LinkStatus%wzendtime = 0
            m_dynust_network_arc_de(:)%bay=0
            m_dynust_network_arc_de(:)%bayR=0
            
            DO i=1,noofarcs
                ALLOCATE(m_dynust_network_arc_de(i)%link_outflow_delay(aggint),stat=error)
                IF(error /= 0) THEN
                    WRITE(911,*) 'allocate link_outflow_delay error - insufficient memory'
	                STOP
                ENDIF
                m_dynust_network_arc_de(i)%link_outflow_delay(:)%psize=0
                
                ALLOCATE(m_dynust_network_arc_de(i)%link_outflow_count(aggint),stat=error)
                IF(error /= 0) THEN
                    WRITE(911,*) 'allocate link_outflow_count error - insufficient memory'
	                STOP
                ENDIF
                m_dynust_network_arc_de(i)%link_outflow_count(:)%psize=0
                
                 ALLOCATE(m_dynust_network_arc_de(i)%link_time(aggint),stat=error)
                IF(error /= 0) THEN
                    WRITE(911,*) 'allocate link_time error - insufficient memory'
	                STOP
                ENDIF
                m_dynust_network_arc_de(i)%link_time(:)=0
                
                ALLOCATE(m_dynust_network_arc_de(i)%link_queue(aggint),stat=error)
                IF(error /= 0) THEN
                    WRITE(911,*) 'allocate link_queue error - insufficient memory'
	                STOP
                ENDIF
                m_dynust_network_arc_de(i)%link_queue(:)%psize=0
                
                ALLOCATE(m_dynust_network_arc_de(i)%link_Qmp(max(simPerAgg,tdspstep)),stat=error)
                IF(error /= 0) THEN
                    WRITE(911,*) 'allocate link_Qmp error - insufficient memory'
	                STOP
                ENDIF
                m_dynust_network_arc_de(i)%link_Qmp(:)%psize=0
                
                ALLOCATE(m_dynust_network_arc_de(i)%link_speed(aggint),stat=error)
                IF(error /= 0) THEN
                    WRITE(911,*) 'allocate link_speed error - insufficient memory'
	                STOP
                ENDIF
                m_dynust_network_arc_de(i)%link_speed(:)=1.0*AttScale ! default is 60 mph = 1.0 mile/min
                
                ALLOCATE(m_dynust_network_arc_de(i)%link_Smp(simPerAgg),stat=error)
                IF(error /= 0) THEN
                    WRITE(911,*) 'allocate link_Smp error - insufficient memory'
	                STOP
                ENDIF
                m_dynust_network_arc_de(i)%link_Smp(:)=0
                
                ALLOCATE(m_dynust_network_arc_de(i)%link_count(aggint),stat=error)
                IF(error /= 0) THEN
                    WRITE(911,*) 'allocate link_count error - insufficient memory'
	                STOP
                ENDIF
                m_dynust_network_arc_de(i)%link_count(:)=0
                
                ALLOCATE(m_dynust_network_arc_de(i)%entry_service(nu_de),stat=error)
                IF(error /= 0) THEN
                    WRITE(911,*) 'allocate entry_service error - insufficient memory'
	                STOP
                ENDIF
                m_dynust_network_arc_de(i)%entry_service(:)=0
                
                ALLOCATE(m_dynust_network_arc_de(i)%opp_linkP(2),stat=error)
                IF(error /= 0) THEN
                    WRITE(911,*) 'allocate opp_linkP error - insufficient memory'
	                STOP
                ENDIF
                m_dynust_network_arc_de(i)%opp_linkP(:)=0
                
                ALLOCATE(m_dynust_network_arc_de(i)%delaystep(nu_de),stat=error)
                IF(error /= 0) THEN
                    WRITE(911,*) 'allocate delaystep error - insufficient memory'
	                STOP
                ENDIF
                m_dynust_network_arc_de(i)%delaystep(:)=0
                
                ALLOCATE(m_dynust_network_arc_de(i)%delayleft(nu_de),stat=error)
                IF(error /= 0) THEN
                    WRITE(911,*) 'allocate delayleft error - insufficient memory'
	                STOP
                ENDIF
                m_dynust_network_arc_de(i)%delayleft(:)=0
                
                ALLOCATE(m_dynust_network_arc_de(i)%AccuVol(nint(SimPeriod)),stat=error)
                IF(error /= 0) THEN
                    WRITE(911,*) 'allocate AccuVol error - insufficient memory'
	                STOP
                ENDIF
                m_dynust_network_arc_de(i)%AccuVol(:)=0
                
                ALLOCATE(m_dynust_network_arc_de(i)%AccuVolT(nint(SimPeriod)),stat=error)
                IF(error /= 0) THEN
                    WRITE(911,*) 'allocate AccuVolT error - insufficient memory'
	                STOP
                ENDIF
                m_dynust_network_arc_de(i)%AccuVolT(:)=0

                ALLOCATE(m_dynust_network_arc_de(i)%AccuVolH(nint(SimPeriod)),stat=error)
                IF(error /= 0) THEN
                    WRITE(911,*) 'allocate AccuVolH error - insufficient memory'
	                STOP
                ENDIF
                m_dynust_network_arc_de(i)%AccuVolH(:)=0
                
                ALLOCATE(m_dynust_network_arc_de(i)%NGenZ2I(nu_iGen),stat=error)
                IF(error /= 0) THEN
                    WRITE(911,*) 'allocate NGenZ2I error - insufficient memory'
	                STOP
                ENDIF
                m_dynust_network_arc_de(i)%NGenZ2I(:)=0
                
                ALLOCATE(m_dynust_network_arc_de(i)%costexp(aggint,3),stat=error)
                IF(error /= 0) THEN
                    WRITE(911,*) 'allocate costexp error - insufficient memory'
	                STOP
                ENDIF
                m_dynust_network_arc_de(i)%costexp(:,:)=0
            END DO
            
            m_dynust_network_arc_de(:)%link_entry_time=0
            m_dynust_network_arc_de(:)%entryrate=0
            m_dynust_network_arc_de(:)%link_entry_queue=0
            m_dynust_network_arc_de(:)%gcratio=0
            m_dynust_network_arc_de(:)%opp_lane=0
            m_dynust_network_arc_de(:)%opp_linkS=0
            m_dynust_network_arc_de(:)%volume=0
            m_dynust_network_arc_de(:)%vehicle_queue=0
            m_dynust_network_arc_de(:)%outflow=0
            m_dynust_network_arc_de(:)%outleft=0
            m_dynust_network_arc_de(:)%truckpct=0
            m_dynust_network_arc_de(:)%left_capacity=0
            m_dynust_network_arc_de(:)%right_capacity=6.0
            m_dynust_network_arc_de(:)%total_count=0
            m_dynust_network_arc_de(:)%RightLane=0
            
            ALLOCATE(STOPcap4w(NLevel,NMove),stat=error)
	        IF(error /= 0) THEN
	            WRITE(911,*) 'allocate STOPcap4w error - insufficient memory'
	            STOP
	        ENDIF
            STOPcap4w(:,:)=0
            
            ALLOCATE(STOPcap2w(Level2N,Move2N),stat=error)
	        IF(error /= 0) THEN
	            WRITE(911,*) 'allocate STOPcap2w error - insufficient memory'
	            STOP
	        ENDIF
            STOPcap2w(:,:)=0

            ALLOCATE(STOPcap2wIND(Level2N),stat=error)
	        IF(error /= 0) THEN
	            WRITE(911,*) 'allocate STOPcap2wIND error - insufficient memory'
	            STOP
	        ENDIF
            STOPcap2wIND(:)=0
            
            ALLOCATE(YieldCap(Level2N,Move2N),stat=error)
	        IF(error /= 0) THEN
	            WRITE(911,*) 'allocate YieldCap error - insufficient memory'
	            STOP
	        ENDIF
            YieldCap(:,:)=0

            ALLOCATE(YieldCapIND(Level2N),stat=error)
	        IF(error /= 0) THEN
	            WRITE(911,*) 'allocate YieldCapIND error - insufficient memory'
	            STOP
	        ENDIF
            YieldCapIND(:)=0
    
            m_dynust_network_arc_de(:)%MaxFlowRate=0
            m_dynust_network_arc_de(:)%MaxFlowRateOrig=0
            m_dynust_network_arc_de(:)%SatFlowRate=0
            m_dynust_network_arc_de(:)%LGrade=0
            m_dynust_network_arc_de(:)%link_detector=0
            m_dynust_network_arc_de(:)%LoadWeight=0
            
            ALLOCATE(LoadWeightID(nzones), stat=error)
	        IF(error /= 0) THEN
	            WRITE(911,*) 'allocate LoadWeightID error - insufficient memory'
	            STOP
	        ENDIF
	        LoadWeightID(:)=.False.
	
            m_dynust_network_arc_de(:)%nout=0
            m_dynust_network_arc_de(:)%npar=0
            m_dynust_network_arc_de(:)%nTruck=0
            m_dynust_network_arc_de(:)%c=0
            m_dynust_network_arc_de(:)%v=0
            m_dynust_network_arc_de(:)%ctmp=0
            m_dynust_network_arc_de(:)%vtmp=0
            m_dynust_network_arc_de(:)%UnivLink%DetStatus=.FALSE.
            m_dynust_network_arc_de(:)%nlanes=0
            m_dynust_network_arc_de(:)%xl=0
            m_dynust_network_arc_de(:)%cmax=0

            OPEN(file='TrafficFlowModel.dat',unit=55,status='old',iostat=error) 
            IF(error /= 0) THEN
              WRITE(911,*) 'Error when opening TrfficFlowModel.dat'
	          STOP
            ENDIF

            READ(55,*,iostat=error) NoOfFlowModel
            CLOSE(55)
            
            ALLOCATE(VKFunc(NoOfFlowModel+1),stat=error) ! allocate FlowModelNum+1 for assigning default model for connectors
	        IF(error /= 0) THEN
	            WRITE(911,*) 'allocate VKFunc error - insufficient memory'
	            STOP
	        ENDIF
	        VKFunc(:)%KCut=0
	        VKFunc(:)%Vf2=0
	        VKFunc(:)%V02=0
	        VKFunc(:)%kjam2=0
	        VKFunc(:)%alpha2=0
	        VKFunc(:)%beta2=0
	        VKFunc(:)%regime=0
	        VKFunc(:)%form=0
	
            m_dynust_network_arc_de(:)%FlowModelNum=0
            
            m_dynust_network_arc_de(:)%Vfadjust=0
            m_dynust_network_arc_de(:)%SpeedLimit=0
            m_dynust_network_arc_de(:)%vlg=0
            m_dynust_network_arc_de(:)%gen=0
            m_dynust_network_arc_de(:)%pcetotal=0
            m_dynust_network_arc_de(:)%entryqueue=0
            m_dynust_network_arc_de(:)%statmpt=0
            m_dynust_network_arc_de(:)%inoutbuff%NVehIn=0
            m_dynust_network_arc_de(:)%inoutbuff%NVehOut=0
            m_dynust_network_arc_de(:)%expgen=0
            m_dynust_network_arc_de(:)%expgenT=0
            m_dynust_network_arc_de(:)%tmp30=0
            m_dynust_network_arc_de(:)%tmp301=0
            m_dynust_network_arc_de(:)%tmp31=0
            m_dynust_network_arc_de(:)%tmp32=0
            m_dynust_network_arc_de(:)%tmp33=0
            m_dynust_network_arc_de(:)%tmp34=0
            m_dynust_network_arc_de(:)%tmp35=0
            m_dynust_network_arc_de(:)%tmp36=0
            m_dynust_network_arc_de(:)%tmp37=0
            m_dynust_network_arc_de(:)%tmp39=0
            m_dynust_network_arc_de(:)%tmp40=0
            m_dynust_network_arc_de(:)%tmp600=0
            m_dynust_network_arc_de(:)%tmp700=0
            m_dynust_network_arc_de(:)%tmp900=0
            
            ALLOCATE(linkidmap(50), stat=error)
            IF(error /= 0) THEN
               WRITE(911,*) 'allocate link_iden error - insufficient memory'
               STOP
   	        ENDIF
    	    linkidmap(:) = 1
    	    linkidmap(1) = 1 
    	    linkidmap(2) = 1
    	    linkidmap(3) = 3
    	    linkidmap(4) = 4
    	    linkidmap(5) = 2
    	    linkidmap(6) = 1
    	    linkidmap(7) = 2
    	    linkidmap(8) = 1
    	    linkidmap(9) = 1					
    	    linkidmap(10)= 1

            ALLOCATE(kgpoint(noofnodes+1),stat=error)
	        IF(error /= 0) THEN
	            WRITE(911,*) 'allocate kgpoint error - insufficient memory'
	            STOP
	        ENDIF
	        kgpoint(:) = 0
	
            CALL LinkAtt_2DSetup(noofarcs)
            CALL LinkTemp_2DSetup(noofarcs)
      
            DO i = 1, noofarcs
                CALL LinkATT_Setup(i,10)
	        ENDDO
	        
	        CALL EnQAtt_2DSetup(noofarcs)
            DO i = 1, noofarcs
                CALL EnQATT_Setup(i,10)
	        ENDDO
	        
	        CALL TChainAtt_2DSetup(noofarcs)
            DO i = 1, noofarcs
                CALL TChainATT_Setup(i,10)
	        ENDDO
	        
	        m_dynust_network_arc_de(:)%DynPCE=0
	        m_dynust_network_arc_de(:)%GRDInd=0
	        m_dynust_network_arc_de(:)%LENInd=0


          IF(iteration == 0) THEN
            ALLOCATE(m_dynust_network_arc_nde(noofarcs),stat=error)
            IF(error /= 0) THEN
	            WRITE(911,*) 'allocate dynust_network_arc_nde error - insufficient memory'
	            STOP
	        ENDIF
	        
            m_dynust_network_arc_nde(:)%link_ams=.FALSE.
            m_dynust_network_arc_nde(:)%UNodeOfBackLink=0
            m_dynust_network_arc_nde(:)%TTimeOfBackLink=0
            m_dynust_network_arc_nde(:)%ForToBackLink=0
            m_dynust_network_arc_nde(:)%BackToForLink=0            
            m_dynust_network_arc_nde(:)%s=0
            m_dynust_network_arc_nde(:)%idnod=0
            m_dynust_network_arc_nde(:)%iunod=0
            m_dynust_network_arc_nde(:)%llink%no=0
            m_dynust_network_arc_nde(:)%inlink%no=0
            m_dynust_network_arc_nde(:)%link_iden=0            
          ENDIF
            	        
        END SUBROUTINE allocate_dynust_network_arc
        
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~        
        SUBROUTINE allocate_dynust_network_maxlinkveh

            INTEGER error,inisize

	        MaxLinkVeh = 250*Longest_link

    	    CALL TranLink_2DSetup(noofarcs)
 	        DO icount = 1, noofarcs 
                inisize = m_InitSize
                CALL TranLink_Setup(icount,inisize)
            ENDDO
    
        END SUBROUTINE allocate_dynust_network_maxlinkveh

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~        
        SUBROUTINE allocate_dynust_network_node

            INTEGER i
            
            ALLOCATE(m_dynust_network_node(noofnodes),stat=error)
            IF(error /= 0) THEN
	            WRITE(911,*) 'allocate dynust_network_node error - insufficient memory'
	            STOP
	        ENDIF
	        
	        m_dynust_network_node(:)%xcor=0
            m_dynust_network_node(:)%ycor=0
            m_dynust_network_node(:)%dzone=0

            
            iConZonePar=10
            
            DO i=1,noofnodes
                ALLOCATE(m_dynust_network_node(i)%node(4),stat=error) 
	            IF(error /= 0) THEN
	                WRITE(911,*) 'allocate node error - insufficient memory'
	                STOP
	            ENDIF
	            m_dynust_network_node(i)%node(:)=0
	            
                ALLOCATE(m_dynust_network_node(i)%iConZone(iConZonePar),stat=error) ! only connection to 4 centriods are allowed for now
	            IF(error /= 0) THEN
	                WRITE(911,*) 'allocate iConZone error - insufficient memory'
	                STOP
	            ENDIF
	            m_dynust_network_node(i)%iConZone(:)=0
	            
            ENDDO
            
            IF(iteration == 0) THEN
                ALLOCATE(BackPointr(noofnodes+1),stat=error)
	            IF(error /= 0) THEN
	                WRITE(911,*) 'allocate BackPointr error - insufficient memory'
	                STOP
	            ENDIF
	            BackPointr(:) = 0
	        
	            ALLOCATE(OutToInNodeNum(999999),stat=error)
 	            IF(error /= 0) THEN
	                WRITE(911,*) 'allocate OutToInNodeNum error - insufficient memory'
	                STOP
	            ENDIF
	            OutToInNodeNum(:)=0
	        ENDIF
	    
        END SUBROUTINE allocate_dynust_network_node
        
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~        
        SUBROUTINE DEALLOCATE_dynust_network_node
            DO i=1,noofnodes
                DEALLOCATE(m_dynust_network_node(i)%node) 
                DEALLOCATE(m_dynust_network_node(i)%iConZone) ! only connection to 4 centriods are allowed for now
            ENDDO
            DEALLOCATE(m_dynust_network_node)        
        END SUBROUTINE



!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~        
        SUBROUTINE allocate_dynust_network_node_nde
            INTEGER i
            ALLOCATE(m_dynust_network_node_nde(noofnodes),stat=error)
            IF(error /= 0) THEN
	            WRITE(911,*) 'allocate dynust_network_node_nde error - insufficient memory'
	            STOP
	        ENDIF
	        
            DO i=1,noofnodes
	            ALLOCATE(m_dynust_network_node_nde(i)%connectivity(noof_master_destinations),stat=error) ! only connection to 4 centriods are allowed for now
	            IF(error /= 0) THEN
	                WRITE(911,*) 'allocate connectivity error - insufficient memory'
	                STOP
	            ENDIF
	            m_dynust_network_node_nde(i)%connectivity(:)=1
            ENDDO
        END SUBROUTINE

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        SUBROUTINE allocate_dynust_network_arcmove

            INTEGER i
            
            ALLOCATE(m_dynust_network_arcmove_de(noofarcs,maxmove),stat=error)
            IF(error /= 0) THEN
	            WRITE(911,*) 'allocate dynust_network_arcmove_de error - insufficient memory'
	            STOP
	        ENDIF
	        
	        m_dynust_network_arcmove_de(:,:)%openalty=0
	        m_dynust_network_arcmove_de(:,:)%green=0
	        m_dynust_network_arcmove_de(:,:)%SignalPreventBack=1
	        m_dynust_network_arcmove_de(:,:)%SignalPreventFor=0
	        m_dynust_network_arcmove_de(:,:)%GeoPreventFor=1
	        m_dynust_network_arcmove_de(:,:)%GeoPreventBack=1
	        m_dynust_network_arcmove_de(:,:)%turnveh=0
	        m_dynust_network_arcmove_de(:,:)%turnvehso=0
	        m_dynust_network_arcmove_de(:,:)%capacity=0
	        m_dynust_network_arcmove_de(:,:)%tmp38=0
	        
	        ALLOCATE(nsign(noofnodes*maxmove,14),stat=error)
	        IF(error /= 0) THEN
	            WRITE(911,*) 'allcoate nsign error - insufficient memory'
	            STOP
	        ENDIF
	        nsign(:,:) = 0
	        
	        IF(iteration == 0) THEN
	        ALLOCATE(m_dynust_network_arcmove_nde(noofarcs,maxmove),stat=error)
            IF(error /= 0) THEN
	            WRITE(911,*) 'allocate dynust_network_arcmove_nde error - insufficient memory'
	            STOP
	        ENDIF
	        m_dynust_network_arcmove_nde(:,:)%penalty=0
	        m_dynust_network_arcmove_nde(:,:)%movein=0
	        m_dynust_network_arcmove_nde(:,:)%move=0
	        ENDIF
        END SUBROUTINE allocate_dynust_network_arcmove
        
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~        
        SUBROUTINE DEALLOCATE_dynust_network_arc

            INTEGER i
            
             DO i=1,noofarcs
                DO isee = 1, aggint
                    IF((m_dynust_network_arc_de(i)%link_outflow_delay(isee)%Psize > 0)) DEALLOCATE(m_dynust_network_arc_de(i)%link_outflow_delay(isee)%P) 
                    IF((m_dynust_network_arc_de(i)%link_outflow_count(isee)%Psize > 0)) DEALLOCATE(m_dynust_network_arc_de(i)%link_outflow_count(isee)%P)
                    IF((m_dynust_network_arc_de(i)%link_queue(isee)%psize > 0)) DEALLOCATE(m_dynust_network_arc_de(i)%link_queue(isee)%p)
                ENDDO
                
                DO isee = 1, max(simPerAgg,tdspstep)
                    IF((m_dynust_network_arc_de(i)%link_qmp(isee)%psize > 0)) DEALLOCATE(m_dynust_network_arc_de(i)%link_qmp(isee)%p)    
                ENDDO
                 
                DEALLOCATE(m_dynust_network_arc_de(i)%link_outflow_delay,stat=error)
                DEALLOCATE(m_dynust_network_arc_de(i)%link_Qmp,stat=error)
                DEALLOCATE(m_dynust_network_arc_de(i)%link_outflow_count,stat=error)
                DEALLOCATE(m_dynust_network_arc_de(i)%link_queue,stat=error)
                DEALLOCATE(m_dynust_network_arc_de(i)%link_time,stat=error)
                DEALLOCATE(m_dynust_network_arc_de(i)%link_speed,stat=error)
                DEALLOCATE(m_dynust_network_arc_de(i)%link_Smp,stat=error)
                DEALLOCATE(m_dynust_network_arc_de(i)%link_count,stat=error)
                DEALLOCATE(m_dynust_network_arc_de(i)%opp_linkP,stat=error)
                DEALLOCATE(m_dynust_network_arc_de(i)%delaystep,stat=error)
                DEALLOCATE(m_dynust_network_arc_de(i)%delayleft,stat=error)
                DEALLOCATE(m_dynust_network_arc_de(i)%AccuVol,stat=error)
                DEALLOCATE(m_dynust_network_arc_de(i)%AccuVolT,stat=error)
                DEALLOCATE(m_dynust_network_arc_de(i)%AccuVolH,stat=error)
                DEALLOCATE(m_dynust_network_arc_de(i)%NGenZ2I,stat=error)
                DEALLOCATE(m_dynust_network_arc_de(i)%costexp,stat=error)
                DEALLOCATE(m_dynust_network_arc_de(i)%entry_service,stat=error)
            ENDDO
            
            DEALLOCATE(m_dynust_network_arc_de,stat=error)
            
            DEALLOCATE(STOPcap4w,stat=error)
            DEALLOCATE(STOPcap2w,stat=error)
	        DEALLOCATE(YieldCap,stat=error)
	        DEALLOCATE(YieldCapIND,stat=error)
	        DEALLOCATE(LoadWeightID,stat=error)
	        DEALLOCATE(VKFunc,stat=error)
	        DEALLOCATE(VKFunc,stat=error)
	        DEALLOCATE(linkidmap,stat=error)
	        DEALLOCATE(kgpoint,stat=error)
        END SUBROUTINE DEALLOCATE_dynust_network_arc

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~        
        SUBROUTINE DEALLOCATE_dynust_network_arcmove
            DEALLOCATE(m_dynust_network_arcmove_de,stat=error)
            DEALLOCATE(nsign,stat=error)
        END SUBROUTINE DEALLOCATE_dynust_network_arcmove
        
END MODULE