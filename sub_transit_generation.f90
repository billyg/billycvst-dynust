      SUBROUTINE TRANSIT_GENERATION(icu,tt)
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~ Copyright (C) 2002-2011 The Arizona Board of Regents on Behalf of The University of Arizona.  ~~!
!~~ Developed by Yi-Chang Chiu, Ph.D. as par to the DynusT project.                               ~~!
!~~ This program is free software: you can redistribute it and/or modify it                       ~~!
!~~ under the terms of the GNU General Public License version 2 as published by                   ~~!
!~~ the Free Software Foundation.                                                                 ~~!
!~~ This program is distributed in the hope that it will be useful,                               ~~!
!~~ but WITHOUT ANY WARRANTY; without even the implied warranty of                                ~~!
!~~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                                          ~~!
!~~ See the GNU General Public License for more details.                                          ~~!
!~~ You should have received a copy of the GNU General Public License                             ~~!
!~~ along with this program (in the file COPYING).                                                ~~!
!~~ IF not, see <http://www.gnu.org/licenses/>.                                                   ~~!
!~~ Please see http://www.dynust.net for contact information and names                            ~~!
!~~ of individual contributors.                                                                   ~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
    USE DYNUST_MAIN_MODULE
    USE DYNUST_VEH_MODULE
    USE DYNUST_NETWORK_MODULE
	USE DYNUST_VEH_PATH_ATT_MODULE
    USE DYNUST_LINK_VEH_LIST_MODULE

	INTEGER Index1Dm, ilink,icu
    REAL value

!> > > > > > > > > >                < < < < < < < < < < <
!>>>>>>>>>>>>>>>>>>>CODES START HERE<<<<<<<<<<<<<<<<<<<<<
!> > > > > > > > > >                < < < < < < < < < < <

      DO 10 i=1,NoofBuses

       IF(m_dynust_bus(i)%ngenbus == 1.or.icu /= m_dynust_bus(i)%buslink) go to 10

         IF(abs(m_dynust_bus(i)%busstart-tt) < 0.05) THEN
	      ilink = m_dynust_bus(i)%buslink

           m_dynust_bus(i)%ngenbus=1
	       TotalBusGen=TotalBusGen+1
           justveh=justveh+1
	      IF(RunMode /= 1) THEN
              j1=jrestore+ 1
	      ELSE
	        j1 = justveh
	      ENDIF
      	  jrestore = jrestore + 1
	      

            IF(j1 > NoofVeh) THEN
              jerror=0
              DO k=1,NoofVeh
                IF(m_dynust_veh(k)%notin == 1) THEN
                  j1=k
                   m_dynust_veh(k)%notin=0
                   jerror=1 
  	             exit
	          ENDIF
              ENDDO
              IF(jerror == 0) THEN
                WRITE(911,*) 'ERROR'
                WRITE(911,*) 'Number of vehicles in the network > NoofVeh'
                WRITE(911,*) 'To Resolve :'
                WRITE(911,*) 'Increase NoofVeh in the paramter file'
                STOP
              ENDIF
            ENDIF

           numcars=numcars+1
           m_dynust_bus(i)%busid=j1
           m_dynust_network_arc_de(ilink)%npar=m_dynust_network_arc_de(ilink)%npar+1
           IF(m_dynust_last_stand(j1)%vehtype == 2.or.m_dynust_last_stand(j1)%vehtype == 5.or.m_dynust_last_stand(j1)%vehtype == 7) m_dynust_network_arc_de(ilink)%nTruck=m_dynust_network_arc_de(ilink)%nTruck+1
	       m_dynust_network_arc_de(ilink)%volume=m_dynust_network_arc_de(ilink)%volume+1
           m_dynust_veh(j1)%mtnum=2.0*AttScale
           m_dynust_network_arc_de(ilink)%pcetotal=m_dynust_network_arc_de(ilink)%pcetotal+m_dynust_veh(j1)%mtnum/AttScale
	       IF(m_dynust_network_arc_de(ilink)%maxden*m_dynust_network_arc_de(ilink)%xl-m_dynust_network_arc_de(ilink)%pcetotal < 0) THEN
	         WRITE(911,*) 'Error!! Possibly wrong setting in vehicle'
	         WRITE(911,*) ' type in scenario.dat'
	         STOP
	       ENDIF

           CALL linkVehList_insert(ilink,j1,m_dynust_veh(j1)%position) !LST

           m_dynust_last_stand(j1)%stime=tt
           m_dynust_veh(j1)%ttilnow=0
           m_dynust_veh(j1)%ttSTOP=0
           m_dynust_veh_nde(j1)%icurrnt=1
           m_dynust_veh(j1)%distans=0.0
           
           m_dynust_veh(j1)%position=m_dynust_network_arc_nde(ilink)%s/2
           IF(tt >= starttm.and.tt < endtm) THEN
		   m_dynust_veh(j1)%itag=1
           ELSE
	       m_dynust_veh(j1)%itag=0
           ENDIF
           m_dynust_last_stand(j1)%isec=m_dynust_bus(i)%buslink
           m_dynust_last_stand(j1)%vehtype = 7
	        m_dynust_last_stand(j1)%vehclass = 1
           DO k=1,m_dynust_bus(i)%NoBusNode+1 ! including the centroid
             evalue = float(BusAtt_Value(i,k,1))
		   CALL vehatt_Insert(j1,k,1,evalue)
           ENDDO
           itmp = m_dynust_bus(i)%NoBusNode
           mpzone = BusAtt_Value(i,itmp,1) ! destination node number
           m_dynust_last_stand(j1)%jdest=MasterDest(m_dynust_network_node(mpzone)%iConZone(2))
		   evalue=float(destination(m_dynust_last_stand(j1)%jdest))
		   CALL vehatt_Insert(j1,m_dynust_bus(i)%NoBusNode+1,1,evalue)
		               
           IF(m_dynust_last_stand(j1)%jdest == 0)THEN
			 WRITE(911,*) "Error in bus generation"
	         WRITE(911,*) "Check the destination for bus:",i
	         STOP
           ENDIF

        m_dynust_veh(j1)%DestVisit = 1 
        m_dynust_veh(j1)%NoOfIntDst = 1
        m_dynust_veh_nde(j1)%IntDestZone(m_dynust_veh(j1)%NoOfIntDst)=m_dynust_network_node(mpzone)%iConZone(2) !original zone number
      ENDIF

10    CONTINUE

END SUBROUTINE
     

