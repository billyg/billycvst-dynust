SUBROUTINE CAL_ACCUMU_STAT(i,j,t)
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~ Copyright (C) 2002-2011 The Arizona Board of Regents on Behalf of The University of Arizona.  ~~!
!~~ Developed by Yi-Chang Chiu, Ph.D. as par to the DynusT project.                               ~~!
!~~ This program is free software: you can redistribute it and/or modify it                       ~~!
!~~ under the terms of the GNU General Public License version 2 as published by                   ~~!
!~~ the Free Software Foundation.                                                                 ~~!
!~~ This program is distributed in the hope that it will be useful,                               ~~!
!~~ but WITHOUT ANY WARRANTY; without even the implied warranty of                                ~~!
!~~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                                          ~~!
!~~ See the GNU General Public License for more details.                                          ~~!
!~~ You should have received a copy of the GNU General Public License                             ~~!
!~~ along with this program (in the file COPYING).                                                ~~!
!~~ IF not, see <http://www.gnu.org/licenses/>.                                                   ~~!
!~~ Please see http://www.dynust.net for contact information and names                            ~~!
!~~ of individual contributors.                                                                   ~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
    USE DYNUST_MAIN_MODULE
    USE DYNUST_VEH_MODULE
    USE DYNUST_VEH_PATH_ATT_MODULE
    INTERFACE
      SUBROUTINE WRITE_PRINT_TRAJECTORY(j1,j2,j3,ts)      
        INTEGER j1, j2, j3
        REAL ts
      END SUBROUTINE
    END INTERFACE
    INTERFACE
      SUBROUTINE PrintAlt(j1,ts)
        INTEGER j1
        REAL ts
      END SUBROUTINE
    END INTERFACE
    INTERFACE
      SUBROUTINE WRITE_SUMMARY(j1,EndID,ts)  
        INTEGER j1
        LOGICAL EndID
        REAL ts
      END SUBROUTINE
    END INTERFACE
    INTERFACE
      SUBROUTINE WRITE_SUMMARY_TYPEBASED(j,EndID,ts)    
        INTEGER j1
        LOGICAL EndID
        REAL ts
      END SUBROUTINE
    END INTERFACE
    INTEGER Index1D
    LOGICAL EndID
    REAL value
    REAL www
!> > > > > > > > > >                < < < < < < < < < < <
!>>>>>>>>>>>>>>>>>>>CODES START HERE<<<<<<<<<<<<<<<<<<<<<
!> > > > > > > > > >                < < < < < < < < < < <

   IF(m_dynust_last_stand(j)%vehtype /= 7) ktotal_out=ktotal_out+1
   
   m_dynust_last_stand(j)%atime = t + m_dynust_veh(j)%tocross

   IF(m_dynust_last_stand(j)%atime < m_dynust_last_stand(j)%stime) THEN
     WRITE(911,*) 'ERROR on arrival time for vehicle ',j
     WRITE(911,*) 'arrival time= ',m_dynust_last_stand(j)%atime,' start time= ',m_dynust_last_stand(j)%stime
     STOP
   ENDIF

   CALL vehatt_Insert(j,m_dynust_veh_nde(j)%icurrnt,3,m_dynust_veh(j)%ttilnow) !3 is for pathtime
   CALL vehatt_Insert(j,m_dynust_veh_nde(j)%icurrnt,2,m_dynust_veh(j)%ttSTOP) !2 is for STOPtime


   IF(iteMax == 0.or.(iteMax > 0.and.iteration == iteMax).or.reach_converg) THEN
     CALL WRITE_PRINT_TRAJECTORY(j,1,18,t)
   ENDIF
   CALL PrintAlt(j,t)
     
   IF(m_dynust_veh(j)%HOVFlag.and.m_dynust_veh(j)%ioc == 1)THEN
      iactual_lov_hot=iactual_lov_hot+1
      time_lov_hot=time_lov_hot+m_dynust_veh(j)%ttilnow
   ELSEIF (.not.m_dynust_veh(j)%HOVFlag.and.m_dynust_veh(j)%ioc == 1)THEN
      iactual_lov_ohot=iactual_lov_ohot+1
      time_lov_ohot=time_lov_ohot+m_dynust_veh(j)%ttilnow
   ELSEIF(m_dynust_veh(j)%HOVFlag.and.m_dynust_veh(j)%ioc == 2)THEN
      iactual_hov_hot=iactual_hov_hot+1
      time_hov_hot=time_hov_hot+m_dynust_veh(j)%ttilnow
   ELSEIF(.not.m_dynust_veh(j)%HOVFlag.and.m_dynust_veh(j)%ioc == 2)THEN
      iactual_hov_ohot=iactual_hov_ohot+1
      time_hov_ohot=time_hov_ohot+m_dynust_veh(j)%ttilnow
   ENDIF

! ---
   www = 0
   IF(m_dynust_veh(j)%itag == 2)THEN
     IF(m_dynust_veh(j)%NoOfIntDst > 1) THEN
	    DO ka=1,m_dynust_veh(j)%NoOfIntDst-1
          www = www + m_dynust_veh(j)%IntDestDwell(ka)/60.0
        ENDDO
     ENDIF
     STOPtemp = m_dynust_veh(j)%ttSTOP
     STOPtime = STOPtime + m_dynust_veh(j)%ttSTOP
     ttt=max(0.0,m_dynust_last_stand(j)%atime-m_dynust_last_stand(j)%stime-m_dynust_veh(j)%ttilnow- www)
     entry_queue1=entry_queue1+ttt
     tt=m_dynust_last_stand(j)%atime-m_dynust_last_stand(j)%stime- www
     triptime1=triptime1+tt
     dtotal1=dtotal1+m_dynust_veh(j)%distans
     vtothr1=vtothr1+m_dynust_veh(j)%ttilnow
     itag2=itag2+1
     IF(m_dynust_veh_nde(j)%info == 1) THEN
       entry_queue2=entry_queue2+ttt
       triptime2=triptime2+tt
       information=information+1
       vtothr2=vtothr2+m_dynust_veh(j)%ttilnow
       dtotal2=dtotal2+m_dynust_veh(j)%distans
       STOPinfo=STOPinfo+STOPtemp
       m_dynust_veh(j)%switch=iabs(m_dynust_veh(j)%switch)-1
       totaldecision=m_dynust_veh(j)%decision(1)+totaldecision
!      totalswitch=m_dynust_veh(j)%switch+totalswitch
       IF(m_dynust_veh(j)%NoOfIntDst == 1) THEN
          entry_queue2_1=entry_queue2_1+ttt
          triptime2_1=triptime2_1+tt
          information_1=information_1+1
          vtothr2_1=vtothr2_1+m_dynust_veh(j)%ttilnow
          dtotal2_1=dtotal2_1+m_dynust_veh(j)%distans
          STOPinfo_1=STOPinfo_1+STOPtemp
       ELSEIF(m_dynust_veh(j)%NoOfIntDst == 2) THEN
          entry_queue2_2=entry_queue2_2+ttt
          triptime2_2=triptime2_2+tt
          information_2=information_2+1
          vtothr2_2=vtothr2_2+m_dynust_veh(j)%ttilnow
          dtotal2_2=dtotal2_2+m_dynust_veh(j)%distans
          STOPinfo_2=STOPinfo_2+STOPtemp
       ELSEIF(m_dynust_veh(j)%NoOfIntDst == 3) THEN
          entry_queue2_3=entry_queue2_3+ttt
          triptime2_3=triptime2_3+tt
          information_3=information_3+1
          vtothr2_3=vtothr2_3+m_dynust_veh(j)%ttilnow
          dtotal2_3=dtotal2_3+m_dynust_veh(j)%distans
          STOPinfo_3=STOPinfo_3+STOPtemp
       ENDIF
       DO is=1,nu_switch+1
          IF(m_dynust_veh(j)%switch == is-1) switchnum(is)=switchnum(is)+1
       ENDDO
       DO is=1,nu_switch+1
          IF(m_dynust_veh(j)%decision(1) == is-1) decisionnum(is)=decisionnum(is)+1
       ENDDO
       IF(m_dynust_veh(j)%switch > nu_switch)  switchnum(nu_switch+1)=switchnum(nu_switch+1)+1
       IF(m_dynust_veh(j)%decision(1) > nu_switch) decisionnum(nu_switch+1)=decisionnum(nu_switch+1)+1
   
     ELSEIF(m_dynust_veh_nde(j)%info == 0) THEN
	   entry_queue3=entry_queue3+ttt
       triptime3=triptime3+tt
       noinformation=noinformation+1
       vtothr3=vtothr3+m_dynust_veh(j)%ttilnow
       dtotal3=dtotal3+m_dynust_veh(j)%distans
       STOPnoinfo=STOPnoinfo+STOPtemp
       IF(m_dynust_veh(j)%NoOfIntDst == 1) THEN
          entry_queue3_1=entry_queue3_1+ttt
          triptime3_1=triptime3_1+tt
          noinformation_1=noinformation_1+1
          vtothr3_1=vtothr3_1+m_dynust_veh(j)%ttilnow
          dtotal3_1=dtotal3_1+m_dynust_veh(j)%distans
          STOPnoinfo_1=STOPnoinfo_1+STOPtemp
       ELSEIF(m_dynust_veh(j)%NoOfIntDst == 2) THEN
          entry_queue3_2=entry_queue3_2+ttt
          triptime3_2=triptime3_2+tt
          noinformation_2=noinformation_2+1
          vtothr3_2=vtothr3_2+m_dynust_veh(j)%ttilnow
          dtotal3_2=dtotal3_2+m_dynust_veh(j)%distans
          STOPnoinfo_2=STOPnoinfo_2+STOPtemp
       ELSEIF(m_dynust_veh(j)%NoOfIntDst == 3) THEN
          entry_queue3_3=entry_queue3_3+ttt
          triptime3_3=triptime3_3+tt
          noinformation_3=noinformation_3+1
          vtothr3_3=vtothr3_3+m_dynust_veh(j)%ttilnow
          dtotal3_3=dtotal3_3+m_dynust_veh(j)%distans
          STOPnoinfo_2=STOPnoinfo_2+STOPtemp
       ENDIF
     ENDIF
	ELSEIF(m_dynust_veh(j)%itag == 0) THEN
       itag0=itag0+1
	ENDIF

    EndID = .false.
    CALL WRITE_SUMMARY(j,EndID,t)  
    CALL WRITE_SUMMARY_TYPEBASED(j,EndID,m_dynust_last_stand(j)%stime)
END SUBROUTINE
 