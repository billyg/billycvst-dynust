      SUBROUTINE SIGNAL_INTERSECTION_CONTROL(t)
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~ Copyright (C) 2002-2011 The Arizona Board of Regents on Behalf of The University of Arizona.  ~~!
!~~ Developed by Yi-Chang Chiu, Ph.D. as par to the DynusT project.                               ~~!
!~~ This program is free software: you can redistribute it and/or modify it                       ~~!
!~~ under the terms of the GNU General Public License version 2 as published by                   ~~!
!~~ the Free Software Foundation.                                                                 ~~!
!~~ This program is distributed in the hope that it will be useful,                               ~~!
!~~ but WITHOUT ANY WARRANTY; without even the implied warranty of                                ~~!
!~~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                                          ~~!
!~~ See the GNU General Public License for more details.                                          ~~!
!~~ You should have received a copy of the GNU General Public License                             ~~!
!~~ along with this program (in the file COPYING).                                                ~~!
!~~ IF not, see <http://www.gnu.org/licenses/>.                                                   ~~!
!~~ Please see http://www.dynust.net for contact information and names                            ~~!
!~~ of individual contributors.                                                                   ~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
      USE DYNUST_MAIN_MODULE
      USE DYNUST_NETWORK_MODULE
      USE DFPORT
      USE RAND_GEN_INT
      REAL t
 !> > > > > > > > > >                < < < < < < < < < < <
!>>>>>>>>>>>>>>>>>>>CODES START HERE<<<<<<<<<<<<<<<<<<<<<
!> > > > > > > > > >                < < < < < < < < < < <

      m_dynust_network_arcmove_de(:,:)%green=0.0
 
      DO 10 i=1,noofnodes
      IF(m_dynust_network_node(i)%node(2) == 1)     THEN 
             CALL SIGNAL_NO_CONTROL(i)
      ELSEIF(m_dynust_network_node(i)%node(2) == 2) THEN
             CALL SIGNAL_YIELD(i)
      ELSEIF(m_dynust_network_node(i)%node(2) == 3) THEN
             CALL SIGNAL_STOP(i)
      ELSEIF(m_dynust_network_node(i)%node(2) == 4) THEN
             CALL SIGNAL_PRETIMED(i,t)
!              CALL Quasi_Control(i)
      ELSEIF(m_dynust_network_node(i)%node(2) == 5) THEN
             CALL SIGNAL_ACTUATED(i,t) 
!              CALL Quasi_Control(i)
      ELSEIF(m_dynust_network_node(i)%node(2) == 6) THEN
             CALL SIGNAL_STOP(i)
      ENDIF
10    CONTINUE



END SUBROUTINE


SUBROUTINE Quasi_control(IntoOutNodeNumber)
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~ Copyright (C) 2002-2009 The Arizona Board of Regents on Behalf of The University of Arizona.  ~~!
!~~ Developed by Yi-Chang Chiu, Ph.D. as par to the DynusT project.                               ~~!
!~~ This program is free software: you can redistribute it and/or modify it                       ~~!
!~~ under the terms of the GNU General Public License version 2 as published by                   ~~!
!~~ the Free Software Foundation.                                                                 ~~!
!~~ This program is distributed in the hope that it will be useful,                               ~~!
!~~ but WITHOUT ANY WARRANTY; without even the implied warranty of                                ~~!
!~~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                                          ~~!
!~~ See the GNU General Public License for more details.                                          ~~!
!~~ You should have received a copy of the GNU General Public License                             ~~!
!~~ along with this program (in the file COPYING).                                                ~~!
!~~ IF not, see <http://www.gnu.org/licenses/>.                                                   ~~!
!~~ Please see http://dynust.net/wikibin/doku.php for contact information and names               ~~!
!~~ of individual contributors.                                                                   ~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!! --

      USE DYNUST_MAIN_MODULE
      USE DYNUST_NETWORK_MODULE
      USE RAND_GEN_INT
            
      INTEGER i1, i2, IntoOutNodeNumber, g1
      REAL g2, totalcount

      i1=backpointr(IntoOutNodeNumber)
      i2=backpointr(IntoOutNodeNumber+1)-1
      totalcount = 0.0
      
	DO i=i1,i2
         ik=m_dynust_network_arc_nde(i)%BackToForLink
         totalcount=totalcount+m_dynust_network_arc_de(ik)%inoutbuff%NVehOut
      ENDDO
! --
      IF(totalcount >= 1.0) THEN
        DO i=i1,i2
         ik=m_dynust_network_arc_nde(i)%BackToForLink
         g2=xminPerSimInt*60.0*m_dynust_network_arc_de(ik)%inoutbuff%NVehOut/totalcount
         g1=ifix(xminPerSimInt*60.0*m_dynust_network_arc_de(ik)%inoutbuff%NVehOut/totalcount)
         rr = ranxy(14) 
         IF(rr <= (g2-g1)) THEN
           m_dynust_network_arcmove_de(ik,1:m_dynust_network_arc_nde(ik)%llink%no)%green=g1+1.0
         ELSE
           m_dynust_network_arcmove_de(ik,1:m_dynust_network_arc_nde(ik)%llink%no)%green=float(g1)
         ENDIF
        ENDDO
	ELSE
        m_dynust_network_arcmove_de(ik,1:m_dynust_network_arc_nde(ik)%llink%no)%green=xminPerSimInt*60
	ENDIF

      
END SUBROUTINE

