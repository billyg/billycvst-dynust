; mark_description "Intel(R) Visual Fortran Compiler XE for applications running on IA-32, Version 13.1.1.171 Build 20130313";
; mark_description "-S";
	.686P
 	.387
	OPTION DOTNAME
	ASSUME	CS:FLAT,DS:FLAT,SS:FLAT
_TEXT	SEGMENT  PARA PUBLIC FLAT  'CODE'
TXTST0:
; -- Begin  _READ_TRAFFIC_FLOW_MODELS
; mark_begin;
IF @Version GE 800
  .MMX
ELSEIF @Version GE 612
  .MMX
  MMWORD TEXTEQU <QWORD>
ENDIF
IF @Version GE 800
  .XMM
ELSEIF @Version GE 614
  .XMM
  XMMWORD TEXTEQU <OWORD>
ENDIF
       ALIGN     16
	PUBLIC _READ_TRAFFIC_FLOW_MODELS
_READ_TRAFFIC_FLOW_MODELS	PROC NEAR 
.B1.1:                          ; Preds .B1.0
        push      ebp                                           ;1.12
        mov       ebp, esp                                      ;1.12
        and       esp, -16                                      ;1.12
        push      esi                                           ;1.12
        push      edi                                           ;1.12
        push      ebx                                           ;1.12
        sub       esp, 692                                      ;1.12
        xor       eax, eax                                      ;22.22
        mov       DWORD PTR [416+esp], eax                      ;22.22
        lea       ecx, DWORD PTR [512+esp]                      ;24.4
        mov       DWORD PTR [420+esp], eax                      ;22.22
        lea       edx, DWORD PTR [296+esp]                      ;24.4
        mov       DWORD PTR [424+esp], eax                      ;22.22
        mov       DWORD PTR [428+esp], 128                      ;22.22
        mov       DWORD PTR [432+esp], 1                        ;22.22
        mov       DWORD PTR [436+esp], eax                      ;22.22
        mov       DWORD PTR [440+esp], eax                      ;22.22
        mov       DWORD PTR [444+esp], eax                      ;22.22
        mov       DWORD PTR [448+esp], eax                      ;22.22
        mov       DWORD PTR [512+esp], eax                      ;24.4
        mov       DWORD PTR [296+esp], 20                       ;24.4
        mov       DWORD PTR [300+esp], OFFSET FLAT: __STRLITPACK_54 ;24.4
        mov       DWORD PTR [304+esp], 3                        ;24.4
        mov       DWORD PTR [308+esp], OFFSET FLAT: __STRLITPACK_53 ;24.4
        push      32                                            ;24.4
        push      edx                                           ;24.4
        push      OFFSET FLAT: __STRLITPACK_55.0.1              ;24.4
        push      -2088435965                                   ;24.4
        push      55                                            ;24.4
        push      ecx                                           ;24.4
        call      _for_open                                     ;24.4
                                ; LOE eax
.B1.161:                        ; Preds .B1.1
        add       esp, 24                                       ;24.4
                                ; LOE eax
.B1.2:                          ; Preds .B1.161
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;24.4
        test      eax, eax                                      ;25.13
        jne       .B1.157       ; Prob 5%                       ;25.13
                                ; LOE
.B1.3:                          ; Preds .B1.189 .B1.2
        mov       DWORD PTR [512+esp], 0                        ;30.4
        lea       eax, DWORD PTR [312+esp]                      ;30.4
        mov       DWORD PTR [312+esp], 25                       ;30.4
        mov       DWORD PTR [316+esp], OFFSET FLAT: __STRLITPACK_50 ;30.4
        mov       DWORD PTR [320+esp], 7                        ;30.4
        mov       DWORD PTR [324+esp], OFFSET FLAT: __STRLITPACK_49 ;30.4
        push      32                                            ;30.4
        push      eax                                           ;30.4
        push      OFFSET FLAT: __STRLITPACK_58.0.1              ;30.4
        push      -2088435965                                   ;30.4
        push      5545                                          ;30.4
        lea       edx, DWORD PTR [532+esp]                      ;30.4
        push      edx                                           ;30.4
        call      _for_open                                     ;30.4
                                ; LOE eax
.B1.162:                        ; Preds .B1.3
        add       esp, 24                                       ;30.4
                                ; LOE eax
.B1.4:                          ; Preds .B1.162
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;30.4
        test      eax, eax                                      ;31.13
        jne       .B1.155       ; Prob 5%                       ;31.13
                                ; LOE
.B1.5:                          ; Preds .B1.188 .B1.4
        xor       eax, eax                                      ;35.4
        mov       DWORD PTR [512+esp], eax                      ;35.4
        push      32                                            ;35.4
        push      OFFSET FLAT: READ_TRAFFIC_FLOW_MODELS$format_pack.0.1 ;35.4
        push      eax                                           ;35.4
        push      OFFSET FLAT: __STRLITPACK_61.0.1              ;35.4
        push      -2088435968                                   ;35.4
        push      5545                                          ;35.4
        lea       edx, DWORD PTR [536+esp]                      ;35.4
        push      edx                                           ;35.4
        call      _for_write_seq_fmt                            ;35.4
                                ; LOE
.B1.163:                        ; Preds .B1.5
        add       esp, 28                                       ;35.4
                                ; LOE
.B1.6:                          ; Preds .B1.163
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;46.4
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFARCS] ;39.4
        test      eax, eax                                      ;39.4
        mov       DWORD PTR [32+esp], edx                       ;46.4
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32] ;46.4
        jle       .B1.154       ; Prob 2%                       ;39.4
                                ; LOE eax edx
.B1.7:                          ; Preds .B1.6
        mov       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;40.48
        mov       DWORD PTR [20+esp], esi                       ;40.48
        mov       esi, eax                                      ;40.70
        imul      edx, edx, 900                                 ;
        shr       esi, 31                                       ;40.70
        add       esi, eax                                      ;40.70
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32] ;40.48
        sar       esi, 1                                        ;40.70
        mov       DWORD PTR [24+esp], ecx                       ;40.48
        xor       ecx, ecx                                      ;
        mov       DWORD PTR [36+esp], edx                       ;
        xor       ebx, ebx                                      ;
        test      esi, esi                                      ;40.70
        jbe       .B1.153       ; Prob 0%                       ;40.70
                                ; LOE eax ecx ebx esi
.B1.8:                          ; Preds .B1.7
        mov       edi, DWORD PTR [32+esp]                       ;
        xor       edx, edx                                      ;
        sub       edi, DWORD PTR [36+esp]                       ;
        mov       DWORD PTR [56+esp], edi                       ;
        imul      edi, DWORD PTR [24+esp], -152                 ;
        add       edi, DWORD PTR [20+esp]                       ;
        mov       DWORD PTR [28+esp], edx                       ;
        mov       DWORD PTR [44+esp], edx                       ;
        mov       DWORD PTR [40+esp], ebx                       ;
        mov       DWORD PTR [16+esp], eax                       ;
        mov       DWORD PTR [48+esp], edi                       ;
        mov       DWORD PTR [52+esp], esi                       ;
        mov       ebx, edx                                      ;
        mov       eax, DWORD PTR [28+esp]                       ;
                                ; LOE eax edx ecx ebx
.B1.9:                          ; Preds .B1.13 .B1.8
        mov       esi, DWORD PTR [56+esp]                       ;40.10
        movsx     esi, WORD PTR [1582+ebx+esi]                  ;40.10
        cmp       esi, ecx                                      ;40.48
        jle       .B1.11        ; Prob 50%                      ;40.48
                                ; LOE eax edx ecx ebx esi
.B1.10:                         ; Preds .B1.9
        mov       edi, DWORD PTR [48+esp]                       ;40.70
        mov       DWORD PTR [44+esp], ebx                       ;
        lea       ebx, DWORD PTR [1+edx+edx]                    ;
        movsx     edi, WORD PTR [300+eax+edi]                   ;40.70
        cmp       edi, 99                                       ;41.10
        cmovne    ecx, esi                                      ;41.10
        mov       esi, DWORD PTR [40+esp]                       ;42.10
        cmovne    esi, ebx                                      ;42.10
        mov       ebx, DWORD PTR [44+esp]                       ;42.10
        mov       DWORD PTR [40+esp], esi                       ;42.10
                                ; LOE eax edx ecx ebx
.B1.11:                         ; Preds .B1.10 .B1.9
        mov       esi, DWORD PTR [56+esp]                       ;40.10
        movsx     esi, WORD PTR [2482+ebx+esi]                  ;40.10
        cmp       esi, ecx                                      ;40.48
        jle       .B1.13        ; Prob 50%                      ;40.48
                                ; LOE eax edx ecx ebx esi
.B1.12:                         ; Preds .B1.11
        mov       edi, DWORD PTR [48+esp]                       ;40.70
        mov       DWORD PTR [44+esp], ebx                       ;
        lea       ebx, DWORD PTR [2+edx+edx]                    ;
        movsx     edi, WORD PTR [452+eax+edi]                   ;40.70
        cmp       edi, 99                                       ;41.10
        cmovne    ecx, esi                                      ;41.10
        mov       esi, DWORD PTR [40+esp]                       ;42.10
        cmovne    esi, ebx                                      ;42.10
        mov       ebx, DWORD PTR [44+esp]                       ;42.10
        mov       DWORD PTR [40+esp], esi                       ;42.10
                                ; LOE eax edx ecx ebx
.B1.13:                         ; Preds .B1.12 .B1.11
        inc       edx                                           ;40.70
        add       eax, 304                                      ;40.70
        add       ebx, 1800                                     ;40.70
        cmp       edx, DWORD PTR [52+esp]                       ;40.70
        jb        .B1.9         ; Prob 63%                      ;40.70
                                ; LOE eax edx ecx ebx
.B1.14:                         ; Preds .B1.13
        mov       ebx, DWORD PTR [40+esp]                       ;
        lea       edx, DWORD PTR [1+edx+edx]                    ;40.70
        mov       eax, DWORD PTR [16+esp]                       ;
                                ; LOE eax edx ecx ebx
.B1.15:                         ; Preds .B1.14 .B1.153
        lea       esi, DWORD PTR [-1+edx]                       ;40.70
        cmp       eax, esi                                      ;40.70
        jbe       .B1.18        ; Prob 0%                       ;40.70
                                ; LOE eax edx ecx ebx
.B1.16:                         ; Preds .B1.15
        imul      esi, edx, 900                                 ;40.65
        mov       edi, DWORD PTR [32+esp]                       ;40.65
        sub       edi, DWORD PTR [36+esp]                       ;40.65
        movsx     esi, WORD PTR [682+esi+edi]                   ;40.10
        cmp       esi, ecx                                      ;40.48
        jle       .B1.18        ; Prob 50%                      ;40.48
                                ; LOE eax edx ebx
.B1.17:                         ; Preds .B1.16
        mov       ecx, DWORD PTR [24+esp]                       ;40.70
        neg       ecx                                           ;40.70
        add       ecx, edx                                      ;40.70
        imul      edi, ecx, 152                                 ;40.70
        mov       esi, DWORD PTR [20+esp]                       ;40.70
        movsx     ecx, WORD PTR [148+esi+edi]                   ;40.70
        cmp       ecx, 99                                       ;42.10
        cmovne    ebx, edx                                      ;42.10
                                ; LOE eax ebx
.B1.18:                         ; Preds .B1.17 .B1.16 .B1.15
        lea       edx, DWORD PTR [1+eax]                        ;44.4
        mov       DWORD PTR [228+esp], edx                      ;44.4
                                ; LOE eax ebx
.B1.19:                         ; Preds .B1.154 .B1.18
        cmp       eax, ebx                                      ;45.4
        mov       DWORD PTR [512+esp], 0                        ;46.4
        cmovl     ebx, eax                                      ;45.4
        lea       eax, DWORD PTR [376+esp]                      ;46.4
        mov       DWORD PTR [236+esp], ebx                      ;45.4
        mov       DWORD PTR [376+esp], 11                       ;46.4
        mov       DWORD PTR [380+esp], OFFSET FLAT: __STRLITPACK_42 ;46.4
        push      32                                            ;46.4
        push      eax                                           ;46.4
        push      OFFSET FLAT: __STRLITPACK_62.0.1              ;46.4
        push      -2088435968                                   ;46.4
        push      5545                                          ;46.4
        lea       edx, DWORD PTR [532+esp]                      ;46.4
        push      edx                                           ;46.4
        call      _for_write_seq_lis                            ;46.4
                                ; LOE ebx
.B1.20:                         ; Preds .B1.19
        mov       DWORD PTR [608+esp], ebx                      ;46.4
        lea       eax, DWORD PTR [608+esp]                      ;46.4
        push      eax                                           ;46.4
        push      OFFSET FLAT: __STRLITPACK_63.0.1              ;46.4
        lea       edx, DWORD PTR [544+esp]                      ;46.4
        push      edx                                           ;46.4
        call      _for_write_seq_lis_xmit                       ;46.4
                                ; LOE ebx
.B1.21:                         ; Preds .B1.20
        mov       DWORD PTR [420+esp], 13                       ;46.4
        lea       eax, DWORD PTR [420+esp]                      ;46.4
        mov       DWORD PTR [424+esp], OFFSET FLAT: __STRLITPACK_41 ;46.4
        push      eax                                           ;46.4
        push      OFFSET FLAT: __STRLITPACK_64.0.1              ;46.4
        lea       edx, DWORD PTR [556+esp]                      ;46.4
        push      edx                                           ;46.4
        call      _for_write_seq_lis_xmit                       ;46.4
                                ; LOE ebx
.B1.22:                         ; Preds .B1.21
        imul      eax, ebx, 900                                 ;46.4
        lea       ecx, DWORD PTR [640+esp]                      ;46.4
        add       eax, DWORD PTR [80+esp]                       ;46.4
        sub       eax, DWORD PTR [84+esp]                       ;46.4
        movzx     edx, WORD PTR [682+eax]                       ;46.4
        mov       WORD PTR [640+esp], dx                        ;46.4
        push      ecx                                           ;46.4
        push      OFFSET FLAT: __STRLITPACK_65.0.1              ;46.4
        lea       ebx, DWORD PTR [568+esp]                      ;46.4
        push      ebx                                           ;46.4
        call      _for_write_seq_lis_xmit                       ;46.4
                                ; LOE
.B1.23:                         ; Preds .B1.22
        mov       DWORD PTR [572+esp], 0                        ;48.4
        lea       eax, DWORD PTR [660+esp]                      ;48.4
        mov       DWORD PTR [660+esp], OFFSET FLAT: _DYNUST_MAIN_MODULE_mp_NOOFFLOWMODEL ;48.4
        push      32                                            ;48.4
        push      eax                                           ;48.4
        push      OFFSET FLAT: __STRLITPACK_66.0.1              ;48.4
        push      -2088435965                                   ;48.4
        push      55                                            ;48.4
        lea       edx, DWORD PTR [592+esp]                      ;48.4
        push      edx                                           ;48.4
        call      _for_read_seq_lis                             ;48.4
                                ; LOE eax
.B1.24:                         ; Preds .B1.23
        mov       ebx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFFLOWMODEL] ;49.4
        xor       edx, edx                                      ;49.4
        test      ebx, ebx                                      ;49.4
        mov       DWORD PTR [_DYNUST_MAIN_MODULE_mp_ERROR], eax ;48.4
        cmovle    ebx, edx                                      ;49.4
        mov       ecx, 4                                        ;49.4
        mov       eax, 1                                        ;49.4
        lea       esi, DWORD PTR [328+esp]                      ;49.4
        mov       DWORD PTR [512+esp], 133                      ;49.4
        mov       DWORD PTR [504+esp], ecx                      ;49.4
        mov       DWORD PTR [516+esp], eax                      ;49.4
        mov       DWORD PTR [508+esp], edx                      ;49.4
        mov       DWORD PTR [532+esp], eax                      ;49.4
        mov       DWORD PTR [524+esp], ebx                      ;49.4
        mov       DWORD PTR [528+esp], ecx                      ;49.4
        push      ecx                                           ;49.4
        push      ebx                                           ;49.4
        push      2                                             ;49.4
        push      esi                                           ;49.4
        call      _for_check_mult_overflow                      ;49.4
                                ; LOE eax
.B1.25:                         ; Preds .B1.24
        mov       edx, DWORD PTR [528+esp]                      ;49.4
        and       eax, 1                                        ;49.4
        and       edx, 1                                        ;49.4
        lea       ecx, DWORD PTR [516+esp]                      ;49.4
        shl       eax, 4                                        ;49.4
        add       edx, edx                                      ;49.4
        or        edx, eax                                      ;49.4
        or        edx, 262144                                   ;49.4
        push      edx                                           ;49.4
        push      ecx                                           ;49.4
        push      DWORD PTR [352+esp]                           ;49.4
        call      _for_alloc_allocatable                        ;49.4
                                ; LOE
.B1.166:                        ; Preds .B1.25
        add       esp, 112                                      ;49.4
                                ; LOE
.B1.26:                         ; Preds .B1.166
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFFLOWMODEL] ;51.4
        test      eax, eax                                      ;51.4
        jle       .B1.42        ; Prob 2%                       ;51.4
                                ; LOE eax
.B1.27:                         ; Preds .B1.26
        movss     xmm1, DWORD PTR [_2il0floatpacket.4]          ;165.111
        movss     xmm0, DWORD PTR [_2il0floatpacket.5]          ;141.14
        mov       DWORD PTR [268+esp], eax                      ;141.14
        mov       ebx, 1                                        ;
                                ; LOE ebx
.B1.28:                         ; Preds .B1.40 .B1.27
        mov       DWORD PTR [512+esp], 0                        ;53.6
        mov       DWORD PTR [648+esp], 22                       ;53.6
        mov       DWORD PTR [652+esp], OFFSET FLAT: __STRLITPACK_39 ;53.6
        push      32                                            ;53.6
        lea       eax, DWORD PTR [652+esp]                      ;53.6
        push      eax                                           ;53.6
        push      OFFSET FLAT: __STRLITPACK_67.0.1              ;53.6
        push      -2088435968                                   ;53.6
        push      5545                                          ;53.6
        lea       edx, DWORD PTR [532+esp]                      ;53.6
        push      edx                                           ;53.6
        call      _for_write_seq_lis                            ;53.6
                                ; LOE ebx
.B1.29:                         ; Preds .B1.28
        mov       DWORD PTR [704+esp], ebx                      ;53.6
        lea       eax, DWORD PTR [704+esp]                      ;53.6
        push      eax                                           ;53.6
        push      OFFSET FLAT: __STRLITPACK_68.0.1              ;53.6
        lea       edx, DWORD PTR [544+esp]                      ;53.6
        push      edx                                           ;53.6
        call      _for_write_seq_lis_xmit                       ;53.6
                                ; LOE ebx
.B1.167:                        ; Preds .B1.29
        add       esp, 36                                       ;53.6
                                ; LOE ebx
.B1.30:                         ; Preds .B1.167
        cmp       DWORD PTR [_DYNUST_MAIN_MODULE_mp_TFLSWITCH], 1 ;54.19
        je        .B1.149       ; Prob 16%                      ;54.19
                                ; LOE ebx
.B1.31:                         ; Preds .B1.30
        mov       DWORD PTR [512+esp], 0                        ;57.8
        lea       eax, DWORD PTR [260+esp]                      ;57.8
        mov       DWORD PTR [664+esp], eax                      ;57.8
        lea       edx, DWORD PTR [664+esp]                      ;57.8
        push      32                                            ;57.8
        push      edx                                           ;57.8
        push      OFFSET FLAT: __STRLITPACK_72.0.1              ;57.8
        push      -2088435968                                   ;57.8
        push      55                                            ;57.8
        lea       ecx, DWORD PTR [532+esp]                      ;57.8
        push      ecx                                           ;57.8
        call      _for_read_seq_lis                             ;57.8
                                ; LOE ebx
.B1.32:                         ; Preds .B1.31
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC+32] ;57.8
        mov       esi, ebx                                      ;57.8
        shl       eax, 5                                        ;57.8
        lea       edi, DWORD PTR [696+esp]                      ;57.8
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC] ;57.8
        shl       esi, 5                                        ;57.8
        sub       edx, eax                                      ;57.8
        lea       ecx, DWORD PTR [24+edx+esi]                   ;57.8
        mov       DWORD PTR [696+esp], ecx                      ;57.8
        push      edi                                           ;57.8
        push      OFFSET FLAT: __STRLITPACK_73.0.1              ;57.8
        lea       eax, DWORD PTR [544+esp]                      ;57.8
        push      eax                                           ;57.8
        call      _for_read_seq_lis_xmit                        ;57.8
                                ; LOE ebx esi
.B1.168:                        ; Preds .B1.32
        add       esp, 36                                       ;57.8
                                ; LOE ebx esi
.B1.33:                         ; Preds .B1.168
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC] ;58.8
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC+32] ;58.8
        mov       DWORD PTR [276+esp], eax                      ;58.8
        shl       eax, 5                                        ;58.8
        lea       ecx, DWORD PTR [esi+edx]                      ;58.8
        sub       ecx, eax                                      ;58.8
        mov       DWORD PTR [28+ecx], 1                         ;58.8
                                ; LOE edx ebx esi
.B1.34:                         ; Preds .B1.152 .B1.33
        mov       ecx, DWORD PTR [276+esp]                      ;60.24
        lea       eax, DWORD PTR [esi+edx]                      ;60.24
        shl       ecx, 5                                        ;60.24
        neg       ecx                                           ;60.24
        mov       DWORD PTR [292+esp], edx                      ;
        mov       edx, 1                                        ;60.24
        mov       edi, DWORD PTR [28+ecx+eax]                   ;60.9
        test      edi, edi                                      ;60.24
        cmovle    edi, edx                                      ;60.24
        mov       DWORD PTR [284+esp], edi                      ;60.24
        cmp       edi, 1                                        ;61.24
        mov       DWORD PTR [28+ecx+eax], edi                   ;60.29
        mov       edx, DWORD PTR [292+esp]                      ;61.24
        je        .B1.106       ; Prob 16%                      ;61.24
                                ; LOE eax edx ecx ebx esi dl cl dh ch
.B1.35:                         ; Preds .B1.34
        cmp       DWORD PTR [284+esp], 2                        ;101.25
        je        .B1.61        ; Prob 16%                      ;101.25
                                ; LOE eax edx ecx ebx esi dl cl dh ch
.B1.36:                         ; Preds .B1.180 .B1.173 .B1.35
        pxor      xmm0, xmm0                                    ;
                                ; LOE ebx xmm0
.B1.37:                         ; Preds .B1.148 .B1.130 .B1.105 .B1.86 .B1.36
                                ;      
        mov       edx, ebx                                      ;140.3
        sub       edx, DWORD PTR [448+esp]                      ;140.3
        mov       eax, DWORD PTR [416+esp]                      ;140.3
        comiss    xmm0, DWORD PTR [_2il0floatpacket.5]          ;141.14
        movss     DWORD PTR [eax+edx*4], xmm0                   ;140.3
        jbe       .B1.40        ; Prob 50%                      ;141.14
                                ; LOE ebx xmm0
.B1.38:                         ; Preds .B1.37
        mov       DWORD PTR [512+esp], 0                        ;142.4
        lea       eax, DWORD PTR [632+esp]                      ;142.4
        mov       DWORD PTR [632+esp], ebx                      ;142.4
        push      32                                            ;142.4
        push      OFFSET FLAT: READ_TRAFFIC_FLOW_MODELS$format_pack.0.1+204 ;142.4
        push      eax                                           ;142.4
        push      OFFSET FLAT: __STRLITPACK_128.0.1             ;142.4
        push      -2088435968                                   ;142.4
        push      511                                           ;142.4
        lea       edx, DWORD PTR [536+esp]                      ;142.4
        push      edx                                           ;142.4
        movss     DWORD PTR [280+esp], xmm0                     ;142.4
        call      _for_write_seq_fmt                            ;142.4
                                ; LOE ebx
.B1.39:                         ; Preds .B1.38
        movss     xmm0, DWORD PTR [280+esp]                     ;
        lea       eax, DWORD PTR [668+esp]                      ;142.4
        movss     DWORD PTR [668+esp], xmm0                     ;142.4
        push      eax                                           ;142.4
        push      OFFSET FLAT: __STRLITPACK_129.0.1             ;142.4
        lea       edx, DWORD PTR [548+esp]                      ;142.4
        push      edx                                           ;142.4
        call      _for_write_seq_fmt_xmit                       ;142.4
                                ; LOE ebx
.B1.169:                        ; Preds .B1.39
        add       esp, 40                                       ;142.4
                                ; LOE ebx
.B1.40:                         ; Preds .B1.169 .B1.37
        inc       ebx                                           ;144.4
        cmp       ebx, DWORD PTR [268+esp]                      ;144.4
        jle       .B1.28        ; Prob 82%                      ;144.4
                                ; LOE ebx
.B1.41:                         ; Preds .B1.40
        mov       eax, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFFLOWMODEL] ;146.4
                                ; LOE eax
.B1.42:                         ; Preds .B1.41 .B1.26
        mov       edi, eax                                      ;146.4
        mov       esi, 300                                      ;147.4
        shl       edi, 5                                        ;146.4
        mov       ebx, 100                                      ;147.4
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC] ;146.4
        add       edi, edx                                      ;146.4
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC+32] ;146.4
        shl       ecx, 5                                        ;146.4
        sub       edi, ecx                                      ;146.4
        mov       DWORD PTR [44+esp], ecx                       ;146.4
        mov       ecx, DWORD PTR [_DYNUST_MAIN_MODULE_mp_NOOFARCS] ;154.4
        test      ecx, ecx                                      ;154.4
        mov       DWORD PTR [76+esp], ecx                       ;154.4
        mov       DWORD PTR [56+edi], 1                         ;146.4
        mov       DWORD PTR [32+edi], esi                       ;147.4
        mov       DWORD PTR [36+edi], ebx                       ;147.4
        mov       DWORD PTR [40+edi], ebx                       ;147.4
        mov       DWORD PTR [44+edi], esi                       ;147.4
        mov       DWORD PTR [48+edi], 1065353216                ;151.4
        mov       DWORD PTR [52+edi], 1065353216                ;152.4
        jle       .B1.56        ; Prob 2%                       ;154.4
                                ; LOE eax edx
.B1.43:                         ; Preds .B1.42
        imul      edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32], -900 ;
        mov       ebx, edx                                      ;
        imul      esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32], -152 ;
        imul      ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE+32], -44 ;
        add       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;
        inc       eax                                           ;155.66
        add       esi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;
        sub       ebx, DWORD PTR [44+esp]                       ;
        add       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE] ;
        mov       DWORD PTR [36+esp], 900                       ;
        mov       DWORD PTR [16+esp], 152                       ;
        mov       DWORD PTR [60+esp], esi                       ;
        mov       DWORD PTR [92+esp], edi                       ;
        mov       DWORD PTR [100+esp], 1                        ;
        mov       DWORD PTR [68+esp], edx                       ;
        mov       DWORD PTR [52+esp], ecx                       ;
        mov       DWORD PTR [268+esp], eax                      ;
        mov       DWORD PTR [84+esp], ebx                       ;
        mov       esi, DWORD PTR [16+esp]                       ;
        mov       edi, DWORD PTR [36+esp]                       ;
                                ; LOE esi edi
.B1.44:                         ; Preds .B1.50 .B1.43
        mov       eax, DWORD PTR [92+esp]                       ;155.10
        movsx     ebx, BYTE PTR [680+edi+eax]                   ;155.10
        cmp       ebx, DWORD PTR [268+esp]                      ;155.51
        jle       .B1.50        ; Prob 50%                      ;155.51
                                ; LOE ebx esi edi
.B1.45:                         ; Preds .B1.44
        mov       DWORD PTR [512+esp], 0                        ;156.9
        lea       eax, DWORD PTR [608+esp]                      ;156.9
        mov       DWORD PTR [608+esp], 51                       ;156.9
        mov       DWORD PTR [612+esp], OFFSET FLAT: __STRLITPACK_7 ;156.9
        push      32                                            ;156.9
        push      eax                                           ;156.9
        push      OFFSET FLAT: __STRLITPACK_130.0.1             ;156.9
        push      -2088435968                                   ;156.9
        push      911                                           ;156.9
        lea       edx, DWORD PTR [532+esp]                      ;156.9
        push      edx                                           ;156.9
        call      _for_write_seq_lis                            ;156.9
                                ; LOE ebx esi edi
.B1.46:                         ; Preds .B1.45
        mov       DWORD PTR [536+esp], 0                        ;157.9
        lea       eax, DWORD PTR [640+esp]                      ;157.9
        mov       DWORD PTR [640+esp], 4                        ;157.9
        mov       DWORD PTR [644+esp], OFFSET FLAT: __STRLITPACK_4 ;157.9
        push      32                                            ;157.9
        push      eax                                           ;157.9
        push      OFFSET FLAT: __STRLITPACK_131.0.1             ;157.9
        push      -2088435968                                   ;157.9
        push      911                                           ;157.9
        lea       edx, DWORD PTR [556+esp]                      ;157.9
        push      edx                                           ;157.9
        call      _for_write_seq_lis                            ;157.9
                                ; LOE ebx esi edi
.B1.47:                         ; Preds .B1.46
        mov       eax, DWORD PTR [108+esp]                      ;157.30
        mov       edx, DWORD PTR [100+esp]                      ;157.9
        imul      ecx, DWORD PTR [28+esi+eax], 44               ;157.30
        mov       eax, DWORD PTR [36+edx+ecx]                   ;157.9
        lea       edx, DWORD PTR [64+esp]                       ;157.9
        mov       DWORD PTR [64+esp], eax                       ;157.9
        push      edx                                           ;157.9
        push      OFFSET FLAT: __STRLITPACK_132.0.1             ;157.9
        lea       ecx, DWORD PTR [568+esp]                      ;157.9
        push      ecx                                           ;157.9
        call      _for_write_seq_lis_xmit                       ;157.9
                                ; LOE ebx esi edi
.B1.48:                         ; Preds .B1.47
        mov       DWORD PTR [684+esp], 4                        ;157.9
        lea       eax, DWORD PTR [684+esp]                      ;157.9
        mov       DWORD PTR [688+esp], OFFSET FLAT: __STRLITPACK_3 ;157.9
        push      eax                                           ;157.9
        push      OFFSET FLAT: __STRLITPACK_133.0.1             ;157.9
        lea       edx, DWORD PTR [580+esp]                      ;157.9
        push      edx                                           ;157.9
        call      _for_write_seq_lis_xmit                       ;157.9
                                ; LOE ebx esi edi
.B1.49:                         ; Preds .B1.48
        mov       eax, DWORD PTR [132+esp]                      ;157.116
        mov       edx, DWORD PTR [124+esp]                      ;157.9
        imul      ecx, DWORD PTR [24+esi+eax], 44               ;157.116
        mov       eax, DWORD PTR [36+edx+ecx]                   ;157.9
        lea       edx, DWORD PTR [728+esp]                      ;157.9
        mov       DWORD PTR [728+esp], eax                      ;157.9
        push      edx                                           ;157.9
        push      OFFSET FLAT: __STRLITPACK_134.0.1             ;157.9
        lea       ecx, DWORD PTR [592+esp]                      ;157.9
        push      ecx                                           ;157.9
        call      _for_write_seq_lis_xmit                       ;157.9
                                ; LOE ebx esi edi
.B1.170:                        ; Preds .B1.49
        add       esp, 84                                       ;157.9
                                ; LOE ebx esi edi
.B1.50:                         ; Preds .B1.170 .B1.44
        shl       ebx, 5                                        ;159.43
        add       esi, 152                                      ;161.4
        mov       eax, DWORD PTR [84+esp]                       ;159.6
        mov       ecx, DWORD PTR [92+esp]                       ;159.6
        mov       edx, DWORD PTR [12+eax+ebx]                   ;159.6
        mov       ebx, DWORD PTR [100+esp]                      ;161.4
        inc       ebx                                           ;161.4
        mov       DWORD PTR [8+edi+ecx], edx                    ;159.6
        add       edi, 900                                      ;161.4
        mov       DWORD PTR [100+esp], ebx                      ;161.4
        cmp       ebx, DWORD PTR [76+esp]                       ;161.4
        jle       .B1.44        ; Prob 82%                      ;161.4
                                ; LOE esi edi
.B1.51:                         ; Preds .B1.50
        imul      edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE+32], -152 ;
        xor       esi, esi                                      ;
        imul      ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE+32], -900 ;
        movss     xmm1, DWORD PTR [_2il0floatpacket.4]          ;165.111
        movss     xmm0, DWORD PTR [_2il0floatpacket.6]          ;171.69
        mov       eax, DWORD PTR [448+esp]                      ;171.55
        xor       ecx, ecx                                      ;
        shl       eax, 2                                        ;
        mov       edx, DWORD PTR [68+esp]                       ;
        neg       eax                                           ;
        sub       edx, DWORD PTR [44+esp]                       ;
        add       eax, DWORD PTR [416+esp]                      ;
        add       edi, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE] ;
        mov       DWORD PTR [68+esp], edx                       ;
        add       ebx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE] ;
        mov       DWORD PTR [52+esp], edi                       ;
        mov       DWORD PTR [36+esp], eax                       ;
        mov       DWORD PTR [60+esp], esi                       ;
        mov       edx, ecx                                      ;
                                ; LOE edx ecx ebx xmm0 xmm1
.B1.52:                         ; Preds .B1.54 .B1.51
        movsx     esi, BYTE PTR [1568+ecx+ebx]                  ;164.37
        movsx     edi, BYTE PTR [1581+ecx+ebx]                  ;165.75
        cvtsi2ss  xmm6, esi                                     ;164.37
        movzx     esi, WORD PTR [1582+ecx+ebx]                  ;165.74
        add       esi, edi                                      ;165.74
        movsx     edi, si                                       ;165.37
        cvtsi2ss  xmm3, edi                                     ;165.37
        divss     xmm3, xmm1                                    ;165.7
        mov       eax, DWORD PTR [52+esp]                       ;164.71
        movsx     edi, BYTE PTR [1580+ecx+ebx]                  ;168.12
        mov       DWORD PTR [44+esp], edi                       ;168.12
        movss     xmm4, DWORD PTR [172+edx+eax]                 ;164.71
        movaps    xmm2, xmm4                                    ;164.7
        divss     xmm4, xmm3                                    ;166.7
        imul      esi, DWORD PTR [164+edx+eax], 152             ;167.7
        mulss     xmm2, xmm6                                    ;164.7
        shl       edi, 5                                        ;169.7
        movss     DWORD PTR [8+eax+esi], xmm4                   ;167.7
        mov       esi, DWORD PTR [68+esp]                       ;169.7
        movsx     eax, WORD PTR [300+edx+eax]                   ;170.10
        cmp       eax, 99                                       ;170.48
        movss     DWORD PTR [1572+ecx+ebx], xmm2                ;164.7
        cvtsi2ss  xmm5, DWORD PTR [12+edi+esi]                  ;169.7
        movss     DWORD PTR [1552+ecx+ebx], xmm3                ;165.7
        movss     DWORD PTR [1596+ecx+ebx], xmm4                ;166.7
        movss     DWORD PTR [1576+ecx+ebx], xmm5                ;169.7
        je        .B1.54        ; Prob 16%                      ;170.48
                                ; LOE edx ecx ebx xmm0 xmm1 xmm6
.B1.53:                         ; Preds .B1.52
        mov       eax, DWORD PTR [36+esp]                       ;171.55
        mov       esi, DWORD PTR [44+esp]                       ;171.55
        movss     xmm2, DWORD PTR [eax+esi*4]                   ;171.55
        divss     xmm2, xmm0                                    ;171.69
        mulss     xmm6, xmm2                                    ;171.9
        movss     DWORD PTR [1336+ecx+ebx], xmm6                ;171.9
        movss     DWORD PTR [1332+ecx+ebx], xmm6                ;172.9
                                ; LOE edx ecx ebx xmm0 xmm1
.B1.54:                         ; Preds .B1.52 .B1.53
        mov       eax, DWORD PTR [60+esp]                       ;163.4
        add       ecx, 900                                      ;163.4
        inc       eax                                           ;163.4
        add       edx, 152                                      ;163.4
        mov       DWORD PTR [60+esp], eax                       ;163.4
        cmp       eax, DWORD PTR [76+esp]                       ;163.4
        jb        .B1.52        ; Prob 82%                      ;163.4
                                ; LOE edx ecx ebx xmm0 xmm1
.B1.55:                         ; Preds .B1.54
        mov       eax, DWORD PTR [44+esp]                       ;
        mov       DWORD PTR [228+esp], eax                      ;168.7
                                ; LOE
.B1.56:                         ; Preds .B1.42 .B1.55
        xor       eax, eax                                      ;175.10
        mov       DWORD PTR [512+esp], eax                      ;175.10
        push      32                                            ;175.10
        push      eax                                           ;175.10
        push      OFFSET FLAT: __STRLITPACK_135.0.1             ;175.10
        push      -2088435968                                   ;175.10
        push      5545                                          ;175.10
        lea       edx, DWORD PTR [532+esp]                      ;175.10
        push      edx                                           ;175.10
        call      _for_close                                    ;175.10
                                ; LOE
.B1.171:                        ; Preds .B1.56
        add       esp, 24                                       ;175.10
                                ; LOE
.B1.57:                         ; Preds .B1.171
        mov       ebx, DWORD PTR [428+esp]                      ;177.1
        test      bl, 1                                         ;177.1
        jne       .B1.59        ; Prob 3%                       ;177.1
                                ; LOE ebx
.B1.58:                         ; Preds .B1.57
        add       esp, 692                                      ;177.1
        pop       ebx                                           ;177.1
        pop       edi                                           ;177.1
        pop       esi                                           ;177.1
        mov       esp, ebp                                      ;177.1
        pop       ebp                                           ;177.1
        ret                                                     ;177.1
                                ; LOE
.B1.59:                         ; Preds .B1.57                  ; Infreq
        mov       edx, ebx                                      ;177.1
        mov       eax, ebx                                      ;177.1
        shr       edx, 1                                        ;177.1
        and       eax, 1                                        ;177.1
        and       edx, 1                                        ;177.1
        add       eax, eax                                      ;177.1
        shl       edx, 2                                        ;177.1
        or        edx, eax                                      ;177.1
        or        edx, 262144                                   ;177.1
        push      edx                                           ;177.1
        push      DWORD PTR [420+esp]                           ;177.1
        call      _for_dealloc_allocatable                      ;177.1
                                ; LOE ebx
.B1.172:                        ; Preds .B1.59                  ; Infreq
        add       esp, 8                                        ;177.1
                                ; LOE ebx
.B1.60:                         ; Preds .B1.172                 ; Infreq
        and       ebx, -2                                       ;177.1
        mov       DWORD PTR [416+esp], 0                        ;177.1
        mov       DWORD PTR [428+esp], ebx                      ;177.1
        add       esp, 692                                      ;177.1
        pop       ebx                                           ;177.1
        pop       edi                                           ;177.1
        pop       esi                                           ;177.1
        mov       esp, ebp                                      ;177.1
        pop       ebp                                           ;177.1
        ret                                                     ;177.1
                                ; LOE
.B1.61:                         ; Preds .B1.35                  ; Infreq
        mov       DWORD PTR [512+esp], 0                        ;103.6
        mov       eax, DWORD PTR [24+ecx+eax]                   ;102.10
        cmp       eax, 1                                        ;102.27
        je        .B1.87        ; Prob 16%                      ;102.27
                                ; LOE eax edx ebx esi dl dh
.B1.62:                         ; Preds .B1.61                  ; Infreq
        cmp       eax, 2                                        ;117.28
        je        .B1.68        ; Prob 16%                      ;117.28
                                ; LOE ebx esi
.B1.63:                         ; Preds .B1.62                  ; Infreq
        mov       eax, 32                                       ;134.9
        lea       edx, DWORD PTR [328+esp]                      ;134.9
        mov       DWORD PTR [328+esp], eax                      ;134.9
        mov       DWORD PTR [332+esp], OFFSET FLAT: __STRLITPACK_15 ;134.9
        push      eax                                           ;134.9
        push      edx                                           ;134.9
        push      OFFSET FLAT: __STRLITPACK_124.0.1             ;134.9
        push      -2088435968                                   ;134.9
        push      911                                           ;134.9
        lea       ecx, DWORD PTR [532+esp]                      ;134.9
        push      ecx                                           ;134.9
        call      _for_write_seq_lis                            ;134.9
                                ; LOE ebx
.B1.64:                         ; Preds .B1.63                  ; Infreq
        mov       DWORD PTR [536+esp], 0                        ;135.6
        lea       eax, DWORD PTR [360+esp]                      ;135.6
        mov       DWORD PTR [360+esp], 39                       ;135.6
        mov       DWORD PTR [364+esp], OFFSET FLAT: __STRLITPACK_13 ;135.6
        push      32                                            ;135.6
        push      eax                                           ;135.6
        push      OFFSET FLAT: __STRLITPACK_125.0.1             ;135.6
        push      -2088435968                                   ;135.6
        push      911                                           ;135.6
        lea       edx, DWORD PTR [556+esp]                      ;135.6
        push      edx                                           ;135.6
        call      _for_write_seq_lis                            ;135.6
                                ; LOE ebx
.B1.65:                         ; Preds .B1.64                  ; Infreq
        mov       DWORD PTR [560+esp], 0                        ;136.6
        lea       eax, DWORD PTR [392+esp]                      ;136.6
        mov       DWORD PTR [392+esp], 36                       ;136.6
        mov       DWORD PTR [396+esp], OFFSET FLAT: __STRLITPACK_11 ;136.6
        push      32                                            ;136.6
        push      eax                                           ;136.6
        push      OFFSET FLAT: __STRLITPACK_126.0.1             ;136.6
        push      -2088435968                                   ;136.6
        push      911                                           ;136.6
        lea       edx, DWORD PTR [580+esp]                      ;136.6
        push      edx                                           ;136.6
        call      _for_write_seq_lis                            ;136.6
                                ; LOE ebx
.B1.66:                         ; Preds .B1.65                  ; Infreq
        push      32                                            ;137.6
        xor       eax, eax                                      ;137.6
        push      eax                                           ;137.6
        push      eax                                           ;137.6
        push      -2088435968                                   ;137.6
        push      eax                                           ;137.6
        push      OFFSET FLAT: __STRLITPACK_127                 ;137.6
        call      _for_stop_core                                ;137.6
                                ; LOE ebx
.B1.173:                        ; Preds .B1.66                  ; Infreq
        add       esp, 96                                       ;137.6
        jmp       .B1.36        ; Prob 100%                     ;137.6
                                ; LOE ebx
.B1.68:                         ; Preds .B1.62                  ; Infreq
        lea       eax, DWORD PTR [20+esp]                       ;118.6
        mov       DWORD PTR [24+esp], eax                       ;118.6
        lea       edx, DWORD PTR [24+esp]                       ;118.6
        push      32                                            ;118.6
        push      edx                                           ;118.6
        push      OFFSET FLAT: __STRLITPACK_112.0.1             ;118.6
        push      -2088435968                                   ;118.6
        push      55                                            ;118.6
        lea       ecx, DWORD PTR [532+esp]                      ;118.6
        push      ecx                                           ;118.6
        call      _for_read_seq_lis                             ;118.6
                                ; LOE ebx esi
.B1.69:                         ; Preds .B1.68                  ; Infreq
        lea       edx, DWORD PTR [56+esp]                       ;118.6
        lea       eax, DWORD PTR [52+esp]                       ;118.6
        mov       DWORD PTR [56+esp], eax                       ;118.6
        push      edx                                           ;118.6
        push      OFFSET FLAT: __STRLITPACK_113.0.1             ;118.6
        lea       ecx, DWORD PTR [544+esp]                      ;118.6
        push      ecx                                           ;118.6
        call      _for_read_seq_lis_xmit                        ;118.6
                                ; LOE ebx esi
.B1.70:                         ; Preds .B1.69                  ; Infreq
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC+32] ;118.6
        lea       edi, DWORD PTR [76+esp]                       ;118.6
        shl       eax, 5                                        ;118.6
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC] ;118.6
        sub       edx, eax                                      ;118.6
        lea       ecx, DWORD PTR [8+edx+esi]                    ;118.6
        mov       DWORD PTR [76+esp], ecx                       ;118.6
        push      edi                                           ;118.6
        push      OFFSET FLAT: __STRLITPACK_114.0.1             ;118.6
        lea       eax, DWORD PTR [556+esp]                      ;118.6
        push      eax                                           ;118.6
        call      _for_read_seq_lis_xmit                        ;118.6
                                ; LOE ebx esi
.B1.71:                         ; Preds .B1.70                  ; Infreq
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC+32] ;118.6
        lea       edi, DWORD PTR [96+esp]                       ;118.6
        shl       eax, 5                                        ;118.6
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC] ;118.6
        sub       edx, eax                                      ;118.6
        lea       ecx, DWORD PTR [12+edx+esi]                   ;118.6
        mov       DWORD PTR [96+esp], ecx                       ;118.6
        push      edi                                           ;118.6
        push      OFFSET FLAT: __STRLITPACK_115.0.1             ;118.6
        lea       eax, DWORD PTR [568+esp]                      ;118.6
        push      eax                                           ;118.6
        call      _for_read_seq_lis_xmit                        ;118.6
                                ; LOE ebx esi
.B1.72:                         ; Preds .B1.71                  ; Infreq
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC+32] ;118.6
        lea       edi, DWORD PTR [116+esp]                      ;118.6
        shl       eax, 5                                        ;118.6
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC] ;118.6
        sub       edx, eax                                      ;118.6
        lea       ecx, DWORD PTR [16+edx+esi]                   ;118.6
        mov       DWORD PTR [116+esp], ecx                      ;118.6
        push      edi                                           ;118.6
        push      OFFSET FLAT: __STRLITPACK_116.0.1             ;118.6
        lea       eax, DWORD PTR [580+esp]                      ;118.6
        push      eax                                           ;118.6
        call      _for_read_seq_lis_xmit                        ;118.6
                                ; LOE ebx esi
.B1.73:                         ; Preds .B1.72                  ; Infreq
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC+32] ;118.6
        lea       edi, DWORD PTR [136+esp]                      ;118.6
        shl       eax, 5                                        ;118.6
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC] ;118.6
        sub       edx, eax                                      ;118.6
        lea       ecx, DWORD PTR [20+edx+esi]                   ;118.6
        mov       DWORD PTR [136+esp], ecx                      ;118.6
        push      edi                                           ;118.6
        push      OFFSET FLAT: __STRLITPACK_117.0.1             ;118.6
        lea       eax, DWORD PTR [592+esp]                      ;118.6
        push      eax                                           ;118.6
        call      _for_read_seq_lis_xmit                        ;118.6
                                ; LOE ebx esi
.B1.174:                        ; Preds .B1.73                  ; Infreq
        add       esp, 84                                       ;118.6
                                ; LOE ebx esi
.B1.74:                         ; Preds .B1.174                 ; Infreq
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC] ;119.9
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC+32] ;119.9
        mov       edx, eax                                      ;119.9
        shl       edx, 5                                        ;119.9
        pxor      xmm0, xmm0                                    ;
        movss     DWORD PTR [68+esp], xmm0                      ;
        lea       edi, DWORD PTR [esi+ecx]                      ;119.9
        sub       edi, edx                                      ;119.9
        mov       edx, DWORD PTR [12+edi]                       ;120.9
        test      edx, edx                                      ;120.9
        mov       DWORD PTR [edi], 0                            ;119.9
        jle       .B1.82        ; Prob 2%                       ;120.9
                                ; LOE eax edx ecx ebx esi
.B1.75:                         ; Preds .B1.74                  ; Infreq
        mov       DWORD PTR [60+esp], edx                       ;
        mov       edi, 1                                        ;
        mov       DWORD PTR [188+esp], esi                      ;
        lea       esi, DWORD PTR [512+esp]                      ;
                                ; LOE eax ebx esi edi
.B1.76:                         ; Preds .B1.193 .B1.75          ; Infreq
        shl       eax, 5                                        ;122.11
        mov       ecx, DWORD PTR [188+esp]                      ;122.11
        neg       eax                                           ;122.11
        add       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC] ;122.11
        cvtsi2ss  xmm0, edi                                     ;124.11
        mov       edx, DWORD PTR [24+eax+ecx]                   ;122.11
        mov       eax, DWORD PTR [28+eax+ecx]                   ;123.11
        mov       DWORD PTR [172+esp], eax                      ;123.11
        lea       eax, DWORD PTR [180+esp]                      ;125.13
        mov       DWORD PTR [228+esp], ebx                      ;121.11
        mov       DWORD PTR [164+esp], edx                      ;122.11
        movss     DWORD PTR [156+esp], xmm0                     ;124.11
        push      eax                                           ;125.13
        lea       edx, DWORD PTR [160+esp]                      ;125.13
        push      edx                                           ;125.13
        lea       ecx, DWORD PTR [180+esp]                      ;125.13
        push      ecx                                           ;125.13
        lea       eax, DWORD PTR [176+esp]                      ;125.13
        push      eax                                           ;125.13
        lea       edx, DWORD PTR [244+esp]                      ;125.13
        push      edx                                           ;125.13
        lea       ecx, DWORD PTR [256+esp]                      ;125.13
        push      ecx                                           ;125.13
        call      _DYNUST_AMS_MODULE_mp_VEHSPEED_UPDATE         ;125.13
                                ; LOE ebx esi edi
.B1.77:                         ; Preds .B1.76                  ; Infreq
        movss     xmm0, DWORD PTR [180+esp]                     ;126.8
        mov       eax, DWORD PTR [180+esp]                      ;126.8
        mov       DWORD PTR [536+esp], 0                        ;126.8
        movss     DWORD PTR [100+esp], xmm0                     ;126.8
        mov       DWORD PTR [416+esp], eax                      ;126.8
        push      32                                            ;126.8
        push      OFFSET FLAT: READ_TRAFFIC_FLOW_MODELS$format_pack.0.1+164 ;126.8
        lea       edx, DWORD PTR [424+esp]                      ;126.8
        push      edx                                           ;126.8
        push      OFFSET FLAT: __STRLITPACK_118.0.1             ;126.8
        push      -2088435968                                   ;126.8
        push      5545                                          ;126.8
        push      esi                                           ;126.8
        call      _for_write_seq_fmt                            ;126.8
                                ; LOE ebx esi edi
.B1.78:                         ; Preds .B1.77                  ; Infreq
        movss     xmm1, DWORD PTR [232+esp]                     ;126.8
        lea       eax, DWORD PTR [452+esp]                      ;126.8
        movss     xmm0, DWORD PTR [_2il0floatpacket.4]          ;126.8
        mulss     xmm0, xmm1                                    ;126.8
        movss     DWORD PTR [136+esp], xmm1                     ;126.8
        movss     DWORD PTR [452+esp], xmm0                     ;126.8
        push      eax                                           ;126.8
        push      OFFSET FLAT: __STRLITPACK_119.0.1             ;126.8
        push      esi                                           ;126.8
        call      _for_write_seq_fmt_xmit                       ;126.8
                                ; LOE ebx esi edi
.B1.79:                         ; Preds .B1.78                  ; Infreq
        movss     xmm0, DWORD PTR [140+esp]                     ;126.70
        lea       eax, DWORD PTR [472+esp]                      ;126.8
        mulss     xmm0, DWORD PTR [_2il0floatpacket.4]          ;126.70
        movss     xmm1, DWORD PTR [148+esp]                     ;126.8
        mulss     xmm1, xmm0                                    ;126.8
        movss     DWORD PTR [148+esp], xmm1                     ;126.8
        movss     DWORD PTR [472+esp], xmm1                     ;126.8
        push      eax                                           ;126.8
        push      OFFSET FLAT: __STRLITPACK_120.0.1             ;126.8
        push      esi                                           ;126.8
        call      _for_write_seq_fmt_xmit                       ;126.8
                                ; LOE ebx esi edi
.B1.175:                        ; Preds .B1.79                  ; Infreq
        add       esp, 76                                       ;126.8
                                ; LOE ebx esi edi
.B1.80:                         ; Preds .B1.175                 ; Infreq
        movss     xmm0, DWORD PTR [68+esp]                      ;127.38
        add       edi, 5                                        ;128.6
        movss     xmm1, DWORD PTR [84+esp]                      ;127.38
        cmp       edi, DWORD PTR [60+esp]                       ;128.6
        maxss     xmm1, xmm0                                    ;127.38
        movss     DWORD PTR [68+esp], xmm1                      ;127.38
        jg        .B1.81        ; Prob 18%                      ;128.6
                                ; LOE ebx esi edi
.B1.193:                        ; Preds .B1.80                  ; Infreq
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC+32] ;57.8
        jmp       .B1.76        ; Prob 100%                     ;57.8
                                ; LOE eax ebx esi edi
.B1.81:                         ; Preds .B1.80                  ; Infreq
        mov       esi, DWORD PTR [188+esp]                      ;
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC] ;129.9
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC+32] ;129.9
                                ; LOE eax ecx ebx esi
.B1.82:                         ; Preds .B1.74 .B1.81           ; Infreq
        shl       eax, 5                                        ;129.9
        add       esi, ecx                                      ;129.9
        neg       eax                                           ;129.9
        add       esi, eax                                      ;129.9
        lea       eax, DWORD PTR [180+esp]                      ;130.14
        cvtsi2ss  xmm0, DWORD PTR [12+esi]                      ;129.9
        movss     DWORD PTR [156+esp], xmm0                     ;129.9
        push      eax                                           ;130.14
        lea       edx, DWORD PTR [160+esp]                      ;130.14
        push      edx                                           ;130.14
        lea       ecx, DWORD PTR [180+esp]                      ;130.14
        push      ecx                                           ;130.14
        lea       esi, DWORD PTR [176+esp]                      ;130.14
        push      esi                                           ;130.14
        lea       edi, DWORD PTR [244+esp]                      ;130.14
        push      edi                                           ;130.14
        lea       eax, DWORD PTR [256+esp]                      ;130.14
        push      eax                                           ;130.14
        call      _DYNUST_AMS_MODULE_mp_VEHSPEED_UPDATE         ;130.14
                                ; LOE ebx
.B1.83:                         ; Preds .B1.82                  ; Infreq
        movss     xmm0, DWORD PTR [180+esp]                     ;131.9
        lea       edx, DWORD PTR [96+esp]                       ;131.9
        mov       eax, DWORD PTR [180+esp]                      ;131.9
        mov       DWORD PTR [536+esp], 0                        ;131.9
        movss     DWORD PTR [40+esp], xmm0                      ;131.9
        mov       DWORD PTR [96+esp], eax                       ;131.9
        push      32                                            ;131.9
        push      OFFSET FLAT: READ_TRAFFIC_FLOW_MODELS$format_pack.0.1+184 ;131.9
        push      edx                                           ;131.9
        push      OFFSET FLAT: __STRLITPACK_121.0.1             ;131.9
        push      -2088435968                                   ;131.9
        push      5545                                          ;131.9
        lea       ecx, DWORD PTR [560+esp]                      ;131.9
        push      ecx                                           ;131.9
        call      _for_write_seq_fmt                            ;131.9
                                ; LOE ebx
.B1.84:                         ; Preds .B1.83                  ; Infreq
        movss     xmm0, DWORD PTR [232+esp]                     ;131.9
        lea       eax, DWORD PTR [132+esp]                      ;131.9
        movss     xmm1, DWORD PTR [_2il0floatpacket.4]          ;131.9
        mulss     xmm1, xmm0                                    ;131.9
        movss     DWORD PTR [132+esp], xmm1                     ;131.9
        push      eax                                           ;131.9
        push      OFFSET FLAT: __STRLITPACK_122.0.1             ;131.9
        lea       edx, DWORD PTR [572+esp]                      ;131.9
        push      edx                                           ;131.9
        movss     DWORD PTR [316+esp], xmm0                     ;131.9
        call      _for_write_seq_fmt_xmit                       ;131.9
                                ; LOE ebx
.B1.85:                         ; Preds .B1.84                  ; Infreq
        movss     xmm1, DWORD PTR [80+esp]                      ;131.71
        lea       eax, DWORD PTR [152+esp]                      ;131.9
        mulss     xmm1, DWORD PTR [_2il0floatpacket.4]          ;131.71
        movss     xmm0, DWORD PTR [316+esp]                     ;
        mulss     xmm0, xmm1                                    ;131.9
        movss     DWORD PTR [152+esp], xmm0                     ;131.9
        push      eax                                           ;131.9
        push      OFFSET FLAT: __STRLITPACK_123.0.1             ;131.9
        lea       edx, DWORD PTR [584+esp]                      ;131.9
        push      edx                                           ;131.9
        movss     DWORD PTR [328+esp], xmm0                     ;131.9
        call      _for_write_seq_fmt_xmit                       ;131.9
                                ; LOE ebx
.B1.176:                        ; Preds .B1.85                  ; Infreq
        movss     xmm0, DWORD PTR [328+esp]                     ;
        add       esp, 76                                       ;131.9
                                ; LOE ebx xmm0
.B1.86:                         ; Preds .B1.176                 ; Infreq
        maxss     xmm0, DWORD PTR [68+esp]                      ;132.36
        jmp       .B1.37        ; Prob 100%                     ;132.36
                                ; LOE ebx xmm0
.B1.87:                         ; Preds .B1.61                  ; Infreq
        mov       eax, DWORD PTR [276+esp]                      ;103.6
        shl       eax, 5                                        ;103.6
        sub       edx, eax                                      ;103.6
        add       edx, esi                                      ;103.6
        mov       DWORD PTR [104+esp], edx                      ;103.6
        lea       edx, DWORD PTR [104+esp]                      ;103.6
        push      32                                            ;103.6
        push      edx                                           ;103.6
        push      OFFSET FLAT: __STRLITPACK_100.0.1             ;103.6
        push      -2088435968                                   ;103.6
        push      55                                            ;103.6
        lea       ecx, DWORD PTR [532+esp]                      ;103.6
        push      ecx                                           ;103.6
        call      _for_read_seq_lis                             ;103.6
                                ; LOE ebx esi
.B1.88:                         ; Preds .B1.87                  ; Infreq
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC+32] ;103.6
        lea       edi, DWORD PTR [144+esp]                      ;103.6
        shl       eax, 5                                        ;103.6
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC] ;103.6
        sub       edx, eax                                      ;103.6
        lea       ecx, DWORD PTR [4+edx+esi]                    ;103.6
        mov       DWORD PTR [144+esp], ecx                      ;103.6
        push      edi                                           ;103.6
        push      OFFSET FLAT: __STRLITPACK_101.0.1             ;103.6
        lea       eax, DWORD PTR [544+esp]                      ;103.6
        push      eax                                           ;103.6
        call      _for_read_seq_lis_xmit                        ;103.6
                                ; LOE ebx esi
.B1.89:                         ; Preds .B1.88                  ; Infreq
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC+32] ;103.6
        lea       edi, DWORD PTR [172+esp]                      ;103.6
        shl       eax, 5                                        ;103.6
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC] ;103.6
        sub       edx, eax                                      ;103.6
        lea       ecx, DWORD PTR [8+edx+esi]                    ;103.6
        mov       DWORD PTR [172+esp], ecx                      ;103.6
        push      edi                                           ;103.6
        push      OFFSET FLAT: __STRLITPACK_102.0.1             ;103.6
        lea       eax, DWORD PTR [556+esp]                      ;103.6
        push      eax                                           ;103.6
        call      _for_read_seq_lis_xmit                        ;103.6
                                ; LOE ebx esi
.B1.90:                         ; Preds .B1.89                  ; Infreq
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC+32] ;103.6
        lea       edi, DWORD PTR [200+esp]                      ;103.6
        shl       eax, 5                                        ;103.6
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC] ;103.6
        sub       edx, eax                                      ;103.6
        lea       ecx, DWORD PTR [12+edx+esi]                   ;103.6
        mov       DWORD PTR [200+esp], ecx                      ;103.6
        push      edi                                           ;103.6
        push      OFFSET FLAT: __STRLITPACK_103.0.1             ;103.6
        lea       eax, DWORD PTR [568+esp]                      ;103.6
        push      eax                                           ;103.6
        call      _for_read_seq_lis_xmit                        ;103.6
                                ; LOE ebx esi
.B1.91:                         ; Preds .B1.90                  ; Infreq
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC+32] ;103.6
        lea       edi, DWORD PTR [228+esp]                      ;103.6
        shl       eax, 5                                        ;103.6
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC] ;103.6
        sub       edx, eax                                      ;103.6
        lea       ecx, DWORD PTR [16+edx+esi]                   ;103.6
        mov       DWORD PTR [228+esp], ecx                      ;103.6
        push      edi                                           ;103.6
        push      OFFSET FLAT: __STRLITPACK_104.0.1             ;103.6
        lea       eax, DWORD PTR [580+esp]                      ;103.6
        push      eax                                           ;103.6
        call      _for_read_seq_lis_xmit                        ;103.6
                                ; LOE ebx esi
.B1.92:                         ; Preds .B1.91                  ; Infreq
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC+32] ;103.6
        lea       edi, DWORD PTR [248+esp]                      ;103.6
        shl       eax, 5                                        ;103.6
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC] ;103.6
        sub       edx, eax                                      ;103.6
        lea       ecx, DWORD PTR [20+edx+esi]                   ;103.6
        mov       DWORD PTR [248+esp], ecx                      ;103.6
        push      edi                                           ;103.6
        push      OFFSET FLAT: __STRLITPACK_105.0.1             ;103.6
        lea       eax, DWORD PTR [592+esp]                      ;103.6
        push      eax                                           ;103.6
        call      _for_read_seq_lis_xmit                        ;103.6
                                ; LOE ebx esi
.B1.177:                        ; Preds .B1.92                  ; Infreq
        add       esp, 84                                       ;103.6
                                ; LOE ebx esi
.B1.93:                         ; Preds .B1.177                 ; Infreq
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC] ;104.9
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC+32] ;104.9
        mov       edx, eax                                      ;104.9
        shl       edx, 5                                        ;104.9
        pxor      xmm0, xmm0                                    ;
        movss     DWORD PTR [100+esp], xmm0                     ;
        lea       edi, DWORD PTR [esi+ecx]                      ;104.9
        sub       edi, edx                                      ;104.9
        mov       edx, DWORD PTR [12+edi]                       ;104.9
        test      edx, edx                                      ;104.9
        jle       .B1.101       ; Prob 2%                       ;104.9
                                ; LOE eax edx ecx ebx esi
.B1.94:                         ; Preds .B1.93                  ; Infreq
        mov       DWORD PTR [92+esp], edx                       ;
        mov       edi, 1                                        ;
        mov       DWORD PTR [188+esp], esi                      ;
        lea       esi, DWORD PTR [512+esp]                      ;
                                ; LOE eax ebx esi edi
.B1.95:                         ; Preds .B1.192 .B1.94          ; Infreq
        shl       eax, 5                                        ;106.11
        mov       ecx, DWORD PTR [188+esp]                      ;106.11
        neg       eax                                           ;106.11
        add       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC] ;106.11
        cvtsi2ss  xmm0, edi                                     ;108.11
        mov       edx, DWORD PTR [24+eax+ecx]                   ;106.11
        mov       eax, DWORD PTR [28+eax+ecx]                   ;107.11
        mov       DWORD PTR [172+esp], eax                      ;107.11
        lea       eax, DWORD PTR [180+esp]                      ;109.13
        mov       DWORD PTR [228+esp], ebx                      ;105.11
        mov       DWORD PTR [164+esp], edx                      ;106.11
        movss     DWORD PTR [156+esp], xmm0                     ;108.11
        push      eax                                           ;109.13
        lea       edx, DWORD PTR [160+esp]                      ;109.13
        push      edx                                           ;109.13
        lea       ecx, DWORD PTR [180+esp]                      ;109.13
        push      ecx                                           ;109.13
        lea       eax, DWORD PTR [176+esp]                      ;109.13
        push      eax                                           ;109.13
        lea       edx, DWORD PTR [244+esp]                      ;109.13
        push      edx                                           ;109.13
        lea       ecx, DWORD PTR [256+esp]                      ;109.13
        push      ecx                                           ;109.13
        call      _DYNUST_AMS_MODULE_mp_VEHSPEED_UPDATE         ;109.13
                                ; LOE ebx esi edi
.B1.96:                         ; Preds .B1.95                  ; Infreq
        movss     xmm0, DWORD PTR [180+esp]                     ;110.8
        mov       eax, DWORD PTR [180+esp]                      ;110.8
        mov       DWORD PTR [536+esp], 0                        ;110.8
        movss     DWORD PTR [132+esp], xmm0                     ;110.8
        mov       DWORD PTR [496+esp], eax                      ;110.8
        push      32                                            ;110.8
        push      OFFSET FLAT: READ_TRAFFIC_FLOW_MODELS$format_pack.0.1+124 ;110.8
        lea       edx, DWORD PTR [504+esp]                      ;110.8
        push      edx                                           ;110.8
        push      OFFSET FLAT: __STRLITPACK_106.0.1             ;110.8
        push      -2088435968                                   ;110.8
        push      5545                                          ;110.8
        push      esi                                           ;110.8
        call      _for_write_seq_fmt                            ;110.8
                                ; LOE ebx esi edi
.B1.97:                         ; Preds .B1.96                  ; Infreq
        movss     xmm1, DWORD PTR [232+esp]                     ;110.8
        lea       eax, DWORD PTR [540+esp]                      ;110.8
        movss     xmm0, DWORD PTR [_2il0floatpacket.4]          ;110.8
        mulss     xmm0, xmm1                                    ;110.8
        movss     DWORD PTR [168+esp], xmm1                     ;110.8
        movss     DWORD PTR [540+esp], xmm0                     ;110.8
        push      eax                                           ;110.8
        push      OFFSET FLAT: __STRLITPACK_107.0.1             ;110.8
        push      esi                                           ;110.8
        call      _for_write_seq_fmt_xmit                       ;110.8
                                ; LOE ebx esi edi
.B1.98:                         ; Preds .B1.97                  ; Infreq
        movss     xmm0, DWORD PTR [172+esp]                     ;110.70
        lea       eax, DWORD PTR [560+esp]                      ;110.8
        mulss     xmm0, DWORD PTR [_2il0floatpacket.4]          ;110.70
        movss     xmm1, DWORD PTR [180+esp]                     ;110.8
        mulss     xmm1, xmm0                                    ;110.8
        movss     DWORD PTR [180+esp], xmm1                     ;110.8
        movss     DWORD PTR [560+esp], xmm1                     ;110.8
        push      eax                                           ;110.8
        push      OFFSET FLAT: __STRLITPACK_108.0.1             ;110.8
        push      esi                                           ;110.8
        call      _for_write_seq_fmt_xmit                       ;110.8
                                ; LOE ebx esi edi
.B1.178:                        ; Preds .B1.98                  ; Infreq
        add       esp, 76                                       ;110.8
                                ; LOE ebx esi edi
.B1.99:                         ; Preds .B1.178                 ; Infreq
        movss     xmm0, DWORD PTR [100+esp]                     ;111.38
        add       edi, 5                                        ;112.6
        movss     xmm1, DWORD PTR [116+esp]                     ;111.38
        cmp       edi, DWORD PTR [92+esp]                       ;112.6
        maxss     xmm1, xmm0                                    ;111.38
        movss     DWORD PTR [100+esp], xmm1                     ;111.38
        jg        .B1.100       ; Prob 18%                      ;112.6
                                ; LOE ebx esi edi
.B1.192:                        ; Preds .B1.99                  ; Infreq
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC+32] ;57.8
        jmp       .B1.95        ; Prob 100%                     ;57.8
                                ; LOE eax ebx esi edi
.B1.100:                        ; Preds .B1.99                  ; Infreq
        mov       esi, DWORD PTR [188+esp]                      ;
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC] ;113.9
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC+32] ;113.9
                                ; LOE eax ecx ebx esi
.B1.101:                        ; Preds .B1.93 .B1.100          ; Infreq
        shl       eax, 5                                        ;113.9
        add       esi, ecx                                      ;113.9
        neg       eax                                           ;113.9
        add       esi, eax                                      ;113.9
        lea       eax, DWORD PTR [180+esp]                      ;114.14
        cvtsi2ss  xmm0, DWORD PTR [12+esi]                      ;113.9
        movss     DWORD PTR [156+esp], xmm0                     ;113.9
        push      eax                                           ;114.14
        lea       edx, DWORD PTR [160+esp]                      ;114.14
        push      edx                                           ;114.14
        lea       ecx, DWORD PTR [180+esp]                      ;114.14
        push      ecx                                           ;114.14
        lea       esi, DWORD PTR [176+esp]                      ;114.14
        push      esi                                           ;114.14
        lea       edi, DWORD PTR [244+esp]                      ;114.14
        push      edi                                           ;114.14
        lea       eax, DWORD PTR [256+esp]                      ;114.14
        push      eax                                           ;114.14
        call      _DYNUST_AMS_MODULE_mp_VEHSPEED_UPDATE         ;114.14
                                ; LOE ebx
.B1.102:                        ; Preds .B1.101                 ; Infreq
        movss     xmm0, DWORD PTR [180+esp]                     ;115.9
        lea       edx, DWORD PTR [224+esp]                      ;115.9
        mov       eax, DWORD PTR [180+esp]                      ;115.9
        mov       DWORD PTR [536+esp], 0                        ;115.9
        movss     DWORD PTR [60+esp], xmm0                      ;115.9
        mov       DWORD PTR [224+esp], eax                      ;115.9
        push      32                                            ;115.9
        push      OFFSET FLAT: READ_TRAFFIC_FLOW_MODELS$format_pack.0.1+144 ;115.9
        push      edx                                           ;115.9
        push      OFFSET FLAT: __STRLITPACK_109.0.1             ;115.9
        push      -2088435968                                   ;115.9
        push      5545                                          ;115.9
        lea       ecx, DWORD PTR [560+esp]                      ;115.9
        push      ecx                                           ;115.9
        call      _for_write_seq_fmt                            ;115.9
                                ; LOE ebx
.B1.103:                        ; Preds .B1.102                 ; Infreq
        movss     xmm0, DWORD PTR [232+esp]                     ;115.9
        lea       eax, DWORD PTR [268+esp]                      ;115.9
        movss     xmm1, DWORD PTR [_2il0floatpacket.4]          ;115.9
        mulss     xmm1, xmm0                                    ;115.9
        movss     DWORD PTR [268+esp], xmm1                     ;115.9
        push      eax                                           ;115.9
        push      OFFSET FLAT: __STRLITPACK_110.0.1             ;115.9
        lea       edx, DWORD PTR [572+esp]                      ;115.9
        push      edx                                           ;115.9
        movss     DWORD PTR [316+esp], xmm0                     ;115.9
        call      _for_write_seq_fmt_xmit                       ;115.9
                                ; LOE ebx
.B1.104:                        ; Preds .B1.103                 ; Infreq
        movss     xmm1, DWORD PTR [100+esp]                     ;115.71
        lea       eax, DWORD PTR [288+esp]                      ;115.9
        mulss     xmm1, DWORD PTR [_2il0floatpacket.4]          ;115.71
        movss     xmm0, DWORD PTR [316+esp]                     ;
        mulss     xmm0, xmm1                                    ;115.9
        movss     DWORD PTR [288+esp], xmm0                     ;115.9
        push      eax                                           ;115.9
        push      OFFSET FLAT: __STRLITPACK_111.0.1             ;115.9
        lea       edx, DWORD PTR [584+esp]                      ;115.9
        push      edx                                           ;115.9
        movss     DWORD PTR [328+esp], xmm0                     ;115.9
        call      _for_write_seq_fmt_xmit                       ;115.9
                                ; LOE ebx
.B1.179:                        ; Preds .B1.104                 ; Infreq
        movss     xmm0, DWORD PTR [328+esp]                     ;
        add       esp, 76                                       ;115.9
                                ; LOE ebx xmm0
.B1.105:                        ; Preds .B1.179                 ; Infreq
        maxss     xmm0, DWORD PTR [100+esp]                     ;116.36
        jmp       .B1.37        ; Prob 100%                     ;116.36
                                ; LOE ebx xmm0
.B1.106:                        ; Preds .B1.34                  ; Infreq
        mov       DWORD PTR [512+esp], 0                        ;63.6
        mov       eax, DWORD PTR [24+ecx+eax]                   ;62.10
        cmp       eax, 1                                        ;62.27
        je        .B1.131       ; Prob 16%                      ;62.27
                                ; LOE eax edx ebx esi dl dh
.B1.107:                        ; Preds .B1.106                 ; Infreq
        cmp       eax, 2                                        ;78.28
        je        .B1.113       ; Prob 16%                      ;78.28
                                ; LOE ebx esi
.B1.108:                        ; Preds .B1.107                 ; Infreq
        mov       eax, 32                                       ;96.9
        lea       edx, DWORD PTR [352+esp]                      ;96.9
        mov       DWORD PTR [352+esp], eax                      ;96.9
        mov       DWORD PTR [356+esp], OFFSET FLAT: __STRLITPACK_29 ;96.9
        push      eax                                           ;96.9
        push      edx                                           ;96.9
        push      OFFSET FLAT: __STRLITPACK_96.0.1              ;96.9
        push      -2088435968                                   ;96.9
        push      911                                           ;96.9
        lea       ecx, DWORD PTR [532+esp]                      ;96.9
        push      ecx                                           ;96.9
        call      _for_write_seq_lis                            ;96.9
                                ; LOE ebx
.B1.109:                        ; Preds .B1.108                 ; Infreq
        mov       DWORD PTR [536+esp], 0                        ;97.6
        lea       eax, DWORD PTR [384+esp]                      ;97.6
        mov       DWORD PTR [384+esp], 39                       ;97.6
        mov       DWORD PTR [388+esp], OFFSET FLAT: __STRLITPACK_27 ;97.6
        push      32                                            ;97.6
        push      eax                                           ;97.6
        push      OFFSET FLAT: __STRLITPACK_97.0.1              ;97.6
        push      -2088435968                                   ;97.6
        push      911                                           ;97.6
        lea       edx, DWORD PTR [556+esp]                      ;97.6
        push      edx                                           ;97.6
        call      _for_write_seq_lis                            ;97.6
                                ; LOE ebx
.B1.110:                        ; Preds .B1.109                 ; Infreq
        mov       DWORD PTR [560+esp], 0                        ;98.6
        lea       eax, DWORD PTR [416+esp]                      ;98.6
        mov       DWORD PTR [416+esp], 36                       ;98.6
        mov       DWORD PTR [420+esp], OFFSET FLAT: __STRLITPACK_25 ;98.6
        push      32                                            ;98.6
        push      eax                                           ;98.6
        push      OFFSET FLAT: __STRLITPACK_98.0.1              ;98.6
        push      -2088435968                                   ;98.6
        push      911                                           ;98.6
        lea       edx, DWORD PTR [580+esp]                      ;98.6
        push      edx                                           ;98.6
        call      _for_write_seq_lis                            ;98.6
                                ; LOE ebx
.B1.111:                        ; Preds .B1.110                 ; Infreq
        push      32                                            ;99.6
        xor       eax, eax                                      ;99.6
        push      eax                                           ;99.6
        push      eax                                           ;99.6
        push      -2088435968                                   ;99.6
        push      eax                                           ;99.6
        push      OFFSET FLAT: __STRLITPACK_99                  ;99.6
        call      _for_stop_core                                ;99.6
                                ; LOE ebx
.B1.180:                        ; Preds .B1.111                 ; Infreq
        add       esp, 96                                       ;99.6
        jmp       .B1.36        ; Prob 100%                     ;99.6
                                ; LOE ebx
.B1.113:                        ; Preds .B1.107                 ; Infreq
        lea       eax, DWORD PTR [20+esp]                       ;79.6
        mov       DWORD PTR [96+esp], eax                       ;79.6
        lea       edx, DWORD PTR [96+esp]                       ;79.6
        push      32                                            ;79.6
        push      edx                                           ;79.6
        push      OFFSET FLAT: __STRLITPACK_85.0.1              ;79.6
        push      -2088435968                                   ;79.6
        push      55                                            ;79.6
        lea       ecx, DWORD PTR [532+esp]                      ;79.6
        push      ecx                                           ;79.6
        call      _for_read_seq_lis                             ;79.6
                                ; LOE ebx esi
.B1.114:                        ; Preds .B1.113                 ; Infreq
        lea       edx, DWORD PTR [136+esp]                      ;79.6
        lea       eax, DWORD PTR [52+esp]                       ;79.6
        mov       DWORD PTR [136+esp], eax                      ;79.6
        push      edx                                           ;79.6
        push      OFFSET FLAT: __STRLITPACK_86.0.1              ;79.6
        lea       ecx, DWORD PTR [544+esp]                      ;79.6
        push      ecx                                           ;79.6
        call      _for_read_seq_lis_xmit                        ;79.6
                                ; LOE ebx esi
.B1.115:                        ; Preds .B1.114                 ; Infreq
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC+32] ;79.6
        lea       edi, DWORD PTR [164+esp]                      ;79.6
        shl       eax, 5                                        ;79.6
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC] ;79.6
        sub       edx, eax                                      ;79.6
        lea       ecx, DWORD PTR [8+edx+esi]                    ;79.6
        mov       DWORD PTR [164+esp], ecx                      ;79.6
        push      edi                                           ;79.6
        push      OFFSET FLAT: __STRLITPACK_87.0.1              ;79.6
        lea       eax, DWORD PTR [556+esp]                      ;79.6
        push      eax                                           ;79.6
        call      _for_read_seq_lis_xmit                        ;79.6
                                ; LOE ebx esi
.B1.116:                        ; Preds .B1.115                 ; Infreq
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC+32] ;79.6
        lea       edi, DWORD PTR [192+esp]                      ;79.6
        shl       eax, 5                                        ;79.6
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC] ;79.6
        sub       edx, eax                                      ;79.6
        lea       ecx, DWORD PTR [12+edx+esi]                   ;79.6
        mov       DWORD PTR [192+esp], ecx                      ;79.6
        push      edi                                           ;79.6
        push      OFFSET FLAT: __STRLITPACK_88.0.1              ;79.6
        lea       eax, DWORD PTR [568+esp]                      ;79.6
        push      eax                                           ;79.6
        call      _for_read_seq_lis_xmit                        ;79.6
                                ; LOE ebx esi
.B1.117:                        ; Preds .B1.116                 ; Infreq
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC+32] ;79.6
        lea       edi, DWORD PTR [220+esp]                      ;79.6
        shl       eax, 5                                        ;79.6
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC] ;79.6
        sub       edx, eax                                      ;79.6
        lea       ecx, DWORD PTR [16+edx+esi]                   ;79.6
        mov       DWORD PTR [220+esp], ecx                      ;79.6
        push      edi                                           ;79.6
        push      OFFSET FLAT: __STRLITPACK_89.0.1              ;79.6
        lea       eax, DWORD PTR [580+esp]                      ;79.6
        push      eax                                           ;79.6
        call      _for_read_seq_lis_xmit                        ;79.6
                                ; LOE ebx esi
.B1.181:                        ; Preds .B1.117                 ; Infreq
        add       esp, 72                                       ;79.6
                                ; LOE ebx esi
.B1.118:                        ; Preds .B1.181                 ; Infreq
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC] ;80.9
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC+32] ;80.9
        mov       edx, eax                                      ;80.9
        shl       edx, 5                                        ;80.9
        pxor      xmm0, xmm0                                    ;
        movss     DWORD PTR [132+esp], xmm0                     ;
        lea       edi, DWORD PTR [esi+ecx]                      ;80.9
        sub       edi, edx                                      ;80.9
        mov       edx, DWORD PTR [12+edi]                       ;82.9
        test      edx, edx                                      ;82.9
        mov       DWORD PTR [edi], 0                            ;80.9
        jle       .B1.126       ; Prob 2%                       ;82.9
                                ; LOE eax edx ecx ebx esi
.B1.119:                        ; Preds .B1.118                 ; Infreq
        mov       DWORD PTR [124+esp], edx                      ;
        mov       edi, 1                                        ;
        mov       DWORD PTR [188+esp], esi                      ;
        lea       esi, DWORD PTR [512+esp]                      ;
                                ; LOE eax ebx esi edi
.B1.120:                        ; Preds .B1.191 .B1.119         ; Infreq
        shl       eax, 5                                        ;84.11
        mov       ecx, DWORD PTR [188+esp]                      ;84.11
        neg       eax                                           ;84.11
        add       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC] ;84.11
        cvtsi2ss  xmm0, edi                                     ;86.11
        mov       edx, DWORD PTR [24+eax+ecx]                   ;84.11
        mov       eax, DWORD PTR [28+eax+ecx]                   ;85.11
        mov       DWORD PTR [172+esp], eax                      ;85.11
        lea       eax, DWORD PTR [180+esp]                      ;87.13
        mov       DWORD PTR [228+esp], ebx                      ;83.11
        mov       DWORD PTR [164+esp], edx                      ;84.11
        movss     DWORD PTR [156+esp], xmm0                     ;86.11
        push      eax                                           ;87.13
        lea       edx, DWORD PTR [160+esp]                      ;87.13
        push      edx                                           ;87.13
        lea       ecx, DWORD PTR [180+esp]                      ;87.13
        push      ecx                                           ;87.13
        lea       eax, DWORD PTR [176+esp]                      ;87.13
        push      eax                                           ;87.13
        lea       edx, DWORD PTR [244+esp]                      ;87.13
        push      edx                                           ;87.13
        lea       ecx, DWORD PTR [256+esp]                      ;87.13
        push      ecx                                           ;87.13
        call      _DYNUST_AMS_MODULE_mp_VEHSPEED_UPDATE         ;87.13
                                ; LOE ebx esi edi
.B1.121:                        ; Preds .B1.120                 ; Infreq
        movss     xmm0, DWORD PTR [180+esp]                     ;88.8
        mov       eax, DWORD PTR [180+esp]                      ;88.8
        mov       DWORD PTR [536+esp], 0                        ;88.8
        movss     DWORD PTR [164+esp], xmm0                     ;88.8
        mov       DWORD PTR [480+esp], eax                      ;88.8
        push      32                                            ;88.8
        push      OFFSET FLAT: READ_TRAFFIC_FLOW_MODELS$format_pack.0.1+84 ;88.8
        lea       edx, DWORD PTR [488+esp]                      ;88.8
        push      edx                                           ;88.8
        push      OFFSET FLAT: __STRLITPACK_90.0.1              ;88.8
        push      -2088435968                                   ;88.8
        push      5545                                          ;88.8
        push      esi                                           ;88.8
        call      _for_write_seq_fmt                            ;88.8
                                ; LOE ebx esi edi
.B1.122:                        ; Preds .B1.121                 ; Infreq
        movss     xmm1, DWORD PTR [232+esp]                     ;88.8
        lea       eax, DWORD PTR [516+esp]                      ;88.8
        movss     xmm0, DWORD PTR [_2il0floatpacket.4]          ;88.8
        mulss     xmm0, xmm1                                    ;88.8
        movss     DWORD PTR [200+esp], xmm1                     ;88.8
        movss     DWORD PTR [516+esp], xmm0                     ;88.8
        push      eax                                           ;88.8
        push      OFFSET FLAT: __STRLITPACK_91.0.1              ;88.8
        push      esi                                           ;88.8
        call      _for_write_seq_fmt_xmit                       ;88.8
                                ; LOE ebx esi edi
.B1.123:                        ; Preds .B1.122                 ; Infreq
        movss     xmm0, DWORD PTR [204+esp]                     ;88.70
        lea       eax, DWORD PTR [544+esp]                      ;88.8
        mulss     xmm0, DWORD PTR [_2il0floatpacket.4]          ;88.70
        movss     xmm1, DWORD PTR [212+esp]                     ;88.8
        mulss     xmm1, xmm0                                    ;88.8
        movss     DWORD PTR [212+esp], xmm1                     ;88.8
        movss     DWORD PTR [544+esp], xmm1                     ;88.8
        push      eax                                           ;88.8
        push      OFFSET FLAT: __STRLITPACK_92.0.1              ;88.8
        push      esi                                           ;88.8
        call      _for_write_seq_fmt_xmit                       ;88.8
                                ; LOE ebx esi edi
.B1.182:                        ; Preds .B1.123                 ; Infreq
        add       esp, 76                                       ;88.8
                                ; LOE ebx esi edi
.B1.124:                        ; Preds .B1.182                 ; Infreq
        movss     xmm0, DWORD PTR [132+esp]                     ;89.38
        add       edi, 5                                        ;90.6
        movss     xmm1, DWORD PTR [148+esp]                     ;89.38
        cmp       edi, DWORD PTR [124+esp]                      ;90.6
        maxss     xmm1, xmm0                                    ;89.38
        movss     DWORD PTR [132+esp], xmm1                     ;89.38
        jg        .B1.125       ; Prob 18%                      ;90.6
                                ; LOE ebx esi edi
.B1.191:                        ; Preds .B1.124                 ; Infreq
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC+32] ;57.8
        jmp       .B1.120       ; Prob 100%                     ;57.8
                                ; LOE eax ebx esi edi
.B1.125:                        ; Preds .B1.124                 ; Infreq
        mov       esi, DWORD PTR [188+esp]                      ;
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC] ;91.9
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC+32] ;91.9
                                ; LOE eax ecx ebx esi
.B1.126:                        ; Preds .B1.118 .B1.125         ; Infreq
        shl       eax, 5                                        ;91.9
        add       esi, ecx                                      ;91.9
        neg       eax                                           ;91.9
        add       esi, eax                                      ;91.9
        lea       eax, DWORD PTR [180+esp]                      ;92.14
        cvtsi2ss  xmm0, DWORD PTR [12+esi]                      ;91.9
        movss     DWORD PTR [156+esp], xmm0                     ;91.9
        push      eax                                           ;92.14
        lea       edx, DWORD PTR [160+esp]                      ;92.14
        push      edx                                           ;92.14
        lea       ecx, DWORD PTR [180+esp]                      ;92.14
        push      ecx                                           ;92.14
        lea       esi, DWORD PTR [176+esp]                      ;92.14
        push      esi                                           ;92.14
        lea       edi, DWORD PTR [244+esp]                      ;92.14
        push      edi                                           ;92.14
        lea       eax, DWORD PTR [256+esp]                      ;92.14
        push      eax                                           ;92.14
        call      _DYNUST_AMS_MODULE_mp_VEHSPEED_UPDATE         ;92.14
                                ; LOE ebx
.B1.127:                        ; Preds .B1.126                 ; Infreq
        movss     xmm0, DWORD PTR [180+esp]                     ;93.9
        lea       edx, DWORD PTR [208+esp]                      ;93.9
        mov       eax, DWORD PTR [180+esp]                      ;93.9
        mov       DWORD PTR [536+esp], 0                        ;93.9
        movss     DWORD PTR [68+esp], xmm0                      ;93.9
        mov       DWORD PTR [208+esp], eax                      ;93.9
        push      32                                            ;93.9
        push      OFFSET FLAT: READ_TRAFFIC_FLOW_MODELS$format_pack.0.1+104 ;93.9
        push      edx                                           ;93.9
        push      OFFSET FLAT: __STRLITPACK_93.0.1              ;93.9
        push      -2088435968                                   ;93.9
        push      5545                                          ;93.9
        lea       ecx, DWORD PTR [560+esp]                      ;93.9
        push      ecx                                           ;93.9
        call      _for_write_seq_fmt                            ;93.9
                                ; LOE ebx
.B1.128:                        ; Preds .B1.127                 ; Infreq
        movss     xmm0, DWORD PTR [232+esp]                     ;93.9
        lea       eax, DWORD PTR [244+esp]                      ;93.9
        movss     xmm1, DWORD PTR [_2il0floatpacket.4]          ;93.9
        mulss     xmm1, xmm0                                    ;93.9
        movss     DWORD PTR [244+esp], xmm1                     ;93.9
        push      eax                                           ;93.9
        push      OFFSET FLAT: __STRLITPACK_94.0.1              ;93.9
        lea       edx, DWORD PTR [572+esp]                      ;93.9
        push      edx                                           ;93.9
        movss     DWORD PTR [316+esp], xmm0                     ;93.9
        call      _for_write_seq_fmt_xmit                       ;93.9
                                ; LOE ebx
.B1.129:                        ; Preds .B1.128                 ; Infreq
        movss     xmm1, DWORD PTR [108+esp]                     ;93.71
        lea       eax, DWORD PTR [272+esp]                      ;93.9
        mulss     xmm1, DWORD PTR [_2il0floatpacket.4]          ;93.71
        movss     xmm0, DWORD PTR [316+esp]                     ;
        mulss     xmm0, xmm1                                    ;93.9
        movss     DWORD PTR [272+esp], xmm0                     ;93.9
        push      eax                                           ;93.9
        push      OFFSET FLAT: __STRLITPACK_95.0.1              ;93.9
        lea       edx, DWORD PTR [584+esp]                      ;93.9
        push      edx                                           ;93.9
        movss     DWORD PTR [328+esp], xmm0                     ;93.9
        call      _for_write_seq_fmt_xmit                       ;93.9
                                ; LOE ebx
.B1.183:                        ; Preds .B1.129                 ; Infreq
        movss     xmm0, DWORD PTR [328+esp]                     ;
        add       esp, 76                                       ;93.9
                                ; LOE ebx xmm0
.B1.130:                        ; Preds .B1.183                 ; Infreq
        maxss     xmm0, DWORD PTR [132+esp]                     ;94.36
        jmp       .B1.37        ; Prob 100%                     ;94.36
                                ; LOE ebx xmm0
.B1.131:                        ; Preds .B1.106                 ; Infreq
        mov       eax, DWORD PTR [276+esp]                      ;63.6
        shl       eax, 5                                        ;63.6
        sub       edx, eax                                      ;63.6
        add       edx, esi                                      ;63.6
        mov       DWORD PTR [232+esp], edx                      ;63.6
        lea       edx, DWORD PTR [232+esp]                      ;63.6
        push      32                                            ;63.6
        push      edx                                           ;63.6
        push      OFFSET FLAT: __STRLITPACK_74.0.1              ;63.6
        push      -2088435968                                   ;63.6
        push      55                                            ;63.6
        lea       ecx, DWORD PTR [532+esp]                      ;63.6
        push      ecx                                           ;63.6
        call      _for_read_seq_lis                             ;63.6
                                ; LOE ebx esi
.B1.132:                        ; Preds .B1.131                 ; Infreq
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC+32] ;63.6
        lea       edi, DWORD PTR [264+esp]                      ;63.6
        shl       eax, 5                                        ;63.6
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC] ;63.6
        sub       edx, eax                                      ;63.6
        lea       ecx, DWORD PTR [4+edx+esi]                    ;63.6
        mov       DWORD PTR [264+esp], ecx                      ;63.6
        push      edi                                           ;63.6
        push      OFFSET FLAT: __STRLITPACK_75.0.1              ;63.6
        lea       eax, DWORD PTR [544+esp]                      ;63.6
        push      eax                                           ;63.6
        call      _for_read_seq_lis_xmit                        ;63.6
                                ; LOE ebx esi
.B1.133:                        ; Preds .B1.132                 ; Infreq
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC+32] ;63.6
        lea       edi, DWORD PTR [284+esp]                      ;63.6
        shl       eax, 5                                        ;63.6
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC] ;63.6
        sub       edx, eax                                      ;63.6
        lea       ecx, DWORD PTR [8+edx+esi]                    ;63.6
        mov       DWORD PTR [284+esp], ecx                      ;63.6
        push      edi                                           ;63.6
        push      OFFSET FLAT: __STRLITPACK_76.0.1              ;63.6
        lea       eax, DWORD PTR [556+esp]                      ;63.6
        push      eax                                           ;63.6
        call      _for_read_seq_lis_xmit                        ;63.6
                                ; LOE ebx esi
.B1.134:                        ; Preds .B1.133                 ; Infreq
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC+32] ;63.6
        lea       edi, DWORD PTR [304+esp]                      ;63.6
        shl       eax, 5                                        ;63.6
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC] ;63.6
        sub       edx, eax                                      ;63.6
        lea       ecx, DWORD PTR [12+edx+esi]                   ;63.6
        mov       DWORD PTR [304+esp], ecx                      ;63.6
        push      edi                                           ;63.6
        push      OFFSET FLAT: __STRLITPACK_77.0.1              ;63.6
        lea       eax, DWORD PTR [568+esp]                      ;63.6
        push      eax                                           ;63.6
        call      _for_read_seq_lis_xmit                        ;63.6
                                ; LOE ebx esi
.B1.135:                        ; Preds .B1.134                 ; Infreq
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC+32] ;63.6
        lea       edi, DWORD PTR [324+esp]                      ;63.6
        shl       eax, 5                                        ;63.6
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC] ;63.6
        sub       edx, eax                                      ;63.6
        lea       ecx, DWORD PTR [16+edx+esi]                   ;63.6
        mov       DWORD PTR [324+esp], ecx                      ;63.6
        push      edi                                           ;63.6
        push      OFFSET FLAT: __STRLITPACK_78.0.1              ;63.6
        lea       eax, DWORD PTR [580+esp]                      ;63.6
        push      eax                                           ;63.6
        call      _for_read_seq_lis_xmit                        ;63.6
                                ; LOE ebx esi
.B1.184:                        ; Preds .B1.135                 ; Infreq
        add       esp, 72                                       ;63.6
                                ; LOE ebx esi
.B1.136:                        ; Preds .B1.184                 ; Infreq
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC] ;64.9
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC+32] ;64.9
        mov       edx, eax                                      ;64.9
        shl       edx, 5                                        ;64.9
        pxor      xmm0, xmm0                                    ;
        movss     DWORD PTR [204+esp], xmm0                     ;
        lea       edi, DWORD PTR [esi+ecx]                      ;64.9
        sub       edi, edx                                      ;64.9
        mov       edx, DWORD PTR [12+edi]                       ;64.9
        test      edx, edx                                      ;64.9
        jle       .B1.144       ; Prob 2%                       ;64.9
                                ; LOE eax edx ecx ebx esi
.B1.137:                        ; Preds .B1.136                 ; Infreq
        mov       DWORD PTR [196+esp], edx                      ;
        mov       edi, 1                                        ;
        mov       DWORD PTR [188+esp], esi                      ;
        lea       esi, DWORD PTR [512+esp]                      ;
                                ; LOE eax ebx esi edi
.B1.138:                        ; Preds .B1.190 .B1.137         ; Infreq
        shl       eax, 5                                        ;66.11
        mov       ecx, DWORD PTR [188+esp]                      ;66.11
        neg       eax                                           ;66.11
        add       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC] ;66.11
        cvtsi2ss  xmm0, edi                                     ;68.11
        mov       edx, DWORD PTR [24+eax+ecx]                   ;66.11
        mov       eax, DWORD PTR [28+eax+ecx]                   ;67.11
        mov       DWORD PTR [172+esp], eax                      ;67.11
        lea       eax, DWORD PTR [180+esp]                      ;69.13
        mov       DWORD PTR [228+esp], ebx                      ;65.11
        mov       DWORD PTR [164+esp], edx                      ;66.11
        movss     DWORD PTR [156+esp], xmm0                     ;68.11
        push      eax                                           ;69.13
        lea       edx, DWORD PTR [160+esp]                      ;69.13
        push      edx                                           ;69.13
        lea       ecx, DWORD PTR [180+esp]                      ;69.13
        push      ecx                                           ;69.13
        lea       eax, DWORD PTR [176+esp]                      ;69.13
        push      eax                                           ;69.13
        lea       edx, DWORD PTR [244+esp]                      ;69.13
        push      edx                                           ;69.13
        lea       ecx, DWORD PTR [256+esp]                      ;69.13
        push      ecx                                           ;69.13
        call      _DYNUST_AMS_MODULE_mp_VEHSPEED_UPDATE         ;69.13
                                ; LOE ebx esi edi
.B1.139:                        ; Preds .B1.138                 ; Infreq
        movss     xmm0, DWORD PTR [180+esp]                     ;70.8
        mov       eax, DWORD PTR [180+esp]                      ;70.8
        mov       DWORD PTR [536+esp], 0                        ;70.8
        movss     DWORD PTR [236+esp], xmm0                     ;70.8
        mov       DWORD PTR [528+esp], eax                      ;70.8
        push      32                                            ;70.8
        push      OFFSET FLAT: READ_TRAFFIC_FLOW_MODELS$format_pack.0.1+44 ;70.8
        lea       edx, DWORD PTR [536+esp]                      ;70.8
        push      edx                                           ;70.8
        push      OFFSET FLAT: __STRLITPACK_79.0.1              ;70.8
        push      -2088435968                                   ;70.8
        push      5545                                          ;70.8
        push      esi                                           ;70.8
        call      _for_write_seq_fmt                            ;70.8
                                ; LOE ebx esi edi
.B1.140:                        ; Preds .B1.139                 ; Infreq
        movss     xmm1, DWORD PTR [232+esp]                     ;70.8
        lea       eax, DWORD PTR [596+esp]                      ;70.8
        movss     xmm0, DWORD PTR [_2il0floatpacket.4]          ;70.8
        mulss     xmm0, xmm1                                    ;70.8
        movss     DWORD PTR [272+esp], xmm1                     ;70.8
        movss     DWORD PTR [596+esp], xmm0                     ;70.8
        push      eax                                           ;70.8
        push      OFFSET FLAT: __STRLITPACK_80.0.1              ;70.8
        push      esi                                           ;70.8
        call      _for_write_seq_fmt_xmit                       ;70.8
                                ; LOE ebx esi edi
.B1.141:                        ; Preds .B1.140                 ; Infreq
        movss     xmm0, DWORD PTR [276+esp]                     ;70.70
        lea       eax, DWORD PTR [616+esp]                      ;70.8
        mulss     xmm0, DWORD PTR [_2il0floatpacket.4]          ;70.70
        movss     xmm1, DWORD PTR [284+esp]                     ;70.8
        mulss     xmm1, xmm0                                    ;70.8
        movss     DWORD PTR [284+esp], xmm1                     ;70.8
        movss     DWORD PTR [616+esp], xmm1                     ;70.8
        push      eax                                           ;70.8
        push      OFFSET FLAT: __STRLITPACK_81.0.1              ;70.8
        push      esi                                           ;70.8
        call      _for_write_seq_fmt_xmit                       ;70.8
                                ; LOE ebx esi edi
.B1.185:                        ; Preds .B1.141                 ; Infreq
        add       esp, 76                                       ;70.8
                                ; LOE ebx esi edi
.B1.142:                        ; Preds .B1.185                 ; Infreq
        movss     xmm0, DWORD PTR [204+esp]                     ;71.38
        add       edi, 5                                        ;72.6
        movss     xmm1, DWORD PTR [220+esp]                     ;71.38
        cmp       edi, DWORD PTR [196+esp]                      ;72.6
        maxss     xmm1, xmm0                                    ;71.38
        movss     DWORD PTR [204+esp], xmm1                     ;71.38
        jg        .B1.143       ; Prob 18%                      ;72.6
                                ; LOE ebx esi edi
.B1.190:                        ; Preds .B1.142                 ; Infreq
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC+32] ;57.8
        jmp       .B1.138       ; Prob 100%                     ;57.8
                                ; LOE eax ebx esi edi
.B1.143:                        ; Preds .B1.142                 ; Infreq
        mov       esi, DWORD PTR [188+esp]                      ;
        mov       ecx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC] ;73.9
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC+32] ;73.9
                                ; LOE eax ecx ebx esi
.B1.144:                        ; Preds .B1.136 .B1.143         ; Infreq
        shl       eax, 5                                        ;73.9
        add       esi, ecx                                      ;73.9
        neg       eax                                           ;73.9
        add       esi, eax                                      ;73.9
        lea       eax, DWORD PTR [180+esp]                      ;74.14
        cvtsi2ss  xmm0, DWORD PTR [12+esi]                      ;73.9
        movss     DWORD PTR [156+esp], xmm0                     ;73.9
        push      eax                                           ;74.14
        lea       edx, DWORD PTR [160+esp]                      ;74.14
        push      edx                                           ;74.14
        lea       ecx, DWORD PTR [180+esp]                      ;74.14
        push      ecx                                           ;74.14
        lea       esi, DWORD PTR [176+esp]                      ;74.14
        push      esi                                           ;74.14
        lea       edi, DWORD PTR [244+esp]                      ;74.14
        push      edi                                           ;74.14
        lea       eax, DWORD PTR [256+esp]                      ;74.14
        push      eax                                           ;74.14
        call      _DYNUST_AMS_MODULE_mp_VEHSPEED_UPDATE         ;74.14
                                ; LOE ebx
.B1.145:                        ; Preds .B1.144                 ; Infreq
        movss     xmm0, DWORD PTR [180+esp]                     ;75.9
        lea       edx, DWORD PTR [296+esp]                      ;75.9
        mov       eax, DWORD PTR [180+esp]                      ;75.9
        mov       DWORD PTR [536+esp], 0                        ;75.9
        movss     DWORD PTR [76+esp], xmm0                      ;75.9
        mov       DWORD PTR [296+esp], eax                      ;75.9
        push      32                                            ;75.9
        push      OFFSET FLAT: READ_TRAFFIC_FLOW_MODELS$format_pack.0.1+64 ;75.9
        push      edx                                           ;75.9
        push      OFFSET FLAT: __STRLITPACK_82.0.1              ;75.9
        push      -2088435968                                   ;75.9
        push      5545                                          ;75.9
        lea       ecx, DWORD PTR [560+esp]                      ;75.9
        push      ecx                                           ;75.9
        call      _for_write_seq_fmt                            ;75.9
                                ; LOE ebx
.B1.146:                        ; Preds .B1.145                 ; Infreq
        movss     xmm0, DWORD PTR [232+esp]                     ;75.9
        lea       eax, DWORD PTR [332+esp]                      ;75.9
        movss     xmm1, DWORD PTR [_2il0floatpacket.4]          ;75.9
        mulss     xmm1, xmm0                                    ;75.9
        movss     DWORD PTR [332+esp], xmm1                     ;75.9
        push      eax                                           ;75.9
        push      OFFSET FLAT: __STRLITPACK_83.0.1              ;75.9
        lea       edx, DWORD PTR [572+esp]                      ;75.9
        push      edx                                           ;75.9
        movss     DWORD PTR [316+esp], xmm0                     ;75.9
        call      _for_write_seq_fmt_xmit                       ;75.9
                                ; LOE ebx
.B1.147:                        ; Preds .B1.146                 ; Infreq
        movss     xmm1, DWORD PTR [116+esp]                     ;75.71
        lea       eax, DWORD PTR [352+esp]                      ;75.9
        mulss     xmm1, DWORD PTR [_2il0floatpacket.4]          ;75.71
        movss     xmm0, DWORD PTR [316+esp]                     ;
        mulss     xmm0, xmm1                                    ;75.9
        movss     DWORD PTR [352+esp], xmm0                     ;75.9
        push      eax                                           ;75.9
        push      OFFSET FLAT: __STRLITPACK_84.0.1              ;75.9
        lea       edx, DWORD PTR [584+esp]                      ;75.9
        push      edx                                           ;75.9
        movss     DWORD PTR [328+esp], xmm0                     ;75.9
        call      _for_write_seq_fmt_xmit                       ;75.9
                                ; LOE ebx
.B1.186:                        ; Preds .B1.147                 ; Infreq
        movss     xmm0, DWORD PTR [328+esp]                     ;
        add       esp, 76                                       ;75.9
                                ; LOE ebx xmm0
.B1.148:                        ; Preds .B1.186                 ; Infreq
        maxss     xmm0, DWORD PTR [204+esp]                     ;76.36
        jmp       .B1.37        ; Prob 100%                     ;76.36
                                ; LOE ebx xmm0
.B1.149:                        ; Preds .B1.30                  ; Infreq
        mov       DWORD PTR [512+esp], 0                        ;55.8
        lea       eax, DWORD PTR [260+esp]                      ;55.8
        mov       DWORD PTR [560+esp], eax                      ;55.8
        lea       edx, DWORD PTR [560+esp]                      ;55.8
        push      32                                            ;55.8
        push      edx                                           ;55.8
        push      OFFSET FLAT: __STRLITPACK_69.0.1              ;55.8
        push      -2088435968                                   ;55.8
        push      55                                            ;55.8
        lea       ecx, DWORD PTR [532+esp]                      ;55.8
        push      ecx                                           ;55.8
        call      _for_read_seq_lis                             ;55.8
                                ; LOE ebx
.B1.150:                        ; Preds .B1.149                 ; Infreq
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC+32] ;55.8
        mov       esi, ebx                                      ;55.8
        shl       eax, 5                                        ;55.8
        lea       edi, DWORD PTR [592+esp]                      ;55.8
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC] ;55.8
        shl       esi, 5                                        ;55.8
        sub       edx, eax                                      ;55.8
        lea       ecx, DWORD PTR [24+edx+esi]                   ;55.8
        mov       DWORD PTR [592+esp], ecx                      ;55.8
        push      edi                                           ;55.8
        push      OFFSET FLAT: __STRLITPACK_70.0.1              ;55.8
        lea       eax, DWORD PTR [544+esp]                      ;55.8
        push      eax                                           ;55.8
        call      _for_read_seq_lis_xmit                        ;55.8
                                ; LOE ebx esi
.B1.151:                        ; Preds .B1.150                 ; Infreq
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC+32] ;55.8
        lea       edi, DWORD PTR [612+esp]                      ;55.8
        shl       eax, 5                                        ;55.8
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC] ;55.8
        sub       edx, eax                                      ;55.8
        lea       ecx, DWORD PTR [28+edx+esi]                   ;55.8
        mov       DWORD PTR [612+esp], ecx                      ;55.8
        push      edi                                           ;55.8
        push      OFFSET FLAT: __STRLITPACK_71.0.1              ;55.8
        lea       eax, DWORD PTR [556+esp]                      ;55.8
        push      eax                                           ;55.8
        call      _for_read_seq_lis_xmit                        ;55.8
                                ; LOE ebx esi
.B1.187:                        ; Preds .B1.151                 ; Infreq
        add       esp, 48                                       ;55.8
                                ; LOE ebx esi
.B1.152:                        ; Preds .B1.187                 ; Infreq
        mov       eax, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC+32] ;60.6
        mov       edx, DWORD PTR [_DYNUST_NETWORK_MODULE_mp_VKFUNC] ;60.6
        mov       DWORD PTR [276+esp], eax                      ;60.6
        jmp       .B1.34        ; Prob 100%                     ;60.6
                                ; LOE edx ebx esi
.B1.153:                        ; Preds .B1.7                   ; Infreq
        mov       edx, 1                                        ;
        jmp       .B1.15        ; Prob 100%                     ;
                                ; LOE eax edx ecx ebx
.B1.154:                        ; Preds .B1.6                   ; Infreq
        imul      edx, edx, 900                                 ;
        xor       ebx, ebx                                      ;
        mov       DWORD PTR [228+esp], 1                        ;39.4
        mov       DWORD PTR [36+esp], edx                       ;
        jmp       .B1.19        ; Prob 100%                     ;
                                ; LOE eax ebx
.B1.155:                        ; Preds .B1.4                   ; Infreq
        mov       DWORD PTR [512+esp], 0                        ;32.8
        lea       eax, DWORD PTR [8+esp]                        ;32.8
        mov       DWORD PTR [8+esp], 43                         ;32.8
        mov       DWORD PTR [12+esp], OFFSET FLAT: __STRLITPACK_47 ;32.8
        push      32                                            ;32.8
        push      eax                                           ;32.8
        push      OFFSET FLAT: __STRLITPACK_59.0.1              ;32.8
        push      -2088435968                                   ;32.8
        push      911                                           ;32.8
        lea       edx, DWORD PTR [532+esp]                      ;32.8
        push      edx                                           ;32.8
        call      _for_write_seq_lis                            ;32.8
                                ; LOE
.B1.156:                        ; Preds .B1.155                 ; Infreq
        push      32                                            ;33.5
        xor       eax, eax                                      ;33.5
        push      eax                                           ;33.5
        push      eax                                           ;33.5
        push      -2088435968                                   ;33.5
        push      eax                                           ;33.5
        push      OFFSET FLAT: __STRLITPACK_60                  ;33.5
        call      _for_stop_core                                ;33.5
                                ; LOE
.B1.188:                        ; Preds .B1.156                 ; Infreq
        add       esp, 48                                       ;33.5
        jmp       .B1.5         ; Prob 100%                     ;33.5
                                ; LOE
.B1.157:                        ; Preds .B1.2                   ; Infreq
        mov       DWORD PTR [512+esp], 0                        ;26.8
        lea       eax, DWORD PTR [esp]                          ;26.8
        mov       DWORD PTR [esp], 38                           ;26.8
        mov       DWORD PTR [4+esp], OFFSET FLAT: __STRLITPACK_51 ;26.8
        push      32                                            ;26.8
        push      eax                                           ;26.8
        push      OFFSET FLAT: __STRLITPACK_56.0.1              ;26.8
        push      -2088435968                                   ;26.8
        push      911                                           ;26.8
        lea       edx, DWORD PTR [532+esp]                      ;26.8
        push      edx                                           ;26.8
        call      _for_write_seq_lis                            ;26.8
                                ; LOE
.B1.158:                        ; Preds .B1.157                 ; Infreq
        push      32                                            ;27.5
        xor       eax, eax                                      ;27.5
        push      eax                                           ;27.5
        push      eax                                           ;27.5
        push      -2088435968                                   ;27.5
        push      eax                                           ;27.5
        push      OFFSET FLAT: __STRLITPACK_57                  ;27.5
        call      _for_stop_core                                ;27.5
                                ; LOE
.B1.189:                        ; Preds .B1.158                 ; Infreq
        add       esp, 48                                       ;27.5
        jmp       .B1.3         ; Prob 100%                     ;27.5
        ALIGN     16
                                ; LOE
; mark_end;
_READ_TRAFFIC_FLOW_MODELS ENDP
_TEXT	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
READ_TRAFFIC_FLOW_MODELS$format_pack.0.1	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	30
	DB	0
	DB	32
	DB	32
	DB	32
	DB	32
	DB	68
	DB	69
	DB	78
	DB	83
	DB	73
	DB	84
	DB	89
	DB	32
	DB	32
	DB	32
	DB	83
	DB	80
	DB	69
	DB	69
	DB	68
	DB	32
	DB	32
	DB	32
	DB	32
	DB	32
	DB	70
	DB	76
	DB	79
	DB	87
	DB	32
	DB	32
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	3
	DB	3
	DB	0
	DB	0
	DB	0
	DB	10
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	3
	DB	3
	DB	0
	DB	0
	DB	0
	DB	10
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	3
	DB	3
	DB	0
	DB	0
	DB	0
	DB	10
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	3
	DB	3
	DB	0
	DB	0
	DB	0
	DB	10
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	3
	DB	3
	DB	0
	DB	0
	DB	0
	DB	10
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	3
	DB	3
	DB	0
	DB	0
	DB	0
	DB	10
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	3
	DB	3
	DB	0
	DB	0
	DB	0
	DB	10
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	3
	DB	3
	DB	0
	DB	0
	DB	0
	DB	10
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
	DB	54
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	46
	DB	0
	DB	84
	DB	104
	DB	101
	DB	32
	DB	116
	DB	114
	DB	97
	DB	102
	DB	102
	DB	105
	DB	99
	DB	32
	DB	102
	DB	108
	DB	111
	DB	119
	DB	32
	DB	109
	DB	111
	DB	100
	DB	101
	DB	108
	DB	32
	DB	115
	DB	101
	DB	116
	DB	116
	DB	105
	DB	110
	DB	103
	DB	32
	DB	102
	DB	111
	DB	114
	DB	32
	DB	102
	DB	108
	DB	111
	DB	119
	DB	32
	DB	109
	DB	111
	DB	100
	DB	101
	DB	108
	DB	32
	DB	0
	DB	0
	DB	36
	DB	0
	DB	0
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	4
	DB	0
	DB	0
	DB	0
	DB	28
	DB	0
	DB	54
	DB	0
	DB	32
	DB	105
	DB	115
	DB	32
	DB	108
	DB	105
	DB	107
	DB	101
	DB	108
	DB	121
	DB	32
	DB	116
	DB	111
	DB	32
	DB	112
	DB	114
	DB	111
	DB	100
	DB	117
	DB	99
	DB	101
	DB	32
	DB	101
	DB	120
	DB	99
	DB	101
	DB	115
	DB	115
	DB	105
	DB	118
	DB	101
	DB	32
	DB	115
	DB	101
	DB	114
	DB	118
	DB	105
	DB	99
	DB	101
	DB	32
	DB	102
	DB	108
	DB	111
	DB	119
	DB	32
	DB	114
	DB	97
	DB	116
	DB	101
	DB	32
	DB	97
	DB	116
	DB	58
	DB	32
	DB	0
	DB	0
	DB	33
	DB	0
	DB	0
	DB	1
	DB	1
	DB	0
	DB	0
	DB	0
	DB	5
	DB	0
	DB	0
	DB	0
	DB	55
	DB	0
	DB	0
	DB	0
_DATA	ENDS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	ALIGN 004H
__STRLITPACK_55.0.1	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	26
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_56.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_58.0.1	DB	56
	DB	4
	DB	13
	DB	0
	DB	56
	DB	4
	DB	26
	DB	0
	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_59.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_61.0.1	DB	1
	DB	0
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_62.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_63.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_64.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_65.0.1	DB	7
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_66.0.1	DB	9
	DB	3
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_67.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_68.0.1	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_72.0.1	DB	9
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_73.0.1	DB	9
	DB	3
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_69.0.1	DB	9
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_70.0.1	DB	9
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_71.0.1	DB	9
	DB	3
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_124.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_125.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_126.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_112.0.1	DB	9
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_113.0.1	DB	9
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_114.0.1	DB	9
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_115.0.1	DB	9
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_116.0.1	DB	26
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_117.0.1	DB	26
	DB	3
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_118.0.1	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_119.0.1	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_120.0.1	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_121.0.1	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_122.0.1	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_123.0.1	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_100.0.1	DB	9
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_101.0.1	DB	9
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_102.0.1	DB	9
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_103.0.1	DB	9
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_104.0.1	DB	26
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_105.0.1	DB	26
	DB	3
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_106.0.1	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_107.0.1	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_108.0.1	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_109.0.1	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_110.0.1	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_111.0.1	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_96.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_97.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_98.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_85.0.1	DB	9
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_86.0.1	DB	9
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_87.0.1	DB	9
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_88.0.1	DB	9
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_89.0.1	DB	26
	DB	3
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_90.0.1	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_91.0.1	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_92.0.1	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_93.0.1	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_94.0.1	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_95.0.1	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_74.0.1	DB	9
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_75.0.1	DB	9
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_76.0.1	DB	9
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_77.0.1	DB	9
	DB	3
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_78.0.1	DB	26
	DB	3
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_79.0.1	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_80.0.1	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_81.0.1	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_82.0.1	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_83.0.1	DB	26
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_84.0.1	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_128.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_129.0.1	DB	26
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_130.0.1	DB	56
	DB	4
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_131.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_132.0.1	DB	9
	DB	1
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_133.0.1	DB	56
	DB	4
	DB	2
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_134.0.1	DB	9
	DB	1
	DB	1
	DB	0
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_135.0.1	DB	1
	DB	0
	DB	0
	DB	0
	DB	0
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
_DATA	ENDS
; -- End  _READ_TRAFFIC_FLOW_MODELS
_RDATA	SEGMENT READONLY DWORD PUBLIC FLAT  'DATA'
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_54	DB	84
	DB	114
	DB	97
	DB	102
	DB	102
	DB	105
	DB	99
	DB	70
	DB	108
	DB	111
	DB	119
	DB	77
	DB	111
	DB	100
	DB	101
	DB	108
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_53	DB	111
	DB	108
	DB	100
	DB	0
__STRLITPACK_51	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	119
	DB	104
	DB	101
	DB	110
	DB	32
	DB	111
	DB	112
	DB	101
	DB	110
	DB	105
	DB	110
	DB	103
	DB	32
	DB	84
	DB	114
	DB	102
	DB	102
	DB	105
	DB	99
	DB	70
	DB	108
	DB	111
	DB	119
	DB	77
	DB	111
	DB	100
	DB	101
	DB	108
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_57	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_50	DB	67
	DB	104
	DB	101
	DB	99
	DB	107
	DB	84
	DB	114
	DB	97
	DB	102
	DB	102
	DB	105
	DB	99
	DB	70
	DB	108
	DB	111
	DB	119
	DB	77
	DB	111
	DB	100
	DB	101
	DB	108
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_49	DB	117
	DB	110
	DB	107
	DB	110
	DB	111
	DB	119
	DB	110
	DB	0
__STRLITPACK_47	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	119
	DB	104
	DB	101
	DB	110
	DB	32
	DB	111
	DB	112
	DB	101
	DB	110
	DB	105
	DB	110
	DB	103
	DB	32
	DB	67
	DB	104
	DB	101
	DB	99
	DB	107
	DB	84
	DB	114
	DB	102
	DB	102
	DB	105
	DB	99
	DB	70
	DB	108
	DB	111
	DB	119
	DB	77
	DB	111
	DB	100
	DB	101
	DB	108
	DB	46
	DB	100
	DB	97
	DB	116
	DB	0
__STRLITPACK_60	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_42	DB	85
	DB	83
	DB	69
	DB	32
	DB	76
	DB	73
	DB	78
	DB	75
	DB	32
	DB	35
	DB	32
	DB	0
__STRLITPACK_41	DB	83
	DB	80
	DB	69
	DB	69
	DB	68
	DB	32
	DB	76
	DB	73
	DB	77
	DB	73
	DB	84
	DB	58
	DB	32
	DB	0
	DB 2 DUP ( 0H)	; pad
__STRLITPACK_39	DB	84
	DB	82
	DB	65
	DB	70
	DB	70
	DB	73
	DB	67
	DB	32
	DB	70
	DB	76
	DB	79
	DB	87
	DB	32
	DB	77
	DB	79
	DB	68
	DB	69
	DB	76
	DB	32
	DB	35
	DB	58
	DB	32
	DB	0
	DB 1 DUP ( 0H)	; pad
__STRLITPACK_15	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	84
	DB	114
	DB	97
	DB	102
	DB	102
	DB	105
	DB	99
	DB	32
	DB	70
	DB	108
	DB	111
	DB	119
	DB	32
	DB	77
	DB	111
	DB	100
	DB	101
	DB	108
	DB	32
	DB	84
	DB	121
	DB	112
	DB	101
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_13	DB	67
	DB	117
	DB	114
	DB	114
	DB	101
	DB	110
	DB	108
	DB	116
	DB	121
	DB	44
	DB	32
	DB	111
	DB	110
	DB	108
	DB	121
	DB	32
	DB	116
	DB	119
	DB	111
	DB	32
	DB	116
	DB	121
	DB	112
	DB	101
	DB	115
	DB	32
	DB	97
	DB	114
	DB	101
	DB	32
	DB	115
	DB	117
	DB	112
	DB	112
	DB	111
	DB	114
	DB	116
	DB	101
	DB	100
	DB	0
__STRLITPACK_11	DB	80
	DB	108
	DB	101
	DB	97
	DB	115
	DB	101
	DB	32
	DB	115
	DB	101
	DB	101
	DB	32
	DB	117
	DB	115
	DB	101
	DB	114
	DB	39
	DB	115
	DB	32
	DB	109
	DB	97
	DB	110
	DB	117
	DB	97
	DB	108
	DB	32
	DB	102
	DB	111
	DB	114
	DB	32
	DB	100
	DB	101
	DB	116
	DB	97
	DB	105
	DB	108
	DB	115
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_127	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_29	DB	69
	DB	114
	DB	114
	DB	111
	DB	114
	DB	32
	DB	105
	DB	110
	DB	32
	DB	84
	DB	114
	DB	97
	DB	102
	DB	102
	DB	105
	DB	99
	DB	32
	DB	70
	DB	108
	DB	111
	DB	119
	DB	32
	DB	77
	DB	111
	DB	100
	DB	101
	DB	108
	DB	32
	DB	84
	DB	121
	DB	112
	DB	101
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_27	DB	67
	DB	117
	DB	114
	DB	114
	DB	101
	DB	110
	DB	108
	DB	116
	DB	121
	DB	44
	DB	32
	DB	111
	DB	110
	DB	108
	DB	121
	DB	32
	DB	116
	DB	119
	DB	111
	DB	32
	DB	116
	DB	121
	DB	112
	DB	101
	DB	115
	DB	32
	DB	97
	DB	114
	DB	101
	DB	32
	DB	115
	DB	117
	DB	112
	DB	112
	DB	111
	DB	114
	DB	116
	DB	101
	DB	100
	DB	0
__STRLITPACK_25	DB	80
	DB	108
	DB	101
	DB	97
	DB	115
	DB	101
	DB	32
	DB	115
	DB	101
	DB	101
	DB	32
	DB	117
	DB	115
	DB	101
	DB	114
	DB	39
	DB	115
	DB	32
	DB	109
	DB	97
	DB	110
	DB	117
	DB	97
	DB	108
	DB	32
	DB	102
	DB	111
	DB	114
	DB	32
	DB	100
	DB	101
	DB	116
	DB	97
	DB	105
	DB	108
	DB	115
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_99	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_7	DB	84
	DB	104
	DB	101
	DB	32
	DB	116
	DB	114
	DB	97
	DB	102
	DB	102
	DB	105
	DB	99
	DB	32
	DB	102
	DB	108
	DB	111
	DB	119
	DB	32
	DB	109
	DB	111
	DB	100
	DB	101
	DB	108
	DB	32
	DB	105
	DB	115
	DB	32
	DB	104
	DB	105
	DB	103
	DB	104
	DB	101
	DB	114
	DB	32
	DB	116
	DB	104
	DB	97
	DB	110
	DB	32
	DB	116
	DB	104
	DB	101
	DB	32
	DB	109
	DB	97
	DB	120
	DB	32
	DB	115
	DB	101
	DB	116
	DB	117
	DB	112
	DB	0
__STRLITPACK_4	DB	76
	DB	105
	DB	110
	DB	107
	DB	0
	DB 3 DUP ( 0H)	; pad
__STRLITPACK_3	DB	61
	DB	61
	DB	61
	DB	62
	DB	0
	DB 3 DUP ( 0H)	; pad
_2il0floatpacket.4	DD	042700000H
_2il0floatpacket.5	DD	045160000H
_2il0floatpacket.6	DD	045610000H
_2il0floatpacket.7	DD	03f800000H
_RDATA	ENDS
_DATA	SEGMENT  DWORD PUBLIC FLAT  'DATA'
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_NODE_NDE:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_VKFUNC:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_TFLSWITCH:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NOOFFLOWMODEL:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_NDE:BYTE
EXTRN	_DYNUST_NETWORK_MODULE_mp_M_DYNUST_NETWORK_ARC_DE:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_NOOFARCS:BYTE
EXTRN	_DYNUST_MAIN_MODULE_mp_ERROR:BYTE
_DATA	ENDS
EXTRN	_for_dealloc_allocatable:PROC
EXTRN	_for_close:PROC
EXTRN	_for_write_seq_fmt_xmit:PROC
EXTRN	_for_read_seq_lis_xmit:PROC
EXTRN	_for_alloc_allocatable:PROC
EXTRN	_for_check_mult_overflow:PROC
EXTRN	_for_read_seq_lis:PROC
EXTRN	_for_write_seq_lis_xmit:PROC
EXTRN	_for_write_seq_fmt:PROC
EXTRN	_for_stop_core:PROC
EXTRN	_for_write_seq_lis:PROC
EXTRN	_for_open:PROC
EXTRN	_DYNUST_AMS_MODULE_mp_VEHSPEED_UPDATE:PROC
EXTRN	__fltused:BYTE
	INCLUDELIB <ifconsol>
	INCLUDELIB <libifcoremt>
	INCLUDELIB <libifport>
	INCLUDELIB <libmmt>
	INCLUDELIB <LIBCMT>
	INCLUDELIB <libirc>
	INCLUDELIB <svml_dispmt>
	INCLUDELIB <OLDNAMES>
	END
